VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmConsOMAnte 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. �rdenes M�dicas anteriores"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   35
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton CmdOM 
      Caption         =   "Pedir OM"
      Height          =   615
      Left            =   9960
      TabIndex        =   64
      Top             =   5400
      Width           =   1575
   End
   Begin VB.CheckBox chkMasOM 
      Caption         =   "M�s OM"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10800
      TabIndex        =   63
      Top             =   720
      Width           =   1095
   End
   Begin VB.CommandButton cmdtraer 
      Caption         =   "Copiar L�neas Seleccionadas"
      Height          =   615
      Left            =   9960
      TabIndex        =   62
      Top             =   4680
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Productos"
      Height          =   375
      Left            =   0
      TabIndex        =   45
      Top             =   0
      Width           =   855
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "OM / PRN anteriores"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Index           =   1
      Left            =   0
      TabIndex        =   36
      Top             =   480
      Width           =   10500
      Begin TabDlg.SSTab tabTab1 
         Height          =   3375
         Index           =   0
         Left            =   120
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   360
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   5953
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0115SEG.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblLabel1(28)"
         Tab(0).Control(1)=   "lblLabel1(14)"
         Tab(0).Control(2)=   "tab1"
         Tab(0).Control(3)=   "txtText1(0)"
         Tab(0).Control(4)=   "txtText1(1)"
         Tab(0).Control(5)=   "chkCheck1(12)"
         Tab(0).Control(6)=   "chkCheck1(0)"
         Tab(0).Control(7)=   "chkCheck1(2)"
         Tab(0).Control(8)=   "chkCheck1(7)"
         Tab(0).Control(9)=   "chkCheck1(15)"
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0115SEG.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Inter�s Cient�fico"
            DataField       =   "FR66INDINTERCIENT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   -66960
            TabIndex        =   13
            Tag             =   "Inter�s Cient�fico?"
            Top             =   360
            Width           =   1815
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Restricci�n volumen"
            DataField       =   "FR66INDRESTVOLUM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   -66960
            TabIndex        =   12
            Tag             =   "Restricci�n de Volumen?"
            Top             =   120
            Width           =   2175
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Medicaci�n Infantil"
            DataField       =   "FR66INDMEDINF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   -69240
            TabIndex        =   11
            Tag             =   "Medicaci�n Infantil?"
            Top             =   360
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Diab�tico"
            DataField       =   "FR66INDPACIDIABET"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -69240
            TabIndex        =   10
            Tag             =   "Diab�tico?"
            Top             =   120
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Orden M�dica"
            DataField       =   "FR66INDOM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   -71520
            TabIndex        =   9
            Tag             =   "Orden M�dica?"
            Top             =   360
            Width           =   1695
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR66CODOMORIPRN"
            Height          =   330
            Index           =   1
            Left            =   -73320
            TabIndex        =   24
            Tag             =   "C�digo OM origen PRN"
            Top             =   360
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR66CODPETICION"
            Height          =   330
            Index           =   0
            Left            =   -74880
            TabIndex        =   22
            Tag             =   "C�digo Petici�n"
            Top             =   360
            Width           =   1100
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   38
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3345
            Index           =   2
            Left            =   120
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   0
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   5900
            _StockProps     =   79
         End
         Begin TabDlg.SSTab tab1 
            Height          =   2415
            Left            =   -74880
            TabIndex        =   41
            TabStop         =   0   'False
            Top             =   840
            Width           =   10095
            _ExtentX        =   17806
            _ExtentY        =   4260
            _Version        =   327681
            Style           =   1
            Tabs            =   4
            Tab             =   1
            TabsPerRow      =   4
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Paciente"
            TabPicture(0)   =   "FR0115SEG.frx":0038
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "txtText1(12)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "txtText1(24)"
            Tab(0).Control(2)=   "txtText1(23)"
            Tab(0).Control(3)=   "txtText1(3)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "txtText1(2)"
            Tab(0).Control(5)=   "txtText1(4)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtText1(5)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(4)"
            Tab(0).Control(8)=   "lblLabel1(7)"
            Tab(0).Control(9)=   "lblLabel1(8)"
            Tab(0).Control(10)=   "lblLabel1(9)"
            Tab(0).ControlCount=   11
            TabCaption(1)   =   "M�dico / Enfermera"
            TabPicture(1)   =   "FR0115SEG.frx":0054
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "lblLabel1(6)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "lblLabel1(10)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "lblLabel1(0)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "lblLabel1(22)"
            Tab(1).Control(3).Enabled=   0   'False
            Tab(1).Control(4)=   "lblLabel1(2)"
            Tab(1).Control(4).Enabled=   0   'False
            Tab(1).Control(5)=   "lblLabel1(18)"
            Tab(1).Control(5).Enabled=   0   'False
            Tab(1).Control(6)=   "lblLabel1(5)"
            Tab(1).Control(6).Enabled=   0   'False
            Tab(1).Control(7)=   "lblLabel1(20)"
            Tab(1).Control(7).Enabled=   0   'False
            Tab(1).Control(8)=   "lblLabel1(27)"
            Tab(1).Control(8).Enabled=   0   'False
            Tab(1).Control(9)=   "dtcDateCombo1(2)"
            Tab(1).Control(9).Enabled=   0   'False
            Tab(1).Control(10)=   "dtcDateCombo1(0)"
            Tab(1).Control(10).Enabled=   0   'False
            Tab(1).Control(11)=   "dtcDateCombo1(1)"
            Tab(1).Control(11).Enabled=   0   'False
            Tab(1).Control(12)=   "txtText1(22)"
            Tab(1).Control(12).Enabled=   0   'False
            Tab(1).Control(13)=   "txtText1(19)"
            Tab(1).Control(13).Enabled=   0   'False
            Tab(1).Control(14)=   "txtText1(11)"
            Tab(1).Control(14).Enabled=   0   'False
            Tab(1).Control(15)=   "txtText1(10)"
            Tab(1).Control(15).Enabled=   0   'False
            Tab(1).Control(16)=   "txtText1(21)"
            Tab(1).Control(16).Enabled=   0   'False
            Tab(1).Control(17)=   "txtText1(20)"
            Tab(1).Control(17).Enabled=   0   'False
            Tab(1).Control(18)=   "txtText1(9)"
            Tab(1).Control(18).Enabled=   0   'False
            Tab(1).Control(19)=   "txtText1(8)"
            Tab(1).Control(19).Enabled=   0   'False
            Tab(1).Control(20)=   "txtText1(7)"
            Tab(1).Control(20).Enabled=   0   'False
            Tab(1).Control(21)=   "txtText1(6)"
            Tab(1).Control(21).Enabled=   0   'False
            Tab(1).ControlCount=   22
            TabCaption(2)   =   "Servicio "
            TabPicture(2)   =   "FR0115SEG.frx":0070
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLabel1(3)"
            Tab(2).Control(1)=   "lblLabel1(1)"
            Tab(2).Control(2)=   "txtText1(17)"
            Tab(2).Control(3)=   "txtText1(16)"
            Tab(2).Control(4)=   "txtText1(14)"
            Tab(2).Control(5)=   "txtText1(15)"
            Tab(2).Control(5).Enabled=   0   'False
            Tab(2).ControlCount=   6
            TabCaption(3)   =   "Observaciones"
            TabPicture(3)   =   "FR0115SEG.frx":008C
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "lblLabel1(24)"
            Tab(3).Control(1)=   "txtText1(18)"
            Tab(3).ControlCount=   2
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   12
               Left            =   -71040
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Cama"
               Top             =   1440
               Width           =   645
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   24
               Left            =   -72960
               TabIndex        =   21
               Tag             =   "C�digo Paciente"
               Top             =   1440
               Width           =   1395
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   23
               Left            =   -74760
               TabIndex        =   19
               Tag             =   "C�digo Paciente"
               Top             =   1440
               Width           =   1395
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   3
               Left            =   -72960
               TabIndex        =   18
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   960
               Width           =   2205
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   2
               Left            =   -74760
               TabIndex        =   17
               Tag             =   "C�digo Paciente"
               Top             =   960
               Width           =   1395
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   4
               Left            =   -70320
               TabIndex        =   20
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Paciente"
               Top             =   960
               Width           =   2445
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   -67680
               TabIndex        =   23
               TabStop         =   0   'False
               Tag             =   "Apellido 2� Paciente"
               Top             =   960
               Width           =   2445
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_ENF"
               Height          =   330
               Index           =   6
               Left            =   120
               TabIndex        =   25
               Tag             =   "C�digo Enfermera"
               Top             =   600
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   7
               Left            =   2280
               TabIndex        =   26
               TabStop         =   0   'False
               Tag             =   "Nombre Enfermera"
               Top             =   600
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_MED"
               Height          =   330
               Index           =   8
               Left            =   120
               TabIndex        =   2
               Tag             =   "C�digo Doctor"
               Top             =   1200
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   9
               Left            =   2280
               TabIndex        =   3
               Tag             =   "Apellido Doctor"
               Top             =   1200
               Width           =   3285
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMMEDI"
               Height          =   330
               Index           =   20
               Left            =   8040
               TabIndex        =   28
               Tag             =   "hora Firma M�dico"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMENF"
               Height          =   330
               Index           =   21
               Left            =   8040
               TabIndex        =   29
               Tag             =   "Hora Firma Enfermera"
               Top             =   1200
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAREDACCI"
               Height          =   330
               Index           =   10
               Left            =   2040
               TabIndex        =   30
               Tag             =   "Hora Redacci�n"
               Top             =   1920
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   11
               Left            =   4920
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Desc. Urgencia"
               Top             =   1920
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   19
               Left            =   3960
               TabIndex        =   27
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Enfermera"
               Top             =   600
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR91CODURGENCIA"
               Height          =   330
               Index           =   22
               Left            =   3840
               TabIndex        =   7
               Tag             =   "C�digo Urgencia"
               Top             =   1920
               Width           =   735
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   15
               Left            =   -72600
               TabIndex        =   16
               TabStop         =   0   'False
               Tag             =   "Desc.Servicio Cargo"
               Top             =   1440
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO_CRG"
               Height          =   330
               Index           =   14
               Left            =   -74760
               TabIndex        =   15
               Tag             =   "C�d.Servicio Cargo"
               Top             =   1440
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   16
               Left            =   -74760
               TabIndex        =   4
               Tag             =   "C�d.Servicio"
               Top             =   720
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   -72600
               TabIndex        =   5
               Tag             =   "Desc.Servicio"
               Top             =   720
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66OBSERV"
               Height          =   1650
               Index           =   18
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   31
               Tag             =   "Observaciones"
               Top             =   600
               Width           =   9525
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   4425
               Index           =   3
               Left            =   -74880
               TabIndex        =   42
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1305
               Index           =   4
               Left            =   -74880
               TabIndex        =   43
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMMEDI"
               Height          =   330
               Index           =   1
               Left            =   5880
               TabIndex        =   1
               Tag             =   "Fecha Firma M�dico"
               Top             =   600
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMENF"
               Height          =   330
               Index           =   0
               Left            =   5880
               TabIndex        =   14
               Tag             =   "Fecha Firma Enfermera"
               Top             =   1200
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECREDACCION"
               Height          =   330
               Index           =   2
               Left            =   120
               TabIndex        =   0
               Tag             =   "Fecha Redacci�n"
               Top             =   1920
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Paciente"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   -74760
               TabIndex        =   61
               Top             =   720
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   -72960
               TabIndex        =   60
               Top             =   720
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "1� Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   -70320
               TabIndex        =   59
               Top             =   720
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "2� Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   -67680
               TabIndex        =   58
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   120
               TabIndex        =   57
               Top             =   360
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   120
               TabIndex        =   56
               Top             =   960
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   5880
               TabIndex        =   55
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   8040
               TabIndex        =   54
               Top             =   360
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   5880
               TabIndex        =   53
               Top             =   960
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   8040
               TabIndex        =   52
               Top             =   960
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   51
               Top             =   1680
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   2040
               TabIndex        =   50
               Top             =   1680
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   3840
               TabIndex        =   49
               Top             =   1680
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -74760
               TabIndex        =   48
               Top             =   480
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio Cargo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   -74760
               TabIndex        =   47
               Top             =   1200
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   -74880
               TabIndex        =   46
               Top             =   360
               Width           =   2655
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.OM origen PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   -73320
            TabIndex        =   44
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   -74880
            TabIndex        =   40
            Top             =   120
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Medicamentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3585
      Index           =   0
      Left            =   0
      TabIndex        =   33
      Top             =   4440
      Width           =   9735
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3075
         Index           =   0
         Left            =   120
         TabIndex        =   34
         Top             =   360
         Width           =   9420
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   3
         stylesets(0).Name=   "rojo"
         stylesets(0).ForeColor=   255
         stylesets(0).Picture=   "FR0115SEG.frx":00A8
         stylesets(1).Name=   "negro"
         stylesets(1).ForeColor=   0
         stylesets(1).Picture=   "FR0115SEG.frx":00C4
         stylesets(2).Name=   "azul"
         stylesets(2).ForeColor=   16711680
         stylesets(2).Picture=   "FR0115SEG.frx":00E0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   16616
         _ExtentY        =   5424
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   32
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmConsOMAnte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmConsOMAnte (FR0115.FRM)                                   *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: Consultar Ordenes M�dicas anteriores                    *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub chkMasOM_Click()
   Screen.MousePointer = vbHourglass
   If chkMasOM.Value = vbChecked Then
      Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
      objWinInfo.objWinActiveForm.intCursorSize = 100
      objWinInfo.DataRefresh
      Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
   Else
      Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
      objWinInfo.objWinActiveForm.intCursorSize = 3
      objWinInfo.DataRefresh
      Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
   End If
   Screen.MousePointer = vbDefault
End Sub

Private Sub CmdOM_Click()
  CmdOM.Enabled = False
  Call objsecurity.LaunchProcess("FR0111")
  CmdOM.Enabled = True
End Sub

Private Sub cmdtraer_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mensaje As String

  cmdtraer.Enabled = False
  If objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
      'Guardamos el n�mero de filas seleccionadas
      mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
      ReDim gintprodbuscado(mintNTotalSelRows, 11)
      gintprodtotal = mintNTotalSelRows
      For mintisel = 0 To mintNTotalSelRows - 1
          'Guardamos el n�mero de fila que est� seleccionada
           mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
           gintprodbuscado(mintisel, 0) = "" & grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk) & "" 'c�digo petici�n
           gintprodbuscado(mintisel, 1) = grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk) 'n�m.l�nea
           gintprodbuscado(mintisel, 2) = grdDBGrid1(0).Columns(6).CellValue(mvarBkmrk) 'c�d.producto
           gintprodbuscado(mintisel, 3) = grdDBGrid1(0).Columns(5).CellValue(mvarBkmrk) 'c�d.operacion
           gintprodbuscado(mintisel, 4) = grdDBGrid1(0).Columns(24).CellValue(mvarBkmrk) 'c�d.producto_dil
           
           gintprodbuscado(mintisel, 5) = grdDBGrid1(0).Columns(7).CellValue(mvarBkmrk) 'INT
           gintprodbuscado(mintisel, 6) = grdDBGrid1(0).Columns(9).CellValue(mvarBkmrk) 'DESC
           gintprodbuscado(mintisel, 7) = grdDBGrid1(0).Columns(13).CellValue(mvarBkmrk) 'CANT
           gintprodbuscado(mintisel, 8) = grdDBGrid1(0).Columns(11).CellValue(mvarBkmrk) 'DOSIS
           gintprodbuscado(mintisel, 9) = grdDBGrid1(0).Columns(12).CellValue(mvarBkmrk) 'UM
           gintprodbuscado(mintisel, 10) = grdDBGrid1(0).Columns(8).CellValue(mvarBkmrk) 'REF
           gintprodbuscado(mintisel, 11) = grdDBGrid1(0).Columns(10).CellValue(mvarBkmrk) 'FF
           
      Next mintisel
      cmdtraer.Enabled = True
      cmdtraer.Enabled = True
      'Unload Me
      gintredactarorden = 1
      If gstrLlamadorProd = "frmPedirPrn" Then
        Unload Me
      Else
        Call objsecurity.LaunchProcess("FR0111")
      End If
  Else
      mensaje = MsgBox("No ha seleccionado ning�n medicamento", vbInformation, "Aviso")
      cmdtraer.Enabled = True
      'Unload Me
  End If
End Sub

Private Sub Form_Activate()
If gstrLlamadorProd = "frmPedirPrn" Then
  CmdOM.Visible = False
End If
'se va al primer registro para refrescar el grid
Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
Call objWinInfo.FormChangeActive(fraframe1(1), False, True)

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .intCursorSize = 3
    
    .strName = "OM"
      
    .strTable = "FR6600"
    
    .strWhere = "CI21CODPERSONA=" & glngselpaciente
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR66CODPETICION", cwDescending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "OM")
    Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR66FECREDACCION", "Fecha Redacci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDMEDINF", "Medicaci�n Infantil?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
   
    'Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "C�digo Grupo Protocolo")
  
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = fraframe1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Detalle OM"
    
    .strTable = "FR2800"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    Call .FormAddRelation("FR66CODPETICION", txtText1(0))
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�d", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "OP", "FR28OPERACION", cwString, 1)
    Call .GridAddColumn(objMultiInfo, "C�d.Medic", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�digo", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Medicamento", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "FF", "", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR28DOSIS", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Medida", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "FR28CANTIDAD", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "PRN", "FR28INDDISPPRN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Frecuencia", "FRG4CODFRECUENCIA", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Periodicidad", "FRH5CODPERIODICIDAD", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Fecha Inicio", "FR28FECINICIO", cwDate)
    Call .GridAddColumn(objMultiInfo, "Hora Inicio", "FR28HORAINICIO", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Fecha Fin", "FR28FECFIN", cwDate)
    Call .GridAddColumn(objMultiInfo, "Hora Fin", "FR28HORAFIN", cwNumeric, 2)
    'Call .GridAddColumn(objMultiInfo, "Minuto Fin", "FR28MINFIN", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Inmediato", "FR28INDCOMIENINMED", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "SN", "FR28INDSN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Prod_Dil", "FR73CODPRODUCTO_DIL", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Diluyente", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Volumen Dil", "FR28CANTIDADDIL", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Tiempo Inf", "FR28TIEMMININF", cwNumeric, 5)
    'Call .GridAddColumn(objMultiInfo, "Notas", "FR28NOTASCOMP", cwString, 2000)
    'Call .GridAddColumn(objMultiInfo, "Cod Producto Diluir", "FR73CODPRODUCTO_DIL", cwNumeric, 9)
    'Call .GridAddColumn(objMultiInfo, "Producto Diluir", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Instrucciones Administraci�n", "FR28INSTRADMIN", cwString, 2000)
    Call .GridAddColumn(objMultiInfo, "Perfusi�n", "FR28VELPERFUSION", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "U.M.Perf.1", "", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "U.M.Perf.2", "", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Vol.Total", "FR28VOLTOTAL", cwString, 2000)
    Call .GridAddColumn(objMultiInfo, "STAT?", "FR28INDCOMIENINMED", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "PERF?", "FR28INDPERF", cwBoolean, 5)
    Call .GridAddColumn(objMultiInfo, "Cod Medicamento 2", "FR73CODPRODUCTO_2", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Medicamento 2", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "FF2", "FRH7CODFORMFAR_2", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis 2", "FR28DOSIS_2", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "UM2", "FR93CODUNIMEDIDA_2", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Vol.2", "FR28VOLUMEN_2", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "Medic no Formulario", "FR28DESPRODUCTO", cwString, 50)
    
    Call .FormCreateInfo(objMasterInfo)
    

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(txtText1(21)).blnInFind = True
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(12)).blnInFind = True
    .CtrlGetInfo(chkCheck1(15)).blnInFind = True
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(14)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(13)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(15)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(16)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(17)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(18)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(19)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(20)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(21)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(22)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(23)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(24)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(26)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(27)).blnInFind = True
    
    
    
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "CI22NOMBRE")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(23)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(23)), txtText1(4), "CI22PRIAPEL")
   
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(24)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(5), "CI22SEGAPEL")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "SG02NOM")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(19), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(11), "FR91DESURGENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(15), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(7), "FR73CODINTFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(9), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(8), "FR73REFERENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(10), "FRH7CODFORMFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(24)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(24)), grdDBGrid1(0).Columns(25), "FR73DESPRODUCTO")

    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(35)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(35)), grdDBGrid1(0).Columns(36), "FR73DESPRODUCTO")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(24)), "CI21CODPERSONA", "SELECT * FROM FR2200J WHERE FR2200J.CI21CODPERSONA= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(12), "AD15CODCAMA")
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
grdDBGrid1(0).Columns(3).Visible = False
grdDBGrid1(0).Columns(4).Visible = False
grdDBGrid1(0).Columns(6).Visible = False
'grdDBGrid1(0).Columns(7).Visible = False
grdDBGrid1(0).Columns(26).Visible = False
grdDBGrid1(0).Columns(30).Visible = False
grdDBGrid1(0).Columns(31).Visible = False


grdDBGrid1(2).Columns(1).Width = 1600
grdDBGrid1(2).Columns(2).Width = 1600
grdDBGrid1(2).Columns(5).Width = 0
grdDBGrid1(2).Columns(7).Width = 0
grdDBGrid1(2).Columns(15).Width = 0
grdDBGrid1(2).Columns(19).Width = 0
grdDBGrid1(2).Columns(21).Width = 0


grdDBGrid1(0).Columns(9).Width = 3500
grdDBGrid1(0).Columns(15).Width = 1000
grdDBGrid1(0).Columns(5).Width = 0
grdDBGrid1(0).Columns(14).Width = 600
grdDBGrid1(0).Columns(8).Width = 1000
grdDBGrid1(0).Columns(11).Width = 700
grdDBGrid1(0).Columns(10).Width = 600
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
   Dim i As Integer
   Dim strEstiloCelda1 As String
   Dim strHoy As String
   Dim strFechaFin As String
   Dim rstfecha As rdoResultset
     
   Set rstfecha = objApp.rdoConnect.OpenResultset("SELECT TO_CHAR(SYSDATE,'YYYYMMDD') FROM DUAL")
   strHoy = rstfecha(0).Value
   Set rstfecha = Nothing

   strFechaFin = Format(grdDBGrid1(0).Columns(20).Value, "yyyymmdd")
   If Index = 0 Then
      If strFechaFin = "" Or strFechaFin > strHoy Then
         strEstiloCelda1 = "negro"
      ElseIf strFechaFin = strHoy Then
         strEstiloCelda1 = "azul"
      ElseIf strFechaFin < strHoy Then
         strEstiloCelda1 = "rojo"
      End If
      For i = 0 To grdDBGrid1(0).Cols - 1
         grdDBGrid1(0).Columns(i).CellStyleSet strEstiloCelda1
      Next i
   End If

End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strFormName = "OM" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "CI2200"

     Set objField = .AddField("CI21CODPERSONA")
     objField.strSmallDesc = "C�digo Paciente"

     Set objField = .AddField("CI22NOMBRE")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("CI22PRIAPEL")
     objField.strSmallDesc = "Primer Apellido"
     
     Set objField = .AddField("CI22SEGAPEL")
     objField.strSmallDesc = "Segundo Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("CI21CODPERSONA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo Enfermera"

     Set objField = .AddField("SG02NOM")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(6), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo M�dico"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "OM" And strCtrl = "txtText1(14)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD32CODTIPODPTO=3 AND " & _
           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Servicio"

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Servicio"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(14), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(16)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD32CODTIPODPTO=3 AND " & _
           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Dpto."

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto."

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(16), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(22)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9100"

     Set objField = .AddField("FR91CODURGENCIA")
     objField.strSmallDesc = "C�digo Urgencia"

     Set objField = .AddField("FR91DESURGENCIA")
     objField.strSmallDesc = "Descripci�n Urgencia"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(22), .cllValues("FR91CODURGENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 

 
End Sub



Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
If objWinInfo.objWinActiveForm.strName = "OM" Then
    If btnButton.Index <> 3 Then
        If btnButton.Index = 30 Then
          Unload Me
        Else
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        End If
    End If
Else
    If objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
End If
End Sub
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
If objWinInfo.objWinActiveForm.strName = "OM" Then
    If intIndex <> 20 Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End If
End If
If objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  'tlbToolbar1.Buttons(3).Enabled = False
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


