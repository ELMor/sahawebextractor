VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmPedirServEspeciales 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Pedir Servicios Especiales"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   38
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame FramePassword 
      Caption         =   "Introduzca C�digo y Password del m�dico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   1575
      Left            =   2520
      TabIndex        =   97
      Top             =   2640
      Visible         =   0   'False
      Width           =   6135
      Begin VB.TextBox txtcodigomedico 
         BackColor       =   &H00FFC0FF&
         Height          =   330
         Left            =   600
         MaxLength       =   6
         TabIndex        =   98
         Tag             =   "C�digo M�dico"
         Top             =   840
         Width           =   1275
      End
      Begin VB.TextBox txtpasswordmedico 
         BackColor       =   &H00FFC0FF&
         Height          =   330
         Left            =   2280
         MaxLength       =   6
         TabIndex        =   100
         Tag             =   "Password M�dico"
         Top             =   840
         Width           =   1275
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   4440
         TabIndex        =   102
         Top             =   480
         Width           =   1095
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   4440
         TabIndex        =   103
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d. M�dico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   17
         Left            =   600
         TabIndex        =   101
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Password"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   19
         Left            =   2280
         TabIndex        =   99
         Top             =   600
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdNoReponer 
      Caption         =   "No  Reponer"
      Height          =   375
      Left            =   360
      TabIndex        =   90
      Top             =   7680
      Width           =   1575
   End
   Begin VB.CommandButton cmdgrupoterapmedic 
      Caption         =   "Grp.Terap. Medicamentos"
      Height          =   495
      Left            =   10320
      TabIndex        =   89
      Top             =   6480
      Width           =   1575
   End
   Begin VB.CommandButton cmdgrupoterapprod 
      Caption         =   "Grp.Terap. Productos"
      Height          =   495
      Left            =   10320
      TabIndex        =   88
      Top             =   5880
      Width           =   1575
   End
   Begin VB.CommandButton cmdleerlector 
      Caption         =   "LECTOR"
      Height          =   915
      Left            =   10560
      Picture         =   "FR0192.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   87
      Top             =   7080
      Width           =   975
   End
   Begin VB.CommandButton cmdbusprotquirofano 
      Caption         =   "Protocolos"
      Height          =   375
      Left            =   10320
      TabIndex        =   86
      Top             =   5400
      Width           =   1575
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   10680
      Top             =   1560
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdInformacion 
      Caption         =   "&Informaci�n"
      Height          =   375
      Left            =   10320
      TabIndex        =   81
      Top             =   4920
      Width           =   1575
   End
   Begin VB.CommandButton cmdNoForm 
      Caption         =   "Prod. &no formulario"
      Height          =   375
      Left            =   10320
      TabIndex        =   79
      Top             =   2640
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdVerPRN 
      Caption         =   "&Ver PRN"
      Height          =   375
      Left            =   8400
      TabIndex        =   78
      Top             =   7680
      Width           =   1575
   End
   Begin VB.CommandButton cmdVerCesta 
      Caption         =   "Ver &Cesta"
      Height          =   375
      Left            =   6720
      TabIndex        =   61
      Top             =   7680
      Width           =   1575
   End
   Begin VB.CommandButton CmdCesta 
      Caption         =   "&A�adir a la Cesta"
      Height          =   375
      Left            =   4560
      TabIndex        =   60
      Top             =   7680
      Width           =   1575
   End
   Begin VB.CommandButton cmdEnviar 
      Caption         =   "&Enviar PRN"
      Height          =   375
      Left            =   2880
      TabIndex        =   59
      Top             =   7680
      Width           =   1575
   End
   Begin VB.CommandButton CmdUltimaOM 
      Caption         =   "&Ultima OM"
      Height          =   375
      Left            =   10320
      TabIndex        =   58
      Top             =   4440
      Width           =   1575
   End
   Begin VB.CommandButton cmdProductos 
      Caption         =   "&Productos"
      Height          =   375
      Left            =   10320
      TabIndex        =   57
      Top             =   3960
      Width           =   1575
   End
   Begin VB.CommandButton cmdGupoProd 
      Caption         =   "&Grupos de  Productos "
      Height          =   495
      Left            =   10320
      TabIndex        =   56
      Top             =   3360
      Width           =   1575
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3600
      Index           =   0
      Left            =   120
      TabIndex        =   54
      Top             =   3960
      Width           =   10095
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3195
         Index           =   0
         Left            =   120
         TabIndex        =   55
         Top             =   360
         Width           =   9825
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   1
         stylesets(0).Name=   "LineaBloqueada"
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "FR0192.frx":0842
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   17330
         _ExtentY        =   5636
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Index           =   1
      Left            =   120
      TabIndex        =   40
      Top             =   480
      Width           =   10140
      Begin TabDlg.SSTab tabTab1 
         Height          =   3015
         Index           =   0
         Left            =   120
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   360
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   5318
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0192.frx":085E
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(28)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(13)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "tab1"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "chkCheck1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "chkCheck1(7)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(25)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(26)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "chkCheck1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "chkCheck1(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "chkCheck1(5)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "chkCheck1(15)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).ControlCount=   12
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0192.frx":087A
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Inter�s Cient�fico"
            DataField       =   "FR66INDINTERCIENT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   7440
            TabIndex        =   4
            Tag             =   "Inter�s Cient�fico?"
            Top             =   120
            Width           =   1815
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Al�rgico"
            DataField       =   "FR66INDALERGIA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   5040
            TabIndex        =   5
            Tag             =   "Alergico?"
            Top             =   360
            Width           =   1815
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "�Cesta?"
            DataField       =   "fr66indcesta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   7440
            TabIndex        =   6
            Tag             =   "�Cesta?"
            Top             =   360
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "OM"
            DataField       =   "FR66INDOM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   7440
            TabIndex        =   8
            Tag             =   "Medicaci�n Infantil?"
            Top             =   600
            Visible         =   0   'False
            Width           =   2055
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   26
            Left            =   2040
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   2685
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   25
            Left            =   1680
            TabIndex        =   1
            Tag             =   "Estado Petici�n"
            Top             =   360
            Width           =   315
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Restricci�n volumen"
            DataField       =   "FR66INDRESTVOLUM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   5040
            TabIndex        =   7
            Tag             =   "Restricci�n de Volumen?"
            Top             =   600
            Width           =   2175
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Diab�tico"
            DataField       =   "FR66INDPACIDIABET"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   5040
            TabIndex        =   3
            Tag             =   "Diab�tico?"
            Top             =   120
            Width           =   1575
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR66CODPETICION"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Petici�n"
            Top             =   360
            Width           =   1100
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   42
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   2
            Left            =   -74880
            TabIndex        =   43
            TabStop         =   0   'False
            Top             =   120
            Width           =   9255
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16325
            _ExtentY        =   5054
            _StockProps     =   79
         End
         Begin TabDlg.SSTab tab1 
            Height          =   2175
            Left            =   120
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   720
            Width           =   9255
            _ExtentX        =   16325
            _ExtentY        =   3836
            _Version        =   327681
            Style           =   1
            Tabs            =   4
            Tab             =   1
            TabsPerRow      =   4
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Paciente"
            TabPicture(0)   =   "FR0192.frx":0896
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "lblLabel1(9)"
            Tab(0).Control(1)=   "lblLabel1(8)"
            Tab(0).Control(2)=   "lblLabel1(7)"
            Tab(0).Control(3)=   "lblLabel1(4)"
            Tab(0).Control(4)=   "lblLabel1(11)"
            Tab(0).Control(5)=   "lblLabel1(14)"
            Tab(0).Control(6)=   "lblLabel1(42)"
            Tab(0).Control(7)=   "lblLabel1(40)"
            Tab(0).Control(8)=   "lblLabel1(39)"
            Tab(0).Control(9)=   "lblLabel1(38)"
            Tab(0).Control(10)=   "lblLabel1(41)"
            Tab(0).Control(11)=   "lblLabel1(3)"
            Tab(0).Control(12)=   "lblLabel1(15)"
            Tab(0).Control(13)=   "txtText1(5)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(4)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(2)"
            Tab(0).Control(16)=   "txtText1(3)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(23)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(1)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "Text1"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(33)"
            Tab(0).Control(21)=   "txtText1(32)"
            Tab(0).Control(22)=   "txtText1(46)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(12)"
            Tab(0).Control(24)=   "txtText1(29)"
            Tab(0).Control(25)=   "txtText1(13)"
            Tab(0).Control(26)=   "txtText1(14)"
            Tab(0).ControlCount=   27
            TabCaption(1)   =   "Servicio"
            TabPicture(1)   =   "FR0192.frx":08B2
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "lblLabel1(1)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "lblLabel1(16)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "lblLabel1(46)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "lblLabel1(12)"
            Tab(1).Control(3).Enabled=   0   'False
            Tab(1).Control(4)=   "txtText1(16)"
            Tab(1).Control(4).Enabled=   0   'False
            Tab(1).Control(5)=   "txtText1(17)"
            Tab(1).Control(5).Enabled=   0   'False
            Tab(1).Control(6)=   "txtText1(15)"
            Tab(1).Control(6).Enabled=   0   'False
            Tab(1).Control(7)=   "txtText1(27)"
            Tab(1).Control(7).Enabled=   0   'False
            Tab(1).Control(8)=   "txtText1(62)"
            Tab(1).Control(8).Enabled=   0   'False
            Tab(1).Control(9)=   "txtText1(61)"
            Tab(1).Control(9).Enabled=   0   'False
            Tab(1).Control(10)=   "txtText1(24)"
            Tab(1).Control(10).Enabled=   0   'False
            Tab(1).ControlCount=   11
            TabCaption(2)   =   "Peticionario"
            TabPicture(2)   =   "FR0192.frx":08CE
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLabel1(27)"
            Tab(2).Control(1)=   "lblLabel1(20)"
            Tab(2).Control(2)=   "lblLabel1(5)"
            Tab(2).Control(3)=   "lblLabel1(18)"
            Tab(2).Control(4)=   "lblLabel1(0)"
            Tab(2).Control(5)=   "lblLabel1(10)"
            Tab(2).Control(6)=   "lblLabel1(22)"
            Tab(2).Control(7)=   "lblLabel1(6)"
            Tab(2).Control(8)=   "lblLabel1(2)"
            Tab(2).Control(9)=   "dtcDateCombo1(2)"
            Tab(2).Control(10)=   "dtcDateCombo1(0)"
            Tab(2).Control(11)=   "dtcDateCombo1(1)"
            Tab(2).Control(12)=   "txtText1(6)"
            Tab(2).Control(13)=   "txtText1(7)"
            Tab(2).Control(13).Enabled=   0   'False
            Tab(2).Control(14)=   "txtText1(8)"
            Tab(2).Control(15)=   "txtText1(20)"
            Tab(2).Control(16)=   "txtText1(21)"
            Tab(2).Control(17)=   "txtText1(10)"
            Tab(2).Control(18)=   "txtText1(11)"
            Tab(2).Control(18).Enabled=   0   'False
            Tab(2).Control(19)=   "txtText1(19)"
            Tab(2).Control(19).Enabled=   0   'False
            Tab(2).Control(20)=   "txtText1(22)"
            Tab(2).Control(21)=   "txtText1(9)"
            Tab(2).ControlCount=   22
            TabCaption(3)   =   "Observaciones"
            TabPicture(3)   =   "FR0192.frx":08EA
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "txtText1(18)"
            Tab(3).Control(1)=   "lblLabel1(24)"
            Tab(3).ControlCount=   2
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_FEN"
               Height          =   330
               Index           =   24
               Left            =   240
               TabIndex        =   104
               Tag             =   "Hora Firma Enfermera"
               Top             =   1560
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO_CRG"
               Height          =   330
               Index           =   61
               Left            =   1440
               TabIndex        =   95
               Tag             =   "C�d.Dpto.Cargo"
               Top             =   1560
               Visible         =   0   'False
               Width           =   360
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   62
               Left            =   1920
               TabIndex        =   94
               Tag             =   "Dpto.Cargo"
               Top             =   1560
               Visible         =   0   'False
               Width           =   3405
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD41CODSECCION"
               Height          =   330
               Index           =   27
               Left            =   5160
               TabIndex        =   92
               Tag             =   "C�d.Secci�n"
               Top             =   840
               Width           =   915
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   15
               Left            =   6240
               TabIndex        =   91
               Tag             =   "Secci�n"
               Top             =   840
               Width           =   2880
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD07CODPROCESO"
               Height          =   330
               Index           =   14
               Left            =   -67440
               TabIndex        =   83
               Tag             =   "Proceso"
               Top             =   1320
               Visible         =   0   'False
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD01CODASISTENCI"
               Height          =   330
               Index           =   13
               Left            =   -66480
               TabIndex        =   82
               Tag             =   "Asistencia"
               Top             =   1320
               Visible         =   0   'False
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   29
               Left            =   -67200
               TabIndex        =   16
               Tag             =   "Superficie Corporal"
               Top             =   840
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Height          =   330
               Index           =   12
               Left            =   -69240
               TabIndex        =   77
               Tag             =   "C�digo Sexo"
               Top             =   1200
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   46
               Left            =   -69840
               TabIndex        =   13
               TabStop         =   0   'False
               Tag             =   "Cama"
               Top             =   840
               Width           =   900
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR66PESOPAC"
               Height          =   330
               Index           =   32
               Left            =   -68880
               MaxLength       =   3
               TabIndex        =   14
               Tag             =   "Peso"
               Top             =   840
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR66ALTUPAC"
               Height          =   330
               Index           =   33
               Left            =   -68160
               MaxLength       =   3
               TabIndex        =   15
               Tag             =   "Altura Cm"
               Top             =   840
               Width           =   600
            End
            Begin VB.TextBox Text1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Left            =   -71640
               TabIndex        =   11
               TabStop         =   0   'False
               Tag             =   "Edad"
               Top             =   840
               Width           =   420
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   1
               Left            =   -71040
               TabIndex        =   12
               TabStop         =   0   'False
               Tag             =   "Sexo"
               Top             =   840
               Width           =   1140
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   9
               Left            =   -73200
               TabIndex        =   23
               Tag             =   "Apellido Doctor"
               Top             =   600
               Width           =   3285
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR91CODURGENCIA"
               Height          =   330
               Index           =   22
               Left            =   -71160
               TabIndex        =   33
               Tag             =   "C�digo Urgencia"
               Top             =   1800
               Width           =   735
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   19
               Left            =   -71520
               TabIndex        =   28
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Enfermera"
               Top             =   1200
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   11
               Left            =   -70080
               TabIndex        =   34
               TabStop         =   0   'False
               Tag             =   "Desc. Urgencia"
               Top             =   1800
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAREDACCI"
               Height          =   330
               Index           =   10
               Left            =   -72840
               TabIndex        =   32
               Tag             =   "Hora Redacci�n"
               Top             =   1800
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMENF"
               Height          =   330
               Index           =   21
               Left            =   -67680
               TabIndex        =   30
               Tag             =   "Hora Firma Enfermera"
               Top             =   1200
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMMEDI"
               Height          =   330
               Index           =   20
               Left            =   -67680
               TabIndex        =   25
               Tag             =   "hora Firma M�dico"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_MED"
               Height          =   330
               Index           =   8
               Left            =   -74880
               TabIndex        =   22
               Tag             =   "C�digo Doctor"
               Top             =   600
               Width           =   1635
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   7
               Left            =   -73200
               TabIndex        =   27
               TabStop         =   0   'False
               Tag             =   "Nombre Enfermera"
               Top             =   1200
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               DataField       =   "sg02cod_enf"
               Height          =   330
               Index           =   6
               Left            =   -74880
               TabIndex        =   26
               Tag             =   "C�digo Enfermera"
               Top             =   1200
               Width           =   1635
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   1200
               TabIndex        =   21
               Tag             =   "Desc.Departamento"
               Top             =   840
               Width           =   3840
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   16
               Left            =   240
               TabIndex        =   20
               Tag             =   "C�d.Departamento"
               Top             =   840
               Width           =   915
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   23
               Left            =   -73200
               TabIndex        =   10
               TabStop         =   0   'False
               Tag             =   "Historia"
               Top             =   840
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   3
               Left            =   -74760
               TabIndex        =   17
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   1680
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   2
               Left            =   -74760
               TabIndex        =   9
               Tag             =   "C�digo Paciente"
               Top             =   840
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   4
               Left            =   -71760
               TabIndex        =   18
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Paciente"
               Top             =   1680
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   -68760
               TabIndex        =   19
               TabStop         =   0   'False
               Tag             =   "Apellido 2� Paciente"
               Top             =   1680
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66OBSERV"
               Height          =   1485
               Index           =   18
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   35
               Tag             =   "Observaciones"
               Top             =   600
               Width           =   9045
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   4425
               Index           =   3
               Left            =   -74880
               TabIndex        =   44
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1305
               Index           =   4
               Left            =   -74880
               TabIndex        =   45
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMMEDI"
               Height          =   330
               Index           =   1
               Left            =   -69840
               TabIndex        =   24
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   600
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMENF"
               Height          =   330
               Index           =   0
               Left            =   -69840
               TabIndex        =   29
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   1200
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECREDACCION"
               Height          =   330
               Index           =   2
               Left            =   -74880
               TabIndex        =   31
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   1800
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.Firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   240
               TabIndex        =   105
               Top             =   1320
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento de Cargo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   46
               Left            =   1440
               TabIndex        =   96
               Top             =   1320
               Visible         =   0   'False
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Secci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   5160
               TabIndex        =   93
               Top             =   600
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Proceso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   -67440
               TabIndex        =   85
               Top             =   1200
               Visible         =   0   'False
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Asistencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   -66720
               TabIndex        =   84
               Top             =   1200
               Visible         =   0   'False
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Superficie Corporal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   41
               Left            =   -67200
               TabIndex        =   80
               Top             =   600
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cama"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   38
               Left            =   -69840
               TabIndex        =   76
               Top             =   600
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Peso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   39
               Left            =   -68880
               TabIndex        =   75
               Top             =   600
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Altura Cm"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   40
               Left            =   -68160
               TabIndex        =   74
               Top             =   600
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Edad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   42
               Left            =   -71640
               TabIndex        =   73
               Top             =   600
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Sexo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   -71040
               TabIndex        =   72
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   -69840
               TabIndex        =   71
               Top             =   960
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   -71160
               TabIndex        =   70
               Top             =   1560
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   -67680
               TabIndex        =   69
               Top             =   960
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   -72840
               TabIndex        =   68
               Top             =   1560
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   -74880
               TabIndex        =   67
               Top             =   1560
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   -67680
               TabIndex        =   66
               Top             =   360
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   -69840
               TabIndex        =   65
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   -74880
               TabIndex        =   64
               Top             =   360
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   -74880
               TabIndex        =   63
               Top             =   960
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio Peticionario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   240
               TabIndex        =   62
               Top             =   600
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Historia "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   -73200
               TabIndex        =   51
               Top             =   600
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Persona"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   -74760
               TabIndex        =   50
               Top             =   600
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   -74760
               TabIndex        =   49
               Top             =   1440
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 1�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   -71760
               TabIndex        =   48
               Top             =   1440
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 2�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   -68760
               TabIndex        =   47
               Top             =   1440
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   -74880
               TabIndex        =   46
               Top             =   360
               Width           =   2655
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   1680
            TabIndex        =   53
            Top             =   120
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   120
            TabIndex        =   52
            Top             =   120
            Width           =   1455
         End
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Productos"
      Height          =   375
      Left            =   0
      TabIndex        =   39
      Top             =   0
      Width           =   855
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   37
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPedirServEspeciales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmPedirPRN(FR0171.FRM)                                      *
'* AUTOR: JUAN CARLOS RUEDA                                             *
'* FECHA: AGOSTO DE 1999                                                *
'* DESCRIPCION: Petici�n PRN                                            *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim blnNoPrimAct As Boolean
Dim mstrCodPac As String
Dim A�adirCesta As Boolean

'Dim mblnChange As Boolean
Private Sub RealizarApuntes()
  'Al realizar la dispensaci�n se almacenan los movimientos, tanto de salida como de entrada
  'FR8000 es la tabla de salidas de almac�n: sacamos del almac�n de Farmacia y lo pasamos al almac�n del servicio
  'FR3500 es la tabla de entradas de almac�n: metemos en el almac�n del Servicio lo sacado de Farmacia
  'FR6500 esla tabla en la que se almacenan lo entregado a un servicio
  
  'Movimientos en Dispensacion 05/02/2000 JRC
  
  Dim strCodDpto As String 'Para guardar el c�digo del Servicio
  Dim strNumNec As String 'Para guardar el n�mero de necesidad
  Dim strFR19 As String
  Dim qryFR19 As rdoQuery
  Dim rdoFR19 As rdoResultset
  Dim strIns As String
  Dim strFR73 As String
  Dim qryFR73 As rdoQuery
  Dim rdoFR73 As rdoResultset
  Dim strTamEnv As String
  Dim strCant As String
  Dim strUpd As String
  Dim qryUpd As rdoQuery
  Dim rdoUpd As rdoResultset
  Dim strCP As String
  Dim blnEntroEnWhile As Boolean
  Dim strCodNec As String
  Dim strCantDil As String
  Dim strCant2 As String
  Dim rstFR66 As rdoResultset
  Dim strFR66 As String
  
  'On Error GoTo Rea_Apu1
  'Screen.MousePointer = vbHourglass
  blnEntroEnWhile = False
  strCodDpto = txtText1(16).Text
  strNumNec = txtText1(0).Text
    
  strFR73 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?"
  Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
  
  strFR66 = "SELECT * FROM FR6600 WHERE FR66CODPETICION=" & strNumNec
  Set rstFR66 = objApp.rdoConnect.OpenResultset(strFR66)
  
  strFR19 = "SELECT * " & _
            "  FROM FR2800 " & _
            " WHERE FR66CODPETICION = ? " & _
            "   AND FR28CANTIDAD > ? "
            
  Set qryFR19 = objApp.rdoConnect.CreateQuery("", strFR19)
  qryFR19(0) = strNumNec
  qryFR19(1) = 0
  
  
  'Se tienen las l�neas dispensadas en ese momento
  Set rdoFR19 = qryFR19.OpenResultset()
  While Not rdoFR19.EOF
    blnEntroEnWhile = True
    
    
    If Not IsNull(rdoFR19.rdoColumns("FR28CANTIDAD").Value) Then
      strCant = CCur(rdoFR19.rdoColumns("FR28CANTIDAD").Value)
    Else
      strCant = 0
    End If
    strCant = objGen.ReplaceStr(strCant, ",", ".", 1)
    
    strCodNec = strNumNec
    
    
    'Se realiza el insert en FR6500
    strIns = "INSERT INTO FR6500 (FR65CODIGO,CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA," & _
                "FR65CANTIDAD,FR73CODPRODUCTO_DIL," & _
                "FR65CANTIDADDIL,FR73CODPRODUCTO_2,FR65DOSIS_2," & _
                "FR93CODUNIMEDIDA_2,FR65DESPRODUCTO,FR65DOSIS,FR93CODUNIMEDIDA," & _
                "FR66CODPETICION,FR65INDPRN,AD01CODASISTENCI,AD07CODPROCESO," & _
                "FR65INDINTERCIENT,AD02CODDPTO_CRG) VALUES ("
    strIns = strIns & "FR65CODIGO_SEQUENCE.NEXTVAL,"
    strIns = strIns & txtText1(2).Text & ","
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO").Value & ",SYSDATE,TO_CHAR(SYSDATE,'HH24'),"
    Else
      strIns = strIns & "999999999" & ",SYSDATE,TO_CHAR(SYSDATE,'HH24'),"
    End If
    strIns = strIns & strCant & ","
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value & ","
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
      strIns = strIns & objGen.ReplaceStr(rdoFR19.rdoColumns("FR28CANTIDADDIL").Value, ",", ".", 1) & ","
      strCantDil = objGen.ReplaceStr(rdoFR19.rdoColumns("FR28CANTIDADDIL").Value, ",", ".", 1)
    Else
      strIns = strIns & "0,"
      strCantDil = 0
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value & ","
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DOSIS_2").Value) Then
      strIns = strIns & objGen.ReplaceStr(rdoFR19.rdoColumns("FR28DOSIS_2").Value, ",", ".", 1) & ","
       If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value) Then
         qryFR73(0) = rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value
         Set rdoFR73 = qryFR73.OpenResultset()
         If Not rdoFR73.EOF Then
           If Not IsNull(rdoFR73.rdoColumns("FR73DOSIS").Value) Then
             If rdoFR73.rdoColumns("FR73DOSIS").Value > 0 Then
               strCant2 = Format(rdoFR19.rdoColumns("FR28DOSIS_2").Value / rdoFR73.rdoColumns("FR73DOSIS").Value, "##0.00")
             Else
               strCant2 = Format(rdoFR19.rdoColumns("FR28DOSIS_2").Value / 1, "##0.00")
             End If
           Else
             strCant2 = Format(rdoFR19.rdoColumns("FR28DOSIS_2").Value / 1, "##0.00")
           End If
         Else
           strCant2 = Format(rdoFR19.rdoColumns("FR28DOSIS_2").Value / 1, "##0.00")
         End If
       Else
        strCant2 = 0
       End If
    Else
      strIns = strIns & "0,"
      strCant2 = 0
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR93CODUNIMEDIDA_2").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DESPRODUCTO").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR28DESPRODUCTO").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DOSIS").Value) Then
      strIns = strIns & objGen.ReplaceStr(rdoFR19.rdoColumns("FR28DOSIS").Value, ",", ".", 1) & ","
    Else
      strIns = strIns & "0,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR93CODUNIMEDIDA").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR93CODUNIMEDIDA").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    strIns = strIns & txtText1(0).Text & ",1," & txtText1(13) & "," & txtText1(14) & ","
    If IsNull(rstFR66.rdoColumns("FR66INDINTERCIENT").Value) Then
         strIns = strIns & "null" & ","
    Else
         strIns = strIns & rstFR66.rdoColumns("FR66INDINTERCIENT").Value & ","
    End If
    If IsNull(rstFR66.rdoColumns("AD02CODDPTO_CRG").Value) Then
         strIns = strIns & "null"
    Else
         strIns = strIns & rstFR66.rdoColumns("AD02CODDPTO_CRG").Value
    End If
    strIns = strIns & ")"
    objApp.rdoConnect.Execute strIns, 64
    
    rdoFR19.MoveNext
  Wend
  
  If blnEntroEnWhile Then
    qryFR73.Close
    Set qryFR73 = Nothing
    Set rdoFR73 = Nothing
  End If
  
  qryFR19.Close
  Set qryFR19 = Nothing
  Set rdoFR19 = Nothing
Rea_Apu1:
  'Screen.MousePointer = vbDefault
End Sub

Private Function EsNecesarioSerMedico() As Boolean
  'Esta funci�n devuelve TRUE si es necesario ser m�dico para para hacer la petici�n
  'Categor�as 1 Consultor y 6 Residente
  'Si alguno de los medicamentos pedidos no est� en el stock ni en los m�dulos
  'de la planta entonces es necesario que sea un m�dico en que realice la petici�n
  Dim strPet As String
  Dim qryPet As rdoQuery
  Dim rdoPet As rdoResultset
  Dim strPrdPerm As String
  Dim qryPrdPerm As rdoQuery
  Dim rdoPrdPerm As rdoResultset
  Dim blnDebeSerMedico As Boolean
  Dim strMedicamento As String
  Dim strCat As String
  Dim qryCat As rdoQuery
  Dim rstCat As rdoResultset
  Dim rstOM As rdoResultset
  Dim strOM As String
  Dim rstprod As rdoResultset
  Dim strprod As String
  
  Screen.MousePointer = vbHourglass
  blnDebeSerMedico = False
  
  strPet = "SELECT * " & _
           "  FROM FR2800,FR7300 " & _
           " WHERE FR2800.FR73CODPRODUCTO = FR7300.FR73CODPRODUCTO AND FR66CODPETICION = ? "
  Set qryPet = objApp.rdoConnect.CreateQuery("", strPet)
  qryPet(0) = txtText1(0).Text
  Set rdoPet = qryPet.OpenResultset()
  
  strPrdPerm = "SELECT COUNT(*) " & _
               "  FROM FR0900 " & _
               " WHERE FR73CODPRODUCTO = ? " & _
               "   AND FR41CODGRUPPROD IN (SELECT FR41CODGRUPPROD " & _
                                          "  FROM FR4100 " & _
                                          " WHERE FRI7CODTIPGRP IN (2,3) " & _
                                          "   AND AD02CODDPTO = ? )"
  Set qryPrdPerm = objApp.rdoConnect.CreateQuery("", strPrdPerm)
  
  'Para cada l�nea de la petici�n se comprueba si puede pedir ese medicamento
  While (Not rdoPet.EOF) And (blnDebeSerMedico = False)
    If Not IsNull(rdoPet.rdoColumns("FR00CODGRPTERAP").Value) Then
      If Len(Trim(rdoPet.rdoColumns("FR00CODGRPTERAP").Value)) >= 0 Then
        If UCase(Left(rdoPet.rdoColumns("FR00CODGRPTERAP").Value, 1)) <> "Q" Then
          'Es un medicamento y hay que mirar si est� en el stock en los m�dulos
          strMedicamento = rdoPet.rdoColumns("FR73CODINTFAR") & "  " & rdoPet.rdoColumns("FR73DESPRODUCTO")
          qryPrdPerm(0) = rdoPet.rdoColumns("FR73CODPRODUCTO").Value
          qryPrdPerm(1) = txtText1(16).Text
          Set rdoPrdPerm = qryPrdPerm.OpenResultset()
          If rdoPrdPerm.rdoColumns(0).Value = 0 Then
            'El medicamento no est� ni en el m�dulo ni el stock de la planta
            blnDebeSerMedico = True
          End If
          '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
          'Irene 28/junio/2000
          'se comprueba que el medicamento no est� en la OM validada del paciente
          strOM = "SELECT * FROM FR6600 WHERE FR66INDOM=-1 AND FR26CODESTPETIC=4 AND "
          strOM = strOM & "CI21CODPERSONA=" & txtText1(2).Text & " AND "
          strOM = strOM & " (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL) "
          strOM = strOM & " AND "
          strOM = strOM & " (FR66INDFM=0 OR FR66INDFM IS NULL) "
          Set rstOM = objApp.rdoConnect.OpenResultset(strOM)
          If rstOM.EOF Then  'el paciente no tiene ninguna OM validada
              blnDebeSerMedico = True
          Else  'el paciente tiene una OM validada
              'se mira si el producto pedido est� en esa OM
              strprod = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & _
                         rstOM.rdoColumns("FR66CODPETICION").Value
              Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
              Do While Not rstprod.EOF
                If rstprod.rdoColumns("FR73CODPRODUCTO").Value = _
                   rdoPet.rdoColumns("FR73CODPRODUCTO").Value Then
                   blnDebeSerMedico = False
                   Exit Do
                Else
                  blnDebeSerMedico = True
                End If
              rstprod.MoveNext
              Loop
              rstprod.Close
              Set rstprod = Nothing
          End If
          rstOM.Close
          Set rstOM = Nothing
          '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        End If
      End If
    End If
    rdoPet.MoveNext
  Wend
  Screen.MousePointer = vbDefault
  If blnDebeSerMedico = True Then
    'Se comprueba que el usuario sea m�dico, en caso contrario no se deja hacer la petici�n
    strCat = "select * from sg0200 where sg02cod= ? "
    Set qryCat = objApp.rdoConnect.CreateQuery("", strCat)
    qryCat(0) = objsecurity.strUser
    Set rstCat = qryCat.OpenResultset()
    If rstCat.EOF = False Then
      Select Case rstCat("AD30CODCATEGORIA").Value
        Case 1, 5, 6
          blnDebeSerMedico = False
        Case Else
          blnDebeSerMedico = True
          MsgBox "El medicamento " & strMedicamento & " solo puede ser pedido por un M�dico"
      End Select
    End If
  End If
  
  EsNecesarioSerMedico = blnDebeSerMedico
End Function

Private Sub cmdaceptar_Click()
Dim objEncript As New clsCWMRES
Dim stra As String
Dim rsta As rdoResultset
Dim strupdate As String
Dim strcategoria As String
Dim rstcategoria As rdoResultset

cmdAceptar.Enabled = False
If txtcodigomedico.Text <> "" And txtpasswordmedico.Text <> "" Then
    stra = "select * from sg0200 where SG02COD=UPPER('" & txtcodigomedico.Text & "')"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not rsta.EOF Then
        If objEncript.Encript(UCase(txtpasswordmedico.Text)) = UCase(rsta.rdoColumns("SG02PASSW").Value) Then
          'se comprueba que el usuario introducido sea m�dico
          strcategoria = "SELECT * FROM SG0200 WHERE SG02COD=" & "'" & UCase(txtcodigomedico.Text) & "'"
          Set rstcategoria = objApp.rdoConnect.OpenResultset(strcategoria)
          If Not rstcategoria.EOF Then
            Select Case rstcategoria.rdoColumns("AD30CODCATEGORIA").Value
              Case 1, 18 'm�dico, t�cnico de laboratorio, enfermera,farmac�utico
                strupdate = "UPDATE FR6600 SET SG02COD_FEN=" & _
                            "'" & rsta.rdoColumns("SG02COD").Value & "'" & "," & _
                            "FR26CODESTPETIC=1" & _
                            " WHERE FR66CODPETICION=" & txtText1(0).Text
                objApp.rdoConnect.Execute strupdate, 64
                objApp.rdoConnect.Execute "Commit", 64
                Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
                objWinInfo.DataRefresh
                FramePassword.Visible = False
                txtpasswordmedico.Text = ""
                txtcodigomedico.Text = ""
                If A�adirCesta = True Then
                  A�adirCesta = False
                  cmdCesta_Click
                Else
                  cmdenviar_Click
                End If
              Case Else
                strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=16" & _
                            " WHERE FR66CODPETICION=" & txtText1(0).Text
                objApp.rdoConnect.Execute strupdate, 64
                objApp.rdoConnect.Execute "Commit", 64
                Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
                objWinInfo.DataRefresh
                If A�adirCesta = True Then
                    Call MsgBox("El usuario introducido es " & rstcategoria.rdoColumns("AD30CODCATEGORIA").Value & "." & Chr(13) & _
                                "La petici�n NO ha sido enviada a Cesta.", vbInformation, "Aviso")
                    A�adirCesta = False
                Else
                    Call MsgBox("El usuario introducido es " & rstcategoria.rdoColumns("AD30CODCATEGORIA").Value & "." & Chr(13) & _
                            "El PRN NO ha sido enviado.", vbInformation, "Aviso")
                End If
                txtpasswordmedico.Text = ""
                txtcodigomedico.Text = ""
            End Select
          Else  'el m�dico no se encuentra registrado en ninguna categor�a
            strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=16" & _
                        " WHERE FR66CODPETICION=" & txtText1(0).Text
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "Commit", 64
            Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
            objWinInfo.DataRefresh
            If A�adirCesta = True Then
                Call MsgBox("El c�digo introducido es incorrecto. El m�dico no existe." & Chr(13) & _
                            "La petici�n NO ha sido enviada a Cesta.", vbInformation, "Aviso")
                A�adirCesta = False
            Else
                Call MsgBox("El c�digo introducido es incorrecto. El m�dico no existe." & Chr(13) & _
                        "El PRN NO ha sido enviado.", vbInformation, "Aviso")
            End If
            txtpasswordmedico.Text = ""
            txtcodigomedico.Text = ""
          End If
        Else
          strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=16" & _
                    " WHERE FR66CODPETICION=" & txtText1(0).Text
          objApp.rdoConnect.Execute strupdate, 64
          objApp.rdoConnect.Execute "Commit", 64
          Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
          objWinInfo.DataRefresh
          If A�adirCesta = True Then
              Call MsgBox("El password introducido es incorrecto." & Chr(13) & _
                      "La petici�n NO ha sido enviada a Cesta.", vbInformation, "Aviso")
              A�adirCesta = False
          Else
              Call MsgBox("El password introducido es incorrecto." & Chr(13) & _
                      "El PRN NO ha sido enviado.", vbInformation, "Aviso")
          End If
          txtpasswordmedico.Text = ""
          txtcodigomedico.Text = ""
        End If
    Else
      strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=16" & _
                    " WHERE FR66CODPETICION=" & txtText1(0).Text
      objApp.rdoConnect.Execute strupdate, 64
      objApp.rdoConnect.Execute "Commit", 64
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      objWinInfo.DataRefresh
      If A�adirCesta = True Then
          Call MsgBox("El c�digo introducido es incorrecto. El m�dico no existe." & Chr(13) & _
              "La petici�n NO ha sido enviada a Cesta.", vbInformation, "Aviso")
          A�adirCesta = False
      Else
          Call MsgBox("El c�digo introducido es incorrecto. El m�dico no existe." & Chr(13) & _
              "El PRN NO ha sido enviado.", vbInformation, "Aviso")
      End If
      txtpasswordmedico.Text = ""
      txtcodigomedico.Text = ""
    End If
    rsta.Close
    Set rsta = Nothing
Else
  strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=16" & _
                    " WHERE FR66CODPETICION=" & txtText1(0).Text
  objApp.rdoConnect.Execute strupdate, 64
  objApp.rdoConnect.Execute "Commit", 64
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.DataRefresh
  If A�adirCesta = True Then
      Call MsgBox("El c�digo y el password son incorrectos." & Chr(13) & _
                  "La petici�n NO ha sido enviada a Cesta.", vbInformation, "Aviso")
      A�adirCesta = False
  Else
      Call MsgBox("El c�digo y el password son incorrectos." & Chr(13) & _
                  "El PRN NO ha sido enviado.", vbInformation, "Aviso")
  End If
  txtpasswordmedico.Text = ""
  txtcodigomedico.Text = ""
End If
     
cmdAceptar.Enabled = True
FramePassword.Visible = False

End Sub
Private Sub cmdcancelar_Click()
Dim mensaje As String
Dim strupdate As String

cmdCancelar.Enabled = False
mensaje = MsgBox("Est� seguro que desea cancelar la introducci�n del m�dico?", vbYesNo, "Aviso")
If mensaje = 6 Then 'si
  strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=16" & _
              " WHERE FR66CODPETICION=" & txtText1(0).Text
  objApp.rdoConnect.Execute strupdate, 64
  objApp.rdoConnect.Execute "Commit", 64
  FramePassword.Visible = False
  txtpasswordmedico.Text = ""
  txtcodigomedico.Text = ""
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.DataRefresh
  If A�adirCesta = True Then
    Call MsgBox("La petici�n NO ha sido enviada a Cesta.", vbInformation, "Aviso")
    A�adirCesta = False
  Else
    Call MsgBox("El PRN NO ha sido enviado.", vbInformation, "Aviso")
  End If
End If
cmdCancelar.Enabled = True
End Sub

Private Sub cmdbusprotquirofano_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim mvarBook As Variant
Dim mblnBook As Boolean
Dim strinsert As String

  If objWinInfo.objWinActiveForm.strName = "PRN" Then
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    End If
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Exit Sub
    End If
  End If

  cmdbusprotquirofano.Enabled = False
  If IsNumeric(txtText1(0).Text) And txtText1(16).Text <> "" Then
    If txtText1(25).Text <> 1 Then
      Call MsgBox("No puede a�adir datos a la Petici�n", vbInformation, "Aviso")
      cmdbusprotquirofano.Enabled = True
      Exit Sub
    End If
    noinsertar = True
    mblnBook = False
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    'glngcodpeticion = txtText1(0).Text
    Call objsecurity.LaunchProcess("FR0193")
    'gstrLlamadorProd = ""
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      grdDBGrid1(0).Bookmark = mvarBook
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0)  'c�d.prod
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1)  'interno
          grdDBGrid1(0).Columns(7).Value = gintprodbuscado(v, 4)  'ref
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2)  'desc.prod
          grdDBGrid1(0).Columns(10).Value = gintprodbuscado(v, 5) 'FF
          grdDBGrid1(0).Columns(11).Value = gintprodbuscado(v, 6) 'dosis
          grdDBGrid1(0).Columns(12).Value = gintprodbuscado(v, 7) 'UM
          grdDBGrid1(0).Columns(13).Value = gintprodbuscado(v, 3) 'cantidad
          grdDBGrid1(0).Columns(14).Value = "/"
          If mblnBook = False Then
            mblnBook = True
            mvarBook = grdDBGrid1(0).Bookmark
          End If
          strinsert = "INSERT INTO FR2800(FR66CODPETICION,FR28NUMLINEA,FR28OPERACION,"
          strinsert = strinsert & "FR73CODPRODUCTO,FRH7CODFORMFAR,FR28DOSIS,FR93CODUNIMEDIDA,"
          strinsert = strinsert & "FR28CANTIDAD,FR28REFERENCIA,FR28CANTDISP) VALUES ("
          strinsert = strinsert & grdDBGrid1(0).Columns(3).Value & ","
          strinsert = strinsert & grdDBGrid1(0).Columns(4).Value & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(14).Value & "'" & ","
          strinsert = strinsert & grdDBGrid1(0).Columns(5).Value & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(10).Value & "'" & ","
          If IsNumeric(grdDBGrid1(0).Columns(11).Value) Then
            strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns(11).Value, ",", ".", 1) & ","
          Else
            strinsert = strinsert & "NULL" & ","
          End If
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(12).Value & "'" & ","
          strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").Value, ",", ".", 1) & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(7).Value & "'" & ","
          strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").Value, ",", ".", 1)
          strinsert = strinsert & ")"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
          objWinInfo.objWinActiveForm.blnChanged = False
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      grdDBGrid1(0).Bookmark = mvarBook
      grdDBGrid1(0).Col = 13
    End If
    gintprodtotal = 0
  End If
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  cmdbusprotquirofano.Enabled = True
End Sub

Private Sub cmdCesta_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim strupdate As String
Dim blnNOAdd As Boolean
Dim SQL As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim rstfirma As rdoResultset
Dim strfirma As String
  
  blnNOAdd = False
  glngselpaciente = txtText1(2).Text
  If objWinInfo.objWinActiveForm.blnChanged = True Then
    MsgBox "Debe salvar los cambios para poder a�adir a la Cesta", vbInformation, "Cesta"
    Exit Sub
  End If
  CmdCesta.Enabled = False
  Me.Enabled = False
  SQL = "SELECT COUNT(*)"
  SQL = SQL & " FROM AD4100"
  SQL = SQL & " WHERE AD02CODDPTO = ?"
  SQL = SQL & " AND SYSDATE BETWEEN AD41FECINICIO AND NVL(AD41FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
  Set qry = objApp.rdoConnect.CreateQuery("", SQL)
  qry(0) = txtText1(16).Text
  Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
  If rs(0).Value = 0 Then
  Else
    If txtText1(27).Text = "" Then
      MsgBox "El Servicio tiene secciones, es Obligatorio elegir una SECCION.", vbExclamation
      CmdCesta.Enabled = True
      Me.Enabled = True
      Exit Sub
    End If
  End If
  rs.Close
  Set rs = Nothing
  
  If txtText1(0).Text <> "" And txtText1(16).Text <> "" Then
    stra = "SELECT * FROM FR6600 WHERE FR66CODPETICION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not IsNull(rsta("FR26CODESTPETIC").Value) Then
      If Not IsNull(rsta("FR66INDCESTA").Value) Then
        If (rsta("FR66INDCESTA").Value = -1) Then
          Call MsgBox("La Petici�n ya estaba a�adida a la Cesta", vbExclamation, "Aviso")
          blnNOAdd = True
        End If
      End If
      If ((rsta("FR26CODESTPETIC").Value <> 1) And _
         (rsta("FR26CODESTPETIC").Value <> 16)) Or _
         (blnNOAdd) Then
        Call MsgBox("No puede a�adir la Petici�n a la Cesta", vbExclamation, "Aviso")
      Else
        'Irene 28/junio/2000
        strfirma = "SELECT SG02COD_FEN FROM FR6600 WHERE FR66CODPETICION=" & txtText1(0).Text
        Set rstfirma = objApp.rdoConnect.OpenResultset(strfirma)
        If Not rstfirma.EOF Then
          If IsNull(rstfirma.rdoColumns("SG02COD_FEN").Value) Then
            If EsNecesarioSerMedico Then
              A�adirCesta = True
              FramePassword.Visible = True
              CmdCesta.Enabled = True
              Me.Enabled = True
              txtcodigomedico.SetFocus
              A�adirCesta = False
              Exit Sub
            Else
              A�adirCesta = True
              strupdate = "UPDATE FR6600 SET SG02COD_FEN=" & _
                    "'" & objsecurity.strUser & "'" & "," & _
                    " FR26CODESTPETIC=1" & _
                    " WHERE FR66CODPETICION=" & txtText1(0).Text
              objApp.rdoConnect.Execute strupdate, 64
              objApp.rdoConnect.Execute "Commit", 64
              Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
              objWinInfo.DataRefresh
              txtpasswordmedico.Text = ""
              txtcodigomedico.Text = ""
              A�adirCesta = False
            End If
          End If
        End If
        Call RealizarApuntes
        strupdate = "UPDATE FR6600 SET FR66INDCESTA=-1,SG02COD_MED = '" & objsecurity.strUser & "'" & _
                   " WHERE FR66CODPETICION=" & txtText1(0).Text
        objApp.rdoConnect.Execute strupdate, 64
        objApp.rdoConnect.Execute "Commit", 64
        MsgBox "La petici�n ha sido a�adida a la Cesta Correctamente", vbInformation, "A�adir a Cesta"
        Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
        objWinInfo.DataRefresh
      End If
    Else
      Call MsgBox("No puede a�adir la Petici�n a la Cesta", vbExclamation, "A�adir a Cesta")
    End If
    rsta.Close
    Set rsta = Nothing
  End If
  Me.Enabled = True
  CmdCesta.Enabled = True

End Sub

Private Sub cmdenviar_Click()
Dim strupdate As String
Dim mensaje As String
Dim rsta As rdoResultset
Dim stra As String
Dim hora As Variant
Dim blnEnviada As Boolean
Dim strmedidadil As String
Dim curDosis2 As Currency
Dim curCantidad2 As Currency
Dim auxProd2 As String
Dim strProd2 As String
Dim qryProd2 As rdoQuery
Dim rstProd2 As rdoResultset
Dim auxProd1 As String
Dim strProd1 As String
Dim qryProd1 As rdoQuery
Dim rstProd1 As rdoResultset
Dim curDosis1 As Currency
Dim curCantidad1 As Currency
Dim vntAuxCantidad1 As Variant
Dim vntAuxCantidad2 As Variant
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim strlinea As String
Dim strpaciente As String
Dim qrypaciente As rdoQuery
Dim rstpaciente As rdoResultset
Dim Auxcama As String
Dim strcama As String
Dim qrycama As rdoQuery
Dim rstcama As rdoResultset
Dim auxDpto As String
Dim strdpto As String
Dim qrydpto As rdoQuery
Dim rstdpto As rdoResultset
Dim auxSolucion As String
Dim strSolucion As String
Dim qrySolucion As rdoQuery
Dim rstSolucion As rdoResultset
Dim strcaducidad As String
Dim strfecha As String
Dim rstfecha As rdoResultset
Dim Auxfecha As String
Dim auxVia As String
Dim strVia As String
Dim qryVia As rdoQuery
Dim rstVia As rdoResultset
Dim strHoras As String
Dim strMinutos As String
Dim strHAdm As String
Dim strInstrucctiones As String
Dim i As Integer
Dim auxInstrucctiones As String
Dim auxVolTotal As String
Dim auxstr As String
Dim auxcant As String
Dim crlf As String
Dim Incremento As Integer
Dim contLinea As Integer
Dim strFR73 As String
Dim qryFR73 As rdoQuery
Dim rdoFR73 As rdoResultset
Dim strPet As String
Dim strD As String
Dim strH As String
Dim strSQL As String
Dim rstSQL As rdoResultset
Dim escribir_cabecera As Boolean
Dim strlineahasta As String
Dim nosaltarproducto As Boolean
Dim strfirma As String
Dim rstfirma As rdoResultset

  blnEnviada = False
  cmdEnviar.Enabled = False
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'objWinInfo.DataRefresh
  If objWinInfo.objWinActiveForm.blnChanged = True Then
    MsgBox "Debe salvar los cambios para poder enviar el PRN", vbInformation, "Enviar PRN"
    cmdEnviar.Enabled = True
    Exit Sub
  End If

  Me.Enabled = False
  '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  'Irene 28/junio/2000
  strfirma = "SELECT SG02COD_FEN FROM FR6600 WHERE FR66CODPETICION=" & txtText1(0).Text
  Set rstfirma = objApp.rdoConnect.OpenResultset(strfirma)
  If Not rstfirma.EOF Then
    If IsNull(rstfirma.rdoColumns("SG02COD_FEN").Value) Then
      If EsNecesarioSerMedico Then
        FramePassword.Visible = True
        cmdEnviar.Enabled = True
        Me.Enabled = True
        txtcodigomedico.SetFocus
        Exit Sub
      Else
        strupdate = "UPDATE FR6600 SET SG02COD_FEN=" & _
              "'" & objsecurity.strUser & "'" & "," & _
              " FR26CODESTPETIC=1" & _
              " WHERE FR66CODPETICION=" & txtText1(0).Text
        objApp.rdoConnect.Execute strupdate, 64
        objApp.rdoConnect.Execute "Commit", 64
        Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
        objWinInfo.DataRefresh
        txtpasswordmedico.Text = ""
        txtcodigomedico.Text = ""
      End If
    End If
  End If
  '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  'si es PRN se borra la posible secci�n que pueda haberse guardado
  If IsNumeric(txtText1(27).Text) Then
      Call MsgBox("Se eliminar� la Secci�n introducida", vbInformation, "Aviso")
      strupdate = "UPDATE FR6600 SET AD41CODSECCION=NULL WHERE FR66CODPETICION=" & txtText1(0).Text
      objApp.rdoConnect.Execute strupdate, 64
      objApp.rdoConnect.Execute "Commit", 64
  End If
  
  If txtText1(0).Text <> "" Then
    If txtText1(25).Text = 1 And txtText1(16).Text <> "" And chkCheck1(3).Value <> 1 Then 'redactada
      stra = "(SELECT TO_CHAR(SYSDATE,'HH24') FROM DUAL)"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      hora = rsta.rdoColumns(0).Value
      hora = hora & "."
      stra = ""
      stra = "(SELECT TO_CHAR(SYSDATE,'mi') FROM DUAL)"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      hora = hora & rsta.rdoColumns(0).Value
      'Hay que hacer los inserts en FR6500, FR8000 y FR3500
      Call RealizarApuntes
      strupdate = "UPDATE FR6600 SET FR66FECFIRMMEDI=SYSDATE," & _
                   "FR66HORAFIRMMEDI=" & hora & "," & _
                   "FR26CODESTPETIC=3,FR66FECENVIO=SYSDATE,SG02COD_MED = '" & objsecurity.strUser & "'" & _
                   " WHERE FR66CODPETICION=" & txtText1(0).Text
      objApp.rdoConnect.Execute strupdate, 64
      objApp.rdoConnect.Execute "Commit", 64
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      objWinInfo.DataRefresh
                      
      mensaje = MsgBox("El PRN ha sido firmado y enviado", vbInformation, "Farmacia")
      blnEnviada = True
    Else
      'If txtText1(25).Text = 2 Then 'firmada
      If txtText1(25).Text = 16 Then
        strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=3,FR66FECENVIO=SYSDATE" & _
                    " WHERE FR66CODPETICION=" & txtText1(0).Text
                    '"SG02COD_ENF=" & "'" & objsecurity.strUser & "'"
        objApp.rdoConnect.Execute strupdate, 64
        objApp.rdoConnect.Execute "Commit", 64
        'Hay que hacer los inserts en FR6500, FR8000 y FR3500
        Call RealizarApuntes
        Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
        objWinInfo.DataRefresh
        mensaje = MsgBox("El PRN ha sido enviado", vbInformation, "Farmacia")
        blnEnviada = True
      Else
        mensaje = MsgBox("No se puede enviar como PRN", vbInformation, "Farmacia")
      End If
    End If
  End If
  If blnEnviada Then
    ''Hay que hacer los inserts en FR6500, FR8000 y FR3500
    'Call RealizarApuntes
    'Se imprime
    'Call Imprimir("FR1711.RPT", 1)
    blnImpresoraSeleccionada = False
    Load frmFabPrinter
    Call frmFabPrinter.Show(vbModal)
    'Call objsecurity.LaunchProcess("FRIMPR")
    If blnImpresoraSeleccionada = False Then
      MsgBox "No se ha seleccionado ninguna impresora", vbInformation, "PRN"
      cmdEnviar.Enabled = True
      Me.Enabled = True
      Exit Sub
    End If
  
    escribir_cabecera = True
    
    strFR73 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?"
    Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
    
    'Para cada l�nea del grid
    strSQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY'), TO_CHAR(SYSDATE, 'HH24:MI') FROM DUAL"
    Set rstSQL = objApp.rdoConnect.OpenResultset(strSQL)
    If Not rstSQL.EOF Then
      strD = rstSQL.rdoColumns(0).Value
      strH = rstSQL.rdoColumns(1).Value
    Else
      strD = ""
      strH = ""
    End If
    strPet = txtText1(0).Text
    mintNTotalSelRows = grdDBGrid1(0).Rows
    contLinea = 0
    nosaltarproducto = False
    'INICIO DEL BUCLE DE LOS PRODUCTOS
    grdDBGrid1(0).MoveFirst
    mintisel = 0
    While mintisel < mintNTotalSelRows
      mvarBkmrk = grdDBGrid1(0).RowBookmark(mintisel)
      crlf = Chr$(13) & Chr$(10)
      Incremento = 30
      If escribir_cabecera = True Then
        escribir_cabecera = False
        strlinea = crlf
        strlinea = strlinea & "N" & crlf
        strlinea = strlinea & "I8,1,034" & crlf
        strlinea = strlinea & "Q599,24" & crlf
        strlinea = strlinea & "R0,0" & crlf
        strlinea = strlinea & "S2" & crlf
        strlinea = strlinea & "D5" & crlf
        strlinea = strlinea & "ZB" & crlf
    
        strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
        contLinea = contLinea + 1
          
        strlinea = strlinea & Texto_Etiqueta("CLINICA UNIVERSITARIA. Servicio de Farmacia ", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
        contLinea = contLinea + 1
      
        strlinea = strlinea & "LE0," & (contLinea * Incremento) + 8 & ",740,8" & crlf
        contLinea = contLinea + 1
      
        strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
        contLinea = contLinea + 1
        
        strlinea = strlinea & Texto_Etiqueta("C�d.Pet.: " & strPet & " Fecha: " & strD & " Hora: " & strH, 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
        contLinea = contLinea + 1
          
        strdpto = "SELECT AD02DESDPTO FROM AD0200,FR6600 WHERE AD0200.AD02CODDPTO=FR6600.AD02CODDPTO AND FR66CODPETICION = ? "
        Set qrydpto = objApp.rdoConnect.CreateQuery("", strdpto)
        qrydpto(0) = txtText1(0).Text 'grdDBGrid1(0).Columns("C�digo Petici�n").CellValue(mvarBkmrk)
        
        Set rstdpto = qrydpto.OpenResultset(strdpto)
        If Not rstdpto.EOF Then
          auxDpto = rstdpto(0)
        Else
          auxDpto = ""
        End If
        rstdpto.Close
        qrydpto.Close
        
        If Len(Trim(txtText1(46).Text)) >= 0 Then
          strcama = "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD15CODCAMA= GCFN07(?) "
          Set qrycama = objApp.rdoConnect.CreateQuery("", strcama)
          qrycama(0) = txtText1(46).Text 'grdDBGrid1(0).Columns("Cama").CellValue(mvarBkmrk)
          Set rstcama = qrycama.OpenResultset(strcama)
          If Not rstcama.EOF Then
            Auxcama = rstcama(0)
          Else
            Auxcama = ""
          End If
          rstcama.Close
          qrycama.Close
        Else
          Auxcama = "SIN CAMA"
        End If
      
        strpaciente = "SELECT CI2200.CI22PRIAPEL || ',' || CI2200.CI22SEGAPEL || ',' || CI2200.CI22NOMBRE,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA= ? "
        Set qrypaciente = objApp.rdoConnect.CreateQuery("", strpaciente)
        qrypaciente(0) = txtText1(2).Text 'grdDBGrid1(0).Columns("Persona").CellValue(mvarBkmrk)
        Set rstpaciente = qrypaciente.OpenResultset(strpaciente)
        If Not rstpaciente.EOF Then
          If Not IsNull(rstpaciente(0)) Then
            strlinea = strlinea & Texto_Etiqueta(formatear(rstpaciente(0), 30, True, True) & " " & formatear(auxDpto, 20, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
            contLinea = contLinea + 1
          Else
            strlinea = strlinea & Texto_Etiqueta(formatear("", 30, True, True) & " " & formatear(auxDpto, 20, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
            contLinea = contLinea + 1
          End If
          If Not IsNull(rstpaciente(1)) Then
            strlinea = strlinea & Texto_Etiqueta(formatear(rstpaciente(1), 30, True, True) & " " & Auxcama, 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
            contLinea = contLinea + 1
          Else
            strlinea = strlinea & Texto_Etiqueta(formatear("", 30, True, True) & " " & Auxcama, 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
            contLinea = contLinea + 1
          End If
        End If
        rstpaciente.Close
        qrypaciente.Close
    
        strfecha = "SELECT TO_CHAR(SYSDATE,'DD-MM-YYYY') FROM DUAL"
        Set rstfecha = objApp.rdoConnect.OpenResultset(strfecha)
        Auxfecha = rstfecha(0).Value
    
        auxVia = ""
        
        strHoras = ""
        strMinutos = ""
        
        strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
        contLinea = contLinea + 1
      End If 'si escribir_cabecera=true
    
      strlineahasta = strlinea
      If grdDBGrid1(0).Columns("C�d.Prod").CellValue(mvarBkmrk) = "999999999" Then
        auxProd1 = grdDBGrid1(0).Columns("Prod").CellValue(mvarBkmrk) & " " & _
                   grdDBGrid1(0).Columns("Referencia").CellValue(mvarBkmrk) & " " & _
                   grdDBGrid1(0).Columns("Prod No Formulario").CellValue(mvarBkmrk) & " " & _
                   grdDBGrid1(0).Columns("Cantidad").CellValue(mvarBkmrk)
      Else
        auxProd1 = grdDBGrid1(0).Columns("Prod").CellValue(mvarBkmrk) & " " & _
                   grdDBGrid1(0).Columns("Referencia").CellValue(mvarBkmrk) & " " & _
                   grdDBGrid1(0).Columns("Desc.Producto").CellValue(mvarBkmrk) & " " & _
                   grdDBGrid1(0).Columns("Cantidad").CellValue(mvarBkmrk)
                   qryFR73(0) = grdDBGrid1(0).Columns("C�d.Prod").CellValue(mvarBkmrk)
        Set rdoFR73 = qryFR73.OpenResultset()
        If Not IsNull(rdoFR73.rdoColumns("FR73INDPSICOT").Value) Then
          If rdoFR73.rdoColumns("FR73INDPSICOT").Value = -1 Then
            auxProd1 = auxProd1 & " (PSICOTROPICO)"
          End If
        End If
        If Not IsNull(rdoFR73.rdoColumns("FR73INDESTUPEFACI").Value) Then
          If rdoFR73.rdoColumns("FR73INDESTUPEFACI").Value = -1 Then
            auxProd1 = auxProd1 & " (ESTUPEFACIENTE)"
          End If
        End If
        If Not IsNull(rdoFR73.rdoColumns("FRH9UBICACION").Value) Then
          auxProd1 = auxProd1 & " " & rdoFR73.rdoColumns("FRH9UBICACION").Value
        End If
      End If
    
      'strlinea = strlinea & Texto_Etiqueta("  " & auxProd1, 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      strlinea = strlinea & Texto_Etiqueta("  " & formatear(auxProd1, 48, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      contLinea = contLinea + 1
    
      'sobra   strlinea = strlinea & Texto_Etiqueta("", 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
      'sobra  contLinea = contLinea + 1
    
      strInstrucctiones = grdDBGrid1(0).Columns("Inst. de Administraci�n").CellValue(mvarBkmrk)
      i = 0
      auxInstrucctiones = ""
      While strInstrucctiones <> "" And i <> 5
        If InStr(strInstrucctiones, Chr(13)) > 0 Then
          auxInstrucctiones = Left$(strInstrucctiones, InStr(strInstrucctiones, Chr(13)) - 1)
          strInstrucctiones = Right$(strInstrucctiones, Len(strInstrucctiones) - InStr(strInstrucctiones, Chr(13)))
          strlinea = strlinea & Texto_Etiqueta(formatear(auxInstrucctiones, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
          contLinea = contLinea + 1
        Else
          If Chr(10) <> strInstrucctiones Then
            auxInstrucctiones = strInstrucctiones
            strInstrucctiones = ""
            strlinea = strlinea & Texto_Etiqueta(formatear(auxInstrucctiones, 50, True, True), 0, (contLinea * Incremento) + 8, 0, "4", 1, 1, "N") & crlf
            contLinea = contLinea + 1
          Else
            strInstrucctiones = ""
          End If
        End If
        i = i + 1
      Wend
  
      If (contLinea = 18) Then 'no caben m�s l�neas en esta etiqueta
        strlinea = strlinea & "P1" & crlf
        strlinea = strlinea & " " & crlf
        Printer.Print strlinea
        Printer.EndDoc
        escribir_cabecera = True
        contLinea = 0
      Else
        If (contLinea > 18) Then  'el n�mero de l�neas se pasa
          strlinea = strlineahasta  'se quita el �ltimo producto
          strlinea = strlinea & "P1" & crlf
          strlinea = strlinea & " " & crlf
          Printer.Print strlinea
          Printer.EndDoc
          escribir_cabecera = True
          contLinea = 0
          nosaltarproducto = True
        Else
          If (mintisel = mintNTotalSelRows - 1) Then  'es el �ltimo producto,se manda imprimir
            strlinea = strlinea & "P1" & crlf
            strlinea = strlinea & " " & crlf
            Printer.Print strlinea
            Printer.EndDoc
            escribir_cabecera = True
            contLinea = 0
          End If
        End If
      End If
      If nosaltarproducto = False Then
        grdDBGrid1(0).MoveNext
        mintisel = mintisel + 1
      Else
        nosaltarproducto = False
      End If
    Wend
    qryFR73.Close
    Set qryFR73 = Nothing
    Set rdoFR73 = Nothing
  End If
  
  cmdEnviar.Enabled = True
  Me.Enabled = True

End Sub
Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  strWhere = "{AD0200.AD02CODDPTO}={FR6601J.AD02CODDPTO} AND " & _
             "{FR6601J.FR66CODPETICION}={FR2800.FR66CODPETICION} AND " & _
             "{FR2800.FR73CODPRODUCTO}={FR7300.FR73CODPRODUCTO} AND " & _
             "{FR6601J.FR66CODPETICION}=" & txtText1(0).Text
             
             
  CrystalReport1.SelectionFormula = strWhere
  'On Error GoTo Err_imp4
  CrystalReport1.Action = 1
Err_imp4:

End Sub

Private Sub cmdgrupoterapmedic_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim mvarBook As Variant
Dim mblnBook As Boolean
Dim strinsert As String

  If objWinInfo.objWinActiveForm.strName = "PRN" Then
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    End If
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Exit Sub
    End If
  End If

  cmdgrupoterapmedic.Enabled = False
  If IsNumeric(txtText1(0).Text) And txtText1(16).Text <> "" Then
    If txtText1(25).Text <> 1 Then
      Call MsgBox("No puede a�adir datos a la Petici�n", vbInformation, "Aviso")
      cmdgrupoterapmedic.Enabled = True
      Exit Sub
    End If
    noinsertar = True
    mblnBook = False
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    gstrLlamador = "Medicamentos"
    Call objsecurity.LaunchProcess("FR0195")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      grdDBGrid1(0).Bookmark = mvarBook
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0)
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1)
          grdDBGrid1(0).Columns(7).Value = gintprodbuscado(v, 8)
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2)
          grdDBGrid1(0).Columns(9).Value = gintprodbuscado(v, 9)
          grdDBGrid1(0).Columns(10).Value = gintprodbuscado(v, 5)
          grdDBGrid1(0).Columns(11).Value = gintprodbuscado(v, 3)
          grdDBGrid1(0).Columns(12).Value = gintprodbuscado(v, 4)
          grdDBGrid1(0).Columns(14).Value = "/"
          grdDBGrid1(0).Columns("Cantidad").Value = 1
          If mblnBook = False Then
            mblnBook = True
            mvarBook = grdDBGrid1(0).Bookmark
          End If
          strinsert = "INSERT INTO FR2800(FR66CODPETICION,FR28NUMLINEA,FR28OPERACION,"
          strinsert = strinsert & "FR73CODPRODUCTO,FRH7CODFORMFAR,FR28DOSIS,FR93CODUNIMEDIDA,"
          strinsert = strinsert & "FR28CANTIDAD,FR28REFERENCIA,FR28CANTDISP) VALUES ("
          strinsert = strinsert & grdDBGrid1(0).Columns(3).Value & ","
          strinsert = strinsert & grdDBGrid1(0).Columns(4).Value & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(14).Value & "'" & ","
          strinsert = strinsert & grdDBGrid1(0).Columns(5).Value & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(10).Value & "'" & ","
          If IsNumeric(grdDBGrid1(0).Columns(11).Value) Then
            strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns(11).Value, ",", ".", 1) & ","
          Else
            strinsert = strinsert & "NULL" & ","
          End If
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(12).Value & "'" & ","
          strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").Value, ",", ".", 1) & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(7).Value & "'" & ","
          strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").Value, ",", ".", 1)
          strinsert = strinsert & ")"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
          objWinInfo.objWinActiveForm.blnChanged = False
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      grdDBGrid1(0).Bookmark = mvarBook
      grdDBGrid1(0).Col = 13
    End If
    gintprodtotal = 0
  End If
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  cmdgrupoterapmedic.Enabled = True
End Sub

Private Sub cmdgrupoterapprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim mvarBook As Variant
Dim mblnBook As Boolean
Dim strinsert As String

  If objWinInfo.objWinActiveForm.strName = "PRN" Then
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    End If
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Exit Sub
    End If
  End If

  cmdgrupoterapprod.Enabled = False
  If IsNumeric(txtText1(0).Text) And txtText1(16).Text <> "" Then
    If txtText1(25).Text <> 1 Then
      Call MsgBox("No puede a�adir datos a la Petici�n", vbInformation, "Aviso")
      cmdgrupoterapprod.Enabled = True
      Exit Sub
    End If
    noinsertar = True
    mblnBook = False
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    gstrLlamador = "Material Sanitario"
    Call objsecurity.LaunchProcess("FR0195")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      grdDBGrid1(0).Bookmark = mvarBook
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0)  'c�d.prod
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1)  'c�d.interno
          grdDBGrid1(0).Columns(7).Value = gintprodbuscado(v, 8)  'referencia
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2)  'descripci�n
          grdDBGrid1(0).Columns(10).Value = gintprodbuscado(v, 5) 'FF
          grdDBGrid1(0).Columns(11).Value = gintprodbuscado(v, 3) 'Dosis
          grdDBGrid1(0).Columns(12).Value = gintprodbuscado(v, 4) 'UM
          grdDBGrid1(0).Columns("Cantidad").Value = 1             'cantidad
          grdDBGrid1(0).Columns(14).Value = "/"
          If mblnBook = False Then
            mblnBook = True
            mvarBook = grdDBGrid1(0).Bookmark
          End If
          strinsert = "INSERT INTO FR2800(FR66CODPETICION,FR28NUMLINEA,FR28OPERACION,"
          strinsert = strinsert & "FR73CODPRODUCTO,FRH7CODFORMFAR,FR28DOSIS,FR93CODUNIMEDIDA,"
          strinsert = strinsert & "FR28CANTIDAD,FR28REFERENCIA,FR28CANTDISP) VALUES ("
          strinsert = strinsert & grdDBGrid1(0).Columns(3).Value & ","
          strinsert = strinsert & grdDBGrid1(0).Columns(4).Value & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(14).Value & "'" & ","
          strinsert = strinsert & grdDBGrid1(0).Columns(5).Value & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(10).Value & "'" & ","
          If IsNumeric(grdDBGrid1(0).Columns(11).Value) Then
            strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns(11).Value, ",", ".", 1) & ","
          Else
            strinsert = strinsert & "NULL" & ","
          End If
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(12).Value & "'" & ","
          strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").Value, ",", ".", 1) & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(7).Value & "'" & ","
          strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").Value, ",", ".", 1)
          strinsert = strinsert & ")"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
          objWinInfo.objWinActiveForm.blnChanged = False
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      grdDBGrid1(0).Bookmark = mvarBook
      grdDBGrid1(0).Col = 13
    End If
    gintprodtotal = 0
  End If
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  cmdgrupoterapprod.Enabled = True
End Sub

Private Sub cmdGupoProd_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim mvarBook As Variant
Dim mblnBook As Boolean
Dim strinsert As String

  If objWinInfo.objWinActiveForm.strName = "PRN" Then
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    End If
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Exit Sub
    End If
  End If

  cmdGupoProd.Enabled = False
  If IsNumeric(txtText1(0).Text) And txtText1(16).Text <> "" Then
    If txtText1(25).Text <> 1 Then
      Call MsgBox("No puede a�adir datos a la Petici�n", vbInformation, "Aviso")
      cmdGupoProd.Enabled = True
      Exit Sub
    End If
    noinsertar = True
    mblnBook = False
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    gstrLlamadorProd = "frmPedirServEspeciales"
    glngcodpeticion = txtText1(0).Text
    Call objsecurity.LaunchProcess("FR0167")
    gstrLlamadorProd = ""
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      grdDBGrid1(0).Bookmark = mvarBook
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0)
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1)
          grdDBGrid1(0).Columns(7).Value = gintprodbuscado(v, 5)
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2)
          grdDBGrid1(0).Columns(9).Value = gintprodbuscado(v, 8)
          grdDBGrid1(0).Columns(10).Value = gintprodbuscado(v, 9)
          grdDBGrid1(0).Columns(11).Value = gintprodbuscado(v, 3)
          grdDBGrid1(0).Columns(12).Value = gintprodbuscado(v, 4)
          grdDBGrid1(0).Columns(14).Value = "/"
          grdDBGrid1(0).Columns("Cantidad").Value = 1
          If mblnBook = False Then
            mblnBook = True
            mvarBook = grdDBGrid1(0).Bookmark
          End If
          strinsert = "INSERT INTO FR2800(FR66CODPETICION,FR28NUMLINEA,FR28OPERACION,"
          strinsert = strinsert & "FR73CODPRODUCTO,FRH7CODFORMFAR,FR28DOSIS,FR93CODUNIMEDIDA,"
          strinsert = strinsert & "FR28CANTIDAD,FR28REFERENCIA,FR28CANTDISP) VALUES ("
          strinsert = strinsert & grdDBGrid1(0).Columns(3).Value & ","
          strinsert = strinsert & grdDBGrid1(0).Columns(4).Value & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(14).Value & "'" & ","
          strinsert = strinsert & grdDBGrid1(0).Columns(5).Value & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(10).Value & "'" & ","
          If IsNumeric(grdDBGrid1(0).Columns(11).Value) Then
            strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns(11).Value, ",", ".", 1) & ","
          Else
            strinsert = strinsert & "NULL" & ","
          End If
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(12).Value & "'" & ","
          strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").Value, ",", ".", 1) & ","
          strinsert = strinsert & "'" & grdDBGrid1(0).Columns(7).Value & "'" & ","
          strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").Value, ",", ".", 1)
          strinsert = strinsert & ")"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
          objWinInfo.objWinActiveForm.blnChanged = False
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      grdDBGrid1(0).Bookmark = mvarBook
      grdDBGrid1(0).Col = 13
    End If
    gintprodtotal = 0
  End If
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  cmdGupoProd.Enabled = True
End Sub

Private Sub cmdInformacion_Click()

  If objWinInfo.objWinActiveForm.strName = "PRN" Then
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    End If
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Exit Sub
    End If
  End If

  cmdInformacion.Enabled = False
  If grdDBGrid1(0).Rows > 0 Then
    glngMasInfProd = grdDBGrid1(0).Columns(5).Value
    Call objsecurity.LaunchProcess("FR0200")
    glngMasInfProd = 0
  Else
    Call MsgBox("Debe elegir un producto.", vbInformation, "Aviso")
  End If
  cmdInformacion.Enabled = True
End Sub

Private Sub cmdleerlector_Click()
Dim v As Integer
Dim i As Integer
'Dim noinsertar As Boolean
Dim mensaje As String
Dim mvarBook As Variant
Dim mblnBook As Boolean
Dim rstprod As rdoResultset
Dim strprod As String
Dim strinsert As String

  If objWinInfo.objWinActiveForm.strName = "PRN" Then
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    End If
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Exit Sub
    End If
  End If

cmdleerlector.Enabled = False

If IsNumeric(txtText1(0).Text) And txtText1(16).Text <> "" Then
  If txtText1(25).Text <> 1 Then
    Call MsgBox("No puede a�adir datos a la Petici�n", vbInformation, "Aviso")
    cmdleerlector.Enabled = True
    Exit Sub
  End If
  'noinsertar = True
  mblnBook = False
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Load frmLECTOROM
  Call frmLECTOROM.Show(vbModal)
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  If gintprodtotal > 0 Then
    grdDBGrid1(0).Bookmark = mvarBook
    For v = 0 To gintprodtotal - 1
      'se mira que no se inserte 2 veces el mismo producto
      'If grdDBGrid1(0).Rows > 0 Then
      '  grdDBGrid1(0).MoveFirst
      '  For i = 0 To grdDBGrid1(0).Rows - 1
      '    If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
      '       And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
      '      'no se inserta el producto pues ya est� insertado en el grid
      '      noinsertar = False
      '      Exit For
      '    Else
      '      noinsertar = True
      '    End If
      '    grdDBGrid1(0).MoveNext
      '  Next i
      'End If
      'If noinsertar = True Then
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0)  'c�d.prod
        grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1)  'interno
        grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2)  'desc.prod
        grdDBGrid1(0).Columns(11).Value = gintprodbuscado(v, 3) 'dosis
        
        strprod = "SELECT FRH7CODFORMFAR,FR93CODUNIMEDIDA,FR73REFERENCIA FROM FR7300 " & _
                " WHERE FR73CODPRODUCTO=" & gintprodbuscado(v, 0)
        Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
        If Not IsNull(rstprod.rdoColumns("FRH7CODFORMFAR").Value) Then
            grdDBGrid1(0).Columns(10).Value = rstprod.rdoColumns("FRH7CODFORMFAR").Value 'FF
        End If
        If Not IsNull(rstprod.rdoColumns("FR93CODUNIMEDIDA").Value) Then
            grdDBGrid1(0).Columns(12).Value = rstprod.rdoColumns("FR93CODUNIMEDIDA").Value 'UM
        End If
        If Not IsNull(rstprod.rdoColumns("FR73REFERENCIA").Value) Then
            grdDBGrid1(0).Columns(7).Value = rstprod.rdoColumns("FR73REFERENCIA").Value 'REF
        End If
        If Not IsNull(rstprod.rdoColumns("FRH7CODFORMFAR").Value) Then
            grdDBGrid1(0).Columns(10).Value = rstprod.rdoColumns("FRH7CODFORMFAR").Value 'FF
        End If
        grdDBGrid1(0).Columns(13).Value = gintprodbuscado(v, 3) 'CANTIDAD
        grdDBGrid1(0).Columns(14).Value = "/"
        If mblnBook = False Then
          mblnBook = True
          mvarBook = grdDBGrid1(0).Bookmark
        End If
        strinsert = "INSERT INTO FR2800(FR66CODPETICION,FR28NUMLINEA,FR28OPERACION,"
        strinsert = strinsert & "FR73CODPRODUCTO,FRH7CODFORMFAR,FR28DOSIS,FR93CODUNIMEDIDA,"
        strinsert = strinsert & "FR28CANTIDAD,FR28REFERENCIA,FR28CANTDISP) VALUES ("
        strinsert = strinsert & grdDBGrid1(0).Columns(3).Value & ","
        strinsert = strinsert & grdDBGrid1(0).Columns(4).Value & ","
        strinsert = strinsert & "'" & grdDBGrid1(0).Columns(14).Value & "'" & ","
        strinsert = strinsert & grdDBGrid1(0).Columns(5).Value & ","
        strinsert = strinsert & "'" & grdDBGrid1(0).Columns(10).Value & "'" & ","
        strinsert = strinsert & grdDBGrid1(0).Columns(11).Value & ","
        strinsert = strinsert & "'" & grdDBGrid1(0).Columns(12).Value & "'" & ","
        strinsert = strinsert & grdDBGrid1(0).Columns("Cantidad").Value & ","
        strinsert = strinsert & "'" & grdDBGrid1(0).Columns(7).Value & "'" & ","
        strinsert = strinsert & grdDBGrid1(0).Columns("Cantidad").Value
        strinsert = strinsert & ")"
        objApp.rdoConnect.Execute strinsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        objWinInfo.objWinActiveForm.blnChanged = False
      'Else
      '  mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
      '  Chr(13), vbInformation)
      'End If
      'noinsertar = True
    Next v
    grdDBGrid1(0).Bookmark = mvarBook
    grdDBGrid1(0).Col = 13
  End If
  gintprodtotal = 0
End If
Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
cmdleerlector.Enabled = True

End Sub

Private Sub cmdNoForm_Click()
  
  If objWinInfo.objWinActiveForm.strName = "PRN" Then
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    End If
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Exit Sub
    End If
  End If
  
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  grdDBGrid1(0).Columns(5).Value = 999999999
  grdDBGrid1(0).Columns(12).Value = "NE"
  grdDBGrid1(0).Columns(14).Value = "/"
  grdDBGrid1(0).Columns("Cantidad").Value = 1
End Sub

Private Sub cmdNoReponer_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim strupdate As String

cmdNoReponer.Enabled = False

mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
If mintNTotalSelRows > 0 Then
  For mintisel = 0 To mintNTotalSelRows - 1
    mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
    strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=-1" & _
                " WHERE FR66CODPETICION=" & grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk) & _
                " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk)
    objApp.rdoConnect.Execute strupdate, 64
  Next mintisel
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.DataRefresh
Else
  Call MsgBox("Debe seleccionar alg�n producto", vbInformation, "Aviso")
End If

cmdNoReponer.Enabled = True
End Sub

Private Sub cmdProductos_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim mvarBook As Variant
Dim mblnBook As Boolean
Dim strinsert As String

  If objWinInfo.objWinActiveForm.strName = "PRN" Then
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    End If
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Exit Sub
    End If
  End If

  cmdProductos.Enabled = False
  If IsNumeric(txtText1(0).Text) And txtText1(16).Text <> "" Then
    If txtText1(25).Text <> 1 Then
      Call MsgBox("No puede a�adir datos a la Petici�n", vbInformation, "Aviso")
      cmdProductos.Enabled = True
      Exit Sub
    End If
    noinsertar = True
    mblnBook = False
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    gstrLlamadorProd = "frmPedirServEspeciales"
    glngcodpeticion = txtText1(0).Text
    Call objsecurity.LaunchProcess("FR0165")
    gstrLlamadorProd = ""
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      grdDBGrid1(0).Bookmark = mvarBook
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0)
          grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1)
          grdDBGrid1(0).Columns(7).Value = gintprodbuscado(v, 8)
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2)
          grdDBGrid1(0).Columns(9).Value = gintprodbuscado(v, 9)
          grdDBGrid1(0).Columns(10).Value = gintprodbuscado(v, 5)
          grdDBGrid1(0).Columns(11).Value = gintprodbuscado(v, 3)
          grdDBGrid1(0).Columns(12).Value = gintprodbuscado(v, 4)
          grdDBGrid1(0).Columns(14).Value = "/"
          grdDBGrid1(0).Columns("Cantidad").Value = 1
          If mblnBook = False Then
            mblnBook = True
            mvarBook = grdDBGrid1(0).Bookmark
          End If
        strinsert = "INSERT INTO FR2800(FR66CODPETICION,FR28NUMLINEA,FR28OPERACION,"
        strinsert = strinsert & "FR73CODPRODUCTO,FRH7CODFORMFAR,FR28DOSIS,FR93CODUNIMEDIDA,"
        strinsert = strinsert & "FR28CANTIDAD,FR28REFERENCIA,FR28CANTDISP) VALUES ("
        strinsert = strinsert & grdDBGrid1(0).Columns(3).Value & ","
        strinsert = strinsert & grdDBGrid1(0).Columns(4).Value & ","
        strinsert = strinsert & "'" & grdDBGrid1(0).Columns(14).Value & "'" & ","
        strinsert = strinsert & grdDBGrid1(0).Columns(5).Value & ","
        strinsert = strinsert & "'" & grdDBGrid1(0).Columns(10).Value & "'" & ","
        If IsNumeric(grdDBGrid1(0).Columns(11).Value) Then
          strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns(11).Value, ",", ".", 1) & ","
        Else
          strinsert = strinsert & "NULL" & ","
        End If
        strinsert = strinsert & "'" & grdDBGrid1(0).Columns(12).Value & "'" & ","
        strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").Value, ",", ".", 1) & ","
        strinsert = strinsert & "'" & grdDBGrid1(0).Columns(7).Value & "'" & ","
        strinsert = strinsert & objGen.ReplaceStr(grdDBGrid1(0).Columns("Cantidad").Value, ",", ".", 1)
        strinsert = strinsert & ")"
        objApp.rdoConnect.Execute strinsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        objWinInfo.objWinActiveForm.blnChanged = False
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      grdDBGrid1(0).Bookmark = mvarBook
      grdDBGrid1(0).Col = 13
    End If
    gintprodtotal = 0
  End If
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  cmdProductos.Enabled = True
End Sub


Private Sub CmdUltimaOM_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim mvarBook As Variant
Dim mblnBook As Boolean

  If objWinInfo.objWinActiveForm.strName = "PRN" Then
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    End If
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Exit Sub
    End If
  End If

  CmdUltimaOM.Enabled = False
  If IsNumeric(txtText1(0).Text) And txtText1(16).Text <> "" Then
    If txtText1(25).Text <> 1 Then
      Call MsgBox("No puede a�adir datos a la Petici�n", vbInformation, "Aviso")
      CmdUltimaOM.Enabled = True
      Exit Sub
    End If
    noinsertar = True
    mblnBook = False
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    gstrLlamadorProd = "frmPedirServEspeciales"
    glngselpaciente = txtText1(2).Text
    Call objsecurity.LaunchProcess("FR0115")
    gstrLlamadorProd = ""
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    If gintprodtotal > 0 Then
      grdDBGrid1(0).Bookmark = mvarBook
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(0).Rows > 0 Then
          grdDBGrid1(0).MoveFirst
          For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(0).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 2) 'C�digo producto
          'grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 5)
          grdDBGrid1(0).Columns(7).Value = gintprodbuscado(v, 10)
          grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 6)
          If IsNumeric(gintprodbuscado(v, 7)) Then
            grdDBGrid1(0).Columns("Cantidad").Value = gintprodbuscado(v, 7)
          Else
            grdDBGrid1(0).Columns("Cantidad").Value = 1
          End If
          grdDBGrid1(0).Columns(10).Value = gintprodbuscado(v, 11)
          grdDBGrid1(0).Columns(11).Value = gintprodbuscado(v, 8)
          grdDBGrid1(0).Columns(12).Value = gintprodbuscado(v, 9)
          grdDBGrid1(0).Columns(14).Value = "/"
          grdDBGrid1(0).Columns("Inst. de Administraci�n").Value = gintprodbuscado(v, 5)
          
          If mblnBook = False Then
            mblnBook = True
            mvarBook = grdDBGrid1(0).Bookmark
          End If
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      grdDBGrid1(0).Bookmark = mvarBook
      grdDBGrid1(0).Col = 13
    End If
    gintprodtotal = 0
  End If
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.DataSave
  CmdUltimaOM.Enabled = True
End Sub

Private Sub cmdVerCesta_Click()
  cmdVerCesta.Enabled = False
  gstrLlamadorPRN = "CestaServEspecial"
  glngselpaciente = txtText1(2).Text
  Call objsecurity.LaunchProcess("FR0172")
  gstrLlamadorPRN = ""
  cmdVerCesta.Enabled = True
End Sub


Private Sub cmdVerPRN_Click()
  cmdVerPRN.Enabled = False
  gstrLlamadorPRN = "PRNServEspecial"
  glngselpaciente = txtText1(2).Text
  Call objsecurity.LaunchProcess("FR0172")
  gstrLlamadorPRN = ""
  cmdVerPRN.Enabled = True
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
On Error GoTo ERR
If Index = 0 Then
  grdDBGrid1(0).Columns(15).Value = grdDBGrid1(0).Columns(13).Value
  If grdDBGrid1(0).Columns("Bloqueada").Value = -1 Then
      For i = 3 To 18
          grdDBGrid1(0).Columns(i).CellStyleSet "LineaBloqueada"
      Next i
  End If
End If
ERR:

End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
If strFormName = "PRN" And strCtrlName = "txtText1(13)" Then
    aValues(2) = txtText1(14).Text  'proceso
End If
If strFormName = "PRN" And strCtrlName = "txtText1(27)" Then  'SECCION
    aValues(2) = txtText1(16).Text  'DEPARTAMENTO
End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  If grdDBGrid1(2).Rows > 0 Then
    grdDBGrid1(2).Columns("C�digo Petici�n").Position = 1
    grdDBGrid1(2).Columns("Fecha Inicio Vigencia").Position = 2
    grdDBGrid1(2).Columns("Hora Redacci�n").Position = 3
    grdDBGrid1(2).Columns("Estado").Position = 4
    grdDBGrid1(2).Columns("Nombre Paciente").Position = 5
    'grdDBGrid1(2).Columns("Apellido 1� Paciente").Position = 6
    'grdDBGrid1(2).Columns("Apellido 2� Paciente").Position = 7
    'grdDBGrid1(2).Columns("Historia").Position = 8
    'grdDBGrid1(2).Columns("Cama").Position = 9
    grdDBGrid1(2).Columns("Estado").Position = 10
    'grdDBGrid1(2).Columns("Edad").Position = 11
    'grdDBGrid1(2).Columns("Sexo").Position = 12
    'grdDBGrid1(2).Columns("Peso").Position = 13
    'grdDBGrid1(2).Columns("Altura Cm").Position = 14
    grdDBGrid1(2).Columns("Desc.Departamento").Position = 15
    grdDBGrid1(2).Columns("Apellido Doctor").Position = 16
    'grdDBGrid1(2).Columns("Apellido 1� Enfermera").Position = 17
  End If
  
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  If strFormName = "Detalle PRN" Then
    grdDBGrid1(0).Columns(15).Value = grdDBGrid1(0).Columns(13).Value
  End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

'Private Sub Validar_orden()
''procedimiento que coge todos los productos de la orden validada y
''escribe un registro en FR3200
'Dim rsta As rdoResultset
'Dim stra As String
'Dim i As Integer
'Dim strInsert As String
'Dim hora As Variant
'Dim strfec As String
'Dim rstfec As rdoResultset
'Dim strobser As String
'Dim rstobser As rdoResultset
'
''se determina la hora del sistema
'strfec = "(SELECT TO_CHAR(SYSDATE,'HH24') FROM DUAL)"
'Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
'hora = rstfec.rdoColumns(0).Value
'hora = hora & "."
'strfec = ""
'strfec = "(SELECT TO_CHAR(SYSDATE,'mi') FROM DUAL)"
'Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
'hora = hora & rstfec.rdoColumns(0).Value
'rstfec.Close
'Set rstfec = Nothing
'
'
'
'grdDBGrid1(0).MoveFirst
'For i = 0 To grdDBGrid1(0).Rows - 1
'    stra = "SELECT FR28DOSIS,FR34CODVIA,FR28VOLUMEN,FR28TIEMINFMIN,FRG4CODFRECUENCIA," & _
'            "FR28INDSN,FR93CODUNIMEDIDA,FR28INDDISPPRN,FR28INDCOMIENINMED," & _
'            "FRH5CODPERIODICIDAD,FRH7CODFORMFAR,FR28OPERACION,FR28CANTIDAD," & _
'            "TO_CHAR(FR28FECINICIO,'DD/MM/YYYY'),FR28HORAINICIO," & _
'            "TO_DATE(TO_CHAR(FR28FECFIN,'DD/MM/YYYY'),'DD/MM/YYYY'),FR28HORAFIN," & _
'            "FR28INDVIAOPC,FR73CODPRODUCTO_DIL,FR28CANTIDADDIL,FR28TIEMMININF" & _
'            " FROM FR2800 WHERE " & _
'            "FR66CODPETICION=" & txtText1(0).Text & _
'            " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value & _
'            " AND FR73CODPRODUCTO=" & grdDBGrid1(0).Columns(6).Value
'    Set rsta = objApp.rdoConnect.OpenResultset(stra)
'
'    strInsert = "INSERT INTO FR3200 (FR66CODPETICION,FR28NUMLINEA,FR32NUMMODIFIC," & _
'            "FR28DOSIS,FR34CODVIA,FR32VOLUMEN,FR32TIEMINFMIN,FRG4CODFRECUENCIA," & _
'            "FR32OBSERVFARM," & _
'            "SG02COD_FRM,FR32FECMODIVALI,FR32HORAMODIVALI,FR32INDSN," & _
'            "FR93CODUNIMEDIDA," & _
'            "FR32INDISPEN,FR32INDCOMIENINMED,FRH5CODPERIODICIDAD,FRH7CODFORMFAR," & _
'            "FR32OPERACION,FR32CANTIDAD,FR32FECINICIO,FR32HORAINICIO," & _
'            "FR32FECFIN,FR32HORAFIN,FR32INDVIAOPC,FR73CODPRODUCTO_DIL," & _
'            "FR32CANTIDADDIL,FR32TIEMMININF,FR73CODPRODUCTO" & _
'            ")" & _
'            " VALUES (" & _
'            txtText1(0).Text & "," & _
'            grdDBGrid1(0).Columns(5).Value & "," & _
'            "1" & ","
'            If IsNull(rsta.rdoColumns("FR28DOSIS").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & objGen.ReplaceStr(rsta.rdoColumns("FR28DOSIS").Value, ",", ".", 1) & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR34CODVIA").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & "'" & rsta.rdoColumns("FR34CODVIA").Value & "'" & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR28VOLUMEN").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & objGen.ReplaceStr(rsta.rdoColumns("FR28VOLUMEN").Value, ",", ".", 1) & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR28TIEMINFMIN").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & objGen.ReplaceStr(rsta.rdoColumns("FR28TIEMINFMIN").Value, ",", ".", 1) & ","
'            End If
'            If IsNull(rsta.rdoColumns("FRG4CODFRECUENCIA").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & "'" & rsta.rdoColumns("FRG4CODFRECUENCIA").Value & "'" & ","
'            End If
'            strobser = "SELECT FR57OBSERVACION FROM FR5700 WHERE " & _
'                      "FR57CODPETOBSER=" & txtText1(0).Text & _
'                      " AND FR57NUMLINEA=" & grdDBGrid1(0).Columns(5).Value & _
'                      "AND FR73CODPRODUCTO=" & grdDBGrid1(0).Columns(6).Value
'            Set rstobser = objApp.rdoConnect.OpenResultset(strobser)
'            If Not rstobser.EOF Then
'            If IsNull(rstobser.rdoColumns("FR57OBSERVACION").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & "'" & rstobser.rdoColumns("FR57OBSERVACION").Value & "'" & ","
'            End If
'            Else
'                strInsert = strInsert & "Null" & ","
'            End If
'            strInsert = strInsert & "'" & objsecurity.strUser & "'" & "," & _
'            "SYSDATE" & "," & _
'            hora & ","
'            If IsNull(rsta.rdoColumns("FR28INDSN").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & rsta.rdoColumns("FR28INDSN").Value & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & "'" & rsta.rdoColumns("FR93CODUNIMEDIDA").Value & "'" & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR28INDDISPPRN").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & rsta.rdoColumns("FR28INDDISPPRN").Value & ","
'           End If
'            If IsNull(rsta.rdoColumns("FR28INDCOMIENINMED").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & rsta.rdoColumns("FR28INDCOMIENINMED").Value & ","
'            End If
'            If IsNull(rsta.rdoColumns("FRH5CODPERIODICIDAD").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & "'" & rsta.rdoColumns("FRH5CODPERIODICIDAD").Value & "'" & ","
'            End If
'            If IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & "'" & rsta.rdoColumns("FRH7CODFORMFAR").Value & "'" & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR28OPERACION").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & "'" & rsta.rdoColumns("FR28OPERACION").Value & "'" & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR28CANTIDAD").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & objGen.ReplaceStr(rsta.rdoColumns("FR28CANTIDAD").Value, ",", ".", 1) & ","
'            End If
'            If IsNull(rsta.rdoColumns(13).Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & "TO_CHAR(TO_DATE('" & rsta.rdoColumns(13).Value & "','DD/MM/YYYY'))" & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR28HORAINICIO").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & rsta.rdoColumns("FR28HORAINICIO").Value & ","
'            End If
'            If IsNull(rsta.rdoColumns(15).Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & rsta.rdoColumns(15).Value & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR28HORAFIN").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & rsta.rdoColumns("FR28HORAFIN").Value & ","
'            End If
'           If IsNull(rsta.rdoColumns("FR28INDVIAOPC").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & rsta.rdoColumns("FR28INDVIAOPC").Value & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR28CANTIDADDIL").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & objGen.ReplaceStr(rsta.rdoColumns("FR28CANTIDADDIL").Value, ",", ".", 1) & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR28TIEMMININF").Value) Then
'                strInsert = strInsert & "Null" & ","
'            Else
'                strInsert = strInsert & rsta.rdoColumns("FR28TIEMMININF").Value & ","
'            End If
'            strInsert = strInsert & grdDBGrid1(0).Columns(6).Value & ")"
'
'            objApp.rdoConnect.Execute strInsert, 64
'            objApp.rdoConnect.Execute "Commit", 64
'
'
'            strInsert = "UPDATE FR3200 SET FR32FECINICIO=(SELECT FR28FECINICIO FROM FR2800" & _
'                        " WHERE " & _
'                        "FR66CODPETICION=" & txtText1(0).Text & _
'                        " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value & _
'                        " AND FR73CODPRODUCTO=" & grdDBGrid1(0).Columns(6).Value & _
'                        ") WHERE FR66CODPETICION=" & txtText1(0).Text & _
'                        " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value
'            objApp.rdoConnect.Execute strInsert, 64
'            objApp.rdoConnect.Execute "Commit", 64
'
'            rsta.Close
'            Set rsta = Nothing
'            rstobser.Close
'            Set rstobser = Nothing
'
'grdDBGrid1(0).MoveNext
'Next i
'
'End Sub

'Private Sub cmdbloquear_Click()'
'
'Dim vntDatos(1 To 9) As Variant
'Dim strprod As String
'Dim rstprod As rdoResultset
'
'cmdbloquear.Enabled = False'
'
''se mira que el producto no sea estupefaciente
'strprod = "SELECT FR73INDESTUPEFACI FROM FR7300 WHERE " & _
'            "FR73CODPRODUCTO=" & grdDBGrid1(0).Columns(6).Value
'Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
'If rstprod.rdoColumns(0).Value = -1 Then
'    Call MsgBox("El producto es un estupefaciente." & Chr(13) & _
'        "No puede bloquear o desbloquear la l�nea.", vbInformation, "Aviso")
'    cmdbloquear.Enabled = True
'    Exit Sub
'End If
'    rstprod.Close
'    Set rstprod = Nothing
'If txtText1(25).Text = 4 Then 'orden validada
'    Call MsgBox("La Orden M�dica ya est� validada." & Chr(13) & _
'                      "No puede bloquear o desbloquear l�neas.", vbInformation, "Aviso")
'Else
'    If grdDBGrid1(0).Rows > 0 Then
'        vntDatos(1) = grdDBGrid1(0).Columns(4).Value
'        vntDatos(2) = grdDBGrid1(0).Columns(5).Value
'        'vntDatos(3) = chkCheck1(12).Value
'        vntDatos(4) = 0
'        vntDatos(5) = 0
'        vntDatos(6) = "null"
'        vntDatos(7) = 0
 '       vntDatos(8) = grdDBGrid1(0).Columns(6).Value
'        vntDatos(9) = "null"
'        Call objsecurity.LaunchProcess("FR0105", vntDatos)
'        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
'        objWinInfo.DataRefresh
'    End If
'End If
'cmdbloquear.Enabled = True
'End Sub

'Private Sub cmdbuscarprod_Click()
'Dim mensaje As String'
'
'cmdbuscarprod.Enabled = False
'If txtText1(25).Text = 4 Then 'orden validada
'    mensaje = MsgBox("La Orden M�dica ya est� validada." & Chr(13) & _
'                      "No puede a�adir nuevos productos.", vbInformation, "Aviso")
'Else
'    'frmBusProductos
'    gintbusprod = 1
'    Call objsecurity.LaunchProcess("FR0119")
'    '***********************
'     If gintbusprod = 1 Then
'            Call objsecurity.LaunchProcess("FR0150")
'     End If
'    '*********************
'    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
'    objWinInfo.DataRefresh
'    gintbusprod = 0
'End If
'cmdbuscarprod.Enabled = True
'End Sub



'Private Sub cmdmodificar_Click()
'Dim mensaje As String
'Dim strInsert As String
'Dim rsta As rdoResultset
'Dim stra As String
'Dim rstlinea As rdoResultset
'Dim strlinea As String
'Dim linea As Long
'Dim strupdate As String
'Dim strprod As String
'Dim rstprod As rdoResultset
'
'
'cmdmodificar.Enabled = False
'If grdDBGrid1(0).Rows > 0 Then
'If txtText1(25).Text = 4 Then 'orden validada
'    mensaje = MsgBox("La Orden M�dica ya est� validada." & Chr(13) & _
'                      "No puede modificar sus productos.", vbInformation, "Aviso")
'Else
''se mira que el producto no sea estupefaciente
'strprod = "SELECT FR73INDESTUPEFACI FROM FR7300 WHERE " & _
'            "FR73CODPRODUCTO=" & grdDBGrid1(0).Columns(6).Value
'Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
'If rstprod.rdoColumns(0).Value = -1 Then
'    mensaje = MsgBox("El producto es un estupefaciente." & Chr(13) & _
'        "No puede modificar la l�nea.", vbInformation, "Aviso")
'    rstprod.Close
'    Set rstprod = Nothing
'Else 'no es estupefaciente
'    rstprod.Close
'    Set rstprod = Nothing
'    'se mira si la l�nea est� bloqueada
'    strprod = "SELECT FR28INDBLOQUEADA FROM FR2800 WHERE FR66CODPETICION=" & grdDBGrid1(0).Columns(4).Value & _
'      " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value & _
'      " AND FR73CODPRODUCTO=" & grdDBGrid1(0).Columns(6).Value
'    Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
'    If rstprod.rdoColumns(0).Value = -1 Then
'       mensaje = MsgBox("La l�nea est� bloqueada. No puede modificarse", vbInformation, "Aviso")
'       rstprod.Close
'       Set rstprod = Nothing
'    Else 'la l�nea no est� bloqueada
'        rstprod.Close
'        Set rstprod = Nothing
'        'hay que crear una nueva l�nea id�ntica a la seleccionada
'        strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
'                    grdDBGrid1(0).Columns(4).Value
'        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
'        If IsNull(rstlinea.rdoColumns(0).Value) Then
'                linea = 1 + grdDBGrid1(0).Row
'        Else
'                linea = rstlinea.rdoColumns(0).Value + 1 + grdDBGrid1(0).Row
'        End If
'        gintlinea = linea
'        rstlinea.Close
'        Set rstlinea = Nothing'
'
'        stra = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & grdDBGrid1(0).Columns(4).Value & _
'              " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value & _
'              " AND FR73CODPRODUCTO=" & grdDBGrid1(0).Columns(6).Value
'        Set rsta = objApp.rdoConnect.OpenResultset(stra)
'
'        strInsert = "INSERT INTO FR2800 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO,FR34CODVIA," & _
'          "FR28DOSIS,FR28VOLUMEN,FR28TIEMINFMIN,FRG4CODFRECUENCIA,FR28INDDISPPRN,FR28INDCOMIENINMED," & _
'          "FR28INDBLOQUEADA,FR28FECBLOQUEO," & _
'          "SG02CODPERSBLOQ,FR28INDSN,FR93CODUNIMEDIDA," & _
'          "FR28INDESTOM,FRH5CODPERIODICIDAD,FRH7CODFORMFAR,FR28OPERACION," & _
'          "FR28CANTIDAD,FR28FECINICIO,FR28HORAINICIO,FR28FECFIN,FR28HORAFIN,FR28INDVIAOPC," & _
'          "FR73CODPRODUCTO_DIL,FR28CANTIDADDIL,FR28TIEMMININF" & _
'          ") VALUES " & _
'          "(" & grdDBGrid1(0).Columns(4).Value & "," & _
'          linea & "," & _
'          grdDBGrid1(0).Columns(6).Value & ","
'
'
'          If IsNull(rsta.rdoColumns("FR34CODVIA").Value) Then
'            strInsert = strInsert & "null" & ","
'          Else
'            strInsert = strInsert & "'" & rsta.rdoColumns("FR34CODVIA").Value & "'" & ","
'          End If
'            If IsNull(rsta.rdoColumns("FR28DOSIS").Value) Then
'              strInsert = strInsert & "null" & ","
'            Else
'              strInsert = strInsert & objGen.ReplaceStr(rsta.rdoColumns("FR28DOSIS").Value, ",", ".", 1) & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR28VOLUMEN").Value) Then
'              strInsert = strInsert & "null" & ","
'            Else
'              strInsert = strInsert & objGen.ReplaceStr(rsta.rdoColumns("FR28VOLUMEN").Value, ",", ".", 1) & ","
'            End If
'            If IsNull(rsta.rdoColumns("FR28TIEMINFMIN").Value) Then
'              strInsert = strInsert & "null" & ","
'            Else
'              strInsert = strInsert & objGen.ReplaceStr(rsta.rdoColumns("FR28TIEMINFMIN").Value, ",", ".", 1) & ","
'            End If
'            If IsNull(rsta.rdoColumns("FRG4CODFRECUENCIA").Value) Then
'              strInsert = strInsert & "null" & ","
'            Else
'              strInsert = strInsert & "'" & rsta.rdoColumns("FRG4CODFRECUENCIA").Value & "'" & ","
'            End If
'        If IsNull(rsta.rdoColumns("FR28INDDISPPRN").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & rsta.rdoColumns("FR28INDDISPPRN").Value & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28INDCOMIENINMED").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & rsta.rdoColumns("FR28INDCOMIENINMED").Value & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28INDBLOQUEADA").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & rsta.rdoColumns("FR28INDBLOQUEADA").Value & ","
'        End If
'        strInsert = strInsert & "null" & ","
'        strInsert = strInsert & "null" & ","
'        If IsNull(rsta.rdoColumns("FR28INDSN").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & rsta.rdoColumns("FR28INDSN").Value & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & "'" & rsta.rdoColumns("FR93CODUNIMEDIDA").Value & "'" & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28INDESTOM").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & rsta.rdoColumns("FR28INDESTOM").Value & ","
'        End If
'        If IsNull(rsta.rdoColumns("FRH5CODPERIODICIDAD").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & "'" & rsta.rdoColumns("FRH5CODPERIODICIDAD").Value & "'" & ","
'        End If
'        If IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & "'" & rsta.rdoColumns("FRH7CODFORMFAR").Value & "'" & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28OPERACION").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & "'" & rsta.rdoColumns("FR28OPERACION").Value & "'" & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28CANTIDAD").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & objGen.ReplaceStr(rsta.rdoColumns("FR28CANTIDAD").Value, ",", ".", 1) & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28FECINICIO").Value) Then
'            strInsert = strInsert & "Null" & ","
'        Else
'            strInsert = strInsert & "TO_DATE('" & rsta.rdoColumns("FR28FECINICIO").Value & "','DD/MM/YYYY')" & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28HORAINICIO").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & rsta.rdoColumns("FR28HORAINICIO").Value & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28FECFIN").Value) Then
'            strInsert = strInsert & "Null" & ","
'        Else
'            strInsert = strInsert & "TO_DATE('" & rsta.rdoColumns("FR28FECFIN").Value & "','DD/MM/YYYY')" & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28HORAFIN").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & rsta.rdoColumns("FR28HORAFIN").Value & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28INDVIAOPC").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & rsta.rdoColumns("FR28INDVIAOPC").Value & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28CANTIDADDIL").Value) Then
'          strInsert = strInsert & "null" & ","
'        Else
'          strInsert = strInsert & objGen.ReplaceStr(rsta.rdoColumns("FR28CANTIDADDIL").Value, ",", ".", 1) & ","
'        End If
'        If IsNull(rsta.rdoColumns("FR28TIEMMININF").Value) Then
'          strInsert = strInsert & "null"
'        Else
'          strInsert = strInsert & rsta.rdoColumns("FR28TIEMMININF").Value
'        End If
'        strInsert = strInsert & ")"
'        objApp.rdoConnect.Execute strInsert, 64
'        objApp.rdoConnect.Execute "Commit", 64
'        rsta.Close
'        Set rsta = Nothing
'        'se bloquea la l�nea seleccionada
'        strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=-1," & _
'               "FR28FECBLOQUEO=(SELECT SYSDATE FROM DUAL)," & _
'               "SG02CODPERSBLOQ='" & objsecurity.strUser & "'" & _
'            " WHERE FR66CODPETICION=" & grdDBGrid1(0).Columns(4).Value & _
'            " AND FR28NUMLINEA=" & grdDBGrid1(0).Columns(5).Value & _
'            " AND FR73CODPRODUCTO=" & grdDBGrid1(0).Columns(6).Value
'        objApp.rdoConnect.Execute strupdate, 64
'        objApp.rdoConnect.Execute "Commit", 64
'
'        Call objsecurity.LaunchProcess("FR0150")
'
'        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
'        objWinInfo.DataRefresh
'   End If
'End If
'End If
'End If
'cmdmodificar.Enabled = True
'End Sub



Private Sub Form_Activate()
  Dim strCodDpto As String
  Dim strserviciomed As String
  Dim rstserviciomed As rdoResultset
  
  txtText1(29).Locked = True
  gblnEsPRN = True
  
  
If gblnSelPaciente = True Then
  Me.Enabled = False
    'frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Proceso").Value
    'frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Asistencia").Value
    'frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Dpto.").Value
    ''frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Cama").Value
    'frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Doctor").Value
    'gblnSelPaciente = False
  If blnNoPrimAct = False Then
      strserviciomed = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD=" & _
                       "'" & objsecurity.strUser & "' AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
      Set rstserviciomed = objApp.rdoConnect.OpenResultset(strserviciomed)
      If Not rstserviciomed.EOF Then
        'ad02coddpto = rstserviciomed.rdoColumns(0).Value
        strCodDpto = rstserviciomed.rdoColumns(0).Value    'servicio
      Else
        rstserviciomed.Close
        Set rstserviciomed = Nothing
        strCodDpto = frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Dpto.").Value
      End If
    Call Nueva_PRN(frmSelPacienteServEspeciales.IdPersona1.Text, frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Proceso").Value, frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Asistencia").Value, strCodDpto, frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Doctor").Value)
    blnNoPrimAct = True
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  End If
  Me.Enabled = True
  Exit Sub
End If
  
  Screen.MousePointer = vbDefault

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "PRN"
      
    .strTable = "FR6600"
        
    Call .FormAddOrderField("FR66CODPETICION", cwAscending)
   
    strKey = .strDataBase & .strTable
    
    'intFirmarPRN = gintfirmarOM = 1
    'glngpeticion = vntData(2)
    
    If gintfirmarOM = 1 Then
      .intAllowance = cwAllowModify
      mstrCodPac = glngpeticion
      .strWhere = "FR66CODPETICION = " & mstrCodPac & _
                " AND (FR66INDESTUPEFACIENTE=0 or FR66INDESTUPEFACIENTE IS NULL)" & _
                " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
    Else
      mstrCodPac = glngselpaciente
      .strWhere = "(fr66indom<>-1 or fr66indom is null) and CI21CODPERSONA=" & mstrCodPac & _
                " AND (FR66INDESTUPEFACIENTE=0 or FR66INDESTUPEFACIENTE IS NULL)" & _
                " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
    End If
    
    'If gblnSelPaciente = True Then
    '  .intAllowance = cwAllowModify + cwAllowDelete
    'End If
    
    Call .FormCreateFilterWhere(strKey, "PRN")
    Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDALERGIA", "Al�rgico?", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Medicaci�n Infantil?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
   
    'Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "C�digo Grupo Protocolo")

  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Detalle PRN"
    
    .strTable = "FR2800"
    '.intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    Call .FormAddRelation("FR66CODPETICION", txtText1(0))
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfo, "C�digo", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Prod", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Tam Env", "", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "FF", "FRH7CODFORMFAR", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR28DOSIS", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "UM", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "FR28CANTIDAD", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Operaci�n", "FR28OPERACION", cwString, 1)
    Call .GridAddColumn(objMultiInfo, "Cant Disp", "FR28CANTDISP", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Prod No Formulario", "FR28DESPRODUCTO", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Inst. de Administraci�n", "FR28INSTRADMIN", cwString, 2000)
    Call .GridAddColumn(objMultiInfo, "Bloqueada", "FR28INDBLOQUEADA", cwBoolean)
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).intKeyNo = 1
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(0).Columns("Cantidad")).blnMandatory = True

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    '.CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    '.CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(txtText1(21)).blnInFind = True
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(txtText1(25)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(5)).blnInFind = True
    .CtrlGetInfo(chkCheck1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(15)).blnInFind = True
    
    .CtrlGetInfo(txtText1(29)).blnNegotiated = False
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(4), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(5), "CI22SEGAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(23), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(12), "CI30CODSEXO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(15)), "CI21CODPERSONA", "SELECT * FROM FR2200J WHERE FR2200J.CI21CODPERSONA= ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(15)), txtText1(46), "DESCCAMA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(13)), "AD01CODASISTENCI", "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD01CODASISTENCI= ? AND AD07CODPROCESO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(13)), txtText1(46), "GCFN06(AD15CODCAMA)")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(27)), "AD41CODSECCION", "SELECT AD41DESSECCION FROM AD4100 WHERE AD41CODSECCION= ? AND AD02CODDPTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(27)), txtText1(15), "AD41DESSECCION")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(12)), "CI30CODSEXO", "SELECT * FROM CI3000 WHERE CI30CODSEXO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(12)), txtText1(1), "CI30DESSEXO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "SG02NOM")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(19), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(11), "FR91DESURGENCIA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(15), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(61)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(61)), txtText1(62), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(26), "FR26DESESTADOPET")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(7), "FR73REFERENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(8), "FR73DESPRODUCTO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(9)), "FR34CODVIA", "SELECT * FROM FR3400 WHERE FR34CODVIA = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(9)), grdDBGrid1(0).Columns(10), "FR34DESVIA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(19)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(19)), grdDBGrid1(0).Columns(20), "FR93DESUNIMEDIDA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(13)), "FRG4CODFRECUENCIA", "SELECT * FROM FRG400 WHERE FRG4CODFRECUENCIA = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(13)), grdDBGrid1(0).Columns(14), "FRG4DESFRECUENCIA")
    
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(6)).blnForeign = True
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    '.CtrlGetInfo(txtText1(14)).blnForeign = True
    .CtrlGetInfo(txtText1(16)).blnForeign = True
    .CtrlGetInfo(txtText1(22)).blnForeign = True
    .CtrlGetInfo(txtText1(25)).blnForeign = True
    .CtrlGetInfo(txtText1(27)).blnForeign = True
    .CtrlGetInfo(txtText1(61)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnForeign = True
    
    .CtrlGetInfo(Text1).blnNegotiated = False
    '.CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnForeign = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(19)).blnForeign = True
     
    .CtrlGetInfo(txtText1(2)).blnReadOnly = True
    .CtrlGetInfo(txtText1(10)).blnReadOnly = True
    .CtrlGetInfo(txtText1(20)).blnReadOnly = True
    .CtrlGetInfo(txtText1(21)).blnReadOnly = True
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True
    .CtrlGetInfo(txtText1(25)).blnReadOnly = True
    .CtrlGetInfo(chkCheck1(3)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(10)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnReadOnly = True
    Call .WinRegister
    Call .WinStabilize
    
  End With
grdDBGrid1(0).Columns(3).Visible = False
grdDBGrid1(0).Columns(4).Visible = False
grdDBGrid1(0).Columns(5).Visible = False
grdDBGrid1(0).Columns(9).Visible = False
grdDBGrid1(0).Columns(14).Visible = False
grdDBGrid1(0).Columns(15).Visible = False
grdDBGrid1(0).Columns(18).Visible = False

grdDBGrid1(0).Columns(0).Width = 0
grdDBGrid1(0).Columns(5).Width = 1000   'c�d.producto
grdDBGrid1(0).Columns(6).Width = 800   'c�d interno
grdDBGrid1(0).Columns(7).Width = 1200   'referencia
grdDBGrid1(0).Columns(8).Width = 4000   'desc.producto
grdDBGrid1(0).Columns(9).Width = 800   'tam env
grdDBGrid1(0).Columns(10).Width = 500   'ff
grdDBGrid1(0).Columns(11).Width = 700   'dosis
grdDBGrid1(0).Columns(12).Width = 700  'c�d.medida
grdDBGrid1(0).Columns(13).Width = 1000  'cantidad compuesto

'mblnChange = True
  If gintfirmarOM = 1 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0) ' al primero
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
Dim strDelete As String
Dim stra As String
Dim rsta As rdoResultset
Dim strFR6600 As String
Dim rstFR6600 As rdoResultset

  intCancel = objWinInfo.WinExit
  If intCancel = 0 Then
    If IsNumeric(txtText1(0).Text) Then
        stra = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        If rsta.EOF Then
          strDelete = "DELETE FR6600 WHERE FR66CODPETICION=" & txtText1(0).Text
          objApp.rdoConnect.Execute strDelete, 64
          objApp.rdoConnect.Execute "Commit", 64
        Else
          strFR6600 = "SELECT * FROM FR6600 WHERE FR66CODPETICION=" & txtText1(0).Text
          Set rstFR6600 = objApp.rdoConnect.OpenResultset(strFR6600)
          If Not rstFR6600.EOF Then
            If IsNull(rstFR6600("FR26CODESTPETIC").Value) Then
              MsgBox "No puede salir sin haber Enviado o haber A�adido a Cesta la Petici�n", vbCritical, "Aviso"
              intCancel = -1
              Exit Sub
            Else
              If (rstFR6600("FR26CODESTPETIC").Value <> 3 And rstFR6600("FR26CODESTPETIC").Value <> 16) Then
                If IsNull(rstFR6600("FR66INDCESTA").Value) Then
                  MsgBox "No puede salir sin haber Enviado o haber A�adido a Cesta la Petici�n", vbCritical, "Aviso"
                  intCancel = -1
                  Exit Sub
                Else
                  If rstFR6600("FR66INDCESTA").Value <> -1 Then
                    MsgBox "No puede salir sin haber Enviado o haber A�adido a Cesta la Petici�n", vbCritical, "Aviso"
                    intCancel = -1
                    Exit Sub
                  End If
                End If
              End If
            End If
          End If
          rstFR6600.Close
          Set rstFR6600 = Nothing
        End If
        rsta.Close
        Set rsta = Nothing
    End If
  End If

End Sub

Private Sub Form_Unload(intCancel As Integer)
gblnEsPRN = False
Call objWinInfo.WinDeRegister
Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strFormName = "PRN" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "CI2200"

     Set objField = .AddField("CI21CODPERSONA")
     objField.strSmallDesc = "C�digo Paciente"

     Set objField = .AddField("CI22NOMBRE")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("CI22NUMHISTORIA")
     objField.strSmallDesc = "Historia"
     
     Set objField = .AddField("CI22PRIAPEL")
     objField.strSmallDesc = "Apellido 1�"
     
     Set objField = .AddField("CI22SEGAPEL")
     objField.strSmallDesc = "Apellido 2�"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("CI21CODPERSONA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "PRN" And strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"
     
     If txtText1(16).Text = "" Then
        .strWhere = " where sg02cod in (select sg02cod from ad0300 where ad02coddpto in " & _
                 "(select ad02coddpto from ad0300 where sg02cod='" & objsecurity.strUser & "') AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) )" & _
                 " and ad30codcategoria in (6,15)"
      Else
        .strWhere = " where sg02cod in (select sg02cod from ad0300 where ad02coddpto = " & txtText1(16).Text & " AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) " & _
                 ") and ad30codcategoria in (6,15)"
      End If

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo Enfermera"

     Set objField = .AddField("SG02NOM")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(6), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "PRN" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"
     
     If txtText1(16).Text = "" Then
        .strWhere = " where sg02cod in (select sg02cod from ad0300 where ad02coddpto in " & _
                 "(select ad02coddpto from ad0300 where sg02cod='" & objsecurity.strUser & "') AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) )" & _
                 " and ad30codcategoria in (1,2,3,8,18,19)"
      Else
        .strWhere = " where sg02cod in (select sg02cod from ad0300 where ad02coddpto = " & txtText1(16).Text & " AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) )" & _
                 " and ad30codcategoria in (1,2,3,8,18,19)"
      End If

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo M�dico"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 'If strFormName = "PRN" And strCtrl = "txtText1(14)" Then
 '   Set objSearch = New clsCWSearch
 '   With objSearch
 '    .strTable = "AD0200"
 '    .strWhere = "WHERE AD32CODTIPODPTO=3 AND " & _
 '          "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
 '          "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))" & _
 '          " AND AD02CODDPTO IN (SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "')"
'
'     Set objField = .AddField("AD02CODDPTO")
'     objField.strSmallDesc = "C�digo Servicio"'
'
'     Set objField = .AddField("AD02DESDPTO")
'     objField.strSmallDesc = "Descripci�n Servicio"

'     If .Search Then
'      Call objWinInfo.CtrlSet(txtText1(14), .cllValues("AD02CODDPTO"))
'     End If
 '  End With
 '  Set objSearch = Nothing
 'End If
 
 If strFormName = "PRN" And strCtrl = "txtText1(16)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02FECINICIO<TRUNC(SYSDATE) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>SYSDATE))" & _
           " AND AD02CODDPTO IN (SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "' AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) )"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Dpto."

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto."

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(16), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "PRN" And strCtrl = "txtText1(61)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02FECINICIO<TRUNC(SYSDATE) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>SYSDATE))" & _
           " AND AD02CODDPTO IN (SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "' AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) )"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Dpto."

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto."

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(61), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "PRN" And strCtrl = "txtText1(27)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD4100"

     Set objField = .AddField("AD41CODSECCION")
     objField.strSmallDesc = "C�digo Secci�n"

     Set objField = .AddField("AD41DESSECCION")
     objField.strSmallDesc = "Descripci�n Secci�n"
     
     If IsNumeric(txtText1(16).Text) Then
      .strWhere = "where AD02CODDPTO=" & txtText1(16).Text
     Else
       Call MsgBox("Debe introducir el Servicio antes de ver sus Secciones", vbInformation, "Aviso")
       Exit Sub
     End If

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(27), .cllValues("AD41CODSECCION"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "PRN" And strCtrl = "txtText1(22)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9100"

     Set objField = .AddField("FR91CODURGENCIA")
     objField.strSmallDesc = "C�digo Urgencia"

     Set objField = .AddField("FR91DESURGENCIA")
     objField.strSmallDesc = "Descripci�n Urgencia"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(22), .cllValues("FR91CODURGENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle PRN" And strCtrl = "grdDBGrid1(0).C�d.Prod" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"

     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"

     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(6), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
' If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).V�a" Then
'    Set objSearch = New clsCWSearch
'    With objSearch
'     .strTable = "FR3400"'
'
'     Set objField = .AddField("FR34CODVIA")
'     objField.strSmallDesc = "C�digo V�a"
'
'     Set objField = .AddField("FR34DESVIA")
'     objField.strSmallDesc = "Descripci�n"
'
'     If .Search Then
'      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(9), .cllValues("FR34CODVIA"))
'     End If
'   End With
'   Set objSearch = Nothing
' End If
 
' If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).Medida" Then
'    Set objSearch = New clsCWSearch
'    With objSearch
'     .strTable = "FR9300"'
'
'     Set objField = .AddField("FR93CODUNIMEDIDA")
'     objField.strSmallDesc = "C�digo Unidad de Medida"
'
'     Set objField = .AddField("FR93DESUNIMEDIDA")
'     objField.strSmallDesc = "Descripci�n"
'
'     If .Search Then
'      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(19), .cllValues("FR93CODUNIMEDIDA"))
'     End If
'   End With
'   Set objSearch = Nothing
' End If
 
End Sub



Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim mensaje As String
Dim SQL As String
Dim qry As rdoQuery
Dim rs As rdoResultset

If txtText1(16).Text <> "" Then
    stra = "SELECT * FROM AD0200 " & _
           "WHERE AD02FECINICIO<TRUNC(SYSDATE) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>SYSDATE))" & _
           " AND AD02CODDPTO=" & txtText1(16).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.EOF Then
        mensaje = MsgBox("El servicio es incorrecto.", vbInformation, "Aviso")
        blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
End If
If blnCancel = False Then
  If strFormName = "PRN" Then
    'si est� clicado Inter�s Cient�fico debe haber metido el Dpto. de Cargo
    If chkCheck1(15).Value = 1 Then
      If Trim(txtText1(61).Text) = "" Then
        mensaje = MsgBox("El Departamento de Cargo es obligatorio", vbInformation, "Aviso")
        blnCancel = True
      End If
    End If
    If txtText1(16).Text <> "" Then
      SQL = "SELECT COUNT(*)"
      SQL = SQL & " FROM AD4100"
      SQL = SQL & " WHERE AD02CODDPTO = ?"
      SQL = SQL & " AND SYSDATE BETWEEN AD41FECINICIO AND NVL(AD41FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
      Set qry = objApp.rdoConnect.CreateQuery("", SQL)
      qry(0) = txtText1(16).Text
      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If rs(0).Value = 0 Then
      Else
        If txtText1(27).Text = "" Then
          MsgBox "El Servicio tiene secciones, es Obligatorio elegir una SECCION.", vbExclamation
          blnCancel = True
        End If
      End If
      rs.Close
      Set rs = Nothing
    End If
  End If
End If

If IsNumeric(txtText1(16).Text) And IsNumeric(txtText1(27).Text) And txtText1(27).Visible = True Then
    stra = "SELECT COUNT(*) FROM AD4100 WHERE AD02CODDPTO=" & txtText1(16).Text
    stra = stra & " AND " & "AD41CODSECCION=" & txtText1(27).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value = 0 Then
      Call MsgBox("La secci�n es incorrecta", vbExclamation, "Aviso")
      blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
End If

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
   tab1.Tab = 0
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)

Dim sqlstr As String
Dim rsta As rdoResultset
Dim strfec As String
Dim rstfec As rdoResultset
Dim hora As Variant
Dim marca As Variant
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim strfri600 As String
Dim rstfri600 As rdoResultset
Dim rstCat As rdoResultset
Dim strCat As String
Dim strfecnac As String
Dim rstfecnac As rdoResultset
Dim edad As Integer
Dim rstservicio As rdoResultset
Dim strservicio As String
Dim auxProceso
Dim auxAsistencia
Dim strProcAsis As String
Dim rstProcAsis As rdoResultset
Dim strpaciente As String
Dim rstpaciente As rdoResultset
Dim mensaje As String
Dim strserviciomed As String
Dim rstserviciomed As rdoResultset
Dim strAD08 As String
Dim qryAD08 As rdoQuery
Dim rdoAD08 As rdoResultset


  If (btnButton.Index = 8 Or btnButton.Index = 2) And objWinInfo.objWinActiveForm.strName = "Detalle PRN" Then
    If txtText1(0).Text <> "" Then
      If txtText1(25).Text <> 1 Then
        If btnButton.Index = 2 Then
          Call MsgBox("No puede a�adir datos a la Petici�n", vbInformation, "Aviso")
        Else
          Call MsgBox("No puede borrar datos de la Petici�n", vbInformation, "Aviso")
        End If
        Exit Sub
      End If
    End If
  End If
  
  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "PRN" Then
    Dim pesta�a As Integer
    pesta�a = tab1.Tab
    tab1.Tab = 2
    tab1.Tab = pesta�a
    
    strfec = "(SELECT TO_CHAR(SYSDATE,'HH24') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = rstfec.rdoColumns(0).Value & ","
    strfec = "(SELECT TO_CHAR(SYSDATE,'mi') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = hora & rstfec.rdoColumns(0).Value
    strfec = "(SELECT SYSDATE FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR66CODPETICION_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    Call objWinInfo.CtrlSet(dtcDateCombo1(2), rstfec(0).Value)
    Call objWinInfo.CtrlSet(txtText1(10), hora)
    Call objWinInfo.CtrlSet(txtText1(25), 1)
    
    If gblnSelPaciente = True Then
      glngselpaciente = frmSelPacienteServEspeciales.IdPersona1.Text
      auxProceso = frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Proceso").Value
      auxAsistencia = frmSelPacienteServEspeciales.grdDBGrid1(1).Columns("Asistencia").Value
    Else
      strProcAsis = "select ad1500.AD07CODPROCESO,ad1500.AD01CODASISTENCI "
      strProcAsis = strProcAsis & " From ad1500, ad0800, ad0100, ad0700 "
      strProcAsis = strProcAsis & " Where ad1500.AD07CODPROCESO = ad0800.AD07CODPROCESO "
      strProcAsis = strProcAsis & " and ad1500.AD07CODPROCESO=ad0700.AD07CODPROCESO"
      strProcAsis = strProcAsis & " and ad1500.AD01CODASISTENCI=ad0800.AD01CODASISTENCI"
      strProcAsis = strProcAsis & " and ad1500.AD01CODASISTENCI=ad0100.AD01CODASISTENCI"
      strProcAsis = strProcAsis & " and ad0700.ci21codpersona=" & glngselpaciente
      strProcAsis = strProcAsis & " and ad0100.ci21codpersona=" & glngselpaciente
      strProcAsis = strProcAsis & " and ad0800.AD34CODESTADO=1"
      'strProcAsis = strProcAsis & " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(ad0100.AD01FECINICIO,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(ad0100.AD01FECFIN,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))"
      'strProcAsis = strProcAsis & " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(ad0700.AD07FECHORAINICI,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(ad0700.AD07FECHORAFIN,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))"
      strProcAsis = strProcAsis & " AND TRUNC(SYSDATE) BETWEEN TRUNC(AD0800.AD08FECINICIO) AND NVL(TRUNC(AD0800.AD08FECFIN),TO_DATE('31/12/9999','DD/MM/YYYY'))"
      strProcAsis = strProcAsis & " ORDER BY AD0800.AD08FECINICIO DESC "
      
      Set rstProcAsis = objApp.rdoConnect.OpenResultset(strProcAsis)
      
      If rstProcAsis.EOF Then
        strpaciente = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = " & glngselpaciente
        Set rstpaciente = objApp.rdoConnect.OpenResultset(strpaciente)
        If rstpaciente.EOF Then
          mensaje = MsgBox("No ha escogido ning�n Paciente ", vbCritical)
        Else
          'El paciente no tiene cama, por lo que se busca el proceso asistencia en la AD0800 sin cama
          strAD08 = "SELECT AD0800.AD01CODASISTENCI,AD0800.AD07CODPROCESO"
          strAD08 = strAD08 & ",AD0800.AD08FECINICIO,AD0800.AD08FECFIN,AD0100.CI21CODPERSONA"
          strAD08 = strAD08 & " FROM AD0800,AD0100 WHERE "
          strAD08 = strAD08 & " AD0800.AD01CODASISTENCI=AD0100.AD01CODASISTENCI"
          strAD08 = strAD08 & "  AND AD0800.AD34CODESTADO=1 "
          strAD08 = strAD08 & " AND  TRUNC(SYSDATE) BETWEEN TRUNC(AD0800.AD08FECINICIO) AND NVL(TRUNC(AD0800.AD08FECFIN),TO_DATE('31/12/9999','DD/MM/YYYY'))"
          strAD08 = strAD08 & " AND CI21CODPERSONA = ? "
          strAD08 = strAD08 & " ORDER BY AD0800.AD08FECINICIO DESC "
          Set qryAD08 = objApp.rdoConnect.CreateQuery("", strAD08)
          qryAD08(0) = glngselpaciente
          Set rdoAD08 = qryAD08.OpenResultset()
          If rdoAD08.EOF Then
            mensaje = MsgBox("El Paciente: " & rstpaciente("CI22NOMBRE").Value & _
                      rstpaciente("CI22PRIAPEL").Value & _
                      rstpaciente("CI22SEGAPEL").Value & _
                      " (Num.Historia: " & rstpaciente("CI22NUMHISTORIA").Value & " )" & _
                      Chr(13) & " No tiene asignado PROCESO-ASISTENCIA." & _
                      Chr(13), vbCritical)
            rstpaciente.Close
            Set rstpaciente = Nothing
            qryAD08.Close
            Set qryAD08 = Nothing
            Set rdoAD08 = Nothing
            Exit Sub
          Else
            If IsNull(rdoAD08("AD07CODPROCESO").Value) Or IsNull(rdoAD08("AD01CODASISTENCI").Value) Then
              strpaciente = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = " & glngselpaciente
              Set rstpaciente = objApp.rdoConnect.OpenResultset(strpaciente)
              If rstpaciente.EOF Then
                mensaje = MsgBox("No ha escogido ning�n Paciente ", vbCritical)
              Else
                mensaje = MsgBox("El Paciente: " & rstpaciente("CI22NOMBRE").Value & _
                        rstpaciente("CI22PRIAPEL").Value & _
                        rstpaciente("CI22SEGAPEL").Value & _
                        " (Num.Historia: " & rstpaciente("CI22NUMHISTORIA").Value & " )" & _
                        Chr(13) & " No tiene asignado PROCESO-ASISTENCIA." & _
                        Chr(13), vbCritical)
              End If
              rstpaciente.Close
              Set rstpaciente = Nothing
              Exit Sub
            Else
              auxProceso = rdoAD08("AD07CODPROCESO").Value
              auxAsistencia = rdoAD08("AD01CODASISTENCI").Value
              qryAD08.Close
              Set qryAD08 = Nothing
              Set rdoAD08 = Nothing
            End If
          End If
        End If
        rstpaciente.Close
        Set rstpaciente = Nothing
        'Exit Sub
      Else
        If IsNull(rstProcAsis("AD07CODPROCESO").Value) Or IsNull(rstProcAsis("AD01CODASISTENCI").Value) Then
          strpaciente = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = " & glngselpaciente
          Set rstpaciente = objApp.rdoConnect.OpenResultset(strpaciente)
          If rstpaciente.EOF Then
            mensaje = MsgBox("No ha escogido ning�n Paciente ", vbCritical)
          Else
            mensaje = MsgBox("El Paciente: " & rstpaciente("CI22NOMBRE").Value & _
                      rstpaciente("CI22PRIAPEL").Value & _
                      rstpaciente("CI22SEGAPEL").Value & _
                      " (Num.Historia: " & rstpaciente("CI22NUMHISTORIA").Value & " )" & _
                      Chr(13) & " No tiene asignado PROCESO-ASISTENCIA." & _
                      Chr(13), vbCritical)
          End If
          rstpaciente.Close
          Set rstpaciente = Nothing
          Exit Sub
        Else
          auxProceso = rstProcAsis("AD07CODPROCESO").Value
          auxAsistencia = rstProcAsis("AD01CODASISTENCI").Value
        End If
      End If
      rstProcAsis.Close
      Set rstProcAsis = Nothing
    End If
    Call objWinInfo.CtrlSet(txtText1(2), glngselpaciente)   'C�d.Paciente
    'si el paciente no tiene cama se mete como servicio el del m�dico que hace Login
    strserviciomed = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD=" & _
                     "'" & objsecurity.strUser & "' AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
    Set rstserviciomed = objApp.rdoConnect.OpenResultset(strserviciomed)
    If Not rstserviciomed.EOF Then
      Call objWinInfo.CtrlSet(txtText1(16), rstserviciomed.rdoColumns(0).Value)    'servicio
      rstserviciomed.Close
      Set rstserviciomed = Nothing
    Else
      rstserviciomed.Close
      Set rstserviciomed = Nothing
      'se calcula el servicio del paciente
      strservicio = "SELECT AD02CODDPTO FROM AD0500 WHERE " & _
                    "AD01CODASISTENCI=" & auxAsistencia & _
                    " AND AD07CODPROCESO=" & auxProceso
      Set rstservicio = objApp.rdoConnect.OpenResultset(strservicio)
      If Not rstservicio.EOF Then
        Call objWinInfo.CtrlSet(txtText1(16), rstservicio.rdoColumns(0).Value)    'servicio
      Else
        Call MsgBox("El m�dico " & objsecurity.strUser & " no est� asignado a ning�n servicio", vbInformation, "Aviso")
      End If
      rstservicio.Close
      Set rstservicio = Nothing
    End If
    Call objWinInfo.CtrlSet(txtText1(14), auxProceso)
    Call objWinInfo.CtrlSet(txtText1(13), auxAsistencia)
    
    'se calcula la edad
    strfecnac = "SELECT CI22FECNACIM FROM CI2200 WHERE CI21CODPERSONA=" & glngselpaciente
    Set rstfecnac = objApp.rdoConnect.OpenResultset(strfecnac)
    If Not rstfecnac.EOF Then
      'edad = DateDiff("d", rstfecnac.rdoColumns(0).Value, Date) \ 365
      Call objWinInfo.CtrlSet(Text1, DateDiff("d", rstfecnac.rdoColumns(0).Value, Date) \ 365)
    Else
      Call objWinInfo.CtrlSet(Text1, "")
    End If
    rstfecnac.Close
    Set rstfecnac = Nothing
    
    'se busca la categor�a
    strCat = "select * from sg0200 where sg02cod='" & objsecurity.strUser & "'"
    Set rstCat = objApp.rdoConnect.OpenResultset(strCat)
    If Not rstCat.EOF = False Then
      If rstCat("AD30CODCATEGORIA").Value = 6 Or rstCat("AD30CODCATEGORIA").Value = 15 Then
        Call objWinInfo.CtrlSet(txtText1(6), objsecurity.strUser)
      Else
        Call objWinInfo.CtrlSet(txtText1(8), objsecurity.strUser)
      End If
    End If
    rstCat.Close
    Set rstCat = Nothing
    
    rsta.Close
    Set rsta = Nothing
    strfri600 = "select count(*) from fri600 where ci21codpersona=" & txtText1(2).Text
    Set rstfri600 = objApp.rdoConnect.OpenResultset(strfri600)
    If rstfri600(0).Value > 0 Then
      Call objWinInfo.CtrlSet(chkCheck1(5), 1)
    Else
      Call objWinInfo.CtrlSet(chkCheck1(5), 0)
    End If
    txtText1(0).SetFocus
    Call objWinInfo.CtrlGotFocus
    Call objWinInfo.CtrlLostFocus
    rstfec.Close
    Set rstfec = Nothing
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Detalle PRN" Then
      If IsNumeric(txtText1(0).Text) Then
        marca = grdDBGrid1(0).Bookmark
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                   txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        'marca = grdDBGrid1(1).Bookmark
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          'linea = 1 + grdDBGrid1(1).Row
          If IsNumeric(grdDBGrid1(0).Columns(4).CellValue(marca)) Then
            linea = 1 + grdDBGrid1(0).Columns(4).CellValue(marca)
          Else
            linea = 1
          End If
        Else
          'linea = rstlinea.rdoColumns(0).Value + 1 + grdDBGrid1(1).Row
          If CInt(rstlinea.rdoColumns(0).Value) > CInt(grdDBGrid1(0).Columns(4).CellValue(marca)) Then
            linea = rstlinea.rdoColumns(0).Value + 1
          Else
            linea = grdDBGrid1(0).Columns(4).CellValue(marca) + 1
          End If
        End If
        grdDBGrid1(0).Columns(4).Value = linea
        grdDBGrid1(0).Col = 13
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna petici�n", vbInformation
        Exit Sub
      End If
    Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
  End If
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 40 'Guardar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Select
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
     Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
     Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
     If intIndex = 0 Then
      If IsNumeric(txtText1(25).Text) Then
        If txtText1(25).Text <> 1 Then
          grdDBGrid1(0).Columns(13).Locked = True
        Else
          grdDBGrid1(0).Columns(13).Locked = False
        End If
      End If
     End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
     Call objWinInfo.CtrlDataChange
     If intIndex = 0 Then
        If grdDBGrid1(0).Col = 13 Then
          grdDBGrid1(0).Columns(15).Value = grdDBGrid1(0).Columns(13).Value
        End If
     End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
If intIndex = 15 Then
 If IsNumeric(txtText1(25).Text) Then
  If txtText1(25).Text = 3 Then
    chkCheck1(15).Enabled = False
    Call MsgBox("El PRN est� enviado. No puede modificarlo", vbInformation, "Aviso")
  End If
 End If
End If
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
If intIndex = 15 Then
  If chkCheck1(15).Value = 1 Then
   txtText1(61).Visible = True
   txtText1(62).Visible = True
   lblLabel1(46).Visible = True
   txtText1(61).BackColor = &HFFFF00
   If IsNumeric(txtText1(16).Text) Then
      txtText1(61).Text = txtText1(16).Text
      txtText1(61).SetFocus
      SendKeys ("{TAB}")
   End If
  Else
   txtText1(61).Visible = False
   txtText1(62).Visible = False
   lblLabel1(46).Visible = False
   txtText1(61).Text = ""
   txtText1(62).Text = ""
   txtText1(61).BackColor = &HFFFFFF
  End If
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
If intIndex = 61 Then
  If txtText1(25).Text = 3 Then
    txtText1(61).Enabled = False
    Call MsgBox("El PRN est� enviado. No puede modificarlo", vbInformation, "Aviso")
  End If
End If
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Dim rstDosis As rdoResultset
  Dim strDosis As String
  Dim strPotPeso As String
  Dim rstPotPeso As rdoResultset
  Dim strPotAlt As String
  Dim rstPotAlt As rdoResultset
  Dim strFactor As String
  Dim rstFactor As rdoResultset
  Dim intPeso As Integer
  Dim intAltura As Integer
  Dim vntMasa As Variant

  Call objWinInfo.CtrlLostFocus
  
  If intIndex = 32 Then
    If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
        intPeso = txtText1(32).Text
        intAltura = txtText1(33).Text
        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
        txtText1(29).Text = vntMasa
    End If
  End If
  If intIndex = 33 Then
    If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
        intPeso = txtText1(32).Text
        intAltura = txtText1(33).Text
        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
        txtText1(29).Text = vntMasa
    End If
  End If
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Dim rstPotPeso As rdoResultset
  Dim rstFactor As rdoResultset
  Dim rstPotAlt As rdoResultset
  Dim strPotPeso As String
  Dim strPotAlt As String
  Dim strFactor As String
  Dim intPeso As Integer
  Dim intAltura As Integer
  Dim vntMasa As Variant
  Dim rsta As rdoResultset
  Dim stra As String
  
  Call objWinInfo.CtrlDataChange
  'PESO
  If intIndex = 32 Then
    If Not IsNumeric(txtText1(32).Text) Then
        txtText1(32).Text = ""
        txtText1(29).Text = ""
    Else
        If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
            strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
            Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
            strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
            Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
            strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
            Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
            intPeso = txtText1(32).Text
            intAltura = txtText1(33).Text
            vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
            vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
            vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
            txtText1(29).Text = vntMasa
            rstPotPeso.Close
            Set rstPotPeso = Nothing
            rstFactor.Close
            Set rstFactor = Nothing
            rstPotAlt.Close
            Set rstPotAlt = Nothing
        Else
            txtText1(29).Text = ""
        End If
    End If
  End If
  'ALTURA
  If intIndex = 33 Then
    If Not IsNumeric(txtText1(33).Text) Then
        txtText1(33).Text = ""
        txtText1(29).Text = ""
    Else
        If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
            strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
            Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
            strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
            Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
            strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
            Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
            intPeso = txtText1(32).Text
            intAltura = txtText1(33).Text
            vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
            vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
            vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
            txtText1(29).Text = vntMasa
            rstPotPeso.Close
            Set rstPotPeso = Nothing
            rstFactor.Close
            Set rstFactor = Nothing
            rstPotAlt.Close
            Set rstPotAlt = Nothing
        Else
            txtText1(29).Text = ""
        End If
    End If
  End If
  If intIndex = 16 Then
    If IsNumeric(txtText1(16).Text) Then
        stra = "SELECT COUNT(*) FROM AD4100 WHERE AD02CODDPTO=" & txtText1(16).Text
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        If rsta.rdoColumns(0).Value = 0 Then
            txtText1(27).Visible = False
            txtText1(15).Visible = False
            lblLabel1(16).Visible = False
            txtText1(27).Text = ""
        Else
            txtText1(27).Visible = True
            txtText1(15).Visible = True
            lblLabel1(16).Visible = True
        End If
    End If
  End If
End Sub






Private Sub Nueva_PRN(auxPaciente, auxProc, auxAsist, auxDpto, auxDoctor)
Dim sqlSeq As String
Dim rstSeq As rdoResultset
Dim gSPcodpeticion
Dim ad02coddpto
Dim sg02cod_med
Dim strfec As String
Dim rstfec As rdoResultset
Dim hora
Dim fr66horaredacci
Dim strpeso As String
Dim rstpeso As rdoResultset
Dim strfri600 As String
Dim rstfri600 As rdoResultset
Dim intAlergico As Integer
Dim strinsert As String
Dim strfecnac As String
Dim rstfecnac As rdoResultset
Dim edad

    sqlSeq = "SELECT FR66CODPETICION_SEQUENCE.nextval FROM dual"
    Set rstSeq = objApp.rdoConnect.OpenResultset(sqlSeq)
    gSPcodpeticion = rstSeq.rdoColumns(0).Value
    rstSeq.Close
    Set rstSeq = Nothing
  
    If auxDpto = "" Then
      'si el paciente no tiene cama se mete como servicio el del m�dico que hace Login
      Dim strserviciomed As String
      Dim rstserviciomed As rdoResultset
      strserviciomed = "SELECT AD02CODDPTO FROM AD0500 WHERE  " & _
                "AD01CODASISTENCI=" & auxAsist & _
                " AND AD07CODPROCESO=" & auxProc
      Set rstserviciomed = objApp.rdoConnect.OpenResultset(strserviciomed)
      If Not rstserviciomed.EOF Then
        ad02coddpto = rstserviciomed.rdoColumns(0).Value
      Else
        strserviciomed = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD=" & _
                         "'" & objsecurity.strUser & "' AND TRUNC(SYSDATE) BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
        Set rstserviciomed = objApp.rdoConnect.OpenResultset(strserviciomed)
        If Not rstserviciomed.EOF Then
          ad02coddpto = rstserviciomed.rdoColumns(0).Value
        Else
          Call MsgBox("El m�dico " & objsecurity.strUser & " no est� asignado a ning�n servicio", vbInformation, "Aviso")
        End If
      End If
      rstserviciomed.Close
      Set rstserviciomed = Nothing
      rstserviciomed.Close
      Set rstserviciomed = Nothing
    Else
      ad02coddpto = auxDpto
    End If
    If auxDoctor = "" Then
      sg02cod_med = objsecurity.strUser
    Else
      sg02cod_med = auxDoctor
    End If
  
    strfec = "(SELECT TO_CHAR(SYSDATE,'HH24') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = rstfec.rdoColumns(0).Value & "."
    strfec = "(SELECT TO_CHAR(SYSDATE,'mi') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = hora & rstfec.rdoColumns(0).Value
    rstfec.Close
    Set rstfec = Nothing
    fr66horaredacci = hora
  
    strpeso = "SELECT AD38PESO,AD38ALTURA FROM AD3800 WHERE " & _
              "AD01CODASISTENCI=" & auxAsist
    Set rstpeso = objApp.rdoConnect.OpenResultset(strpeso)
    
    strfri600 = "select count(*) from fri600 where ci21codpersona=" & auxPaciente
    Set rstfri600 = objApp.rdoConnect.OpenResultset(strfri600)
    If rstfri600(0).Value > 0 Then
      intAlergico = 1
    Else
      intAlergico = 0
    End If
    rstfri600.Close
    Set rstfri600 = Nothing
  
    strinsert = "INSERT INTO FR6600 (FR66CODPETICION,SG02COD_MED,AD02CODDPTO,"
    strinsert = strinsert & "CI21CODPERSONA,FR66FECREDACCION,FR66HORAREDACCI,FR26CODESTPETIC,FR66INDOM,"
    strinsert = strinsert & "FR66PESOPAC,FR66ALTUPAC,FR66INDALERGIA,"
    strinsert = strinsert & "AD07CODPROCESO,AD01CODASISTENCI"
    strinsert = strinsert & ") VALUES "
    strinsert = strinsert & "("
    strinsert = strinsert & gSPcodpeticion & ","
    strinsert = strinsert & "'" & sg02cod_med & "'" & ","
    strinsert = strinsert & ad02coddpto & ","
    strinsert = strinsert & auxPaciente & ","
    strinsert = strinsert & "SYSDATE" & ","
    strinsert = strinsert & fr66horaredacci & ","
    strinsert = strinsert & "1,"
    strinsert = strinsert & "0," 'indicador de OM
    If Not rstpeso.EOF Then
      If IsNull(rstpeso.rdoColumns(0).Value) Then
        strinsert = strinsert & "NULL" & ","
      Else
        strinsert = strinsert & objGen.ReplaceStr(rstpeso.rdoColumns(0).Value, ",", ".", 1) & ","
      End If
      If IsNull(rstpeso.rdoColumns(1).Value) Then
        strinsert = strinsert & "NULL"
      Else
        strinsert = strinsert & objGen.ReplaceStr(rstpeso.rdoColumns(1).Value, ",", ".", 1)
      End If
    Else
      strinsert = strinsert & "NULL,NULL"
    End If
    strinsert = strinsert & "," & intAlergico
    strinsert = strinsert & "," & auxProc
    strinsert = strinsert & "," & auxAsist
    strinsert = strinsert & ")"
    rstpeso.Close
    Set rstpeso = Nothing
  
    objApp.rdoConnect.Execute strinsert, 64
    objApp.rdoConnect.Execute "Commit", 64
  
    'se calcula la edad
    strfecnac = "SELECT CI22FECNACIM FROM CI2200 WHERE CI21CODPERSONA=" & auxPaciente
    Set rstfecnac = objApp.rdoConnect.OpenResultset(strfecnac)
    If Not rstfecnac.EOF Then
      If Not IsNull(rstfecnac.rdoColumns(0).Value) Then
        edad = DateDiff("d", rstfecnac.rdoColumns(0).Value, Date) \ 365
        Text1.Text = edad
      Else
        Text1.Text = ""
      End If
    Else
        Text1.Text = ""
    End If
    rstfecnac.Close
    Set rstfecnac = Nothing
    
    Text1.Locked = True
  
    objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=0 AND CI21CODPERSONA=" & auxPaciente _
             & " AND AD02CODDPTO=" & ad02coddpto & " AND FR66CODPETICION=" & gSPcodpeticion & _
              " AND (FR66INDESTUPEFACIENTE=0 or FR66INDESTUPEFACIENTE IS NULL)" & _
              " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
    'Call objWinInfo.DataRefresh

    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))

End Sub


