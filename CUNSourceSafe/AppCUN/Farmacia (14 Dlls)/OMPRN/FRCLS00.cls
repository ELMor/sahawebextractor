VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************
Const PRWinOM                         As String = "FR0111"
Const PRWinsituacioncarro             As String = "FR0114"
Const PRWinBuscarGruProt              As String = "FR0117"
Const PRWinBuscarProt                 As String = "FR0118"
Const PRWinBuscarGruProdPOM           As String = "FR0167"
Const PRWinBuscarProdPOM              As String = "FR0165"
Const PRWinConsOMAnte                 As String = "FR0115"
Const PRWinVerIntMedica               As String = "FR0116"
Const PRWinFirmarOMPRNPOM             As String = "FR0169"
Const PRWinVerOMSinEnv                As String = "FR0122"
Const PRWinEstOMSinEnv                As String = "FR0123"
Const PRWinBuscarGrpTerap             As String = "FR0153"
Const PRWinAlergiasPaciente           As String = "FR0155"
Const PRWinSelPaciente                As String = "FR0152"
Const PRWinVerPerfilFTPOM             As String = "FR0160"
Const PRWinPedirPRN                   As String = "FR0171"
Const PRWinCesta                      As String = "FR0172"
Const PRWinVerInfMasProd              As String = "FR0200"
Const PRWinConcatenarInsAdmin         As String = "FR0180"
Const PRWinRestricciones              As String = "FR0182"
Const PRWinBusProtocolosQuirofano     As String = "FR0193"
Const PRWinPedirServEspeciales        As String = "FR0192"
Const PRWinSelPacienteServEspeciales  As String = "FR0194"
Const PRWinBusGruposTerapeuticos      As String = "FR0195"
Const PRWinRedactarOMPRNEstupef       As String = "FR0196"
Const PRWinRedOMPRNFM                 As String = "FR0311"


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objCW = mobjCW
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
  
  objCW.SetClock (60 * 4)
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False

End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
  Dim intTipoVar As Integer
  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess

    'REDACTAR ORDEN M�DICA
    'jcr'
    Case PRWinVerPerfilFTPOM
      Load frmVerPerfilFTPOM
      'Call objsecurity.AddHelpContext(528)
      Call frmVerPerfilFTPOM.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerPerfilFTPOM
      Set frmVerPerfilFTPOM = Nothing
      glngPaciente = 0
    Case PRWinOM
      intTipoVar = VarType(vntData)
      If (intTipoVar <> 10) Then
        If (vntData(1) = "frmFirmarOMPRNPLA") Then
          gintfirmarOM = 1
          glngpeticion = vntData(2)
        End If
      End If
      Load frmRedactarOMPRN
      'Call objsecurity.AddHelpContext(528)
      Call frmRedactarOMPRN.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRedactarOMPRN
      Set frmRedactarOMPRN = Nothing
    Case PRWinsituacioncarro
      Load frmVerSitCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmVerSitCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerSitCarro
      Set frmVerSitCarro = Nothing
    Case PRWinBuscarGruProt
      Load frmBusGrpProt
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpProt.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusGrpProt
      Set frmBusGrpProt = Nothing
    Case PRWinBuscarProt
      Load frmBusProtocolos
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProtocolos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusProtocolos
      Set frmBusProtocolos = Nothing
    Case PRWinBuscarGruProdPOM
      Load frmBusGrpProdPOM
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpProdPOM.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusGrpProdPOM
      Set frmBusGrpProdPOM = Nothing
    Case PRWinBuscarProdPOM
      Load frmBusProductosPOM
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProductosPOM.Show(vbModal)
     Call objsecurity.RemoveHelpContext
      Unload frmBusProductosPOM
      Set frmBusProductosPOM = Nothing
    Case PRWinConsOMAnte
      glngselpaciente = vntData(1)
      Load frmConsOMAnte
      'Call objsecurity.AddHelpContext(528)
      Call frmConsOMAnte.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmConsOMAnte
      Set frmConsOMAnte = Nothing
      glngselpaciente = 0
    Case PRWinVerIntMedica
      Load frmVerIntMedica
      'Call objsecurity.AddHelpContext(528)
      Call frmVerIntMedica.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerIntMedica
      Set frmVerIntMedica = Nothing
    Case PRWinFirmarOMPRNPOM
      Load frmFirmarOMPRNPOM
      'Call objsecurity.AddHelpContext(528)
      Call frmFirmarOMPRNPOM.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmFirmarOMPRNPOM
      Set frmFirmarOMPRNPOM = Nothing
    Case PRWinVerOMSinEnv
      Load frmVerOMSinEnv
      'Call objsecurity.AddHelpContext(528)
      Call frmVerOMSinEnv.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerOMSinEnv
      Set frmVerOMSinEnv = Nothing
    Case PRWinEstOMSinEnv
      Load frmEstOMSinEnv
      'Call objsecurity.AddHelpContext(528)
      Call frmEstOMSinEnv.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmEstOMSinEnv
      Set frmEstOMSinEnv = Nothing
    Case PRWinBuscarGrpTerap
      Load frmBusGrpTera
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpTera.Show(vbModal)
      'Call FrmDispEstupef.RemoveHelpContext
      Unload frmBusGrpTera
      Set frmBusGrpTera = Nothing
    Case PRWinAlergiasPaciente
      Load frmAlergiasPaciente
      'Call objsecurity.AddHelpContext(528)
      Call frmAlergiasPaciente.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmAlergiasPaciente
      Set frmAlergiasPaciente = Nothing
    Case PRWinSelPaciente
      Load frmSelPaciente
      'Call objsecurity.AddHelpContext(528)
      Call frmSelPaciente.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmSelPaciente
      Set frmSelPaciente = Nothing
    Case PRWinPedirPRN
      intTipoVar = VarType(vntData)
      If (intTipoVar <> 10) Then
        If (vntData(1) = "frmFirmarOMPRNPLA") Then
          gintfirmarOM = 1
          glngpeticion = vntData(2)
        Else
          glngCodPaciente = vntData(1)
          glngselpaciente = vntData(1)
        End If
      End If
      Load frmPedirPRN
      'Call objsecurity.AddHelpContext(528)
      Call frmPedirPRN.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmPedirPRN
      Set frmPedirPRN = Nothing
      glngCodPaciente = 0
      glngselpaciente = 0
    Case PRWinCesta
      Load frmCesta
      'Call objsecurity.AddHelpContext(528)
      Call frmCesta.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmCesta
      Set frmCesta = Nothing
    Case PRWinVerInfMasProd
      Load frmVerInfMasProd
      'Call objsecurity.AddHelpContext(528)
      Call frmVerInfMasProd.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmVerInfMasProd
      Set frmVerInfMasProd = Nothing
    Case PRWinConcatenarInsAdmin
      Load frmConcatenarInsAdmin
      'Call objsecurity.AddHelpContext(528)
      Call frmConcatenarInsAdmin.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmConcatenarInsAdmin
      Set frmConcatenarInsAdmin = Nothing
    Case PRWinRestricciones
      Load frmRestricciones
      'Call objsecurity.AddHelpContext(528)
      Call frmRestricciones.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmRestricciones
      Set frmRestricciones = Nothing
   Case PRWinBusProtocolosQuirofano
      Load frmBusProtocolosQuirofano
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProtocolosQuirofano.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusProtocolosQuirofano
      Set frmBusProtocolosQuirofano = Nothing
   Case PRWinPedirServEspeciales
      Load frmPedirServEspeciales
      'Call objsecurity.AddHelpContext(528)
      Call frmPedirServEspeciales.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmPedirServEspeciales
      Set frmPedirServEspeciales = Nothing
   Case PRWinSelPacienteServEspeciales
      Load frmSelPacienteServEspeciales
      'Call objsecurity.AddHelpContext(528)
      Call frmSelPacienteServEspeciales.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmSelPacienteServEspeciales
      Set frmSelPacienteServEspeciales = Nothing
   Case PRWinBusGruposTerapeuticos
      Load frmBusGruposTerapeuticos
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGruposTerapeuticos.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusGruposTerapeuticos
      Set frmBusGruposTerapeuticos = Nothing
   Case PRWinRedactarOMPRNEstupef
      Load frmRedactarOMPRNEstupef
      'Call objsecurity.AddHelpContext(528)
      Call frmRedactarOMPRNEstupef.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmRedactarOMPRNEstupef
      Set frmRedactarOMPRNEstupef = Nothing
   Case PRWinRedOMPRNFM
      Load frmRedOMPRNFM
      'Call objsecurity.AddHelpContext(528)
      Call frmRedOMPRNFM.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmRedOMPRNFM
      Set frmRedOMPRNFM = Nothing
  End Select
  Call ERR.Clear

End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 27, 1 To 4) As Variant
       
  
  'REDACTAR ORDEN M�DICA
  
  aProcess(1, 1) = PRWinOM
  aProcess(1, 2) = "Redactar Orden M�dica"
  aProcess(1, 3) = False
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinsituacioncarro
  aProcess(2, 2) = "Situaci�n de los Carros"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PRWinBuscarGruProt
  aProcess(3, 2) = "Buscar Grupo de Protocolos"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = PRWinBuscarProt
  aProcess(4, 2) = "Buscar Protocolos"
  aProcess(4, 3) = False
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = PRWinBuscarGruProdPOM
  aProcess(5, 2) = "Buscar Grupo de Productos"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow
  
  aProcess(6, 1) = PRWinBuscarProdPOM
  aProcess(6, 2) = "Buscar Productos"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = PRWinConsOMAnte
  aProcess(7, 2) = "OM anteriores"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
  
  aProcess(8, 1) = PRWinVerIntMedica
  aProcess(8, 2) = "Ver Interacciones"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = PRWinFirmarOMPRNPOM
  aProcess(9, 2) = "Firmar OM/PRN"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
  
  aProcess(10, 1) = PRWinVerOMSinEnv
  aProcess(10, 2) = "Enviar OM/PRN"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow
  
  aProcess(11, 1) = PRWinEstOMSinEnv
  aProcess(11, 2) = "Estudiar OM/PRN sin enviar"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
  
  aProcess(12, 1) = PRWinBuscarGrpTerap
  aProcess(12, 2) = "Buscar Grupos Terap�uticos"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow
  
  aProcess(13, 1) = PRWinAlergiasPaciente
  aProcess(13, 2) = "Alergias del Paciente"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeWindow
  
  aProcess(14, 1) = PRWinSelPaciente
  aProcess(14, 2) = "Seleccionar Paciente"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow
  
  aProcess(15, 1) = PRWinVerPerfilFTPOM
  aProcess(15, 2) = "Consultar Perfil FTP"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeWindow
  
  aProcess(16, 1) = PRWinPedirPRN
  aProcess(16, 2) = "PedirPRN"
  aProcess(16, 3) = False
  aProcess(16, 4) = cwTypeWindow
  
  aProcess(17, 1) = PRWinVerInfMasProd
  aProcess(17, 2) = "Mas Informacion Producto"
  aProcess(17, 3) = False
  aProcess(17, 4) = cwTypeWindow
  
  aProcess(18, 1) = PRWinCesta
  aProcess(18, 2) = "Ver Cesta/PRN"
  aProcess(18, 3) = False
  aProcess(18, 4) = cwTypeWindow
  
  aProcess(19, 1) = PRWinRestricciones
  aProcess(19, 2) = "Restricciones al uso de un medicamento"
  aProcess(19, 3) = False
  aProcess(19, 4) = cwTypeWindow
  
  aProcess(20, 1) = PRWinConcatenarInsAdmin
  aProcess(20, 2) = "Buscar Instrucciones de Administraci�n"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeWindow
  
  aProcess(21, 1) = PRWinBusProtocolosQuirofano
  aProcess(21, 2) = "Buscar Protocolos de Quir�fano"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeWindow
  
  aProcess(22, 1) = PRWinPedirServEspeciales
  aProcess(22, 2) = "Pedir Servicios Especiales"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeWindow
  
  aProcess(23, 1) = PRWinSelPacienteServEspeciales
  aProcess(23, 2) = "Seleccionar Paciente Serv.Especial"
  aProcess(23, 3) = True
  aProcess(23, 4) = cwTypeWindow
  
  aProcess(24, 1) = PRWinBusGruposTerapeuticos
  aProcess(24, 2) = "Buscar Grupos Terap�uticos"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeWindow
  
  aProcess(25, 1) = PRWinRedactarOMPRNEstupef
  aProcess(25, 2) = "Redactar Orden M�dica Estupefacientes"
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeWindow
  
  aProcess(26, 1) = PRWinRedOMPRNFM
  aProcess(26, 2) = "Redactar Orden M�dica F�rmula Magistral"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeWindow
  
End Sub
