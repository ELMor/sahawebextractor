VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmBusProtocolos2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Protocolos"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdtraerprot 
      Caption         =   "Traer Protocolo"
      Height          =   375
      Left            =   10440
      TabIndex        =   20
      Top             =   3600
      Width           =   1335
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Protocolo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Index           =   1
      Left            =   120
      TabIndex        =   11
      Top             =   480
      Width           =   11700
      Begin TabDlg.SSTab tabTab1 
         Height          =   2175
         Index           =   0
         Left            =   120
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   360
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   3836
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0118_22.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblLabel1(14)"
         Tab(0).Control(1)=   "lblLabel1(28)"
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(3)=   "lblLabel1(2)"
         Tab(0).Control(4)=   "lblLabel1(0)"
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(6)=   "lblLabel1(3)"
         Tab(0).Control(7)=   "dtcDateCombo1(1)"
         Tab(0).Control(8)=   "dtcDateCombo1(0)"
         Tab(0).Control(9)=   "txtText1(0)"
         Tab(0).Control(10)=   "txtText1(1)"
         Tab(0).Control(11)=   "txtText1(2)"
         Tab(0).Control(12)=   "txtText1(3)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "chkCheck1(1)"
         Tab(0).Control(14)=   "txtText1(4)"
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0118_22.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            DataField       =   "FR75INDICACIONES"
            Height          =   330
            Index           =   4
            Left            =   -74760
            TabIndex        =   7
            Tag             =   "Indicaciones"
            Top             =   1680
            Width           =   8340
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Privado?"
            DataField       =   "FR75INDPRIVADO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -66240
            TabIndex        =   6
            Tag             =   "Privado?"
            Top             =   1680
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   3
            Left            =   -69120
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Dpto"
            Top             =   1080
            Width           =   3900
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   2
            Left            =   -70320
            TabIndex        =   4
            Tag             =   "C�d.Dpto"
            Top             =   1080
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR75DESPROTOCOLO"
            Height          =   450
            Index           =   1
            Left            =   -73080
            MultiLine       =   -1  'True
            TabIndex        =   1
            Tag             =   "Desc. Prot"
            Top             =   360
            Width           =   7860
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR75CODPROTOCOLO"
            Height          =   330
            Index           =   0
            Left            =   -74760
            TabIndex        =   0
            Tag             =   "C�d.Prot"
            Top             =   360
            Width           =   1020
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2145
            Index           =   2
            Left            =   0
            TabIndex        =   14
            Top             =   0
            Width           =   11415
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   20135
            _ExtentY        =   3784
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR75FECINIVIG"
            Height          =   330
            Index           =   0
            Left            =   -74760
            TabIndex        =   2
            Tag             =   "Inicio Vig"
            Top             =   1080
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR75FECFINVIG"
            Height          =   330
            Index           =   1
            Left            =   -72600
            TabIndex        =   3
            Tag             =   " Fin Vig"
            Top             =   1080
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Indicaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   -74760
            TabIndex        =   23
            Top             =   1440
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Desc.Dpto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -69120
            TabIndex        =   22
            Top             =   840
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Dpto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -70320
            TabIndex        =   21
            Top             =   840
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   -74760
            TabIndex        =   19
            Top             =   840
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   -72600
            TabIndex        =   18
            Top             =   840
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Protocolo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   -74760
            TabIndex        =   17
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Protocolo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   -73080
            TabIndex        =   15
            Top             =   120
            Width           =   2175
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Medicamentos del Protocolo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4680
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   3240
      Width           =   10215
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4185
         Index           =   0
         Left            =   135
         TabIndex        =   16
         Top             =   360
         Width           =   9960
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   17568
         _ExtentY        =   7382
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmBusProtocolos2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmBusProtocolos (FR0118.FRM)                                *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: buscar Protocolos en la OM                              *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim blnnoactivate As Boolean


Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


Private Sub cmdtraerprot_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mensaje As String
Dim rsta As rdoResultset
Dim stra As String
Dim rstrest As rdoResultset
Dim strrest As String
Dim strinsert As String

cmdtraerprot.Enabled = False
'If objWinInfo.objWinActiveForm.strName = "Protocolos" Then
'Guardamos el n�mero de filas seleccionadas
'mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
mintNTotalSelRows = grdDBGrid1(0).Rows
ReDim gintprodbuscado(mintNTotalSelRows, 10)
gintprodtotal = mintNTotalSelRows
grdDBGrid1(0).MoveFirst
For mintisel = 0 To mintNTotalSelRows - 1
     mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
     gintprodbuscado(mintisel, 0) = grdDBGrid1(0).Columns(5).CellValue(mvarBkmrk) 'COD PROD
     gintprodbuscado(mintisel, 1) = grdDBGrid1(0).Columns(6).CellValue(mvarBkmrk) 'COD INT
     gintprodbuscado(mintisel, 2) = grdDBGrid1(0).Columns(8).CellValue(mvarBkmrk) 'DESC PROD
     gintprodbuscado(mintisel, 3) = grdDBGrid1(0).Columns(12).CellValue(mvarBkmrk) 'DOSIS
     gintprodbuscado(mintisel, 4) = grdDBGrid1(0).Columns(13).CellValue(mvarBkmrk) 'UM
     gintprodbuscado(mintisel, 7) = grdDBGrid1(0).Columns(7).CellValue(mvarBkmrk) 'REF
     gintprodbuscado(mintisel, 8) = grdDBGrid1(0).Columns(10).CellValue(mvarBkmrk) 'FF
     gintprodbuscado(mintisel, 9) = grdDBGrid1(0).Columns(9).CellValue(mvarBkmrk) 'TAM

     gintprodbuscado(mintisel, 5) = grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk) 'linea
     gintprodbuscado(mintisel, 6) = grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk) 'protocolo
     
     gintprodbuscado(mintisel, 10) = grdDBGrid1(0).Columns(11).CellValue(mvarBkmrk) 'CANTIDAD
     '----------------------------------------------------------------------------------
       'para cada producto hay que mirar si es de uso restringido
       stra = "SELECT FR73INDUSOREST FROM FR7300 WHERE FR73CODPRODUCTO=" & _
               grdDBGrid1(0).Columns(5).CellValue(mvarBkmrk)
       Set rsta = objApp.rdoConnect.OpenResultset(stra)
       If rsta.rdoColumns(0).Value = -1 Then
          'si es de uso restringido se llama a la pantalla de restricciones,pero antes
          'se vuelca el contenido de FRA900 en FRA400
              stra = "SELECT * FROM FRA900 WHERE FR73CODPRODUCTO=" & _
                     grdDBGrid1(0).Columns(5).CellValue(mvarBkmrk)
              Set rsta = objApp.rdoConnect.OpenResultset(stra)
              While Not rsta.EOF
                  strrest = "SELECT FRA9CODREST FROM FRA400 WHERE FR73CODPRODUCTO=" & _
                            grdDBGrid1(0).Columns(5).CellValue(mvarBkmrk) & " AND " & _
                            "FR66CODPETICION=" & glngcodpeticion & " AND " & _
                            "FRA9CODREST=" & rsta.rdoColumns("FRA9CODREST").Value
                  Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
                  If rstrest.EOF Then
                    strinsert = "INSERT INTO FRA400(FR73CODPRODUCTO,FR66CODPETICION," & _
                              "FRA9CODREST,FRA4DESCREST,FRA4INDSELECCIONADO) VALUES(" & _
                              grdDBGrid1(0).Columns(5).CellValue(mvarBkmrk) & "," & _
                              glngcodpeticion & "," & _
                              rsta.rdoColumns("FRA9CODREST").Value & "," & _
                              "'" & rsta.rdoColumns("FRA9DESREST").Value & "'" & "," & _
                              "0" & ")"
                    objApp.rdoConnect.Execute strinsert, 64
                    objApp.rdoConnect.Execute "Commit", 64
                  End If
                  rstrest.Close
                  Set rstrest = Nothing
              rsta.MoveNext
              Wend
              rsta.Close
              Set rsta = Nothing
              glngcodprod = grdDBGrid1(0).Columns(5).CellValue(mvarBkmrk)
              Call objsecurity.LaunchProcess("FR0182")
       End If
       '---------------------------------------------------------------------------------
grdDBGrid1(0).MoveNext
Next mintisel
cmdtraerprot.Enabled = True
Unload Me
'Else
'mensaje = MsgBox("No ha seleccionado ning�n protocolo", vbInformation, "Aviso")
'cmdtraerprot.Enabled = True
'End If
End Sub

Private Sub Form_Activate()
If blnnoactivate = False Then
    blnnoactivate = True
    'se va al primer registro para refrescar el grid
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Protocolos"
      
    .strTable = "FR7500"
    .intAllowance = cwAllowReadOnly
    
    If gintbuscargruprot = 1 Then
        .strWhere = "FR75CODPROTOCOLO=" & frmBusGrpProt.grdDBGrid1(0).Columns(3).Value
    Else
      If gstrLlamadorProd = "frmPedirPrn" Then
        .strWhere = "((FR75INDPRIVADO = 0) Or " & _
                    "(FR75INDPRIVADO = -1 And " & _
                       "AD02CODDPTO =" & frmPedirPRN.txtText1(16).Text & _
                    " AND FR75FECINIVIG < (SELECT SYSDATE FROM DUAL)" & _
                    " AND ((FR75FECFINVIG IS NULL) OR (FR75FECFINVIG > (SELECT SYSDATE FROM DUAL)))))"
      Else
        .strWhere = "((FR75INDPRIVADO = 0) Or " & _
                    "(FR75INDPRIVADO = -1 And " & _
                       "AD02CODDPTO  IN (SELECT AD02CODDPTO FROM AD0300 " & _
                                         "WHERE SG02COD ='" & objsecurity.strUser & "')))" & _
                    " AND FR75FECINIVIG < (SELECT SYSDATE FROM DUAL)" & _
                    " AND ((FR75FECFINVIG IS NULL) OR (FR75FECFINVIG > (SELECT SYSDATE FROM DUAL)))"
      End If
    End If
    
    Call .FormAddOrderField("FR75CODPROTOCOLO", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Protocolo")
    Call .FormAddFilterWhere(strKey, "FR75CODPROTOCOLO", "C�digo Protocolo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR75DESPROTOCOLO", "Descripci�n Protocolo", cwString)
    Call .FormAddFilterWhere(strKey, "FR75FECINIVIG", "Fecha Inicio Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR75FECFINVIG", "Fecha Fin Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�d.Dpto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR75INDPRIVADO", "Privado?", cwBoolean)
    

    Call .FormAddFilterOrder(strKey, "FR75CODPROTOCOLO", "C�digo Protocolo")
    Call .FormAddFilterOrder(strKey, "FR75DESPROTOCOLO", "Descripci�n Protocolo")
    Call .FormAddFilterOrder(strKey, "FR75FECINIVIG", "Fecha Inicio Vigencia")
    Call .FormAddFilterOrder(strKey, "FR75FECFINVIG", "Fecha Fin Vigencia")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�d.Dpto")

  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Productos"
    
    .strTable = "FR1100"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR11NUMLINEA", cwAscending)
    Call .FormAddRelation("FR75CODPROTOCOLO", txtText1(0))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Medicamentos del Protocolo")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d.Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "Forma Farmace�tica", cwString)
    Call .FormAddFilterWhere(strKey, "FR11CANTIDAD", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR11DOSIS", "Dosis", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "Unidad de Medida", cwString)
    Call .FormAddFilterWhere(strKey, "FRG4CODFRECUENCIA", "C�d.Frecuencia", cwString)
    Call .FormAddFilterWhere(strKey, "FR34CODVIA", "C�d.V�a", cwString)
    Call .FormAddFilterWhere(strKey, "FRH5CODPERIODICIDAD", "C�d.Periodicidad", cwString)
    Call .FormAddFilterWhere(strKey, "FR11FECINICIO", "Fecha Inicio", cwDate)
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfo, "C�d. Protocolo", "FR75CODPROTOCOLO", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Linea", "FR11NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "C�d.Medicamento", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Prod", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Descripci�n Medicamento", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Tam Env", "", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "Forma", "FRH7CODFORMFAR", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "FR11CANTIDAD", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR11DOSIS", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "UM", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Frecuencia", "FRG4CODFRECUENCIA", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Periodicidad", "FRH5CODPERIODICIDAD", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Inicio", "FR11FECINICIO", cwDate)
    
    
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    .CtrlGetInfo(txtText1(1)).blnMandatory = True

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(10)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(13)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(14)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(15)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(16)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(17)).blnInFind = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(7), "FR73REFERENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(8), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(9), "FR73TAMENVASE")

    Call .WinRegister
    Call .WinStabilize
    
  End With
  
  grdDBGrid1(2).Columns(1).Width = 800
  grdDBGrid1(2).Columns(2).Width = 3000
  grdDBGrid1(2).Columns(3).Width = 1000
  grdDBGrid1(2).Columns(4).Width = 1000
  grdDBGrid1(2).Columns(5).Width = 600
  grdDBGrid1(2).Columns(6).Width = 2000
  grdDBGrid1(2).Columns(7).Width = 780
  
  grdDBGrid1(0).Columns(8).Width = 3000
  grdDBGrid1(0).Columns(14).Width = 1500
  grdDBGrid1(0).Columns(16).Width = 1500
  grdDBGrid1(0).Columns(17).Width = 1200
  
    grdDBGrid1(0).Columns(3).Visible = False
    grdDBGrid1(0).Columns(4).Visible = False
    grdDBGrid1(0).Columns(5).Visible = False
    grdDBGrid1(0).Columns(9).Visible = False

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


