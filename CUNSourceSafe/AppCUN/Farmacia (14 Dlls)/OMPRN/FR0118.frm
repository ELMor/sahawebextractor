VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Begin VB.Form frmBusProtocolos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Protocolos"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   46
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdtraerprot 
      Caption         =   "&Traer Protocolo"
      Height          =   375
      Left            =   10440
      TabIndex        =   208
      Top             =   1680
      Width           =   1335
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Protocolo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Index           =   1
      Left            =   0
      TabIndex        =   57
      Top             =   480
      Width           =   10185
      Begin TabDlg.SSTab tabTab1 
         Height          =   2535
         Index           =   0
         Left            =   120
         TabIndex        =   58
         TabStop         =   0   'False
         Top             =   360
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   4471
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0118.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "SSTab1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0118.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   59
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2265
            Index           =   2
            Left            =   -74880
            TabIndex        =   60
            TabStop         =   0   'False
            Top             =   120
            Width           =   9495
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16748
            _ExtentY        =   3995
            _StockProps     =   79
         End
         Begin TabDlg.SSTab SSTab1 
            Height          =   2295
            Index           =   0
            Left            =   120
            TabIndex        =   126
            Top             =   120
            Width           =   9495
            _ExtentX        =   16748
            _ExtentY        =   4048
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Datos Generales"
            TabPicture(0)   =   "FR0118.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(24)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(0)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(1)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(2)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(3)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "dtcDateCombo1(0)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "dtcDateCombo1(1)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(0)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(1)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(3)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(2)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "chkCheck1(0)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).ControlCount=   12
            TabCaption(1)   =   "Indicaciones"
            TabPicture(1)   =   "FR0118.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(11)"
            Tab(1).ControlCount=   1
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Indicador Privado"
               DataField       =   "FR75INDPRIVADO"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   132
               Tag             =   "Priv.|Indicador Privado"
               Top             =   1200
               Width           =   1935
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   2
               Left            =   2280
               TabIndex        =   131
               Tag             =   "C�d. Dpto. Propietario"
               Top             =   1200
               Width           =   372
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   3
               Left            =   2760
               TabIndex        =   130
               TabStop         =   0   'False
               Tag             =   "Dpto. Propietario"
               Top             =   1200
               Width           =   4800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR75DESPROTOCOLO"
               Height          =   330
               Index           =   1
               Left            =   1560
               TabIndex        =   129
               Tag             =   "Protocolo|Descripci�n Protocolo"
               Top             =   600
               Width           =   7560
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR75CODPROTOCOLO"
               Height          =   330
               Index           =   0
               Left            =   120
               Locked          =   -1  'True
               TabIndex        =   128
               Tag             =   "C�digo Protocolo"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR75INDICACIONES"
               Height          =   1650
               Index           =   11
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   127
               Tag             =   "Indicaciones del Protocolo"
               Top             =   480
               Width           =   9240
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR75FECFINVIG"
               Height          =   330
               Index           =   1
               Left            =   2280
               TabIndex        =   133
               Tag             =   "Fecha Fin Vigencia"
               Top             =   1800
               Width           =   1620
               _Version        =   65537
               _ExtentX        =   2857
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR75FECINIVIG"
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   134
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   1800
               Width           =   1620
               _Version        =   65537
               _ExtentX        =   2857
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dpto. Propietario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   2280
               TabIndex        =   139
               Top             =   960
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Protocolo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   1560
               TabIndex        =   138
               Top             =   360
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cod. Protocolo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   137
               Top             =   360
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fin Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   2280
               TabIndex        =   136
               Top             =   1560
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Inicio Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Index           =   24
               Left            =   120
               TabIndex        =   135
               Top             =   1560
               Width           =   1455
            End
         End
      End
   End
   Begin VB.Frame fraFrame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4560
      Index           =   0
      Left            =   0
      TabIndex        =   45
      Top             =   3360
      Width           =   11655
      Begin TabDlg.SSTab tabTab1 
         Height          =   4215
         Index           =   1
         Left            =   120
         TabIndex        =   44
         TabStop         =   0   'False
         Top             =   240
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   7435
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0118.frx":0070
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "Frame1"
         Tab(0).Control(1)=   "tab2"
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0118.frx":008C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "lblLabel1(57)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "lblLabel1(56)"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "lblLabel1(55)"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "lblLabel1(54)"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).Control(4)=   "lblLabel1(53)"
         Tab(1).Control(4).Enabled=   0   'False
         Tab(1).Control(5)=   "lblLabel1(44)"
         Tab(1).Control(5).Enabled=   0   'False
         Tab(1).Control(6)=   "grdDBGrid1(6)"
         Tab(1).Control(6).Enabled=   0   'False
         Tab(1).ControlCount=   7
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'None
            Caption         =   "Medicamentos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3600
            Left            =   -74760
            TabIndex        =   87
            Top             =   480
            Width           =   9135
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR11DIASADM"
               Height          =   330
               Index           =   10
               Left            =   7920
               Locked          =   -1  'True
               TabIndex        =   144
               Tag             =   "D�as de administraci�n, separados por comas"
               Top             =   1920
               Visible         =   0   'False
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11NUMDIAS"
               Height          =   330
               Index           =   9
               Left            =   5520
               TabIndex        =   142
               Tag             =   "D�as"
               Top             =   840
               Width           =   855
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Dosis Peso?"
               DataField       =   "FR11DOSISPESO"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   6600
               TabIndex        =   141
               Tag             =   "Dosis por Peso?"
               Top             =   600
               Width           =   1455
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Dosis Superf.?"
               DataField       =   "FR11DOSISSUP"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   6600
               TabIndex        =   140
               Tag             =   "Dosis por Superficie Corporal?"
               Top             =   840
               Width           =   1575
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR11INSTRADMIN"
               Height          =   930
               Index           =   39
               Left            =   0
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   24
               Tag             =   "Inst.Admin.|Instrucciones para administraci�n"
               Top             =   2040
               Width           =   5205
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11CANTDISP"
               Height          =   330
               Index           =   58
               Left            =   3240
               TabIndex        =   125
               Tag             =   "Cant.Dispens."
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.CommandButton cmdExplorador 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   525
               Index           =   1
               Left            =   4080
               TabIndex        =   32
               Top             =   2400
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.CommandButton cmdWord 
               Caption         =   "W"
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   525
               Left            =   4680
               TabIndex        =   33
               Top             =   2400
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR11PATHFORMAG"
               Height          =   330
               Index           =   57
               Left            =   360
               TabIndex        =   31
               Tag             =   "Arch.Inf.F.M.|Archivo Inf. F�rmula Magistral"
               Top             =   2520
               Visible         =   0   'False
               Width           =   4725
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11REFERENCIA"
               Height          =   330
               Index           =   56
               Left            =   2760
               TabIndex        =   123
               Tag             =   "Referencia"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR11DESPRODUCTO"
               Height          =   330
               Index           =   55
               Left            =   120
               TabIndex        =   3
               Tag             =   "Medic.No Form.|""Medic. No Formulario"""
               Top             =   120
               Visible         =   0   'False
               Width           =   5295
            End
            Begin VB.TextBox txtText1 
               Height          =   330
               Index           =   13
               Left            =   0
               TabIndex        =   2
               Tag             =   "Medicamento"
               Top             =   240
               Width           =   5295
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11UBICACION"
               Height          =   330
               Index           =   54
               Left            =   2520
               TabIndex        =   122
               Tag             =   "Ubicaci�n"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11OPERACION"
               Height          =   330
               Index           =   53
               Left            =   3000
               TabIndex        =   121
               Tag             =   "Operaci�n"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODPRODUCTO_2"
               Height          =   330
               Index           =   52
               Left            =   0
               TabIndex        =   43
               Tag             =   "C�digo Medicamento 2"
               Top             =   2640
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   51
               Left            =   0
               TabIndex        =   19
               Tag             =   "Medicamento 2"
               Top             =   3240
               Width           =   5340
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11DOSIS_2"
               Height          =   330
               Index           =   50
               Left            =   6120
               TabIndex        =   21
               Tag             =   "Dosis2"
               Top             =   3240
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR93CODUNIMEDIDA_2"
               Height          =   330
               Index           =   49
               Left            =   7080
               TabIndex        =   22
               Tag             =   "UM2"
               Top             =   3240
               Width           =   660
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FRH7CODFORMFAR_2"
               Height          =   330
               Index           =   48
               Left            =   5400
               TabIndex        =   20
               Tag             =   "FF2"
               Top             =   3240
               Width           =   615
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11VOLUMEN_2"
               Height          =   330
               Index           =   47
               Left            =   7800
               TabIndex        =   23
               Tag             =   "vol.Prod.2|Volumen(ml) Producto 2"
               Top             =   3240
               Width           =   732
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11VELPERFUSION"
               Height          =   330
               Index           =   82
               Left            =   5400
               TabIndex        =   25
               Tag             =   "Perfusi�n|Velocidad de Perfusi�n"
               Top             =   2040
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11VOLTOTAL"
               Height          =   330
               Index           =   44
               Left            =   8040
               TabIndex        =   28
               Tag             =   "Vol.Total|Volumen Total (ml)"
               Top             =   2640
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11VOLUMEN"
               Height          =   330
               Index           =   38
               Left            =   7800
               TabIndex        =   7
               Tag             =   "Volum.|Volumen(ml)"
               Top             =   240
               Width           =   732
            End
            Begin VB.TextBox txtMinutos 
               Alignment       =   1  'Right Justify
               Height          =   330
               Left            =   7080
               TabIndex        =   18
               Tag             =   "Tiempo Infusi�n(min)"
               Top             =   1440
               Width           =   375
            End
            Begin VB.TextBox txtHoras 
               Alignment       =   1  'Right Justify
               Height          =   330
               Left            =   6600
               TabIndex        =   17
               Tag             =   "Tiempo Infusi�n(hor)"
               Top             =   1440
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FRH7CODFORMFAR"
               Height          =   330
               Index           =   35
               Left            =   5400
               TabIndex        =   4
               Tag             =   "FF"
               Top             =   240
               Width           =   615
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11tiemmininf"
               Height          =   330
               Index           =   43
               Left            =   3720
               TabIndex        =   92
               Tag             =   "T.Inf.(min)|Tiempo Infusi�n(min)"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11CANTIDADDIL"
               Height          =   330
               Index           =   42
               Left            =   5520
               TabIndex        =   16
               Tag             =   "Vol.Dil.|Volumen(ml)"
               Top             =   1440
               Width           =   732
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   41
               Left            =   0
               TabIndex        =   15
               Tag             =   "Descripci�n Producto Diluir"
               Top             =   1440
               Width           =   5385
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODPRODUCTO_DIL"
               Height          =   330
               Index           =   40
               Left            =   1200
               TabIndex        =   14
               Tag             =   "C�digo Producto diluir"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "PERF"
               DataField       =   "FR11INDPERF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   8280
               TabIndex        =   30
               Tag             =   "PERF?|Mezcla IV en Farmacia"
               Top             =   1280
               Width           =   855
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "STAT"
               DataField       =   "FR11indcomieninmed"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   8280
               TabIndex        =   29
               Tag             =   "STAT?|Pedir primera dosis"
               Top             =   800
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11horafin"
               Height          =   330
               Index           =   36
               Left            =   7320
               TabIndex        =   27
               Tag             =   "H.F.|Hora Fin"
               Top             =   2640
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11horainicio"
               Height          =   330
               Index           =   37
               Left            =   6120
               TabIndex        =   13
               Tag             =   "H.I.|Hora Inicio"
               Top             =   2640
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR93CODUNIMEDIDA"
               Height          =   330
               Index           =   31
               Left            =   7080
               TabIndex        =   6
               Tag             =   "UM"
               Top             =   240
               Width           =   660
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11DOSIS"
               Height          =   330
               Index           =   30
               Left            =   6120
               TabIndex        =   5
               Tag             =   "Dosis"
               Top             =   240
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11CANTIDAD"
               Height          =   330
               Index           =   28
               Left            =   0
               TabIndex        =   8
               Tag             =   "Cantidad"
               Top             =   840
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   27
               Left            =   0
               TabIndex        =   1
               Tag             =   "C�digo Medicamento"
               Top             =   2280
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "PRN"
               DataField       =   "FR11INDDISPPRN"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   1440
               TabIndex        =   91
               Tag             =   "Prn?"
               Top             =   2760
               Visible         =   0   'False
               Width           =   735
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Cambiar V�a"
               DataField       =   "FR11INDVIAOPC"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   3000
               TabIndex        =   90
               Tag             =   "Cambiar V�a?"
               Top             =   2760
               Visible         =   0   'False
               Width           =   1455
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR11numlinea"
               Height          =   405
               Index           =   45
               Left            =   2040
               TabIndex        =   89
               Tag             =   "Num.Linea"
               Top             =   2280
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR75CODPROTOCOLO"
               Height          =   405
               Index           =   34
               Left            =   1800
               TabIndex        =   88
               Tag             =   "C�digo Petici�n"
               Top             =   2280
               Visible         =   0   'False
               Width           =   375
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR11fecinicio"
               Height          =   330
               Index           =   3
               Left            =   5400
               TabIndex        =   12
               Tag             =   "Inicio|Fecha Inicio Vigencia"
               Top             =   2640
               Visible         =   0   'False
               Width           =   660
               _Version        =   65537
               _ExtentX        =   1164
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR11fecfin"
               Height          =   330
               Index           =   4
               Left            =   6600
               TabIndex        =   26
               Tag             =   "Fin|Fecha Fin"
               Top             =   2640
               Visible         =   0   'False
               Width           =   660
               _Version        =   65537
               _ExtentX        =   1164
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
               DataField       =   "fr34codvia"
               Height          =   330
               Index           =   1
               Left            =   2760
               TabIndex        =   10
               Tag             =   "V�a|C�digo V�a"
               Top             =   840
               Width           =   765
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets.count =   2
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777215
               stylesets(0).Picture=   "FR0118.frx":00A8
               stylesets(1).Name=   "Inactivo"
               stylesets(1).BackColor=   255
               stylesets(1).Picture=   "FR0118.frx":00C4
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Nombre"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1349
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 0"
            End
            Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
               DataField       =   "frg4codfrecuencia"
               Height          =   330
               Index           =   0
               Left            =   960
               TabIndex        =   9
               Tag             =   "Frec.|Cod.Frecuencia"
               Top             =   840
               Width           =   1725
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets.count =   2
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777215
               stylesets(0).Picture=   "FR0118.frx":00E0
               stylesets(1).Name=   "Inactivo"
               stylesets(1).BackColor=   255
               stylesets(1).Picture=   "FR0118.frx":00FC
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Nombre"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3043
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 0"
            End
            Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
               DataField       =   "frh5codperiodicidad"
               Height          =   330
               Index           =   3
               Left            =   3600
               TabIndex        =   11
               Tag             =   "Period.|Cod. Periodicidad"
               Top             =   840
               Width           =   1845
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets.count =   2
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777215
               stylesets(0).Picture=   "FR0118.frx":0118
               stylesets(1).Name=   "Inactivo"
               stylesets(1).BackColor=   255
               stylesets(1).Picture=   "FR0118.frx":0134
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Nombre"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3254
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 0"
            End
            Begin MSComDlg.CommonDialog CommonDialog1 
               Left            =   3480
               Top             =   2520
               _ExtentX        =   847
               _ExtentY        =   847
               _Version        =   327681
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dias de Agenda"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   7
               Left            =   7680
               TabIndex        =   145
               Top             =   1680
               Visible         =   0   'False
               Width           =   1365
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "D�as"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   5520
               TabIndex        =   143
               Top             =   600
               Width           =   420
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Archivo Inf. F�rmula Magistral"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   84
               Left            =   360
               TabIndex        =   124
               Top             =   2280
               Visible         =   0   'False
               Width           =   2550
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Medicamento 2"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   83
               Left            =   0
               TabIndex        =   120
               Top             =   3000
               Width           =   1305
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   82
               Left            =   6120
               TabIndex        =   119
               Top             =   3000
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "UM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   81
               Left            =   7080
               TabIndex        =   118
               Top             =   3000
               Width           =   300
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "FF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   80
               Left            =   5400
               TabIndex        =   117
               Top             =   3000
               Width           =   225
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Med2"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   79
               Left            =   0
               TabIndex        =   116
               Top             =   2400
               Visible         =   0   'False
               Width           =   1125
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Volumen(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   78
               Left            =   7800
               TabIndex        =   115
               Top             =   3000
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "ml/hora"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   77
               Left            =   6420
               TabIndex        =   114
               Top             =   2085
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Vel.Perfus."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   52
               Left            =   5400
               TabIndex        =   113
               Top             =   1800
               Width           =   945
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Vol.Total(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   51
               Left            =   8040
               TabIndex        =   112
               Top             =   2400
               Width           =   1080
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Instrucciones para administraci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   50
               Left            =   0
               TabIndex        =   111
               Top             =   1800
               Width           =   2865
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Volumen(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   49
               Left            =   7800
               TabIndex        =   110
               Top             =   0
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Sol"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   48
               Left            =   1200
               TabIndex        =   109
               Top             =   2040
               Visible         =   0   'False
               Width           =   930
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   47
               Left            =   0
               TabIndex        =   108
               Top             =   2040
               Visible         =   0   'False
               Width           =   600
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "FF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   37
               Left            =   5400
               TabIndex        =   107
               Top             =   0
               Width           =   225
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tiempo Infusi�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   36
               Left            =   6600
               TabIndex        =   106
               Top             =   1200
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Volumen(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   35
               Left            =   5520
               TabIndex        =   105
               Top             =   1200
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Soluci�n para Diluir"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   34
               Left            =   0
               TabIndex        =   104
               Top             =   1200
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   30
               Left            =   7320
               TabIndex        =   103
               Top             =   2400
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Inicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   33
               Left            =   5400
               TabIndex        =   102
               Top             =   2400
               Visible         =   0   'False
               Width           =   465
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   32
               Left            =   6120
               TabIndex        =   101
               Top             =   2400
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Fin"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   31
               Left            =   6600
               TabIndex        =   100
               Top             =   2400
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Periodicidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   29
               Left            =   3600
               TabIndex        =   99
               Top             =   600
               Width           =   1335
            End
            Begin VB.Label lblLabel1 
               Caption         =   "V�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   25
               Left            =   2760
               TabIndex        =   98
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Frecuencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   23
               Left            =   960
               TabIndex        =   97
               Top             =   600
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "UM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   21
               Left            =   7080
               TabIndex        =   96
               Top             =   0
               Width           =   300
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   19
               Left            =   6120
               TabIndex        =   95
               Top             =   0
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cantidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   0
               TabIndex        =   94
               Top             =   600
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Medicamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   16
               Left            =   0
               TabIndex        =   93
               Top             =   0
               Width           =   1140
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   0
            Left            =   -74880
            TabIndex        =   48
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3105
            Index           =   5
            Left            =   -74880
            TabIndex        =   49
            TabStop         =   0   'False
            Top             =   120
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   5477
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3465
            Index           =   6
            Left            =   420
            TabIndex        =   50
            Top             =   600
            Width           =   10215
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   6
            stylesets(0).Name=   "Estupefacientes"
            stylesets(0).BackColor=   14671839
            stylesets(0).Picture=   "FR0118.frx":0150
            stylesets(1).Name=   "FormMagistral"
            stylesets(1).BackColor=   8454016
            stylesets(1).Picture=   "FR0118.frx":016C
            stylesets(2).Name=   "Medicamentos"
            stylesets(2).BackColor=   16777215
            stylesets(2).Picture=   "FR0118.frx":0188
            stylesets(3).Name=   "MezclaIV2M"
            stylesets(3).BackColor=   8454143
            stylesets(3).Picture=   "FR0118.frx":01A4
            stylesets(4).Name=   "PRNenOM"
            stylesets(4).BackColor=   12615935
            stylesets(4).Picture=   "FR0118.frx":01C0
            stylesets(5).Name=   "Fluidoterapia"
            stylesets(5).BackColor=   16776960
            stylesets(5).Picture=   "FR0118.frx":01DC
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18018
            _ExtentY        =   6112
            _StockProps     =   79
         End
         Begin TabDlg.SSTab tab2 
            Height          =   4005
            Left            =   -74880
            TabIndex        =   61
            TabStop         =   0   'False
            Top             =   120
            Width           =   9495
            _ExtentX        =   16748
            _ExtentY        =   7064
            _Version        =   327681
            Style           =   1
            Tabs            =   6
            TabsPerRow      =   6
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Medicamento"
            TabPicture(0)   =   "FR0118.frx":01F8
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(64)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Mezcla IV 2M"
            TabPicture(1)   =   "FR0118.frx":0214
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lblLabel1(22)"
            Tab(1).ControlCount=   1
            TabCaption(2)   =   "PRN en OM"
            TabPicture(2)   =   "FR0118.frx":0230
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLabel1(27)"
            Tab(2).ControlCount=   1
            TabCaption(3)   =   "Flu�doterapia"
            TabPicture(3)   =   "FR0118.frx":024C
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "lblLabel1(43)"
            Tab(3).ControlCount=   1
            TabCaption(4)   =   "Estupefacientes"
            TabPicture(4)   =   "FR0118.frx":0268
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "lblLabel1(45)"
            Tab(4).ControlCount=   1
            TabCaption(5)   =   "Form Magistral"
            TabPicture(5)   =   "FR0118.frx":0284
            Tab(5).ControlEnabled=   0   'False
            Tab(5).Control(0)=   "lblLabel1(46)"
            Tab(5).ControlCount=   1
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   4425
               Index           =   9
               Left            =   -74880
               TabIndex        =   62
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1305
               Index           =   10
               Left            =   -74880
               TabIndex        =   63
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00808080&
               Caption         =   "Form Magistral"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   58
               Left            =   -69025
               TabIndex        =   86
               Top             =   90
               Width           =   1050
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H000000FF&
               Caption         =   "Estupefacientes"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   59
               Left            =   -70440
               TabIndex        =   85
               Top             =   90
               Width           =   1170
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00FFFF80&
               Caption         =   "Flu�doterapia"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   60
               Left            =   -71520
               TabIndex        =   84
               Top             =   90
               Width           =   915
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H0080FFFF&
               Caption         =   "PRN en OM"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   61
               Left            =   -72555
               TabIndex        =   83
               Top             =   90
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H0080FF80&
               Caption         =   "Mezcla IV 2M"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   63
               Left            =   -73725
               TabIndex        =   82
               Top             =   90
               Width           =   960
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00C0C0C0&
               Caption         =   "Medicamentos"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   64
               Left            =   120
               TabIndex        =   81
               Top             =   90
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   65
               Left            =   -74880
               TabIndex        =   80
               Top             =   480
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   66
               Left            =   -70920
               TabIndex        =   79
               Top             =   600
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   67
               Left            =   -67320
               TabIndex        =   78
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   68
               Left            =   -69360
               TabIndex        =   77
               Top             =   600
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   69
               Left            =   -74760
               TabIndex        =   76
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   70
               Left            =   -74760
               TabIndex        =   75
               Top             =   600
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   71
               Left            =   -70920
               TabIndex        =   74
               Top             =   1320
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   72
               Left            =   -72840
               TabIndex        =   73
               Top             =   1320
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   73
               Left            =   -74760
               TabIndex        =   72
               Top             =   1320
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   74
               Left            =   -74760
               TabIndex        =   71
               Top             =   1320
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que env�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   75
               Left            =   -69720
               TabIndex        =   70
               Top             =   1320
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N� Colegiado"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   76
               Left            =   -72120
               TabIndex        =   69
               Top             =   1320
               Width           =   1335
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00C0C0C0&
               Caption         =   "Mezcla IV 2M"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   22
               Left            =   -73725
               TabIndex        =   68
               Top             =   90
               Width           =   960
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00C0C0C0&
               Caption         =   "PRN en OM"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   27
               Left            =   -72550
               TabIndex        =   67
               Top             =   90
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00C0C0C0&
               Caption         =   "Flu�doterapia"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   43
               Left            =   -71520
               TabIndex        =   66
               Top             =   90
               Width           =   915
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00C0C0C0&
               Caption         =   "Estupefacientes"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   45
               Left            =   -70400
               TabIndex        =   65
               Top             =   90
               Width           =   1170
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00C0C0C0&
               Caption         =   "Form Magistral"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   46
               Left            =   -69025
               TabIndex        =   64
               Top             =   90
               Width           =   1050
            End
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Medicamentos"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   44
            Left            =   420
            TabIndex        =   56
            Top             =   210
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H0080FFFF&
            Caption         =   "Mezcla IV 2M"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   53
            Left            =   1935
            TabIndex        =   55
            Top             =   210
            Width           =   1260
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFC0FF&
            Caption         =   "PRN en OM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   54
            Left            =   3345
            TabIndex        =   54
            Top             =   210
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFF80&
            Caption         =   "Flu�doterapia"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   55
            Left            =   4620
            TabIndex        =   53
            Top             =   210
            Width           =   1275
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            Caption         =   "Estupefacientes"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   56
            Left            =   6060
            TabIndex        =   52
            Top             =   210
            Width           =   1485
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H0080FF80&
            Caption         =   "Form Magistral"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   57
            Left            =   7710
            TabIndex        =   51
            Top             =   210
            Width           =   1425
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Periodicidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Left            =   1800
      TabIndex        =   146
      Top             =   3960
      Visible         =   0   'False
      Width           =   6615
      Begin VB.CommandButton cmdAceptarPer 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   2640
         TabIndex        =   177
         Top             =   2280
         Width           =   1455
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   29
         Left            =   5310
         TabIndex        =   176
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   30
         Left            =   5910
         TabIndex        =   175
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   22
         Left            =   1110
         TabIndex        =   174
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   23
         Left            =   1710
         TabIndex        =   173
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   24
         Left            =   2310
         TabIndex        =   172
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   25
         Left            =   2910
         TabIndex        =   171
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   26
         Left            =   3510
         TabIndex        =   170
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   27
         Left            =   4110
         TabIndex        =   169
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   28
         Left            =   4710
         TabIndex        =   168
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   15
         Left            =   2910
         TabIndex        =   167
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   16
         Left            =   3510
         TabIndex        =   166
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   17
         Left            =   4110
         TabIndex        =   165
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   18
         Left            =   4710
         TabIndex        =   164
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   19
         Left            =   5310
         TabIndex        =   163
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   20
         Left            =   5910
         TabIndex        =   162
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   21
         Left            =   510
         TabIndex        =   161
         TabStop         =   0   'False
         Top             =   1920
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   8
         Left            =   4710
         TabIndex        =   160
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   9
         Left            =   5310
         TabIndex        =   159
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   10
         Left            =   5910
         TabIndex        =   158
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   11
         Left            =   510
         TabIndex        =   157
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   12
         Left            =   1110
         TabIndex        =   156
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   13
         Left            =   1710
         TabIndex        =   155
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   14
         Left            =   2310
         TabIndex        =   154
         TabStop         =   0   'False
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   153
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   2
         Left            =   1080
         TabIndex        =   152
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   3
         Left            =   1680
         TabIndex        =   151
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   4
         Left            =   2280
         TabIndex        =   150
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   5
         Left            =   2910
         TabIndex        =   149
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   6
         Left            =   3480
         TabIndex        =   148
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   7
         Left            =   4080
         TabIndex        =   147
         TabStop         =   0   'False
         Top             =   720
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "29"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   36
         Left            =   5310
         TabIndex        =   207
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "30"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   35
         Left            =   5910
         TabIndex        =   206
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "22"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   34
         Left            =   1110
         TabIndex        =   205
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "23"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   33
         Left            =   1710
         TabIndex        =   204
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "24"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   32
         Left            =   2310
         TabIndex        =   203
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "25"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   31
         Left            =   2910
         TabIndex        =   202
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "26"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   30
         Left            =   3510
         TabIndex        =   201
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "27"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   29
         Left            =   4110
         TabIndex        =   200
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "28"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   28
         Left            =   4710
         TabIndex        =   199
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "15"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   27
         Left            =   2910
         TabIndex        =   198
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "16"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   26
         Left            =   3510
         TabIndex        =   197
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "17"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   25
         Left            =   4110
         TabIndex        =   196
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "18"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   24
         Left            =   4710
         TabIndex        =   195
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "19"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   5310
         TabIndex        =   194
         Top             =   1080
         Width           =   225
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "20"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   22
         Left            =   5910
         TabIndex        =   193
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "21"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   21
         Left            =   510
         TabIndex        =   192
         Top             =   1680
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   20
         Left            =   4710
         TabIndex        =   191
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   19
         Left            =   5310
         TabIndex        =   190
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "10"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   5910
         TabIndex        =   189
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "11"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   17
         Left            =   510
         TabIndex        =   188
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "12"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   1110
         TabIndex        =   187
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "13"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   1710
         TabIndex        =   186
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "14"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   14
         Left            =   2310
         TabIndex        =   185
         Top             =   1080
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   12
         Left            =   1110
         TabIndex        =   184
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   11
         Left            =   1710
         TabIndex        =   183
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   2310
         TabIndex        =   182
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   2910
         TabIndex        =   181
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   3510
         TabIndex        =   180
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   4110
         TabIndex        =   179
         Top             =   480
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   480
         TabIndex        =   178
         Top             =   480
         Width           =   120
      End
   End
   Begin VB.CommandButton cmdbuscargruprod 
      Caption         =   "Grupo Medicamentos"
      Height          =   375
      Left            =   9240
      TabIndex        =   39
      Top             =   4680
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdbuscargruprot 
      Caption         =   "Grupos Terap�uticos"
      Height          =   375
      Left            =   9240
      TabIndex        =   38
      Top             =   4320
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdbuscarprot 
      Caption         =   "Protocolos"
      Height          =   375
      Left            =   9240
      TabIndex        =   37
      Top             =   6120
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdInformacion 
      Caption         =   "Informaci�n"
      Height          =   375
      Left            =   9240
      TabIndex        =   42
      Top             =   6840
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdMedNoForm 
      Caption         =   "Medic. No Formulario"
      Height          =   375
      Left            =   9240
      TabIndex        =   41
      Top             =   6480
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdSolDil 
      Caption         =   "Soluci�n para diluir"
      Height          =   375
      Left            =   9240
      TabIndex        =   35
      Top             =   5400
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdMed2 
      Caption         =   "Medicamento 2"
      Height          =   375
      Left            =   9240
      TabIndex        =   36
      Top             =   5760
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdInstAdm 
      Caption         =   "Instr.Administraci�n"
      Height          =   375
      Left            =   9240
      TabIndex        =   40
      Top             =   5040
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdbuscarprod 
      Caption         =   "Medicamentos"
      Height          =   375
      Left            =   9240
      TabIndex        =   34
      Top             =   3960
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label lblLabel1 
      Caption         =   "V�a"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   26
      Left            =   0
      TabIndex        =   47
      Top             =   0
      Width           =   975
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmBusProtocolos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDefProtocolos (FR0067.FRM)                                *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA:                                                               *
'* DESCRIPCION: Definir Protocolos                                      *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Dim vntAux As Variant
Dim intDia(30) As Integer
Dim intNumDia As Integer
Dim mstrOperacion As String 'saber que tipo de linea estamos creando en el hijo
'mstrOperacion = "/" 'Medicamento
'mstrOperacion = "M" 'Mezcla IV 2M
'mstrOperacion = "P" 'PRN en OM
'mstrOperacion = "F" 'Fluidoterapia
'mstrOperacion = "E" 'Estupefaciente
'mstrOperacion = "L" 'Form.Magistral
Dim intCambioAlgo As Integer 'Para saber si cambian las horas y minutos, por no ser campos CW
Dim blnDetalle As Boolean 'Para saber si estoy en modo detalle en el hijo
Dim blnBoton As Boolean 'Para saber si he pulsado un boton
Dim intBotonMasInf As Integer 'Para dar informacion en funcion del campo en el que este el cursor
Dim ginteventloadRedOM As Integer
Dim blnObligatorio As Boolean

Private Sub cmdExplorador_Click(Index As Integer)
Dim intRetVal
Dim strOpenFileName As String
Dim pathFileName As String
Dim finpath As Integer

If txtText1(34).Text = "" Then
   MsgBox "Debe hacer nuevo registro primero con el bot�n de F�rmula Magistral.", vbInformation, "Aviso"
  Exit Sub
End If


If Index = 0 Then
Else
    On Error Resume Next
    pathFileName = Dir(txtText1(57).Text)
    finpath = InStr(1, txtText1(57).Text, pathFileName, 1)
    pathFileName = Left(txtText1(57).Text, finpath - 1)
    CommonDialog1.InitDir = pathFileName
    CommonDialog1.filename = txtText1(57).Text
    CommonDialog1.CancelError = True
    CommonDialog1.Filter = "*.*"
    CommonDialog1.ShowOpen
    If ERR.Number <> 32755 Then    ' El usuario eligi� Cancelar.
        strOpenFileName = CommonDialog1.filename
        txtText1(57).SetFocus
        Call objWinInfo.CtrlSet(txtText1(57), "")
        txtText1(57).SetFocus
        Call objWinInfo.CtrlSet(txtText1(57), strOpenFileName)
    End If
End If

End Sub


Private Sub cmdInformacion_Click()

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
If Campos_Obligatorios = True Then
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If

cmdInformacion.Enabled = False
Me.Enabled = False
  
  Select Case intBotonMasInf
  Case 41 'solucion para diluir
    If txtText1(40).Text <> "" Then
      glngMasInfProd = txtText1(40).Text
      Call objsecurity.LaunchProcess("FR0184")
    Else
      Call MsgBox("No hay soluci�n para diluir.", vbInformation, "Aviso")
      Me.Enabled = True
      cmdInformacion.Enabled = True
      Exit Sub
    End If
  Case 51 'producto2
    If txtText1(52).Text <> "" Then
      glngMasInfProd = txtText1(52).Text
      Call objsecurity.LaunchProcess("FR0184")
    Else
      Call MsgBox("No hay Medicamento 2.", vbInformation, "Aviso")
      Me.Enabled = True
      cmdInformacion.Enabled = True
      Exit Sub
    End If
  Case Else
    If txtText1(27).Text <> "" Then
      glngMasInfProd = txtText1(27).Text
      Call objsecurity.LaunchProcess("FR0184")
    Else
      Call MsgBox("No hay Medicamento.", vbInformation, "Aviso")
      Me.Enabled = True
      cmdInformacion.Enabled = True
      Exit Sub
    End If
  End Select

glngMasInfProd = 0
Me.Enabled = True
cmdInformacion.Enabled = True

End Sub

Private Sub cmdInstAdm_Click()
Dim mensaje As String
Dim strupdate As String

If mstrOperacion = "P" Then
  cmdInstAdm.Enabled = False
  Me.Enabled = False
    If txtText1(0).Text <> "" Then
      If txtText1(34).Text = "" Then
        mensaje = MsgBox("Debe hacer nuevo registro primero.", vbInformation, "Aviso")
        Me.Enabled = True
        cmdInstAdm.Enabled = True
        Exit Sub
      End If
      Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
      gstrLlamadorProd = "frmDefProtocolos"
      gstrInstAdmin = ""
      gstrInstAdmin = txtText1(39).Text
      Call objsecurity.LaunchProcess("FR0188")
      Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
      If gstrInstAdmin <> "" Then
        txtText1(39).Text = gstrInstAdmin
      End If
      gstrInstAdmin = ""
      gstrLlamadorProd = ""
    End If
  Me.Enabled = True
  cmdInstAdm.Enabled = True
Else
  'If tlbToolbar1.Buttons(4).Enabled = True Then
  '  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  'End If
  'If tlbToolbar1.Buttons(4).Enabled = True Then
  '  Exit Sub
  'End If
  If Campos_Obligatorios = True Then
    Exit Sub
  ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  End If
  cmdInstAdm.Enabled = False
  Me.Enabled = False
    If txtText1(0).Text <> "" Then
      If txtText1(34).Text = "" Then
        mensaje = MsgBox("Debe hacer nuevo registro primero.", vbInformation, "Aviso")
        Me.Enabled = True
        cmdInstAdm.Enabled = True
        Exit Sub
      End If
      Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
      gstrLlamadorProd = "frmDefProtocolos"
      gstrInstAdmin = ""
      gstrInstAdmin = txtText1(39).Text
      Call objsecurity.LaunchProcess("FR0188")
      Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
      If gstrInstAdmin <> "" Then
        strupdate = "UPDATE FR1100 SET "
        strupdate = strupdate & "FR11INSTRADMIN='" & gstrInstAdmin & "'"
        strupdate = strupdate & " WHERE FR75CODPROTOCOLO=" & txtText1(0).Text
        strupdate = strupdate & " AND FR11NUMLINEA=" & txtText1(45).Text
        objApp.rdoConnect.Execute strupdate, 64
        Call objWinInfo.DataRefresh
      End If
      gstrInstAdmin = ""
      gstrLlamadorProd = ""
    End If
  Me.Enabled = True
  cmdInstAdm.Enabled = True
End If

End Sub

Private Sub cmdMed2_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strProducto As String
Dim rstProducto As rdoResultset
Dim strupdate As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
If Campos_Obligatorios = True Then
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If


cmdMed2.Enabled = False
Me.Enabled = False

  If txtText1(0).Text <> "" Then
    gstrLlamadorProd = "frmDefProtocolosmed2"
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    glngcodpeticion = txtText1(0).Text
    Call objsecurity.LaunchProcess("FR0073")
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    gstrLlamadorProd = ""
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If gintprodbuscado(v, 0) <> "" Then
          strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                    gintprodbuscado(v, 0)
          Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
          
          strupdate = "UPDATE FR1100 SET "
          strupdate = strupdate & "FR73CODPRODUCTO_2=" & gintprodbuscado(v, 0)
          If Not IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
            strupdate = strupdate & ",FRH7CODFORMFAR_2='" & rstProducto("FRH7CODFORMFAR").Value & "'"
          End If
          If IsNull(rstProducto("FR73DOSIS").Value) Then
            strupdate = strupdate & ",FR11DOSIS_2=1"
          Else
            vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
            strupdate = strupdate & ",FR11DOSIS_2=" & vntAux
          End If
          If Not IsNull(rstProducto("FR73INDDOSISPESO").Value) Then
            strupdate = strupdate & ",FR11DOSISPESO=" & rstProducto("FR73INDDOSISPESO").Value
          End If
          If Not IsNull(rstProducto("FR73INDDOSISSUPCORP").Value) Then
            strupdate = strupdate & ",FR11DOSISSUP=" & rstProducto("FR73INDDOSISSUPCORP").Value
          End If
          If Not IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
            strupdate = strupdate & ",FR93CODUNIMEDIDA_2='" & rstProducto("FR93CODUNIMEDIDA").Value & "'"
          End If
          If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
            strupdate = strupdate & ",FR11VOLUMEN_2='" & rstProducto("FR73VOLUMEN").Value & "'"
          End If
          VolTotal = 0
          If txtText1(38).Text = "" Then
            Vol1 = 0
          Else
            Vol1 = CCur(txtText1(38).Text)
          End If
          If txtText1(42).Text = "" Then
            Vol2 = 0
          Else
            Vol2 = CCur(txtText1(42).Text)
          End If
          If IsNull(rstProducto("FR73VOLUMEN").Value) Then
            Vol3 = 0
          Else
            Vol3 = CCur(rstProducto("FR73VOLUMEN").Value)
          End If
          VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
          vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
          strupdate = strupdate & ",FR11VOLTOTAL=" & vntAux
          If txtText1(43).Text = "" Then
            TiemMinInf = 0
          Else
            TiemMinInf = CCur(txtText1(43).Text) / 60
          End If
          If TiemMinInf <> 0 Then
            VolPerf = Format(VolTotal / TiemMinInf, "0.00")
          Else
            VolPerf = 0
          End If
          vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
          strupdate = strupdate & ",FR11VELPERFUSION=" & vntAux
          
          strupdate = strupdate & " WHERE FR75CODPROTOCOLO=" & txtText1(0).Text
          strupdate = strupdate & " AND FR11NUMLINEA=" & txtText1(45).Text
          
          objApp.rdoConnect.Execute strupdate, 64
          
          rstProducto.Close
          Set rstProducto = Nothing
          
          Call objWinInfo.DataRefresh

        End If
      Next v
    End If
    gintprodtotal = 0
  End If

Me.Enabled = True
cmdMed2.Enabled = True

End Sub

Private Sub cmdMedNoForm_Click()
Dim mensaje As String

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
If Campos_Obligatorios = True Then
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If


blnBoton = True
cmdMedNoForm.Enabled = False
Me.Enabled = False

  If txtText1(0).Text <> "" Then
    tab2.Tab = 0
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
    Call objWinInfo.CtrlSet(txtText1(27), 999999999)
    Call objWinInfo.CtrlSet(cboDBCombo1(3), "Diario")
  End If

Me.Enabled = True
cmdMedNoForm.Enabled = True
blnBoton = False
txtText1(55).SetFocus

End Sub

Private Sub cmdSolDil_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strProducto As String
Dim rstProducto As rdoResultset
Dim strupdate As String
Dim strinsert As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
If Campos_Obligatorios = True Then
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If


cmdSolDil.Enabled = False
Me.Enabled = False

  If txtText1(0).Text <> "" Then
    If cmdSolDil.Caption = "Soluci�n Intravenosa" Then
      gstrLlamadorProd = "frmDefProtocolossolFlui"
    Else
      gstrLlamadorProd = "frmDefProtocolossoldil"
    End If
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    glngcodpeticion = txtText1(0).Text
    Call objsecurity.LaunchProcess("FR0073")
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    gstrLlamadorProd = ""
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If gintprodbuscado(v, 0) <> "" Then
          strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                    gintprodbuscado(v, 0)
          Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
            
          If cmdSolDil.Caption = "Soluci�n Intravenosa" Then
            strlinea = "SELECT MAX(FR11NUMLINEA) FROM FR1100 WHERE FR75CODPROTOCOLO=" & _
              txtText1(0).Text
            Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
            If IsNull(rstlinea.rdoColumns(0).Value) Then
              linea = 1
            Else
              linea = rstlinea.rdoColumns(0).Value + 1
            End If
          
            '??????????????????????????-
            strinsert = "INSERT INTO FR1100"
            strinsert = strinsert & " (FR75CODPROTOCOLO,FR11NUMLINEA,"
            strinsert = strinsert & "FR73CODPRODUCTO_DIL,"
            strinsert = strinsert & "FR11VOLTOTAL,FR11CANTIDADDIL,FR11TIEMMININF,"
            strinsert = strinsert & "FR11OPERACION,"
            strinsert = strinsert & "FR11CANTIDAD,FR11CANTDISP,FRG4CODFRECUENCIA,"
            strinsert = strinsert & "FR34CODVIA,FR11INDVIAOPC,FR11VELPERFUSION,"
            strinsert = strinsert & "FRH5CODPERIODICIDAD"
            strinsert = strinsert & ") VALUES " & _
                      "(" & _
                      txtText1(0).Text & "," & _
                      linea & "," & _
                      gintprodbuscado(v, 0) & ","
            'If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
            '  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLINFIV").Value, ",", ".", 1)
            '  strinsert = strinsert & vntAux & "," & vntAux & ","
            '  VolTotal = CCur(rstProducto("FR73VOLINFIV").Value)
            'Else
            '  VolTotal = 0
            '  strinsert = strinsert & "NULL," & "NULL,"
            'End If
            If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
              vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & "," & vntAux & ","
              VolTotal = CCur(rstProducto("FR73VOLUMEN").Value)
            Else
              VolTotal = 0
              strinsert = strinsert & "NULL," & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
              strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
            Else
              strinsert = strinsert & "NULL,"
            End If
            strinsert = strinsert & "'F',1,1," 'FR11CANTIDAD,FR11CANTDISP
            
            If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
              strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR34CODVIA").Value) Then
              strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
              strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
              vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            Else
              If txtText1(43).Text = "" Then
                TiemMinInf = 0
              Else
                TiemMinInf = CCur(txtText1(43).Text) / 60
              End If
              If TiemMinInf <> 0 Then
                VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              Else
                VolPerf = 0
                strinsert = strinsert & VolPerf & ","
              End If
            End If
            strinsert = strinsert & "'Diario')"
            
            objApp.rdoConnect.Execute strinsert, 64
            
            rstProducto.Close
            Set rstProducto = Nothing
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
            
          Else
            strupdate = "UPDATE FR1100 SET "
            strupdate = strupdate & "FR73CODPRODUCTO_DIL=" & gintprodbuscado(v, 0)
            'If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
            If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
              vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
              strupdate = strupdate & ",FR11CANTIDADDIL=" & vntAux
            End If
            If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
              strupdate = strupdate & ",FR11TIEMMININF=" & rstProducto("FR73TIEMINF").Value
            End If
            VolTotal = 0
            If txtText1(38).Text = "" Then
              Vol1 = 0
            Else
              Vol1 = CCur(txtText1(38).Text)
            End If
            If IsNull(rstProducto("FR73VOLUMEN").Value) Then
              Vol2 = 0
            Else
              Vol2 = CCur(rstProducto("FR73VOLUMEN").Value)
            End If
            If txtText1(47).Text = "" Then
              Vol3 = 0
            Else
              Vol3 = CCur(txtText1(47).Text)
            End If
            VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
            vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
            strupdate = strupdate & ",FR11VOLTOTAL=" & vntAux
            If txtText1(43).Text = "" Then
              TiemMinInf = 0
            Else
              TiemMinInf = CCur(txtText1(43).Text) / 60
            End If
            If TiemMinInf <> 0 Then
              VolPerf = Format(VolTotal / TiemMinInf, "0.00")
            Else
              VolPerf = 0
            End If
            vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
            strupdate = strupdate & ",FR11VELPERFUSION=" & vntAux
            
            strupdate = strupdate & " WHERE FR75CODPROTOCOLO=" & txtText1(0).Text
            strupdate = strupdate & " AND FR11NUMLINEA=" & txtText1(45).Text
            
            objApp.rdoConnect.Execute strupdate, 64
            
            rstProducto.Close
            Set rstProducto = Nothing
            
            Call objWinInfo.DataRefresh
          End If
        End If
      Next v
    End If
    gintprodtotal = 0
  End If

Me.Enabled = True
cmdSolDil.Enabled = True

End Sub

Private Sub cmdtraerprot_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mensaje As String
Dim rsta As rdoResultset
Dim stra As String
Dim rstrest As rdoResultset
Dim strrest As String
Dim strinsert As String

cmdtraerprot.Enabled = False
'If objWinInfo.objWinActiveForm.strName = "Protocolos" Then
'Guardamos el n�mero de filas seleccionadas
'mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
mintNTotalSelRows = grdDBGrid1(6).Rows
ReDim gintprodbuscado(mintNTotalSelRows, 10)
gintprodtotal = mintNTotalSelRows
grdDBGrid1(6).MoveFirst
For mintisel = 0 To mintNTotalSelRows - 1
     mvarBkmrk = grdDBGrid1(6).SelBookmarks(mintisel)
     gintprodbuscado(mintisel, 0) = grdDBGrid1(6).Columns("C�digo Medicamento").CellValue(mvarBkmrk) 'COD PROD
     'gintprodbuscado(mintisel, 1) = grdDBGrid1(6).Columns(6).CellValue(mvarBkmrk) 'COD INT
     gintprodbuscado(mintisel, 2) = grdDBGrid1(6).Columns("Medicamento").CellValue(mvarBkmrk) 'DESC PROD
     gintprodbuscado(mintisel, 3) = grdDBGrid1(6).Columns("Dosis").CellValue(mvarBkmrk) 'DOSIS
     gintprodbuscado(mintisel, 4) = grdDBGrid1(6).Columns("UM").CellValue(mvarBkmrk) 'UM
     gintprodbuscado(mintisel, 7) = grdDBGrid1(6).Columns("Referencia").CellValue(mvarBkmrk) 'REF
     gintprodbuscado(mintisel, 8) = grdDBGrid1(6).Columns("FF").CellValue(mvarBkmrk) 'FF
     'gintprodbuscado(mintisel, 9) = grdDBGrid1(6).Columns(9).CellValue(mvarBkmrk) 'TAM

     gintprodbuscado(mintisel, 5) = grdDBGrid1(6).Columns("Num.Linea").CellValue(mvarBkmrk) 'linea
     gintprodbuscado(mintisel, 6) = grdDBGrid1(6).Columns("C�digo Petici�n").CellValue(mvarBkmrk) 'protocolo
     
     gintprodbuscado(mintisel, 10) = grdDBGrid1(6).Columns("Cantidad").CellValue(mvarBkmrk) 'CANTIDAD
     '----------------------------------------------------------------------------------
       'para cada producto hay que mirar si es de uso restringido
       stra = "SELECT FR73INDUSOREST FROM FR7300 WHERE FR73CODPRODUCTO=" & _
               grdDBGrid1(6).Columns("C�digo Medicamento").CellValue(mvarBkmrk)
       Set rsta = objApp.rdoConnect.OpenResultset(stra)
       If rsta.rdoColumns(0).Value = -1 Then
          'si es de uso restringido se llama a la pantalla de restricciones,pero antes
          'se vuelca el contenido de FRA900 en FRA400
              stra = "SELECT * FROM FRA900 WHERE FR73CODPRODUCTO=" & _
                     grdDBGrid1(6).Columns("C�digo Medicamento").CellValue(mvarBkmrk)
              Set rsta = objApp.rdoConnect.OpenResultset(stra)
              While Not rsta.EOF
                  strrest = "SELECT FRA9CODREST FROM FRA400 WHERE FR73CODPRODUCTO=" & _
                            grdDBGrid1(6).Columns("C�digo Medicamento").CellValue(mvarBkmrk) & " AND " & _
                            "FR66CODPETICION=" & glngcodpeticion & " AND " & _
                            "FRA9CODREST=" & rsta.rdoColumns("FRA9CODREST").Value
                  Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
                  If rstrest.EOF Then
                    strinsert = "INSERT INTO FRA400(FR73CODPRODUCTO,FR66CODPETICION," & _
                              "FRA9CODREST,FRA4DESCREST,FRA4INDSELECCIONADO) VALUES(" & _
                              grdDBGrid1(6).Columns("C�digo Medicamento").CellValue(mvarBkmrk) & "," & _
                              glngcodpeticion & "," & _
                              rsta.rdoColumns("FRA9CODREST").Value & "," & _
                              "'" & rsta.rdoColumns("FRA9DESREST").Value & "'" & "," & _
                              "0" & ")"
                    objApp.rdoConnect.Execute strinsert, 64
                    objApp.rdoConnect.Execute "Commit", 64
                  End If
                  rstrest.Close
                  Set rstrest = Nothing
              rsta.MoveNext
              Wend
              rsta.Close
              Set rsta = Nothing
              glngcodprod = grdDBGrid1(6).Columns(5).CellValue(mvarBkmrk)
              Call objsecurity.LaunchProcess("FR0182")
       End If
       '---------------------------------------------------------------------------------
grdDBGrid1(6).MoveNext
Next mintisel
cmdtraerprot.Enabled = True
Unload Me
'Else
'mensaje = MsgBox("No ha seleccionado ning�n protocolo", vbInformation, "Aviso")
'cmdtraerprot.Enabled = True
'End If
End Sub

Private Sub cmdWord_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim strpathword As String
  
On Error GoTo error_shell
  
Me.Enabled = False
'Par�metros generales
'5,Ubicaci�n de Word,C:\Archivos de programa\Microsoft Office\Office\winword.exe,TEXTO
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=5"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strpathword = rsta(0).Value
  Else
    rsta.Close
    Set rsta = Nothing
    MsgBox "Debe introducir el Path del Word en par�metros generales." & Chr(13) & _
           "C�digo par�metros generales : 5 " & Chr(13) & _
           "Ej.: C:\Archivos de programa\Microsoft Office\Office\winword.exe", vbInformation
    Me.Enabled = True
    Exit Sub
  End If
  rsta.Close
  Set rsta = Nothing
  
  If txtText1(57).Text = "" Then
    MsgBox "Debe introducir el Path y el nombre del fichero del documento", vbInformation
    Me.Enabled = True
    Exit Sub
  End If
  
  Shell strpathword & " " & txtText1(57).Text, vbMaximizedFocus
  Me.Enabled = True
  Exit Sub
  
error_shell:

  If ERR.Number <> 0 Then
    MsgBox "Error N�mero: " & ERR.Number & Chr(13) & ERR.Description, vbCritical
  End If
  Me.Enabled = True
  Exit Sub

End Sub


Private Sub lblLabel1_DblClick(Index As Integer)
    
    Select Case Index
    Case 44
        If tab2.Tab = 0 Then
          blnDetalle = True
          tab2.Tab = 1
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 0
    Case 53
        If tab2.Tab = 1 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 1
    Case 54
        If tab2.Tab = 2 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 2
    Case 55
        If tab2.Tab = 3 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 3
    Case 56
        If tab2.Tab = 4 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 4
    Case 57
        If tab2.Tab = 5 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 5
        txtText1(57).SetFocus
    End Select
    
    Call presentacion_pantalla

End Sub

Private Sub tab2_Click(PreviousTab As Integer)
Dim auxtab As Integer
    
If blnObligatorio = False Then
  If objWinInfo.objWinActiveForm.strName <> "Detalle Protocolo" Then
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
  End If

  If tab2.Tab <> PreviousTab And blnDetalle Then
    'If tlbToolbar1.Buttons(4).Enabled = True Then
      'Guardar
      If Campos_Obligatorios = True Then
        blnObligatorio = True
        tab2.Tab = PreviousTab
        Exit Sub
      ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
        auxtab = tab2.Tab
        Call objWinInfo.DataSave
        blnObligatorio = False
        tab2.Tab = auxtab
      End If
    'Else
      blnObligatorio = False
      'Vaciar pantalla
      objWinInfo.intWinStatus = cwModeSingleEmpty
      Call objWinInfo.WinStabilize
    'End If
  End If

  Call presentacion_pantalla
  If tabTab1(1).Tab = 0 And tab2.Tab = 5 And blnDetalle Then
    txtText1(57).SetFocus
  ElseIf tabTab1(1).Tab = 0 And blnDetalle Then
    If txtText1(35).Enabled = True And txtText1(35).Visible = True Then
      txtText1(35).SetFocus
    End If
  End If
Else
  blnObligatorio = False
End If
    
End Sub

Private Sub tab2_DblClick()
  
    Call presentacion_pantalla

End Sub



Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If

End Sub


Private Sub cmdAceptarPer_Click()
Dim i As Integer
Dim strdias As String
Dim strupdate As String
    
  tlbToolbar1.Enabled = True
  Frame2.Visible = False
  cmdbuscargruprot.Enabled = True
  cmdbuscarprot.Enabled = True
  cmdbuscargruprod.Enabled = True
  cmdbuscarprod.Enabled = True
  fraframe1(0).Enabled = True
  fraframe1(1).Enabled = True

  For i = 1 To 30
    If Check1(i).Value = 1 Then
      strdias = strdias & "," & i
    End If
  Next i
  If strdias <> "" Then
    strdias = Right(strdias, Len(strdias) - 1)
  End If
  'Call objWinInfo.CtrlSet(txtText1(10), strdias)

  strupdate = "UPDATE FR1100 SET FR11DIASADM='" & strdias & "'"
  strupdate = strupdate & ",FR11NUMDIAS=NULL"
  strupdate = strupdate & " WHERE FR75CODPROTOCOLO=" & txtText1(0).Text
  strupdate = strupdate & " AND FR11NUMLINEA=" & txtText1(45).Text

  objApp.rdoConnect.Execute strupdate, 64
  objApp.rdoConnect.Execute "Commit", 64

  objWinInfo.DataRefresh
    
End Sub


Private Sub cmdbuscargruprod_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strinsert As String
Dim stra As String
Dim rsta As rdoResultset
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstProducto As rdoResultset
Dim strProducto As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String
Dim blnIffiv As Boolean
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
If Campos_Obligatorios = True Then
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If

blnBoton = True
cmdbuscargruprod.Enabled = False
Me.Enabled = False

  If txtText1(0).Text <> "" Then
    gstrLlamadorProd = "frmDefProtocolosGM"
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    glngcodpeticion = txtText1(0).Text
    Call objsecurity.LaunchProcess("FR0187")
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    gstrLlamadorProd = ""
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        strlinea = "SELECT MAX(FR11NUMLINEA) FROM FR1100 WHERE FR75CODPROTOCOLO=" & _
          txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          linea = 1
        Else
          linea = rstlinea.rdoColumns(0).Value + 1
        End If
        If gintprodbuscado(v, 0) <> "" Then
          strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                    gintprodbuscado(v, 0)
          Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
          If IsNull(rstProducto("FR73INDINFIV").Value) Then
            blnIffiv = False
          Else
            If rstProducto("FR73INDINFIV").Value = -1 Then
              blnIffiv = True
            Else
              blnIffiv = False
            End If
          End If
          If blnIffiv = False Then
            strinsert = "INSERT INTO FR1100 (FR75CODPROTOCOLO,FR11NUMLINEA,FR73CODPRODUCTO," & _
                        "FR11DOSIS,FR11DOSISPESO,FR11DOSISSUP,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR11VOLUMEN,FR11VOLTOTAL,FR34CODVIA," & _
                        "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR11OPERACION," & _
                        "FR11INDVIAOPC,FR11CANTIDADDIL,FR11CANTIDAD,FR11CANTDISP,FR11TIEMMININF"
            strinsert = strinsert & ",FR11REFERENCIA,FR11UBICACION,FR11VELPERFUSION"
            strinsert = strinsert & ",FRH5CODPERIODICIDAD"
            strinsert = strinsert & ") VALUES " & _
                        "(" & _
                        txtText1(0).Text & "," & _
                        linea & "," & _
                        gintprodbuscado(v, 0) & ","
            If IsNull(rstProducto("FR73DOSIS").Value) Then
              strinsert = strinsert & "1,"
            Else
              vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            End If
            If IsNull(rstProducto("FR73INDDOSISPESO").Value) Then
              strinsert = strinsert & "NULL,"
            Else
              strinsert = strinsert & rstProducto("FR73INDDOSISPESO").Value & ","
            End If
            If IsNull(rstProducto("FR73INDDOSISSUPCORP").Value) Then
              strinsert = strinsert & "NULL,"
            Else
              strinsert = strinsert & rstProducto("FR73INDDOSISSUPCORP").Value & ","
            End If
            If IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
            End If
            If IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA").Value & "',"
            End If
            If IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR11VOLTOTAL
              strinsert = strinsert & "null,null,"
            Else
              vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & "," & vntAux & ","
            End If
            If IsNull(rstProducto("FR34CODVIA").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
            End If
            If IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
                strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FRH7CODFORMFAR").Value & "',"
            End If
            'If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Or mstrOperacion <> "=" Then
            If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR73CODPRODUCTO_DIL").Value & ","
            End If
            If IsNull(rstProducto("FR73INDESTUPEFACI")) Then
              strinsert = strinsert & "'" & mstrOperacion & "',"
            Else
              If rstProducto("FR73INDESTUPEFACI") = -1 Then
                strinsert = strinsert & "'E',"
              Else
                strinsert = strinsert & "'" & mstrOperacion & "',"
              End If
            End If
            If IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
            End If
            'If IsNull(rstProducto("FR73VOLINFIV").Value) Or mstrOperacion <> "=" Then
            If IsNull(rstProducto("FR73VOLINFIV").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR73VOLINFIV").Value & ","
            End If
            strinsert = strinsert & "1,1," 'FR11CANTIDAD,FR11CANTDISP
            'If IsNull(rstProducto("FR73TIEMINF").Value) Or mstrOperacion <> "=" Then
            If IsNull(rstProducto("FR73TIEMINF").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
            End If
            If IsNull(rstProducto("FR73REFERENCIA").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR73REFERENCIA").Value & "',"
            End If
            If IsNull(rstProducto("FRH9UBICACION").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FRH9UBICACION").Value & "',"
            End If
            If IsNull(rstProducto("FR73VELPERFUSION").Value) Then
              strinsert = strinsert & "null,"
            Else
              vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            End If
            strinsert = strinsert & "'Diario')" 'FRH5CODPERIODICIDAD
          Else 'IV
            strinsert = "INSERT INTO FR1100"
            strinsert = strinsert & " (FR75CODPROTOCOLO,FR11NUMLINEA,"
            strinsert = strinsert & "FR73CODPRODUCTO_DIL,"
            strinsert = strinsert & "FR11VOLTOTAL,FR11CANTIDADDIL,FR11TIEMMININF,"
            strinsert = strinsert & "FR11OPERACION,"
            strinsert = strinsert & "FR11CANTIDAD,FR11CANTDISP,FRG4CODFRECUENCIA,"
            strinsert = strinsert & "FR34CODVIA,FR11INDVIAOPC,FR11VELPERFUSION,"
            strinsert = strinsert & "FRH5CODPERIODICIDAD"
            strinsert = strinsert & ") VALUES " & _
                      "(" & _
                      txtText1(0).Text & "," & _
                      linea & "," & _
                      gintprodbuscado(v, 0) & ","
            'If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
            '  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLINFIV").Value, ",", ".", 1)
            '  strinsert = strinsert & vntAux & "," & vntAux & ","
            '  VolTotal = CCur(rstProducto("FR73VOLINFIV").Value)
            'Else
            '  VolTotal = 0
            '  strinsert = strinsert & "NULL," & "NULL,"
            'End If
            If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
              vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & "," & vntAux & ","
              VolTotal = CCur(rstProducto("FR73VOLUMEN").Value)
            Else
              VolTotal = 0
              strinsert = strinsert & "NULL," & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
              strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
            Else
              strinsert = strinsert & "NULL,"
            End If
            strinsert = strinsert & "'F',1,1," 'FR11CANTIDAD,FR11CANTDISP
            
            If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
              strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR34CODVIA").Value) Then
              strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
              strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
              vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            Else
              If txtText1(43).Text = "" Then
                TiemMinInf = 0
              Else
                TiemMinInf = CCur(txtText1(43).Text) / 60
              End If
              If TiemMinInf <> 0 Then
                VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              Else
                VolPerf = 0
                strinsert = strinsert & VolPerf & ","
              End If
            End If
            strinsert = strinsert & "'Diario')"
          End If
          
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
          
          rstlinea.Close
          Set rstlinea = Nothing
          
          rstProducto.Close
          Set rstProducto = Nothing
          'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
        End If
      Next v
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
    End If
    gintprodtotal = 0
  End If

Me.Enabled = True
txtText1(35).SetFocus
cmdbuscargruprod.Enabled = True
blnBoton = False

End Sub

Private Sub cmdbuscargruprot_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strinsert As String
Dim stra As String
Dim rsta As rdoResultset
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstProducto As rdoResultset
Dim strProducto As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String
Dim blnIffiv As Boolean
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
If Campos_Obligatorios = True Then
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If

blnBoton = True
cmdbuscargruprot.Enabled = False
Me.Enabled = False

  If txtText1(0).Text <> "" Then
    gstrLlamadorProd = "frmDefProtocolosGT"
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    glngcodpeticion = txtText1(0).Text
    Call objsecurity.LaunchProcess("FR0072")
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    gstrLlamadorProd = ""
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        strlinea = "SELECT MAX(FR11NUMLINEA) FROM FR1100 WHERE FR75CODPROTOCOLO=" & _
                  txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          linea = 1
        Else
          linea = rstlinea.rdoColumns(0).Value + 1
        End If
        If gintprodbuscado(v, 0) <> "" Then
          strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                        gintprodbuscado(v, 0)
          Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
          If IsNull(rstProducto("FR73INDINFIV").Value) Then
            blnIffiv = False
          Else
            If rstProducto("FR73INDINFIV").Value = -1 Then
              blnIffiv = True
            Else
              blnIffiv = False
            End If
          End If
          If blnIffiv = False Then
            strinsert = "INSERT INTO FR1100 (FR75CODPROTOCOLO,FR11NUMLINEA,FR73CODPRODUCTO," & _
                        "FR11DOSIS,FR11DOSISPESO,FR11DOSISSUP,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR11VOLUMEN,FR11VOLTOTAL,FR34CODVIA," & _
                        "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR11OPERACION," & _
                        "FR11INDVIAOPC,FR11CANTIDADDIL,FR11CANTIDAD,FR11CANTDISP,FR11TIEMMININF"
            strinsert = strinsert & ",FR11REFERENCIA,FR11UBICACION,FR11VELPERFUSION"
            strinsert = strinsert & ",FRH5CODPERIODICIDAD"
            strinsert = strinsert & ") VALUES " & _
                        "(" & _
                        txtText1(0).Text & "," & _
                        linea & "," & _
                        gintprodbuscado(v, 0) & ","
            If IsNull(rstProducto("FR73DOSIS").Value) Then
              strinsert = strinsert & "1,"
            Else
              vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            End If
            If IsNull(rstProducto("FR73INDDOSISPESO").Value) Then
              strinsert = strinsert & "NULL,"
            Else
              strinsert = strinsert & rstProducto("FR73INDDOSISPESO").Value & ","
            End If
            If IsNull(rstProducto("FR73INDDOSISSUPCORP").Value) Then
              strinsert = strinsert & "NULL,"
            Else
              strinsert = strinsert & rstProducto("FR73INDDOSISSUPCORP").Value & ","
            End If
            If IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
            End If
            If IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA").Value & "',"
            End If
            If IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR11VOLTOTAL
              strinsert = strinsert & "null,null,"
            Else
              vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & "," & vntAux & ","
            End If
            If IsNull(rstProducto("FR34CODVIA").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
            End If
            If IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FRH7CODFORMFAR").Value & "',"
            End If
            'If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Or mstrOperacion <> "=" Then
            If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR73CODPRODUCTO_DIL").Value & ","
            End If
            If IsNull(rstProducto("FR73INDESTUPEFACI")) Then
              strinsert = strinsert & "'" & mstrOperacion & "',"
            Else
              If rstProducto("FR73INDESTUPEFACI") = -1 Then
                strinsert = strinsert & "'E',"
              Else
                strinsert = strinsert & "'" & mstrOperacion & "',"
              End If
            End If
            If IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
            End If
            'If IsNull(rstProducto("FR73VOLINFIV").Value) Or mstrOperacion <> "=" Then
            If IsNull(rstProducto("FR73VOLINFIV").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR73VOLINFIV").Value & ","
            End If
            strinsert = strinsert & "1,1," 'FR11CANTIDAD,FR11CANTDISP
            'If IsNull(rstProducto("FR73TIEMINF").Value) Or mstrOperacion <> "=" Then
            If IsNull(rstProducto("FR73TIEMINF").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
            End If
            If IsNull(rstProducto("FR73REFERENCIA").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FR73REFERENCIA").Value & "',"
            End If
            If IsNull(rstProducto("FRH9UBICACION").Value) Then
              strinsert = strinsert & "null,"
            Else
              strinsert = strinsert & "'" & rstProducto("FRH9UBICACION").Value & "',"
            End If
            If IsNull(rstProducto("FR73VELPERFUSION").Value) Then
              strinsert = strinsert & "null,"
            Else
              vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            End If
            strinsert = strinsert & "'Diario')" 'FRH5CODPERIODICIDAD
          Else 'IV
            strinsert = "INSERT INTO FR1100"
            strinsert = strinsert & " (FR75CODPROTOCOLO,FR11NUMLINEA,"
            strinsert = strinsert & "FR73CODPRODUCTO_DIL,"
            strinsert = strinsert & "FR11VOLTOTAL,FR11CANTIDADDIL,FR11TIEMMININF,"
            strinsert = strinsert & "FR11OPERACION,"
            strinsert = strinsert & "FR11CANTIDAD,FR11CANTDISP,FRG4CODFRECUENCIA,"
            strinsert = strinsert & "FR34CODVIA,FR11INDVIAOPC,FR11VELPERFUSION,"
            strinsert = strinsert & "FRH5CODPERIODICIDAD"
            strinsert = strinsert & ") VALUES " & _
                      "(" & _
                      txtText1(0).Text & "," & _
                      linea & "," & _
                      gintprodbuscado(v, 0) & ","
            'If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
            '  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLINFIV").Value, ",", ".", 1)
            '  strinsert = strinsert & vntAux & "," & vntAux & ","
            '  VolTotal = CCur(rstProducto("FR73VOLINFIV").Value)
            'Else
            '  VolTotal = 0
            '  strinsert = strinsert & "NULL," & "NULL,"
            'End If
            If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
              vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & "," & vntAux & ","
              VolTotal = CCur(rstProducto("FR73VOLUMEN").Value)
            Else
              VolTotal = 0
              strinsert = strinsert & "NULL," & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
              strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
            Else
              strinsert = strinsert & "NULL,"
            End If
            strinsert = strinsert & "'F',1,1," 'FR11CANTIDAD,FR11CANTDISP
            
            If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
              strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR34CODVIA").Value) Then
              strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
              strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
            Else
              strinsert = strinsert & "NULL,"
            End If
            If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
              vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
              strinsert = strinsert & vntAux & ","
            Else
              If txtText1(43).Text = "" Then
                TiemMinInf = 0
              Else
                TiemMinInf = CCur(txtText1(43).Text) / 60
              End If
              If TiemMinInf <> 0 Then
                VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              Else
                VolPerf = 0
                strinsert = strinsert & VolPerf & ","
              End If
            End If
            strinsert = strinsert & "'Diario')"
          End If
          
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
                  
          rstlinea.Close
          Set rstlinea = Nothing
          
          rstProducto.Close
          Set rstProducto = Nothing
          'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
        End If
      Next v
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
    End If
    gintprodtotal = 0
  End If

Me.Enabled = True
txtText1(35).SetFocus
cmdbuscargruprot.Enabled = True
blnBoton = False

End Sub

Private Sub cmdbuscarprod_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strinsert As String
Dim stra As String
Dim rsta As rdoResultset
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstProducto As rdoResultset
Dim strProducto As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String
Dim strupdate As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim blnIffiv As Boolean

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
If Campos_Obligatorios = True Then
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If

blnBoton = True
cmdbuscarprod.Enabled = False
Me.Enabled = False


  If txtText1(0).Text <> "" Then
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Select Case mstrOperacion
    Case "/" 'Medicamento
        gstrLlamadorProd = "frmDefProtocolosMed"
        Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
        glngcodpeticion = txtText1(0).Text
        Call objsecurity.LaunchProcess("FR0073")
        Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    Case "M" 'Mezcla IV 2M
        gstrLlamadorProd = "frmDefProtocolosMed"
        Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
        glngcodpeticion = txtText1(0).Text
        Call objsecurity.LaunchProcess("FR0073")
        Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    Case "P" 'PRN en OM
        gstrLlamadorProd = "frmDefProtocolosMed"
        Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
        glngcodpeticion = txtText1(0).Text
        Call objsecurity.LaunchProcess("FR0073")
        Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    Case "F" 'Fluidoterapia
      '??????????????????????????
        gstrLlamadorProd = "frmDefProtocolosFlui"
        Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
        glngcodpeticion = txtText1(0).Text
        Call objsecurity.LaunchProcess("FR0073")
        Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    Case "E" 'Estupefaciente
        gstrLlamadorProd = "frmDefProtocolosEstu"
        Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
        glngcodpeticion = txtText1(0).Text
        Call objsecurity.LaunchProcess("FR0073")
        Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    Case "L" 'Form.Magistral
        blnBoton = True
        cmdbuscarprod.Enabled = False
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        'Call objWinInfo.CtrlSet(txtText1(31), "NE")
        'Call objWinInfo.CtrlSet(txtText1(28), 1)
        Me.Enabled = True
        txtText1(57).SetFocus
        cmdbuscarprod.Enabled = True
        blnBoton = False
        Exit Sub
    End Select
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If gintprodtotal > 0 Then
      If gstrLlamadorProd <> "frmDefProtocolosFlui" Then
        For v = 0 To gintprodtotal - 1
          strlinea = "SELECT MAX(FR11NUMLINEA) FROM FR1100 WHERE FR75CODPROTOCOLO=" & _
          txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If IsNull(rstlinea.rdoColumns(0).Value) Then
            linea = 1
          Else
            linea = rstlinea.rdoColumns(0).Value + 1
          End If
          If gintprodbuscado(v, 0) <> "" Then
            strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
            gintprodbuscado(v, 0)
            Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
            If IsNull(rstProducto("FR73INDINFIV").Value) Then
              blnIffiv = False
            Else
              If rstProducto("FR73INDINFIV").Value = -1 Then
                blnIffiv = True
              Else
                blnIffiv = False
              End If
            End If
            If blnIffiv = False Then
              strinsert = "INSERT INTO FR1100 (FR75CODPROTOCOLO,FR11NUMLINEA,FR73CODPRODUCTO," & _
                          "FR11DOSIS,FR11DOSISPESO,FR11DOSISSUP,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR11VOLUMEN,FR11VOLTOTAL,FR34CODVIA," & _
                          "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR11OPERACION," & _
                          "FR11INDVIAOPC,FR11CANTIDADDIL,FR11CANTIDAD,FR11CANTDISP,FR11TIEMMININF"
              strinsert = strinsert & ",FR11REFERENCIA,FR11UBICACION,FR11VELPERFUSION"
              strinsert = strinsert & ",FRH5CODPERIODICIDAD"
              strinsert = strinsert & ") VALUES " & _
                          "(" & _
                          txtText1(0).Text & "," & _
                          linea & "," & _
                          gintprodbuscado(v, 0) & ","
              If IsNull(rstProducto("FR73DOSIS").Value) Then
                strinsert = strinsert & "1,"
              Else
                vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              End If
              If IsNull(rstProducto("FR73INDDOSISPESO").Value) Then
                strinsert = strinsert & "NULL,"
              Else
                strinsert = strinsert & rstProducto("FR73INDDOSISPESO").Value & ","
              End If
              If IsNull(rstProducto("FR73INDDOSISSUPCORP").Value) Then
                strinsert = strinsert & "NULL,"
              Else
                strinsert = strinsert & rstProducto("FR73INDDOSISSUPCORP").Value & ","
              End If
              If IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
              End If
              If IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA").Value & "',"
              End If
              If IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR11VOLTOTAL
                strinsert = strinsert & "null,null,"
              Else
                vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & "," & vntAux & ","
              End If
              If IsNull(rstProducto("FR34CODVIA").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
              End If
              If IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FRH7CODFORMFAR").Value & "',"
              End If
              If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & rstProducto("FR73CODPRODUCTO_DIL").Value & ","
              End If
              If IsNull(rstProducto("FR73INDESTUPEFACI")) Then
                strinsert = strinsert & "'" & mstrOperacion & "',"
              Else
                If rstProducto("FR73INDESTUPEFACI") = -1 Then
                  strinsert = strinsert & "'E',"
                Else
                  strinsert = strinsert & "'" & mstrOperacion & "',"
                End If
              End If
              If IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
              End If
              If IsNull(rstProducto("FR73VOLINFIV").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & rstProducto("FR73VOLINFIV").Value & ","
              End If
              strinsert = strinsert & "1,1," 'FR11CANTIDAD,FR11CANTDISP
              If IsNull(rstProducto("FR73TIEMINF").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
              End If
              If IsNull(rstProducto("FR73REFERENCIA").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FR73REFERENCIA").Value & "',"
              End If
              If IsNull(rstProducto("FRH9UBICACION").Value) Then
                strinsert = strinsert & "null,"
              Else
                strinsert = strinsert & "'" & rstProducto("FRH9UBICACION").Value & "',"
              End If
              If IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                strinsert = strinsert & "null,"
              Else
                vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              End If
              strinsert = strinsert & "'Diario')" 'FRH5CODPERIODICIDAD
            Else 'IV
              strinsert = "INSERT INTO FR1100"
              strinsert = strinsert & " (FR75CODPROTOCOLO,FR11NUMLINEA,"
              strinsert = strinsert & "FR73CODPRODUCTO_DIL,"
              strinsert = strinsert & "FR11VOLTOTAL,FR11CANTIDADDIL,FR11TIEMMININF,"
              strinsert = strinsert & "FR11OPERACION,"
              strinsert = strinsert & "FR11CANTIDAD,FR11CANTDISP,FRG4CODFRECUENCIA,"
              strinsert = strinsert & "FR34CODVIA,FR11INDVIAOPC,FR11VELPERFUSION,"
              strinsert = strinsert & "FRH5CODPERIODICIDAD"
              strinsert = strinsert & ") VALUES " & _
                        "(" & _
                        txtText1(0).Text & "," & _
                        linea & "," & _
                        gintprodbuscado(v, 0) & ","
              'If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
              '  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLINFIV").Value, ",", ".", 1)
              '  strinsert = strinsert & vntAux & "," & vntAux & ","
              '  VolTotal = CCur(rstProducto("FR73VOLINFIV").Value)
              'Else
              '  VolTotal = 0
              '  strinsert = strinsert & "NULL," & "NULL,"
              'End If
              If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & "," & vntAux & ","
                VolTotal = CCur(rstProducto("FR73VOLUMEN").Value)
              Else
                VolTotal = 0
                strinsert = strinsert & "NULL," & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
                strinsert = strinsert & rstProducto("FR73TIEMINF").Value & ","
              Else
                strinsert = strinsert & "NULL,"
              End If
              strinsert = strinsert & "'F',1,1," 'FR11CANTIDAD,FR11CANTDISP
              
              If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                strinsert = strinsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR34CODVIA").Value) Then
                strinsert = strinsert & "'" & rstProducto("FR34CODVIA").Value & "',"
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                strinsert = strinsert & rstProducto("FR73INDVIAOPCION").Value & ","
              Else
                strinsert = strinsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                strinsert = strinsert & vntAux & ","
              Else
                If txtText1(43).Text = "" Then
                  TiemMinInf = 0
                Else
                  TiemMinInf = CCur(txtText1(43).Text) / 60
                End If
                If TiemMinInf <> 0 Then
                  VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                  vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                  strinsert = strinsert & vntAux & ","
                Else
                  VolPerf = 0
                  strinsert = strinsert & VolPerf & ","
                End If
              End If
              strinsert = strinsert & "'Diario')"
            End If
            
            objApp.rdoConnect.Execute strinsert, 64
            objApp.rdoConnect.Execute "Commit", 64
            
            If mstrOperacion = "P" Then
              strupdate = "UPDATE FR1100 SET FR11INDDISPPRN=-1"
              strupdate = strupdate & " WHERE FR75CODPROTOCOLO=" & txtText1(0).Text
              strupdate = strupdate & " AND FR11NUMLINEA=" & linea
              objApp.rdoConnect.Execute strupdate, 64
              objApp.rdoConnect.Execute "Commit", 64
            End If
            
            rstlinea.Close
            Set rstlinea = Nothing
            rstProducto.Close
            Set rstProducto = Nothing
          End If
        Next v
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
        If mstrOperacion = "P" Then
          MsgBox "Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n.", vbInformation, "Aviso"
          Call objWinInfo.CtrlDataChange
          txtText1(39).SetFocus
        End If
      Else
        '???????????????????????????-
        For v = 0 To gintprodtotal - 1
          strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                    gintprodbuscado(v, 0)
          Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
          
          strupdate = "UPDATE FR1100 SET "
          strupdate = strupdate & "FR73CODPRODUCTO=" & gintprodbuscado(v, 0)
          If Not IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
            strupdate = strupdate & ",FRH7CODFORMFAR='" & rstProducto("FRH7CODFORMFAR").Value & "'"
          End If
          If IsNull(rstProducto("FR73DOSIS").Value) Then
            strupdate = strupdate & ",FR11DOSIS=1"
          Else
            vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
            strupdate = strupdate & ",FR11DOSIS=" & vntAux
          End If
          If Not IsNull(rstProducto("FR73INDDOSISPESO").Value) Then
            strupdate = strupdate & ",FR11DOSISPESO=" & rstProducto("FR73INDDOSISPESO").Value
          End If
          If Not IsNull(rstProducto("FR73INDDOSISSUPCORP").Value) Then
            strupdate = strupdate & ",FR11DOSISSUP=" & rstProducto("FR73INDDOSISSUPCORP").Value
          End If
          If Not IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
            strupdate = strupdate & ",FR93CODUNIMEDIDA='" & rstProducto("FR93CODUNIMEDIDA").Value & "'"
          End If
          If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR11VOLTOTAL?
            vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
            strupdate = strupdate & ",FR11VOLUMEN=" & vntAux
          End If
          If Not IsNull(rstProducto("FR73REFERENCIA").Value) Then
            strupdate = strupdate & ",FR11REFERENCIA='" & rstProducto("FR73REFERENCIA").Value & "'"
          End If
          If Not IsNull(rstProducto("FRH9UBICACION").Value) Then
            strupdate = strupdate & ",FR11UBICACION='" & rstProducto("FRH9UBICACION").Value & "'"
          End If
          
          VolTotal = 0
          If IsNull(rstProducto("FR73VOLUMEN").Value) Then
            Vol1 = 0
          Else
            Vol1 = CCur(rstProducto("FR73VOLUMEN").Value)
          End If
          If txtText1(42).Text = "" Then
            Vol2 = 0
          Else
            Vol2 = CCur(txtText1(42).Text)
          End If
          If txtText1(47).Text = "" Then
            Vol3 = 0
          Else
            Vol3 = CCur(txtText1(47).Text)
          End If
          VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
          vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
          strupdate = strupdate & ",FR11VOLTOTAL=" & vntAux
          If txtText1(43).Text = "" Then
            TiemMinInf = 0
          Else
            TiemMinInf = CCur(txtText1(43).Text) / 60
          End If
          If TiemMinInf <> 0 Then
            VolPerf = Format(VolTotal / TiemMinInf, "0.00")
          Else
            VolPerf = 0
          End If
          vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
          strupdate = strupdate & ",FR11VELPERFUSION=" & vntAux
          
          strupdate = strupdate & " WHERE FR75CODPROTOCOLO=" & txtText1(0).Text
          strupdate = strupdate & " AND FR11NUMLINEA=" & txtText1(45).Text
          
          objApp.rdoConnect.Execute strupdate, 64
          
          rstProducto.Close
          Set rstProducto = Nothing
        Next v
        Call objWinInfo.DataRefresh
        '???????????????????????????-
      End If
    End If
    gintprodtotal = 0
  End If

gstrLlamadorProd = ""
Me.Enabled = True
txtText1(35).SetFocus
cmdbuscarprod.Enabled = True
blnBoton = False

End Sub

Private Sub cmdbuscarprot_Click()
Dim strinsert As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If
If Campos_Obligatorios = True Then
  Exit Sub
ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End If


blnBoton = True
cmdbuscarprot.Enabled = False
Me.Enabled = False


  If txtText1(0).Text <> "" Then
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    glngcodpeticion = txtText1(0).Text
    Call objsecurity.LaunchProcess("FR0185")
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    If gintprodtotal > 0 Then
      strlinea = "SELECT MAX(FR11NUMLINEA) FROM FR1100 WHERE FR75CODPROTOCOLO=" & _
                txtText1(0).Text
      Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
      If IsNull(rstlinea.rdoColumns(0).Value) Then
        linea = 0
      Else
        linea = rstlinea.rdoColumns(0).Value
      End If
      rstlinea.Close
      Set rstlinea = Nothing
      strinsert = "INSERT INTO FR1100 ("
      strinsert = strinsert & "FR75CODPROTOCOLO,"
      strinsert = strinsert & "FR11NUMLINEA,"
      strinsert = strinsert & "FR73CODPRODUCTO,"
      strinsert = strinsert & "FR11DOSIS,"
      strinsert = strinsert & "FR93CODUNIMEDIDA,"
      strinsert = strinsert & "FR11VOLUMEN,"
      strinsert = strinsert & "FR11TIEMINFMIN,"
      strinsert = strinsert & "FR34CODVIA,"
      strinsert = strinsert & "FRG4CODFRECUENCIA,"
      strinsert = strinsert & "FR11INDDISPPRN,"
      strinsert = strinsert & "FR11INDCOMIENINMED,"
      strinsert = strinsert & "FRH5CODPERIODICIDAD,"
      strinsert = strinsert & "FR11INDBLOQUEADA,"
      strinsert = strinsert & "FR11FECBLOQUEO,"
      strinsert = strinsert & "SG02CODPERSBLOQ,"
      strinsert = strinsert & "FR11INDSN,"
      strinsert = strinsert & "FR11INDESTOM,"
      strinsert = strinsert & "FRH7CODFORMFAR,"
      strinsert = strinsert & "FR11OPERACION,"
      strinsert = strinsert & "FR11CANTIDAD,"
      strinsert = strinsert & "FR11FECINICIO,"
      strinsert = strinsert & "FR11HORAINICIO,"
      strinsert = strinsert & "FR11FECFIN,"
      strinsert = strinsert & "FR11HORAFIN,"
      strinsert = strinsert & "FR11INDVIAOPC,"
      strinsert = strinsert & "FR73CODPRODUCTO_DIL,"
      strinsert = strinsert & "FR11CANTIDADDIL,"
      strinsert = strinsert & "FR11TIEMMININF,"
      strinsert = strinsert & "FR11REFERENCIA,"
      strinsert = strinsert & "FR11CANTDISP,"
      strinsert = strinsert & "FR11UBICACION,"
      strinsert = strinsert & "FR11INDPERF,"
      strinsert = strinsert & "FR11INSTRADMIN,"
      strinsert = strinsert & "FR11VELPERFUSION,"
      strinsert = strinsert & "FR11VOLTOTAL,"
      strinsert = strinsert & "FR73CODPRODUCTO_2,"
      strinsert = strinsert & "FRH7CODFORMFAR_2,"
      strinsert = strinsert & "FR11DOSIS_2,"
      strinsert = strinsert & "FR93CODUNIMEDIDA_2,"
      strinsert = strinsert & "FR11DESPRODUCTO,"
      strinsert = strinsert & "FR11PATHFORMAG,"
      strinsert = strinsert & "FR11DIASADM,"
      strinsert = strinsert & "FR11NUMDIAS,"
      strinsert = strinsert & "FR11DOSISPESO,"
      strinsert = strinsert & "FR11DOSISSUP,"
      strinsert = strinsert & "FR11VOLUMEN_2"
      strinsert = strinsert & ")"
      strinsert = strinsert & " SELECT "
      strinsert = strinsert & txtText1(0).Text
      strinsert = strinsert & ",FR11NUMLINEA + " & linea
      strinsert = strinsert & ",FR73CODPRODUCTO,FR11DOSIS,FR93CODUNIMEDIDA,"
      strinsert = strinsert & "FR11VOLUMEN,FR11TIEMINFMIN,FR34CODVIA,FRG4CODFRECUENCIA,FR11INDDISPPRN,FR11INDCOMIENINMED,"
      strinsert = strinsert & "FRH5CODPERIODICIDAD,FR11INDBLOQUEADA,FR11FECBLOQUEO,SG02CODPERSBLOQ,FR11INDSN,FR11INDESTOM,"
      strinsert = strinsert & "FRH7CODFORMFAR,FR11OPERACION,FR11CANTIDAD,FR11FECINICIO,FR11HORAINICIO,FR11FECFIN,FR11HORAFIN,"
      strinsert = strinsert & "FR11INDVIAOPC,FR73CODPRODUCTO_DIL,FR11CANTIDADDIL,FR11TIEMMININF,FR11REFERENCIA,FR11CANTDISP,"
      strinsert = strinsert & "FR11UBICACION,FR11INDPERF,FR11INSTRADMIN,FR11VELPERFUSION,FR11VOLTOTAL,FR73CODPRODUCTO_2,"
      strinsert = strinsert & "FRH7CODFORMFAR_2,FR11DOSIS_2,FR93CODUNIMEDIDA_2,FR11DESPRODUCTO,FR11PATHFORMAG,FR11DIASADM,"
      strinsert = strinsert & "FR11NUMDIAS,FR11DOSISPESO,FR11DOSISSUP,FR11VOLUMEN_2 "
      strinsert = strinsert & "FROM FR1100 "
      strinsert = strinsert & "WHERE FR75CODPROTOCOLO="
      strinsert = strinsert & gintprodbuscado(0, 6)
      
      objApp.rdoConnect.Execute strinsert, 64
      objApp.rdoConnect.Execute "Commit", 64
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
    End If
    gintprodtotal = 0
  End If

Me.Enabled = True
txtText1(35).SetFocus
cmdbuscarprot.Enabled = True
blnBoton = False

End Sub


Private Sub Form_Activate()

If ginteventloadRedOM = 0 Then
  
  Me.Enabled = False 'Para que no toquen en ninguna parte de la
                     'pantalla hasta que este cargada

  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  Call presentacion_pantalla
  blnDetalle = False

  grdDBGrid1(2).Columns("Protocolo").Position = 1
  grdDBGrid1(2).Columns("Indicaciones del Protocolo").Position = 2
  grdDBGrid1(2).Columns("Dpto. Propietario").Position = 3
  grdDBGrid1(2).Columns("Priv.").Position = 4
  grdDBGrid1(2).Columns("Fecha Inicio Vigencia").Position = 5
  grdDBGrid1(2).Columns("Fecha Fin Vigencia").Position = 6
  grdDBGrid1(2).Columns("C�digo Protocolo").Visible = False
  grdDBGrid1(2).Columns("C�d. Dpto. Propietario").Visible = False
  
  grdDBGrid1(6).Columns("Medicamento").Position = 1
  'grdDBGrid1(6).Columns("Referencia").Position = 2
  grdDBGrid1(6).Columns("FF").Position = 2
  grdDBGrid1(6).Columns("Dosis").Position = 3
  grdDBGrid1(6).Columns("UM").Position = 4
  grdDBGrid1(6).Columns("Volum.").Position = 5
  grdDBGrid1(6).Columns("Cantidad").Position = 6
  grdDBGrid1(6).Columns("Frec.").Position = 7
  grdDBGrid1(6).Columns("V�a").Position = 8
  grdDBGrid1(6).Columns("Period.").Position = 9
  grdDBGrid1(6).Columns("Inicio").Position = 10
  grdDBGrid1(6).Columns("H.I.").Position = 11
  grdDBGrid1(6).Columns("Descripci�n Producto Diluir").Position = 12
  grdDBGrid1(6).Columns("Vol.Dil.").Position = 13
  grdDBGrid1(6).Columns("T.Inf.(min)").Position = 14
  grdDBGrid1(6).Columns("Inst.Admin.").Position = 15
  grdDBGrid1(6).Columns("Fin").Position = 16
  grdDBGrid1(6).Columns("H.F.").Position = 17
  grdDBGrid1(6).Columns("Perfusi�n").Position = 18
  grdDBGrid1(6).Columns("Vol.Total").Position = 19
  grdDBGrid1(6).Columns("STAT?").Position = 20
  grdDBGrid1(6).Columns("PERF?").Position = 21
  grdDBGrid1(6).Columns("Medicamento 2").Position = 22
  grdDBGrid1(6).Columns("FF2").Position = 23
  grdDBGrid1(6).Columns("Dosis2").Position = 24
  grdDBGrid1(6).Columns("UM2").Position = 25
  grdDBGrid1(6).Columns("vol.Prod.2").Position = 26
  grdDBGrid1(6).Columns("Medic.No Form.").Position = 27
  grdDBGrid1(6).Columns("Arch.Inf.F.M.").Position = 28
  
  grdDBGrid1(6).Columns("C�digo Medicamento").Visible = False
  grdDBGrid1(6).Columns("Medicamento").Width = 2235
  'grdDBGrid1(6).Columns("Descripci�n Medicamento").Caption = "Medicamento"
  grdDBGrid1(6).Columns("Medic.No Form.").Width = 2235
  grdDBGrid1(6).Columns("Arch.Inf.F.M.").Width = 2235
  'grdDBGrid1(6).Columns("Medic. No Formulario").Caption = "Medic.No Form."
  grdDBGrid1(6).Columns("FF").Width = 390
  grdDBGrid1(6).Columns("Dosis").Width = 945
  grdDBGrid1(6).Columns("UM").Width = 510
  grdDBGrid1(6).Columns("Cantidad").Width = 915
  grdDBGrid1(6).Columns("Frec.").Width = 929
  'grdDBGrid1(6).Columns("Cod.Frecuencia").Caption = "Frec."
  grdDBGrid1(6).Columns("V�a").Width = 600
  'grdDBGrid1(6).Columns("C�digo V�a").Caption = "V�a"
  grdDBGrid1(6).Columns("Period.").Width = 1065
  'grdDBGrid1(6).Columns("Period.").Caption = "Period."
  grdDBGrid1(6).Columns("Inicio").Width = 1095
  'grdDBGrid1(6).Columns("Fecha Inicio Vigencia").Caption = "Inicio"
  grdDBGrid1(6).Columns("H.I.").Width = 540
  'grdDBGrid1(6).Columns("Hora Inicio").Caption = "H.I."
  grdDBGrid1(6).Columns("C�digo Producto diluir").Visible = False
  grdDBGrid1(6).Columns("Descripci�n Producto Diluir").Width = 2160
  grdDBGrid1(6).Columns("Vol.Dil.").Width = 825
  'grdDBGrid1(6).Columns("Volumen(ml)").Caption = "Vol.Dil."
  grdDBGrid1(6).Columns("Inst.Admin.").Width = 2500
  'grdDBGrid1(6).Columns("Instrucciones para administraci�n").Width = 2500
  'grdDBGrid1(6).Columns("Instrucciones para administraci�n").Caption = "Inst.Admin."
  grdDBGrid1(6).Columns("C�digo Medicamento 2").Visible = False
  grdDBGrid1(6).Columns("Medicamento 2").Width = 1815
  'grdDBGrid1(6).Columns("Descripci�n Medicamento 2").Caption = "Medicamento 2"
  grdDBGrid1(6).Columns("FF2").Width = 390
  grdDBGrid1(6).Columns("Dosis2").Width = 1289
  grdDBGrid1(6).Columns("UM2").Width = 645
  grdDBGrid1(6).Columns("Fin").Width = 1095
  'grdDBGrid1(6).Columns("Fecha Fin").Caption = "Fin"
  grdDBGrid1(6).Columns("H.F.").Width = 540
  'grdDBGrid1(6).Columns("Hora Fin").Caption = "H.F."
  grdDBGrid1(6).Columns("Perfusi�n").Width = 1170
  grdDBGrid1(6).Columns("Vol.Total").Width = 1170
  grdDBGrid1(6).Columns("STAT?").Width = 525
  grdDBGrid1(6).Columns("PERF?").Width = 645
  grdDBGrid1(6).Columns("C�digo Petici�n").Visible = False
  grdDBGrid1(6).Columns("Num.Linea").Visible = False
  grdDBGrid1(6).Columns("Cambiar V�a?").Visible = False
  grdDBGrid1(6).Columns("Prn?").Visible = False
  grdDBGrid1(6).Columns("T.Inf.(min)").Width = 825
  grdDBGrid1(6).Columns("Operaci�n").Visible = False
  grdDBGrid1(6).Columns("Ubicaci�n").Visible = False
  grdDBGrid1(6).Columns("Referencia").Width = 1100
  grdDBGrid1(6).Columns("Referencia").Visible = False
  grdDBGrid1(6).Columns("Volum.").Width = 825
  grdDBGrid1(6).Columns("Cant.Dispens.").Visible = False

  ginteventloadRedOM = 1
  Me.Enabled = True
  
  If tabTab1(1).Tab = 0 Then
    blnDetalle = True
  Else
    blnDetalle = False
  End If
  Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
  txtText1(0).SetFocus
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objDetailInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm
Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Protocolos"
      
    .strTable = "FR7500"
    
    .blnAskPrimary = False
    .intAllowance = cwAllowReadOnly
    
    If gintbuscargruprot = 1 Then
        .strWhere = "FR75CODPROTOCOLO=" & frmBusGrpProt.grdDBGrid1(0).Columns(3).Value
    Else
      If gstrLlamadorProd = "frmPedirPrn" Then
        .strWhere = "((FR75INDPRIVADO = 0) Or " & _
                    "(FR75INDPRIVADO = -1 And " & _
                       "AD02CODDPTO =" & frmPedirPRN.txtText1(16).Text & _
                    " AND FR75FECINIVIG < (SELECT SYSDATE FROM DUAL)" & _
                    " AND ((FR75FECFINVIG IS NULL) OR (FR75FECFINVIG > (SELECT SYSDATE FROM DUAL)))))"
      Else
        If gstrLlamadorProd = "frmPedirServEspeciales" Then
          .strWhere = "((FR75INDPRIVADO = 0) Or " & _
                    "(FR75INDPRIVADO = -1 And " & _
                       "AD02CODDPTO =" & frmPedirServEspeciales.txtText1(16).Text & _
                    " AND FR75FECINIVIG < (SELECT SYSDATE FROM DUAL)" & _
                    " AND ((FR75FECFINVIG IS NULL) OR (FR75FECFINVIG > (SELECT SYSDATE FROM DUAL)))))"
        Else
            .strWhere = "((FR75INDPRIVADO = 0) Or " & _
                        "(FR75INDPRIVADO = -1 And " & _
                           "AD02CODDPTO  IN (SELECT AD02CODDPTO FROM AD0300 " & _
                                             "WHERE SG02COD ='" & objsecurity.strUser & "')))" & _
                        " AND FR75FECINIVIG < (SELECT SYSDATE FROM DUAL)" & _
                        " AND ((FR75FECFINVIG IS NULL) OR (FR75FECFINVIG > (SELECT SYSDATE FROM DUAL)))"
        End If
      End If
    End If
    
    Call .FormAddOrderField("FR75DESPROTOCOLO", cwAscending)
   
    Call .objPrinter.Add("FR0281", "Protocolos Terap�uticos")
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Protocolos")
    Call .FormAddFilterWhere(strKey, "FR75CODPROTOCOLO", "C�d.Protocolo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR75DESPROTOCOLO", "Descripci�n de protocolo", cwString)
    Call .FormAddFilterWhere(strKey, "FR75INDPRIVADO", "Privado?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "Departamento Propietario", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR75FECINIVIG", "Inicio Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR75FECFINVIG", "Fin Vigencia", cwDate)
    Call .FormAddFilterOrder(strKey, "FR75DESPROTOCOLO", "Descripci�n de protocolo")

  End With

  With objDetailInfo
    .strName = "Detalle Protocolo"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = fraframe1(1)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(6)
    .strTable = "FR1100" 'Perfil FTP
    .blnAskPrimary = False
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("FR11NUMLINEA", cwAscending)
    Call .FormAddRelation("FR75CODPROTOCOLO", txtText1(0))
    
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Detalle Protocolo")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR11CANTIDAD", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "Forma Farmace�tica", cwString)
    Call .FormAddFilterWhere(strKey, "FR11DOSIS", "Dosis", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "Unidad Medida", cwString)
    Call .FormAddFilterWhere(strKey, "frg4codfrecuencia", "Frecuencia", cwString)
    Call .FormAddFilterWhere(strKey, "fr34codvia", "V�a", cwString)
    Call .FormAddFilterWhere(strKey, "frh5codperiodicidad", "Periodicidad", cwString)
    Call .FormAddFilterWhere(strKey, "FR11fecinicio", "Fecha Inicio", cwDate)
    Call .FormAddFilterWhere(strKey, "FR11horainicio", "Hora Inicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR11fecfin", "Fecha Fin", cwDate)
    Call .FormAddFilterWhere(strKey, "FR11horafin", "Hora Fin", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO_DIL", "Cod.Producto Diluy.", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR11indcomieninmed", "Comienzo Inmediato?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR11indsn", "S.N?", cwBoolean)
  End With

  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
  
   
    
    Call .FormCreateInfo(objMasterInfo)
    
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    '.CtrlGetInfo(txtText1(8)).blnInFind = True
    '.CtrlGetInfo(txtText1(10)).blnInFind = True
    '.CtrlGetInfo(txtText1(16)).blnInFind = True
    '.CtrlGetInfo(txtText1(20)).blnInFind = True
    '.CtrlGetInfo(txtText1(22)).blnInFind = True
    '.CtrlGetInfo(txtText1(25)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    '.CtrlGetInfo(chkCheck1(2)).blnInFind = True
    '.CtrlGetInfo(chkCheck1(7)).blnInFind = True
    '.CtrlGetInfo(chkCheck1(12)).blnInFind = True
    '.CtrlGetInfo(chkCheck1(15)).blnInFind = True
    
    'productos
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(35)).blnInFind = True
    .CtrlGetInfo(txtText1(30)).blnInFind = True
    .CtrlGetInfo(txtText1(31)).blnInFind = True
    .CtrlGetInfo(txtText1(38)).blnInFind = True
    .CtrlGetInfo(txtText1(28)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(3)).blnInFind = True
    '.CtrlGetInfo(dtcDateCombo1(3)).blnInFind = True
    '.CtrlGetInfo(txtText1(37)).blnInFind = True
    .CtrlGetInfo(txtText1(41)).blnInFind = True
    .CtrlGetInfo(txtText1(42)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(4)).blnInFind = True
    '.CtrlGetInfo(dtcDateCombo1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(42)).blnInFind = True
    '.CtrlGetInfo(txtText1(36)).blnInFind = True
    
    '.CtrlGetInfo(Text1).blnNegotiated = False
    '.CtrlGetInfo(txtText1(29)).blnNegotiated = False
    .CtrlGetInfo(txtHoras).blnNegotiated = False
    .CtrlGetInfo(txtMinutos).blnNegotiated = False
    
    '.CtrlGetInfo(txtText1(1)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(29)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(24)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(14)).blnReadOnly = True
    
    '.CtrlGetInfo(txtOperacion).blnNegotiated = False
    '.CtrlGetInfo(Text1).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CODDPTO", "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "AD02DESDPTO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "CI30CODSEXO", "SELECT * FROM CI3000 WHERE CI30CODSEXO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(6), "CI30DESSEXO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "SG02APE1")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(24)), "SG02COD_FEN", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(19), "SG02APE1")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(21), "SG02NUMCOLEGIADO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(15), "SG02APE1")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(11), "FR91DESURGENCIA")
    
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(26), "FR26DESESTADOPET")
    
    '**********************************************************************************
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(27)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(27)), txtText1(13), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(40)), "FR73CODPRODUCTO_DIL", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(40)), txtText1(41), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(52)), "FR73CODPRODUCTO_2", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(52)), txtText1(51), "FR73DESPRODUCTO")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(52)), txtText1(47), "FR73VOLUMEN")
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(47)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(8)).blnForeign = True
    '.CtrlGetInfo(txtText1(16)).blnForeign = True
    '.CtrlGetInfo(txtText1(22)).blnForeign = True
    '.CtrlGetInfo(txtText1(25)).blnForeign = True
    
    .CtrlGetInfo(txtText1(27)).blnForeign = True
    .CtrlGetInfo(txtText1(40)).blnForeign = True
    .CtrlGetInfo(txtText1(31)).blnForeign = True
    '.CtrlGetInfo(txtText1(81)).blnForeign = True
    '.CtrlGetInfo(txtText1(80)).blnForeign = True
    .CtrlGetInfo(txtText1(35)).blnForeign = True
     
    '.CtrlGetInfo(txtText1(10)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(20)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(24)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(25)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    '.CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(38)).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(44)).blnReadOnly = True
    
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT FR34CODVIA,FR34DESVIA FROM FR3400 ORDER BY FR34CODVIA"
    
    .CtrlGetInfo(cboDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(0)).strSQL = "SELECT FRG4CODFRECUENCIA,FRG4DESFRECUENCIA FROM FRG400 ORDER BY FRG4CODFRECUENCIA"
    
    .CtrlGetInfo(cboDBCombo1(3)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT FRH5CODPERIODICIDAD,FRH5DESPERIODICIDAD FROM FRH500 ORDER BY FRH5CODPERIODICIDAD"
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

  ginteventloadRedOM = 0
  Frame2.Visible = False
  mstrOperacion = "/"
  blnObligatorio = False
  blnBoton = False
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  gintfirmarOM = 0
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
Dim v As Integer

  If Index = 6 Then
  v = 43
  Select Case grdDBGrid1(6).Columns("Operaci�n").Value
  Case "/"  'Medicamento
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "Medicamentos"
    Next i
  Case "M"  'Mezcla IV 2M
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "MezclaIV2M"
    Next i
  Case "P"  'PRN en OM
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "PRNenOM"
    Next i
  Case "F" 'Fluidoterapia
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "Fluidoterapia"
    Next i
  Case "E" 'Estupefaciente
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "Estupefacientes"
    Next i
  Case "L" 'Form.Magistral
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "FormMagistral"
    Next i
  End Select
  End If
  
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
 If strFormName = "Protocolos" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�d. Dpto. Propietario"

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto. Propietario"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle Protocolo" And strCtrl = "txtText1(27)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
 
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Medicamento"
 
 
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
 
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(27), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle Protocolo" And strCtrl = "txtText1(40)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strWhere = " WHERE FR73INDINFIV=-1"
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Medicamento"
 
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
    
     Set objField = .AddField("FR73VOLUMEN")
     objField.strSmallDesc = "Volumen"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     Set objField = .AddField("FR73VOLINFIV")
     objField.strSmallDesc = "Volumen Infusi�n IV"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(40), .cllValues("FR73CODPRODUCTO"))
      Call objWinInfo.CtrlSet(txtText1(42), .cllValues("FR73VOLINFIV"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle Protocolo" And strCtrl = "txtText1(31)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad de Medida"
     
     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(31), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "Detalle Protocolo" And strCtrl = "grdDBGrid1(0).Forma Farmace�tica" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"

     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"
     
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(18), .cllValues("FRH7CODFORMFAR"))
     End If
   End With
   Set objSearch = Nothing
 End If
 If strFormName = "Detalle Protocolo" And strCtrl = "txtText1(35)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"
 
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"
 
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"
 
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(35), .cllValues("FRH7CODFORMFAR"))
     End If
    End With
    Set objSearch = Nothing
 End If

End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)

Me.Enabled = False

  If strFormName <> "Detalle Protocolo" Then
    If tabTab1(1).Tab <> 1 Then
      tabTab1(1).Tab = 1
      objWinInfo.DataMoveFirst
      Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
    End If
  End If

Me.Enabled = True

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'txtOperacion.Text = cboDBCombo1(4).Text
'On Error GoTo Error
    
  If objWinInfo.objWinActiveForm.strName <> "Protocolos" Then
    If objWinInfo.intWinStatus = 1 Then
      If grdDBGrid1(6).Rows > 0 Then
        Select Case grdDBGrid1(6).Columns("Operaci�n").Value
        Case "/"  'Medicamento
          tab2.Tab = 0
        Case "M"  'Mezcla IV 2M
          tab2.Tab = 1
        Case "P"  'PRN en OM
          tab2.Tab = 2
        Case "F" 'Fluidoterapia
          tab2.Tab = 3
        Case "E" 'Estupefaciente
          tab2.Tab = 4
        Case "L" 'Form.Magistral
          tab2.Tab = 5
        End Select
        Call presentacion_pantalla
      End If
    End If
  End If

'Error:
'  Exit Sub

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    
  If strFormName = "Detalle Protocolo" And cboDBCombo1(3).Text = "Agenda" Then
   ' Call Periodicidad
  End If
  intCambioAlgo = 0

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim mensaje As String

  If objWinInfo.objWinActiveForm.strName <> "Protocolos" Then
    txtText1(58).Text = txtText1(28).Text
    Select Case tab2.Tab 'Campos Obligatorios
    Case 0
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) <> "Agenda" Then
        If txtText1(9).Text = "" Then
          mensaje = MsgBox("El campo Num.D�as para Periodicidad distinta de Agenda es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(9).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
    Case 1
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) <> "Agenda" Then
        If txtText1(9).Text = "" Then
          mensaje = MsgBox("El campo Num.D�as para Periodicidad distinta de Agenda es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(9).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If txtText1(52).Text <> "" Then
        If txtText1(48).Text = "" Then
          mensaje = MsgBox("El campo Forma Farmace�tica del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(48).SetFocus
          Exit Sub
        End If
        If txtText1(50).Text = "" Then
          mensaje = MsgBox("El campo Dosis del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(50).SetFocus
          Exit Sub
        End If
        If txtText1(49).Text = "" Then
          mensaje = MsgBox("El campo Unidad de Medida del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(49).SetFocus
          Exit Sub
        End If
      End If
    Case 2
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) <> "Agenda" Then
        If txtText1(9).Text = "" Then
          mensaje = MsgBox("El campo Num.D�as para Periodicidad distinta de Agenda es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(9).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If txtText1(39).Text = "" Then
        mensaje = MsgBox("Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(39).SetFocus
        Exit Sub
      End If
    Case 3
      If txtText1(40).Text = "" Then
        mensaje = MsgBox("El campo Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(42).SetFocus
        Exit Sub
      End If
      If txtText1(42).Text = "" Then
        mensaje = MsgBox("El campo Volumen de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(42).SetFocus
        Exit Sub
      End If
      If txtHoras.Text = "" Then
        mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtHoras.SetFocus
        Exit Sub
      End If
      If txtMinutos.Text = "" Then
        mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtMinutos.SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) <> "Agenda" Then
        If txtText1(9).Text = "" Then
          mensaje = MsgBox("El campo Num.D�as para Periodicidad distinta de Agenda es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(9).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(27).Text <> "" Then
        If txtText1(35).Text = "" Then
          mensaje = MsgBox("El campo Forma Farmace�tica del Medicamento es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(35).SetFocus
          Exit Sub
        End If
        If txtText1(30).Text = "" Then
          mensaje = MsgBox("El campo Dosis del Medicamento es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(30).SetFocus
          Exit Sub
        End If
        If txtText1(31).Text = "" Then
          mensaje = MsgBox("El campo Unidad de Medida del Medicamento es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(31).SetFocus
          Exit Sub
        End If
      End If
    Case 4
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Estupefaciente es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) <> "Agenda" Then
        If txtText1(9).Text = "" Then
          mensaje = MsgBox("El campo Num.D�as para Periodicidad distinta de Agenda es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(9).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
    Case 5
      If txtText1(55).Text = "" Then
        mensaje = MsgBox("El campo Descripci�n de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(55).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) <> "Agenda" Then
        If txtText1(9).Text = "" Then
          mensaje = MsgBox("El campo Num.D�as para Periodicidad distinta de Agenda es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(9).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(57).Text = "" Then
        mensaje = MsgBox("El campo Archivo Inf. de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(57).SetFocus
        Exit Sub
      End If
    End Select
  End If

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Protocolos" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, False))
    End If
    Set objPrinter = Nothing
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
  
  If Index = 1 Then
    If tabTab1(1).Tab = 0 Then
      blnDetalle = True
    Else
      blnDetalle = False
    End If
  End If
  
  If Index = 1 Then
    If objWinInfo.objWinActiveForm.strName <> "Detalle Protocolo" Then
      Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    End If
    If grdDBGrid1(6).Rows > 0 Then
      Select Case grdDBGrid1(6).Columns("Operaci�n").Value
      Case "/"  'Medicamento
        tab2.Tab = 0
      Case "M"  'Mezcla IV 2M
        tab2.Tab = 1
      Case "P"  'PRN en OM
        tab2.Tab = 2
      Case "F" 'Fluidoterapia
        tab2.Tab = 3
      Case "E" 'Estupefaciente
        tab2.Tab = 4
      Case "L" 'Form.Magistral
        tab2.Tab = 5
      End Select
    End If
    Call presentacion_pantalla
  End If
    
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstfec As rdoResultset
Dim strfec As String
Dim hora As Variant

  If btnButton.Index = 30 Then 'Salir
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  End If

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Protocolos" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        sqlstr = "SELECT FR75CODPROTOCOLO_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
        rsta.Close
        Set rsta = Nothing
        strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
        Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
        Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstfec.rdoColumns(0).Value)
        rstfec.Close
        Set rstfec = Nothing
        Exit Sub
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Detalle Protocolo" Then
      If blnBoton = False Then
        MsgBox "Para hacer un nuevo registro, ha de utilizar los buscadores de productos.", vbInformation
        Exit Sub
      End If
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FR11NUMLINEA) FROM FR1100 WHERE FR75CODPROTOCOLO=" & _
                  txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          linea = 1
        Else
          linea = rstlinea.rdoColumns(0).Value + 1
        End If
        txtText1(53).Text = mstrOperacion
        Select Case tab2.Tab
          Case 0
            mstrOperacion = "/" 'Medicamento
          Case 1
            mstrOperacion = "M" 'Mezcla IV 2M
          Case 2
            mstrOperacion = "P" 'PRN en OM
          Case 3
            mstrOperacion = "F" 'Fluidoterapia
          Case 4
            mstrOperacion = "E" 'Estupefaciente
          Case 5
            mstrOperacion = "L" 'Form.Magistral
        End Select
        txtText1(53).Text = mstrOperacion
        txtText1(45).Text = linea
        'SendKeys ("{TAB}")
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna petici�n", vbInformation
        Exit Sub
      End If
    Else
      If btnButton.Index = 4 And objWinInfo.objWinActiveForm.strName = "Detalle Protocolo" Then
        If txtText1(27).Text <> "" Then
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        Else
          'MsgBox "El Medicamento es obligatorio.", vbInformation
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Exit Sub
        End If
      Else
        If objWinInfo.objWinActiveForm.strName = "Detalle Protocolo" Then
          blnDetalle = False
          Select Case btnButton.Index
          Case 21 'Primero
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 22 'Anterior
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 23 'Siguiente
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 24 'Ultimo
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case Else 'Otro boton
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          End Select
          If tabTab1(1).Tab = 0 Then
            blnDetalle = True
          Else
            blnDetalle = False
          End If
        Else
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        End If
      End If
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 40 'Guardar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
      
    Select Case grdDBGrid1(6).Columns("Operaci�n").Value
    Case "/"  'Medicamento
      tab2.Tab = 0
    Case "M"  'Mezcla IV 2M
      tab2.Tab = 1
    Case "P"  'PRN en OM
      tab2.Tab = 2
    Case "F" 'Fluidoterapia
      tab2.Tab = 3
    Case "E" 'Estupefaciente
      tab2.Tab = 4
    Case "L" 'Form.Magistral
      tab2.Tab = 5
    End Select
    Call presentacion_pantalla

End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    If intIndex = 6 Then
      blnDetalle = False
    End If
    
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    If intIndex = 6 Then
      Call presentacion_pantalla
      If tabTab1(1).Tab = 0 Then
        blnDetalle = True
      Else
        blnDetalle = False
      End If
    End If
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  
  If intIndex = 0 Then
    If chkCheck1(0).Value = 1 Then
      objWinInfo.CtrlGetInfo(txtText1(2)).blnReadOnly = False
      txtText1(2).Locked = False
      txtText1(2).BackColor = vbWhite
    Else
      Call objWinInfo.CtrlSet(txtText1(2), "")
      txtText1(3).Text = ""
      objWinInfo.CtrlGetInfo(txtText1(2)).blnReadOnly = True
      txtText1(2).Locked = True
      txtText1(2).BackColor = txtText1(3).BackColor
    End If
  End If
  
  If intIndex = 6 Then
     If chkCheck1(6).Value <> 0 Then
        chkCheck1(7).Value = 0
     End If
  End If

  If intIndex = 7 Then
     If chkCheck1(7).Value <> 0 Then
        chkCheck1(6).Value = 0
     End If
  End If
  
  
  Select Case intIndex
    Case 1:
        If chkCheck1(1).Value = 1 Then
            chkCheck1(3).Value = 0
            chkCheck1(4).Value = 0
            chkCheck1(3).Enabled = False
            chkCheck1(4).Enabled = False
        Else
            chkCheck1(3).Enabled = True
            chkCheck1(4).Enabled = True
        End If
    Case 3:
        If chkCheck1(3).Value = 1 Then
            chkCheck1(1).Value = 0
            chkCheck1(4).Value = 0
            chkCheck1(1).Enabled = False
            chkCheck1(4).Enabled = False
        Else
            chkCheck1(1).Enabled = True
            chkCheck1(4).Enabled = True
        End If
    Case 4:
        If chkCheck1(4).Value = 1 Then
            chkCheck1(3).Value = 0
            chkCheck1(1).Value = 0
            chkCheck1(3).Enabled = False
            chkCheck1(1).Enabled = False
        Else
            chkCheck1(3).Enabled = True
            chkCheck1(1).Enabled = True
        End If
    Case 5:
        If chkCheck1(5).Value = 1 Then
            cboDBCombo1(1).Enabled = True
            objWinInfo.CtrlGetInfo(cboDBCombo1(1)).blnReadOnly = False
        Else
            cboDBCombo1(1).Enabled = False
            objWinInfo.CtrlGetInfo(cboDBCombo1(1)).blnReadOnly = True
        End If
  End Select

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtHoras_Change()
  
  If intCambioAlgo = 1 Then
    If txtHoras.Text <> "" And txtMinutos.Text <> "" Then
      If txtHoras.Text > 999 Then
        Beep
        txtHoras.Text = 100
      End If
      If IsNumeric(txtHoras.Text) And IsNumeric(txtMinutos.Text) Then
        'txtText1(43).Text = (txtHoras * 60) + txtMinutos
        Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60) + txtMinutos)
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    Else
      If txtHoras.Text <> "" Then
        If IsNumeric(txtHoras.Text) Then
          If txtHoras.Text > 999 Then
            Beep
            txtHoras.Text = 100
          End If
          Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60))
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      Else
        If IsNumeric(txtMinutos.Text) Then
          Call objWinInfo.CtrlSet(txtText1(43), txtMinutos)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If

End Sub

Private Sub txtHoras_KeyPress(KeyAscii As Integer)
    
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9")
    Case Else
      KeyAscii = 0
    End Select
    
    intCambioAlgo = 1
End Sub

Private Sub txtMinutos_Change()
  
  If intCambioAlgo = 1 Then
    If txtHoras.Text <> "" And txtMinutos.Text <> "" Then
      If txtMinutos.Text > 59 Then
        Beep
        txtMinutos.Text = 59
      End If
      If IsNumeric(txtHoras.Text) And IsNumeric(txtMinutos.Text) Then
        'txtText1(43).Text = (txtHoras * 60) + txtMinutos
        Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60) + txtMinutos)
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    Else
      If txtHoras.Text <> "" Then
        If IsNumeric(txtHoras.Text) Then
          Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60))
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      Else
        If IsNumeric(txtMinutos.Text) Then
          If txtMinutos.Text > 59 Then
            Beep
            txtMinutos.Text = 59
          End If
          Call objWinInfo.CtrlSet(txtText1(43), txtMinutos)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If

End Sub

Private Sub txtMinutos_KeyPress(KeyAscii As Integer)
    
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9")
    Case Else
      KeyAscii = 0
    End Select
    
    intCambioAlgo = 1
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  intBotonMasInf = intIndex
  
  If intIndex = 51 Then
    If tab2.Tab = 1 Then
      cmdMed2.Top = cmdbuscarprod.Top
      If txtText1(27).Text <> "" Then
        'cmdMed2.Visible = True
        cmdbuscarprod.Visible = False
      Else
        cmdMed2.Visible = False
        'cmdbuscarprod.Visible = True
      End If
      cmdSolDil.Visible = False
    End If
  ElseIf intIndex = 41 Then
    cmdSolDil.Top = cmdbuscarprod.Top
    If tab2.Tab <> 3 Then
      If txtText1(27).Text <> "" Then
        'cmdSolDil.Visible = True
        cmdbuscarprod.Visible = False
      Else
        cmdSolDil.Visible = False
        'cmdbuscarprod.Visible = True
      End If
    End If
    cmdMed2.Visible = False
    If tab2.Tab = 3 Then
      cmdSolDil.Caption = "Soluci�n Intravenosa"
    Else
      cmdSolDil.Caption = "Soluci�n para diluir"
    End If
  ElseIf intIndex = 55 Then
    If tab2.Tab <> 5 Then
      lblLabel1(16).Caption = "Medicamento No Formulario"
    Else
      lblLabel1(16).Caption = "F�rmula Magistral"
    End If
  ElseIf intIndex = 57 Then
    cmdbuscarprod.Caption = "Form. Magistrales"
  Else
    'lblLabel1(16).Caption = "Medicamento"
    cmdMed2.Visible = False
    If tab2.Tab = 3 And txtText1(40).Text = "" Then
      cmdSolDil.Caption = "Soluci�n Intravenosa"
      cmdSolDil.Top = cmdbuscarprod.Top
      'cmdSolDil.Visible = True
      cmdbuscarprod.Visible = False
    Else
      cmdSolDil.Visible = False
      'cmdbuscarprod.Visible = True
    End If
  End If
  
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)

  If Index = 32 Then
    If IsNumeric(txtText1(32).Text) Then
      objWinInfo.objWinActiveForm.blnChanged = True
      tlbToolbar1.Buttons(4).Enabled = True
    End If
  End If
  'ALTURA
  If Index = 33 Then
    If IsNumeric(txtText1(33).Text) Then
      objWinInfo.objWinActiveForm.blnChanged = True
      tlbToolbar1.Buttons(4).Enabled = True
    End If
  End If

End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
Dim rstDosis As rdoResultset
Dim strDosis As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant

  Call objWinInfo.CtrlLostFocus
  If intIndex = 32 Then
    If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
        intPeso = txtText1(32).Text
        intAltura = txtText1(33).Text
        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
        txtText1(29).Text = vntMasa
    End If
  End If
  If intIndex = 33 Then
    If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
        intPeso = txtText1(32).Text
        intAltura = txtText1(33).Text
        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
        txtText1(29).Text = vntMasa
    End If
  End If
'  If intIndex = 28 Then
'    If Trim(txtText1(27).Text) <> "" Then
'        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
'                  Trim(txtText1(27).Text)
'        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
'        If rstDosis.EOF = False Then
'            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(28).Text) Then
'                Call objWinInfo.CtrlSet(txtText1(30), txtText1(28).Text * rstDosis("fr73dosis").Value)
'            End If
'        End If
'    End If
'  End If
'  If intIndex = 30 Then
'    If Trim(txtText1(27).Text) <> "" Then
'        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
'                  Trim(txtText1(27).Text)
'        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
'        If rstDosis.EOF = False Then
'            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(30).Text) Then
'                Call objWinInfo.CtrlSet(txtText1(28), txtText1(30).Text / rstDosis("fr73dosis").Value)
'            End If
'        End If
'    End If
'  End If

  
  If intIndex = 28 Then
    If mstrOperacion <> "F" Then
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(28).Text) Then
              Call objWinInfo.CtrlSet(txtText1(30), txtText1(28).Text * rstDosis("fr73dosis").Value)
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                Call objWinInfo.CtrlSet(txtText1(38), txtText1(28).Text * rstDosis("fr73volumen").Value)
              End If
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    Else 'I.V.
      If Trim(txtText1(40).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(40).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73volumen").Value) = False And IsNumeric(txtText1(28).Text) Then
              Call objWinInfo.CtrlSet(txtText1(42), txtText1(28).Text * rstDosis("fr73volumen").Value)
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    End If
  End If
  
  If intIndex = 30 Then
    If mstrOperacion <> "F" Then
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(30).Text) Then
              If rstDosis("fr73dosis").Value <> 0 Then
                Call objWinInfo.CtrlSet(txtText1(28), txtText1(30).Text / rstDosis("fr73dosis").Value)
              End If
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                If rstDosis("fr73dosis").Value <> 0 Then
                  Call objWinInfo.CtrlSet(txtText1(38), (txtText1(30).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value)
                End If
              End If
            End If
        End If
      End If
    Else 'I.V.
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(30).Text) Then
              'Call objWinInfo.CtrlSet(txtText1(28), txtText1(30).Text / rstDosis("fr73dosis").Value)
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                If rstDosis("fr73dosis").Value <> 0 Then
                  Call objWinInfo.CtrlSet(txtText1(38), (txtText1(30).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value)
                End If
              End If
            End If
        End If
      End If
    End If
  End If

  If intIndex = 50 Then
    If mstrOperacion = "M" Then
      If Trim(txtText1(52).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(52).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(50).Text) And IsNull(rstDosis("fr73volumen").Value) = False Then
              If rstDosis("fr73dosis").Value <> 0 And IsNumeric(txtText1(50).Text) Then
                Call objWinInfo.CtrlSet(txtText1(47), (txtText1(50).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value)
              End If
            End If
        End If
      End If
    End If
  End If
  
  If intIndex = 42 Then
    If mstrOperacion <> "F" Then
    Else 'I.V.
      If Trim(txtText1(40).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(40).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73volumen").Value) = False And IsNumeric(txtText1(28).Text) Then
              If rstDosis("fr73volumen").Value <> 0 And IsNumeric(txtText1(42).Text) Then
                Call objWinInfo.CtrlSet(txtText1(28), txtText1(42).Text / rstDosis("fr73volumen").Value)
              End If
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    End If
  End If

End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim rstPotPeso As rdoResultset
Dim rstFactor As rdoResultset
Dim rstPotAlt As rdoResultset
Dim strPotPeso As String
Dim strPotAlt As String
Dim strFactor As String
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim rsta As rdoResultset
Dim stra As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency

  Call objWinInfo.CtrlDataChange
  
  If intIndex = 43 Then
    If IsNumeric(txtText1(43).Text) Then
      If intCambioAlgo = 0 Then
        txtHoras.Text = txtText1(43).Text \ 60
        txtMinutos.Text = txtText1(43).Text Mod 60
      End If
    Else
        txtHoras.Text = ""
        txtMinutos.Text = ""
    End If
  End If
  
  If intIndex = 27 Then
    If txtText1(27).Text = "999999999" Then
      lblLabel1(16).Caption = "Medicamento No Formulario"
      txtText1(55).Top = 240
      txtText1(55).Left = 0
      txtText1(55).Visible = True
    Else
      lblLabel1(16).Caption = "Medicamento"
      txtText1(55).Top = -1240
      txtText1(55).Left = 0
      txtText1(55).Visible = False
      If txtText1(35).Visible = True And txtText1(35).Enabled = True Then
        txtText1(35).SetFocus
      End If
    End If
  End If
   
  'PESO
  If intIndex = 32 Then
    If Not IsNumeric(txtText1(32).Text) Then
        txtText1(32).Text = ""
        txtText1(29).Text = ""
    Else
        If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
            strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
            Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
            strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
            Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
            strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
            Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
            intPeso = txtText1(32).Text
            intAltura = txtText1(33).Text
            vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
            vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
            vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
            txtText1(29).Text = vntMasa
            rstPotPeso.Close
            Set rstPotPeso = Nothing
            rstFactor.Close
            Set rstFactor = Nothing
            rstPotAlt.Close
            Set rstPotAlt = Nothing
        Else
            txtText1(29).Text = ""
        End If
    End If
  End If
  'ALTURA
  If intIndex = 33 Then
    If Not IsNumeric(txtText1(33).Text) Then
        txtText1(33).Text = ""
        txtText1(29).Text = ""
    Else
        If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
            strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
            Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
            strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
            Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
            strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
            Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
            intPeso = txtText1(32).Text
            intAltura = txtText1(33).Text
            vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
            vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
            vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
            txtText1(29).Text = vntMasa
            rstPotPeso.Close
            Set rstPotPeso = Nothing
            rstFactor.Close
            Set rstFactor = Nothing
            rstPotAlt.Close
            Set rstPotAlt = Nothing
        Else
            txtText1(29).Text = ""
        End If
    End If
  End If
  'HORA (frame Medicamentos)
  If intIndex = 36 Then
    If Not IsNumeric(txtText1(36).Text) Then
        txtText1(36).Text = ""
    Else
        If txtText1(36).Text > 24 Then
            txtText1(36).Text = ""
                Call MsgBox("Hora fuera de rango", vbCritical, "Aviso")
        End If
    End If
  End If

  Select Case intIndex
  Case 38, 42, 47, 43, 44, 82
    VolTotal = 0
    If txtText1(38).Text = "" Then
      Vol1 = 0
    Else
      Vol1 = CCur(txtText1(38).Text)
    End If
    If txtText1(42).Text = "" Then
      Vol2 = 0
    Else
      Vol2 = CCur(txtText1(42).Text)
    End If
    If txtText1(47).Text = "" Then
      Vol3 = 0
    Else
      Vol3 = CCur(txtText1(47).Text)
    End If
    VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
    If txtText1(43).Text = "" Then
      TiemMinInf = 0
    Else
      TiemMinInf = CCur(txtText1(43).Text) / 60
    End If
    If TiemMinInf <> 0 Then
      VolPerf = Format(VolTotal / TiemMinInf, "0.00")
    Else
      VolPerf = 0
    End If
    If txtText1(44).Text <> "" Then
      If VolTotal <> CCur(txtText1(44)) Then
        txtText1(44).Text = VolTotal
      End If
    Else
      If VolTotal <> 0 Then
        txtText1(44).Text = VolTotal
      End If
    End If
    If txtText1(82).Text <> "" Then
      If VolPerf <> CCur(txtText1(82)) Then
        txtText1(82).Text = VolPerf
      End If
    Else
      If VolPerf <> 0 Then
        txtText1(82).Text = VolPerf
      End If
    End If
  End Select


End Sub

Private Sub presentacion_pantalla()

  Select Case tab2.Tab
  Case 0, 2, 4
    lblLabel1(16).Top = 0 'Linea Medicamento....
    lblLabel1(16).Visible = True
    txtText1(13).Top = 240
    txtText1(13).Visible = True
    lblLabel1(37).Top = 0
    lblLabel1(37).Visible = True
    txtText1(35).Top = 240
    txtText1(35).Visible = True
    lblLabel1(19).Left = 6120
    lblLabel1(19).Top = 0
    lblLabel1(19).Visible = True
    txtText1(30).Left = 6120
    txtText1(30).Top = 240
    txtText1(30).Visible = True
    lblLabel1(21).Left = 7080
    lblLabel1(21).Top = 0
    lblLabel1(21).Visible = True
    txtText1(31).Left = 7080
    txtText1(31).Top = 240
    txtText1(31).Visible = True
    lblLabel1(49).Top = 0
    lblLabel1(49).Visible = True
    txtText1(38).Top = 240
    txtText1(38).Visible = True
    
    lblLabel1(17).Top = 600 'Linea Cantidad...
    lblLabel1(17).Visible = True
    txtText1(28).Top = 840
    txtText1(28).Visible = True
    lblLabel1(23).Left = 960
    lblLabel1(23).Top = 600
    lblLabel1(23).Visible = True
    cboDBCombo1(0).Left = 960
    cboDBCombo1(0).Top = 840
    cboDBCombo1(0).Visible = True
    lblLabel1(25).Left = 2760
    lblLabel1(25).Top = 600
    lblLabel1(25).Visible = True
    cboDBCombo1(1).Left = 2760
    cboDBCombo1(1).Top = 840
    cboDBCombo1(1).Visible = True
    lblLabel1(29).Left = 3600
    lblLabel1(29).Top = 600
    lblLabel1(29).Visible = True
    cboDBCombo1(3).Left = 3600
    cboDBCombo1(3).Top = 840
    cboDBCombo1(3).Visible = True
    'lblLabel1(33).Left = 5520
    'lblLabel1(33).Top = 600
    lblLabel1(33).Visible = False
    'dtcDateCombo1(3).Left = 5520
    'dtcDateCombo1(3).Top = 840
    dtcDateCombo1(3).Visible = False
    'lblLabel1(32).Left = 7440
    'lblLabel1(32).Top = 600
    lblLabel1(32).Visible = False
    'txtText1(37).Top = 7440
    'txtText1(37).Top = 840
    txtText1(37).Visible = False
    
    lblLabel1(34).Top = 1200 'Linea Sol.Dil....
    lblLabel1(34).Caption = "Soluci�n para Diluir"
    lblLabel1(34).Visible = True
    txtText1(41).Top = 1440
    txtText1(41).Visible = True
    lblLabel1(35).Top = 1200
    lblLabel1(35).Visible = True
    txtText1(42).Top = 1440
    txtText1(42).Visible = True
    lblLabel1(36).Top = 1200
    lblLabel1(36).Visible = True
    txtHoras.Top = 1440
    txtHoras.Visible = True
    txtMinutos.Top = 1440
    txtMinutos.Visible = True
    
    lblLabel1(50).Top = 1800 'Linea Instr.Admin. y Vel.Perf....
    lblLabel1(50).Visible = True
    txtText1(39).Top = 2040
    txtText1(39).Visible = True
    lblLabel1(52).Top = 1800
    lblLabel1(52).Visible = True
    txtText1(82).Top = 2040
    txtText1(82).Visible = True
    'lblLabel1(62).Top = 1800
    'lblLabel1(62).Visible = True
    'txtText1(81).Top = 2040
    'txtText1(81).Visible = True
    lblLabel1(77).Top = 2080
    lblLabel1(77).Visible = True
    'txtText1(80).Top = 2040
    'txtText1(80).Visible = True
    lblLabel1(51).Top = 1800
    lblLabel1(51).Visible = True
    txtText1(44).Top = 2040
    txtText1(44).Visible = True
    
    'lblLabel1(31).Left = 5400
    'lblLabel1(31).Top = 2400 'Linea Fecha fin...
    lblLabel1(31).Visible = False
    'dtcDateCombo1(4).Left = 5400
    'dtcDateCombo1(4).Top = 2640
    dtcDateCombo1(4).Visible = False
    'lblLabel1(30).Left = 7320
    'lblLabel1(30).Top = 2400
    lblLabel1(30).Visible = False
    'txtText1(36).Left = 7320
    'txtText1(36).Top = 2640
    txtText1(36).Visible = False
    
    lblLabel1(79).Top = 3000 'Linea Prod2....
    'lblLabel1(79).Visible = False
    txtText1(52).Top = 3240
    'txtText1(52).Visible = False
    lblLabel1(83).Top = 3000
    lblLabel1(83).Visible = False
    txtText1(51).Top = 3240
    txtText1(51).Visible = False
    lblLabel1(80).Top = 3000
    lblLabel1(80).Visible = False
    txtText1(48).Top = 3240
    txtText1(48).Visible = False
    lblLabel1(82).Top = 3000
    lblLabel1(82).Visible = False
    txtText1(50).Top = 3240
    txtText1(50).Visible = False
    lblLabel1(81).Top = 3000
    lblLabel1(81).Visible = False
    txtText1(49).Top = 3240
    txtText1(49).Visible = False
    lblLabel1(78).Top = 3000
    lblLabel1(78).Visible = False
    txtText1(47).Top = 3240
    txtText1(47).Visible = False
    
    chkCheck1(3).Top = 800
    chkCheck1(3).Visible = True
    chkCheck1(4).Top = 1280
    chkCheck1(4).Visible = True
  
    lblLabel1(84).Visible = False
    txtText1(57).Visible = False
    cmdExplorador(1).Visible = False
    cmdWord.Visible = False
    
    If txtText1(27).Text = "999999999" Then
      txtText1(55).Visible = True
    Else
      txtText1(55).Visible = False
    End If
    
    lblLabel1(8).Left = 5520
    lblLabel1(8).Visible = True
    txtText1(9).Left = 5520
    txtText1(9).Visible = True
    chkCheck1(6).Visible = True
    chkCheck1(7).Visible = True
  
  Case 3 'Fluidoterapia
    lblLabel1(16).Top = 1200 'Linea Medicamento....
    lblLabel1(16).Visible = True
    txtText1(13).Top = 1440
    txtText1(13).Visible = True
    lblLabel1(37).Top = 1200
    lblLabel1(37).Visible = True
    txtText1(35).Top = 1440
    txtText1(35).Visible = True
    lblLabel1(19).Left = 6120
    lblLabel1(19).Top = 1200
    lblLabel1(19).Visible = True
    txtText1(30).Left = 6120
    txtText1(30).Top = 1440
    txtText1(30).Visible = True
    lblLabel1(21).Left = 7080
    lblLabel1(21).Top = 1200
    lblLabel1(21).Visible = True
    txtText1(31).Left = 7080
    txtText1(31).Top = 1440
    txtText1(31).Visible = True
    lblLabel1(49).Top = 1200
    lblLabel1(49).Visible = True
    txtText1(38).Top = 1440
    txtText1(38).Visible = True
    
    lblLabel1(17).Top = 600 'Linea Cantidad...
    lblLabel1(17).Visible = True
    txtText1(28).Top = 840
    txtText1(28).Visible = True
    lblLabel1(23).Left = 960
    lblLabel1(23).Top = 600
    lblLabel1(23).Visible = True
    cboDBCombo1(0).Left = 960
    cboDBCombo1(0).Top = 840
    cboDBCombo1(0).Visible = True
    lblLabel1(25).Left = 2760
    lblLabel1(25).Top = 600
    lblLabel1(25).Visible = True
    cboDBCombo1(1).Left = 2760
    cboDBCombo1(1).Top = 840
    cboDBCombo1(1).Visible = True
    lblLabel1(29).Left = 3600
    lblLabel1(29).Top = 600
    lblLabel1(29).Visible = True
    cboDBCombo1(3).Left = 3600
    cboDBCombo1(3).Top = 840
    cboDBCombo1(3).Visible = True
    'lblLabel1(33).Left = 5520
    'lblLabel1(33).Top = 600
    lblLabel1(33).Visible = False
    'dtcDateCombo1(3).Left = 5520
    'dtcDateCombo1(3).Top = 840
    dtcDateCombo1(3).Visible = False
    'lblLabel1(32).Left = 7440
    'lblLabel1(32).Top = 600
    lblLabel1(32).Visible = False
    'txtText1(37).Top = 7440
    'txtText1(37).Top = 840
    txtText1(37).Visible = False
    
    lblLabel1(34).Top = 0 'Linea Sol.Dil....
    lblLabel1(34).Caption = "Soluci�n Intravenosa"
    lblLabel1(34).Visible = True
    txtText1(41).Top = 240
    txtText1(41).Visible = True
    lblLabel1(35).Top = 0
    lblLabel1(35).Visible = True
    txtText1(42).Top = 240
    txtText1(42).Visible = True
    lblLabel1(36).Top = 0
    lblLabel1(36).Visible = True
    txtHoras.Top = 240
    txtHoras.Visible = True
    txtMinutos.Top = 240
    txtMinutos.Visible = True
    
    lblLabel1(50).Top = 1800 'Linea Instr.Admin. y Vel.Perf....
    lblLabel1(50).Visible = True
    txtText1(39).Top = 2040
    txtText1(39).Visible = True
    lblLabel1(52).Top = 1800
    lblLabel1(52).Visible = True
    txtText1(82).Top = 2040
    txtText1(82).Visible = True
    'lblLabel1(62).Top = 1800
    'lblLabel1(62).Visible = True
    'txtText1(81).Top = 2040
    'txtText1(81).Visible = True
    lblLabel1(77).Top = 2080
    lblLabel1(77).Visible = True
    'txtText1(80).Top = 2040
    'txtText1(80).Visible = True
    lblLabel1(51).Top = 1800
    lblLabel1(51).Visible = True
    txtText1(44).Top = 2040
    txtText1(44).Visible = True
    
    'lblLabel1(31).Left = 5400
    'lblLabel1(31).Top = 2400 'Linea Fecha fin...
    lblLabel1(31).Visible = False
    'dtcDateCombo1(4).Left = 5400
    'dtcDateCombo1(4).Top = 2640
    dtcDateCombo1(4).Visible = False
    'lblLabel1(30).Left = 7320
    'lblLabel1(30).Top = 2400
    lblLabel1(30).Visible = False
    'txtText1(36).Left = 7320
    'txtText1(36).Top = 2640
    txtText1(36).Visible = False
    
    lblLabel1(79).Top = 3000 'Linea Prod2....
    'lblLabel1(79).Visible = False
    txtText1(52).Top = 3240
    'txtText1(52).Visible = False
    lblLabel1(83).Top = 3000
    lblLabel1(83).Visible = False
    txtText1(51).Top = 3240
    txtText1(51).Visible = False
    lblLabel1(80).Top = 3000
    lblLabel1(80).Visible = False
    txtText1(48).Top = 3240
    txtText1(48).Visible = False
    lblLabel1(82).Top = 3000
    lblLabel1(82).Visible = False
    txtText1(50).Top = 3240
    txtText1(50).Visible = False
    lblLabel1(81).Top = 3000
    lblLabel1(81).Visible = False
    txtText1(49).Top = 3240
    txtText1(49).Visible = False
    lblLabel1(78).Top = 3000
    lblLabel1(78).Visible = False
    txtText1(47).Top = 3240
    txtText1(47).Visible = False
    
    chkCheck1(3).Top = 400
    chkCheck1(3).Visible = True
    chkCheck1(4).Top = 880
    chkCheck1(4).Visible = True
  
    lblLabel1(84).Visible = False
    txtText1(57).Visible = False
    cmdExplorador(1).Visible = False
    cmdWord.Visible = False
    
    If txtText1(27).Text = "999999999" Then
      txtText1(55).Visible = True
    Else
      txtText1(55).Visible = False
    End If
    
    lblLabel1(8).Left = 5520
    lblLabel1(8).Visible = True
    txtText1(9).Left = 5520
    txtText1(9).Visible = True
    chkCheck1(6).Visible = True
    chkCheck1(7).Visible = True
    
  Case 1 'Mezcla IV 2M
    lblLabel1(16).Top = 0 'Linea Medicamento....
    lblLabel1(16).Visible = True
    txtText1(13).Top = 240
    txtText1(13).Visible = True
    lblLabel1(37).Top = 0
    lblLabel1(37).Visible = True
    txtText1(35).Top = 240
    txtText1(35).Visible = True
    lblLabel1(19).Left = 6120
    lblLabel1(19).Top = 0
    lblLabel1(19).Visible = True
    txtText1(30).Left = 6120
    txtText1(30).Top = 240
    txtText1(30).Visible = True
    lblLabel1(21).Left = 7080
    lblLabel1(21).Top = 0
    lblLabel1(21).Visible = True
    txtText1(31).Left = 7080
    txtText1(31).Top = 240
    txtText1(31).Visible = True
    lblLabel1(49).Top = 0
    lblLabel1(49).Visible = True
    txtText1(38).Top = 240
    txtText1(38).Visible = True
    
    lblLabel1(17).Top = 600 'Linea Cantidad...
    lblLabel1(17).Visible = True
    txtText1(28).Top = 840
    txtText1(28).Visible = True
    lblLabel1(23).Left = 960
    lblLabel1(23).Top = 600
    lblLabel1(23).Visible = True
    cboDBCombo1(0).Left = 960
    cboDBCombo1(0).Top = 840
    cboDBCombo1(0).Visible = True
    lblLabel1(25).Left = 2760
    lblLabel1(25).Top = 600
    lblLabel1(25).Visible = True
    cboDBCombo1(1).Left = 2760
    cboDBCombo1(1).Top = 840
    cboDBCombo1(1).Visible = True
    lblLabel1(29).Left = 3600
    lblLabel1(29).Top = 600
    lblLabel1(29).Visible = True
    cboDBCombo1(3).Left = 3600
    cboDBCombo1(3).Top = 840
    cboDBCombo1(3).Visible = True
    'lblLabel1(33).Left = 5520
    'lblLabel1(33).Top = 600
    lblLabel1(33).Visible = False
    'dtcDateCombo1(3).Left = 5520
    'dtcDateCombo1(3).Top = 840
    dtcDateCombo1(3).Visible = False
    'lblLabel1(32).Left = 7440
    'lblLabel1(32).Top = 600
    lblLabel1(32).Visible = False
    'txtText1(37).Top = 7440
    'txtText1(37).Top = 840
    txtText1(37).Visible = False
    
    lblLabel1(34).Top = 1200 'Linea Sol.Dil....
    lblLabel1(34).Caption = "Soluci�n para Diluir"
    lblLabel1(34).Visible = True
    txtText1(41).Top = 1440
    txtText1(41).Visible = True
    lblLabel1(35).Top = 1200
    lblLabel1(35).Visible = True
    txtText1(42).Top = 1440
    txtText1(42).Visible = True
    lblLabel1(36).Top = 1200
    lblLabel1(36).Visible = True
    txtHoras.Top = 1440
    txtHoras.Visible = True
    txtMinutos.Top = 1440
    txtMinutos.Visible = True
    
    lblLabel1(50).Top = 2400 'Linea Instr.Admin. y Vel.Perf....
    lblLabel1(50).Visible = True
    txtText1(39).Top = 2640
    txtText1(39).Visible = True
    lblLabel1(52).Top = 2400
    lblLabel1(52).Visible = True
    txtText1(82).Top = 2640
    txtText1(82).Visible = True
    'lblLabel1(62).Top = 2400
    'lblLabel1(62).Visible = True
    'txtText1(81).Top = 2640
    'txtText1(81).Visible = True
    lblLabel1(77).Top = 2680
    lblLabel1(77).Visible = True
    'txtText1(80).Top = 2640
    'txtText1(80).Visible = True
    lblLabel1(51).Top = 2400
    lblLabel1(51).Visible = True
    txtText1(44).Top = 2640
    txtText1(44).Visible = True
    
    'lblLabel1(31).Left = 5400
    'lblLabel1(31).Top = 3000 'Linea Fecha fin...
    lblLabel1(31).Visible = False
    'dtcDateCombo1(4).Left = 5400
    'dtcDateCombo1(4).Top = 3240
    dtcDateCombo1(4).Visible = False
    'lblLabel1(30).Left = 7320
    'lblLabel1(30).Top = 3000
    lblLabel1(30).Visible = False
    'txtText1(36).Left = 7320
    'txtText1(36).Top = 3240
    txtText1(36).Visible = False
    
    'lblLabel1(79).Top = 1800
    'lblLabel1(79).Visible = True
    'txtText1(52).Top = 2040
    'txtText1(52).Visible = True
    lblLabel1(83).Top = 1800 'Linea Prod2....
    lblLabel1(83).Visible = True
    txtText1(51).Top = 2040
    txtText1(51).Visible = True
    lblLabel1(80).Top = 1800
    lblLabel1(80).Visible = True
    txtText1(48).Top = 2040
    txtText1(48).Visible = True
    lblLabel1(82).Top = 1800
    lblLabel1(82).Visible = True
    txtText1(50).Top = 2040
    txtText1(50).Visible = True
    lblLabel1(81).Top = 1800
    lblLabel1(81).Visible = True
    txtText1(49).Top = 2040
    txtText1(49).Visible = True
    lblLabel1(78).Top = 1800
    lblLabel1(78).Visible = True
    txtText1(47).Top = 2040
    txtText1(47).Visible = True
    
    chkCheck1(3).Top = 800
    chkCheck1(3).Visible = True
    chkCheck1(4).Top = 1280
    chkCheck1(4).Visible = True
  
    lblLabel1(84).Visible = False
    txtText1(57).Visible = False
    cmdExplorador(1).Visible = False
    cmdWord.Visible = False
  
    If txtText1(27).Text = "999999999" Then
      txtText1(55).Visible = True
    Else
      txtText1(55).Visible = False
    End If
    
    lblLabel1(8).Left = 5520
    lblLabel1(8).Visible = True
    txtText1(9).Left = 5520
    txtText1(9).Visible = True
    chkCheck1(6).Visible = True
    chkCheck1(7).Visible = True
  
  Case 5 'Form.Magistral
    lblLabel1(84).Left = 0
    lblLabel1(84).Top = 1200
    lblLabel1(84).Visible = True
    txtText1(57).Left = 0
    txtText1(57).Top = 1440
    txtText1(57).Visible = True
    cmdExplorador(1).Left = 4920
    cmdExplorador(1).Top = 1240
    cmdExplorador(1).Visible = True
    cmdWord.Left = 5520
    cmdWord.Top = 1240
    cmdWord.Visible = True
    
    'lblLabel1(16).Visible = False 'Linea Medicamento....
    txtText1(13).Visible = False
    lblLabel1(37).Visible = False
    txtText1(35).Visible = False
    lblLabel1(19).Top = 640
    lblLabel1(19).Left = 0
    lblLabel1(19).Visible = True
    txtText1(30).Top = 840
    txtText1(30).Left = 0
    txtText1(30).Visible = True
    lblLabel1(21).Left = 1000
    lblLabel1(21).Top = 640
    lblLabel1(21).Visible = True
    txtText1(31).Left = 1000
    txtText1(31).Top = 840
    txtText1(31).Visible = True
    lblLabel1(49).Visible = False
    txtText1(38).Visible = False
    
    lblLabel1(17).Visible = False 'Linea Cantidad...
    txtText1(28).Visible = False
    lblLabel1(23).Left = 1960
    lblLabel1(23).Visible = True
    cboDBCombo1(0).Left = 1960
    cboDBCombo1(0).Visible = True
    lblLabel1(25).Left = 3760
    lblLabel1(25).Visible = True
    cboDBCombo1(1).Left = 3760
    cboDBCombo1(1).Visible = True
    lblLabel1(29).Left = 4600
    lblLabel1(29).Visible = True
    cboDBCombo1(3).Left = 4600
    cboDBCombo1(3).Visible = True
    
    'lblLabel1(33).Left = 0
    'lblLabel1(33).Top = 600 'Linea Fecha Inicio...
    lblLabel1(33).Visible = False
    'dtcDateCombo1(3).Left = 0
    'dtcDateCombo1(3).Top = 840
    dtcDateCombo1(3).Visible = False
    'lblLabel1(32).Left = 1920
    'lblLabel1(32).Top = 600
    lblLabel1(32).Visible = False
    'txtText1(37).Left = 1920
    'txtText1(37).Top = 840
    txtText1(37).Visible = False
    
    lblLabel1(34).Visible = False 'Linea Sol.Dil....
    txtText1(41).Visible = False
    lblLabel1(35).Visible = False
    txtText1(42).Visible = False
    lblLabel1(36).Visible = False
    txtHoras.Visible = False
    txtMinutos.Visible = False
    
    lblLabel1(50).Top = 2400 'Linea Instr.Admin. y Vel.Perf....
    lblLabel1(50).Visible = True
    txtText1(39).Top = 2640
    txtText1(39).Visible = True
    lblLabel1(52).Visible = False
    txtText1(82).Visible = False
    'lblLabel1(62).Visible = False
    'txtText1(81).Visible = False
    lblLabel1(77).Visible = False
    'txtText1(80).Visible = False
    lblLabel1(51).Visible = False
    txtText1(44).Visible = False
    
    'lblLabel1(31).Left = 0
    'lblLabel1(31).Top = 1800 'Linea Fecha fin...
    lblLabel1(31).Visible = False
    'dtcDateCombo1(4).Left = 0
    'dtcDateCombo1(4).Top = 2040
    dtcDateCombo1(4).Visible = False
    'lblLabel1(30).Left = 1920
    'lblLabel1(30).Top = 1800
    lblLabel1(30).Visible = False
    'txtText1(36).Left = 1920
    'txtText1(36).Top = 2040
    txtText1(36).Visible = False
    
    'lblLabel1(79).Top = 1800
    'lblLabel1(79).Visible = False
    'txtText1(52).Top = 2040
    'txtText1(52).Visible = False
    lblLabel1(83).Visible = False 'Linea Prod2....
    txtText1(51).Visible = False
    lblLabel1(80).Visible = False
    txtText1(48).Visible = False
    lblLabel1(82).Visible = False
    txtText1(50).Visible = False
    lblLabel1(81).Visible = False
    txtText1(49).Visible = False
    lblLabel1(78).Visible = False
    txtText1(47).Visible = False
    
    chkCheck1(3).Top = 800
    chkCheck1(3).Visible = True
    chkCheck1(4).Visible = False

    txtText1(55).Top = 240
    txtText1(55).Left = 0
    txtText1(55).Visible = True
    lblLabel1(16).Top = 0
    lblLabel1(16).Caption = "F�rmula Magistral"
    
    lblLabel1(8).Left = 6500
    lblLabel1(8).Visible = True
    txtText1(9).Left = 6500
    txtText1(9).Visible = True
    chkCheck1(6).Visible = False
    chkCheck1(7).Visible = False

  End Select


  Select Case tab2.Tab 'Campos Obligatorios
  Case 0
    txtText1(55).BackColor = txtText1(1).BackColor 'obligatorio
    txtText1(35).BackColor = txtText1(1).BackColor
    txtText1(30).BackColor = txtText1(1).BackColor
    txtText1(31).BackColor = txtText1(1).BackColor
    txtText1(28).BackColor = txtText1(1).BackColor
    cboDBCombo1(0).BackColor = txtText1(1).BackColor
    cboDBCombo1(1).BackColor = txtText1(1).BackColor
    cboDBCombo1(3).BackColor = txtText1(1).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = vbWhite
  Case 1
    txtText1(55).BackColor = vbWhite
    txtText1(35).BackColor = txtText1(1).BackColor
    txtText1(30).BackColor = txtText1(1).BackColor
    txtText1(31).BackColor = txtText1(1).BackColor
    txtText1(28).BackColor = txtText1(1).BackColor
    cboDBCombo1(0).BackColor = txtText1(1).BackColor
    cboDBCombo1(1).BackColor = txtText1(1).BackColor
    cboDBCombo1(3).BackColor = txtText1(1).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = vbWhite
  Case 2
    txtText1(55).BackColor = vbWhite
    txtText1(35).BackColor = txtText1(1).BackColor
    txtText1(30).BackColor = txtText1(1).BackColor
    txtText1(31).BackColor = txtText1(1).BackColor
    txtText1(28).BackColor = txtText1(1).BackColor
    cboDBCombo1(0).BackColor = txtText1(1).BackColor
    cboDBCombo1(1).BackColor = txtText1(1).BackColor
    cboDBCombo1(3).BackColor = txtText1(1).BackColor
    txtText1(39).BackColor = txtText1(1).BackColor
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = vbWhite
  Case 3
    txtText1(55).BackColor = vbWhite
    txtText1(35).BackColor = vbWhite
    txtText1(30).BackColor = vbWhite
    txtText1(31).BackColor = vbWhite
    txtText1(28).BackColor = txtText1(1).BackColor
    cboDBCombo1(0).BackColor = txtText1(1).BackColor
    cboDBCombo1(1).BackColor = txtText1(1).BackColor
    cboDBCombo1(3).BackColor = txtText1(1).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = txtText1(1).BackColor
    txtHoras.BackColor = txtText1(1).BackColor
    txtMinutos.BackColor = txtText1(1).BackColor
    txtText1(57).BackColor = vbWhite
  Case 4
    txtText1(55).BackColor = vbWhite
    txtText1(35).BackColor = txtText1(1).BackColor
    txtText1(30).BackColor = txtText1(1).BackColor
    txtText1(31).BackColor = txtText1(1).BackColor
    txtText1(28).BackColor = txtText1(1).BackColor
    cboDBCombo1(0).BackColor = txtText1(1).BackColor
    cboDBCombo1(1).BackColor = txtText1(1).BackColor
    cboDBCombo1(3).BackColor = txtText1(1).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = vbWhite
  Case 5
    txtText1(55).BackColor = txtText1(1).BackColor
    txtText1(35).BackColor = vbWhite
    txtText1(30).BackColor = txtText1(1).BackColor
    txtText1(31).BackColor = txtText1(1).BackColor
    txtText1(28).BackColor = vbWhite
    cboDBCombo1(0).BackColor = txtText1(1).BackColor
    cboDBCombo1(1).BackColor = txtText1(1).BackColor
    cboDBCombo1(3).BackColor = txtText1(1).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = txtText1(1).BackColor
  End Select



  Select Case tab2.Tab
  Case 0
    cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = False
    mstrOperacion = "/" 'Medicamento
    If txtText1(55).Visible = True Then
      lblLabel1(16).Caption = "Medicamento No Formulario"
    Else
      lblLabel1(16).Caption = "Medicamento"
    End If
  Case 1
    cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = True
    mstrOperacion = "M" 'Mezcla IV 2M
    lblLabel1(16).Caption = "Medicamento"
  Case 2
    cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = False
    mstrOperacion = "P" 'PRN en OM
    lblLabel1(16).Caption = "Medicamento"
  Case 3
    cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = False
    mstrOperacion = "F" 'Fluidoterapia
    lblLabel1(16).Caption = "Medicamento"
  Case 4
    cmdbuscarprod.Caption = "Estupefacientes"
    'cmdMed2.Visible = False
    mstrOperacion = "E" 'Estupefaciente
    lblLabel1(16).Caption = "Estupefaciente"
  Case 5
    cmdbuscarprod.Caption = "Form. Magistrales"
    'cmdMed2.Visible = False
    mstrOperacion = "L" 'Form.Magistral
    lblLabel1(16).Caption = "F�rmula Magistral"
  End Select
  
  If tabTab1(1).Tab = 0 Then
    'botones habilitados
    If tab2.Tab = 0 Or tab2.Tab = 1 Or tab2.Tab = 2 Then
      cmdbuscarprod.Enabled = True
      cmdbuscarprot.Enabled = True
      cmdbuscargruprot.Enabled = True
      cmdbuscargruprod.Enabled = True
      cmdMedNoForm.Enabled = True
      cmdSolDil.Enabled = True
      cmdMed2.Enabled = True
      cmdInformacion.Enabled = True
      cmdInstAdm.Enabled = True
    Else
      cmdbuscarprod.Enabled = True
      cmdbuscarprot.Enabled = True
      cmdbuscargruprot.Enabled = False
      cmdbuscargruprod.Enabled = False
      cmdMedNoForm.Enabled = True
      cmdSolDil.Enabled = True
      cmdMed2.Enabled = True
      cmdInstAdm.Enabled = True
      If tab2.Tab <> 5 Then
        cmdInformacion.Enabled = True
      Else
        cmdInformacion.Enabled = False
      End If
    End If
  Else
    'botones deshabilitados
    cmdbuscarprod.Enabled = False
    cmdbuscarprot.Enabled = False
    cmdbuscargruprot.Enabled = False
    cmdbuscargruprod.Enabled = False
    cmdMedNoForm.Enabled = False
    cmdSolDil.Enabled = False
    cmdMed2.Enabled = False
    cmdInformacion.Enabled = False
    cmdInstAdm.Enabled = False
  End If

End Sub

Private Sub Periodicidad()
Dim strdias As String
Dim intPos As Integer
Dim i As Integer
  
  'Frame2.Visible = True
  Call Frame2.ZOrder(0)
  cmdbuscargruprot.Enabled = False
  cmdbuscarprot.Enabled = False
  cmdbuscargruprod.Enabled = False
  cmdbuscarprod.Enabled = False
  fraframe1(0).Enabled = False
  fraframe1(1).Enabled = False
  tlbToolbar1.Enabled = False
  
  strdias = txtText1(10)
  For i = 1 To 30
    Check1(i).Value = 0
  Next i
  While strdias <> ""
    intPos = InStr(1, strdias, ",")
    If intPos <> 0 Then
      Check1(Val(Left(strdias, intPos - 1))).Value = 1
    Else
      Check1(strdias).Value = 1
    End If
    intPos = InStr(1, strdias, ",")
    If intPos <> 0 Then
      strdias = Right(strdias, Len(strdias) - intPos)
    Else
      strdias = ""
    End If
  Wend

End Sub


Private Function Campos_Obligatorios() As Boolean
Dim blnIncorrecta As Boolean
Dim mensaje As String
Dim auxControl As Control
    
  mensaje = ""
  blnIncorrecta = False
  Select Case txtText1(53).Text 'Campos Obligatorios
  Case "/" 'Medicamento
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Medicamento es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If cboDBCombo1(3) <> "Agenda" Then
      If txtText1(9).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(9)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Num.D�as es obligatorio para Periodicidad distinta de Agenda."
      End If
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
  Case "M" 'Mezcla IV 2M
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Medicamento es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If cboDBCombo1(3) <> "Agenda" Then
      If txtText1(9).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(9)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Num.D�as es obligatorio para Periodicidad distinta de Agenda."
      End If
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
    If txtText1(52).Text <> "" Then
      If txtText1(48).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(48)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica del Medicamento 2 es obligatorio."
      End If
      If txtText1(50).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(50)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Dosis del Medicamento 2 es obligatorio."
      End If
      If txtText1(49).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(49)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Unidad de Medida del Medicamento 2 es obligatorio."
      End If
    End If
  Case "P" 'PRN en OM
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Medicamento es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If cboDBCombo1(3) <> "Agenda" Then
      If txtText1(9).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(9)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Num.D�as es obligatorio para Periodicidad distinta de Agenda."
      End If
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
    If txtText1(39).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(39)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n."
    End If
  Case "F" 'Fluidoterapia
    If txtText1(40).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(42)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Soluci�n Intravenosa es obligatorio."
    End If
    If txtText1(42).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(42)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n Intravenosa es obligatorio."
    End If
    If txtHoras.Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtHoras
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n Intravenosa es obligatorio."
    End If
    If txtMinutos.Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtMinutos
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n Intravenosa es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad de la Soluci�n Intravenosa es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia de la Soluci�n Intravenosa es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a de la Soluci�n Intravenosa es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad de la Soluci�n Intravenosa es obligatorio."
    End If
    If cboDBCombo1(3) <> "Agenda" Then
      If txtText1(9).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(9)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Num.D�as es obligatorio para Periodicidad distinta de Agenda."
      End If
    End If
    If txtText1(27).Text <> "" Then
      If txtText1(35).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(35)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica del Medicamento es obligatorio."
      End If
      If txtText1(30).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(30)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Dosis del Medicamento es obligatorio."
      End If
      If txtText1(31).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(31)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Unidad de Medida del Medicamento es obligatorio."
      End If
    End If
  Case "E" 'Estupefaciente
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Estupefaciente es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If cboDBCombo1(3) <> "Agenda" Then
      If txtText1(9).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(9)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Num.D�as es obligatorio para Periodicidad distinta de Agenda."
      End If
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
  Case "L" 'Form.Magistral
    If txtText1(55).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(55)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Descripci�n de la F�rmula Magistral es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If cboDBCombo1(3) <> "Agenda" Then
      If txtText1(9).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(9)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Num.D�as es obligatorio para Periodicidad distinta de Agenda."
      End If
    End If
    If txtText1(57).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(57)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Archivo Inf. de la F�rmula Magistral es obligatorio."
    End If
  End Select
  
  If blnIncorrecta = False Then
    Campos_Obligatorios = False
  Else
    Campos_Obligatorios = True
    MsgBox mensaje, vbInformation, "Aviso"
    auxControl.SetFocus
  End If

End Function


