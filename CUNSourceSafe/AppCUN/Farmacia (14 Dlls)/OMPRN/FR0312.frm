VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmVisPRN 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Visualizar PRN"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0124.frx":0000
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Height          =   375
      Left            =   8400
      TabIndex        =   18
      Top             =   7680
      Width           =   1935
   End
   Begin VB.TextBox txtHistoria 
      Height          =   315
      Left            =   10680
      MaxLength       =   7
      TabIndex        =   7
      Top             =   1125
      Width           =   975
   End
   Begin VB.Frame Frame5 
      Caption         =   "Historia :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   10560
      TabIndex        =   17
      Top             =   840
      Width           =   1215
   End
   Begin VB.CommandButton cmdFiltrar 
      Caption         =   "FILTRAR"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   10560
      TabIndex        =   8
      Top             =   1800
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Reco&ger Cestas"
      Height          =   375
      Left            =   1800
      TabIndex        =   11
      Top             =   7680
      Width           =   1695
   End
   Begin VB.CommandButton cmdCesta 
      Caption         =   "&Preparar"
      Height          =   375
      Left            =   4920
      TabIndex        =   10
      Top             =   7680
      Width           =   1935
   End
   Begin VB.Frame Frame1 
      Caption         =   "Seleccionar :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   2415
      Left            =   120
      TabIndex        =   14
      Top             =   480
      Width           =   11775
      Begin Crystal.CrystalReport CrystalReport1 
         Left            =   7800
         Top             =   1680
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   327680
         PrintFileLinesPerPage=   60
      End
      Begin VB.Frame Frame2 
         Caption         =   "Servicio :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1980
         Left            =   6120
         TabIndex        =   27
         Top             =   240
         Width           =   4215
         Begin VB.CheckBox chkservicio 
            Caption         =   "Todos los servicios"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   29
            Top             =   1200
            Value           =   1  'Checked
            Width           =   2055
         End
         Begin VB.TextBox txtServicio 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   28
            TabStop         =   0   'False
            Tag             =   "Desc.Servicio"
            Top             =   720
            Width           =   2325
         End
         Begin SSDataWidgets_B.SSDBCombo cboservicio 
            Height          =   315
            Left            =   120
            TabIndex        =   30
            TabStop         =   0   'False
            Top             =   360
            Width           =   735
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2223
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1296
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin ComctlLib.ListView lvwSec 
            Height          =   1335
            Left            =   2520
            TabIndex        =   32
            Top             =   480
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   2355
            View            =   3
            MultiSelect     =   -1  'True
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            _Version        =   327682
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Secciones:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   2520
            TabIndex        =   33
            Top             =   240
            Width           =   960
         End
      End
      Begin VB.CheckBox chkFechas 
         Caption         =   "Todas las Fechas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4080
         TabIndex        =   26
         Top             =   240
         Value           =   1  'Checked
         Width           =   1935
      End
      Begin VB.Frame Frame4 
         Height          =   2055
         Left            =   1440
         TabIndex        =   16
         Top             =   240
         Width           =   2535
         Begin VB.OptionButton optOMPRN 
            Caption         =   "Parciales + Enviadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   300
            Index           =   0
            Left            =   120
            TabIndex        =   19
            Top             =   1000
            Width           =   2295
         End
         Begin VB.OptionButton optOMPRN 
            Caption         =   "Redactadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   300
            Index           =   1
            Left            =   120
            TabIndex        =   2
            Top             =   120
            Width           =   1455
         End
         Begin VB.OptionButton optOMPRN 
            Caption         =   "Enviadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   300
            Index           =   2
            Left            =   120
            TabIndex        =   3
            Top             =   400
            Width           =   2175
         End
         Begin VB.OptionButton optOMPRN 
            Caption         =   "Dispensadas Parciales"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   300
            Index           =   3
            Left            =   120
            TabIndex        =   4
            Top             =   700
            Width           =   2295
         End
         Begin VB.OptionButton optOMPRN 
            Caption         =   "Dispensadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   300
            Index           =   4
            Left            =   120
            TabIndex        =   5
            Top             =   1320
            Width           =   1695
         End
         Begin VB.OptionButton optOMPRN 
            BackColor       =   &H00C0C0C0&
            Caption         =   "Con l�neas bloqueadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   300
            Index           =   5
            Left            =   120
            TabIndex        =   6
            Top             =   1600
            Width           =   2295
         End
      End
      Begin VB.Frame Frame3 
         Height          =   2055
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   1215
         Begin VB.OptionButton optCesta 
            Caption         =   "PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   1
            Left            =   120
            TabIndex        =   0
            Top             =   600
            Width           =   855
         End
         Begin VB.OptionButton optCesta 
            Caption         =   "Cestas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   0
            Left            =   120
            TabIndex        =   1
            Top             =   960
            Width           =   975
         End
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcredaccion 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   4080
         TabIndex        =   20
         Top             =   720
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcEnvio 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   4080
         TabIndex        =   21
         Top             =   1320
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDispensacion 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   4080
         TabIndex        =   22
         Top             =   1920
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Redacci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   4080
         TabIndex        =   25
         Top             =   480
         Width           =   1515
      End
      Begin VB.Label lblEnvio 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Envio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   4080
         TabIndex        =   24
         Top             =   1080
         Width           =   1080
      End
      Begin VB.Label lblDispen 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Dispensaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   4080
         TabIndex        =   23
         Top             =   1680
         Width           =   1740
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4695
      Index           =   2
      Left            =   120
      TabIndex        =   9
      Tag             =   "Actuaciones Asociadas"
      Top             =   2880
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
         Height          =   4215
         Index           =   0
         Left            =   120
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   360
         Width           =   11370
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "NoDispensada"
         stylesets(0).BackColor=   16776960
         stylesets(0).Picture=   "FR0124.frx":000C
         stylesets(1).Name=   "Dispensada"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "FR0124.frx":0028
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20055
         _ExtentY        =   7435
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   12
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Enabled         =   0   'False
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Enabled         =   0   'False
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Enabled         =   0   'False
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Enabled         =   0   'False
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Enabled         =   0   'False
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Visible         =   0   'False
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Visible         =   0   'False
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Visible         =   0   'False
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Visible         =   0   'False
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Visible         =   0   'False
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmVisPRN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0124.FRM                                                   *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: visualizar OM/PRN                                       *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim blnInload As Boolean
Dim glstrWhere As String

Private Sub auxDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
    
  If Index = 0 Then
    If auxDBGrid1(0).Columns("C�d.Estado").Value = 5 Or auxDBGrid1(0).Columns("C�d.Estado").Value = 4 Then
      For i = 0 To 23
        auxDBGrid1(0).Columns(i).CellStyleSet "Dispensada"
      Next i
    Else
      For i = 0 To 23
        auxDBGrid1(0).Columns(i).CellStyleSet "NoDispensada"
      Next i
    End If
  End If

End Sub

Private Sub cboservicio_Change()
'If cboservicio <> "" Then
'  chkservicio.Value = False
'  txtServicio.Text = cboservicio.Columns(1).Value
'End If

'auxDBGrid1(0).RemoveAll
'''''''''''''''''''''''''''''''''''''''''
Dim intDptoSel As Integer

If IsNumeric(cboservicio.Text) Then
  If chkservicio.Value = 0 Then
  Else 'todos los servicios
    chkservicio.Value = False
  End If
  txtServicio.Text = cboservicio.Columns(1).Value
End If

End Sub

Private Sub cboservicio_Click()
  
'If cboservicio <> "" Then
'  chkservicio.Value = False
'  txtServicio.Text = cboservicio.Columns(1).Value
'End If

Dim intDptoSel As Integer
If IsNumeric(cboservicio.Text) Then
  If chkservicio.Value = 0 Then
  Else 'todos los servicios
      chkservicio.Value = False
  End If
  txtServicio.Text = cboservicio.Columns(1).Value
  intDptoSel = cboservicio
  Call pCargarSecciones(intDptoSel)
End If

End Sub

Private Sub cboservicio_CloseUp()
Dim intDptoSel As Integer
'  If cboservicio <> "" Then
'    chkservicio.Value = False
'    txtServicio.Text = cboservicio.Columns(1).Value
'  End If

If IsNumeric(cboservicio.Text) Then
    If chkservicio.Value = 0 Then
    Else 'todos los servicios
        chkservicio.Value = False
    End If
    txtServicio.Text = cboservicio.Columns(1).Value
  End If

  If Trim(cboservicio.Text) <> "" Then
    If chkservicio.Value = 0 Then
      txtServicio.Text = cboservicio.Columns(1).Value
      If cboservicio <> "" Then
        intDptoSel = cboservicio
        Call pCargarSecciones(intDptoSel)
      End If
    Else 'todos los servicios
      txtServicio.Text = cboservicio.Columns(1).Value
      chkservicio.Value = 0
    End If
  Else
    txtServicio.Text = ""
    chkservicio.Value = 1
  End If
  auxDBGrid1(0).RemoveAll

End Sub


Private Sub chkFechas_Click()

  If chkFechas = 1 Then
    dtcredaccion.Text = ""
    dtcEnvio.Text = ""
    dtcDispensacion.Text = ""
  End If
  auxDBGrid1(0).RemoveAll

End Sub

Private Sub chkservicio_Click()
  
  If chkservicio = 1 Then
    cboservicio.Text = ""
    txtServicio.Text = ""
  End If

auxDBGrid1(0).RemoveAll

End Sub

Private Sub cmdCesta_Click()
Dim strUpd As String
Dim qryUpd As rdoQuery
Dim intNumLin As Integer
Dim mintisel As Integer
Dim mvarBkmrk As Variant
Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
  
  cmdCesta.Enabled = False
  Screen.MousePointer = vbHourglass
  Me.Enabled = False
  gintfirmarOM = 1
  If optCesta(1).Value = True Then 'es PRN
    If auxDBGrid1(0).SelBookmarks.Count > 0 Then
      glngpeticion = auxDBGrid1(0).Columns("C�digo Petici�n").Value
      strUpd = "UPDATE FR2800 " & _
               "   SET FR28CANTPEDORI = FR28CANTIDAD " & _
               "  WHERE FR66CODPETICION = ?  AND FR28CANTPEDORI IS NULL"
      Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
      qryUpd(0) = glngpeticion
      qryUpd.Execute
    Else
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      cmdCesta.Enabled = True
      MsgBox "Debe seleccionar un PRN", vbInformation, "Dispensar PRN"
      Exit Sub
    End If
    gstrLlamadorProd = "PRN"
  Else 'es Cesta
    gstrLlamadorProd = "Cesta"
    If auxDBGrid1(0).SelBookmarks.Count > 0 Then
        gstrCodDpto = auxDBGrid1(0).Columns("C�d.Servicio").Value
    Else
      gstrLlamadorProd = ""
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      cmdCesta.Enabled = True
      Exit Sub
    End If
    If optOMPRN(0).Value = True Then
      gstrEstCesta = " AND FR26CODESTPETIC IN (3,9)"
      gintEstCesta = 2
      If auxDBGrid1(0).SelBookmarks.Count > 0 Then
        gstrCodDpto = auxDBGrid1(0).Columns("C�d.Servicio").Value
        gstrEstCesta = " AND FR26CODESTPETIC IN (3,9) "
        
        strUpd = "UPDATE FR2800 "
        strUpd = strUpd & " SET FR28CANTPEDORI = FR28CANTIDAD "
        strUpd = strUpd & " WHERE NVL(FR28CANTPEDORI,0) <> FR28CANTIDAD "
        strUpd = strUpd & " AND FR66CODPETICION IN "
        strUpd = strUpd & "  (SELECT FR66CODPETICION "
        strUpd = strUpd & "   FROM FR6600 "
        strUpd = strUpd & "   WHERE FR26CODESTPETIC = ? "
        strUpd = strUpd & "   AND AD02CODDPTO = ? "
        strUpd = strUpd & "   AND (FR66INDOM IS NULL OR FR66INDOM = ?) "
        strUpd = strUpd & "   AND FR66INDCESTA = ? "
        strUpd = strUpd & "  )  AND FR28CANTPEDORI IS NULL"
        
        Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
        qryUpd(0) = 3
        qryUpd(1) = gstrCodDpto
        qryUpd(2) = 0
        qryUpd(3) = -1
        qryUpd.Execute
      End If
    ElseIf optOMPRN(1).Value = True Then 'FR26CODESTPETIC=1
      gstrEstCesta = " AND FR26CODESTPETIC=1 "
      gintEstCesta = 1
    ElseIf optOMPRN(2).Value = True Then 'FR26CODESTPETIC=3
      gintEstCesta = 2
      If auxDBGrid1(0).SelBookmarks.Count > 0 Then
        gstrCodDpto = auxDBGrid1(0).Columns("C�d.Servicio").Value
        gstrEstCesta = " AND FR26CODESTPETIC=3 "
        
        strUpd = "UPDATE FR2800 "
        strUpd = strUpd & " SET FR28CANTPEDORI = FR28CANTIDAD "
        strUpd = strUpd & " WHERE NVL(FR28CANTPEDORI,0) <> FR28CANTIDAD "
        strUpd = strUpd & " AND FR66CODPETICION IN "
        strUpd = strUpd & "  (SELECT FR66CODPETICION "
        strUpd = strUpd & "   FROM FR6600 "
        strUpd = strUpd & "   WHERE FR26CODESTPETIC = ? "
        strUpd = strUpd & "   AND AD02CODDPTO = ? "
        strUpd = strUpd & "   AND (FR66INDOM IS NULL OR FR66INDOM = ?) "
        strUpd = strUpd & "   AND FR66INDCESTA = ? "
        strUpd = strUpd & "  )  AND FR28CANTPEDORI IS NULL"
        
        Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
        qryUpd(0) = 3
        qryUpd(1) = gstrCodDpto
        qryUpd(2) = 0
        qryUpd(3) = -1
        qryUpd.Execute
      End If
    ElseIf optOMPRN(3).Value = True Then
      gintEstCesta = 3
      gstrEstCesta = " AND  FR26CODESTPETIC=9  "
    ElseIf optOMPRN(4).Value = True Then
      gintEstCesta = 4
      gstrEstCesta = " AND FR26CODESTPETIC=5 "
    ElseIf optOMPRN(5).Value = True Then
      gintEstCesta = 5
      gstrEstCesta = " AND FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 " & _
                                       "WHERE FR28INDBLOQUEADA=-1) "
    End If
   
  End If
  
  If auxDBGrid1(0).SelBookmarks.Count > 0 Then
   gstrCodDpto = auxDBGrid1(0).Columns("C�d.Servicio").Value
  Else
    Me.Enabled = True
    Screen.MousePointer = vbDefault
    cmdCesta.Enabled = True
    MsgBox "Debe seleccionar una Cesta", vbInformation, "Dispensar Cestas"
    gstrEstCesta = 0
    Exit Sub
  End If
  
  If optCesta(1).Value = True Then 'es PRN
    Call objsecurity.LaunchProcess("FR0125")
  Else
    SQL = "SELECT COUNT(*)"
    SQL = SQL & " FROM AD4100"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND SYSDATE BETWEEN AD41FECINICIO AND NVL(AD41FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = auxDBGrid1(0).Columns("C�d.Servicio").Value
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rs(0).Value = 0 Then
      Call objsecurity.LaunchProcess("FR0159")
    Else
      Call objsecurity.LaunchProcess("FR0359")
    End If
    rs.Close
    Set rs = Nothing
  End If
  gstrEstCesta = 0
  gstrLlamadorProd = ""
  Me.Enabled = True
  Screen.MousePointer = vbDefault
  cmdCesta.Enabled = True
End Sub

Private Sub cmdConsultar_Click()
Dim vntData(2) As Variant

  cmdConsultar.Enabled = False
  Screen.MousePointer = vbHourglass
  Me.Enabled = False
  gintfirmarOM = 1
    If auxDBGrid1(0).SelBookmarks.Count > 0 Then
      'glngpeticion = grdDBGrid1(1).Columns(3).Value
    Else
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      cmdConsultar.Enabled = True
      MsgBox "Debe seleccionar un PRN � Cesta", vbInformation, "Error"
      Exit Sub
    End If
    
  vntData(1) = "VisPRNOM" 'gstrLlamadorProd
  vntData(2) = auxDBGrid1(0).Columns("C�digo Petici�n").Value 'glngpeticion
  Call objsecurity.LaunchProcess("FR0725", vntData)
  
  Me.Enabled = True
  Screen.MousePointer = vbDefault
  cmdConsultar.Enabled = True

End Sub

Private Sub cmdfiltrar_Click()
Dim mensaje As String
Dim i%
Dim contSecciones As Integer
    
  If optOMPRN(4).Value = True Then
    If Not IsDate(dtcDispensacion) Or cboservicio = "" Then
      mensaje = MsgBox("Debe elegir Fecha de Dispensaci�n y Servicio," & Chr(13) & "porque si no hay demasiados registros.", vbInformation, "Aviso")
      Exit Sub
    End If
  End If
    
  Me.Enabled = False
  Screen.MousePointer = vbHourglass
  glstrWhere = ""
  
  
  If cboservicio.Text = "" And chkservicio = 0 Then
    Screen.MousePointer = vbDefault
    mensaje = MsgBox("No hay ning�n servicio seleccionado.", vbInformation, "Aviso")
  Else
    If chkservicio = 1 Then
      If optCesta(1).Value = True Then
        If optOMPRN(0).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC IN (3,9)" & _
                       " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                       " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        ElseIf optOMPRN(1).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=1" & _
                       " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                       " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        ElseIf optOMPRN(2).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=3" & _
                       " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                       " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        ElseIf optOMPRN(3).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=9" & _
                       " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                       " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        ElseIf optOMPRN(4).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=5" & _
                       " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                       " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        ElseIf optOMPRN(5).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0" & _
                " AND FR6600.FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 " & _
                                       "WHERE FR28INDBLOQUEADA=-1)" & _
                " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        End If
      Else
        If optOMPRN(0).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC IN (3,9)"
        ElseIf optOMPRN(1).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=1"
        ElseIf optOMPRN(2).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=3"
        ElseIf optOMPRN(3).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=9"
        ElseIf optOMPRN(4).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=5"
        ElseIf optOMPRN(5).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 " & _
                " AND FR6600.FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 " & _
                                       "WHERE FR28INDBLOQUEADA=-1)"
        End If
      End If
    Else 'todos los servicios
      If optCesta(1).Value = True Then
        If optOMPRN(0).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC IN (3,9)  AND FR6600.AD02CODDPTO=" & cboservicio.Text & _
                      " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                      " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        ElseIf optOMPRN(1).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=1 AND FR6600.AD02CODDPTO=" & cboservicio.Text & _
                       " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                       " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        ElseIf optOMPRN(2).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=3 AND FR6600.AD02CODDPTO=" & cboservicio.Text & _
                       " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                       " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        ElseIf optOMPRN(3).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=9 AND FR6600.AD02CODDPTO=" & cboservicio.Text & _
                      " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                      " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        ElseIf optOMPRN(4).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=5 AND FR6600.AD02CODDPTO=" & cboservicio.Text & _
                       " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                       " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        ElseIf optOMPRN(5).Value = True Then
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.AD02CODDPTO=" & cboservicio.Text & _
                " AND FR6600.FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 " & _
                "WHERE FR28INDBLOQUEADA=-1)" & _
                " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
                " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
        End If
      Else
        If optOMPRN(0).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC IN (3,9)  AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(1).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=1 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(2).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=3 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(3).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=9 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(4).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=5 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(5).Value = True Then
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.AD02CODDPTO=" & cboservicio.Text & _
          " AND FR6600.FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 " & _
                                 "WHERE FR28INDBLOQUEADA=-1)"
        End If
      End If
    End If
    
    If IsNumeric(txtHistoria.Text) Then
      If glstrWhere <> "" Then
        If glstrWhere <> "-1=0" Then
          glstrWhere = glstrWhere & " AND FR6600.CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txtHistoria.Text & ")"
        Else
          glstrWhere = " FR6600.CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txtHistoria.Text & ")"
        End If
      Else
        glstrWhere = " FR6600.CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txtHistoria.Text & ")"
      End If
    End If
    
    If glstrWhere <> "-1=0" Then
      If IsDate(dtcredaccion) Then
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECREDACCION)=TO_DATE('" & dtcredaccion & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcEnvio) Then
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECENVIO)=TO_DATE('" & dtcEnvio & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcDispensacion) Then
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECDISPEN)=TO_DATE('" & dtcDispensacion & "','DD/MM/YYYY')"
      End If
    Else
      glstrWhere = " 1=1 "
      If IsDate(dtcredaccion) Then
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECREDACCION)=TO_DATE('" & dtcredaccion & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcEnvio) Then
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECENVIO)=TO_DATE('" & dtcEnvio & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcDispensacion) Then
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECDISPEN)=TO_DATE('" & dtcDispensacion & "','DD/MM/YYYY')"
      End If
    End If
    
  'secciones''''''''''''''''''''''''''''''''''''''''''''''''''''
'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If lvwSec.ListItems.Count > 0 Then
    If lvwSec.ListItems(1).Selected Then
    Else
      For i = 2 To lvwSec.ListItems.Count
        If lvwSec.ListItems(i).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            glstrWhere = glstrWhere & " AND ("
          Else
            glstrWhere = glstrWhere & " OR "
          End If
          'glstrWhere = glstrWhere & "FR6600.AD41CODSECCION=" & lvwSec.ListItems(i).Tag
           If i = 2 Then
            glstrWhere = glstrWhere & "FR6600.AD41CODSECCION IS NULL"
          Else
            glstrWhere = glstrWhere & "FR6600.AD41CODSECCION=" & lvwSec.ListItems(i).Tag
          End If
        End If
      Next i
      If contSecciones > 0 Then
        glstrWhere = glstrWhere & ") "
      End If
    End If
  End If
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    Call Refrescar_Grid
  End If
  Screen.MousePointer = vbDefault
  Me.Enabled = True

  If optOMPRN(4) And IsDate(dtcDispensacion) And cboservicio <> "" Then
    If auxDBGrid1(0).Rows > 0 Then
      tlbToolbar1.Buttons(6).Enabled = True
    End If
  Else
    tlbToolbar1.Buttons(6).Enabled = False
  End If

End Sub

Private Sub Command1_Click()
  Dim strCodServ As String
  Dim strUpd As String
  Dim qryUpd As rdoQuery
  Dim rstUpd As rdoResultset
  Command1.Enabled = False
  If optCesta(0).Value = True And optOMPRN(1).Value = True Then
    If chkservicio.Value = 1 Then
      On Error GoTo Error_Recogida_Cesta
      strUpd = "UPDATE FR6600 " & _
               "   SET FR66FECENVIO=SYSDATE,FR26CODESTPETIC = ? " & _
               " WHERE (FR66INDOM = ? OR FR66INDOM IS NULL) " & _
               "   AND FR66INDCESTA = ? AND FR26CODESTPETIC = ? "
      Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
      qryUpd(0) = 3
      qryUpd(1) = 0
      qryUpd(2) = -1
      qryUpd(3) = 1
      qryUpd.Execute
      Screen.MousePointer = vbHourglass
      'Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
      'objWinInfo.DataRefresh
      Call Refrescar_Grid
      Screen.MousePointer = vbDefault
    Else
      On Error GoTo Error_Recogida_Cesta
      If Len(Trim(cboservicio.Columns(1).Value)) > 0 Then
        strCodServ = cboservicio.Columns(0).Value
        strUpd = "UPDATE FR6600 " & _
                 "   SET FR66FECENVIO=SYSDATE,FR26CODESTPETIC = ? " & _
                 " WHERE AD02CODDPTO = ? " & _
                 "   AND (FR66INDOM = ? OR FR66INDOM IS NULL) " & _
                 "   AND FR66INDCESTA = ?  AND FR26CODESTPETIC = ? "
        Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
        qryUpd(0) = 3
        qryUpd(1) = strCodServ
        qryUpd(2) = 0
        qryUpd(3) = -1
        qryUpd(4) = 1
        qryUpd.Execute
        Screen.MousePointer = vbHourglass
        'Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
        'objWinInfo.DataRefresh
        Call Refrescar_Grid
        Screen.MousePointer = vbDefault
      Else
        Screen.MousePointer = vbDefault
        MsgBox "No se han podido recoger las cestas por problemas en el departamento", vbInformation, "Recoger Cestas"
        Command1.Enabled = True
        Exit Sub
      End If
    End If
  Else
    Screen.MousePointer = vbDefault
    MsgBox "Solo pueden recogerse las Cestas Redactadas", vbInformation, "Recoger Cestas"
    Command1.Enabled = True
  End If
  Command1.Enabled = True
  Exit Sub
Error_Recogida_Cesta:
  Screen.MousePointer = vbDefault
  MsgBox "Se han producido incidencias durente la recogida de cestas", vbInformation, "Recoger Cestas"
  Command1.Enabled = True
End Sub

Private Sub dtcDispensacion_Change()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxDBGrid1(0).RemoveAll
End Sub

Private Sub dtcDispensacion_CloseUp()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxDBGrid1(0).RemoveAll
End Sub


Private Sub dtcEnvio_Change()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxDBGrid1(0).RemoveAll
End Sub

Private Sub dtcEnvio_CloseUp()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxDBGrid1(0).RemoveAll
End Sub


Private Sub dtcredaccion_Change()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxDBGrid1(0).RemoveAll
End Sub

Private Sub dtcredaccion_CloseUp()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxDBGrid1(0).RemoveAll
End Sub



Private Sub lvwSec_Click()
Dim i%
If lvwSec.ListItems.Count > 0 Then
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  If lvwSec.ListItems(1).Selected Then
      For i = 2 To lvwSec.ListItems.Count
          lvwSec.ListItems(i).Selected = False
      Next i
  End If
  auxDBGrid1(0).RemoveAll
End If
End Sub

Private Sub optCesta_Click(Index As Integer)
  
  If optOMPRN(1).Value = True And optCesta(0).Value = True Then
    Command1.Enabled = True
  Else
    Command1.Enabled = False
  End If
If optCesta(1).Value = True Then 'PRN
  lvwSec.Visible = False
  Label1(1).Visible = False
Else
  If cboservicio <> "" Then
    Call pCargarSecciones(cboservicio)
  End If
End If

auxDBGrid1(0).RemoveAll

End Sub

Private Sub optOMPRN_Click(Index As Integer)

  If optOMPRN(1).Value = True And optCesta(0).Value = True Then
    Command1.Enabled = True
  Else
    Command1.Enabled = False
  End If
  
  Select Case Index
  Case 1
    lblEnvio.Visible = False
    dtcEnvio.Text = ""
    dtcEnvio.Visible = False
    lblDispen.Visible = False
    dtcDispensacion.Text = ""
    dtcDispensacion.Visible = False
    cmdCesta.Enabled = False
  Case 2
    lblEnvio.Visible = True
    dtcEnvio.Text = ""
    dtcEnvio.Visible = True
    lblDispen.Visible = False
    dtcDispensacion.Text = ""
    dtcDispensacion.Visible = False
    cmdCesta.Enabled = True
    'cmdCesta.Caption = "&Preparar"
  Case 4
    lblEnvio.Visible = True
    dtcEnvio.Text = ""
    dtcEnvio.Visible = True
    lblDispen.Visible = True
    dtcDispensacion.Text = ""
    dtcDispensacion.Visible = True
    cmdCesta.Enabled = False
    'cmdCesta.Caption = "Cons.&Acum."
  Case 5
    lblEnvio.Visible = True
    dtcEnvio.Text = ""
    dtcEnvio.Visible = True
    lblDispen.Visible = True
    dtcDispensacion.Text = ""
    dtcDispensacion.Visible = True
    cmdCesta.Enabled = False
  Case Else
    lblEnvio.Visible = True
    dtcEnvio.Text = ""
    dtcEnvio.Visible = True
    lblDispen.Visible = True
    dtcDispensacion.Text = ""
    dtcDispensacion.Visible = True
    cmdCesta.Enabled = True
    'cmdCesta.Caption = "&Preparar"
  End Select
  

auxDBGrid1(0).RemoveAll

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


Private Sub Form_Activate()
  Dim stra As String
  Dim rsta As rdoResultset
  
If blnInload = True Then
  blnInload = False

  cboservicio.RemoveAll
  stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
         "WHERE AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND" & _
         "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL))) ORDER BY AD02DESDPTO"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While (Not rsta.EOF)
      Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
      rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  optCesta(1).Value = True 'PRN
  lvwSec.Visible = False
  Label1(1).Visible = False
  optOMPRN(2).Value = True
  Screen.MousePointer = vbDefault
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
'Dim objMultiInfo As New clsCWForm
'Dim strKey As String
Dim J As Integer
Dim intDptoSel As Integer
  
  
    Set objWinInfo = New clsCWWin

    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)

'    With objMultiInfo
'        .strName = "Petici�n"
'        Set .objFormContainer = fraFrame1(2)
'        Set .objFatherContainer = Nothing
'        Set .tabMainTab = Nothing
'        Set .grdGrid = grddbgrid1(1)
'        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
'        .strTable = "FR6600"
'        .intAllowance = cwAllowReadOnly
'
'        .strWhere = "-1=0" 'el grid vac�o
'
'        Call .FormAddOrderField("FR66CODPETICION", cwDescending)
'
'        strKey = .strDataBase & .strTable
'        Call .FormCreateFilterWhere(strKey, "Petici�n")
'        Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
'        Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
'        Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
'        Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
'        Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
'        Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
'        Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
'        Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
'        Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
'        Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
'        Call .FormAddFilterWhere(strKey, "FR66INDMEDINF", "Medicaci�n Infantil?", cwBoolean)
'        Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
'        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
'    End With
'
'    With objWinInfo
'
'        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
'
'        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
'        Call .GridAddColumn(objMultiInfo, "C�digo Petici�n", "FR66CODPETICION", cwNumeric, 9)
'        Call .GridAddColumn(objMultiInfo, "Cesta", "FR66INDCESTA", cwBoolean)
'        Call .GridAddColumn(objMultiInfo, "C�d.Estado", "FR26CODESTPETIC", cwNumeric, 1)
'        Call .GridAddColumn(objMultiInfo, "Estado", "", cwString, 30)
'        Call .GridAddColumn(objMultiInfo, "Fecha Emisi�n", "FR66FECREDACCION", cwDate)
'        Call .GridAddColumn(objMultiInfo, "Hora", "FR66HORAREDACCI", cwDecimal, 2)
'        Call .GridAddColumn(objMultiInfo, "Fecha Firma", "FR66FECFIRMMEDI", cwDate)
'        Call .GridAddColumn(objMultiInfo, "Hora Firma", "FR66HORAFIRMMEDI", cwDecimal, 2)
'        Call .GridAddColumn(objMultiInfo, "C�digo Persona", "CI21CODPERSONA", cwNumeric, 7)
'        Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7)
'        Call .GridAddColumn(objMultiInfo, "Cama", "", cwString, 7)
'        Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25)
'        Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25)
'        Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25)
'        Call .GridAddColumn(objMultiInfo, "C�d.M�dico", "SG02COD_MED", cwString, 6)
'        Call .GridAddColumn(objMultiInfo, "Dr.", "", cwString, 30)
'        Call .GridAddColumn(objMultiInfo, "C�d.Urgencia", "FR91CODURGENCIA", cwNumeric, 2)
'        Call .GridAddColumn(objMultiInfo, "Urgencia", "", cwString, 6)
'        Call .GridAddColumn(objMultiInfo, "C�d.Servicio", "AD02CODDPTO", cwNumeric, 3)
'        Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 30)
'        Call .GridAddColumn(objMultiInfo, "Proceso", "AD07CODPROCESO", cwNumeric, 10)
'        Call .GridAddColumn(objMultiInfo, "Asistencia", "AD01CODASISTENCI", cwNumeric, 10)
'        Call .FormCreateInfo(objMultiInfo)
'        Call .FormChangeColor(objMultiInfo)
'        'Se indican los campos por los que se desea buscar
'        .CtrlGetInfo(grddbgrid1(1).Columns(3)).blnInFind = True
'        .CtrlGetInfo(grddbgrid1(1).Columns(4)).blnInFind = True
'        .CtrlGetInfo(grddbgrid1(1).Columns(5)).blnInFind = True
'        .CtrlGetInfo(grddbgrid1(1).Columns(7)).blnInFind = True
'        .CtrlGetInfo(grddbgrid1(1).Columns(8)).blnInFind = True
'        .CtrlGetInfo(grddbgrid1(1).Columns(9)).blnInFind = True
'        .CtrlGetInfo(grddbgrid1(1).Columns(10)).blnInFind = True
'        .CtrlGetInfo(grddbgrid1(1).Columns(11)).blnInFind = True
'        .CtrlGetInfo(grddbgrid1(1).Columns(17)).blnInFind = True
'        .CtrlGetInfo(grddbgrid1(1).Columns(19)).blnInFind = True
'        .CtrlGetInfo(grddbgrid1(1).Columns(21)).blnInFind = True
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(5)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(5)), grddbgrid1(1).Columns(6), "FR26DESESTADOPET")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(11)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(11)), grddbgrid1(1).Columns(12), "CI22NUMHISTORIA")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(11)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(11)), grddbgrid1(1).Columns(14), "CI22NOMBRE")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(11)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(11)), grddbgrid1(1).Columns(15), "CI22PRIAPEL")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(11)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(11)), grddbgrid1(1).Columns(16), "CI22SEGAPEL")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(17)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(17)), grddbgrid1(1).Columns(18), "SG02APE1")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(19)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(19)), grddbgrid1(1).Columns(20), "FR91DESURGENCIA")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(21)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(21)), grddbgrid1(1).Columns(22), "AD02DESDPTO")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns("Asistencia")), "AD01CODASISTENCI", "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD01CODASISTENCI= ? AND AD07CODPROCESO=?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns("Asistencia")), grddbgrid1(1).Columns("Cama"), "GCFN06(AD15CODCAMA)")
'
'        'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(11)), "CI21CODPERSONA", "SELECT * FROM FR2200J WHERE FR2200J.CI21CODPERSONA= ?")
'        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(11)), grdDBGrid1(1).Columns("Cama"), "DESCCAMA")
'
'        Call .WinRegister
'        Call .WinStabilize
'    End With
    
    blnInload = True
    
auxDBGrid1(0).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
For J = 0 To 21
  Call auxDBGrid1(0).Columns.Add(J)
Next J
auxDBGrid1(0).Columns(0).Caption = "C�digo Petici�n"
auxDBGrid1(0).Columns(0).Visible = True
auxDBGrid1(0).Columns(0).Locked = True
auxDBGrid1(0).Columns(1).Caption = "Cesta"
auxDBGrid1(0).Columns(1).Visible = False
auxDBGrid1(0).Columns(1).Locked = True
auxDBGrid1(0).Columns(2).Caption = "C�d.Estado"
auxDBGrid1(0).Columns(2).Visible = False
auxDBGrid1(0).Columns(2).Locked = True
auxDBGrid1(0).Columns(3).Caption = "Estado"
auxDBGrid1(0).Columns(3).Visible = False
auxDBGrid1(0).Columns(3).Locked = True
auxDBGrid1(0).Columns(4).Caption = "Fecha Redacci�n"
auxDBGrid1(0).Columns(4).Visible = True
auxDBGrid1(0).Columns(4).Locked = True
auxDBGrid1(0).Columns(5).Caption = "Hora"
auxDBGrid1(0).Columns(5).Visible = True
auxDBGrid1(0).Columns(5).Locked = True
auxDBGrid1(0).Columns(6).Caption = "Fecha Firma"
auxDBGrid1(0).Columns(6).Visible = False
auxDBGrid1(0).Columns(6).Locked = True
auxDBGrid1(0).Columns(7).Caption = "Hora Firma"
auxDBGrid1(0).Columns(7).Visible = False
auxDBGrid1(0).Columns(7).Locked = True
auxDBGrid1(0).Columns(8).Caption = "C�digo Persona"
auxDBGrid1(0).Columns(8).Visible = False
auxDBGrid1(0).Columns(8).Locked = True
auxDBGrid1(0).Columns(9).Caption = "Historia"
auxDBGrid1(0).Columns(9).Visible = True
auxDBGrid1(0).Columns(9).Locked = True
auxDBGrid1(0).Columns(10).Caption = "Cama"
auxDBGrid1(0).Columns(10).Visible = True
auxDBGrid1(0).Columns(10).Locked = True
auxDBGrid1(0).Columns(11).Caption = "Nombre"
auxDBGrid1(0).Columns(11).Visible = True
auxDBGrid1(0).Columns(11).Locked = True
auxDBGrid1(0).Columns(12).Caption = "Apellido 1�"
auxDBGrid1(0).Columns(12).Visible = True
auxDBGrid1(0).Columns(12).Locked = True
auxDBGrid1(0).Columns(13).Caption = "Apellido 2�"
auxDBGrid1(0).Columns(13).Visible = True
auxDBGrid1(0).Columns(13).Locked = True
auxDBGrid1(0).Columns(14).Caption = "C�d.M�dico"
auxDBGrid1(0).Columns(14).Visible = False
auxDBGrid1(0).Columns(14).Locked = True
auxDBGrid1(0).Columns(15).Caption = "Dr."
auxDBGrid1(0).Columns(15).Visible = True
auxDBGrid1(0).Columns(15).Locked = True
auxDBGrid1(0).Columns(16).Caption = "C�d.Urgencia"
auxDBGrid1(0).Columns(16).Visible = False
auxDBGrid1(0).Columns(16).Locked = True
auxDBGrid1(0).Columns(17).Caption = "Urgencia"
auxDBGrid1(0).Columns(17).Visible = True
auxDBGrid1(0).Columns(17).Locked = True
auxDBGrid1(0).Columns(18).Caption = "C�d.Servicio"
auxDBGrid1(0).Columns(18).Visible = True
auxDBGrid1(0).Columns(18).Locked = True
auxDBGrid1(0).Columns(19).Caption = "Servicio"
auxDBGrid1(0).Columns(19).Visible = True
auxDBGrid1(0).Columns(19).Locked = True
''''''''''''''''''''''''''''''''''''''''''''''''''
auxDBGrid1(0).Columns(20).Caption = "C�d.Secci�n"
auxDBGrid1(0).Columns(20).Visible = True
auxDBGrid1(0).Columns(20).Locked = True
auxDBGrid1(0).Columns(21).Caption = "Secci�n"
auxDBGrid1(0).Columns(21).Visible = True
auxDBGrid1(0).Columns(21).Locked = True

''''''''''''''''''''''''''''''''''''''''''''''''''
auxDBGrid1(0).Columns(22).Caption = "Proceso"
auxDBGrid1(0).Columns(22).Visible = False
auxDBGrid1(0).Columns(22).Locked = True
auxDBGrid1(0).Columns(23).Caption = "Asistencia"
auxDBGrid1(0).Columns(23).Visible = False
auxDBGrid1(0).Columns(23).Locked = True
auxDBGrid1(0).Columns(24).Caption = "Fecha Envio"
auxDBGrid1(0).Columns(24).Visible = True
auxDBGrid1(0).Columns(24).Locked = True
auxDBGrid1(0).Columns(25).Caption = "Fecha Dispensacion"
auxDBGrid1(0).Columns(25).Visible = True
auxDBGrid1(0).Columns(25).Locked = True

auxDBGrid1(0).Columns("Urgencia").Position = 21
auxDBGrid1(0).Columns("Fecha Envio").Position = 6
auxDBGrid1(0).Columns("Fecha Dispensacion").Position = 7
    
auxDBGrid1(0).Columns(0).Width = 800 'c�d petici�n
auxDBGrid1(0).Columns(3).Width = 1600 'estado
auxDBGrid1(0).Columns(4).Width = 1200 'fecha redacci�n
auxDBGrid1(0).Columns(5).Width = 700 'hora redacci�n
auxDBGrid1(0).Columns(6).Width = 1200 'fecha firma
auxDBGrid1(0).Columns(7).Width = 500 'hora firma
auxDBGrid1(0).Columns(9).Width = 800 'historia
auxDBGrid1(0).Columns(10).Width = 600 'cama
auxDBGrid1(0).Columns(11).Width = 1530 'nombre
auxDBGrid1(0).Columns(12).Width = 1530 'apellido 1�
auxDBGrid1(0).Columns(13).Width = 1530 'apellido 2�
auxDBGrid1(0).Columns(15).Width = 1600 'doctor
auxDBGrid1(0).Columns(17).Width = 800 'urgencia
auxDBGrid1(0).Columns(18).Width = 750 'servicio

auxDBGrid1(0).Redraw = True

For J = 1 To 29
tlbToolbar1.Buttons(J).Enabled = False
Next J

'se cargan las secciones del Dpto. seleccionado
lvwSec.ColumnHeaders.Add , , , 2000
If cboservicio <> "" Then
  intDptoSel = cboservicio
  Call pCargarSecciones(intDptoSel)
End If
    
End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
'
'  If strFormName = "Petici�n" And strCtrlName = "grdDBGrid1(1).Asistencia" Then
'      'aValues(2) = grdDBGrid1(1).Columns("Proceso").Value  'proceso
'      aValues(2) = objWinInfo.objWinActiveForm.rdoCursor("AD07CODPROCESO").Value
'  End If
'
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
'    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
'    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
'    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
'    Call objWinInfo.WinDeRegister
'    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
'    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
'    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

  '30 Salir
  If btnButton.Index = 30 Then
    Unload Me
  ElseIf btnButton.Index = 6 Then
    If optCesta(1).Value Then
      Call Imprimir("FR1242.RPT", 0)
    Else
      If lvwSec.ListItems.Count > 0 Then
        Call Imprimir("FR1245.RPT", 0)
      Else
        Call Imprimir("FR1241.RPT", 0)
      End If
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 60 'Eliminar
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
    Unload Me
  Case Else
    'Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    'Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    'Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
  'Call objWinInfo.GridDblClick
  'se pasa el n� de orden para ir a frmRedactarOMPRN y firmar
  'If grddbgrid1(1).Columns(3).Value <> "" Then
  '  gintfirmarOM = 1
  '  glngpeticion = grddbgrid1(1).Columns(3).Value
  '  Call objsecurity.LaunchProcess("FR0125")
  'End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    'Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    'Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdDBgrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
'    Dim i As Integer
'
'    If Index = 1 Then
'        If grddbgrid1(1).Columns(5).Value = 5 Or grddbgrid1(1).Columns(5).Value = 4 Then
'          For i = 3 To 21
'              grddbgrid1(1).Columns(i).CellStyleSet "Dispensada"
'          Next i
'        Else
'          For i = 3 To 21
'              grddbgrid1(1).Columns(i).CellStyleSet "NoDispensada"
'          Next i
'        End If
'    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    'Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    'Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    'Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    'Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtHistoria_Change()

auxDBGrid1(0).RemoveAll

End Sub

Private Sub Refrescar_Grid()
Dim strselect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim mvntAcumulado As Variant
Dim mvntAcumuladoDisp As Variant
Dim mvntAcumuladoOri As Variant
Dim mvntProducto As Variant
Dim i As Integer
Dim strsec As String
Dim rstsec As rdoResultset
Dim codigoseccion
Dim seccion

  
Me.Enabled = False
Screen.MousePointer = vbHourglass

auxDBGrid1(0).Redraw = False
auxDBGrid1(0).RemoveAll
  
strselect = "SELECT "
strselect = strselect & " FR6600.FR66CODPETICION"
strselect = strselect & ",FR6600.FR66INDCESTA"
strselect = strselect & ",FR6600.FR26CODESTPETIC"
strselect = strselect & ",FR2600.FR26DESESTADOPET"
strselect = strselect & ",FR6600.FR66FECREDACCION"
strselect = strselect & ",FR6600.FR66HORAREDACCI"
strselect = strselect & ",FR6600.FR66FECFIRMMEDI"
strselect = strselect & ",FR6600.FR66HORAFIRMMEDI"
strselect = strselect & ",FR6600.CI21CODPERSONA"
strselect = strselect & ",CI2200.CI22NUMHISTORIA"
strselect = strselect & ",GCFN06(AD1500.AD15CODCAMA) CAMADESC"
strselect = strselect & ",CI2200.CI22NOMBRE"
strselect = strselect & ",CI2200.CI22PRIAPEL"
strselect = strselect & ",CI2200.CI22SEGAPEL"
strselect = strselect & ",FR6600.SG02COD_MED"
strselect = strselect & ",SG0200.SG02APE1"
strselect = strselect & ",FR6600.FR91CODURGENCIA"
strselect = strselect & ",FR9100.FR91DESURGENCIA"
strselect = strselect & ",FR6600.AD02CODDPTO"
strselect = strselect & ",AD0200.AD02DESDPTO"
strselect = strselect & ",FR6600.AD07CODPROCESO"
strselect = strselect & ",FR6600.AD01CODASISTENCI"
strselect = strselect & ",FR6600.FR66FECENVIO"
strselect = strselect & ",FR6600.FR66FECDISPEN"

strselect = strselect & " FROM FR6600,FR2600,CI2200,SG0200,FR9100,AD1500,AD0200"
strselect = strselect & " WHERE "

strselect = strselect & " FR6600.FR26CODESTPETIC=FR2600.FR26CODESTPETIC"
strselect = strselect & " AND FR6600.CI21CODPERSONA=CI2200.CI21CODPERSONA"
strselect = strselect & " AND FR6600.SG02COD_MED=SG0200.SG02COD(+)"
strselect = strselect & " AND FR6600.FR91CODURGENCIA=FR9100.FR91CODURGENCIA(+)"
strselect = strselect & " AND FR6600.AD07CODPROCESO=AD1500.AD07CODPROCESO(+)"
strselect = strselect & " AND FR6600.AD01CODASISTENCI=AD1500.AD01CODASISTENCI(+)"
strselect = strselect & " AND FR6600.AD02CODDPTO=AD0200.AD02CODDPTO"

strselect = strselect & " AND " & glstrWhere

strselect = strselect & " ORDER BY FR66CODPETICION DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strselect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF
  If Not IsNull(rsta("FR66CODPETICION").Value) Then
    strsec = "SELECT FR6600.AD41CODSECCION,AD4100.AD41DESSECCION" & _
             " FROM FR6600,AD4100 WHERE " & _
             "FR6600.AD41CODSECCION=AD4100.AD41CODSECCION" & " AND " & _
             "FR6600.FR66CODPETICION=" & rsta("FR66CODPETICION").Value
    Set rstsec = objApp.rdoConnect.OpenResultset(strsec)
    If Not rstsec.EOF Then
        If Not IsNull(rstsec.rdoColumns("AD41CODSECCION").Value) _
            And Not IsNull(rstsec.rdoColumns("AD41DESSECCION").Value) Then
                    codigoseccion = rstsec.rdoColumns("AD41CODSECCION").Value
                    seccion = rstsec.rdoColumns("AD41DESSECCION").Value
        Else
                    codigoseccion = ""
                    seccion = ""
        End If
    Else
      codigoseccion = ""
      seccion = ""
    End If
    rstsec.Close
    Set rstsec = Nothing
  End If



  strlinea = rsta("FR66CODPETICION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66INDCESTA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR26CODESTPETIC").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR26DESESTADOPET").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66FECREDACCION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66HORAREDACCI").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66FECFIRMMEDI").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66HORAFIRMMEDI").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CI21CODPERSONA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CI22NUMHISTORIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CAMADESC").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CI22NOMBRE").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CI22PRIAPEL").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CI22SEGAPEL").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("SG02COD_MED").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("SG02APE1").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR91CODURGENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR91DESURGENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD02CODDPTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD02DESDPTO").Value & Chr(vbKeyTab)
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  strlinea = strlinea & codigoseccion & Chr(vbKeyTab)
  strlinea = strlinea & seccion & Chr(vbKeyTab)
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  strlinea = strlinea & rsta("AD07CODPROCESO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD01CODASISTENCI").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66FECENVIO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66FECDISPEN").Value
  auxDBGrid1(0).AddItem strlinea
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
auxDBGrid1(0).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Imprimir(strListado As String, intDes As Integer)
'JMRL 19991125
'Toma como par�metro el listado a imprimir y lo manda a la impresora;
'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
Dim strWhere As String
Dim strDNS As String
Dim strUser As String
Dim strPass As String
Dim strPATH As String
Dim contSecciones As Integer
Dim i As Integer

  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.GetDataBasePasswordOld
  strPATH = "C:\Archivos de programa\cun\rpt\"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  
  If strListado = "FR1245.RPT" Then
    If lvwSec.ListItems.Count > 0 Then
      strWhere = "{FR6604J.AD02CODDPTO}=" & cboservicio
      'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
      contSecciones = 0
      If lvwSec.ListItems.Count > 0 Then
        If lvwSec.ListItems(1).Selected Then
        Else
          For i = 2 To lvwSec.ListItems.Count
            If lvwSec.ListItems(i).Selected = True Then
              If contSecciones = 0 Then
                contSecciones = contSecciones + 1
                strWhere = strWhere & " AND ("
              Else
                strWhere = strWhere & " OR "
              End If
              If i = 2 Then
                strWhere = strWhere & "isnull({FR6604J.AD41CODSECCION})"
              Else
                strWhere = strWhere & "{FR6604J.AD41CODSECCION}=" & lvwSec.ListItems(i).Tag
              End If
            End If
          Next i
          If contSecciones > 0 Then
            strWhere = strWhere & ") "
          End If
        End If
      End If
      strWhere = strWhere & " AND ToText({FR6604J.FR66FECDISPEN}) ='" & dtcDispensacion & "'"
      If IsDate(dtcredaccion) Then
        strWhere = strWhere & " AND ToText({FR6604J.FR66FECREDACCION}) ='" & dtcredaccion & "'"
      End If
      If IsDate(dtcEnvio) Then
        strWhere = strWhere & " AND ToText({FR6604J.FR66FECENVIO}) ='" & dtcEnvio & "'"
      End If
      strWhere = strWhere & " AND ({FR6604J.FR26CODESTPETIC}=5 or {FR6604J.FR26CODESTPETIC}=9)"
    End If
  ElseIf strListado = "FR1241.RPT" Then
    strWhere = "{FR6600.AD02CODDPTO}=" & cboservicio
    strWhere = strWhere & " AND ToText({FR6600.FR66FECDISPEN}) ='" & dtcDispensacion & "'"
    If IsDate(dtcredaccion) Then
      strWhere = strWhere & " AND ToText({FR6600.FR66FECREDACCION}) ='" & dtcredaccion & "'"
    End If
    If IsDate(dtcEnvio) Then
      strWhere = strWhere & " AND ToText({FR6600.FR66FECENVIO}) ='" & dtcEnvio & "'"
    End If
  Else
    strWhere = "{FR6600.AD02CODDPTO}=" & cboservicio
    strWhere = strWhere & " AND ToText({FR6600.FR66FECDISPEN}) ='" & dtcDispensacion & "'"
    If IsDate(dtcredaccion) Then
      strWhere = strWhere & " AND ToText({FR6600.FR66FECREDACCION}) ='" & dtcredaccion & "'"
    End If
    If IsDate(dtcEnvio) Then
      strWhere = strWhere & " AND ToText({FR6600.FR66FECENVIO}) ='" & dtcEnvio & "'"
    End If
  End If
  
  CrystalReport1.SelectionFormula = strWhere
  'On Error GoTo Err_imp1
  CrystalReport1.Action = 1

Err_imp1:
End Sub

Private Sub pCargarSecciones(intDptoSel%)
Dim SQL$, qry As rdoQuery, rs As rdoResultset
Dim item As ListItem
    
    'se cargan los secciones del Dpto. seleccionado
    lvwSec.ListItems.Clear
    If optCesta(1).Value = True Then 'PRN
      lvwSec.Visible = False
      Label1(1).Visible = False
      Exit Sub
    End If

    SQL = "SELECT AD41CODSECCION,AD41DESSECCION"
    SQL = SQL & " FROM AD4100"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND SYSDATE BETWEEN AD41FECINICIO AND NVL(AD41FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If Not rs.EOF Then
      Set item = lvwSec.ListItems.Add(, , "TODAS")
      item.Tag = 0
      item.Selected = True
      Set item = lvwSec.ListItems.Add(, , "Sin Secci�n")
      item.Tag = intDptoSel
      lvwSec.Visible = True
      Label1(1).Visible = True
    Else
      lvwSec.Visible = False
      Label1(1).Visible = False
    End If
    
    Do While Not rs.EOF
        Set item = lvwSec.ListItems.Add(, , rs!AD41DESSECCION)
        item.Tag = rs!AD41CODSECCION
        rs.MoveNext
    Loop
    rs.Close
    qry.Close

End Sub



