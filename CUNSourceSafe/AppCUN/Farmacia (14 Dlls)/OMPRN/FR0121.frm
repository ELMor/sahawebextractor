VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmFirmarOMPRNPOM 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Firmar OM / PRN"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0121.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Servicio :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1215
      Left            =   5040
      TabIndex        =   9
      Top             =   480
      Width           =   6855
      Begin VB.TextBox txtServicio 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   1560
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Tag             =   "Desc.Servicio"
         Top             =   360
         Width           =   5085
      End
      Begin VB.CheckBox chkservicio 
         Caption         =   "Todos los servicios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   840
         Width           =   2175
      End
      Begin SSDataWidgets_B.SSDBCombo cboservicio 
         Height          =   315
         Left            =   240
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   360
         Width           =   1215
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2223
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2143
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Seleccionar :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1215
      Left            =   120
      TabIndex        =   8
      Top             =   480
      Width           =   4455
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   3120
         TabIndex        =   11
         Top             =   600
         Width           =   1215
      End
      Begin VB.OptionButton optOMPRN 
         Caption         =   "Option1"
         Height          =   195
         Index           =   0
         Left            =   3240
         TabIndex        =   0
         Top             =   720
         Width           =   1095
      End
      Begin VB.OptionButton optOMPRN 
         Caption         =   "OM / PRN"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   3
         Left            =   2880
         TabIndex        =   3
         Top             =   360
         Width           =   1215
      End
      Begin VB.OptionButton optOMPRN 
         Caption         =   "PRN"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   2
         Left            =   1560
         TabIndex        =   2
         Top             =   360
         Width           =   855
      End
      Begin VB.OptionButton optOMPRN 
         Caption         =   "OM"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   1
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "OM / PRN sin firmar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6135
      Index           =   2
      Left            =   120
      TabIndex        =   4
      Tag             =   "Actuaciones Asociadas"
      Top             =   1800
      Width           =   11775
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   5655
         Index           =   1
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   360
         Width           =   11610
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   20479
         _ExtentY        =   9975
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmFirmarOMPRNPOM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0169.FRM                                                   *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: firmar OM/PRN                                           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1



Private Sub cboservicio_CloseUp()
If chkservicio = 0 Then
    If optOMPRN(1).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL AND AD02CODDPTO=" & cboservicio.Text
    End If
    If optOMPRN(2).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL AND AD02CODDPTO=" & cboservicio.Text
    End If
    If optOMPRN(3).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=-1 OR FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL AND AD02CODDPTO=" & cboservicio.Text
    End If
Else 'todos los servicios
    If optOMPRN(1).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL"
    End If
    If optOMPRN(2).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL"
    End If
    If optOMPRN(3).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=-1 OR FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL"
    End If
End If
txtServicio.Text = cboservicio.Columns(1).Value
objWinInfo.DataRefresh

End Sub



Private Sub chkservicio_Click()
If chkservicio = 1 Then
    cboservicio.Text = ""
    txtServicio.Text = ""
    If optOMPRN(1).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL"
    End If
    If optOMPRN(2).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL"
    End If
    If optOMPRN(3).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=-1 OR FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL"
    End If
Else
    If cboservicio <> "" Then
        cboservicio.Text = cboservicio.Columns(0).Value
        txtServicio.Text = cboservicio.Columns(1).Value
        If optOMPRN(1).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL AND AD02CODDPTO=" & cboservicio.Text
        End If
        If optOMPRN(2).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL AND AD02CODDPTO=" & cboservicio.Text
        End If
        If optOMPRN(3).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=-1 OR FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL AND AD02CODDPTO=" & cboservicio.Text
        End If
    End If
End If
objWinInfo.DataRefresh
End Sub

Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset
cboservicio.RemoveAll
stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
            "WHERE AD32CODTIPODPTO=3 AND " & _
            "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
            "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While (Not rsta.EOF)
    Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
    rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing

objWinInfo.DataRefresh
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "OM/PRN"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "FR6600"
        .intAllowance = cwAllowReadOnly
        .strWhere = "FR26CODESTPETIC=1 AND FR66CODPETICION IS NULL" 's�lo las OM/PRN sin firmar(estado=redactada)
        
        Call .FormAddOrderField("FR66CODPETICION", cwAscending)
    
        strKey = .strDataBase & .strTable
        Call .FormCreateFilterWhere(strKey, "OM")
        Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
        Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
        Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
        Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
        Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
        Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
        Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
        Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
        Call .FormAddFilterWhere(strKey, "FR66INDMEDINF", "Medicaci�n Infantil?", cwBoolean)
        Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�digo Petici�n", "FR66CODPETICION", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Estado", "FR26CODESTPETIC", cwNumeric, 1)
        Call .GridAddColumn(objMultiInfo, "Desc.Estado", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Fecha", "FR66FECREDACCION", cwDate)
        Call .GridAddColumn(objMultiInfo, "Hora", "FR66HORAREDACCI", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "C�digo Persona", "CI21CODPERSONA", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "C�d.M�dico", "SG02COD_MED", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Dr.", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d.Urgencia", "FR91CODURGENCIA", cwNumeric, 2)
        Call .GridAddColumn(objMultiInfo, "Urgencia", "", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "C�d.Servicio", "AD02CODDPTO", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 30)
        
        
        Call .FormCreateInfo(objMultiInfo)
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(13)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(15)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnInFind = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(5), "FR26DESESTADOPET")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(9), "CI22NUMHISTORIA")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(10), "CI22NOMBRE")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(11), "CI22PRIAPEL")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(12), "CI22SEGAPEL")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(13)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(13)), grdDBGrid1(1).Columns(14), "SG02APE1")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(15)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91DESURGENCIA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(15)), grdDBGrid1(1).Columns(16), "FR91CODURGENCIA")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(17)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(17)), grdDBGrid1(1).Columns(18), "AD02DESDPTO")
   
        Call .WinRegister
        Call .WinStabilize
    End With
    
    grdDBGrid1(1).Columns(4).Visible = False 'c�d.estado
    grdDBGrid1(1).Columns(5).Visible = False 'desc.estado
    grdDBGrid1(1).Columns(8).Visible = False 'c�d.paciente
    grdDBGrid1(1).Columns(13).Visible = False 'c�d.m�dico
    grdDBGrid1(1).Columns(15).Visible = False 'c�d.urgencia
    grdDBGrid1(1).Columns(17).Visible = False 'c�d.servicio
    
    grdDBGrid1(1).Columns(3).Width = 1000 'c�d petici�n
    grdDBGrid1(1).Columns(6).Width = 1200 'fecha
    grdDBGrid1(1).Columns(7).Width = 700 'hora
    grdDBGrid1(1).Columns(9).Width = 1000 'historia
    grdDBGrid1(1).Columns(10).Width = 1000 'nombre
    grdDBGrid1(1).Columns(11).Width = 1600 'apellido 1�
    grdDBGrid1(1).Columns(12).Width = 1600 'apellido 2�
    grdDBGrid1(1).Columns(14).Width = 1600 'doctor
    grdDBGrid1(1).Columns(16).Width = 800 'urgencia
    grdDBGrid1(1).Columns(18).Width = 750 'servicio
    
    
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub


Private Sub optOMPRN_Click(Index As Integer)
Dim mensaje As String
If Index <> 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    If cboservicio.Text = "" And chkservicio = 0 Then
        mensaje = MsgBox("No hay ning�n servicio seleccionado.", vbInformation, "Aviso")
    Else
    If chkservicio = 0 Then
        Select Case Index
            Case 1
              objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL AND AD02CODDPTO=" & cboservicio.Text
            Case 2
              objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL AND AD02CODDPTO=" & cboservicio.Text
            Case 3
              objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=-1 OR FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL AND AD02CODDPTO=" & cboservicio.Text
        End Select
    Else 'todos los servicios
        Select Case Index
        Case 1
          objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL"
        Case 2
          objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL"
        Case 3
          objWinInfo.objWinActiveForm.strWhere = "(FR66INDOM=-1 OR FR66INDOM=0 OR FR66INDOM IS NULL) AND FR26CODESTPETIC=1 AND FR66CODPETICION IS NOT NULL"
    End Select
    End If
    objWinInfo.DataRefresh
    End If
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
Call objWinInfo.GridDblClick
'se pasa el n� de orden para ir a frmRedactarOMPRN y firmar
gintfirmarOM = 1
glngpeticion = grdDBGrid1(1).Columns(3).Value
Call objsecurity.LaunchProcess("FR0111")
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


