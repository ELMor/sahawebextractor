VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEstPetFarmOM_2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Estudiar Petici�n Farmacia OM"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdSustMed2 
      Caption         =   "Sust Medicamento 2"
      Height          =   375
      Left            =   10320
      TabIndex        =   84
      Top             =   6360
      Width           =   1575
   End
   Begin VB.CommandButton cmdSustSol 
      Caption         =   "Sust Soluci�n"
      Height          =   375
      Left            =   10320
      TabIndex        =   83
      Top             =   5760
      Width           =   1575
   End
   Begin VB.CommandButton cmdSustituir 
      Caption         =   "Sust Medicamento"
      Height          =   375
      Left            =   10320
      TabIndex        =   70
      Top             =   5160
      Width           =   1575
   End
   Begin VB.CommandButton cmdValidar 
      Caption         =   "Validar"
      Height          =   375
      Left            =   10440
      TabIndex        =   69
      Top             =   2760
      Width           =   1455
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4080
      Index           =   0
      Left            =   120
      TabIndex        =   67
      Top             =   3840
      Width           =   10095
      Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
         Height          =   3555
         Index           =   0
         Left            =   120
         TabIndex        =   68
         Top             =   360
         Width           =   9825
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   1
         stylesets(0).Name=   "Bloqueada"
         stylesets(0).ForeColor=   16777215
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "FR0179seg.frx":0000
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   17330
         _ExtentY        =   6271
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "Anular Petici�n"
      Height          =   375
      Left            =   10320
      TabIndex        =   61
      Top             =   7200
      Width           =   1575
   End
   Begin VB.CommandButton cmdbloquear 
      Caption         =   "Bloq / Desbloq"
      Height          =   375
      Left            =   10320
      TabIndex        =   60
      Top             =   4560
      Width           =   1575
   End
   Begin VB.CommandButton cmdperfil 
      Caption         =   "Perfil FTP"
      Height          =   375
      Left            =   10440
      TabIndex        =   59
      Top             =   1560
      Width           =   1455
   End
   Begin VB.CommandButton cmdhistoria 
      Caption         =   "Historia Cl�nica"
      Height          =   375
      Left            =   10440
      TabIndex        =   58
      Top             =   2160
      Width           =   1455
   End
   Begin VB.CommandButton cmdalergia 
      Caption         =   "Alergias"
      Height          =   375
      Left            =   10440
      TabIndex        =   57
      Top             =   960
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Productos"
      Height          =   375
      Left            =   0
      TabIndex        =   43
      Top             =   0
      Width           =   855
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Index           =   1
      Left            =   120
      TabIndex        =   34
      Top             =   480
      Width           =   10260
      Begin TabDlg.SSTab tabTab1 
         Height          =   2775
         Index           =   0
         Left            =   120
         TabIndex        =   35
         TabStop         =   0   'False
         Top             =   360
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   4895
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0179seg.frx":001C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(28)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(14)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(13)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "tab1"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkCheck1(12)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "chkCheck1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "chkCheck1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "chkCheck1(7)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "chkCheck1(15)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(25)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(26)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "chkCheck1(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0179seg.frx":0038
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grddbgrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Cesta"
            DataField       =   "FR66INDCESTA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   7560
            TabIndex        =   30
            Tag             =   "Cesta?"
            Top             =   360
            Width           =   1695
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   26
            Left            =   3720
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   25
            Left            =   3360
            TabIndex        =   2
            Tag             =   "Estado Petici�n"
            Top             =   360
            Width           =   315
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Inter�s Cient�fico"
            DataField       =   "FR66INDINTERCIENT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   7560
            TabIndex        =   29
            Tag             =   "Inter�s Cient�fico?"
            Top             =   120
            Width           =   1815
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Restricci�n volumen"
            DataField       =   "FR66INDRESTVOLUM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   5520
            TabIndex        =   28
            Tag             =   "Restricci�n de Volumen?"
            Top             =   600
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Al�rgico"
            DataField       =   "FR66INDALERGIA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   5520
            TabIndex        =   27
            Tag             =   "Al�rgico?"
            Top             =   360
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Diab�tico"
            DataField       =   "FR66INDPACIDIABET"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   5520
            TabIndex        =   26
            Tag             =   "Diab�tico?"
            Top             =   120
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Orden M�dica"
            DataField       =   "FR66INDOM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   7560
            TabIndex        =   31
            Tag             =   "Orden M�dica?"
            Top             =   600
            Width           =   1695
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR66CODOMORIPRN"
            Height          =   330
            Index           =   1
            Left            =   1440
            TabIndex        =   1
            Tag             =   "C�digo OM origen PRN"
            Top             =   360
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR66CODPETICION"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Petici�n"
            Top             =   360
            Width           =   1100
         End
         Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
            Height          =   2505
            Index           =   2
            Left            =   -74880
            TabIndex        =   37
            TabStop         =   0   'False
            Top             =   120
            Width           =   9375
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16536
            _ExtentY        =   4419
            _StockProps     =   79
         End
         Begin TabDlg.SSTab tab1 
            Height          =   1815
            Left            =   120
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   840
            Width           =   9375
            _ExtentX        =   16536
            _ExtentY        =   3201
            _Version        =   327681
            Style           =   1
            Tabs            =   5
            TabsPerRow      =   5
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Paciente"
            TabPicture(0)   =   "FR0179seg.frx":0054
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(9)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(8)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(7)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(4)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(11)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(41)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(38)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(39)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(40)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(42)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(3)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(5)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(4)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(2)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(3)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(23)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "Text2"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(46)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(32)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(33)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "Text1"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(12)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).ControlCount=   22
            TabCaption(1)   =   "M�dico / Enfermera"
            TabPicture(1)   =   "FR0179seg.frx":0070
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(24)"
            Tab(1).Control(1)=   "txtText1(6)"
            Tab(1).Control(2)=   "txtText1(7)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "txtText1(8)"
            Tab(1).Control(4)=   "txtText1(9)"
            Tab(1).Control(5)=   "txtText1(20)"
            Tab(1).Control(6)=   "txtText1(21)"
            Tab(1).Control(7)=   "txtText1(19)"
            Tab(1).Control(7).Enabled=   0   'False
            Tab(1).Control(8)=   "dtcDateCombo1(1)"
            Tab(1).Control(9)=   "dtcDateCombo1(0)"
            Tab(1).Control(10)=   "lblLabel1(12)"
            Tab(1).Control(11)=   "lblLabel1(27)"
            Tab(1).Control(12)=   "lblLabel1(20)"
            Tab(1).Control(13)=   "lblLabel1(5)"
            Tab(1).Control(14)=   "lblLabel1(18)"
            Tab(1).Control(15)=   "lblLabel1(2)"
            Tab(1).Control(16)=   "lblLabel1(22)"
            Tab(1).ControlCount=   17
            TabCaption(2)   =   "Redacci�n"
            TabPicture(2)   =   "FR0179seg.frx":008C
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "txtText1(22)"
            Tab(2).Control(1)=   "txtText1(11)"
            Tab(2).Control(1).Enabled=   0   'False
            Tab(2).Control(2)=   "txtText1(10)"
            Tab(2).Control(3)=   "dtcDateCombo1(2)"
            Tab(2).Control(4)=   "lblLabel1(6)"
            Tab(2).Control(5)=   "lblLabel1(10)"
            Tab(2).Control(6)=   "lblLabel1(0)"
            Tab(2).ControlCount=   7
            TabCaption(3)   =   "Servicio"
            TabPicture(3)   =   "FR0179seg.frx":00A8
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "txtText1(17)"
            Tab(3).Control(1)=   "txtText1(16)"
            Tab(3).Control(2)=   "lblLabel1(1)"
            Tab(3).ControlCount=   3
            TabCaption(4)   =   "Observaciones"
            TabPicture(4)   =   "FR0179seg.frx":00C4
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "txtText1(18)"
            Tab(4).Control(1)=   "lblLabel1(24)"
            Tab(4).ControlCount=   2
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   12
               Left            =   3960
               TabIndex        =   76
               TabStop         =   0   'False
               Tag             =   "Sexo"
               Top             =   720
               Width           =   1140
            End
            Begin VB.TextBox Text1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Left            =   3360
               TabIndex        =   75
               TabStop         =   0   'False
               Tag             =   "Edad"
               Top             =   720
               Width           =   420
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR66ALTUPAC"
               Height          =   330
               Index           =   33
               Left            =   6840
               MaxLength       =   3
               TabIndex        =   74
               Tag             =   "Altura Cm"
               Top             =   720
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR66PESOPAC"
               Height          =   330
               Index           =   32
               Left            =   6120
               MaxLength       =   3
               TabIndex        =   73
               Tag             =   "Peso"
               Top             =   720
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   46
               Left            =   5160
               TabIndex        =   72
               TabStop         =   0   'False
               Tag             =   "Cama"
               Top             =   720
               Width           =   900
            End
            Begin VB.TextBox Text2 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Left            =   7800
               TabIndex        =   71
               Tag             =   "Superficie Corporal"
               Top             =   720
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR91CODURGENCIA"
               Height          =   330
               Index           =   22
               Left            =   -71160
               TabIndex        =   21
               Tag             =   "C�digo Urgencia"
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   11
               Left            =   -70080
               TabIndex        =   22
               TabStop         =   0   'False
               Tag             =   "Desc. Urgencia"
               Top             =   960
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAREDACCI"
               Height          =   330
               Index           =   10
               Left            =   -72840
               TabIndex        =   20
               Tag             =   "Hora Redacci�n"
               Top             =   960
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   -72600
               TabIndex        =   24
               Tag             =   "Desc.Departamento"
               Top             =   840
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   16
               Left            =   -74760
               TabIndex        =   23
               Tag             =   "C�d.Departamento"
               Top             =   840
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66OBSERV"
               Height          =   1050
               Index           =   18
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   25
               Tag             =   "Observaciones"
               Top             =   600
               Width           =   8925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_FEN"
               Height          =   330
               Index           =   24
               Left            =   -66720
               TabIndex        =   18
               Tag             =   "Hora Firma Enfermera"
               Top             =   1440
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   23
               Left            =   1800
               TabIndex        =   5
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   720
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   3
               Left            =   240
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   1320
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   2
               Left            =   240
               TabIndex        =   4
               Tag             =   "C�digo Paciente"
               Top             =   720
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   4
               Left            =   3240
               TabIndex        =   7
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Paciente"
               Top             =   1320
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   6240
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Apellido 2� Paciente"
               Top             =   1320
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "sg02cod_enf"
               Height          =   330
               Index           =   6
               Left            =   -74880
               TabIndex        =   13
               Tag             =   "C�digo Enfermera"
               Top             =   1200
               Width           =   1635
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   7
               Left            =   -73200
               TabIndex        =   14
               TabStop         =   0   'False
               Tag             =   "Nombre Enfermera"
               Top             =   1200
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_MED"
               Height          =   330
               Index           =   8
               Left            =   -74880
               TabIndex        =   9
               Tag             =   "C�digo Doctor"
               Top             =   600
               Width           =   1635
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   9
               Left            =   -73200
               TabIndex        =   10
               Tag             =   "Apellido Doctor"
               Top             =   600
               Width           =   3285
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMMEDI"
               Height          =   330
               Index           =   20
               Left            =   -67680
               TabIndex        =   12
               Tag             =   "hora Firma M�dico"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMENF"
               Height          =   330
               Index           =   21
               Left            =   -67680
               TabIndex        =   17
               Tag             =   "Hora Firma Enfermera"
               Top             =   1200
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   19
               Left            =   -71520
               TabIndex        =   15
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Enfermera"
               Top             =   1200
               Width           =   1605
            End
            Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
               Height          =   4425
               Index           =   3
               Left            =   -74880
               TabIndex        =   40
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
               Height          =   1305
               Index           =   4
               Left            =   -74880
               TabIndex        =   41
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMMEDI"
               Height          =   330
               Index           =   1
               Left            =   -69840
               TabIndex        =   11
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   600
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMENF"
               Height          =   330
               Index           =   0
               Left            =   -69840
               TabIndex        =   16
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   1200
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECREDACCION"
               Height          =   330
               Index           =   2
               Left            =   -74880
               TabIndex        =   19
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   960
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Sexo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   3960
               TabIndex        =   82
               Top             =   480
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Edad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   42
               Left            =   3360
               TabIndex        =   81
               Top             =   480
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Altura Cm"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   40
               Left            =   6840
               TabIndex        =   80
               Top             =   480
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Peso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   39
               Left            =   6120
               TabIndex        =   79
               Top             =   480
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cama"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   38
               Left            =   5160
               TabIndex        =   78
               Top             =   480
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Sup Corporal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   41
               Left            =   7800
               TabIndex        =   77
               Top             =   480
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   -71160
               TabIndex        =   66
               Top             =   720
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   -72840
               TabIndex        =   65
               Top             =   720
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   -74880
               TabIndex        =   64
               Top             =   720
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -74760
               TabIndex        =   63
               Top             =   600
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   -74880
               TabIndex        =   62
               Top             =   360
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.Firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   -66720
               TabIndex        =   55
               Top             =   1200
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Historia "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   1800
               TabIndex        =   54
               Top             =   480
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Persona"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   240
               TabIndex        =   53
               Top             =   480
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   240
               TabIndex        =   52
               Top             =   1080
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 1�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   3240
               TabIndex        =   51
               Top             =   1080
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 2�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   6240
               TabIndex        =   50
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   -74880
               TabIndex        =   49
               Top             =   960
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   -74880
               TabIndex        =   48
               Top             =   360
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   -69840
               TabIndex        =   47
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   -67680
               TabIndex        =   46
               Top             =   360
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   -69840
               TabIndex        =   45
               Top             =   960
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   -67680
               TabIndex        =   44
               Top             =   960
               Width           =   1935
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   3360
            TabIndex        =   56
            Top             =   120
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.OM origen PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   1440
            TabIndex        =   42
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   120
            TabIndex        =   38
            Top             =   120
            Width           =   1455
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   32
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmEstPetFarmOM_2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmEstPetFarm(FR0125.FRM)                                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: estudiar petici�n farmacia                              *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Dim mblnGrabar As Boolean
Dim marca()


Private Sub cmdalergia_Click()
  
  cmdalergia.Enabled = False
  If chkCheck1(2).Value = 1 Then
    Call objsecurity.LaunchProcess("FR0155")
  Else
    Call MsgBox("El Paciente no es al�rgico", vbExclamation, "Farmacia")
  End If
  cmdalergia.Enabled = True

End Sub

Private Sub cmdAnular_Click()
    Dim intMsg As Integer
    Dim strupdate As String
On Error GoTo err
    cmdAnular.Enabled = False
    If frmVisOM.grddbgrid1(1).Columns(5).Value = 1 Then
      Call MsgBox("La OM est� en estado de Redactado", vbInformation, "Aviso")
      cmdAnular.Enabled = True
      Exit Sub
    End If
    If txtText1(0).Text <> "" Then
        intMsg = MsgBox("�Est� seguro que desea Anular la Petici�n?", vbQuestion + vbYesNo)
        If intMsg = vbYes Then
            strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=8 WHERE FR66CODPETICION=" & txtText1(0).Text
            objApp.rdoConnect.Execute strupdate, 64
            objWinInfo.DataRefresh
        End If
    End If
err:
    cmdAnular.Enabled = True
End Sub

'Private Sub cmdDispensar_Click()
'    Dim intMsg As Integer
'    Dim strupdate As String
'    Dim Total As Boolean
'    Dim i As Integer
'    'On Error GoTo err
'    cmdDispensar.Enabled = False
'    If frmVisOM.grddbgrid1(1).Columns(5).Value = 1 Then
'      Call MsgBox("El PRN est� en estado de Redactado", vbInformation, "Aviso")
'      cmdDispensar.Enabled = True
'      Exit Sub
'    End If
'    If txtText1(0).Text <> "" Then
'        intMsg = MsgBox("�Est� seguro que desea dispensar la Petici�n?", vbQuestion + vbYesNo)
'        If intMsg = vbYes Then
'            mblnGrabar = True
'            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'            If mblnGrabar = False Then
'              cmdDispensar.Enabled = True
'              Exit Sub
'            End If
'            Total = True
'            If grddbgrid1(0).Rows > 0 Then
'              grddbgrid1(0).MoveFirst
'              For i = 0 To grddbgrid1(0).Rows - 1
'                If grddbgrid1(0).Columns(16).Value <> grddbgrid1(0).Columns(18).Value _
'                And grddbgrid1(0).Columns(3).Value <> True Then
'                  'no se inserta el producto pues ya est� insertado en el grid
'                  Total = False
'                  Exit For
'                End If
'                grddbgrid1(0).MoveNext
'              Next i
'            End If
'            If Total Then
'              strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=5 WHERE FR66CODPETICION=" & grddbgrid1(0).Columns(4).Value
'              objApp.rdoConnect.Execute strupdate, 64
'              Call Validar_orden
'            Else
'              strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=9 WHERE FR66CODPETICION=" & grddbgrid1(0).Columns(4).Value
'              objApp.rdoConnect.Execute strupdate, 64
'            End If
'            Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
'            objWinInfo.DataRefresh
'        End If
'    End If
'err:
'    cmdDispensar.Enabled = True
'End Sub


Private Sub cmdSustituir_Click()
  Call Sustituir
End Sub

Private Sub cmdSustMed2_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strProducto As String
Dim rstProducto As rdoResultset
Dim strupdate As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String
Dim vntAux As Variant

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If

  cmdSustMed2.Enabled = False

  If txtText1(0).Text <> "" Then
    If CInt(txtText1(25).Text) <> 1 And CInt(txtText1(25).Text) <> 2 And CInt(txtText1(25).Text) <> 3 And txtText1(25).Text <> 9 Then
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else
      gstrLlamadorProd = "frmRedactarOMPRNmed2"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0189")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      gstrLlamadorProd = ""
      If gintprodtotal > 0 Then
        For v = 0 To gintprodtotal - 1
          If gintprodbuscado(v, 0) <> "" Then
            strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                      gintprodbuscado(v, 0)
            Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
            
            strupdate = "UPDATE FR2800 SET "
            strupdate = strupdate & "FR73CODPRODUCTO_2=" & gintprodbuscado(v, 0)
            If Not IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
              strupdate = strupdate & ",FRH7CODFORMFAR_2='" & rstProducto("FRH7CODFORMFAR").Value & "'"
            End If
            'If Not IsNull(rstProducto("FR73DOSIS").Value) Then
            '  strupdate = strupdate & ",FR28DOSIS_2=" & rstProducto("FR73DOSIS").Value
            'End If
            If IsNull(rstProducto("FR73DOSIS").Value) Then
              strupdate = strupdate & ",FR28DOSIS_2=1"
            Else
              If rstProducto("FR73INDDOSISPESO").Value = -1 Then
                If txtText1(32).Text = "" Then
                  Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                  cmdSustMed2.Enabled = True
                  'blnBoton = False
                  gintprodtotal = 0
                  Exit Sub
                Else
                  strupdate = strupdate & ",FR28DOSIS_2=" & rstProducto("FR73DOSIS").Value * txtText1(32).Text
                End If
              Else
                If rstProducto("FR73INDDOSISSUPCORP").Value = -1 Then
                  If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                    Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                    cmdSustMed2.Enabled = True
                    'blnBoton = False
                    gintprodtotal = 0
                    Exit Sub
                  Else
                    strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                    Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                    strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                    Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                    strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                    Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                    intPeso = txtText1(32).Text
                    intAltura = txtText1(33).Text
                    vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                    vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                    vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR73DOSIS").Value
                    vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                    strupdate = strupdate & ",FR28DOSIS_2=" & vntMasa
                    rstPotPeso.Close
                    rstPotAlt.Close
                    rstFactor.Close
                    Set rstPotPeso = Nothing
                    Set rstPotAlt = Nothing
                    Set rstFactor = Nothing
                  End If
                Else
                  vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                  strupdate = strupdate & ",FR28DOSIS_2=" & vntAux
                End If
              End If
            End If

            If Not IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
              strupdate = strupdate & ",FR93CODUNIMEDIDA_2='" & rstProducto("FR93CODUNIMEDIDA").Value & "'"
            End If
            VolTotal = 0
            If grddbgrid1(0).Columns(14).Value = "" Then
              Vol1 = 0
            Else
              Vol1 = CCur(grddbgrid1(0).Columns(14).Value)
            End If
            If grddbgrid1(0).Columns(32).Value = "" Then
              Vol2 = 0
            Else
              Vol2 = CCur(grddbgrid1(0).Columns(32).Value)
            End If
            If IsNull(rstProducto("FR73VOLUMEN").Value) Then
              Vol3 = 0
            Else
              Vol3 = CCur(rstProducto("FR73VOLUMEN").Value)
            End If
            VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
            vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
            strupdate = strupdate & ",FR28VOLTOTAL=" & vntAux
            If grddbgrid1(0).Columns(33).Value = "" Then
              TiemMinInf = 0
            Else
              TiemMinInf = CCur(grddbgrid1(0).Columns(33).Value) / 60
            End If
            If TiemMinInf <> 0 Then
              VolPerf = Format(VolTotal / TiemMinInf, "0.00")
            Else
              VolPerf = 0
            End If
            vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
            strupdate = strupdate & ",FR28VELPERFUSION=" & vntAux
            
            strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
            strupdate = strupdate & " AND FR28NUMLINEA=" & grddbgrid1(0).Columns(5).Value
            
            objApp.rdoConnect.Execute strupdate, 64
            
            rstProducto.Close
            Set rstProducto = Nothing
            
            Call objWinInfo.DataRefresh

          End If
        Next v
      End If
      gintprodtotal = 0
    End If
  End If

cmdSustMed2.Enabled = True
End Sub

Private Sub cmdSustSol_Click()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strProducto As String
Dim rstProducto As rdoResultset
Dim strupdate As String
Dim strInsert As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim vntAux As Variant

'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
'End If
'If tlbToolbar1.Buttons(4).Enabled = True Then
'  Exit Sub
'End If

  cmdSustSol.Enabled = False
  If txtText1(0).Text <> "" Then
    If CInt(txtText1(25).Text) <> 1 And CInt(txtText1(25).Text) <> 2 And CInt(txtText1(25).Text) <> 3 And txtText1(25).Text <> 9 Then
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else
      'If cmdSolDil.Caption = "Soluci�n Intravenosa" Then
      '  gstrLlamadorProd = "frmRedactarOMPRNsolFlui"
      'Else
      '  gstrLlamadorProd = "frmRedactarOMPRNsoldil"
      'End If
      If grddbgrid1(0).Columns(3).Value = True Then
        Call MsgBox("La l�nea est� bloqueada", vbInformation, "Aviso")
        cmdSustSol.Enabled = True
        Exit Sub
      End If
      If grddbgrid1(0).Columns(47).Value = "F" Then
        Call cmdbloquear_Click
        gstrLlamadorProd = "frmRedactarOMPRNsolFlui"
      Else
        gstrLlamadorProd = "frmRedactarOMPRNsoldil"
      End If
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0189")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      gstrLlamadorProd = ""
      If gintprodtotal > 0 Then
        For v = 0 To gintprodtotal - 1
          If gintprodbuscado(v, 0) <> "" Then
            strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                      gintprodbuscado(v, 0)
            Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
              
            If grddbgrid1(0).Columns(47).Value = "F" Then
              strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                txtText1(0).Text
              Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
              If IsNull(rstlinea.rdoColumns(0).Value) Then
                linea = 1
              Else
                linea = rstlinea.rdoColumns(0).Value + 1
              End If
            
              '??????????????????????????-
              strInsert = "INSERT INTO FR2800"
              strInsert = strInsert & " (FR66CODPETICION,FR28NUMLINEA,"
              strInsert = strInsert & "FR73CODPRODUCTO_DIL,FR93CODUNIMEDIDA,"
              strInsert = strInsert & "FR28VOLTOTAL,FR28CANTIDADDIL,FR28TIEMMININF,"
              strInsert = strInsert & "FR28OPERACION,FR28FECINICIO,"
              strInsert = strInsert & "FR28HORAINICIO,"
              strInsert = strInsert & "FR28CANTIDAD,FR28CANTDISP,FRG4CODFRECUENCIA,"
              strInsert = strInsert & "FR34CODVIA,FR28INDVIAOPC,FR28VELPERFUSION,"
              'strinsert = strinsert & "FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2"
              strInsert = strInsert & "FRH5CODPERIODICIDAD"
              strInsert = strInsert & ") VALUES " & _
                        "(" & _
                        txtText1(0).Text & "," & _
                        linea & "," & _
                        gintprodbuscado(v, 0) & ","
              strInsert = strInsert & "null," 'la U.M. es del medicamento
              If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VOLINFIV").Value, ",", ".", 1)
                strInsert = strInsert & vntAux & "," & vntAux & ","
                VolTotal = CCur(rstProducto("FR73VOLINFIV").Value)
              Else
                VolTotal = 0
                strInsert = strInsert & "NULL," & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
                strInsert = strInsert & rstProducto("FR73TIEMINF").Value & ","
              Else
                strInsert = strInsert & "NULL,"
              End If
              strInsert = strInsert & "'F',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),1,1," 'FR28CANTIDAD,FR28CANTDISP
              
              If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                strInsert = strInsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
              Else
                strInsert = strInsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR34CODVIA").Value) Then
                strInsert = strInsert & "'" & rstProducto("FR34CODVIA").Value & "',"
              Else
                strInsert = strInsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                strInsert = strInsert & rstProducto("FR73INDVIAOPCION").Value & ","
              Else
                strInsert = strInsert & "NULL,"
              End If
              If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                strInsert = strInsert & vntAux & ","
              Else
                If grddbgrid1(0).Columns(33).Value = "" Then
                  TiemMinInf = 0
                Else
                  TiemMinInf = CCur(grddbgrid1(0).Columns(33).Value) / 60
                End If
                If TiemMinInf <> 0 Then
                  VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                  vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                  strInsert = strInsert & vntAux & ","
                Else
                  VolPerf = 0
                  strInsert = strInsert & VolPerf & ","
                End If
              End If
              strInsert = strInsert & "'Diario')"
              'If Not IsNull(rstProducto("FR93CODUNIMEDIDA_PERF1").Value) Then
              '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF1").Value & "',"
              'Else
              '  strinsert = strinsert & "NULL,"
              'End If
              'If Not IsNull(rstProducto("FR93CODUNIMEDIDA_PERF2").Value) Then
              '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF2").Value & "')"
              'Else
              '  strinsert = strinsert & "NULL)"
              'End If
              
              objApp.rdoConnect.Execute strInsert, 64
              
              rstProducto.Close
              Set rstProducto = Nothing
              Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
              Call objWinInfo.DataRefresh
            Else
              strupdate = "UPDATE FR2800 SET "
              strupdate = strupdate & "FR73CODPRODUCTO_DIL=" & gintprodbuscado(v, 0)
              If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
                vntAux = objGen.ReplaceStr(rstProducto("FR73VOLINFIV").Value, ",", ".", 1)
                strupdate = strupdate & ",FR28CANTIDADDIL=" & vntAux
              End If
              If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
                strupdate = strupdate & ",FR28TIEMMININF=" & rstProducto("FR73TIEMINF").Value
              End If
              VolTotal = 0
              If grddbgrid1(0).Columns(14).Value = "" Then
                Vol1 = 0
              Else
                Vol1 = CCur(grddbgrid1(0).Columns(14).Value)
              End If
              If IsNull(rstProducto("FR73VOLINFIV").Value) Then
                Vol2 = 0
              Else
                Vol2 = CCur(rstProducto("FR73VOLINFIV").Value)
              End If
              If grddbgrid1(0).Columns(39).Value = "" Then
                Vol3 = 0
              Else
                Vol3 = CCur(grddbgrid1(0).Columns(39).Value)
              End If
              VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
              vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
              strupdate = strupdate & ",FR28VOLTOTAL=" & vntAux
              If grddbgrid1(0).Columns(33).Value = "" Then
                TiemMinInf = 0
              Else
                TiemMinInf = CCur(grddbgrid1(0).Columns(33).Value) / 60
              End If
              If TiemMinInf <> 0 Then
                VolPerf = Format(VolTotal / TiemMinInf, "0.00")
              Else
                VolPerf = 0
              End If
              vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
              strupdate = strupdate & ",FR28VELPERFUSION=" & vntAux
              
              strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
              strupdate = strupdate & " AND FR28NUMLINEA=" & grddbgrid1(0).Columns(5).Value
              
              objApp.rdoConnect.Execute strupdate, 64
              
              rstProducto.Close
              Set rstProducto = Nothing
              
              Call objWinInfo.DataRefresh
            End If
          End If
        Next v
      End If
      gintprodtotal = 0
    End If
  End If

cmdSustSol.Enabled = True
End Sub

'Private Sub CmdTotal_Click()
'    Dim intMsg As Integer
'    Dim strupdate As String
'
'    On Error GoTo err
'    CmdTotal.Enabled = False
'    If frmVisOM.grddbgrid1(1).Columns(5).Value = 1 Then
'      Call MsgBox("La OM est� en estado de Redactado", vbInformation, "Aviso")
'      CmdTotal.Enabled = True
'      Exit Sub
'    End If
'    If txtText1(0).Text <> "" Then
'      intMsg = MsgBox("�Est� seguro que desea dispensar totalmente la Petici�n?", vbQuestion + vbYesNo)
'      If intMsg = vbYes Then
'        strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=5 WHERE FR66CODPETICION=" & grddbgrid1(0).Columns(4).Value
'        objApp.rdoConnect.Execute strupdate, 64
'        objWinInfo.DataRefresh
'        Call Validar_orden
'      End If
'    End If
'err:
'    CmdTotal.Enabled = True
'End Sub

Private Sub cmdValidar_Click()
'procedimiento que coge todos los productos de la orden validada y
'escribe un registro en FR3200
Dim rsta As rdoResultset
Dim stra As String
Dim i As Integer
Dim strInsert As String
Dim hora As Variant
Dim strfec As String
Dim rstfec As rdoResultset
Dim strobser As String
Dim rstobser As rdoResultset
Dim strObservacion As String

If txtText1(25).Text = 3 Then
    'On Error GoTo err
    'se determina la hora del sistema
    strfec = "(SELECT TO_CHAR(SYSDATE,'HH24') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = rstfec.rdoColumns(0).Value
    hora = hora & "."
    strfec = ""
    strfec = "(SELECT TO_CHAR(SYSDATE,'mi') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = hora & rstfec.rdoColumns(0).Value
    rstfec.Close
    Set rstfec = Nothing
    
    strobser = "SELECT FR57OBSERVACION FROM FR5700 WHERE " & _
              "FR57CODPETOBSER=" & txtText1(0).Text & _
              " AND FR57NUMLINEA=" & grddbgrid1(0).Columns(5).Value

    Set rstobser = objApp.rdoConnect.OpenResultset(strobser)
    If Not rstobser.EOF Then
        If IsNull(rstobser.rdoColumns("FR57OBSERVACION").Value) Then
            strObservacion = "Null"
        Else
            strObservacion = "'" & rstobser.rdoColumns("FR57OBSERVACION").Value & "'"
        End If
    Else
        strObservacion = "Null"
    End If
    
    
    strInsert = "insert into fr3200 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO,FR32NUMMODIFIC," & _
                        "FR28DOSIS,FR34CODVIA,FR32VOLUMEN,FR32TIEMINFMIN,FRG4CODFRECUENCIA," & _
                        "FR32OBSERVFARM,SG02COD_FRM,FR32FECMODIVALI,FR32HORAMODIVALI," & _
                        "FR32INDSN,FR93CODUNIMEDIDA,FR32INDISPEN,FR32INDCOMIENINMED," & _
                        "FRH5CODPERIODICIDAD,FRH7CODFORMFAR,FR32OPERACION,FR32CANTIDAD," & _
                        "FR32FECINICIO,FR32HORAINICIO,FR32FECFIN,FR32HORAFIN,FR32INDVIAOPC," & _
                        "FR73CODPRODUCTO_DIL,FR32CANTIDADDIL,FR32TIEMMININF,FR32REFERENCIA," & _
                        "FR32CANTDISP,FR32UBICACION,FR32INDPERF,FR32INSTRADMIN," & _
                        "FR32VELPERFUSION,FR32VOLTOTAL,FR73CODPRODUCTO_2,FRH7CODFORMFAR_2," & _
                        "FR32DOSIS_2,FR93CODUNIMEDIDA_2,FR32DESPRODUCTO,FR32DIATOMAULT," & _
                        "FR32HORATOMAULT,FR32INDESTFAB)"
    strInsert = strInsert & " select FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO,1,FR28DOSIS,FR34CODVIA" & _
                      ",FR28VOLUMEN,FR28TIEMINFMIN,FRG4CODFRECUENCIA," & strObservacion & ",'" & _
                      objsecurity.strUser & "',to_date('" & Date & "','dd/mm/yyyy')," & hora & "," & _
                      "FR28INDSN,FR93CODUNIMEDIDA,FR28INDDISPPRN," & _
                      "FR28INDCOMIENINMED,FRH5CODPERIODICIDAD,FRH7CODFORMFAR,FR28OPERACION," & _
                      "FR28CANTIDAD,FR28FECINICIO,FR28HORAINICIO,FR28FECFIN,FR28HORAFIN,FR28INDVIAOPC," & _
                      "FR73CODPRODUCTO_DIL,FR28CANTIDADDIL,FR28TIEMMININF,FR28REFERENCIA," & _
                      "FR28CANTDISP,FR28UBICACION,FR28INDPERF,FR28INSTRADMIN," & _
                      "FR28VELPERFUSION,FR28VOLTOTAL,FR73CODPRODUCTO_2,FRH7CODFORMFAR_2," & _
                      "FR28DOSIS_2,FR93CODUNIMEDIDA_2,FR28DESPRODUCTO" & ",to_date('" & Date & "','dd/mm/yyyy'),-1,0 " & _
              "From fr2800 where FR66CODPETICION=" & txtText1(0).Text & _
              " and fr28operacion<>'E' and " & _
              "(fr28indbloqueada<>-1 or fr28indbloqueada is null)"
    objApp.rdoConnect.Execute strInsert, 64
    objApp.rdoConnect.Execute "Commit", 64
    
    
    stra = "select count(*) from fr3200 where FR66CODPETICION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    
    If rsta(0).Value = 0 Then
      Call MsgBox("La Orden M�dica no se puede Validar", vbExclamation, "Aviso")
    Else
      strInsert = "UPDATE FR6600 SET FR26CODESTPETIC=8 WHERE CI21CODPERSONA=" & _
                 txtText1(2).Text & " AND FR26CODESTPETIC IN (4,7)"
      objApp.rdoConnect.Execute strInsert, 64
      objApp.rdoConnect.Execute "Commit", 64
      
      strInsert = "UPDATE FR6600 SET FR26CODESTPETIC=4 WHERE FR66CODPETICION=" & _
                    txtText1(0).Text
      objApp.rdoConnect.Execute strInsert, 64
      objApp.rdoConnect.Execute "Commit", 64
      
      'Hay que hacer la insert en FR6500
      Dim strIns65 As String
      strIns65 = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,FR65DOSIS," & _
                 "FR34CODVIA,FRG4CODFRECUENCIA,FR65ADITIVOS,FR65MLPORHORA,FR65GOTASPORMINUTO,SG02COD_ENF," & _
                 "AD02CODDPTO,FR65OBSERV,FR65INDRECETA,FR65CANTRECETA,FR65INDCONSUMIDO,FR65INDQUIROFANO," & _
                 "FR65CANTIDAD,FR93CODUNIMEDIDA) VALUES"
                 
      Call MsgBox("La OM est� Validada", vbInformation, "Aviso")
    End If
    rstobser.Close
    Set rstobser = Nothing
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.DataRefresh
  Else
    If txtText1(25).Text = 4 Then
      Call MsgBox("La OM ya est� Validada", vbInformation, "Aviso")
    Else
      Call MsgBox("La OM debe estar Enviada", vbInformation, "Aviso")
    End If
  End If
Exit Sub

err:
End Sub

Private Sub Form_Activate()
Dim strfecnac As String
Dim rstfecnac As rdoResultset
Dim edad As Integer
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant

    'se calcula la edad
    strfecnac = "SELECT CI22FECNACIM FROM CI2200 WHERE CI21CODPERSONA=" & txtText1(2).Text
    Set rstfecnac = objApp.rdoConnect.OpenResultset(strfecnac)
    If Not rstfecnac.EOF Then
      If Not IsNull(rstfecnac.rdoColumns(0).Value) Then
        edad = DateDiff("d", rstfecnac.rdoColumns(0).Value, Date) \ 365
        Text1.Text = edad
      Else
        Text1.Text = ""
      End If
    Else
        Text1.Text = ""
    End If
    rstfecnac.Close
    Set rstfecnac = Nothing
    
    Text1.Locked = True

    If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
        intPeso = txtText1(32).Text
        intAltura = txtText1(33).Text
        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
        Text2.Text = vntMasa
    End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
On Error GoTo err
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
err:
End Sub

Private Sub Validar_orden()
End Sub

Private Sub cmdbloquear_Click()

  Dim vntDatos(1 To 9) As Variant
  Dim strprod As String
  Dim rstprod As rdoResultset

    cmdbloquear.Enabled = False
    If frmVisOM.grddbgrid1(1).Columns(5).Value = 1 Then
      Call MsgBox("La OM est� en estado de Redactado", vbInformation, "Aviso")
      cmdbloquear.Enabled = True
      Exit Sub
    End If
    If grddbgrid1(0).Rows > 0 Then
      If grddbgrid1(0).Columns(47).Value = "E" Then
          Call MsgBox("El producto es un estupefaciente." & Chr(13) & _
              "No puede bloquear o desbloquear la l�nea.", vbInformation, "Aviso")
          cmdbloquear.Enabled = True
          Exit Sub
      End If
      If txtText1(25).Text = 4 Or txtText1(25).Text = 5 Then 'orden validada
          Call MsgBox("La Orden M�dica ya est� validada o dispensada." & Chr(13) & _
                            "No puede bloquear o desbloquear l�neas.", vbInformation, "Aviso")
      Else
          If grddbgrid1(0).Rows > 0 Then
              vntDatos(1) = grddbgrid1(0).Columns(4).Value
              vntDatos(2) = grddbgrid1(0).Columns(5).Value
              vntDatos(3) = chkCheck1(12).Value
              vntDatos(4) = 0
              vntDatos(5) = 0
              vntDatos(6) = "null"
              vntDatos(7) = 0
              Select Case grddbgrid1(0).Columns(47).Value
                Case "F":
                  vntDatos(8) = grddbgrid1(0).Columns(30).Value
                Case "L":
                  vntDatos(8) = ""
                Case Else:
                  vntDatos(8) = grddbgrid1(0).Columns(6).Value
              End Select
              vntDatos(8) = grddbgrid1(0).Columns(6).Value
              vntDatos(9) = "null"
              Call objsecurity.LaunchProcess("FR0105", vntDatos)
              Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
              objWinInfo.DataRefresh
          End If
      End If
  End If
  cmdbloquear.Enabled = True
End Sub
Private Sub Sustituir()
Dim v As Integer
Dim i As Integer
Dim mensaje As String
Dim strInsert As String
Dim stra As String
Dim rsta As rdoResultset
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstProducto As rdoResultset
Dim strProducto As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim strMasaCorp As String
'Dim strIgAnt As String
'Dim rstIgAnt As rdoResultset
'Dim blnMas As Boolean
Dim strupdate As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim strfec As String
Dim rstfec As rdoResultset
Dim blnIffiv As Boolean
Dim vntAux As Variant
'FR73INDMATIMP
'FR73INDPRODSAN

'''blnBoton = True
cmdSustituir.Enabled = False

  If txtText1(0).Text <> "" Then
    If CInt(txtText1(25).Text) <> 1 And CInt(txtText1(25).Text) <> 2 And CInt(txtText1(25).Text) <> 3 And txtText1(25).Text <> 9 Then
      mensaje = MsgBox("No puede a�adir m�s medicamentos." & Chr(13) & _
                "La OM est� " & txtText1(26).Text, vbInformation, "Aviso")
    Else
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If grddbgrid1(0).Columns(3).Value = True Then
    Call MsgBox("La l�nea est� bloqueada", vbInformation, "Aviso")
    cmdSustituir.Enabled = True
    Exit Sub
  End If
  Select Case grddbgrid1(0).Columns(47).Value
  Case "/" 'Medicamento
      'gstrLlamadorProd = "Producto"
      Call cmdbloquear_Click
      gstrLlamadorProd = "frmRedactarOMPRNMed"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0189")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "M" 'Mezcla IV 2M
      'gstrLlamadorProd = "Producto"
      Call cmdbloquear_Click
      gstrLlamadorProd = "frmRedactarOMPRNMed"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0189")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "P" 'PRN en OM
      'gstrLlamadorProd = "Producto"
      Call cmdbloquear_Click
      gstrLlamadorProd = "frmRedactarOMPRNMed"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0189")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "F" 'Fluidoterapia
    '??????????????????????????
      gstrLlamadorProd = "frmRedactarOMPRNFlui"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0189")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Case "E" 'Estupefaciente
      Call cmdbloquear_Click
      gstrLlamadorProd = "frmRedactarOMPRNEstu"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      glngcodpeticion = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0189")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
'''  Case "L" 'Form.Magistral
      'gstrLlamadorProd = "frmRedactarOMPRNFoMa"
      'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      'glngcodpeticion = txtText1(0).Text
      'Call objsecurity.LaunchProcess("FR0165")
      'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
'''      blnBoton = True
'''      cmdSustituir.Enabled = False
'''      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
'''      Call objWinInfo.CtrlSet(txtText1(31), "NE")
'''      Call objWinInfo.CtrlSet(txtText1(28), 1)
      
'''      strfec = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY'),TO_CHAR(SYSDATE,'HH24') FROM DUAL"
'''      Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
'''      Call objWinInfo.CtrlSet(dtcDateCombo1(3), rstfec.rdoColumns(0).Value)
'''      Call objWinInfo.CtrlSet(txtText1(37), rstfec.rdoColumns(1).Value)
'''      rstfec.Close
'''      Set rstfec = Nothing
      
'''      txtText1(57).SetFocus
 '''     cmdSustituir.Enabled = True
   '''   blnBoton = False
  '''    Exit Sub
  End Select

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If gintprodtotal > 0 Then
        If gstrLlamadorProd <> "frmRedactarOMPRNFlui" Then
          For v = 0 To gintprodtotal - 1
            strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
            txtText1(0).Text
            Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
            If IsNull(rstlinea.rdoColumns(0).Value) Then
              linea = 1
            Else
              linea = rstlinea.rdoColumns(0).Value + 1
            End If
            If gintprodbuscado(v, 0) <> "" Then
              strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
              gintprodbuscado(v, 0)
              Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
              If IsNull(rstProducto("FR73INDINFIV").Value) Then
                blnIffiv = False
              Else
                If rstProducto("FR73INDINFIV").Value = -1 Then
                  blnIffiv = True
                Else
                  blnIffiv = False
                End If
              End If
              If blnIffiv = False Then
                strInsert = "INSERT INTO FR2800 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO," & _
                            "FR28DOSIS,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR28VOLUMEN,FR28VOLTOTAL,FR34CODVIA," & _
                            "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR28OPERACION,FR28FECINICIO," & _
                            "FR28HORAINICIO,FR28INDVIAOPC,FR28CANTIDADDIL,FR28CANTIDAD,FR28CANTDISP,FR28TIEMMININF"
                strInsert = strInsert & ",FR28REFERENCIA,FR28UBICACION,FR28VELPERFUSION"
                ',FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2
                strInsert = strInsert & ",FRH5CODPERIODICIDAD"
                strInsert = strInsert & ") VALUES " & _
                            "(" & _
                            txtText1(0).Text & "," & _
                            linea & "," & _
                            gintprodbuscado(v, 0) & ","
                If IsNull(rstProducto("FR73DOSIS").Value) Then
                  strInsert = strInsert & "1,"
                Else
                  If rstProducto("FR73INDDOSISPESO").Value = -1 Then
                    If txtText1(32).Text = "" Then
                      Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                      cmdSustituir.Enabled = True
                     ''' blnBoton = False
                      gintprodtotal = 0
                      gstrLlamadorProd = ""
                      Exit Sub
                    Else
                      strInsert = strInsert & rstProducto("FR73DOSIS").Value * txtText1(32).Text & ","
                    End If
                  Else
                    If rstProducto("FR73INDDOSISSUPCORP").Value = -1 Then
                      If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                        Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                        cmdSustituir.Enabled = True
                      '''  blnBoton = False
                        gintprodtotal = 0
                        gstrLlamadorProd = ""
                        Exit Sub
                      Else
                        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                        intPeso = txtText1(32).Text
                        intAltura = txtText1(33).Text
                        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR73DOSIS").Value
                        vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                        strInsert = strInsert & vntMasa & ","
                        rstPotPeso.Close
                        rstPotAlt.Close
                        rstFactor.Close
                        Set rstPotPeso = Nothing
                        Set rstPotAlt = Nothing
                        Set rstFactor = Nothing
                      End If
                    Else
                      vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                      strInsert = strInsert & vntAux & ","
                    End If
                  End If
                End If
                'If blnMas Then
                '  If IsNull(rstIgAnt("FRG4CODFRECUENCIA").Value) Then
                '    strInsert = strInsert & "null,"
                '  Else
                '    strInsert = strInsert & "'" & rstIgAnt("FRG4CODFRECUENCIA").Value & "',"
                '  End If
                'Else
                  If IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                    strInsert = strInsert & "null,"
                  Else
                    strInsert = strInsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
                  End If
                'End If
                If IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
                  strInsert = strInsert & "null,"
                Else
                  strInsert = strInsert & "'" & rstProducto("FR93CODUNIMEDIDA").Value & "',"
                End If
                If IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR28VOLTOTAL
                  strInsert = strInsert & "null,null,"
                Else
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
                  strInsert = strInsert & vntAux & "," & vntAux & ","
                End If
                'If blnMas Then
                '  If IsNull(rstIgAnt("FR34CODVIA").Value) Then
                '    strInsert = strInsert & "null,"
                '  Else
                '    strInsert = strInsert & "'" & rstIgAnt("FR34CODVIA").Value & "',"
                '  End If
                'Else
                  If IsNull(rstProducto("FR34CODVIA").Value) Then
                    strInsert = strInsert & "null,"
                  Else
                    strInsert = strInsert & "'" & rstProducto("FR34CODVIA").Value & "',"
                  End If
                'End If
                If IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
                  strInsert = strInsert & "null,"
                Else
                  strInsert = strInsert & "'" & rstProducto("FRH7CODFORMFAR").Value & "',"
                End If
                'If IsNull(rstProducto("FR73CODPRODUCTO_DIL").Value) Or grddbgrid1(0).columns(45).value <> "=" Then
                If IsNull(rstProducto("FR73CODPRODUCTO_REC").Value) Then
                  strInsert = strInsert & "null,"
                Else
                  strInsert = strInsert & rstProducto("FR73CODPRODUCTO_REC").Value & ","
                End If
                'If blnMas Then
                '  If IsNull(rstIgAnt("FR28FECINICIO").Value) Then
                '    strInsert = strInsert & "'" & mstrOperacion & "',null,"
                '  Else
                '    strInsert = strInsert & "'" & mstrOperacion & "'," & "to_date('" & rstIgAnt("FR28FECINICIO").Value & "','dd/mm/yyyy')," ',"
                '  End If
                '  If IsNull(rstIgAnt("FR28HORAINICIO").Value) Then
                '    strInsert = strInsert & "null,"
                '  Else
                '    strInsert = strInsert & "'" & rstIgAnt("FR28HORAINICIO").Value & "',"
                '  End If
                'Else
                '  strinsert = strinsert & "'" & mstrOperacion & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                'End If
                If IsNull(rstProducto("FR73INDESTUPEFACI").Value) Then
                  strInsert = strInsert & "'" & grddbgrid1(0).Columns(47).Value & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                Else
                  If rstProducto("FR73INDESTUPEFACI").Value = -1 Then
                    strInsert = strInsert & "'" & "E" & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                  Else
                    strInsert = strInsert & "'" & grddbgrid1(0).Columns(47).Value & "',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),"
                  End If
                End If
                If IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                  strInsert = strInsert & "null,"
                Else
                  strInsert = strInsert & rstProducto("FR73INDVIAOPCION").Value & ","
                End If
                'If IsNull(rstProducto("FR73VOLINFIV").Value) Or mstrOperacion <> "=" Then
                If IsNull(rstProducto("FR73VOLINFIV").Value) Then
                  strInsert = strInsert & "null,"
                Else
                  strInsert = strInsert & rstProducto("FR73VOLINFIV").Value & ","
                End If
                strInsert = strInsert & "1,1," 'FR28CANTIDAD,FR28CANTDISP
                'If IsNull(rstProducto("FR73TIEMINF").Value) Or mstrOperacion <> "=" Then
                If IsNull(rstProducto("FR73TIEMINF").Value) Then
                  strInsert = strInsert & "null,"
                Else
                  strInsert = strInsert & rstProducto("FR73TIEMINF").Value & ","
                End If
                If IsNull(rstProducto("FR73REFERENCIA").Value) Then
                  strInsert = strInsert & "null,"
                Else
                  strInsert = strInsert & "'" & rstProducto("FR73REFERENCIA").Value & "',"
                End If
                If IsNull(rstProducto("FRH9UBICACION").Value) Then
                  strInsert = strInsert & "null,"
                Else
                  strInsert = strInsert & "'" & rstProducto("FRH9UBICACION").Value & "',"
                End If
                If IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                  strInsert = strInsert & "null,"
                Else
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                  strInsert = strInsert & vntAux & ","
                End If
                strInsert = strInsert & "'Diario')" 'FRH5CODPERIODICIDAD
                'If IsNull(rstProducto("FR93CODUNIMEDIDA_PERF1").Value) Then
                '  strinsert = strinsert & "null,"
                'Else
                '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF1").Value & "',"
                'End If
                'If IsNull(rstProducto("FR93CODUNIMEDIDA_PERF2").Value) Then
                '  strinsert = strinsert & "null)"
                'Else
                '  strinsert = strinsert & "'" & rstProducto("FR93CODUNIMEDIDA_PERF2").Value & "')"
                'End If
              Else 'IV
                strInsert = "INSERT INTO FR2800"
                strInsert = strInsert & " (FR66CODPETICION,FR28NUMLINEA,"
                strInsert = strInsert & "FR73CODPRODUCTO_DIL,FR93CODUNIMEDIDA,"
                strInsert = strInsert & "FR28VOLTOTAL,FR28CANTIDADDIL,FR28TIEMMININF,"
                strInsert = strInsert & "FR28OPERACION,FR28FECINICIO,"
                strInsert = strInsert & "FR28HORAINICIO,"
                strInsert = strInsert & "FR28CANTIDAD,FR28CANTDISP,FRG4CODFRECUENCIA,"
                strInsert = strInsert & "FR34CODVIA,FR28INDVIAOPC,FR28VELPERFUSION,"
                'strinsert = strinsert & "FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2"
                strInsert = strInsert & "FRH5CODPERIODICIDAD"
                strInsert = strInsert & ") VALUES " & _
                          "(" & _
                          txtText1(0).Text & "," & _
                          linea & "," & _
                          gintprodbuscado(v, 0) & ",'NE',"
                If Not IsNull(rstProducto("FR73VOLINFIV").Value) Then
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VOLINFIV").Value, ",", ".", 1)
                  strInsert = strInsert & vntAux & "," & vntAux & ","
                  VolTotal = CCur(rstProducto("FR73VOLINFIV").Value)
                Else
                  VolTotal = 0
                  strInsert = strInsert & "NULL," & "NULL,"
                End If
                If Not IsNull(rstProducto("FR73TIEMINF").Value) Then
                  strInsert = strInsert & rstProducto("FR73TIEMINF").Value & ","
                Else
                  strInsert = strInsert & "NULL,"
                End If
                strInsert = strInsert & "'F',to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy'),TO_CHAR(SYSDATE,'HH24'),1,1," 'FR28CANTIDAD,FR28CANTDISP
                
                If Not IsNull(rstProducto("FRG4CODFRECUENCIA_USU").Value) Then
                  strInsert = strInsert & "'" & rstProducto("FRG4CODFRECUENCIA_USU").Value & "',"
                Else
                  strInsert = strInsert & "NULL,"
                End If
                If Not IsNull(rstProducto("FR34CODVIA").Value) Then
                  strInsert = strInsert & "'" & rstProducto("FR34CODVIA").Value & "',"
                Else
                  strInsert = strInsert & "NULL,"
                End If
                If Not IsNull(rstProducto("FR73INDVIAOPCION").Value) Then
                  strInsert = strInsert & rstProducto("FR73INDVIAOPCION").Value & ","
                Else
                  strInsert = strInsert & "NULL,"
                End If
                If Not IsNull(rstProducto("FR73VELPERFUSION").Value) Then
                  vntAux = objGen.ReplaceStr(rstProducto("FR73VELPERFUSION").Value, ",", ".", 1)
                  strInsert = strInsert & vntAux & ","
                Else
                  If grddbgrid1(0).Columns(33).Value = "" Then
                    TiemMinInf = 0
                  Else
                    TiemMinInf = CCur(grddbgrid1(0).Columns(33).Value) / 60
                  End If
                  If TiemMinInf <> 0 Then
                    VolPerf = Format(VolTotal / TiemMinInf, "0.00")
                    vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
                    strInsert = strInsert & vntAux & ","
                  Else
                    VolPerf = 0
                    strInsert = strInsert & VolPerf & ","
                  End If
                End If
                strInsert = strInsert & "'Diario')"
              End If

              objApp.rdoConnect.Execute strInsert, 64
              objApp.rdoConnect.Execute "Commit", 64
              
              If grddbgrid1(0).Columns(47).Value = "P" Then
                strupdate = "UPDATE FR2800 SET FR28INDDISPPRN=-1"
                strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
                strupdate = strupdate & " AND FR28NUMLINEA=" & linea
                objApp.rdoConnect.Execute strupdate, 64
                objApp.rdoConnect.Execute "Commit", 64
              End If
              
              rstlinea.Close
              Set rstlinea = Nothing
              rstProducto.Close
              Set rstProducto = Nothing
              'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
            End If
          Next v
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
          objWinInfo.DataRefresh
          If grddbgrid1(0).Columns(47).Value = "P" Then
            MsgBox "Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n.", vbInformation, "Aviso"
            Call objWinInfo.CtrlDataChange
            ''txtText1(39).SetFocus
          End If
        Else
          '???????????????????????????-
          For v = 0 To gintprodtotal - 1
            strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                      gintprodbuscado(v, 0)
            Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
            
            strupdate = "UPDATE FR2800 SET "
            strupdate = strupdate & "FR73CODPRODUCTO=" & gintprodbuscado(v, 0)
            If Not IsNull(rstProducto("FRH7CODFORMFAR").Value) Then
              strupdate = strupdate & ",FRH7CODFORMFAR='" & rstProducto("FRH7CODFORMFAR").Value & "'"
            End If
            'If Not IsNull(rstProducto("FR73DOSIS").Value) Then
            '  strupdate = strupdate & ",FR28DOSIS=" & rstProducto("FR73DOSIS").Value
            'End If
            If IsNull(rstProducto("FR73DOSIS").Value) Then
              strupdate = strupdate & ",FR28DOSIS=1"
            Else
              If rstProducto("FR73INDDOSISPESO").Value = -1 Then
                If txtText1(32).Text = "" Then
                  Call MsgBox("El Paciente debe tener Peso", vbExclamation, "Aviso")
                  cmdSustituir.Enabled = True
                 ''' blnBoton = False
                  gintprodtotal = 0
                  Exit Sub
                Else
                  strupdate = strupdate & ",FR28DOSIS=" & rstProducto("FR73DOSIS").Value * txtText1(32).Text
                End If
              Else
                If rstProducto("FR73INDDOSISSUPCORP").Value = -1 Then
                  If txtText1(32).Text = "" Or txtText1(33).Text = "" Then
                    Call MsgBox("El Paciente debe tener Peso y Altura", vbExclamation, "Aviso")
                    cmdSustituir.Enabled = True
                    '''blnBoton = False
                    gintprodtotal = 0
                    Exit Sub
                  Else
                    strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
                    Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
                    strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
                    Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
                    strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
                    Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
                    intPeso = txtText1(32).Text
                    intAltura = txtText1(33).Text
                    vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
                    vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
                    vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value * rstProducto("FR73DOSIS").Value
                    vntMasa = objGen.ReplaceStr(vntMasa, ",", ".", 1)
                    strupdate = strupdate & ",FR28DOSIS=" & vntMasa
                    rstPotPeso.Close
                    rstPotAlt.Close
                    rstFactor.Close
                    Set rstPotPeso = Nothing
                    Set rstPotAlt = Nothing
                    Set rstFactor = Nothing
                  End If
                Else
                  vntAux = objGen.ReplaceStr(rstProducto("FR73DOSIS").Value, ",", ".", 1)
                  strupdate = strupdate & ",FR28DOSIS=" & vntAux
                End If
              End If
            End If
            
            If Not IsNull(rstProducto("FR93CODUNIMEDIDA").Value) Then
              strupdate = strupdate & ",FR93CODUNIMEDIDA='" & rstProducto("FR93CODUNIMEDIDA").Value & "'"
            End If
            If Not IsNull(rstProducto("FR73VOLUMEN").Value) Then 'FR28VOLTOTAL?
              vntAux = objGen.ReplaceStr(rstProducto("FR73VOLUMEN").Value, ",", ".", 1)
              strupdate = strupdate & ",FR28VOLUMEN=" & vntAux
            End If
            If Not IsNull(rstProducto("FR73REFERENCIA").Value) Then
              strupdate = strupdate & ",FR28REFERENCIA='" & rstProducto("FR73REFERENCIA").Value & "'"
            End If
            If Not IsNull(rstProducto("FRH9UBICACION").Value) Then
              strupdate = strupdate & ",FR28UBICACION='" & rstProducto("FRH9UBICACION").Value & "'"
            End If
            
            VolTotal = 0
            If IsNull(rstProducto("FR73VOLUMEN").Value) Then
              Vol1 = 0
            Else
              Vol1 = CCur(rstProducto("FR73VOLUMEN").Value)
            End If
            If grddbgrid1(0).Columns(36).Value = "" Then
              Vol2 = 0
            Else
              Vol2 = CCur(grddbgrid1(0).Columns(36).Value)
            End If
            If grddbgrid1(0).Columns(44).Value = "" Then
              Vol3 = 0
            Else
              Vol3 = CCur(grddbgrid1(0).Columns(44).Value)
            End If
            VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
            vntAux = objGen.ReplaceStr(VolTotal, ",", ".", 1)
            strupdate = strupdate & ",FR28VOLTOTAL=" & vntAux
            If grddbgrid1(0).Columns(33).Value = "" Then
              TiemMinInf = 0
            Else
              TiemMinInf = CCur(grddbgrid1(0).Columns(33).Value) / 60
            End If
            If TiemMinInf <> 0 Then
              VolPerf = Format(VolTotal / TiemMinInf, "0.00")
            Else
              VolPerf = 0
            End If
            vntAux = objGen.ReplaceStr(VolPerf, ",", ".", 1)
            strupdate = strupdate & ",FR28VELPERFUSION=" & vntAux
            
            strupdate = strupdate & " WHERE FR66CODPETICION=" & txtText1(0).Text
            strupdate = strupdate & " AND FR28NUMLINEA=" & grddbgrid1(0).Columns(5).Value
            
            objApp.rdoConnect.Execute strupdate, 64
            
            rstProducto.Close
            Set rstProducto = Nothing
          Next v
          Call objWinInfo.DataRefresh
          '???????????????????????????-
        End If
      End If
      gintprodtotal = 0
    End If
  End If

'tabTab1(1).Tab = 1
'objWinInfo.DataMoveLast
'tabTab1(1).Tab = 0
gstrLlamadorProd = ""
''txtText1(35).SetFocus
cmdSustituir.Enabled = True
'''blnBoton = False

End Sub

'Private Sub cmdmodificar_Click()
'
'Dim mensaje As String
'Dim strInsert As String
'Dim rsta As rdoResultset
'Dim stra As String
'Dim rstlinea As rdoResultset
'Dim strlinea As String
'Dim linea As Long
'Dim strupdate As String
'Dim strprod As String
'Dim rstprod As rdoResultset
'Dim v As Integer
'  cmdmodificar.Enabled = False
'    If frmVisOM.grddbgrid1(1).Columns(5).Value = 1 Then
'      Call MsgBox("La OM est� en estado de Redactado", vbInformation, "Aviso")
'      cmdmodificar.Enabled = True
'      Exit Sub
'    End If
'    If grddbgrid1(0).Rows > 0 Then
'      If txtText1(25).Text = 4 Or txtText1(25).Text = 5 Then 'orden validada
'          mensaje = MsgBox("La Orden M�dica ya est� validada o dispensada." & Chr(13) & _
'                          "No puede modificar sus productos.", vbInformation, "Aviso")
'      Else
'        'se mira que el producto no sea estupefaciente
'        'strprod = "SELECT FR73INDESTUPEFACI FROM FR7300 WHERE " & _
'        '          "FR73CODPRODUCTO=" & grddbgrid1(0).Columns(6).Value
'        'Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
'        'If rstprod.rdoColumns(0).Value = -1 Then
'        If grddbgrid1(0).Columns(47).Value = "E" Then
'            mensaje = MsgBox("El producto es un estupefaciente." & Chr(13) & _
'                "No puede modificar la l�nea.", vbInformation, "Aviso")
'            'rstprod.Close
'            'Set rstprod = Nothing
'        Else 'no es estupefaciente
'            'rstprod.Close
'            'Set rstprod = Nothing
'            'se mira si la l�nea est� bloqueada
'            strprod = "SELECT FR28INDBLOQUEADA FROM FR2800 WHERE FR66CODPETICION=" & grddbgrid1(0).Columns(4).Value & _
'              " AND FR28NUMLINEA=" & grddbgrid1(0).Columns(5).Value
'              '" AND FR73CODPRODUCTO=" & grddbgrid1(0).Columns(6).Value
'            Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
'            If rstprod.rdoColumns(0).Value = -1 Then
'              mensaje = MsgBox("La l�nea est� bloqueada. No puede modificarse", vbInformation, "Aviso")
'              rstprod.Close
'              Set rstprod = Nothing
'            Else 'la l�nea no est� bloqueada
'              rstprod.Close
'              Set rstprod = Nothing
'              Call objsecurity.LaunchProcess("FR0150")
'              If gintprodtotal > 0 Then
'                If gintprodtotal > 1 Then
'                  MsgBox "Debe traer s�lo 1 producto."
'                Else
'                'hay que crear una nueva l�nea
'                  strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
'                              grddbgrid1(0).Columns(4).Value
'                  Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
'                  If IsNull(rstlinea.rdoColumns(0).Value) = False Then
'                        linea = 1 + rstlinea(0).Value
'                  Else
'                    linea = 1
'                  End If
'                  gintlinea = linea
'                  rstlinea.Close
'                  Set rstlinea = Nothing
'
'                  stra = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & grddbgrid1(0).Columns(4).Value & _
'                        " AND FR28NUMLINEA=" & grddbgrid1(0).Columns(5).Value
'                        '" AND FR73CODPRODUCTO=" & grddbgrid1(0).Columns(6).Value
'                  Set rsta = objApp.rdoConnect.OpenResultset(stra)
'
'                  strInsert = "INSERT INTO FR2800 (FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO,FR34CODVIA," & _
'                    "FR28DOSIS,FR28VOLUMEN," & _
'                    "FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR28OPERACION," & _
'                    "FR28CANTIDAD,FR28CANTDISP) VALUES " & _
'                    "(" & grddbgrid1(0).Columns(4).Value & "," & _
'                    linea & "," & _
'                    gintprodbuscado(v, 0) & ","
'
'
'                  If gintprodbuscado(v, 7) = "" Then
'                      strInsert = strInsert & "null" & ","
'                  Else
'                      strInsert = strInsert & "'" & gintprodbuscado(v, 7) & "'" & ","
'                  End If
'                  If gintprodbuscado(v, 3) = "" Then
'                      strInsert = strInsert & "null" & ","
'                  Else
'                      strInsert = strInsert & objGen.ReplaceStr(gintprodbuscado(v, 3), ",", ".", 1) & ","
'                  End If
'                  If gintprodbuscado(v, 6) = "" Then
'                      strInsert = strInsert & "null" & ","
'                  Else
'                      strInsert = strInsert & objGen.ReplaceStr(gintprodbuscado(v, 6), ",", ".", 1) & ","
'                  End If
'                  If gintprodbuscado(v, 4) = "" Then
'                      strInsert = strInsert & "null" & ","
'                  Else
'                      strInsert = strInsert & "'" & gintprodbuscado(v, 4) & "',"
'                  End If
'                  If gintprodbuscado(v, 5) = "" Then
'                      strInsert = strInsert & "null" & ",'/'," & _
'                      grddbgrid1(0).Columns(16).Value & ","
'                  Else
'                      strInsert = strInsert & "'" & gintprodbuscado(v, 5) & "'" & ",'/'," & _
'                      grddbgrid1(0).Columns(16).Value & ","
 '                 End If
 '                 If grddbgrid1(0).Columns(18).Value = "" Then
'                      strInsert = strInsert & "null)"
'                  Else
'                      strInsert = strInsert & grddbgrid1(0).Columns(18).Value & ")"
'                  End If
'                  objApp.rdoConnect.Execute strInsert, 64
'                  objApp.rdoConnect.Execute "Commit", 64
'                  rsta.Close
'                  Set rsta = Nothing
'                  'se bloquea la l�nea seleccionada
'                  strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=-1," & _
'                      "FR28FECBLOQUEO=(SELECT SYSDATE FROM DUAL)," & _
'                      "SG02CODPERSBLOQ='" & objsecurity.strUser & "'" & _
'                      " WHERE FR66CODPETICION=" & grddbgrid1(0).Columns(4).Value & _
'                      " AND FR28NUMLINEA=" & grddbgrid1(0).Columns(5).Value & _
'                      " AND FR73CODPRODUCTO=" & grddbgrid1(0).Columns(6).Value
'                  objApp.rdoConnect.Execute strupdate, 64
'                  objApp.rdoConnect.Execute "Commit", 64
'
'                  'Call objsecurity.LaunchProcess("FR0150")
'
'                  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
'                  objWinInfo.DataRefresh
'                End If
'              End If
'            End If
'        End If
'      End If
'    End If
'  cmdmodificar.Enabled = True
'End Sub

Private Sub cmdperfil_Click()
Dim mensaje As String
  cmdperfil.Enabled = False
  
  If txtText1(0).Text <> "" Then
      If txtText1(2).Text <> "" Then
          glngPaciente = txtText1(2).Text
          Call objsecurity.LaunchProcess("FR0162")
      Else
          mensaje = MsgBox("No hay ning�n paciente.", vbInformation, "Aviso")
      End If
  End If
  
  cmdperfil.Enabled = True
End Sub

'Private Sub cmdvalidar_Click()
'Dim strupdate As String
'Dim mensaje As String
'Dim rsta As rdoResultset
'Dim stra As String
'Dim i As Integer
'Dim intestupefac As Integer
'Dim strpet As String
'Dim rstpet As rdoResultset

'On Error GoTo err
'cmdvalidar.Enabled = False
'intestupefac = 0
''se mira si la petici�n tiene productos que son estupefacientes
'grdDBGrid1(0).MoveFirst
'For i = 0 To grdDBGrid1(0).Rows - 1
'        stra = "SELECT FR73INDESTUPEFACI FROM FR7300 WHERE FR73CODPRODUCTO=" & _
'                grdDBGrid1(0).Columns(6).Value
'        Set rsta = objApp.rdoConnect.OpenResultset(stra)
'        If (rsta.rdoColumns(0).Value = 0 Or IsNull(rsta.rdoColumns(0).Value)) Then
'           'en la petici�n hay al menos un producto que no es estupefaciente
'           intestupefac = 1
'           Exit For
'        End If
'        rsta.Close
'        Set rsta = Nothing
'grdDBGrid1(0).MoveNext
'Next i
'
'
'If txtText1(25).Text = 3 Then 'enviada
'    If grdDBGrid1(0).Rows = 0 Then
'             'las �rdenes m�dicas de ese paciente que tengan Estado=4,7 (Validada,Asignada a Carro)
'             'se deben anular y poner Estado=8 (Anulada)
''             strpet = "SELECT * FROM FR6600 WHERE FR26CODESTPETIC IN (4,7)" & _
'                      " AND CI21CODPERSONA=" & txtText1(2).Text & _
'                      " AND AD02CODDPTO=" & txtText1(16).Text
'             Set rstpet = objApp.rdoConnect.OpenResultset(strpet)
'             While Not rstpet.EOF
'                strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=8" & _
'                            " WHERE FR66CODPETICION=" & _
'                            rstpet.rdoColumns("FR66CODPETICION").Value
'                objApp.rdoConnect.Execute strupdate, 64
'                objApp.rdoConnect.Execute "Commit", 64
'             rstpet.MoveNext
'             Wend
'             rstpet.Close
'             Set rstpet = Nothing
'             strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=4" & _
'                          " WHERE FR66CODPETICION=" & txtText1(0).Text
'             objApp.rdoConnect.Execute strupdate, 64
'             objApp.rdoConnect.Execute "Commit", 64
'             Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
'             objWinInfo.DataRefresh
'             Call Validar_orden
'             mensaje = MsgBox("La Orden M�dica ha sido validada", vbInformation, "Farmacia")
'    Else
'            If intestupefac = 1 Then
'                'las �rdenes m�dicas de ese paciente que tengan Estado=4,7 (Validada,Asignada a Carro)
'                'se deben anular y poner Estado=8 (Anulada)
'                strpet = "SELECT * FROM FR6600 WHERE FR26CODESTPETIC IN (4,7)" & _
'                      " AND CI21CODPERSONA=" & txtText1(2).Text & _
'                      " AND AD02CODDPTO=" & txtText1(16).Text
'                Set rstpet = objApp.rdoConnect.OpenResultset(strpet)
'                While Not rstpet.EOF
'                    strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=8" & _
'                            " WHERE FR66CODPETICION=" & _
'                            rstpet.rdoColumns("FR66CODPETICION").Value
'                    objApp.rdoConnect.Execute strupdate, 64
'                    objApp.rdoConnect.Execute "Commit", 64
'                rstpet.MoveNext
'                Wend
'                rstpet.Close
'                Set rstpet = Nothing
'                strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=4" & _
'                             " WHERE FR66CODPETICION=" & txtText1(0).Text
'                objApp.rdoConnect.Execute strupdate, 64
'                objApp.rdoConnect.Execute "Commit", 64
'                Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
'                objWinInfo.DataRefresh
'                Call Validar_orden
'                mensaje = MsgBox("La Orden M�dica ha sido validada", vbInformation, "Farmacia")
'            Else 'los productos son todos estupefacientes
'                mensaje = MsgBox("La Orden M�dica no puede ser validada por estar compuesta" & _
'                           Chr(13) & " de productos estupefacientes.", vbInformation, "Farmacia")
'            End If
'    End If
'Else
'        If txtText1(25).Text = 1 Or txtText1(25).Text = 2 Then
'            mensaje = MsgBox("La Orden M�dica no est� a�n enviada para ser validada.", vbInformation, "Farmacia")
'        End If
'        If txtText1(25).Text = 4 Then
 '           mensaje = MsgBox("La Orden M�dica ya est� validada.", vbInformation, "Farmacia")
'        End If
'End If
'cmdvalidar.Enabled = True
'err:
'End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim rsta As rdoResultset
  Dim stra As String
  Dim strPeticiones As String
  
  On Error GoTo err
  Set objWinInfo = New clsCWWin
 
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grddbgrid1(2)
    
    .strName = "OM"
      
    .strTable = "FR6600"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR66CODPETICION", cwAscending)
    
    .strWhere = "FR66CODPETICION=" & glngpeticion
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "OM")
    Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDALERGIA", "Al�rgico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
   
    'Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "C�digo Grupo Protocolo")

  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    'If gstrLlamadorProd = "Cesta" Then
    '  Set .objFatherContainer = Nothing
    'Else
      Set .objFatherContainer = fraFrame1(1)
    'End If
    Set .tabMainTab = Nothing
    Set .grdGrid = grddbgrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Detalle OM"
    
    .strTable = "FR2800"
    '.intAllowance = cwAllowReadOnly
    
    Call .FormAddRelation("FR66CODPETICION", txtText1(0))
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "Bloq", "FR28INDBLOQUEADA", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Pet", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�digo", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Medicamento", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Tam Env", "", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "FF", "FRH7CODFORMFAR", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR28DOSIS", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "UM", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Volumen", "FR28VOLUMEN", cwNumeric, 6)
    Call .GridAddColumn(objMultiInfo, "Desc.UM", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Can Ped", "FR28CANTIDAD", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "Ac Ped", "", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Can Disp", "FR28CANTDISP", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "Ac Disp", "", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "PRN", "FR28INDDISPPRN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Frecuencia", "FRG4CODFRECUENCIA", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Periodicidad", "FRH5CODPERIODICIDAD", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Fecha Inicio", "FR28FECINICIO", cwDate)
    Call .GridAddColumn(objMultiInfo, "Hora Inicio", "FR28HORAINICIO", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Fecha Fin", "FR28FECFIN", cwDate)
    Call .GridAddColumn(objMultiInfo, "Hora Fin", "FR28HORAFIN", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Inmediato", "FR28INDCOMIENINMED", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "SN", "FR28INDSN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Prod_Dil", "FR73CODPRODUCTO_DIL", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Diluyente", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Cantidad Dil", "FR28CANTIDADDIL", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "Tiempo Inf", "FR28TIEMMININF", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Instrucciones Administraci�n", "FR28INSTRADMIN", cwString, 2000)
    Call .GridAddColumn(objMultiInfo, "Perfusi�n", "FR28VELPERFUSION", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "Vol.Total", "FR28VOLTOTAL", cwString, 2000)
    Call .GridAddColumn(objMultiInfo, "STAT?", "FR28INDCOMIENINMED", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "PERF?", "FR28INDPERF", cwBoolean, 5)
    Call .GridAddColumn(objMultiInfo, "Cod Medicamento 2", "FR73CODPRODUCTO_2", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Medicamento 2", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "FF2", "FRH7CODFORMFAR_2", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis 2", "FR28DOSIS_2", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "UM2", "FR93CODUNIMEDIDA_2", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Volumen2", "FR28VOLUMEN_2", cwNumeric, 6)
    Call .GridAddColumn(objMultiInfo, "Medic no Formulario", "FR28DESPRODUCTO", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Ubicaci�n", "FR28UBICACION", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Operacion", "FR28OPERACION", cwString, 1)
    ''Call .GridAddColumn(objMultiInfo, "Volumen Dil", "FR28CANTIDADDIL", cwDecimal, 2)
    ''Call .GridAddColumn(objMultiInfo, "U.M.Perf.1", "FR93CODUNIMEDIDA_PERF1", cwString, 5)
    ''Call .GridAddColumn(objMultiInfo, "U.M.Perf.2", "FR93CODUNIMEDIDA_PERF2", cwString, 5)
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grddbgrid1(0).Columns(4)).intKeyNo = 1
    .CtrlGetInfo(grddbgrid1(0).Columns(5)).intKeyNo = 1
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    '.CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(txtText1(21)).blnInFind = True
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(txtText1(25)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(12)).blnInFind = True
    .CtrlGetInfo(chkCheck1(15)).blnInFind = True
    
    .CtrlGetInfo(Text2).blnNegotiated = False
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "CI22NOMBRE")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(4), "CI22PRIAPEL")
   
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(5), "CI22SEGAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(23), "CI22NUMHISTORIA")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "SG02NOM")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(19), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(11), "FR91DESURGENCIA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(15), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(26), "FR26DESESTADOPET")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(6)), grddbgrid1(0).Columns(7), "FR73CODINTFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(6)), grddbgrid1(0).Columns(9), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(6)), grddbgrid1(0).Columns(8), "FR73REFERENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(6)), grddbgrid1(0).Columns(10), "FR73TAMENVASE")
    'Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(6)), grddbgrid1(0).Columns(29), "FRH9UBICACION")
     
    'Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(19)), "FR34CODVIA", "SELECT * FROM FR3400 WHERE FR34CODVIA = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(19)), grddbgrid1(0).Columns(20), "FR34DESVIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(13)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(13)), grddbgrid1(0).Columns(15), "FR93DESUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(39)), "FR73CODPRODUCTO_2", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(39)), grddbgrid1(0).Columns(40), "FR73DESPRODUCTO")
    'Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(39)), grddbgrid1(0).Columns(44), "FR73VOLUMEN")
    
    
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(0).Columns(30)), "FR73CODPRODUCTO_DIL", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(0).Columns(30)), grddbgrid1(0).Columns(31), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM FR2200J WHERE CI21CODPERSONA= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(46), "DESCCAMA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(12), "CI30DESSEXO")
    
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(6)).blnForeign = True
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    '.CtrlGetInfo(txtText1(14)).blnForeign = True
    .CtrlGetInfo(txtText1(16)).blnForeign = True
    .CtrlGetInfo(txtText1(22)).blnForeign = True
    .CtrlGetInfo(txtText1(25)).blnForeign = True
    .CtrlGetInfo(grddbgrid1(0).Columns(6)).blnForeign = True
    '.CtrlGetInfo(grddbgrid1(0).Columns(19)).blnForeign = True
    .CtrlGetInfo(grddbgrid1(0).Columns(13)).blnForeign = True
     
    '.CtrlGetInfo(txtText1(8)).blnReadOnly = True
    .CtrlGetInfo(txtText1(10)).blnReadOnly = True
    .CtrlGetInfo(txtText1(20)).blnReadOnly = True
    .CtrlGetInfo(txtText1(21)).blnReadOnly = True
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True
    .CtrlGetInfo(txtText1(25)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(11)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(12)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(13)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(14)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(16)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(17)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(20)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(21)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(22)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(23)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(24)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(25)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(26)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(27)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(28)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(29)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(30)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(31)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(32)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(33)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(34)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(35)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(36)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(37)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(38)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(39)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(40)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(41)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(42)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(43)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(44)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(45)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(46)).blnReadOnly = True
    .CtrlGetInfo(grddbgrid1(0).Columns(47)).blnReadOnly = True
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
grddbgrid1(0).Columns(0).Visible = False
grddbgrid1(0).Columns(3).Visible = False
If gstrLlamadorProd <> "Cesta" Then
  grddbgrid1(0).Columns(4).Visible = False
  grddbgrid1(0).Columns(9).Width = 1500   'desc.producto
  grddbgrid1(0).Columns(10).Width = 800   'TAM ENVASE
Else
  grddbgrid1(0).Columns(9).Width = 1600   'desc.producto
  grddbgrid1(0).Columns(4).Width = 800   'peticion
  grddbgrid1(0).Columns(10).Width = 500   'TAM ENVASE
End If
grddbgrid1(0).Columns(5).Visible = False
grddbgrid1(0).Columns(6).Visible = False
grddbgrid1(0).Columns(10).Visible = False
grddbgrid1(0).Columns(15).Visible = False
grddbgrid1(0).Columns(17).Visible = False
grddbgrid1(0).Columns(19).Visible = False
grddbgrid1(0).Columns(47).Visible = False

grddbgrid1(0).Columns(0).Width = 0
grddbgrid1(0).Columns(3).Width = 500 'bloqueada?
grddbgrid1(0).Columns(6).Width = 1000   'c�d.producto
grddbgrid1(0).Columns(7).Width = 800   'c�d interno
grddbgrid1(0).Columns(8).Width = 1000   'REFERENCIA
grddbgrid1(0).Columns(9).Width = 3000   'desc
grddbgrid1(0).Columns(11).Width = 500   'FF
grddbgrid1(0).Columns(22).Width = 400   'c�d.v�a
'grddbgrid1(0).Columns(20).Width = 1500   'desc.v�a
'grddbgrid1(0).Columns(21).Width = 1100  'volumen
grddbgrid1(0).Columns(33).Width = 1100  'tiempo infusi�n
grddbgrid1(0).Columns(21).Width = 1000  'c�d.frecuencia
'grddbgrid1(0).Columns(24).Width = 1500  'c�d.frecuencia
grddbgrid1(0).Columns(20).Width = 500  'prn
grddbgrid1(0).Columns(28).Width = 500  'comienzo inmediato
grddbgrid1(0).Columns(29).Width = 500  'seg�n niveles
grddbgrid1(0).Columns(16).Width = 800  'cantidad pedida
grddbgrid1(0).Columns(17).Width = 800  'ac pedido
grddbgrid1(0).Columns(18).Width = 800  'cantidad disp
grddbgrid1(0).Columns(19).Width = 800  'ac disp
grddbgrid1(0).Columns(13).Width = 500  'c�d.medida
grddbgrid1(0).Columns(15).Width = 1000  'desc. medida
grddbgrid1(0).Columns(12).Width = 600   'dosis

Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)

err:
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
On Error GoTo err
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
err:
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
On Error GoTo err
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
err:
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
On Error GoTo err
  intCancel = objWinInfo.WinExit
err:
End Sub

Private Sub Form_Unload(intCancel As Integer)
On Error GoTo err
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
err:
End Sub


Private Sub IdPersona1_Click()

End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
On Error GoTo err
If strFormName = "OM" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "CI2200"

     Set objField = .AddField("CI21CODPERSONA")
     objField.strSmallDesc = "C�digo Paciente"

     Set objField = .AddField("CI22NOMBRE")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("CI22NUMHISTORIA")
     objField.strSmallDesc = "Historia"
     
     Set objField = .AddField("CI22PRIAPEL")
     objField.strSmallDesc = "Apellido 1�"
     
     Set objField = .AddField("CI22SEGAPEL")
     objField.strSmallDesc = "Apellido 2�"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("CI21CODPERSONA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo Enfermera"

     Set objField = .AddField("SG02NOM")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(6), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo M�dico"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 'If strFormName = "OM" And strCtrl = "txtText1(14)" Then
 '   Set objSearch = New clsCWSearch
 '   With objSearch
 '    .strTable = "AD0200"
 '    .strWhere = "WHERE AD32CODTIPODPTO=3 AND " & _
 '          "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
 '          "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"
'
'     Set objField = .AddField("AD02CODDPTO")
'     objField.strSmallDesc = "C�digo Servicio"
'
'     Set objField = .AddField("AD02DESDPTO")
'     objField.strSmallDesc = "Descripci�n Servicio"
'
'     If .Search Then
'      Call objWinInfo.CtrlSet(txtText1(14), .cllValues("AD02CODDPTO"))
'     End If
'   End With
'   Set objSearch = Nothing
' End If
 
 If strFormName = "OM" And strCtrl = "txtText1(16)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD32CODTIPODPTO=3 AND " & _
           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Dpto."

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto."

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(16), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(22)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9100"

     Set objField = .AddField("FR91CODURGENCIA")
     objField.strSmallDesc = "C�digo Urgencia"

     Set objField = .AddField("FR91DESURGENCIA")
     objField.strSmallDesc = "Descripci�n Urgencia"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(22), .cllValues("FR91CODURGENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBgrid1(0).C�d.Prod" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"

     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"

     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grddbgrid1(0).Columns(6), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 'If strFormName = "Detalle OM" And strCtrl = "grdDBgrid1(0).V�a" Then
 '   Set objSearch = New clsCWSearch
 '   With objSearch
 '    .strTable = "FR3400"'
'
'     Set objField = .AddField("FR34CODVIA")
'     objField.strSmallDesc = "C�digo V�a"
'
'     Set objField = .AddField("FR34DESVIA")
'     objField.strSmallDesc = "Descripci�n"
'
'     If .Search Then
'      Call objWinInfo.CtrlSet(grddbgrid1(0).Columns(19), .cllValues("FR34CODVIA"))
'     End If
'   End With
'   Set objSearch = Nothing
' End If
 
' If strFormName = "Detalle OM" And strCtrl = "grdDBgrid1(0).Medida" Then
'    Set objSearch = New clsCWSearch
'    With objSearch
'     .strTable = "FR9300"'
'
'     Set objField = .AddField("FR93CODUNIMEDIDA")
'     objField.strSmallDesc = "C�digo Unidad de Medida"
'
'     Set objField = .AddField("FR93DESUNIMEDIDA")
'     objField.strSmallDesc = "Descripci�n"
'
'     If .Search Then
'      Call objWinInfo.CtrlSet(grddbgrid1(0).Columns(13), .cllValues("FR93CODUNIMEDIDA"))
'     End If
'   End With
'   Set objSearch = Nothing
' End If
err:
End Sub



Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim mensaje As String
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
On Error GoTo err
  mblnGrabar = True
  If grddbgrid1(0).Columns(16).Value <> "" Then
    If IsNumeric(grddbgrid1(0).Columns(18).Value) = True Then
      If CDec(grddbgrid1(0).Columns(18).Value) < 0 Or CDec(grddbgrid1(0).Columns(18).Value) > CDec(grddbgrid1(0).Columns(18).Value) Then
        Call MsgBox("Hay datos incorrectos en la cantidad suministrada", vbExclamation, "Aviso")
        mblnGrabar = False
        blnCancel = True
        'Call objWinInfo.CtrlSet(grdDBgrid1(1).Columns(10), 0)
      End If
    Else
      Call MsgBox("Hay datos incorrectos en la cantidad suministrada", vbExclamation, "Aviso")
      mblnGrabar = False
      blnCancel = True
    End If
  Else
    If IsNumeric(grddbgrid1(0).Columns(18).Value) = False Then
        Call MsgBox("Hay datos incorrectos en la cantidad suministrada", vbExclamation, "Aviso")
        mblnGrabar = False
        blnCancel = True
    Else
      If grddbgrid1(0).Columns(18).Value < 0 Then
        Call MsgBox("Hay datos incorrectos en la cantidad suministrada", vbExclamation, "Aviso")
        mblnGrabar = False
        blnCancel = True
      End If
    End If
  End If
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
If txtText1(16).Text <> "" Then
    stra = "SELECT * FROM AD0200 " & _
           "WHERE AD32CODTIPODPTO=3 AND " & _
           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))" & _
           " AND AD02CODDPTO=" & txtText1(16).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.EOF Then
        mensaje = MsgBox("El servicio es incorrecto.", vbInformation, "Aviso")
        blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
End If

'If txtText1(14).Text <> "" Then
'    stra = "SELECT * FROM AD0200 " & _
'           "WHERE AD32CODTIPODPTO=3 AND " & _
'           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
'           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))" & _
'           " AND AD02CODDPTO=" & txtText1(14).Text
'    Set rsta = objApp.rdoConnect.OpenResultset(stra)
'    If rsta.EOF Then
'        mensaje = MsgBox("El servicio de cargo es incorrecto.", vbInformation, "Aviso")
'        blnCancel = True
'    End If
'    rsta.Close
'    Set rsta = Nothing
'End If
err:
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  On Error GoTo err
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If

err:
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
On Error GoTo err
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
err:
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
On Error GoTo err
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
     Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
     Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
     If intIndex = 0 Then
        If grddbgrid1(0).Columns(17).Value = "" Then
          grddbgrid1(0).Columns(19).Locked = True
        Else
          grddbgrid1(0).Columns(19).Locked = False
        End If
     End If
err:
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
     Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdDBgrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    Dim i As Integer
    Dim sqlstr As String
    Dim rsta As rdoResultset
    On Error GoTo err
    If Index = 0 Then
        If grddbgrid1(0).Columns(3).Value = -1 Then
            For i = 3 To 47
                grddbgrid1(0).Columns(i).CellStyleSet "Bloqueada"
            Next i
        End If
        
    End If
err:
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub

Private Sub cboCombo1_Change(Index As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub txtText1_Change(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


