VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmObservaciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Bloqueo de L�neas."
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   73
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Index           =   1
      Left            =   120
      TabIndex        =   78
      Top             =   480
      Width           =   11580
      Begin TabDlg.SSTab tabTab1 
         Height          =   2415
         Index           =   1
         Left            =   120
         TabIndex        =   79
         TabStop         =   0   'False
         Top             =   360
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   4260
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0177.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(21)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(23)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(28)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "tab1"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "chkCheck1(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(26)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(25)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "chkCheck1(15)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "chkCheck1(7)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "chkCheck1(5)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "chkCheck1(6)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "chkCheck1(12)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(9)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(10)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0177.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grddbgrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            DataField       =   "FR66CODPETICION"
            Height          =   330
            Index           =   10
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Petici�n"
            Top             =   360
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR66CODOMORIPRN"
            Height          =   330
            Index           =   9
            Left            =   1440
            TabIndex        =   1
            Tag             =   "C�digo OM origen PRN"
            Top             =   360
            Width           =   1100
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Orden M�dica"
            DataField       =   "FR66INDOM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   8400
            TabIndex        =   31
            Tag             =   "Orden M�dica?"
            Top             =   600
            Width           =   1695
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Diab�tico"
            DataField       =   "FR66INDPACIDIABET"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   6120
            TabIndex        =   26
            Tag             =   "Diab�tico?"
            Top             =   120
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Al�rgico"
            DataField       =   "FR66INDALERGIA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   6120
            TabIndex        =   27
            Tag             =   "Al�rgico?"
            Top             =   360
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Restricci�n volumen"
            DataField       =   "FR66INDRESTVOLUM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   6120
            TabIndex        =   28
            Tag             =   "Restricci�n de Volumen?"
            Top             =   600
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Inter�s Cient�fico"
            DataField       =   "FR66INDINTERCIENT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   8400
            TabIndex        =   29
            Tag             =   "Inter�s Cient�fico?"
            Top             =   120
            Width           =   1815
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   25
            Left            =   3360
            TabIndex        =   2
            Tag             =   "Estado Petici�n"
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   26
            Left            =   3720
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   1605
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Cesta"
            DataField       =   "FR66INDCESTA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   8400
            TabIndex        =   30
            Tag             =   "Cesta?"
            Top             =   360
            Width           =   1695
         End
         Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   80
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
            Height          =   2145
            Index           =   2
            Left            =   -74880
            TabIndex        =   81
            TabStop         =   0   'False
            Top             =   120
            Width           =   10575
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18653
            _ExtentY        =   3784
            _StockProps     =   79
         End
         Begin TabDlg.SSTab tab1 
            Height          =   1575
            Left            =   120
            TabIndex        =   82
            TabStop         =   0   'False
            Top             =   720
            Width           =   10575
            _ExtentX        =   18653
            _ExtentY        =   2778
            _Version        =   327681
            Style           =   1
            Tabs            =   5
            TabsPerRow      =   5
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Paciente"
            TabPicture(0)   =   "FR0177.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(11)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(13)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(14)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(15)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(16)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(23)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtText1(13)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(14)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(15)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(19)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).ControlCount=   10
            TabCaption(1)   =   "M�dico / Enfermera"
            TabPicture(1)   =   "FR0177.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lblLabel1(12)"
            Tab(1).Control(1)=   "lblLabel1(27)"
            Tab(1).Control(2)=   "lblLabel1(20)"
            Tab(1).Control(3)=   "lblLabel1(18)"
            Tab(1).Control(4)=   "lblLabel1(22)"
            Tab(1).Control(5)=   "lblLabel1(25)"
            Tab(1).Control(6)=   "lblLabel1(26)"
            Tab(1).Control(7)=   "dtcDateCombo1(6)"
            Tab(1).Control(8)=   "dtcDateCombo1(5)"
            Tab(1).Control(9)=   "txtText1(24)"
            Tab(1).Control(10)=   "txtText1(20)"
            Tab(1).Control(11)=   "txtText1(21)"
            Tab(1).Control(11).Enabled=   0   'False
            Tab(1).Control(12)=   "txtText1(32)"
            Tab(1).Control(13)=   "txtText1(33)"
            Tab(1).Control(14)=   "txtText1(34)"
            Tab(1).Control(15)=   "txtText1(35)"
            Tab(1).Control(16)=   "txtText1(36)"
            Tab(1).Control(16).Enabled=   0   'False
            Tab(1).ControlCount=   17
            TabCaption(2)   =   "Redacci�n"
            TabPicture(2)   =   "FR0177.frx":0070
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLabel1(10)"
            Tab(2).Control(1)=   "lblLabel1(29)"
            Tab(2).Control(2)=   "lblLabel1(30)"
            Tab(2).Control(3)=   "dtcDateCombo1(2)"
            Tab(2).Control(4)=   "txtText1(22)"
            Tab(2).Control(5)=   "txtText1(11)"
            Tab(2).Control(5).Enabled=   0   'False
            Tab(2).Control(6)=   "txtText1(37)"
            Tab(2).ControlCount=   7
            TabCaption(3)   =   "Servicio"
            TabPicture(3)   =   "FR0177.frx":008C
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "txtText1(65)"
            Tab(3).Control(1)=   "txtText1(64)"
            Tab(3).Control(2)=   "txtText1(16)"
            Tab(3).Control(3)=   "txtText1(17)"
            Tab(3).Control(4)=   "lblLabel1(32)"
            Tab(3).Control(5)=   "lblLabel1(31)"
            Tab(3).ControlCount=   6
            TabCaption(4)   =   "Observaciones"
            TabPicture(4)   =   "FR0177.frx":00A8
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "txtText1(18)"
            Tab(4).Control(1)=   "LabelObservRojo"
            Tab(4).Control(2)=   "lblLabel1(24)"
            Tab(4).ControlCount=   3
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO_CRG"
               Height          =   330
               Index           =   65
               Left            =   -69480
               TabIndex        =   169
               Tag             =   "C�d.Dpto.Cargo"
               Top             =   840
               Width           =   360
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   64
               Left            =   -69000
               TabIndex        =   168
               Tag             =   "Dpto.Cargo"
               Top             =   840
               Width           =   3405
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   16
               Left            =   -74760
               TabIndex        =   23
               Tag             =   "C�d.Departamento"
               Top             =   840
               Width           =   1155
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   -73440
               TabIndex        =   24
               Tag             =   "Desc.Departamento"
               Top             =   840
               Width           =   3480
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAREDACCI"
               Height          =   330
               Index           =   37
               Left            =   -72720
               TabIndex        =   20
               Tag             =   "Hora Redacci�n"
               Top             =   960
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   11
               Left            =   -69960
               TabIndex        =   22
               TabStop         =   0   'False
               Tag             =   "Desc. Urgencia"
               Top             =   960
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR91CODURGENCIA"
               Height          =   330
               Index           =   22
               Left            =   -71040
               TabIndex        =   21
               Tag             =   "C�digo Urgencia"
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   36
               Left            =   -71520
               TabIndex        =   15
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Enfermera"
               Top             =   1200
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMENF"
               Height          =   330
               Index           =   35
               Left            =   -67680
               TabIndex        =   17
               Tag             =   "Hora Firma Enfermera"
               Top             =   1200
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMMEDI"
               Height          =   330
               Index           =   34
               Left            =   -67680
               TabIndex        =   12
               Tag             =   "hora Firma M�dico"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   33
               Left            =   -73200
               TabIndex        =   10
               Tag             =   "Apellido Doctor"
               Top             =   600
               Width           =   3285
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_MED"
               Height          =   330
               Index           =   32
               Left            =   -74880
               TabIndex        =   9
               Tag             =   "C�digo Doctor"
               Top             =   600
               Width           =   1635
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   21
               Left            =   -73200
               TabIndex        =   14
               TabStop         =   0   'False
               Tag             =   "Nombre Enfermera"
               Top             =   1200
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               DataField       =   "sg02cod_enf"
               Height          =   330
               Index           =   20
               Left            =   -74880
               TabIndex        =   13
               Tag             =   "C�digo Enfermera"
               Top             =   1200
               Width           =   1635
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_FEN"
               Height          =   330
               Index           =   24
               Left            =   -65520
               TabIndex        =   18
               Tag             =   "Hora Firma Enfermera"
               Top             =   840
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   31
               Left            =   -71520
               TabIndex        =   88
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Enfermera"
               Top             =   1200
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMENF"
               Height          =   330
               Index           =   30
               Left            =   -67680
               TabIndex        =   87
               Tag             =   "Hora Firma Enfermera"
               Top             =   1200
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMMEDI"
               Height          =   330
               Index           =   29
               Left            =   -67680
               TabIndex        =   86
               Tag             =   "hora Firma M�dico"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   28
               Left            =   -73200
               TabIndex        =   85
               Tag             =   "Apellido Doctor"
               Top             =   600
               Width           =   3285
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_MED"
               Height          =   330
               Index           =   27
               Left            =   -74880
               TabIndex        =   84
               Tag             =   "C�digo Doctor"
               Top             =   600
               Width           =   1635
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   19
               Left            =   6240
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Apellido 2� Paciente"
               Top             =   1200
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   15
               Left            =   3240
               TabIndex        =   7
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Paciente"
               Top             =   1200
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   14
               Left            =   240
               TabIndex        =   4
               Tag             =   "C�digo Paciente"
               Top             =   600
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   13
               Left            =   240
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   1200
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   23
               Left            =   2280
               TabIndex        =   5
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   600
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66OBSERV"
               Height          =   810
               Index           =   18
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   25
               Tag             =   "Observaciones"
               Top             =   600
               Width           =   10245
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAREDACCI"
               Height          =   330
               Index           =   12
               Left            =   -72840
               TabIndex        =   83
               Tag             =   "Hora Redacci�n"
               Top             =   960
               Width           =   612
            End
            Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
               Height          =   4425
               Index           =   3
               Left            =   -74880
               TabIndex        =   89
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
               Height          =   1305
               Index           =   4
               Left            =   -74880
               TabIndex        =   90
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMENF"
               Height          =   330
               Index           =   3
               Left            =   -69840
               TabIndex        =   91
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   1200
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECREDACCION"
               Height          =   330
               Index           =   4
               Left            =   -74880
               TabIndex        =   92
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   960
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMMEDI"
               Height          =   330
               Index           =   5
               Left            =   -69840
               TabIndex        =   11
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   600
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMENF"
               Height          =   330
               Index           =   6
               Left            =   -69840
               TabIndex        =   16
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   1200
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECREDACCION"
               Height          =   330
               Index           =   2
               Left            =   -74760
               TabIndex        =   19
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   960
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento de Cargo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   32
               Left            =   -69480
               TabIndex        =   170
               Top             =   600
               Width           =   2655
            End
            Begin VB.Label LabelObservRojo 
               BackColor       =   &H00C0C0C0&
               Caption         =   " Observaciones"
               ForeColor       =   &H000000FF&
               Height          =   195
               Left            =   -70680
               TabIndex        =   167
               Top             =   90
               Visible         =   0   'False
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   31
               Left            =   -74760
               TabIndex        =   117
               Top             =   600
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   30
               Left            =   -74760
               TabIndex        =   116
               Top             =   720
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   29
               Left            =   -72720
               TabIndex        =   115
               Top             =   720
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   -71040
               TabIndex        =   114
               Top             =   720
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   26
               Left            =   -67680
               TabIndex        =   113
               Top             =   960
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   25
               Left            =   -69840
               TabIndex        =   112
               Top             =   960
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   -67680
               TabIndex        =   111
               Top             =   360
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   -69840
               TabIndex        =   110
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   -74880
               TabIndex        =   109
               Top             =   360
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   -74880
               TabIndex        =   108
               Top             =   960
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.Firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   -65760
               TabIndex        =   107
               Top             =   600
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   19
               Left            =   -69840
               TabIndex        =   103
               Top             =   960
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   -69840
               TabIndex        =   102
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 2�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   6240
               TabIndex        =   101
               Top             =   960
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 1�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   3240
               TabIndex        =   100
               Top             =   960
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   240
               TabIndex        =   99
               Top             =   960
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Persona"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   240
               TabIndex        =   98
               Top             =   360
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Historia "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   2280
               TabIndex        =   97
               Top             =   360
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   -74880
               TabIndex        =   96
               Top             =   360
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   -74760
               TabIndex        =   95
               Top             =   600
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   -74880
               TabIndex        =   94
               Top             =   720
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   -71160
               TabIndex        =   93
               Top             =   720
               Width           =   855
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   120
            TabIndex        =   106
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.OM origen PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   23
            Left            =   1440
            TabIndex        =   105
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   21
            Left            =   3360
            TabIndex        =   104
            Top             =   120
            Width           =   735
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "L�neas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4695
      Index           =   0
      Left            =   120
      TabIndex        =   75
      Top             =   3360
      Width           =   11565
      Begin TabDlg.SSTab tabTab1 
         Height          =   4260
         Index           =   0
         Left            =   120
         TabIndex        =   76
         TabStop         =   0   'False
         Top             =   360
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   7514
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0177.frx":00C4
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "SSTab1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0177.frx":00E0
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grddbgrid1(0)"
         Tab(1).ControlCount=   1
         Begin TabDlg.SSTab SSTab1 
            Height          =   4095
            Left            =   120
            TabIndex        =   118
            Top             =   120
            Width           =   10605
            _ExtentX        =   18706
            _ExtentY        =   7223
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            Tab             =   1
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Bloqueo"
            TabPicture(0)   =   "FR0177.frx":00FC
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "chkCheck1(3)"
            Tab(0).Control(1)=   "chkCheck1(2)"
            Tab(0).Control(2)=   "chkCheck1(1)"
            Tab(0).Control(3)=   "chkCheck1(0)"
            Tab(0).Control(4)=   "txtText1(0)"
            Tab(0).Control(5)=   "txtText1(1)"
            Tab(0).Control(6)=   "txtText1(4)"
            Tab(0).Control(7)=   "txtText1(5)"
            Tab(0).Control(8)=   "txtText1(6)"
            Tab(0).Control(9)=   "txtText1(7)"
            Tab(0).Control(10)=   "txtText1(8)"
            Tab(0).Control(11)=   "dtcDateCombo1(1)"
            Tab(0).Control(12)=   "dtcDateCombo1(0)"
            Tab(0).Control(13)=   "lblLabel1(2)"
            Tab(0).Control(14)=   "lblLabel1(5)"
            Tab(0).Control(15)=   "lblLabel1(3)"
            Tab(0).Control(16)=   "lblLabel1(4)"
            Tab(0).Control(17)=   "lblLabel1(6)"
            Tab(0).ControlCount=   18
            TabCaption(1)   =   "L�nea bloqueada"
            TabPicture(1)   =   "FR0177.frx":0118
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "lblLabel1(0)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "lblLabel1(1)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "txtText1(2)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "txtText1(3)"
            Tab(1).Control(3).Enabled=   0   'False
            Tab(1).Control(4)=   "Frame1"
            Tab(1).Control(4).Enabled=   0   'False
            Tab(1).ControlCount=   5
            Begin VB.Frame Frame1 
               BorderStyle     =   0  'None
               Caption         =   "Medicamentos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   3720
               Left            =   120
               TabIndex        =   130
               Top             =   360
               Width           =   10335
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   63
                  Left            =   3840
                  TabIndex        =   140
                  Tag             =   "C�digo Petici�n"
                  Top             =   1200
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   62
                  Left            =   4080
                  TabIndex        =   139
                  Tag             =   "Num.Linea"
                  Top             =   1200
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Cambiar V�a"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   3480
                  TabIndex        =   138
                  Tag             =   "Cambiar V�a?"
                  Top             =   1800
                  Visible         =   0   'False
                  Width           =   1455
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "PRN"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   10
                  Left            =   4440
                  TabIndex        =   137
                  Tag             =   "Prn?"
                  Top             =   1200
                  Visible         =   0   'False
                  Width           =   735
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   61
                  Left            =   1680
                  TabIndex        =   136
                  Tag             =   "C�digo Medicamento"
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   60
                  Left            =   0
                  TabIndex        =   49
                  Tag             =   "Cantidad"
                  Top             =   840
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   59
                  Left            =   6120
                  TabIndex        =   46
                  Tag             =   "Dosis"
                  Top             =   240
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   58
                  Left            =   7080
                  TabIndex        =   47
                  Tag             =   "UM"
                  Top             =   240
                  Width           =   660
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   57
                  Left            =   7440
                  TabIndex        =   54
                  Tag             =   "Hora Inicio"
                  Top             =   840
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   56
                  Left            =   7440
                  TabIndex        =   63
                  Tag             =   "Hora Fin"
                  Top             =   2040
                  Width           =   375
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "STAT"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   9
                  Left            =   8400
                  TabIndex        =   59
                  Tag             =   "STAT?"
                  Top             =   1080
                  Width           =   855
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "PERF"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   8400
                  TabIndex        =   60
                  Tag             =   "PERF?"
                  Top             =   1680
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   46
                  Left            =   3360
                  TabIndex        =   135
                  Tag             =   "C�digo Producto diluir"
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   45
                  Left            =   0
                  TabIndex        =   55
                  Tag             =   "Descripci�n Producto Diluir"
                  Top             =   1440
                  Width           =   5385
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   42
                  Left            =   5520
                  TabIndex        =   56
                  Tag             =   "Volumen(ml)"
                  Top             =   1440
                  Width           =   732
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   43
                  Left            =   3240
                  TabIndex        =   134
                  Tag             =   "Tiempo Infusi�n(min)"
                  Top             =   1200
                  Visible         =   0   'False
                  Width           =   612
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   41
                  Left            =   5400
                  TabIndex        =   45
                  Tag             =   "FF"
                  Top             =   240
                  Width           =   615
               End
               Begin VB.TextBox txtHoras 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Left            =   6600
                  TabIndex        =   57
                  Tag             =   "Tiempo Infusi�n(hor)"
                  Top             =   1440
                  Width           =   375
               End
               Begin VB.TextBox txtMinutos 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Left            =   7080
                  TabIndex        =   58
                  Tag             =   "Tiempo Infusi�n(min)"
                  Top             =   1440
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   40
                  Left            =   7800
                  TabIndex        =   48
                  Tag             =   "Volumen(ml) Producto"
                  Top             =   240
                  Width           =   732
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   44
                  Left            =   8160
                  TabIndex        =   67
                  Tag             =   "Vol.Total|Volumen Total (ml)"
                  Top             =   2640
                  Width           =   1095
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   82
                  Left            =   5520
                  TabIndex        =   64
                  Tag             =   "Perfusi�n|Velocidad de Perfusi�n"
                  Top             =   2640
                  Width           =   975
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   81
                  Left            =   6600
                  TabIndex        =   65
                  Tag             =   "U.M.Perf.1|C�d. de la unidad de medida de la velocidad de perfusi�n"
                  Top             =   2640
                  Width           =   615
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   80
                  Left            =   7395
                  TabIndex        =   66
                  Tag             =   "U.M.Perf.2|C�d. de la unidad de medida de la velocidad de perfusi�n"
                  Top             =   2640
                  Width           =   615
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   47
                  Left            =   7800
                  TabIndex        =   72
                  Tag             =   "Volumen(ml) Producto 2"
                  Top             =   3360
                  Width           =   732
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   48
                  Left            =   5400
                  TabIndex        =   69
                  Tag             =   "FF2"
                  Top             =   3360
                  Width           =   615
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   49
                  Left            =   7080
                  TabIndex        =   71
                  Tag             =   "UM2"
                  Top             =   3360
                  Width           =   660
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   50
                  Left            =   6120
                  TabIndex        =   70
                  Tag             =   "Dosis2"
                  Top             =   3360
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   51
                  Left            =   0
                  TabIndex        =   68
                  Tag             =   "Descripci�n Medicamento 2"
                  Top             =   3360
                  Width           =   5340
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   52
                  Left            =   2520
                  TabIndex        =   133
                  Tag             =   "C�digo Medicamento 2"
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   53
                  Left            =   2640
                  TabIndex        =   132
                  Tag             =   "Operaci�n"
                  Top             =   1200
                  Visible         =   0   'False
                  Width           =   612
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Index           =   54
                  Left            =   4200
                  TabIndex        =   131
                  Tag             =   "Ubicaci�n"
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   612
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   930
                  Index           =   39
                  Left            =   0
                  Locked          =   -1  'True
                  MultiLine       =   -1  'True
                  ScrollBars      =   3  'Both
                  TabIndex        =   61
                  Tag             =   "Inst.Admin.|Instrucciones para administraci�n"
                  Top             =   2040
                  Width           =   5325
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   38
                  Left            =   0
                  TabIndex        =   43
                  Tag             =   "Descripci�n Medicamento"
                  Top             =   240
                  Width           =   5295
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   55
                  Left            =   120
                  TabIndex        =   44
                  Tag             =   "Medic. No Formulario"
                  Top             =   120
                  Visible         =   0   'False
                  Width           =   5295
               End
               Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
                  Height          =   330
                  Index           =   7
                  Left            =   5520
                  TabIndex        =   53
                  Tag             =   "Fecha Inicio Vigencia"
                  Top             =   840
                  Width           =   1860
                  _Version        =   65537
                  _ExtentX        =   3281
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DefaultDate     =   ""
                  MinDate         =   "1900/1/1"
                  MaxDate         =   "2100/12/31"
                  Format          =   "DD/MM/YYYY"
                  AllowNullDate   =   -1  'True
                  ShowCentury     =   -1  'True
                  Mask            =   2
                  StartofWeek     =   2
               End
               Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
                  Height          =   330
                  Index           =   8
                  Left            =   5520
                  TabIndex        =   62
                  Tag             =   "Fecha Fin"
                  Top             =   2040
                  Width           =   1860
                  _Version        =   65537
                  _ExtentX        =   3281
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DefaultDate     =   ""
                  MinDate         =   "1900/1/1"
                  MaxDate         =   "2100/12/31"
                  Format          =   "DD/MM/YYYY"
                  AllowNullDate   =   -1  'True
                  ShowCentury     =   -1  'True
                  Mask            =   2
                  StartofWeek     =   2
               End
               Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
                  Height          =   330
                  Index           =   1
                  Left            =   2760
                  TabIndex        =   51
                  Tag             =   "C�digo V�a"
                  Top             =   840
                  Width           =   765
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets.count =   2
                  stylesets(0).Name=   "Activo"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).Picture=   "FR0177.frx":0134
                  stylesets(1).Name=   "Inactivo"
                  stylesets(1).BackColor=   255
                  stylesets(1).Picture=   "FR0177.frx":0150
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   1349
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   16777215
                  DataFieldToDisplay=   "Column 0"
               End
               Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
                  Height          =   330
                  Index           =   0
                  Left            =   960
                  TabIndex        =   50
                  Tag             =   "Cod.Frecuencia"
                  Top             =   840
                  Width           =   1725
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets.count =   2
                  stylesets(0).Name=   "Activo"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).Picture=   "FR0177.frx":016C
                  stylesets(1).Name=   "Inactivo"
                  stylesets(1).BackColor=   255
                  stylesets(1).Picture=   "FR0177.frx":0188
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   3043
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   16777215
                  DataFieldToDisplay=   "Column 0"
               End
               Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
                  Height          =   330
                  Index           =   3
                  Left            =   3600
                  TabIndex        =   52
                  Tag             =   "Cod. Periodicidad"
                  Top             =   840
                  Width           =   1845
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets.count =   2
                  stylesets(0).Name=   "Activo"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).Picture=   "FR0177.frx":01A4
                  stylesets(1).Name=   "Inactivo"
                  stylesets(1).BackColor=   255
                  stylesets(1).Picture=   "FR0177.frx":01C0
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   3254
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   16777215
                  DataFieldToDisplay=   "Column 0"
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Medicamento"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   54
                  Left            =   0
                  TabIndex        =   166
                  Top             =   0
                  Width           =   1140
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Cantidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   53
                  Left            =   0
                  TabIndex        =   165
                  Top             =   600
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Dosis"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   46
                  Left            =   6120
                  TabIndex        =   164
                  Top             =   0
                  Width           =   480
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "UM"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   45
                  Left            =   7080
                  TabIndex        =   163
                  Top             =   0
                  Width           =   300
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Frecuencia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   44
                  Left            =   960
                  TabIndex        =   162
                  Top             =   600
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "V�a"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   43
                  Left            =   2760
                  TabIndex        =   161
                  Top             =   600
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Periodicidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   42
                  Left            =   3600
                  TabIndex        =   160
                  Top             =   600
                  Width           =   1335
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Fecha Fin"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   41
                  Left            =   5520
                  TabIndex        =   159
                  Top             =   1800
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hora "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   40
                  Left            =   7440
                  TabIndex        =   158
                  Top             =   600
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Fecha Inicio"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   39
                  Left            =   5520
                  TabIndex        =   157
                  Top             =   600
                  Width           =   1815
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hora "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   38
                  Left            =   7440
                  TabIndex        =   156
                  Top             =   1800
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Soluci�n para Diluir"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   34
                  Left            =   0
                  TabIndex        =   155
                  Top             =   1200
                  Width           =   2055
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Volumen(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   35
                  Left            =   5520
                  TabIndex        =   154
                  Top             =   1200
                  Width           =   1035
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Tiempo Infusi�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   36
                  Left            =   6600
                  TabIndex        =   153
                  Top             =   1200
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "FF"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   37
                  Left            =   5400
                  TabIndex        =   152
                  Top             =   0
                  Width           =   225
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Volumen(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   49
                  Left            =   7800
                  TabIndex        =   151
                  Top             =   0
                  Width           =   1035
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Instrucciones para administraci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   50
                  Left            =   0
                  TabIndex        =   150
                  Top             =   1800
                  Width           =   2865
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Vol.Total(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   51
                  Left            =   8160
                  TabIndex        =   149
                  Top             =   2400
                  Width           =   1080
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Vel.Perfus."
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   52
                  Left            =   5520
                  TabIndex        =   148
                  Top             =   2400
                  Width           =   945
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Uni. Medida"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   62
                  Left            =   6600
                  TabIndex        =   147
                  Top             =   2400
                  Width           =   1035
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "/"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   77
                  Left            =   7260
                  TabIndex        =   146
                  Top             =   2685
                  Width           =   105
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Volumen(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   33
                  Left            =   7800
                  TabIndex        =   145
                  Top             =   3120
                  Width           =   1035
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "FF"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   80
                  Left            =   5400
                  TabIndex        =   144
                  Top             =   3120
                  Width           =   225
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "UM"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   81
                  Left            =   7080
                  TabIndex        =   143
                  Top             =   3120
                  Width           =   300
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Dosis"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   82
                  Left            =   6120
                  TabIndex        =   142
                  Top             =   3120
                  Width           =   480
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Medicamento 2"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   83
                  Left            =   0
                  TabIndex        =   141
                  Top             =   3120
                  Width           =   1305
               End
            End
            Begin VB.TextBox txtText1 
               Height          =   570
               Index           =   3
               Left            =   120
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   127
               Tag             =   "Descripci�n Producto"
               Top             =   720
               Width           =   6015
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   2
               Left            =   7080
               TabIndex        =   126
               Tag             =   "C�digo Producto"
               Top             =   840
               Width           =   1100
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Necesidad Quir�fano"
               DataField       =   "FR57INDNECESQUIR"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   -73200
               TabIndex        =   37
               Tag             =   "Necesidad Quir�fano?"
               Top             =   960
               Width           =   2415
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Pedido Farmacia"
               DataField       =   "FR57INDPEDFARM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   -73200
               TabIndex        =   36
               Tag             =   "Pedido Farmacia?"
               Top             =   720
               Width           =   2175
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "O.M.E.C."
               DataField       =   "FR57INDOMEC"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -74760
               TabIndex        =   35
               Tag             =   "O.M.E.C.?"
               Top             =   960
               Width           =   1095
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "OM/PRN"
               DataField       =   "FR57INDOMPRN"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   -74760
               TabIndex        =   34
               Tag             =   "OM/PRN?"
               Top             =   720
               Width           =   1335
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR57CODPETOBSER"
               Height          =   330
               Index           =   0
               Left            =   -73680
               TabIndex        =   32
               Tag             =   "C�digo Petici�n"
               Top             =   1560
               Visible         =   0   'False
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR57NUMLINEA"
               Height          =   330
               Index           =   1
               Left            =   -67080
               TabIndex        =   33
               Tag             =   "N�mero L�nea"
               Top             =   1560
               Visible         =   0   'False
               Width           =   372
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR27CODTIPOBSER"
               Height          =   330
               Index           =   4
               Left            =   -74760
               TabIndex        =   40
               Tag             =   "C�digo Tipo Observaci�n"
               Top             =   1560
               Width           =   372
            End
            Begin VB.TextBox txtText1 
               Height          =   330
               Index           =   5
               Left            =   -72480
               TabIndex        =   41
               Tag             =   "Descripci�n Tipo Obsevaci�n"
               Top             =   1560
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR57OBSERVACION"
               Height          =   1650
               Index           =   6
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   42
               Tag             =   "Observaciones"
               Top             =   2160
               Width           =   9000
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR57CODOBSFARM"
               Height          =   330
               Index           =   7
               Left            =   -68160
               TabIndex        =   120
               Tag             =   "C�digo Petici�n"
               Top             =   2400
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_VAL"
               Height          =   330
               Index           =   8
               Left            =   -66600
               TabIndex        =   119
               Tag             =   "C�digo Validador"
               Top             =   1800
               Visible         =   0   'False
               Width           =   1100
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR57FECINIVIG"
               Height          =   330
               Index           =   1
               Left            =   -70560
               TabIndex        =   38
               Tag             =   "Fecha Inicio Vigencia OF"
               Top             =   960
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR57FECFINVIG"
               Height          =   330
               Index           =   0
               Left            =   -68400
               TabIndex        =   39
               Tag             =   "Fecha Inicio Vigencia OF"
               Top             =   960
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   129
               Top             =   480
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cod. Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   6960
               TabIndex        =   128
               Top             =   600
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Fin "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   -68400
               TabIndex        =   125
               Top             =   720
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Inicio "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   -70560
               TabIndex        =   124
               Top             =   720
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d. Tipo Observaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   -74760
               TabIndex        =   123
               Top             =   1320
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Desc. Tipo Observaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   -72480
               TabIndex        =   122
               Top             =   1320
               Width           =   2535
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   -74760
               TabIndex        =   121
               Top             =   1920
               Width           =   2535
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
            Height          =   4065
            Index           =   0
            Left            =   -74880
            TabIndex        =   77
            TabStop         =   0   'False
            Top             =   120
            Width           =   9495
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16748
            _ExtentY        =   7170
            _StockProps     =   79
            Caption         =   "BLOQUEADA"
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   74
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmObservaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmBloqueoLinea (FR0105.FRM)                                 *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Bloqueo de L�neas                                       *
'* ARGUMENTOS:  vntdata(1)=fr57codpetobserv                             *
'*              vntdata(2)=fr57numlinea                                 *
'*              vntdata(3)=fr57indomprn                                 *
'*              vntdata(4)=fr57indomec                                  *
'*              vntdata(5)=fr57indnecesquir                             *
'*              vntdata(6)=fr57fecinivig                                *
'*              vntdata(7)=fr57indpedfarm                               *
'*              vntdata(8)=fr73codproducto                              *
'*              vntdata(9)=fr57fecfinvig                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************


Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Dim LINK2 As clsCWLinked




Private Sub LabelObservRojo_Click()
  tab1.Tab = 4
End Sub

Private Sub tab1_Click(PreviousTab As Integer)
If txtText1(18).Text <> "" Then
  LabelObservRojo.Top = 80
  LabelObservRojo.Left = 4320
  LabelObservRojo.Visible = True
  LabelObservRojo.ZOrder (0)
Else
  LabelObservRojo.Visible = False
End If
End Sub


Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  Dim link As clsCWLinked
  

  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grddbgrid1(2)
    
    '.intCursorSize = 3
    
    .strName = "Petici�n"
      
    .strTable = "FR6600"
    
    .strWhere = "FR66CODPETICION=" & frmVerLineasBloq.grddbgrid1(0).Columns(4).Value
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR66CODPETICION", cwDescending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "OM")
    Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR66FECREDACCION", "Fecha Redacci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDMEDINF", "Medicaci�n Infantil?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
   
    'Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "C�digo Grupo Protocolo")
  
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grddbgrid1(0)
    '.intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Linea Bloquedada"
    
    .strTable = "FR5700"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR57CODOBSFARM", cwDescending)
    Call .FormAddRelation("FR57CODPETOBSER", txtText1(10))
    .strWhere = "FR57CODPETOBSER=" & frmVerLineasBloq.grddbgrid1(0).Columns(4).Value & _
              " AND FR57NUMLINEA=" & frmVerLineasBloq.grddbgrid1(0).Columns(5).Value
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormDetail)
    
    
    Call .FormCreateInfo(objMasterInfo)
    
    .CtrlGetInfo(txtText1(39)).blnNegotiated = False
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(26), "FR26DESESTADOPET")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(13), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(15), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(19), "CI22SEGAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(23), "CI22NUMHISTORIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(20)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(20)), txtText1(21), "SG02NOM")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(20)), txtText1(36), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(32)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(32)), txtText1(33), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(11), "FR91DESURGENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(65)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(65)), txtText1(64), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR57CODPETOBSER", "SELECT * FROM FR2800 WHERE FR66CODPETICION=? AND FR28NUMLINEA =? ")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(61), "FR73CODPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(41), "frh7codformfar")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(59), "fr28dosis")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(58), "fr93codunimedida")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), chkCheck1(9), "FR28INDCOMIENINMED")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), chkCheck1(8), "FR28INDPERF")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(60), "FR28CANTIDAD")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), cboDBCombo1(0), "FRG4CODFRECUENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), cboDBCombo1(1), "FR34CODVIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), cboDBCombo1(3), "FRH5CODPERIODICIDAD")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), dtcDateCombo1(7), "FR28FECINICIO")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), dtcDateCombo1(8), "FR28FECFIN")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(57), "FR28HORAINICIO")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(56), "FR28HORAFIN")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(42), "FR28CANTIDADDIL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(52), "FR73CODPRODUCTO_2")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(46), "FR73CODPRODUCTO_DIL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(54), "FR28UBICACION")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(53), "FR28OPERACION")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(43), "FR28TIEMMININF")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(39), "FR28INSTADMIN")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(82), "FR28VELPERFUSION")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(81), "FR93CODUNIMEDIDA_PERF1")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(80), "FR93CODUNIMEDIDA_PERF2")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(44), "FR28VOLTOTAL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(48), "FRH7CODFORMFAR_2")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(50), "FR28DOSIS_2")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(49), "FR93CODUNIMEDIDA_2")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(61)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(61)), txtText1(38), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(52)), "FR73CODPRODUCTO_2", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(52)), txtText1(51), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(46)), "FR73CODPRODUCTO_DIL", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(46)), txtText1(45), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "FR27CODTIPOBSER", "SELECT * FROM FR2700 WHERE FR27CODTIPOBSER=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(5), "FR27DESTIPOBSER")
    
    Call .WinRegister
    Call .WinStabilize
  
  End With
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Tipos de Interacci�n" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch

  
  If strCtrl = "txtText1(4)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR2700"
     
     Set objField = .AddField("FR27CODTIPOBSER")
     objField.strSmallDesc = "Tipo Observaci�n"
         
     Set objField = .AddField("FR27DESTIPOBSER")
     objField.strSmallDesc = "Descripci�n Tipo Observaci�n"
     
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(4), .cllValues("FR27CODTIPOBSER"))
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
    
  'para comprobar que se trata del control en cuestion
  If strFormName = "Linea Bloquedada" And strCtrlName = "txtText1(0)" Then
    aValues(2) = txtText1(1)
  End If
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Dim intpreg As Integer
    Dim strupdate As String
    Dim rsta As rdoResultset
    Dim sqlstr As String
    
    Select Case btnButton.Index
        Case 2:
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          sqlstr = "SELECT FR57CODOBSFARM_SEQUENCE.nextval FROM dual"
          Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
          txtText1(7).Text = rsta(0).Value
          txtText1(0).Text = gvntdatafr0105(1)
          txtText1(1).Text = gvntdatafr0105(2)
          txtText1(2).Text = gvntdatafr0105(8)
          chkCheck1(0).Value = gvntdatafr0105(3)
          chkCheck1(1).Value = gvntdatafr0105(4)
          chkCheck1(3).Value = gvntdatafr0105(5)
          chkCheck1(2).Value = gvntdatafr0105(7)
          txtText1(8).Text = objsecurity.strUser
          If gvntdatafr0105(9) <> "null" Then
              dtcDateCombo1(0).Date = gvntdatafr0105(9)
          End If
          If gvntdatafr0105(6) <> "null" Then
              dtcDateCombo1(1).Date = gvntdatafr0105(6)
          End If
          rsta.Close
          Set rsta = Nothing
        Case 8:
            intpreg = MsgBox("�Est� seguro de que desea Desbloquear esta l�nea?", vbQuestion + vbYesNo)
            If intpreg = vbNo Then
                Exit Sub
            End If
            strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=0 where FR66CODPETICION=" & gvntdatafr0105(1) & _
                    " AND FR28NUMLINEA=" & gvntdatafr0105(2)
            objApp.rdoConnect.Execute strupdate, 64
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
        Case 30:
            If txtText1(7).Text <> "" Then
                sqlstr = "SELECT * from FR5700 WHERE FR57CODOBSFARM=" & txtText1(7).Text
                Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
                If rsta.EOF = True Then
                    strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=0 where FR66CODPETICION=" & gvntdatafr0105(1) & _
                    " AND FR28NUMLINEA=" & gvntdatafr0105(2)
                    objApp.rdoConnect.Execute strupdate, 64
                End If
                rsta.Close
                Set rsta = Nothing
            End If
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        Case Else:
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End Select
    
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub







' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Dim stra As String
  Dim rsta As rdoResultset
  
  Call objWinInfo.CtrlDataChange
  
  If intIndex = 18 Then
    If txtText1(18).Text <> "" Then
      LabelObservRojo.Top = 80
      LabelObservRojo.Left = 4320
      LabelObservRojo.Visible = True
      LabelObservRojo.ZOrder (0)
    Else
      LabelObservRojo.Visible = False
    End If
  End If
  
  If intIndex = 0 Then
    If txtText1(0).Text <> "" And txtText1(1).Text <> "" Then
      stra = "SELECT * FROM FR2800 WHERE  FR66CODPETICION=" & txtText1(0).Text & _
            " AND FR28NUMLINEA=" & txtText1(1).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta.EOF = False Then
          If IsNull(rsta("FR28INSTRADMIN").Value) = False Then
            txtText1(39).Text = rsta("FR28INSTRADMIN").Value
          End If
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  End If
  If intIndex = 61 Then
    If txtText1(61).Text = "999999999" Then
      lblLabel1(54).Caption = "Medicamento No Formulario"
      txtText1(55).Top = 240
      txtText1(55).Left = 0
      txtText1(55).Visible = True
    Else
      lblLabel1(54).Caption = "Medicamento"
      txtText1(55).Visible = False
    End If
  End If
  'tlbToolbar1.Buttons(4).Enabled = True
End Sub

