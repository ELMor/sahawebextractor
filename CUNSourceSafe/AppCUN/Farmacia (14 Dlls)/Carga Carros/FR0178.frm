VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmVisOM 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Visualizar OM"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0178.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame4 
      Caption         =   "Seleccionar "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   2055
      Left            =   120
      TabIndex        =   11
      Top             =   480
      Width           =   5655
      Begin VB.OptionButton optOMPRN 
         Caption         =   "Cerradas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   6
         Left            =   360
         TabIndex        =   19
         Top             =   1680
         Width           =   1215
      End
      Begin VB.OptionButton optOMPRN 
         Caption         =   "Con l�neas bloqueadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   5
         Left            =   360
         TabIndex        =   17
         Top             =   1320
         Width           =   2415
      End
      Begin VB.OptionButton optOMPRN 
         Caption         =   "Validadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   4
         Left            =   360
         TabIndex        =   16
         Top             =   960
         Width           =   1215
      End
      Begin VB.OptionButton optOMPRN 
         Caption         =   "Dispensadas Parciales"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Index           =   3
         Left            =   1680
         TabIndex        =   15
         Top             =   840
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.OptionButton optOMPRN 
         Caption         =   "Enviadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   2
         Left            =   360
         TabIndex        =   14
         Top             =   600
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton optOMPRN 
         Caption         =   "Redactadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   360
         TabIndex        =   13
         Top             =   240
         Width           =   1455
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4680
         TabIndex        =   12
         Top             =   480
         Value           =   1  'Checked
         Width           =   855
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcentrega 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   2760
         TabIndex        =   22
         Tag             =   "Inicio|Fecha Inicio Vigencia"
         Top             =   480
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcValidacion 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   2760
         TabIndex        =   23
         Tag             =   "Inicio|Fecha Inicio Vigencia"
         Top             =   1080
         Visible         =   0   'False
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcCierre 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   2760
         TabIndex        =   24
         Tag             =   "Inicio|Fecha Inicio Vigencia"
         Top             =   1680
         Visible         =   0   'False
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label lblCierre 
         Caption         =   "Fecha Cierre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   2760
         TabIndex        =   21
         Top             =   1440
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.Label lblValidacion 
         Caption         =   "Fecha Validaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   2760
         TabIndex        =   20
         Top             =   840
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "Fecha Redacci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   2760
         TabIndex        =   18
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Historia :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   855
      Left            =   6000
      TabIndex        =   10
      Top             =   1680
      Width           =   1695
      Begin VB.TextBox txtHistoria 
         Height          =   315
         Left            =   240
         MaxLength       =   7
         TabIndex        =   3
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdFiltar 
      Caption         =   "&Filtrar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9360
      TabIndex        =   4
      Top             =   1920
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      Caption         =   "Servicio :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1215
      Left            =   6000
      TabIndex        =   9
      Top             =   480
      Width           =   5655
      Begin VB.TextBox txtServicio 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   1
         TabStop         =   0   'False
         Tag             =   "Desc.Servicio"
         Top             =   360
         Width           =   4125
      End
      Begin VB.CheckBox chkservicio 
         Caption         =   "Todos los servicios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   840
         Value           =   1  'Checked
         Width           =   2055
      End
      Begin SSDataWidgets_B.SSDBCombo cboservicio 
         Height          =   315
         Left            =   240
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   360
         Width           =   1095
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2223
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1931
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5295
      Index           =   2
      Left            =   120
      TabIndex        =   5
      Tag             =   "Actuaciones Asociadas"
      Top             =   2760
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4815
         Index           =   1
         Left            =   120
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   360
         Width           =   11370
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "NoDispensada"
         stylesets(0).BackColor=   16776960
         stylesets(0).Picture=   "FR0178.frx":000C
         stylesets(1).Name=   "Dispensada"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "FR0178.frx":0028
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20055
         _ExtentY        =   8493
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmVisOM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0124.FRM                                                   *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: visualizar OM/PRN                                       *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim blnInload As Boolean
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cboservicio_Change()

If IsNumeric(cboservicio.Text) Then
    If chkservicio.Value = 0 Then
    Else
        chkservicio.Value = False
    End If
    txtServicio.Text = cboservicio.Columns(1).Value
End If

  grddbgrid1(1).RemoveAll

End Sub

Private Sub cboservicio_Click()

If IsNumeric(cboservicio.Text) Then
    If chkservicio.Value = 0 Then
    Else
        chkservicio.Value = False
    End If
    txtServicio.Text = cboservicio.Columns(1).Value
End If

  grddbgrid1(1).RemoveAll

End Sub

Private Sub Check1_Click()

If Check1 = 1 Then
    dtcentrega.Text = ""
    dtcValidacion.Text = ""
    dtcCierre.Text = ""
End If

  grddbgrid1(1).RemoveAll

End Sub

Private Sub cmdFiltar_Click()

cmdFiltar.Enabled = False
Screen.MousePointer = vbHourglass
  If chkservicio = 1 Then
    cboservicio.Text = ""
    txtServicio.Text = ""
    If optOMPRN(1).Value = True Then
      objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=1"
      If IsDate(dtcentrega) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcValidacion) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcCierre) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
      End If
    End If
    If optOMPRN(2).Value = True Then
      objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=3"
      If IsDate(dtcentrega) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcValidacion) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcCierre) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
      End If
    End If
    If optOMPRN(3).Value = True Then
      objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=9"
      If IsDate(dtcentrega) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcValidacion) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcCierre) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
      End If
    End If
    If optOMPRN(4).Value = True Then
      objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=4"
      If IsDate(dtcentrega) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcValidacion) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcCierre) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
      End If
    End If
    If optOMPRN(6).Value = True Then
      objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=11"
      If IsDate(dtcentrega) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcValidacion) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcCierre) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
      End If
    End If
    If optOMPRN(5).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1" & _
                " AND FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 " & _
                                         "WHERE FR28INDBLOQUEADA=-1)"
      If IsDate(dtcentrega) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcValidacion) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcCierre) And Check1.Value = 0 Then
        objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
      End If
    End If
  Else
    If cboservicio <> "" Then
        txtServicio.Text = cboservicio.Columns(1).Value
        If optOMPRN(1).Value = True Then
          objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=1 AND AD02CODDPTO=" & cboservicio.Text
          If IsDate(dtcentrega) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcValidacion) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcCierre) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
          End If
        End If
        If optOMPRN(2).Value = True Then
          objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=3 AND AD02CODDPTO=" & cboservicio.Text
          If IsDate(dtcentrega) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcValidacion) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcCierre) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
          End If
        End If
        If optOMPRN(3).Value = True Then
          objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=9 AND AD02CODDPTO=" & cboservicio.Text
          If IsDate(dtcentrega) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcValidacion) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcCierre) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
          End If
        End If
        If optOMPRN(4).Value = True Then
          objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=4 AND AD02CODDPTO=" & cboservicio.Text
          If IsDate(dtcentrega) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcValidacion) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcCierre) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
          End If
        End If
        If optOMPRN(6).Value = True Then
          objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 AND FR26CODESTPETIC=11 AND AD02CODDPTO=" & cboservicio.Text
          If IsDate(dtcentrega) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcValidacion) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcCierre) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
          End If
        End If
        If optOMPRN(5).Value = True Then
              objWinInfo.objWinActiveForm.strWhere = "FR66INDOM=-1 " & _
              " AND FR66CODPETICION IN (SELECT FR66CODPETICION FROM FR2800 " & _
                                       "WHERE FR28INDBLOQUEADA=-1) AND AD02CODDPTO=" & cboservicio.Text
          If IsDate(dtcentrega) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECREDACCION)=TO_DATE('" & dtcentrega & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcValidacion) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECVALIDACION)=TO_DATE('" & dtcValidacion & "','DD/MM/YYYY')"
          End If
          If IsDate(dtcCierre) And Check1.Value = 0 Then
            objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR66FECCIERRE)=TO_DATE('" & dtcCierre & "','DD/MM/YYYY')"
          End If
        End If
    End If
  End If

    If IsNumeric(txthistoria.Text) Then
      If objWinInfo.objWinActiveForm.strWhere <> "" Then
        If objWinInfo.objWinActiveForm.strWhere <> "-1=0" Then
          objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txthistoria.Text & ")"
        Else
          objWinInfo.objWinActiveForm.strWhere = " CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txthistoria.Text & ")"
        End If
      Else
        objWinInfo.objWinActiveForm.strWhere = " CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txthistoria.Text & ")"
      End If
    End If

Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
objWinInfo.DataRefresh
Screen.MousePointer = vbDefault
cmdFiltar.Enabled = True

End Sub

Private Sub dtcentrega_Change()

If Check1 = 1 Then
    Check1 = 0
End If
grddbgrid1(1).RemoveAll

End Sub


Private Sub dtcentrega_CloseUp()

If Check1 = 1 Then
    Check1 = 0
End If
grddbgrid1(1).RemoveAll

End Sub
Private Sub dtcValidacion_Change()

If Check1 = 1 Then
    Check1 = 0
End If
grddbgrid1(1).RemoveAll

End Sub


Private Sub dtcValidacion_CloseUp()

If Check1 = 1 Then
    Check1 = 0
End If
grddbgrid1(1).RemoveAll

End Sub
Private Sub dtcCierre_Change()

If Check1 = 1 Then
    Check1 = 0
End If
grddbgrid1(1).RemoveAll

End Sub


Private Sub dtcCierre_CloseUp()

If Check1 = 1 Then
    Check1 = 0
End If
grddbgrid1(1).RemoveAll

End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)

If strFormName = "Petici�n" And strCtrlName = "grdDBGrid1(1).Asistencia" Then
    'aValues(2) = grdDBGrid1(1).Columns("Proceso").Value  'proceso
    aValues(2) = objWinInfo.objWinActiveForm.rdoCursor("AD07CODPROCESO").Value
End If

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


Private Sub cboservicio_CloseUp()

  If IsNumeric(cboservicio.Text) Then
    If chkservicio.Value = 0 Then
    Else
        chkservicio.Value = False
    End If
    txtServicio.Text = cboservicio.Columns(1).Value
  End If
  
  grddbgrid1(1).RemoveAll

End Sub



Private Sub chkservicio_Click()

If chkservicio = 1 Then
    cboservicio.Text = ""
    txtServicio.Text = ""
Else
    If cboservicio <> "" Then
    End If
End If

  grddbgrid1(1).RemoveAll

End Sub





Private Sub Form_Activate()
  Dim stra As String
  Dim rsta As rdoResultset
  
If blnInload Then
  cboservicio.RemoveAll
  stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
         "WHERE AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND" & _
         "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL))) ORDER BY AD02DESDPTO"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While (Not rsta.EOF)
      Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
      rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  optOMPRN(2).Value = True
  
  stra = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  dtcentrega.Text = rsta.rdoColumns(0).Value
  dtcValidacion.Text = rsta.rdoColumns(0).Value
  dtcCierre.Text = rsta.rdoColumns(0).Value
  rsta.Close
  Set rsta = Nothing
  dtcentrega.Text = ""
  dtcValidacion.Text = ""
  dtcCierre.Text = ""
  blnInload = False
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMultiInfo As New clsCWForm
Dim strKey As String
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Petici�n"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grddbgrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR6600"
        .intAllowance = cwAllowReadOnly
        .strWhere = "-1=0" 'el grid vac�o
        
        Call .FormAddOrderField("FR66CODPETICION", cwDescending)
        '.intCursorSize = 0
    
        strKey = .strDataBase & .strTable
        Call .FormCreateFilterWhere(strKey, "Petici�n")
        Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
        Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
        Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
        Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
        Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
        Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
        Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
        Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
        Call .FormAddFilterWhere(strKey, "FR66INDMEDINF", "Medicaci�n Infantil?", cwBoolean)
        Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�digo Petici�n", "FR66CODPETICION", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Cesta", "FR66INDCESTA", cwBoolean)
        Call .GridAddColumn(objMultiInfo, "C�d.Estado", "FR26CODESTPETIC", cwNumeric, 1)
        Call .GridAddColumn(objMultiInfo, "Estado", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Emisi�n", "FR66FECREDACCION", cwDate)
        Call .GridAddColumn(objMultiInfo, "Hora", "FR66HORAREDACCI", cwDecimal, 2)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Call .GridAddColumn(objMultiInfo, "Validaci�n", "FR66FECVALIDACION", cwDate)
        Call .GridAddColumn(objMultiInfo, "Cierre", "FR66FECCIERRE", cwDate)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Call .GridAddColumn(objMultiInfo, "Fecha Firma", "FR66FECFIRMMEDI", cwDate)
        Call .GridAddColumn(objMultiInfo, "Hora Firma", "FR66HORAFIRMMEDI", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "C�digo Persona", "CI21CODPERSONA", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo, "Cama", "", cwString, 7)
        Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "C�d.M�dico", "SG02COD_MED", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Dr.", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d.Urgencia", "FR91CODURGENCIA", cwNumeric, 2)
        Call .GridAddColumn(objMultiInfo, "Urgencia", "", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "C�d.Servicio", "AD02CODDPTO", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Proceso", "AD07CODPROCESO", cwNumeric, 10)
        Call .GridAddColumn(objMultiInfo, "Asistencia", "AD01CODASISTENCI", cwNumeric, 10)
        
        Call .FormCreateInfo(objMultiInfo)
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grddbgrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grddbgrid1(1).Columns(4)).blnInFind = True
        .CtrlGetInfo(grddbgrid1(1).Columns(5)).blnInFind = True
        .CtrlGetInfo(grddbgrid1(1).Columns(7)).blnInFind = True
        .CtrlGetInfo(grddbgrid1(1).Columns(8)).blnInFind = True
        .CtrlGetInfo(grddbgrid1(1).Columns(11)).blnInFind = True
        .CtrlGetInfo(grddbgrid1(1).Columns(12)).blnInFind = True
        .CtrlGetInfo(grddbgrid1(1).Columns(13)).blnInFind = True
        .CtrlGetInfo(grddbgrid1(1).Columns(19)).blnInFind = True
        .CtrlGetInfo(grddbgrid1(1).Columns(21)).blnInFind = True
        .CtrlGetInfo(grddbgrid1(1).Columns(23)).blnInFind = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(5)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(5)), grddbgrid1(1).Columns(6), "FR26DESESTADOPET")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(13)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(13)), grddbgrid1(1).Columns(14), "CI22NUMHISTORIA")
        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(13)), grddbgrid1(1).Columns(16), "CI22NOMBRE")
        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(13)), grddbgrid1(1).Columns(17), "CI22PRIAPEL")
        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(13)), grddbgrid1(1).Columns(18), "CI22SEGAPEL")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(19)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(19)), grddbgrid1(1).Columns(20), "SG02APE1")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(21)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(21)), grddbgrid1(1).Columns(22), "FR91DESURGENCIA")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns(23)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns(23)), grddbgrid1(1).Columns(24), "AD02DESDPTO")
   
        Call .CtrlCreateLinked(.CtrlGetInfo(grddbgrid1(1).Columns("Asistencia")), "AD01CODASISTENCI", "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD01CODASISTENCI= ? AND AD07CODPROCESO=?")
        Call .CtrlAddLinked(.CtrlGetInfo(grddbgrid1(1).Columns("Asistencia")), grddbgrid1(1).Columns("Cama"), "GCFN06(AD15CODCAMA)")
        
   
        Call .WinRegister
        Call .WinStabilize
    End With
    
    grddbgrid1(1).Columns(4).Visible = False '
    grddbgrid1(1).Columns(5).Visible = False 'c�d.estado
    grddbgrid1(1).Columns(6).Visible = False 'des.estado
    grddbgrid1(1).Columns(8).Visible = False 'hora de redacci�n
    grddbgrid1(1).Columns(11).Visible = False 'firma
    grddbgrid1(1).Columns(12).Visible = False 'hora firma
    grddbgrid1(1).Columns(13).Visible = False 'c�d.paciente
    grddbgrid1(1).Columns(18).Visible = False 'c�d.m�dico
    grddbgrid1(1).Columns(20).Visible = False 'c�d.urgencia
    grddbgrid1(1).Columns(22).Visible = False 'c�d.servicio
    
    grddbgrid1(1).Columns(3).Width = 1000 'c�d petici�n
    grddbgrid1(1).Columns(6).Width = 1600 'estado
    grddbgrid1(1).Columns(7).Width = 1700 'fecha redacci�n
    grddbgrid1(1).Columns(8).Width = 700 'hora redacci�n
    grddbgrid1(1).Columns(9).Width = 1700 'fecha validaci�n
    grddbgrid1(1).Columns(10).Width = 1700 'fecha cierre
    grddbgrid1(1).Columns(11).Width = 1200 'fecha firma
    grddbgrid1(1).Columns(12).Width = 700 'hora firma
    grddbgrid1(1).Columns(14).Width = 900 'historia
    grddbgrid1(1).Columns(15).Width = 700 'cama
    grddbgrid1(1).Columns(16).Width = 1530 'apellido 1�
    grddbgrid1(1).Columns(17).Width = 1530 'apellido 2�
    grddbgrid1(1).Columns(19).Width = 1600 'doctor
    grddbgrid1(1).Columns(21).Width = 800 'urgencia
    grddbgrid1(1).Columns(23).Width = 750 'servicio
   
blnInload = True

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub


Private Sub optOMPRN_Click(Index As Integer)

grddbgrid1(1).RemoveAll

Select Case Index
  Case 4 'validadas
    dtcValidacion.Visible = True
    lblValidacion.Visible = True
  Case 6 'cerradas
    dtcValidacion.Visible = True
    lblValidacion.Visible = True
    dtcCierre.Visible = True
    lblCierre.Visible = True
  Case Else
    dtcValidacion.Visible = False
    dtcCierre.Visible = False
    lblValidacion.Visible = False
    lblCierre.Visible = False
End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
Dim stra As String
Dim rsta As rdoResultset

Call objWinInfo.GridDblClick
'se pasa el n� de orden para ir a frmRedactarOMPRN y firmar
If IsNumeric(grddbgrid1(1).Columns(3).Value) Then
    stra = "SELECT * FROM FR6600 WHERE FR66CODPETICION=" & grddbgrid1(1).Columns(3).Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not rsta.EOF Then
        Me.Enabled = False
        Screen.MousePointer = vbHourglass
        gintfirmarOM = 1
        glngpeticion = grddbgrid1(1).Columns(3).Value
        Call objsecurity.LaunchProcess("FR0179")
        Screen.MousePointer = vbDefault
        Me.Enabled = True
    Else
        Call MsgBox("La petici�n ha sido borrada, refresque la pantalla por favor.", vbInformation, "Aviso")
    End If
    rsta.Close
    Set rsta = Nothing
End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdDBgrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
    
    If Index = 1 Then
        If grddbgrid1(1).Columns(5).Value = 5 Or grddbgrid1(1).Columns(5).Value = 4 Then
          For i = 3 To 21
              grddbgrid1(1).Columns(i).CellStyleSet "Dispensada"
          Next i
        Else
          For i = 3 To 21
              grddbgrid1(1).Columns(i).CellStyleSet "NoDispensada"
          Next i
        End If
    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtHistoria_Change()

grddbgrid1(1).RemoveAll

End Sub
