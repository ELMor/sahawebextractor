VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************

'Const PRWinAlertaInmovilizacion             As String = "FR0003"
Const PRWinDefEnsayClinic                   As String = "FR0225"
Const PRWinRedactarOMEC                     As String = "FR0226"
Const PRWinSelPacOMEC                       As String = "FR0227"
Const PRWinConsOMECAnte                     As String = "FR0228"
Const PRWinDefExpEnsayClinic                As String = "FR0229"
Const PRWinDefPacEnsayClin                  As String = "FR0230"
Const PRWinFirmarOMEC                       As String = "FR0231"
Const PRWinOMECSinEnv                       As String = "FR0232"
Const PRWinRevOMEC                          As String = "FR0233"
Const PRWinDispensarOMEC                    As String = "FR0234"
Const PRWinDispProdOMEC                     As String = "FR0235"
Const PRWinDevEnvOMEC                       As String = "FR0236"
Const PRWinBusGrpTera                       As String = "FR0237"
Const PRWinBusProdProt                      As String = "FR0238"
Const PRWinVerIntMedEC                      As String = "FR0239"
Const PRWinAlerPacEC                        As String = "FR0240"
Const PRWinBusGrpTeraEC                     As String = "FR0241"
Const PRWinBusProtocolosEC                  As String = "FR0242"
Const PRWinBusGrpProdEC                     As String = "FR0243"
Const PRWinBusProductosEC                   As String = "FR0244"
Const PRWinVerPerfilFTPEC                   As String = "FR0245"
Const PRWinBusPacEC                         As String = "FR0246"
Const PRWinBuscarEnsayClinic                As String = "FR0247"

'CIM
Const PRWinControlarAlerta                  As String = "FR0200"
Const PRWinActualizarBIM                    As String = "FR0201"
Const PRWinRegistrarConsultas               As String = "FR0202"
Const PRWinBuscarRespuesta                  As String = "FR0203"
Const PRWinConsultasNoRespondidas           As String = "FR0204"
Const PRWinConsultaCompleta                 As String = "FR0207"
Const PRWinEnviarRespCorreo                 As String = "FR0206"

Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
  '  Case PRWinAlertaInmovilizacion
  '    Load frmMantAlertInmov
  '    'Call objsecurity.AddHelpContext(528)
  '    Call frmMantAlertInmov.Show(vbModal)
  '    'Call objsecurity.RemoveHelpContext
  '    Unload frmMantAlertInmov
  '    Set frmMantAlertInmov = Nothing
      
    Case PRWinDefEnsayClinic
      Load frmDefEnsayClinic
      'Call objsecurity.AddHelpContext(528)
      Call frmDefEnsayClinic.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmDefEnsayClinic
      Set frmDefEnsayClinic = Nothing
      
    Case PRWinRedactarOMEC
      Load frmRedactarOMEC
      'Call objsecurity.AddHelpContext(528)
      Call frmRedactarOMEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmRedactarOMEC
      Set frmRedactarOMEC = Nothing
      
    Case PRWinSelPacOMEC
      Load frmSelPacOMEC
      'Call objsecurity.AddHelpContext(528)
      Call frmSelPacOMEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmSelPacOMEC
      Set frmSelPacOMEC = Nothing
      
    Case PRWinConsOMECAnte
      Load frmConsOMECAnte
      'Call objsecurity.AddHelpContext(528)
      Call frmConsOMECAnte.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmConsOMECAnte
      Set frmConsOMECAnte = Nothing
      
    Case PRWinDefExpEnsayClinic
      Load frmDefExpEnsayClinic
      'Call objsecurity.AddHelpContext(528)
      Call frmDefExpEnsayClinic.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmDefExpEnsayClinic
      Set frmDefExpEnsayClinic = Nothing
      
    Case PRWinDefPacEnsayClin
      Load frmDefPacEnsayClin
      'Call objsecurity.AddHelpContext(528)
      Call frmDefPacEnsayClin.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmDefPacEnsayClin
      Set frmDefPacEnsayClin = Nothing
      
    Case PRWinFirmarOMEC
      Load frmFirmarOMEC
      'Call objsecurity.AddHelpContext(528)
      Call frmFirmarOMEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmFirmarOMEC
      Set frmFirmarOMEC = Nothing
      
    Case PRWinOMECSinEnv
      Load frmOMECSinEnv
      'Call objsecurity.AddHelpContext(528)
      Call frmOMECSinEnv.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmOMECSinEnv
      Set frmOMECSinEnv = Nothing
      
    Case PRWinRevOMEC
      Load frmRevOMEC
      'Call objsecurity.AddHelpContext(528)
      Call frmRevOMEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmRevOMEC
      Set frmRevOMEC = Nothing
      
    Case PRWinDispensarOMEC
      Load frmDispensarOMEC
      'Call objsecurity.AddHelpContext(528)
      Call frmDispensarOMEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmDispensarOMEC
      Set frmDispensarOMEC = Nothing
      
    Case PRWinDispProdOMEC
      Load frmDispProdOMEC
      'Call objsecurity.AddHelpContext(528)
      Call frmDispProdOMEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmDispProdOMEC
      Set frmDispProdOMEC = Nothing
      
    Case PRWinDevEnvOMEC
      Load frmDevEnvOMEC
      'Call objsecurity.AddHelpContext(528)
      Call frmDevEnvOMEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmDevEnvOMEC
      Set frmDevEnvOMEC = Nothing
      
    Case PRWinBusGrpTera
      Load frmBusGrpTera
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpTera.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusGrpTera
      Set frmBusGrpTera = Nothing
      
    Case PRWinBusProdProt
      Load frmBusProdProt
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProdProt.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusProdProt
      Set frmBusProdProt = Nothing
      
    Case PRWinBusGrpTeraEC
      Load frmBusGrpTeraEC
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpTeraEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusGrpTeraEC
      Set frmBusGrpTeraEC = Nothing

    Case PRWinBusProtocolosEC
      Load frmBusProtocolosEC
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProtocolosEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusProtocolosEC
      Set frmBusProtocolosEC = Nothing

    Case PRWinBusGrpProdEC
      Load frmBusGrpProdEC
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpProdEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusGrpProdEC
      Set frmBusGrpProdEC = Nothing

    Case PRWinBusProductosEC
      Load frmBusProductosEC
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProductosEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusProductosEC
      Set frmBusProductosEC = Nothing

    Case PRWinVerPerfilFTPEC
      Load frmVerPerfilFTPEC
      'Call objsecurity.AddHelpContext(528)
      Call frmVerPerfilFTPEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmVerPerfilFTPEC
      Set frmVerPerfilFTPEC = Nothing

    Case PRWinBusPacEC
      Load frmBusPacEC
      'Call objsecurity.AddHelpContext(528)
      Call frmBusPacEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBusPacEC
      Set frmBusPacEC = Nothing
    Case PRWinBuscarEnsayClinic
      Load frmBuscarEnsayClinic
      'Call objsecurity.AddHelpContext(528)
      Call frmBuscarEnsayClinic.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmBuscarEnsayClinic
      Set frmBuscarEnsayClinic = Nothing
    Case PRWinVerIntMedEC
      Load frmVerIntMedEC
      'Call objsecurity.AddHelpContext(528)
      Call frmVerIntMedEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmVerIntMedEC
      Set frmVerIntMedEC = Nothing
    Case PRWinAlerPacEC
      Load frmAlerPacEC
      'Call objsecurity.AddHelpContext(528)
      Call frmAlerPacEC.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmAlerPacEC
      Set frmAlerPacEC = Nothing
    
    
      
    'CIM
    Case PRWinControlarAlerta
     Load frmControlarAlerta
     'Call objsecurity.AddHelpContext(528)
     Call frmControlarAlerta.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmControlarAlerta
     Set frmControlarAlerta = Nothing
    Case PRWinActualizarBIM
     Load frmActualizarBIM
     'Call objsecurity.AddHelpContext(528)
     Call frmActualizarBIM.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmActualizarBIM
     Set frmActualizarBIM = Nothing
   Case PRWinRegistrarConsultas
     Load frmRegistrarConsultas
     'Call objsecurity.AddHelpContext(528)
     Call frmRegistrarConsultas.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmRegistrarConsultas
     Set frmRegistrarConsultas = Nothing
   Case PRWinBuscarRespuesta
     Load frmBuscarRespuesta
     'Call objsecurity.AddHelpContext(528)
     Call frmBuscarRespuesta.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmBuscarRespuesta
     Set frmBuscarRespuesta = Nothing
  Case PRWinConsultasNoRespondidas
     Load frmConsultasNoRespondidas
     'Call objsecurity.AddHelpContext(528)
     Call frmConsultasNoRespondidas.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmConsultasNoRespondidas
     Set frmConsultasNoRespondidas = Nothing
  Case PRWinConsultaCompleta
     Load frmConsultaCompleta
     'Call objsecurity.AddHelpContext(528)
     Call frmConsultaCompleta.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmConsultaCompleta
     Set frmConsultaCompleta = Nothing
  Case PRWinEnviarRespCorreo
     Load frmEnviarRespCorreo
     'Call objsecurity.AddHelpContext(528)
     Call frmEnviarRespCorreo.Show(vbModal)
     Call objsecurity.RemoveHelpContext
     Unload frmEnviarRespCorreo
     Set frmEnviarRespCorreo = Nothing

'    'Case Else
'    '  ' el c�digo de proceso no es v�lido y se devuelve falso
'    '  LaunchProcess = False
  End Select
  Call ERR.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 35, 1 To 4) As Variant
      
  ' VENTANAS
  'MANTENIMIENTO
  aProcess(1, 1) = PRWinBuscarEnsayClinic
  aProcess(1, 2) = "Buscar Protocolos E.C"
  aProcess(1, 3) = False
  aProcess(1, 4) = cwTypeWindow

  aProcess(2, 1) = PRWinDefEnsayClinic
  aProcess(2, 2) = "Definir Ensayo Cl�nico"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow

  aProcess(3, 1) = PRWinRedactarOMEC
  aProcess(3, 2) = "Redactar Orden M�dica de Ensayo Cl�nico"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow

  aProcess(4, 1) = PRWinSelPacOMEC
  aProcess(4, 2) = "Seleccionar Pacientes para O.M. de E.C."
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow

  aProcess(5, 1) = PRWinConsOMECAnte
  aProcess(5, 2) = "Consultar O.M. de E.C. Anteriores"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow

  aProcess(6, 1) = PRWinDefExpEnsayClinic
  aProcess(6, 2) = "Definir Expedientes de Ensayo Cl�nico"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow

  aProcess(7, 1) = PRWinDefPacEnsayClin
  aProcess(7, 2) = "Asignaci�n Pacientes a Ensayo Cl�nico"
  aProcess(7, 3) = True
  aProcess(7, 4) = cwTypeWindow

  aProcess(8, 1) = PRWinFirmarOMEC
  aProcess(8, 2) = "Firmar O.M. de Ensayo Cl�nico"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow

  aProcess(9, 1) = PRWinOMECSinEnv
  aProcess(9, 2) = "Enviar O.M. de Ensayo Cl�nico"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow

  aProcess(10, 1) = PRWinRevOMEC
  aProcess(10, 2) = "Revisar O.M. de Ensayo Cl�nico"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow

  aProcess(11, 1) = PRWinDispensarOMEC
  aProcess(11, 2) = "Dispensar O.M. de Ensayo Cl�nico"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow

  aProcess(12, 1) = PRWinDispProdOMEC
  aProcess(12, 2) = "Dispensar Medicamento de O.M.E.C."
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow

  aProcess(13, 1) = PRWinDevEnvOMEC
  aProcess(13, 2) = "Devoluci�n Envase de O.M.E.C."
  aProcess(13, 3) = True
  aProcess(13, 4) = cwTypeWindow
  
  aProcess(14, 1) = PRWinControlarAlerta
  aProcess(14, 2) = "Controlar Alertas de Inmovilizaci�n"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow
  
  aProcess(15, 1) = PRWinActualizarBIM
  aProcess(15, 2) = "Actualizar BIM"
  aProcess(15, 3) = True
  aProcess(15, 4) = cwTypeWindow
  
  aProcess(16, 1) = PRWinRegistrarConsultas
  aProcess(16, 2) = "Registrar Consultas al CIM"
  aProcess(16, 3) = True
  aProcess(16, 4) = cwTypeWindow
  
  aProcess(17, 1) = PRWinBuscarRespuesta
  aProcess(17, 2) = "Buscar Respuesta a una consulta hecha al CIM"
  aProcess(17, 3) = False
  aProcess(17, 4) = cwTypeWindow
  
  aProcess(18, 1) = PRWinConsultasNoRespondidas
  aProcess(18, 2) = "Consultas al CIM no respondidas"
  aProcess(18, 3) = True
  aProcess(18, 4) = cwTypeWindow
  
  aProcess(19, 1) = PRWinConsultaCompleta
  aProcess(19, 2) = "Consulta Completa No Respondida"
  aProcess(19, 3) = False
  aProcess(19, 4) = cwTypeWindow
  
  aProcess(20, 1) = PRWinEnviarRespCorreo
  aProcess(20, 2) = "Enviar Mensaje de Correo"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeWindow
  
  aProcess(21, 1) = PRWinBusGrpTera
  aProcess(21, 2) = "Buscar Grupo Terap�utico"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeWindow
  
  aProcess(22, 1) = PRWinBusProdProt
  aProcess(22, 2) = "Buscar Medicamento"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeWindow
  
  aProcess(23, 1) = PRWinBusGrpTeraEC
  aProcess(23, 2) = "Buscar Grp.Tera. O.M.E.C."
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeWindow
  
  aProcess(24, 1) = PRWinBusProtocolosEC
  aProcess(24, 2) = "Buscar Protocolo O.M.E.C."
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeWindow
  
  aProcess(25, 1) = PRWinBusGrpProdEC
  aProcess(25, 2) = "Buscar Grp.Medi. O.M.E.C."
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeWindow
  
  aProcess(26, 1) = PRWinBusProductosEC
  aProcess(26, 2) = "Buscar Productos O.M.E.C."
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeWindow
  
  aProcess(27, 1) = PRWinVerPerfilFTPEC
  aProcess(27, 2) = "Ver Perfil FTP O.M.E.C."
  aProcess(27, 3) = False
  aProcess(27, 4) = cwTypeWindow

  aProcess(28, 1) = PRWinBusPacEC
  aProcess(28, 2) = "Buscar Pacientes E.C."
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeWindow
  
  aProcess(29, 1) = PRWinVerIntMedEC
  aProcess(29, 2) = "Ver Interacciones EC"
  aProcess(29, 3) = False
  aProcess(29, 4) = cwTypeWindow
  
  aProcess(30, 1) = PRWinAlerPacEC
  aProcess(30, 2) = "Ver Alergias EC"
  aProcess(30, 3) = False
  aProcess(30, 4) = cwTypeWindow
  
End Sub
