VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmSelPacOMEC 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Seleccionar Paciente para Orden M�dica de Ensayo Cl�nico"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0227.frx":0000
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Ensayo Cl�nico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Index           =   1
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   11505
      Begin TabDlg.SSTab tabTab1 
         Height          =   1935
         Index           =   0
         Left            =   120
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   3413
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0227.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "SSTab1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0227.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1665
            Index           =   2
            Left            =   -74880
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   120
            Width           =   10575
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18653
            _ExtentY        =   2937
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab SSTab1 
            Height          =   1695
            Index           =   0
            Left            =   120
            TabIndex        =   8
            Top             =   120
            Width           =   10695
            _ExtentX        =   18865
            _ExtentY        =   2990
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Datos Generales"
            TabPicture(0)   =   "FR0227.frx":0044
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(1)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(2)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(3)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(4)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "txtText1(4)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(5)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtText1(6)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(7)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(8)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(9)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).ControlCount=   10
            TabCaption(1)   =   "Descripci�n"
            TabPicture(1)   =   "FR0227.frx":0060
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(11)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               DataField       =   "FR77DESENSAYOCLI"
               Height          =   1290
               Index           =   11
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   19
               Tag             =   "Descripci�n Ensayo Cl�nico"
               Top             =   360
               Width           =   9240
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   9
               Left            =   3960
               TabIndex        =   14
               Tag             =   "C�d. Servicio"
               Top             =   1200
               Width           =   372
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   8
               Left            =   4440
               TabIndex        =   13
               TabStop         =   0   'False
               Tag             =   "Descripci�n Servicio"
               Top             =   1200
               Width           =   4800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR77NOMENSAYOCLI"
               Height          =   330
               Index           =   7
               Left            =   1560
               TabIndex        =   12
               Tag             =   "Ensayo Cl�nico"
               Top             =   600
               Width           =   7560
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR77CODPROTEC"
               Height          =   330
               Index           =   6
               Left            =   120
               Locked          =   -1  'True
               TabIndex        =   11
               Tag             =   "C�digo Protocolo"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02CODMEDRESP"
               Height          =   330
               Index           =   5
               Left            =   120
               TabIndex        =   10
               Tag             =   "C�digo Doctor"
               Top             =   1200
               Width           =   1155
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   4
               Left            =   1320
               TabIndex        =   9
               Tag             =   "Apellido Doctor"
               Top             =   1200
               Width           =   2445
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   3960
               TabIndex        =   18
               Top             =   960
               Width           =   705
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Ensayo Cl�nico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   1560
               TabIndex        =   17
               Top             =   360
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cod. Protocolo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   16
               Top             =   360
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Promotor"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   15
               Top             =   960
               Width           =   765
            End
         End
      End
   End
   Begin VB.CommandButton cmdValidar 
      Caption         =   "O.M. de E.C. Anteriores"
      Height          =   375
      Left            =   4920
      TabIndex        =   4
      Top             =   7560
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pacientes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Index           =   2
      Left            =   120
      TabIndex        =   0
      Top             =   2880
      Width           =   11535
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4095
         Index           =   1
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   11250
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   19844
         _ExtentY        =   7223
         _StockProps     =   79
         Caption         =   "PACIENTES"
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmSelPacOMEC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0227.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA                                             *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Validar Pedido Farmacia                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


Private Sub cmdValidar_Click()
  
cmdValidar.Enabled = False
  If grdDBGrid1(1).Rows > 0 Then
    gintredactarorden = 1
    'se pasa el c�digo de paciente
    glngselpaciente = grdDBGrid1(1).Columns(3).Value
    glngselprotec = txtText1(6).Text
    Call objsecurity.LaunchProcess("FR0228")
  Else
    Call MsgBox("No hay ning�n paciente seleccionado", vbCritical, "Aviso")
  End If
cmdValidar.Enabled = True

End Sub

Private Sub Form_Activate()
    If gintredactarorden = 1 Then
        'gintredactarorden = 0
        'se llama a Redactar Orden M�dica
        Call objsecurity.LaunchProcess("FR0226")
        Unload Me
    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMultiInfo As New clsCWForm
Dim objMasterInfo As New clsCWForm
Dim strKey As String
  
    'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin

  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                  cwWithAll)
  
  With objMasterInfo
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "E.C."
      
    .strTable = "FR7700"
    
    '.blnAskPrimary = False
    
    Call .FormAddOrderField("FR77NOMENSAYOCLI", cwAscending)
    
    .intAllowance = cwAllowReadOnly
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Protocolos")
    Call .FormAddFilterWhere(strKey, "FR77CODPROTEC", "C�d.Protocolo E.C.", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR77NOMENSAYOCLI", "Ensayo Cl�nico", cwString)
    Call .FormAddFilterWhere(strKey, "SG02CODMEDRESP", "M�dico Responsable", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�d.Servicio", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR77FECINIVIG", "Inicio Vigencia", cwDate)
    'Call .FormAddFilterWhere(strKey, "FR77FECFINVIG", "Fin Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR77DESENSAYOCLI", "Descripci�n de protocolo E.C.", cwString)
    Call .FormAddFilterOrder(strKey, "FR77NOMENSAYOCLI", "Ensayo Cl�nico")

  End With

  With objMultiInfo
    .strName = "Pedidos a Firmar"
    Set .objFormContainer = fraframe1(2)
    Set .objFatherContainer = fraframe1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    '.strDataBase = objEnv.GetValue("Main")
    '.strTable = "CI2200"
    '.strTable = "FR6100"
    .strTable = "FR6101J"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddRelation("FR77CODPROTEC", txtText1(6))
    Call .FormAddOrderField("CI22NOMBRE", cwAscending)
  
  End With

  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo, "Cod.Pers.", "CI21CODPERSONA", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Nombre", "CI22NOMBRE", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Apellido 1�", "CI22PRIAPEL", cwString)
    Call .GridAddColumn(objMultiInfo, "Apellido 2�", "CI22SEGAPEL", cwString)
    Call .GridAddColumn(objMultiInfo, "DNI", "CI22DNI", cwString)
    Call .GridAddColumn(objMultiInfo, "Fecha de Nacimiento", "CI22FECNACIM", cwDate)
    Call .GridAddColumn(objMultiInfo, "Historia", "CI22NUMHISTORIA", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "N� Seguridad social", "CI22NUMSEGSOC", cwString)
    Call .GridAddColumn(objMultiInfo, "Provincia", "CI22DESPROVI", cwString)
    Call .GridAddColumn(objMultiInfo, "Localidad", "CI22DESLOCALID", cwString)
    Call .GridAddColumn(objMultiInfo, "Pa�s", "CI19CODPAIS", cwString)
    Call .GridAddColumn(objMultiInfo, "Cod.Sexo", "CI30CODSEXO", cwNumeric, 1)
    Call .GridAddColumn(objMultiInfo, "Sexo", "CI30DESSEXO", cwString)
    
    'Call .FormCreateInfo(objMultiInfo)
    Call .FormCreateInfo(objMasterInfo)
    
    Call .FormChangeColor(objMultiInfo)
    
    
    Call .WinRegister
    Call .WinStabilize
  End With
    
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
'    Dim rsta As rdoResultset
'    Dim stra As String
'    Dim rst2000 As rdoResultset
'    Dim str2000 As String
'    Dim strinsertar As String
'    Dim strCodigo As String
'    Dim rstCodigo As rdoResultset
    
    Call objWinInfo.GridDblClick
    
'    str2000 = "select * from fr2000 where fr55codnecesunid=" & grdDBGrid1(1).Columns(3).Value
'    Set rst2000 = objApp.rdoConnect.OpenResultset(str2000)
'    While rst2000.EOF = False
'        stra = "select * from fr1900 where fr55codnecesunid=" & grdDBGrid1(1).Columns(3).Value & _
'                " and fr20numlinea=" & rst2000("fr20numlinea").Value
'        Set rsta = objApp.rdoConnect.OpenResultset(stra)
'        If rsta.EOF = True Then
'            strCodigo = "SELECT FR19CODNECESVAL_SEQUENCE.nextval FROM dual"
'            Set rstCodigo = objApp.rdoConnect.OpenResultset(strCodigo)
'            strinsertar = "INSERT INTO FR1900 (FR55CODNECESUNID,FR20NUMLINEA,FR19CODNECESVAL," & _
'                       "FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR19CANTNECESQUIR,SG02COD_VAL," & _
'                       "FR19FECVALIDACION,FR93CODUNIMEDIDA_SUM,FR19CANTSUMIFARM)" & _
'                       " VALUES (" & _
'                       rst2000("fr55codnecesunid").Value & "," & _
'                       rst2000("fr20numlinea").Value & "," & _
'                       rstCodigo(0).Value & "," & _
'                       rst2000("fr73codproducto").Value & "," & _
'                       "'" & rst2000("fr93codunimedida").Value & "'" & "," & _
'                       rst2000("fr20cantnecesquir").Value & ",'" & _
'                       objsecurity.strUser & "'," & _
'                       "sysdate," & _
'                       "'" & rst2000("fr93codunimedida").Value & "'" & "," & _
'                       "0)"
'            objApp.rdoConnect.Execute strinsertar, 64
'            objApp.rdoConnect.Execute "Commit", 64
'            rstCodigo.Close
'            Set rstCodigo = Nothing
'        End If
'        rsta.Close
'        Set rsta = Nothing
'        rst2000.MoveNext
'    Wend
'    rst2000.Close
'    Set rst2000 = Nothing
'    Call objsecurity.LaunchProcess("FR0136")
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


