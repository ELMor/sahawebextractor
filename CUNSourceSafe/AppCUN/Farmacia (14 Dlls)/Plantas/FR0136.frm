VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form FrmVerPedPtaNoVal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. PEDIR SERVICIO FARMACIA. Ver Pedido Planta no Dispensado"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0136.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   10440
      Top             =   6360
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "Anular"
      Height          =   375
      Left            =   1800
      TabIndex        =   27
      Top             =   7560
      Width           =   1935
   End
   Begin VB.CommandButton CmdTotal 
      Caption         =   "Dispensado Total"
      Height          =   375
      Left            =   7080
      TabIndex        =   26
      Top             =   7560
      Width           =   1935
   End
   Begin VB.CommandButton cmdProducto 
      Caption         =   "Sustituir Producto"
      Height          =   375
      Left            =   10080
      TabIndex        =   25
      Top             =   4680
      Width           =   1815
   End
   Begin VB.CommandButton cmdBloquear 
      Caption         =   "Bloquear/Desbloquear"
      Height          =   375
      Left            =   10080
      TabIndex        =   24
      Top             =   5400
      Width           =   1815
   End
   Begin VB.CommandButton cmdValidar 
      Caption         =   "Dispensar"
      Height          =   375
      Left            =   4440
      TabIndex        =   23
      Top             =   7560
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Index           =   1
      Left            =   120
      TabIndex        =   12
      Top             =   480
      Width           =   11340
      Begin TabDlg.SSTab tabTab1 
         Height          =   2295
         Index           =   0
         Left            =   120
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   360
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0136.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(16)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(19)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(23)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(10)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(6)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(28)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "dtcDateCombo1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txttext1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txttext1(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txttext1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txttext1(6)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txttext1(3)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txttext1(5)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txttext1(4)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0136.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   4
            Left            =   2040
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Persona Peticionaria"
            Top             =   1800
            Width           =   3015
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   5
            Left            =   480
            TabIndex        =   4
            Tag             =   "C�digo Servicio"
            Top             =   1080
            Width           =   1100
         End
         Begin VB.TextBox txttext1 
            DataField       =   "SG02COD_PDS"
            Height          =   330
            Index           =   3
            Left            =   480
            TabIndex        =   6
            Tag             =   "Persona Peticionaria"
            Top             =   1800
            Width           =   1095
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   6
            Left            =   2040
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Descripci�n Servicio"
            Top             =   1080
            Width           =   5175
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR55CODNECESUNID"
            Height          =   330
            Index           =   0
            Left            =   480
            TabIndex        =   0
            Tag             =   "C�digo Necesidad"
            Top             =   360
            Width           =   1100
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   1
            Left            =   2880
            TabIndex        =   1
            Tag             =   "Estado Petici�n"
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   2
            Left            =   3360
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   1605
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   0
            Left            =   -74880
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1905
            Index           =   2
            Left            =   -74880
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   240
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   3360
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR55FECPETICION"
            Height          =   330
            Index           =   0
            Left            =   7320
            TabIndex        =   3
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cod.Peticionario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   480
            TabIndex        =   22
            Top             =   1560
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Persona Peticionaria"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   2040
            TabIndex        =   21
            Top             =   1560
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   480
            TabIndex        =   20
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   2040
            TabIndex        =   19
            Top             =   840
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   23
            Left            =   7320
            TabIndex        =   18
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Necesidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   19
            Left            =   480
            TabIndex        =   17
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   2880
            TabIndex        =   16
            Top             =   120
            Width           =   735
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Necesidades de la Unidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Index           =   2
      Left            =   120
      TabIndex        =   8
      Top             =   3360
      Width           =   9855
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3495
         Index           =   1
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   9570
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   1
         stylesets(0).Name=   "Bloqueada"
         stylesets(0).ForeColor=   16777215
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "FR0136.frx":0044
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   16880
         _ExtentY        =   6165
         _StockProps     =   79
         Caption         =   "NECESIDADES DE LA UNIDAD"
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   10
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmVerPedPtaNoVal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmVerPedPtaNoVal (FR0136.FRM)                               *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Ver Pedido Planta no Validado                           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Dim mblnGrabar As Boolean
Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  strWhere = "{AD0200.AD02CODDPTO}={FR5500.AD02CODDPTO} AND " & _
             "{FR5500.FR55CODNECESUNID}={FR1900.FR55CODNECESUNID} AND " & _
             "{FR1900.FR73CODPRODUCTO}={FR7300.FR73CODPRODUCTO} AND " & _
             "{FR1900.FR55CODNECESUNID}=" & txtText1(0).Text & " AND " & _
             "{FR1900.FR19INDBLOQ} = 0"
             
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp3
  CrystalReport1.Action = 1
Err_imp3:

End Sub
Private Sub RealizarApuntes(strBoton As String)
  'Al realizar la dispensaci�n se almacenan los movimientos, tanto de salida como de entrada
  'FR8000 es la tabla de salidas de almac�n: sacamos del almac�n de Farmacia y lo pasamos al almac�n del servicio
  'FR3500 es la tabla de entradas de almac�n: metemos en el almac�n del Servicio lo sacado de Farmacia
  'FRK100 esla tabla en la que se almacenan lo entregado a un servicio
  'strBoton contiene el nombre del bot�n desde el que se ha llamado al procedimiento
  
  Dim strCodDpto As String 'Para guardar el c�digo del Servicio
  Dim strNumNec As String 'Para guardar el n�mero de necesidad
  Dim strFR19 As String
  Dim qryFR19 As rdoQuery
  Dim rdoFR19 As rdoResultset
  Dim strIns As String
  Dim strFR73 As String
  Dim qryFR73 As rdoQuery
  Dim rdoFR73 As rdoResultset
  Dim strTamEnv As String
  Dim strCant As String
  Dim strUpd As String
  Dim qryUpd As rdoQuery
  Dim rdoUpd As rdoResultset
  Dim strCP As String
  Dim strDptoFar As String
  Dim strIns80 As String
  Dim strIns35 As String
  Dim strFRH2 As String
  Dim qryFRH2 As rdoQuery
  Dim rdoFRH2 As rdoResultset
  Dim strAlmFar As String
  Dim blnEntroEnWhile As Boolean
  Dim strSql As String
  Dim rdoSql As rdoResultset
  Dim strCodMov As String
  Dim strCod80 As String
  Dim strCod35 As String
  Dim strFR04 As String
  Dim qryFR04  As rdoQuery
  Dim rdoFR04 As rdoResultset
  Dim strAlmDes As String
  Dim strCodNec As String
  
  Screen.MousePointer = vbHourglass
  blnEntroEnWhile = False
  strCodDpto = txtText1(5).Text
  strNumNec = txtText1(0).Text
    
  strFR73 = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?"
  Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
  
  strFR04 = "SELECT * FROM FR0400 WHERE AD02CODDPTO = ?"
  Set qryFR04 = objApp.rdoConnect.CreateQuery("", strFR04)
  qryFR04(0) = strCodDpto
  Set rdoFR04 = qryFR04.OpenResultset()
  If Not IsNull(rdoFR04.rdoColumns("FR04CODALMACEN").Value) Then
    strAlmDes = rdoFR04.rdoColumns("FR04CODALMACEN").Value
  Else
   strAlmDes = 999 'Almac�n ficticio
  End If
  qryFR04.Close
  Set qryFR04 = Nothing
  Set rdoFR04 = Nothing
  
  strUpd = "UPDATE FR1900 " & _
           "   SET FR19INDFAC = ? , FR19CANTNECESQUIR = ?, FR19CANTSUMIFARM = ?" & _
           " WHERE FR55CODNECESUNID = ? " & _
           "   AND FR19CODNECESVAL = ? "
           
  Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
           
  'Se obtiene el almac�n principal de Farmacia
  strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
  Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
  qryFRH2(0) = 28
  Set rdoFRH2 = qryFRH2.OpenResultset()
  If IsNull(rdoFRH2.rdoColumns(0).Value) Then
    strAlmFar = "0"
  Else
    strAlmFar = rdoFRH2.rdoColumns(0).Value
  End If
  qryFRH2.Close
  Set qryFRH2 = Nothing
  Set rdoFRH2 = Nothing
  
  strFR19 = "SELECT * " & _
            "  FROM FR1900 " & _
            " WHERE FR55CODNECESUNID = ? " & _
            "   AND FR19INDFAC = ? " & _
            "   AND FR19CANTSUMIFARM > ? " & _
            "   AND FR19INDBLOQ = ? "
            
  Set qryFR19 = objApp.rdoConnect.CreateQuery("", strFR19)
  qryFR19(0) = strNumNec
  qryFR19(1) = 0
  qryFR19(2) = 0
  qryFR19(3) = 0
  
  'Se tienen las l�neas dispensadas en ese momento
  Set rdoFR19 = qryFR19.OpenResultset()
  While Not rdoFR19.EOF
    blnEntroEnWhile = True
    qryFR73(0) = rdoFR19.rdoColumns("FR73CODPRODUCTO").Value
    Set rdoFR73 = qryFR73.OpenResultset()
    If IsNull(rdoFR73.rdoColumns("FR73TAMENVASE").Value) Then
      strTamEnv = 1
    Else
      strTamEnv = rdoFR73.rdoColumns("FR73TAMENVASE").Value
    End If
    strCant = CCur(strTamEnv) * CCur(rdoFR19.rdoColumns("FR19CANTSUMIFARM").Value)
    strCant = objGen.ReplaceStr(strCant, ",", ".", 1)
    
    strCodNec = rdoFR19.rdoColumns("FR55CODNECESUNID").Value
    
    strSql = "SELECT FRK1CODMOV_SEQUENCE.NEXTVAL FROM DUAL"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSql)
    strCodMov = rdoSql.rdoColumns(0).Value
    rdoSql.Close
    Set rdoSql = Nothing
    
    'Se realiza el insert en FRK100
    strIns = "INSERT INTO FRK100 (FRK1CODMOV,FRK1FECMOV,AD02CODDPTO,FR73CODPRODUCTO, " & _
                                 "FRK1CANTIDAD,FRK1TAMENVASE,FRK1INDMOD,FRK1INDFAC,FRK1CODNECESUNID) VALUES (" & _
                                 strCodMov & ",SYSDATE," & strCodDpto & "," & rdoFR19.rdoColumns("FR73CODPRODUCTO").Value & "," & _
                                  strCant & "," & strTamEnv & "," & rdoFR19.rdoColumns("FR19INDMOD").Value & ",0," & strCodNec & ")"
    objApp.rdoConnect.Execute strIns, 64
    
    'Se modifica FR1900
    If (rdoFR19.rdoColumns("FR19CANTSUMIFARM").Value = rdoFR19.rdoColumns("FR19CANTNECESQUIR").Value) Or (UCase(strBoton) = "DT") Then
      'La l�nea est� totalmente dipensada, y se marca como tal (-1)
      qryUpd(0) = -1
      qryUpd(1) = rdoFR19.rdoColumns("FR19CANTNECESQUIR").Value
      qryUpd(2) = strCant
      qryUpd(3) = rdoFR19.rdoColumns("FR55CODNECESUNID").Value
      qryUpd(4) = rdoFR19.rdoColumns("FR19CODNECESVAL").Value
      qryUpd.Execute
    Else
      'La l�nea est� parcialmente dispensada, se pone como cantidad pedida la pendiente de entregar
      strCP = CCur(rdoFR19.rdoColumns("FR19CANTNECESQUIR").Value) - CCur(rdoFR19.rdoColumns("FR19CANTSUMIFARM").Value)
      qryUpd(0) = 0
      qryUpd(1) = objGen.ReplaceStr(strCP, ",", ".", 1)
      qryUpd(2) = 0
      qryUpd(3) = rdoFR19.rdoColumns("FR55CODNECESUNID").Value
      qryUpd(4) = rdoFR19.rdoColumns("FR19CODNECESVAL").Value
      qryUpd.Execute
    End If
    
    strSql = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM DUAL"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSql)
    strCod80 = rdoSql.rdoColumns(0).Value
    rdoSql.Close
    Set rdoSql = Nothing
    
    'Se realiza rl insert en FR8000: salidas de alamc�n
    strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,FR90CODTIPMOV," & _
                "FR80FECMOVIMIENTO,FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA,FR80UNISALEN) VALUES (" & _
                strCod80 & "," & strAlmFar & "," & strAlmDes & ",3,SYSDATE," & _
                rdoFR19.rdoColumns("FR73CODPRODUCTO").Value & "," & strCant & ",1,'NE'," & strCant & ")"
    objApp.rdoConnect.Execute strIns80, 64
    
    strSql = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM DUAL"
    Set rdoSql = objApp.rdoConnect.OpenResultset(strSql)
    strCod35 = rdoSql.rdoColumns(0).Value
    rdoSql.Close
    Set rdoSql = Nothing
    'Se realiza el insert en FR3500: entradas de almac�n
    strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
               "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD," & _
               "FR93CODUNIMEDIDA,FR35UNIENT) VALUES (" & strCod35 & "," & _
               strAlmFar & "," & strAlmDes & ",3,SYSDATE," & rdoFR19.rdoColumns("FR73CODPRODUCTO").Value & "," & strCant & ",1,'NE'," & strCant & ")"
    objApp.rdoConnect.Execute strIns35, 64
    rdoFR19.MoveNext
  Wend
  
  If blnEntroEnWhile Then
    qryFR73.Close
    Set qryFR73 = Nothing
    Set rdoFR73 = Nothing
  End If
  
  qryFR19.Close
  Set qryFR19 = Nothing
  Set rdoFR19 = Nothing
  Screen.MousePointer = vbDefault
End Sub
Private Sub cmdAnular_Click()
    Dim qryupdate As rdoQuery
    Dim intMsg As Integer
    Dim strupdate As String

    If gstrEst = "5" Then 'Dispensada Total
      Exit Sub
    End If
    
    If gstrEst = "8" Then 'Anulada
      cmdAnular.Enabled = False
      If txtText1(0).Text <> "" Then
        intMsg = MsgBox("�Est� seguro que desea Quitar la Anulaci�n de la Petici�n?", vbQuestion + vbYesNo)
        If intMsg = vbYes Then
            strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=? WHERE FR55CODNECESUNID=?"
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = 3
            qryupdate(1) = txtText1(0).Text
            gstrEst = "3"
            cmdAnular.Caption = "Anular"
            qryupdate.Execute
            Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
            objWinInfo.DataRefresh
            Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
            objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
            objWinInfo.DataRefresh
        End If
      End If
      cmdAnular.Enabled = True
      Exit Sub
    End If
    
    cmdAnular.Enabled = False
    If txtText1(0).Text <> "" Then
        intMsg = MsgBox("�Est� seguro que desea Anular la Petici�n?", vbQuestion + vbYesNo)
        If intMsg = vbYes Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
            'strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=8 WHERE FR55CODNECESUNID=" & txtText1(0).Text
            strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=? WHERE FR55CODNECESUNID=?"
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = 8
            qryupdate(1) = txtText1(0).Text
            qryupdate.Execute
            cmdAnular.Caption = "Quitar Anulaci�n"
            gstrEst = "8"
            'objApp.rdoConnect.Execute strupdate, 64
            Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
            objWinInfo.DataRefresh
            Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
            objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        End If
    End If
    cmdAnular.Enabled = True
End Sub


Private Sub CmdTotal_Click()
    Dim qryupdate As rdoQuery
    Dim intMsg As Integer
    Dim strupdate As String
    
   
  If (txtText1(1).Text = 3) Or (txtText1(1).Text = 9) Then
    If gstrEst = "5" Then
      Exit Sub
    End If
    CmdTotal.Enabled = False
    If txtText1(0).Text <> "" Then
        intMsg = MsgBox("�Est� seguro que desea dispensar totalmente la Petici�n?", vbQuestion + vbYesNo)
        If intMsg = vbYes Then
            strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=? WHERE FR55CODNECESUNID=?"
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = 5
            qryupdate(1) = txtText1(0).Text
            qryupdate.Execute
            objWinInfo.DataRefresh
            Call RealizarApuntes("DT")
            Call Imprimir("FR1362.RPT", 1)
        End If
    End If
  End If
  CmdTotal.Enabled = True
End Sub


Private Sub Form_Activate()
  If gstrEst = "8" Then 'Anulada
    cmdAnular.Caption = "Quitar Anulaci�n"
  Else
    cmdAnular.Caption = "Anular"
  End If
  Screen.MousePointer = vbDefault
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  mblnGrabar = True
  If grdDBGrid1(1).Columns(13).Value <> "" Then
    If IsNumeric(grdDBGrid1(1).Columns(14).Value) = True Then
      If CDec(grdDBGrid1(1).Columns(14).Value) < 0 Or CDec(grdDBGrid1(1).Columns(14).Value) > CDec(grdDBGrid1(1).Columns(13).Value) Then
        Call MsgBox("Hay datos incorrectos en la cantidad suministrada", vbExclamation, "Aviso")
        mblnGrabar = False
        blnCancel = True
        'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(10), 0)
      End If
    Else
      Call MsgBox("Hay datos incorrectos en la cantidad suministrada", vbExclamation, "Aviso")
      mblnGrabar = False
      blnCancel = True
    End If
  Else
    If IsNumeric(grdDBGrid1(1).Columns(14).Value) = False Then
        Call MsgBox("Hay datos incorrectos en la cantidad suministrada", vbExclamation, "Aviso")
        mblnGrabar = False
        blnCancel = True
    Else
      If grdDBGrid1(1).Columns(14).Value < 0 Then
        Call MsgBox("Hay datos incorrectos en la cantidad suministrada", vbExclamation, "Aviso")
        mblnGrabar = False
        blnCancel = True
      End If
    End If
  End If
  
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  'Call MsgBox("1")
  'If IsNumeric(grdDBGrid1(1).Columns(10).Value) = True Then
  '  If grdDBGrid1(1).Columns(10).Value < 0 Then
  '    Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(10), 0)
  '  End If
  'End If
End Sub


Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub



Private Sub cmdbloquear_Click()
    Dim qryupdate As rdoQuery
    Dim strupdate As String
    Dim qrya As rdoQuery
    Dim rsta As rdoResultset
    Dim sqlstr As String
    
  If (txtText1(1).Text = 3) Or (txtText1(1).Text = 9) Then
    If gstrEst = "5" Then
      Exit Sub
    End If
    cmdBloquear.Enabled = False
    If grdDBGrid1(1).Columns(3).Value <> "" And grdDBGrid1(1).Columns(4).Value <> "" Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
        'sqlstr = "SELECT count(*) from fr2000 where fr55codnecesunid=" & grdDBGrid1(1).Columns(3).Value & _
        '         " and fr20numlinea=" & grdDBGrid1(1).Columns(4).Value & _
        '         " and fr20indbloqueo=-1"
        sqlstr = "SELECT count(*) from fr2000 where fr55codnecesunid=?" & _
                 " and fr20numlinea=?" & _
                 " and fr20indbloqueo=?"
        Set qrya = objApp.rdoConnect.CreateQuery("", sqlstr)
        qrya(0) = grdDBGrid1(1).Columns(3).Value
        qrya(1) = grdDBGrid1(1).Columns(4).Value
        qrya(2) = -1
        Set rsta = qrya.OpenResultset(sqlstr)
        'Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        If rsta(0).Value = 0 Then
            'strupdate = "UPDATE FR2000 SET FR20INDBLOQUEO=-1 WHERE FR55CODNECESUNID=" & grdDBGrid1(1).Columns(3).Value & _
            '            " AND FR20NUMLINEA=" & grdDBGrid1(1).Columns(4).Value
            strupdate = "UPDATE FR2000 SET FR20INDBLOQUEO=? WHERE FR55CODNECESUNID=?" & _
                        " AND FR20NUMLINEA=?"
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = -1
            qryupdate(1) = grdDBGrid1(1).Columns(3).Value
            qryupdate(2) = grdDBGrid1(1).Columns(4).Value
            qryupdate.Execute
            
            strupdate = "UPDATE FR1900 SET FR19INDBLOQ=? WHERE FR55CODNECESUNID=? " & _
                        " AND FR19CODNECESVAL=? "
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = -1
            qryupdate(1) = grdDBGrid1(1).Columns(3).Value
            qryupdate(2) = grdDBGrid1(1).Columns("Cod Neces Val").Value
            qryupdate.Execute
            
            
            grdDBGrid1(1).Columns(22).Value = True
            'objApp.rdoConnect.Execute strupdate, 64
        Else
            'strupdate = "UPDATE FR2000 SET FR20INDBLOQUEO=0 WHERE FR55CODNECESUNID=" & grdDBGrid1(1).Columns(3).Value & _
            '            " AND FR20NUMLINEA=" & grdDBGrid1(1).Columns(4).Value
            strupdate = "UPDATE FR2000 SET FR20INDBLOQUEO=? WHERE FR55CODNECESUNID=?" & _
                        " AND FR20NUMLINEA=?"
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = 0
            qryupdate(1) = grdDBGrid1(1).Columns(3).Value
            qryupdate(2) = grdDBGrid1(1).Columns(4).Value
            qryupdate.Execute
            
            strupdate = "UPDATE FR1900 SET FR19INDBLOQ=? WHERE FR55CODNECESUNID=?" & _
                        " AND FR19CODNECESVAL=?"
            Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
            qryupdate(0) = 0
            qryupdate(1) = grdDBGrid1(1).Columns(3).Value
            qryupdate(2) = grdDBGrid1(1).Columns("Cod Neces Val").Value
            qryupdate.Execute
            
            grdDBGrid1(1).Columns(22).Value = False
            'objApp.rdoConnect.Execute strupdate, 64
        End If
    End If
    objWinInfo.DataRefresh
    rsta.Close
    Set rsta = Nothing
  End If
  cmdBloquear.Enabled = True
End Sub

Private Sub cmdProducto_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim fila As Integer
Dim sqlstr As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim qryupdate As rdoQuery
Dim strupdate As String
Dim cantidad As Variant
Dim codigo As Variant
Dim strlinea As String
Dim qrylinea As rdoQuery
Dim rstlinea As rdoResultset
Dim linea As Integer
Dim strInsert As String
Dim strMod As String

On Error GoTo ERR

  
  If gstrEst = "5" Then
    Exit Sub
  End If
If (txtText1(1).Text = 3) Or (txtText1(1).Text = 9) Then
  If grdDBGrid1(1).SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar una linea."
    Exit Sub
  End If
  If grdDBGrid1(1).Columns(3).Value <> "" And grdDBGrid1(1).Columns(4).Value <> "" Then
    If IsNull(grdDBGrid1(1).Columns("Mod").Visible) Then
        strMod = "0"
      Else
        If Len(Trim(grdDBGrid1(1).Columns("Mod").Value)) > 0 Then
          If grdDBGrid1(1).Columns("Mod").Visible = True Then
            strMod = "-1"
          Else
            strMod = "0"
          End If
        End If
      End If
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'sqlstr = "SELECT count(*) from fr2000 where fr55codnecesunid=" & grdDBGrid1(1).Columns(3).Value & _
      '         " and fr20numlinea=" & grdDBGrid1(1).Columns(4).Value & _
      '         " and fr20indbloqueo=-1"
      sqlstr = "SELECT count(*) from fr2000 where fr55codnecesunid=?" & _
               " and fr20numlinea=?" & _
               " and fr20indbloqueo=?"
      Set qrya = objApp.rdoConnect.CreateQuery("", sqlstr)
      qrya(0) = grdDBGrid1(1).Columns(3).Value
      qrya(1) = grdDBGrid1(1).Columns(4).Value
      qrya(2) = -1
      Set rsta = qrya.OpenResultset(sqlstr)
      'Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If rsta(0).Value = 0 Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
          'strupdate = "UPDATE FR2000 SET FR20INDBLOQUEO=-1 WHERE FR55CODNECESUNID=" & grdDBGrid1(1).Columns(3).Value & _
          '            " AND FR20NUMLINEA=" & grdDBGrid1(1).Columns(4).Value
          strupdate = "UPDATE FR2000 SET FR20INDBLOQUEO=? WHERE FR55CODNECESUNID=?" & _
                      " AND FR20NUMLINEA=?"
          Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
          qryupdate(0) = -1
          qryupdate(1) = grdDBGrid1(1).Columns(3).Value
          qryupdate(2) = grdDBGrid1(1).Columns(4).Value
          qryupdate.Execute
          grdDBGrid1(1).Columns(22).Value = True
          'objApp.rdoConnect.Execute strupdate, 64
          cantidad = grdDBGrid1(1).Columns(13).Value
          codigo = grdDBGrid1(1).Columns(21).Value
      Else
        MsgBox "La l�nea est� bloqueada"
        Exit Sub
      End If
  End If
  fila = grdDBGrid1(1).Row
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    Call objWinInfo.DataSave
  End If
  
  cmdProducto.Enabled = False
  
'  If grdDBGrid1(0).SelBookmarks.Count = 0 Then
'    MsgBox "Debe seleccionar una linea."
'  Else
    
    noinsertar = True
   'grdDBGrid1(1).SelBookmarks.RemoveAll
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
    Call objsecurity.LaunchProcess("FR0119")
    If gintprodtotal > 0 Then
      If gintprodtotal > 1 Then
        MsgBox "Debe traer s�lo 1 producto."
      Else
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'strlinea = "SELECT MAX(FR20NUMLINEA) FROM FR1900 WHERE FR55CODNECESUNID=" & _
      '           txtText1(0).Text
      strlinea = "SELECT MAX(FR20NUMLINEA) FROM FR1900 WHERE FR55CODNECESUNID=?"
      Set qrylinea = objApp.rdoConnect.CreateQuery("", strlinea)
      qrylinea(0) = txtText1(0).Text
      Set rstlinea = qrylinea.OpenResultset(strlinea)
      'Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
      'marca = grdDBGrid1(1).Bookmark
      If IsNull(rstlinea.rdoColumns(0).Value) = False Then
          linea = 1 + rstlinea(0).Value
        Else
          linea = 1
      End If
          
      'grdDBGrid1(1).Columns(4).Value = linea
      'grdDBGrid1(1).Columns(15).Value = Date
      'grdDBGrid1(1).Columns(16).Value = objsecurity.strUser
      strInsert = "INSERT INTO FR1900 "
      strInsert = strInsert & "(FR19INDMOD,FR20NUMLINEA,FR55CODNECESUNID"
      strInsert = strInsert & ",FR19CODNECESVAL,FR73CODPRODUCTO"
      strInsert = strInsert & ",FR93CODUNIMEDIDA,FR19CANTNECESQUIR"
      strInsert = strInsert & ",SG02COD_VAL,FR19FECVALIDACION"
      strInsert = strInsert & ",FR93CODUNIMEDIDA_SUM,FR19CANTSUMIFARM)"
      strInsert = strInsert & " VALUES (" & strMod & "," & linea
      strInsert = strInsert & "," & txtText1(0).Text
      strInsert = strInsert & "," & codigo
      strInsert = strInsert & "," & gintprodbuscado(v, 0)
      strInsert = strInsert & "," & "'" & gintprodbuscado(v, 4) & "'"
      strInsert = strInsert & "," & cantidad
      strInsert = strInsert & ",'" & objsecurity.strUser & "'"
      strInsert = strInsert & ",TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')"
      strInsert = strInsert & "," & "'" & gintprodbuscado(v, 4) & "'"
      strInsert = strInsert & "," & cantidad
      strInsert = strInsert & ")"
      objApp.rdoConnect.Execute strInsert, 64
      objWinInfo.DataRefresh
            'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(5), gintprodbuscado(v, 0))
            'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(6), gintprodbuscado(v, 1))
            'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(8), gintprodbuscado(v, 2))
            'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(12), cantidad)
            'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(13), cantidad)
            'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(11), gintprodbuscado(v, 4))
            'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(18), gintprodbuscado(v, 4))
            'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(20), codigo)
          'objWinInfo.objWinActiveForm.blnChanged = True
          'Else
          '  mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          '  Chr(13), vbInformation)
          'End If
          'noinsertar = True
        'Next v
      End If
    End If
    gintprodtotal = 0
End If
cmdProducto.Enabled = True
Exit Sub
  
ERR:
MsgBox ERR.Description
  cmdProducto.Enabled = True
  Exit Sub
End Sub

Private Sub cmdvalidar_Click()
    Dim intMsg As Integer
    Dim qryupdate As rdoQuery
    Dim strupdate As String
    Dim Total As Boolean
    Dim i As Integer
    
  If (txtText1(1).Text = 3) Or (txtText1(1).Text = 9) Then
    If gstrEst = "5" Then
      Exit Sub
    End If
  
    cmdValidar.Enabled = False
    If txtText1(0).Text <> "" Then
        intMsg = MsgBox("�Est� seguro que desea dispensar la Petici�n?", vbQuestion + vbYesNo)
        If intMsg = vbYes Then
            mblnGrabar = True
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
            If mblnGrabar = False Then
              cmdValidar.Enabled = True
              Exit Sub
            End If
            Total = True
            If grdDBGrid1(1).Rows > 0 Then
              grdDBGrid1(1).MoveFirst
              For i = 0 To grdDBGrid1(1).Rows - 1
                If grdDBGrid1(1).Columns(13).Value <> grdDBGrid1(1).Columns(14).Value _
                And grdDBGrid1(1).Columns(22).Value <> True Then
                  'no se inserta el producto pues ya est� insertado en el grid
                  Total = False
                  Exit For
                End If
                grdDBGrid1(1).MoveNext
              Next i
            End If
            If Total Then
              strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=? WHERE FR55CODNECESUNID=?"
              Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
              qryupdate(0) = 5
              qryupdate(1) = txtText1(0).Text
              qryupdate.Execute
              Call RealizarApuntes("DT")
            Else
              strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=? WHERE FR55CODNECESUNID=?"
              Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
              qryupdate(0) = 9
              qryupdate(1) = txtText1(0).Text
              qryupdate.Execute
              Call RealizarApuntes("DP")
            End If
            Call Imprimir("FR1362.RPT", 1)
            Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
            objWinInfo.DataRefresh
        End If
    End If
  End If
  cmdValidar.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String

  
    Set objWinInfo = New clsCWWin
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
      With objMasterInfo
        Set .objFormContainer = fraFrame1(1)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(0)
        Set .grdGrid = grdDBGrid1(2)
        
        .strName = "Necesidad Unidad"
          
        .strTable = "FR5500"
        .strWhere = "FR55CODNECESUNID=" & frmValPedPtaFarma.grdDBGrid1(1).Columns(3).Value
        .intAllowance = cwAllowReadOnly
        Call .FormAddOrderField("FR55CODNECESUNID", cwAscending)
       
        strKey = .strDataBase & .strTable
        'Call .FormCreateFilterWhere(strKey, "Necesidad Unidad")
        'Call .FormAddFilterWhere(strKey, "FR55CODNECESUNID", "C�d. Necesidad Unidad", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�d. Peticionario", cwString)
        'Call .FormAddFilterWhere(strKey, "FR55FECPETICION", "Fecha Petici�n", cwDate)
        'Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
        'Call .FormAddFilterOrder(strKey, "FR55CODNECESUNID", "C�digo Necesidad Unidad")
    
      End With
    With objMultiInfo
        .strName = "Detalle Necesidad"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = fraFrame1(1)
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR1900"
        
        Select Case gstrEst
        Case "3", "4" 'Enviada y Validada
          .intAllowance = cwAllowAll
        Case "5" 'Dispensada Total
          .intAllowance = cwAllowReadOnly
        Case "8" 'Anulada
          .intAllowance = cwAllowAdd
        Case "9" 'Dispensada Parcial
          .intAllowance = cwAllowAll
          .strWhere = "FR19CANTNECESQUIR > FR19CANTSUMIFARM "
        Case Else
          .intAllowance = cwAllowAll
        End Select
        
      
        Call .objPrinter.Add("FR1361", "Preparaci�n")
        Call .objPrinter.Add("FR1362", "Entrega")
                        
        Call .FormAddOrderField("FR19CODNECESVAL", cwAscending)
        Call .FormAddOrderField("FR20NUMLINEA", cwAscending)
        
        strKey = .strDataBase & .strTable
        
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Necesidades")
        Call .FormAddFilterWhere(strKey, "FR55CODNECESVAL", "C�d Necesidad Validado", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR20NUMLINEA", "N�mero de L�nea", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d. Producto", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "", "Producto", cwString)
        Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida", cwString)
        'Call .FormAddFilterWhere(strKey, "", "Unidad Medida", cwString)
        Call .FormAddFilterWhere(strKey, "FR19CANTNECESQUIR", "Cantidad", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR19FECVALIDACION", "Fecha Validaci�n", cwDate)
        Call .FormAddFilterWhere(strKey, "SG02COD_VAL", "Persona Valida", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA_SUM", "C�d Unidad Medida Sum", cwString)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR55CODNECESUNID", "C�d Necesidad Unidad")
        Call .FormAddFilterOrder(strKey, "FR20NUMLINEA", "N�mero de L�nea")
        Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�d. Producto")
        'Call .FormAddFilterOrder(strKey, "", "Producto")
        Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida")
        'Call .FormAddFilterOrder(strKey, "", "Unidad Medida")
        Call .FormAddFilterOrder(strKey, "FR19CANTNECESQUIR", "Cantidad")
        Call .FormAddFilterOrder(strKey, "FR19FECVALIDACION", "Fecha Validaci�n")
        Call .FormAddFilterOrder(strKey, "SG02COD_VAL", "Persona Valida")
        Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA_SUM", "C�d Unidad Medida Sum")
        
        Call .FormAddRelation("FR55CODNECESUNID", txtText1(0))
    End With

    With objWinInfo
        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
        Call .GridAddColumn(objMultiInfo, "C�d. Necesidad", "FR55CODNECESUNID", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "L�nea", "FR20NUMLINEA", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "C�d. Prod", "FR73CODPRODUCTO", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Producto", "", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
        Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
        Call .GridAddColumn(objMultiInfo, "Tam Env", "", cwDecimal, 10)
        Call .GridAddColumn(objMultiInfo, "FF", "", cwString, 5)
        Call .GridAddColumn(objMultiInfo, "D�sis", "", cwDecimal, 9)
        Call .GridAddColumn(objMultiInfo, "U.M.", "FR93CODUNIMEDIDA", cwString, 5)
        Call .GridAddColumn(objMultiInfo, "Cantidad", "FR19CANTNECESQUIR", cwNumeric, 11)
        Call .GridAddColumn(objMultiInfo, "Cant Disp", "FR19CANTSUMIFARM", cwNumeric, 11)
        Call .GridAddColumn(objMultiInfo, "Unid.Medida", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Fecha Validaci�n", "FR19FECVALIDACION", cwDate)
        Call .GridAddColumn(objMultiInfo, "C�d.Val", "SG02COD_VAL", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Valida", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d.Sum", "FR93CODUNIMEDIDA", cwString, 5)
        Call .GridAddColumn(objMultiInfo, "Unid.Medida Sum", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Cod Neces Val", "FR19CODNECESVAL", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Bloq", "", cwBoolean)
        Call .GridAddColumn(objMultiInfo, "Ubicaci�n", "", cwString, 10)
        Call .GridAddColumn(objMultiInfo, "Mod", "FR19INDMOD", cwBoolean, 1)
        
        Call .FormCreateInfo(objMasterInfo)
    
        ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        '.CtrlGetInfo(grdDBGrid1(1).Columns(4)).intKeyNo = 1
        'Se indica que campos son obligatorios y cuales son clave primaria
        .CtrlGetInfo(txtText1(0)).intKeyNo = 1

        Call .FormChangeColor(objMultiInfo)
    
        '.CtrlGetInfo(txttext1(0)).blnInFind = True
        '.CtrlGetInfo(txttext1(1)).blnInFind = True
        '.CtrlGetInfo(txttext1(2)).blnInFind = True
        '.CtrlGetInfo(txttext1(3)).blnInFind = True
        '.CtrlGetInfo(txttext1(4)).blnInFind = True
        '.CtrlGetInfo(txttext1(5)).blnInFind = True
        '.CtrlGetInfo(txttext1(6)).blnInFind = True
        '.CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
        
       .CtrlGetInfo(grdDBGrid1(1).Columns(13)).blnReadOnly = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "SG02COD_PDS", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(4), "SG02APE1")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(6), "AD02DESDPTO")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(2), "FR26DESESTADOPET")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "FR73CODINTFAR")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(8), "FR73DESPRODUCTO")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(7), "FR73REFERENCIA")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(11), "FR73DOSIS")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(10), "FRH7CODFORMFAR")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(9), "FR73TAMENVASE")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(23), "FRH9UBICACION")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(12)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(12)), grdDBGrid1(1).Columns(15), "FR93DESUNIMEDIDA")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(17)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(17)), grdDBGrid1(1).Columns(18), "SG02APE1")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(19)), "FR93CODUNIMEDIDA_SUM", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(19)), grdDBGrid1(1).Columns(20), "FR93DESUNIMEDIDA")
        
        
    '.CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnForeign = True
      .CtrlGetInfo(grdDBGrid1(1).Columns(12)).blnReadOnly = True
        '.CtrlGetInfo(txttext1(3)).blnForeign = True
        '.CtrlGetInfo(txttext1(5)).blnForeign = True
        
        
        Call .WinRegister
        Call .WinStabilize
    End With
    grdDBGrid1(1).Columns(4).Width = 800
    grdDBGrid1(1).Columns(5).Width = 1000
    grdDBGrid1(1).Columns(6).Width = 850
    grdDBGrid1(1).Columns(7).Width = 1000
    grdDBGrid1(1).Columns(8).Width = 2800
    grdDBGrid1(1).Columns(9).Width = 800
    grdDBGrid1(1).Columns(13).Width = 900
    grdDBGrid1(1).Columns(14).Width = 900
    grdDBGrid1(1).Columns(11).Width = 600
    grdDBGrid1(1).Columns(12).Width = 600
    grdDBGrid1(1).Columns(15).Width = 2500
    grdDBGrid1(1).Columns(20).Width = 500
    grdDBGrid1(1).Columns(13).Width = 1000
    grdDBGrid1(1).Columns(23).Width = 1000
    grdDBGrid1(1).Columns(0).Visible = False
    grdDBGrid1(1).Columns(3).Visible = False
    grdDBGrid1(1).Columns(4).Visible = False
    grdDBGrid1(1).Columns(5).Visible = False
    grdDBGrid1(1).Columns(15).Visible = False
    grdDBGrid1(1).Columns(16).Visible = False
    grdDBGrid1(1).Columns(19).Visible = False
    grdDBGrid1(1).Columns(20).Visible = False
    grdDBGrid1(1).Columns(21).Visible = False
    grdDBGrid1(1).Columns(22).Visible = False
    grdDBGrid1(1).Columns("Mod").Visible = False
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    Dim i As Integer
    Dim sqlstr As String
    Dim qrya As rdoQuery
    Dim rsta As rdoResultset
    
    If Index = 1 Then
        If grdDBGrid1(1).Columns(3).Value <> "" And grdDBGrid1(1).Columns(4).Value <> "" Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
            'sqlstr = "SELECT count(*) from fr2000 where fr55codnecesunid=" & grdDBGrid1(1).Columns(3).Value & _
            '         " and fr20numlinea=" & grdDBGrid1(1).Columns(4).Value & _
            '         " and fr20indbloqueo=-1"
            sqlstr = "SELECT count(*) from fr2000 where fr55codnecesunid=?" & _
                     " and fr20numlinea=?" & _
                     " and fr20indbloqueo=?"
            Set qrya = objApp.rdoConnect.CreateQuery("", sqlstr)
            qrya(0) = grdDBGrid1(1).Columns(3).Value
            qrya(1) = grdDBGrid1(1).Columns(4).Value
            qrya(2) = -1
            Set rsta = qrya.OpenResultset(sqlstr)
            'Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
            If rsta(0).Value > 0 Then
                For i = 3 To 23
                    grdDBGrid1(1).Columns(i).CellStyleSet "Bloqueada"
                Next i
                grdDBGrid1(1).Columns(22).Value = True
            End If
          End If
    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

Private Function FillStr(ByVal strBuffer As String)
  FillStr = strBuffer & String(128 - Len(strBuffer), vbNullChar)
End Function



Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strWhere As String
  Dim strFilter As String

  If strFormName = "Detalle Necesidad" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      If intReport = 1 Then
        Call Imprimir("FR1361.RPT", 0)
      Else
        Call Imprimir("FR1362.RPT", 0)
      End If
              'strWhere = "WHERE AD0200." & Chr(34) & "AD02CODDPTO" & Chr(34) & "= FR5500." & Chr(34) & "AD02CODDPTO" & Chr(34) & " AND " & _
      '           "FR5500." & Chr(34) & "FR55CODNECESUNID" & Chr(34) & " = FR1900." & Chr(34) & "FR55CODNECESUNID" & Chr(34) & " AND " & _
      '           "FR1900." & Chr(34) & "FR73CODPRODUCTO" & Chr(34) & " = FR7300." & Chr(34) & "FR73CODPRODUCTO" & Chr(34) & _
      '           " AND FR1900." & Chr(34) & "FR55CODNECESUNID" & Chr(34) & "=" & txtText1(0).Text
      'strWhere = "WHERE FR1900." & Chr(34) & "FR55CODNECESUNID" & Chr(34) & "=" & txttext1(0).Text
      'strFilter = ""
      'Call objPrinter.ShowReport(strWhere, strFilter)
      
      
      'CrystalReport1.Connect = "DSN = DESASIC;UID = cun;PWD = tuy;DSQ = Administration"
      'CrystalReport1.ReportFileName = "C:\Archivos de programa\CUN\Rpt\FR1361.rpt"
      'strWhere = "{AD0200.AD02CODDPTO} = {FR5500.AD02CODDPTO} AND " & _
      '         "{FR5500.FR55CODNECESUNID} = {FR1900.FR55CODNECESUNID} AND " & _
      '         "{FR1900.FR73CODPRODUCTO} = {FR7300.FR73CODPRODUCTO}" & _
      '         "AND {FR1900.FR55CODNECESUNID} = " & txttext1(0).Text
      '
      'CrystalReport1.SelectionFormula = strWhere
      'CrystalReport1.Action = 1
    End If
    Set objPrinter = Nothing
  End If
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  'Dim strlinea As String
  'Dim rstlinea As rdoResultset
  'Dim linea As Integer
  'linea = objWinInfo.objWinActiveForm.rdoCursor!FR20NUMLINEA
  'GRDDBGRID1(1).Columns(
  'mblnGrabar = True
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  'If btnButton.Index = 2 Then
  '
  '    strlinea = "SELECT MAX(FR20NUMLINEA) FROM FR1900 WHERE FR55CODNECESUNID=" & _
  '               txttext1(0).Text
  '    Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
  '    'marca = grdDBGrid1(1).Bookmark
  '    If IsNull(rstlinea.rdoColumns(0).Value) = False Then
  '        linea = 1 + rstlinea(0).Value
  '      Else
  '        linea = 1
  '    End If
  '    grdDBGrid1(1).Columns(4).Value = linea
  '    grdDBGrid1(1).Columns(15).Value = Date
  '    grdDBGrid1(1).Columns(16).Value = objsecurity.strUser
   ' End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    'Dim intMsg As Integer
   '
   ' If intIndex = 4 Then
   '     intMsg = MsgBox("�Est� seguro de la devoluci�n de este envase?", vbQuestion + vbYesNo)
   '     If intMsg = vbNo Then
   '         Exit Sub
   '     End If
   '     If grdDBGrid1(1).Columns(3).Value = False Then
   '         If grdDBGrid1(1).Columns(4).Value = "" Then
   '             Call MsgBox("Debe indicar la Cantidad Devuelta", vbInformation)
   '             Exit Sub
   '         End If
   '     Else
   '        Call MsgBox("Debe indicar que se ha devuelto el envase", vbInformation)
   '        Exit Sub
   '     End If
   ' End If
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Dim vntDatos(1 To 2) As Variant
    
    Call objWinInfo.GridDblClick
    'If grdDBGrid1(1).Rows > 0 Then
    '    vntDatos(1) = "FrmRedPedStPlanta"
    '    vntDatos(2) = grdDBGrid1(1).Columns(5).Value
    '    Call objsecurity.LaunchProcess("FR0003", vntDatos)
    'End If
End Sub
Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
Dim qrya As rdoQuery
Dim stra As String
Dim rsta As rdoResultset
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    If intIndex = 1 And grdDBGrid1(1).Columns(5).Value <> "" And grdDBGrid1(1).Columns(12).Value = "" Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(1).Columns(5).Value
      stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = ?"
      Set qrya = objApp.rdoConnect.CreateQuery("", stra)
      qrya(0) = grdDBGrid1(1).Columns(5).Value
      Set rsta = qrya.OpenResultset(stra)
      'Set rsta = objApp.rdoConnect.OpenResultset(stra)
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(12), rsta(0).Value)
      rsta.Close
      Set rsta = Nothing
    End If
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
    'If intIndex = 1 Then
    '    'se controla que en la columna cantidad no se metan n�meros negativos
    '    If IsNumeric(grdDBGrid1(1).Columns(10).Value) Then
    '      If grdDBGrid1(1).Columns(10).Value < 0 Then
    '          Call MsgBox("El campo Cantidad no admite valores negativos", vbInformation, "Aviso")
    '          Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(10), 0)
    '          grdDBGrid1(1).Update
    '      End If
    '    Else
    '          'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(10), 0)
    '          'grdDBGrid1(1).Update
    '    End If
    'End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


