VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmFirmarPedStPta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. FIRMAR PEDIDO FARMACIA. Firmar Pedido Planta."
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0132.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdredactarpedido 
      Caption         =   "Redactar Pedido"
      Height          =   375
      Left            =   1800
      TabIndex        =   12
      Top             =   7560
      Width           =   1935
   End
   Begin VB.CommandButton cmdFirmar 
      Caption         =   "Firmar"
      Height          =   375
      Left            =   6600
      TabIndex        =   11
      Top             =   7560
      Width           =   1935
   End
   Begin VB.CommandButton cmdFirmarEnviar 
      Caption         =   "Firmar y Enviar"
      Height          =   375
      Left            =   8640
      TabIndex        =   10
      Top             =   7560
      Width           =   1935
   End
   Begin VB.Frame Frame2 
      Caption         =   "Servicio "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1095
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   11535
      Begin VB.TextBox txtServicio 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "Desc.Servicio"
         Top             =   600
         Width           =   5445
      End
      Begin VB.CheckBox chkservicio 
         Caption         =   "Todos los servicios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   8880
         TabIndex        =   6
         Top             =   600
         Width           =   2175
      End
      Begin SSDataWidgets_B.SSDBCombo cboservicio 
         Height          =   315
         Left            =   360
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   600
         Width           =   2295
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2223
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4048
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   2760
         TabIndex        =   9
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d. Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   28
         Left            =   360
         TabIndex        =   7
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Peticiones sin Firmar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5535
      Index           =   2
      Left            =   120
      TabIndex        =   0
      Top             =   1800
      Width           =   11535
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   5055
         Index           =   1
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   11250
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   19844
         _ExtentY        =   8916
         _StockProps     =   79
         Caption         =   "PETICIONES SIN FIRMAR"
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmFirmarPedStPta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0132.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA                                             *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: firmar Pedido Farmacia                                  *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1



Private Sub cmdredactarpedido_Click()
If grdDBGrid1(1).Rows > 0 Then
    'se pasa el n� de orden para ir a frmRedPedStPlanta y firmar
    gintfirmarStPlanta = 1
    glngnumpeticion = grdDBGrid1(1).Columns(3).Value
    gblnNoDesdeMenu = True
    Call objsecurity.LaunchProcess("FR0131")
    gblnNoDesdeMenu = False
    gintfirmarStPlanta = 0
    objWinInfo.DataRefresh
End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


Private Sub cboservicio_CloseUp()
If IsNumeric(cboservicio.Text) Then
    If chkservicio = 0 Then
        objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC=1 AND AD02CODDPTO=" & cboservicio.Text & _
                " AND (FR55INDESTUPEFACIENTE=0 OR FR55INDESTUPEFACIENTE IS NULL)" & _
                " AND (FR55INDFM=0 OR FR55INDFM IS NULL)"
        txtServicio.Text = cboservicio.Columns(1).Value
    Else 'todos los servicios
        chkservicio.Value = 0
    End If
    objWinInfo.DataRefresh
End If
End Sub
Private Sub cboservicio_Click()
If IsNumeric(cboservicio.Text) Then
    If chkservicio = 0 Then
        objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC=1 AND AD02CODDPTO=" & cboservicio.Text & _
                  " AND (FR55INDESTUPEFACIENTE=0 OR FR55INDESTUPEFACIENTE IS NULL)" & _
                  " AND (FR55INDFM=0 OR FR55INDFM IS NULL)"
        txtServicio.Text = cboservicio.Columns(1).Value
    Else 'todos los servicios
        chkservicio.Value = 0
    End If
    objWinInfo.DataRefresh
End If
End Sub

Private Sub cmdfirmar_Click()
    Dim intMsg As Integer
    Dim strupdate As String
    
    cmdFirmar.Enabled = False
    If grdDBGrid1(1).Rows > 0 Then
        intMsg = MsgBox("�Est� seguro que desea firmar la Petici�n?", vbQuestion + vbYesNo)
        If intMsg = vbYes Then
            strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=2 WHERE FR55CODNECESUNID=" & grdDBGrid1(1).Columns(3).Value
            objApp.rdoConnect.Execute strupdate, 64
            Call MsgBox("La petici�n ha sido Firmada", vbInformation, "Aviso")
            objWinInfo.DataRefresh
        End If
    End If
    cmdFirmar.Enabled = True
End Sub

Private Sub cmdfirmarenviar_Click()
    Dim intMsg As Integer
    Dim strupdate As String
    
    cmdFirmarEnviar.Enabled = False
    If grdDBGrid1(1).Rows > 0 Then
        intMsg = MsgBox("�Est� seguro que desea firmar y enviar la Petici�n?", vbQuestion + vbYesNo)
        If intMsg = vbYes Then
            strupdate = "UPDATE FR5500 SET FR55FECENVIO=SYSDATE,FR26CODESTPETIC=3 WHERE FR55CODNECESUNID=" & grdDBGrid1(1).Columns(3).Value
            objApp.rdoConnect.Execute strupdate, 64
            Call MsgBox("La petici�n ha sido Firmada y Enviada", vbInformation, "Aviso")
            objWinInfo.DataRefresh
        End If
    End If
    cmdFirmarEnviar.Enabled = True
End Sub

Private Sub chkservicio_Click()
    If chkservicio = 1 Then
        cboservicio.Text = ""
        txtServicio.Text = ""
        objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC=1" & _
                    " AND (FR55INDESTUPEFACIENTE=0 OR FR55INDESTUPEFACIENTE IS NULL)" & _
                    " AND (FR55INDFM=0 OR FR55INDFM IS NULL)"
    Else
        If cboservicio <> "" Then
            txtServicio.Text = cboservicio.Columns(1).Value
            objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC=1 AND AD02CODDPTO=" & cboservicio.Text & _
                      " AND (FR55INDESTUPEFACIENTE=0 OR FR55INDESTUPEFACIENTE IS NULL)" & _
                      " AND (FR55INDFM=0 OR FR55INDFM IS NULL)"
        End If
    End If
    objWinInfo.DataRefresh
End Sub

Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset

    cboservicio.RemoveAll
    stra = "select AD02CODDPTO,AD02DESDPTO from AD0200" & _
            " WHERE " & _
            " AD02FECINICIO < (SELECT SYSDATE FROM DUAL)" & _
            " AND ((AD02FECFIN IS NULL) OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL))) ORDER BY AD02DESDPTO"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    While (Not rsta.EOF)
        Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
        rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
    
    objWinInfo.DataRefresh
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Pedidos a Firmar"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "FR5500"
        .intAllowance = cwAllowReadOnly
        
        .strWhere = "FR26CODESTPETIC=1 AND FR55CODNECESUNID IS NULL" & _
                " AND (FR55INDESTUPEFACIENTE=0 OR FR55INDESTUPEFACIENTE IS NULL)" & _
                " AND (FR55INDFM=0 OR FR55INDFM IS NULL)"
        
        Call .FormAddOrderField("FR55CODNECESUNID", cwAscending)
    
        strKey = .strDataBase & .strTable
        Call .FormCreateFilterWhere(strKey, "Pedidos Farmacia")
        Call .FormAddFilterWhere(strKey, "FR55CODNECESUNID", "C�digo Petici�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�d. Dispensa", cwString)
        Call .FormAddFilterWhere(strKey, "FR55FECPETICION", "Fecha Petici�n", cwDate)
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�digo Petici�n", "FR55CODNECESUNID", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Estado", "FR26CODESTPETIC", cwNumeric, 1)
        Call .GridAddColumn(objMultiInfo, "Desc.Estado", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Fecha", "FR55FECPETICION", cwDate)
        Call .GridAddColumn(objMultiInfo, "C�d.Dispensa", "SG02COD_PDS", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Dispensario", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d.Servicio", "AD02CODDPTO", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 30)
        
        
        Call .FormCreateInfo(objMultiInfo)
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(5), "FR26DESESTADOPET")
                
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(7)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(7)), grdDBGrid1(1).Columns(8), "SG02APE1")
                
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), grdDBGrid1(1).Columns(10), "AD02DESDPTO")
   
        Call .WinRegister
        Call .WinStabilize
    End With
    
    grdDBGrid1(1).Columns(4).Visible = False 'c�d.estado
    grdDBGrid1(1).Columns(5).Visible = False 'desc.estado
    
    grdDBGrid1(1).Columns(3).Width = 1400 'c�d petici�n
    grdDBGrid1(1).Columns(6).Width = 1200 'fecha
    grdDBGrid1(1).Columns(7).Width = 1200 'doctor
    grdDBGrid1(1).Columns(8).Width = 2900 'doctor
    grdDBGrid1(1).Columns(9).Width = 1200 'servicio
    grdDBGrid1(1).Columns(10).Width = 2900 'servicio
    
    
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


