VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Begin VB.Form frmEstPetFarmOM 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Consultar OM"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   76
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Prescripci�n M�dica"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Index           =   1
      Left            =   0
      TabIndex        =   87
      Top             =   480
      Width           =   10185
      Begin TabDlg.SSTab tabTab1 
         Height          =   2655
         Index           =   0
         Left            =   120
         TabIndex        =   88
         TabStop         =   0   'False
         Top             =   360
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   4683
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0779.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(13)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(14)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(28)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "tab1"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(26)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(25)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "chkCheck1(15)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "chkCheck1(7)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "chkCheck1(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "chkCheck1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "chkCheck1(12)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "chkOMPRN(6)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0779.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkOMPRN 
            Caption         =   "PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   7200
            TabIndex        =   9
            Tag             =   "PRN?"
            Top             =   600
            Width           =   735
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR66CODOMORIPRN"
            Height          =   330
            Index           =   1
            Left            =   1320
            TabIndex        =   1
            Tag             =   "C�digo OM origen PRN"
            Top             =   360
            Width           =   1100
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Orden M�dica"
            DataField       =   "FR66INDOM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   7200
            TabIndex        =   8
            Tag             =   "Orden M�dica?"
            Top             =   360
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Diab�tico"
            DataField       =   "FR66INDPACIDIABET"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   5160
            TabIndex        =   4
            Tag             =   "Diab�tico?"
            Top             =   120
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Al�rgico"
            DataField       =   "FR66INDALERGIA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   5160
            TabIndex        =   5
            Tag             =   "Alergico?"
            Top             =   360
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Restricci�n volumen"
            DataField       =   "FR66INDRESTVOLUM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   5160
            TabIndex        =   6
            Tag             =   "Restricci�n de Volumen?"
            Top             =   600
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Inter�s Cient�fico"
            DataField       =   "FR66INDINTERCIENT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   7200
            TabIndex        =   7
            Tag             =   "Inter�s Cient�fico?"
            Top             =   120
            Width           =   1815
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   25
            Left            =   3120
            TabIndex        =   2
            Tag             =   "Estado Petici�n"
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   26
            Left            =   3480
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Petici�n"
            Top             =   360
            Width           =   1100
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   89
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2385
            Index           =   2
            Left            =   -74880
            TabIndex        =   90
            TabStop         =   0   'False
            Top             =   120
            Width           =   9495
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16748
            _ExtentY        =   4207
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab tab1 
            Height          =   1815
            Left            =   120
            TabIndex        =   91
            TabStop         =   0   'False
            Top             =   720
            Width           =   9495
            _ExtentX        =   16748
            _ExtentY        =   3201
            _Version        =   327681
            Style           =   1
            Tabs            =   4
            TabsPerRow      =   4
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Paciente"
            TabPicture(0)   =   "FR0779.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(2)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(42)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(41)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(40)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(39)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(38)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(11)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(7)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(8)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(9)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(7)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(6)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "Text1"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(29)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(33)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(32)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(46)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(23)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(3)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(2)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(4)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(5)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(59)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(60)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).ControlCount=   24
            TabCaption(1)   =   "M�dico"
            TabPicture(1)   =   "FR0779.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lblLabel1(0)"
            Tab(1).Control(1)=   "lblLabel1(10)"
            Tab(1).Control(2)=   "lblLabel1(6)"
            Tab(1).Control(3)=   "lblLabel1(20)"
            Tab(1).Control(4)=   "lblLabel1(5)"
            Tab(1).Control(5)=   "lblLabel1(18)"
            Tab(1).Control(6)=   "lblLabel1(15)"
            Tab(1).Control(7)=   "dtcDateCombo1(2)"
            Tab(1).Control(8)=   "dtcDateCombo1(1)"
            Tab(1).Control(9)=   "txtText1(10)"
            Tab(1).Control(10)=   "txtText1(11)"
            Tab(1).Control(10).Enabled=   0   'False
            Tab(1).Control(11)=   "txtText1(22)"
            Tab(1).Control(12)=   "txtText1(8)"
            Tab(1).Control(13)=   "txtText1(9)"
            Tab(1).Control(14)=   "txtText1(20)"
            Tab(1).Control(15)=   "txtText1(12)"
            Tab(1).ControlCount=   16
            TabCaption(2)   =   "Servicio"
            TabPicture(2)   =   "FR0779.frx":0070
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLabel1(4)"
            Tab(2).Control(0).Enabled=   0   'False
            Tab(2).Control(1)=   "lblLabel1(3)"
            Tab(2).Control(1).Enabled=   0   'False
            Tab(2).Control(2)=   "lblLabel1(12)"
            Tab(2).Control(2).Enabled=   0   'False
            Tab(2).Control(3)=   "lblLabel1(1)"
            Tab(2).Control(3).Enabled=   0   'False
            Tab(2).Control(4)=   "lblLabel1(62)"
            Tab(2).Control(4).Enabled=   0   'False
            Tab(2).Control(5)=   "txtText1(21)"
            Tab(2).Control(5).Enabled=   0   'False
            Tab(2).Control(6)=   "txtText1(19)"
            Tab(2).Control(6).Enabled=   0   'False
            Tab(2).Control(7)=   "txtText1(15)"
            Tab(2).Control(7).Enabled=   0   'False
            Tab(2).Control(8)=   "txtText1(14)"
            Tab(2).Control(8).Enabled=   0   'False
            Tab(2).Control(9)=   "txtText1(24)"
            Tab(2).Control(9).Enabled=   0   'False
            Tab(2).Control(10)=   "txtText1(16)"
            Tab(2).Control(10).Enabled=   0   'False
            Tab(2).Control(11)=   "txtText1(17)"
            Tab(2).Control(11).Enabled=   0   'False
            Tab(2).Control(12)=   "txtText1(61)"
            Tab(2).Control(12).Enabled=   0   'False
            Tab(2).Control(13)=   "txtText1(62)"
            Tab(2).Control(13).Enabled=   0   'False
            Tab(2).ControlCount=   14
            TabCaption(3)   =   "Observaciones"
            TabPicture(3)   =   "FR0779.frx":008C
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "lblLabel1(24)"
            Tab(3).Control(1)=   "txtText1(18)"
            Tab(3).ControlCount=   2
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   62
               Left            =   -69360
               TabIndex        =   280
               Tag             =   "Dpto.Cargo"
               Top             =   600
               Width           =   3405
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO_CRG"
               Height          =   330
               Index           =   61
               Left            =   -69840
               TabIndex        =   279
               Tag             =   "C�d.Dpto.Cargo"
               Top             =   600
               Width           =   360
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD01CODASISTENCI"
               Height          =   330
               Index           =   60
               Left            =   9120
               TabIndex        =   275
               Tag             =   "Asistencia"
               Top             =   1440
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD07CODPROCESO"
               Height          =   330
               Index           =   59
               Left            =   9120
               TabIndex        =   274
               Tag             =   "Proceso"
               Top             =   1080
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66OBSERV"
               Height          =   1170
               Index           =   18
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   39
               Tag             =   "Observaciones"
               Top             =   600
               Width           =   9165
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   6120
               TabIndex        =   20
               TabStop         =   0   'False
               Tag             =   "Apellido 2� Paciente"
               Top             =   1320
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   4
               Left            =   3120
               TabIndex        =   19
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Paciente"
               Top             =   1320
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   2
               Left            =   9120
               TabIndex        =   22
               Tag             =   "C�digo Paciente"
               Top             =   720
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   3
               Left            =   120
               TabIndex        =   18
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   1320
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   23
               Left            =   120
               TabIndex        =   11
               TabStop         =   0   'False
               Tag             =   "Historia Paciente"
               Top             =   600
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   46
               Left            =   3840
               TabIndex        =   14
               TabStop         =   0   'False
               Tag             =   "Cama"
               Top             =   600
               Width           =   900
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR66PESOPAC"
               Height          =   330
               Index           =   32
               Left            =   5160
               MaxLength       =   3
               TabIndex        =   15
               Tag             =   "Peso"
               Top             =   600
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR66ALTUPAC"
               Height          =   330
               Index           =   33
               Left            =   6240
               MaxLength       =   3
               TabIndex        =   16
               Tag             =   "Altura Cm"
               Top             =   600
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   29
               Left            =   7320
               TabIndex        =   17
               Tag             =   "Superficie Corporal"
               Top             =   600
               Width           =   600
            End
            Begin VB.TextBox Text1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Left            =   1800
               TabIndex        =   12
               TabStop         =   0   'False
               Tag             =   "Edad"
               Top             =   600
               Width           =   420
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "AD02CODDPTO_MED"
               Height          =   330
               Index           =   12
               Left            =   -70920
               TabIndex        =   25
               Tag             =   "Departamento"
               Top             =   600
               Width           =   1275
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMMEDI"
               Height          =   330
               Index           =   20
               Left            =   -67320
               TabIndex        =   27
               Tag             =   "Hora Firma M�dico"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   9
               Left            =   -72720
               TabIndex        =   24
               Tag             =   "Apellido Doctor"
               Top             =   600
               Width           =   1725
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_MED"
               Height          =   330
               Index           =   8
               Left            =   -74760
               TabIndex        =   23
               Tag             =   "C�digo Doctor"
               Top             =   600
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   -73680
               TabIndex        =   33
               Tag             =   "Desc.Departamento"
               Top             =   600
               Width           =   3480
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   16
               Left            =   -74760
               TabIndex        =   32
               Tag             =   "C�d.Departamento"
               Top             =   600
               Width           =   1035
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR91CODURGENCIA"
               Height          =   330
               Index           =   22
               Left            =   -70920
               TabIndex        =   30
               Tag             =   "C�digo Urgencia"
               Top             =   1320
               Width           =   735
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   11
               Left            =   -70080
               TabIndex        =   31
               TabStop         =   0   'False
               Tag             =   "Desc. Urgencia"
               Top             =   1320
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR66HORAREDACCI"
               Height          =   330
               Index           =   10
               Left            =   -72840
               TabIndex        =   29
               Tag             =   "Hora Redacci�n"
               Top             =   1320
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   6
               Left            =   2520
               TabIndex        =   13
               TabStop         =   0   'False
               Tag             =   "Sexo"
               Top             =   600
               Width           =   1140
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   7
               Left            =   9120
               TabIndex        =   21
               TabStop         =   0   'False
               Tag             =   "Sexo"
               Top             =   360
               Visible         =   0   'False
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_FEN"
               Height          =   330
               Index           =   24
               Left            =   -74760
               TabIndex        =   34
               Tag             =   "M�dico que firma"
               Top             =   1320
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_ENF"
               Height          =   330
               Index           =   14
               Left            =   -69720
               TabIndex        =   37
               Tag             =   "M�dico que env�a"
               Top             =   1320
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   15
               Left            =   -68880
               TabIndex        =   38
               Tag             =   "Apellido Doctor"
               Top             =   1320
               Width           =   1725
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   19
               Left            =   -74040
               TabIndex        =   35
               Tag             =   "Apellido Doctor"
               Top             =   1320
               Width           =   1725
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   21
               Left            =   -72120
               TabIndex        =   36
               Tag             =   "N� Colegiado"
               Top             =   1320
               Width           =   720
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   4425
               Index           =   3
               Left            =   -74880
               TabIndex        =   92
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1305
               Index           =   4
               Left            =   -74880
               TabIndex        =   93
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMMEDI"
               Height          =   330
               Index           =   1
               Left            =   -69360
               TabIndex        =   26
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   600
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECREDACCION"
               Height          =   330
               Index           =   2
               Left            =   -74760
               TabIndex        =   28
               Tag             =   "Fecha Redacci�n"
               Top             =   1320
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento de Cargo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   62
               Left            =   -69840
               TabIndex        =   281
               Top             =   360
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   -74880
               TabIndex        =   115
               Top             =   360
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 2�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   6120
               TabIndex        =   114
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 1�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   3120
               TabIndex        =   113
               Top             =   1080
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   120
               TabIndex        =   112
               Top             =   1080
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Historia "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   120
               TabIndex        =   111
               Top             =   360
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cama"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   38
               Left            =   3840
               TabIndex        =   110
               Top             =   360
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Peso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   39
               Left            =   5160
               TabIndex        =   109
               Top             =   360
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Altura Cm"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   40
               Left            =   6240
               TabIndex        =   108
               Top             =   360
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Superficie Corporal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   41
               Left            =   7320
               TabIndex        =   107
               Top             =   360
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Edad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   42
               Left            =   1800
               TabIndex        =   106
               Top             =   360
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   -70920
               TabIndex        =   105
               Top             =   360
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   -67320
               TabIndex        =   104
               Top             =   360
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   -69360
               TabIndex        =   103
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   -74760
               TabIndex        =   102
               Top             =   360
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -74760
               TabIndex        =   101
               Top             =   360
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   -70920
               TabIndex        =   100
               Top             =   1080
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   -72840
               TabIndex        =   99
               Top             =   1080
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   -74760
               TabIndex        =   98
               Top             =   1080
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Sexo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   2520
               TabIndex        =   97
               Top             =   360
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   -74760
               TabIndex        =   96
               Top             =   1080
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que env�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   -69720
               TabIndex        =   95
               Top             =   1080
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N� Colegiado"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   -72120
               TabIndex        =   94
               Top             =   1080
               Width           =   1335
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   120
            TabIndex        =   118
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.OM origen PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   1320
            TabIndex        =   117
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   3120
            TabIndex        =   116
            Top             =   120
            Width           =   735
         End
      End
   End
   Begin VB.Frame fraFrame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4560
      Index           =   0
      Left            =   0
      TabIndex        =   75
      Top             =   3480
      Width           =   10095
      Begin TabDlg.SSTab tabTab1 
         Height          =   4215
         Index           =   1
         Left            =   120
         TabIndex        =   74
         TabStop         =   0   'False
         Top             =   240
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   7435
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0779.frx":00A8
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "Frame1"
         Tab(0).Control(1)=   "tab2"
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0779.frx":00C4
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "lblLabel1(57)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "lblLabel1(56)"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "lblLabel1(55)"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "lblLabel1(54)"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).Control(4)=   "lblLabel1(53)"
         Tab(1).Control(4).Enabled=   0   'False
         Tab(1).Control(5)=   "lblLabel1(44)"
         Tab(1).Control(5).Enabled=   0   'False
         Tab(1).Control(6)=   "grdDBGrid1(6)"
         Tab(1).Control(6).Enabled=   0   'False
         Tab(1).ControlCount=   7
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'None
            Caption         =   "FR93CODUNIMEDIDA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3600
            Left            =   -74760
            TabIndex        =   235
            Top             =   480
            Width           =   9135
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Bloq"
               DataField       =   "FR28INDBLOQUEADA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   8040
               TabIndex        =   278
               TabStop         =   0   'False
               Tag             =   "Bloq"
               Top             =   2520
               Visible         =   0   'False
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR28INSTRADMIN"
               Height          =   930
               Index           =   39
               Left            =   0
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   63
               Tag             =   "Inst.Admin.|Instrucciones para administraci�n"
               Top             =   2040
               Width           =   5205
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28CANTDISP"
               Height          =   330
               Index           =   58
               Left            =   3240
               TabIndex        =   273
               Tag             =   "Cant.Dispens."
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.CommandButton cmdExplorador 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   525
               Index           =   1
               Left            =   4080
               TabIndex        =   71
               Top             =   2400
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.CommandButton cmdWord 
               Caption         =   "W"
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   525
               Left            =   4680
               TabIndex        =   72
               Top             =   2400
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR28PATHFORMAG"
               Height          =   330
               Index           =   57
               Left            =   360
               TabIndex        =   70
               Tag             =   "Arch.Inf.F.M.|Archivo Inf. F�rmula Magistral"
               Top             =   2520
               Visible         =   0   'False
               Width           =   4725
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28REFERENCIA"
               Height          =   330
               Index           =   56
               Left            =   2760
               TabIndex        =   271
               Tag             =   "Referencia"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR28DESPRODUCTO"
               Height          =   330
               Index           =   55
               Left            =   120
               TabIndex        =   42
               Tag             =   "Medic.No Form.|""Medic. No Formulario"""
               Top             =   120
               Visible         =   0   'False
               Width           =   5295
            End
            Begin VB.TextBox txtText1 
               Height          =   330
               Index           =   13
               Left            =   0
               TabIndex        =   41
               Tag             =   "Medicamento"
               Top             =   240
               Width           =   5295
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28UBICACION"
               Height          =   330
               Index           =   54
               Left            =   2520
               TabIndex        =   270
               Tag             =   "Ubicaci�n"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28OPERACION"
               Height          =   330
               Index           =   53
               Left            =   3000
               TabIndex        =   269
               Tag             =   "Operaci�n"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODPRODUCTO_2"
               Height          =   330
               Index           =   52
               Left            =   0
               TabIndex        =   73
               Tag             =   "C�digo Medicamento 2"
               Top             =   2640
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   51
               Left            =   0
               TabIndex        =   58
               Tag             =   "Medicamento 2"
               Top             =   3240
               Width           =   5340
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28DOSIS_2"
               Height          =   330
               Index           =   50
               Left            =   6120
               TabIndex        =   60
               Tag             =   "Dosis2"
               Top             =   3240
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR93CODUNIMEDIDA_2"
               Height          =   330
               Index           =   49
               Left            =   7080
               TabIndex        =   61
               Tag             =   "UM2"
               Top             =   3240
               Width           =   660
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FRH7CODFORMFAR_2"
               Height          =   330
               Index           =   48
               Left            =   5400
               TabIndex        =   59
               Tag             =   "FF2"
               Top             =   3240
               Width           =   615
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28VOLUMEN_2"
               Height          =   330
               Index           =   47
               Left            =   7800
               TabIndex        =   62
               Tag             =   "Vol.Prod.2|Volumen(ml) Producto 2"
               Top             =   3240
               Width           =   732
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28VELPERFUSION"
               Height          =   330
               Index           =   82
               Left            =   5400
               TabIndex        =   64
               Tag             =   "Perfusi�n|Velocidad de Perfusi�n"
               Top             =   2040
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28VOLTOTAL"
               Height          =   330
               Index           =   44
               Left            =   8040
               TabIndex        =   67
               Tag             =   "Vol.Total|Volumen Total (ml)"
               Top             =   2040
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28VOLUMEN"
               Height          =   330
               Index           =   38
               Left            =   7800
               TabIndex        =   46
               Tag             =   "Volum.|Volumen(ml)"
               Top             =   240
               Width           =   732
            End
            Begin VB.TextBox txtMinutos 
               Alignment       =   1  'Right Justify
               Height          =   330
               Left            =   7080
               TabIndex        =   57
               Tag             =   "Tiempo Infusi�n(min)"
               Top             =   1440
               Width           =   375
            End
            Begin VB.TextBox txtHoras 
               Alignment       =   1  'Right Justify
               Height          =   330
               Left            =   6600
               TabIndex        =   56
               Tag             =   "Tiempo Infusi�n(hor)"
               Top             =   1440
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FRH7CODFORMFAR"
               Height          =   330
               Index           =   35
               Left            =   5400
               TabIndex        =   43
               Tag             =   "FF"
               Top             =   240
               Width           =   615
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "fr28tiemmininf"
               Height          =   330
               Index           =   43
               Left            =   3720
               TabIndex        =   240
               Tag             =   "T.Inf.(min)|Tiempo Infusi�n(min)"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28CANTIDADDIL"
               Height          =   330
               Index           =   42
               Left            =   5520
               TabIndex        =   55
               Tag             =   "Vol.Dil.|Volumen(ml)"
               Top             =   1440
               Width           =   732
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   41
               Left            =   0
               TabIndex        =   54
               Tag             =   "Descripci�n Producto Diluir"
               Top             =   1440
               Width           =   5385
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODPRODUCTO_DIL"
               Height          =   330
               Index           =   40
               Left            =   1200
               TabIndex        =   53
               Tag             =   "C�digo Producto diluir"
               Top             =   2280
               Visible         =   0   'False
               Width           =   255
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "PERF"
               DataField       =   "FR28INDPERF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   8280
               TabIndex        =   69
               Tag             =   "PERF|Mezcla IV en Farmacia"
               Top             =   1280
               Width           =   855
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "STAT"
               DataField       =   "fr28indcomieninmed"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   8280
               TabIndex        =   68
               Tag             =   "STAT|Pedir la primera dosis inmediatamente"
               Top             =   800
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "fr28horafin"
               Height          =   330
               Index           =   36
               Left            =   7320
               TabIndex        =   66
               Tag             =   "H.F.|Hora Fin"
               Top             =   2640
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "fr28horainicio"
               Height          =   330
               Index           =   37
               Left            =   7440
               TabIndex        =   52
               Tag             =   "H.I.|Hora Inicio"
               Top             =   840
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR93CODUNIMEDIDA"
               Height          =   330
               Index           =   31
               Left            =   7080
               TabIndex        =   45
               Tag             =   "UM"
               Top             =   240
               Width           =   660
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28DOSIS"
               Height          =   330
               Index           =   30
               Left            =   6120
               TabIndex        =   44
               Tag             =   "Dosis"
               Top             =   240
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR28CANTIDAD"
               Height          =   330
               Index           =   28
               Left            =   0
               TabIndex        =   47
               Tag             =   "Cantidad"
               Top             =   840
               Width           =   855
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   27
               Left            =   0
               TabIndex        =   40
               Tag             =   "C�digo Medicamento"
               Top             =   2280
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "PRN"
               DataField       =   "FR28INDDISPPRN"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   1440
               TabIndex        =   239
               Tag             =   "Prn?"
               Top             =   2760
               Visible         =   0   'False
               Width           =   735
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Cambiar V�a"
               DataField       =   "FR28INDVIAOPC"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   3000
               TabIndex        =   238
               Tag             =   "Cambiar V�a?"
               Top             =   2760
               Visible         =   0   'False
               Width           =   1455
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "fr28numlinea"
               Height          =   405
               Index           =   45
               Left            =   2040
               TabIndex        =   237
               Tag             =   "Num.Linea"
               Top             =   2280
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "fr66codpeticion"
               Height          =   405
               Index           =   34
               Left            =   1800
               TabIndex        =   236
               Tag             =   "C�digo Petici�n"
               Top             =   2280
               Visible         =   0   'False
               Width           =   375
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "fr28fecinicio"
               Height          =   330
               Index           =   3
               Left            =   5520
               TabIndex        =   51
               Tag             =   "Inicio|Fecha Inicio Vigencia"
               Top             =   840
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "fr28fecfin"
               Height          =   330
               Index           =   4
               Left            =   5400
               TabIndex        =   65
               Tag             =   "Fin|Fecha Fin"
               Top             =   2640
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
               DataField       =   "fr34codvia"
               Height          =   330
               Index           =   1
               Left            =   2760
               TabIndex        =   49
               Tag             =   "V�a|C�digo V�a"
               Top             =   840
               Width           =   765
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets.count =   2
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777215
               stylesets(0).Picture=   "FR0779.frx":00E0
               stylesets(1).Name=   "Inactivo"
               stylesets(1).BackColor=   255
               stylesets(1).Picture=   "FR0779.frx":00FC
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Nombre"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1349
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 0"
            End
            Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
               DataField       =   "frg4codfrecuencia"
               Height          =   330
               Index           =   0
               Left            =   960
               TabIndex        =   48
               Tag             =   "Frec.|Cod.Frecuencia"
               Top             =   840
               Width           =   1725
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets.count =   2
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777215
               stylesets(0).Picture=   "FR0779.frx":0118
               stylesets(1).Name=   "Inactivo"
               stylesets(1).BackColor=   255
               stylesets(1).Picture=   "FR0779.frx":0134
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Nombre"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3043
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 0"
            End
            Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
               DataField       =   "frh5codperiodicidad"
               Height          =   330
               Index           =   3
               Left            =   3600
               TabIndex        =   50
               Tag             =   "Period.|Cod. Periodicidad"
               Top             =   840
               Width           =   1845
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets.count =   2
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777215
               stylesets(0).Picture=   "FR0779.frx":0150
               stylesets(1).Name=   "Inactivo"
               stylesets(1).BackColor=   255
               stylesets(1).Picture=   "FR0779.frx":016C
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Nombre"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3254
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 0"
            End
            Begin MSComDlg.CommonDialog CommonDialog1 
               Left            =   3480
               Top             =   2520
               _ExtentX        =   847
               _ExtentY        =   847
               _Version        =   327681
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Archivo Inf. F�rmula Magistral"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   84
               Left            =   360
               TabIndex        =   272
               Top             =   2280
               Visible         =   0   'False
               Width           =   2550
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Medicamento 2"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   83
               Left            =   0
               TabIndex        =   268
               Top             =   3000
               Width           =   1305
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   82
               Left            =   6120
               TabIndex        =   267
               Top             =   3000
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "UM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   81
               Left            =   7080
               TabIndex        =   266
               Top             =   3000
               Width           =   300
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "FF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   80
               Left            =   5400
               TabIndex        =   265
               Top             =   3000
               Width           =   225
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Med2"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   79
               Left            =   0
               TabIndex        =   264
               Top             =   2400
               Visible         =   0   'False
               Width           =   1125
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Volumen(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   78
               Left            =   7800
               TabIndex        =   263
               Top             =   3000
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "ml/hora"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   77
               Left            =   6420
               TabIndex        =   262
               Top             =   2085
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Vel.Perfus."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   52
               Left            =   5400
               TabIndex        =   261
               Top             =   1800
               Width           =   945
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Vol.Total(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   51
               Left            =   8040
               TabIndex        =   260
               Top             =   1800
               Width           =   1080
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Instrucciones para administraci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   50
               Left            =   0
               TabIndex        =   259
               Top             =   1800
               Width           =   2865
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Volumen(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   49
               Left            =   7800
               TabIndex        =   258
               Top             =   0
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Sol"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   48
               Left            =   1200
               TabIndex        =   257
               Top             =   2040
               Visible         =   0   'False
               Width           =   930
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   47
               Left            =   0
               TabIndex        =   256
               Top             =   2040
               Visible         =   0   'False
               Width           =   600
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "FF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   37
               Left            =   5400
               TabIndex        =   255
               Top             =   0
               Width           =   225
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tiempo Infusi�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   36
               Left            =   6600
               TabIndex        =   254
               Top             =   1200
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Volumen(ml)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   35
               Left            =   5520
               TabIndex        =   253
               Top             =   1200
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Soluci�n para Diluir"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   34
               Left            =   0
               TabIndex        =   252
               Top             =   1200
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   30
               Left            =   7320
               TabIndex        =   251
               Top             =   2400
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Inicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   33
               Left            =   5520
               TabIndex        =   250
               Top             =   600
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   32
               Left            =   7440
               TabIndex        =   249
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Fin"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   31
               Left            =   5400
               TabIndex        =   248
               Top             =   2400
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Periodicidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   29
               Left            =   3600
               TabIndex        =   247
               Top             =   600
               Width           =   1335
            End
            Begin VB.Label lblLabel1 
               Caption         =   "V�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   25
               Left            =   2760
               TabIndex        =   246
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Frecuencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   23
               Left            =   960
               TabIndex        =   245
               Top             =   600
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "UM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   21
               Left            =   7080
               TabIndex        =   244
               Top             =   0
               Width           =   300
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   19
               Left            =   6120
               TabIndex        =   243
               Top             =   0
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cantidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   0
               TabIndex        =   242
               Top             =   600
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Medicamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   16
               Left            =   0
               TabIndex        =   241
               Top             =   0
               Width           =   1140
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   0
            Left            =   -74880
            TabIndex        =   78
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3105
            Index           =   5
            Left            =   -74880
            TabIndex        =   79
            TabStop         =   0   'False
            Top             =   120
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   5477
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3465
            Index           =   6
            Left            =   420
            TabIndex        =   80
            Top             =   600
            Width           =   8895
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   7
            stylesets(0).Name=   "Estupefacientes"
            stylesets(0).BackColor=   255
            stylesets(0).Picture=   "FR0779.frx":0188
            stylesets(1).Name=   "Bloqueada"
            stylesets(1).BackColor=   16711680
            stylesets(1).Picture=   "FR0779.frx":01A4
            stylesets(2).Name=   "FormMagistral"
            stylesets(2).BackColor=   8421504
            stylesets(2).Picture=   "FR0779.frx":01C0
            stylesets(3).Name=   "Medicamentos"
            stylesets(3).BackColor=   4259584
            stylesets(3).Picture=   "FR0779.frx":01DC
            stylesets(4).Name=   "MezclaIV2M"
            stylesets(4).BackColor=   16711935
            stylesets(4).Picture=   "FR0779.frx":01F8
            stylesets(5).Name=   "Fluidoterapia"
            stylesets(5).BackColor=   16777088
            stylesets(5).Picture=   "FR0779.frx":0214
            stylesets(6).Name=   "PRNenOM"
            stylesets(6).BackColor=   8454143
            stylesets(6).Picture=   "FR0779.frx":0230
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15690
            _ExtentY        =   6112
            _StockProps     =   79
         End
         Begin TabDlg.SSTab tab2 
            Height          =   4000
            Left            =   -74880
            TabIndex        =   209
            TabStop         =   0   'False
            Top             =   120
            Width           =   9375
            _ExtentX        =   16536
            _ExtentY        =   7064
            _Version        =   327681
            Style           =   1
            Tabs            =   6
            TabsPerRow      =   6
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Medicamento"
            TabPicture(0)   =   "FR0779.frx":024C
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(64)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Mezcla IV 2M"
            TabPicture(1)   =   "FR0779.frx":0268
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lblLabel1(22)"
            Tab(1).ControlCount=   1
            TabCaption(2)   =   "PRN en OM"
            TabPicture(2)   =   "FR0779.frx":0284
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLabel1(27)"
            Tab(2).ControlCount=   1
            TabCaption(3)   =   "Flu�doterapia"
            TabPicture(3)   =   "FR0779.frx":02A0
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "lblLabel1(43)"
            Tab(3).ControlCount=   1
            TabCaption(4)   =   "Estupefacientes"
            TabPicture(4)   =   "FR0779.frx":02BC
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "lblLabel1(45)"
            Tab(4).ControlCount=   1
            TabCaption(5)   =   "Form Magistral"
            TabPicture(5)   =   "FR0779.frx":02D8
            Tab(5).ControlEnabled=   0   'False
            Tab(5).Control(0)=   "lblLabel1(46)"
            Tab(5).ControlCount=   1
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   4425
               Index           =   9
               Left            =   -74880
               TabIndex        =   210
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1305
               Index           =   10
               Left            =   -74880
               TabIndex        =   211
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00808080&
               Caption         =   "Form Magistral"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   58
               Left            =   -69025
               TabIndex        =   234
               Top             =   90
               Width           =   1050
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H000000FF&
               Caption         =   "Estupefacientes"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   59
               Left            =   -70440
               TabIndex        =   233
               Top             =   90
               Width           =   1170
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00FFFF80&
               Caption         =   "Flu�doterapia"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   60
               Left            =   -71520
               TabIndex        =   232
               Top             =   90
               Width           =   915
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H0080FFFF&
               Caption         =   "PRN en OM"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   61
               Left            =   -72555
               TabIndex        =   231
               Top             =   90
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H0080FF80&
               Caption         =   "Mezcla IV 2M"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   63
               Left            =   -73725
               TabIndex        =   230
               Top             =   90
               Width           =   960
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H0000C000&
               Caption         =   "Medicamentos"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   64
               Left            =   120
               TabIndex        =   229
               Top             =   90
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   65
               Left            =   -74880
               TabIndex        =   228
               Top             =   480
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   66
               Left            =   -70920
               TabIndex        =   227
               Top             =   600
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   67
               Left            =   -67320
               TabIndex        =   226
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   68
               Left            =   -69360
               TabIndex        =   225
               Top             =   600
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   69
               Left            =   -74760
               TabIndex        =   224
               Top             =   600
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   70
               Left            =   -74760
               TabIndex        =   223
               Top             =   600
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   71
               Left            =   -70920
               TabIndex        =   222
               Top             =   1320
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   72
               Left            =   -72840
               TabIndex        =   221
               Top             =   1320
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   73
               Left            =   -74760
               TabIndex        =   220
               Top             =   1320
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   74
               Left            =   -74760
               TabIndex        =   219
               Top             =   1320
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "M�dico que env�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   75
               Left            =   -69720
               TabIndex        =   218
               Top             =   1320
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N� Colegiado"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   76
               Left            =   -72120
               TabIndex        =   217
               Top             =   1320
               Width           =   1335
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00FF80FF&
               Caption         =   "Mezcla IV 2M"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   22
               Left            =   -73725
               TabIndex        =   216
               Top             =   90
               Width           =   960
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H0080FFFF&
               Caption         =   "PRN en OM"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   27
               Left            =   -72550
               TabIndex        =   215
               Top             =   90
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00FFFF80&
               Caption         =   "Flu�doterapia"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   43
               Left            =   -71520
               TabIndex        =   214
               Top             =   90
               Width           =   915
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H000000FF&
               Caption         =   "Estupefacientes"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   45
               Left            =   -70400
               TabIndex        =   213
               Top             =   90
               Width           =   1170
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BackColor       =   &H00808080&
               Caption         =   "Form Magistral"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   46
               Left            =   -69025
               TabIndex        =   212
               Top             =   90
               Width           =   1050
            End
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H0000C000&
            Caption         =   "Medicamentos"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   44
            Left            =   420
            TabIndex        =   86
            Top             =   210
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H00FF80FF&
            Caption         =   "Mezcla IV 2M"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   53
            Left            =   1935
            TabIndex        =   85
            Top             =   210
            Width           =   1260
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H0080FFFF&
            Caption         =   "PRN en OM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   54
            Left            =   3345
            TabIndex        =   84
            Top             =   210
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFF80&
            Caption         =   "Flu�doterapia"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   55
            Left            =   4620
            TabIndex        =   83
            Top             =   210
            Width           =   1275
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H000000FF&
            Caption         =   "Estupefacientes"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   56
            Left            =   6060
            TabIndex        =   82
            Top             =   210
            Width           =   1485
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackColor       =   &H00808080&
            Caption         =   "Form Magistral"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   57
            Left            =   7710
            TabIndex        =   81
            Top             =   210
            Width           =   1425
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   10
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Periodicidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   360
      TabIndex        =   119
      Top             =   3960
      Width           =   8535
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   29
         Left            =   6480
         TabIndex        =   150
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   30
         Left            =   7200
         TabIndex        =   149
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   22
         Left            =   1200
         TabIndex        =   148
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   23
         Left            =   2040
         TabIndex        =   147
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   24
         Left            =   2760
         TabIndex        =   146
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   25
         Left            =   3480
         TabIndex        =   145
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   26
         Left            =   4200
         TabIndex        =   144
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   27
         Left            =   4920
         TabIndex        =   143
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   28
         Left            =   5760
         TabIndex        =   142
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   15
         Left            =   3480
         TabIndex        =   141
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   16
         Left            =   4200
         TabIndex        =   140
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   17
         Left            =   4920
         TabIndex        =   139
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   18
         Left            =   5760
         TabIndex        =   138
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   19
         Left            =   6480
         TabIndex        =   137
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   20
         Left            =   7200
         TabIndex        =   136
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   21
         Left            =   480
         TabIndex        =   135
         TabStop         =   0   'False
         Top             =   2640
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   8
         Left            =   5760
         TabIndex        =   134
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   9
         Left            =   6480
         TabIndex        =   133
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   10
         Left            =   7200
         TabIndex        =   132
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   11
         Left            =   480
         TabIndex        =   131
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   12
         Left            =   1200
         TabIndex        =   130
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   13
         Left            =   1965
         TabIndex        =   129
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   14
         Left            =   2760
         TabIndex        =   128
         TabStop         =   0   'False
         Top             =   1800
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   127
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   235
         Index           =   2
         Left            =   1230
         TabIndex        =   126
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   3
         Left            =   1965
         TabIndex        =   125
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   4
         Left            =   2760
         TabIndex        =   124
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   5
         Left            =   3480
         TabIndex        =   123
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   6
         Left            =   4200
         TabIndex        =   122
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Index           =   7
         Left            =   4920
         TabIndex        =   121
         TabStop         =   0   'False
         Top             =   960
         Width           =   255
      End
      Begin VB.CommandButton cmdAceptarPer 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   3360
         TabIndex        =   120
         TabStop         =   0   'False
         Top             =   3120
         Width           =   1455
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(4)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   2760
         TabIndex        =   277
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   2760
         TabIndex        =   276
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   29
         Left            =   7200
         TabIndex        =   208
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   28
         Left            =   6480
         TabIndex        =   207
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   27
         Left            =   5760
         TabIndex        =   206
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   26
         Left            =   4920
         TabIndex        =   205
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   25
         Left            =   4200
         TabIndex        =   204
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   24
         Left            =   3480
         TabIndex        =   203
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   2760
         TabIndex        =   202
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   22
         Left            =   2040
         TabIndex        =   201
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   21
         Left            =   1200
         TabIndex        =   200
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   20
         Left            =   480
         TabIndex        =   199
         Top             =   2160
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   19
         Left            =   7200
         TabIndex        =   198
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   18
         Left            =   6480
         TabIndex        =   197
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   17
         Left            =   5760
         TabIndex        =   196
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   4920
         TabIndex        =   195
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   15
         Left            =   4200
         TabIndex        =   194
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   3480
         TabIndex        =   193
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   2760
         TabIndex        =   192
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   1965
         TabIndex        =   191
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   1200
         TabIndex        =   190
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   480
         TabIndex        =   189
         Top             =   1320
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   7185
         TabIndex        =   188
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   6480
         TabIndex        =   187
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   5760
         TabIndex        =   186
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   4920
         TabIndex        =   185
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   4200
         TabIndex        =   184
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   3465
         TabIndex        =   183
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   1965
         TabIndex        =   182
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   1230
         TabIndex        =   181
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   480
         TabIndex        =   180
         Top             =   480
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(29)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   36
         Left            =   6480
         TabIndex        =   179
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(30)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   35
         Left            =   7200
         TabIndex        =   178
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(22)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   34
         Left            =   1200
         TabIndex        =   177
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(23)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   33
         Left            =   2040
         TabIndex        =   176
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(24)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   32
         Left            =   2760
         TabIndex        =   175
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(25)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   31
         Left            =   3480
         TabIndex        =   174
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(26)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   30
         Left            =   4200
         TabIndex        =   173
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(27)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   29
         Left            =   4920
         TabIndex        =   172
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(28)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   28
         Left            =   5760
         TabIndex        =   171
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(15)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   27
         Left            =   3480
         TabIndex        =   170
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(16)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   26
         Left            =   4200
         TabIndex        =   169
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(17)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   25
         Left            =   4920
         TabIndex        =   168
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(18)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   24
         Left            =   5760
         TabIndex        =   167
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(19)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   6480
         TabIndex        =   166
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(20)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   22
         Left            =   7200
         TabIndex        =   165
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(21)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   21
         Left            =   480
         TabIndex        =   164
         Top             =   2400
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(8)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   20
         Left            =   5760
         TabIndex        =   163
         Top             =   720
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(9)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   19
         Left            =   6480
         TabIndex        =   162
         Top             =   720
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(10)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   18
         Left            =   7200
         TabIndex        =   161
         Top             =   720
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(11)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   17
         Left            =   480
         TabIndex        =   160
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(12)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   1200
         TabIndex        =   159
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(13)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   15
         Left            =   1965
         TabIndex        =   158
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(14)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   2760
         TabIndex        =   157
         Top             =   1560
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   480
         TabIndex        =   156
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(2)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   1230
         TabIndex        =   155
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(3)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   1965
         TabIndex        =   154
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(5)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   3480
         TabIndex        =   153
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(6)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   4200
         TabIndex        =   152
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "(7)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   4920
         TabIndex        =   151
         Top             =   720
         Width           =   240
      End
   End
   Begin VB.Label lblLabel1 
      Caption         =   "V�a"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   26
      Left            =   0
      TabIndex        =   77
      Top             =   0
      Width           =   975
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmEstPetFarmOM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRedactarOMPRN (FR0111.FRM)                                *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: redactar Orden M�dica                                   *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Dim vntAux As Variant
Dim intDia(30) As Integer
Dim intNumDia As Integer
Dim mstrOperacion As String 'saber que tipo de linea estamos creando en el hijo
'mstrOperacion = "/" 'Medicamento
'mstrOperacion = "M" 'Mezcla IV 2M
'mstrOperacion = "P" 'PRN en OM
'mstrOperacion = "F" 'Fluidoterapia
'mstrOperacion = "E" 'Estupefaciente
'mstrOperacion = "L" 'Form.Magistral
Dim intCambioAlgo As Integer 'Para saber si cambian las horas y minutos, por no ser campos CW
Dim blnDetalle As Boolean 'Para saber si estoy en modo detalle en el hijo
Dim blnBoton As Boolean 'Para saber si he pulsado un boton
Dim intBotonMasInf As Integer 'Para dar informacion en funcion del campo en el que este el cursor
Dim blnFormActRest As Boolean
Dim blnObligatorio As Boolean
Dim strOrigen As String 'Para saber si el evento change se produce por modificaciones en el campo o no;
                        'vale V cuando se ha modificado manualmente la velocidad de perfusi�n,
                        'y vale T cuando se ha modificado manualmente el tiempo de perfusi�n
Dim blnNoVerAgen As Boolean 'True: No hay que mostrar la agenda
Dim mblnError As Boolean



Private Sub chkOMPRN_Click(Index As Integer)

If objWinInfo.intWinStatus = 1 Then
  If chkOMPRN(6).Value = 1 Then
    If chkCheck1(12).Value <> 0 Then
      Call objWinInfo.CtrlSet(chkCheck1(12), 0)
    End If
  Else
    If chkCheck1(12).Value <> 1 Then
      Call objWinInfo.CtrlSet(chkCheck1(12), 1)
    End If
  End If
End If

End Sub



Private Sub cmdExplorador_Click(Index As Integer)
Dim intRetVal
Dim strOpenFileName As String
Dim pathFileName As String
Dim finpath As Integer

If txtText1(34).Text = "" Then
   MsgBox "Debe hacer nuevo registro primero con el bot�n de F�rmula Magistral.", vbInformation, "Aviso"
  Exit Sub
End If


If Index = 0 Then
Else
    On Error Resume Next
    pathFileName = Dir(txtText1(57).Text)
    finpath = InStr(1, txtText1(57).Text, pathFileName, 1)
    pathFileName = Left(txtText1(57).Text, finpath - 1)
    CommonDialog1.InitDir = pathFileName
    CommonDialog1.filename = txtText1(57).Text
    CommonDialog1.CancelError = True
    CommonDialog1.Filter = "*.*"
    CommonDialog1.ShowOpen
    If err.Number <> 32755 Then    ' El usuario eligi� Cancelar.
        strOpenFileName = CommonDialog1.filename
        txtText1(57).SetFocus
        Call objWinInfo.CtrlSet(txtText1(57), "")
        txtText1(57).SetFocus
        Call objWinInfo.CtrlSet(txtText1(57), strOpenFileName)
    End If
End If

End Sub








Private Sub cmdWord_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim strpathword As String
  
On Error GoTo error_shell
  
'Par�metros generales
'5,Ubicaci�n de Word,C:\Archivos de programa\Microsoft Office\Office\winword.exe,TEXTO
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=5"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strpathword = rsta(0).Value
  Else
    rsta.Close
    Set rsta = Nothing
    MsgBox "Debe introducir el Path del Word en par�metros generales." & Chr(13) & _
           "C�digo par�metros generales : 5 " & Chr(13) & _
           "Ej.: C:\Archivos de programa\Microsoft Office\Office\winword.exe", vbInformation
    Exit Sub
  End If
  rsta.Close
  Set rsta = Nothing
  
  If txtText1(57).Text = "" Then
    MsgBox "Debe introducir el Path y el nombre del fichero del documento", vbInformation
    Exit Sub
  End If
  
  Shell strpathword & " " & txtText1(57).Text, vbMaximizedFocus

error_shell:

  If err.Number <> 0 Then
    MsgBox "Error N�mero: " & err.Number & Chr(13) & err.Description, vbCritical
  End If

End Sub


Private Sub lblLabel1_DblClick(Index As Integer)
    
    Select Case Index
    Case 44
        If tab2.Tab = 0 Then
          blnDetalle = True
          tab2.Tab = 1
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 0
    Case 53
        If tab2.Tab = 1 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 1
    Case 54
        If tab2.Tab = 2 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 2
    Case 55
        If tab2.Tab = 3 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 3
    Case 56
        If tab2.Tab = 4 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 4
    Case 57
        If tab2.Tab = 5 Then
          blnDetalle = True
          tab2.Tab = 0
        End If
        tabTab1(1).Tab = 0
        tab2.Tab = 5
        txtText1(57).SetFocus
    End Select
    
    Call presentacion_pantalla

End Sub


Private Sub tab2_Click(PreviousTab As Integer)
Dim auxtab As Integer
    
If blnObligatorio = False Then
  If objWinInfo.objWinActiveForm.strName <> "Detalle OM" Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  End If

  If tab2.Tab <> PreviousTab And blnDetalle Then
    'If tlbToolbar1.Buttons(4).Enabled = True Then
      'Guardar
      If Campos_Obligatorios = True Then
        'tab2.SetFocus
        'Call objWinInfo.CtrlGotFocus
        blnObligatorio = True
        tab2.Tab = PreviousTab
        Exit Sub
      'Else
      ElseIf tlbToolbar1.Buttons(4).Enabled = True Then
        auxtab = tab2.Tab
        Call objWinInfo.DataSave
        blnObligatorio = False
        tab2.Tab = auxtab
      End If
    'Else
      blnObligatorio = False
      'Vaciar pantalla
      objWinInfo.intWinStatus = cwModeSingleEmpty
      Call objWinInfo.WinStabilize
    'End If
  End If

  Call presentacion_pantalla
  If tabTab1(1).Tab = 0 And tab2.Tab = 5 And blnDetalle Then
    txtText1(57).SetFocus
  ElseIf tabTab1(1).Tab = 0 And blnDetalle Then
    If txtText1(35).Enabled = True And txtText1(35).Visible = True Then
      txtText1(35).SetFocus
    End If
  End If
Else
  blnObligatorio = False
End If

End Sub

Private Sub tab2_DblClick()
  
    Call presentacion_pantalla

End Sub




Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If

End Sub


Private Sub cmdAceptarPer_Click()
  Dim intDiaSemana As Integer
  Dim intsuma As Integer
  Dim i As Integer
  Dim strDiasAdm As String
  Dim strUpd28 As String
  Dim qryUpd28 As rdoQuery
  
  tlbToolbar1.Enabled = True
  Frame2.Visible = False
  'cmdsitcarro.Enabled = True
  'cmdfirmar.Enabled = True
  'cmdfirmarenviar.Enabled = True
  'cmdenviar.Enabled = True
  'cmdbuscargruprot.Enabled = True
  'cmdbuscarprot.Enabled = True
  'cmdbuscargruprod.Enabled = True
  'cmdbuscarprod.Enabled = True
  'cmdperfil.Enabled = True
  'cmdHCpaciente.Enabled = True
  'cmdAlergia.Enabled = True
  fraFrame1(0).Enabled = True
  fraFrame1(1).Enabled = True
  strDiasAdm = ""
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  For i = 1 To 30
    If Check1(i).Value = 1 Then
      'Call insertar(i - 1)
      strDiasAdm = strDiasAdm & (i - 1) & "*"
      Check1(i).Value = 0
    End If
  Next i
  
  If Len(strDiasAdm) > 0 Then
    strUpd28 = "UPDATE FR2800 " & _
               "   SET FR28DIASADM = ? " & _
               " WHERE FR66CODPETICION = ? " & _
               "   AND FR28NUMLINEA = ? "
    Set qryUpd28 = objApp.rdoConnect.CreateQuery("", strUpd28)
    qryUpd28(0) = strDiasAdm
    qryUpd28(1) = txtText1(0).Text
    qryUpd28(2) = txtText1(45).Text
    qryUpd28.Execute
  End If
  
  'objWinInfo.DataRefresh
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
End Sub




Private Sub Form_Activate()
  Dim strfecnac As String
  Dim rstfecnac As rdoResultset
  Dim edad As Integer
  Dim strPotPeso As String
  Dim rstPotPeso As rdoResultset
  Dim strPotAlt As String
  Dim rstPotAlt As rdoResultset
  Dim strFactor As String
  Dim rstFactor As rdoResultset
  Dim intPeso As Integer
  Dim intAltura As Integer
  Dim vntMasa As Variant

  Me.Enabled = False
  
  'se calcula la edad
  strfecnac = "SELECT CI22FECNACIM FROM CI2200 WHERE CI21CODPERSONA=" & txtText1(2).Text
  Set rstfecnac = objApp.rdoConnect.OpenResultset(strfecnac)
  If Not rstfecnac.EOF Then
    If Not IsNull(rstfecnac.rdoColumns(0).Value) Then
      edad = DateDiff("d", rstfecnac.rdoColumns(0).Value, Date) \ 365
      Text1.Text = edad
    Else
      Text1.Text = ""
    End If
  Else
    Text1.Text = ""
  End If
  rstfecnac.Close
  Set rstfecnac = Nothing
    
  Text1.Locked = True

  If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
    strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
    Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
    strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
    Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
    strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
    Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
    intPeso = txtText1(32).Text
    intAltura = txtText1(33).Text
    vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
    vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
    vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
    txtText1(29).Text = vntMasa
  End If

  Me.Enabled = True

  If tabTab1(1).Tab = 0 Then
    blnDetalle = True
  Else
    blnDetalle = False
  End If
  Screen.MousePointer = vbDefault
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objDetailInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm
Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    .strWhere = "FR66CODPETICION=" & glngpeticion & _
        " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
        " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
    .strName = "OM"
    .intAllowance = cwAllowReadOnly
    .strTable = "FR6600"
    
    Call .FormAddOrderField("FR66CODPETICION", cwDescending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "OM")
    Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDALERGIA", "Alergia?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
  End With

  With objDetailInfo
    .strName = "Detalle OM"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(6)
    .strTable = "FR2800" 'Perfil FTP
    .blnAskPrimary = False
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    Call .FormAddRelation("FR66CODPETICION", txtText1(0))
    
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Medicamentos")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR28CANTIDAD", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "Forma Farmace�tica", cwString)
    Call .FormAddFilterWhere(strKey, "FR28DOSIS", "Dosis", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "Unidad Medida", cwString)
    Call .FormAddFilterWhere(strKey, "frg4codfrecuencia", "Frecuencia", cwString)
    Call .FormAddFilterWhere(strKey, "fr34codvia", "V�a", cwString)
    Call .FormAddFilterWhere(strKey, "frh5codperiodicidad", "Periodicidad", cwString)
    Call .FormAddFilterWhere(strKey, "fr28fecinicio", "Fecha Inicio", cwDate)
    Call .FormAddFilterWhere(strKey, "fr28horainicio", "Hora Inicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "fr28fecfin", "Fecha Fin", cwDate)
    Call .FormAddFilterWhere(strKey, "fr28horafin", "Hora Fin", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO_DIL", "Cod.Producto Diluy.", cwNumeric)
    Call .FormAddFilterWhere(strKey, "fr28indcomieninmed", "Comienzo Inmediato?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "fr28indsn", "S.N?", cwBoolean)
  End With

  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
  
   
    
    Call .FormCreateInfo(objMasterInfo)
    
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(txtText1(25)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(12)).blnInFind = True
    .CtrlGetInfo(chkCheck1(15)).blnInFind = True
    
    'productos
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(35)).blnInFind = True
    .CtrlGetInfo(txtText1(30)).blnInFind = True
    .CtrlGetInfo(txtText1(31)).blnInFind = True
    .CtrlGetInfo(txtText1(38)).blnInFind = True
    .CtrlGetInfo(txtText1(28)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(3)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(37)).blnInFind = True
    .CtrlGetInfo(txtText1(41)).blnInFind = True
    .CtrlGetInfo(txtText1(42)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(4)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(42)).blnInFind = True
    .CtrlGetInfo(txtText1(36)).blnInFind = True
    
    .CtrlGetInfo(Text1).blnNegotiated = False
    .CtrlGetInfo(txtText1(29)).blnNegotiated = False
    .CtrlGetInfo(txtHoras).blnNegotiated = False
    .CtrlGetInfo(txtMinutos).blnNegotiated = False
    
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True
    .CtrlGetInfo(txtText1(29)).blnReadOnly = True
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True
    .CtrlGetInfo(txtText1(14)).blnReadOnly = True
    
    '.CtrlGetInfo(txtOperacion).blnNegotiated = False
     .CtrlGetInfo(Text1).blnReadOnly = True
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM FR2200J WHERE CI21CODPERSONA= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(4), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(5), "CI22SEGAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(23), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(7), "CI30CODSEXO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM FR2200J WHERE CI21CODPERSONA= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(46), "DESCCAMA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "CI30CODSEXO", "SELECT * FROM CI3000 WHERE CI30CODSEXO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(6), "CI30DESSEXO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(24)), "SG02COD_FEN", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(19), "SG02APE1")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(21), "SG02NUMCOLEGIADO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(15), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(11), "FR91DESURGENCIA")
    
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(61)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(61)), txtText1(62), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(26), "FR26DESESTADOPET")
    '**********************************************************************************
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(27)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(27)), txtText1(13), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(40)), "FR73CODPRODUCTO_DIL", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(40)), txtText1(41), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(52)), "FR73CODPRODUCTO_2", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(52)), txtText1(51), "FR73DESPRODUCTO")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(52)), txtText1(47), "FR73VOLUMEN")
    .CtrlGetInfo(txtText1(47)).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    .CtrlGetInfo(txtText1(16)).blnForeign = True
    .CtrlGetInfo(txtText1(22)).blnForeign = True
    .CtrlGetInfo(txtText1(25)).blnForeign = True
        
    .CtrlGetInfo(txtText1(27)).blnForeign = True
    .CtrlGetInfo(txtText1(41)).blnForeign = True
    .CtrlGetInfo(txtText1(31)).blnForeign = True
    '.CtrlGetInfo(txtText1(81)).blnForeign = True
    '.CtrlGetInfo(txtText1(80)).blnForeign = True
    .CtrlGetInfo(txtText1(35)).blnForeign = True
    
    .CtrlGetInfo(txtText1(48)).blnForeign = True
    .CtrlGetInfo(txtText1(49)).blnForeign = True
     
    .CtrlGetInfo(txtText1(10)).blnReadOnly = True
    .CtrlGetInfo(txtText1(20)).blnReadOnly = True
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True
    .CtrlGetInfo(txtText1(25)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = True
    .CtrlGetInfo(txtText1(44)).blnReadOnly = True
    
    .CtrlGetInfo(dtcDateCombo1(3)).blnMandatory = True
    .CtrlGetInfo(txtText1(37)).blnMandatory = True
    .CtrlGetInfo(txtText1(38)).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(59)).blnInGrid = False 'Proceso
    .CtrlGetInfo(txtText1(60)).blnInGrid = False 'Asistencia
    
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(1)).strSql = "SELECT FR34CODVIA,FR34DESVIA FROM FR3400 ORDER BY FR34CODVIA"
    
    .CtrlGetInfo(cboDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(0)).strSql = "SELECT FRG4CODFRECUENCIA,FRG4DESFRECUENCIA FROM FRG400 ORDER BY FRG4CODFRECUENCIA"
    
    .CtrlGetInfo(cboDBCombo1(3)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(3)).strSql = "SELECT FRH5CODPERIODICIDAD,FRH5DESPERIODICIDAD FROM FRH500 ORDER BY FRH5CODPERIODICIDAD"
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

  ginteventloadRedOM = 0
  Frame2.Visible = False
  'mstrOperacion = "/"
  blnFormActRest = False
  blnObligatorio = False
  Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  gintfirmarOM = 0
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBgrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
  Dim i As Integer
  Dim v As Integer
  Dim sqlstr As String
  Dim rsta As rdoResultset

  If Index = 6 Then
  v = 37
  Select Case grdDBGrid1(6).Columns("Operaci�n").Value
  Case "/"  'Medicamento
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "Medicamentos"
    Next i
  Case "M"  'Mezcla IV 2M
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "MezclaIV2M"
    Next i
  Case "P"  'PRN en OM
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "PRNenOM"
    Next i
  Case "F" 'Fluidoterapia
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "Fluidoterapia"
    Next i
  Case "E" 'Estupefaciente
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "Estupefacientes"
    Next i
  Case "L" 'Form.Magistral
    For i = 0 To v
      grdDBGrid1(6).Columns(i).CellStyleSet "FormMagistral"
    Next i
  End Select
  End If
    
  On Error GoTo err
  If Index = 6 Then
    If grdDBGrid1(6).Columns("Bloq").Value = -1 Then
      For i = 1 To 40
        grdDBGrid1(6).Columns(i).CellStyleSet "Bloqueada"
      Next i
    End If
        
  End If
err:
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strFormName = "OM" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR2200J" 'pacientes con cama ocupada

     Set objField = .AddField("CI21CODPERSONA")
     objField.strSmallDesc = "C�digo Paciente"

     Set objField = .AddField("CI22NOMBRE")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("CI22NUMHISTORIA")
     objField.strSmallDesc = "Historia"
     
     Set objField = .AddField("CI22PRIAPEL")
     objField.strSmallDesc = "Apellido 1�"
     
     Set objField = .AddField("CI22SEGAPEL")
     objField.strSmallDesc = "Apellido 2�"

     Set objField = .AddField("DESCCAMA")
     objField.strSmallDesc = "Cama"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("CI21CODPERSONA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "OM" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo M�dico"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "OM" And strCtrl = "txtText1(16)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD32CODTIPODPTO=3 AND " & _
           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Dpto."

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto."

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(16), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(22)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9100"

     Set objField = .AddField("FR91CODURGENCIA")
     objField.strSmallDesc = "C�digo Urgencia"

     Set objField = .AddField("FR91DESURGENCIA")
     objField.strSmallDesc = "Descripci�n Urgencia"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(22), .cllValues("FR91CODURGENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(27)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
 
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Medicamento"
 
 
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
 
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(27), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(41)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     '.strWhere = " WHERE FR73INDPRODREC=-1"
     .strWhere = " WHERE FR73INDINFIV=-1"
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Medicamento"
 
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
    
     Set objField = .AddField("FR73VOLUMEN")
     objField.strSmallDesc = "Volumen"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     'Set objField = .AddField("FR73VOLINFIV")
     'objField.strSmallDesc = "Volumen Infusi�n IV"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(40), .cllValues("FR73CODPRODUCTO"))
      'Call objWinInfo.CtrlSet(txtText1(42), .cllValues("FR73VOLINFIV"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(31)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad de Medida"
     
     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(31), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(49)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad de Medida"
     
     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(49), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).Forma Farmace�tica" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"

     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"
     
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(18), .cllValues("FRH7CODFORMFAR"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(35)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"
 
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"
 
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"
 
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(35), .cllValues("FRH7CODFORMFAR"))
     End If
    End With
    Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(48)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"
 
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"
 
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"
 
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(48), .cllValues("FRH7CODFORMFAR"))
     End If
    End With
    Set objSearch = Nothing
 End If

End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)

Me.Enabled = False

  If strFormName = "Detalle OM" Then
    If txtText1(25).Text <> "" Then
      If txtText1(25).Text <> 1 Then
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
          'objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
          'objWinInfo.DataRefresh
        End If
      Else
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
          'objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
          'objWinInfo.DataRefresh
        End If
      End If
    End If
  Else
    If tabTab1(1).Tab <> 1 Then
      tabTab1(1).Tab = 1
      objWinInfo.DataMoveFirst
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    End If
    If txtText1(25).Text <> "" Then
      If txtText1(25).Text <> 1 Then
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAdd Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
          'objWinInfo.DataRefresh
        End If
      Else
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
          'objWinInfo.DataRefresh
        End If
      End If
    End If
  End If

Me.Enabled = True

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'txtOperacion.Text = cboDBCombo1(4).Text
'On Error GoTo Error
  
  If strFormName = "Detalle OM" Then
  '  If (cboDBCombo1(1).Text = "IV") Or (cboDBCombo1(1).Text = "PC") Or (cboDBCombo1(1).Text = "PIN") Then
  '    'Hay que activar la velocidad de perfusi�n
  '    objWinInfo.CtrlGetInfo(txtText1(82)).blnReadOnly = False
  '  Else
  '    'Hay que desactivar la velocidad de perfusi�n
  '    objWinInfo.CtrlGetInfo(txtText1(82)).blnReadOnly = True
  '  End If
  objWinInfo.CtrlGetInfo(txtText1(82)).blnReadOnly = False
  End If
  
  If objWinInfo.objWinActiveForm.strName = "OM" Then
    If objWinInfo.intWinStatus = 1 Then
      If Not objWinInfo.objWinActiveForm.rdoCursor.EOF Then
        If objWinInfo.objWinActiveForm.rdoCursor("FR66INDOM").Value = 0 Then
        'If chkCheck1(12).Value = 0 Then
        chkOMPRN(6).Value = 1
        End If
      End If
    End If
    If txtText1(25).Text <> "" Then
      If txtText1(25).Text <> 1 Then
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAdd Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        End If
      Else
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
          objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        End If
      End If
    End If
  
  Else
    If txtText1(25).Text <> "" Then
      If txtText1(25).Text <> 1 Then
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowReadOnly Then
          'objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
          'JMRL objWinInfo.DataRefresh
        End If
      Else
        If objWinInfo.objWinActiveForm.intAllowance <> cwAllowAll Then
          'objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
          'JMRL objWinInfo.DataRefresh
        End If
      End If
    End If
    
    If objWinInfo.intWinStatus = 1 Then
      If grdDBGrid1(6).Rows > 0 Then
        Select Case grdDBGrid1(6).Columns("Operaci�n").Value
        Case "/"  'Medicamento
          tab2.Tab = 0
        Case "M"  'Mezcla IV 2M
          tab2.Tab = 1
        Case "P"  'PRN en OM
          tab2.Tab = 2
        Case "F" 'Fluidoterapia
          tab2.Tab = 3
        Case "E" 'Estupefaciente
          tab2.Tab = 4
        Case "L" 'Form.Magistral
          tab2.Tab = 5
        End Select
        Call presentacion_pantalla
      End If
    End If
  End If

'Error:
'  Exit Sub

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    
  If strFormName = "Detalle OM" And cboDBCombo1(3).Text = "Agenda" And blnNoVerAgen = False Then
    Call periodicidad
    blnNoVerAgen = False
  End If
  intCambioAlgo = 0

End Sub


Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim strDelete As String

  If objWinInfo.objWinActiveForm.strName = "OM" Then
    If txtText1(25).Text <> 1 Then
      blnCancel = True
      Call MsgBox("No se puede borrar una petici�n " & txtText1(26).Text, _
                   vbInformation, "Aviso")
      Exit Sub
    End If
    If txtText1(0).Text <> "" Then
      stra = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        blnCancel = True
        Call MsgBox("No se puede borrar una Petici�n hasta borrar todos sus Medicamentos.", _
                     vbInformation, "Aviso")
        rsta.Close
        Set rsta = Nothing
        Exit Sub
      Else
        strDelete = "DELETE FROM FRA400 WHERE FR66CODPETICION=" & txtText1(0).Text
        objApp.rdoConnect.Execute strDelete, 64
        objApp.rdoConnect.Execute "Commit", 64
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  Else 'Detalle OM
  End If

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim rsta As rdoResultset
  Dim stra As String
  Dim mensaje As String
  Dim dblHoraFin As Double
  Dim dblHoraInicio As Double
  
  If objWinInfo.objWinActiveForm.strName = "OM" Then
    If txtText1(2).Text <> "" Then
      stra = "SELECT CI21CODPERSONA FROM FR2200J " & _
             " WHERE CI21CODPERSONA=" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta.EOF Then
        mensaje = MsgBox("El paciente es incorrecto.", vbInformation, "Aviso")
        blnCancel = True
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  Else 'Detalle
    txtText1(58).Text = txtText1(28).Text
    Select Case txtText1(53).Text 'Campos Obligatorios
    Case "/" 'Medicamento
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
    Case "M"  'Mezcla IV 2M
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If txtText1(52).Text <> "" Then
        If txtText1(48).Text = "" Then
          mensaje = MsgBox("El campo Forma Farmace�tica del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(48).SetFocus
          Exit Sub
        End If
        If txtText1(50).Text = "" Then
          mensaje = MsgBox("El campo Dosis del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(50).SetFocus
          Exit Sub
        End If
        If txtText1(49).Text = "" Then
          mensaje = MsgBox("El campo Unidad de Medida del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(49).SetFocus
          Exit Sub
        End If
      End If
    Case "P"  'PRN en OM
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If txtText1(39).Text = "" Then
        mensaje = MsgBox("Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(39).SetFocus
        Exit Sub
      End If
    Case "F"  'Fluidoterapia
      If txtText1(40).Text = "" Then
        mensaje = MsgBox("El campo Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(42).SetFocus
        Exit Sub
      End If
      If txtText1(42).Text = "" Then
        mensaje = MsgBox("El campo Volumen de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(42).SetFocus
        Exit Sub
      End If
      If txtHoras.Text = "" Then
        mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtHoras.SetFocus
        Exit Sub
      End If
      If txtMinutos.Text = "" Then
        mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtMinutos.SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(27).Text <> "" Then
        If txtText1(35).Text = "" Then
          mensaje = MsgBox("El campo Forma Farmace�tica del Medicamento es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(35).SetFocus
          Exit Sub
        End If
        If txtText1(30).Text = "" Then
          mensaje = MsgBox("El campo Dosis del Medicamento es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(30).SetFocus
          Exit Sub
        End If
        If txtText1(31).Text = "" Then
          mensaje = MsgBox("El campo Unidad de Medida del Medicamento es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(31).SetFocus
          Exit Sub
        End If
      End If
    Case "E" 'Estupefaciente
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Estupefaciente es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      If txtText1(35).Text = "" Then
        mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(35).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
    Case "L"  'Form.Magistral
      If txtText1(55).Text = "" Then
        mensaje = MsgBox("El campo Descripci�n de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(55).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(57).Text = "" Then
        mensaje = MsgBox("El campo Archivo Inf. de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(57).SetFocus
        Exit Sub
      End If
    End Select
    End If
  
  
  If strFormName = "Detalle OM" Then
   'Se comprueba que la fecha de inicio de la l�nea de detalle de la OM se menor
    'que la fecha de fin
    If (dtcDateCombo1(4).Text <> "") Then
      'Hay fecha de fin
      If dtcDateCombo1(4).Date < dtcDateCombo1(3).Date Then
        mensaje = MsgBox("La fecha de fin no puede ser menor que la fecha de inicio", vbInformation, "Aviso")
        blnCancel = True
        dtcDateCombo1(4).SetFocus
      Else
        If dtcDateCombo1(4).Date = dtcDateCombo1(3).Date Then
          If IsNumeric(txtText1(36).Text) Then
            'Hay hora de fin y hay que compararla con la hora de inicio
            If IsNumeric(txtText1(37).Text) Then
              dblHoraFin = txtText1(36).Text
              dblHoraInicio = txtText1(37).Text
              If dblHoraFin < dblHoraInicio Then
                mensaje = MsgBox("La hora de Fin debe ser mayor que la hora de Inicio", vbInformation, "Aviso")
                blnCancel = True
                txtText1(37).SetFocus
              End If
            Else
              mensaje = MsgBox("Debe introducir la hora de inicio", vbInformation, "Aviso")
              blnCancel = True
              txtText1(37).SetFocus
            End If
          End If
        End If
      End If
    End If
  End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
  
  If Index = 1 Then
    If tabTab1(1).Tab = 0 Then
      blnDetalle = True
    Else
      blnDetalle = False
    End If
  End If
  
  If Index = 1 Then
    If objWinInfo.objWinActiveForm.strName <> "Detalle OM" Then
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    End If
    If grdDBGrid1(6).Rows > 0 Then
      If grdDBGrid1(6).Enabled = True Then
      Select Case grdDBGrid1(6).Columns("Operaci�n").Value
      Case "/"  'Medicamento
        tab2.Tab = 0
      Case "M"  'Mezcla IV 2M
        tab2.Tab = 1
      Case "P"  'PRN en OM
        tab2.Tab = 2
      Case "F" 'Fluidoterapia
        tab2.Tab = 3
      Case "E" 'Estupefaciente
        tab2.Tab = 4
      Case "L" 'Form.Magistral
        tab2.Tab = 5
      End Select
      End If
    End If
    Call presentacion_pantalla
  End If
    
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstfec As rdoResultset
Dim strfec As String
Dim hora As Variant
Dim auxProceso, auxAsistencia As Currency
Dim strpeso As String
Dim rstpeso As rdoResultset

  If ((btnButton.Index = 21) Or (btnButton.Index = 22) Or (btnButton.Index = 23) Or (btnButton.Index = 24)) And objWinInfo.objWinActiveForm.strName <> "Detalle OM" Then
    Exit Sub
  End If

  If btnButton.Index = 30 Then 'Salir
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  End If
  

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "OM" Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR66CODPETICION_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    'por defecto se clica que sea Orden M�dica
    Call objWinInfo.CtrlSet(chkCheck1(12), 1)
    'fecha y hora de redacci�n
    strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    Call objWinInfo.CtrlSet(dtcDateCombo1(2), rstfec.rdoColumns(0).Value)
    strfec = "(SELECT TO_CHAR(SYSDATE,'HH24') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = rstfec.rdoColumns(0).Value & ","
    strfec = "(SELECT TO_CHAR(SYSDATE,'mi') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    hora = hora & rstfec.rdoColumns(0).Value
    Call objWinInfo.CtrlSet(txtText1(10), hora)
    rstfec.Close
    Set rstfec = Nothing
    Call objWinInfo.CtrlSet(txtText1(2), glngselpaciente)  'paciente
    Call objWinInfo.CtrlSet(txtText1(25), 1)  'estado REDACTADA
    Call objWinInfo.CtrlSet(txtText1(26), "REDACTADA")
    'se selecciona el servicio ----------------------------------
    Dim strad0100 As String
    Dim rstad0100 As rdoResultset
    Dim strad0800 As String
    Dim rstad0800 As rdoResultset
    Dim strad1500 As String
    Dim rstad1500 As rdoResultset
    
    strad0100 = "SELECT AD01CODASISTENCI FROM AD0100 WHERE " & _
                "CI21CODPERSONA=" & glngselpaciente & _
                " AND AD34CODESTADO=1 " & _
                " AND AD01FECFIN IS NULL" & _
                " AND AD01FECINICIO = (SELECT MAX(AD01FECINICIO) FROM AD0100 WHERE " & _
                                       "CI21CODPERSONA=" & glngselpaciente & _
                                       " AND AD34CODESTADO=1 " & _
                                       " AND AD01FECFIN IS NULL)"
    Set rstad0100 = objApp.rdoConnect.OpenResultset(strad0100)
    auxAsistencia = rstad0100.rdoColumns(0).Value
    strad0800 = "SELECT AD07CODPROCESO FROM AD0800 WHERE " & _
        "AD01CODASISTENCI=" & rstad0100.rdoColumns(0).Value & _
        " AND AD34CODESTADO=1 " & _
        " AND AD08FECFIN IS NULL" & _
        " AND AD08FECINICIO = (SELECT MAX(AD08FECINICIO) FROM AD0800 WHERE " & _
                            "AD01CODASISTENCI=" & rstad0100.rdoColumns(0).Value & _
                            " AND AD34CODESTADO=1 " & _
                            " AND AD08FECFIN IS NULL)"
    Set rstad0800 = objApp.rdoConnect.OpenResultset(strad0800)
    auxProceso = rstad0800.rdoColumns(0).Value
    strad1500 = "SELECT AD02CODDPTO FROM AD1500 WHERE " & _
              "AD01CODASISTENCI=" & rstad0100.rdoColumns(0).Value & _
              " AND AD07CODPROCESO=" & rstad0800.rdoColumns(0).Value
    Set rstad1500 = objApp.rdoConnect.OpenResultset(strad1500)
    Call objWinInfo.CtrlSet(txtText1(16), rstad1500.rdoColumns(0).Value)    'servicio
    rstad0100.Close
    Set rstad0100 = Nothing
    rstad0800.Close
    Set rstad0800 = Nothing
    rstad1500.Close
    Set rstad1500 = Nothing
    '------------------------------------------------------------
    txtText1(8).Text = objsecurity.strUser
    txtText1(59).Text = auxProceso
    txtText1(60).Text = auxAsistencia
    
    strpeso = "SELECT AD38PESO,AD38ALTURA FROM AD3800 WHERE " & _
              "AD01CODASISTENCI=" & auxAsistencia
    Set rstpeso = objApp.rdoConnect.OpenResultset(strpeso)
    If Not rstpeso.EOF Then
      If Not IsNull(rstpeso.rdoColumns(0).Value) Then
        txtText1(32).Text = rstpeso.rdoColumns(0).Value
      End If
      If Not IsNull(rstpeso.rdoColumns(1).Value) Then
        txtText1(33).Text = rstpeso.rdoColumns(1).Value
      End If
    End If
    rstpeso.Close
    Set rstpeso = Nothing
    
    'SendKeys ("{TAB}")
    rsta.Close
    Set rsta = Nothing
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
      If blnBoton = False Then
        MsgBox "Para hacer un nuevo registro, ha de utilizar los buscadores de productos.", vbInformation
        Exit Sub
      End If
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                  txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea.rdoColumns(0).Value) Then
          linea = 1
        Else
          linea = rstlinea.rdoColumns(0).Value + 1
        End If
        txtText1(53).Text = mstrOperacion
        Select Case tab2.Tab
          Case 0
            mstrOperacion = "/" 'Medicamento
          Case 1
            mstrOperacion = "M" 'Mezcla IV 2M
          Case 2
            mstrOperacion = "P" 'PRN en OM
          Case 3
            mstrOperacion = "F" 'Fluidoterapia
          Case 4
            mstrOperacion = "E" 'Estupefaciente
          Case 5
            mstrOperacion = "L" 'Form.Magistral
        End Select
        txtText1(53).Text = mstrOperacion
        txtText1(45).Text = linea
        'SendKeys ("{TAB}")
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna petici�n", vbInformation
        Exit Sub
      End If
    Else
      If btnButton.Index = 4 And objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
        If txtText1(27).Text <> "" Then
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        Else
          'MsgBox "El Medicamento es obligatorio.", vbInformation
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Exit Sub
        End If
      Else
        If objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
          blnDetalle = False
          Select Case btnButton.Index
          Case 21 'Primero
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 22 'Anterior
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 23 'Siguiente
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 24 'Ultimo
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case Else 'Otro boton
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          End Select
          If tabTab1(1).Tab = 0 Then
            blnDetalle = True
          Else
            blnDetalle = False
          End If
        Else
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        End If
      End If
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 40 'Guardar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
      
    Select Case grdDBGrid1(6).Columns("Operaci�n").Value
    Case "/"  'Medicamento
      tab2.Tab = 0
    Case "M"  'Mezcla IV 2M
      tab2.Tab = 1
    Case "P"  'PRN en OM
      tab2.Tab = 2
    Case "F" 'Fluidoterapia
      tab2.Tab = 3
    Case "E" 'Estupefaciente
      tab2.Tab = 4
    Case "L" 'Form.Magistral
      tab2.Tab = 5
    End Select
    Call presentacion_pantalla

End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    If intIndex = 6 Then
      blnDetalle = False
    End If
    
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    If intIndex = 6 Then
      Call presentacion_pantalla
      If tabTab1(1).Tab = 0 Then
        blnDetalle = True
      Else
        blnDetalle = False
      End If
    End If
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  
  Select Case intIndex
  Case 0
    If chkCheck1(0).Value = 0 Then
      chkCheck1(0).ForeColor = vbBlack
    Else
      chkCheck1(0).ForeColor = vbRed
    End If
  Case 2
    If chkCheck1(2).Value = 0 Then
      chkCheck1(2).ForeColor = vbBlack
    Else
      chkCheck1(2).ForeColor = vbRed
    End If
  Case 7
    If chkCheck1(7).Value = 0 Then
      chkCheck1(7).ForeColor = vbBlack
    Else
      chkCheck1(7).ForeColor = vbRed
    End If
  End Select
  
  Select Case intIndex
    Case 1:
        If chkCheck1(1).Value = 1 Then
            chkCheck1(3).Value = 0
            chkCheck1(4).Value = 0
            chkCheck1(3).Enabled = False
            chkCheck1(4).Enabled = False
        Else
            chkCheck1(3).Enabled = True
            chkCheck1(4).Enabled = True
        End If
    Case 5:
        If chkCheck1(5).Value = 1 Then
            cboDBCombo1(1).Enabled = True
            objWinInfo.CtrlGetInfo(cboDBCombo1(1)).blnReadOnly = False
        Else
            cboDBCombo1(1).Enabled = False
            objWinInfo.CtrlGetInfo(cboDBCombo1(1)).blnReadOnly = True
        End If
    Case 12:
      If chkCheck1(12).Value = 1 Then
        If chkOMPRN(6).Value <> 0 Then
          chkOMPRN(6).Value = 0
        End If
        If txtText1(1).Text <> "" Then
          Call objWinInfo.CtrlSet(txtText1(1), "")
        End If
      Else
        If chkOMPRN(6).Value <> 1 Then
          chkOMPRN(6).Value = 1
        End If
        If txtText1(1).Text = "" Then
          Call objWinInfo.CtrlSet(txtText1(1), txtText1(0))
        End If
      End If
  End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  
  'If (cboDBCombo1(1).Text = "IV") Or (cboDBCombo1(1).Text = "PC") Or (cboDBCombo1(1).Text = "PIN") Then
  ' 'Hay que activar la velocidad de perfusi�n
  '  objWinInfo.CtrlGetInfo(txtText1(82)).blnReadOnly = False
  'Else
  ' 'Hay que desactivar la velocidad de perfusi�n
  '  objWinInfo.CtrlGetInfo(txtText1(82)).blnReadOnly = True
  'End If
End Sub

Private Sub txtHoras_Change()
  strOrigen = "T"
  If intCambioAlgo = 1 Then
    If txtHoras.Text <> "" And txtMinutos.Text <> "" Then
      If txtHoras.Text > 100 Then
        Beep
        txtHoras.Text = 100
      End If
      If IsNumeric(txtHoras.Text) And IsNumeric(txtMinutos.Text) Then
        'txtText1(43).Text = (txtHoras * 60) + txtMinutos
        Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60) + txtMinutos)
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    Else
      If txtHoras.Text <> "" Then
        If IsNumeric(txtHoras.Text) Then
          If txtHoras.Text > 100 Then
            Beep
            txtHoras.Text = 100
          End If
          Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60))
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      Else
        If IsNumeric(txtMinutos.Text) Then
          Call objWinInfo.CtrlSet(txtText1(43), txtMinutos)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If

End Sub

Private Sub txtHoras_KeyPress(KeyAscii As Integer)
    intCambioAlgo = 1
    strOrigen = "T"
End Sub

Private Sub txtMinutos_Change()
  
  strOrigen = "T"
  If intCambioAlgo = 1 Then
    If txtHoras.Text <> "" And txtMinutos.Text <> "" Then
      If txtMinutos.Text > 59 Then
        Beep
        txtMinutos.Text = 59
      End If
      If IsNumeric(txtHoras.Text) And IsNumeric(txtMinutos.Text) Then
        'txtText1(43).Text = (txtHoras * 60) + txtMinutos
        Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60) + txtMinutos)
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    Else
      If txtHoras.Text <> "" Then
        If IsNumeric(txtHoras.Text) Then
          Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60))
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      Else
        If IsNumeric(txtMinutos.Text) Then
          If txtMinutos.Text > 59 Then
            Beep
            txtMinutos.Text = 59
          End If
          Call objWinInfo.CtrlSet(txtText1(43), txtMinutos)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If

End Sub

Private Sub txtMinutos_KeyPress(KeyAscii As Integer)
    intCambioAlgo = 1
    strOrigen = "T"
End Sub


Private Sub txtText1_DblClick(Index As Integer)

  If objWinInfo.CtrlGetInfo(txtText1(Index)).blnForeign Then
    If stbStatusBar1.Panels(2).Text = "Lista" Then
      Call objWinInfo_cwForeign(objWinInfo.objWinActiveForm.strName, "txtText1(" & Index & ")")
    End If
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  intBotonMasInf = intIndex
  
  If intIndex = 51 Then
    If (tab2.Tab = 1) Or (tab2.Tab = 3) Then
      'cmdMed2.Top = cmdbuscarprod.Top
      If txtText1(27).Text <> "" Then
        'cmdMed2.Visible = True
        'cmdbuscarprod.Visible = False
      Else
        'cmdMed2.Visible = False
        'cmdbuscarprod.Visible = True
      End If
      'cmdSolDil.Visible = False
    End If
  ElseIf intIndex = 41 Then
    'cmdSolDil.Top = cmdbuscarprod.Top
    If tab2.Tab <> 3 Then
      If txtText1(27).Text <> "" Then
        'cmdSolDil.Visible = True
        'cmdbuscarprod.Visible = False
      Else
        'cmdSolDil.Visible = False
        'cmdbuscarprod.Visible = True
      End If
    End If
    'cmdMed2.Visible = False
    If tab2.Tab = 3 Then
      'cmdSolDil.Caption = "Soluci�n Intravenosa"
      'cmdSolDil.Visible = True
      'cmdbuscarprod.Visible = False
    Else
      'cmdSolDil.Caption = "Soluci�n para diluir"
    End If
  ElseIf intIndex = 55 Then
    If tab2.Tab <> 5 Then
      lblLabel1(16).Caption = "Medicamento No Formulario"
    Else
      lblLabel1(16).Caption = "F�rmula Magistral"
    End If
  ElseIf intIndex = 57 Then
    'cmdbuscarprod.Caption = "Form. Magistrales"
  Else
    'lblLabel1(16).Caption = "Medicamento"
    'cmdMed2.Visible = False
    If tab2.Tab = 3 And txtText1(40).Text = "" Then
      'cmdSolDil.Caption = "Soluci�n Intravenosa"
      'cmdSolDil.Top = cmdbuscarprod.Top
      'cmdSolDil.Visible = True
      'cmdbuscarprod.Visible = False
    Else
      'cmdSolDil.Visible = False
      'cmdbuscarprod.Visible = True
    End If
  End If
  'If intIndex = 41 Then
  '  Call objWinInfo_cwForeign("Detalle OM", "txtText1(41)")
  'End If
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)

  If Index = 32 Then
    If IsNumeric(txtText1(32).Text) Then
      objWinInfo.objWinActiveForm.blnChanged = True
      tlbToolbar1.Buttons(4).Enabled = True
    End If
  End If
  'ALTURA
  If Index = 33 Then
    If IsNumeric(txtText1(33).Text) Then
      objWinInfo.objWinActiveForm.blnChanged = True
      tlbToolbar1.Buttons(4).Enabled = True
    End If
  End If
  If (Index = 82) Then
    strOrigen = "V"
  End If
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
Dim rstDosis As rdoResultset
Dim strDosis As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
'Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
'Dim TiemMinInf As Currency
'Dim dblTiempo As Double


  Call objWinInfo.CtrlLostFocus
  If intIndex = 32 Then
    If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
        intPeso = txtText1(32).Text
        intAltura = txtText1(33).Text
        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
        txtText1(29).Text = vntMasa
    End If
  End If
  If intIndex = 33 Then
    If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
        intPeso = txtText1(32).Text
        intAltura = txtText1(33).Text
        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
        txtText1(29).Text = vntMasa
    End If
  End If
  
  If intIndex = 28 Then
    If mstrOperacion <> "F" Then
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(28).Text) Then
              Call objWinInfo.CtrlSet(txtText1(30), txtText1(28).Text * rstDosis("fr73dosis").Value)
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                Call objWinInfo.CtrlSet(txtText1(38), txtText1(28).Text * rstDosis("fr73volumen").Value)
              End If
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    Else 'I.V.
      If Trim(txtText1(40).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(40).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73volumen").Value) = False And IsNumeric(txtText1(28).Text) Then
              Call objWinInfo.CtrlSet(txtText1(42), txtText1(28).Text * rstDosis("fr73volumen").Value)
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    End If
  End If
  
  If intIndex = 30 Then
    If mstrOperacion <> "F" Then
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(30).Text) Then
              If rstDosis("fr73dosis").Value <> 0 Then
                Call objWinInfo.CtrlSet(txtText1(28), txtText1(30).Text / rstDosis("fr73dosis").Value)
              End If
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                If rstDosis("fr73dosis").Value <> 0 Then
                  Call objWinInfo.CtrlSet(txtText1(38), (txtText1(30).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value)
                End If
              End If
            End If
        End If
      End If
    Else 'I.V.
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(30).Text) Then
              'Call objWinInfo.CtrlSet(txtText1(28), txtText1(30).Text / rstDosis("fr73dosis").Value)
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                If rstDosis("fr73dosis").Value <> 0 Then
                  Call objWinInfo.CtrlSet(txtText1(38), (txtText1(30).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value)
                End If
              End If
            End If
        End If
      End If
    End If
  End If

  If intIndex = 50 Then
    If mstrOperacion = "M" Then
      If Trim(txtText1(52).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(52).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(50).Text) And IsNull(rstDosis("fr73volumen").Value) = False Then
              If rstDosis("fr73dosis").Value <> 0 And IsNumeric(txtText1(50).Text) Then
                Call objWinInfo.CtrlSet(txtText1(47), (txtText1(50).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value)
              End If
            End If
        End If
      End If
    End If
  End If
  
  If intIndex = 42 Then
    If mstrOperacion <> "F" Then
    Else 'I.V.
      If Trim(txtText1(40).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(40).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73volumen").Value) = False And IsNumeric(txtText1(28).Text) Then
              If rstDosis("fr73volumen").Value <> 0 And IsNumeric(txtText1(42).Text) Then
                Call objWinInfo.CtrlSet(txtText1(28), txtText1(42).Text / rstDosis("fr73volumen").Value)
              End If
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    End If
  End If
  
  
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim rstPotPeso As rdoResultset
Dim rstFactor As rdoResultset
Dim rstPotAlt As rdoResultset
Dim strPotPeso As String
Dim strPotAlt As String
Dim strFactor As String
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim rsta As rdoResultset
Dim stra As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim dblTiempo As Double

  Call objWinInfo.CtrlDataChange
  
  If intIndex = 43 Then
    If IsNumeric(txtText1(43).Text) Then
      If (intCambioAlgo = 0) And (strOrigen = "T") Then
        txtHoras.Text = txtText1(43).Text \ 60
        txtMinutos.Text = txtText1(43).Text Mod 60
      End If
    Else
        txtHoras.Text = ""
        txtMinutos.Text = ""
    End If
  End If
  
  If (intIndex = 82) And (strOrigen = "V") Then
    VolTotal = 0
    If txtText1(38).Text = "" Then
      Vol1 = 0
    Else
      Vol1 = CCur(txtText1(38).Text)
    End If
    If txtText1(42).Text = "" Then
      Vol2 = 0
    Else
      Vol2 = CCur(txtText1(42).Text)
    End If
    If txtText1(47).Text = "" Then
      Vol3 = 0
    Else
      Vol3 = CCur(txtText1(47).Text)
    End If
    VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
    'Se ha modificado la velocidad de perfusi�n
    If (VolTotal > 0) And IsNumeric(txtText1(82).Text) Then
      If txtText1(82).Text > 0 Then
        'Se puede calcular el tiempo Tiempo = VolTotal/ (VelPerf * 60)
        dblTiempo = (VolTotal / txtText1(82).Text) * 60
        txtHoras.Text = dblTiempo \ 60
        txtMinutos.Text = dblTiempo Mod 60
      End If
    Else
      'No hacer nada, de momento
    End If
  End If
  
  If intIndex = 27 Then
    If txtText1(27).Text = "999999999" Then
      lblLabel1(16).Caption = "Medicamento No Formulario"
      txtText1(55).Top = 240
      txtText1(55).Left = 0
      txtText1(55).Visible = True
    Else
      lblLabel1(16).Caption = "Medicamento"
      txtText1(55).Top = -1240
      txtText1(55).Left = 0
      txtText1(55).Visible = False
      If txtText1(35).Visible = True And txtText1(35).Enabled = True Then
        txtText1(35).SetFocus
      End If
    End If
  End If
   
  'PESO
  If intIndex = 32 Then
    If Not IsNumeric(txtText1(32).Text) Then
        txtText1(32).Text = ""
        txtText1(29).Text = ""
    Else
        If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
            strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
            Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
            strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
            Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
            strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
            Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
            intPeso = txtText1(32).Text
            intAltura = txtText1(33).Text
            vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
            vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
            vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
            txtText1(29).Text = vntMasa
            rstPotPeso.Close
            Set rstPotPeso = Nothing
            rstFactor.Close
            Set rstFactor = Nothing
            rstPotAlt.Close
            Set rstPotAlt = Nothing
        Else
            txtText1(29).Text = ""
        End If
    End If
  End If
  'ALTURA
  If intIndex = 33 Then
    If Not IsNumeric(txtText1(33).Text) Then
        txtText1(33).Text = ""
        txtText1(29).Text = ""
    Else
        If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
            strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
            Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
            strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
            Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
            strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
            Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
            intPeso = txtText1(32).Text
            intAltura = txtText1(33).Text
            vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
            vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
            vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
            txtText1(29).Text = vntMasa
            rstPotPeso.Close
            Set rstPotPeso = Nothing
            rstFactor.Close
            Set rstFactor = Nothing
            rstPotAlt.Close
            Set rstPotAlt = Nothing
        Else
            txtText1(29).Text = ""
        End If
    End If
  End If
  'HORA (frame Medicamentos)
  If intIndex = 36 Then
    If Not IsNumeric(txtText1(36).Text) Then
        txtText1(36).Text = ""
    Else
        If txtText1(36).Text > 24 Then
            txtText1(36).Text = ""
                Call MsgBox("Hora fuera de rango", vbCritical, "Aviso")
        End If
    End If
  End If

  Select Case intIndex
  Case 38, 42, 47, 43, 44 ', 82
    VolTotal = 0
    If txtText1(38).Text = "" Then
      Vol1 = 0
    Else
      Vol1 = CCur(txtText1(38).Text)
    End If
    If txtText1(42).Text = "" Then
      Vol2 = 0
    Else
      Vol2 = CCur(txtText1(42).Text)
    End If
    If txtText1(47).Text = "" Then
      Vol3 = 0
    Else
      Vol3 = CCur(txtText1(47).Text)
    End If
    VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
    If txtText1(43).Text = "" Then
      TiemMinInf = 0
    Else
      TiemMinInf = CCur(txtText1(43).Text) / 60
    End If
    If TiemMinInf <> 0 Then
      VolPerf = Format(VolTotal / TiemMinInf, "0.00")
    Else
      VolPerf = 0
    End If
    If txtText1(44).Text <> "" Then
      If VolTotal <> CCur(txtText1(44)) Then
        txtText1(44).Text = VolTotal
      End If
    Else
      If VolTotal <> 0 Then
        txtText1(44).Text = VolTotal
      End If
    End If
    If txtText1(82).Text <> "" Then
      If VolPerf <> CCur(txtText1(82)) Then
        txtText1(82).Text = VolPerf
      End If
    Else
      If VolPerf <> 0 Then
        txtText1(82).Text = VolPerf
      End If
    End If
  End Select
  
End Sub

Private Sub presentacion_pantalla()

  Select Case tab2.Tab
  Case 0, 2, 4
    lblLabel1(16).Top = 0 'Linea Medicamento....
    lblLabel1(16).Visible = True
    txtText1(13).Top = 240
    txtText1(13).Visible = True
    lblLabel1(37).Top = 0
    lblLabel1(37).Visible = True
    txtText1(35).Top = 240
    txtText1(35).Visible = True
    lblLabel1(19).Left = 6120
    lblLabel1(19).Top = 0 'dosis
    lblLabel1(19).Visible = True
    txtText1(30).Left = 6120
    txtText1(30).Top = 240
    txtText1(30).Visible = True
    lblLabel1(21).Left = 7080
    lblLabel1(21).Top = 0
    lblLabel1(21).Visible = True
    txtText1(31).Left = 7080
    txtText1(31).Top = 240
    txtText1(31).Visible = True
    lblLabel1(49).Top = 0
    lblLabel1(49).Visible = True
    txtText1(38).Top = 240
    txtText1(38).Visible = True
    
    lblLabel1(17).Top = 600 'Linea Cantidad...
    lblLabel1(17).Visible = True
    txtText1(28).Top = 840
    txtText1(28).Visible = True
    lblLabel1(23).Left = 960
    lblLabel1(23).Top = 600
    lblLabel1(23).Visible = True
    cboDBCombo1(0).Left = 960
    cboDBCombo1(0).Top = 840
    cboDBCombo1(0).Visible = True
    lblLabel1(25).Left = 2760
    lblLabel1(25).Top = 600
    lblLabel1(25).Visible = True
    cboDBCombo1(1).Left = 2760
    cboDBCombo1(1).Top = 840
    cboDBCombo1(1).Visible = True
    lblLabel1(29).Left = 3600
    lblLabel1(29).Top = 600
    lblLabel1(29).Visible = True
    cboDBCombo1(3).Left = 3600
    cboDBCombo1(3).Top = 840
    cboDBCombo1(3).Visible = True
    lblLabel1(33).Left = 5520
    lblLabel1(33).Top = 600
    lblLabel1(33).Visible = True
    dtcDateCombo1(3).Left = 5520
    dtcDateCombo1(3).Top = 840
    dtcDateCombo1(3).Visible = True
    lblLabel1(32).Left = 7440
    lblLabel1(32).Top = 600
    lblLabel1(32).Visible = True
    txtText1(37).Left = 7440
    txtText1(37).Top = 840
    txtText1(37).Visible = True
    
    lblLabel1(34).Top = 1200 'Linea Sol.Dil....
    lblLabel1(34).Caption = "Soluci�n para Diluir"
    lblLabel1(34).Visible = True
    txtText1(41).Top = 1440
    txtText1(41).Visible = True
    lblLabel1(35).Top = 1200
    lblLabel1(35).Visible = True
    txtText1(42).Top = 1440
    txtText1(42).Visible = True
    lblLabel1(36).Top = 1200
    lblLabel1(36).Visible = True
    txtHoras.Top = 1440
    txtHoras.Visible = True
    txtMinutos.Top = 1440
    txtMinutos.Visible = True
    
    lblLabel1(50).Top = 1800 'Linea Instr.Admin. y Vel.Perf....
    lblLabel1(50).Visible = True
    txtText1(39).Top = 2040
    txtText1(39).Visible = True
    lblLabel1(52).Top = 1800
    lblLabel1(52).Visible = True
    txtText1(82).Top = 2040
    txtText1(82).Visible = True
    'lblLabel1(62).Top = 1800
    'lblLabel1(62).Visible = True
    'txtText1(81).Top = 2040
    'txtText1(81).Visible = True
    lblLabel1(77).Top = 2080
    lblLabel1(77).Visible = True
    'txtText1(80).Top = 2040
    'txtText1(80).Visible = True
    lblLabel1(51).Top = 1800
    lblLabel1(51).Visible = True
    txtText1(44).Top = 2040
    txtText1(44).Visible = True
    
    lblLabel1(31).Left = 5400
    lblLabel1(31).Top = 2400 'Linea Fecha fin...
    lblLabel1(31).Visible = True
    dtcDateCombo1(4).Left = 5400
    dtcDateCombo1(4).Top = 2640
    dtcDateCombo1(4).Visible = True
    lblLabel1(30).Left = 7320
    lblLabel1(30).Top = 2400
    lblLabel1(30).Visible = True
    txtText1(36).Left = 7320
    txtText1(36).Top = 2640
    txtText1(36).Visible = True
    
    lblLabel1(79).Top = 3000 'Linea Prod2....
    'lblLabel1(79).Visible = False
    txtText1(52).Top = 3240
    'txtText1(52).Visible = False
    lblLabel1(83).Top = 3000
    lblLabel1(83).Visible = False
    txtText1(51).Top = 3240
    txtText1(51).Visible = False
    lblLabel1(80).Top = 3000
    lblLabel1(80).Visible = False
    txtText1(48).Top = 3240
    txtText1(48).Visible = False
    lblLabel1(82).Top = 3000
    lblLabel1(82).Visible = False
    txtText1(50).Top = 3240
    txtText1(50).Visible = False
    lblLabel1(81).Top = 3000
    lblLabel1(81).Visible = False
    txtText1(49).Top = 3240
    txtText1(49).Visible = False
    lblLabel1(78).Top = 3000
    lblLabel1(78).Visible = False
    txtText1(47).Top = 3240
    txtText1(47).Visible = False
    
    chkCheck1(3).Top = 800
    chkCheck1(3).Visible = True
    chkCheck1(4).Top = 1280
    chkCheck1(4).Visible = True
  
    lblLabel1(84).Visible = False
    txtText1(57).Visible = False
    cmdExplorador(1).Visible = False
    cmdWord.Visible = False
    
    If txtText1(27).Text = "999999999" Then
      txtText1(55).Visible = True
    Else
      txtText1(55).Visible = False
    End If
    
  Case 3 'Fluidoterapia
    lblLabel1(16).Top = 1200 'Linea Medicamento....
    lblLabel1(16).Visible = True
    txtText1(13).Top = 1440
    txtText1(13).Visible = True
    lblLabel1(37).Top = 1200
    lblLabel1(37).Visible = True
    txtText1(35).Top = 1440
    txtText1(35).Visible = True
    lblLabel1(19).Left = 6120
    lblLabel1(19).Top = 1200 'dosis
    lblLabel1(19).Visible = True
    txtText1(30).Left = 6120
    txtText1(30).Top = 1440
    txtText1(30).Visible = True
    lblLabel1(21).Left = 7080
    lblLabel1(21).Top = 1200
    lblLabel1(21).Visible = True
    txtText1(31).Left = 7080
    txtText1(31).Top = 1440
    txtText1(31).Visible = True
    lblLabel1(49).Top = 1200
    lblLabel1(49).Visible = True
    txtText1(38).Top = 1440
    txtText1(38).Visible = True
    
    lblLabel1(17).Top = 600 'Linea Cantidad...
    lblLabel1(17).Visible = True
    txtText1(28).Top = 840
    txtText1(28).Visible = True
    lblLabel1(23).Left = 960
    lblLabel1(23).Top = 600
    lblLabel1(23).Visible = True
    cboDBCombo1(0).Left = 960
    cboDBCombo1(0).Top = 840
    cboDBCombo1(0).Visible = True
    lblLabel1(25).Left = 2760
    lblLabel1(25).Top = 600
    lblLabel1(25).Visible = True
    cboDBCombo1(1).Left = 2760
    cboDBCombo1(1).Top = 840
    cboDBCombo1(1).Visible = True
    lblLabel1(29).Left = 3600
    lblLabel1(29).Top = 600
    lblLabel1(29).Visible = True
    cboDBCombo1(3).Left = 3600
    cboDBCombo1(3).Top = 840
    cboDBCombo1(3).Visible = True
    lblLabel1(33).Left = 5520
    lblLabel1(33).Top = 600
    lblLabel1(33).Visible = True
    dtcDateCombo1(3).Left = 5520
    dtcDateCombo1(3).Top = 840
    dtcDateCombo1(3).Visible = True
    lblLabel1(32).Left = 7440
    lblLabel1(32).Top = 600
    lblLabel1(32).Visible = True
    txtText1(37).Left = 7440
    txtText1(37).Top = 840
    txtText1(37).Visible = True
    
    lblLabel1(34).Top = 0 'Linea Sol.Dil....
    lblLabel1(34).Caption = "Soluci�n Intravenosa"
    lblLabel1(34).Visible = True
    txtText1(41).Top = 240
    txtText1(41).Visible = True
    lblLabel1(35).Top = 0
    lblLabel1(35).Visible = True
    txtText1(42).Top = 240
    txtText1(42).Visible = True
    lblLabel1(36).Top = 0
    lblLabel1(36).Visible = True
    txtHoras.Top = 240
    txtHoras.Visible = True
    txtMinutos.Top = 240
    txtMinutos.Visible = True
    
    'lblLabel1(50).Top = 1800 'Linea Instr.Admin. y Vel.Perf....
    'lblLabel1(50).Visible = True
    lblLabel1(50).Top = 2400 'Linea Instr.Admin. y Vel.Perf....
    lblLabel1(50).Visible = True
    'txtText1(39).Top = 2040
    'txtText1(39).Visible = True
    txtText1(39).Top = 2640
    txtText1(39).Visible = True

    'lblLabel1(52).Top = 1800
    'lblLabel1(52).Visible = True
    lblLabel1(52).Top = 2400
    lblLabel1(52).Visible = True
    'txtText1(82).Top = 2040
    'txtText1(82).Visible = True
    txtText1(82).Top = 2640
    txtText1(82).Visible = True

    'lblLabel1(62).Top = 1800
    'lblLabel1(62).Visible = True
    'txtText1(81).Top = 2040
    'txtText1(81).Visible = True
    'lblLabel1(77).Top = 2080
    'lblLabel1(77).Visible = True
    lblLabel1(77).Top = 2680
    lblLabel1(77).Visible = True
    'txtText1(80).Top = 2040
    'txtText1(80).Visible = True
    'lblLabel1(51).Top = 1800
    'lblLabel1(51).Visible = True
    lblLabel1(51).Top = 2400
    lblLabel1(51).Visible = True
    'txtText1(44).Top = 2040
    'txtText1(44).Visible = True
    txtText1(44).Top = 2640
    txtText1(44).Visible = True

    'lblLabel1(31).Left = 5400
    'lblLabel1(31).Top = 2400 'Linea Fecha fin...
    'lblLabel1(31).Visible = True
    lblLabel1(31).Left = 5400
    lblLabel1(31).Top = 3000 'Linea Fecha fin...
    lblLabel1(31).Visible = True
    'dtcDateCombo1(4).Left = 5400
    'dtcDateCombo1(4).Top = 2640
    'dtcDateCombo1(4).Visible = True
    dtcDateCombo1(4).Left = 5400
    dtcDateCombo1(4).Top = 3240
    dtcDateCombo1(4).Visible = True

    'lblLabel1(30).Left = 7320
    'lblLabel1(30).Top = 2400
    'lblLabel1(30).Visible = True
    lblLabel1(30).Left = 7320
    lblLabel1(30).Top = 3000
    lblLabel1(30).Visible = True
    'txtText1(36).Left = 7320
    'txtText1(36).Top = 2640
    'txtText1(36).Visible = True
    txtText1(36).Left = 7320
    txtText1(36).Top = 3240
    txtText1(36).Visible = True

    lblLabel1(79).Top = 3000 'Linea Prod2....
    'lblLabel1(79).Visible = False
    txtText1(52).Top = 3240
    'txtText1(52).Visible = False
    'lblLabel1(83).Top = 3000
    'lblLabel1(83).Visible = False
    lblLabel1(83).Top = 1800
    lblLabel1(83).Visible = True
    'txtText1(51).Top = 3240
    'txtText1(51).Visible = False
    txtText1(51).Top = 2040
    txtText1(51).Visible = True

    'lblLabel1(80).Top = 3000
    'lblLabel1(80).Visible = False
    lblLabel1(80).Top = 1800
    lblLabel1(80).Visible = True
    'txtText1(48).Top = 3240
    'txtText1(48).Visible = False
    txtText1(48).Top = 2040
    txtText1(48).Visible = True

    'lblLabel1(82).Top = 3000
    'lblLabel1(82).Visible = False
    lblLabel1(82).Top = 1800
    lblLabel1(82).Visible = True
    'txtText1(50).Top = 3240
    'txtText1(50).Visible = False
    txtText1(50).Top = 2040
    txtText1(50).Visible = True

    'lblLabel1(81).Top = 3000
    'lblLabel1(81).Visible = False
    lblLabel1(81).Top = 1800
    lblLabel1(81).Visible = True
    'txtText1(49).Top = 3240
    'txtText1(49).Visible = False
    txtText1(49).Top = 2040
    txtText1(49).Visible = True

    'lblLabel1(78).Top = 3000
    'lblLabel1(78).Visible = False
    lblLabel1(78).Top = 1800
    lblLabel1(78).Visible = True
    'txtText1(47).Top = 3240
    'txtText1(47).Visible = False
    txtText1(47).Top = 2040
    txtText1(47).Visible = True

    chkCheck1(3).Top = 400
    chkCheck1(3).Visible = True
    chkCheck1(4).Top = 880
    chkCheck1(4).Visible = True
  
    lblLabel1(84).Visible = False
    txtText1(57).Visible = False
    cmdExplorador(1).Visible = False
    cmdWord.Visible = False
    
    If txtText1(27).Text = "999999999" Then
      txtText1(55).Visible = True
    Else
      txtText1(55).Visible = False
    End If
    
  Case 1 'Mezcla IV 2M
    lblLabel1(16).Top = 0 'Linea Medicamento....
    lblLabel1(16).Visible = True
    txtText1(13).Top = 240
    txtText1(13).Visible = True
    lblLabel1(37).Top = 0
    lblLabel1(37).Visible = True
    txtText1(35).Top = 240
    txtText1(35).Visible = True
    lblLabel1(19).Left = 6120
    lblLabel1(19).Top = 0 'dosis
    lblLabel1(19).Visible = True
    txtText1(30).Left = 6120
    txtText1(30).Top = 240
    txtText1(30).Visible = True
    lblLabel1(21).Left = 7080
    lblLabel1(21).Top = 0
    lblLabel1(21).Visible = True
    txtText1(31).Left = 7080
    txtText1(31).Top = 240
    txtText1(31).Visible = True
    lblLabel1(49).Top = 0
    lblLabel1(49).Visible = True
    txtText1(38).Top = 240
    txtText1(38).Visible = True
    
    lblLabel1(17).Top = 600 'Linea Cantidad...
    lblLabel1(17).Visible = True
    txtText1(28).Top = 840
    txtText1(28).Visible = True
    lblLabel1(23).Left = 960
    lblLabel1(23).Top = 600
    lblLabel1(23).Visible = True
    cboDBCombo1(0).Left = 960
    cboDBCombo1(0).Top = 840
    cboDBCombo1(0).Visible = True
    lblLabel1(25).Left = 2760
    lblLabel1(25).Top = 600
    lblLabel1(25).Visible = True
    cboDBCombo1(1).Left = 2760
    cboDBCombo1(1).Top = 840
    cboDBCombo1(1).Visible = True
    lblLabel1(29).Left = 3600
    lblLabel1(29).Top = 600
    lblLabel1(29).Visible = True
    cboDBCombo1(3).Left = 3600
    cboDBCombo1(3).Top = 840
    cboDBCombo1(3).Visible = True
    lblLabel1(33).Left = 5520
    lblLabel1(33).Top = 600
    lblLabel1(33).Visible = True
    dtcDateCombo1(3).Left = 5520
    dtcDateCombo1(3).Top = 840
    dtcDateCombo1(3).Visible = True
    lblLabel1(32).Left = 7440
    lblLabel1(32).Top = 600
    lblLabel1(32).Visible = True
    txtText1(37).Left = 7440
    txtText1(37).Top = 840
    txtText1(37).Visible = True
    
    lblLabel1(34).Top = 1200 'Linea Sol.Dil....
    lblLabel1(34).Caption = "Soluci�n para Diluir"
    lblLabel1(34).Visible = True
    txtText1(41).Top = 1440
    txtText1(41).Visible = True
    lblLabel1(35).Top = 1200
    lblLabel1(35).Visible = True
    txtText1(42).Top = 1440
    txtText1(42).Visible = True
    lblLabel1(36).Top = 1200
    lblLabel1(36).Visible = True
    txtHoras.Top = 1440
    txtHoras.Visible = True
    txtMinutos.Top = 1440
    txtMinutos.Visible = True
    
    lblLabel1(50).Top = 2400 'Linea Instr.Admin. y Vel.Perf....
    lblLabel1(50).Visible = True
    txtText1(39).Top = 2640
    txtText1(39).Visible = True
    lblLabel1(52).Top = 2400
    lblLabel1(52).Visible = True
    txtText1(82).Top = 2640
    txtText1(82).Visible = True
    'lblLabel1(62).Top = 2400
    'lblLabel1(62).Visible = True
    'txtText1(81).Top = 2640
    'txtText1(81).Visible = True
    lblLabel1(77).Top = 2680
    lblLabel1(77).Visible = True
    'txtText1(80).Top = 2640
    'txtText1(80).Visible = True
    lblLabel1(51).Top = 2400
    lblLabel1(51).Visible = True
    txtText1(44).Top = 2640
    txtText1(44).Visible = True
    
    lblLabel1(31).Left = 5400
    lblLabel1(31).Top = 3000 'Linea Fecha fin...
    lblLabel1(31).Visible = True
    dtcDateCombo1(4).Left = 5400
    dtcDateCombo1(4).Top = 3240
    dtcDateCombo1(4).Visible = True
    lblLabel1(30).Left = 7320
    lblLabel1(30).Top = 3000
    lblLabel1(30).Visible = True
    txtText1(36).Left = 7320
    txtText1(36).Top = 3240
    txtText1(36).Visible = True
    
    'lblLabel1(79).Top = 1800
    'lblLabel1(79).Visible = True
    'txtText1(52).Top = 2040
    'txtText1(52).Visible = True
    lblLabel1(83).Top = 1800 'Linea Prod2....
    lblLabel1(83).Visible = True
    txtText1(51).Top = 2040
    txtText1(51).Visible = True
    lblLabel1(80).Top = 1800
    lblLabel1(80).Visible = True
    txtText1(48).Top = 2040
    txtText1(48).Visible = True
    lblLabel1(82).Top = 1800
    lblLabel1(82).Visible = True
    txtText1(50).Top = 2040
    txtText1(50).Visible = True
    lblLabel1(81).Top = 1800
    lblLabel1(81).Visible = True
    txtText1(49).Top = 2040
    txtText1(49).Visible = True
    lblLabel1(78).Top = 1800
    lblLabel1(78).Visible = True
    txtText1(47).Top = 2040
    txtText1(47).Visible = True
    
    chkCheck1(3).Top = 800
    chkCheck1(3).Visible = True
    chkCheck1(4).Top = 1280
    chkCheck1(4).Visible = True
  
    lblLabel1(84).Visible = False
    txtText1(57).Visible = False
    cmdExplorador(1).Visible = False
    cmdWord.Visible = False
  
    If txtText1(27).Text = "999999999" Then
      txtText1(55).Visible = True
    Else
      txtText1(55).Visible = False
    End If
  
  Case 5 'Form.Magistral
    lblLabel1(84).Left = 0
    lblLabel1(84).Top = 1200
    lblLabel1(84).Visible = True
    txtText1(57).Left = 0
    txtText1(57).Top = 1440
    txtText1(57).Visible = True
    cmdExplorador(1).Left = 4920
    cmdExplorador(1).Top = 1240
    cmdExplorador(1).Visible = True
    cmdWord.Left = 5520
    cmdWord.Top = 1240
    cmdWord.Visible = True
    
    'lblLabel1(16).Visible = False 'Linea Medicamento....
    txtText1(13).Visible = False
    lblLabel1(37).Visible = False
    txtText1(35).Visible = False
    lblLabel1(19).Top = 600
    lblLabel1(19).Left = 0
    lblLabel1(19).Visible = True 'dosis
    txtText1(30).Top = 840
    txtText1(30).Left = 0
    txtText1(30).Visible = True
    lblLabel1(21).Top = 600
    lblLabel1(21).Left = 1000
    lblLabel1(21).Visible = True
    txtText1(31).Top = 840
    txtText1(31).Left = 1000
    txtText1(31).Visible = True
    lblLabel1(49).Visible = False
    txtText1(38).Visible = False
    
    lblLabel1(17).Visible = False 'Linea Cantidad...
    txtText1(28).Visible = False
    lblLabel1(23).Left = 1960
    lblLabel1(23).Visible = True
    cboDBCombo1(0).Left = 1960
    cboDBCombo1(0).Visible = True
    lblLabel1(25).Left = 3760
    lblLabel1(25).Visible = True
    cboDBCombo1(1).Left = 3760
    cboDBCombo1(1).Visible = True
    lblLabel1(29).Left = 4600
    lblLabel1(29).Visible = True
    cboDBCombo1(3).Left = 4600
    cboDBCombo1(3).Visible = True
    
    lblLabel1(33).Left = 5520
    lblLabel1(33).Top = 0 'Linea Fecha Inicio...
    lblLabel1(33).Visible = True
    dtcDateCombo1(3).Left = 5520
    dtcDateCombo1(3).Top = 240
    dtcDateCombo1(3).Visible = True
    lblLabel1(32).Left = 7440
    lblLabel1(32).Top = 0
    lblLabel1(32).Visible = True
    txtText1(37).Left = 7440
    txtText1(37).Top = 240
    txtText1(37).Visible = True
    
    lblLabel1(34).Visible = False 'Linea Sol.Dil....
    txtText1(41).Visible = False
    lblLabel1(35).Visible = False
    txtText1(42).Visible = False
    lblLabel1(36).Visible = False
    txtHoras.Visible = False
    txtMinutos.Visible = False
    
    lblLabel1(50).Top = 2400 'Linea Instr.Admin. y Vel.Perf....
    lblLabel1(50).Visible = True
    txtText1(39).Top = 2640
    txtText1(39).Visible = True
    lblLabel1(52).Visible = False
    txtText1(82).Visible = False
    'lblLabel1(62).Visible = False
    'txtText1(81).Visible = False
    lblLabel1(77).Visible = False
    'txtText1(80).Visible = False
    lblLabel1(51).Visible = False
    txtText1(44).Visible = False
    
    lblLabel1(31).Left = 0
    lblLabel1(31).Top = 1800 'Linea Fecha fin...
    lblLabel1(31).Visible = True
    dtcDateCombo1(4).Left = 0
    dtcDateCombo1(4).Top = 2040
    dtcDateCombo1(4).Visible = True
    lblLabel1(30).Left = 1920
    lblLabel1(30).Top = 1800
    lblLabel1(30).Visible = True
    txtText1(36).Left = 1920
    txtText1(36).Top = 2040
    txtText1(36).Visible = True
    
    'lblLabel1(79).Top = 1800
    'lblLabel1(79).Visible = False
    'txtText1(52).Top = 2040
    'txtText1(52).Visible = False
    lblLabel1(83).Visible = False 'Linea Prod2....
    txtText1(51).Visible = False
    lblLabel1(80).Visible = False
    txtText1(48).Visible = False
    lblLabel1(82).Visible = False
    txtText1(50).Visible = False
    lblLabel1(81).Visible = False
    txtText1(49).Visible = False
    lblLabel1(78).Visible = False
    txtText1(47).Visible = False
    
    chkCheck1(3).Top = 800
    chkCheck1(3).Visible = True
    chkCheck1(4).Visible = False

    txtText1(55).Top = 240
    txtText1(55).Left = 0
    txtText1(55).Visible = True
    lblLabel1(16).Top = 0
    lblLabel1(16).Caption = "F�rmula Magistral"
  End Select

  Select Case tab2.Tab 'Campos Obligatorios
  Case 0
    txtText1(55).BackColor = txtText1(37).BackColor 'obligatorio
    txtText1(35).BackColor = txtText1(37).BackColor
    txtText1(30).BackColor = txtText1(37).BackColor
    txtText1(31).BackColor = txtText1(37).BackColor
    txtText1(28).BackColor = txtText1(37).BackColor
    cboDBCombo1(0).BackColor = txtText1(37).BackColor
    cboDBCombo1(1).BackColor = txtText1(37).BackColor
    cboDBCombo1(3).BackColor = txtText1(37).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = vbWhite
  Case 1
    txtText1(55).BackColor = vbWhite
    txtText1(35).BackColor = txtText1(37).BackColor
    txtText1(30).BackColor = txtText1(37).BackColor
    txtText1(31).BackColor = txtText1(37).BackColor
    txtText1(28).BackColor = txtText1(37).BackColor
    cboDBCombo1(0).BackColor = txtText1(37).BackColor
    cboDBCombo1(1).BackColor = txtText1(37).BackColor
    cboDBCombo1(3).BackColor = txtText1(37).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = vbWhite
  Case 2
    txtText1(55).BackColor = vbWhite
    txtText1(35).BackColor = txtText1(37).BackColor
    txtText1(30).BackColor = txtText1(37).BackColor
    txtText1(31).BackColor = txtText1(37).BackColor
    txtText1(28).BackColor = txtText1(37).BackColor
    cboDBCombo1(0).BackColor = txtText1(37).BackColor
    cboDBCombo1(1).BackColor = txtText1(37).BackColor
    cboDBCombo1(3).BackColor = txtText1(37).BackColor
    txtText1(39).BackColor = txtText1(37).BackColor
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = vbWhite
  Case 3
    txtText1(55).BackColor = vbWhite
    txtText1(35).BackColor = vbWhite
    txtText1(30).BackColor = vbWhite
    txtText1(31).BackColor = vbWhite
    txtText1(28).BackColor = txtText1(37).BackColor
    cboDBCombo1(0).BackColor = txtText1(37).BackColor
    cboDBCombo1(1).BackColor = txtText1(37).BackColor
    cboDBCombo1(3).BackColor = txtText1(37).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = txtText1(37).BackColor
    txtHoras.BackColor = txtText1(37).BackColor
    txtMinutos.BackColor = txtText1(37).BackColor
    txtText1(57).BackColor = vbWhite
  Case 4
    txtText1(55).BackColor = vbWhite
    txtText1(35).BackColor = txtText1(37).BackColor
    txtText1(30).BackColor = txtText1(37).BackColor
    txtText1(31).BackColor = txtText1(37).BackColor
    txtText1(28).BackColor = txtText1(37).BackColor
    cboDBCombo1(0).BackColor = txtText1(37).BackColor
    cboDBCombo1(1).BackColor = txtText1(37).BackColor
    cboDBCombo1(3).BackColor = txtText1(37).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = vbWhite
  Case 5
    txtText1(55).BackColor = txtText1(37).BackColor
    txtText1(35).BackColor = vbWhite
    txtText1(30).BackColor = txtText1(37).BackColor
    txtText1(31).BackColor = txtText1(37).BackColor
    txtText1(28).BackColor = vbWhite
    cboDBCombo1(0).BackColor = txtText1(37).BackColor
    cboDBCombo1(1).BackColor = txtText1(37).BackColor
    cboDBCombo1(3).BackColor = txtText1(37).BackColor
    txtText1(39).BackColor = vbWhite
    txtText1(42).BackColor = vbWhite
    txtHoras.BackColor = vbWhite
    txtMinutos.BackColor = vbWhite
    txtText1(57).BackColor = txtText1(37).BackColor
  End Select

  Select Case tab2.Tab
  Case 0
    'cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = False
    mstrOperacion = "/" 'Medicamento
    If txtText1(55).Visible = True Then
      lblLabel1(16).Caption = "Medicamento No Formulario"
    Else
      lblLabel1(16).Caption = "Medicamento"
    End If
  Case 1
    'cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = True
    mstrOperacion = "M" 'Mezcla IV 2M
    lblLabel1(16).Caption = "Medicamento"
  Case 2
    'cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = False
    mstrOperacion = "P" 'PRN en OM
    lblLabel1(16).Caption = "Medicamento"
  Case 3
    'cmdbuscarprod.Caption = "Medicamentos"
    'cmdMed2.Visible = False
    mstrOperacion = "F" 'Fluidoterapia
    lblLabel1(34).Caption = "Electrolito"
    lblLabel1(16).Caption = "Electrolito 2"
  Case 4
    'cmdbuscarprod.Caption = "Estupefacientes"
    'cmdMed2.Visible = False
    mstrOperacion = "E" 'Estupefaciente
    lblLabel1(16).Caption = "Estupefaciente"
  Case 5
    'cmdbuscarprod.Caption = "Form. Magistrales"
    'cmdMed2.Visible = False
    mstrOperacion = "L" 'Form.Magistral
    lblLabel1(16).Caption = "F�rmula Magistral"
  End Select
  
  If tabTab1(1).Tab = 0 Then
    'botones habilitados
    If tab2.Tab = 0 Or tab2.Tab = 1 Or tab2.Tab = 2 Then
      'cmdbuscarprod.Enabled = True
      'cmdbuscarprot.Enabled = True
      'cmdbuscargruprot.Enabled = True
      'cmdbuscargruprod.Enabled = True
      'cmdMedNoForm.Enabled = True
      'cmdSolDil.Enabled = True
      'cmdMed2.Enabled = True
      'cmdInformacion.Enabled = True
      'cmdInstAdm.Enabled = True
    Else
      'cmdbuscarprod.Enabled = True
      'cmdbuscarprot.Enabled = True
      'cmdbuscargruprot.Enabled = False
      'cmdbuscargruprod.Enabled = False
      'cmdMedNoForm.Enabled = True
      'cmdSolDil.Enabled = True
      'cmdMed2.Enabled = True
      'cmdInstAdm.Enabled = True
      If tab2.Tab <> 5 Then
      '  cmdInformacion.Enabled = True
      Else
      '  cmdInformacion.Enabled = False
      End If
    End If
  Else
    'botones deshabilitados
    'cmdbuscarprod.Enabled = False
    'cmdbuscarprot.Enabled = False
    'cmdbuscargruprot.Enabled = False
    'cmdbuscargruprod.Enabled = False
    'cmdMedNoForm.Enabled = False
    'cmdSolDil.Enabled = False
    'cmdMed2.Enabled = False
    'cmdInformacion.Enabled = False
    'cmdInstAdm.Enabled = False
  End If

End Sub


Private Sub Agenda(strdias As String)
Dim intPos As Integer

  intNumDia = 1
  While strdias <> ""
    intPos = InStr(1, strdias, ",")
    If intPos <> 0 Then
      intDia(intNumDia) = Val(Left(strdias, intPos - 1))
      intNumDia = intNumDia + 1
    Else
      intDia(intNumDia) = Val(strdias)
      intNumDia = intNumDia + 1
    End If
    intPos = InStr(1, strdias, ",")
    If intPos <> 0 Then
      strdias = Right(strdias, Len(strdias) - intPos)
    Else
      strdias = ""
    End If
  Wend

End Sub

Private Sub insertar(intsuma As Integer, strCodPet As String, strNumLin As String, _
                     strFR73CODPRODUCTO As String, strFR28DOSIS As String, strFRG4CODFRECUENCIA As String, _
                     strFR93CODUNIMEDIDA As String, strFR34CODVIA As String, strFRH7CODFORMFAR As String, _
                     strFR73CODPRODUCTO_DIL As String, strFR28OPERACION As String, strFR28HORAINICIO As String, _
                     strFR28INDDISPPRN As String, strFR28INDCOMIENINMED As String, strFR28INDPERF As String, _
                     strFR28CANTIDAD As String, strFR28CANTIDADDIL As String, strFR28TIEMMININF As String, _
                     strFR28INDVIAOPC As String, strFR28REFERENCIA As String, strFR28UBICACION As String, _
                     strFR28VELPERFUSION As String, strFR28INSTRADMIN As String, strFR28VOLTOTAL As String, _
                     strFR73CODPRODUCTO_2 As String, strFRH7CODFORMFAR_2 As String, strFR28DOSIS_2 As String, _
                     strFR93CODUNIMEDIDA_2 As String, strFR28VOLUMEN_2 As String, strFR28DESPRODUCTO As String, _
                     strFR28PATHFORMAG As String, strFR28VOLUMEN As String)
  Dim strlinea As String
  Dim rstlinea As rdoResultset
  Dim strInsert As String
  Dim dtfecha As Date
  Dim linea As Integer
  Dim lineaactual As Integer
  Dim lineaaux As Integer
  Dim lineaact As Integer
  Dim strDelete As String
    
  lineaactual = strNumLin
  lineaaux = strNumLin
  lineaact = lineaactual
  
  While lineaact <= lineaaux
    strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
    strCodPet
    Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
    
    If IsNull(rstlinea.rdoColumns(0).Value) Then
      linea = 1
    Else
      linea = rstlinea.rdoColumns(0).Value + 1
    End If
    dtfecha = DateAdd("d", intsuma, Date)

    strInsert = "INSERT INTO FR2800 ("
    strInsert = strInsert & "FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO,"
    strInsert = strInsert & "FR28DOSIS,FRG4CODFRECUENCIA,FR93CODUNIMEDIDA,FR34CODVIA,"
    strInsert = strInsert & "FRH7CODFORMFAR,FR73CODPRODUCTO_DIL,FR28OPERACION,"
    strInsert = strInsert & "FR28FECINICIO,FR28HORAINICIO,"
    strInsert = strInsert & "FR28INDDISPPRN,FR28INDCOMIENINMED,FRH5CODPERIODICIDAD,"
    strInsert = strInsert & "FR28INDSN,FR28CANTIDAD,FR28CANTDISP,FR28FECFIN,FR28HORAFIN,"
    strInsert = strInsert & "FR28CANTIDADDIL,FR28TIEMMININF,FR28INDVIAOPC"
    strInsert = strInsert & ",FR28REFERENCIA,FR28UBICACION,FR28VELPERFUSION"
    'strinsert = strinsert & ",FR93CODUNIMEDIDA_PERF1,FR93CODUNIMEDIDA_PERF2
    strInsert = strInsert & ",FR28VOLUMEN,FR28INDPERF"
    strInsert = strInsert & ",FR28INSTRADMIN,FR28VOLTOTAL,FR73CODPRODUCTO_2"
    strInsert = strInsert & ",FRH7CODFORMFAR_2,FR28DOSIS_2,FR93CODUNIMEDIDA_2,FR28VOLUMEN_2"
    strInsert = strInsert & ",FR28DESPRODUCTO,FR28PATHFORMAG"
    strInsert = strInsert & ") VALUES " & _
      "(" & _
      strCodPet & "," & _
      linea & ","
      strInsert = strInsert & strFR73CODPRODUCTO & ","
      vntAux = objGen.ReplaceStr(strFR28DOSIS, ",", ".", 1)
      strInsert = strInsert & vntAux & ","
      strInsert = strInsert & strFRG4CODFRECUENCIA & ","
      strInsert = strInsert & strFR93CODUNIMEDIDA & ","
      strInsert = strInsert & strFR34CODVIA & ","
      strInsert = strInsert & strFRH7CODFORMFAR & ","
      strInsert = strInsert & strFR73CODPRODUCTO_DIL & ","
      strInsert = strInsert & strFR28OPERACION & ",to_date('" & dtfecha & "','dd/mm/yyyy'),"
      strInsert = strInsert & strFR28HORAINICIO & ","
      strInsert = strInsert & strFR28INDDISPPRN & ","
      strInsert = strInsert & strFR28INDCOMIENINMED & ",'Diario',"
      strInsert = strInsert & strFR28INDPERF & ","
      If UCase(strFR28CANTIDAD) = "NULL" Then 'FR28CANTIDAD,FR28CANTDISP
        strInsert = strInsert & "null,null,"
      Else
        vntAux = objGen.ReplaceStr(strFR28CANTIDAD, ",", ".", 1)
        strInsert = strInsert & vntAux & "," & vntAux & ","
      End If
      strInsert = strInsert & "to_date('" & dtfecha & "','dd/mm/yyyy'),"
      strInsert = strInsert & strFR28HORAINICIO & ","
      strInsert = strInsert & strFR28CANTIDADDIL & ","
      strInsert = strInsert & strFR28TIEMMININF & ","
      strInsert = strInsert & strFR28INDVIAOPC & ","
      strInsert = strInsert & strFR28REFERENCIA & ","
      strInsert = strInsert & strFR28UBICACION & ","
      vntAux = objGen.ReplaceStr(strFR28VELPERFUSION, ",", ".", 1)
      strInsert = strInsert & vntAux & ","
      vntAux = objGen.ReplaceStr(strFR28VOLUMEN, ",", ".", 1)
      strInsert = strInsert & vntAux & ","
      strInsert = strInsert & strFR28INDPERF & ","
      strInsert = strInsert & strFR28INSTRADMIN & ","
      vntAux = objGen.ReplaceStr(strFR28VOLTOTAL, ",", ".", 1)
      strInsert = strInsert & vntAux & ","
      strInsert = strInsert & strFR73CODPRODUCTO_2 & ","
      strInsert = strInsert & strFRH7CODFORMFAR_2 & ","
      vntAux = objGen.ReplaceStr(strFR28DOSIS_2, ",", ".", 1)
      strInsert = strInsert & vntAux & ","
      strInsert = strInsert & strFR93CODUNIMEDIDA_2 & ","
      vntAux = objGen.ReplaceStr(strFR28VOLUMEN_2, ",", ".", 1)
      strInsert = strInsert & vntAux & ","
      strInsert = strInsert & strFR28DESPRODUCTO & ","
      strInsert = strInsert & strFR28PATHFORMAG & ")"
    
    
    objApp.rdoConnect.Execute strInsert, 64
    objApp.rdoConnect.Execute "Commit", 64
  
    rstlinea.Close
    Set rstlinea = Nothing
  
    lineaact = lineaact + 1
    If lineaaux <> lineaactual Then
      objWinInfo.DataMoveNext
    End If
  Wend
  
  If lineaactual = lineaaux Then
    strDelete = "delete fr2800 where FR66CODPETICION=" & strCodPet & _
      " AND FR28NUMLINEA=" & lineaactual
    objApp.rdoConnect.Execute strDelete, 64
    objApp.rdoConnect.Execute "Commit", 64
  Else
    While strNumLin <> lineaactual
      objWinInfo.DataMovePrevious
      strDelete = "delete fr2800 where FR66CODPETICION=" & strCodPet & _
        " AND FR28NUMLINEA=" & strNumLin
      objApp.rdoConnect.Execute strDelete, 64
      objApp.rdoConnect.Execute "Commit", 64
    Wend
  End If

End Sub

Private Sub periodicidad()
'Dim fecha As Date
'Dim i As Integer
'Dim dia As String
'Dim mes As String
'
'
'  Frame2.Visible = True
'  Call Frame2.ZOrder(0)
'  cmdsitcarro.Enabled = False
'  cmdfirmar.Enabled = False
'  cmdfirmarenviar.Enabled = False
'  cmdenviar.Enabled = False
'  cmdbuscargruprot.Enabled = False
'  cmdbuscarprot.Enabled = False
'  cmdbuscargruprod.Enabled = False
'  cmdbuscarprod.Enabled = False
'  cmdperfil.Enabled = False
'  cmdHCpaciente.Enabled = False
'  cmdAlergia.Enabled = False
'  fraFrame1(0).Enabled = False
'  fraFrame1(1).Enabled = False
'  tlbToolbar1.Enabled = False
'
'  For i = 0 To 29
'    fecha = DateAdd("d", i, Date)
'    dia = DatePart("d", fecha)
'    mes = DatePart("m", fecha)
'    Label2(i).Caption = dia & "/" & mes
'  Next i'
'
End Sub

Private Function Peticion_Correcta() As Boolean
Dim strselect As String
Dim rstselect As rdoResultset
Dim strOperacion As String
Dim blnIncorrecta As Boolean
Dim IntLinea As Integer
Dim mensaje As String
    
  If txtText1(0).Text = "" Then
    mensaje = MsgBox("No hay Orden M�dica para firmar.", vbInformation, "Aviso")
    Peticion_Correcta = False
    Exit Function
  End If
    
  IntLinea = 0
  blnIncorrecta = False
  strselect = "SELECT * FROM FR2800 "
  strselect = strselect & " WHERE FR66CODPETICION=" & txtText1(0).Text
  Set rstselect = objApp.rdoConnect.OpenResultset(strselect)
  While Not rstselect.EOF And blnIncorrecta = False
    IntLinea = IntLinea + 1
    Select Case rstselect("FR28OPERACION").Value 'Campos Obligatorios
    Case "/" 'Medicamento
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR73CODPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
      Else
        If rstselect("FR73CODPRODUCTO").Value = 999999999 And IsNull(rstselect("FR28DESPRODUCTO")) Then
          blnIncorrecta = True  '("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If IsNull(rstselect("FRH7CODFORMFAR")) Then
        blnIncorrecta = True  '("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DOSIS")) Then
        blnIncorrecta = True  '("El campo Dosis es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
        blnIncorrecta = True  '("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDAD")) Then
        blnIncorrecta = True  '("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO_DIL")) Then
        If IsNull(rstselect("FR28CANTIDADDIL")) Then
          blnIncorrecta = True  '("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28TIEMMININF")) Then
          blnIncorrecta = True  '("El campo Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
      End If
    Case "M" 'Mezcla IV 2M
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR73CODPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
      Else
        If rstselect("FR73CODPRODUCTO").Value = 999999999 And IsNull(rstselect("FR28DESPRODUCTO")) Then
          blnIncorrecta = True  '("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If IsNull(rstselect("FRH7CODFORMFAR")) Then
        blnIncorrecta = True  '("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DOSIS")) Then
        blnIncorrecta = True  '("El campo Dosis es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
        blnIncorrecta = True  '("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDAD")) Then
        blnIncorrecta = True  '("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO_DIL")) Then
        If IsNull(rstselect("FR28CANTIDADDIL")) Then
          blnIncorrecta = True  '("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28TIEMMININF")) Then
          blnIncorrecta = True  '("El campo Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO_2")) Then
        If IsNull(rstselect("FRH7CODFORMFAR_2")) Then
          blnIncorrecta = True  '("El campo Forma Farmace�tica del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28DOSIS_2")) Then
          blnIncorrecta = True  '("El campo Dosis del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR93CODUNIMEDIDA_2")) Then
          blnIncorrecta = True  '("El campo Unidad de Medida del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
        End If
      End If
    Case "P" 'PRN en OM
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR73CODPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
      Else
        If rstselect("FR73CODPRODUCTO").Value = 999999999 And IsNull(rstselect("FR28DESPRODUCTO")) Then
          blnIncorrecta = True  '("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If IsNull(rstselect("FRH7CODFORMFAR")) Then
        blnIncorrecta = True  '("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DOSIS")) Then
        blnIncorrecta = True  '("El campo Dosis es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
        blnIncorrecta = True  '("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDAD")) Then
        blnIncorrecta = True  '("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO_DIL")) Then
        If IsNull(rstselect("FR28CANTIDADDIL")) Then
          blnIncorrecta = True  '("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28TIEMMININF")) Then
          blnIncorrecta = True  '("El campo Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If IsNull(rstselect("FR28INSTRADMIN")) Then
        blnIncorrecta = True  '("Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n.", vbInformation, "Aviso")
      End If
    Case "F" 'Fluidoterapia
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR73CODPRODUCTO_DIL")) Then
        blnIncorrecta = True  '("El campo Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDADDIL")) Then
        blnIncorrecta = True  '("El campo Volumen de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28TIEMMININF")) Then
        blnIncorrecta = True  '("El campo Tiempo de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDAD")) Then
        blnIncorrecta = True  '("El campo Cantidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO")) Then
        If IsNull(rstselect("FRH7CODFORMFAR")) Then
          blnIncorrecta = True  '("El campo Forma Farmace�tica del Medicamento es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28DOSIS")) Then
          blnIncorrecta = True  '("El campo Dosis del Medicamento es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
          blnIncorrecta = True  '("El campo Unidad de Medida del Medicamento es obligatorio.", vbInformation, "Aviso")
        End If
      End If
    Case "E" 'Estupefaciente
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR73CODPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Estupefaciente es obligatorio.", vbInformation, "Aviso")
      Else
        If rstselect("FR73CODPRODUCTO").Value = 999999999 And IsNull(rstselect("FR28DESPRODUCTO")) Then
          blnIncorrecta = True  '("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
        End If
      End If
      If IsNull(rstselect("FRH7CODFORMFAR")) Then
        blnIncorrecta = True  '("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DOSIS")) Then
        blnIncorrecta = True  '("El campo Dosis es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
        blnIncorrecta = True  '("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28CANTIDAD")) Then
        blnIncorrecta = True  '("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
      End If
      If Not IsNull(rstselect("FR73CODPRODUCTO_DIL")) Then
        If IsNull(rstselect("FR28CANTIDADDIL")) Then
          blnIncorrecta = True  '("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
        If IsNull(rstselect("FR28TIEMMININF")) Then
          blnIncorrecta = True  '("El campo Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
        End If
      End If
    Case "L" 'Form.Magistral
      If IsNull(rstselect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DESPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Descripci�n de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28DOSIS")) Then
        blnIncorrecta = True  '("El campo Dosis es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR93CODUNIMEDIDA")) Then
        blnIncorrecta = True  '("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstselect("FR28PATHFORMAG")) Then
        blnIncorrecta = True  '("El campo Archivo Inf. de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
      End If
    End Select
    rstselect.MoveNext
  Wend
  
  If IntLinea = 0 Then
    mensaje = MsgBox("La Orden M�dica no tiene Medicamentos.", vbCritical, "Error")
    Peticion_Correcta = False
  ElseIf blnIncorrecta = False Then
    Peticion_Correcta = True
  Else
    mensaje = MsgBox("La linea : " & IntLinea & " de la Orden M�dica tiene campos obligatorios no informados.", vbCritical, "Error")
    Peticion_Correcta = False
  End If
  
  rstselect.Close
  Set rstselect = Nothing

End Function

Private Function Campos_Obligatorios() As Boolean
Dim blnIncorrecta As Boolean
Dim mensaje As String
Dim auxControl As Control
    
  mensaje = ""
  blnIncorrecta = False
  Select Case txtText1(53).Text 'Campos Obligatorios
  Case "/" 'Medicamento
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Medicamento es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
  Case "M" 'Mezcla IV 2M
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Medicamento es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
    If txtText1(52).Text <> "" Then
      If txtText1(48).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(48)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica del Medicamento 2 es obligatorio."
      End If
      If txtText1(50).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(50)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Dosis del Medicamento 2 es obligatorio."
      End If
      If txtText1(49).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(49)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Unidad de Medida del Medicamento 2 es obligatorio."
      End If
    End If
  Case "P" 'PRN en OM
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Medicamento es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
    If txtText1(39).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(39)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n."
    End If
  Case "F" 'Fluidoterapia
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If txtText1(40).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(42)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Soluci�n Intravenosa es obligatorio."
    End If
    If txtText1(42).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(42)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n Intravenosa es obligatorio."
    End If
    If txtHoras.Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtHoras
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n Intravenosa es obligatorio."
    End If
    If txtMinutos.Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtMinutos
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n Intravenosa es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad de la Soluci�n Intravenosa es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia de la Soluci�n Intravenosa es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a de la Soluci�n Intravenosa es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad de la Soluci�n Intravenosa es obligatorio."
    End If
    If txtText1(27).Text <> "" Then
      If txtText1(35).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(35)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica del Medicamento es obligatorio."
      End If
      If txtText1(30).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(30)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Dosis del Medicamento es obligatorio."
      End If
      If txtText1(31).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(31)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Unidad de Medida del Medicamento es obligatorio."
      End If
    End If
  Case "E" 'Estupefaciente
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Estupefaciente es obligatorio."
    Else
      If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(55)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Descripci�n Medicamento No Formulario es obligatorio."
      End If
    End If
    If txtText1(35).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(35)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Forma Farmace�tica es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If txtText1(28).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(28)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Cantidad es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If txtText1(40).Text <> "" Then
      If txtText1(42).Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(42)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Volumen de la Soluci�n para Diluir es obligatorio."
      End If
      If txtHoras.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtHoras
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
      If txtMinutos.Text = "" Then
        If blnIncorrecta = False Then
          Set auxControl = txtMinutos
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio."
      End If
    End If
  Case "L" 'Form.Magistral
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If txtText1(55).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(55)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Descripci�n de la F�rmula Magistral es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If txtText1(57).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(57)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Archivo Inf. de la F�rmula Magistral es obligatorio."
    End If
  End Select
  
  If blnIncorrecta = False Then
    Campos_Obligatorios = False
  Else
    Campos_Obligatorios = True
    MsgBox mensaje, vbInformation, "Aviso"
    auxControl.SetFocus
  End If

End Function

