VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form FrmRegAdministr 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMINISTRAR MEDICAMENTOS. Registrar Administraci�n."
   ClientHeight    =   4485
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0137.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdreceta 
      Caption         =   "Receta"
      Height          =   375
      Left            =   5040
      TabIndex        =   27
      Top             =   7680
      Width           =   1815
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Perfil Farmacoterap�utico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5055
      Index           =   1
      Left            =   120
      TabIndex        =   29
      Top             =   2520
      Width           =   11625
      Begin TabDlg.SSTab tabTab1 
         Height          =   4620
         Index           =   1
         Left            =   120
         TabIndex        =   32
         TabStop         =   0   'False
         Tag             =   "Fecha de Fin"
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   8149
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0137.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lbllabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lbllabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lbllabel1(4)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lbllabel1(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lbllabel1(8)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lbllabel1(9)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lbllabel1(10)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lbllabel1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lbllabel1(5)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lbllabel1(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lbllabel1(18)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "dtcDateCombo1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "cboUnidadMedida"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "cboFrecuencia"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "cboVia"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "tabTab1(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(0)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(1)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(6)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(7)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(8)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(2)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(3)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "chkCheck1(0)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(4)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(10)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "chkCheck1(2)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(9)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(5)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).ControlCount=   29
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0137.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR65HORA"
            Height          =   330
            Index           =   5
            Left            =   2160
            TabIndex        =   6
            Tag             =   "Hora"
            Top             =   480
            Width           =   492
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000011&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            Index           =   9
            Left            =   3960
            TabIndex        =   65
            Tag             =   "C�digo Producto"
            Top             =   120
            Width           =   855
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "�Quir�fano?"
            DataField       =   "FR65INDQUIROFANO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   19
            Tag             =   "Administrado en Quir�fano"
            Top             =   2160
            Width           =   1695
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR65CANTIDAD"
            Height          =   330
            Index           =   10
            Left            =   1080
            TabIndex        =   11
            Tag             =   "Cantidad"
            Top             =   1200
            Width           =   735
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            Height          =   330
            Index           =   4
            Left            =   240
            TabIndex        =   10
            Tag             =   "F.F. Forma Farmac�utica"
            Top             =   1200
            Width           =   735
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "�Consumido?"
            DataField       =   "FR65INDCONSUMIDO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   18
            Tag             =   "Consumido"
            Top             =   1800
            Width           =   1695
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR65GOTASPORMINUTO"
            Height          =   330
            Index           =   3
            Left            =   8520
            TabIndex        =   17
            Tag             =   "Gotas Minuto"
            Top             =   1200
            Width           =   732
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR65MLPORHORA"
            Height          =   330
            Index           =   2
            Left            =   7320
            TabIndex        =   16
            Tag             =   "Ml Hora"
            Top             =   1200
            Width           =   732
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR65DOSIS"
            Height          =   330
            Index           =   8
            Left            =   1920
            TabIndex        =   12
            Tag             =   "Dosis"
            Top             =   1200
            Width           =   1095
         End
         Begin VB.TextBox txtText1 
            DataField       =   "SG02COD_ENF"
            Height          =   330
            Index           =   7
            Left            =   10080
            TabIndex        =   35
            TabStop         =   0   'False
            Top             =   1320
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   6
            Left            =   10080
            TabIndex        =   34
            TabStop         =   0   'False
            Top             =   1680
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   1
            Left            =   3960
            ScrollBars      =   3  'Both
            TabIndex        =   9
            Tag             =   "Descripci�n Producto"
            Top             =   480
            Width           =   6780
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000E&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            Index           =   0
            Left            =   3000
            TabIndex        =   8
            Tag             =   "C�digo Producto"
            Top             =   480
            Width           =   855
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3945
            Index           =   2
            Left            =   -74760
            TabIndex        =   26
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   6959
            _StockProps     =   79
            Caption         =   "DEPARTAMENTOS REALIZADORES"
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   1695
            Index           =   0
            Left            =   120
            TabIndex        =   41
            TabStop         =   0   'False
            Top             =   2760
            Width           =   10575
            _ExtentX        =   18653
            _ExtentY        =   2990
            _Version        =   327681
            Style           =   1
            Tabs            =   4
            TabsPerRow      =   4
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Observaciones"
            TabPicture(0)   =   "FR0137.frx":0044
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lbllabel1(12)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "txtText1(14)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).ControlCount=   2
            TabCaption(1)   =   "Aditivos"
            TabPicture(1)   =   "FR0137.frx":0060
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lbllabel1(7)"
            Tab(1).Control(1)=   "txtText1(12)"
            Tab(1).ControlCount=   2
            TabCaption(2)   =   "Receta"
            TabPicture(2)   =   "FR0137.frx":007C
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lbllabel1(13)"
            Tab(2).Control(1)=   "chkCheck1(1)"
            Tab(2).Control(2)=   "txtText1(15)"
            Tab(2).ControlCount=   3
            TabCaption(3)   =   "Servicio"
            TabPicture(3)   =   "FR0137.frx":0098
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "lbllabel1(15)"
            Tab(3).Control(1)=   "lbllabel1(16)"
            Tab(3).Control(2)=   "txtText1(16)"
            Tab(3).Control(3)=   "txtText1(17)"
            Tab(3).ControlCount=   4
            Begin VB.TextBox txtText1 
               Height          =   330
               Index           =   17
               Left            =   -72360
               TabIndex        =   25
               Tag             =   "Descripci�n Servicio"
               Top             =   900
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   16
               Left            =   -74640
               TabIndex        =   24
               Tag             =   "C�digo Servicio"
               Top             =   900
               Width           =   372
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR65CANTRECETA"
               Height          =   330
               Index           =   15
               Left            =   -72480
               TabIndex        =   23
               Tag             =   "Cantidad de Receta"
               Top             =   900
               Width           =   852
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "�Receta?"
               DataField       =   "FR65INDRECETA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -74520
               TabIndex        =   22
               Tag             =   "Receta"
               Top             =   780
               Width           =   1455
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR65OBSERV"
               Height          =   675
               Index           =   14
               Left            =   240
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   20
               Tag             =   "Observaciones"
               Top             =   780
               Width           =   9900
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR65ADITIVOS"
               Height          =   690
               Index           =   12
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   21
               Tag             =   "Aditivos"
               Top             =   720
               Width           =   8940
            End
            Begin VB.Label lbllabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   16
               Left            =   -72360
               TabIndex        =   55
               Top             =   660
               Width           =   1770
            End
            Begin VB.Label lbllabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo de Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   15
               Left            =   -74640
               TabIndex        =   54
               Top             =   660
               Width           =   1620
            End
            Begin VB.Label lbllabel1 
               AutoSize        =   -1  'True
               Caption         =   "Cantidad de Receta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   13
               Left            =   -72480
               TabIndex        =   53
               Top             =   660
               Width           =   1710
            End
            Begin VB.Label lbllabel1 
               AutoSize        =   -1  'True
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   240
               TabIndex        =   52
               Top             =   540
               Width           =   1275
            End
            Begin VB.Label lbllabel1 
               AutoSize        =   -1  'True
               Caption         =   "Aditivos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   7
               Left            =   -74760
               TabIndex        =   51
               Top             =   480
               Width           =   690
            End
            Begin VB.Label lbllabel1 
               Caption         =   "Desviaci�n permitida"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   38
               Left            =   -69600
               TabIndex        =   50
               Top             =   1740
               Width           =   2055
            End
            Begin VB.Label lbllabel1 
               Caption         =   "Tipo Fabricaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   37
               Left            =   -74760
               TabIndex        =   49
               Top             =   1740
               Width           =   2055
            End
            Begin VB.Label lbllabel1 
               Caption         =   "Demanda/Consumo Anual"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   30
               Left            =   -71880
               TabIndex        =   48
               Top             =   600
               Width           =   2295
            End
            Begin VB.Label lbllabel1 
               Caption         =   "Stock M�nimo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   28
               Left            =   -74640
               TabIndex        =   47
               Top             =   1440
               Width           =   1575
            End
            Begin VB.Label lbllabel1 
               Caption         =   "Stock Preferente"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   26
               Left            =   -74640
               TabIndex        =   46
               Top             =   600
               Width           =   1575
            End
            Begin VB.Label lbllabel1 
               Caption         =   "C�d.proveedor C"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   25
               Left            =   -69600
               TabIndex        =   45
               Top             =   1320
               Width           =   1815
            End
            Begin VB.Label lbllabel1 
               Caption         =   "C�d.proveedor A"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   23
               Left            =   -74760
               TabIndex        =   44
               Top             =   1320
               Width           =   1815
            End
            Begin VB.Label lbllabel1 
               Caption         =   "Lote Econ�mico Pedido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   -74760
               TabIndex        =   43
               Top             =   540
               Width           =   2175
            End
            Begin VB.Label lbllabel1 
               Caption         =   "Coste Unitario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   -71520
               TabIndex        =   42
               Top             =   600
               Width           =   1935
            End
         End
         Begin SSDataWidgets_B.SSDBCombo cboVia 
            DataField       =   "FR34CODVIA"
            Height          =   315
            Left            =   5880
            TabIndex        =   15
            Tag             =   "Via"
            Top             =   1200
            Width           =   1215
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2223
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2143
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo cboFrecuencia 
            DataField       =   "FRG4CODFRECUENCIA"
            Height          =   315
            Left            =   4440
            TabIndex        =   14
            Tag             =   "Frecuencia"
            Top             =   1200
            Width           =   1215
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2223
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2143
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo cboUnidadMedida 
            DataField       =   "FR93CODUNIMEDIDA"
            Height          =   315
            Left            =   3120
            TabIndex        =   13
            Tag             =   "U.M. Unidad de Medida"
            Top             =   1200
            Width           =   1215
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2223
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2143
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR65FECHA"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   5
            Tag             =   "Fecha"
            Top             =   480
            Width           =   1800
            _Version        =   65537
            _ExtentX        =   3175
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Fecha"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   18
            Left            =   240
            TabIndex        =   67
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   2160
            TabIndex        =   66
            Top             =   240
            Width           =   495
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cantidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   1080
            TabIndex        =   64
            Top             =   960
            Width           =   765
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   3120
            TabIndex        =   63
            Top             =   960
            Width           =   420
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Forma F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   240
            TabIndex        =   62
            Top             =   960
            Width           =   750
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Gotas por Minuto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   8520
            TabIndex        =   40
            Top             =   960
            Width           =   1470
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Ml por Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   7320
            TabIndex        =   39
            Top             =   960
            Width           =   1005
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Frecuencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   4440
            TabIndex        =   38
            Top             =   960
            Width           =   960
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "V�a"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   5880
            TabIndex        =   37
            Top             =   960
            Width           =   315
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   1920
            TabIndex        =   36
            Top             =   960
            Width           =   480
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   3000
            TabIndex        =   33
            Top             =   240
            Width           =   780
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Index           =   2
      Left            =   120
      TabIndex        =   28
      Top             =   480
      Width           =   11655
      Begin TabDlg.SSTab tabTab1 
         Height          =   1575
         HelpContextID   =   90001
         Index           =   2
         Left            =   120
         TabIndex        =   56
         TabStop         =   0   'False
         Top             =   360
         Width           =   11250
         _ExtentX        =   19844
         _ExtentY        =   2778
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0137.frx":00B4
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lbllabel1(17)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lbllabel1(19)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lbllabel1(20)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lbllabel1(21)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lbllabel1(24)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(18)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(19)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(20)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(21)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(22)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0137.frx":00D0
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   22
            Left            =   240
            TabIndex        =   0
            Tag             =   "C�digo Persona"
            Top             =   360
            Width           =   852
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            Index           =   21
            Left            =   240
            TabIndex        =   2
            Tag             =   "Historia"
            Top             =   960
            Width           =   852
         End
         Begin VB.TextBox txtText1 
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            Index           =   20
            Left            =   1800
            TabIndex        =   3
            Tag             =   "Primer Apellido"
            Top             =   960
            Width           =   2700
         End
         Begin VB.TextBox txtText1 
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            Index           =   19
            Left            =   5400
            TabIndex        =   4
            Tag             =   "Segundo Apellido"
            Top             =   960
            Width           =   2700
         End
         Begin VB.TextBox txtText1 
            DataField       =   "CI22NOMBRE"
            Height          =   330
            Index           =   18
            Left            =   1800
            TabIndex        =   1
            Tag             =   "Nombre"
            Top             =   360
            Width           =   2700
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1365
            Index           =   0
            Left            =   -74880
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   120
            Width           =   10575
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18653
            _ExtentY        =   2408
            _StockProps     =   79
            Caption         =   "PACIENTES"
            ForeColor       =   0
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   24
            Left            =   240
            TabIndex        =   61
            Top             =   120
            Width           =   705
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Historia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   240
            TabIndex        =   60
            Top             =   720
            Width           =   660
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Apellido 2�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   5400
            TabIndex        =   59
            Top             =   720
            Width           =   930
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Apellido 1�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   1800
            TabIndex        =   58
            Top             =   720
            Width           =   930
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   1800
            TabIndex        =   57
            Top             =   120
            Width           =   660
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   30
      Top             =   4200
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmRegAdministr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0137.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: 30 SEPTIEMBRE 1998                                            *
'* DESCRIPCION: Registrar Administraci�n                                *
'* ARGUMENTOS:                                                          *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cboFrecuencia_CloseUp()
    objWinInfo.objWinActiveForm.blnChanged = True
    tlbToolbar1.Buttons(4).Enabled = True
End Sub
Private Sub cboUnidadMedida_CloseUp()
    objWinInfo.objWinActiveForm.blnChanged = True
    tlbToolbar1.Buttons(4).Enabled = True
End Sub
Private Sub cboVia_CloseUp()
    objWinInfo.objWinActiveForm.blnChanged = True
    tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Cancel = objWinInfo.WinExit
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


Private Sub cmdreceta_Click()
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.objWinActiveForm.intAllowance = cwAllowAdd
    objWinInfo.DataNew
    chkCheck1(1).Value = 1
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
   Dim objMasterInfo As New clsCWForm
   Dim objDetailInfo As New clsCWForm
   Dim strKey As String
   Dim rsta As rdoResultset
   Dim sqlstr As String
   
   Set objWinInfo = New clsCWWin
   
   Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
   
'  Rellenar el combo "cboVia"
   Me.cboVia.FieldSeparator = ";"
   Me.cboVia.RemoveAll
   sqlstr = "SELECT FR34CODVIA,FR34DESVIA FROM FR3400"
   Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
   While Not rsta.EOF
      Call Me.cboVia.AddItem(rsta.rdoColumns(0).Value & ";" & rsta.rdoColumns(1).Value)
      rsta.MoveNext
   Wend
   rsta.Close
   Set rsta = Nothing
  
'  Rellenar el combo "cboFrecuencia"
   Me.cboFrecuencia.FieldSeparator = ";"
   Me.cboFrecuencia.RemoveAll
   sqlstr = "SELECT FRG4CODFRECUENCIA,FRG4DESFRECUENCIA FROM FRG400"
   Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
   While Not rsta.EOF
      Call Me.cboFrecuencia.AddItem(rsta.rdoColumns(0).Value & ";" & rsta.rdoColumns(1).Value)
      rsta.MoveNext
   Wend
   rsta.Close
   Set rsta = Nothing
  
'  Rellenar el combo "cboUnidadMedida"
   Me.cboUnidadMedida.FieldSeparator = ";"
   Me.cboUnidadMedida.RemoveAll
   sqlstr = "SELECT FR93CODUNIMEDIDA,FR93DESUNIMEDIDA FROM FR9300"
   Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
   While Not rsta.EOF
      Call Me.cboUnidadMedida.AddItem(rsta.rdoColumns(0).Value & ";" & rsta.rdoColumns(1).Value)
      rsta.MoveNext
   Wend
   rsta.Close
   Set rsta = Nothing
  
   With objMasterInfo
      Set .objFormContainer = fraFrame1(2)
      Set .objFatherContainer = Nothing
      Set .tabMainTab = tabTab1(2)
      Set .grdGrid = grdDBGrid1(0)
      .strName = "Pacientes"
      .strTable = "CI2200"
      .intAllowance = cwAllowReadOnly
      Call .FormAddOrderField("CI21CODPERSONA", cwAscending)
      
      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Pacientes")
      Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
      Call .FormAddFilterWhere(strKey, "CI22HISTORIA", "Historia", cwNumeric)
      Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
      Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
      Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
          
      Call .FormAddFilterOrder(strKey, "CI21CODPERSONA", "C�digo Paciente")
      Call .FormAddFilterOrder(strKey, "CI22HISTORIA", "Historia")
      Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
      Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
      Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
   End With
   
   With objDetailInfo
      .strName = "Registrar Administraci�n"
      Set .objFormContainer = fraFrame1(1)
      Set .objFatherContainer = fraFrame1(2)
      Set .tabMainTab = tabTab1(1)
      Set .grdGrid = grdDBGrid1(2)
      '.strDataBase = objEnv.GetValue("Main")
      .strTable = "FR6500" 'Perfil FTP
      Call .FormAddOrderField("FR65FECHA", cwDescending)
      Call .FormAddOrderField("FR65HORA", cwDescending)
      
      Call .FormAddRelation("CI21CODPERSONA", txtText1(22))
      .intAllowance = cwAllowAll
      'Call .objPrinter.Add("PR1281", "Listado por Departamentos con sus Actuaciones")
      
      '.blnHasMaint = True
      
      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Registrar Administraci�n")
      Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR65FECHA", "Fecha", cwDate)
      Call .FormAddFilterWhere(strKey, "FR65HORA", "Hora", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR65DOSIS", "D�sis", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR34CODVIA", "C�digo V�a", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FRG4CODFRECUENCIA", "Frecuencia", cwString)
      Call .FormAddFilterWhere(strKey, "FR65ADITIVOS", "Aditivos", cwString)
      Call .FormAddFilterWhere(strKey, "FR65MLPORHORA", "Ml por Hora", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR65GOTASPORMINUTO", "Gotas por Minuto", cwNumeric)
      Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�digo Enfermero", cwBoolean)
      Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR65OBSERV", "Observaciones", cwString)
      Call .FormAddFilterWhere(strKey, "FR65INDRECETA", "�Receta?", cwBoolean)
      Call .FormAddFilterWhere(strKey, "FR65CANTRECETA", "Cantidad Receta", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR65INDCONSUMIDO", "�Consumido?", cwBoolean)
      Call .FormAddFilterWhere(strKey, "FR65INDQUIROFANO", "�Quir�fano?", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR65CANTIDAD", "Cantidad", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "Unidad de Medida", cwString)
      
      'Call .FormAddFilterWhere(strKey, "FR50CODPRESENT", "C�digo Presentaci�n", cwNumeric)
      
      Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
      Call .FormAddFilterOrder(strKey, "FR65FECHA", "Fecha")
      Call .FormAddFilterOrder(strKey, "FR65HORA", "Hora")
   End With
      
   With objWinInfo
      Call .FormAddInfo(objMasterInfo, cwFormDetail)
      Call .FormAddInfo(objDetailInfo, cwFormDetail)
      
      Call .FormCreateInfo(objMasterInfo)
      
      ' CI21CODPERSONA
      .CtrlGetInfo(txtText1(6)).blnInGrid = False
      ' SG02COD_ENF
      .CtrlGetInfo(txtText1(7)).blnInGrid = False
      
      .CtrlGetInfo(chkCheck1(0)).blnInFind = False
      .CtrlGetInfo(chkCheck1(1)).blnInFind = False
      
      ' FR73CODPRODUCTO
      .CtrlGetInfo(txtText1(0)).blnInFind = True
      ' Descripci�n del producto
      '.CtrlGetInfo(txtText1(1)).blnInFind = True
      ' FR65MLPORHORA
      .CtrlGetInfo(txtText1(2)).blnInFind = True
      ' FR65GOTASPORMINUTO
      .CtrlGetInfo(txtText1(3)).blnInFind = True
      ' Forma farmac�utica del producto
      ' .CtrlGetInfo(txtText1(4)).blnInFind = True
      ' FR65DOSIS
      .CtrlGetInfo(txtText1(8)).blnInFind = True
      ' FR65CANTRECETA
      .CtrlGetInfo(txtText1(15)).blnInFind = True
      ' AD02CODDPTO (C�digo de servicio)
      .CtrlGetInfo(txtText1(16)).blnInFind = True
      'HORA
      .CtrlGetInfo(txtText1(5)).blnInFind = True
      'FECHA
      .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
      'CONSUMIDO?
      .CtrlGetInfo(chkCheck1(0)).blnInFind = True
      'QUIR�FANO?
      .CtrlGetInfo(chkCheck1(2)).blnInFind = True
      
      
      
      .CtrlGetInfo(txtText1(0)).blnForeign = True
      ' Forma farmac�utica del producto
      ' .CtrlGetInfo(txtText1(4)).blnForeign = True
      .CtrlGetInfo(txtText1(16)).blnForeign = True
      
      .CtrlGetInfo(txtText1(22)).blnInFind = True
      .CtrlGetInfo(txtText1(21)).blnInFind = True
      .CtrlGetInfo(txtText1(18)).blnInFind = True
      .CtrlGetInfo(txtText1(20)).blnInFind = True
      .CtrlGetInfo(txtText1(19)).blnInFind = True
      
      'Call .FormChangeColor(objDetailInfo)
      
'      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(1), "FR73DESPRODUCTO")
      
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(9)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(9)), txtText1(4), "FRH7CODFORMFAR")
      
      ' C�digo de servicio, descripci�n de servicio
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO=?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
      
      Call .WinRegister
      Call .WinStabilize
   End With
   
   txtText1(9).Visible = False
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
   intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
   intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
   Dim strselect As String
   Dim rstselect As rdoResultset
   Dim strupdate As String

   strselect = "SELECT FR65INDCONSUMIDO FROM FR6500 WHERE FR65FECHA=TO_DATE('" & dtcDateCombo1(0).Text & "','dd/mm/yyyy')" & _
               " AND FR65HORA=" & txtText1(5).Text & " AND FR73CODPRODUCTO=" & txtText1(0).Text & _
               " AND CI21CODPERSONA=" & txtText1(6).Text
   Set rstselect = objApp.rdoConnect.OpenResultset(strselect)
   If Not rstselect.EOF Then
      If rstselect(0).Value = 0 And chkCheck1(0).Value = Unchecked And IsNumeric(txtText1(16).Text) Then
         strselect = "SELECT FR04CODALMACEN,FR47ACUSALIDA FROM FR4700 WHERE FR73CODPRODUCTO=" & txtText1(0).Text & _
                     " AND FR04CODALMACEN IN (SELECT FR04CODALMACEN FROM AD0200 WHERE AD02CODDPTO=" & _
                     txtText1(16).Text & ")"
         Set rstselect = objApp.rdoConnect.OpenResultset(strselect)
         strupdate = "UPDATE FR4700 SET FR47ACUSALIDA=" & rstselect(1).Value + txtText1(8).Text & _
                     " WHERE FFR04CODALMACEN=" & rstselect(0).Value & _
                     " AND FR73CODPRODUCTO=" & txtText1(0).Text
         objApp.rdoConnect.Execute strupdate, 64
      Else
         If rstselect(0).Value = 1 And chkCheck1(0).Value = False And IsNumeric(txtText1(16).Text) Then
            strselect = "SELECT FR04CODALMACEN,FR47ACUSALIDA FROM FR4700 WHERE FR73CODPRODUCTO=" & txtText1(0).Text & _
                         " AND FR04CODALMACEN IN (SELECT FR04CODALMACEN FROM AD0200 WHERE AD02CODDPTO=" & _
                         txtText1(16).Text & ")"
            Set rstselect = objApp.rdoConnect.OpenResultset(strselect)
            strupdate = "UPDATE FR4700 SET FR47ACUSALIDA=" & rstselect(1).Value - txtText1(8).Text & _
                         " WHERE FFR04CODALMACEN=" & rstselect(0).Value & _
                         " AND FR73CODPRODUCTO=" & txtText1(0).Text
            objApp.rdoConnect.Execute strupdate, 64
         End If
      End If
   End If
   rstselect.Close
   Set rstselect = Nothing
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Departamentos Realizadores" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  Dim strSentenciaWhere As String
  
  If strFormName = "Registrar Administraci�n" And strCtrl = "txtText1(0)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strWhere = "WHERE FR73FECFINVIG IS NULL" 'AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
         
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo del Producto"
         
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n del Producto "
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmac�utica "
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("ad02coddpto"))
      Call objWinInfo.CtrlSet(txtText1(0), .cllValues("FR73CODPRODUCTO"))
      Call objWinInfo.CtrlSet(txtText1(4), .cllValues("FRH7CODFORMFAR"))
     End If
   End With
  End If
  
  If strFormName = "Registrar Administraci�n" And strCtrl = "txtText1(4)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR5000"
     .strOrder = "ORDER BY FR50CODPRESENT ASC"
         
     Set objField = .AddField("FR50CODPRESENT")
     objField.strSmallDesc = "C�digo de Presentaci�n"
         
     Set objField = .AddField("FR50DESPRESENT")
     objField.strSmallDesc = "Descripci�n de Presentaci�n "
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("ad02coddpto"))
      Call objWinInfo.CtrlSet(txtText1(4), .cllValues("FR50CODPRESENT"))
     End If
   End With
   Set objSearch = Nothing
 End If
  
  If strFormName = "Registrar Administraci�n" And strCtrl = "txtText1(16)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "AD0200"
      .strOrder = "ORDER BY AD02CODDPTO ASC"
      strSentenciaWhere = ""
      strSentenciaWhere = "WHERE AD32CODTIPODPTO=3 AND "
      strSentenciaWhere = strSentenciaWhere & "AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND "
      strSentenciaWhere = strSentenciaWhere & "((AD02FECFIN IS NULL) OR "
      strSentenciaWhere = strSentenciaWhere & "(AD02FECFIN>=(SELECT SYSDATE FROM DUAL)))"
      .strWhere = strSentenciaWhere
     
      Set objField = .AddField("AD02CODDPTO")
      objField.strSmallDesc = "C�digo de Servicio"
         
      Set objField = .AddField("AD02DESDPTO")
      objField.strSmallDesc = "Descripci�n de Servicio"
         
      If .Search Then
         'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("ad02coddpto"))
         Call objWinInfo.CtrlSet(txtText1(16), .cllValues("AD02CODDPTO"))
      End If
   End With
   Set objSearch = Nothing
 End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabTab1_GotFocus(Index As Integer)
   If Index = 1 Then
      ' C�digo de producto
      On Error Resume Next
      ' C�d Producto
      grdDBGrid1(2).Columns(3).Visible = False
      ' Desc Producto
      grdDBGrid1(2).Columns(4).Width = 120 * 20
      ' Fecha
      grdDBGrid1(2).Columns(1).Width = 120 * 9
      ' Hora
      grdDBGrid1(2).Columns(2).Width = 120 * 4
      ' Cantidad
      grdDBGrid1(2).Columns(5).Width = 120 * 7
      ' Dosis
      grdDBGrid1(2).Columns(6).Width = 120 * 5
      ' U.M. Unidad de Medida
      grdDBGrid1(2).Columns(7).Width = 120 * 4
      ' Frecuencia
      grdDBGrid1(2).Columns(8).Width = 120 * 8
      ' Via
      grdDBGrid1(2).Columns(9).Width = 120 * 3
      ' Ml x Hora
      grdDBGrid1(2).Columns(10).Width = 120 * 7
      ' Gotas x Minuto
      grdDBGrid1(2).Columns(11).Width = 120 * 9
      ' Consumido
      grdDBGrid1(2).Columns(12).Width = 120 * 7
      ' Administrado en Quir�fano
      grdDBGrid1(2).Columns(13).Width = 120 * 18
      ' Observaciones
      grdDBGrid1(2).Columns(14).Width = 120 * 40
      ' Aditivos
      grdDBGrid1(2).Columns(15).Width = 120 * 40
      ' Receta
      grdDBGrid1(2).Columns(16).Width = 120 * 6
      ' Cantidad receta
      grdDBGrid1(2).Columns(17).Width = 120 * 14
      ' C�d Servicio
      grdDBGrid1(2).Columns(18).Width = 120 * 12
      ' Desc Servicio
      grdDBGrid1(2).Columns(19).Width = 120 * 20
      ' Cod Producto
      grdDBGrid1(2).Columns(20).Visible = False
      ' Forma Farmac�utica
      grdDBGrid1(2).Columns(21).Width = 120 * 4

      grdDBGrid1(2).Columns(21).Position = 5
      
      On Error GoTo 0
   End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Select Case btnButton.Index
        'NUEVO
        Case 2:
            Call MsgBox("Para introducir una Receta pulse el bot�n Receta")
            Exit Sub
        'GUARDAR
        Case 4:
            objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
    End Select
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
     Select Case intIndex
        'NUEVO
        Case 10:
            Call MsgBox("Para introducir una Receta pulse el bot�n Receta")
            Exit Sub
        'GUARDAR
        Case 40:
            objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
    End Select
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
   Call objWinInfo.CtrlDataChange
   If intIndex = 0 Then
      txtText1(9) = txtText1(0)
   End If
End Sub
