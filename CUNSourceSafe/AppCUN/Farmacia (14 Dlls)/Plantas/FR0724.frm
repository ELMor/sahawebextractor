VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmVisPRN 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Visualizar PRN"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0724.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdConsAcum 
      Caption         =   "Cons.Acum."
      Enabled         =   0   'False
      Height          =   375
      Left            =   7320
      TabIndex        =   29
      Top             =   7680
      Width           =   1935
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   10680
      Top             =   2280
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CheckBox chkFechas 
      Caption         =   "Todas las Fechas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4320
      TabIndex        =   21
      Top             =   600
      Value           =   1  'Checked
      Width           =   2055
   End
   Begin VB.TextBox txtHistoria 
      Height          =   315
      Left            =   6600
      MaxLength       =   7
      TabIndex        =   10
      Top             =   2520
      Width           =   1215
   End
   Begin VB.Frame Frame5 
      Caption         =   "Historia :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   6480
      TabIndex        =   20
      Top             =   2280
      Width           =   1695
   End
   Begin VB.CommandButton cmdFiltrar 
      Caption         =   "FILTRAR"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9000
      TabIndex        =   11
      Top             =   2400
      Width           =   1335
   End
   Begin VB.CommandButton cmdCesta 
      Caption         =   "&Consultar"
      Height          =   375
      Left            =   3960
      TabIndex        =   13
      Top             =   7680
      Width           =   1935
   End
   Begin VB.Frame Frame2 
      Caption         =   "Servicio :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1695
      Left            =   6360
      TabIndex        =   17
      Top             =   480
      Width           =   5415
      Begin VB.TextBox txtServicio 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "Desc.Servicio"
         Top             =   840
         Width           =   2445
      End
      Begin VB.CheckBox chkservicio 
         Caption         =   "Todos los servicios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   1320
         Visible         =   0   'False
         Width           =   2055
      End
      Begin SSDataWidgets_B.SSDBCombo cboservicio 
         Height          =   315
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   480
         Width           =   735
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2223
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1296
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin ComctlLib.ListView lvwSec 
         Height          =   1095
         Left            =   2640
         TabIndex        =   30
         Top             =   480
         Visible         =   0   'False
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   1931
         View            =   3
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Secciones:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   2640
         TabIndex        =   31
         Top             =   240
         Visible         =   0   'False
         Width           =   960
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Seleccionar :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   2415
      Left            =   120
      TabIndex        =   16
      Top             =   480
      Width           =   4095
      Begin VB.Frame Frame4 
         Height          =   2055
         Left            =   1440
         TabIndex        =   19
         Top             =   240
         Width           =   2535
         Begin VB.OptionButton optOMPRN 
            Caption         =   "Redactadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   1
            Left            =   120
            TabIndex        =   2
            Top             =   120
            Width           =   1455
         End
         Begin VB.OptionButton optOMPRN 
            Caption         =   "Enviadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   2
            Left            =   120
            TabIndex        =   3
            Top             =   480
            Width           =   2175
         End
         Begin VB.OptionButton optOMPRN 
            Caption         =   "Dispensadas Parciales"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   3
            Left            =   120
            TabIndex        =   4
            Top             =   840
            Width           =   2295
         End
         Begin VB.OptionButton optOMPRN 
            Caption         =   "Dispensadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   4
            Left            =   120
            TabIndex        =   5
            Top             =   1560
            Width           =   1695
         End
         Begin VB.OptionButton optOMPRN 
            Caption         =   "Parciales + Enviadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   5
            Left            =   120
            TabIndex        =   6
            Top             =   1200
            Width           =   2295
         End
      End
      Begin VB.Frame Frame3 
         Height          =   2055
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   1215
         Begin VB.OptionButton optCesta 
            Caption         =   "PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   1
            Left            =   120
            TabIndex        =   0
            Top             =   720
            Width           =   735
         End
         Begin VB.OptionButton optCesta 
            Caption         =   "Cestas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   375
            Index           =   0
            Left            =   120
            TabIndex        =   1
            Top             =   1080
            Width           =   975
         End
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Index           =   2
      Left            =   120
      TabIndex        =   12
      Tag             =   "Actuaciones Asociadas"
      Top             =   3000
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
         Height          =   4095
         Index           =   0
         Left            =   120
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   360
         Width           =   11490
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "NoDispensada"
         stylesets(0).BackColor=   16776960
         stylesets(0).Picture=   "FR0724.frx":000C
         stylesets(1).Name=   "Dispensada"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "FR0724.frx":0028
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20267
         _ExtentY        =   7223
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   14
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcredaccion 
      DataField       =   "fr28fecinicio"
      Height          =   330
      Left            =   4320
      TabIndex        =   22
      Top             =   1200
      Width           =   1860
      _Version        =   65537
      _ExtentX        =   3281
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcEnvio 
      DataField       =   "fr28fecinicio"
      Height          =   330
      Left            =   4320
      TabIndex        =   23
      Top             =   1800
      Width           =   1860
      _Version        =   65537
      _ExtentX        =   3281
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDispensacion 
      DataField       =   "fr28fecinicio"
      Height          =   330
      Left            =   4320
      TabIndex        =   24
      Top             =   2400
      Width           =   1860
      _Version        =   65537
      _ExtentX        =   3281
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin VB.Label lblDispen 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Dispensaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   4320
      TabIndex        =   27
      Top             =   2160
      Width           =   1740
   End
   Begin VB.Label lblEnvio 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Envio"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   4320
      TabIndex        =   26
      Top             =   1560
      Width           =   1080
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Redacci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   4320
      TabIndex        =   25
      Top             =   960
      Width           =   1515
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Visible         =   0   'False
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Visible         =   0   'False
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Visible         =   0   'False
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Visible         =   0   'False
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmVisPRN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0724.FRM                                                   *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: visualizar OM/PRN                                       *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim blnInload As Boolean
Dim glstrWhere As String


Private Sub auxDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
    
  If Index = 0 Then
    If auxdbgrid1(0).Columns("C�d.Estado").Value = 5 Then
      'For i = 0 To 23
      For i = 0 To 25
          auxdbgrid1(0).Columns(i).CellStyleSet "Dispensada"
      Next i
    Else
      'For i = 0 To 23
      For i = 0 To 25
          auxdbgrid1(0).Columns(i).CellStyleSet "NoDispensada"
      Next i
    End If
  End If

End Sub

Private Sub cboservicio_Change()
Dim intDptoSel As Integer

If IsNumeric(cboservicio.Text) Then
  If chkservicio.Value = 0 Then
  Else 'todos los servicios
    chkservicio.Value = False
  End If
  txtServicio.Text = cboservicio.Columns(1).Value
End If

End Sub

Private Sub cboservicio_Click()
Dim intDptoSel As Integer
  If IsNumeric(cboservicio.Text) Then
    If chkservicio.Value = 0 Then
    Else 'todos los servicios
        chkservicio.Value = False
    End If
    txtServicio.Text = cboservicio.Columns(1).Value
    intDptoSel = cboservicio
    Call pCargarSecciones(intDptoSel)
  End If

End Sub

Private Sub chkFechas_Click()

  If chkFechas = 1 Then
    dtcredaccion.Text = ""
    dtcEnvio.Text = ""
    dtcDispensacion.Text = ""
  End If
  auxdbgrid1(0).RemoveAll

End Sub

Private Sub cmdCesta_Click()
  
  cmdCesta.Enabled = False
  Screen.MousePointer = vbHourglass
  Me.Enabled = False
  gintfirmarOM = 1
  'If optCesta(1).Value = True Then 'es PRN
    If auxdbgrid1(0).SelBookmarks.Count > 0 Then
      glngpeticion = auxdbgrid1(0).Columns("C�digo Petici�n").Value
    Else
      Me.Enabled = True
      Screen.MousePointer = vbDefault
      cmdCesta.Enabled = True
      MsgBox "Debe seleccionar una Linea", vbInformation, "Aviso"
      Exit Sub
    End If
    gstrLlamadorProd = "frmVisPRN.Acumulados.Individual"
  'Else 'es Cesta
  '  gstrLlamadorProd = "Cesta"
  'End If
  Call objsecurity.LaunchProcess("FR0725")
  gstrLlamadorProd = ""
  Me.Enabled = True
  Screen.MousePointer = vbDefault
  cmdCesta.Enabled = True

End Sub

Private Sub cmdConsAcum_Click()
  
  cmdConsAcum.Enabled = False
  Screen.MousePointer = vbHourglass
  Me.Enabled = False
  gintfirmarOM = 1
  If auxdbgrid1(0).SelBookmarks.Count > 0 Then
    glngpeticion = auxdbgrid1(0).Columns("C�digo Petici�n").Value
  Else
    Me.Enabled = True
    Screen.MousePointer = vbDefault
    cmdConsAcum.Enabled = True
    MsgBox "Debe seleccionar una Linea", vbInformation, "Aviso"
    Exit Sub
  End If
  gstrLlamadorProd = "frmVisPRN.Acumulados"
  Call objsecurity.LaunchProcess("FR0725")
  gstrLlamadorProd = ""
  Me.Enabled = True
  Screen.MousePointer = vbDefault
  cmdConsAcum.Enabled = True

End Sub

Private Sub cmdFiltrar_Click()
Dim mensaje As String
Dim i%
Dim contSecciones As Integer
 
  If optOMPRN(4).Value = True Then
    If Not IsDate(dtcDispensacion) Or cboservicio = "" Then
      mensaje = MsgBox("Debe elegir Fecha de Dispensaci�n y Servicio," & Chr(13) & "porque si no hay demasiados registros.", vbInformation, "Aviso")
      Exit Sub
    End If
  End If

  glstrWhere = ""
  Screen.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
  If cboservicio.Text = "" And chkservicio = 0 Then
    Screen.MousePointer = vbDefault
    mensaje = MsgBox("No hay ning�n servicio seleccionado.", vbInformation, "Aviso")
    glstrWhere = "-1=0"
  Else
    If chkservicio = 1 Then 'todos los servicios
      If optCesta(1) = True Then 'PRNs
        If optOMPRN(1).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "(FR66INDCESTA<>-1 OR FR66INDCESTA IS NULL) AND FR66INDOM=0 AND FR26CODESTPETIC=1"
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=1"
        ElseIf optOMPRN(2).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "(FR66INDCESTA<>-1 OR FR66INDCESTA IS NULL) AND FR66INDOM=0 AND FR26CODESTPETIC=3"
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=3"
        ElseIf optOMPRN(3).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "(FR66INDCESTA<>-1 OR FR66INDCESTA IS NULL) AND FR66INDOM=0 AND FR26CODESTPETIC=9"
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=9"
        ElseIf optOMPRN(4).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "(FR66INDCESTA<>-1 OR FR66INDCESTA IS NULL) AND FR66INDOM=0 AND FR26CODESTPETIC=5"
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=5"
        ElseIf optOMPRN(5).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "(FR66INDCESTA<>-1 OR FR66INDCESTA IS NULL) AND FR66INDOM=0 AND FR26CODESTPETIC=5"
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC in (3,9)"
        End If
      Else 'Cestas
        If optOMPRN(1).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "FR66INDCESTA=-1 AND FR66INDOM=0 AND FR26CODESTPETIC=1"
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=1"
        ElseIf optOMPRN(2).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "FR66INDCESTA=-1 AND FR66INDOM=0 AND FR26CODESTPETIC=3"
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=3"
        ElseIf optOMPRN(3).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "FR66INDCESTA=-1 AND FR66INDOM=0 AND FR26CODESTPETIC=9"
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=9"
        ElseIf optOMPRN(4).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "FR66INDCESTA=-1 AND FR66INDOM=0 AND FR26CODESTPETIC=5"
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=5"
        ElseIf optOMPRN(5).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "FR66INDCESTA=-1 AND FR66INDOM=0 AND FR26CODESTPETIC=5"
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC in (3,9)"
        End If
      End If
    Else
      If optCesta(1) = True Then 'PRNs
        If optOMPRN(1).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "(FR66INDCESTA<>-1 OR FR66INDCESTA IS NULL) AND FR66INDOM=0 AND FR26CODESTPETIC=1 AND AD02CODDPTO=" & cboservicio.Text
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=1 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(2).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "(FR66INDCESTA<>-1 OR FR66INDCESTA IS NULL) AND FR66INDOM=0 AND FR26CODESTPETIC=3 AND AD02CODDPTO=" & cboservicio.Text
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=3 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(3).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "(FR66INDCESTA<>-1 OR FR66INDCESTA IS NULL) AND FR66INDOM=0 AND FR26CODESTPETIC=9 AND AD02CODDPTO=" & cboservicio.Text
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=9 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(4).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "(FR66INDCESTA<>-1 OR FR66INDCESTA IS NULL) AND FR66INDOM=0 AND FR26CODESTPETIC=5 AND AD02CODDPTO=" & cboservicio.Text
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=5 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(5).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "(FR66INDCESTA<>-1 OR FR66INDCESTA IS NULL) AND FR66INDOM=0 AND FR26CODESTPETIC=5 AND AD02CODDPTO=" & cboservicio.Text
          glstrWhere = "(FR6600.FR66INDCESTA<>-1 OR FR6600.FR66INDCESTA IS NULL) AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC in (3,9) AND FR6600.AD02CODDPTO=" & cboservicio.Text
        End If
      Else 'Cestas
        If optOMPRN(1).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "FR66INDCESTA=-1 AND FR66INDOM=0 AND FR26CODESTPETIC=1 AND AD02CODDPTO=" & cboservicio.Text
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=1 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(2).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "FR66INDCESTA=-1 AND FR66INDOM=0 AND FR26CODESTPETIC=3 AND AD02CODDPTO=" & cboservicio.Text
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=3 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(3).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "FR66INDCESTA=-1 AND FR66INDOM=0 AND FR26CODESTPETIC=9 AND AD02CODDPTO=" & cboservicio.Text
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=9 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(4).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "FR66INDCESTA=-1 AND FR66INDOM=0 AND FR26CODESTPETIC=5 AND AD02CODDPTO=" & cboservicio.Text
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=5 AND FR6600.AD02CODDPTO=" & cboservicio.Text
        ElseIf optOMPRN(5).Value = True Then
          'objWinInfo.objWinActiveForm.strWhere = "FR66INDCESTA=-1 AND FR66INDOM=0 AND FR26CODESTPETIC=5 AND AD02CODDPTO=" & cboservicio.Text
          glstrWhere = "FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC in (3,9) AND FR6600.AD02CODDPTO=" & cboservicio.Text
        End If
      End If
    End If
    
    If IsNumeric(txtHistoria.Text) Then
      'If objWinInfo.objWinActiveForm.strWhere <> "" Then
      If glstrWhere <> "" Then
        'If objWinInfo.objWinActiveForm.strWhere <> "-1=0" Then
        If glstrWhere <> "-1=0" Then
          'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txtHistoria.Text & ")"
          glstrWhere = glstrWhere & " AND FR6600.CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txtHistoria.Text & ")"
        Else
          'objWinInfo.objWinActiveForm.strWhere = " CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txtHistoria.Text & ")"
          glstrWhere = " FR6600.CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txtHistoria.Text & ")"
        End If
      Else
        'objWinInfo.objWinActiveForm.strWhere = " CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txtHistoria.Text & ")"
        glstrWhere = " FR6600.CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txtHistoria.Text & ")"
      End If
    End If
    
    'If objWinInfo.objWinActiveForm.strWhere <> "-1=0" Then
    If glstrWhere <> "-1=0" Then
      If IsDate(dtcredaccion) Then
        'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR6600.FR66FECREDACCION)=TO_DATE('" & dtcredaccion & "','DD/MM/YYYY')"
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECREDACCION)=TO_DATE('" & dtcredaccion & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcEnvio) Then
        'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR6600.FR66FECENVIO)=TO_DATE('" & dtcEnvio & "','DD/MM/YYYY')"
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECENVIO)=TO_DATE('" & dtcEnvio & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcDispensacion) Then
        'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR6600.FR66FECDISPEN)=TO_DATE('" & dtcDispensacion & "','DD/MM/YYYY')"
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECDISPEN)=TO_DATE('" & dtcDispensacion & "','DD/MM/YYYY')"
      End If
    Else
      'objWinInfo.objWinActiveForm.strWhere = " 1=1 "
      glstrWhere = " 1=1 "
      If IsDate(dtcredaccion) Then
        'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR6600.FR66FECREDACCION)=TO_DATE('" & dtcredaccion & "','DD/MM/YYYY')"
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECREDACCION)=TO_DATE('" & dtcredaccion & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcEnvio) Then
        'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR6600.FR66FECENVIO)=TO_DATE('" & dtcEnvio & "','DD/MM/YYYY')"
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECENVIO)=TO_DATE('" & dtcEnvio & "','DD/MM/YYYY')"
      End If
      If IsDate(dtcDispensacion) Then
        'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR6600.FR66FECDISPEN)=TO_DATE('" & dtcDispensacion & "','DD/MM/YYYY')"
        glstrWhere = glstrWhere & " AND TRUNC(FR6600.FR66FECDISPEN)=TO_DATE('" & dtcDispensacion & "','DD/MM/YYYY')"
      End If
    End If
'secciones''''''''''''''''''''''''''''''''''''''''''''''''''''
'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If lvwSec.ListItems.Count > 0 Then
    If lvwSec.ListItems(1).Selected Then
    Else
      For i = 2 To lvwSec.ListItems.Count
        If lvwSec.ListItems(i).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            glstrWhere = glstrWhere & " AND ("
          Else
            glstrWhere = glstrWhere & " OR "
          End If
          'glstrWhere = glstrWhere & "FR6600.AD41CODSECCION=" & lvwSec.ListItems(i).Tag
           If i = 2 Then
            glstrWhere = glstrWhere & "FR6600.AD41CODSECCION IS NULL"
          Else
            glstrWhere = glstrWhere & "FR6600.AD41CODSECCION=" & lvwSec.ListItems(i).Tag
          End If
        End If
      Next i
      If contSecciones > 0 Then
        glstrWhere = glstrWhere & ") "
      End If
    End If
  End If
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Me.Enabled = False
    Call Refrescar_Grid
    'objWinInfo.DataRefresh
    Me.Enabled = True
  End If
  
  If optOMPRN(2) Or optOMPRN(3) Or optOMPRN(5) Then
    If auxdbgrid1(0).Rows > 0 Then
      tlbToolbar1.Buttons(6).Enabled = True
    Else
      tlbToolbar1.Buttons(6).Enabled = False
    End If
  Else
    If optOMPRN(4) And IsDate(dtcDispensacion) And cboservicio <> "" Then
      If auxdbgrid1(0).Rows > 0 Then
        tlbToolbar1.Buttons(6).Enabled = True
      Else
        tlbToolbar1.Buttons(6).Enabled = False
      End If
    Else
      tlbToolbar1.Buttons(6).Enabled = False
    End If
  End If
  
  
  If optCesta(0) Then
    If auxdbgrid1(0).Rows > 0 Then
      cmdConsAcum.Enabled = True
    End If
  Else
    cmdConsAcum.Enabled = False
  End If
  
  Screen.MousePointer = vbDefault

End Sub

Private Sub dtcDispensacion_Change()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxdbgrid1(0).RemoveAll
End Sub

Private Sub dtcDispensacion_CloseUp()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxdbgrid1(0).RemoveAll
End Sub

Private Sub dtcEnvio_Change()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxdbgrid1(0).RemoveAll
End Sub

Private Sub dtcEnvio_CloseUp()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxdbgrid1(0).RemoveAll
End Sub

Private Sub dtcredaccion_Change()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxdbgrid1(0).RemoveAll
End Sub

Private Sub dtcredaccion_CloseUp()
If chkFechas = 1 Then
    chkFechas = 0
End If
auxdbgrid1(0).RemoveAll
End Sub



Private Sub lvwSec_Click()
Dim i%
If lvwSec.ListItems.Count > 0 Then
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  If lvwSec.ListItems(1).Selected Then
      For i = 2 To lvwSec.ListItems.Count
          lvwSec.ListItems(i).Selected = False
      Next i
  End If
  auxdbgrid1(0).RemoveAll
End If
End Sub

'Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
'
'  If strFormName = "Petici�n" And strCtrlName = "grdDBGrid1(1).Asistencia" Then
'    aValues(2) = objWinInfo.objWinActiveForm.rdoCursor("AD07CODPROCESO").Value
'  End If
'
'End Sub

Private Sub optCesta_Click(Index As Integer)

auxdbgrid1(0).RemoveAll
    
    If optCesta(1) Then
      lvwSec.Visible = False
      Label1(1).Visible = False
    Else
      If cboservicio <> "" Then
        Call pCargarSecciones(cboservicio)
      End If
    End If

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


Private Sub cboservicio_CloseUp()
 Dim intDptoSel As Integer
 
  If IsNumeric(cboservicio.Text) Then
    If chkservicio.Value = 0 Then
    Else 'todos los servicios
        chkservicio.Value = False
    End If
    txtServicio.Text = cboservicio.Columns(1).Value
  End If

  If Trim(cboservicio.Text) <> "" Then
    If chkservicio.Value = 0 Then
      txtServicio.Text = cboservicio.Columns(1).Value
      If cboservicio <> "" Then
        intDptoSel = cboservicio
        Call pCargarSecciones(intDptoSel)
      End If
    Else 'todos los servicios
      txtServicio.Text = cboservicio.Columns(1).Value
      chkservicio.Value = 0
    End If
  Else
    txtServicio.Text = ""
    chkservicio.Value = 1
  End If
  auxdbgrid1(0).RemoveAll

End Sub



Private Sub chkservicio_Click()
  
  If chkservicio = 1 Then
    cboservicio.Text = ""
    txtServicio.Text = ""
  Else
    If cboservicio <> "" Then
        txtServicio.Text = cboservicio.Columns(1).Value
    End If
  End If

auxdbgrid1(0).RemoveAll

End Sub

Private Sub Form_Activate()
  Dim stra As String
  Dim rsta As rdoResultset
    
If blnInload = True Then
  blnInload = False
  'cboservicio.RemoveAll
  'stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
  '       "WHERE AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND" & _
  '       "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL))) ORDER BY AD02DESDPTO"
  'Set rsta = objApp.rdoConnect.OpenResultset(stra)
  'While (Not rsta.EOF)
  '    Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
  '    rsta.MoveNext
  'Wend
  'rsta.Close
  'Set rsta = Nothing
  
  stra = "SELECT AD0200.AD02CODDPTO,AD0200.AD02DESDPTO FROM AD0300,AD0200 WHERE SG02COD='" & objsecurity.strUser & "'"
  stra = stra & " AND AD0300.AD02CODDPTO=AD0200.AD02CODDPTO AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    cboservicio.Value = rsta(0).Value
    txtServicio.Text = rsta(1).Value
  End If
  cboservicio.RemoveAll
  While (Not rsta.EOF)
      Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
      rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  
  If cboservicio <> "" Then
    Call pCargarSecciones(cboservicio)
  End If
  
  optCesta(1).Value = True 'PRN
  optOMPRN(2).Value = True
  Screen.MousePointer = vbDefault
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
'Dim objMultiInfo As New clsCWForm
'Dim strKey As String
Dim j As Integer
  
    Set objWinInfo = New clsCWWin
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
'    With objMultiInfo
'        .strName = "Petici�n"
'        Set .objFormContainer = fraframe1(2)
'        Set .objFatherContainer = Nothing
'        Set .tabMainTab = Nothing
'        Set .grdGrid = grdDBGrid1(1)
'        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
'        .strTable = "FR6600"
'        .intAllowance = cwAllowReadOnly
'        .strWhere = "-1=0" 'el grid vac�o
'
'        Call .FormAddOrderField("FR66CODPETICION", cwDescending)
'
'        strKey = .strDataBase & .strTable
'        Call .FormCreateFilterWhere(strKey, "Petici�n")
'        Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
'        Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
'        Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
'        Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
'        Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
'        Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
'        Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
'        Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
'        Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
'        Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
'        Call .FormAddFilterWhere(strKey, "FR66INDMEDINF", "Medicaci�n Infantil?", cwBoolean)
'        Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
'        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
'    End With
'
'    With objWinInfo
'
'        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
'
'        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
'        Call .GridAddColumn(objMultiInfo, "C�digo Petici�n", "FR66CODPETICION", cwNumeric, 9)
'        Call .GridAddColumn(objMultiInfo, "Cesta", "FR66INDCESTA", cwBoolean)
'        Call .GridAddColumn(objMultiInfo, "C�d.Estado", "FR26CODESTPETIC", cwNumeric, 1)
'        Call .GridAddColumn(objMultiInfo, "Estado", "", cwString, 30)
'        Call .GridAddColumn(objMultiInfo, "Fecha Emisi�n", "FR66FECREDACCION", cwDate)
'        Call .GridAddColumn(objMultiInfo, "Hora", "FR66HORAREDACCI", cwDecimal, 2)
'        Call .GridAddColumn(objMultiInfo, "Fecha Firma", "FR66FECFIRMMEDI", cwDate)
'        Call .GridAddColumn(objMultiInfo, "Hora Firma", "FR66HORAFIRMMEDI", cwDecimal, 2)
'        Call .GridAddColumn(objMultiInfo, "C�digo Persona", "CI21CODPERSONA", cwNumeric, 7)
'        Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7)
'        Call .GridAddColumn(objMultiInfo, "Cama", "", cwString, 7)
'        Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25)
'        Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25)
'        Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25)
'        Call .GridAddColumn(objMultiInfo, "C�d.M�dico", "SG02COD_MED", cwString, 6)
'        Call .GridAddColumn(objMultiInfo, "Dr.", "", cwString, 30)
'        Call .GridAddColumn(objMultiInfo, "C�d.Urgencia", "FR91CODURGENCIA", cwNumeric, 2)
'        Call .GridAddColumn(objMultiInfo, "Urgencia", "", cwString, 6)
'        Call .GridAddColumn(objMultiInfo, "C�d.Servicio", "AD02CODDPTO", cwNumeric, 3)
'        Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 30)
'
'        Call .GridAddColumn(objMultiInfo, "Proceso", "AD07CODPROCESO", cwNumeric, 10)
'        Call .GridAddColumn(objMultiInfo, "Asistencia", "AD01CODASISTENCI", cwNumeric, 10)
'
'        Call .FormCreateInfo(objMultiInfo)
'
'        Call .FormChangeColor(objMultiInfo)
'
'        'Se indican los campos por los que se desea buscar
'        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(19)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(21)).blnInFind = True
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "FR26DESESTADOPET")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(11)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(11)), grdDBGrid1(1).Columns(12), "CI22NUMHISTORIA")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(11)), grdDBGrid1(1).Columns(14), "CI22NOMBRE")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(11)), grdDBGrid1(1).Columns(15), "CI22PRIAPEL")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(11)), grdDBGrid1(1).Columns(16), "CI22SEGAPEL")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(17)), "SG02COD_MED", "SELECT SG02APE1 || ',' || SG02NOM DOCTOR FROM SG0200 WHERE SG02COD = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(17)), grdDBGrid1(1).Columns(18), "DOCTOR")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(19)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(19)), grdDBGrid1(1).Columns(20), "FR91DESURGENCIA")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(21)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(21)), grdDBGrid1(1).Columns(22), "AD02DESDPTO")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("Asistencia")), "AD01CODASISTENCI", "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD01CODASISTENCI= ? AND AD07CODPROCESO=?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns("Asistencia")), grdDBGrid1(1).Columns("Cama"), "GCFN06(AD15CODCAMA)")
'
'        Call .WinRegister
'        Call .WinStabilize
'    End With
    
blnInload = True
    
auxdbgrid1(0).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
'For j = 0 To 21
For j = 0 To 23
  Call auxdbgrid1(0).Columns.Add(j)
Next j
auxdbgrid1(0).Columns(0).Caption = "C�digo Petici�n"
auxdbgrid1(0).Columns(0).Visible = True
auxdbgrid1(0).Columns(0).Locked = True
auxdbgrid1(0).Columns(1).Caption = "Cesta"
auxdbgrid1(0).Columns(1).Visible = False
auxdbgrid1(0).Columns(1).Locked = True
auxdbgrid1(0).Columns(2).Caption = "C�d.Estado"
auxdbgrid1(0).Columns(2).Visible = False
auxdbgrid1(0).Columns(2).Locked = True
auxdbgrid1(0).Columns(3).Caption = "Estado"
auxdbgrid1(0).Columns(3).Visible = False
auxdbgrid1(0).Columns(3).Locked = True
auxdbgrid1(0).Columns(4).Caption = "Fecha Redacci�n"
auxdbgrid1(0).Columns(4).Visible = True
auxdbgrid1(0).Columns(4).Locked = True
auxdbgrid1(0).Columns(5).Caption = "Hora"
auxdbgrid1(0).Columns(5).Visible = True
auxdbgrid1(0).Columns(5).Locked = True
auxdbgrid1(0).Columns(6).Caption = "Fecha Firma"
auxdbgrid1(0).Columns(6).Visible = False
auxdbgrid1(0).Columns(6).Locked = True
auxdbgrid1(0).Columns(7).Caption = "Hora Firma"
auxdbgrid1(0).Columns(7).Visible = False
auxdbgrid1(0).Columns(7).Locked = True
auxdbgrid1(0).Columns(8).Caption = "C�digo Persona"
auxdbgrid1(0).Columns(8).Visible = False
auxdbgrid1(0).Columns(8).Locked = True
auxdbgrid1(0).Columns(9).Caption = "Historia"
auxdbgrid1(0).Columns(9).Visible = True
auxdbgrid1(0).Columns(9).Locked = True
auxdbgrid1(0).Columns(10).Caption = "Cama"
auxdbgrid1(0).Columns(10).Visible = True
auxdbgrid1(0).Columns(10).Locked = True
auxdbgrid1(0).Columns(11).Caption = "Nombre"
auxdbgrid1(0).Columns(11).Visible = True
auxdbgrid1(0).Columns(11).Locked = True
auxdbgrid1(0).Columns(12).Caption = "Apellido 1�"
auxdbgrid1(0).Columns(12).Visible = True
auxdbgrid1(0).Columns(12).Locked = True
auxdbgrid1(0).Columns(13).Caption = "Apellido 2�"
auxdbgrid1(0).Columns(13).Visible = True
auxdbgrid1(0).Columns(13).Locked = True
auxdbgrid1(0).Columns(14).Caption = "C�d.M�dico"
auxdbgrid1(0).Columns(14).Visible = False
auxdbgrid1(0).Columns(14).Locked = True
auxdbgrid1(0).Columns(15).Caption = "Dr."
auxdbgrid1(0).Columns(15).Visible = True
auxdbgrid1(0).Columns(15).Locked = True
auxdbgrid1(0).Columns(16).Caption = "C�d.Urgencia"
auxdbgrid1(0).Columns(16).Visible = False
auxdbgrid1(0).Columns(16).Locked = True
auxdbgrid1(0).Columns(17).Caption = "Urgencia"
auxdbgrid1(0).Columns(17).Visible = True
auxdbgrid1(0).Columns(17).Locked = True
auxdbgrid1(0).Columns(18).Caption = "C�d.Servicio"
auxdbgrid1(0).Columns(18).Visible = True
auxdbgrid1(0).Columns(18).Locked = True
auxdbgrid1(0).Columns(19).Caption = "Servicio"
auxdbgrid1(0).Columns(19).Visible = True
auxdbgrid1(0).Columns(19).Locked = True
''''''''''''''''''''''''''''''''''''''''''''''''''
auxdbgrid1(0).Columns(20).Caption = "C�d.Secci�n"
auxdbgrid1(0).Columns(20).Visible = False
auxdbgrid1(0).Columns(20).Locked = True
auxdbgrid1(0).Columns(21).Caption = "Secci�n"
auxdbgrid1(0).Columns(21).Visible = True
auxdbgrid1(0).Columns(21).Locked = True

''''''''''''''''''''''''''''''''''''''''''''''''''
auxdbgrid1(0).Columns(22).Caption = "Proceso"
auxdbgrid1(0).Columns(22).Visible = False
auxdbgrid1(0).Columns(22).Locked = True
auxdbgrid1(0).Columns(23).Caption = "Asistencia"
auxdbgrid1(0).Columns(23).Visible = False
auxdbgrid1(0).Columns(23).Locked = True
auxdbgrid1(0).Columns(24).Caption = "Fecha Envio"
auxdbgrid1(0).Columns(24).Visible = True
auxdbgrid1(0).Columns(24).Locked = True
auxdbgrid1(0).Columns(25).Caption = "Fecha Dispensacion"
auxdbgrid1(0).Columns(25).Visible = True
auxdbgrid1(0).Columns(25).Locked = True

auxdbgrid1(0).Columns("Urgencia").Position = 21
auxdbgrid1(0).Columns("Fecha Envio").Position = 6
auxdbgrid1(0).Columns("Fecha Dispensacion").Position = 7
    
auxdbgrid1(0).Columns(0).Width = 800 'c�d petici�n
auxdbgrid1(0).Columns(3).Width = 1600 'estado
auxdbgrid1(0).Columns(4).Width = 1200 'fecha redacci�n
auxdbgrid1(0).Columns(5).Width = 700 'hora redacci�n
auxdbgrid1(0).Columns(6).Width = 1200 'fecha firma
auxdbgrid1(0).Columns(7).Width = 500 'hora firma
auxdbgrid1(0).Columns(9).Width = 800 'historia
auxdbgrid1(0).Columns(10).Width = 600 'cama
auxdbgrid1(0).Columns(11).Width = 1530 'nombre
auxdbgrid1(0).Columns(12).Width = 1530 'apellido 1�
auxdbgrid1(0).Columns(13).Width = 1530 'apellido 2�
auxdbgrid1(0).Columns(15).Width = 1600 'doctor
auxdbgrid1(0).Columns(17).Width = 800 'urgencia
auxdbgrid1(0).Columns(18).Width = 750 'servicio

auxdbgrid1(0).Redraw = True

For j = 1 To 29
tlbToolbar1.Buttons(j).Enabled = False
Next j

'se cargan las secciones del Dpto. seleccionado
lvwSec.ColumnHeaders.Add , , , 2000
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    'intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    'intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
'    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
'    Call objWinInfo.WinDeRegister
'    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub


Private Sub optOMPRN_Click(Index As Integer)

auxdbgrid1(0).RemoveAll

  Select Case Index
  Case 1
    lblEnvio.Visible = False
    dtcEnvio.Text = ""
    dtcEnvio.Visible = False
    lblDispen.Visible = False
    dtcDispensacion.Text = ""
    dtcDispensacion.Visible = False
  Case 2
    lblEnvio.Visible = True
    dtcEnvio.Text = ""
    dtcEnvio.Visible = True
    lblDispen.Visible = False
    dtcDispensacion.Text = ""
    dtcDispensacion.Visible = False
  Case 3, 4, 5
    lblEnvio.Visible = True
    dtcEnvio.Text = ""
    dtcEnvio.Visible = True
    lblDispen.Visible = True
    dtcDispensacion.Text = ""
    dtcDispensacion.Visible = True
  End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
'    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
'    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  '30 Salir
  If btnButton.Index = 30 Then
    Unload Me
  ElseIf btnButton.Index = 6 Then
    If optOMPRN(2) Or optOMPRN(3) Or optOMPRN(5) Then
      If optCesta(1).Value Then
        Call Imprimir("FR1244.RPT", 0)
      Else
        If lvwSec.ListItems.Count > 0 Then
          Call Imprimir("FR1246.RPT", 0)
        Else
          Call Imprimir("FR1243.RPT", 0)
        End If
      End If
    Else
      If optCesta(1).Value Then
        Call Imprimir("FR1242.RPT", 0)
      Else
        If lvwSec.ListItems.Count > 0 Then
          Call Imprimir("FR1245.RPT", 0)
        Else
          Call Imprimir("FR1241.RPT", 0)
        End If
      End If
    End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    'Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    'Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    'Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    'Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    'Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    'Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtHistoria_Change()

auxdbgrid1(0).RemoveAll

End Sub

Private Sub Refrescar_Grid()
Dim strselect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim mvntAcumulado As Variant
Dim mvntAcumuladoDisp As Variant
Dim mvntAcumuladoOri As Variant
Dim mvntProducto As Variant
Dim i As Integer
Dim rstsec As rdoResultset
Dim strsec As String
Dim codigoseccion
Dim seccion

  
Me.Enabled = False
Screen.MousePointer = vbHourglass

auxdbgrid1(0).Redraw = False
auxdbgrid1(0).RemoveAll
  
strselect = "SELECT "
strselect = strselect & " FR6600.FR66CODPETICION"
strselect = strselect & ",FR6600.FR66INDCESTA"
strselect = strselect & ",FR6600.FR26CODESTPETIC"
strselect = strselect & ",FR2600.FR26DESESTADOPET"
strselect = strselect & ",FR6600.FR66FECREDACCION"
strselect = strselect & ",FR6600.FR66HORAREDACCI"
strselect = strselect & ",FR6600.FR66FECFIRMMEDI"
strselect = strselect & ",FR6600.FR66HORAFIRMMEDI"
strselect = strselect & ",FR6600.CI21CODPERSONA"
strselect = strselect & ",CI2200.CI22NUMHISTORIA"
strselect = strselect & ",GCFN06(AD1500.AD15CODCAMA) CAMADESC"
strselect = strselect & ",CI2200.CI22NOMBRE"
strselect = strselect & ",CI2200.CI22PRIAPEL"
strselect = strselect & ",CI2200.CI22SEGAPEL"
strselect = strselect & ",FR6600.SG02COD_MED"
strselect = strselect & ",SG0200.SG02APE1"
strselect = strselect & ",FR6600.FR91CODURGENCIA"
strselect = strselect & ",FR9100.FR91DESURGENCIA"
strselect = strselect & ",FR6600.AD02CODDPTO"
strselect = strselect & ",AD0200.AD02DESDPTO"
strselect = strselect & ",FR6600.AD07CODPROCESO"
strselect = strselect & ",FR6600.AD01CODASISTENCI"
strselect = strselect & ",FR6600.FR66FECENVIO"
strselect = strselect & ",FR6600.FR66FECDISPEN"

strselect = strselect & " FROM FR6600,FR2600,CI2200,SG0200,FR9100,AD1500,AD0200"
strselect = strselect & " WHERE "

strselect = strselect & " FR6600.FR26CODESTPETIC=FR2600.FR26CODESTPETIC"
strselect = strselect & " AND FR6600.CI21CODPERSONA=CI2200.CI21CODPERSONA"
strselect = strselect & " AND FR6600.SG02COD_MED=SG0200.SG02COD(+)"
strselect = strselect & " AND FR6600.FR91CODURGENCIA=FR9100.FR91CODURGENCIA(+)"
strselect = strselect & " AND FR6600.AD07CODPROCESO=AD1500.AD07CODPROCESO(+)"
strselect = strselect & " AND FR6600.AD01CODASISTENCI=AD1500.AD01CODASISTENCI(+)"
strselect = strselect & " AND FR6600.AD02CODDPTO=AD0200.AD02CODDPTO"
strselect = strselect & " AND (FR6600.FR66INDESTUPEFACIENTE=0 OR FR6600.FR66INDESTUPEFACIENTE IS NULL)"
strselect = strselect & " AND (FR6600.FR66INDFM=0 OR FR6600.FR66INDFM IS NULL)"

strselect = strselect & " AND " & glstrWhere

strselect = strselect & " ORDER BY FR66CODPETICION DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strselect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF
  If Not IsNull(rsta("FR66CODPETICION").Value) Then
    strsec = "SELECT FR6600.AD41CODSECCION,AD4100.AD41DESSECCION" & _
             " FROM FR6600,AD4100 WHERE " & _
             "FR6600.AD41CODSECCION=AD4100.AD41CODSECCION" & " AND " & _
             "FR6600.FR66CODPETICION=" & rsta("FR66CODPETICION").Value
    Set rstsec = objApp.rdoConnect.OpenResultset(strsec)
    If Not rstsec.EOF Then
        If Not IsNull(rstsec.rdoColumns("AD41CODSECCION").Value) _
            And Not IsNull(rstsec.rdoColumns("AD41DESSECCION").Value) Then
                    codigoseccion = rstsec.rdoColumns("AD41CODSECCION").Value
                    seccion = rstsec.rdoColumns("AD41DESSECCION").Value
        Else
                    codigoseccion = ""
                    seccion = ""
        End If
    Else
      codigoseccion = ""
      seccion = ""
    End If
    rstsec.Close
    Set rstsec = Nothing
  End If

  strlinea = rsta("FR66CODPETICION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66INDCESTA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR26CODESTPETIC").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR26DESESTADOPET").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66FECREDACCION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66HORAREDACCI").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66FECFIRMMEDI").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66HORAFIRMMEDI").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CI21CODPERSONA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CI22NUMHISTORIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CAMADESC").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CI22NOMBRE").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CI22PRIAPEL").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CI22SEGAPEL").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("SG02COD_MED").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("SG02APE1").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR91CODURGENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR91DESURGENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD02CODDPTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD02DESDPTO").Value & Chr(vbKeyTab)
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  strlinea = strlinea & codigoseccion & Chr(vbKeyTab)
  strlinea = strlinea & seccion & Chr(vbKeyTab)
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  strlinea = strlinea & rsta("AD07CODPROCESO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD01CODASISTENCI").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66FECENVIO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66FECDISPEN").Value
  auxdbgrid1(0).AddItem strlinea
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
auxdbgrid1(0).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Imprimir(strListado As String, intDes As Integer)
'JMRL 19991125
'Toma como par�metro el listado a imprimir y lo manda a la impresora;
'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
Dim strWhere As String
Dim strDNS As String
Dim strUser As String
Dim strPass As String
Dim strPATH As String
Dim contSecciones As Integer
Dim i As Integer
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.GetDataBasePasswordOld
  strPATH = "C:\Archivos de programa\cun\rpt\"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  
  If strListado = "FR1242.RPT" Or strListado = "FR1244.RPT" Then
    strWhere = "{FR6600.AD02CODDPTO}=" & cboservicio & _
      " AND ({FR6600.FR66INDESTUPEFACIENTE}=0 OR {FR6600.FR66INDESTUPEFACIENTE} IS NULL)" & _
      " AND ({FR6600.FR66INDFM}=0 OR {FR6600.FR66INDFM} IS NULL)"
    If IsDate(dtcDispensacion) Then
      strWhere = strWhere & " AND ToText({FR6600.FR66FECDISPEN}) ='" & dtcDispensacion & "'"
    End If
    If IsDate(dtcredaccion) Then
      strWhere = strWhere & " AND ToText({FR6600.FR66FECREDACCION}) ='" & dtcredaccion & "'"
    End If
    If IsDate(dtcEnvio) Then
      strWhere = strWhere & " AND ToText({FR6600.FR66FECENVIO}) ='" & dtcEnvio & "'"
    End If
    If IsNumeric(txtHistoria.Text) Then
      If auxdbgrid1(0).Rows > 0 Then
        strWhere = strWhere & " AND {FR6600.CI21CODPERSONA}=" & auxdbgrid1(0).Columns("C�digo Persona").Value
      End If
    End If
    If optOMPRN(2) Then '3
      strWhere = strWhere & " AND {FR6600.FR26CODESTPETIC}=3"
    ElseIf optOMPRN(3) Then '9
      strWhere = strWhere & " AND {FR6600.FR26CODESTPETIC}=9"
    ElseIf optOMPRN(5) Then '3,9
      strWhere = strWhere & " AND ({FR6600.FR26CODESTPETIC}=3 OR {FR6600.FR26CODESTPETIC}=9) "
    End If
  Else
    If lvwSec.ListItems.Count > 0 Then
      strWhere = "{FR6604J.AD02CODDPTO}=" & cboservicio
      'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
      contSecciones = 0
      If lvwSec.ListItems.Count > 0 Then
        If lvwSec.ListItems(1).Selected Then
        Else
          For i = 2 To lvwSec.ListItems.Count
            If lvwSec.ListItems(i).Selected = True Then
              If contSecciones = 0 Then
                contSecciones = contSecciones + 1
                strWhere = strWhere & " AND ("
              Else
                strWhere = strWhere & " OR "
              End If
              If i = 2 Then
                strWhere = strWhere & "isnull({FR6604J.AD41CODSECCION})"
              Else
                strWhere = strWhere & "{FR6604J.AD41CODSECCION}=" & lvwSec.ListItems(i).Tag
              End If
            End If
          Next i
          If contSecciones > 0 Then
            strWhere = strWhere & ") "
          End If
        End If
      End If
      If IsDate(dtcDispensacion) Then
        strWhere = strWhere & " AND ToText({FR6604J.FR66FECDISPEN}) ='" & dtcDispensacion & "'"
      End If
      If IsDate(dtcredaccion) Then
        strWhere = strWhere & " AND ToText({FR6604J.FR66FECREDACCION}) ='" & dtcredaccion & "'"
      End If
      If IsDate(dtcEnvio) Then
        strWhere = strWhere & " AND ToText({FR6604J.FR66FECENVIO}) ='" & dtcEnvio & "'"
      End If
      'If IsNumeric(txtHistoria.Text) Then
      '  If auxdbgrid1(0).Rows > 0 Then
      '    strWhere = strWhere & " AND {FR6604J.CI21CODPERSONA}=" & auxdbgrid1(0).Columns("C�digo Persona").Value
      '  End If
      'End If
      If optOMPRN(2) Then '3
        strWhere = strWhere & " AND {FR6604J.FR26CODESTPETIC}=3"
      ElseIf optOMPRN(3) Then '9
        strWhere = strWhere & " AND {FR6604J.FR26CODESTPETIC}=9"
      ElseIf optOMPRN(5) Then '3,9
        strWhere = strWhere & " AND ({FR6604J.FR26CODESTPETIC}=3 OR {FR6604J.FR26CODESTPETIC}=9) "
      End If
      If strListado = "FR1245.RPT" Then
        strWhere = strWhere & " AND ({FR6604J.FR26CODESTPETIC}=5 or {FR6604J.FR26CODESTPETIC}=9)"
      Else
        strWhere = strWhere & " AND {FR6604J.FR28CANTIDAD}>0"
      End If
    Else
      strWhere = "{FR6600.AD02CODDPTO}=" & cboservicio
      If IsDate(dtcDispensacion) Then
        strWhere = strWhere & " AND ToText({FR6600.FR66FECDISPEN}) ='" & dtcDispensacion & "'"
      End If
      If IsDate(dtcredaccion) Then
        strWhere = strWhere & " AND ToText({FR6600.FR66FECREDACCION}) ='" & dtcredaccion & "'"
      End If
      If IsDate(dtcEnvio) Then
        strWhere = strWhere & " AND ToText({FR6600.FR66FECENVIO}) ='" & dtcEnvio & "'"
      End If
      If IsNumeric(txtHistoria.Text) Then
        If auxdbgrid1(0).Rows > 0 Then
          strWhere = strWhere & " AND {FR6600.CI21CODPERSONA}=" & auxdbgrid1(0).Columns("C�digo Persona").Value
        End If
      End If
      If optOMPRN(2) Then '3
        strWhere = strWhere & " AND {FR6600.FR26CODESTPETIC}=3"
      ElseIf optOMPRN(3) Then '9
        strWhere = strWhere & " AND {FR6600.FR26CODESTPETIC}=9"
      ElseIf optOMPRN(5) Then '3,9
        strWhere = strWhere & " AND ({FR6600.FR26CODESTPETIC}=3 OR {FR6600.FR26CODESTPETIC}=9) "
      End If
    End If
  End If
  
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp1
  CrystalReport1.Action = 1

Err_imp1:
End Sub

Private Sub pCargarSecciones(intDptoSel%)
Dim SQL$, qry As rdoQuery, rs As rdoResultset
Dim item As ListItem
    
    
    
    'se cargan los secciones del Dpto. seleccionado
    lvwSec.ListItems.Clear
    If optCesta(1) Then
      lvwSec.Visible = False
      Label1(1).Visible = False
      Exit Sub
    End If

    SQL = "SELECT AD41CODSECCION,AD41DESSECCION"
    SQL = SQL & " FROM AD4100"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND SYSDATE BETWEEN AD41FECINICIO AND NVL(AD41FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If Not rs.EOF Then
      Set item = lvwSec.ListItems.Add(, , "TODAS")
      item.Tag = 0
      item.Selected = True
      Set item = lvwSec.ListItems.Add(, , "Sin Secci�n")
      item.Tag = intDptoSel
      lvwSec.Visible = True
      Label1(1).Visible = True
    Else
      lvwSec.Visible = False
      Label1(1).Visible = False
    End If
    
    Do While Not rs.EOF
        Set item = lvwSec.ListItems.Add(, , rs!AD41DESSECCION)
        item.Tag = rs!AD41CODSECCION
        rs.MoveNext
    Loop
    rs.Close
    qry.Close

End Sub



