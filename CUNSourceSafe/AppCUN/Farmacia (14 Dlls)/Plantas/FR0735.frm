VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmValPedPtaFarma 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Consultar Pedido Planta."
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0735.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Consultar"
      Height          =   375
      Left            =   10440
      TabIndex        =   11
      Top             =   840
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Caption         =   "Servicio "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2415
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   11655
      Begin Crystal.CrystalReport CrystalReport1 
         Left            =   10440
         Top             =   1920
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   327680
         PrintFileLinesPerPage=   60
      End
      Begin VB.CheckBox chkFechas 
         Caption         =   "Todas las Fechas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6360
         TabIndex        =   15
         Top             =   600
         Value           =   1  'Checked
         Width           =   1935
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Pendientes + Disp.Parciales"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   14
         Top             =   1320
         Width           =   2775
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Dispensadas Parciales"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   13
         Top             =   1560
         Width           =   2415
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Enviadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   12
         Top             =   840
         Width           =   1335
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Anuladas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   10
         Top             =   2040
         Width           =   1335
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Dispensadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   9
         Top             =   1800
         Width           =   1455
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Pendientes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   8
         Top             =   1080
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.CommandButton Command1 
         Caption         =   "FILTRAR"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   10440
         TabIndex        =   7
         Top             =   1320
         Width           =   1095
      End
      Begin VB.TextBox txtServicio 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Tag             =   "Desc.Servicio"
         ToolTipText     =   "Desc.Servicio"
         Top             =   360
         Width           =   3405
      End
      Begin VB.CheckBox chkservicio 
         Caption         =   "Todos los servicios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   1680
         TabIndex        =   3
         ToolTipText     =   "Todos Servicios"
         Top             =   720
         Visible         =   0   'False
         Width           =   2055
      End
      Begin SSDataWidgets_B.SSDBCombo cboservicio 
         Height          =   315
         Left            =   240
         TabIndex        =   1
         ToolTipText     =   "C�digo Servicio"
         Top             =   360
         Width           =   975
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2223
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1720
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcredaccion 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   6360
         TabIndex        =   16
         Top             =   1200
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcEnvio 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   6360
         TabIndex        =   17
         Top             =   1800
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDispensacion 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   8280
         TabIndex        =   18
         Top             =   1800
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcPendiente 
         DataField       =   "fr28fecinicio"
         Height          =   330
         Left            =   8280
         TabIndex        =   19
         Top             =   1200
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin ComctlLib.ListView lvwSec 
         Height          =   1095
         Left            =   3120
         TabIndex        =   25
         Top             =   1200
         Visible         =   0   'False
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   1931
         View            =   3
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Secciones:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   3120
         TabIndex        =   26
         Top             =   960
         Visible         =   0   'False
         Width           =   960
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Petici�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   6360
         TabIndex        =   23
         Top             =   960
         Width           =   1290
      End
      Begin VB.Label lblEnvio 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Envio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   6360
         TabIndex        =   22
         Top             =   1560
         Width           =   1080
      End
      Begin VB.Label lblDispen 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Dispensaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   8280
         TabIndex        =   21
         Top             =   1560
         Width           =   1740
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Pendiente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Index           =   0
         Left            =   8280
         TabIndex        =   20
         Top             =   960
         Width           =   1455
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Peticiones de Planta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4935
      Index           =   2
      Left            =   120
      TabIndex        =   0
      Top             =   3000
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
         Height          =   4455
         Index           =   0
         Left            =   120
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   360
         Width           =   11370
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "NoDispensada"
         stylesets(0).BackColor=   16776960
         stylesets(0).Picture=   "FR0735.frx":000C
         stylesets(1).Name=   "Dispensada"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "FR0735.frx":0028
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20055
         _ExtentY        =   7858
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Visible         =   0   'False
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Visible         =   0   'False
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Visible         =   0   'False
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Visible         =   0   'False
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmValPedPtaFarma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0735.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA                                             *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Validar Pedido Farmacia                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim blnInload As Boolean
Dim glstrWhere As String


Private Sub auxDBGrid1_DblClick(Index As Integer)
  
  If auxdbgrid1(0).Rows > 0 Then
    Screen.MousePointer = vbHourglass
    Me.Enabled = False
    'ALMACENAR EL ESTADO DE LA PETICI�N
    gstrEst = auxdbgrid1(0).Columns("Estado").Value
    Call objsecurity.LaunchProcess("FR0736")
    gstrEst = ""
    Me.Enabled = True
    Screen.MousePointer = vbDefault
  End If

End Sub

Private Sub auxDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
    
  If auxdbgrid1(0).Columns("Estado").Value = 5 Then
    For i = 0 To 10
        auxdbgrid1(0).Columns(i).CellStyleSet "Dispensada"
    Next i
  Else
    For i = 0 To 10
        auxdbgrid1(0).Columns(i).CellStyleSet "NoDispensada"
    Next i
  End If

End Sub


Private Sub cboservicio_Click()
Dim intDptoSel As Integer

  If cboservicio <> "" Then
    intDptoSel = cboservicio
    Call pCargarSecciones(intDptoSel)
  End If

End Sub

Private Sub chkFechas_Click()
  If chkFechas = 1 Then
    dtcredaccion.Text = ""
    dtcEnvio.Text = ""
    dtcDispensacion.Text = ""
    dtcPendiente.Text = ""
  End If
  auxdbgrid1(0).RemoveAll
End Sub

Private Sub Command1_Click()
Dim strLis As String
Dim i%
Dim contSecciones As Integer
  
  If Option1(1).Value Then
    If Not IsDate(dtcDispensacion) Or cboservicio = "" Then
      MsgBox "Debe elegir Fecha de Dispensaci�n y Servicio, " & Chr(13) & "porque si no hay demasiados registros.", vbInformation, "Aviso"
      Exit Sub
    End If
  End If
  
  Screen.MousePointer = vbHourglass
  Me.Enabled = False
  glstrWhere = ""
  
  strLis = ""
  If Option1(3) Then
    strLis = "3"
  End If
  If Option1(0) Then
    strLis = "10"
  End If
  If Option1(4) Then
    strLis = "9"
  End If
  If Option1(1) Then
    strLis = "5"
  End If
  If Option1(2) Then
    strLis = "8"
  End If
  If Option1(5) Then
    strLis = "9,10"
  End If
  
  If chkservicio = 1 Then
    cboservicio.Text = ""
    txtServicio.Text = ""
    If Not Option1(5) Then
      'objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC=" & strLis
      glstrWhere = "FR5500.FR26CODESTPETIC=" & strLis & _
               " AND (FR55INDESTUPEFACIENTE=0 OR FR55INDESTUPEFACIENTE IS NULL)" & _
               " AND (FR55INDFM=0 OR FR55INDFM IS NULL)"
    Else
      'objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC in (" & strLis & ")"
      glstrWhere = "FR5500.FR26CODESTPETIC in (" & strLis & ")" & _
              " AND (FR55INDESTUPEFACIENTE=0 OR FR55INDESTUPEFACIENTE IS NULL)" & _
              " AND (FR55INDFM=0 OR FR55INDFM IS NULL)"
    End If
  Else
    If Trim(cboservicio) <> "" Then
      txtServicio.Text = cboservicio.Columns(1).Value
      If Not Option1(5) Then
        'objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC=" & strLis & " AND AD02CODDPTO=" & cboservicio.Text
        glstrWhere = "FR5500.FR26CODESTPETIC=" & strLis & " AND FR5500.AD02CODDPTO=" & cboservicio.Text & _
              " AND (FR55INDESTUPEFACIENTE=0 OR FR55INDESTUPEFACIENTE IS NULL)" & _
              " AND (FR55INDFM=0 OR FR55INDFM IS NULL)"
      Else
        'objWinInfo.objWinActiveForm.strWhere = "FR26CODESTPETIC in (" & strLis & ") AND AD02CODDPTO=" & cboservicio.Text
        glstrWhere = "FR5500.FR26CODESTPETIC in (" & strLis & ") AND FR5500.AD02CODDPTO=" & cboservicio.Text & _
              " AND (FR55INDESTUPEFACIENTE=0 OR FR55INDESTUPEFACIENTE IS NULL)" & _
              " AND (FR55INDFM=0 OR FR55INDFM IS NULL)"
      End If
    Else
      'objWinInfo.objWinActiveForm.strWhere = "-1=0"
      glstrWhere = "-1=0"
    End If
  End If
  
  If chkFechas.Value = 0 Then
    If IsDate(dtcredaccion) Then
      'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR5500.FR55FECPETICION)=TO_DATE('" & dtcredaccion & "','DD/MM/YYYY')"
      glstrWhere = glstrWhere & " AND TRUNC(FR5500.FR55FECPETICION)=TO_DATE('" & dtcredaccion & "','DD/MM/YYYY')"
    End If
    If IsDate(dtcEnvio) Then
      'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR5500.FR55FECENVIO)=TO_DATE('" & dtcEnvio & "','DD/MM/YYYY')"
      glstrWhere = glstrWhere & " AND TRUNC(FR5500.FR55FECENVIO)=TO_DATE('" & dtcEnvio & "','DD/MM/YYYY')"
    End If
    If IsDate(dtcPendiente) Then
      'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR5500.FR55FECPEND)=TO_DATE('" & dtcPendiente & "','DD/MM/YYYY')"
      glstrWhere = glstrWhere & " AND TRUNC(FR5500.FR55FECPEND)=TO_DATE('" & dtcPendiente & "','DD/MM/YYYY')"
    End If
    If IsDate(dtcDispensacion) Then
      'objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND TRUNC(FR5500.FR55FECDISPEN)=TO_DATE('" & dtcDispensacion & "','DD/MM/YYYY')"
      glstrWhere = glstrWhere & " AND TRUNC(FR5500.FR55FECDISPEN)=TO_DATE('" & dtcDispensacion & "','DD/MM/YYYY')"
    End If
  End If
  
  
   'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If lvwSec.ListItems.Count > 0 Then
    If lvwSec.ListItems(1).Selected Then
    Else
      For i = 2 To lvwSec.ListItems.Count
        If lvwSec.ListItems(i).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            glstrWhere = glstrWhere & " AND ("
          Else
            glstrWhere = glstrWhere & " OR "
          End If
          If i = 2 Then
            glstrWhere = glstrWhere & "FR5500.AD41CODSECCION IS NULL"
          Else
            glstrWhere = glstrWhere & "FR5500.AD41CODSECCION=" & lvwSec.ListItems(i).Tag
          End If
        End If
      Next i
      If contSecciones > 0 Then
        glstrWhere = glstrWhere & ") "
      End If
    End If
  End If
  
  Call Refrescar_Grid
  'objWinInfo.DataRefresh
  
  If Option1(3) Then
    If auxdbgrid1(0).Rows > 0 Then
      'Command3.Enabled = True
    End If
  Else
    'Command3.Enabled = False
  End If
  
  If Option1(0).Value = True Or Option1(4).Value = True Or Option1(5).Value = True Then
    If auxdbgrid1(0).Rows > 0 Then
      tlbToolbar1.Buttons(6).Enabled = True
    Else
      tlbToolbar1.Buttons(6).Enabled = False
    End If
  Else
    If Option1(1).Value = True And IsDate(dtcDispensacion) And cboservicio <> "" Then
      If auxdbgrid1(0).Rows > 0 Then
        tlbToolbar1.Buttons(6).Enabled = True
      Else
        tlbToolbar1.Buttons(6).Enabled = False
      End If
    Else
      tlbToolbar1.Buttons(6).Enabled = False
    End If
  End If
  
  Me.Enabled = True
  Screen.MousePointer = vbDefault

End Sub

Private Sub Command2_Click()
  'ALMACENAR EL ESTADO DE LA PETICI�N
  gstrEst = auxdbgrid1(0).Columns("Estado").Value
  Call objsecurity.LaunchProcess("FR0736")
  gstrEst = ""
    
'  If grdDBGrid1(1).Rows = 0 Then
'    Screen.MousePointer = vbDefault
'    Exit Sub
'  End If
'  Screen.MousePointer = vbHourglass
'
'  'ALMACENAR EL ESTADO DE LA PETICI�N
'  gstrEst = ""
'  If Option1(3) Then
'    gstrEst = "3"
'  End If
'  If Option1(0) Then
'    gstrEst = "10"
'  End If
'  If Option1(4) Then
'    gstrEst = "9"
'  End If
'  If Option1(1) Then
'    gstrEst = "5"
'  End If
'  If Option1(2) Then
'    gstrEst = "8"
'  End If
'  If Option1(5) Then
'    gstrEst = "9,10"
'  End If
'  gintservicio = grdDBGrid1(1).Columns("C�d.Servicio").Value
'  Call objsecurity.LaunchProcess("FR0136")
'  gstrEst = ""
'  gintservicio = -1
'  Screen.MousePointer = vbDefault

End Sub


Private Sub dtcDispensacion_Change()
  
  If chkFechas = 1 Then
    chkFechas = 0
  End If
  auxdbgrid1(0).RemoveAll
  
End Sub

Private Sub dtcDispensacion_CloseUp()
  
  If chkFechas = 1 Then
    chkFechas = 0
  End If
  auxdbgrid1(0).RemoveAll
  
End Sub

Private Sub dtcEnvio_Change()
  
  If chkFechas = 1 Then
    chkFechas = 0
  End If
  auxdbgrid1(0).RemoveAll
  
End Sub

Private Sub dtcEnvio_CloseUp()
  
  If chkFechas = 1 Then
    chkFechas = 0
  End If
  auxdbgrid1(0).RemoveAll
  
End Sub

Private Sub dtcPendiente_Change()
  
  If chkFechas = 1 Then
    chkFechas = 0
  End If
  auxdbgrid1(0).RemoveAll
  
End Sub

Private Sub dtcPendiente_CloseUp()
  
  If chkFechas = 1 Then
    chkFechas = 0
  End If
  auxdbgrid1(0).RemoveAll
  
End Sub

Private Sub dtcredaccion_Change()
  
  If chkFechas = 1 Then
    chkFechas = 0
  End If
  auxdbgrid1(0).RemoveAll
  
End Sub

Private Sub dtcredaccion_CloseUp()
  
  If chkFechas = 1 Then
    chkFechas = 0
  End If
  auxdbgrid1(0).RemoveAll
  
End Sub


Private Sub lvwSec_BeforeLabelEdit(Cancel As Integer)
Dim i%
  'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  If lvwSec.ListItems(1).Selected Then
      For i = 2 To lvwSec.ListItems.Count
          lvwSec.ListItems(i).Selected = False
      Next i
  End If
  auxdbgrid1(0).RemoveAll
End Sub

Private Sub Option1_Click(Index As Integer)
  
  'If Index = 3 Then
  '  Command2.Enabled = False
  'Else
  '  Command2.Enabled = True
  'End If
  auxdbgrid1(0).RemoveAll

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


Private Sub cboservicio_CloseUp()
Dim intDptoSel As Integer

If Trim(cboservicio.Text) <> "" Then
  If chkservicio.Value = 0 Then
    txtServicio.Text = cboservicio.Columns(1).Value
    If cboservicio <> "" Then
      intDptoSel = cboservicio
      Call pCargarSecciones(intDptoSel)
    End If
  Else 'todos los servicios
    txtServicio.Text = cboservicio.Columns(1).Value
    chkservicio.Value = 0
  End If
Else
  txtServicio.Text = ""
  chkservicio.Value = 1
End If
auxdbgrid1(0).RemoveAll

End Sub



Private Sub chkservicio_Click()
  
  If chkservicio = 1 Then
    cboservicio.Text = ""
    txtServicio.Text = ""
  Else
    If Trim(cboservicio) <> "" Then
      txtServicio.Text = cboservicio.Columns(1).Value
    End If
  End If
  auxdbgrid1(0).RemoveAll

End Sub


Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset

  If blnInload Then
    blnInload = False
    'cboservicio.RemoveAll
    'stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
    '       "WHERE SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) " & _
    '       " order by AD02DESDPTO "
    'Set rsta = objApp.rdoConnect.OpenResultset(stra)
    'While (Not rsta.EOF)
    '    Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
    '    rsta.MoveNext
    'Wend
    'rsta.Close
    'Set rsta = Nothing
    
    stra = "SELECT AD0200.AD02CODDPTO,AD0200.AD02DESDPTO FROM AD0300,AD0200 WHERE SG02COD='" & objsecurity.strUser & "'"
    stra = stra & " AND AD0300.AD02CODDPTO=AD0200.AD02CODDPTO AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')) "
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not rsta.EOF Then
      cboservicio.Value = rsta(0).Value
      txtServicio.Text = rsta(1).Value
    End If
    cboservicio.RemoveAll
    While (Not rsta.EOF)
        Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
        rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
    If cboservicio <> "" Then
      Call pCargarSecciones(cboservicio)
    End If
  End If
    
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
'Dim objMultiInfo As New clsCWForm
'Dim strKey As String
Dim j As Integer

blnInload = True
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
'    With objMultiInfo
'        .strName = "Pedidos a Firmar"
'        Set .objFormContainer = fraframe1(2)
'        Set .objFatherContainer = Nothing
'        Set .tabMainTab = Nothing
'        Set .grdGrid = grdDBGrid1(1)
'        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
'        .strTable = "FR5500"
'        .intAllowance = cwAllowReadOnly
'        .strWhere = "-1=0"
'
'        Call .FormAddOrderField("FR55FECPETICION", cwDescending)
'        Call .FormAddOrderField("FR55CODNECESUNID", cwDescending)
'
'        strKey = .strDataBase & .strTable
'        Call .FormCreateFilterWhere(strKey, "Pedidos Farmacia")
'        Call .FormAddFilterWhere(strKey, "FR55CODNECESUNID", "C�digo Petici�n", cwNumeric)
'        Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�d. Dispensa", cwString)
'        Call .FormAddFilterWhere(strKey, "FR55FECPETICION", "Fecha Petici�n", cwDate)
'        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
'    End With

'    With objWinInfo
'
'        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
'
'        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
'        Call .GridAddColumn(objMultiInfo, "C�digo Petici�n", "FR55CODNECESUNID", cwNumeric, 9)
'        Call .GridAddColumn(objMultiInfo, "Estado", "FR26CODESTPETIC", cwNumeric, 1)
'        Call .GridAddColumn(objMultiInfo, "Desc.Estado", "", cwString, 30)
'        Call .GridAddColumn(objMultiInfo, "Fecha", "FR55FECPETICION", cwDate)
'        Call .GridAddColumn(objMultiInfo, "C�d.Servicio", "AD02CODDPTO", cwNumeric, 3)
'        Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 30)
'        Call .GridAddColumn(objMultiInfo, "C�d.Dispensa", "SG02COD_PDS", cwString, 6)
'        Call .GridAddColumn(objMultiInfo, "Dispensado por ", "", cwString, 30)
'
'        Call .FormCreateInfo(objMultiInfo)
'
'        Call .FormChangeColor(objMultiInfo)
'
'        'Se indican los campos por los que se desea buscar
'
'        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
'        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(5), "FR26DESESTADOPET")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), grdDBGrid1(1).Columns(10), "SG02APE1")
'
'        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(7)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
'        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(7)), grdDBGrid1(1).Columns(8), "AD02DESDPTO")
'
'        Call .WinRegister
'        Call .WinStabilize
'    End With
'    grdDBGrid1(1).Columns(0).Visible = False
'    grdDBGrid1(1).Columns(4).Visible = False 'c�d.estado
'    'grdDBGrid1(1).Columns(5).Visible = False 'desc.estado
'    grdDBGrid1(1).Columns(3).Width = 1400 'c�d petici�n
'    grdDBGrid1(1).Columns(5).Width = 1600 'estado
'    grdDBGrid1(1).Columns(6).Width = 1200 'fecha
'    grdDBGrid1(1).Columns(9).Width = 1200 'doctor
'    grdDBGrid1(1).Columns(10).Width = 2900
'    grdDBGrid1(1).Columns(7).Width = 1200 'servicio
'    grdDBGrid1(1).Columns(8).Width = 2900 'servicio
    
gstrEst = ""
auxdbgrid1(0).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
For j = 0 To 10
  Call auxdbgrid1(0).Columns.Add(j)
Next j

auxdbgrid1(0).Columns(0).Caption = "Petici�n"
auxdbgrid1(0).Columns(0).Visible = True
auxdbgrid1(0).Columns(0).Locked = True
auxdbgrid1(0).Columns(1).Caption = "Estado"
auxdbgrid1(0).Columns(1).Visible = False
auxdbgrid1(0).Columns(1).Locked = True
auxdbgrid1(0).Columns(2).Caption = "Desc.Estado"
auxdbgrid1(0).Columns(2).Visible = True
auxdbgrid1(0).Columns(2).Locked = True
auxdbgrid1(0).Columns(3).Caption = "Fecha"
auxdbgrid1(0).Columns(3).Visible = True
auxdbgrid1(0).Columns(3).Locked = True
auxdbgrid1(0).Columns(4).Caption = "C�d.Servicio"
auxdbgrid1(0).Columns(4).Visible = True
auxdbgrid1(0).Columns(4).Locked = True
auxdbgrid1(0).Columns(5).Caption = "Servicio"
auxdbgrid1(0).Columns(5).Visible = True
auxdbgrid1(0).Columns(5).Locked = True
''''''''''''''''''''''''''''''''''''''''''''''''''
auxdbgrid1(0).Columns(6).Caption = "C�d.Secci�n"
auxdbgrid1(0).Columns(6).Visible = False
auxdbgrid1(0).Columns(6).Locked = True
auxdbgrid1(0).Columns(7).Caption = "Secci�n"
auxdbgrid1(0).Columns(7).Visible = True
auxdbgrid1(0).Columns(7).Locked = True
''''''''''''''''''''''''''''''''''''''''''''''''''
auxdbgrid1(0).Columns(8).Caption = "C�d.Dispensa"
auxdbgrid1(0).Columns(8).Visible = True
auxdbgrid1(0).Columns(8).Locked = True
auxdbgrid1(0).Columns(9).Caption = "Dispensado por "
auxdbgrid1(0).Columns(9).Visible = True
auxdbgrid1(0).Columns(9).Locked = True
auxdbgrid1(0).Columns(10).Caption = "Fec.Envio"
auxdbgrid1(0).Columns(10).Visible = True
auxdbgrid1(0).Columns(10).Locked = True
auxdbgrid1(0).Columns(11).Caption = "Fec.Pend."
auxdbgrid1(0).Columns(11).Visible = True
auxdbgrid1(0).Columns(11).Locked = True
auxdbgrid1(0).Columns(12).Caption = "Fec.Dispen."
auxdbgrid1(0).Columns(12).Visible = True
auxdbgrid1(0).Columns(12).Locked = True
auxdbgrid1(0).Redraw = True

auxdbgrid1(0).Columns("Petici�n").Width = 800 'c�d petici�n
auxdbgrid1(0).Columns("Desc.Estado").Width = 1450 'estado
auxdbgrid1(0).Columns("Fecha").Width = 1200 'fecha
auxdbgrid1(0).Columns("C�d.Dispensa").Width = 1200 'doctor
auxdbgrid1(0).Columns("Dispensado por ").Width = 2900
auxdbgrid1(0).Columns("C�d.Servicio").Width = 400 'servicio
auxdbgrid1(0).Columns("Servicio").Width = 2900 'servicio

auxdbgrid1(0).Columns("Fec.Envio").Position = 4
auxdbgrid1(0).Columns("Fec.Pend.").Position = 5
auxdbgrid1(0).Columns("Fec.Dispen.").Position = 6

For j = 1 To 29
tlbToolbar1.Buttons(j).Enabled = False
Next j

'se cargan las secciones del Dpto. seleccionado
lvwSec.ColumnHeaders.Add , , , 2000

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    'intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    'intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    'intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    'Call objWinInfo.WinDeRegister
    'Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    'Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
'    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

  '30 Salir
  If btnButton.Index = 30 Then
    Unload Me
  ElseIf btnButton.Index = 6 Then
    If Option1(0).Value = True Or Option1(4).Value = True Or Option1(5).Value = True Then
      If lvwSec.ListItems.Count > 0 Then
        Call Imprimir("FR1354.RPT", 0)
      Else
        Call Imprimir("FR1352.RPT", 0)
      End If
    Else
      If lvwSec.ListItems.Count > 0 Then
        Call Imprimir("FR1353.RPT", 0)
      Else
        Call Imprimir("FR1351.RPT", 0)
      End If
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 60 'Eliminar
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
    Unload Me
  Case Else
    'Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    'Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    'Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    'Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    'Call objWinInfo.CtrlDataChange
End Sub


Private Sub Refrescar_Grid()
Dim strselect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim mvntAcumulado As Variant
Dim mvntAcumuladoDisp As Variant
Dim mvntAcumuladoOri As Variant
Dim mvntProducto As Variant
Dim i As Integer
Dim strsec As String
Dim rstsec As rdoResultset
Dim codigoseccion
Dim seccion

  
Me.Enabled = False
Screen.MousePointer = vbHourglass

auxdbgrid1(0).Redraw = False
auxdbgrid1(0).RemoveAll
  
strselect = "SELECT "
strselect = strselect & " FR5500.FR55CODNECESUNID"
strselect = strselect & ",FR5500.FR26CODESTPETIC"
strselect = strselect & ",FR2600.FR26DESESTADOPET"
strselect = strselect & ",FR5500.FR55FECPETICION"
strselect = strselect & ",FR5500.AD02CODDPTO"
strselect = strselect & ",AD0200.AD02DESDPTO"
strselect = strselect & ",FR5500.SG02COD_PDS"
strselect = strselect & ",SG0200.SG02APE1"
strselect = strselect & ",FR55FECENVIO"
strselect = strselect & ",FR55FECPEND"
strselect = strselect & ",FR55FECDISPEN"

strselect = strselect & " FROM FR5500,FR2600,SG0200,AD0200"
strselect = strselect & " WHERE "

strselect = strselect & " FR5500.FR26CODESTPETIC=FR2600.FR26CODESTPETIC"
strselect = strselect & " AND FR5500.SG02COD_PDS=SG0200.SG02COD(+)"
strselect = strselect & " AND FR5500.AD02CODDPTO=AD0200.AD02CODDPTO"
strselect = strselect & " AND (FR5500.FR55INDESTUPEFACIENTE=0 OR FR5500.FR55INDESTUPEFACIENTE IS NULL)"
strselect = strselect & " AND (FR5500.FR55INDFM=0 OR FR5500.FR55INDFM IS NULL)"

strselect = strselect & " AND " & glstrWhere

strselect = strselect & " ORDER BY FR55FECPETICION DESC,FR55CODNECESUNID DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strselect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF

  '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   If Not IsNull(rsta("FR55CODNECESUNID").Value) Then
    strsec = "SELECT FR5500.AD41CODSECCION,AD4100.AD41DESSECCION" & _
             " FROM FR5500,AD4100 WHERE " & _
             "FR5500.AD41CODSECCION=AD4100.AD41CODSECCION" & " AND " & _
             "FR5500.FR55CODNECESUNID=" & rsta("FR55CODNECESUNID").Value
    Set rstsec = objApp.rdoConnect.OpenResultset(strsec)
    If Not rstsec.EOF Then
        If Not IsNull(rstsec.rdoColumns("AD41CODSECCION").Value) _
            And Not IsNull(rstsec.rdoColumns("AD41DESSECCION").Value) Then
                    codigoseccion = rstsec.rdoColumns("AD41CODSECCION").Value
                    seccion = rstsec.rdoColumns("AD41DESSECCION").Value
        Else
                    codigoseccion = ""
                    seccion = ""
        End If
    Else
      codigoseccion = ""
      seccion = ""
    End If
    rstsec.Close
    Set rstsec = Nothing
  End If
   
  '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  strlinea = rsta("FR55CODNECESUNID").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR26CODESTPETIC").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR26DESESTADOPET").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR55FECPETICION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD02CODDPTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD02DESDPTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & codigoseccion & Chr(vbKeyTab)
  strlinea = strlinea & seccion & Chr(vbKeyTab)
  strlinea = strlinea & rsta("SG02COD_PDS").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("SG02APE1").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR55FECENVIO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR55FECPEND").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR55FECDISPEN").Value
  auxdbgrid1(0).AddItem strlinea
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
auxdbgrid1(0).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Imprimir(strListado As String, intDes As Integer)
'JMRL 19991125
'Toma como par�metro el listado a imprimir y lo manda a la impresora;
'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
Dim strWhere As String
Dim strDNS As String
Dim strUser As String
Dim strPass As String
Dim strPATH As String
Dim contSecciones As Integer
Dim i As Integer

  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  
  If lvwSec.ListItems.Count Then
    strWhere = "{FR5501J.AD02CODDPTO}=" & cboservicio
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    contSecciones = 0
    If lvwSec.ListItems.Count > 0 Then
      If lvwSec.ListItems(1).Selected Then
      Else
        For i = 2 To lvwSec.ListItems.Count
          If lvwSec.ListItems(i).Selected = True Then
            If contSecciones = 0 Then
              contSecciones = contSecciones + 1
              strWhere = strWhere & " AND ("
            Else
              strWhere = strWhere & " OR "
            End If
            If i = 2 Then
              strWhere = strWhere & "isnull({FR5501J.AD41CODSECCION})"
            Else
              strWhere = strWhere & "{FR5501J.AD41CODSECCION}=" & lvwSec.ListItems(i).Tag
            End If
          End If
        Next i
        If contSecciones > 0 Then
          strWhere = strWhere & ") "
        End If
      End If
    End If
    If IsDate(dtcredaccion) Then
      strWhere = strWhere & " AND ToText({FR5501J.FR55FECPETICION}) ='" & dtcredaccion & "'"
    End If
    If IsDate(dtcEnvio) Then
      strWhere = strWhere & " AND ToText({FR5501J.FR55FECENVIO}) ='" & dtcEnvio & "'"
    End If
    If IsDate(dtcPendiente) Then
      strWhere = strWhere & " AND ToText({FR5501J.FR55FECPEND}) ='" & dtcPendiente & "'"
    End If
    If IsDate(dtcDispensacion) Then
      strWhere = strWhere & " AND ToText({FR5501J.FR55FECDISPEN}) ='" & dtcDispensacion & "'"
    End If
    If Option1(0).Value Then '10
      strWhere = strWhere & " AND {FR5501J.FR26CODESTPETIC}=10"
    ElseIf Option1(4).Value Then '9
      strWhere = strWhere & " AND {FR5501J.FR26CODESTPETIC}=9"
    ElseIf Option1(5).Value Then '9 O 10
      strWhere = strWhere & " AND ({FR5501J.FR26CODESTPETIC}=9 OR {FR5501J.FR26CODESTPETIC}=10)"
    End If
    If strListado = "FR1353.RPT" Then
      strWhere = strWhere & " AND ({FR5501J.FR26CODESTPETIC}=5 or {FR5501J.FR26CODESTPETIC}=9)"
      strWhere = strWhere & " AND {FR5501J.FR19INDDISPENSADO}=-1 "
    Else
      strWhere = strWhere & " AND ({FR5501J.FR19INDDISPENSADO}=0 or isnull({FR5501J.FR19INDDISPENSADO}))"
    End If
  Else
    strWhere = "{AD0200.AD02CODDPTO}=" & cboservicio & " AND {FR1900.FR19INDBLOQ}=0 " & _
             " AND ({FR55INDESTUPEFACIENTE}=0 OR {FR55INDESTUPEFACIENTE} IS NULL)" & _
             " AND ({FR5500.FR55INDFM}=0 OR {FR5500.FR55INDFM} IS NULL)"
    If IsDate(dtcredaccion) Then
      strWhere = strWhere & " AND ToText({FR5500.FR55FECPETICION}) ='" & dtcredaccion & "'"
    End If
    If IsDate(dtcEnvio) Then
      strWhere = strWhere & " AND ToText({FR5500.FR55FECENVIO}) ='" & dtcEnvio & "'"
    End If
    If IsDate(dtcPendiente) Then
      strWhere = strWhere & " AND ToText({FR5500.FR55FECPEND}) ='" & dtcPendiente & "'"
    End If
    If IsDate(dtcDispensacion) Then
      strWhere = strWhere & " AND ToText({FR5500.FR55FECDISPEN}) ='" & dtcDispensacion & "'"
    End If
    If Option1(0).Value Then '10
      strWhere = strWhere & " AND {FR5500.FR26CODESTPETIC}=10"
    ElseIf Option1(4).Value Then '9
      strWhere = strWhere & " AND {FR5500.FR26CODESTPETIC}=9"
    ElseIf Option1(5).Value Then '9 O 10
      strWhere = strWhere & " AND ({FR5500.FR26CODESTPETIC}=9 OR {FR5500.FR26CODESTPETIC}=10)"
    End If
  End If
             
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp3
  CrystalReport1.Action = 1
Err_imp3:

End Sub

Private Sub pCargarSecciones(intDptoSel%)
Dim SQL$, qry As rdoQuery, rs As rdoResultset
Dim item As ListItem
    
    'se cargan los secciones del Dpto. seleccionado
    lvwSec.ListItems.Clear

    SQL = "SELECT AD41CODSECCION,AD41DESSECCION"
    SQL = SQL & " FROM AD4100"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND SYSDATE BETWEEN AD41FECINICIO AND NVL(AD41FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If Not rs.EOF Then
      Set item = lvwSec.ListItems.Add(, , "TODAS")
      item.Tag = 0
      item.Selected = True
      Set item = lvwSec.ListItems.Add(, , "Sin Secci�n")
      item.Tag = intDptoSel
      Label1(1).Visible = True
      lvwSec.Visible = True
    Else
      Label1(1).Visible = False
      lvwSec.Visible = False
    End If
    
    Do While Not rs.EOF
        Set item = lvwSec.ListItems.Add(, , rs!AD41DESSECCION)
        item.Tag = rs!AD41CODSECCION
        rs.MoveNext
    Loop
    rs.Close
    qry.Close

End Sub



