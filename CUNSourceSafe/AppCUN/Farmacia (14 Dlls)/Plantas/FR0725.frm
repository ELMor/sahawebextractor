VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmEstPetFarm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Estudiar Petici�n Farmacia PRN"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdBloquear 
      Caption         =   "Quitar Productos"
      Height          =   615
      Left            =   10560
      TabIndex        =   114
      Top             =   5760
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "Anular"
      Height          =   615
      Left            =   10560
      TabIndex        =   113
      Top             =   1680
      Visible         =   0   'False
      Width           =   1095
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   10920
      Top             =   6480
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Productos"
      Height          =   375
      Left            =   0
      TabIndex        =   43
      Top             =   0
      Width           =   855
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Index           =   1
      Left            =   120
      TabIndex        =   34
      Top             =   480
      Width           =   10260
      Begin TabDlg.SSTab tabTab1 
         Height          =   2775
         Index           =   0
         Left            =   120
         TabIndex        =   35
         TabStop         =   0   'False
         Top             =   360
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   4895
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0725.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(28)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(14)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(13)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "tab1"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkCheck1(12)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "chkCheck1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "chkCheck1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "chkCheck1(7)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "chkCheck1(15)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(25)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(26)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "chkCheck1(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0725.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grddbgrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Cesta"
            DataField       =   "FR66INDCESTA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   7560
            TabIndex        =   30
            Tag             =   "Cesta?"
            Top             =   360
            Width           =   1695
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   26
            Left            =   3720
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   1605
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   25
            Left            =   3360
            TabIndex        =   2
            Tag             =   "Estado Petici�n"
            Top             =   360
            Width           =   315
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Inter�s Cient�fico"
            DataField       =   "FR66INDINTERCIENT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   7560
            TabIndex        =   29
            Tag             =   "Inter�s Cient�fico?"
            Top             =   120
            Width           =   1815
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Restricci�n volumen"
            DataField       =   "FR66INDRESTVOLUM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   5520
            TabIndex        =   28
            Tag             =   "Restricci�n de Volumen?"
            Top             =   600
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Al�rgico"
            DataField       =   "FR66INDALERGIA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   5520
            TabIndex        =   27
            Tag             =   "Al�rgico?"
            Top             =   360
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Diab�tico"
            DataField       =   "FR66INDPACIDIABET"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   5520
            TabIndex        =   26
            Tag             =   "Diab�tico?"
            Top             =   120
            Width           =   1575
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Orden M�dica"
            DataField       =   "FR66INDOM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   7560
            TabIndex        =   31
            Tag             =   "Orden M�dica?"
            Top             =   600
            Width           =   1695
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR66CODOMORIPRN"
            Height          =   330
            Index           =   1
            Left            =   1440
            TabIndex        =   1
            Tag             =   "C�digo OM origen PRN"
            Top             =   360
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR66CODPETICION"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Petici�n"
            Top             =   360
            Width           =   1100
         End
         Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
            Height          =   2505
            Index           =   2
            Left            =   -74880
            TabIndex        =   37
            TabStop         =   0   'False
            Top             =   120
            Width           =   9375
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16536
            _ExtentY        =   4419
            _StockProps     =   79
         End
         Begin TabDlg.SSTab tab1 
            Height          =   1815
            Left            =   120
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   840
            Width           =   9375
            _ExtentX        =   16536
            _ExtentY        =   3201
            _Version        =   327681
            Style           =   1
            Tabs            =   5
            TabsPerRow      =   5
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Paciente"
            TabPicture(0)   =   "FR0725.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(9)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(8)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(7)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(4)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(11)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(3)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(42)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(40)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(39)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(16)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(41)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(5)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(4)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(2)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(3)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(23)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(13)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "Text1"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(14)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(15)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(46)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "Text2"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).ControlCount=   22
            TabCaption(1)   =   "M�dico / Enfermera"
            TabPicture(1)   =   "FR0725.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(24)"
            Tab(1).Control(1)=   "txtText1(6)"
            Tab(1).Control(2)=   "txtText1(7)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "txtText1(8)"
            Tab(1).Control(4)=   "txtText1(9)"
            Tab(1).Control(5)=   "txtText1(20)"
            Tab(1).Control(6)=   "txtText1(21)"
            Tab(1).Control(7)=   "txtText1(19)"
            Tab(1).Control(7).Enabled=   0   'False
            Tab(1).Control(8)=   "dtcDateCombo1(1)"
            Tab(1).Control(9)=   "dtcDateCombo1(0)"
            Tab(1).Control(10)=   "lblLabel1(12)"
            Tab(1).Control(11)=   "lblLabel1(27)"
            Tab(1).Control(12)=   "lblLabel1(20)"
            Tab(1).Control(13)=   "lblLabel1(5)"
            Tab(1).Control(14)=   "lblLabel1(18)"
            Tab(1).Control(15)=   "lblLabel1(2)"
            Tab(1).Control(16)=   "lblLabel1(22)"
            Tab(1).ControlCount=   17
            TabCaption(2)   =   "Redacci�n"
            TabPicture(2)   =   "FR0725.frx":0070
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "txtText1(22)"
            Tab(2).Control(1)=   "txtText1(11)"
            Tab(2).Control(1).Enabled=   0   'False
            Tab(2).Control(2)=   "txtText1(10)"
            Tab(2).Control(3)=   "dtcDateCombo1(2)"
            Tab(2).Control(4)=   "lblLabel1(6)"
            Tab(2).Control(5)=   "lblLabel1(10)"
            Tab(2).Control(6)=   "lblLabel1(0)"
            Tab(2).ControlCount=   7
            TabCaption(3)   =   "Servicio"
            TabPicture(3)   =   "FR0725.frx":008C
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "txtText1(61)"
            Tab(3).Control(0).Enabled=   0   'False
            Tab(3).Control(1)=   "txtText1(62)"
            Tab(3).Control(1).Enabled=   0   'False
            Tab(3).Control(2)=   "txtText1(17)"
            Tab(3).Control(2).Enabled=   0   'False
            Tab(3).Control(3)=   "txtText1(16)"
            Tab(3).Control(3).Enabled=   0   'False
            Tab(3).Control(4)=   "lblLabel1(46)"
            Tab(3).Control(4).Enabled=   0   'False
            Tab(3).Control(5)=   "lblLabel1(1)"
            Tab(3).Control(5).Enabled=   0   'False
            Tab(3).ControlCount=   6
            TabCaption(4)   =   "Observaciones"
            TabPicture(4)   =   "FR0725.frx":00A8
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "txtText1(18)"
            Tab(4).Control(1)=   "lblLabel1(24)"
            Tab(4).ControlCount=   2
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO_CRG"
               Height          =   330
               Index           =   61
               Left            =   -69720
               TabIndex        =   116
               Tag             =   "C�d.Dpto.Cargo"
               Top             =   840
               Width           =   360
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   62
               Left            =   -69240
               TabIndex        =   115
               Tag             =   "Dpto.Cargo"
               Top             =   840
               Width           =   3405
            End
            Begin VB.TextBox Text2 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Left            =   8040
               TabIndex        =   106
               Tag             =   "Superficie Corporal"
               Top             =   720
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   46
               Left            =   5280
               TabIndex        =   105
               TabStop         =   0   'False
               Tag             =   "Cama"
               Top             =   720
               Width           =   900
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR66PESOPAC"
               Height          =   330
               Index           =   15
               Left            =   6360
               MaxLength       =   3
               TabIndex        =   104
               Tag             =   "Peso"
               Top             =   720
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR66ALTUPAC"
               Height          =   330
               Index           =   14
               Left            =   7080
               MaxLength       =   3
               TabIndex        =   103
               Tag             =   "Altura Cm"
               Top             =   720
               Width           =   600
            End
            Begin VB.TextBox Text1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Left            =   3360
               TabIndex        =   102
               TabStop         =   0   'False
               Tag             =   "Edad"
               Top             =   720
               Width           =   420
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   13
               Left            =   3960
               TabIndex        =   101
               TabStop         =   0   'False
               Tag             =   "Sexo"
               Top             =   720
               Width           =   1140
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR91CODURGENCIA"
               Height          =   330
               Index           =   22
               Left            =   -71160
               TabIndex        =   21
               Tag             =   "C�digo Urgencia"
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   11
               Left            =   -70080
               TabIndex        =   22
               TabStop         =   0   'False
               Tag             =   "Desc. Urgencia"
               Top             =   960
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAREDACCI"
               Height          =   330
               Index           =   10
               Left            =   -72840
               TabIndex        =   20
               Tag             =   "Hora Redacci�n"
               Top             =   960
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   -73320
               TabIndex        =   24
               Tag             =   "Desc.Departamento"
               Top             =   840
               Width           =   3480
            End
            Begin VB.TextBox txtText1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   16
               Left            =   -74760
               TabIndex        =   23
               Tag             =   "C�d.Departamento"
               Top             =   840
               Width           =   1275
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66OBSERV"
               Height          =   1050
               Index           =   18
               Left            =   -74880
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   25
               Tag             =   "Observaciones"
               Top             =   600
               Width           =   8925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_FEN"
               Height          =   330
               Index           =   24
               Left            =   -66720
               TabIndex        =   18
               Tag             =   "Hora Firma Enfermera"
               Top             =   1440
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   23
               Left            =   1800
               TabIndex        =   5
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   720
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   3
               Left            =   240
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Nombre Paciente"
               Top             =   1320
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   2
               Left            =   240
               TabIndex        =   4
               Tag             =   "C�digo Paciente"
               Top             =   720
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   4
               Left            =   3240
               TabIndex        =   7
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Paciente"
               Top             =   1320
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   6240
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Apellido 2� Paciente"
               Top             =   1320
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "sg02cod_enf"
               Height          =   330
               Index           =   6
               Left            =   -74880
               TabIndex        =   13
               Tag             =   "C�digo Enfermera"
               Top             =   1200
               Width           =   1635
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   7
               Left            =   -73200
               TabIndex        =   14
               TabStop         =   0   'False
               Tag             =   "Nombre Enfermera"
               Top             =   1200
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_MED"
               Height          =   330
               Index           =   8
               Left            =   -74880
               TabIndex        =   9
               Tag             =   "C�digo Doctor"
               Top             =   600
               Width           =   1635
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   9
               Left            =   -73200
               TabIndex        =   10
               Tag             =   "Apellido Doctor"
               Top             =   600
               Width           =   3285
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMMEDI"
               Height          =   330
               Index           =   20
               Left            =   -67680
               TabIndex        =   12
               Tag             =   "hora Firma M�dico"
               Top             =   600
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR66HORAFIRMENF"
               Height          =   330
               Index           =   21
               Left            =   -67680
               TabIndex        =   17
               Tag             =   "Hora Firma Enfermera"
               Top             =   1200
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   19
               Left            =   -71520
               TabIndex        =   15
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Enfermera"
               Top             =   1200
               Width           =   1605
            End
            Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
               Height          =   4425
               Index           =   3
               Left            =   -74880
               TabIndex        =   40
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   7805
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
               Height          =   1305
               Index           =   4
               Left            =   -74880
               TabIndex        =   41
               TabStop         =   0   'False
               Top             =   120
               Width           =   9855
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17383
               _ExtentY        =   2302
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMMEDI"
               Height          =   330
               Index           =   1
               Left            =   -69840
               TabIndex        =   11
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   600
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECFIRMENF"
               Height          =   330
               Index           =   0
               Left            =   -69840
               TabIndex        =   16
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   1200
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR66FECREDACCION"
               Height          =   330
               Index           =   2
               Left            =   -74880
               TabIndex        =   19
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   960
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento de Cargo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   46
               Left            =   -69720
               TabIndex        =   117
               Top             =   600
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Sup Corporal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   41
               Left            =   8040
               TabIndex        =   112
               Top             =   480
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cama"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   5280
               TabIndex        =   111
               Top             =   480
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Peso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   39
               Left            =   6360
               TabIndex        =   110
               Top             =   480
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Altura Cm"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   40
               Left            =   7080
               TabIndex        =   109
               Top             =   480
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Edad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   42
               Left            =   3360
               TabIndex        =   108
               Top             =   480
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Sexo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   3960
               TabIndex        =   107
               Top             =   480
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Urgencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   -71160
               TabIndex        =   61
               Top             =   720
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   -72840
               TabIndex        =   60
               Top             =   720
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Redacci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   -74880
               TabIndex        =   59
               Top             =   720
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -74760
               TabIndex        =   58
               Top             =   600
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   -74880
               TabIndex        =   57
               Top             =   360
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.Firma"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   -66720
               TabIndex        =   55
               Top             =   1200
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Historia "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   1800
               TabIndex        =   54
               Top             =   480
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Persona"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   240
               TabIndex        =   53
               Top             =   480
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   240
               TabIndex        =   52
               Top             =   1080
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 1�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   3240
               TabIndex        =   51
               Top             =   1080
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 2�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   6240
               TabIndex        =   50
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   -74880
               TabIndex        =   49
               Top             =   960
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   -74880
               TabIndex        =   48
               Top             =   360
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   -69840
               TabIndex        =   47
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma M�dico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   -67680
               TabIndex        =   46
               Top             =   360
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   -69840
               TabIndex        =   45
               Top             =   960
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Firma Enfermera"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   -67680
               TabIndex        =   44
               Top             =   960
               Width           =   1935
            End
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   3360
            TabIndex        =   56
            Top             =   120
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.OM origen PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   1440
            TabIndex        =   42
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   120
            TabIndex        =   38
            Top             =   120
            Width           =   1455
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   32
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4095
      Left            =   120
      TabIndex        =   62
      TabStop         =   0   'False
      Top             =   3840
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   7223
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Normal"
      TabPicture(0)   =   "FR0725.frx":00C4
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Acumulados"
      TabPicture(1)   =   "FR0725.frx":00E0
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(2)"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraFrame1 
         Height          =   3615
         Index           =   2
         Left            =   -74880
         TabIndex        =   99
         Top             =   360
         Width           =   9975
         Begin SSDataWidgets_B.SSDBGrid auxdbgrid1 
            Height          =   3315
            Index           =   7
            Left            =   120
            TabIndex        =   100
            TabStop         =   0   'False
            Top             =   240
            Width           =   9705
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   9
            stylesets.count =   1
            stylesets(0).Name=   "Bloqueada"
            stylesets(0).ForeColor=   16777215
            stylesets(0).BackColor=   255
            stylesets(0).Picture=   "FR0725.frx":00FC
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns.Count   =   9
            Columns(0).Width=   1614
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   12632256
            Columns(1).Width=   1773
            Columns(1).Caption=   "Referencia"
            Columns(1).Name =   "Referencia"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   12632256
            Columns(2).Width=   4577
            Columns(2).Caption=   "Producto"
            Columns(2).Name =   "Producto"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(2).HasBackColor=   -1  'True
            Columns(2).BackColor=   12632256
            Columns(3).Width=   741
            Columns(3).Caption=   "FF"
            Columns(3).Name =   "FF"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(3).HasBackColor=   -1  'True
            Columns(3).BackColor=   12632256
            Columns(4).Width=   1164
            Columns(4).Caption=   "Dosis"
            Columns(4).Name =   "Dosis"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Locked=   -1  'True
            Columns(4).HasBackColor=   -1  'True
            Columns(4).BackColor=   12632256
            Columns(5).Width=   794
            Columns(5).Caption=   "UM"
            Columns(5).Name =   "UM"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Locked=   -1  'True
            Columns(5).HasBackColor=   -1  'True
            Columns(5).BackColor=   12632256
            Columns(6).Width=   1852
            Columns(6).Caption=   "Cant Ped"
            Columns(6).Name =   "Cant Ped"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).Locked=   -1  'True
            Columns(6).HasBackColor=   -1  'True
            Columns(6).BackColor=   12632256
            Columns(7).Width=   2064
            Columns(7).Caption=   "Cant Disp"
            Columns(7).Name =   "Cant Disp"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(7).Locked=   -1  'True
            Columns(7).HasBackColor=   -1  'True
            Columns(7).BackColor=   12632256
            Columns(8).Width=   1561
            Columns(8).Caption=   "Cant Ori"
            Columns(8).Name =   "Cant Ori"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(8).Locked=   -1  'True
            Columns(8).HasBackColor=   -1  'True
            Columns(8).BackColor=   12632256
            _ExtentX        =   17119
            _ExtentY        =   5847
            _StockProps     =   79
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Productos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3600
         Index           =   0
         Left            =   120
         TabIndex        =   97
         Top             =   360
         Width           =   9975
         Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
            Height          =   3195
            Index           =   0
            Left            =   120
            TabIndex        =   98
            Top             =   360
            Width           =   9825
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   1
            stylesets(0).Name=   "Bloqueada"
            stylesets(0).ForeColor=   16777215
            stylesets(0).BackColor=   255
            stylesets(0).Picture=   "FR0725.frx":0118
            SelectTypeRow   =   3
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   17330
            _ExtentY        =   5636
            _StockProps     =   79
         End
      End
      Begin VB.TextBox txtText1 
         DataField       =   "SG02COD_FEN"
         Height          =   330
         Index           =   12
         Left            =   -66720
         TabIndex        =   78
         Tag             =   "Hora Firma Enfermera"
         Top             =   1440
         Width           =   612
      End
      Begin VB.TextBox txtText1 
         DataField       =   "sg02cod_enf"
         Height          =   330
         Index           =   31
         Left            =   -74880
         TabIndex        =   77
         Tag             =   "C�digo Enfermera"
         Top             =   1200
         Width           =   1635
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   32
         Left            =   -73200
         TabIndex        =   76
         TabStop         =   0   'False
         Tag             =   "Nombre Enfermera"
         Top             =   1200
         Width           =   1605
      End
      Begin VB.TextBox txtText1 
         DataField       =   "SG02COD_MED"
         Height          =   330
         Index           =   33
         Left            =   -74880
         TabIndex        =   75
         Tag             =   "C�digo Doctor"
         Top             =   600
         Width           =   1635
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   34
         Left            =   -73200
         TabIndex        =   74
         Tag             =   "Apellido Doctor"
         Top             =   600
         Width           =   3285
      End
      Begin VB.TextBox txtText1 
         DataField       =   "FR66HORAFIRMMEDI"
         Height          =   330
         Index           =   35
         Left            =   -67680
         TabIndex        =   73
         Tag             =   "hora Firma M�dico"
         Top             =   600
         Width           =   612
      End
      Begin VB.TextBox txtText1 
         DataField       =   "FR66HORAFIRMENF"
         Height          =   330
         Index           =   36
         Left            =   -67680
         TabIndex        =   72
         Tag             =   "Hora Firma Enfermera"
         Top             =   1200
         Width           =   612
      End
      Begin VB.TextBox txtText1 
         DataField       =   "FR66HORAREDACCI"
         Height          =   330
         Index           =   37
         Left            =   -72840
         TabIndex        =   71
         Tag             =   "Hora Redacci�n"
         Top             =   1800
         Width           =   612
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   38
         Left            =   -70080
         TabIndex        =   70
         TabStop         =   0   'False
         Tag             =   "Desc. Urgencia"
         Top             =   1800
         Width           =   4125
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   39
         Left            =   -71520
         TabIndex        =   69
         TabStop         =   0   'False
         Tag             =   "Apellido 1� Enfermera"
         Top             =   1200
         Width           =   1605
      End
      Begin VB.TextBox txtText1 
         DataField       =   "FR91CODURGENCIA"
         Height          =   330
         Index           =   40
         Left            =   -71160
         TabIndex        =   68
         Tag             =   "C�digo Urgencia"
         Top             =   1800
         Width           =   735
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   41
         Left            =   -72600
         TabIndex        =   67
         TabStop         =   0   'False
         Tag             =   "Desc.Servicio Cargo"
         Top             =   1440
         Width           =   5400
      End
      Begin VB.TextBox txtText1 
         DataField       =   "AD02CODDPTO_CRG"
         Height          =   330
         Index           =   42
         Left            =   -74760
         TabIndex        =   66
         Tag             =   "C�d.Servicio Cargo"
         Top             =   1440
         Width           =   2000
      End
      Begin VB.TextBox txtText1 
         DataField       =   "AD02CODDPTO"
         Height          =   330
         Index           =   43
         Left            =   -74760
         TabIndex        =   65
         Tag             =   "C�d.Departamento"
         Top             =   840
         Width           =   2000
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   44
         Left            =   -72600
         TabIndex        =   64
         Tag             =   "Desc.Departamento"
         Top             =   840
         Width           =   5400
      End
      Begin VB.TextBox txtText1 
         DataField       =   "FR66OBSERV"
         Height          =   1410
         Index           =   45
         Left            =   -74880
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   63
         Tag             =   "Observaciones"
         Top             =   600
         Width           =   8925
      End
      Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
         Height          =   4425
         Index           =   5
         Left            =   -74880
         TabIndex        =   79
         TabStop         =   0   'False
         Top             =   240
         Width           =   10455
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   18441
         _ExtentY        =   7805
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grddbgrid1 
         Height          =   1305
         Index           =   6
         Left            =   -74880
         TabIndex        =   80
         TabStop         =   0   'False
         Top             =   120
         Width           =   9855
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   17383
         _ExtentY        =   2302
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         DataField       =   "FR66FECFIRMMEDI"
         Height          =   330
         Index           =   3
         Left            =   -69840
         TabIndex        =   81
         Tag             =   "Fecha Inicio Vigencia"
         Top             =   600
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         DataField       =   "FR66FECFIRMENF"
         Height          =   330
         Index           =   4
         Left            =   -69840
         TabIndex        =   82
         Tag             =   "Fecha Inicio Vigencia"
         Top             =   1200
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         DataField       =   "FR66FECREDACCION"
         Height          =   330
         Index           =   5
         Left            =   -74880
         TabIndex        =   83
         Tag             =   "Fecha Inicio Vigencia"
         Top             =   1800
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d.Firma"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   -66720
         TabIndex        =   96
         Top             =   1200
         Width           =   855
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Enfermera"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   25
         Left            =   -74880
         TabIndex        =   95
         Top             =   960
         Width           =   975
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Dr."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   26
         Left            =   -74880
         TabIndex        =   94
         Top             =   360
         Width           =   495
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Fecha Firma M�dico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   29
         Left            =   -69840
         TabIndex        =   93
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Hora Firma M�dico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   30
         Left            =   -67680
         TabIndex        =   92
         Top             =   360
         Width           =   1695
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Fecha Firma Enfermera"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   31
         Left            =   -69840
         TabIndex        =   91
         Top             =   960
         Width           =   2055
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Hora Firma Enfermera"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   32
         Left            =   -67680
         TabIndex        =   90
         Top             =   960
         Width           =   1935
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Fecha Redacci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   33
         Left            =   -74880
         TabIndex        =   89
         Top             =   1560
         Width           =   1815
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Hora Redacci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   34
         Left            =   -72840
         TabIndex        =   88
         Top             =   1560
         Width           =   1575
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Urgencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   35
         Left            =   -71160
         TabIndex        =   87
         Top             =   1560
         Width           =   855
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   36
         Left            =   -74760
         TabIndex        =   86
         Top             =   600
         Width           =   2055
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Servicio Cargo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   37
         Left            =   -74760
         TabIndex        =   85
         Top             =   1200
         Width           =   2055
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Observaciones"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   38
         Left            =   -74880
         TabIndex        =   84
         Top             =   360
         Width           =   2655
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmEstPetFarm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmEstPetFarm(FR0725.FRM)                                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: estudiar petici�n farmacia                              *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Function CrearWhere(strPal As String, strLis) As String
  'strLis tiene la forma (1123,4444459), strPal es un nombre de campo
  'CrearWhere se utiliza para pasar a Crystal la sentencias SQL de tipo
  'FR66CODTICION IN (13446,443775)
  'al formato (FR66CODTICION = 13446 OR FR66CODTICION = 443775)
  Dim strCar As String
  Dim strResLis As String
  Dim strSalida As String
  
  strResLis = Right(strLis, Len(strLis) - 1)
  strSalida = "("
  While (strCar <> ")")
    If (strCar <> ")") Then
      strSalida = strSalida & strPal & " = "
    End If
    While (strCar <> ",") And (strCar <> ")")
      strSalida = strSalida & Left(strResLis, 1)
      strCar = Left(strResLis, 1)
      strResLis = Right(strResLis, Len(strResLis) - 1)
    Wend
    If (strCar = ",") Then
      strSalida = Left(strSalida, Len(strSalida) - 1) & " OR "
      strCar = Left(strResLis, 1)
    End If
  Wend
  CrearWhere = strSalida
End Function


Private Sub cmdAnular_Click()
Dim intMsg As Integer
Dim strupdate As String
Dim intFacturar As Integer
Dim strInsert As String


On Error GoTo err

If txtText1(25).Text <> 1 Then 'Redactado
  Call MsgBox("La petici�n debe estar en estado Redactado", vbInformation, "Aviso")
ElseIf txtText1(25).Text = 8 Then 'Anulado
  Call MsgBox("La petici�n ya est� Anulada", vbInformation, "Aviso")
End If

cmdAnular.Enabled = False

  If txtText1(0).Text <> "" Then
    If txtText1(25).Text <> 8 Then 'no est� y anulada
      intMsg = MsgBox("�Est� seguro que desea Anular la Petici�n?", vbQuestion + vbYesNo, "Aviso")
      If intMsg = vbYes Then
        strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=8 WHERE FR66CODPETICION=" & txtText1(0).Text
        objApp.rdoConnect.Execute strupdate, 64
        Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
        objWinInfo.DataRefresh
        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
        strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=-1 WHERE " & _
                    " FR66CODPETICION=" & txtText1(0).Text
        objApp.rdoConnect.Execute strupdate, 64
        objWinInfo.DataRefresh
        Call Anular_Facturacion
      End If
    Else
      Call MsgBox("La petici�n ya est� Anulada", vbInformation, "Aviso")
    End If
  End If

err:
    cmdAnular.Enabled = True

End Sub

Private Sub cmdbloquear_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim strlineas
Dim intMsg As String
Dim strDelete As String
Dim intFacturar As Integer
Dim strupdate As String
Dim strFR28 As String
Dim rdoFR28 As rdoResultset

If txtText1(25).Text <> 1 Then 'Redactado
  Call MsgBox("La petici�n debe estar en estado Redactado", vbInformation, "Aviso")
ElseIf txtText1(25).Text = 8 Then 'Anulado
  Call MsgBox("La petici�n ya est� Anulada", vbInformation, "Aviso")
End If

cmdBloquear.Enabled = False

If txtText1(25).Text <> 8 Then
    strlineas = "("
    mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
    For mintisel = 0 To mintNTotalSelRows - 1
       mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
       If strlineas = "(" Then
        If grdDBGrid1(0).Columns("Bloq").CellValue(mvarBkmrk) = 0 Then
          strlineas = strlineas & grdDBGrid1(0).Columns("L�nea").CellValue(mvarBkmrk)
        End If
       Else
        If grdDBGrid1(0).Columns("Bloq").CellValue(mvarBkmrk) = 0 Then
          strlineas = strlineas & "," & grdDBGrid1(0).Columns("L�nea").CellValue(mvarBkmrk)
        End If
       End If
    Next mintisel
    strlineas = strlineas & ")"
    
    If strlineas <> "()" Then
      intMsg = MsgBox("�Est� seguro que desea Anular las L�neas?", vbQuestion + vbYesNo, "Aviso")
      If intMsg = vbYes Then
        Call Anular_Facturacion_Lineas(strlineas)
        strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=-1" & _
                    " WHERE FR66CODPETICION=" & txtText1(0).Text & _
                    " AND FR28NUMLINEA IN " & strlineas
        objApp.rdoConnect.Execute strupdate, 64
        objWinInfo.DataRefresh
      End If
      'Anulada si quitan todos los productos
      strFR28 = "SELECT COUNT(*) FROM FR2800 " & _
                " WHERE FR66CODPETICION =" & txtText1(0).Text & _
                " AND FR28INDBLOQUEADA= 0 "
      Set rdoFR28 = objApp.rdoConnect.OpenResultset(strFR28)
      If rdoFR28(0).Value = 0 Then
        strupdate = "UPDATE FR6600 SET FR26CODESTPETIC=8 WHERE FR66CODPETICION=" & txtText1(0).Text
        objApp.rdoConnect.Execute strupdate, 64
        Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
        objWinInfo.DataRefresh
      End If
      rdoFR28.Close
      Set rdoFR28 = Nothing
    End If
Else
  Call MsgBox("La petici�n ya est� Anulada", vbInformation, "Aviso")
End If

cmdBloquear.Enabled = True

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
On Error GoTo err
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
err:
End Sub


Private Sub Form_Activate()
Dim strfecnac As String
Dim rstfecnac As rdoResultset
Dim edad As Integer
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant

On Error GoTo err
  If gintfirmarOM = 1 Then
    gintfirmarOM = 0
    'se calcula la edad
    strfecnac = "SELECT CI22FECNACIM FROM CI2200 WHERE CI21CODPERSONA=" & txtText1(2).Text
    Set rstfecnac = objApp.rdoConnect.OpenResultset(strfecnac)
    If Not rstfecnac.EOF Then
      If Not IsNull(rstfecnac.rdoColumns(0).Value) Then
        edad = DateDiff("d", rstfecnac.rdoColumns(0).Value, Date) \ 365
        Text1.Text = edad
      Else
        Text1.Text = ""
      End If
    Else
        Text1.Text = ""
    End If
    rstfecnac.Close
    Set rstfecnac = Nothing
    Text1.Locked = True
  End If
  If IsNumeric(txtText1(14).Text) And IsNumeric(txtText1(15).Text) Then
      strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
      Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
      strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
      Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
      strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
      Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
      intPeso = txtText1(14).Text
      intAltura = txtText1(15).Text
      vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
      vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
      vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
      Text2.Text = vntMasa
  End If
  
  If gstrLlamadorProd = "frmVisPRN.Acumulados" Then
    Call Refrescar_Grid
  ElseIf gstrLlamadorProd = "frmVisPRN.Acumulados.Individual" Then
    If frmVisPRN.optCesta(0) Then
      If txtText1(25).Text = 1 Then
        cmdAnular.Visible = True
        cmdBloquear.Visible = True
      End If
    End If
  End If
  
  Screen.MousePointer = vbDefault

err:
  Screen.MousePointer = vbDefault
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm
Dim strKey As String
  
  On Error GoTo err
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "PRN"
      
    .strTable = "FR6600"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR66CODPETICION", cwAscending)
    'Call .objPrinter.Add("FR1251", "Preparaci�n de Cesta")
    'Call .objPrinter.Add("FR1252", "Dispensaci�n de Cesta")
    
    .strWhere = "FR66CODPETICION=" & glngpeticion & _
        " AND (FR66INDESTUPEFACIENTE=0 OR FR66INDESTUPEFACIENTE IS NULL)" & _
        " AND (FR66INDFM=0 OR FR66INDFM IS NULL)"
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "PRN")
    Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDALERGIA", "Al�rgico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
   
    'Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "C�digo Grupo Protocolo")

  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Detalle PRN"
    
    .strTable = "FR2800"
    .intAllowance = cwAllowReadOnly
    
    'Call .objPrinter.Add("FR1251", "Preparaci�n de Cesta")
    'Call .objPrinter.Add("FR1252", "Dispensaci�n de Cesta")
    
    Call .FormAddRelation("FR66CODPETICION", txtText1(0))
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    
    .strWhere = " FR28INDBLOQUEADA = 0 "
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "Bloq", "FR28INDBLOQUEADA", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Pet", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Prod", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Tam Env", "", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "FF", "FRH7CODFORMFAR", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR28DOSIS", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "UM", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Desc.UM", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Can Ped", "FR28CANTIDAD", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "Ac Ped", "", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Can Disp", "FR28CANTDISP", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "Ac Disp", "", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Desc.V�a", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Volumen", "FR28VOLUMEN", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "Tiempo Infusi�n", "FR28TIEMINFMIN", cwDecimal, 4)
    Call .GridAddColumn(objMultiInfo, "C�d.Frecuencia", "FRG4CODFRECUENCIA", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Frecuencia", "", cwString, 60)
    Call .GridAddColumn(objMultiInfo, "PRN", "FR28INDDISPPRN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "C.I", "FR28INDCOMIENINMED", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "S.N", "FR28INDSN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Periodicidad", "FRH5CODPERIODICIDAD", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Ubicaci�n", "", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Pet.Orig.", "FR28CANTPEDORI", cwDecimal, 2)
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).intKeyNo = 1
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    '.CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(txtText1(21)).blnInFind = True
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(txtText1(25)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(12)).blnInFind = True
    .CtrlGetInfo(chkCheck1(15)).blnInFind = True
    
    .CtrlGetInfo(Text2).blnNegotiated = False
    '.CtrlGetInfo(txtText1(29)).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(4), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(5), "CI22SEGAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(23), "CI22NUMHISTORIA")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(13), "CI30CODSEXO")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "SG02NOM")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(19), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(11), "FR91DESURGENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(61)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(61)), txtText1(62), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(26), "FR26DESESTADOPET")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(7), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(9), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(8), "FR73REFERENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(10), "FR73TAMENVASE")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(29), "FRH9UBICACION")
     
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(19)), "FR34CODVIA", "SELECT * FROM FR3400 WHERE FR34CODVIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(19)), grdDBGrid1(0).Columns(20), "FR34DESVIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(13)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(13)), grdDBGrid1(0).Columns(14), "FR93DESUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(23)), "FRG4CODFRECUENCIA", "SELECT * FROM FRG400 WHERE FRG4CODFRECUENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(23)), grdDBGrid1(0).Columns(24), "FRG4DESFRECUENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM FR2200J WHERE CI21CODPERSONA= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(46), "DESCCAMA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(13), "CI30DESSEXO")
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(6)).blnForeign = True
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    '.CtrlGetInfo(txtText1(14)).blnForeign = True
    .CtrlGetInfo(txtText1(16)).blnForeign = True
    .CtrlGetInfo(txtText1(22)).blnForeign = True
    .CtrlGetInfo(txtText1(25)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(19)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(13)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns("Can Disp")).blnMandatory = True
    
     
    '.CtrlGetInfo(txtText1(8)).blnReadOnly = True
    .CtrlGetInfo(txtText1(10)).blnReadOnly = True
    .CtrlGetInfo(txtText1(20)).blnReadOnly = True
    .CtrlGetInfo(txtText1(21)).blnReadOnly = True
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True
    .CtrlGetInfo(txtText1(25)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(13)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(19)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(21)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(22)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(23)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(25)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(26)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(27)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(28)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(15)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(16)).blnReadOnly = True
    '.CtrlGetInfo().blnNegotiated = False
    Call .WinRegister
    Call .WinStabilize
    
  End With
grdDBGrid1(0).Columns(0).Visible = False
grdDBGrid1(0).Columns(3).Visible = False
grdDBGrid1(0).Columns(4).Visible = False
grdDBGrid1(0).Columns(9).Width = 1500   'desc.producto
grdDBGrid1(0).Columns(10).Width = 800   'TAM ENVASE
grdDBGrid1(0).Columns(5).Visible = False
grdDBGrid1(0).Columns(6).Visible = False
grdDBGrid1(0).Columns(14).Visible = False

grdDBGrid1(0).Columns(0).Width = 0
grdDBGrid1(0).Columns(3).Width = 500 'bloqueada?
grdDBGrid1(0).Columns(6).Width = 1000   'c�d.producto
grdDBGrid1(0).Columns(7).Width = 800   'c�d interno
grdDBGrid1(0).Columns(8).Width = 1000   'REFERENCIA
grdDBGrid1(0).Columns(11).Width = 500   'FF
grdDBGrid1(0).Columns(19).Width = 400   'c�d.v�a
grdDBGrid1(0).Columns(20).Width = 1500   'desc.v�a
grdDBGrid1(0).Columns(21).Width = 1100  'volumen
grdDBGrid1(0).Columns(22).Width = 1100  'tiempo infusi�n
grdDBGrid1(0).Columns(23).Width = 1000  'c�d.frecuencia
grdDBGrid1(0).Columns(24).Width = 1500  'c�d.frecuencia
grdDBGrid1(0).Columns(25).Width = 500  'prn
grdDBGrid1(0).Columns(26).Width = 500  'comienzo inmediato
grdDBGrid1(0).Columns(27).Width = 500  'seg�n niveles
grdDBGrid1(0).Columns(15).Width = 800  'cantidad pedida
grdDBGrid1(0).Columns(16).Width = 800  'ac pedido
grdDBGrid1(0).Columns(17).Width = 800  'cantidad disp
grdDBGrid1(0).Columns(18).Width = 800  'ac disp
grdDBGrid1(0).Columns(13).Width = 500  'c�d.medida
grdDBGrid1(0).Columns(14).Width = 1000  'desc. medida
grdDBGrid1(0).Columns(12).Width = 600   'dosis

Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)

If gstrLlamadorProd = "frmVisPRN.Acumulados" Then
  SSTab1.Tab = 1
  SSTab1.TabEnabled(0) = False
Else
  SSTab1.TabEnabled(1) = False
End If

err:
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
On Error GoTo err
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
err:
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
On Error GoTo err
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
err:
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
On Error GoTo err
  intCancel = objWinInfo.WinExit
err:
End Sub

Private Sub Form_Unload(intCancel As Integer)
On Error GoTo err
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
err:
End Sub


Private Sub IdPersona1_Click()

End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
On Error GoTo err
If strFormName = "OM" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "CI2200"

     Set objField = .AddField("CI21CODPERSONA")
     objField.strSmallDesc = "C�digo Paciente"

     Set objField = .AddField("CI22NOMBRE")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("CI22NUMHISTORIA")
     objField.strSmallDesc = "Historia"
     
     Set objField = .AddField("CI22PRIAPEL")
     objField.strSmallDesc = "Apellido 1�"
     
     Set objField = .AddField("CI22SEGAPEL")
     objField.strSmallDesc = "Apellido 2�"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("CI21CODPERSONA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo Enfermera"

     Set objField = .AddField("SG02NOM")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(6), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo M�dico"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 'If strFormName = "OM" And strCtrl = "txtText1(14)" Then
 '   Set objSearch = New clsCWSearch
 '   With objSearch
 '    .strTable = "AD0200"
 '    .strWhere = "WHERE AD32CODTIPODPTO=3 AND " & _
 '          "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
 '          "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"
'
'     Set objField = .AddField("AD02CODDPTO")
'     objField.strSmallDesc = "C�digo Servicio"
'
'     Set objField = .AddField("AD02DESDPTO")
'     objField.strSmallDesc = "Descripci�n Servicio"
'
'     If .Search Then
'      Call objWinInfo.CtrlSet(txtText1(14), .cllValues("AD02CODDPTO"))
'     End If
'   End With
'   Set objSearch = Nothing
' End If
 
 If strFormName = "OM" And strCtrl = "txtText1(16)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD32CODTIPODPTO=3 AND " & _
           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Dpto."

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto."

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(16), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(22)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9100"

     Set objField = .AddField("FR91CODURGENCIA")
     objField.strSmallDesc = "C�digo Urgencia"

     Set objField = .AddField("FR91DESURGENCIA")
     objField.strSmallDesc = "Descripci�n Urgencia"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(22), .cllValues("FR91CODURGENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle PRN" And strCtrl = "grdDBgrid1(0).C�d.Prod" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"

     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"

     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(6), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle PRN" And strCtrl = "grdDBgrid1(0).V�a" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR3400"

     Set objField = .AddField("FR34CODVIA")
     objField.strSmallDesc = "C�digo V�a"
     
     Set objField = .AddField("FR34DESVIA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(19), .cllValues("FR34CODVIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle PRN" And strCtrl = "grdDBgrid1(0).Medida" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad de Medida"
     
     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(13), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
err:
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  'On Error GoTo err
  'If strFormName = "PRN" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      If intReport = 1 Then
        'Call Imprimir("FR1251.RPT", 0)
      Else
        'Call Imprimir("FR1252.RPT", 0)
      End If
    End If
    Set objPrinter = Nothing
  'End If

'err:
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
On Error GoTo err
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
err:
End Sub


Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
On Error GoTo err
   tab1.Tab = 0
err:
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
On Error GoTo err
  If btnButton.Index = 30 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
On Error GoTo err
    Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
On Error GoTo err
     Call objWinInfo.GridDblClick
err:
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  On Error GoTo err
     Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
err:
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub

Private Sub cboCombo1_Change(Index As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlLostFocus
err:
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlGotFocus
err:
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)

  Call objWinInfo.CtrlLostFocus

End Sub

Private Sub txtText1_Change(intIndex As Integer)
On Error GoTo err
  Call objWinInfo.CtrlDataChange
err:
End Sub


Private Sub Refrescar_Grid()
Dim strselect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim mvntAcumuladoPed As Variant
Dim mvntAcumuladoDisp As Variant
Dim mvntAcumuladoOri As Variant
Dim mvntProducto As Variant
Dim i As Integer
Dim contSecciones As Integer
Dim k As Integer
  
Screen.MousePointer = vbHourglass

auxdbgrid1(7).Redraw = False
auxdbgrid1(7).RemoveAll
  
strselect = "SELECT "
strselect = strselect & " FR2800.FR28INDBLOQUEADA"
strselect = strselect & ",FR2800.FR66CODPETICION"
strselect = strselect & ",FR2800.FR28NUMLINEA"
strselect = strselect & ",FR2800.FR73CODPRODUCTO"
strselect = strselect & ",FR7300.FR73CODINTFAR"
strselect = strselect & ",FR7300.FR73REFERENCIA"
strselect = strselect & ",FR7300.FR73DESPRODUCTO"
strselect = strselect & ",FR2800.FRH7CODFORMFAR"
strselect = strselect & ",FR2800.FR28DOSIS"
strselect = strselect & ",FR2800.FR93CODUNIMEDIDA"
strselect = strselect & ",NVL(FR2800.FR28CANTIDAD,0) CANTIDAD"
strselect = strselect & ",NVL(FR2800.FR28CANTDISP,0) CANTDISP"
strselect = strselect & ",NVL(FR2800.FR28CANTPEDORI,0) CANTORI"

strselect = strselect & " FROM FR2800,FR6600,FR7300"
strselect = strselect & " WHERE "

strselect = strselect & " FR2800.FR66CODPETICION=FR6600.FR66CODPETICION"
strselect = strselect & " AND FR2800.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
strselect = strselect & " AND FR2800.FR28INDBLOQUEADA=0"

  If frmVisPRN.optOMPRN(1).Value = True Then
    strselect = strselect & " AND FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=1 AND FR6600.AD02CODDPTO=" & frmVisPRN.auxdbgrid1(0).Columns("C�d.Servicio").Value
    strselect = strselect & " AND FR2800.FR28CANTIDAD>0 "
  ElseIf frmVisPRN.optOMPRN(2).Value = True Then
    strselect = strselect & " AND FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=3 AND FR6600.AD02CODDPTO=" & frmVisPRN.auxdbgrid1(0).Columns("C�d.Servicio").Value
    strselect = strselect & " AND FR2800.FR28CANTIDAD>0 "
  ElseIf frmVisPRN.optOMPRN(3).Value = True Then
    strselect = strselect & " AND FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=9 AND FR6600.AD02CODDPTO=" & frmVisPRN.auxdbgrid1(0).Columns("C�d.Servicio").Value
    strselect = strselect & " AND FR2800.FR28CANTIDAD>0 "
  ElseIf frmVisPRN.optOMPRN(4).Value = True Then
    strselect = strselect & " AND FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC=5 AND FR6600.AD02CODDPTO=" & frmVisPRN.auxdbgrid1(0).Columns("C�d.Servicio").Value
  ElseIf frmVisPRN.optOMPRN(5).Value = True Then
    strselect = strselect & " AND FR6600.FR66INDCESTA=-1 AND FR6600.FR66INDOM=0 AND FR6600.FR26CODESTPETIC in (3,9) AND FR6600.AD02CODDPTO=" & frmVisPRN.auxdbgrid1(0).Columns("C�d.Servicio").Value
    strselect = strselect & " AND FR2800.FR28CANTIDAD>0 "
  End If
  If frmVisPRN.txtHistoria.Text <> "" Then
    strselect = strselect & " AND FR6600.CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & frmVisPRN.txtHistoria.Text & ")"
  End If
  If IsDate(frmVisPRN.dtcredaccion) Then
    strselect = strselect & " AND TRUNC(FR6600.FR66FECREDACCION)=TO_DATE('" & frmVisPRN.dtcredaccion & "','DD/MM/YYYY')"
  End If
  If IsDate(frmVisPRN.dtcEnvio) Then
    strselect = strselect & " AND TRUNC(FR6600.FR66FECENVIO)=TO_DATE('" & frmVisPRN.dtcEnvio & "','DD/MM/YYYY')"
  End If
  If IsDate(frmVisPRN.dtcDispensacion) Then
    strselect = strselect & " AND TRUNC(FR6600.FR66FECDISPEN)=TO_DATE('" & frmVisPRN.dtcDispensacion & "','DD/MM/YYYY')"
  End If
'secciones''''''''''''''''''''''''''''''''''''''''''''''''''''
'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
  contSecciones = 0
  If frmVisPRN.lvwSec.ListItems.Count > 0 Then
    If frmVisPRN.lvwSec.ListItems(1).Selected Then
    Else
      For k = 2 To frmVisPRN.lvwSec.ListItems.Count
        If frmVisPRN.lvwSec.ListItems(k).Selected = True Then
          If contSecciones = 0 Then
            contSecciones = contSecciones + 1
            strselect = strselect & " AND ("
          Else
            strselect = strselect & " OR "
          End If
          If k = 2 Then
            strselect = strselect & "FR6600.AD41CODSECCION IS NULL"
          Else
            strselect = strselect & "FR6600.AD41CODSECCION=" & frmVisPRN.lvwSec.ListItems(k).Tag
          End If
        End If
      Next k
      If contSecciones > 0 Then
        strselect = strselect & ") "
      End If
    End If
  End If
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

strselect = strselect & " ORDER BY FR73DESPRODUCTO"

Set qrya = objApp.rdoConnect.CreateQuery("", strselect)
Set rsta = qrya.OpenResultset()

mvntProducto = -1
mvntAcumuladoPed = 0
mvntAcumuladoDisp = 0
If Not rsta.EOF Then
  mvntProducto = rsta("FR73CODPRODUCTO").Value
End If
strlinea = ""
While Not rsta.EOF
  If mvntProducto <> rsta("FR73CODPRODUCTO").Value Then
    auxdbgrid1(7).AddItem strlinea
    mvntAcumuladoPed = rsta("CANTIDAD").Value
    mvntAcumuladoDisp = rsta("CANTDISP").Value
    mvntAcumuladoOri = rsta("CANTORI").Value
    mvntProducto = rsta("FR73CODPRODUCTO").Value
  Else
    mvntAcumuladoPed = mvntAcumuladoPed + rsta("CANTIDAD").Value
    mvntAcumuladoDisp = mvntAcumuladoDisp + rsta("CANTDISP").Value
    mvntAcumuladoOri = mvntAcumuladoOri + rsta("CANTORI").Value
  End If
  strlinea = rsta("FR73CODINTFAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73REFERENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73DESPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRH7CODFORMFAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28DOSIS").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & mvntAcumuladoPed & Chr(vbKeyTab)
  strlinea = strlinea & mvntAcumuladoDisp & Chr(vbKeyTab)
  strlinea = strlinea & mvntAcumuladoOri & Chr(vbKeyTab)
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
If strlinea <> "" Then
  auxdbgrid1(7).AddItem strlinea
End If
auxdbgrid1(7).Redraw = True
  
Screen.MousePointer = vbDefault

End Sub

Private Sub Anular_Facturacion()
Dim strFR19 As String
Dim rdoFR19 As rdoResultset
Dim strIns
Dim strCant As String
Dim strCant2 As String
Dim strCantDil As String
Dim strFR66 As String
Dim rdoFR66 As rdoResultset
Dim auxProceso, auxAsistencia

strFR66 = "SELECT * FROM FR6600 " & _
          " WHERE FR66CODPETICION =" & txtText1(0).Text
Set rdoFR66 = objApp.rdoConnect.OpenResultset(strFR66)
If Not rdoFR66.EOF Then
  auxProceso = rdoFR66("AD07CODPROCESO").Value
  auxAsistencia = rdoFR66("AD01CODASISTENCI").Value
End If

strFR19 = "SELECT * FROM FR2800 " & _
          " WHERE FR66CODPETICION =" & txtText1(0).Text & _
          " AND FR28CANTIDAD > 0 "
Set rdoFR19 = objApp.rdoConnect.OpenResultset(strFR19)
While Not rdoFR19.EOF
    If Not IsNull(rdoFR19.rdoColumns("FR28CANTIDAD").Value) Then
      strCant = CCur(rdoFR19.rdoColumns("FR28CANTIDAD").Value)
    Else
      strCant = 0
    End If
    strCant = objGen.ReplaceStr(strCant, ",", ".", 1)
    
    strIns = "INSERT INTO FR6500 (FR65CODIGO,CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA," & _
                "FR65CANTIDAD,FR73CODPRODUCTO_DIL," & _
                "FR65CANTIDADDIL,FR73CODPRODUCTO_2,FR65DOSIS_2," & _
                "FR93CODUNIMEDIDA_2,FR65DESPRODUCTO,FR65DOSIS,FR93CODUNIMEDIDA," & _
                "FR66CODPETICION,FR65INDPRN,AD01CODASISTENCI,AD07CODPROCESO," & _
                "FR65INDINTERCIENT,AD02CODDPTO_CRG) VALUES ("
    strIns = strIns & "FR65CODIGO_SEQUENCE.NEXTVAL,"
    strIns = strIns & txtText1(2).Text & ","
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO").Value & ",SYSDATE,TO_CHAR(SYSDATE,'HH24'),"
    Else
      strIns = strIns & "999999999" & ",SYSDATE,TO_CHAR(SYSDATE,'HH24'),"
    End If
    strIns = strIns & "-" & strCant & ","   'cantidad negativa
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value & ","
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
      strIns = strIns & "-" & objGen.ReplaceStr(rdoFR19.rdoColumns("FR28CANTIDADDIL").Value, ",", ".", 1) & ","
      strCantDil = objGen.ReplaceStr(rdoFR19.rdoColumns("FR28CANTIDADDIL").Value, ",", ".", 1)
    Else
      strIns = strIns & "0,"
      strCantDil = 0
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value & ","
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DOSIS_2").Value) Then
      strIns = strIns & "-" & objGen.ReplaceStr(rdoFR19.rdoColumns("FR28DOSIS_2").Value, ",", ".", 1) & ","
    Else
      strIns = strIns & "0,"
      strCant2 = 0
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR93CODUNIMEDIDA_2").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DESPRODUCTO").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR28DESPRODUCTO").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DOSIS").Value) Then
      strIns = strIns & objGen.ReplaceStr(rdoFR19.rdoColumns("FR28DOSIS").Value, ",", ".", 1) & ","
    Else
      strIns = strIns & "0,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR93CODUNIMEDIDA").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR93CODUNIMEDIDA").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    strIns = strIns & txtText1(0).Text & ",1," & auxAsistencia & "," & auxProceso & ","
    If IsNull(rdoFR66("FR66INDINTERCIENT").Value) Then
         strIns = strIns & "null" & ","
    Else
         strIns = strIns & rdoFR66("FR66INDINTERCIENT").Value & ","
    End If
    If IsNull(rdoFR66("AD02CODDPTO_CRG").Value) Then
         strIns = strIns & "null"
    Else
         strIns = strIns & rdoFR66("AD02CODDPTO_CRG").Value
    End If
    strIns = strIns & ")"
    objApp.rdoConnect.Execute strIns, 64
rdoFR19.MoveNext
Wend
rdoFR19.Close
Set rdoFR19 = Nothing
rdoFR66.Close
Set rdoFR66 = Nothing
End Sub

Private Sub Anular_Facturacion_Lineas(lineas_peticion)
Dim strFR19 As String
Dim rdoFR19 As rdoResultset
Dim strIns
Dim strCant As String
Dim strCant2 As String
Dim strCantDil As String
Dim strFR66 As String
Dim rdoFR66 As rdoResultset
Dim auxProceso, auxAsistencia

strFR66 = "SELECT * FROM FR6600 " & _
          " WHERE FR66CODPETICION =" & txtText1(0).Text
Set rdoFR66 = objApp.rdoConnect.OpenResultset(strFR66)
If Not rdoFR66.EOF Then
  auxProceso = rdoFR66("AD07CODPROCESO").Value
  auxAsistencia = rdoFR66("AD01CODASISTENCI").Value
End If

strFR19 = "SELECT * FROM FR2800 " & _
          " WHERE FR66CODPETICION =" & txtText1(0).Text & _
          " AND FR28CANTIDAD > 0 " & _
          " AND FR28NUMLINEA IN " & lineas_peticion
Set rdoFR19 = objApp.rdoConnect.OpenResultset(strFR19)
While Not rdoFR19.EOF
    If Not IsNull(rdoFR19.rdoColumns("FR28CANTIDAD").Value) Then
      strCant = CCur(rdoFR19.rdoColumns("FR28CANTIDAD").Value)
    Else
      strCant = 0
    End If
    strCant = objGen.ReplaceStr(strCant, ",", ".", 1)
    
    strIns = "INSERT INTO FR6500 (FR65CODIGO,CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA," & _
                "FR65CANTIDAD,FR73CODPRODUCTO_DIL," & _
                "FR65CANTIDADDIL,FR73CODPRODUCTO_2,FR65DOSIS_2," & _
                "FR93CODUNIMEDIDA_2,FR65DESPRODUCTO,FR65DOSIS,FR93CODUNIMEDIDA," & _
                "FR66CODPETICION,FR65INDPRN,AD01CODASISTENCI,AD07CODPROCESO," & _
                "FR65INDINTERCIENT,AD02CODDPTO_CRG) VALUES ("
    strIns = strIns & "FR65CODIGO_SEQUENCE.NEXTVAL,"
    strIns = strIns & txtText1(2).Text & ","
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO").Value & ",SYSDATE,TO_CHAR(SYSDATE,'HH24'),"
    Else
      strIns = strIns & "999999999" & ",SYSDATE,TO_CHAR(SYSDATE,'HH24'),"
    End If
    strIns = strIns & "-" & strCant & ","   'cantidad negativa
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value & ","
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
      strIns = strIns & "-" & objGen.ReplaceStr(rdoFR19.rdoColumns("FR28CANTIDADDIL").Value, ",", ".", 1) & ","
      strCantDil = objGen.ReplaceStr(rdoFR19.rdoColumns("FR28CANTIDADDIL").Value, ",", ".", 1)
    Else
      strIns = strIns & "0,"
      strCantDil = 0
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value) Then
      strIns = strIns & rdoFR19.rdoColumns("FR73CODPRODUCTO_2").Value & ","
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DOSIS_2").Value) Then
      strIns = strIns & "-" & objGen.ReplaceStr(rdoFR19.rdoColumns("FR28DOSIS_2").Value, ",", ".", 1) & ","
    Else
      strIns = strIns & "0,"
      strCant2 = 0
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR93CODUNIMEDIDA_2").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR93CODUNIMEDIDA_2").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DESPRODUCTO").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR28DESPRODUCTO").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR28DOSIS").Value) Then
      strIns = strIns & objGen.ReplaceStr(rdoFR19.rdoColumns("FR28DOSIS").Value, ",", ".", 1) & ","
    Else
      strIns = strIns & "0,"
    End If
    If Not IsNull(rdoFR19.rdoColumns("FR93CODUNIMEDIDA").Value) Then
      strIns = strIns & "'" & rdoFR19.rdoColumns("FR93CODUNIMEDIDA").Value & "',"
    Else
      strIns = strIns & "null,"
    End If
    strIns = strIns & txtText1(0).Text & ",1," & auxAsistencia & "," & auxProceso & ","
    If IsNull(rdoFR66("FR66INDINTERCIENT").Value) Then
         strIns = strIns & "null" & ","
    Else
         strIns = strIns & rdoFR66("FR66INDINTERCIENT").Value & ","
    End If
    If IsNull(rdoFR66("AD02CODDPTO_CRG").Value) Then
         strIns = strIns & "null"
    Else
         strIns = strIns & rdoFR66("AD02CODDPTO_CRG").Value
    End If
    strIns = strIns & ")"
    objApp.rdoConnect.Execute strIns, 64
rdoFR19.MoveNext
Wend
rdoFR19.Close
Set rdoFR19 = Nothing
rdoFR66.Close
Set rdoFR66 = Nothing
End Sub


