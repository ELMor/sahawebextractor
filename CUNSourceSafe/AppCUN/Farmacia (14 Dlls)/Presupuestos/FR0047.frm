VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmDefGrpPresup 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Definir Grupos Presupuestarios"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0047.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   " Ejercicio"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   5400
      TabIndex        =   9
      Top             =   1440
      Width           =   1575
      Begin SSDataWidgets_B.SSDBCombo cboservicio 
         Height          =   330
         Left            =   240
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   360
         Width           =   1095
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns(0).Width=   1879
         Columns(0).Caption=   "Ejercicio"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   2
         Columns(0).FieldLen=   256
         _ExtentX        =   1931
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdasociar 
      Caption         =   "Asociar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5640
      TabIndex        =   8
      Top             =   2760
      Width           =   1215
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Grupos Presupuestarios"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Index           =   2
      Left            =   120
      TabIndex        =   6
      Tag             =   "Actuaciones Asociadas"
      Top             =   5160
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2415
         Index           =   2
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   360
         Width           =   11370
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   20055
         _ExtentY        =   4260
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Servicios"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Index           =   1
      Left            =   7200
      TabIndex        =   4
      Tag             =   "Actuaciones Asociadas"
      Top             =   600
      Width           =   4575
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3855
         Index           =   1
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   360
         Width           =   4290
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   7567
         _ExtentY        =   6800
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Grupos de Compras"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Tag             =   "Actuaciones Asociadas"
      Top             =   600
      Width           =   5055
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3855
         Index           =   0
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   4770
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   8414
         _ExtentY        =   6800
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefGrpPresup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDefGrpPresup(FR0047.FRM)                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: definir grupos presupuestarios                          *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1



Private Sub rellenar_FRH100(grupo, Servicio)
Dim strInsert As String

strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'01.ENERO'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'02.FEBRERO'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'03.MARZO'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'04.ABRIL'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'05.MAYO'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'06.JUNIO'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'07.JULIO'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'08.AGOSTO'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'09.SEPTIEMBRE'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'10.OCTUBRE'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'11.NOVIEMBRE'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
strInsert = "INSERT INTO FRH100 (FRG9EJERCICIO,FR41CODGRUPPROD,AD02CODDPTO," & _
                   "FRH1MES,FRH1CANTIDADPTAS,FRH1CANTIDADEURO) VALUES (" & _
                   cboservicio & "," & _
                   grupo & "," & _
                   Servicio & "," & _
                   "'12.DICIEMBRE'" & "," & _
                   "0,0" & ")"
objApp.rdoConnect.Execute strInsert, 64
objApp.rdoConnect.Execute "Commit", 64
End Sub

Private Sub cboservicio_LostFocus()
Dim mensaje As String
If Not IsNumeric(cboservicio) Then
    mensaje = MsgBox("El ejercicio contiene un valor no num�rico", vbExclamation, "Aviso")
    cboservicio = ""
Else
    If Len(cboservicio) > 4 Then
       mensaje = MsgBox("El ejercicio debe contener un a�o de 4 d�gitos", vbExclamation, "Aviso")
       cboservicio = ""
    End If
End If
End Sub

Private Sub cmdasociar_Click()
Dim mensaje As String
Dim mintiselgrupo As Integer
Dim mintNTotalSelRowsgrupo As Integer
Dim mvarBkmrkgrupo As Variant
Dim mintiselserv As Integer
Dim mintNTotalSelRowsserv As Integer
Dim mvarBkmrkserv As Variant
Dim strInsert As String
Dim rsta As rdoResultset
Dim stra As String
Dim listagrupo As String
Dim listaserv As String
Dim strejercicio As String
Dim rstejercicio As rdoResultset
Dim strinsertG9 As String
Dim strcombo As String
Dim rstcombo As rdoResultset
        
cmdasociar.Enabled = False

If cboservicio = "" Then
    mensaje = MsgBox("Debe seleccionar un ejercicio antes de asociar.", vbInformation, "Aviso")
Else
   'se mira si el ejercicio existe en FRG900
   strejercicio = "SELECT COUNT(*) FROM FRG900 WHERE FRG9EJERCICIO=" & cboservicio
   Set rstejercicio = objApp.rdoConnect.OpenResultset(strejercicio)
   If rstejercicio.rdoColumns(0).Value = 0 Then
   'si el ejercicio no est� se mete en la tabla FRG900
        strinsertG9 = "INSERT INTO FRG900 VALUES(" & cboservicio & ")"
        objApp.rdoConnect.Execute strinsertG9, 64
        objApp.rdoConnect.Execute "Commit", 64
        'se refresca la combo de ejercicios
        Call cboservicio.AddItem(cboservicio)
   End If
   rstejercicio.Close
   Set rstejercicio = Nothing

mintNTotalSelRowsgrupo = grdDBGrid1(0).SelBookmarks.Count
mintNTotalSelRowsserv = grdDBGrid1(1).SelBookmarks.Count
If mintNTotalSelRowsgrupo > 0 And mintNTotalSelRowsserv > 0 Then
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'se forman 2 listas. Una con los grupos seleccionados y otra con los servicios
        listagrupo = "("
        mvarBkmrkgrupo = grdDBGrid1(0).SelBookmarks(mintiselgrupo)
        listagrupo = listagrupo & grdDBGrid1(0).Columns(3).CellValue(mvarBkmrkgrupo)
        For mintiselgrupo = 1 To mintNTotalSelRowsgrupo - 1
            mvarBkmrkgrupo = grdDBGrid1(0).SelBookmarks(mintiselgrupo)
            listagrupo = listagrupo & "," & grdDBGrid1(0).Columns(3).CellValue(mvarBkmrkgrupo)
        Next mintiselgrupo
        listagrupo = listagrupo & ")"
        
        listaserv = "("
        mvarBkmrkserv = grdDBGrid1(1).SelBookmarks(mintiselserv)
        listaserv = listaserv & grdDBGrid1(1).Columns(3).CellValue(mvarBkmrkserv)
        For mintiselserv = 1 To mintNTotalSelRowsserv - 1
            mvarBkmrkserv = grdDBGrid1(1).SelBookmarks(mintiselserv)
            listaserv = listaserv & "," & grdDBGrid1(1).Columns(3).CellValue(mvarBkmrkserv)
        Next mintiselserv
        listaserv = listaserv & ")"
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        For mintiselgrupo = 0 To mintNTotalSelRowsgrupo - 1
            mvarBkmrkgrupo = grdDBGrid1(0).SelBookmarks(mintiselgrupo)
            mintNTotalSelRowsserv = grdDBGrid1(1).SelBookmarks.Count
            For mintiselserv = 0 To mintNTotalSelRowsserv - 1
            mvarBkmrkserv = grdDBGrid1(1).SelBookmarks(mintiselserv)
            stra = "SELECT COUNT(*) FROM FRH100 WHERE " & _
                "FRG9EJERCICIO=" & cboservicio & " AND " & _
                "FR41CODGRUPPROD=" & grdDBGrid1(0).Columns(3).CellValue(mvarBkmrkgrupo) & " AND " & _
                "AD02CODDPTO=" & grdDBGrid1(1).Columns(3).CellValue(mvarBkmrkserv)
            Set rsta = objApp.rdoConnect.OpenResultset(stra)
            If rsta.rdoColumns(0).Value = 0 Then
                  'se inserta en FRH100
                  Call rellenar_FRH100(grdDBGrid1(0).Columns(3).CellValue(mvarBkmrkgrupo), _
                                   grdDBGrid1(1).Columns(3).CellValue(mvarBkmrkserv))
            End If
            rsta.Close
            Set rsta = Nothing
            Next mintiselserv
        Next mintiselgrupo
        Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
        objWinInfo.objWinActiveForm.strWhere = "FRG9EJERCICIO=" & _
                                            cboservicio & " AND " & _
                                            "FR41CODGRUPPROD IN " & listagrupo & " AND " & _
                                            "AD02CODDPTO IN " & listaserv
        objWinInfo.DataRefresh
Else
        mensaje = MsgBox("Debe seleccionar al menos un grupo y un servicio.", vbInformation, "Aviso")
End If
End If

cmdasociar.Enabled = True
End Sub

Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset
cboservicio.RemoveAll
stra = "select FRG9EJERCICIO from FRG900 ORDER BY FRG9EJERCICIO DESC"
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While (Not rsta.EOF)
    Call cboservicio.AddItem(rsta.rdoColumns("FRG9EJERCICIO").Value)
    rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing

objWinInfo.DataRefresh
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm
    Dim objMultiInfo1 As New clsCWForm
    Dim objMultiInfo2 As New clsCWForm
    Dim strKey As String
    Dim rstServiCompr As rdoResultset
    Dim strServiCompr As String
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
    
        strServiCompr = ""
        strServiCompr = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=4"
        Set rstServiCompr = objApp.rdoConnect.OpenResultset(strServiCompr)
        strServiCompr = rstServiCompr("FRH2PARAMGEN").Value
        rstServiCompr.Close
        Set rstServiCompr = Nothing
        
        .strName = "Grupos de Compras"
        Set .objFormContainer = fraframe1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(0)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR4100"
        .intAllowance = cwAllowReadOnly
        's�lamente los grupos del servicio de Compras
        .strWhere = "AD02CODDPTO=" & strServiCompr
        
        Call .FormAddOrderField("FR41DESGRUPPROD", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Grupos de Compras")
        Call .FormAddFilterWhere(strKey, "FR41CODGRUPPROD", "C�digo Grupo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR41DESGRUPPROD", "Descripci�n Grupo", cwString)

        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "C�digo Grupo")
        Call .FormAddFilterOrder(strKey, "FR41DESGRUPPROD", "Descripci�n Grupo")
    End With
    
    With objMultiInfo1
        .strName = "Servicios"
        Set .objFormContainer = fraframe1(1)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "AD0200"
        .strWhere = "AD32CODTIPODPTO=3 AND AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL))) "
        .intAllowance = cwAllowReadOnly
        
        Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Grupos de Compras")
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
        Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Descripci�n Servicio", cwString)

        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Servicio")
        Call .FormAddFilterOrder(strKey, "AD02DESDPTO", "Descripci�n Servicio")
    End With
    
    With objMultiInfo2
        .strName = "Grupos Presupuestarios"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(2)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FRH100"
        .intAllowance = cwAllowReadOnly
        'para que al entrar este grid no muestre nada
        .strWhere = "FRG9EJERCICIO is null"
        
        Call .FormAddOrderField("FRG9EJERCICIO", cwAscending)
        Call .FormAddOrderField("AD02CODDPTO", cwAscending)
        Call .FormAddOrderField("FR41CODGRUPPROD", cwAscending)
        Call .FormAddOrderField("FRH1MES", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Grupos Presupuestarios")
        Call .FormAddFilterWhere(strKey, "FRG9EJERCICIO", "Ejercicio", cwNumeric)
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "Servicio", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR41CODGRUPPROD", "Grupo de Productos", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FRH1MES", "Mes", cwString)
        Call .FormAddFilterWhere(strKey, "FRH1CANTIDADPTAS", "Cantidad Ptas", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FRH1CANTIDADEURO", "Cantidad Euros", cwNumeric)
        

        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FRG9EJERCICIO", "Ejercicio")
        Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "Servicio")
        Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "Grupo de Productos")
        Call .FormAddFilterOrder(strKey, "FRH1MES", "Mes")
        Call .FormAddFilterOrder(strKey, "FRH1CANTIDADPTAS", "Cantidad Ptas")
        Call .FormAddFilterOrder(strKey, "FRH1CANTIDADEURO", "Cantidad Euros")
    End With

    With objWinInfo
        'GRUPOS DE COMPRAS
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�digo Grupo", "FR41CODGRUPPROD", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo, "Grupo", "FR41DESGRUPPROD", cwString, 50)
        Call .GridAddColumn(objMultiInfo, "C�d.Servicio", "AD02CODDPTO", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 30)
        
        
        Call .FormCreateInfo(objMultiInfo)
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
        
        'SERVICIOS
        Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo1, "C�digo Servicio", "AD02CODDPTO", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo1, "Servicio", "AD02DESDPTO", cwString, 30)
        
        Call .FormCreateInfo(objMultiInfo1)
        Call .FormChangeColor(objMultiInfo1)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        
        'GRUPOS PRESUPUESTARIOS
        Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo2, "Ejercicio", "FRG9EJERCICIO", cwNumeric, 4)
        Call .GridAddColumn(objMultiInfo2, "C�d.Servicio", "AD02CODDPTO", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo2, "Servicio", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo2, "C�d.Grupo", "FR41CODGRUPPROD", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo2, "Grupo de Compra", "", cwString, 50)
        Call .GridAddColumn(objMultiInfo2, "Mes", "FRH1MES", cwString, 12)
        Call .GridAddColumn(objMultiInfo2, "Cantidad Ptas", "FRH1CANTIDADPTAS", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo2, "Cantidad Euro", "FRH1CANTIDADEURO", cwDecimal, 2)
        
        
        Call .FormCreateInfo(objMultiInfo2)
        Call .FormChangeColor(objMultiInfo2)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(2).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(8)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(9)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(10)).blnInFind = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD32CODTIPODPTO=3 AND AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL))) AND AD02CODDPTO = ? ")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "AD02DESDPTO")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(4)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD32CODTIPODPTO=3 AND AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL))) AND AD02CODDPTO = ? ")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(4)), grdDBGrid1(2).Columns(5), "AD02DESDPTO")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(6)), "FR41CODGRUPPROD", "SELECT * FROM FR4100 WHERE FR41CODGRUPPROD = ? ")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(6)), grdDBGrid1(2).Columns(7), "FR41DESGRUPPROD")

        Call .WinRegister
        Call .WinStabilize
    End With
    
    grdDBGrid1(0).Columns(3).Visible = False
    grdDBGrid1(0).Columns(5).Visible = False
    grdDBGrid1(0).Columns(6).Visible = False
    grdDBGrid1(1).Columns(3).Visible = False
    
    Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
    objWinInfo.DataRefresh
    
    grdDBGrid1(0).Columns(4).Width = 4200
    grdDBGrid1(1).Columns(4).Width = 3900
    
    grdDBGrid1(2).Columns(3).Width = 1000
    grdDBGrid1(2).Columns(4).Width = 1050
    grdDBGrid1(2).Columns(5).Width = 1950
    grdDBGrid1(2).Columns(6).Width = 1000
    grdDBGrid1(2).Columns(7).Width = 1950
    
    grdDBGrid1(2).Columns(8).Width = 1350
    grdDBGrid1(2).Columns(9).Width = 1200
    grdDBGrid1(2).Columns(10).Width = 1200
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim mintiselgrupo As Integer
Dim mintNTotalSelRowsgrupo As Integer
Dim mvarBkmrkgrupo As Variant
Dim mintiselserv As Integer
Dim mintNTotalSelRowsserv As Integer
Dim mvarBkmrkserv As Variant
Dim listagrupo As String
Dim listaserv As String

If objWinInfo.objWinActiveForm.strName = "Grupos Presupuestarios" And btnButton.Index = 26 Then

mintNTotalSelRowsgrupo = grdDBGrid1(0).SelBookmarks.Count
mintNTotalSelRowsserv = grdDBGrid1(1).SelBookmarks.Count
If mintNTotalSelRowsgrupo > 0 And mintNTotalSelRowsserv > 0 And cboservicio <> "" Then
    'se forman 2 listas. Una con los grupos seleccionados y otra con los servicios
    listagrupo = "("
    mvarBkmrkgrupo = grdDBGrid1(0).SelBookmarks(mintiselgrupo)
    listagrupo = listagrupo & grdDBGrid1(0).Columns(3).CellValue(mvarBkmrkgrupo)
    For mintiselgrupo = 1 To mintNTotalSelRowsgrupo - 1
        mvarBkmrkgrupo = grdDBGrid1(0).SelBookmarks(mintiselgrupo)
        listagrupo = listagrupo & "," & grdDBGrid1(0).Columns(3).CellValue(mvarBkmrkgrupo)
    Next mintiselgrupo
    listagrupo = listagrupo & ")"
        
    listaserv = "("
    mvarBkmrkserv = grdDBGrid1(1).SelBookmarks(mintiselserv)
    listaserv = listaserv & grdDBGrid1(1).Columns(3).CellValue(mvarBkmrkserv)
    For mintiselserv = 1 To mintNTotalSelRowsserv - 1
        mvarBkmrkserv = grdDBGrid1(1).SelBookmarks(mintiselserv)
        listaserv = listaserv & "," & grdDBGrid1(1).Columns(3).CellValue(mvarBkmrkserv)
    Next mintiselserv
    listaserv = listaserv & ")"

    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    objWinInfo.objWinActiveForm.strWhere = "FRG9EJERCICIO=" & _
                                cboservicio & " AND " & _
                                "FR41CODGRUPPROD IN " & listagrupo & " AND " & _
                                "AD02CODDPTO IN " & listaserv
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End If


Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
Dim mintiselgrupo As Integer
Dim mintNTotalSelRowsgrupo As Integer
Dim mvarBkmrkgrupo As Variant
Dim mintiselserv As Integer
Dim mintNTotalSelRowsserv As Integer
Dim mvarBkmrkserv As Variant
Dim listagrupo As String
Dim listaserv As String

If objWinInfo.objWinActiveForm.strName = "Grupos Presupuestarios" And intIndex = 10 Then

mintNTotalSelRowsgrupo = grdDBGrid1(0).SelBookmarks.Count
mintNTotalSelRowsserv = grdDBGrid1(1).SelBookmarks.Count
If mintNTotalSelRowsgrupo > 0 And mintNTotalSelRowsserv > 0 And cboservicio <> "" Then
    'se forman 2 listas. Una con los grupos seleccionados y otra con los servicios
    listagrupo = "("
    mvarBkmrkgrupo = grdDBGrid1(0).SelBookmarks(mintiselgrupo)
    listagrupo = listagrupo & grdDBGrid1(0).Columns(3).CellValue(mvarBkmrkgrupo)
    For mintiselgrupo = 1 To mintNTotalSelRowsgrupo - 1
        mvarBkmrkgrupo = grdDBGrid1(0).SelBookmarks(mintiselgrupo)
        listagrupo = listagrupo & "," & grdDBGrid1(0).Columns(3).CellValue(mvarBkmrkgrupo)
    Next mintiselgrupo
    listagrupo = listagrupo & ")"
        
    listaserv = "("
    mvarBkmrkserv = grdDBGrid1(1).SelBookmarks(mintiselserv)
    listaserv = listaserv & grdDBGrid1(1).Columns(3).CellValue(mvarBkmrkserv)
    For mintiselserv = 1 To mintNTotalSelRowsserv - 1
        mvarBkmrkserv = grdDBGrid1(1).SelBookmarks(mintiselserv)
        listaserv = listaserv & "," & grdDBGrid1(1).Columns(3).CellValue(mvarBkmrkserv)
    Next mintiselserv
    listaserv = listaserv & ")"

    Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
    objWinInfo.objWinActiveForm.strWhere = "FRG9EJERCICIO=" & _
                                cboservicio & " AND " & _
                                "FR41CODGRUPPROD IN " & listagrupo & " AND " & _
                                "AD02CODDPTO IN " & listaserv
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End If


Else
        Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End If
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


