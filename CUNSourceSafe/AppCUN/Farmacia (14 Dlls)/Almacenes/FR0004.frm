VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMantAlmacen 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MANTENIMIENTO FARMACIA. Mantenimiento Almac�n."
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Almac�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6615
      Index           =   0
      Left            =   600
      TabIndex        =   0
      Top             =   960
      Width           =   10250
      Begin TabDlg.SSTab tabTab1 
         Height          =   5940
         Index           =   0
         Left            =   240
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   480
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   10478
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0004.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(6)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(4)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(7)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(10)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(8)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtcDateCombo1(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "dtcDateCombo1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(4)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "chkCheck1(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "chkCheck1(2)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(3)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(2)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).ControlCount=   18
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0004.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   2
            Left            =   360
            TabIndex        =   3
            Tag             =   "C�d. Ser.|C�digo Servicio"
            Top             =   1200
            Width           =   630
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   3
            Left            =   1680
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Descripci�n Servicio"
            Top             =   1200
            Width           =   5295
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "FR04INDPRINCIPAL"
            Height          =   255
            Index           =   2
            Left            =   5280
            TabIndex        =   7
            Tag             =   "A.P.S.|Almac�n Principal del Servicio?"
            Top             =   2040
            Width           =   255
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "FR04INDDEPOSPROVEE"
            Height          =   255
            Index           =   1
            Left            =   2760
            TabIndex        =   6
            Tag             =   "A.D.|Almac�n de Dep�sito?"
            Top             =   2040
            Width           =   255
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR04TPEXAMRECALE"
            Height          =   330
            Index           =   4
            Left            =   360
            TabIndex        =   5
            Tag             =   "%Exam.|TP Productos a Examinar en el recuento aleatorio"
            Top             =   1920
            Width           =   1110
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR04DESALMACEN"
            Height          =   330
            Index           =   1
            Left            =   1680
            ScrollBars      =   2  'Vertical
            TabIndex        =   2
            Tag             =   "Descripci�n Almac�n|Descripci�n Almac�n"
            Top             =   480
            Width           =   5160
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR04CODALMACEN"
            Height          =   330
            Index           =   0
            Left            =   360
            TabIndex        =   1
            Tag             =   "C�d. Alm.|C�digo Almac�n"
            Top             =   480
            Width           =   630
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5385
            Index           =   0
            Left            =   -74760
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   240
            Width           =   9015
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15901
            _ExtentY        =   9499
            _StockProps     =   79
            Caption         =   "ALMACEN"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR04FECFINVIG"
            Height          =   330
            Index           =   0
            Left            =   3240
            TabIndex        =   9
            Tag             =   "Fin Vig.|Fecha Fin Vigencia Almac�n"
            Top             =   2760
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR04FECINIVIG"
            Height          =   330
            Index           =   1
            Left            =   360
            TabIndex        =   8
            Tag             =   "Inicio Vig.|Fecha Inicio Vigencia Almac�n"
            Top             =   2760
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d. Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   360
            TabIndex        =   22
            Top             =   960
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   1680
            TabIndex        =   21
            Top             =   960
            Width           =   1770
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Almac�n Principal del Servicio?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   5280
            TabIndex        =   20
            Top             =   1800
            Width           =   2700
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Almac�n de Dep�sito?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   2760
            TabIndex        =   19
            Top             =   1800
            Width           =   2655
         End
         Begin VB.Label lblLabel1 
            Caption         =   "% Productos a Examinar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   360
            TabIndex        =   18
            Top             =   1680
            Width           =   2175
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   360
            TabIndex        =   17
            Top             =   2520
            Width           =   2775
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   3240
            TabIndex        =   16
            Top             =   2520
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Almac�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   1680
            TabIndex        =   14
            Top             =   240
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Almac�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   13
            Top             =   240
            Width           =   1815
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMantAlmacen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmMantAlmacen (FR0004.FRM)                                  *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: 19 DE AGOSTO DE 1998                                          *
'* DESCRIPCION: mantenimiento de Almacen                                *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gintIndice As Integer


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
   Dim mensaje As String
   If intIndex = 1 Then
      If dtcDateCombo1(1).Text <> "" And dtcDateCombo1(0).Text <> "" Then
         If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) > 0 Then
            mensaje = MsgBox("La fecha de Inicio de Vigencia es mayor que la de Fin", vbInformation, "Aviso")
            dtcDateCombo1(1).Text = ""
         End If
      End If
   End If
   If intIndex = 0 Then
      If dtcDateCombo1(1).Text <> "" And dtcDateCombo1(0).Text <> "" Then
         If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) > 0 Then
            mensaje = MsgBox("La fecha de Fin de Vigencia es menor que la de Inicio", vbInformation, "Aviso")
            dtcDateCombo1(0).Text = ""
         End If
      End If
   End If
  
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
   Dim mensaje As String
   If intIndex = 1 Then
      If dtcDateCombo1(1).Text <> "" And dtcDateCombo1(0).Text <> "" Then
         If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) > 0 Then
            mensaje = MsgBox("La fecha de Inicio de Vigencia es mayor que la de Fin", vbInformation, "Aviso")
            dtcDateCombo1(1).Text = ""
         End If
      End If
   End If
   If intIndex = 0 Then
      If dtcDateCombo1(1).Text <> "" And dtcDateCombo1(0).Text <> "" Then
         If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) > 0 Then
            mensaje = MsgBox("La fecha de Fin de Vigencia es menor que la de Inicio", vbInformation, "Aviso")
            dtcDateCombo1(0).Text = ""
         End If
      End If
   End If
  
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Almacen"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "FR0400"
    .intCursorSize = 0
    
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    Call .FormAddOrderField("FR04CODALMACEN", cwAscending)
    
    Call .objPrinter.Add("FRR041", "Almacenes")
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Almacen")
    Call .FormAddFilterWhere(strKey, "FR04CODALMACEN", "C�digo Almac�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR04DESALMACEN", "Descripci�n Almac�n", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "Cod.Servicio", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR08CODCENTRCOSTE", "Centro Coste", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR04TPEXAMRECALE", "TP Productos a Examinar", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR04INDALMPRINCI", "Almac�n de Farmacia?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR04INDDEPOSPROVEE", "Almac�n de Dep�sito?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR04INDPRINCIPAL", "Almac�n Principal del Servicio?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR04FECINIVIG", "Fecha Inicio Vigencia Almac�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR04FECFINVIG", "Fecha Fin Vigencia Almac�n", cwDate)
    
    Call .FormAddFilterOrder(strKey, "FR04CODALMACEN", "C�digo Almac�n")
 
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
    
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    .CtrlGetInfo(txtText1(1)).blnMandatory = True

    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    '.CtrlGetInfo(txtText1(5)).blnInFind = True
    '.CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
'    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    '.CtrlGetInfo(txtText1(5)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL))) AND AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "AD02DESDPTO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "FR08CODCENTRCOSTE", "SELECT * FROM FR0800 WHERE FR08CODCENTRCOSTE = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(6), "FR08DESCENTRCOSTE")
    
    Call .WinRegister
    Call .WinStabilize
  End With

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim objSearch As clsCWSearch
  
  If strFormName = "Almacen" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD32CODTIPODPTO=3 AND AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL)))"
     .strOrder = "ORDER BY AD02CODDPTO ASC"
     
     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Servicio"
         
     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Servicio"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
  'If strFormName = "Almacen" And strCtrl = "txtText1(5)" Then
  '  Set objSearch = New clsCWSearch
  '  With objSearch
  '   .strTable = "FR0800"
  '   .strOrder = "ORDER BY FR08CODCENTRCOSTE ASC"
  '
  '   Set objField = .AddField("FR08CODCENTRCOSTE")
  '   objField.strSmallDesc = "C�digo Centro Coste"
  '
  '   Set objField = .AddField("FR08DESCENTRCOSTE")
  '   objField.strSmallDesc = "Descripci�n Centro Coste"
  '
  '   If .Search Then
  '    Call objWinInfo.CtrlSet(txtText1(5), .cllValues("FR08CODCENTRCOSTE"))
  '   End If
  ' End With
  ' Set objSearch = Nothing
  'End If

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim sqlstr As String
Dim curTP As Currency

  If txtText1(2).Text <> "" Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
    'sqlstr = "SELECT * FROM AD0200 WHERE AD02CODDPTO=" & txtText1(2).Text
    'sqlstr = sqlstr & " AND AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL)))"
    sqlstr = "SELECT * FROM AD0200 WHERE AD02CODDPTO=?"
    sqlstr = sqlstr & " AND SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    Set qrya = objApp.rdoConnect.CreateQuery("", sqlstr)
    qrya(0) = txtText1(2).Text
    Set rsta = qrya.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    'Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If rsta.EOF = True Then
      MsgBox "El C�d.Servicio es incorrecto.", vbExclamation
      blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
  End If
  
   If txtText1(4).Text <> "" Then
      If IsNumeric(txtText1(4).Text) Then
         curTP = txtText1(4).Text
         If curTP < 0 Then
            MsgBox "% Productos a Examinar ha de ser Positivo", vbExclamation
            txtText1(4).Text = ""
         End If
      End If
   End If
         
      
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Almacen" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
     
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, False))
    End If
    Set objPrinter = Nothing
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)

    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Dim qryalmacen As rdoQuery
  Dim stralmacen As String
  Dim rstalmacen As rdoResultset
  
  If txtText1(2).Text = "" Then
    Call MsgBox("Debe introducir primero el servicio", vbInformation, "Aviso")
    chkCheck1(2).Value = 0
    Exit Sub
  Else
    If chkCheck1(2).Value = 1 Then
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
      'stralmacen = "select count(*) from fr0400 where ad02coddpto=" & txtText1(2).Text & _
      '    " and fr04indprincipal=-1 and FR04CODALMACEN<>" & txtText1(0).Text
      stralmacen = "select count(*) from fr0400 where ad02coddpto=?" & _
          " and fr04indprincipal=? and FR04CODALMACEN<>?"
      Set qryalmacen = objApp.rdoConnect.CreateQuery("", stralmacen)
      qryalmacen(0) = txtText1(2).Text
      qryalmacen(1) = -1
      qryalmacen(2) = txtText1(0).Text
      Set rstalmacen = qryalmacen.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      'Set rstalmacen = objApp.rdoConnect.OpenResultset(stralmacen)
      If rstalmacen(0).Value > 0 Then
        Call MsgBox("Ya hay un almacen principal para ese servicio", vbInformation, "Aviso")
        chkCheck1(2).Value = 0
        Exit Sub
      End If
    End If
  End If
    Call objWinInfo.CtrlDataChange
End Sub

