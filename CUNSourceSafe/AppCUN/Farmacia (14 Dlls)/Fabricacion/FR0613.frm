VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form frmRecCarroMez 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Dispensar Mezclas. Salida de Carros Mezclas"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdRecTodos 
      Caption         =   "Recoger TODOS"
      Height          =   735
      Left            =   10680
      TabIndex        =   25
      Top             =   3120
      Width           =   1095
   End
   Begin VB.CommandButton cmdImprTodos 
      Caption         =   "Imprimir Selecci�n"
      Height          =   675
      Left            =   10680
      TabIndex        =   24
      Top             =   1800
      Width           =   1095
   End
   Begin VB.CommandButton cmdSelTodos 
      Caption         =   "Sel.Todos"
      Height          =   315
      Left            =   10680
      TabIndex        =   21
      Top             =   1320
      Width           =   1095
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   10680
      Top             =   3600
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdrecogercarro 
      Caption         =   "Dispensar Carro"
      Height          =   255
      Left            =   4800
      TabIndex        =   20
      Top             =   3960
      Width           =   2055
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Carros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3375
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   480
      Width           =   10380
      Begin TabDlg.SSTab tabTab1 
         Height          =   2895
         Index           =   0
         Left            =   240
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   5106
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0613.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(7)"
         Tab(0).Control(1)=   "txtText1(6)"
         Tab(0).Control(2)=   "txtText1(5)"
         Tab(0).Control(3)=   "txtText1(2)"
         Tab(0).Control(4)=   "txtText1(1)"
         Tab(0).Control(5)=   "txtText1(0)"
         Tab(0).Control(6)=   "txtText1(4)"
         Tab(0).Control(7)=   "txtText1(3)"
         Tab(0).Control(8)=   "lblLabel1(6)"
         Tab(0).Control(9)=   "lblLabel1(1)"
         Tab(0).Control(10)=   "lblLabel1(3)"
         Tab(0).Control(11)=   "lblLabel1(0)"
         Tab(0).Control(12)=   "lblLabel1(4)"
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0613.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74HORASALIDA"
            Height          =   330
            Index           =   7
            Left            =   -73440
            TabIndex        =   7
            Tag             =   "Hora"
            Top             =   2520
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74DIA"
            Height          =   330
            Index           =   6
            Left            =   -74640
            TabIndex        =   6
            Tag             =   "D�a"
            Top             =   2520
            Width           =   500
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR87DESESTCARRO"
            Height          =   330
            Index           =   5
            Left            =   -74040
            TabIndex        =   5
            Tag             =   "Estado Carro"
            Top             =   1800
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR87CODESTCARRO"
            Height          =   330
            Index           =   2
            Left            =   -74640
            TabIndex        =   4
            Tag             =   "C�s.Estado Carro"
            Top             =   1800
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD02DESDPTO"
            Height          =   330
            Index           =   1
            Left            =   -74040
            TabIndex        =   1
            Tag             =   "Servicio"
            Top             =   360
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   0
            Left            =   -74640
            TabIndex        =   0
            Tag             =   "C�d.Servicio"
            Top             =   360
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR07DESCARRO"
            Height          =   330
            Index           =   4
            Left            =   -74040
            TabIndex        =   3
            Tag             =   "Carro"
            Top             =   1080
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR07CODCARRO"
            Height          =   330
            Index           =   3
            Left            =   -74640
            TabIndex        =   2
            Tag             =   "C�d.Carro"
            Top             =   1080
            Width           =   400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   2
            Left            =   0
            TabIndex        =   12
            Top             =   0
            Width           =   9975
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17595
            _ExtentY        =   5054
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   -73440
            TabIndex        =   19
            Top             =   2280
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "D�a Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -74640
            TabIndex        =   18
            Top             =   2280
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   -74640
            TabIndex        =   17
            Top             =   1560
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -74640
            TabIndex        =   15
            Top             =   120
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   -74640
            TabIndex        =   13
            Top             =   840
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Peticiones del Carro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3705
      Index           =   0
      Left            =   0
      TabIndex        =   14
      Top             =   4320
      Width           =   11895
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3225
         Index           =   0
         Left            =   75
         TabIndex        =   16
         Top             =   360
         Width           =   11745
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   20717
         _ExtentY        =   5689
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo cbohora 
      Height          =   315
      Left            =   10680
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   840
      Width           =   975
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ";"
      DefColWidth     =   9
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "Hora Salida"
      Columns(0).Name =   "Hora Salida"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1720
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Hora Salida"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   10680
      TabIndex        =   23
      Top             =   600
      Width           =   1095
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRecCarroMez"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRecCarroMez (FR0613.FRM)                                 *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: recoger carro                                           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1


Private Sub cbohora_CloseUp()

Dim strFechaSer As String
Dim rstFechaSer As rdoResultset
Dim fecha As Date
Dim intDiaSem As Integer
Dim DiaSem(7) As String
    
strFechaSer = "SELECT SYSDATE FROM DUAL"
Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
While rstFechaSer.StillExecuting
Wend

fecha = rstFechaSer(0).Value
rstFechaSer.Close
intDiaSem = WeekDay(fecha, vbMonday)
DiaSem(1) = "LUN"
DiaSem(2) = "MAR"
DiaSem(3) = "MIE"
DiaSem(4) = "JUE"
DiaSem(5) = "VIE"
DiaSem(6) = "SAB"
DiaSem(7) = "DOM"

If cboHora.Text <> "" Then
  Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
  objWinInfo.objWinActiveForm.strWhere = "FR87CODESTCARRO IN (1,2,3,6) AND FR74DIA='" & DiaSem(intDiaSem) & "'"
  objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " AND FR74HORASALIDA= " & objGen.ReplaceStr(cboHora, ",", ".", 1)
  objWinInfo.DataRefresh
  'grdDBGrid1(0).RemoveAll
End If

End Sub

Private Sub cmdImprTodos_Click()

'Call objWinInfo_cwPrint("Peticiones del Carro")
Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
tlbToolbar1.Buttons(6).Enabled = True
Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6))

End Sub

Private Sub cmdRecTodos_Click()
Dim strUpdate As String
Dim hora As String
Dim mensaje As String
Dim strSql As String
Dim qrySQL As rdoQuery
Dim rstSQL As rdoResultset
Dim qryUpd As rdoQuery
Dim strinsert As String
Dim qryInsert As rdoQuery
Dim updEstCarMIV As String
Dim qryupdEstCarMIV As rdoQuery
Dim updEstCarMIV_Ult As String
Dim qryupdEstCarMIV_Ult As rdoQuery

Screen.MousePointer = vbHourglass
cmdRecTodos.Enabled = False
Me.Enabled = False

Call cmdSelTodos_Click

  objCW.SetClock (60 * 4)
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False

  'transforma la coma de separaci�n de los decimales por un punto
  hora = objGen.ReplaceStr(cboHora, ",", ".", 1)
    
  strinsert = "INSERT INTO FR6500 (FR65CODIGO,CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,"
  strinsert = strinsert & " FR65CANTIDAD,FR73CODPRODUCTO_DIL,"
  strinsert = strinsert & " FR65CANTIDADDIL,FR73CODPRODUCTO_2,FR65DOSIS_2,"
  strinsert = strinsert & " FR65DESPRODUCTO,"
  strinsert = strinsert & " FR66CODPETICION,FR65INDPRN,AD01CODASISTENCI,AD07CODPROCESO,"
  strinsert = strinsert & " FR65INDINTERCIENT,AD02CODDPTO_CRG) "
  strinsert = strinsert & " SELECT FR65CODIGO_SEQUENCE.NEXTVAL,"
  strinsert = strinsert & " CI21CODPERSONA,"
  strinsert = strinsert & " CODPRODUCTO_1,SYSDATE,TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI'))" & ","
  strinsert = strinsert & " CANTIDAD_1,CODPRODUCTO_DIL,"
  strinsert = strinsert & " CANTDIL,CODPRODUCTO_2,"
  strinsert = strinsert & " DOSIS_2,DESPRODUCTO_1,"
  strinsert = strinsert & " FR66CODPETICION,0,"
  strinsert = strinsert & " AD01CODASISTENCI,"
  strinsert = strinsert & " AD07CODPROCESO,FR66INDINTERCIENT,AD02CODDPTO_CRG"
  strinsert = strinsert & " FROM "
  strinsert = strinsert & " ( SELECT DISTINCT "
  strinsert = strinsert & "  FR2800.FR66CODPETICION"
  strinsert = strinsert & "  ,FR2800.FR28NUMLINEA"
  strinsert = strinsert & "  ,FR3300.FR33HORATOMA"
  strinsert = strinsert & "  ,FR6600.CI21CODPERSONA"
  strinsert = strinsert & "  ,FR6600.AD07CODPROCESO"
  strinsert = strinsert & "  ,FR6600.AD01CODASISTENCI"
  strinsert = strinsert & "  ,FR6600.FR66INDINTERCIENT"
  strinsert = strinsert & "  ,FR6600.AD02CODDPTO_CRG"
  strinsert = strinsert & "  ,FR7300_1.FR73CODPRODUCTO CODPRODUCTO_1"
  strinsert = strinsert & "  ,DECODE(FR7300_1.FR73CODPRODUCTO,999999999,FR2800.FR28DESPRODUCTO,FR7300_1.FR73DESPRODUCTO)"
  strinsert = strinsert & "  DESPRODUCTO_1"
  strinsert = strinsert & "  ,FR2800.FR28CANTIDAD CANTIDAD_1"
  strinsert = strinsert & "  ,FR2800.FR73CODPRODUCTO_DIL CODPRODUCTO_DIL"
  strinsert = strinsert & "  ,FR2800.FR28CANTIDADDIL CANTDIL"
  strinsert = strinsert & "  ,FR7300_2.FR73CODPRODUCTO CODPRODUCTO_2"
  strinsert = strinsert & "  ,FR2800.FR28DOSIS_2 DOSIS_2"
  strinsert = strinsert & " FROM FR3300,FR2800,FR6600,FR7300"
  strinsert = strinsert & " FR7300_1,FR7300"
  strinsert = strinsert & " FR7300_2,FR7300 FR7300_DIL"
  strinsert = strinsert & " WHERE FR3300.FR33INDMEZCLA=?"
  strinsert = strinsert & " AND  FR3300.FR66CODPETICION=FR2800.FR66CODPETICION"
  strinsert = strinsert & " AND  FR3300.FR33MOTDIFERENCIA=?"
  strinsert = strinsert & " AND  FR3300.FR28NUMLINEA=FR2800.FR28NUMLINEA"
  strinsert = strinsert & " AND  FR3300.FR66CODPETICION=FR6600.FR66CODPETICION"
  strinsert = strinsert & " AND  FR6600.FR66INDOM=?"
  strinsert = strinsert & " AND  FR2800.FR73CODPRODUCTO=FR7300_1.FR73CODPRODUCTO(+)"
  strinsert = strinsert & " AND  FR2800.FR73CODPRODUCTO_2=FR7300_2.FR73CODPRODUCTO(+)"
  strinsert = strinsert & " AND  FR2800.FR73CODPRODUCTO_DIL=FR7300_DIL.FR73CODPRODUCTO(+)"
  strinsert = strinsert & " AND  FR3300.FR33FECCARGA=TRUNC(sysdate)"
  strinsert = strinsert & " AND  FR3300.FR33HORACARGA=" & hora
  strinsert = strinsert & ")"
  Set qryInsert = objApp.rdoConnect.CreateQuery("", strinsert)
  qryInsert(0) = -1
  qryInsert(1) = "A�adido"
  qryInsert(2) = -1
  qryInsert.Execute
                  
                  
                  
  '28-SEPTIEMBRE-2000, se quitan los movimientos a almac�n
  '-------------------------------------------------------
  'Call Movimientos_Almacen_2(hora, grdDBGrid1(2).Columns("D�a").Value)
  Call Ins_FR6500_Prod_Por_Mezcla_2(hora)

  'Se cambia el estado de los carros (estado=1 No se ha iniciado la carga)
  'si el carro recogido es el �ltimo del d�a
  strSql = "SELECT MAX(FR74HORASALIDA)"
  strSql = strSql & " FROM FR7400"
  strSql = strSql & " WHERE "
  strSql = strSql & " FR74DIA =? "
  strSql = strSql & " AND FR87CODESTCARRO_MIV IN (1,6,2,3)"
  Set qrySQL = objApp.rdoConnect.CreateQuery("", strSql)
  
  'se cambia el estado del carro (estado=3 Cerrado � 1 si es el ultimo del dia)
  updEstCarMIV = "UPDATE FR7400 SET FR87CODESTCARRO_MIV=?,FR87CODESTCARRO_CIT=? WHERE "
  updEstCarMIV = updEstCarMIV & " FR74DIA=?"
  updEstCarMIV = updEstCarMIV & " AND FR74HORASALIDA=" & objGen.ReplaceStr(cboHora, ",", ".", 1)
  Set qryupdEstCarMIV = objApp.rdoConnect.CreateQuery("", updEstCarMIV)
    
  'se cambia el estado del carro (estado=3 Cerrado � 1 si es el ultimo del dia)
  updEstCarMIV_Ult = "UPDATE FR7400 SET FR87CODESTCARRO_MIV=?,FR87CODESTCARRO_CIT=? WHERE "
  updEstCarMIV_Ult = updEstCarMIV_Ult & " FR74DIA=?"
  Set qryupdEstCarMIV_Ult = objApp.rdoConnect.CreateQuery("", updEstCarMIV_Ult)
    
  'se cambia el estado del carro (estado=3 Cerrado o 1)
  qrySQL(0) = grdDBGrid1(2).Columns("D�a").Value
  Set rstSQL = qrySQL.OpenResultset()
  While rstSQL.StillExecuting
  Wend
  If rstSQL.rdoColumns(0).Value = cboHora Or rstSQL.rdoColumns(0).Value = 0 Then
    qryupdEstCarMIV_Ult(0) = 1
    qryupdEstCarMIV_Ult(1) = 1
    qryupdEstCarMIV_Ult(2) = grdDBGrid1(2).Columns("D�a").Value
    qryupdEstCarMIV_Ult.Execute
  Else
    qryupdEstCarMIV(0) = 3
    qryupdEstCarMIV(1) = 3
    qryupdEstCarMIV(2) = grdDBGrid1(2).Columns("D�a").Value
    qryupdEstCarMIV.Execute
  End If
  rstSQL.Close
  Set rstSQL = Nothing
    

Screen.MousePointer = vbDefault
Me.Enabled = True
cmdRecTodos.Enabled = True

  MsgBox "Mezclas Recogidas", vbInformation, "Aviso"

End Sub

Private Sub cmdSelTodos_Click()
Dim i As Integer

cmdSelTodos.Enabled = False

grdDBGrid1(2).Redraw = False
grdDBGrid1(2).SelBookmarks.RemoveAll
grdDBGrid1(2).MoveFirst ' Position at the first row
For i = 0 To grdDBGrid1(2).Rows
  grdDBGrid1(2).SelBookmarks.Add grdDBGrid1(2).Bookmark
  grdDBGrid1(2).MoveNext
Next i
grdDBGrid1(2).Redraw = True
cmdSelTodos.Enabled = True
End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    If objWinInfo.objWinActiveForm.strName = "Carros" Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub


Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cmdrecogercarro_Click()
Dim strUpdate As String
Dim hora As String
Dim mensaje As String
Dim strSql As String
Dim qrySQL As rdoQuery
Dim rstSQL As rdoResultset
Dim qryUpd As rdoQuery
Dim mintNTotalSelRows As Integer
Dim mintisel As Integer
Dim mvarBkmrk As Variant
Dim updEstCarMIV As String
Dim qryupdEstCarMIV As rdoQuery
Dim updEstCarMIV_Ult As String
Dim qryupdEstCarMIV_Ult As rdoQuery

Screen.MousePointer = vbHourglass
Me.Enabled = False
cmdrecogercarro.Enabled = False

'  strCarros = ""

'se actualiza la tabla FR6500
mintNTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
  
'Se cambia el estado de los carros (estado=1 No se ha iniciado la carga)
'si el carro recogido es el �ltimo del d�a
strSql = "SELECT MAX(FR74HORASALIDA)"
strSql = strSql & " FROM FR7400"
strSql = strSql & " WHERE FR07CODCARRO=?"
strSql = strSql & " AND FR74DIA =? "
strSql = strSql & " AND FR87CODESTCARRO_MIV IN (1,6,2,3)"
Set qrySQL = objApp.rdoConnect.CreateQuery("", strSql)

'se cambia el estado del carro (estado=3 Cerrado � 1 si es el ultimo del dia)
updEstCarMIV = "UPDATE FR7400 SET FR87CODESTCARRO_MIV=?,FR87CODESTCARRO_CIT=? WHERE "
updEstCarMIV = updEstCarMIV & "FR07CODCARRO=?"
updEstCarMIV = updEstCarMIV & " AND FR74DIA=?"
updEstCarMIV = updEstCarMIV & " AND FR74HORASALIDA=" & objGen.ReplaceStr(cboHora, ",", ".", 1)
Set qryupdEstCarMIV = objApp.rdoConnect.CreateQuery("", updEstCarMIV)

'se cambia el estado del carro (estado=3 Cerrado � 1 si es el ultimo del dia)
updEstCarMIV_Ult = "UPDATE FR7400 SET FR87CODESTCARRO_MIV=?,FR87CODESTCARRO_CIT=? WHERE "
updEstCarMIV_Ult = updEstCarMIV_Ult & "FR07CODCARRO=?"
updEstCarMIV_Ult = updEstCarMIV_Ult & " AND FR74DIA=?"
Set qryupdEstCarMIV_Ult = objApp.rdoConnect.CreateQuery("", updEstCarMIV_Ult)

For mintisel = 0 To mintNTotalSelRows - 1
  mvarBkmrk = grdDBGrid1(2).SelBookmarks(mintisel)
  'Call RealizarApuntes(grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk))
  Call Insertar_FR6500(grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk), _
                        grdDBGrid1(2).Columns("Hora").CellValue(mvarBkmrk))
'  strCarros = strCarros & grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk) & ","

  Call Ins_FR6500_Prod_Por_Mezcla(grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk), _
                        grdDBGrid1(2).Columns("Hora").CellValue(mvarBkmrk))

  hora = grdDBGrid1(2).Columns("Hora").CellValue(mvarBkmrk)
  hora = objGen.ReplaceStr(hora, ",", ".", 1)

  '28-SEPTIEMBRE-2000, se quitan los movimientos a almac�n
  '-------------------------------------------------------
  'Call Movimientos_Almacen(grdDBGrid1(2).Columns("C�d.Servicio").CellValue(mvarBkmrk), _
                           grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk), hora)

  'se cambia el estado del carro (estado=3 Cerrado o 1)
  qrySQL(0) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
  qrySQL(1) = grdDBGrid1(2).Columns("D�a").CellValue(mvarBkmrk)
  Set rstSQL = qrySQL.OpenResultset()
  While rstSQL.StillExecuting
  Wend
  If rstSQL.rdoColumns(0).Value = cboHora Or rstSQL.rdoColumns(0).Value = 0 Then
    qryupdEstCarMIV_Ult(0) = 1
    qryupdEstCarMIV_Ult(1) = 1
    qryupdEstCarMIV_Ult(2) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
    qryupdEstCarMIV_Ult(3) = grdDBGrid1(2).Columns("D�a").CellValue(mvarBkmrk)
    qryupdEstCarMIV_Ult.Execute
  Else
    qryupdEstCarMIV(0) = 3
    qryupdEstCarMIV(1) = 3
    qryupdEstCarMIV(2) = grdDBGrid1(2).Columns("C�d.Carro").CellValue(mvarBkmrk)
    qryupdEstCarMIV(3) = grdDBGrid1(2).Columns("D�a").CellValue(mvarBkmrk)
    qryupdEstCarMIV.Execute
  End If
  
Next mintisel
  
'  If mintNTotalSelRows > 0 Then
'    strCarros = Left$(strCarros, Len(strCarros) - 1)
'    objWinInfo.objWinActiveForm.strWhere = objWinInfo.objWinActiveForm.strWhere & " FR07CODCARRO IN (" & strCarros & ")"
'  End If
  
Screen.MousePointer = vbDefault
Me.Enabled = True
cmdrecogercarro.Enabled = True

  MsgBox "Salida de Carros Terminada", vbInformation, "Aviso"

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Activate()
    If objWinInfo.objWinActiveForm.strName = "Carros" Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm
Dim strKey As String
Dim DiaSem(7) As String
Dim fecha As Date
Dim strFechaSer As String
Dim rstFechaSer As rdoResultset
Dim intDiaSem As Integer
Dim stra As String
Dim rsta As rdoResultset

strFechaSer = "SELECT SYSDATE FROM DUAL"
Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
While rstFechaSer.StillExecuting
Wend

fecha = rstFechaSer(0).Value
intDiaSem = WeekDay(fecha, vbMonday)
DiaSem(1) = "LUN"
DiaSem(2) = "MAR"
DiaSem(3) = "MIE"
DiaSem(4) = "JUE"
DiaSem(5) = "VIE"
DiaSem(6) = "SAB"
DiaSem(7) = "DOM"
  
cboHora.RemoveAll
stra = "SELECT DISTINCT FR74HORASALIDA FROM FR0701J " & _
" WHERE FR87CODESTCARRO IN (1,2,3,6) AND FR74DIA='" & DiaSem(intDiaSem) & "'"
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While rsta.StillExecuting
Wend

While (Not rsta.EOF)
    Call cboHora.AddItem(rsta.rdoColumns("FR74HORASALIDA").Value)
    rsta.MoveNext
Wend
rsta.Close
Set rsta = Nothing
cboHora.MoveFirst
cboHora = cboHora.Columns(0).Value
  
strFechaSer = "SELECT SYSDATE FROM DUAL"
Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
While rstFechaSer.StillExecuting
Wend

fecha = rstFechaSer(0).Value
intDiaSem = WeekDay(fecha, vbMonday)
DiaSem(1) = "LUN"
DiaSem(2) = "MAR"
DiaSem(3) = "MIE"
DiaSem(4) = "JUE"
DiaSem(5) = "VIE"
DiaSem(6) = "SAB"
DiaSem(7) = "DOM"
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Carros"
    
    .intCursorSize = 0
      
      
    .strTable = "FR0701J"
    .intAllowance = cwAllowReadOnly
    'estado=6 <Carga Analizada>
    .strWhere = "FR87CODESTCARRO IN (1,2,3,6) AND FR74DIA='" & DiaSem(intDiaSem) & "'"
    .strWhere = .strWhere & " AND FR74HORASALIDA= " & objGen.ReplaceStr(cboHora, ",", ".", 1)
    
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    Call .FormAddOrderField("FR74DIA", cwAscending)
    Call .FormAddOrderField("FR74HORASALIDA", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Carros")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "C�digo Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR07DESCARRO", "Descripci�n Carro", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Descripci�n Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR87CODESTCARRO", "C�d.Estado Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR87DESESTCARRO", "Desc.Estado Carro", cwString)
    Call .FormAddFilterWhere(strKey, "FR74DIA", "D�a Salida", cwString)
    Call .FormAddFilterWhere(strKey, "FR74HORASALIDA", "Hora Salida", cwNumeric)
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Peticiones del Carro"
    .intAllowance = cwAllowReadOnly
    
    '.strTable = "FR3306J"
    .strTable = "FR3307J"
    
    .intCursorSize = 0
    
    '.strWhere = "FR33FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & _
                " AND FR33INDMEZCLA=-1 AND FR33MOTDIFERENCIA='A�adido'"
    
    Call .objPrinter.Add("FR6131", "Dispensaci�n de Mezclas del Carro")
    Call .objPrinter.Add("FR6132", "Dispensaci�n de Citost�ticos del Carro")
    
'    Call .FormAddOrderField("FR07CODCARRO", cwAscending)
'    Call .FormAddOrderField("FR33HORATOMA", cwAscending)
'    Call .FormAddRelation("FR07CODCARRO", txtText1(3))
'    Call .FormAddRelation("FR33HORACARGA", txtText1(7))
    
    Call .FormAddRelation("CODCARRO", txtText1(3))
    Call .FormAddRelation("HORACAR", txtText1(7))
    Call .FormAddOrderField("CODCARRO", cwAscending)
    Call .FormAddOrderField("PACIENTE", cwAscending)
    Call .FormAddOrderField("DESDPTO", cwAscending)
    Call .FormAddOrderField("FECCARGA", cwAscending)
    Call .FormAddOrderField("HORACAR", cwAscending)
    Call .FormAddOrderField("HORAHAS", cwAscending)
    Call .FormAddOrderField("PETIC", cwAscending)
    Call .FormAddOrderField("LINEA", cwAscending)
    Call .FormAddOrderField("HORATOM", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones del Carro")
    'Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "Carro", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "AD15CODCAMA", "Cama", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR33CANTIDAD", "Cantidad", cwNumeric)

    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    'Call .GridAddColumn(objMultiInfo, "Motivo", "FR33MOTDIFERENCIA", cwString, 10)
    'Call .GridAddColumn(objMultiInfo, "Car.", "CODCARRO", cwNumeric, 3)
    'Call .GridAddColumn(objMultiInfo, "Carro", "DESCARRO", cwString, 30)
    'Call .GridAddColumn(objMultiInfo, "Dpto", "CODDPTO", cwNumeric, 3)
    'Call .GridAddColumn(objMultiInfo, "Departamento", "DESDPTO", cwString, 30)
    'Call .GridAddColumn(objMultiInfo, "FecCarga", "FECCARGA", cwDate)
    'Call .GridAddColumn(objMultiInfo, "HoraCarga", "HORACAR", cwDecimal, 6)
    'Call .GridAddColumn(objMultiInfo, "HoraSal", "HORASAL", cwDecimal)
    'Call .GridAddColumn(objMultiInfo, "HoraHas", "HORAHAS", cwDecimal)
    Call .GridAddColumn(objMultiInfo, "Cama", "cama", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Paciente", "PACIENTE", cwString, 77)
    Call .GridAddColumn(objMultiInfo, "Historia", "HISTORIA", cwNumeric, 7)
    'Call .GridAddColumn(objMultiInfo, "Proc-Asist", "P_A", cwString, 81)
    Call .GridAddColumn(objMultiInfo, "H.Adm.", "HORATOM", cwDecimal, 6)
    'Call .GridAddColumn(objMultiInfo, "Carro", "PETIC", cwNumeric, 9)
    'Call .GridAddColumn(objMultiInfo, "Carro", "LINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Medicamento_1", "DESPRODUCTO_1", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Dosis1", "DOSIS_1", cwDecimal, 12)
    Call .GridAddColumn(objMultiInfo, "UM1", "UM_1", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Diluyente", "DESPRODUCTO_DIL", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Cant.Dil.", "CANTDIL", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "UM_dil", "UM_DIL", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Medicamento_2", "DESPRODUCTO_2", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Dosis2", "DOSIS_2", cwDecimal, 12)
    Call .GridAddColumn(objMultiInfo, "UM2", "UM_2", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Instr.Admin.", "INSTRADMIN", cwString, 2000)
 
'    Call .GridAddColumn(objMultiInfo, "Carro", "FR07CODCARRO", cwNumeric, 3)
'    Call .GridAddColumn(objMultiInfo, "Cama", "GCFN06(AD15CODCAMA)", cwString, 7)
'    Call .GridAddColumn(objMultiInfo, "Persona", "CI21CODPERSONA", cwNumeric, 7)
'    Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7) 'historia
'    Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25) 'nombre
'    Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25) 'apellido 1�
'    Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25) 'apellido 2�
'    'Call .GridAddColumn(objMultiInfo, "Estado", "FR33MOTDIFERENCIA", cwString, 10)
'    Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
'    Call .GridAddColumn(objMultiInfo, "Int", "", cwString, 6)
'    Call .GridAddColumn(objMultiInfo, "Cant", "FR33CANTIDAD", cwDecimal, 2)
'    Call .GridAddColumn(objMultiInfo, "Producto", "FR33DESPROD", cwString, 50)
'    Call .GridAddColumn(objMultiInfo, "F.F", "FR33FORMFAR", cwString, 3)
'    Call .GridAddColumn(objMultiInfo, "Dosis", "FR33DOSIS", cwNumeric, 9)
'    Call .GridAddColumn(objMultiInfo, "U.M", "FR33UNIMEDIDA", cwString, 5)
'    Call .GridAddColumn(objMultiInfo, "Toma", "FR33HORATOMA", cwNumeric, 2)
    
    Call .FormCreateInfo(objMasterInfo)
    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    
'    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
'    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
'    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnInFind = True
'    .CtrlGetInfo(grdDBGrid1(0).Columns(13)).blnInFind = True
'    .CtrlGetInfo(grdDBGrid1(0).Columns(14)).blnInFind = True
'    .CtrlGetInfo(grdDBGrid1(0).Columns(15)).blnInFind = True
'    .CtrlGetInfo(grdDBGrid1(0).Columns(16)).blnInFind = True
'    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnInFind = True
  
    '.CtrlGetInfo(txtText1(3)).blnInGrid = False 'c�d.carro
    .CtrlGetInfo(txtText1(2)).blnInGrid = False 'c�d.estado carro
    .CtrlGetInfo(txtText1(5)).blnInGrid = False

'    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "CI22NUMHISTORIA")
    
'    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(7), "CI22NOMBRE")
    
'    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(8), "CI22PRIAPEL")
    
'    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(9), "CI22SEGAPEL")
    
'    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), grdDBGrid1(0).Columns(11), "FR73CODINTFAR")
     
'    grdDBGrid1(0).Columns(3).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
 grdDBGrid1(2).Columns("Servicio").Width = 2850
 grdDBGrid1(2).Columns("C�d.Carro").Width = 900
 grdDBGrid1(2).Columns("Carro").Width = 2850
 grdDBGrid1(2).Columns("D�a").Width = 600
 grdDBGrid1(2).Columns("Hora").Width = 600
  
 
  grdDBGrid1(0).Columns("Cama").Width = 800
  grdDBGrid1(0).Columns("Paciente").Width = 1300
  grdDBGrid1(0).Columns("Historia").Width = 800
  grdDBGrid1(0).Columns("H.Adm.").Width = 800
  grdDBGrid1(0).Columns("Medicamento_1").Width = 1300
  grdDBGrid1(0).Columns("Dosis1").Width = 800
  grdDBGrid1(0).Columns("UM1").Width = 450
  grdDBGrid1(0).Columns("Diluyente").Width = 1300
  grdDBGrid1(0).Columns("Cant.Dil.").Width = 800
  grdDBGrid1(0).Columns("UM_dil").Width = 450
  grdDBGrid1(0).Columns("Medicamento_2").Width = 1300
  grdDBGrid1(0).Columns("Dosis2").Width = 800
  grdDBGrid1(0).Columns("UM2").Width = 450
  grdDBGrid1(0).Columns("Instr.Admin.").Width = 2500
 
 
 Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
Dim strWhere  As String
Dim strFilter As String
Dim vntResp As Variant
Dim strUpdate As String
Dim hora As String
Dim strSql As String
Dim qrySQL As rdoQuery
Dim rstSQL As rdoResultset
Dim qryUpd As rdoQuery
  
If strFormName = "Peticiones del Carro" Then
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    Select Case intReport
    Case 1
      Call Imprimir_Mez("FR6131.RPT", 0)
    Case 2
      Call Imprimir_Cit("FR6132.RPT", 0)
    End Select
  End If
  Set objPrinter = Nothing
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  If intIndex = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
  End If
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    If intIndex = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
        'objWinInfo.objWinActiveForm.strWhere = " FR07CODCARRO IN (" & grdDBGrid1(2).Columns("C�d.Carro").Value & ")"
    End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdDBGrid1_Click(Index As Integer)
If Index = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  If intIndex = 1 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub Imprimir_Mez(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strCarros As String
  Dim nTotal As Long
  Dim nTotalSelRows As Integer
  Dim i As Integer
  Dim bkmrk As Variant
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.getdatabasepasswordold
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  
  strCarros = ""
  nTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdDBGrid1(2).SelBookmarks(i)
      strCarros = strCarros & grdDBGrid1(2).Columns("C�d.Carro").CellValue(bkmrk) & ","
    Next i
    strCarros = Left$(strCarros, Len(strCarros) - 1)
  Else
    strCarros = grdDBGrid1(2).Columns("C�d.Carro").Value
  End If
  strWhere = ""
  
  strWhere = strWhere & " {FR3304J.HORACAR} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
  strWhere = strWhere & " AND {FR3304J.HORASAL} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
  strWhere = strWhere & " AND " & CrearWhere("{FR3304J.CODCARRO}", "(" & strCarros & ")")
  strWhere = strWhere & " AND {FR3304J.FR33INDCITOSTATICO}=0"
             
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp1
  CrystalReport1.Action = 1
  
Err_imp1:
End Sub
Private Sub Imprimir_Cit(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strCarros As String
  Dim nTotal As Long
  Dim nTotalSelRows As Integer
  Dim i As Integer
  Dim bkmrk As Variant
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.getdatabasepasswordold
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  
  strCarros = ""
  nTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
  If nTotalSelRows > 0 Then
    For i = 0 To nTotalSelRows - 1
      bkmrk = grdDBGrid1(2).SelBookmarks(i)
      strCarros = strCarros & grdDBGrid1(2).Columns("C�d.Carro").CellValue(bkmrk) & ","
    Next i
    strCarros = Left$(strCarros, Len(strCarros) - 1)
  Else
    strCarros = grdDBGrid1(2).Columns("C�d.Carro").Value
  End If
  strWhere = ""
  
  strWhere = strWhere & " {FR3304J.HORACAR} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
  strWhere = strWhere & " AND {FR3304J.HORASAL} = " & objGen.ReplaceStr(cboHora, ",", ".", 1)
  strWhere = strWhere & " AND " & CrearWhere("{FR3304J.CODCARRO}", "(" & strCarros & ")")
  strWhere = strWhere & " AND {FR3304J.FR33INDCITOSTATICO}=-1"
             
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp1
  CrystalReport1.Action = 1
  
Err_imp1:
End Sub



Private Function CrearWhere(strPal As String, strLis) As String
  'strLis tiene la forma (1123,4444459), strPal es un nombre de campo
  'CrearWhere se utiliza para pasar a Crystal la sentencias SQL de tipo
  'FR66CODTICION IN (13446,443775)
  'al formato (FR66CODTICION = 13446 OR FR66CODTICION = 443775)
  Dim strCar As String
  Dim strResLis As String
  Dim strSalida As String
  
  strResLis = Right(strLis, Len(strLis) - 1)
  strSalida = "("
  While (strCar <> ")")
    If (strCar <> ")") Then
      strSalida = strSalida & strPal & " = "
    End If
    While (strCar <> ",") And (strCar <> ")")
      strSalida = strSalida & Left(strResLis, 1)
      strCar = Left(strResLis, 1)
      strResLis = Right(strResLis, Len(strResLis) - 1)
    Wend
    If (strCar = ",") Then
      strSalida = Left(strSalida, Len(strSalida) - 1) & " OR "
      strCar = Left(strResLis, 1)
    End If
  Wend
  CrearWhere = strSalida
End Function


Private Sub Movimientos_Almacen(dpto, carro, hora)
Dim qrydes As rdoQuery
Dim rstdes As rdoResultset
Dim strdes As String
Dim qryori As rdoQuery
Dim rstori As rdoResultset
Dim strori As String
Dim strinsertentrada As String
Dim strinsertsalida As String


'se obtiene el almac�n que pide el producto
strdes = "SELECT FR04CODALMACEN FROM FR0400 WHERE AD02CODDPTO=?"
strdes = strdes & " AND FR0400.FR04INDPRINCIPAL=?"
Set qrydes = objApp.rdoConnect.CreateQuery("", strdes)
qrydes(0) = dpto
qrydes(1) = -1
Set rstdes = qrydes.OpenResultset()
While rstdes.StillExecuting
Wend
'se obtiene el almac�n del servicio 1(Farmacia) que es el que dispensa el producto
'strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
         "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
         "AND FRH2CODPARAMGEN=?"
strori = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=?"
Set qryori = objApp.rdoConnect.CreateQuery("", strori)
'qryori(0) = 3
qryori(0) = 28
Set rstori = qryori.OpenResultset()
While rstori.StillExecuting
Wend

  If (Not rstdes.EOF And Not rstori.EOF) Then
    If Not IsNull(rstori.rdoColumns(0).Value) And Not IsNull(rstdes.rdoColumns(0).Value) Then
          
    'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
    'Farmacia y el destino es el almac�n al que se dispensa
      strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                      "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) "
      strinsertentrada = strinsertentrada & "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval,"
      strinsertentrada = strinsertentrada & rstori.rdoColumns(0).Value & ","
      strinsertentrada = strinsertentrada & rstdes.rdoColumns(0).Value & ","
      'strinsertentrada = strinsertentrada & "8,SYSDATE,NULL,"
      strinsertentrada = strinsertentrada & "2,SYSDATE,NULL,"
      strinsertentrada = strinsertentrada & "FR3300.FR73CODPRODUCTO,"
      strinsertentrada = strinsertentrada & "FR3300.FR33CANTIDAD,0,"
      strinsertentrada = strinsertentrada & "FR3300.FR33UNIMEDIDA"
      strinsertentrada = strinsertentrada & " FROM FR3300"
      strinsertentrada = strinsertentrada & " WHERE  "
      strinsertentrada = strinsertentrada & " FR3300.FR33FECCARGA=TRUNC(SYSDATE)"
      strinsertentrada = strinsertentrada & " AND FR3300.FR07CODCARRO=" & carro
      strinsertentrada = strinsertentrada & " AND FR3300.FR33HORACARGA=" & hora
      strinsertentrada = strinsertentrada & " AND FR3300.FR33INDMEZCLA=-1 AND FR3300.FR33MOTDIFERENCIA='A�adido'"
      
      objApp.rdoConnect.Execute strinsertentrada, 64
      objApp.rdoConnect.Execute "Commit", 64
            
      'se actualiza el inventario de salida(FR8000) en el que el origen de salida es farmacia
      'y el destino de la salida el almac�n al que se dispensa
      strinsertsalida = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV," & _
                      "FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) "
      strinsertsalida = strinsertsalida & "SELECT FR80NUMMOV_SEQUENCE.nextval,"
      strinsertsalida = strinsertsalida & rstori.rdoColumns(0).Value & ","
      strinsertsalida = strinsertsalida & rstdes.rdoColumns(0).Value & ","
      'strinsertsalida = strinsertsalida & "7,SYSDATE,NULL,"
      strinsertsalida = strinsertsalida & "2,SYSDATE,NULL,"
      strinsertsalida = strinsertsalida & "FR3300.FR73CODPRODUCTO,"
      strinsertsalida = strinsertsalida & "FR3300.FR33CANTIDAD,0,"
      strinsertsalida = strinsertsalida & "FR3300.FR33UNIMEDIDA"
      strinsertsalida = strinsertsalida & " FROM FR3300"
      strinsertsalida = strinsertsalida & " WHERE  "
      strinsertsalida = strinsertsalida & " FR3300.FR33FECCARGA=TRUNC(SYSDATE)"
      strinsertsalida = strinsertsalida & " AND FR3300.FR07CODCARRO=" & carro
      strinsertsalida = strinsertsalida & " AND FR3300.FR33HORACARGA=" & hora
      strinsertsalida = strinsertsalida & " AND FR3300.FR33INDMEZCLA=-1 AND FR3300.FR33MOTDIFERENCIA='A�adido'"

      objApp.rdoConnect.Execute strinsertsalida, 64
      objApp.rdoConnect.Execute "Commit", 64
    End If
End If

rstdes.Close
Set rstdes = Nothing
rstori.Close
Set rstori = Nothing

End Sub

Private Sub Insertar_FR6500(carro As Variant, hora As Variant)
Dim strinsert As String
Dim stra As String
Dim qrya As rdoQuery

strinsert = "INSERT INTO FR6500 (FR65CODIGO,CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,"
strinsert = strinsert & " FR65CANTIDAD,FR73CODPRODUCTO_DIL,"
strinsert = strinsert & " FR65CANTIDADDIL,FR73CODPRODUCTO_2,FR65DOSIS_2,"
strinsert = strinsert & " FR65DESPRODUCTO,"
strinsert = strinsert & " FR66CODPETICION,FR65INDPRN,AD01CODASISTENCI,AD07CODPROCESO,"
strinsert = strinsert & " FR65INDINTERCIENT,AD02CODDPTO_CRG) "

strinsert = strinsert & " SELECT FR65CODIGO_SEQUENCE.NEXTVAL,"
strinsert = strinsert & " CI21CODPERSONA,"
strinsert = strinsert & " CODPRODUCTO_1,SYSDATE,TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI'))" & ","
strinsert = strinsert & " CANTIDAD_1,CODPRODUCTO_DIL,"
strinsert = strinsert & " CANTDIL,CODPRODUCTO_2,"
strinsert = strinsert & " DOSIS_2,DESPRODUCTO_1,"
strinsert = strinsert & " FR66CODPETICION,0,"
strinsert = strinsert & " AD01CODASISTENCI,"
strinsert = strinsert & " AD07CODPROCESO,FR66INDINTERCIENT,AD02CODDPTO_CRG"
strinsert = strinsert & " FROM "

stra = stra & " ( SELECT DISTINCT "
stra = stra & "  FR2800.FR66CODPETICION"
stra = stra & "  ,FR2800.FR28NUMLINEA"
stra = stra & "  ,FR3300.FR33HORATOMA"
stra = stra & "  ,FR6600.CI21CODPERSONA"
stra = stra & "  ,FR6600.AD07CODPROCESO"
stra = stra & "  ,FR6600.AD01CODASISTENCI"
stra = stra & "  ,FR6600.FR66INDINTERCIENT"
stra = stra & "  ,FR6600.AD02CODDPTO_CRG"
stra = stra & "  ,FR7300_1.FR73CODPRODUCTO CODPRODUCTO_1"
stra = stra & "  ,DECODE(FR7300_1.FR73CODPRODUCTO,999999999,FR2800.FR28DESPRODUCTO,FR7300_1.FR73DESPRODUCTO)"
stra = stra & "  DESPRODUCTO_1"
stra = stra & "  ,FR2800.FR28CANTIDAD CANTIDAD_1"
stra = stra & "  ,FR2800.FR73CODPRODUCTO_DIL CODPRODUCTO_DIL"
stra = stra & "  ,FR2800.FR28CANTIDADDIL CANTDIL"
stra = stra & "  ,FR7300_2.FR73CODPRODUCTO CODPRODUCTO_2"
stra = stra & "  ,FR2800.FR28DOSIS_2 DOSIS_2"
stra = stra & " FROM FR3300,FR2800,FR6600,FR7300"
stra = stra & " FR7300_1,FR7300"
stra = stra & " FR7300_2,FR7300 FR7300_DIL"
stra = stra & " WHERE FR3300.FR33INDMEZCLA=?"
stra = stra & " AND  FR3300.FR66CODPETICION=FR2800.FR66CODPETICION"
stra = stra & " AND  FR3300.FR33MOTDIFERENCIA=?"
stra = stra & " AND  FR3300.FR28NUMLINEA=FR2800.FR28NUMLINEA"
stra = stra & " AND  FR3300.FR66CODPETICION=FR6600.FR66CODPETICION"
stra = stra & " AND  FR6600.FR66INDOM=?"
stra = stra & " AND  FR2800.FR73CODPRODUCTO=FR7300_1.FR73CODPRODUCTO(+)"
stra = stra & " AND  FR2800.FR73CODPRODUCTO_2=FR7300_2.FR73CODPRODUCTO(+)"
stra = stra & " AND  FR2800.FR73CODPRODUCTO_DIL=FR7300_DIL.FR73CODPRODUCTO(+)"
stra = stra & " AND  FR3300.FR07CODCARRO=?"
stra = stra & " AND  FR3300.FR33FECCARGA=TRUNC(sysdate)"
stra = stra & " AND  FR3300.FR33HORACARGA=" & objGen.ReplaceStr(hora, ",", ".", 1) & ")"

strinsert = strinsert & stra
Set qrya = objApp.rdoConnect.CreateQuery("", strinsert)
qrya(0) = -1
qrya(1) = "A�adido"
qrya(2) = -1
qrya(3) = carro
qrya.Execute

End Sub

Private Sub Movimientos_Almacen_2(hora, dia)
Dim qryori As rdoQuery
Dim rstori As rdoResultset
Dim strori As String
Dim strinsertentrada As String
Dim strinsertsalida As String



'se obtiene el almac�n del servicio 1(Farmacia) que es el que dispensa el producto
'strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
         "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
         "AND FRH2CODPARAMGEN=?"
strori = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=?"
Set qryori = objApp.rdoConnect.CreateQuery("", strori)
'qryori(0) = 3
qryori(0) = 28
Set rstori = qryori.OpenResultset()
While rstori.StillExecuting
Wend

  If Not rstori.EOF Then
    If Not IsNull(rstori.rdoColumns(0).Value) Then
          
    'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
    'Farmacia y el destino es el almac�n al que se dispensa
      strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                      "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) "
      strinsertentrada = strinsertentrada & "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval,"
      strinsertentrada = strinsertentrada & rstori.rdoColumns(0).Value & ","
      strinsertentrada = strinsertentrada & "FR0400.FR04CODALMACEN,"
      'strinsertentrada = strinsertentrada & "8,SYSDATE,NULL,"
      strinsertentrada = strinsertentrada & "2,SYSDATE,NULL,"
      strinsertentrada = strinsertentrada & "FR3300.FR73CODPRODUCTO,"
      strinsertentrada = strinsertentrada & "FR3300.FR33CANTIDAD,0,"
      strinsertentrada = strinsertentrada & "FR3300.FR33UNIMEDIDA"
      strinsertentrada = strinsertentrada & " FROM FR3300,FR0700,FR0400"
      strinsertentrada = strinsertentrada & " WHERE  "
      strinsertentrada = strinsertentrada & " FR3300.FR33FECCARGA=TRUNC(SYSDATE)"
      strinsertentrada = strinsertentrada & " AND FR3300.FR33HORACARGA=" & hora
      strinsertentrada = strinsertentrada & " AND FR3300.FR33INDMEZCLA=-1 AND FR3300.FR33MOTDIFERENCIA='A�adido'"
      strinsertentrada = strinsertentrada & " AND FR3300.FR07CODCARRO=FR0700.FR07CODCARRO"
      strinsertentrada = strinsertentrada & " AND FR0400.AD02CODDPTO=FR0700.AD02CODDPTO"
      strinsertentrada = strinsertentrada & " AND FR0400.FR04INDPRINCIPAL=-1"

      objApp.rdoConnect.Execute strinsertentrada, 64
      objApp.rdoConnect.Execute "Commit", 64
            
      'se actualiza el inventario de salida(FR8000) en el que el origen de salida es farmacia
      'y el destino de la salida el almac�n al que se dispensa
      strinsertsalida = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV," & _
                      "FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) "
      strinsertsalida = strinsertsalida & "SELECT FR80NUMMOV_SEQUENCE.nextval,"
      strinsertsalida = strinsertsalida & rstori.rdoColumns(0).Value & ","
      strinsertsalida = strinsertsalida & "FR0400.FR04CODALMACEN,"
      'strinsertsalida = strinsertsalida & "7,SYSDATE,NULL,"
      strinsertsalida = strinsertsalida & "2,SYSDATE,NULL,"
      strinsertsalida = strinsertsalida & "FR3300.FR73CODPRODUCTO,"
      strinsertsalida = strinsertsalida & "FR3300.FR33CANTIDAD,0,"
      strinsertsalida = strinsertsalida & "FR3300.FR33UNIMEDIDA"
      strinsertsalida = strinsertsalida & " FROM FR3300,FR0700,FR0400"
      strinsertsalida = strinsertsalida & " WHERE  "
      strinsertsalida = strinsertsalida & " FR3300.FR33FECCARGA=TRUNC(SYSDATE)"
      strinsertsalida = strinsertsalida & " AND FR3300.FR33HORACARGA=" & hora
      strinsertsalida = strinsertsalida & " AND FR3300.FR33INDMEZCLA=-1 AND FR3300.FR33MOTDIFERENCIA='A�adido'"
      strinsertsalida = strinsertsalida & " AND FR3300.FR07CODCARRO=FR0700.FR07CODCARRO"
      strinsertsalida = strinsertsalida & " AND FR0400.AD02CODDPTO=FR0700.AD02CODDPTO"
      strinsertsalida = strinsertsalida & " AND FR0400.FR04INDPRINCIPAL=-1"

      objApp.rdoConnect.Execute strinsertsalida, 64
      objApp.rdoConnect.Execute "Commit", 64
    End If
  End If

rstori.Close
Set rstori = Nothing

End Sub




Private Sub Ins_FR6500_Prod_Por_Mezcla(carro As Variant, hora As Variant)
Dim strinsert As String
Dim stra As String
Dim qrya As rdoQuery

strinsert = "INSERT INTO FR6500 (FR65CODIGO,"
strinsert = strinsert & " CI21CODPERSONA,"
strinsert = strinsert & " FR73CODPRODUCTO,"
strinsert = strinsert & " FR65FECHA,FR65HORA,"
strinsert = strinsert & " FR65CANTIDAD,"
strinsert = strinsert & " FR65DESPRODUCTO,"
strinsert = strinsert & " FR66CODPETICION,"
strinsert = strinsert & " FR65INDPRN,AD01CODASISTENCI,"
strinsert = strinsert & " AD07CODPROCESO,FR65INDINTERCIENT,AD02CODDPTO_CRG) "

strinsert = strinsert & " SELECT FR65CODIGO_SEQUENCE.NEXTVAL,"
strinsert = strinsert & " CI21CODPERSONA,"
strinsert = strinsert & " FR73CODPRODUCTO,"
strinsert = strinsert & " SYSDATE,TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI'))" & ","
strinsert = strinsert & " 1,"
strinsert = strinsert & " FR73DESPRODUCTO,"
strinsert = strinsert & " FR66CODPETICION,"
strinsert = strinsert & " 0,"
strinsert = strinsert & " AD01CODASISTENCI,"
strinsert = strinsert & " AD07CODPROCESO,FR66INDINTERCIENT,AD02CODDPTO_CRG"
strinsert = strinsert & " FROM "

stra = stra & " ( SELECT DISTINCT "
stra = stra & "  FR3300.FR66CODPETICION"
stra = stra & "  ,FR3300.FR28NUMLINEA"
stra = stra & "  ,FR3300.FR33HORATOMA"
stra = stra & "  ,FR6600.CI21CODPERSONA"
stra = stra & "  ,FR6600.AD07CODPROCESO"
stra = stra & "  ,FR6600.AD01CODASISTENCI"
stra = stra & "  ,FR6600.FR66INDINTERCIENT"
stra = stra & "  ,FR6600.AD02CODDPTO_CRG"
stra = stra & "  ,FR7300.FR73CODPRODUCTO"
stra = stra & "  ,FR7300.FR73DESPRODUCTO"
stra = stra & " FROM FR3300,FR6600,FR7300"
stra = stra & " WHERE FR3300.FR33INDMEZCLA=?"
stra = stra & " AND  FR3300.FR33MOTDIFERENCIA=?"
stra = stra & " AND  FR3300.FR66CODPETICION=FR6600.FR66CODPETICION"
stra = stra & " AND  FR6600.FR66INDOM=?"
stra = stra & " AND  FR3300.FR07CODCARRO=?"
stra = stra & " AND  FR3300.FR33FECCARGA=TRUNC(sysdate)"
stra = stra & " AND  FR3300.FR33HORACARGA=" & objGen.ReplaceStr(hora, ",", ".", 1)
stra = stra & " AND  FR7300.FR73CODINTFAR='999028'" & ")"

strinsert = strinsert & stra
Set qrya = objApp.rdoConnect.CreateQuery("", strinsert)
qrya(0) = -1
qrya(1) = "A�adido"
qrya(2) = -1
qrya(3) = carro
qrya.Execute

End Sub

Private Sub Ins_FR6500_Prod_Por_Mezcla_2(hora As Variant)
Dim strinsert As String
Dim stra As String
Dim qrya As rdoQuery

strinsert = "INSERT INTO FR6500 (FR65CODIGO,"
strinsert = strinsert & " CI21CODPERSONA,"
strinsert = strinsert & " FR73CODPRODUCTO,"
strinsert = strinsert & " FR65FECHA,FR65HORA,"
strinsert = strinsert & " FR65CANTIDAD,"
strinsert = strinsert & " FR65DESPRODUCTO,"
strinsert = strinsert & " FR66CODPETICION,"
strinsert = strinsert & " FR65INDPRN,AD01CODASISTENCI,"
strinsert = strinsert & " AD07CODPROCESO,FR65INDINTERCIENT,AD02CODDPTO_CRG) "

strinsert = strinsert & " SELECT FR65CODIGO_SEQUENCE.NEXTVAL,"
strinsert = strinsert & " CI21CODPERSONA,"
strinsert = strinsert & " FR73CODPRODUCTO,"
strinsert = strinsert & " SYSDATE,TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI'))" & ","
strinsert = strinsert & " 1,"
strinsert = strinsert & " FR73DESPRODUCTO,"
strinsert = strinsert & " FR66CODPETICION,"
strinsert = strinsert & " 0,"
strinsert = strinsert & " AD01CODASISTENCI,"
strinsert = strinsert & " AD07CODPROCESO,FR66INDINTERCIENT,AD02CODDPTO_CRG"
strinsert = strinsert & " FROM "

stra = stra & " ( SELECT DISTINCT "
stra = stra & "  FR3300.FR66CODPETICION"
stra = stra & "  ,FR3300.FR28NUMLINEA"
stra = stra & "  ,FR3300.FR33HORATOMA"
stra = stra & "  ,FR6600.CI21CODPERSONA"
stra = stra & "  ,FR6600.AD07CODPROCESO"
stra = stra & "  ,FR6600.AD01CODASISTENCI"
stra = stra & "  ,FR6600.FR66INDINTERCIENT"
stra = stra & "  ,FR6600.AD02CODDPTO_CRG"
stra = stra & "  ,FR7300.FR73CODPRODUCTO"
stra = stra & "  ,FR7300.FR73DESPRODUCTO"
stra = stra & " FROM FR3300,FR6600,FR7300"
stra = stra & " WHERE FR3300.FR33INDMEZCLA=?"
stra = stra & " AND  FR3300.FR33MOTDIFERENCIA=?"
stra = stra & " AND  FR3300.FR66CODPETICION=FR6600.FR66CODPETICION"
stra = stra & " AND  FR6600.FR66INDOM=?"
stra = stra & " AND  FR3300.FR33FECCARGA=TRUNC(sysdate)"
stra = stra & " AND  FR3300.FR33HORACARGA=" & objGen.ReplaceStr(hora, ",", ".", 1)
stra = stra & " AND  FR7300.FR73CODINTFAR='999028'" & ")"

strinsert = strinsert & stra
Set qrya = objApp.rdoConnect.CreateQuery("", strinsert)
qrya(0) = -1
qrya(1) = "A�adido"
qrya(2) = -1
qrya.Execute

End Sub


