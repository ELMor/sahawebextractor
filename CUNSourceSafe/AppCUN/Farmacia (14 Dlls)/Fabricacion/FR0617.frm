VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmMovimientosAlmacen 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Consumos de Mezclas"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdBuscarGruposProductos 
      Caption         =   "Grupos de Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9720
      TabIndex        =   12
      Top             =   4320
      Width           =   2055
   End
   Begin VB.Frame FrameMensaje 
      Height          =   1335
      Left            =   2160
      TabIndex        =   10
      Top             =   3240
      Visible         =   0   'False
      Width           =   6015
      Begin VB.Label LabelMensaje 
         Caption         =   "Actualizando Consumos. Espere un momento, por favor..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00004080&
         Height          =   855
         Left            =   480
         TabIndex        =   11
         Top             =   360
         Visible         =   0   'False
         Width           =   5175
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filtro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   1815
      Left            =   9720
      TabIndex        =   5
      Top             =   1200
      Width           =   1935
      Begin SSDataWidgets_B.SSDBCombo cboHora 
         Height          =   315
         Left            =   120
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   600
         Width           =   975
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns(0).Width=   2831
         Columns(0).Caption=   "Hora Salida"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1720
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcFecha 
         Height          =   330
         Left            =   120
         TabIndex        =   7
         Top             =   1320
         Width           =   1740
         _Version        =   65537
         _ExtentX        =   3069
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowNullDate   =   -1  'True
      End
      Begin VB.Label Label2 
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008080&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Hora"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008080&
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   615
      End
   End
   Begin VB.CommandButton cmdBuscarProductos 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9720
      TabIndex        =   4
      Top             =   3720
      Width           =   2055
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Consumos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7215
      Index           =   2
      Left            =   120
      TabIndex        =   0
      Tag             =   "Actuaciones Asociadas"
      Top             =   600
      Width           =   9375
      Begin SSDataWidgets_B.SSDBGrid GridConsumos 
         Height          =   6615
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   9090
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         AllowDelete     =   -1  'True
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   16034
         _ExtentY        =   11668
         _StockProps     =   79
         Caption         =   "CONSUMOS"
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMovimientosAlmacen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmMantAlerta(FR0167.FRM)                                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 2000                                            *
'* DESCRIPCION: Consumos                                                *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cbohora_CloseUp()
    Call Rellenar_Grid
End Sub

Private Sub cmdBuscarGruposProductos_Click()
Dim strProducto As String
Dim rstProducto As rdoResultset
Dim v As Integer

cmdBuscarGruposProductos.Enabled = False
If IsDate(dtcFecha.Text) And IsNumeric(cboHora.Text) Then
    Call objsecurity.LaunchProcess("FR0619")
    If gintprodtotal > 0 Then
            For v = 0 To gintprodtotal - 1
              If gintprodbuscado(v, 0) <> "" Then
                Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
                strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                          gintprodbuscado(v, 0)
                Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
                GridConsumos.Columns("C�digo Producto").Value = gintprodbuscado(v, 0)
                GridConsumos.Columns("Producto").Value = rstProducto.rdoColumns("FR73DESPRODUCTO").Value
                rstProducto.Close
                Set rstProducto = Nothing
              End If
            Next v
    End If
    gintprodtotal = 0
Else
    Call MsgBox("Debe especificar Fecha y Hora.", vbInformation, "Aviso")
End If
cmdBuscarGruposProductos.Enabled = True
End Sub

Private Sub cmdBuscarProductos_Click()
Dim strProducto As String
Dim rstProducto As rdoResultset
Dim v As Integer

cmdBuscarProductos.Enabled = False
If IsDate(dtcFecha.Text) And IsNumeric(cboHora.Text) Then
    Call objsecurity.LaunchProcess("FR0618")
    If gintprodtotal > 0 Then
            For v = 0 To gintprodtotal - 1
              If gintprodbuscado(v, 0) <> "" Then
                Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
                strProducto = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                          gintprodbuscado(v, 0)
                Set rstProducto = objApp.rdoConnect.OpenResultset(strProducto)
                GridConsumos.Columns("C�digo Producto").Value = gintprodbuscado(v, 0)
                GridConsumos.Columns("Producto").Value = rstProducto.rdoColumns("FR73DESPRODUCTO").Value
                rstProducto.Close
                Set rstProducto = Nothing
              End If
            Next v
    End If
    gintprodtotal = 0
Else
    Call MsgBox("Debe especificar Fecha y Hora.", vbInformation, "Aviso")
End If
cmdBuscarProductos.Enabled = True
End Sub

Private Sub dtcFecha_Change()
  Rellenar_Grid
End Sub

Private Sub dtcFecha_CloseUp()
  Rellenar_Grid
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String
    Dim i As Integer
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
i = 0
Call GridConsumos.Columns.Add(i)
GridConsumos.Columns(i).Visible = False
GridConsumos.Columns(i).Caption = "C�digo"
GridConsumos.Columns(i).Locked = True
i = i + 1
Call GridConsumos.Columns.Add(i)
GridConsumos.Columns(i).Visible = False
GridConsumos.Columns(i).Caption = "Almac�n Origen"
GridConsumos.Columns(i).Locked = True
i = i + 1
Call GridConsumos.Columns.Add(i)
GridConsumos.Columns(i).Visible = False
GridConsumos.Columns(i).Caption = "Almac�n Destino"
GridConsumos.Columns(i).Locked = True
i = i + 1
Call GridConsumos.Columns.Add(i)
GridConsumos.Columns(i).Visible = False
GridConsumos.Columns(i).Caption = "Tipo Movimiento"
GridConsumos.Columns(i).Locked = True
i = i + 1
Call GridConsumos.Columns.Add(i)
GridConsumos.Columns(i).Visible = False
GridConsumos.Columns(i).Caption = "Fecha"
GridConsumos.Columns(i).Locked = True
i = i + 1
Call GridConsumos.Columns.Add(i)
GridConsumos.Columns(i).Visible = False
GridConsumos.Columns(i).Caption = "C�digo Producto"
GridConsumos.Columns(i).Locked = False
i = i + 1
Call GridConsumos.Columns.Add(i)
GridConsumos.Columns(i).Visible = True
GridConsumos.Columns(i).Caption = "Producto"
GridConsumos.Columns(i).Locked = True
GridConsumos.Columns(i).Width = 6500
i = i + 1
Call GridConsumos.Columns.Add(i)
GridConsumos.Columns(i).Visible = True
GridConsumos.Columns(i).Caption = "Cantidad"
GridConsumos.Columns(i).Locked = False
GridConsumos.Columns(i).Width = 1150
i = i + 1
Call GridConsumos.Columns.Add(i)
GridConsumos.Columns(i).Visible = False
GridConsumos.Columns(i).Caption = "Precio"
GridConsumos.Columns(i).Locked = True
i = i + 1
Call GridConsumos.Columns.Add(i)
GridConsumos.Columns(i).Visible = False
GridConsumos.Columns(i).Caption = "UM"
GridConsumos.Columns(i).Locked = True
i = i + 1
Call GridConsumos.Columns.Add(i)
GridConsumos.Columns(i).Visible = True
GridConsumos.Columns(i).Caption = "Unisalen"
GridConsumos.Columns(i).Locked = True
GridConsumos.Columns(i).Width = 0
      
'se rellena la combo de las horas
cboHora.AddItem 10
cboHora.AddItem 16
cboHora.AddItem 22

Call Inicializar_Toolbar

End Sub



Private Sub GridConsumos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strDelete As String
Dim respuesta As Integer

DispPromptMsg = 0

respuesta = MsgBox("            �Desea Borrar el registro?            " & Chr(13), _
                                       vbYesNo, "Pregunta")
If respuesta = 7 Then 'no
  Cancel = 1
  Else 'si
    Me.MousePointer = vbHourglass
    stra = "SELECT * FROM FR8000 WHERE FR80NUMMOV=" & GridConsumos.Columns("C�digo").Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not rsta.EOF Then
      strDelete = "DELETE FR3500 WHERE FR90CODTIPMOV=2 AND "
      strDelete = strDelete & " FR73CODPRODUCTO=" & rsta.rdoColumns("FR73CODPRODUCTO").Value
      strDelete = strDelete & " AND FR35CANTPRODUCTO=" & objGen.ReplaceStr(rsta.rdoColumns("FR80CANTPROD").Value, ",", ".", 1)
      strDelete = strDelete & " AND FR35FECMOVIMIENTO=TO_DATE('" & dtcFecha.Date & " " & cboHora.Text & ":00:00','DD/MM/YYYY HH24:MI:SS')"
      objApp.rdoConnect.Execute strDelete, 64
      objApp.rdoConnect.Execute "Commit", 64
      strDelete = "DELETE FR8000 WHERE FR80NUMMOV=" & GridConsumos.Columns("C�digo").Value
      objApp.rdoConnect.Execute strDelete, 64
      objApp.rdoConnect.Execute "Commit", 64
      Call Rellenar_Grid
    End If
    rsta.Close
    Set rsta = Nothing
    Me.MousePointer = vbDefault
End If

End Sub

Private Sub GridConsumos_KeyPress(KeyAscii As Integer)
If GridConsumos.Col = 7 Then
 Select Case KeyAscii
    Case 8, Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), _
            Asc("7"), Asc("8"), Asc("9"), Asc(",")
    Case Else
      KeyAscii = 0
 End Select
 If Len(GridConsumos.Columns("Cantidad").Value) > 8 And KeyAscii <> 8 Then 'KeyAscii=8->Borrar
  KeyAscii = 0
End If
 
End If
End Sub

Private Sub GridConsumos_KeyUp(KeyCode As Integer, Shift As Integer)
If GridConsumos.Col = 7 Then
  GridConsumos.Columns("Unisalen").Value = GridConsumos.Columns("Cantidad").Value
End If
tlbToolbar1.Buttons(4).Enabled = True
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Alerta" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, False))
    End If
    Set objPrinter = Nothing
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim i As Integer
Dim stra As String
Dim strinsert As String
Dim strUpdate As String
Dim strDelete As String
Dim respuesta As Integer

Select Case btnButton.Index
  Case 2:
      If IsDate(dtcFecha.Text) And IsNumeric(cboHora.Text) Then
          GridConsumos.Update
          tlbToolbar1.Buttons(4).Enabled = True
          'se hace un nuevo registro
          GridConsumos.AddNew
          sqlstr = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
          Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
          GridConsumos.Columns("C�digo").Value = rsta.rdoColumns(0).Value
          GridConsumos.Columns("Almac�n Origen").Value = 0 'Almac�n Origen (Farmacia=0)
          GridConsumos.Columns("Almac�n Destino").Value = 7 'Almac�n Destino(Fabricaci�n=7)
          GridConsumos.Columns("Tipo Movimiento").Value = 2 'C�digo de Movimiento
          GridConsumos.Columns("Fecha").Value = dtcFecha.Date & " " & cboHora.Text & ":00:00"
          GridConsumos.Columns("Precio").Value = 1 'Precio
          GridConsumos.Columns("UM").Value = "NE" 'UM
          rsta.Close
          Set rsta = Nothing
          GridConsumos.Col = 7
      Else
          Call MsgBox("Debe especificar Fecha y Hora.", vbInformation, "Aviso")
      End If
  Case 4:
          'se recorre el grid y se hace insert de los registros nuevos
          Me.MousePointer = vbHourglass
          'se mira si alguna cantidad est� vac�a
          GridConsumos.MoveFirst
          For i = 0 To GridConsumos.Rows - 1
            GridConsumos.Columns("Unisalen").Value = GridConsumos.Columns("Cantidad").Value
            If GridConsumos.Columns("Cantidad").Value = "" Then
              Call MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
              Me.MousePointer = vbDefault
              Exit Sub
            Else
            'se comprueba que el formato sea correcto
              If Not IsNumeric(GridConsumos.Columns("Cantidad").Value) Then
                Call MsgBox("El campo Cantidad no tiene un formato correcto (999999999,99).", vbInformation, "Aviso")
                Me.MousePointer = vbDefault
                Exit Sub
              End If
            End If
          GridConsumos.MoveNext
          Next i
          LabelMensaje.Visible = True
          FrameMensaje.Visible = True
          GridConsumos.MoveFirst
          For i = 0 To GridConsumos.Rows - 1
              stra = "SELECT * FROM FR8000 WHERE FR80NUMMOV=" & GridConsumos.Columns("C�digo").Value
              Set rsta = objApp.rdoConnect.OpenResultset(stra)
              If rsta.EOF Then
                strinsert = "INSERT INTO FR8000(FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,"
                strinsert = strinsert & "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR73CODPRODUCTO,"
                strinsert = strinsert & "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA,FR80UNISALEN)"
                strinsert = strinsert & " VALUES ("
                strinsert = strinsert & GridConsumos.Columns("C�digo").Value & ","
                strinsert = strinsert & GridConsumos.Columns("Almac�n Origen").Value & ","
                strinsert = strinsert & GridConsumos.Columns("Almac�n Destino").Value & ","
                strinsert = strinsert & GridConsumos.Columns("Tipo Movimiento").Value & ","
                strinsert = strinsert & "TO_DATE('" & GridConsumos.Columns("Fecha").Value & "','DD/MM/YYYY HH24:MI:SS')" & ","
                strinsert = strinsert & GridConsumos.Columns("C�digo Producto").Value & ","
                strinsert = strinsert & objGen.ReplaceStr(GridConsumos.Columns("Cantidad").Value, ",", ".", 1) & ","
                strinsert = strinsert & GridConsumos.Columns("Precio").Value & ","
                strinsert = strinsert & "'" & GridConsumos.Columns("UM").Value & "'" & ","
                strinsert = strinsert & objGen.ReplaceStr(GridConsumos.Columns("Unisalen").Value, ",", ".", 1)
                strinsert = strinsert & ")"
                objApp.rdoConnect.Execute strinsert, 64
                objApp.rdoConnect.Execute "Commit", 64
              Else
                strUpdate = "UPDATE FR8000 SET FR80CANTPROD="
                strUpdate = strUpdate & objGen.ReplaceStr(GridConsumos.Columns("Cantidad").Value, ",", ".", 1)
                strUpdate = strUpdate & " WHERE FR80NUMMOV=" & GridConsumos.Columns("C�digo").Value
                objApp.rdoConnect.Execute strUpdate, 64
                objApp.rdoConnect.Execute "Commit", 64
              End If
              rsta.Close
              Set rsta = Nothing
              tlbToolbar1.Buttons(4).Enabled = False
            GridConsumos.MoveNext
          Next i
          'Actualizaci�n de FR3500
          strDelete = "DELETE FR3500 WHERE FR90CODTIPMOV=2 AND FR35FECMOVIMIENTO="
          strDelete = strDelete & "TO_DATE('" & dtcFecha.Date & " " & cboHora.Text & ":00:00','DD/MM/YYYY HH24:MI:SS')"
          objApp.rdoConnect.Execute strDelete, 64
          objApp.rdoConnect.Execute "Commit", 64
          
          strinsert = "INSERT INTO FR3500(FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,"
          strinsert = strinsert & "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI,FR73CODPRODUCTO,"
          strinsert = strinsert & "FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA,FR35UNIENT)"
          strinsert = strinsert & " SELECT FR35CODMOVIMIENTO_SEQUENCE.NEXTVAL,7,0,2,"
          strinsert = strinsert & "FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO,FR80CANTPROD,"
          strinsert = strinsert & "FR80PRECUNI,FR93CODUNIMEDIDA,FR80UNISALEN FROM FR8000"
          strinsert = strinsert & " WHERE FR90CODTIPMOV=2 AND FR80FECMOVIMIENTO="
          strinsert = strinsert & "TO_DATE('" & dtcFecha.Date & " " & cboHora.Text & ":00:00','DD/MM/YYYY HH24:MI:SS')"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
          LabelMensaje.Visible = False
          FrameMensaje.Visible = False
          Me.MousePointer = vbDefault
  Case 6:
          'Imprimir
  Case 21:
          Call GridConsumos.MoveFirst
  Case 22:
          Call GridConsumos.MovePrevious
  Case 23:
          Call GridConsumos.MoveNext
  Case 24:
          Call GridConsumos.MoveLast
  Case 26:
          Call Rellenar_Grid
  Case 30:
          If tlbToolbar1.Buttons(4).Enabled = False Then
            Unload Me
          Else
            respuesta = MsgBox("            �Desea salvar los cambios realizados?            " & Chr(13), _
                                vbYesNoCancel, "Aviso")
             If respuesta = 2 Then 'cancelar
              Exit Sub
             End If
             If respuesta = 7 Then 'no
              Unload Me
             End If
             If respuesta = 6 Then 'si
                Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
             End If
          End If
End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 40 'Guardar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub





Private Sub Rellenar_Grid()
Dim stra As String
Dim rsta As rdoResultset

If IsDate(dtcFecha.Text) And IsNumeric(cboHora.Text) Then
  GridConsumos.RemoveAll
  stra = "SELECT FR8000.*,FR7300.FR73DESPRODUCTO FROM FR8000,FR7300 WHERE "
  stra = stra & "FR8000.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO AND "
  stra = stra & "FR8000.FR90CODTIPMOV=2 AND FR8000.FR80FECMOVIMIENTO="
  stra = stra & "TO_DATE('" & dtcFecha.Date & " " & cboHora.Text & ":00:00','DD/MM/YYYY HH24:MI:SS')"
  stra = stra & "ORDER BY FR7300.FR73DESPRODUCTO"
  
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While Not rsta.EOF
    GridConsumos.AddItem rsta.rdoColumns("FR80NUMMOV").Value & Chr(vbKeyTab) & _
                         rsta.rdoColumns("FR04CODALMACEN_ORI").Value & Chr(vbKeyTab) & _
                         rsta.rdoColumns("FR04CODALMACEN_DES").Value & Chr(vbKeyTab) & _
                         rsta.rdoColumns("FR90CODTIPMOV").Value & Chr(vbKeyTab) & _
                         rsta.rdoColumns("FR80FECMOVIMIENTO").Value & Chr(vbKeyTab) & _
                         rsta.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                         rsta.rdoColumns("FR73DESPRODUCTO").Value & Chr(vbKeyTab) & _
                         rsta.rdoColumns("FR80CANTPROD").Value & Chr(vbKeyTab) & _
                         rsta.rdoColumns("FR80PRECUNI").Value & Chr(vbKeyTab) & _
                         rsta.rdoColumns("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab) & _
                         rsta.rdoColumns("FR80UNISALEN").Value
  rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  tlbToolbar1.Buttons(4).Enabled = False
Else
  Call MsgBox("Seleccione una Fecha y Hora.", vbInformation, "Aviso")
End If

End Sub

Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.item(2).Enabled = False
   tlbToolbar1.Buttons.item(3).Enabled = False
   tlbToolbar1.Buttons.item(4).Enabled = False
   tlbToolbar1.Buttons.item(6).Enabled = False
   tlbToolbar1.Buttons.item(8).Enabled = False
   tlbToolbar1.Buttons.item(10).Enabled = False
   tlbToolbar1.Buttons.item(11).Enabled = False
   tlbToolbar1.Buttons.item(12).Enabled = False
   tlbToolbar1.Buttons.item(14).Enabled = False
   tlbToolbar1.Buttons.item(16).Enabled = False
   tlbToolbar1.Buttons.item(18).Enabled = False
   tlbToolbar1.Buttons.item(19).Enabled = False
   tlbToolbar1.Buttons.item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuDatosOpcion(80).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub

