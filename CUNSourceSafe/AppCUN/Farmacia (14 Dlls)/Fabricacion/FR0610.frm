VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmOMModAsigFM 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. F�rmulas Magistrales. Asignar Form.Magistral Normalizada"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7575
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   13361
      _Version        =   327681
      TabOrientation  =   3
      Tabs            =   2
      TabsPerRow      =   1
      TabHeight       =   520
      ShowFocusRect   =   0   'False
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "FR0610.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "CrystalReport1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraFrame1(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraFrame1(0)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdFMnormalizada"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdAnularPac"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "FR0610.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(2)"
      Tab(1).Control(1)=   "fraFrame1(3)"
      Tab(1).Control(2)=   "cmdFMNormSer"
      Tab(1).Control(3)=   "cmdAnularServ"
      Tab(1).ControlCount=   4
      Begin VB.CommandButton cmdAnularPac 
         Caption         =   "Anular"
         Height          =   495
         Left            =   10320
         TabIndex        =   202
         Top             =   5280
         Width           =   1095
      End
      Begin VB.CommandButton cmdAnularServ 
         Caption         =   "Anular"
         Height          =   495
         Left            =   -64800
         TabIndex        =   201
         Top             =   5280
         Width           =   1095
      End
      Begin VB.CommandButton cmdFMNormSer 
         Caption         =   "F.M. Normalizada"
         Height          =   735
         Left            =   -64800
         TabIndex        =   200
         Top             =   4320
         Width           =   1095
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4455
         Index           =   3
         Left            =   -74880
         TabIndex        =   181
         Top             =   2880
         Width           =   9735
         Begin TabDlg.SSTab tabTab1 
            Height          =   4095
            Index           =   3
            Left            =   120
            TabIndex        =   182
            TabStop         =   0   'False
            Top             =   240
            Width           =   9495
            _ExtentX        =   16748
            _ExtentY        =   7223
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0610.frx":0038
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "Frame2"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0610.frx":0054
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(0)"
            Tab(1).ControlCount=   1
            Begin VB.Frame Frame2 
               BorderStyle     =   0  'None
               Caption         =   "Medicamentos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   3240
               Left            =   240
               TabIndex        =   183
               Top             =   120
               Width           =   8415
               Begin VB.TextBox txtText1 
                  DataField       =   "FR93CODUNIMEDIDA"
                  Height          =   330
                  Index           =   76
                  Left            =   6360
                  TabIndex        =   192
                  Tag             =   "U.M.P."
                  Top             =   600
                  Width           =   540
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR20DESFORMAG"
                  Height          =   330
                  Index           =   75
                  Left            =   0
                  TabIndex        =   191
                  Tag             =   "Form.Magistral"
                  Top             =   600
                  Width           =   5295
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR20PATHFORMAG"
                  Height          =   330
                  Index           =   74
                  Left            =   0
                  TabIndex        =   190
                  Tag             =   "Arch.Inf.F.M.|Archivo Inf. F�rmula Magistral"
                  Top             =   1680
                  Width           =   4245
               End
               Begin VB.CommandButton cmdWord2 
                  Caption         =   "W"
                  BeginProperty Font 
                     Name            =   "MS Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   350
                  Left            =   4320
                  TabIndex        =   189
                  Top             =   1680
                  Width           =   495
               End
               Begin VB.CommandButton cmdExplorador 
                  Caption         =   "..."
                  BeginProperty Font 
                     Name            =   "MS Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   525
                  Index           =   0
                  Left            =   6720
                  TabIndex        =   188
                  Top             =   1560
                  Visible         =   0   'False
                  Width           =   495
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR20CANTNECESQUIR"
                  Height          =   330
                  Index           =   73
                  Left            =   5400
                  TabIndex        =   187
                  Tag             =   "Cantidad"
                  Top             =   600
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR55CODNECESUNID"
                  Height          =   330
                  Index           =   72
                  Left            =   0
                  TabIndex        =   186
                  Tag             =   "C�d. Necesidad"
                  Top             =   2760
                  Visible         =   0   'False
                  Width           =   1100
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR20NUMLINEA"
                  Height          =   330
                  Index           =   71
                  Left            =   1560
                  TabIndex        =   185
                  Tag             =   "Linea"
                  Top             =   2760
                  Visible         =   0   'False
                  Width           =   1100
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CODPRODUCTO"
                  Height          =   330
                  Index           =   70
                  Left            =   3240
                  TabIndex        =   184
                  Tag             =   "C�d.Producto"
                  Top             =   2760
                  Visible         =   0   'False
                  Width           =   1100
               End
               Begin MSComDlg.CommonDialog CommonDialog2 
                  Left            =   6120
                  Top             =   1560
                  _ExtentX        =   847
                  _ExtentY        =   847
                  _Version        =   327681
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "F�rmula Magistral"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   61
                  Left            =   0
                  TabIndex        =   198
                  Top             =   360
                  Width           =   1500
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Cantidad   preparar"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   60
                  Left            =   5400
                  TabIndex        =   197
                  Top             =   360
                  Width           =   1650
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Archivo Inf. F�rmula Magistral"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   59
                  Left            =   0
                  TabIndex        =   196
                  Top             =   1440
                  Width           =   2550
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�d. Necesidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   58
                  Left            =   0
                  TabIndex        =   195
                  Top             =   2520
                  Visible         =   0   'False
                  Width           =   1365
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Linea"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   57
                  Left            =   1560
                  TabIndex        =   194
                  Top             =   2520
                  Visible         =   0   'False
                  Width           =   480
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�d.Producto"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   56
                  Left            =   3240
                  TabIndex        =   193
                  Top             =   2520
                  Visible         =   0   'False
                  Width           =   1170
               End
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   3705
               Index           =   0
               Left            =   -74880
               TabIndex        =   199
               TabStop         =   0   'False
               Top             =   240
               Width           =   8895
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15690
               _ExtentY        =   6535
               _StockProps     =   79
            End
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2775
         Index           =   2
         Left            =   -74880
         TabIndex        =   158
         Top             =   120
         Width           =   11220
         Begin TabDlg.SSTab tabTab1 
            Height          =   2295
            Index           =   2
            Left            =   120
            TabIndex        =   159
            TabStop         =   0   'False
            Top             =   360
            Width           =   10935
            _ExtentX        =   19288
            _ExtentY        =   4048
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0610.frx":0070
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(22)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(27)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(43)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(44)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(45)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(46)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(53)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(54)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(55)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(64)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "dtcDateCombo1(6)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "chkCheck1(6)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(61)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(62)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(63)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(64)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(65)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(66)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(67)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(68)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(69)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(79)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(80)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "chkCheck1(8)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).ControlCount=   24
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0610.frx":008C
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(1)"
            Tab(1).ControlCount=   1
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Inter�s Cient�fico"
               DataField       =   "FR55INDINTERCIENT"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   5640
               TabIndex        =   208
               Tag             =   "Inter�s Cient�fico?"
               Top             =   1800
               Width           =   1815
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   80
               Left            =   8160
               TabIndex        =   207
               Tag             =   "Dpto.Cargo"
               Top             =   1800
               Width           =   2325
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO_CRG"
               Height          =   330
               Index           =   79
               Left            =   7680
               TabIndex        =   206
               Tag             =   "C�d.Dpto.Cargo"
               Top             =   1800
               Width           =   360
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   69
               Left            =   2520
               TabIndex        =   169
               TabStop         =   0   'False
               Tag             =   "Estado"
               Top             =   360
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR26CODESTPETIC"
               Height          =   330
               Index           =   68
               Left            =   2040
               TabIndex        =   168
               Tag             =   "Estado Petici�n"
               Top             =   360
               Width           =   315
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR55CODNECESUNID"
               Height          =   330
               Index           =   67
               Left            =   120
               TabIndex        =   167
               Tag             =   "C�d. Necesidad"
               Top             =   360
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   66
               Left            =   1680
               TabIndex        =   166
               TabStop         =   0   'False
               Tag             =   "Descripci�n Servicio"
               Top             =   1080
               Width           =   3735
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD_PDS"
               Height          =   330
               Index           =   65
               Left            =   120
               TabIndex        =   165
               Tag             =   "C�d. Peticionario"
               Top             =   1800
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   64
               Left            =   120
               TabIndex        =   164
               Tag             =   "C�digo Servicio"
               Top             =   1080
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   63
               Left            =   1680
               TabIndex        =   163
               TabStop         =   0   'False
               Tag             =   "Persona Peticionaria"
               Top             =   1800
               Width           =   3735
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "AD41CODSECCION"
               Height          =   330
               Index           =   62
               Left            =   5640
               TabIndex        =   162
               Tag             =   "C�digo Secci�n"
               Top             =   1080
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   61
               Left            =   6960
               TabIndex        =   161
               TabStop         =   0   'False
               Tag             =   "Descripci�n Secci�n"
               Top             =   1080
               Width           =   3495
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "FM"
               DataField       =   "FR55INDFM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   8280
               TabIndex        =   160
               Tag             =   "Form.Magistral?"
               Top             =   360
               Value           =   1  'Checked
               Visible         =   0   'False
               Width           =   735
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   2025
               Index           =   1
               Left            =   -74880
               TabIndex        =   170
               TabStop         =   0   'False
               Top             =   120
               Width           =   10335
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18230
               _ExtentY        =   3572
               _StockProps     =   79
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR55FECPETICION"
               Height          =   330
               Index           =   6
               Left            =   5640
               TabIndex        =   171
               Tag             =   "Fecha Petici�n"
               Top             =   360
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Departamento de Cargo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   64
               Left            =   7680
               TabIndex        =   209
               Top             =   1560
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Estado"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   55
               Left            =   2040
               TabIndex        =   180
               Top             =   120
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d. Necesidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   54
               Left            =   120
               TabIndex        =   179
               Top             =   120
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Petici�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   53
               Left            =   5640
               TabIndex        =   178
               Top             =   120
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   46
               Left            =   1680
               TabIndex        =   177
               Top             =   840
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d. Servicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   45
               Left            =   120
               TabIndex        =   176
               Top             =   840
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Persona Peticionaria"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   44
               Left            =   1680
               TabIndex        =   175
               Top             =   1560
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.Peticionario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   43
               Left            =   120
               TabIndex        =   174
               Top             =   1560
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�d. Secci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   27
               Left            =   5640
               TabIndex        =   173
               Top             =   840
               Width           =   1155
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Secci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   6960
               TabIndex        =   172
               Top             =   840
               Width           =   1815
            End
         End
      End
      Begin VB.CommandButton cmdFMnormalizada 
         Caption         =   "F.M. Normalizada"
         Height          =   735
         Left            =   10320
         TabIndex        =   157
         Top             =   4320
         Width           =   1095
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4320
         Index           =   0
         Left            =   120
         TabIndex        =   80
         Top             =   3060
         Width           =   10095
         Begin TabDlg.SSTab tabTab1 
            Height          =   3975
            Index           =   1
            Left            =   120
            TabIndex        =   81
            TabStop         =   0   'False
            Top             =   240
            Width           =   9855
            _ExtentX        =   17383
            _ExtentY        =   7011
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0610.frx":00A8
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "Frame1"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0610.frx":00C4
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(6)"
            Tab(1).ControlCount=   1
            Begin VB.Frame Frame1 
               BorderStyle     =   0  'None
               Caption         =   "Medicamentos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   3720
               Left            =   240
               TabIndex        =   82
               Top             =   120
               Width           =   9255
               Begin VB.Frame Frame3 
                  BorderStyle     =   0  'None
                  Height          =   375
                  Left            =   8640
                  TabIndex        =   121
                  Top             =   3240
                  Width           =   495
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR28INSTRADMIN"
                  Height          =   1065
                  Index           =   39
                  Left            =   0
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   120
                  Tag             =   "Inst.Admin.|Instrucciones para administraci�n"
                  Top             =   2640
                  Width           =   8445
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28CANTDISP"
                  Height          =   330
                  Index           =   58
                  Left            =   7200
                  TabIndex        =   119
                  Tag             =   "Cant.Dispens."
                  Top             =   3600
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.CommandButton cmdExplorador 
                  Caption         =   "..."
                  BeginProperty Font 
                     Name            =   "MS Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   1
                  Left            =   4320
                  TabIndex        =   118
                  Top             =   1440
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.CommandButton cmdWord 
                  Caption         =   "W"
                  BeginProperty Font 
                     Name            =   "MS Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   350
                  Left            =   3720
                  TabIndex        =   117
                  Top             =   1440
                  Width           =   495
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR28PATHFORMAG"
                  Height          =   330
                  Index           =   57
                  Left            =   0
                  TabIndex        =   116
                  Tag             =   "Arch.Inf.F.M.|Archivo Inf. F�rmula Magistral"
                  Top             =   1440
                  Width           =   3645
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28REFERENCIA"
                  Height          =   330
                  Index           =   56
                  Left            =   6480
                  TabIndex        =   115
                  Tag             =   "Referencia"
                  Top             =   3600
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR28DESPRODUCTO"
                  Height          =   330
                  Index           =   55
                  Left            =   0
                  TabIndex        =   114
                  Tag             =   "F�rmula Magistral"
                  Top             =   240
                  Width           =   5295
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   13
                  Left            =   6480
                  TabIndex        =   113
                  Tag             =   "Medicamento"
                  Top             =   2280
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28UBICACION"
                  Height          =   330
                  Index           =   54
                  Left            =   6240
                  TabIndex        =   112
                  Tag             =   "Ubicaci�n"
                  Top             =   3600
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28OPERACION"
                  Height          =   330
                  Index           =   53
                  Left            =   6720
                  TabIndex        =   111
                  Tag             =   "Operaci�n"
                  Top             =   3600
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CODPRODUCTO_2"
                  Height          =   330
                  Index           =   52
                  Left            =   8880
                  TabIndex        =   110
                  Tag             =   "C�digo Medicamento 2"
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   51
                  Left            =   7800
                  TabIndex        =   109
                  Tag             =   "Medicamento 2"
                  Top             =   3600
                  Visible         =   0   'False
                  Width           =   1380
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28DOSIS_2"
                  Height          =   330
                  Index           =   50
                  Left            =   6960
                  TabIndex        =   108
                  Tag             =   "Dosis2"
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR93CODUNIMEDIDA_2"
                  Height          =   330
                  Index           =   49
                  Left            =   6360
                  TabIndex        =   107
                  Tag             =   "UMP."
                  Top             =   240
                  Width           =   540
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FRH7CODFORMFAR_2"
                  Height          =   330
                  Index           =   48
                  Left            =   6600
                  TabIndex        =   106
                  Tag             =   "FF2"
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28VOLUMEN_2"
                  Height          =   330
                  Index           =   47
                  Left            =   7800
                  TabIndex        =   105
                  Tag             =   "Vol.Prod.2|Volumen(ml) Producto 2"
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28VELPERFUSION"
                  Height          =   330
                  Index           =   82
                  Left            =   6480
                  TabIndex        =   104
                  Tag             =   "Perfusi�n|Velocidad de Perfusi�n"
                  Top             =   1800
                  Visible         =   0   'False
                  Width           =   975
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28VOLTOTAL"
                  Height          =   330
                  Index           =   44
                  Left            =   7800
                  TabIndex        =   103
                  Tag             =   "Vol.Total|Volumen Total (ml)"
                  Top             =   2040
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28VOLUMEN"
                  Height          =   330
                  Index           =   38
                  Left            =   7800
                  TabIndex        =   102
                  Tag             =   "Volum.|Volumen(ml)"
                  Top             =   240
                  Visible         =   0   'False
                  Width           =   732
               End
               Begin VB.TextBox txtMinutos 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Left            =   7800
                  TabIndex        =   101
                  Tag             =   "Tiempo Infusi�n(min)"
                  Top             =   1440
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.TextBox txtHoras 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Left            =   7320
                  TabIndex        =   100
                  Tag             =   "Tiempo Infusi�n(hor)"
                  Top             =   1440
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FRH7CODFORMFAR"
                  Height          =   330
                  Index           =   35
                  Left            =   5160
                  TabIndex        =   99
                  Tag             =   "FF"
                  Top             =   2160
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "fr28tiemmininf"
                  Height          =   330
                  Index           =   43
                  Left            =   7440
                  TabIndex        =   98
                  Tag             =   "T.Inf.(min)|Tiempo Infusi�n(min)"
                  Top             =   3600
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28CANTIDADDIL"
                  Height          =   330
                  Index           =   42
                  Left            =   6240
                  TabIndex        =   97
                  Tag             =   "Vol.Dil.|Volumen(ml)"
                  Top             =   1440
                  Visible         =   0   'False
                  Width           =   732
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   41
                  Left            =   6000
                  TabIndex        =   96
                  Tag             =   "Descripci�n Producto Diluir"
                  Top             =   2160
                  Visible         =   0   'False
                  Width           =   345
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CODPRODUCTO_DIL"
                  Height          =   330
                  Index           =   40
                  Left            =   8160
                  TabIndex        =   95
                  Tag             =   "C�digo Producto diluir"
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "PERF"
                  DataField       =   "FR28INDPERF"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   8280
                  TabIndex        =   94
                  Tag             =   "PERF|Mezcla IV en Farmacia"
                  Top             =   1035
                  Visible         =   0   'False
                  Width           =   855
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "STAT"
                  DataField       =   "fr28indcomieninmed"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   8280
                  TabIndex        =   93
                  Tag             =   "STAT|Pedir la primera dosis inmediatamente"
                  Top             =   800
                  Visible         =   0   'False
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "fr28horafin"
                  Height          =   330
                  Index           =   36
                  Left            =   4560
                  TabIndex        =   92
                  Tag             =   "H.F.|Hora Fin"
                  Top             =   2040
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "fr28horainicio"
                  Height          =   330
                  Index           =   37
                  Left            =   1920
                  TabIndex        =   91
                  Tag             =   "H.I.|Hora Inicio"
                  Top             =   2040
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR93CODUNIMEDIDA"
                  Height          =   330
                  Index           =   31
                  Left            =   960
                  TabIndex        =   90
                  Tag             =   "UM"
                  Top             =   840
                  Width           =   660
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28DOSIS"
                  Height          =   330
                  Index           =   30
                  Left            =   0
                  TabIndex        =   89
                  Tag             =   "Dosis"
                  Top             =   840
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR28CANTIDAD"
                  Height          =   330
                  Index           =   28
                  Left            =   5400
                  TabIndex        =   88
                  Tag             =   "Cantidad"
                  Top             =   240
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CODPRODUCTO"
                  Height          =   330
                  Index           =   27
                  Left            =   8520
                  TabIndex        =   87
                  Tag             =   "C�digo Medicamento"
                  Top             =   3240
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "PRN"
                  DataField       =   "FR28INDDISPPRN"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   1
                  Left            =   7920
                  TabIndex        =   86
                  Tag             =   "Prn?"
                  Top             =   2640
                  Visible         =   0   'False
                  Width           =   735
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Cambiar V�a"
                  DataField       =   "FR28INDVIAOPC"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   5
                  Left            =   6240
                  TabIndex        =   85
                  Tag             =   "Cambiar V�a?"
                  Top             =   2760
                  Visible         =   0   'False
                  Width           =   1455
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "fr28numlinea"
                  Height          =   405
                  Index           =   45
                  Left            =   5760
                  TabIndex        =   84
                  Tag             =   "Num.Linea"
                  Top             =   3600
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "fr66codpeticion"
                  Height          =   405
                  Index           =   34
                  Left            =   5400
                  TabIndex        =   83
                  Tag             =   "C�digo Petici�n"
                  Top             =   3600
                  Visible         =   0   'False
                  Width           =   255
               End
               Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
                  DataField       =   "fr28fecinicio"
                  Height          =   330
                  Index           =   3
                  Left            =   0
                  TabIndex        =   122
                  Tag             =   "Inicio|Fecha Inicio Vigencia"
                  Top             =   2040
                  Width           =   1860
                  _Version        =   65537
                  _ExtentX        =   3281
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DefaultDate     =   ""
                  MinDate         =   "1900/1/1"
                  MaxDate         =   "2100/12/31"
                  Format          =   "DD/MM/YYYY"
                  AllowNullDate   =   -1  'True
                  ShowCentury     =   -1  'True
                  Mask            =   2
                  StartofWeek     =   2
               End
               Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
                  DataField       =   "fr28fecfin"
                  Height          =   330
                  Index           =   4
                  Left            =   2640
                  TabIndex        =   123
                  Tag             =   "Fin|Fecha Fin"
                  Top             =   2040
                  Width           =   1860
                  _Version        =   65537
                  _ExtentX        =   3281
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DefaultDate     =   ""
                  MinDate         =   "1900/1/1"
                  MaxDate         =   "2100/12/31"
                  Format          =   "DD/MM/YYYY"
                  AllowNullDate   =   -1  'True
                  ShowCentury     =   -1  'True
                  Mask            =   2
                  StartofWeek     =   2
               End
               Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
                  DataField       =   "fr34codvia"
                  Height          =   330
                  Index           =   1
                  Left            =   3480
                  TabIndex        =   124
                  Tag             =   "V�a|C�digo V�a"
                  Top             =   840
                  Width           =   765
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets.count =   2
                  stylesets(0).Name=   "Activo"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).Picture=   "FR0610.frx":00E0
                  stylesets(1).Name=   "Inactivo"
                  stylesets(1).BackColor=   255
                  stylesets(1).Picture=   "FR0610.frx":00FC
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   1349
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   16777215
                  DataFieldToDisplay=   "Column 0"
               End
               Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
                  DataField       =   "frg4codfrecuencia"
                  Height          =   330
                  Index           =   0
                  Left            =   1680
                  TabIndex        =   125
                  Tag             =   "Frec.|Cod.Frecuencia"
                  Top             =   840
                  Width           =   1725
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets.count =   2
                  stylesets(0).Name=   "Activo"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).Picture=   "FR0610.frx":0118
                  stylesets(1).Name=   "Inactivo"
                  stylesets(1).BackColor=   255
                  stylesets(1).Picture=   "FR0610.frx":0134
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   3043
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   16777215
                  DataFieldToDisplay=   "Column 0"
               End
               Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
                  DataField       =   "frh5codperiodicidad"
                  Height          =   330
                  Index           =   3
                  Left            =   4320
                  TabIndex        =   126
                  Tag             =   "Period.|Cod. Periodicidad"
                  Top             =   840
                  Width           =   1845
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets.count =   2
                  stylesets(0).Name=   "Activo"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).Picture=   "FR0610.frx":0150
                  stylesets(1).Name=   "Inactivo"
                  stylesets(1).BackColor=   255
                  stylesets(1).Picture=   "FR0610.frx":016C
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   3254
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   16777215
                  DataFieldToDisplay=   "Column 0"
               End
               Begin MSComDlg.CommonDialog CommonDialog1 
                  Left            =   6360
                  Top             =   720
                  _ExtentX        =   847
                  _ExtentY        =   847
                  _Version        =   327681
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Archivo Inf. F�rmula Magistral"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   84
                  Left            =   0
                  TabIndex        =   155
                  Top             =   1200
                  Width           =   2550
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Medicamento 2"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   83
                  Left            =   5400
                  TabIndex        =   154
                  Top             =   2520
                  Visible         =   0   'False
                  Width           =   1305
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Dosis"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   82
                  Left            =   6960
                  TabIndex        =   153
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   480
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "UM"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   81
                  Left            =   7440
                  TabIndex        =   152
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   300
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "FF"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   80
                  Left            =   6600
                  TabIndex        =   151
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   225
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�digo Med2"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   79
                  Left            =   5400
                  TabIndex        =   150
                  Top             =   3120
                  Visible         =   0   'False
                  Width           =   1125
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Volumen(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   78
                  Left            =   7800
                  TabIndex        =   149
                  Top             =   3000
                  Visible         =   0   'False
                  Width           =   1035
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "ml/hora"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   77
                  Left            =   6420
                  TabIndex        =   148
                  Top             =   2085
                  Visible         =   0   'False
                  Width           =   660
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Vel.Perfus."
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   52
                  Left            =   5400
                  TabIndex        =   147
                  Top             =   1800
                  Visible         =   0   'False
                  Width           =   945
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Vol.Total(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   51
                  Left            =   7800
                  TabIndex        =   146
                  Top             =   1800
                  Visible         =   0   'False
                  Width           =   1080
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Instrucciones para administraci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   50
                  Left            =   0
                  TabIndex        =   145
                  Top             =   2400
                  Width           =   2865
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Volumen(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   49
                  Left            =   7800
                  TabIndex        =   144
                  Top             =   0
                  Visible         =   0   'False
                  Width           =   1035
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�digo Sol"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   48
                  Left            =   5400
                  TabIndex        =   143
                  Top             =   2760
                  Visible         =   0   'False
                  Width           =   930
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�digo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   47
                  Left            =   5400
                  TabIndex        =   142
                  Top             =   2040
                  Visible         =   0   'False
                  Width           =   600
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "FF"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   37
                  Left            =   5160
                  TabIndex        =   141
                  Top             =   1920
                  Visible         =   0   'False
                  Width           =   225
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Tiempo Infusi�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   36
                  Left            =   7320
                  TabIndex        =   140
                  Top             =   1200
                  Visible         =   0   'False
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Volumen(ml)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   35
                  Left            =   6240
                  TabIndex        =   139
                  Top             =   1200
                  Visible         =   0   'False
                  Width           =   1035
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Soluci�n para Diluir"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   34
                  Left            =   5400
                  TabIndex        =   138
                  Top             =   3360
                  Visible         =   0   'False
                  Width           =   2055
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hora "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   30
                  Left            =   4560
                  TabIndex        =   137
                  Top             =   1800
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Fecha Inicio"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   33
                  Left            =   0
                  TabIndex        =   136
                  Top             =   1800
                  Width           =   1815
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hora "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   32
                  Left            =   1920
                  TabIndex        =   135
                  Top             =   1800
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Fecha Fin"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   31
                  Left            =   2640
                  TabIndex        =   134
                  Top             =   1800
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Periodicidad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   29
                  Left            =   4320
                  TabIndex        =   133
                  Top             =   600
                  Width           =   1335
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "V�a"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   25
                  Left            =   3480
                  TabIndex        =   132
                  Top             =   600
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Frecuencia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   23
                  Left            =   1680
                  TabIndex        =   131
                  Top             =   600
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "UM"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   21
                  Left            =   960
                  TabIndex        =   130
                  Top             =   600
                  Width           =   300
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Dosis"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   19
                  Left            =   0
                  TabIndex        =   129
                  Top             =   600
                  Width           =   480
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Cantidad   preparar"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   17
                  Left            =   5400
                  TabIndex        =   128
                  Top             =   0
                  Width           =   1650
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "F�rmula Magistral"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   16
                  Left            =   0
                  TabIndex        =   127
                  Top             =   0
                  Width           =   1500
               End
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   3825
               Index           =   6
               Left            =   -74820
               TabIndex        =   156
               Top             =   120
               Width           =   9255
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               stylesets.count =   6
               stylesets(0).Name=   "Estupefacientes"
               stylesets(0).BackColor=   14671839
               stylesets(0).Picture=   "FR0610.frx":0188
               stylesets(1).Name=   "FormMagistral"
               stylesets(1).BackColor=   8454016
               stylesets(1).Picture=   "FR0610.frx":01A4
               stylesets(2).Name=   "Medicamentos"
               stylesets(2).BackColor=   16777215
               stylesets(2).Picture=   "FR0610.frx":01C0
               stylesets(3).Name=   "MezclaIV2M"
               stylesets(3).BackColor=   8454143
               stylesets(3).Picture=   "FR0610.frx":01DC
               stylesets(4).Name=   "Fluidoterapia"
               stylesets(4).BackColor=   16776960
               stylesets(4).Picture=   "FR0610.frx":01F8
               stylesets(5).Name=   "PRNenOM"
               stylesets(5).BackColor=   12615935
               stylesets(5).Picture=   "FR0610.frx":0214
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   0
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   16325
               _ExtentY        =   6747
               _StockProps     =   79
            End
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3015
         Index           =   1
         Left            =   120
         TabIndex        =   4
         Top             =   60
         Width           =   10185
         Begin TabDlg.SSTab tabTab1 
            Height          =   2655
            Index           =   0
            Left            =   120
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   240
            Width           =   9975
            _ExtentX        =   17595
            _ExtentY        =   4683
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0610.frx":0230
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(28)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(14)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(13)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "tab1"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "chkOMPRN(6)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(1)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "chkCheck1(12)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "chkCheck1(0)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "chkCheck1(2)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "chkCheck1(7)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "chkCheck1(15)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(25)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(26)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(0)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).ControlCount=   14
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0610.frx":024C
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(2)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR66CODPETICION"
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   15
               Tag             =   "C�digo Petici�n"
               Top             =   360
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   26
               Left            =   3480
               TabIndex        =   14
               TabStop         =   0   'False
               Tag             =   "Estado"
               Top             =   360
               Width           =   1605
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR26CODESTPETIC"
               Height          =   330
               Index           =   25
               Left            =   3120
               TabIndex        =   13
               Tag             =   "Estado Petici�n"
               Top             =   360
               Width           =   315
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Inter�s Cient�fico"
               DataField       =   "FR66INDINTERCIENT"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   7680
               TabIndex        =   12
               Tag             =   "Inter�s Cient�fico?"
               Top             =   120
               Width           =   1815
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Restricci�n Volumen"
               DataField       =   "FR66INDRESTVOLUM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   5400
               TabIndex        =   11
               Tag             =   "Restricci�n de Volumen?"
               Top             =   600
               Width           =   2175
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Al�rgico"
               DataField       =   "FR66INDALERGIA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   5400
               TabIndex        =   10
               Tag             =   "Alergico?"
               Top             =   360
               Width           =   2055
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Diab�tico"
               DataField       =   "FR66INDPACIDIABET"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   5400
               TabIndex        =   9
               Tag             =   "Diab�tico?"
               Top             =   120
               Width           =   1575
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Orden M�dica"
               DataField       =   "FR66INDOM"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   7680
               TabIndex        =   8
               Tag             =   "Orden M�dica?"
               Top             =   360
               Width           =   1575
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR66CODOMORIPRN"
               Height          =   330
               Index           =   1
               Left            =   1320
               TabIndex        =   7
               Tag             =   "C�digo OM origen PRN"
               Top             =   360
               Width           =   1100
            End
            Begin VB.CheckBox chkOMPRN 
               Caption         =   "PRN"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   7680
               TabIndex        =   6
               Tag             =   "PRN?"
               Top             =   600
               Width           =   735
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   2385
               Index           =   2
               Left            =   -74880
               TabIndex        =   16
               TabStop         =   0   'False
               Top             =   120
               Width           =   9495
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   16748
               _ExtentY        =   4207
               _StockProps     =   79
            End
            Begin TabDlg.SSTab tab1 
               Height          =   1815
               Left            =   120
               TabIndex        =   17
               TabStop         =   0   'False
               Top             =   720
               Width           =   9495
               _ExtentX        =   16748
               _ExtentY        =   3201
               _Version        =   327681
               Style           =   1
               Tabs            =   5
               TabsPerRow      =   5
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Paciente"
               TabPicture(0)   =   "FR0610.frx":0268
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(87)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(86)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(9)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(8)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "lblLabel1(7)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "lblLabel1(11)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "lblLabel1(38)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "lblLabel1(39)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "lblLabel1(40)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "lblLabel1(41)"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "lblLabel1(42)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "lblLabel1(2)"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).Control(12)=   "txtText1(60)"
               Tab(0).Control(12).Enabled=   0   'False
               Tab(0).Control(13)=   "txtText1(59)"
               Tab(0).Control(13).Enabled=   0   'False
               Tab(0).Control(14)=   "txtText1(5)"
               Tab(0).Control(14).Enabled=   0   'False
               Tab(0).Control(15)=   "txtText1(4)"
               Tab(0).Control(15).Enabled=   0   'False
               Tab(0).Control(16)=   "txtText1(2)"
               Tab(0).Control(16).Enabled=   0   'False
               Tab(0).Control(17)=   "txtText1(3)"
               Tab(0).Control(17).Enabled=   0   'False
               Tab(0).Control(18)=   "txtText1(23)"
               Tab(0).Control(18).Enabled=   0   'False
               Tab(0).Control(19)=   "txtText1(46)"
               Tab(0).Control(19).Enabled=   0   'False
               Tab(0).Control(20)=   "txtText1(32)"
               Tab(0).Control(20).Enabled=   0   'False
               Tab(0).Control(21)=   "txtText1(33)"
               Tab(0).Control(21).Enabled=   0   'False
               Tab(0).Control(22)=   "txtText1(29)"
               Tab(0).Control(22).Enabled=   0   'False
               Tab(0).Control(23)=   "Text1"
               Tab(0).Control(23).Enabled=   0   'False
               Tab(0).Control(24)=   "txtText1(6)"
               Tab(0).Control(24).Enabled=   0   'False
               Tab(0).Control(25)=   "txtText1(7)"
               Tab(0).Control(25).Enabled=   0   'False
               Tab(0).ControlCount=   26
               TabCaption(1)   =   "M�dico"
               TabPicture(1)   =   "FR0610.frx":0284
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "txtText1(12)"
               Tab(1).Control(1)=   "txtText1(20)"
               Tab(1).Control(2)=   "txtText1(9)"
               Tab(1).Control(3)=   "txtText1(8)"
               Tab(1).Control(4)=   "txtText1(22)"
               Tab(1).Control(5)=   "txtText1(11)"
               Tab(1).Control(5).Enabled=   0   'False
               Tab(1).Control(6)=   "txtText1(10)"
               Tab(1).Control(7)=   "dtcDateCombo1(1)"
               Tab(1).Control(8)=   "dtcDateCombo1(2)"
               Tab(1).Control(9)=   "lblLabel1(15)"
               Tab(1).Control(10)=   "lblLabel1(18)"
               Tab(1).Control(11)=   "lblLabel1(5)"
               Tab(1).Control(12)=   "lblLabel1(20)"
               Tab(1).Control(13)=   "lblLabel1(6)"
               Tab(1).Control(14)=   "lblLabel1(10)"
               Tab(1).Control(15)=   "lblLabel1(0)"
               Tab(1).ControlCount=   16
               TabCaption(2)   =   "Servicio"
               TabPicture(2)   =   "FR0610.frx":02A0
               Tab(2).ControlEnabled=   0   'False
               Tab(2).Control(0)=   "txtText1(78)"
               Tab(2).Control(1)=   "txtText1(77)"
               Tab(2).Control(2)=   "txtText1(21)"
               Tab(2).Control(3)=   "txtText1(19)"
               Tab(2).Control(4)=   "txtText1(15)"
               Tab(2).Control(5)=   "txtText1(14)"
               Tab(2).Control(6)=   "txtText1(24)"
               Tab(2).Control(7)=   "txtText1(16)"
               Tab(2).Control(8)=   "txtText1(17)"
               Tab(2).Control(9)=   "lblLabel1(63)"
               Tab(2).Control(10)=   "lblLabel1(4)"
               Tab(2).Control(11)=   "lblLabel1(3)"
               Tab(2).Control(12)=   "lblLabel1(12)"
               Tab(2).Control(13)=   "lblLabel1(1)"
               Tab(2).ControlCount=   14
               TabCaption(3)   =   "Observaciones"
               TabPicture(3)   =   "FR0610.frx":02BC
               Tab(3).ControlEnabled=   0   'False
               Tab(3).Control(0)=   "txtText1(18)"
               Tab(3).Control(1)=   "lblLabel1(24)"
               Tab(3).ControlCount=   2
               TabCaption(4)   =   "Validaci�n/Cierre"
               TabPicture(4)   =   "FR0610.frx":02D8
               Tab(4).ControlEnabled=   0   'False
               Tab(4).Control(0)=   "dtcDateCombo1(0)"
               Tab(4).Control(1)=   "dtcDateCombo1(5)"
               Tab(4).Control(2)=   "lblLabel1(85)"
               Tab(4).Control(3)=   "lblLabel1(62)"
               Tab(4).ControlCount=   4
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "AD02CODDPTO_CRG"
                  Height          =   330
                  Index           =   78
                  Left            =   -69720
                  TabIndex        =   204
                  Tag             =   "C�d.Dpto.Cargo"
                  Top             =   660
                  Width           =   360
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   77
                  Left            =   -69240
                  TabIndex        =   203
                  Tag             =   "Dpto.Cargo"
                  Top             =   660
                  Width           =   3405
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   21
                  Left            =   -72120
                  TabIndex        =   46
                  Tag             =   "N� Colegiado"
                  Top             =   1380
                  Width           =   720
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   19
                  Left            =   -74040
                  TabIndex        =   45
                  Tag             =   "Apellido Doctor"
                  Top             =   1380
                  Width           =   1725
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   15
                  Left            =   -68880
                  TabIndex        =   44
                  Tag             =   "Apellido Doctor"
                  Top             =   1380
                  Width           =   1725
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_ENF"
                  Height          =   330
                  Index           =   14
                  Left            =   -69720
                  TabIndex        =   43
                  Tag             =   "M�dico que env�a"
                  Top             =   1380
                  Width           =   612
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_FEN"
                  Height          =   330
                  Index           =   24
                  Left            =   -74760
                  TabIndex        =   42
                  Tag             =   "M�dico que firma"
                  Top             =   1380
                  Width           =   612
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   7
                  Left            =   9120
                  TabIndex        =   41
                  TabStop         =   0   'False
                  Tag             =   "Sexo"
                  Top             =   420
                  Visible         =   0   'False
                  Width           =   300
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   6
                  Left            =   2520
                  TabIndex        =   40
                  TabStop         =   0   'False
                  Tag             =   "Sexo"
                  Top             =   660
                  Width           =   1140
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR66HORAREDACCI"
                  Height          =   330
                  Index           =   10
                  Left            =   -72840
                  TabIndex        =   39
                  Tag             =   "Hora Redacci�n"
                  Top             =   1380
                  Width           =   612
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   11
                  Left            =   -70080
                  TabIndex        =   38
                  TabStop         =   0   'False
                  Tag             =   "Desc. Urgencia"
                  Top             =   1380
                  Width           =   4125
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR91CODURGENCIA"
                  Height          =   330
                  Index           =   22
                  Left            =   -70920
                  TabIndex        =   37
                  Tag             =   "C�digo Urgencia"
                  Top             =   1380
                  Width           =   735
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "AD02CODDPTO"
                  Height          =   330
                  Index           =   16
                  Left            =   -74760
                  TabIndex        =   36
                  Tag             =   "C�d.Departamento"
                  Top             =   660
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   17
                  Left            =   -73560
                  TabIndex        =   35
                  Tag             =   "Desc.Departamento"
                  Top             =   660
                  Width           =   3600
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_MED"
                  Height          =   330
                  Index           =   8
                  Left            =   -74760
                  TabIndex        =   34
                  Tag             =   "C�digo Doctor"
                  Top             =   660
                  Width           =   2000
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   9
                  Left            =   -72720
                  TabIndex        =   33
                  Tag             =   "Apellido Doctor"
                  Top             =   660
                  Width           =   1725
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR66HORAFIRMMEDI"
                  Height          =   330
                  Index           =   20
                  Left            =   -67320
                  TabIndex        =   32
                  Tag             =   "Hora Firma M�dico"
                  Top             =   660
                  Width           =   612
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "AD02CODDPTO_MED"
                  Height          =   330
                  Index           =   12
                  Left            =   -70920
                  TabIndex        =   31
                  Tag             =   "Departamento"
                  Top             =   660
                  Width           =   1275
               End
               Begin VB.TextBox Text1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Left            =   1800
                  TabIndex        =   30
                  TabStop         =   0   'False
                  Tag             =   "Edad"
                  Top             =   660
                  Width           =   420
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  Index           =   29
                  Left            =   7320
                  TabIndex        =   29
                  Tag             =   "Superficie Corporal"
                  Top             =   660
                  Width           =   600
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "FR66ALTUPAC"
                  Height          =   330
                  Index           =   33
                  Left            =   6240
                  MaxLength       =   3
                  TabIndex        =   28
                  Tag             =   "Altura Cm"
                  Top             =   660
                  Width           =   600
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "FR66PESOPAC"
                  Height          =   330
                  Index           =   32
                  Left            =   5160
                  MaxLength       =   3
                  TabIndex        =   27
                  Tag             =   "Peso"
                  Top             =   660
                  Width           =   600
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   46
                  Left            =   3840
                  TabIndex        =   26
                  TabStop         =   0   'False
                  Tag             =   "Cama"
                  Top             =   660
                  Width           =   900
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   23
                  Left            =   120
                  TabIndex        =   25
                  TabStop         =   0   'False
                  Tag             =   "Historia Paciente"
                  Top             =   660
                  Width           =   1500
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   3
                  Left            =   120
                  TabIndex        =   24
                  TabStop         =   0   'False
                  Tag             =   "Nombre Paciente"
                  Top             =   1380
                  Width           =   1600
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "CI21CODPERSONA"
                  Height          =   330
                  Index           =   2
                  Left            =   9120
                  TabIndex        =   23
                  Tag             =   "C�digo Paciente"
                  Top             =   780
                  Visible         =   0   'False
                  Width           =   300
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   4
                  Left            =   1800
                  TabIndex        =   22
                  TabStop         =   0   'False
                  Tag             =   "Apellido 1� Paciente"
                  Top             =   1380
                  Width           =   2000
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   5
                  Left            =   3840
                  TabIndex        =   21
                  TabStop         =   0   'False
                  Tag             =   "Apellido 2� Paciente"
                  Top             =   1380
                  Width           =   2000
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR66OBSERV"
                  Height          =   1170
                  Index           =   18
                  Left            =   -74880
                  MultiLine       =   -1  'True
                  ScrollBars      =   3  'Both
                  TabIndex        =   20
                  Tag             =   "Observaciones"
                  Top             =   600
                  Width           =   9165
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "AD07CODPROCESO"
                  Height          =   330
                  Index           =   59
                  Left            =   6000
                  TabIndex        =   19
                  Tag             =   "Proceso"
                  Top             =   1380
                  Width           =   1500
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "AD01CODASISTENCI"
                  Height          =   330
                  Index           =   60
                  Left            =   7680
                  TabIndex        =   18
                  Tag             =   "Asistencia"
                  Top             =   1380
                  Width           =   1500
               End
               Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
                  DataField       =   "FR66FECFIRMMEDI"
                  Height          =   330
                  Index           =   1
                  Left            =   -69360
                  TabIndex        =   47
                  Tag             =   "Fecha Inicio Vigencia"
                  Top             =   660
                  Width           =   1860
                  _Version        =   65537
                  _ExtentX        =   3281
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DefaultDate     =   ""
                  MinDate         =   "1900/1/1"
                  MaxDate         =   "2100/12/31"
                  Format          =   "DD/MM/YYYY"
                  AllowNullDate   =   -1  'True
                  ShowCentury     =   -1  'True
                  Mask            =   2
                  StartofWeek     =   2
               End
               Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
                  DataField       =   "FR66FECREDACCION"
                  Height          =   330
                  Index           =   2
                  Left            =   -74760
                  TabIndex        =   48
                  Tag             =   "Fecha Redacci�n"
                  Top             =   1380
                  Width           =   1860
                  _Version        =   65537
                  _ExtentX        =   3281
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DefaultDate     =   ""
                  MinDate         =   "1900/1/1"
                  MaxDate         =   "2100/12/31"
                  Format          =   "DD/MM/YYYY"
                  AllowNullDate   =   -1  'True
                  ShowCentury     =   -1  'True
                  Mask            =   2
                  StartofWeek     =   2
               End
               Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
                  DataField       =   "FR66FECVALIDACION"
                  Height          =   330
                  Index           =   0
                  Left            =   -74640
                  TabIndex        =   49
                  Tag             =   "Fecha Validaci�n"
                  Top             =   840
                  Width           =   1860
                  _Version        =   65537
                  _ExtentX        =   3281
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DefaultDate     =   ""
                  MinDate         =   "1900/1/1"
                  MaxDate         =   "2100/12/31"
                  Format          =   "DD/MM/YYYY"
                  AllowNullDate   =   -1  'True
                  ShowCentury     =   -1  'True
                  Mask            =   2
                  StartofWeek     =   2
               End
               Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
                  DataField       =   "FR66FECCIERRE"
                  Height          =   330
                  Index           =   5
                  Left            =   -72120
                  TabIndex        =   50
                  Tag             =   "Fecha Cierre"
                  Top             =   840
                  Width           =   1860
                  _Version        =   65537
                  _ExtentX        =   3281
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DefaultDate     =   ""
                  MinDate         =   "1900/1/1"
                  MaxDate         =   "2100/12/31"
                  Format          =   "DD/MM/YYYY"
                  AllowNullDate   =   -1  'True
                  ShowCentury     =   -1  'True
                  Mask            =   2
                  StartofWeek     =   2
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Departamento de Cargo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   63
                  Left            =   -69720
                  TabIndex        =   205
                  Top             =   420
                  Width           =   2655
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "N� Colegiado"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   -72120
                  TabIndex        =   76
                  Top             =   1140
                  Width           =   1335
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "M�dico que env�a"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   -69720
                  TabIndex        =   75
                  Top             =   1140
                  Width           =   1575
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "M�dico que firma"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   12
                  Left            =   -74760
                  TabIndex        =   74
                  Top             =   1140
                  Width           =   1575
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Sexo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   2
                  Left            =   2520
                  TabIndex        =   73
                  Top             =   420
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Fecha Redacci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   0
                  Left            =   -74760
                  TabIndex        =   72
                  Top             =   1140
                  Width           =   1815
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hora Redacci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   10
                  Left            =   -72840
                  TabIndex        =   71
                  Top             =   1140
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Urgencia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   6
                  Left            =   -70920
                  TabIndex        =   70
                  Top             =   1140
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Servicio"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   1
                  Left            =   -74760
                  TabIndex        =   69
                  Top             =   420
                  Width           =   2055
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Dr."
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   20
                  Left            =   -74760
                  TabIndex        =   68
                  Top             =   420
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Fecha Firma M�dico"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   5
                  Left            =   -69360
                  TabIndex        =   67
                  Top             =   420
                  Width           =   1815
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hora"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   18
                  Left            =   -67320
                  TabIndex        =   66
                  Top             =   420
                  Width           =   495
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Departamento"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   15
                  Left            =   -70920
                  TabIndex        =   65
                  Top             =   420
                  Width           =   1215
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Edad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   42
                  Left            =   1800
                  TabIndex        =   64
                  Top             =   420
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Superficie Corporal"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   41
                  Left            =   7320
                  TabIndex        =   63
                  Top             =   420
                  Width           =   1695
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Altura Cm"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   40
                  Left            =   6240
                  TabIndex        =   62
                  Top             =   420
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Peso"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   39
                  Left            =   5160
                  TabIndex        =   61
                  Top             =   420
                  Width           =   615
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Cama"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   38
                  Left            =   3840
                  TabIndex        =   60
                  Top             =   420
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Historia "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   120
                  TabIndex        =   59
                  Top             =   420
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   7
                  Left            =   120
                  TabIndex        =   58
                  Top             =   1140
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Apellido 1�"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   1800
                  TabIndex        =   57
                  Top             =   1140
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Apellido 2�"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   9
                  Left            =   3840
                  TabIndex        =   56
                  Top             =   1140
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Observaciones"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   24
                  Left            =   -74880
                  TabIndex        =   55
                  Top             =   360
                  Width           =   2655
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Fecha Validaci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   62
                  Left            =   -74640
                  TabIndex        =   54
                  Top             =   600
                  Width           =   1815
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Fecha Cierre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   85
                  Left            =   -72120
                  TabIndex        =   53
                  Top             =   600
                  Width           =   1815
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Proceso"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   86
                  Left            =   6000
                  TabIndex        =   52
                  Top             =   1140
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Asistencia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   87
                  Left            =   7680
                  TabIndex        =   51
                  Top             =   1140
                  Width           =   975
               End
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Estado"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   3120
               TabIndex        =   79
               Top             =   120
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.OM origen PRN"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   1320
               TabIndex        =   78
               Top             =   120
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d. Petici�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   28
               Left            =   120
               TabIndex        =   77
               Top             =   120
               Width           =   1215
            End
         End
      End
      Begin Crystal.CrystalReport CrystalReport1 
         Left            =   10440
         Top             =   660
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   327680
         PrintFileLinesPerPage=   60
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      Caption         =   "V�a"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   26
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   975
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmOMModAsigFM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmOMPRN (FR0610.FRM)                                *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: redactar Orden M�dica                                   *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim intCambioAlgo As Integer
Dim mstrOperacion As String 'saber que tipo de linea estamos creando en el hijo
'mstrOperacion = "/" 'Medicamento
'mstrOperacion = "M" 'Mezcla IV 2M
'mstrOperacion = "P" 'PRN en OM
'mstrOperacion = "F" 'Fluidoterapia
'mstrOperacion = "E" 'Estupefaciente
'mstrOperacion = "L" 'Form.Magistral
Dim blnObligatorio As Boolean
Dim strOrigen As String 'Para saber si el evento change se produce por modificaciones en el campo o no;
                        'vale V cuando se ha modificado manualmente la velocidad de perfusi�n,
                        'y vale T cuando se ha modificado manualmente el tiempo de perfusi�n
Dim blnNoVerAgen As Boolean 'True: No hay que mostrar la agenda
Dim blnInload As Boolean
Dim strBoton As String

Dim codFormMagistral
Dim descFormMagistral As String

Private Function blnErrorFecFin()
  'Devuelte TRUE si alguna de las fechas de fin est� mal
  Dim intNumLin As Integer
  Dim strSql As String
  Dim qrySQL As rdoQuery
  Dim rstSQL As rdoResultset
  Dim mvarBkmrk As Variant
  Dim mintisel As Integer
  Dim blnError As Boolean
  Dim blnCerrarSQL As Boolean
  
  blnCerrarSQL = False
  blnError = False
  strSql = "SELECT TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')-TO_DATE(?,'DD/MM/YYYY') FROM DUAL"
  Set qrySQL = objApp.rdoConnect.CreateQuery("", strSql)
  
  intNumLin = grdDBGrid1(6).Rows
  For mintisel = 0 To intNumLin - 1
    mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
    If Not IsNull(grdDBGrid1(6).Columns("Fin").CellValue(mvarBkmrk)) Then
      'On Error GoTo ErrorFec
      qrySQL(0) = Format(grdDBGrid1(6).Columns("Fin").CellValue(mvarBkmrk), "dd/mm/yyyy")
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        blnCerrarSQL = True
        If rstSQL.rdoColumns(0).Value > 0 Then
          blnError = True
        End If
      Else
        blnError = True
      End If
    End If
  Next mintisel
     
  If blnCerrarSQL Then
    qrySQL.Close
    Set qrySQL = Nothing
    Set rstSQL = Nothing
  End If
  blnErrorFecFin = blnError
  Exit Function
ErrorFec:
  blnError = True
  blnErrorFecFin = blnError
  
End Function

Private Sub Imprimir(strListado As String, intDes As Integer, strCodPRN As String)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  strWhere = "{AD0200.AD02CODDPTO}={FR6601J.AD02CODDPTO} AND " & _
             "{FR6601J.FR66CODPETICION}={FR2800.FR66CODPETICION} AND " & _
             "{FR2800.FR73CODPRODUCTO}={FR7300.FR73CODPRODUCTO} AND " & _
             "{FR6601J.FR66CODPETICION}=" & strCodPRN
             
             
  CrystalReport1.SelectionFormula = strWhere
  'On Error GoTo Err_imp4
  CrystalReport1.Action = 1
Err_imp4:

End Sub


Private Sub chkOMPRN_Click(Index As Integer)

If objWinInfo.intWinStatus = 1 Then
  If chkOMPRN(6).Value = 1 Then
    If chkCheck1(12).Value <> 0 Then
      Call objWinInfo.CtrlSet(chkCheck1(12), 0)
    End If
  Else
    If chkCheck1(12).Value <> 1 Then
      Call objWinInfo.CtrlSet(chkCheck1(12), 1)
    End If
  End If
End If

End Sub





Private Sub cmdAnularPac_Click()
Dim strUpdate As String
Dim mensaje As String
Dim strDelete As String
Dim stra As String
Dim rsta As rdoResultset
Dim PeticionNoAnulable As Boolean

Me.Enabled = False
cmdAnularPac.Enabled = False
Screen.MousePointer = vbHourglass

If IsNumeric(txtText1(0).Text) Then
  PeticionNoAnulable = False
  'todos los procesos seben estar es estado Generado , es dcir,ni iniciado ni terminado
  'porque si no, no se puede anular la petici�n
  stra = "SELECT FR37CODESTOF FROM FR5900 WHERE FR66CODPETICION=" & txtText1(0).Text
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While Not rsta.EOF
    If rsta.rdoColumns("FR37CODESTOF").Value <> 1 Then
      PeticionNoAnulable = True
    End If
  rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  If PeticionNoAnulable = False Then
      strUpdate = "UPDATE FR6600 SET FR26CODESTPETIC=8 WHERE"
      strUpdate = strUpdate & " FR66CODPETICION=" & txtText1(0).Text
      objApp.rdoConnect.Execute strUpdate, 64
      objApp.rdoConnect.Execute "Commit", 64
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      objWinInfo.objWinActiveForm.blnChanged = False
      objWinInfo.DataRefresh
      mensaje = MsgBox("La Petici�n ha sido Anulada", vbInformation, "Farmacia")
      cmdAnularPac.Visible = False
      cmdFMnormalizada.Visible = False
      'hay que borrar los registros de FR5900 y FR2400
      strDelete = "DELETE FR2400 WHERE FR59CODORDFABR IN "
      strDelete = strDelete & " (SELECT FR59CODORDFABR FROM FR5900 WHERE FR66CODPETICION="
      strDelete = strDelete & txtText1(0).Text & ")"
      objApp.rdoConnect.Execute strDelete, 64
      objApp.rdoConnect.Execute "Commit", 64
      strDelete = "DELETE FR5900 WHERE FR66CODPETICION=" & txtText1(0).Text
      objApp.rdoConnect.Execute strDelete, 64
      objApp.rdoConnect.Execute "Commit", 64
  Else
      PeticionNoAnulable = True
      mensaje = MsgBox("La petici�n no puede anularse por tener procesos iniciados o terminados", vbInformation, "Aviso")
  End If
End If

cmdAnularPac.Enabled = True
Screen.MousePointer = vbDefault
Me.Enabled = True
End Sub



Private Sub cmdAnularServ_Click()
Dim strUpdate As String
Dim mensaje As String
Dim strDelete As String
Dim stra As String
Dim rsta As rdoResultset
Dim PeticionNoAnulable As Boolean

Me.Enabled = False
cmdAnularServ.Enabled = False
Screen.MousePointer = vbHourglass

If IsNumeric(txtText1(67).Text) Then
  PeticionNoAnulable = False
  'todos los procesos seben estar es estado Generado , es dcir,ni iniciado ni terminado
  'porque si no, no se puede anular la petici�n
  stra = "SELECT FR37CODESTOF FROM FR5900 WHERE FR55CODNECESUNID=" & txtText1(67).Text
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While Not rsta.EOF
    If rsta.rdoColumns("FR37CODESTOF").Value <> 1 Then
      PeticionNoAnulable = True
    End If
  rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  If PeticionNoAnulable = False Then
      strUpdate = "UPDATE FR5500 SET FR26CODESTPETIC=8 WHERE"
      strUpdate = strUpdate & " FR55CODNECESUNID=" & txtText1(67).Text
      objApp.rdoConnect.Execute strUpdate, 64
      objApp.rdoConnect.Execute "Commit", 64
      Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
      objWinInfo.DataRefresh
      Call MsgBox("La Petici�n ha sido Anulada", vbInformation, "Aviso")
      cmdAnularServ.Visible = False
      cmdFMNormSer.Visible = False
      'hay que borrar los registros de FR5900 y FR2400
      strDelete = "DELETE FR2400 WHERE FR59CODORDFABR IN "
      strDelete = strDelete & " (SELECT FR59CODORDFABR FROM FR5900 WHERE FR55CODNECESUNID="
      strDelete = strDelete & txtText1(67).Text & ")"
      objApp.rdoConnect.Execute strDelete, 64
      objApp.rdoConnect.Execute "Commit", 64
      strDelete = "DELETE FR5900 WHERE FR55CODNECESUNID=" & txtText1(67).Text
      objApp.rdoConnect.Execute strDelete, 64
      objApp.rdoConnect.Execute "Commit", 64
  Else
      PeticionNoAnulable = True
      mensaje = MsgBox("La petici�n no puede anularse por tener procesos iniciados o terminados", vbInformation, "Aviso")
  End If
End If
cmdAnularServ.Enabled = True
Screen.MousePointer = vbDefault
Me.Enabled = True
End Sub

Private Sub cmdExplorador_Click(Index As Integer)
Dim intRetVal
Dim strOpenFileName As String
Dim pathFileName As String
Dim finpath As Integer



If Index = 0 Then
    If txtText1(74).Text = "" Then
      Exit Sub
    End If
    On Error Resume Next
    pathFileName = Dir(txtText1(74).Text)
    finpath = InStr(1, txtText1(74).Text, pathFileName, 1)
    pathFileName = Left(txtText1(74).Text, finpath - 1)
    CommonDialog2.InitDir = pathFileName
    CommonDialog2.filename = txtText1(74).Text
    CommonDialog2.CancelError = True
    CommonDialog2.Filter = "*.*"
    CommonDialog2.ShowOpen
    If Err.Number <> 32755 Then    ' El usuario eligi� Cancelar.
        strOpenFileName = CommonDialog2.filename
        txtText1(74).SetFocus
        Call objWinInfo.CtrlSet(txtText1(74), "")
        txtText1(74).SetFocus
        Call objWinInfo.CtrlSet(txtText1(74), strOpenFileName)
    End If
Else
    If txtText1(57).Text = "" Then
      Exit Sub
    End If
    On Error Resume Next
    pathFileName = Dir(txtText1(57).Text)
    finpath = InStr(1, txtText1(57).Text, pathFileName, 1)
    pathFileName = Left(txtText1(57).Text, finpath - 1)
    CommonDialog1.InitDir = pathFileName
    CommonDialog1.filename = txtText1(57).Text
    CommonDialog1.CancelError = True
    CommonDialog1.Filter = "*.*"
    CommonDialog1.ShowOpen
    If Err.Number <> 32755 Then    ' El usuario eligi� Cancelar.
        strOpenFileName = CommonDialog1.filename
        txtText1(57).SetFocus
        Call objWinInfo.CtrlSet(txtText1(57), "")
        txtText1(57).SetFocus
        Call objWinInfo.CtrlSet(txtText1(57), strOpenFileName)
    End If
End If

End Sub


Private Sub cmdFMnormalizada_Click()
Dim strinsert As String
Dim linea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strUpdate As String

Screen.MousePointer = vbHourglass
cmdFMnormalizada.Enabled = False
If IsNumeric(txtText1(0).Text) And IsNumeric(txtText1(45).Text) Then
  
strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
            txtText1(0).Text & " AND NVL(FR28INDBLOQUEADA,0)=0"
Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
linea = rstlinea.rdoColumns(0).Value + 1
rstlinea.Close
Set rstlinea = Nothing

codFormMagistral = -1
descFormMagistral = ""
'While codFormMagistral = -1
Call objWinInfo_cwForeign("Detalle OM", "txtText1(27)")
'Wend
If codFormMagistral <> -1 Then
  strinsert = "INSERT INTO FR2800(FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO,FR28DOSIS,"
  strinsert = strinsert & "FR93CODUNIMEDIDA,FR28VOLUMEN,FR28TIEMINFMIN,FR34CODVIA,FRG4CODFRECUENCIA,"
  strinsert = strinsert & "FR28INDDISPPRN,FR28INDCOMIENINMED,FRH5CODPERIODICIDAD,FR28INDBLOQUEADA,"
  strinsert = strinsert & "FR28FECBLOQUEO,SG02CODPERSBLOQ,FR28INDSN,FR28INDESTOM,FRH7CODFORMFAR,"
  strinsert = strinsert & "FR28OPERACION,FR28CANTIDAD,FR28FECINICIO,FR28HORAINICIO,FR28FECFIN,"
  strinsert = strinsert & "FR28HORAFIN,FR28INDVIAOPC,FR73CODPRODUCTO_DIL,FR28CANTIDADDIL,"
  strinsert = strinsert & "FR28TIEMMININF,FR28REFERENCIA,FR28CANTDISP,FR28UBICACION,FR28INDPERF,"
  strinsert = strinsert & "FR28INSTRADMIN,FR28VELPERFUSION,FR28VOLTOTAL,FR73CODPRODUCTO_2,"
  strinsert = strinsert & "FRH7CODFORMFAR_2,FR28DOSIS_2,FR93CODUNIMEDIDA_2,FR28DESPRODUCTO,"
  strinsert = strinsert & "FR28PATHFORMAG,FR28VOLUMEN_2,FR28DIASADM,FR28CANTPEDORI" & ")"
  strinsert = strinsert & " "
  strinsert = strinsert & "SELECT " & txtText1(0).Text & "," & linea & "," & codFormMagistral
  strinsert = strinsert & ",FR28DOSIS,"
  strinsert = strinsert & "FR93CODUNIMEDIDA,FR28VOLUMEN,FR28TIEMINFMIN,FR34CODVIA,FRG4CODFRECUENCIA,"
  strinsert = strinsert & "FR28INDDISPPRN,FR28INDCOMIENINMED,FRH5CODPERIODICIDAD,FR28INDBLOQUEADA,"
  strinsert = strinsert & "FR28FECBLOQUEO,SG02CODPERSBLOQ,FR28INDSN,FR28INDESTOM,FRH7CODFORMFAR,"
  strinsert = strinsert & "FR28OPERACION,FR28CANTIDAD,FR28FECINICIO,FR28HORAINICIO,FR28FECFIN,"
  strinsert = strinsert & "FR28HORAFIN,FR28INDVIAOPC,FR73CODPRODUCTO_DIL,FR28CANTIDADDIL,"
  strinsert = strinsert & "FR28TIEMMININF,FR28REFERENCIA,FR28CANTDISP,FR28UBICACION,FR28INDPERF,"
  strinsert = strinsert & "FR28INSTRADMIN,FR28VELPERFUSION,FR28VOLTOTAL,FR73CODPRODUCTO_2,"
  strinsert = strinsert & "FRH7CODFORMFAR_2,FR28DOSIS_2,FR93CODUNIMEDIDA_2,'" & descFormMagistral & "',"
  strinsert = strinsert & "FR28PATHFORMAG,FR28VOLUMEN_2,FR28DIASADM,FR28CANTPEDORI"
  strinsert = strinsert & " FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text & " AND "
  strinsert = strinsert & " FR28NUMLINEA=" & txtText1(45).Text
  objApp.rdoConnect.Execute strinsert, 64
  objApp.rdoConnect.Execute "Commit", 64
  
  strUpdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=-1 WHERE FR66CODPETICION=" & _
              txtText1(0).Text & " AND FR28NUMLINEA=" & txtText1(45).Text
  objApp.rdoConnect.Execute strUpdate, 64
  objApp.rdoConnect.Execute "Commit", 64

  strUpdate = "UPDATE FR6600 SET FR26CODESTPETIC=14 WHERE FR66CODPETICION=" & txtText1(0).Text
  objApp.rdoConnect.Execute strUpdate, 64
  objApp.rdoConnect.Execute "Commit", 64
              
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))

  Call Ordenes_Fabricacion("Paciente")
  cmdFMnormalizada.Enabled = False
Else
  cmdFMnormalizada.Enabled = True
End If
Else
  cmdFMnormalizada.Enabled = True
End If
Screen.MousePointer = vbDefault

End Sub


Private Sub cmdFMNormSer_Click()
Dim strinsert As String
Dim linea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strUpdate As String
Dim stra As String
Dim rsta As rdoResultset

Screen.MousePointer = vbHourglass
cmdFMNormSer.Enabled = False
If IsNumeric(txtText1(67).Text) And IsNumeric(txtText1(71).Text) Then
  
  strlinea = "SELECT MAX(FR20NUMLINEA) FROM FR2000 WHERE FR55CODNECESUNID=" & _
              txtText1(67).Text & " AND NVL(FR20INDBLOQUEO,0)=0"
  Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
  linea = rstlinea.rdoColumns(0).Value + 1
  rstlinea.Close
  Set rstlinea = Nothing
  
  codFormMagistral = -1
  descFormMagistral = ""
  Call objWinInfo_cwForeign("Detalle Necesidad", "txtText1(70)")
  'se mira si se ha seleccionado una FM o no se ha seleccionado ninguna
  If codFormMagistral <> -1 Then
      strinsert = "INSERT INTO FR2000(FR55CODNECESUNID,FR20NUMLINEA,FR73CODPRODUCTO,FR20DESFORMAG,"
      strinsert = strinsert & "FR93CODUNIMEDIDA,FR20CANTNECESQUIR,FR20INDBLOQUEO,FR20INDMOD,FR20PATHFORMAG"
      strinsert = strinsert & ")"
      strinsert = strinsert & " "
      strinsert = strinsert & "SELECT " & txtText1(67).Text & "," & linea & "," & codFormMagistral
      strinsert = strinsert & ",'" & descFormMagistral & "',"
      strinsert = strinsert & "FR93CODUNIMEDIDA,FR20CANTNECESQUIR,FR20INDBLOQUEO,FR20INDMOD,FR20PATHFORMAG"
      strinsert = strinsert & " FROM FR2000 WHERE FR55CODNECESUNID=" & txtText1(67).Text & " AND "
      strinsert = strinsert & " FR20NUMLINEA=" & txtText1(71).Text
      objApp.rdoConnect.Execute strinsert, 64
      objApp.rdoConnect.Execute "Commit", 64
      
      strUpdate = "UPDATE FR2000 SET FR20INDBLOQUEO=-1 WHERE FR55CODNECESUNID=" & _
                  txtText1(67).Text & " AND FR20NUMLINEA=" & txtText1(71).Text
      objApp.rdoConnect.Execute strUpdate, 64
      objApp.rdoConnect.Execute "Commit", 64
    
      strUpdate = "UPDATE FR5500 SET FR26CODESTPETIC=14 WHERE FR55CODNECESUNID=" & txtText1(67).Text
      objApp.rdoConnect.Execute strUpdate, 64
      objApp.rdoConnect.Execute "Commit", 64
                  
      Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      
      Call Ordenes_Fabricacion("Servicio")
      cmdFMNormSer.Enabled = False
  Else
      cmdFMNormSer.Enabled = True
  End If
Else
  cmdFMNormSer.Enabled = True
End If

Screen.MousePointer = vbDefault

End Sub

Private Sub cmdWord_Click()
Dim strpathword As String
Dim strPathW As String
Dim rstaPathW As rdoResultset
Dim strUbicacion As String
Dim strDocumUbicacion As String
Dim strplantilla As String
Dim parametroubicacionfichero As Integer
Dim parametroubicacionplantilla As Integer

Dim wword As Word.Application
  
On Error GoTo error_shell
If txtText1(27).Text = "" Then
  parametroubicacionfichero = 36
  parametroubicacionplantilla = 37
Else
  parametroubicacionfichero = 34
  parametroubicacionplantilla = 35
End If
  
  strPathW = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=" & parametroubicacionfichero
  Set rstaPathW = objApp.rdoConnect.OpenResultset(strPathW)
  If Not rstaPathW.EOF Then
    strUbicacion = rstaPathW(0).Value
  End If
  rstaPathW.Close
  Set rstaPathW = Nothing
  
  strPathW = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=" & parametroubicacionplantilla
  Set rstaPathW = objApp.rdoConnect.OpenResultset(strPathW)
  If Not rstaPathW.EOF Then
    strplantilla = rstaPathW(0).Value
  End If
  rstaPathW.Close
  Set rstaPathW = Nothing
  
  strDocumUbicacion = strUbicacion & txtText1(57).Text
  
  If txtText1(57).Text <> "" Then
      Set wword = New Word.Application
      If Dir(strDocumUbicacion) = "" Then
        wword.Documents.Add strplantilla
        wword.ActiveDocument.SaveAs strDocumUbicacion
      Else
        wword.Documents.Open strDocumUbicacion
      End If
      wword.Visible = True
  End If
  
error_shell:

  If Err.Number <> 0 Then
    MsgBox "Error N�mero: " & Err.Number & Chr(13) & Err.Description, vbCritical
  End If

End Sub

Private Sub cmdWord2_Click()


Dim strPathW As String
Dim rstaPathW As rdoResultset
Dim strplantilla As String
Dim strDocumUbicacion As String
Dim strUbicacion As String
Dim parametroubicacionfichero As Integer
Dim parametroubicacionplantilla As Integer

Dim wword As Word.Application
  
On Error GoTo error_shell
If txtText1(70).Text = "999999999" Then
  parametroubicacionfichero = 36
  parametroubicacionplantilla = 37
Else
  parametroubicacionfichero = 34
  parametroubicacionplantilla = 35
End If
  
  strPathW = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=" & parametroubicacionfichero
  Set rstaPathW = objApp.rdoConnect.OpenResultset(strPathW)
  If Not rstaPathW.EOF Then
    strUbicacion = rstaPathW(0).Value
  End If
  rstaPathW.Close
  Set rstaPathW = Nothing
  
  strPathW = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=" & parametroubicacionplantilla
  Set rstaPathW = objApp.rdoConnect.OpenResultset(strPathW)
  If Not rstaPathW.EOF Then
    strplantilla = rstaPathW(0).Value
  End If
  rstaPathW.Close
  Set rstaPathW = Nothing
  
  strDocumUbicacion = strUbicacion & txtText1(74).Text
  
  If txtText1(74).Text <> "" Then
      Set wword = New Word.Application
      If Dir(strDocumUbicacion) = "" Then
        wword.Documents.Add strplantilla
        wword.ActiveDocument.SaveAs strDocumUbicacion
      Else
        wword.Documents.Open strDocumUbicacion
      End If
      wword.Visible = True
  End If
  
error_shell:

  If Err.Number <> 0 Then
    MsgBox "Error N�mero: " & Err.Number & Chr(13) & Err.Description, vbCritical
  End If

End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
If strFormName = "OM" And strCtrlName = "txtText1(60)" Then
    aValues(2) = txtText1(59).Text  'proceso
End If
If strFormName = "Necesidad Unidad" And strCtrlName = "txtText1(62)" Then
    aValues(2) = txtText1(64).Text  'DPTO
End If
End Sub






Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If

End Sub




Private Sub Form_Activate()

If blnInload = True And (gstrLlamador = "FrmVerPteFab.Pac" Or gstrLlamador = "FrmVerPteFab.Pac.Ver") Then
    Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
    grdDBGrid1(6).Columns("Medicamento").Position = 1
    'grdDBGrid1(6).Columns("FF").Position = 2
    grdDBGrid1(6).Columns("Dosis").Position = 3
    grdDBGrid1(6).Columns("UM").Position = 4
    grdDBGrid1(6).Columns("Volum.").Position = 5
    grdDBGrid1(6).Columns("Cantidad").Position = 6
    grdDBGrid1(6).Columns("Frec.").Position = 7
    grdDBGrid1(6).Columns("V�a").Position = 8
    grdDBGrid1(6).Columns("Period.").Position = 9
    grdDBGrid1(6).Columns("Inicio").Position = 10
    grdDBGrid1(6).Columns("H.I.").Position = 11
    grdDBGrid1(6).Columns("Descripci�n Producto Diluir").Position = 12
    grdDBGrid1(6).Columns("Vol.Dil.").Position = 13
    grdDBGrid1(6).Columns("T.Inf.(min)").Position = 14
    grdDBGrid1(6).Columns("Inst.Admin.").Position = 15
    grdDBGrid1(6).Columns("Fin").Position = 16
    grdDBGrid1(6).Columns("H.F.").Position = 17
    grdDBGrid1(6).Columns("Perfusi�n").Position = 18
    grdDBGrid1(6).Columns("Vol.Total").Position = 19
    grdDBGrid1(6).Columns("STAT").Position = 20
    grdDBGrid1(6).Columns("PERF").Position = 21
    grdDBGrid1(6).Columns("Medicamento 2").Position = 22
    grdDBGrid1(6).Columns("FF2").Position = 23
    grdDBGrid1(6).Columns("Dosis2").Position = 24
    grdDBGrid1(6).Columns("UMP.").Position = 25
    grdDBGrid1(6).Columns("F�rmula Magistral").Position = 26
    grdDBGrid1(6).Columns("Arch.Inf.F.M.").Position = 27
    grdDBGrid1(6).Columns("C�digo Medicamento").Visible = False
    grdDBGrid1(6).Columns("Medicamento").Width = 2235
    grdDBGrid1(6).Columns("F�rmula Magistral").Width = 2235
    grdDBGrid1(6).Columns("Arch.Inf.F.M.").Width = 2235
    'grdDBGrid1(6).Columns("FF").Width = 390
    grdDBGrid1(6).Columns("Dosis").Width = 945
    grdDBGrid1(6).Columns("UM").Width = 510
    grdDBGrid1(6).Columns("Cantidad").Width = 915
    grdDBGrid1(6).Columns("Frec.").Width = 929
    grdDBGrid1(6).Columns("V�a").Width = 600
    grdDBGrid1(6).Columns("Period.").Width = 1065
    grdDBGrid1(6).Columns("Inicio").Width = 1095
    grdDBGrid1(6).Columns("H.I.").Width = 540
    grdDBGrid1(6).Columns("C�digo Producto diluir").Visible = False
    grdDBGrid1(6).Columns("Descripci�n Producto Diluir").Width = 2160
    grdDBGrid1(6).Columns("Vol.Dil.").Width = 825
    grdDBGrid1(6).Columns("Inst.Admin.").Width = 2500
    grdDBGrid1(6).Columns("C�digo Medicamento 2").Visible = False
    grdDBGrid1(6).Columns("Medicamento 2").Width = 1815
    grdDBGrid1(6).Columns("FF2").Width = 390
    grdDBGrid1(6).Columns("Dosis2").Width = 1289
    grdDBGrid1(6).Columns("UMP.").Width = 645
    grdDBGrid1(6).Columns("Fin").Width = 1095
    grdDBGrid1(6).Columns("H.F.").Width = 540
    grdDBGrid1(6).Columns("Perfusi�n").Width = 1170
    grdDBGrid1(6).Columns("Vol.Total").Width = 1170
    grdDBGrid1(6).Columns("STAT").Width = 525
    grdDBGrid1(6).Columns("PERF").Width = 645
    grdDBGrid1(6).Columns("C�digo Petici�n").Visible = False
    grdDBGrid1(6).Columns("Num.Linea").Visible = False
    grdDBGrid1(6).Columns("Cambiar V�a?").Visible = False
    grdDBGrid1(6).Columns("Prn?").Visible = False
    grdDBGrid1(6).Columns("T.Inf.(min)").Width = 825
    grdDBGrid1(6).Columns("Operaci�n").Visible = False
    grdDBGrid1(6).Columns("Ubicaci�n").Visible = False
    grdDBGrid1(6).Columns("Referencia").Width = 1100
    grdDBGrid1(6).Columns("Referencia").Visible = False
    grdDBGrid1(6).Columns("Volum.").Width = 825
    grdDBGrid1(6).Columns("Cant.Dispens.").Visible = False
  Me.Enabled = True

  blnInload = False
End If
If objWinInfo.objWinActiveForm.strName = "Necesidad Unidad" Then
  Call objWinInfo.DataRefresh
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim objDetailInfo As New clsCWForm
Dim objMasterInfo2 As New clsCWForm
Dim objMultiInfo2 As New clsCWForm
Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "OM"
    
    .strTable = "FR6600"
    
    Call .FormAddOrderField("FR66CODPETICION", cwDescending)
    
    .intAllowance = cwAllowReadOnly
    
    If gstrLlamador = "FrmVerPteFab.Pac" Then
      .strWhere = " FR66CODPETICION=" & gcodigopeticion
      SSTab1.TabVisible(1) = False
    ElseIf gstrLlamador = "FrmVerPteFab.Pac.Ver" Then
      .strWhere = " FR66CODPETICION=" & gcodigopeticion
      SSTab1.TabVisible(1) = False
      cmdFMnormalizada.Enabled = False
    Else
      .strWhere = " -1=0 "
    End If
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "OM")
    Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "C�d. Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMENF", "Fecha Firma Enfermera", cwDate)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�d. M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma M�dico", cwDate)
    Call .FormAddFilterWhere(strKey, "FR66FECVALIDACION", "Fecha Validaci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR66FECCIERRE", "Fecha Cierre", cwDate)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR66INDOM", "Orden M�dica?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDPACIDIABET", "Paciente Diab�tico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDRESTVOLUM", "Restricci�n Volumen?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDINTERCIENT", "Inter�s Cient�fico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR66INDALERGIA", "Alergia?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�digo Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
  End With

  With objDetailInfo
    .strName = "Detalle OM"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(6)
    .strTable = "FR2800" 'Perfil FTP
    .blnAskPrimary = False
    
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    Call .FormAddRelation("FR66CODPETICION", txtText1(0))
    
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Medicamentos")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR28CANTIDAD", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "Forma Farmace�tica", cwString)
    Call .FormAddFilterWhere(strKey, "FR28DOSIS", "Dosis", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "Unidad Medida", cwString)
    Call .FormAddFilterWhere(strKey, "frg4codfrecuencia", "Frecuencia", cwString)
    Call .FormAddFilterWhere(strKey, "fr34codvia", "V�a", cwString)
    Call .FormAddFilterWhere(strKey, "frh5codperiodicidad", "Periodicidad", cwString)
    Call .FormAddFilterWhere(strKey, "fr28fecinicio", "Fecha Inicio", cwDate)
    Call .FormAddFilterWhere(strKey, "fr28horainicio", "Hora Inicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "fr28fecfin", "Fecha Fin", cwDate)
    Call .FormAddFilterWhere(strKey, "fr28horafin", "Hora Fin", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO_DIL", "Cod.Producto Diluy.", cwNumeric)
    Call .FormAddFilterWhere(strKey, "fr28indcomieninmed", "Comienzo Inmediato?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "fr28indsn", "S.N?", cwBoolean)
  End With

  With objMasterInfo2
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(2)
    Set .grdGrid = grdDBGrid1(1)
    
    .strName = "Necesidad Unidad"
    .blnAskPrimary = False
      
    .strTable = "FR5500"
    .intAllowance = cwAllowReadOnly
    
    If gstrLlamador = "FrmVerPteFab.Ser" Then
      .strWhere = " FR55CODNECESUNID=" & gcodigopeticion
      SSTab1.TabVisible(0) = False
    ElseIf gstrLlamador = "FrmVerPteFab.Ser.Ver" Then
      cmdFMNormSer.Enabled = False
      .strWhere = " FR55CODNECESUNID=" & gcodigopeticion
      SSTab1.TabVisible(0) = False
    Else
      .strWhere = " -1=0 "
    End If
    
    Call .FormAddOrderField("FR55FECPETICION", cwDescending)
    Call .FormAddOrderField("FR55CODNECESUNID", cwDescending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Necesidad Unidad")
    Call .FormAddFilterWhere(strKey, "FR55CODNECESUNID", "C�d. Necesidad Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�d. Peticionario", cwString)
    Call .FormAddFilterWhere(strKey, "FR55FECPETICION", "Fecha Petici�n", cwDate)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
    Call .FormAddFilterOrder(strKey, "FR55CODNECESUNID", "C�digo Necesidad Unidad")
  
  End With

  With objMultiInfo2
      .strName = "Detalle Necesidad"
      Set .objFormContainer = fraFrame1(3)
      Set .objFatherContainer = fraFrame1(2)
      Set .tabMainTab = tabTab1(3)
      Set .grdGrid = grdDBGrid1(0)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
      
      .strTable = "FR2000"
      
      .intAllowance = cwAllowReadOnly
      
      .blnAskPrimary = False
      
      Call .FormAddOrderField("FR20NUMLINEA", cwAscending)
      
      strKey = .strDataBase & .strTable
      
      'Se establecen los campos por los que se puede filtrar
      'Call .FormCreateFilterWhere(strKey, "Necesidades")
      'Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d. Producto", cwNumeric)
      'Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida", cwString)
      'Call .FormAddFilterWhere(strKey, "FR20CANTNECESQUIR", "Cantidad", cwNumeric)
      
      'Se establecen los campos por los que se puede ordenar con el filtro
      'Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�d. Producto")
      'Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida")
      'Call .FormAddFilterOrder(strKey, "FR20CANTNECESQUIR", "Cantidad")
      
      Call .FormAddRelation("FR55CODNECESUNID", txtText1(67))
  End With

  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    Call .FormAddInfo(objMasterInfo2, cwFormDetail)
    Call .FormAddInfo(objMultiInfo2, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    Call .FormCreateInfo(objMasterInfo2)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(txtText1(25)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(5)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(12)).blnInFind = True
    .CtrlGetInfo(chkCheck1(15)).blnInFind = True
    
    'productos
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    '.CtrlGetInfo(txtText1(35)).blnInFind = True
    .CtrlGetInfo(txtText1(30)).blnInFind = True
    .CtrlGetInfo(txtText1(31)).blnInFind = True
    .CtrlGetInfo(txtText1(38)).blnInFind = True
    .CtrlGetInfo(txtText1(28)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(3)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(37)).blnInFind = True
    .CtrlGetInfo(txtText1(41)).blnInFind = True
    .CtrlGetInfo(txtText1(42)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(4)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(42)).blnInFind = True
    .CtrlGetInfo(txtText1(36)).blnInFind = True
    
    .CtrlGetInfo(Text1).blnNegotiated = False
    .CtrlGetInfo(txtText1(29)).blnNegotiated = False
    .CtrlGetInfo(txtHoras).blnNegotiated = False
    .CtrlGetInfo(txtMinutos).blnNegotiated = False
    
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True
    .CtrlGetInfo(txtText1(29)).blnReadOnly = True
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True
    .CtrlGetInfo(txtText1(14)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(5)).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(59)).blnReadOnly = True
    .CtrlGetInfo(txtText1(60)).blnReadOnly = True
    
    '.CtrlGetInfo(txtOperacion).blnNegotiated = False
     .CtrlGetInfo(Text1).blnReadOnly = True
     .CtrlGetInfo(txtText1(57)).blnReadOnly = True
     .CtrlGetInfo(txtText1(74)).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(3)).blnNegotiated = False
    .CtrlGetInfo(txtText1(3)).blnReadOnly = True
    .CtrlGetInfo(txtText1(4)).blnNegotiated = False
    .CtrlGetInfo(txtText1(4)).blnReadOnly = True
    .CtrlGetInfo(txtText1(5)).blnNegotiated = False
    .CtrlGetInfo(txtText1(5)).blnReadOnly = True
    .CtrlGetInfo(txtText1(23)).blnNegotiated = False
    .CtrlGetInfo(txtText1(23)).blnReadOnly = True
    .CtrlGetInfo(txtText1(7)).blnNegotiated = False
    .CtrlGetInfo(txtText1(7)).blnReadOnly = True
    .CtrlGetInfo(txtText1(6)).blnNegotiated = False
    .CtrlGetInfo(txtText1(6)).blnReadOnly = True
    .CtrlGetInfo(txtText1(19)).blnNegotiated = False
    .CtrlGetInfo(txtText1(19)).blnReadOnly = True
    .CtrlGetInfo(txtText1(21)).blnNegotiated = False
    .CtrlGetInfo(txtText1(21)).blnReadOnly = True
    .CtrlGetInfo(txtText1(15)).blnNegotiated = False
    .CtrlGetInfo(txtText1(15)).blnReadOnly = True
    .CtrlGetInfo(txtText1(26)).blnNegotiated = False
    .CtrlGetInfo(txtText1(26)).blnReadOnly = True
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA= ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "CI22NOMBRE")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(4), "CI22PRIAPEL")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(5), "CI22SEGAPEL")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(23), "CI22NUMHISTORIA")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(7), "CI30CODSEXO")
    
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(60)), "AD01CODASISTENCI", "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE AD01CODASISTENCI= ? AND AD07CODPROCESO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(60)), txtText1(46), "GCFN06(AD15CODCAMA)")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "CI30CODSEXO", "SELECT * FROM CI3000 WHERE CI30CODSEXO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(6), "CI30DESSEXO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "SG02COD_MED", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "SG02APE1")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(24)), "SG02COD_FEN", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(19), "SG02APE1")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(21), "SG02NUMCOLEGIADO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "SG02COD_ENF", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(15), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(22)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(22)), txtText1(11), "FR91DESURGENCIA")
    
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(78)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(78)), txtText1(77), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(79)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(79)), txtText1(80), "AD02DESDPTO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(26), "FR26DESESTADOPET")
    '**********************************************************************************
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(27)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(27)), txtText1(13), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(40)), "FR73CODPRODUCTO_DIL", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(40)), txtText1(41), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(52)), "FR73CODPRODUCTO_2", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(52)), txtText1(51), "FR73DESPRODUCTO")
    .CtrlGetInfo(txtText1(47)).blnReadOnly = True
    
    '.CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    .CtrlGetInfo(txtText1(16)).blnForeign = True
    .CtrlGetInfo(txtText1(22)).blnForeign = True
    '.CtrlGetInfo(txtText1(25)).blnForeign = True
        
    .CtrlGetInfo(txtText1(27)).blnForeign = True
    '.CtrlGetInfo(txtText1(40)).blnForeign = True
    .CtrlGetInfo(txtText1(31)).blnForeign = True
    '.CtrlGetInfo(txtText1(81)).blnForeign = True
    '.CtrlGetInfo(txtText1(80)).blnForeign = True
    '.CtrlGetInfo(txtText1(35)).blnForeign = True
    
    .CtrlGetInfo(txtText1(48)).blnForeign = True
    .CtrlGetInfo(txtText1(49)).blnForeign = True
     
    .CtrlGetInfo(txtText1(10)).blnReadOnly = True
    .CtrlGetInfo(txtText1(20)).blnReadOnly = True
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True
    .CtrlGetInfo(txtText1(25)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = True
    .CtrlGetInfo(txtText1(44)).blnReadOnly = True
    
    .CtrlGetInfo(dtcDateCombo1(3)).blnMandatory = True
    .CtrlGetInfo(txtText1(37)).blnMandatory = True
    .CtrlGetInfo(txtText1(55)).blnMandatory = True
    '.CtrlGetInfo(txtText1(35)).blnMandatory = True
    .CtrlGetInfo(txtText1(30)).blnMandatory = True
    .CtrlGetInfo(txtText1(31)).blnMandatory = True
    .CtrlGetInfo(txtText1(28)).blnMandatory = True
    '.CtrlGetInfo(txtText1(57)).blnMandatory = True
    .CtrlGetInfo(txtText1(49)).blnMandatory = True
    
    .CtrlGetInfo(cboDBCombo1(0)).blnMandatory = True
    .CtrlGetInfo(cboDBCombo1(1)).blnMandatory = True
    .CtrlGetInfo(cboDBCombo1(3)).blnMandatory = True
    
    .CtrlGetInfo(txtText1(38)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(35)).blnReadOnly = True 'FF
    
    .CtrlGetInfo(txtText1(59)).blnInGrid = False 'Proceso
    .CtrlGetInfo(txtText1(60)).blnInGrid = False 'Asistencia
    
    .CtrlGetInfo(txtText1(35)).blnInGrid = False 'FF
    
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(1)).strSql = "SELECT FR34CODVIA,FR34DESVIA FROM FR3400 ORDER BY FR34CODVIA"
    
    .CtrlGetInfo(cboDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(0)).strSql = "SELECT FRG4CODFRECUENCIA,FRG4DESFRECUENCIA FROM FRG400 ORDER BY FRG4CODFRECUENCIA"
    
    .CtrlGetInfo(cboDBCombo1(3)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(3)).strSql = "SELECT FRH5CODPERIODICIDAD,FRH5DESPERIODICIDAD FROM FRH500 WHERE FRH5CODPERIODICIDAD<>'Agenda' ORDER BY FRH5CODPERIODICIDAD"
    
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(65)), "SG02COD_PDS", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(65)), txtText1(63), "SG02APE1")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(64)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(64)), txtText1(66), "AD02DESDPTO")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(68)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(68)), txtText1(69), "FR26DESESTADOPET")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(62)), "AD41CODSECCION", "SELECT AD41DESSECCION FROM AD4100 WHERE AD41CODSECCION= ? AND AD02CODDPTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(62)), txtText1(61), "AD41DESSECCION")

    .CtrlGetInfo(txtText1(70)).blnForeign = True

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

  intCambioAlgo = 0
  mstrOperacion = "L"
  blnInload = True
  strBoton = ""

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
Dim strDelete As String
Dim stra As String
Dim rsta As rdoResultset

  intCancel = objWinInfo.WinExit
  If intCancel = 0 Then
    If IsNumeric(txtText1(0).Text) Then
        stra = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        If rsta.EOF Then
            strDelete = "DELETE FR6600 WHERE FR66CODPETICION=" & txtText1(0).Text
            objApp.rdoConnect.Execute strDelete, 64
            objApp.rdoConnect.Execute "Commit", 64
        End If
        rsta.Close
        Set rsta = Nothing
    End If
  End If

End Sub

Private Sub Form_Unload(intCancel As Integer)
Dim strDelete As String
Dim stra As String
Dim rsta As rdoResultset

If IsNumeric(txtText1(0).Text) Then
    stra = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.EOF Then
        strDelete = "DELETE FR6600 WHERE FR66CODPETICION=" & txtText1(0).Text
        objApp.rdoConnect.Execute strDelete, 64
        objApp.rdoConnect.Execute "Commit", 64
    End If
    rsta.Close
    Set rsta = Nothing
End If
gintfirmarOM = 0
Call objWinInfo.WinDeRegister
Call objWinInfo.WinRemoveInfo

End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
Dim v As Integer

  If Index = 6 Then
    v = 39
    Select Case grdDBGrid1(6).Columns("Operaci�n").Value
    Case "/"  'Medicamento
      For i = 0 To v
        grdDBGrid1(6).Columns(i).CellStyleSet "Medicamentos"
      Next i
    Case "M"  'Mezcla IV 2M
      For i = 0 To v
        grdDBGrid1(6).Columns(i).CellStyleSet "MezclaIV2M"
      Next i
    Case "P"  'PRN en OM
      For i = 0 To v
        grdDBGrid1(6).Columns(i).CellStyleSet "PRNenOM"
      Next i
    Case "F" 'Fluidoterapia
      For i = 0 To v
        grdDBGrid1(6).Columns(i).CellStyleSet "Fluidoterapia"
      Next i
    'Case "E" 'Estupefaciente
    '  For i = 0 To v
    '    grdDBGrid1(6).Columns(i).CellStyleSet "Estupefacientes"
    '  Next i
    Case "L" 'Form.Magistral
      For i = 0 To v
        grdDBGrid1(6).Columns(i).CellStyleSet "FormMagistral"
      Next i
    End Select
  End If
  
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strFormName = "OM" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR2200J" 'pacientes con cama ocupada

     Set objField = .AddField("CI21CODPERSONA")
     objField.strSmallDesc = "C�digo Paciente"

     Set objField = .AddField("CI22NOMBRE")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("CI22NUMHISTORIA")
     objField.strSmallDesc = "Historia"
     
     Set objField = .AddField("CI22PRIAPEL")
     objField.strSmallDesc = "Apellido 1�"
     
     Set objField = .AddField("CI22SEGAPEL")
     objField.strSmallDesc = "Apellido 2�"

     Set objField = .AddField("DESCCAMA")
     objField.strSmallDesc = "Cama"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("CI21CODPERSONA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "OM" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "sg0200"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo M�dico"
     
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Primer Apellido"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "OM" And strCtrl = "txtText1(16)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02FECINICIO<TRUNC(SYSDATE) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>SYSDATE))"

     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Dpto."

     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Dpto."

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(16), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "OM" And strCtrl = "txtText1(22)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9100"

     Set objField = .AddField("FR91CODURGENCIA")
     objField.strSmallDesc = "C�digo Urgencia"

     Set objField = .AddField("FR91DESURGENCIA")
     objField.strSmallDesc = "Descripci�n Urgencia"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(22), .cllValues("FR91CODURGENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(27)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strWhere = " WHERE FR73INDFABRICABLE=-1 "
     .strWhere = .strWhere & " AND TRUNC(SYSDATE) BETWEEN TRUNC(FR73FECINIVIG) AND NVL(TRUNC(FR73FECFINVIG),TO_DATE('31/12/9999','DD/MM/YYYY'))"
     
     .strOrder = " ORDER BY FR73DESPRODUCTO"

     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Medicamento"
     objField.blnInDialog = False
     objField.blnInGrid = False
 
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo Interno"
     
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n Medicamento"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farm."
     
     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"
     
     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Uni.Medida"
     
     Set objField = .AddField("FR34CODVIA")
     objField.strSmallDesc = "V�a"
     
     Set objField = .AddField("FRG4CODFRECUENCIA_USU")
     objField.strSmallDesc = "Frecuencia"
     
     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"
     
     Set objField = .AddField("FR00CODGRPTERAP")
     objField.strSmallDesc = "C�d.Grupo Terape�tico"
 
     If .Search Then
      'Call objWinInfo.CtrlSet(txtText1(27), .cllValues("FR73CODPRODUCTO"))
      'Call objWinInfo.CtrlSet(txtText1(55), .cllValues("FR73DESPRODUCTO"))
      'Call objWinInfo.CtrlSet(txtText1(30), .cllValues("FR73DOSIS"))
      'Call objWinInfo.CtrlSet(txtText1(31), .cllValues("FR93CODUNIMEDIDA"))
      'Call objWinInfo.CtrlSet(cboDBCombo1(1), .cllValues("FR34CODVIA"))
      'Call objWinInfo.CtrlSet(cboDBCombo1(0), .cllValues("FRG4CODFRECUENCIA_USU"))
      codFormMagistral = .cllValues("FR73CODPRODUCTO")
      descFormMagistral = .cllValues("FR73DESPRODUCTO")
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(41)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     '.strWhere = " WHERE FR73INDPRODREC=-1"
     .strWhere = " WHERE FR73INDINFIV=-1"
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Medicamento"
 
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
    
     Set objField = .AddField("FR73VOLUMEN")
     objField.strSmallDesc = "Volumen"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     'Set objField = .AddField("FR73VOLINFIV")
     'objField.strSmallDesc = "Volumen Infusi�n IV"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(40), .cllValues("FR73CODPRODUCTO"))
      'Call objWinInfo.CtrlSet(txtText1(42), .cllValues("FR73VOLINFIV"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(31)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad de Medida"
     
     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(31), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(49)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad de Medida"
     
     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(49), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).Forma Farmace�tica" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"

     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"
     
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(18), .cllValues("FRH7CODFORMFAR"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(35)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"
 
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"
 
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"
 
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(35), .cllValues("FRH7CODFORMFAR"))
     End If
    End With
    Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "txtText1(48)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"
 
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"
 
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"
 
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(48), .cllValues("FRH7CODFORMFAR"))
     End If
    End With
    Set objSearch = Nothing
 End If

 If strFormName = "Detalle Necesidad" And strCtrl = "txtText1(70)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strWhere = " WHERE FR73INDFABRICABLE=-1 "
     .strWhere = .strWhere & " AND TRUNC(SYSDATE) BETWEEN TRUNC(FR73FECINIVIG) AND NVL(TRUNC(FR73FECFINVIG),TO_DATE('31/12/9999','DD/MM/YYYY'))"
     
     .strOrder = " ORDER BY FR73DESPRODUCTO"

     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Medicamento"
     objField.blnInDialog = False
     objField.blnInGrid = False
 
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo Interno"
     
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n Medicamento"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farm."
     
     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"
     
     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Uni.Medida"
     
     Set objField = .AddField("FR34CODVIA")
     objField.strSmallDesc = "V�a"
     
     Set objField = .AddField("FRG4CODFRECUENCIA_USU")
     objField.strSmallDesc = "Frecuencia"
     
     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"
     
     Set objField = .AddField("FR00CODGRPTERAP")
     objField.strSmallDesc = "C�d.Grupo Terape�tico"
 
     If .Search Then
      codFormMagistral = .cllValues("FR73CODPRODUCTO")
      descFormMagistral = .cllValues("FR73DESPRODUCTO")
     End If
   End With
   Set objSearch = Nothing
 End If

End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'txtOperacion.Text = cboDBCombo1(4).Text
'On Error GoTo Error
  
  If strFormName = "Detalle OM" Then
    If chkCheck1(5).Value = 1 Then
      cboDBCombo1(1).Enabled = True
      objWinInfo.CtrlGetInfo(cboDBCombo1(1)).blnReadOnly = False
    End If
  objWinInfo.CtrlGetInfo(txtText1(82)).blnReadOnly = False
  End If
  
  If objWinInfo.objWinActiveForm.strName = "OM" Then
    If objWinInfo.intWinStatus = 1 Then
      If Not objWinInfo.objWinActiveForm.rdoCursor.EOF Then
        If objWinInfo.objWinActiveForm.rdoCursor("FR66INDOM").Value = 0 Then
        chkOMPRN(6).Value = 1
        End If
      End If
    End If
  End If

  If strFormName = "Detalle OM" Then
    'Campos Linkados
    Dim srtSQL As String
    Dim qrySQL As rdoQuery
    Dim rstSQL As rdoResultset
    
    txtText1(3).Text = ""
    txtText1(4).Text = ""
    txtText1(5).Text = ""
    txtText1(23).Text = ""
    txtText1(7).Text = ""
    If txtText1(2).Text <> "" Then
      srtSQL = "SELECT * FROM CI2200 WHERE CI21CODPERSONA= ?"
      Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
      qrySQL(0) = txtText1(2).Text
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        If Not IsNull(rstSQL("CI22NOMBRE").Value) Then
          txtText1(3).Text = rstSQL("CI22NOMBRE").Value
        End If
        If Not IsNull(rstSQL("CI22PRIAPEL").Value) Then
          txtText1(4).Text = rstSQL("CI22PRIAPEL").Value
        End If
        If Not IsNull(rstSQL("CI22SEGAPEL").Value) Then
          txtText1(5).Text = rstSQL("CI22SEGAPEL").Value
        End If
        If Not IsNull(rstSQL("CI22NUMHISTORIA").Value) Then
          txtText1(23).Text = rstSQL("CI22NUMHISTORIA").Value
        End If
        If Not IsNull(rstSQL("CI30CODSEXO").Value) Then
          txtText1(7).Text = rstSQL("CI30CODSEXO").Value
        End If
      End If
      rstSQL.Close
      Set rstSQL = Nothing
      qrySQL.Close
      Set qrySQL = Nothing
    End If
    txtText1(6).Text = ""
    If txtText1(7).Text <> "" Then
      srtSQL = "SELECT * FROM CI3000 WHERE CI30CODSEXO = ?"
      Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
      qrySQL(0) = txtText1(7).Text
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        If Not IsNull(rstSQL("CI30DESSEXO").Value) Then
          txtText1(6).Text = rstSQL("CI30DESSEXO").Value
        End If
      End If
      rstSQL.Close
      Set rstSQL = Nothing
      qrySQL.Close
      Set qrySQL = Nothing
    End If
    txtText1(19).Text = ""
    txtText1(21).Text = ""
    If txtText1(24).Text <> "" Then
      srtSQL = "SELECT * FROM SG0200 WHERE SG02COD = ?"
      Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
      qrySQL(0) = txtText1(24).Text
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        If Not IsNull(rstSQL("SG02APE1").Value) Then
          txtText1(19).Text = rstSQL("SG02APE1").Value
        End If
        If Not IsNull(rstSQL("SG02NUMCOLEGIADO").Value) Then
          txtText1(21).Text = rstSQL("SG02NUMCOLEGIADO").Value
        End If
      End If
      rstSQL.Close
      Set rstSQL = Nothing
      qrySQL.Close
      Set qrySQL = Nothing
    End If
    txtText1(15).Text = ""
    If txtText1(14).Text <> "" Then
      srtSQL = "SELECT * FROM SG0200 WHERE SG02COD = ?"
      Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
      qrySQL(0) = txtText1(14).Text
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        If Not IsNull(rstSQL("SG02APE1").Value) Then
          txtText1(15).Text = rstSQL("SG02APE1").Value
        End If
      End If
      rstSQL.Close
      Set rstSQL = Nothing
      qrySQL.Close
      Set qrySQL = Nothing
    End If
    txtText1(26).Text = ""
    If txtText1(25).Text <> "" Then
      srtSQL = "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?"
      Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
      qrySQL(0) = txtText1(25).Text
      Set rstSQL = qrySQL.OpenResultset()
      If Not rstSQL.EOF Then
        If Not IsNull(rstSQL("FR26DESESTADOPET").Value) Then
          txtText1(26).Text = rstSQL("FR26DESESTADOPET").Value
        End If
      End If
      rstSQL.Close
      Set rstSQL = Nothing
      qrySQL.Close
      Set qrySQL = Nothing
    End If
  End If

'Error:
'  Exit Sub

End Sub



Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim strDelete As String

  If objWinInfo.objWinActiveForm.strName = "OM" Then
    If txtText1(25).Text <> 1 Then
      blnCancel = True
      Call MsgBox("No se puede borrar la petici�n " & txtText1(26).Text, _
                   vbInformation, "Aviso")
      Exit Sub
    End If
    If txtText1(0).Text <> "" Then
      stra = "SELECT * FROM FR2800 WHERE FR66CODPETICION=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        blnCancel = True
        Call MsgBox("No se puede borrar una Petici�n hasta borrar todos sus Medicamentos.", _
                     vbInformation, "Aviso")
        rsta.Close
        Set rsta = Nothing
        Exit Sub
      Else
        strDelete = "DELETE FROM FRA400 WHERE FR66CODPETICION=" & txtText1(0).Text
        objApp.rdoConnect.Execute strDelete, 64
        objApp.rdoConnect.Execute "Commit", 64
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  Else 'Detalle OM
  End If

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim rsta As rdoResultset
  Dim stra As String
  Dim mensaje As String
  Dim dblHoraFin As Double
  Dim dblHoraInicio As Double
  Dim blnFechaFinMenor As Boolean
  Dim qrya As rdoQuery
  
  If objWinInfo.objWinActiveForm.strName = "OM" Then
    If txtText1(2).Text <> "" Then
      stra = "SELECT CI21CODPERSONA FROM FR2200J " & _
             " WHERE CI21CODPERSONA=" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If rsta.EOF Then
        mensaje = MsgBox("El paciente es incorrecto.", vbInformation, "Aviso")
        blnCancel = True
      End If
      rsta.Close
      Set rsta = Nothing
      stra = "SELECT COUNT(*) " & _
             "  FROM FR2800 " & _
             " WHERE FR66CODPETICION = ? " & _
             "   AND (FR28FECFIN IS NOT NULL) " & _
             "   AND ((FR28FECINICIO > FR28FECFIN) " & _
             "        OR ((FR28FECINICIO = FR28FECFIN) AND ((FR28HORAFIN IS NULL) OR (ABS(FR28HORAFIN) > ABS(FR28HORAINICIO)))))"
      Set qrya = objApp.rdoConnect.CreateQuery("", stra)
      qrya(0) = txtText1(0).Text
      Set rsta = qrya.OpenResultset()
      If (rsta.rdoColumns(0).Value > 0) Then
        mensaje = MsgBox("Hay medicamentos con las fecha de fin anterior a la fecha de inicio", vbInformation, "Aviso")
        blnCancel = True
      End If
      qrya.Close
      Set qrya = Nothing
      Set rsta = Nothing
    End If
  Else 'Detalle
    txtText1(58).Text = txtText1(28).Text
    Select Case txtText1(53).Text 'Campos Obligatorios
    Case "/" 'Medicamento
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(55).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      'If txtText1(35).Text = "" Then
      '  mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      '  blnCancel = True
      '  txtText1(35).SetFocus
      '  Exit Sub
      'End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
          Exit Sub
          End If
        End If
    Case "M"  'Mezcla IV 2M
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(55).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      'If txtText1(35).Text = "" Then
      '  mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      '  blnCancel = True
      '''  txtText1(35).SetFocus
      '  Exit Sub
      'End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If txtText1(52).Text <> "" Then
        If txtText1(48).Text = "" Then
          mensaje = MsgBox("El campo Forma Farmace�tica del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(48).SetFocus
          Exit Sub
        End If
        If txtText1(50).Text = "" Then
          mensaje = MsgBox("El campo Dosis del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(50).SetFocus
          Exit Sub
        End If
        If txtText1(49).Text = "" Then
          mensaje = MsgBox("El campo Unidad de Medida del Medicamento 2 es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(49).SetFocus
          Exit Sub
        End If
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
          Exit Sub
          End If
        End If
    Case "P"  'PRN en OM
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Medicamento es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(55).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      'If txtText1(35).Text = "" Then
      '  mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      '  blnCancel = True
      '  txtText1(35).SetFocus
      '  Exit Sub
      'End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If txtText1(39).Text = "" Then
        mensaje = MsgBox("Para PRN en OM es obligatorio " & Chr(13) & "introducir las instrucciones de administraci�n.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(39).SetFocus
        Exit Sub
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
            Exit Sub
          End If
      End If
    Case "F"  'Fluidoterapia
      If txtText1(40).Text = "" Then
        mensaje = MsgBox("El campo Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(42).SetFocus
        Exit Sub
      End If
      If txtText1(42).Text = "" Then
        mensaje = MsgBox("El campo Volumen de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(42).SetFocus
        Exit Sub
      End If
      If txtHoras.Text = "" Then
        mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtHoras.SetFocus
        Exit Sub
      End If
      If txtMinutos.Text = "" Then
        mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtMinutos.SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad de la Soluci�n Intravenosa es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(27).Text <> "" Then
        'If txtText1(35).Text = "" Then
        '  mensaje = MsgBox("El campo Forma Farmace�tica del Medicamento es obligatorio.", vbInformation, "Aviso")
        '  blnCancel = True
        '  txtText1(35).SetFocus
        '  Exit Sub
        'End If
        If txtText1(30).Text = "" Then
          mensaje = MsgBox("El campo Dosis del Medicamento es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(30).SetFocus
          Exit Sub
        End If
        If txtText1(31).Text = "" Then
          mensaje = MsgBox("El campo Unidad de Medida del Medicamento es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(31).SetFocus
          Exit Sub
        End If
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
            Exit Sub
          End If
      End If
    Case "E" 'Estupefaciente
      If txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Estupefaciente es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(55).SetFocus
        Exit Sub
      Else
        If txtText1(27).Text = "999999999" And txtText1(55).Text = "" Then
          mensaje = MsgBox("El campo Descripci�n Medicamento No Formulario es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(55).SetFocus
          Exit Sub
        End If
      End If
      'If txtText1(35).Text = "" Then
      '  mensaje = MsgBox("El campo Forma Farmace�tica es obligatorio.", vbInformation, "Aviso")
      '  blnCancel = True
      '  txtText1(35).SetFocus
      '  Exit Sub
      'End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If txtText1(28).Text = "" Then
        mensaje = MsgBox("El campo Cantidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(28).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(40).Text <> "" Then
        If txtText1(42).Text = "" Then
          mensaje = MsgBox("El campo Volumen de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtText1(42).SetFocus
          Exit Sub
        End If
        If txtHoras.Text = "" Then
          mensaje = MsgBox("El campo Hora de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtHoras.SetFocus
          Exit Sub
        End If
        If txtMinutos.Text = "" Then
          mensaje = MsgBox("El campo Minutos de Tiempo de la Soluci�n para Diluir es obligatorio.", vbInformation, "Aviso")
          blnCancel = True
          txtMinutos.SetFocus
          Exit Sub
        End If
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
            Exit Sub
          End If
      End If
    Case "L"  'Form.Magistral
      If txtText1(55).Text = "" Then
        mensaje = MsgBox("El campo Descripci�n de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(55).SetFocus
        Exit Sub
      End If
      If txtText1(30).Text = "" Then
        mensaje = MsgBox("El campo Dosis es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(30).SetFocus
        Exit Sub
      End If
      If txtText1(31).Text = "" Then
        mensaje = MsgBox("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(31).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(0) = "" Then
        mensaje = MsgBox("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(0).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(1) = "" Then
        mensaje = MsgBox("El campo V�a es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(1).SetFocus
        Exit Sub
      End If
      If cboDBCombo1(3) = "" Then
        mensaje = MsgBox("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        cboDBCombo1(3).SetFocus
        Exit Sub
      End If
      If txtText1(57).Text = "" And txtText1(27).Text = "" Then
        mensaje = MsgBox("El campo Archivo Inf. de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
        blnCancel = True
        txtText1(57).SetFocus
        Exit Sub
      End If
      If IsDate(dtcDateCombo1(4).Date) Then
          If Not IsNumeric(txtText1(36).Text) Then
            mensaje = MsgBox("El campo Hora Fin es obligatorio.", vbInformation, "Aviso")
            blnCancel = True
            txtText1(36).SetFocus
            Exit Sub
          End If
      End If
    End Select
    End If
  
  
  If strFormName = "Detalle OM" Then
   'Se comprueba que la fecha de inicio de la l�nea de detalle de la OM se menor
    'que la fecha de fin
    If (dtcDateCombo1(4).Text <> "") Then
      'Hay fecha de fin
      'If dtcDateCombo1(4).Date < dtcDateCombo1(3).Date Then
      blnFechaFinMenor = False
      Select Case DateDiff("yyyy", dtcDateCombo1(4).Date, dtcDateCombo1(3).Date)
        Case Is > 0
          blnFechaFinMenor = True
        Case Is < 0
          blnFechaFinMenor = False
        Case 0
          Select Case DateDiff("y", dtcDateCombo1(4).Date, dtcDateCombo1(3).Date)
            Case Is > 0
              blnFechaFinMenor = True
            Case Is < 0
              blnFechaFinMenor = False
            Case 0
              blnFechaFinMenor = False
          End Select
      End Select
      
      If blnFechaFinMenor Then
        mensaje = MsgBox("La fecha de fin no puede ser menor que la fecha de inicio", vbInformation, "Aviso")
        blnCancel = True
        'dtcDateCombo1(4).SetFocus
      Else
        If dtcDateCombo1(4).Date = dtcDateCombo1(3).Date Then
          If IsNumeric(txtText1(36).Text) Then
            'Hay hora de fin y hay que compararla con la hora de inicio
            If IsNumeric(txtText1(37).Text) Then
              dblHoraFin = txtText1(36).Text
              dblHoraInicio = txtText1(37).Text
              'If dblHoraFin <> 0 Then
                If dblHoraFin < dblHoraInicio Then
                  mensaje = MsgBox("La hora de Fin debe ser mayor que la hora de Inicio", vbInformation, "Aviso")
                  blnCancel = True
                  txtText1(37).SetFocus
                End If
              'End If
            Else
              mensaje = MsgBox("Debe introducir la hora de inicio", vbInformation, "Aviso")
              blnCancel = True
              txtText1(37).SetFocus
            End If
          End If
        End If
      End If
    End If
  End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'Dim intReport As Integer
'Dim objPrinter As clsCWPrinter
'Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim rstfec As rdoResultset
Dim strfec As String
Dim hora As Variant
Dim auxProceso, auxAsistencia As Currency
Dim strpeso As String
Dim rstpeso As rdoResultset
Dim strad1500 As String
Dim rstad1500 As rdoResultset
Dim strProcAsis As String
Dim rstProcAsis As rdoResultset
Dim strpaciente As String
Dim rstpaciente As rdoResultset
Dim mensaje  As String
Dim stra As String
Dim strDelete As String
Dim strPathW As String
Dim rstaPathW As rdoResultset
Dim strFRH2pathword As String

  If btnButton.Index = 30 Then 'Salir
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  End If

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "OM" Then
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
      If txtText1(0).Text <> "" Then
        If strBoton = "cmdFMnueva" Then
          strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                    txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If IsNull(rstlinea.rdoColumns(0).Value) Then
            linea = 1
          Else
            'linea = rstlinea.rdoColumns(0).Value + 1
            MsgBox "S�lo puede pedir una F�rmula Magistral", vbInformation, "Aviso"
            Exit Sub
          End If
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          txtText1(53).Text = mstrOperacion
          txtText1(45).Text = linea
          
'          strPathW = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=34"
'          Set rstaPathW = objApp.rdoConnect.OpenResultset(strPathW)
'          If Not rstaPathW.EOF Then
'            strFRH2pathword = rstaPathW(0).Value
'          Else
'            MsgBox "Debe introducir el Path de la ubicaci�n de los ficheros Word de Form.Magistrales en par�metros generales." & Chr(13) & _
'                   "C�digo par�metros generales : 34 " & Chr(13) & _
'                   "Ej.: C:\Archivos de programa\cun\", vbInformation
'          End If
'          rstaPathW.Close
'          Set rstaPathW = Nothing
          'txtText1(57).Text = strFRH2pathword & txtText1(8).Text & "_" & txtText1(0).Text & "_PAC.DOC"
          txtText1(57).Text = txtText1(8).Text & "_" & txtText1(0).Text & "_PAC.DOC"
          
          strfec = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY'),TO_CHAR(SYSDATE,'HH24') FROM DUAL"
          Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
          Call objWinInfo.CtrlSet(dtcDateCombo1(3), rstfec.rdoColumns(0).Value)
          Call objWinInfo.CtrlSet(txtText1(37), rstfec.rdoColumns(1).Value)
          rstfec.Close
          Set rstfec = Nothing
          rstlinea.Close
          Set rstlinea = Nothing
          txtText1(55).Locked = False
        Else 'Normalizada
          strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                    txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If IsNull(rstlinea.rdoColumns(0).Value) Then
            linea = 1
          Else
            'linea = rstlinea.rdoColumns(0).Value + 1
            MsgBox "S�lo puede pedir una F�rmula Magistral", vbInformation, "Aviso"
            Exit Sub
          End If
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          txtText1(53).Text = mstrOperacion
          txtText1(45).Text = linea
          
          'strPathW = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=34"
          'Set rstaPathW = objApp.rdoConnect.OpenResultset(strPathW)
          'If Not rstaPathW.EOF Then
          '  strFRH2pathword = rstaPathW(0).Value
          'Else
          '  MsgBox "Debe introducir el Path de la ubicaci�n de los ficheros Word de Form.Magistrales en par�metros generales." & Chr(13) & _
          '         "C�digo par�metros generales : 34 " & Chr(13) & _
          '         "Ej.: C:\Archivos de programa\cun\", vbInformation
          'End If
          'rstaPathW.Close
          'Set rstaPathW = Nothing
          'txtText1(57).Text = strFRH2pathword & txtText1(8).Text & "_" & txtText1(0).Text & "_PAC.DOC"
          
          strfec = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY'),TO_CHAR(SYSDATE,'HH24') FROM DUAL"
          Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
          Call objWinInfo.CtrlSet(dtcDateCombo1(3), rstfec.rdoColumns(0).Value)
          Call objWinInfo.CtrlSet(txtText1(37), rstfec.rdoColumns(1).Value)
          rstfec.Close
          Set rstfec = Nothing
          rstlinea.Close
          Set rstlinea = Nothing
          'While txtText1(27).Text = ""
            Call objWinInfo_cwForeign("Detalle OM", "txtText1(27)")
          'Wend
          If Trim(txtText1(27).Text) = "" Then
            objWinInfo.objWinActiveForm.blnChanged = False
          End If
          txtText1(55).Locked = True
        End If
      Else
        MsgBox "No hay ninguna petici�n", vbInformation
        Exit Sub
      End If
    Else
      If btnButton.Index = 4 And objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
        If txtText1(27).Text <> "" Then
         'If Campos_Obligatorios = True Then
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
         'End If
        Else
          'If Campos_Obligatorios = True Then
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            Exit Sub
          'End If
        End If
      Else
        If objWinInfo.objWinActiveForm.strName = "Detalle OM" Then
          Select Case btnButton.Index
          Case 21 'Primero
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 22 'Anterior
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 23 'Siguiente
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case 24 'Ultimo
            'blnDetalle = False
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          Case Else 'Otro boton
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          End Select
        Else
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        End If
      End If
    End If
  End If
  

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 40 'Guardar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  
  Select Case intIndex
  Case 0
    If chkCheck1(0).Value = 0 Then
      chkCheck1(0).ForeColor = vbBlack
    Else
      chkCheck1(0).ForeColor = vbRed
    End If
  Case 2
    If chkCheck1(2).Value = 0 Then
      chkCheck1(2).ForeColor = vbBlack
    Else
      chkCheck1(2).ForeColor = vbRed
    End If
  Case 7
    If chkCheck1(7).Value = 0 Then
      chkCheck1(7).ForeColor = vbBlack
    Else
      chkCheck1(7).ForeColor = vbRed
    End If
  End Select
  
  Select Case intIndex
    Case 1:
        If chkCheck1(1).Value = 1 Then
            chkCheck1(3).Value = 0
            chkCheck1(4).Value = 0
            chkCheck1(3).Enabled = False
            chkCheck1(4).Enabled = False
        Else
            chkCheck1(3).Enabled = True
            chkCheck1(4).Enabled = True
        End If
    Case 5:
        If chkCheck1(5).Value = 1 Then
            cboDBCombo1(1).Enabled = True
            objWinInfo.CtrlGetInfo(cboDBCombo1(1)).blnReadOnly = False
        Else
            'cboDBCombo1(1).Enabled = False
            'objWinInfo.CtrlGetInfo(cboDBCombo1(1)).blnReadOnly = True
        End If
    Case 12:
      If chkCheck1(12).Value = 1 Then
        If chkOMPRN(6).Value <> 0 Then
          chkOMPRN(6).Value = 0
        End If
        If txtText1(1).Text <> "" Then
          Call objWinInfo.CtrlSet(txtText1(1), "")
        End If
      Else
        If chkOMPRN(6).Value <> 1 Then
          chkOMPRN(6).Value = 1
        End If
        If txtText1(1).Text = "" Then
          Call objWinInfo.CtrlSet(txtText1(1), txtText1(0))
        End If
      End If
  End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtHoras_Change()

  strOrigen = "T"
  If intCambioAlgo = 1 Then
    If txtHoras.Text <> "" And txtMinutos.Text <> "" Then
      If txtHoras.Text > 999 Then
        Beep
        txtHoras.Text = 100
      End If
      If IsNumeric(txtHoras.Text) And IsNumeric(txtMinutos.Text) Then
        'txtText1(43).Text = (txtHoras * 60) + txtMinutos
        Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60) + txtMinutos)
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    Else
      If txtHoras.Text <> "" Then
        If IsNumeric(txtHoras.Text) Then
          If txtHoras.Text > 999 Then
            Beep
            txtHoras.Text = 100
          End If
          Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60))
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      Else
        If IsNumeric(txtMinutos.Text) Then
          Call objWinInfo.CtrlSet(txtText1(43), txtMinutos)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If

End Sub

Private Sub txtHoras_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8
    Case Else
      KeyAscii = 0
    End Select
    intCambioAlgo = 1
    strOrigen = "T"
End Sub

Private Sub txtMinutos_Change()
  
  strOrigen = "T"
  If intCambioAlgo = 1 Then
    If txtHoras.Text <> "" And txtMinutos.Text <> "" Then
      If txtMinutos.Text > 59 Then
        Beep
        txtMinutos.Text = 59
      End If
      If IsNumeric(txtHoras.Text) And IsNumeric(txtMinutos.Text) Then
        'txtText1(43).Text = (txtHoras * 60) + txtMinutos
        Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60) + txtMinutos)
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
      End If
    Else
      If txtHoras.Text <> "" Then
        If IsNumeric(txtHoras.Text) Then
          Call objWinInfo.CtrlSet(txtText1(43), (txtHoras * 60))
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      Else
        If IsNumeric(txtMinutos.Text) Then
          If txtMinutos.Text > 59 Then
            Beep
            txtMinutos.Text = 59
          End If
          Call objWinInfo.CtrlSet(txtText1(43), txtMinutos)
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        Else
          Call objWinInfo.CtrlSet(txtText1(43), "")
          objWinInfo.objWinActiveForm.blnChanged = True
          tlbToolbar1.Buttons(4).Enabled = True
        End If
      End If
    End If
  End If

End Sub

Private Sub txtMinutos_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8
    Case Else
      KeyAscii = 0
    End Select
    intCambioAlgo = 1
    strOrigen = "T"
End Sub


Private Sub txtText1_DblClick(Index As Integer)

  If objWinInfo.CtrlGetInfo(txtText1(Index)).blnForeign Then
    If stbStatusBar1.Panels(2).Text = "Lista" Then
      Call objWinInfo_cwForeign(objWinInfo.objWinActiveForm.strName, "txtText1(" & Index & ")")
    End If
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)

  If Index = 32 Then
    If IsNumeric(txtText1(32).Text) Then
      objWinInfo.objWinActiveForm.blnChanged = True
      tlbToolbar1.Buttons(4).Enabled = True
    End If
  End If
  'ALTURA
  If Index = 33 Then
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), 8
    Case Else
      KeyAscii = 0
    End Select
    
    If IsNumeric(txtText1(33).Text) Then
      objWinInfo.objWinActiveForm.blnChanged = True
      tlbToolbar1.Buttons(4).Enabled = True
    End If
  End If
  If (Index = 82) Then
    strOrigen = "V"
  End If

End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
Dim rstDosis As rdoResultset
Dim strDosis As String
Dim strPotPeso As String
Dim rstPotPeso As rdoResultset
Dim strPotAlt As String
Dim rstPotAlt As rdoResultset
Dim strFactor As String
Dim rstFactor As rdoResultset
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
'Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
'Dim TiemMinInf As Currency
'Dim dblTiempo As Double


  Call objWinInfo.CtrlLostFocus
  If intIndex = 32 Then
    If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
        intPeso = txtText1(32).Text
        intAltura = txtText1(33).Text
        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
        txtText1(29).Text = vntMasa
    End If
  End If
  If intIndex = 33 Then
    If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
        strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
        Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
        strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
        Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
        strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
        Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
        intPeso = txtText1(32).Text
        intAltura = txtText1(33).Text
        vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
        vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
        vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
        txtText1(29).Text = vntMasa
    End If
  End If
  
  If intIndex = 28 Then
    If mstrOperacion <> "F" Then
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(28).Text) Then
              Call objWinInfo.CtrlSet(txtText1(30), Format(txtText1(28).Text * rstDosis("fr73dosis").Value, "######0.00"))
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                Call objWinInfo.CtrlSet(txtText1(38), Format(txtText1(28).Text * rstDosis("fr73volumen").Value, "###0.00"))
              End If
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    Else 'I.V.
      If Trim(txtText1(40).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(40).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73volumen").Value) = False And IsNumeric(txtText1(28).Text) Then
              Call objWinInfo.CtrlSet(txtText1(42), Format(txtText1(28).Text * rstDosis("fr73volumen").Value, "###0.00"))
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    End If
  End If
  
  If intIndex = 30 Then
    If mstrOperacion <> "F" Then
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(30).Text) Then
              If rstDosis("fr73dosis").Value <> 0 Then
                Call objWinInfo.CtrlSet(txtText1(28), Format(txtText1(30).Text / rstDosis("fr73dosis").Value, "##0.00"))
              End If
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                If rstDosis("fr73dosis").Value <> 0 Then
                  Call objWinInfo.CtrlSet(txtText1(38), Format((txtText1(30).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value, "###0.00"))
                End If
              End If
            End If
        End If
      End If
    Else 'I.V.
      If Trim(txtText1(27).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(27).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(30).Text) Then
              'Call objWinInfo.CtrlSet(txtText1(28), txtText1(30).Text / rstDosis("fr73dosis").Value)
              If IsNull(rstDosis("fr73volumen").Value) = False Then
                If rstDosis("fr73dosis").Value <> 0 Then
                  Call objWinInfo.CtrlSet(txtText1(38), Format((txtText1(30).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value, "###0.00"))
                End If
              End If
            End If
        End If
      End If
    End If
  End If

  If intIndex = 50 Then
    If mstrOperacion = "M" Then
      If Trim(txtText1(52).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(52).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73dosis").Value) = False And IsNumeric(txtText1(50).Text) And IsNull(rstDosis("fr73volumen").Value) = False Then
              If rstDosis("fr73dosis").Value <> 0 And IsNumeric(txtText1(50).Text) Then
                Call objWinInfo.CtrlSet(txtText1(47), (txtText1(50).Text / rstDosis("fr73dosis").Value) * rstDosis("fr73volumen").Value)
              End If
            End If
        End If
      End If
    End If
  End If
  
  If intIndex = 42 Then
    If mstrOperacion <> "F" Then
    Else 'I.V.
      If Trim(txtText1(40).Text) <> "" Then
        strDosis = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & _
                  Trim(txtText1(40).Text)
        Set rstDosis = objApp.rdoConnect.OpenResultset(strDosis)
        If rstDosis.EOF = False Then
            If IsNull(rstDosis("fr73volumen").Value) = False And IsNumeric(txtText1(28).Text) Then
              If rstDosis("fr73volumen").Value <> 0 And IsNumeric(txtText1(42).Text) Then
                Call objWinInfo.CtrlSet(txtText1(28), Format(txtText1(42).Text / rstDosis("fr73volumen").Value, , "##0.00"))
              End If
            End If
        End If
        rstDosis.Close
        Set rstDosis = Nothing
      End If
    End If
  End If
  
  
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim rstPotPeso As rdoResultset
Dim rstFactor As rdoResultset
Dim rstPotAlt As rdoResultset
Dim strPotPeso As String
Dim strPotAlt As String
Dim strFactor As String
Dim intPeso As Integer
Dim intAltura As Integer
Dim vntMasa As Variant
Dim rsta As rdoResultset
Dim stra As String
Dim Vol1, Vol2, Vol3, VolTotal, VolPerf As Currency
Dim TiemMinInf As Currency
Dim dblTiempo As Double

  Call objWinInfo.CtrlDataChange
  
  If intIndex = 43 Then
    If IsNumeric(txtText1(43).Text) Then
      If (intCambioAlgo = 0) And (strOrigen = "T") Then
        txtHoras.Text = txtText1(43).Text \ 60
        txtMinutos.Text = txtText1(43).Text Mod 60
      End If
    Else
        txtHoras.Text = ""
        txtMinutos.Text = ""
    End If
  End If
  
  If (intIndex = 82) And (strOrigen = "V") Then
    VolTotal = 0
    If txtText1(38).Text = "" Then
      Vol1 = 0
    Else
      Vol1 = CCur(txtText1(38).Text)
    End If
    If txtText1(42).Text = "" Then
      Vol2 = 0
    Else
      Vol2 = CCur(txtText1(42).Text)
    End If
    If txtText1(47).Text = "" Then
      Vol3 = 0
    Else
      Vol3 = CCur(txtText1(47).Text)
    End If
    VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
    'Se ha modificado la velocidad de perfusi�n
    If (VolTotal > 0) And IsNumeric(txtText1(82).Text) Then
      If txtText1(82).Text > 0 Then
        'Se puede calcular el tiempo Tiempo = VolTotal/ (VelPerf * 60)
        dblTiempo = (VolTotal / txtText1(82).Text) * 60
        txtHoras.Text = dblTiempo \ 60
        txtMinutos.Text = dblTiempo Mod 60
      End If
    Else
      'No hacer nada, de momento
    End If
  End If
  
  'PESO
  If intIndex = 32 Then
    If Not IsNumeric(txtText1(32).Text) Then
        txtText1(32).Text = ""
        txtText1(29).Text = ""
    Else
        If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
            strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
            Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
            strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
            Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
            strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
            Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
            intPeso = txtText1(32).Text
            intAltura = txtText1(33).Text
            vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
            vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
            vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
            txtText1(29).Text = vntMasa
            rstPotPeso.Close
            Set rstPotPeso = Nothing
            rstFactor.Close
            Set rstFactor = Nothing
            rstPotAlt.Close
            Set rstPotAlt = Nothing
        Else
            txtText1(29).Text = ""
        End If
    End If
  End If
  'ALTURA
  If intIndex = 33 Then
    If Not IsNumeric(txtText1(33).Text) Then
        txtText1(33).Text = ""
        txtText1(29).Text = ""
    Else
        If IsNumeric(txtText1(32).Text) And IsNumeric(txtText1(33).Text) Then
            strPotPeso = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=10"
            Set rstPotPeso = objApp.rdoConnect.OpenResultset(strPotPeso)
            strPotAlt = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=11"
            Set rstPotAlt = objApp.rdoConnect.OpenResultset(strPotAlt)
            strFactor = "SELECT * FROM FRH200 WHERE FRH2CODPARAMGEN=12"
            Set rstFactor = objApp.rdoConnect.OpenResultset(strFactor)
            intPeso = txtText1(32).Text
            intAltura = txtText1(33).Text
            vntMasa = intPeso ^ rstPotPeso("FRH2PARAMGEN").Value
            vntMasa = vntMasa * (intAltura ^ rstPotAlt("FRH2PARAMGEN").Value)
            vntMasa = vntMasa * rstFactor("FRH2PARAMGEN").Value
            txtText1(29).Text = vntMasa
            rstPotPeso.Close
            Set rstPotPeso = Nothing
            rstFactor.Close
            Set rstFactor = Nothing
            rstPotAlt.Close
            Set rstPotAlt = Nothing
        Else
            txtText1(29).Text = ""
        End If
    End If
  End If
  'HORA (frame Medicamentos)
  If intIndex = 36 Then
    If Not IsNumeric(txtText1(36).Text) Then
        txtText1(36).Text = ""
    Else
        If txtText1(36).Text > 23 Then
            txtText1(36).Text = ""
                Call MsgBox("Hora fuera de rango. Para indicar las 24 horas adelante un dia la Fecha in y ponga en la Hora Fin un 0 (cero)", vbCritical, "Aviso")
        End If
    End If
  End If

  Select Case intIndex
  Case 38, 42, 47, 43, 44 ', 82
    VolTotal = 0
    If txtText1(38).Text = "" Then
      Vol1 = 0
    Else
      Vol1 = CCur(txtText1(38).Text)
    End If
    If txtText1(42).Text = "" Then
      Vol2 = 0
    Else
      Vol2 = CCur(txtText1(42).Text)
    End If
    If txtText1(47).Text = "" Then
      Vol3 = 0
    Else
      Vol3 = CCur(txtText1(47).Text)
    End If
    VolTotal = Format(Vol1 + Vol2 + Vol3, "0.00")
    If txtText1(43).Text = "" Then
      TiemMinInf = 0
    Else
      TiemMinInf = CCur(txtText1(43).Text) / 60
    End If
    If TiemMinInf <> 0 Then
      VolPerf = Format(VolTotal / TiemMinInf, "0.00")
    Else
      VolPerf = 0
    End If
    If txtText1(44).Text <> "" Then
      If VolTotal <> CCur(txtText1(44)) Then
        txtText1(44).Text = Format(VolTotal, "###0.00")
      End If
    Else
      If VolTotal <> 0 Then
        txtText1(44).Text = Format(VolTotal, "###0.00")
      End If
    End If
    If txtText1(82).Text <> "" Then
      If VolPerf <> CCur(txtText1(82)) Then
        txtText1(82).Text = Format(VolPerf, "#####0.00")
      End If
    Else
      If VolPerf <> 0 Then
        txtText1(82).Text = Format(VolPerf, "#####0.00")
      End If
    End If
  End Select
  
'Campos Linkados
Dim srtSQL As String
Dim qrySQL As rdoQuery
Dim rstSQL As rdoResultset
Select Case intIndex
Case 2
  txtText1(3).Text = ""
  txtText1(4).Text = ""
  txtText1(5).Text = ""
  txtText1(23).Text = ""
  txtText1(7).Text = ""
  If txtText1(2).Text <> "" Then
    srtSQL = "SELECT * FROM CI2200 WHERE CI21CODPERSONA= ?"
    Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
    qrySQL(0) = txtText1(2).Text
    Set rstSQL = qrySQL.OpenResultset()
    If Not rstSQL.EOF Then
      If Not IsNull(rstSQL("CI22NOMBRE").Value) Then
        txtText1(3).Text = rstSQL("CI22NOMBRE").Value
      End If
      If Not IsNull(rstSQL("CI22PRIAPEL").Value) Then
        txtText1(4).Text = rstSQL("CI22PRIAPEL").Value
      End If
      If Not IsNull(rstSQL("CI22SEGAPEL").Value) Then
        txtText1(5).Text = rstSQL("CI22SEGAPEL").Value
      End If
      If Not IsNull(rstSQL("CI22NUMHISTORIA").Value) Then
        txtText1(23).Text = rstSQL("CI22NUMHISTORIA").Value
      End If
      If Not IsNull(rstSQL("CI30CODSEXO").Value) Then
        txtText1(7).Text = rstSQL("CI30CODSEXO").Value
      End If
    End If
    rstSQL.Close
    Set rstSQL = Nothing
    qrySQL.Close
    Set qrySQL = Nothing
  End If
Case 7
  txtText1(6).Text = ""
  If txtText1(7).Text <> "" Then
    srtSQL = "SELECT * FROM CI3000 WHERE CI30CODSEXO = ?"
    Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
    qrySQL(0) = txtText1(7).Text
    Set rstSQL = qrySQL.OpenResultset()
    If Not rstSQL.EOF Then
      If Not IsNull(rstSQL("CI30DESSEXO").Value) Then
        txtText1(6).Text = rstSQL("CI30DESSEXO").Value
      End If
    End If
    rstSQL.Close
    Set rstSQL = Nothing
    qrySQL.Close
    Set qrySQL = Nothing
  End If
Case 24
  txtText1(19).Text = ""
  txtText1(21).Text = ""
  If txtText1(24).Text <> "" Then
    srtSQL = "SELECT * FROM SG0200 WHERE SG02COD = ?"
    Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
    qrySQL(0) = txtText1(24).Text
    Set rstSQL = qrySQL.OpenResultset()
    If Not rstSQL.EOF Then
      If Not IsNull(rstSQL("SG02APE1").Value) Then
        txtText1(19).Text = rstSQL("SG02APE1").Value
      End If
      If Not IsNull(rstSQL("SG02NUMCOLEGIADO").Value) Then
        txtText1(21).Text = rstSQL("SG02NUMCOLEGIADO").Value
      End If
    End If
    rstSQL.Close
    Set rstSQL = Nothing
    qrySQL.Close
    Set qrySQL = Nothing
  End If
Case 14
  txtText1(15).Text = ""
  If txtText1(14).Text <> "" Then
    srtSQL = "SELECT * FROM SG0200 WHERE SG02COD = ?"
    Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
    qrySQL(0) = txtText1(14).Text
    Set rstSQL = qrySQL.OpenResultset()
    If Not rstSQL.EOF Then
      If Not IsNull(rstSQL("SG02APE1").Value) Then
        txtText1(15).Text = rstSQL("SG02APE1").Value
      End If
    End If
    rstSQL.Close
    Set rstSQL = Nothing
    qrySQL.Close
    Set qrySQL = Nothing
  End If
Case 25
  txtText1(26).Text = ""
  If txtText1(25).Text <> "" Then
    srtSQL = "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?"
    Set qrySQL = objApp.rdoConnect.CreateQuery("", srtSQL)
    qrySQL(0) = txtText1(25).Text
    Set rstSQL = qrySQL.OpenResultset()
    If Not rstSQL.EOF Then
      If Not IsNull(rstSQL("FR26DESESTADOPET").Value) Then
        txtText1(26).Text = rstSQL("FR26DESESTADOPET").Value
      End If
    End If
    rstSQL.Close
    Set rstSQL = Nothing
    qrySQL.Close
    Set qrySQL = Nothing
  End If
End Select

End Sub





Private Function Peticion_Correcta() As Boolean
Dim strSelect As String
Dim rstSelect As rdoResultset
Dim strOperacion As String
Dim blnIncorrecta As Boolean
Dim blnFecFin As Boolean
Dim IntLinea As Integer
Dim mensaje As String
Dim strSql As String
Dim qrySQL As rdoQuery
Dim rstSQL As rdoResultset
    
  If txtText1(0).Text = "" Then
    mensaje = MsgBox("No hay Orden M�dica para firmar.", vbInformation, "Aviso")
    Peticion_Correcta = False
    Exit Function
  End If
    
  IntLinea = 0
  blnIncorrecta = False
  strSelect = "SELECT * FROM FR2800 "
  strSelect = strSelect & " WHERE FR66CODPETICION=" & txtText1(0).Text
  Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
  While Not rstSelect.EOF And blnIncorrecta = False
    blnFecFin = False
    IntLinea = IntLinea + 1
    Select Case rstSelect("FR28OPERACION").Value 'Campos Obligatorios
    Case "L" 'Form.Magistral
      If IsNull(rstSelect("FR28FECINICIO").Value) Then
        blnIncorrecta = True  '("El campo Fecha de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstSelect("FR28HORAINICIO")) Then
        blnIncorrecta = True  '("El campo Hora de Inicio es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstSelect("FR28DESPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Descripci�n de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstSelect("FR28DOSIS")) Then
        blnIncorrecta = True  '("El campo Dosis es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstSelect("FR93CODUNIMEDIDA")) Then
        blnIncorrecta = True  '("El campo Unidad de Medida es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstSelect("FRG4CODFRECUENCIA")) Then
        blnIncorrecta = True  '("El campo Frecuencia es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstSelect("FR34CODVIA")) Then
        blnIncorrecta = True  '("El campo V�a es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstSelect("FRH5CODPERIODICIDAD")) Then
        blnIncorrecta = True  '("El campo Periodicidad es obligatorio.", vbInformation, "Aviso")
      End If
      If IsNull(rstSelect("FR28PATHFORMAG")) And IsNull(rstSelect("FR73CODPRODUCTO")) Then
        blnIncorrecta = True  '("El campo Archivo Inf. de la F�rmula Magistral es obligatorio.", vbInformation, "Aviso")
      End If
    End Select
    
    If Not IsNull(rstSelect("fr28fecfin").Value) Then
        strSql = "SELECT TRUNC(SYSDATE)-TO_DATE(?,'DD/MM/YYYY') FROM DUAL"
        Set qrySQL = objApp.rdoConnect.CreateQuery("", strSql)
        qrySQL(0) = Format(rstSelect("fr28fecfin").Value, "dd/mm/yyyy")
        Set rstSQL = qrySQL.OpenResultset()
        If Not rstSQL.EOF Then
          If rstSQL.rdoColumns(0).Value > 0 Then
            blnIncorrecta = True
            blnFecFin = True
          End If
        End If
        qrySQL.Close
        Set rstSQL = Nothing
    End If
    
    rstSelect.MoveNext
  Wend
  
  If IntLinea = 0 Then
    mensaje = MsgBox("La Orden M�dica no tiene Medicamentos.", vbCritical, "Error")
    Peticion_Correcta = False
  ElseIf blnIncorrecta = False Then
    Peticion_Correcta = True
  Else
    If blnFecFin = False Then
      mensaje = MsgBox("La linea : " & IntLinea & " de la Orden M�dica tiene campos obligatorios no informados.", vbCritical, "Error")
    Else
      mensaje = MsgBox("La Fecha Fin de la linea : " & IntLinea & " de la Orden M�dica es anterior a hoy.", vbCritical, "Error")
    End If
    Peticion_Correcta = False
  End If
  
  rstSelect.Close
  Set rstSelect = Nothing

End Function

Private Function Campos_Obligatorios() As Boolean
Dim blnIncorrecta As Boolean
Dim mensaje As String
Dim auxControl As Control
    
  mensaje = ""
  blnIncorrecta = False
  Select Case txtText1(53).Text 'Campos Obligatorios
  Case "L" 'Form.Magistral
    If dtcDateCombo1(3).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = dtcDateCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Fecha de Inicio es obligatorio."
    End If
    If txtText1(37).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(37)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Hora de Inicio es obligatorio."
    End If
    If IsDate(dtcDateCombo1(4).Date) Then
      If Not IsNumeric(txtText1(36).Text) Then
        If blnIncorrecta = False Then
          Set auxControl = txtText1(36)
        End If
        blnIncorrecta = True
        mensaje = mensaje & Chr(13) & "El campo Hora de Fin es obligatorio."
      End If
    End If
    If txtText1(55).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(55)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Descripci�n de la F�rmula Magistral es obligatorio."
    End If
    If txtText1(30).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(30)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Dosis es obligatorio."
    End If
    If txtText1(31).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(31)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Unidad de Medida es obligatorio."
    End If
    If cboDBCombo1(0) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(0)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Frecuencia es obligatorio."
    End If
    If cboDBCombo1(1) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(1)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo V�a es obligatorio."
    End If
    If cboDBCombo1(3) = "" Then
      If blnIncorrecta = False Then
        Set auxControl = cboDBCombo1(3)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Periodicidad es obligatorio."
    End If
    If txtText1(57).Text = "" And txtText1(27).Text = "" Then
      If blnIncorrecta = False Then
        Set auxControl = txtText1(57)
      End If
      blnIncorrecta = True
      mensaje = mensaje & Chr(13) & "El campo Archivo Inf. de la F�rmula Magistral es obligatorio."
    End If
  End Select
  
  If blnIncorrecta = False Then
    Campos_Obligatorios = False
  Else
    Campos_Obligatorios = True
    MsgBox mensaje, vbInformation, "Aviso"
    auxControl.SetFocus
  End If

End Function

Private Sub Ordenes_Fabricacion(tipo As String)
Dim strSelect As String
Dim strinsert As String
Dim bCrearOFNueva As Boolean
Dim rsta As rdoResultset
Dim strSelectFR6900 As String
Dim rstFR6900 As rdoResultset
Dim strSelectFR7000 As String
Dim rstFR7000 As rdoResultset
Dim strSelectFR7100 As String
Dim rstFR7100   As rdoResultset
Dim auxcant

  If tipo = "Paciente" Then
    ' Mirar si existe una orden de fabricaci�n para este c�digo de petici�n
    strSelect = "SELECT COUNT(FR66CODPETICION) FROM FR5900 WHERE " & _
                " FR66CODPETICION=" & txtText1(0).Text & _
                " AND FR28NUMLINEA=" & txtText1(45).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
    If IsNull(rsta.rdoColumns(0).Value) Then
       ' Ok, NO EXISTE -> CREAR UNA NUEVA
       bCrearOFNueva = True
    ElseIf rsta.rdoColumns(0).Value = 0 Then
       ' Ok, NO EXISTE -> CREAR UNA NUEVA
       bCrearOFNueva = True
    Else
       ' Ok, SI EXISTE -> HACER REFERENCIA A LA QUE EXISTE
       bCrearOFNueva = False
    End If
    rsta.Close
    Set rsta = Nothing
    If bCrearOFNueva Then
       ' Crear una OF nueva por cada forma de hacer un producto
       strSelectFR6900 = " SELECT FR69CODPROCFABRIC, FR73CODPRODUCTO, FR69DESPROCESO FROM FR6900 " & _
                         " WHERE FR73CODPRODUCTO = " & codFormMagistral
       Set rstFR6900 = objApp.rdoConnect.OpenResultset(strSelectFR6900)
       If rstFR6900.EOF Then
          strinsert = "insert into fr5900 " & _
                      "  (FR59CODORDFABR, " & _
                      "  FR59OBSORDEN,    " & _
                      "  FR73CODPRODUCTO, " & _
                      "  FR93CODUNIMEDIDA, " & _
                      "  FR59CANTFABRICAR, " & _
                      "  FR37CODESTOF, " & _
                      "  FR66CODPETICION, " & _
                      "  FR28NUMLINEA, " & _
                      "  FR59DESPROCESO,FR59INDPAC) "
          strinsert = strinsert & _
                      "Values " & _
                      "  (FR59CODORDFABR_SEQUENCE.nextval," & _
                      "  NULL," & _
                         codFormMagistral & "," & _
                      "'" & txtText1(49).Text & "'" & "," & _
                         objGen.ReplaceStr(txtText1(28).Text, ",", ".", 1) & "," & _
                      "1," & _
                         txtText1(0).Text & "," & _
                         txtText1(45).Text & "," & _
                      "NULL" & _
                      ",0)"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
       End If
       While Not rstFR6900.EOF
          strSelect = "SELECT FR59CODORDFABR_SEQUENCE.nextval FROM dual"
          Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
          strinsert = "insert into fr5900 " & _
                      "  (FR59CODORDFABR, " & _
                      "  FR59OBSORDEN,    " & _
                      "  FR73CODPRODUCTO, " & _
                      "  FR93CODUNIMEDIDA, " & _
                      "  FR59CANTFABRICAR, " & _
                      "  FR37CODESTOF, " & _
                      "  FR66CODPETICION, " & _
                      "  FR28NUMLINEA, " & _
                      "  FR59DESPROCESO,FR59INDPAC) "
          strinsert = strinsert & _
                      "Values " & _
                      "  (" & rsta.rdoColumns(0).Value & "," & _
                      "  NULL," & _
                         codFormMagistral & "," & _
                      "'" & txtText1(49).Text & "'" & "," & _
                         objGen.ReplaceStr(txtText1(28).Text, ",", ".", 1) & "," & _
                      "1," & _
                         txtText1(0).Text & "," & _
                         txtText1(45).Text & "," & _
                      "'" & rstFR6900.rdoColumns("FR69DESPROCESO").Value & _
                      "',0)"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64

          strSelectFR7000 = "SELECT * " & _
                            " FROM FR7000 " & _
                            " WHERE FR69CODPROCFABRIC = " & rstFR6900.rdoColumns("FR69CODPROCFABRIC").Value

          Set rstFR7000 = objApp.rdoConnect.OpenResultset(strSelectFR7000)

          ' Crear una nueva Operaci�n por cada Operaci�n/Producto
          While Not rstFR7000.EOF ' Recorrer la tabla Proceso Operacion
             strSelectFR7100 = "SELECT * "
             strSelectFR7100 = strSelectFR7100 & "  FROM FR7100 WHERE "
             strSelectFR7100 = strSelectFR7100 & "  FR69CODPROCFABRIC = " & rstFR7000.rdoColumns("FR69CODPROCFABRIC").Value & " And "
             strSelectFR7100 = strSelectFR7100 & "  FR70CODOPERACION = " & rstFR7000.rdoColumns("FR70CODOPERACION").Value

             Set rstFR7100 = objApp.rdoConnect.OpenResultset(strSelectFR7100)
             While Not rstFR7100.EOF ' Recorrer la tabla Proceso Producto
                strinsert = "insert into fr2400 " & _
                            "  (FR59CODORDFABR, " & _
                            "  FR70CODOPERACION, " & _
                            "  FR70DESOPERACION ," & _
                            "  FR24TIEMPPREV, " & _
                            "  SG02COD, " & _
                            "  FR24TIEMPREAL, " & _
                            "  FR73CODPRODUCTO, " & _
                            "  FR24CANTNECESARIA, " & _
                            "  FR93CODUNIMEDIDA, " & _
                            "  FR37CODESTOF)" & _
                            "Values " & _
                            "  (" & rsta.rdoColumns(0).Value & "," & _
                            rstFR7000.rdoColumns("FR70CODOPERACION").Value & "," & _
                            "'" & rstFR7000.rdoColumns("FR70DESOPERACION").Value & "'" & "," & _
                            rstFR7000.rdoColumns("FR70TIEMPOPREPACI").Value & "," & _
                            "'" & objsecurity.strUser & "'" & ","
                If Not IsNull(rstFR7000.rdoColumns("FR70TIEMPEJECUCIO").Value) Then
                   strinsert = strinsert & rstFR7000.rdoColumns("FR70TIEMPEJECUCIO").Value & ","
                Else
                   strinsert = strinsert & " NULL,"
                End If
                strinsert = strinsert & rstFR7100.rdoColumns("FR73CODPRODUCTO").Value & ","
                auxcant = txtText1(28) * rstFR7100.rdoColumns("FR71CANTNECESARIA").Value
                strinsert = strinsert & objGen.ReplaceStr(auxcant, ",", ".", 1) & ","
                If Not IsNull(rstFR7100.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                   strinsert = strinsert & "'" & rstFR7100.rdoColumns("FR93CODUNIMEDIDA").Value & "'" & ","
                Else
                   strinsert = strinsert & " NULL,"
                End If
                strinsert = strinsert & "  1)"
                objApp.rdoConnect.Execute strinsert, 64
                objApp.rdoConnect.Execute "Commit", 64
                rstFR7100.MoveNext
             Wend
             rstFR7100.Close
             Set rstFR7100 = Nothing
             rstFR7000.MoveNext
          Wend
          rstFR7000.Close
          Set rstFR7000 = Nothing
          rstFR6900.MoveNext
       Wend
       rstFR6900.Close
       Set rstFR6900 = Nothing
    End If
    objApp.rdoConnect.Execute "UPDATE FR6600 SET FR26CODESTPETIC=14 WHERE FR66CODPETICION=" & txtText1(0).Text, 64
    objApp.rdoConnect.Execute "Commit", 64
  Else 'Servicio
    ' Mirar si existe una orden de fabricaci�n para este c�digo de petici�n
    strSelect = "SELECT COUNT(FR55CODNECESUNID) FROM FR5900 WHERE " & _
                " FR55CODNECESUNID=" & txtText1(67).Text & _
                " AND FR20NUMLINEA=" & txtText1(71).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
    If IsNull(rsta.rdoColumns(0).Value) Then
       ' Ok, NO EXISTE -> CREAR UNA NUEVA
       bCrearOFNueva = True
    ElseIf rsta.rdoColumns(0).Value = 0 Then
       ' Ok, NO EXISTE -> CREAR UNA NUEVA
       bCrearOFNueva = True
    Else
       ' Ok, SI EXISTE -> HACER REFERENCIA A LA QUE EXISTE
       bCrearOFNueva = False
    End If
    rsta.Close
    Set rsta = Nothing
    If bCrearOFNueva Then
       ' Crear una OF nueva por cada forma de hacer un producto
       strSelectFR6900 = " SELECT FR69CODPROCFABRIC, FR73CODPRODUCTO, FR69DESPROCESO FROM FR6900 " & _
                         " WHERE FR73CODPRODUCTO = " & codFormMagistral
       Set rstFR6900 = objApp.rdoConnect.OpenResultset(strSelectFR6900)
       If rstFR6900.EOF Then
          strinsert = "insert into fr5900 " & _
                      "  (FR59CODORDFABR, " & _
                      "  FR59OBSORDEN,    " & _
                      "  FR73CODPRODUCTO, " & _
                      "  FR93CODUNIMEDIDA, " & _
                      "  FR59CANTFABRICAR, " & _
                      "  FR37CODESTOF, " & _
                      "  FR55CODNECESUNID, " & _
                      "  FR20NUMLINEA, " & _
                      "  FR59DESPROCESO,FR59INDPAC) "
          strinsert = strinsert & _
                      "Values " & _
                      "  (FR59CODORDFABR_SEQUENCE.nextval," & _
                      "  NULL," & _
                         codFormMagistral & "," & _
                      "'" & txtText1(76).Text & "'" & "," & _
                         objGen.ReplaceStr(txtText1(73).Text, ",", ".", 1) & "," & _
                      "1," & _
                         txtText1(67).Text & "," & _
                         txtText1(71).Text & "," & _
                      "NULL" & _
                      ",-1)"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
       End If
       While Not rstFR6900.EOF
          strSelect = "SELECT FR59CODORDFABR_SEQUENCE.nextval FROM dual"
          Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
          strinsert = "insert into fr5900 " & _
                      "  (FR59CODORDFABR, " & _
                      "  FR59OBSORDEN,    " & _
                      "  FR73CODPRODUCTO, " & _
                      "  FR93CODUNIMEDIDA, " & _
                      "  FR59CANTFABRICAR, " & _
                      "  FR37CODESTOF, " & _
                      "  FR55CODNECESUNID, " & _
                      "  FR20NUMLINEA, " & _
                      "  FR59DESPROCESO,FR59INDPAC) "
          strinsert = strinsert & _
                      "Values " & _
                      "  (" & rsta.rdoColumns(0).Value & "," & _
                      "  NULL," & _
                         codFormMagistral & "," & _
                      "'" & txtText1(76).Text & "'" & "," & _
                         objGen.ReplaceStr(txtText1(73).Text, ",", ".", 1) & "," & _
                      "1," & _
                         txtText1(67).Text & "," & _
                         txtText1(71).Text & "," & _
                      "'" & rstFR6900.rdoColumns("FR69DESPROCESO").Value & _
                      "',-1)"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64

          strSelectFR7000 = "SELECT * " & _
                            " FROM FR7000 " & _
                            " WHERE FR69CODPROCFABRIC = " & rstFR6900.rdoColumns("FR69CODPROCFABRIC").Value

          Set rstFR7000 = objApp.rdoConnect.OpenResultset(strSelectFR7000)

          ' Crear una nueva Operaci�n por cada Operaci�n/Producto
          While Not rstFR7000.EOF ' Recorrer la tabla Proceso Operacion
             strSelectFR7100 = "SELECT * "
             strSelectFR7100 = strSelectFR7100 & "  FROM FR7100 WHERE "
             strSelectFR7100 = strSelectFR7100 & "  FR69CODPROCFABRIC = " & rstFR7000.rdoColumns("FR69CODPROCFABRIC").Value & " And "
             strSelectFR7100 = strSelectFR7100 & "  FR70CODOPERACION = " & rstFR7000.rdoColumns("FR70CODOPERACION").Value

             Set rstFR7100 = objApp.rdoConnect.OpenResultset(strSelectFR7100)
             While Not rstFR7100.EOF ' Recorrer la tabla Proceso Producto
                strinsert = "insert into fr2400 " & _
                            "  (FR59CODORDFABR, " & _
                            "  FR70CODOPERACION, " & _
                            "  FR70DESOPERACION ," & _
                            "  FR24TIEMPPREV, " & _
                            "  SG02COD, " & _
                            "  FR24TIEMPREAL, " & _
                            "  FR73CODPRODUCTO, " & _
                            "  FR24CANTNECESARIA, " & _
                            "  FR93CODUNIMEDIDA, " & _
                            "  FR37CODESTOF)" & _
                            "Values " & _
                            "  (" & rsta.rdoColumns(0).Value & "," & _
                            rstFR7000.rdoColumns("FR70CODOPERACION").Value & "," & _
                            "'" & rstFR7000.rdoColumns("FR70DESOPERACION").Value & "'" & "," & _
                            rstFR7000.rdoColumns("FR70TIEMPOPREPACI").Value & "," & _
                            "'" & objsecurity.strUser & "'" & ","
                If Not IsNull(rstFR7000.rdoColumns("FR70TIEMPEJECUCIO").Value) Then
                   strinsert = strinsert & rstFR7000.rdoColumns("FR70TIEMPEJECUCIO").Value & ","
                Else
                   strinsert = strinsert & " NULL,"
                End If
                strinsert = strinsert & rstFR7100.rdoColumns("FR73CODPRODUCTO").Value & ","
                auxcant = txtText1(73) * rstFR7100.rdoColumns("FR71CANTNECESARIA").Value
                strinsert = strinsert & objGen.ReplaceStr(auxcant, ",", ".", 1) & ","
                If Not IsNull(rstFR7100.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                   strinsert = strinsert & "'" & rstFR7100.rdoColumns("FR93CODUNIMEDIDA").Value & "'" & ","
                Else
                   strinsert = strinsert & " NULL,"
                End If
                strinsert = strinsert & "  1)"
                objApp.rdoConnect.Execute strinsert, 64
                objApp.rdoConnect.Execute "Commit", 64
                rstFR7100.MoveNext
             Wend
             rstFR7100.Close
             Set rstFR7100 = Nothing
             rstFR7000.MoveNext
          Wend
          rstFR7000.Close
          Set rstFR7000 = Nothing
          rstFR6900.MoveNext
       Wend
       rstFR6900.Close
       Set rstFR6900 = Nothing
    End If
    objApp.rdoConnect.Execute "UPDATE FR5500 SET FR26CODESTPETIC=14 WHERE FR55CODNECESUNID=" & txtText1(67).Text, 64
    objApp.rdoConnect.Execute "Commit", 64
  End If

End Sub
