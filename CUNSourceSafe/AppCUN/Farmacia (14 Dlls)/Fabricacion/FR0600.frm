VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form FrmVerPteFab 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Fabricaci�n. F�rmulas Magistrales Pendientes de Fabricar"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11520
   HelpContextID   =   30001
   Icon            =   "FR0600.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11520
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11520
      _ExtentX        =   20320
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdTerminar 
      Caption         =   "Terminar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   10080
      TabIndex        =   12
      Top             =   7680
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton cmdAsignar 
      Caption         =   "Asignar F�rm.Mag."
      Height          =   375
      Left            =   3240
      TabIndex        =   11
      Top             =   7680
      Width           =   1695
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6975
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   12303
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      ForeColor       =   8388736
      TabCaption(0)   =   "F�rmulas Magistrales No Normalizadas"
      TabPicture(0)   =   "FR0600.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Timer1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraFrame1(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "�rdenes de Fabricaci�n"
      TabPicture(1)   =   "FR0600.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(1)"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraFrame1 
         Caption         =   "Pendientes de dar de Alta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   6375
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   480
         Width           =   11085
         Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
            Height          =   2865
            Index           =   0
            Left            =   120
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   360
            Width           =   10815
            ScrollBars      =   3
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorEven   =   12632256
            BackColorOdd    =   12632256
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   19076
            _ExtentY        =   5054
            _StockProps     =   79
         End
         Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
            Height          =   2880
            Index           =   2
            Left            =   120
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   3360
            Width           =   10815
            ScrollBars      =   3
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorEven   =   12632256
            BackColorOdd    =   12632256
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   19076
            _ExtentY        =   5080
            _StockProps     =   79
         End
      End
      Begin VB.Timer Timer1 
         Left            =   4680
         Top             =   120
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Pendientes de Fabricar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   6375
         Index           =   1
         Left            =   -74880
         TabIndex        =   7
         Top             =   480
         Width           =   11085
         Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
            Height          =   2865
            Index           =   1
            Left            =   120
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   360
            Width           =   10815
            ScrollBars      =   3
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorEven   =   12632256
            BackColorOdd    =   12632256
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   19076
            _ExtentY        =   5054
            _StockProps     =   79
         End
         Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
            Height          =   2880
            Index           =   3
            Left            =   120
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   3360
            Width           =   10815
            ScrollBars      =   3
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorEven   =   12632256
            BackColorOdd    =   12632256
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   19076
            _ExtentY        =   5080
            _StockProps     =   79
         End
      End
   End
   Begin VB.CommandButton cmdFabricacion 
      Caption         =   "Fabricaci�n"
      Enabled         =   0   'False
      Height          =   375
      Left            =   5160
      TabIndex        =   1
      Top             =   7680
      Width           =   1695
   End
   Begin VB.CommandButton cmdVerOM 
      Caption         =   "Ver Petici�n / Anular "
      Height          =   375
      Left            =   1080
      TabIndex        =   0
      Top             =   7680
      Width           =   1695
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   8055
      Width           =   11520
      _ExtentX        =   20320
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   10680
      Top             =   360
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Visible         =   0   'False
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Visible         =   0   'False
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Visible         =   0   'False
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Visible         =   0   'False
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmVerPteFab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FrmVerPteFab (FR0600.FRM)                                    *
'* AUTOR: Aitor Vi�uela Garc�a                                          *
'* FECHA: Mayo DE 1999                                                  *
'* DESCRIPCION: F�rmulas Magistrales Pendientes                         *
'*             (ver peticiones de fabricaci�n)                          *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
'Dim WithEvents objSearch As clsCWSearch
Dim gstrWhere_PAC As String
Dim gstrWhere_SER As String


Private Sub auxDBGrid1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
  
  If Index = 0 Then
    auxDBGrid1(2).SelBookmarks.RemoveAll
  ElseIf Index = 2 Then
    auxDBGrid1(0).SelBookmarks.RemoveAll
  End If

  If Index = 1 Then
    auxDBGrid1(3).SelBookmarks.RemoveAll
  ElseIf Index = 3 Then
    auxDBGrid1(1).SelBookmarks.RemoveAll
  End If

End Sub

Private Sub auxDBGrid1_SelChange(Index As Integer, ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)

  If Index = 0 Then
    auxDBGrid1(2).SelBookmarks.RemoveAll
  ElseIf Index = 2 Then
    auxDBGrid1(0).SelBookmarks.RemoveAll
  End If

  If Index = 1 Then
    auxDBGrid1(3).SelBookmarks.RemoveAll
  ElseIf Index = 3 Then
    auxDBGrid1(1).SelBookmarks.RemoveAll
  End If

End Sub

Private Sub cmdasignar_Click()

cmdAsignar.Enabled = False
  
Timer1.Enabled = False

If auxDBGrid1(0).SelBookmarks.Count = 0 And auxDBGrid1(2).SelBookmarks.Count = 0 Then
  MsgBox "Debe seleccionar una linea", vbInformation, "Aviso"
  Timer1.Enabled = True
  cmdAsignar.Enabled = True
  Exit Sub
End If
  
If auxDBGrid1(0).SelBookmarks.Count > 0 Then 'petici�n
  gstrLlamador = "FrmVerPteFab.Pac"
  gcodigopeticion = auxDBGrid1(0).Columns("Petici�n").Value
  Call objsecurity.LaunchProcess("FR0610")
  gstrLlamador = ""
  auxDBGrid1(0).SelBookmarks.RemoveAll
End If

If auxDBGrid1(2).SelBookmarks.Count > 0 Then 'petici�n
  gstrLlamador = "FrmVerPteFab.Ser"
  gcodigopeticion = auxDBGrid1(2).Columns("C�d.Necesidad").Value
  Call objsecurity.LaunchProcess("FR0610")
  gstrLlamador = ""
  auxDBGrid1(2).SelBookmarks.RemoveAll
End If

Call refrescar
Timer1.Enabled = True

cmdAsignar.Enabled = True

End Sub

Private Sub cmdterminar_Click()

cmdTerminar.Enabled = False
Timer1.Enabled = False
If auxDBGrid1(1).SelBookmarks.Count = 0 And auxDBGrid1(3).SelBookmarks.Count = 0 Then
  MsgBox "Debe seleccionar una linea", vbInformation, "Aviso"
  Timer1.Enabled = True
  cmdTerminar.Enabled = True
  Exit Sub
End If

Call refrescar
Timer1.Enabled = True
cmdTerminar.Enabled = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los botones
' -----------------------------------------------

Private Sub cmdVerOM_Click()

cmdVerOM.Enabled = False
  
Timer1.Enabled = False

If auxDBGrid1(0).SelBookmarks.Count = 0 And auxDBGrid1(2).SelBookmarks.Count = 0 And auxDBGrid1(1).SelBookmarks.Count = 0 And auxDBGrid1(3).SelBookmarks.Count = 0 Then
  MsgBox "Debe seleccionar una linea", vbInformation, "Aviso"
  Timer1.Enabled = True
  cmdVerOM.Enabled = True
  Exit Sub
End If
  
If auxDBGrid1(0).SelBookmarks.Count > 0 Then 'petici�n
  gstrLlamador = "FrmVerPteFab.Pac.Ver"
  gcodigopeticion = auxDBGrid1(0).Columns("Petici�n").Value
  Call objsecurity.LaunchProcess("FR0610")
  gstrLlamador = ""
  auxDBGrid1(0).SelBookmarks.RemoveAll
End If

If auxDBGrid1(2).SelBookmarks.Count > 0 Then 'petici�n
  gstrLlamador = "FrmVerPteFab.Ser.Ver"
  gcodigopeticion = auxDBGrid1(2).Columns("C�d.Necesidad").Value
  Call objsecurity.LaunchProcess("FR0610")
  gstrLlamador = ""
  auxDBGrid1(2).SelBookmarks.RemoveAll
End If

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
If auxDBGrid1(1).SelBookmarks.Count > 0 Then 'Ordenes de Fabricaci�n Paciente
  gstrLlamador = "FrmVerPteFab.Pac.Ver"
  gcodigopeticion = auxDBGrid1(1).Columns("C�d.Pet.").Value
  Call objsecurity.LaunchProcess("FR0610")
  gstrLlamador = ""
  auxDBGrid1(1).SelBookmarks.RemoveAll
End If
If auxDBGrid1(3).SelBookmarks.Count > 0 Then  'Ordenes de Fabricaci�n Servicio
  gstrLlamador = "FrmVerPteFab.Ser.Ver"
  gcodigopeticion = auxDBGrid1(3).Columns("C�d.Nec.").Value
  Call objsecurity.LaunchProcess("FR0610")
  gstrLlamador = ""
  auxDBGrid1(3).SelBookmarks.RemoveAll
End If
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Call refrescar
Timer1.Enabled = True

cmdVerOM.Enabled = True

End Sub

Private Sub cmdFabricacion_Click()

  cmdFabricacion.Enabled = False
  Timer1.Enabled = False
If auxDBGrid1(1).SelBookmarks.Count = 0 And auxDBGrid1(3).SelBookmarks.Count = 0 Then
  MsgBox "Debe seleccionar una linea", vbInformation, "Aviso"
  Timer1.Enabled = True
  cmdFabricacion.Enabled = True
  Exit Sub
End If
  Call objsecurity.LaunchProcess("FR0601")
Call refrescar
Timer1.Enabled = True
  cmdFabricacion.Enabled = True

End Sub


Private Sub Form_Activate()
If SSTab1.Tab = 0 Then
    cmdAsignar.Visible = True
    cmdFabricacion.Visible = False
    cmdTerminar.Visible = False
    cmdVerOM.Visible = True
    cmdAsignar.Enabled = True
    cmdFabricacion.Enabled = False
    cmdTerminar.Enabled = False
    cmdVerOM.Enabled = True
Else
  cmdAsignar.Visible = False
  cmdFabricacion.Visible = True
  cmdTerminar.Visible = False
  cmdVerOM.Visible = True
  cmdAsignar.Enabled = False
  cmdFabricacion.Enabled = True
  cmdTerminar.Enabled = False
  cmdVerOM.Enabled = True
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
   Set objWinInfo = New clsCWWin

   Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                 Me, tlbToolbar1, stbStatusBar1, _
                                 cwWithAll)

   
Dim k, j, i As Integer

  For k = 1 To 29
    If k <> 6 Then
      tlbToolbar1.Buttons(k).Enabled = False
    End If
  Next k

auxDBGrid1(0).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
For j = 0 To 21
  Call auxDBGrid1(0).Columns.Add(j)
Next j
i = 0
auxDBGrid1(0).Columns(i).Caption = "Fecha"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Petici�n"
auxDBGrid1(0).Columns(i).Visible = False
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1300
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "F�rmula Magistral"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 3000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Cantidad"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 800
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "U.M.P."
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 600
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Archivo Inf. F�rmula Magistral"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 2500
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Historia"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 800
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Nombre"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Apellido 1�"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Apellido 2�"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Dr."
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Servicio"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Dosis"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "U.M."
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 600
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Frecuencia"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "V�a"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Periodicidad"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Fecha Inicio"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "H.I."
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 500
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Fecha Fin"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "H.F."
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 500
i = i + 1
auxDBGrid1(0).Columns(i).Caption = "Instrucciones para Administraci�n"
auxDBGrid1(0).Columns(i).Visible = True
auxDBGrid1(0).Columns(i).Locked = True
auxDBGrid1(0).Columns(i).Width = 4000

auxDBGrid1(0).Redraw = True

gstrWhere_PAC = " 1=1 "
'Call Refrescar_Grid_Pac_Nuevas

auxDBGrid1(2).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
For j = 0 To 7
  Call auxDBGrid1(2).Columns.Add(j)
Next j
i = 0
auxDBGrid1(2).Columns(i).Caption = "Fecha"
auxDBGrid1(2).Columns(i).Visible = True
auxDBGrid1(2).Columns(i).Locked = True
auxDBGrid1(2).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(2).Columns(i).Caption = "C�d.Necesidad"
auxDBGrid1(2).Columns(i).Visible = False
auxDBGrid1(2).Columns(i).Locked = True
auxDBGrid1(2).Columns(i).Width = 1300
i = i + 1
auxDBGrid1(2).Columns(i).Caption = "F�rmula Magistral"
auxDBGrid1(2).Columns(i).Visible = True
auxDBGrid1(2).Columns(i).Locked = True
auxDBGrid1(2).Columns(i).Width = 3000
i = i + 1
auxDBGrid1(2).Columns(i).Caption = "Cantidad"
auxDBGrid1(2).Columns(i).Visible = True
auxDBGrid1(2).Columns(i).Locked = True
auxDBGrid1(2).Columns(i).Width = 800
i = i + 1
auxDBGrid1(2).Columns(i).Caption = "U.M."
auxDBGrid1(2).Columns(i).Visible = True
auxDBGrid1(2).Columns(i).Locked = True
auxDBGrid1(2).Columns(i).Width = 600
i = i + 1
auxDBGrid1(2).Columns(i).Caption = "Archivo Inf. F�rmula Magistral"
auxDBGrid1(2).Columns(i).Visible = True
auxDBGrid1(2).Columns(i).Locked = True
auxDBGrid1(2).Columns(i).Width = 2500
i = i + 1
auxDBGrid1(2).Columns(i).Caption = "Servicio"
auxDBGrid1(2).Columns(i).Visible = True
auxDBGrid1(2).Columns(i).Locked = True
auxDBGrid1(2).Columns(i).Width = 2000
i = i + 1
auxDBGrid1(2).Columns(i).Caption = "Secci�n"
auxDBGrid1(2).Columns(i).Visible = True
auxDBGrid1(2).Columns(i).Locked = True
auxDBGrid1(2).Columns(i).Width = 2000
i = i + 1
auxDBGrid1(2).Columns(i).Caption = "Dr."
auxDBGrid1(2).Columns(i).Visible = True
auxDBGrid1(2).Columns(i).Locked = True
auxDBGrid1(2).Columns(i).Width = 1500


auxDBGrid1(2).Redraw = True

gstrWhere_SER = " 1=1 "

'Call Refrescar_Grid_Serv_Nuevas

auxDBGrid1(1).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
For j = 0 To 20
  Call auxDBGrid1(1).Columns.Add(j)
Next j
i = 0
auxDBGrid1(1).Columns(i).Caption = "Fecha"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "C�d.O.F."
auxDBGrid1(1).Columns(i).Visible = False
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "C�d.Prod."
auxDBGrid1(1).Columns(i).Visible = False
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "F�rmula"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 3000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "U.M."
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 600
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Cantidad"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 800
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "C�d.Estado"
auxDBGrid1(1).Columns(i).Visible = False
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 600
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Estado"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Proceso"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 2000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Historia"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 700
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Nombre"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Apellido 1�"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Apellido 2�"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Dr."
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Servicio"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Observaciones"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 2000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "C�d.Pet."
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Linea"
auxDBGrid1(1).Columns(i).Visible = True
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "C�d.Nec."
auxDBGrid1(1).Columns(i).Visible = False
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Lin.Nec."
auxDBGrid1(1).Columns(i).Visible = False
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(1).Columns(i).Caption = "Serv/Pac?"
auxDBGrid1(1).Columns(i).Visible = False
auxDBGrid1(1).Columns(i).Locked = True
auxDBGrid1(1).Columns(i).Width = 1000

auxDBGrid1(1).Redraw = True

gstrWhere_SER = " 1=1 "

'Call Refrescar_Grid_Pac_OrdenesFabricacion

auxDBGrid1(3).Redraw = False
'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
For j = 0 To 16
  Call auxDBGrid1(3).Columns.Add(j)
Next j
i = 0
auxDBGrid1(3).Columns(i).Caption = "Fecha"
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "C�d.O.F."
auxDBGrid1(3).Columns(i).Visible = False
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "C�d.Prod."
auxDBGrid1(3).Columns(i).Visible = False
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "F�rmula"
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 3000
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "U.M."
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 600
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "Cantidad"
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 800
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "C�d.Estado"
auxDBGrid1(3).Columns(i).Visible = False
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 600
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "Estado"
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "Proceso"
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 2000
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "Servicio"
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "Seccion"
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "Dr."
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1200
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "Observaciones"
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 2000
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "C�d.Pet."
auxDBGrid1(3).Columns(i).Visible = False
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "Linea"
auxDBGrid1(3).Columns(i).Visible = False
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "C�d.Nec."
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "Lin.Nec."
auxDBGrid1(3).Columns(i).Visible = True
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1000
i = i + 1
auxDBGrid1(3).Columns(i).Caption = "Serv/Pac?"
auxDBGrid1(3).Columns(i).Visible = False
auxDBGrid1(3).Columns(i).Locked = True
auxDBGrid1(3).Columns(i).Width = 1000

auxDBGrid1(3).Redraw = True

gstrWhere_SER = " 1=1 "

'Call Refrescar_Grid_Serv_OrdenesFabricacion

Call refrescar
Timer1.Interval = 65535 ' Set interval.
Timer1.Enabled = True

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
'   intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
'   intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_Unload(intCancel As Integer)
'   Call objWinInfo.WinDeRegister
'   Call objWinInfo.WinRemoveInfo
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean

Call objWinInfo.FormPrinterDialog(True, "")
Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
intReport = objPrinter.Selected
If intReport > 0 Then
  Select Case intReport
    Case 1
      Call Imprimir("FR1393.RPT", 0)
  End Select
End If
Set objPrinter = Nothing
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)

  If SSTab1.Tab = 0 Then
    cmdAsignar.Visible = True
    cmdFabricacion.Visible = False
    cmdTerminar.Visible = False
    cmdVerOM.Visible = True
    cmdAsignar.Enabled = True
    cmdFabricacion.Enabled = False
    cmdTerminar.Enabled = False
    cmdVerOM.Enabled = True
  Else
    cmdAsignar.Visible = False
    cmdFabricacion.Visible = True
    cmdTerminar.Visible = False
    cmdVerOM.Visible = True
    cmdAsignar.Enabled = False
    cmdFabricacion.Enabled = True
    cmdTerminar.Enabled = False
    cmdVerOM.Enabled = True
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
'   Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



Private Sub Timer1_Timer()
  Call refrescar
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
If btnButton.Index <> 6 Then
   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
Else
   Call Imprimir("FR1393.RPT", 0)
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)

   Select Case intIndex
   Case 10
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
   Case 20
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
   Case 40
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
   Case 60
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
   Case 80
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
   Case 100
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
   End Select

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
'   Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
'   Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
'   Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
'   Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
'   Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
'   Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
'   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
'   Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
'   Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
'   Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
'   Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
'   Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
'   Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
'   Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
'   Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
'   Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
'   Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
'   Call objWinInfo.CtrlLostFocus
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
'   Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
'   Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
'   Call objWinInfo.CtrlDataChange
'   ' Estado Petici�n
'   If intIndex = 14 Then
'      Select Case txtText1(14).Text
'      Case 0
'         txtEstadoFabricacion.Text = "Pte Fabricar"
'      Case 1
'         txtEstadoFabricacion.Text = "Fabricaci�n Iniciada"
'      Case 2
'         txtEstadoFabricacion.Text = "Fabricaci�n Finalizada"
'      Case Else
'         txtEstadoFabricacion.Text = ""
'      End Select
'   End If
End Sub


Private Sub Refrescar_Grid_Serv_Nuevas()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim Departamento As String
Dim Seccion As String
Dim Medico As String
Dim strAux As String
Dim rstaux As rdoResultset
Dim strpeticion As String
Dim rstpeticion As rdoResultset

Me.Enabled = False
Screen.MousePointer = vbHourglass

auxDBGrid1(2).Redraw = False
auxDBGrid1(2).RemoveAll
  
strSelect = "SELECT "
strSelect = strSelect & " FR2000.FR55CODNECESUNID"
strSelect = strSelect & ",FR2000.FR20DESFORMAG"
strSelect = strSelect & ",FR2000.FR20CANTNECESQUIR"
strSelect = strSelect & ",FR2000.FR93CODUNIMEDIDA"
strSelect = strSelect & ",FR2000.FR20PATHFORMAG"

strSelect = strSelect & " FROM FR2000,FR5500"
strSelect = strSelect & " WHERE "

strSelect = strSelect & " FR2000.FR55CODNECESUNID=FR5500.FR55CODNECESUNID"
strSelect = strSelect & " AND FR5500.FR26CODESTPETIC=3"
strSelect = strSelect & "  AND FR2000.FR73CODPRODUCTO=999999999 AND FR5500.FR55INDFM=-1 AND NVL(FR2000.FR20INDBLOQUEO,0)=0 AND " & gstrWhere_SER

strSelect = strSelect & " ORDER BY FR55CODNECESUNID DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF
 
  strpeticion = "SELECT AD02CODDPTO,AD41CODSECCION,FR55FECENVIO,SG02COD_FIR"
  strpeticion = strpeticion & " FROM FR5500 WHERE "
  strpeticion = strpeticion & " FR55CODNECESUNID=" & rsta("FR55CODNECESUNID").Value
  Set rstpeticion = objApp.rdoConnect.OpenResultset(strpeticion)
  If Not IsNull(rstpeticion("AD02CODDPTO").Value) Then
    strAux = "SELECT AD02DESDPTO FROM AD0200 WHERE "
    strAux = strAux & " AD02CODDPTO=" & rstpeticion("AD02CODDPTO").Value
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Departamento = rstaux("AD02DESDPTO").Value
  Else
    Departamento = " "
  End If
  If Not IsNull(rstpeticion("AD41CODSECCION").Value) Then
    strAux = "SELECT AD41DESSECCION FROM AD4100 WHERE "
    strAux = strAux & " AD41CODSECCION=" & rstpeticion("AD41CODSECCION").Value
    strAux = strAux & " AND "
    strAux = strAux & " AD02CODDPTO=" & rstpeticion("AD02CODDPTO").Value
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Seccion = rstaux("AD41DESSECCION").Value
  Else
    Seccion = " "
  End If
  If Not IsNull(rstpeticion("SG02COD_FIR").Value) Then
    strAux = "SELECT SG02APE1 FROM SG0200 WHERE "
    strAux = strAux & " SG02COD=" & "'" & rstpeticion("SG02COD_FIR").Value & "'"
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Medico = rstaux("SG02APE1").Value
  Else
    Medico = " "
  End If
  rstaux.Close
  Set rstaux = Nothing
  
  strlinea = rstpeticion("FR55FECENVIO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR55CODNECESUNID").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR20DESFORMAG").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR20CANTNECESQUIR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR20PATHFORMAG").Value & Chr(vbKeyTab)
  strlinea = strlinea & Departamento & Chr(vbKeyTab)
  strlinea = strlinea & Seccion & Chr(vbKeyTab)
  strlinea = strlinea & Medico & Chr(vbKeyTab)
  
  auxDBGrid1(2).AddItem strlinea
  rsta.MoveNext
  rstpeticion.Close
  Set rstpeticion = Nothing
Wend
qrya.Close
Set qrya = Nothing
auxDBGrid1(2).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Refrescar_Grid_Pac_Nuevas()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim strpeticion As String
Dim rstpeticion As rdoResultset
Dim rstaux As rdoResultset
Dim strAux As String
Dim Medico As String
Dim dpto As String
Dim NumHistoria As String
Dim NombrePac As String
Dim PriApel As String
Dim SegApel As String

Me.Enabled = False
Screen.MousePointer = vbHourglass

auxDBGrid1(0).Redraw = False
auxDBGrid1(0).RemoveAll
  
strSelect = "SELECT "
strSelect = strSelect & " FR2800.FR66CODPETICION"
strSelect = strSelect & ",FR2800.FR28DESPRODUCTO"
strSelect = strSelect & ",FR2800.FR28CANTIDAD"
strSelect = strSelect & ",FR2800.FR93CODUNIMEDIDA_2"
strSelect = strSelect & ",FR2800.FR28DOSIS"
strSelect = strSelect & ",FR2800.FR93CODUNIMEDIDA"
strSelect = strSelect & ",FR2800.FRG4CODFRECUENCIA"
strSelect = strSelect & ",FR2800.FR34CODVIA"
strSelect = strSelect & ",FR2800.FRH5CODPERIODICIDAD"
strSelect = strSelect & ",FR2800.FR28PATHFORMAG"
strSelect = strSelect & ",FR2800.FR28FECINICIO"
strSelect = strSelect & ",FR2800.FR28HORAINICIO"
strSelect = strSelect & ",FR2800.FR28FECFIN"
strSelect = strSelect & ",FR2800.FR28HORAFIN"
strSelect = strSelect & ",FR2800.FR28INSTRADMIN"

strSelect = strSelect & " FROM FR2800,FR6600"
strSelect = strSelect & " WHERE "

strSelect = strSelect & " FR2800.FR66CODPETICION=FR6600.FR66CODPETICION"
strSelect = strSelect & " AND FR6600.FR26CODESTPETIC=3"
strSelect = strSelect & " AND FR6600.FR66INDFM=-1 AND FR2800.FR73CODPRODUCTO IS NULL AND FR2800.FR28OPERACION='L' AND NVL(FR2800.FR28INDBLOQUEADA,0)=0 AND " & gstrWhere_PAC

strSelect = strSelect & " ORDER BY FR66CODPETICION DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF
  
  'Paciente,M�dico que firma,Fecha y Hora de Firma,Dpto.solicitante
  strpeticion = "SELECT CI21CODPERSONA,SG02COD_FEN,FR66FECFIRMMEDI,"
  strpeticion = strpeticion & "FR66HORAFIRMMEDI,AD02CODDPTO FROM FR6600 WHERE "
  strpeticion = strpeticion & " FR66CODPETICION=" & rsta("FR66CODPETICION").Value
  Set rstpeticion = objApp.rdoConnect.OpenResultset(strpeticion)
  'Persona
  strAux = "SELECT CI22NUMHISTORIA,CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 "
  strAux = strAux & " WHERE CI21CODPERSONA=" & rstpeticion.rdoColumns("CI21CODPERSONA").Value
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not IsNull(rstaux.rdoColumns("CI22NUMHISTORIA").Value) Then
    NumHistoria = rstaux.rdoColumns("CI22NUMHISTORIA").Value
  Else
    NumHistoria = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22NOMBRE").Value) Then
    NombrePac = rstaux.rdoColumns("CI22NOMBRE").Value
  Else
    NombrePac = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22PRIAPEL").Value) Then
    PriApel = rstaux.rdoColumns("CI22PRIAPEL").Value
  Else
    PriApel = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22SEGAPEL").Value) Then
    SegApel = rstaux.rdoColumns("CI22SEGAPEL").Value
  Else
    SegApel = " "
  End If
  'M�dico que firma la petici�n
  strAux = "SELECT SG02APE1,SG02NUMCOLEGIADO FROM SG0200 WHERE "
  strAux = strAux & " SG02COD=" & "'" & rstpeticion.rdoColumns("SG02COD_FEN").Value & "'"
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not rstaux.EOF Then
    If Not IsNull(rstaux.rdoColumns("SG02APE1").Value) Then
      Medico = rstaux.rdoColumns("SG02APE1").Value
    Else
      Medico = " "
    End If
  Else
    Medico = " "
  End If
  'Departamento
  strAux = "SELECT AD02DESDPTO FROM AD0200 WHERE "
  strAux = strAux & " AD02CODDPTO=" & rstpeticion.rdoColumns("AD02CODDPTO").Value
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not IsNull(rstaux.rdoColumns("AD02DESDPTO").Value) Then
    dpto = rstaux.rdoColumns("AD02DESDPTO").Value
  Else
    dpto = " "
  End If
  rstaux.Close
  Set rstaux = Nothing

  strlinea = rstpeticion("FR66FECFIRMMEDI").Value & Chr(vbKeyTab)
  'strlinea = strlinea & rsta("FR66HORAFIRMMEDI").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66CODPETICION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28DESPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28CANTIDAD").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA_2").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28PATHFORMAG").Value & Chr(vbKeyTab)
  strlinea = strlinea & NumHistoria & Chr(vbKeyTab)
  strlinea = strlinea & NombrePac & Chr(vbKeyTab)
  strlinea = strlinea & PriApel & Chr(vbKeyTab)
  strlinea = strlinea & SegApel & Chr(vbKeyTab)
  strlinea = strlinea & Medico & Chr(vbKeyTab)
  strlinea = strlinea & dpto & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28DOSIS").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRG4CODFRECUENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR34CODVIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRH5CODPERIODICIDAD").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28FECINICIO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28HORAINICIO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28FECFIN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28HORAFIN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28INSTRADMIN").Value
  auxDBGrid1(0).AddItem strlinea
  rsta.MoveNext
rstpeticion.Close
Set rstpeticion = Nothing
Wend
qrya.Close
Set qrya = Nothing
auxDBGrid1(0).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Refrescar_Grid_Serv_OrdenesFabricacion()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim strformula As String
Dim rstprod As rdoResultset
Dim strprod As String
Dim strestado As String
Dim rstEstado As rdoResultset
Dim Estado As String
Dim strpeticion As String
Dim rstpeticion As rdoResultset
Dim rstaux As rdoResultset
Dim strAux As String
Dim Medico As String
Dim dpto As String
Dim Seccion As String
Dim Departamento As String


Me.Enabled = False
Screen.MousePointer = vbHourglass

auxDBGrid1(3).Redraw = False
auxDBGrid1(3).RemoveAll
  
strSelect = "SELECT "
strSelect = strSelect & "FR5900.FR59CODORDFABR"
strSelect = strSelect & ",FR5900.FR59OBSORDEN"
strSelect = strSelect & ",FR5900.FR73CODPRODUCTO"
strSelect = strSelect & ",FR5900.FR93CODUNIMEDIDA"
strSelect = strSelect & ",FR5900.FR59CANTFABRICAR"
strSelect = strSelect & ",FR5900.FR37CODESTOF"
strSelect = strSelect & ",FR5900.FR66CODPETICION"
strSelect = strSelect & ",FR5900.FR28NUMLINEA"
strSelect = strSelect & ",FR5900.FR59DESPROCESO"
strSelect = strSelect & ",FR5900.FR55CODNECESUNID"
strSelect = strSelect & ",FR5900.FR20NUMLINEA"
strSelect = strSelect & ",FR5900.FR59INDPAC"

strSelect = strSelect & " FROM FR5900"
strSelect = strSelect & " WHERE "
strSelect = strSelect & " FR5900.FR59INDPAC=-1 AND FR5900.FR37CODESTOF IN (1,3) AND FR5900.FR55CODNECESUNID IS NOT NULL"

strSelect = strSelect & " ORDER BY FR59CODORDFABR DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF

  'descripci�n de la F�rmula
  strprod = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO="
  strprod = strprod & rsta("FR73CODPRODUCTO").Value
  Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
  If Not rstprod.EOF Then
    If Not IsNull(rstprod.rdoColumns("FR73DESPRODUCTO").Value) Then
      strformula = rstprod.rdoColumns("FR73DESPRODUCTO").Value
    Else
      strformula = " "
    End If
  Else
    strformula = " "
  End If
  rstprod.Close
  Set rstprod = Nothing
  
  'descripci�n del estado
  strestado = "SELECT FR37DESESTOF FROM FR3700 WHERE"
  strestado = strestado & " FR37CODESTOF=" & rsta("FR37CODESTOF").Value
  Set rstEstado = objApp.rdoConnect.OpenResultset(strestado)
  If Not rstEstado.EOF Then
    If Not IsNull(rstEstado.rdoColumns("FR37DESESTOF").Value) Then
      Estado = rstEstado.rdoColumns("FR37DESESTOF").Value
    Else
      Estado = " "
    End If
  Else
    Estado = " "
  End If
  rstEstado.Close
  Set rstEstado = Nothing

  'departamento,secci�n,fecha de env�o y m�dico que firma
  strpeticion = "SELECT AD02CODDPTO,AD41CODSECCION,FR55FECENVIO,SG02COD_FIR"
  strpeticion = strpeticion & " FROM FR5500 WHERE "
  strpeticion = strpeticion & " FR55CODNECESUNID=" & rsta("FR55CODNECESUNID").Value
  Set rstpeticion = objApp.rdoConnect.OpenResultset(strpeticion)
  If Not IsNull(rstpeticion("AD02CODDPTO").Value) Then
    strAux = "SELECT AD02DESDPTO FROM AD0200 WHERE "
    strAux = strAux & " AD02CODDPTO=" & rstpeticion("AD02CODDPTO").Value
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Departamento = rstaux("AD02DESDPTO").Value
  Else
    Departamento = " "
  End If
  If Not IsNull(rstpeticion("AD41CODSECCION").Value) Then
    strAux = "SELECT AD41DESSECCION FROM AD4100 WHERE "
    strAux = strAux & " AD41CODSECCION=" & rstpeticion("AD41CODSECCION").Value
    strAux = strAux & " AND "
    strAux = strAux & " AD02CODDPTO=" & rstpeticion("AD02CODDPTO").Value
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Seccion = rstaux("AD41DESSECCION").Value
  Else
    Seccion = " "
  End If
  If Not IsNull(rstpeticion("SG02COD_FIR").Value) Then
    strAux = "SELECT SG02APE1 FROM SG0200 WHERE "
    strAux = strAux & " SG02COD=" & "'" & rstpeticion("SG02COD_FIR").Value & "'"
    Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
    Medico = rstaux("SG02APE1").Value
  Else
    Medico = " "
  End If
  rstaux.Close
  Set rstaux = Nothing
  
  strlinea = rstpeticion.rdoColumns("FR55FECENVIO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR59CODORDFABR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & strformula & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR59CANTFABRICAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR37CODESTOF").Value & Chr(vbKeyTab)
  strlinea = strlinea & Estado & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR59DESPROCESO").Value & Chr(vbKeyTab)
  strlinea = strlinea & Departamento & Chr(vbKeyTab)
  strlinea = strlinea & Seccion & Chr(vbKeyTab)
  strlinea = strlinea & Medico & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR59OBSORDEN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66CODPETICION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28NUMLINEA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR55CODNECESUNID").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR20NUMLINEA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR59INDPAC").Value
  
  auxDBGrid1(3).AddItem strlinea
  rsta.MoveNext
  rstpeticion.Close
  Set rstpeticion = Nothing
Wend
qrya.Close
Set qrya = Nothing
auxDBGrid1(3).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub Refrescar_Grid_Pac_OrdenesFabricacion()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String
Dim strformula As String
Dim rstprod As rdoResultset
Dim strprod As String
Dim strestado As String
Dim rstEstado As rdoResultset
Dim Estado As String
Dim strpeticion As String
Dim rstpeticion As rdoResultset
Dim rstaux As rdoResultset
Dim strAux As String
Dim Medico As String
Dim dpto As String
Dim NumHistoria As String
Dim NombrePac As String
Dim PriApel As String
Dim SegApel As String

Me.Enabled = False
Screen.MousePointer = vbHourglass

auxDBGrid1(1).Redraw = False
auxDBGrid1(1).RemoveAll
  
strSelect = "SELECT "
strSelect = strSelect & "FR5900.FR59CODORDFABR"
strSelect = strSelect & ",FR5900.FR59OBSORDEN"
strSelect = strSelect & ",FR5900.FR73CODPRODUCTO"
strSelect = strSelect & ",FR5900.FR93CODUNIMEDIDA"
strSelect = strSelect & ",FR5900.FR59CANTFABRICAR"
strSelect = strSelect & ",FR5900.FR37CODESTOF"
strSelect = strSelect & ",FR5900.FR66CODPETICION"
strSelect = strSelect & ",FR5900.FR28NUMLINEA"
strSelect = strSelect & ",FR5900.FR59DESPROCESO"
strSelect = strSelect & ",FR55CODNECESUNID"
strSelect = strSelect & ",FR20NUMLINEA"
strSelect = strSelect & ",FR59INDPAC"

strSelect = strSelect & " FROM FR5900"
strSelect = strSelect & " WHERE "
strSelect = strSelect & " FR5900.FR59INDPAC=0 AND FR5900.FR37CODESTOF IN (1,3) AND FR5900.FR66CODPETICION IS NOT NULL"

strSelect = strSelect & " ORDER BY FR59CODORDFABR DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF
  'descripci�n de la f�rmula
  strprod = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO="
  strprod = strprod & rsta("FR73CODPRODUCTO").Value
  Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
  If Not rstprod.EOF Then
    If Not IsNull(rstprod.rdoColumns("FR73DESPRODUCTO").Value) Then
      strformula = rstprod.rdoColumns("FR73DESPRODUCTO").Value
    Else
      strformula = " "
    End If
  Else
    strformula = " "
  End If
  rstprod.Close
  Set rstprod = Nothing
  
  'descripci�n del estado
  strestado = "SELECT FR37DESESTOF FROM FR3700 WHERE"
  strestado = strestado & " FR37CODESTOF=" & rsta("FR37CODESTOF").Value
  Set rstEstado = objApp.rdoConnect.OpenResultset(strestado)
  If Not rstEstado.EOF Then
    If Not IsNull(rstEstado.rdoColumns("FR37DESESTOF").Value) Then
      Estado = rstEstado.rdoColumns("FR37DESESTOF").Value
    Else
      Estado = " "
    End If
  Else
    Estado = " "
  End If
  rstEstado.Close
  Set rstEstado = Nothing
  
  'Paciente,M�dico que firma,Fecha y Hora de Firma,Dpto.solicitante
  strpeticion = "SELECT CI21CODPERSONA,SG02COD_FEN,FR66FECFIRMMEDI,"
  strpeticion = strpeticion & "FR66HORAFIRMMEDI,AD02CODDPTO FROM FR6600 WHERE "
  strpeticion = strpeticion & " FR66CODPETICION=" & rsta("FR66CODPETICION").Value
  Set rstpeticion = objApp.rdoConnect.OpenResultset(strpeticion)
  'Persona
  strAux = "SELECT CI22NUMHISTORIA,CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 "
  strAux = strAux & " WHERE CI21CODPERSONA=" & rstpeticion.rdoColumns("CI21CODPERSONA").Value
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not IsNull(rstaux.rdoColumns("CI22NUMHISTORIA").Value) Then
    NumHistoria = rstaux.rdoColumns("CI22NUMHISTORIA").Value
  Else
    NumHistoria = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22NOMBRE").Value) Then
    NombrePac = rstaux.rdoColumns("CI22NOMBRE").Value
  Else
    NombrePac = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22PRIAPEL").Value) Then
    PriApel = rstaux.rdoColumns("CI22PRIAPEL").Value
  Else
    PriApel = " "
  End If
  If Not IsNull(rstaux.rdoColumns("CI22SEGAPEL").Value) Then
    SegApel = rstaux.rdoColumns("CI22SEGAPEL").Value
  Else
    SegApel = " "
  End If
  'M�dico que firma la petici�n
  strAux = "SELECT SG02APE1,SG02NUMCOLEGIADO FROM SG0200 WHERE "
  strAux = strAux & " SG02COD=" & "'" & rstpeticion.rdoColumns("SG02COD_FEN").Value & "'"
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not rstaux.EOF Then
    If Not IsNull(rstaux.rdoColumns("SG02APE1").Value) Then
      Medico = rstaux.rdoColumns("SG02APE1").Value
    Else
      Medico = " "
    End If
  Else
    Medico = " "
  End If
  'Departamento
  strAux = "SELECT AD02DESDPTO FROM AD0200 WHERE "
  strAux = strAux & " AD02CODDPTO=" & rstpeticion.rdoColumns("AD02CODDPTO").Value
  Set rstaux = objApp.rdoConnect.OpenResultset(strAux)
  If Not IsNull(rstaux.rdoColumns("AD02DESDPTO").Value) Then
    dpto = rstaux.rdoColumns("AD02DESDPTO").Value
  Else
    dpto = " "
  End If
  rstaux.Close
  Set rstaux = Nothing
  
  strlinea = rstpeticion("FR66FECFIRMMEDI").Value & Chr(vbKeyTab)
  'strlinea = strlinea & rsta("FR66HORAFIRMMEDI").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR59CODORDFABR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & strformula & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR59CANTFABRICAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR37CODESTOF").Value & Chr(vbKeyTab)
  strlinea = strlinea & Estado & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR59DESPROCESO").Value & Chr(vbKeyTab)
  strlinea = strlinea & NumHistoria & Chr(vbKeyTab)
  strlinea = strlinea & NombrePac & Chr(vbKeyTab)
  strlinea = strlinea & PriApel & Chr(vbKeyTab)
  strlinea = strlinea & SegApel & Chr(vbKeyTab)
  strlinea = strlinea & Medico & Chr(vbKeyTab)
  strlinea = strlinea & dpto & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR59OBSORDEN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR66CODPETICION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR28NUMLINEA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR55CODNECESUNID").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR20NUMLINEA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR59INDPAC").Value
  auxDBGrid1(1).AddItem strlinea
  rsta.MoveNext
rstpeticion.Close
Set rstpeticion = Nothing
Wend
qrya.Close
Set qrya = Nothing
auxDBGrid1(1).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub refrescar()
Dim aux As Integer

  Call Refrescar_Grid_Pac_Nuevas
  Call Refrescar_Grid_Serv_Nuevas
  Call Refrescar_Grid_Pac_OrdenesFabricacion
  Call Refrescar_Grid_Serv_OrdenesFabricacion
  
  aux = auxDBGrid1(0).Rows + auxDBGrid1(2).Rows
  SSTab1.TabCaption(0) = "F�rmulas Magistrales No Normalizadas : " & aux
  aux = auxDBGrid1(1).Rows + auxDBGrid1(3).Rows
  SSTab1.TabCaption(1) = "Ordenes de Fabricaci�n : " & aux

End Sub


Private Sub Imprimir(strListado As String, intDes As Integer)
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String

  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword '"tuy" 'objsecurity.GetDataBasePasswordOld
  strPATH = "C:\Archivos de programa\cun\rpt\"
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  'se crea la where
  strWhere = strWhere & "-1=-1"
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp1
  CrystalReport1.Action = 1

Err_imp1:
  
End Sub
