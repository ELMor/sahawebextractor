VERSION 5.00
Begin VB.Form FrmPrincipal 
   Caption         =   "FARMACIA. Funci�n Gestionar Compras"
   ClientHeight    =   8595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11595
   LinkTopic       =   "Form1"
   ScaleHeight     =   8595
   ScaleWidth      =   11595
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame2 
      Caption         =   "Gestionar Fabricaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   2640
      TabIndex        =   0
      Top             =   1560
      Width           =   6135
      Begin VB.CommandButton Command1 
         Caption         =   "Reenvasado"
         Height          =   375
         Index           =   2
         Left            =   1680
         TabIndex        =   3
         Top             =   1320
         Width           =   2775
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Mezclas"
         Height          =   375
         Index           =   1
         Left            =   1680
         TabIndex        =   2
         Top             =   840
         Width           =   2775
      End
      Begin VB.CommandButton Command1 
         Caption         =   "F�rmulas Magistrales"
         Height          =   375
         Index           =   0
         Left            =   1680
         TabIndex        =   1
         Top             =   360
         Width           =   2775
      End
   End
End
Attribute VB_Name = "FrmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click(Index As Integer)
  Select Case Index
    Case 0
      Call FrmVerPteFab.Show(vbModal)
    Case 1
      Call FrmCrtlFabMez.Show(vbModal)
    Case 2
      Call FrmReenvasado.Show(vbModal)
  End Select
End Sub

