VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmVerCargaCarroMez 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Peticiones de Mezclas en Carros"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Peticiones de Mezclas del Carro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3960
      Index           =   0
      Left            =   0
      TabIndex        =   14
      Top             =   4080
      Width           =   11895
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3465
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   360
         Width           =   11625
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "BLANCO"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "FR0607.frx":0000
         stylesets(1).Name=   "AZUL"
         stylesets(1).BackColor=   16776960
         stylesets(1).Picture=   "FR0607.frx":001C
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20505
         _ExtentY        =   6112
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Mezclas del Carro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2040
      Index           =   2
      Left            =   6720
      TabIndex        =   20
      Top             =   1920
      Visible         =   0   'False
      Width           =   5175
      Begin TabDlg.SSTab tabTab1 
         Height          =   1455
         Index           =   1
         Left            =   240
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   480
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   2566
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0607.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(7)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(9)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(10)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(11)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(8)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(9)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(10)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(11)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(12)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(13)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(14)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0607.frx":0054
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR32DIATOMAULT"
            Height          =   330
            Index           =   14
            Left            =   2040
            TabIndex        =   35
            Tag             =   "Dia Ult."
            Top             =   960
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR32HORATOMAULT"
            Height          =   330
            Index           =   13
            Left            =   1080
            TabIndex        =   33
            Tag             =   "Hora Ult."
            Top             =   960
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR28NUMLINEA"
            Height          =   330
            Index           =   12
            Left            =   240
            TabIndex        =   31
            Tag             =   "Linea"
            Top             =   960
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR66CODPETICION"
            Height          =   330
            Index           =   11
            Left            =   2640
            TabIndex        =   29
            Tag             =   "Peticion"
            Top             =   360
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   10
            Left            =   1080
            TabIndex        =   27
            Tag             =   "Persona"
            Top             =   360
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD15CODCAMA"
            Height          =   330
            Index           =   9
            Left            =   2040
            TabIndex        =   25
            Tag             =   "Cama"
            Top             =   360
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR07CODCARRO"
            Height          =   330
            Index           =   8
            Left            =   240
            TabIndex        =   23
            Tag             =   "C�d.Carro"
            Top             =   360
            Width           =   400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   945
            Index           =   1
            Left            =   -74880
            TabIndex        =   22
            Top             =   120
            Width           =   4215
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   7435
            _ExtentY        =   1667
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dia Ult."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   2040
            TabIndex        =   36
            Top             =   720
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora Ult."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   1080
            TabIndex        =   34
            Top             =   720
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Linea"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   240
            TabIndex        =   32
            Top             =   720
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Peticion"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   2640
            TabIndex        =   30
            Top             =   120
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   1080
            TabIndex        =   28
            Top             =   120
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cama"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   2040
            TabIndex        =   26
            Top             =   120
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   240
            TabIndex        =   24
            Top             =   120
            Width           =   465
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Carros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3615
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   480
      Width           =   11580
      Begin TabDlg.SSTab tabTab1 
         Height          =   3135
         Index           =   0
         Left            =   240
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   5530
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0607.frx":0070
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(7)"
         Tab(0).Control(1)=   "txtText1(6)"
         Tab(0).Control(2)=   "txtText1(5)"
         Tab(0).Control(3)=   "txtText1(2)"
         Tab(0).Control(4)=   "txtText1(1)"
         Tab(0).Control(5)=   "txtText1(0)"
         Tab(0).Control(6)=   "txtText1(4)"
         Tab(0).Control(7)=   "txtText1(3)"
         Tab(0).Control(8)=   "lblLabel1(6)"
         Tab(0).Control(9)=   "lblLabel1(1)"
         Tab(0).Control(10)=   "lblLabel1(3)"
         Tab(0).Control(11)=   "lblLabel1(0)"
         Tab(0).Control(12)=   "lblLabel1(4)"
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0607.frx":008C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74HORASALIDA"
            Height          =   330
            Index           =   7
            Left            =   -71760
            TabIndex        =   7
            Tag             =   "Hora"
            Top             =   2640
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74DIA"
            Height          =   330
            Index           =   6
            Left            =   -74640
            TabIndex        =   6
            Tag             =   "D�a"
            Top             =   2640
            Width           =   1935
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR87DESESTCARRO"
            Height          =   330
            Index           =   5
            Left            =   -74040
            TabIndex        =   5
            Tag             =   "Estado Carro"
            Top             =   1920
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR87CODESTCARRO"
            Height          =   330
            Index           =   2
            Left            =   -74640
            TabIndex        =   4
            Tag             =   "C�s.Estado Carro"
            Top             =   1920
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD02DESDPTO"
            Height          =   330
            Index           =   1
            Left            =   -74040
            TabIndex        =   1
            Tag             =   "Servicio"
            Top             =   480
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   0
            Left            =   -74640
            TabIndex        =   0
            Tag             =   "C�d.Servicio"
            Top             =   480
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR07DESCARRO"
            Height          =   330
            Index           =   4
            Left            =   -74040
            TabIndex        =   3
            Tag             =   "Carro"
            Top             =   1200
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR07CODCARRO"
            Height          =   330
            Index           =   3
            Left            =   -74640
            TabIndex        =   2
            Tag             =   "C�d.Carro"
            Top             =   1200
            Width           =   400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3105
            Index           =   2
            Left            =   0
            TabIndex        =   12
            Top             =   0
            Width           =   11055
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19500
            _ExtentY        =   5477
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   -71760
            TabIndex        =   19
            Top             =   2400
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "D�a Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -74640
            TabIndex        =   18
            Top             =   2400
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   -74640
            TabIndex        =   17
            Top             =   1680
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -74640
            TabIndex        =   15
            Top             =   240
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   -74640
            TabIndex        =   13
            Top             =   960
            Width           =   1455
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmVerCargaCarroMez"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmVerCargaCarroMez (FR0607.FRM)                               *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: preparar carga del carro                                *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim Insertar_en_el_Carro As Boolean
Dim hora_de_toma As Double
Dim mstrDiaSem As String
Dim gstrEstilo As String
Dim gintlinea As Integer
Dim gintHora As Integer

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer


  If Index = 0 And grdDBGrid1(0).Rows > 0 Then
    If gintlinea <> grdDBGrid1(0).Columns("L�nea").Value Then
      If gstrEstilo = "BLANCO" Then
        gstrEstilo = "AZUL"
      Else
        gstrEstilo = "BLANCO"
      End If
    End If
    If gintHora <> grdDBGrid1(0).Columns("H.Adm.").Value Then
      If gstrEstilo = "BLANCO" Then
        gstrEstilo = "AZUL"
      Else
        gstrEstilo = "BLANCO"
      End If
    End If
    gintHora = grdDBGrid1(0).Columns("H.Adm.").Value
    gintlinea = grdDBGrid1(0).Columns("L�nea").Value
    For i = 0 To 22
      grdDBGrid1(0).Columns(i).CellStyleSet gstrEstilo
    Next i
    Select Case grdDBGrid1(0).Columns("Grupo").Value
    Case 1
      grdDBGrid1(0).Columns("Tipo").Value = "Prod."
    Case 2
      grdDBGrid1(0).Columns("Tipo").Value = "Dil."
    Case 3
      grdDBGrid1(0).Columns("Tipo").Value = "Prod2"
    End Select
  End If

End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
  If strFormName = "Peticiones del Carro" And strCtrlName = "grdDBGrid1(0).Petici�n" Then
    aValues(2) = grdDBGrid1(0).Columns(13).Value
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    If objWinInfo.objWinActiveForm.strName = "Carros" Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub


Private Sub Form_Activate()
If objWinInfo.objWinActiveForm.strName = "Carros" Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim objMasterInfo2 As New clsCWForm
  Dim strKey As String
  Dim DiaSem(7) As String
  Dim fecha As Date
  Dim strFechaSer As String
  Dim rstFechaSer As rdoResultset
  Dim intDiaSem As Integer
  
    strFechaSer = "SELECT SYSDATE FROM DUAL"
    Set rstFechaSer = objApp.rdoConnect.OpenResultset(strFechaSer)
    fecha = rstFechaSer(0).Value
    intDiaSem = WeekDay(fecha, vbMonday)
    DiaSem(1) = "LUN"
    DiaSem(2) = "MAR"
    DiaSem(3) = "MIE"
    DiaSem(4) = "JUE"
    DiaSem(5) = "VIE"
    DiaSem(6) = "SAB"
    DiaSem(7) = "DOM"
    
    
  
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Carros"
      
    .strTable = "FR0701J"
    .intAllowance = cwAllowReadOnly
    's�lo los carros cuyo estado es <No se ha iniciado la carga>
    '.strWhere = "FR87CODESTCARRO=1 AND FR74DIA='" & DiaSem(intDiaSem) & "'"
    .strWhere = "FR87CODESTCARRO in (1,2) AND FR74DIA='" & DiaSem(intDiaSem) & "'"
    '.strWhere = " FR74DIA='" & DiaSem(intDiaSem) & "'"
    mstrDiaSem = DiaSem(intDiaSem)
    
    Call .FormAddOrderField("FR74DIA", cwAscending)
    Call .FormAddOrderField("FR74HORASALIDA", cwAscending)
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Carros")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "C�digo Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR07DESCARRO", "Descripci�n Carro", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Descripci�n Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR87CODESTCARRO", "C�d.Estado Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR87DESESTCARRO", "Desc.Estado Carro", cwString)
    Call .FormAddFilterWhere(strKey, "FR74DIA", "D�a Salida", cwString)
    Call .FormAddFilterWhere(strKey, "FR74HORASALIDA", "Hora Salida", cwNumeric)
  End With
  
  With objMasterInfo2
    Set .objFormContainer = fraframe1(2)
    Set .objFatherContainer = fraframe1(1)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Mezclas del Carro"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddRelation("FR07CODCARRO", txtText1(3))
    
    'Call .FormAddRelation("FR32DIATOMAULT", txtText1(6))
    Call .FormAddRelation("FR32HORATOMAULT", txtText1(7))
    
    .strTable = "FRJ902J"
    
    .strWhere = " TO_DATE(TO_CHAR(FR32DIATOMAULT,'DD/MM/YYYY'),'DD/MM/YYYY')=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')"
    
    .strWhere = .strWhere & " AND (FR32OPERACION in ('F','M') OR FR32INDPERF=-1)"
    .strWhere = .strWhere & " AND FRFN01(FRH5CODPERIODICIDAD,FR32FECINICIO,'Peticion Farmacia')=1"
    .strWhere = .strWhere & " AND FRFN03(FR07CODCARRO,FRG4CODFRECUENCIA,FR66CODPETICION,FR28NUMLINEA,FR73CODPRODUCTO,FR73CODPRODUCTO_DIL,FR73CODPRODUCTO_2,FRG5HORA)=1"
    '.strWhere = .strWhere & " AND SYSDATE <= NVL(FR32FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    
    'strKey = .strDataBase & .strTable
    'Call .FormCreateFilterWhere(strKey, "Peticiones del Carro")
    'Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "Carro", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "Producto", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR06CANTIDAD", "Cantidad", cwNumeric)
    
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = fraframe1(2)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Peticiones del Carro"
    .intAllowance = cwAllowReadOnly
    

    '.strTable = "FR0602J"
    .strTable = "FR0600"
    
    Call .FormAddRelation("FR07CODCARRO", txtText1(8))
'    Call .FormAddRelation("AD15CODCAMA", txtText1(9))
'    Call .FormAddRelation("CI21CODPERSONA", txtText1(10))
'    Call .FormAddRelation("FR66CODPETICION", txtText1(11))
'    Call .FormAddRelation("FR28NUMLINEA", txtText1(12))
    Call .FormAddRelation("FR06FECCARGA", txtText1(14)) 'FR32DIATOMAULT
    Call .FormAddRelation("FR06HORACARGA", txtText1(13)) 'FR32HORATOMAULT
    
    .strWhere = " FR06FECCARGA=TRUNC(SYSDATE)"
    
    Call .objPrinter.Add("FR1381", "Peticiones del Carro")
    
    Call .FormAddOrderField("FR07CODCARRO", cwAscending)
    Call .FormAddOrderField("FR06HORATOMA", cwAscending)
    Call .FormAddOrderField("FR66CODPETICION", cwAscending)
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    Call .FormAddOrderField("FR06GRUPO", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones del Carro")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR06CANTIDAD", "Cantidad", cwNumeric)
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMasterInfo2, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfo, "Carro", "FR07CODCARRO", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Persona", "CI21CODPERSONA", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Cama", "GCFN06(AD15CODCAMA)", cwString, 7)
    Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7) 'historia
    Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25) 'nombre
    Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25) 'apellido 1�
    Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25) 'apellido 2�
    Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Int", "", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Petici�n", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Ref", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Cant", "FR06CANTIDAD", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "F.F", "FR06FORMFAR", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR06DOSIS", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "U.M", "FR06UNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Grupo", "FR06GRUPO", cwNumeric, 1)
    Call .GridAddColumn(objMultiInfo, "H.Adm.", "FR06HORATOMA", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Tipo", "", cwString, 5)
    
    
    'Call .GridAddColumn(objMultiInfo, "Fecha Carga", "FR06FECCARGA", cwDate)
    'Call .GridAddColumn(objMultiInfo, "Hora Carga", "FR06HORACARGA", cwNumeric, 2)

    
    Call .FormCreateInfo(objMasterInfo)
    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(16)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(17)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(18)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(19)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(14)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(15)).blnInFind = True
  
    .CtrlGetInfo(txtText1(2)).blnInGrid = False 'c�d.estado carro
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(6), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(7), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(8), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(9), "CI22SEGAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), grdDBGrid1(0).Columns(11), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), grdDBGrid1(0).Columns("Ref"), "FR73REFERENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), grdDBGrid1(0).Columns(16), "FR73DESPRODUCTO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), "FR66CODPETICION", "SELECT * FROM FR3200 WHERE FR66CODPETICION=? AND FR28NUMLINEA =? ")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), grdDBGrid1(0).Columns(17), "FRH7CODFORMFAR")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), grdDBGrid1(0).Columns(18), "FR28DOSIS")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), grdDBGrid1(0).Columns(19), "FR93CODUNIMEDIDA")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), grdDBGrid1(0).Columns(14), "FR32REFERENCIA")
    
    grdDBGrid1(0).Columns(3).Visible = False
    grdDBGrid1(0).Columns(12).Visible = False
    grdDBGrid1(0).Columns(13).Visible = False
    grdDBGrid1(0).Columns(4).Visible = False
    grdDBGrid1(0).Columns(10).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
  gstrEstilo = "BLANCO"
  gintlinea = 0
  gintHora = -1
  
 grdDBGrid1(2).Columns(1).Width = 1100 'c�d.servicio
 grdDBGrid1(2).Columns(2).Width = 2500 'servicio
 grdDBGrid1(2).Columns(3).Width = 900 'c�d.carro
 grdDBGrid1(2).Columns(4).Width = 2500 'carro
 grdDBGrid1(2).Columns(5).Width = 2500 'estado carro
 grdDBGrid1(2).Columns(6).Width = 600 'd�a
 grdDBGrid1(2).Columns(7).Width = 600 'hora
  
 grdDBGrid1(0).Columns(5).Width = 600 'cama
 grdDBGrid1(0).Columns(4).Width = 1000 'c�d.persona
 grdDBGrid1(0).Columns(6).Width = 800 'historia
 grdDBGrid1(0).Columns(6).Visible = False 'historia
 grdDBGrid1(0).Columns(7).Width = 950 'nombre
 grdDBGrid1(0).Columns(8).Width = 1200 'apellido 1�
 grdDBGrid1(0).Columns(9).Width = 1200 'apellido 2�
 grdDBGrid1(0).Columns(10).Width = 1000 'c�d.prod
 grdDBGrid1(0).Columns(11).Width = 700 'c�d.int
 grdDBGrid1(0).Columns(16).Width = 2800 'desc.prod
 grdDBGrid1(0).Columns(17).Width = 450 'F.F
 grdDBGrid1(0).Columns(18).Width = 700 'dosis
 grdDBGrid1(0).Columns(19).Width = 650 'U.M
 grdDBGrid1(0).Columns(14).Width = 1000 'referencia
 grdDBGrid1(0).Columns(15).Width = 600 'cantidad
 grdDBGrid1(0).Columns("H.Adm.").Position = 3
 grdDBGrid1(0).Columns("Tipo").Position = 4
 grdDBGrid1(0).Columns("Grupo").Visible = False
 grdDBGrid1(0).Columns(14).Visible = False
 
 Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
Dim strWhere As String
Dim strFilter As String

  
  If strFormName = "Peticiones del Carro" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      'strWhere = "WHERE FR0604J." & Chr(34) & "FR06FECCARGA" & Chr(34) & "=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')"
      'strWhere = "WHERE FR0701J." & Chr(34) & "FR87CODESTCARRO" & Chr(34) & "=2 AND FR0701J." & Chr(34) & "FR74DIA" & Chr(34) & "='" & mstrDiaSem & "'" & " AND FR0701J." & Chr(34) & "FR74HORASALIDA" & Chr(34) & " = " & txtText1(7).Text
      strWhere = "WHERE FR0701J." & Chr(34) & "FR87CODESTCARRO" & Chr(34) & " IN (1,2) AND FR0701J." & Chr(34) & "FR74DIA" & Chr(34) & "='" & grdDBGrid1(2).Columns(6).Value & "'" & " AND FR0701J." & Chr(34) & "FR74HORASALIDA" & Chr(34) & " = " & grdDBGrid1(2).Columns(7).Value & " AND FR0701J." & Chr(34) & "FR07CODCARRO" & Chr(34) & "=" & grdDBGrid1(2).Columns(3).Value
      strFilter = ""
      Call objPrinter.ShowReport(strWhere, strFilter)
    End If
    Set objPrinter = Nothing
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  If intIndex = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
  End If
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    If intIndex = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdDBGrid1_Click(Index As Integer)
    If Index = 2 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
  If intIndex = 1 Then
        tlbToolbar1.Buttons(3).Enabled = False
        mnuDatosOpcion(20).Enabled = False
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

