VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmFirmarHojQuirof 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Firmar Hoja de Quir�fano"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Usuario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Index           =   1
      Left            =   480
      TabIndex        =   9
      Top             =   480
      Width           =   11100
      Begin TabDlg.SSTab tabTab1 
         Height          =   2415
         Index           =   0
         Left            =   240
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   480
         Width           =   10695
         _ExtentX        =   18865
         _ExtentY        =   4260
         _Version        =   327681
         TabOrientation  =   3
         Tabs            =   2
         TabsPerRow      =   1
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0128.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(10)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cboservicio"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtServicio"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).ControlCount=   9
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0128.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtServicio 
            BackColor       =   &H00808080&
            Height          =   330
            Left            =   1800
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Descripci�n Quir�fano"
            Top             =   1080
            Width           =   5175
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   1
            Left            =   1800
            ScrollBars      =   2  'Vertical
            TabIndex        =   1
            Tag             =   "Apellido Usuario"
            Top             =   360
            Width           =   5160
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "SG02COD"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   0
            Tag             =   "C�digo Usuario|C�digo Usuario"
            Top             =   360
            Width           =   1230
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2145
            Index           =   2
            Left            =   -74880
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   120
            Width           =   9975
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17595
            _ExtentY        =   3784
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   4
            Tag             =   "Fecha Intervenci�n"
            Top             =   1800
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboservicio 
            Height          =   315
            Left            =   240
            TabIndex        =   2
            Tag             =   "C�d.Quir�fano"
            Top             =   1080
            Width           =   1095
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2223
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7938
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1931
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Intervenci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   240
            TabIndex        =   16
            Top             =   1560
            Width           =   2415
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Quir�fano"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   240
            TabIndex        =   15
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Quir�fano"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   1800
            TabIndex        =   14
            Top             =   840
            Width           =   1905
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Usuario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   13
            Top             =   120
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Hoja de Quir�fano"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4320
      Index           =   0
      Left            =   480
      TabIndex        =   7
      Top             =   3600
      Width           =   11055
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3825
         Index           =   0
         Left            =   135
         TabIndex        =   5
         Top             =   360
         Width           =   10680
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         AllowUpdate     =   0   'False
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   18838
         _ExtentY        =   6747
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmFirmarHojQuirof"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmFirmarHojQuirof (FR0128.FRM)                              *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: AGOSTO DE 1998                                                *
'* DESCRIPCION: Firmar Hoja de Quir�fano                                *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cboservicio_Change()

  Call cboservicio_CloseUp

End Sub

Private Sub cboservicio_CloseUp()
Dim strauxwhere, strauxwhere1, strauxwhere2 As String
  'Call objWinInfo.CtrlDataChange

  strauxwhere = ""
  strauxwhere1 = ""
  strauxwhere2 = ""

  If cboservicio <> "" Then
    strauxwhere1 = "FR97CODQUIROFANO=" & cboservicio
    txtServicio = cboservicio.Columns(1).Value
  Else
    txtServicio = ""
  End If

  If dtcDateCombo1(0) <> "" Then
    strauxwhere2 = "FR44FECINTERVEN=TO_DATE('" & dtcDateCombo1(0) & "','DD/MM/YYYY')"
  End If
  
  If strauxwhere1 = "" And strauxwhere2 = "" Then
    strauxwhere = "1=1"
  ElseIf strauxwhere1 <> "" And strauxwhere2 <> "" Then
    strauxwhere = strauxwhere1 & " AND " & strauxwhere2
  ElseIf strauxwhere1 <> "" Then
    strauxwhere = strauxwhere1
  Else 'strauxwhere2 <> ""
    strauxwhere = strauxwhere2
  End If

  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
  'objWinInfo.objWinActiveForm.strWhere = strauxwhere & " AND SG02COD IS NULL"
  objWinInfo.cllWinForms(2).strWhere = strauxwhere & " AND SG02COD IS NULL AND FR44CODHOJAQUIRO IN (SELECT FR44CODHOJAQUIRO FROM FRG300 WHERE SG02COD_CIR='" & objsecurity.strUser & "' AND FRG3INDPRINCIPAL=-1) and (FR44INDESTHQ=0 or FR44INDESTHQ is null)"
  objWinInfo.DataRefresh
  Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)

End Sub

Private Sub cboservicio_KeyUp(KeyCode As Integer, Shift As Integer)

  Call cboservicio_CloseUp

End Sub

Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset

  cboservicio.RemoveAll
  Call cboservicio.AddItem("" & ";" & "")
  stra = "select FR97CODQUIROFANO,FR97DESQUIROFANO from FR9700 " & _
          "WHERE " & _
           "FR97FECINIVIG<(SELECT SYSDATE FROM DUAL) AND " & _
           "((FR97FECFINVIG IS NULL) OR (FR97FECFINVIG>(SELECT SYSDATE FROM DUAL)))"
           '& _
           " AND FR97CODQUIROFANO IN " & _
           " (SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD='" & objsecurity.strUser & "')"
  
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While (Not rsta.EOF)
      Call cboservicio.AddItem(rsta.rdoColumns("FR97CODQUIROFANO").Value & ";" & rsta.rdoColumns("FR97DESQUIROFANO").Value)
      rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  
  objWinInfo.DataRefresh
  
  cboservicio.BackColor = &H80000005
  dtcDateCombo1(0).BackColor = &H80000005

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Usuario"
      
    .strTable = "SG0200"
    
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("SG02COD", cwAscending)
    
    .strWhere = "SG02COD='" & objsecurity.strUser & "'"
   
    'strKey = .strDataBase & .strTable
    'Call .FormCreateFilterWhere(strKey, "Usuario")
    'Call .FormAddFilterWhere(strKey, "SG02COD", "C�digo Usuario", cwString)
    'Call .FormAddFilterOrder(strKey, "SG02COD", "C�digo Usuario")

  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Hoja Quirofano"
    
    .intAllowance = cwAllowReadOnly
    
    .strTable = "FR4400"
    
    '.strWhere = "AD02CODDPTO IS NULL AND SG02COD IS NULL"
    .strWhere = "SG02COD IS NULL AND FR44CODHOJAQUIRO IN (SELECT FR44CODHOJAQUIRO FROM FRG300 WHERE SG02COD_CIR='" & objsecurity.strUser & "' AND FRG3INDPRINCIPAL=-1) and (FR44INDESTHQ=0 or FR44INDESTHQ is null)"
    
    Call .FormAddOrderField("FR44CODHOJAQUIRO", cwAscending)
    'Call .FormAddRelation("AD02CODDPTO", cboservicio)
    
    strKey = .strDataBase & .strTable
    .intCursorSize = 0
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�d. Hoja Quir�fano", "FR44CODHOJAQUIRO", cwNumeric, 9) '3
    Call .GridAddColumn(objMultiInfo, "C�digo Quir�fano", "FR97CODQUIROFANO", cwString, 3) '4
    Call .GridAddColumn(objMultiInfo, "Quir�fano", "", cwString, 50) '5
    Call .GridAddColumn(objMultiInfo, "Fecha Intervenci�n", "FR44FECINTERVEN", cwDate) '6
    Call .GridAddColumn(objMultiInfo, "Hora Intervenci�n", "FR44HORAINTERV", cwDecimal, 7) '7
    Call .GridAddColumn(objMultiInfo, "Cod.Paci.", "CI21CODPERSONA", cwNumeric, 7) '8
    Call .GridAddColumn(objMultiInfo, "Paciente", "", cwString, 30) '9
'    Call .GridAddColumn(objMultiInfo, "Cirujano", "SG02COD_CIR", cwString, 6) '10
'    Call .GridAddColumn(objMultiInfo, "Apellido Cirujano", "", cwString, 30) '11
    Call .GridAddColumn(objMultiInfo, "N�m. Orden D�a", "FR44NUMORDENDIA", cwNumeric, 3) '12
    'Call .GridAddColumn(objMultiInfo, "C�digo Servicio", "AD02CODDPTO", cwNumeric, 3) '13
    'Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 30) '14
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    .CtrlGetInfo(txtText1(1)).blnMandatory = True

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(cboservicio).blnNegotiated = False
    .CtrlGetInfo(dtcDateCombo1(0)).blnNegotiated = False
    .CtrlGetInfo(txtServicio).blnNegotiated = False
    
    '.CtrlGetInfo(txtText1(0)).blnInFind = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(1), "SG02APE1")
    .CtrlGetInfo(txtText1(0)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "FR97CODQUIROFANO", "SELECT * FROM FR9700 WHERE FR97CODQUIROFANO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(5), "FR97DESQUIROFANO")
    .CtrlGetInfo(txtText1(0)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), "CI21CODPERSONA", "SELECT CI22NOMBRE || ' ' || CI22PRIAPEL || ' ' || CI22SEGAPEL NOMPACIENTE FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), grdDBGrid1(0).Columns(9), "NOMPACIENTE")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), "SG02COD_CIR", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), grdDBGrid1(0).Columns(11), "SG02APE1")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(13)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(13)), grdDBGrid1(0).Columns(14), "AD02DESDPTO")
    
    '.CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

  grdDBGrid1(0).Columns(3).Width = 19 * 90
  grdDBGrid1(0).Columns(4).Width = 16 * 90
  grdDBGrid1(0).Columns(5).Width = 2600
  grdDBGrid1(0).Columns(6).Width = 17 * 90
  grdDBGrid1(0).Columns(7).Width = 16 * 90
  grdDBGrid1(0).Columns(8).Width = 9 * 90
  'grdDBGrid1(0).Columns(10).Width = 8 * 90
  grdDBGrid1(0).Columns(9).Width = 2600
  grdDBGrid1(0).Columns(10).Width = 15 * 90
'  grdDBGrid1(0).Columns(13).Width = 14 * 90

  tabTab1(0).TabVisible(1) = False
  tabTab1(0).Caption = ""

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strCtrl = "txtText1(0)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "SG0200"
     .strOrder = "ORDER BY SG02APE1"

     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo Usuario"

     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Apellido Usuario"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(0), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
 End If

End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)

  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
Dim vntDatos(1 To 2) As Variant


'Call objsecurity.LaunchProcess("FR0126", vntdatos)
  
  'Call objWinInfo.GridDblClick
  If intIndex = 0 Then
    If grdDBGrid1(0).Rows > 0 Then
      'vntDatos(1) = "frmFirmarHojQuirof" 'gstrllamador
      gstrLlamador = "frmFirmarHojQuirof"
      'vntDatos(2) = frmFirmarHojQuirof.grdDBGrid1(0).Columns(3).Value 'gintcodhojaquiro
      gintcodhojaquiro = frmFirmarHojQuirof.grdDBGrid1(0).Columns(3).Value
      Call objsecurity.LaunchProcess("FR0126", vntDatos)
      gstrLlamador = ""
      objWinInfo.DataRefresh
    End If
  End If
   
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  
  Call objWinInfo.CtrlLostFocus
  Call dtcDateCombo1_CloseUp(intIndex)

End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
Dim strauxwhere, strauxwhere1, strauxwhere2 As String
  'Call objWinInfo.CtrlDataChange

  strauxwhere = ""
  strauxwhere1 = ""
  strauxwhere2 = ""

  If cboservicio <> "" Then
    strauxwhere1 = "FR97CODQUIROFANO=" & cboservicio
  End If

  If dtcDateCombo1(intIndex) <> "" Then
    strauxwhere2 = "FR44FECINTERVEN=TO_DATE('" & dtcDateCombo1(intIndex) & "','DD/MM/YYYY')"
  End If
  
  If strauxwhere1 = "" And strauxwhere2 = "" Then
    strauxwhere = "1=1"
  ElseIf strauxwhere1 <> "" And strauxwhere2 <> "" Then
    strauxwhere = strauxwhere1 & " AND " & strauxwhere2
  ElseIf strauxwhere1 <> "" Then
    strauxwhere = strauxwhere1
  Else 'strauxwhere2 <> ""
    strauxwhere = strauxwhere2
  End If

  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
  'objWinInfo.objWinActiveForm.strWhere = strauxwhere & " AND SG02COD IS NULL"
  objWinInfo.cllWinForms(2).strWhere = strauxwhere & " AND SG02COD IS NULL AND FR44CODHOJAQUIRO IN (SELECT FR44CODHOJAQUIRO FROM FRG300 WHERE SG02COD_CIR='" & objsecurity.strUser & "' AND FRG3INDPRINCIPAL=-1) and (FR44INDESTHQ=0 or FR44INDESTHQ is null)"
  objWinInfo.DataRefresh
  Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)

End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)

  Call objWinInfo.CtrlDataChange
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


