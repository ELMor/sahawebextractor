VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmVerHojasInt 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Ver Hojas de Intervenci�n"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3570
   ClientWidth     =   11910
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame framSelec 
      Caption         =   "Selecci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Index           =   1
      Left            =   120
      TabIndex        =   12
      Top             =   480
      Width           =   11700
      Begin VB.TextBox txtCodPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   6600
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   600
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   1800
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   600
         Width           =   4695
      End
      Begin VB.TextBox txtNumHistoria 
         Height          =   320
         Left            =   240
         TabIndex        =   22
         Top             =   600
         Width           =   735
      End
      Begin VB.OptionButton optFirmada 
         Caption         =   "Todas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   7680
         TabIndex        =   21
         Top             =   1800
         Width           =   2055
      End
      Begin VB.TextBox txtServicio 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   3720
         TabIndex        =   19
         TabStop         =   0   'False
         Tag             =   "Descripci�n Servicio"
         Top             =   1800
         Width           =   3375
      End
      Begin VB.CommandButton Command1 
         Caption         =   "FILTRAR"
         Default         =   -1  'True
         Height          =   375
         Left            =   9840
         TabIndex        =   6
         Top             =   1320
         Width           =   1095
      End
      Begin VB.OptionButton optFirmada 
         Caption         =   "Por Firmar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   7680
         TabIndex        =   2
         Top             =   840
         Visible         =   0   'False
         Width           =   2055
      End
      Begin VB.OptionButton optFirmada 
         Caption         =   "Enviadas Farmacia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   7680
         TabIndex        =   5
         Top             =   1560
         Width           =   2055
      End
      Begin VB.OptionButton optFirmada 
         Caption         =   "Terminadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   7680
         TabIndex        =   4
         Top             =   1320
         Width           =   2055
      End
      Begin VB.OptionButton optFirmada 
         Caption         =   "Firmadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   7680
         TabIndex        =   3
         Top             =   1080
         Value           =   -1  'True
         Width           =   2055
      End
      Begin VB.TextBox txtQuirofano 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   1800
         TabIndex        =   13
         TabStop         =   0   'False
         Tag             =   "Descripci�n Quir�fano"
         Top             =   1200
         Width           =   4695
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Tag             =   "Fecha Intervenci�n"
         Top             =   1800
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo cboQuirofano 
         Height          =   315
         Left            =   240
         TabIndex        =   0
         Tag             =   "C�d.Quir�fano"
         Top             =   1200
         Width           =   1095
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2223
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1931
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo cboServicio 
         Height          =   315
         Left            =   2520
         TabIndex        =   17
         Tag             =   "C�d.Servicio"
         Top             =   1800
         Width           =   1095
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2223
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1931
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   1800
         TabIndex        =   25
         Top             =   360
         Width           =   765
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Num.Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   240
         TabIndex        =   23
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   3720
         TabIndex        =   20
         Top             =   1560
         Width           =   1770
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d.Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   2520
         TabIndex        =   18
         Top             =   1560
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n Quir�fano"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   1800
         TabIndex        =   16
         Top             =   960
         Width           =   1905
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d.Quir�fano"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   15
         Top             =   960
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Fecha Intervenci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   14
         Top             =   1560
         Width           =   2415
      End
   End
   Begin VB.CommandButton cmdTotal 
      Caption         =   "Generar Necesidades Totales y Enviar a Farmacia"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3960
      TabIndex        =   9
      Top             =   7680
      Width           =   3855
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Hoja de Intervenci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4800
      Index           =   0
      Left            =   120
      TabIndex        =   7
      Top             =   2760
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4305
         Index           =   0
         Left            =   135
         TabIndex        =   8
         Top             =   360
         Width           =   11400
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         AllowUpdate     =   0   'False
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20108
         _ExtentY        =   7594
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   10
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
End
Attribute VB_Name = "frmVerHojasInt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmVerHojasInt (FR0149.FRM)                              *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: AGOSTO DE 1998                                                *
'* DESCRIPCION: Firmar Hoja de Quir�fano                                *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim blnInLoad As Boolean
Dim gstrWhere As String

Private Sub cboServicio_Change()

  Call cboServicio_CloseUp

End Sub

Private Sub cboServicio_CloseUp()
  
  If cboServicio <> "" Then
    txtServicio = cboServicio.Columns(1).Value
  Else
    txtServicio = ""
  End If

grdDBGrid1(0).RemoveAll

End Sub


Private Sub cboServicio_KeyUp(KeyCode As Integer, Shift As Integer)

  Call cboServicio_CloseUp

End Sub

Private Sub cmdTotal_Click()
Dim rsta, rstb As rdoResultset
Dim sqlstr, strInsert, strupdate As String
Dim numlinea As Integer
Dim rstUM As rdoResultset
Dim strUM As String
Dim auxCodNeces
Dim strdelete As String
Dim strQuiDispen As String
Dim strFRH2 As String
Dim qryFRH2 As rdoQuery
Dim rdoFRH2 As rdoResultset


cmdTotal.Enabled = False
Me.Enabled = False
Me.MousePointer = vbHourglass

    strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
    Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
    qryFRH2(0) = 30
    Set rdoFRH2 = qryFRH2.OpenResultset()
    If IsNull(rdoFRH2.rdoColumns(0).Value) Then
      strQuiDispen = "999"
    Else
      strQuiDispen = rdoFRH2.rdoColumns(0).Value
    End If
    rdoFRH2.Close
    Set rdoFRH2 = Nothing
    qryFRH2.Close
    Set qryFRH2 = Nothing

    sqlstr = "SELECT FRK3CODNECESUNID_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    auxCodNeces = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing

    strInsert = "INSERT INTO FRK300 (FRK3CODNECESUNID," & _
                "FR97CODQUIROFANO,FR26CODESTPETIC,SG02COD_PDS," & _
                "FRK3FECPETICION,FRK3INDQUIANE)" & _
                " VALUES ("
    strInsert = strInsert & auxCodNeces & ",'"
    strInsert = strInsert & strQuiDispen & "',"
    strInsert = strInsert & "3,'" '3 FR26CODESTPETIC, enviada a farmacia
    strInsert = strInsert & objsecurity.strUser & "',"
    strInsert = strInsert & "TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY'),0)"
    objApp.rdoConnect.Execute strInsert, 64

    strInsert = "INSERT INTO FRK400 (FRK3CODNECESUNID,"
    strInsert = strInsert & "FRK4NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,"
    strInsert = strInsert & "FRK4CANTNECESQUIR,FRK4INDBLOQUEO,FRK4INDQUIANE)"
    strInsert = strInsert & " SELECT " & auxCodNeces & ",ROWNUM,PRODQUI,FR7300.FR93CODUNIMEDIDA,CANTCONS,0,0"
    strInsert = strInsert & " FROM FR7300,"
    strInsert = strInsert & " (SELECT FR1800.FR73CODPRODUCTO PRODQUI,SUM(FR1800.FR18CANTCONSUMIDA) CANTCONS"
    strInsert = strInsert & " FROM FR1800,PR6200"
    strInsert = strInsert & " WHERE FR1800.PR62CODHOJAQUIR=PR6200.PR62CODHOJAQUIR"
    strInsert = strInsert & " AND FR1800.FR18CANTCONSUMIDA>0"
    strInsert = strInsert & " AND PR6200.PR62CODESTADO_FAR=2"
    strInsert = strInsert & " GROUP BY FR1800.FR73CODPRODUCTO)"
    strInsert = strInsert & " WHERE PRODQUI=FR7300.FR73CODPRODUCTO"
    objApp.rdoConnect.Execute strInsert, 64

    sqlstr = "SELECT FRK3CODNECESUNID_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    auxCodNeces = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
    
    strInsert = "INSERT INTO FRK300 (FRK3CODNECESUNID," & _
                "FR97CODQUIROFANO,FR26CODESTPETIC,SG02COD_PDS," & _
                "FRK3FECPETICION,FRK3INDQUIANE)" & _
                " VALUES ("
    strInsert = strInsert & auxCodNeces & ",'"
    strInsert = strInsert & strQuiDispen & "',"
    strInsert = strInsert & "3,'" '3 FR26CODESTPETIC, enviada a farmacia
    strInsert = strInsert & objsecurity.strUser & "',"
    strInsert = strInsert & "TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY'),-1)"
    objApp.rdoConnect.Execute strInsert, 64
    
    strInsert = "INSERT INTO FRK400 (FRK3CODNECESUNID,"
    strInsert = strInsert & "FRK4NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,"
    strInsert = strInsert & "FRK4CANTNECESQUIR,FRK4INDBLOQUEO,FRK4INDQUIANE)"
    strInsert = strInsert & " SELECT " & auxCodNeces & ",ROWNUM,PRODANE,FR7300.FR93CODUNIMEDIDA,CANTCONS,0,1"
    strInsert = strInsert & " FROM FR7300,"
    strInsert = strInsert & " (SELECT FR1700.FR73CODPRODUCTO PRODANE,SUM(FR1700.FR17CANTIDAD) CANTCONS"
    strInsert = strInsert & " FROM FR1700,PR6200"
    strInsert = strInsert & " WHERE FR1700.PR62CODHOJAQUIR=PR6200.PR62CODHOJAQUIR"
    strInsert = strInsert & " AND FR1700.FR17CANTIDAD>0"
    strInsert = strInsert & " AND PR6200.PR62CODESTADO_FAR=2"
    strInsert = strInsert & " GROUP BY FR1700.FR73CODPRODUCTO)"
    strInsert = strInsert & " WHERE PRODANE=FR7300.FR73CODPRODUCTO"
    objApp.rdoConnect.Execute strInsert, 64
    
    strupdate = "UPDATE PR6200 SET PR62CODESTADO_FAR=3 WHERE PR62CODESTADO_FAR=2"
    objApp.rdoConnect.Execute strupdate, 64

  strdelete = "DELETE FROM FRK300 WHERE FRK3CODNECESUNID NOT IN (SELECT FRK3CODNECESUNID FROM FRK400)"
  objApp.rdoConnect.Execute strdelete, 64

  Call Refrescar_Grid

Me.Enabled = True
Me.MousePointer = vbDefault

MsgBox "Necesidades de quir�fano generadas y enviadas a Farmacia", vbInformation, "Aviso"

cmdTotal.Enabled = True

End Sub

Private Sub Command1_Click()
Dim strauxwhere, strauxwhere1, strauxwhere2 As String

  strauxwhere = ""
  strauxwhere1 = ""
  strauxwhere2 = ""
  
  If optFirmada(2).Value = True Then
    If txtNumHistoria.Text = "" And Not IsDate(dtcDateCombo1(0)) Then
      MsgBox "Para ver Todas las Hojas, ha de filtrar por Paciente � Fecha obligatoriamente", vbInformation, "Aviso"
      Exit Sub
    End If
  End If

  If cboQuirofano <> "" Then
    strauxwhere1 = " AG1100.AG11DESRECURSO='" & cboQuirofano.Columns(1).Text & "'"
  End If

  If dtcDateCombo1(0) <> "" Then
    strauxwhere2 = "TRUNC(PR6200.PR62FECINI_INTERV)=TO_DATE('" & dtcDateCombo1(0) & "','DD/MM/YYYY')"
  End If
  
  If strauxwhere1 = "" And strauxwhere2 = "" Then
    strauxwhere = "1=1"
  ElseIf strauxwhere1 <> "" And strauxwhere2 <> "" Then
    strauxwhere = strauxwhere1 & " AND " & strauxwhere2
  ElseIf strauxwhere1 <> "" Then
    strauxwhere = strauxwhere1
  Else 'strauxwhere2 <> ""
    strauxwhere = strauxwhere2
  End If
  
  If txtCodPaciente.Text <> "" Then
    strauxwhere = strauxwhere & " AND PR0400.CI21CODPERSONA=" & txtCodPaciente.Text
  End If

  If cboServicio <> "" Then
    strauxwhere = strauxwhere & " AND PR0400.AD02CODDPTO=" & cboServicio
  End If

  If optFirmada(0).Value = True Then
    gstrWhere = strauxwhere & " AND PR6200.PR62CODESTADO_FAR=1"
  ElseIf optFirmada(1).Value = True Then
    gstrWhere = strauxwhere & " AND PR6200.PR62CODESTADO_FAR=2"
  ElseIf optFirmada(2).Value = True Then
    gstrWhere = strauxwhere & " AND PR6200.PR62CODESTADO_FAR>2"
  ElseIf optFirmada(3).Value = True Then
    gstrWhere = strauxwhere & " AND NVL(PR6200.PR62CODESTADO_FAR,0)=0"
  Else 'Todas
    If txtCodPaciente.Text = "" And Not IsDate(dtcDateCombo1(0)) Then
      MsgBox "Para ver Todas las Hojas, ha de filtrar por Paciente � Fecha obligatoriamente", vbInformation, "Aviso"
      gstrWhere = "-1=0"
    Else
      gstrWhere = strauxwhere
    End If
  End If
  
  Me.MousePointer = vbHourglass
  Call Refrescar_Grid
  Me.MousePointer = vbDefault

End Sub


Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)

  If Index = 0 Then
    If Not IsNumeric(grdDBGrid1(0).Columns("C�d.Est.").Value) Then
      grdDBGrid1(0).Columns("Estado").Value = "Por Firmar"
    ElseIf grdDBGrid1(0).Columns("C�d.Est.").Value = 0 Then
      grdDBGrid1(0).Columns("Estado").Value = "Por Firmar"
    ElseIf grdDBGrid1(0).Columns("C�d.Est.").Value = 1 Then
      grdDBGrid1(0).Columns("Estado").Value = "Firmada"
    ElseIf grdDBGrid1(0).Columns("C�d.Est.").Value = 2 Then
      grdDBGrid1(0).Columns("Estado").Value = "Terminada"
    Else
       grdDBGrid1(0).Columns("Estado").Value = "Enviada Farmacia"
   End If
  End If

End Sub

Private Sub IdPersona1_LostFocus()
grdDBGrid1(0).RemoveAll
End Sub


Private Sub optFirmada_Click(Index As Integer)
  
  If Index = 1 Then
    cmdTotal.Enabled = True
  Else
    cmdTotal.Enabled = False
  End If
  grdDBGrid1(0).RemoveAll

End Sub


Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cboQuirofano_Change()

  Call cboQuirofano_CloseUp

End Sub

Private Sub cboQuirofano_CloseUp()

  If cboQuirofano <> "" Then
    txtQuirofano = cboQuirofano.Columns(1).Value
  Else
    txtQuirofano = ""
  End If

grdDBGrid1(0).RemoveAll

End Sub

Private Sub cboQuirofano_KeyUp(KeyCode As Integer, Shift As Integer)

  Call cboQuirofano_CloseUp

End Sub

Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset

  If blnInLoad = True Then
    blnInLoad = False
    cboQuirofano.RemoveAll
    Call cboQuirofano.AddItem("" & ";" & "")
    stra = "select  LTRIM(AG1100.AG11DESRECURSO,'Quir�fano ') CODQUIROFANO,AG11DESRECURSO from AG1100 WHERE AG14CODTIPRECU=33 ORDER BY AG11DESRECURSO"
    '        "WHERE " & _
    '         "FR97FECINIVIG<(SELECT SYSDATE FROM DUAL) AND " & _
    '         "((FR97FECFINVIG IS NULL) OR (FR97FECFINVIG>(SELECT SYSDATE FROM DUAL))) ORDER BY FR97DESQUIROFANO"

    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    While (Not rsta.EOF)
        Call cboQuirofano.AddItem(rsta.rdoColumns("CODQUIROFANO").Value & ";" & rsta.rdoColumns("AG11DESRECURSO").Value)
        rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
  
    Call cboServicio.AddItem("" & ";" & "")
    stra = "select AD02CODDPTO,AD02DESDPTO from AD0200 " & _
            "WHERE " & _
             "TRUNC(AD02FECINICIO)<=TRUNC(SYSDATE) AND " & _
             "((AD02FECFIN IS NULL) OR (AD02FECFIN>TRUNC(SYSDATE))) ORDER BY AD02DESDPTO"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    While (Not rsta.EOF)
        Call cboServicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
        rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
  
    cboQuirofano.BackColor = &H80000005
    dtcDateCombo1(0).BackColor = &H80000005
  End If
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim j As Integer


  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, cwWithToolBar + cwWithoutMenu + cwWithStatusBar)
  

grdDBGrid1(0).Redraw = False
For j = 0 To 12
  Call grdDBGrid1(0).Columns.Add(j)
Next j
grdDBGrid1(0).Columns(0).Caption = "Hoj.Quir."
grdDBGrid1(0).Columns(1).Caption = "C�digo Quir�fano"
grdDBGrid1(0).Columns(2).Caption = "Quir�fano"
grdDBGrid1(0).Columns(3).Caption = "Fecha Intervenci�n"
grdDBGrid1(0).Columns(4).Caption = "Cod.Paci."
grdDBGrid1(0).Columns(5).Caption = "Paciente"
grdDBGrid1(0).Columns(6).Caption = "Cirujano"
grdDBGrid1(0).Columns(7).Caption = "Anestesista"
grdDBGrid1(0).Columns(8).Caption = "Intervenci�n"
grdDBGrid1(0).Columns(9).Caption = "Dpto.Realizador"
grdDBGrid1(0).Columns(10).Caption = "Departamento"
grdDBGrid1(0).Columns(11).Caption = "C�d.Est."
grdDBGrid1(0).Columns(12).Caption = "Estado"

grdDBGrid1(0).Columns(0).Width = 900
grdDBGrid1(0).Columns(2).Width = 1500
grdDBGrid1(0).Columns(3).Width = 1200
grdDBGrid1(0).Columns(5).Width = 2000
grdDBGrid1(0).Columns(6).Width = 2000
grdDBGrid1(0).Columns(7).Width = 2300
grdDBGrid1(0).Columns(8).Width = 2300
grdDBGrid1(0).Columns(9).Width = 900
grdDBGrid1(0).Columns(10).Width = 2300
grdDBGrid1(0).Columns(1).Visible = False
grdDBGrid1(0).Columns(4).Visible = False
grdDBGrid1(0).Columns(11).Visible = False
grdDBGrid1(0).Columns(12).Position = 4
grdDBGrid1(0).Columns(12).Width = 1500
grdDBGrid1(0).Redraw = True

For j = 1 To 29
tlbToolbar1.Buttons(j).Enabled = False
Next j

  blnInLoad = True
  gstrWhere = "-1=0"
  
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  
  If btnButton.Index = 30 Then 'Salir
    Unload Me
  End If

End Sub


Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  If intIndex = 100 Then 'Salir
    Unload Me
  End If
  
End Sub


Private Sub grdDBGrid1_DblClick(intIndex As Integer)
Dim vntDatos(1 To 2) As Variant


'Call objsecurity.LaunchProcess("FR0126", vntdatos)
  
  If intIndex = 0 Then
    If grdDBGrid1(0).Rows > 0 Then
      'vntDatos(1) = "frmVerHojasInt" 'gstrllamador
      gstrLlamador = "frmFirmarHojQuirof"
      'vntDatos(2) = frmVerHojasInt.grdDBGrid1(0).Columns(3).Value 'gintcodhojaquiro
      gintcodhojaquiro = frmVerHojasInt.grdDBGrid1(0).Columns(0).Value
      
      Call objsecurity.LaunchProcess("FR0226", vntDatos)
      
      gstrLlamador = ""
      Me.MousePointer = vbHourglass
      Call Refrescar_Grid
      Me.MousePointer = vbDefault
    End If
  End If
   
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call dtcDateCombo1_CloseUp(intIndex)
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)

grdDBGrid1(0).RemoveAll

End Sub

Private Sub Refrescar_Grid()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strlinea As String

Me.Enabled = False
Screen.MousePointer = vbHourglass

grdDBGrid1(0).Redraw = False
grdDBGrid1(0).RemoveAll
  
strSelect = "SELECT DISTINCT "
strSelect = strSelect & " PR6200.PR62CODHOJAQUIR"
strSelect = strSelect & ",LTRIM(AG1100.AG11DESRECURSO,'Quir�fano ') CODQUIROFANO"
strSelect = strSelect & ",AG1100.AG11DESRECURSO"
strSelect = strSelect & ",PR6200.PR62FECINI_INTERV"
strSelect = strSelect & ",PR0400.CI21CODPERSONA "
strSelect = strSelect & ",CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE"
strSelect = strSelect & ",NVL(SG02CIR.SG02TXTFIRMA, SG02CIR.SG02APE1||' '||SG02CIR.SG02APE2||', '||SG02CIR.SG02NOM) CIRUJANO"
strSelect = strSelect & ",NVL(SG02ANE.SG02TXTFIRMA, SG02ANE.SG02APE1||' '||SG02ANE.SG02APE2||', '||SG02ANE.SG02NOM) ANESTESISTA"
strSelect = strSelect & ",PR0100.PR01DESCORTA "
strSelect = strSelect & ",PR0400.AD02CODDPTO"
strSelect = strSelect & ",AD0200.AD02DESDPTO "
strSelect = strSelect & ",PR6200.PR62CODESTADO_FAR"
'"Estado"

strSelect = strSelect & " FROM PR6200, PR0400, CI2200, AD0200, CI0100, AG1100, PR0100, SG0200 SG02CIR, SG0200 SG02ANE"
strSelect = strSelect & " WHERE PR6200.PR62CODHOJAQUIR=PR0400.PR62CODHOJAQUIR"
strSelect = strSelect & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
strSelect = strSelect & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
strSelect = strSelect & " AND AD0200.AD02CODDPTO = PR0400.AD02CODDPTO"
strSelect = strSelect & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
strSelect = strSelect & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
strSelect = strSelect & " AND PR6200.SG02COD_CIR=SG02CIR.SG02COD(+)"
strSelect = strSelect & " AND PR6200.SG02COD_ANE=SG02ANE.SG02COD(+)"
strSelect = strSelect & " AND PR0400.PR37CODESTADO<>6"
strSelect = strSelect & " AND PR6200.PR62CODESTADO=1"

strSelect = strSelect & " AND " & gstrWhere

strSelect = strSelect & " ORDER BY PR62CODHOJAQUIR DESC"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()

While Not rsta.EOF
  strlinea = rsta("PR62CODHOJAQUIR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CODQUIROFANO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AG11DESRECURSO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("PR62FECINI_INTERV").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CI21CODPERSONA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("PACIENTE").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("CIRUJANO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("ANESTESISTA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("PR01DESCORTA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD02CODDPTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("AD02DESDPTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("PR62CODESTADO_FAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & ""
  grdDBGrid1(0).AddItem strlinea
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
grdDBGrid1(0).Redraw = True
  
Screen.MousePointer = vbDefault
Me.Enabled = True

End Sub

Private Sub txtNumHistoria_Change()

grdDBGrid1(0).RemoveAll
If txtNumHistoria.Text = "" Then
txtPaciente.Text = ""
txtCodPaciente.Text = ""
End If

End Sub

Private Sub txtNumHistoria_KeyPress(KeyAscii As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strpaciente, strCodPac As String


End Sub

Private Sub txtNumHistoria_KeyUp(KeyCode As Integer, Shift As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strpaciente, strCodPac As String

strpaciente = ""
strCodPac = ""
  
  If IsNumeric(txtNumHistoria.Text) Then
      stra = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txtNumHistoria.Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        If Not IsNull(rsta.rdoColumns("CI22NOMBRE").Value) Then
          strpaciente = rsta.rdoColumns("CI22NOMBRE").Value
        End If
        If Not IsNull(rsta.rdoColumns("CI22PRIAPEL").Value) Then
          strpaciente = strpaciente & " " & rsta.rdoColumns("CI22PRIAPEL").Value
        End If
        If Not IsNull(rsta.rdoColumns("CI22SEGAPEL").Value) Then
          strpaciente = strpaciente & " " & rsta.rdoColumns("CI22SEGAPEL").Value
        End If
        If Not IsNull(rsta.rdoColumns("CI21CODPERSONA").Value) Then
          strCodPac = rsta.rdoColumns("CI21CODPERSONA").Value
        End If
      End If
      rsta.Close
      Set rsta = Nothing
  End If
  txtPaciente.Text = strpaciente
  txtCodPaciente.Text = strCodPac


End Sub

Private Sub txtNumHistoria_LostFocus()
Dim stra As String
Dim rsta As rdoResultset
Dim strpaciente, strCodPac As String

strpaciente = ""
strCodPac = ""
  
  If IsNumeric(txtNumHistoria.Text) Then
      stra = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & txtNumHistoria.Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        If Not IsNull(rsta.rdoColumns("CI22NOMBRE").Value) Then
          strpaciente = rsta.rdoColumns("CI22NOMBRE").Value
        End If
        If Not IsNull(rsta.rdoColumns("CI22PRIAPEL").Value) Then
          strpaciente = strpaciente & " " & rsta.rdoColumns("CI22PRIAPEL").Value
        End If
        If Not IsNull(rsta.rdoColumns("CI22SEGAPEL").Value) Then
          strpaciente = strpaciente & " " & rsta.rdoColumns("CI22SEGAPEL").Value
        End If
        If Not IsNull(rsta.rdoColumns("CI21CODPERSONA").Value) Then
          strCodPac = rsta.rdoColumns("CI21CODPERSONA").Value
        End If
      End If
      rsta.Close
      Set rsta = Nothing
  End If
  txtPaciente.Text = strpaciente
  txtCodPaciente.Text = strCodPac

End Sub
