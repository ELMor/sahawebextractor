VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDefProtQuirofano 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA.Protocolos de Quir�fano"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Protocolos Quir�fano"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   600
      Width           =   11685
      Begin TabDlg.SSTab tabTab1 
         Height          =   2580
         Index           =   1
         Left            =   120
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   4551
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0154.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(3)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(4)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).ControlCount=   7
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0154.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   1
            Left            =   1200
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Tag             =   "Descripci�n Tipo Protocolo"
            Top             =   1920
            Width           =   8160
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRI8CODTIPPROTQ"
            Height          =   330
            Index           =   0
            Left            =   360
            ScrollBars      =   2  'Vertical
            TabIndex        =   2
            Tag             =   "Tipo de Protocolo"
            Top             =   1920
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FRI2CODPROTQUI"
            Height          =   330
            Index           =   4
            Left            =   360
            TabIndex        =   0
            Tag             =   "C�digo Protocolo|C�digo Protocolo"
            Top             =   480
            Width           =   1200
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRI2DESPROTQUI"
            Height          =   330
            Index           =   5
            Left            =   360
            ScrollBars      =   2  'Vertical
            TabIndex        =   1
            Tag             =   "Descripci�n Protocolo|Descripci�n Protocolo"
            Top             =   1200
            Width           =   9000
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2265
            Index           =   1
            Left            =   -74880
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   120
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19076
            _ExtentY        =   3995
            _StockProps     =   79
            Caption         =   "PROTOCOLOS"
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo de Protocolo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   15
            Top             =   1680
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Protocolo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   360
            TabIndex        =   13
            Top             =   240
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Protocolo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   360
            TabIndex        =   12
            Top             =   960
            Width           =   2535
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4095
      Index           =   0
      Left            =   120
      TabIndex        =   7
      Top             =   3840
      Width           =   11685
      Begin VB.CommandButton cmdbuscarmaterial 
         Caption         =   "Buscar Material Sanitario"
         Height          =   375
         Left            =   5880
         TabIndex        =   6
         Top             =   3600
         Width           =   1935
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar Medicamentos"
         Height          =   375
         Left            =   3480
         TabIndex        =   5
         Top             =   3600
         Width           =   1935
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3195
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   11415
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         stylesets.count =   2
         stylesets(0).Name=   "Material"
         stylesets(0).BackColor=   12615935
         stylesets(0).Picture=   "FR0154.frx":0038
         stylesets(1).Name=   "Medicamento"
         stylesets(1).BackColor=   16777215
         stylesets(1).Picture=   "FR0154.frx":0054
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowColumnMoving=   2
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20135
         _ExtentY        =   5636
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   9
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefProtQuirofano"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDefProtQuirofano (FR0154.FRM)                             *
'* AUTOR: Aitor Vi�uela Garc�a                                          *
'* FECHA: Marzo DE 1999                                                 *
'* DESCRIPCION: Protocolos de Quirofano                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdbuscarmaterial_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
  
cmdbuscarmaterial.Enabled = False

If txtText1(4).Text <> "" Then
  noinsertar = True
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call objWinInfo.DataRefresh
  gstrMedicamentoMaterial = "Material"
  Call objsecurity.LaunchProcess("FR0166")
  gstrMedicamentoMaterial = ""
  If gintprodtotal > 0 Then
    For v = 0 To gintprodtotal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(0).Rows > 0 Then
        grdDBGrid1(0).MoveFirst
        For i = 0 To grdDBGrid1(0).Rows - 1
          If grdDBGrid1(0).Columns(4).Value = gintprodbuscado(v, 0) _
             And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
            'no se inserta el producto pues ya est� insertado en el grid
            noinsertar = False
            Exit For
          Else
            noinsertar = True
          End If
          grdDBGrid1(0).MoveNext
        Next i
      End If
      If noinsertar = True Then
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        grdDBGrid1(0).Columns(4).Value = gintprodbuscado(v, 0)  'cod.producto
        grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1) 'cod.interno
        grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2) 'des.producto
        stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(4).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(9), rsta.rdoColumns("FRH7CODFORMFAR").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(10), rsta.rdoColumns("FR73DOSIS").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(7), rsta.rdoColumns("FR73REFERENCIA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(11), rsta.rdoColumns("FR93CODUNIMEDIDA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(12), rsta.rdoColumns("FR73INDPRODSAN").Value)
        grdDBGrid1(0).Update
        rsta.Close
        Set rsta = Nothing
      Else
        mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
                  Chr(13), vbInformation)
      End If
      noinsertar = True
    Next v
  End If
  gintprodtotal = 0
End If

cmdbuscarmaterial.Enabled = True
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
  Dim i As Integer
    
    If Index = 0 Then
      If IsNumeric(grdDBGrid1(0).Columns(4).Value) Then
        If grdDBGrid1(0).Columns(12).Value <> True Then
          For i = 6 To 11
              grdDBGrid1(0).Columns(i).CellStyleSet "Medicamento"
          Next i
        Else
          For i = 6 To 11
              grdDBGrid1(0).Columns(i).CellStyleSet "Material"
          Next i
        End If
      End If
    End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cmdBuscar_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
  
  cmdBuscar.Enabled = False

If txtText1(4).Text <> "" Then
  noinsertar = True
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call objWinInfo.DataRefresh
  gstrMedicamentoMaterial = "Medicamento"
  Call objsecurity.LaunchProcess("FR0166")
  gstrMedicamentoMaterial = ""
  If gintprodtotal > 0 Then
    For v = 0 To gintprodtotal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(0).Rows > 0 Then
        grdDBGrid1(0).MoveFirst
        For i = 0 To grdDBGrid1(0).Rows - 1
          If grdDBGrid1(0).Columns(4).Value = gintprodbuscado(v, 0) _
             And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
            'no se inserta el producto pues ya est� insertado en el grid
            noinsertar = False
            Exit For
          Else
            noinsertar = True
          End If
          grdDBGrid1(0).MoveNext
        Next i
      End If
      If noinsertar = True Then
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        grdDBGrid1(0).Columns(4).Value = gintprodbuscado(v, 0)  'cod.producto
        grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1) 'cod.interno
        grdDBGrid1(0).Columns(8).Value = gintprodbuscado(v, 2) 'des.producto
        stra = "SELECT FR93CODUNIMEDIDA,FRH7CODFORMFAR,FR73DOSIS," & _
               "FR73REFERENCIA,FR73INDPRODSAN " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(0).Columns(4).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(9), rsta.rdoColumns("FRH7CODFORMFAR").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(10), rsta.rdoColumns("FR73DOSIS").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(7), rsta.rdoColumns("FR73REFERENCIA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(11), rsta.rdoColumns("FR93CODUNIMEDIDA").Value)
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(12), rsta.rdoColumns("FR73INDPRODSAN").Value)
        grdDBGrid1(0).Update
        rsta.Close
        Set rsta = Nothing
      Else
        mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
                  Chr(13), vbInformation)
      End If
      noinsertar = True
    Next v
  End If
  gintprodtotal = 0
End If

cmdBuscar.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    
    .strName = "Protocolos de Quir�fano"
      
    .strTable = "FRI200"
    .blnAskPrimary = False
    
    Call .FormAddOrderField("FRI2CODPROTQUI", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Protocolos de Quir�fano")

    Call .FormAddFilterWhere(strKey, "FRI2CODPROTQUI", "C�digo Protocolo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRI2DESPROTQUI", "Descripci�n Protocolo", cwString)
    Call .FormAddFilterWhere(strKey, "FRI8CODTIPPROTQ", "Tipo de Protocolo", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "FRI2CODPROTQUI", "C�digo Protocolo")
    Call .FormAddFilterOrder(strKey, "FRI2DESPROTQUI", "Descripci�n Protocolo")

  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
    .strName = "Productos"
    
    .intAllowance = cwAllowDelete + cwAllowModify
    
    .strTable = "FRI300"
            
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
    Call .FormAddRelation("FRI2CODPROTQUI", txtText1(4))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Productos")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRI2CANTIDAD", "Cantidad", cwDecimal)
    
    Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "Producto")
    Call .FormAddFilterOrder(strKey, "FRI2CANTIDAD", "Cantidad")
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�digo Protocolo", "FRI2CODPROTQUI", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�digo Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "FRI2CANTIDAD", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "C�d.Int", "", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "F.F", "", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "U.M", "", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Medicamento/Material", "", cwBoolean)
    
    
    Call .FormCreateInfo(objMasterInfo)

    Call .FormChangeColor(objMultiInfo)
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(6), "FR73CODINTFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(7), "FR73REFERENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(8), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(9), "FRH7CODFORMFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(10), "FR73DOSIS")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(11), "FR93CODUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(12), "FR73INDPRODSAN")
    
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).intKeyNo = 1
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FRI8CODTIPPROTQ", "SELECT FRI8DESTIPPROTQ FROM FRI800 WHERE FRI8CODTIPPROTQ = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(1), "FRI8DESTIPPROTQ")
    
    .CtrlGetInfo(txtText1(0)).blnForeign = True
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
       
  grdDBGrid1(1).Columns(1).Width = 1300
  grdDBGrid1(1).Columns(2).Width = 4300
  grdDBGrid1(1).Columns(3).Width = 1500
  grdDBGrid1(1).Columns(4).Width = 2800
  
  grdDBGrid1(0).Columns(0).Width = 0
  grdDBGrid1(0).Columns(3).Visible = False
  grdDBGrid1(0).Columns(4).Visible = False
  grdDBGrid1(0).Columns(12).Visible = False
  
  grdDBGrid1(0).Columns(5).Width = 1000
  grdDBGrid1(0).Columns(6).Width = 800
  grdDBGrid1(0).Columns(7).Width = 1700
  grdDBGrid1(0).Columns(8).Width = 4300
  grdDBGrid1(0).Columns(9).Width = 800
  grdDBGrid1(0).Columns(10).Width = 1300
  grdDBGrid1(0).Columns(11).Width = 900
  

  Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)
   
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
   Dim objField As clsCWFieldSearch

   If strFormName = "Protocolos de Quir�fano" And strCtrl = "txtText1(0)" Then
      Set objSearch = New clsCWSearch
      With objSearch
         .strTable = "FRI800"
'     .strWhere = "WHERE AD32CODTIPODPTO=3 AND AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL)))"
'     .strOrder = "ORDER BY AD02CODDPTO ASC"
     
         Set objField = .AddField("FRI8CODTIPPROTQ")
         objField.strSmallDesc = "C�digo de Tipo de Protocolo"
         
         Set objField = .AddField("FRI8DESTIPPROTQ")
         objField.strSmallDesc = "Descripci�n de Tipo de Protocolo"
         
         If .Search Then
            Call objWinInfo.CtrlSet(txtText1(0), .cllValues("FRI8CODTIPPROTQ"))
         End If
      End With
      Set objSearch = Nothing
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
    
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String

  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  If (btnButton.Index = 2) And objWinInfo.objWinActiveForm.strName = "Protocolos de Quir�fano" Then
    sqlstr = "SELECT FRI2CODPROTQUI_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(4), rsta.rdoColumns(0).Value)
    txtText1(5).SetFocus
    rsta.Close
    Set rsta = Nothing
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String
    
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  If (intIndex = 10) And objWinInfo.objWinActiveForm.strName = "Protocolos de Quir�fano" Then
    sqlstr = "SELECT FRI2CODPROTQUI_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(4).Text = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
  End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

