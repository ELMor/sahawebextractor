VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmRedHojQuirof 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Redactar Hoja de Quirofano"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   109
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   9360
      Top             =   3960
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   107
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab tabGeneral1 
      Height          =   7455
      Index           =   1
      Left            =   120
      TabIndex        =   108
      TabStop         =   0   'False
      Top             =   480
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   13150
      _Version        =   327681
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Datos Quir�fano"
      TabPicture(0)   =   "FR0126.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Intervenciones"
      TabPicture(1)   =   "FR0126.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Material y Medicaci�n Quir�fano"
      TabPicture(2)   =   "FR0126.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmdbuscarprotesis"
      Tab(2).Control(1)=   "cmdleerlector(0)"
      Tab(2).Control(2)=   "cmdbuscarprod(0)"
      Tab(2).Control(3)=   "cmdbuscargruprod(0)"
      Tab(2).Control(4)=   "cmdgruterapmedicamento(0)"
      Tab(2).Control(5)=   "cmdbuscarmaterial(0)"
      Tab(2).Control(6)=   "cmdbuscarprot(0)"
      Tab(2).Control(7)=   "cmdgruterapmaterial(0)"
      Tab(2).Control(8)=   "fraFrame1(3)"
      Tab(2).Control(9)=   "cmdinter(0)"
      Tab(2).ControlCount=   10
      TabCaption(3)   =   "Datos Anestesia"
      TabPicture(3)   =   "FR0126.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraFrame1(2)"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Material y Medicaci�n Anestesia"
      TabPicture(4)   =   "FR0126.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "fraFrame1(4)"
      Tab(4).Control(1)=   "cmdgruterapmaterial(1)"
      Tab(4).Control(2)=   "cmdbuscarprot(1)"
      Tab(4).Control(3)=   "cmdbuscarmaterial(1)"
      Tab(4).Control(4)=   "cmdgruterapmedicamento(1)"
      Tab(4).Control(5)=   "cmdbuscargruprod(1)"
      Tab(4).Control(6)=   "cmdbuscarprod(1)"
      Tab(4).Control(7)=   "cmdinter(1)"
      Tab(4).Control(8)=   "cmdleerlector(1)"
      Tab(4).Control(9)=   "cmdTerminar"
      Tab(4).ControlCount=   10
      Begin VB.CommandButton cmdTerminar 
         Caption         =   "FACTURAR"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   690
         Left            =   -64800
         TabIndex        =   161
         Top             =   6360
         Width           =   1215
      End
      Begin VB.CommandButton cmdbuscarprotesis 
         Caption         =   "Protesis"
         Height          =   375
         Left            =   -65040
         TabIndex        =   68
         Top             =   2040
         Width           =   1600
      End
      Begin VB.CommandButton cmdleerlector 
         Caption         =   "LECTOR"
         Height          =   1035
         Index           =   1
         Left            =   -64680
         Picture         =   "FR0126.frx":008C
         Style           =   1  'Graphical
         TabIndex        =   105
         Top             =   4080
         Width           =   975
      End
      Begin VB.CommandButton cmdleerlector 
         Caption         =   "LECTOR"
         Height          =   1035
         Index           =   0
         Left            =   -64680
         Picture         =   "FR0126.frx":08CE
         Style           =   1  'Graphical
         TabIndex        =   73
         Top             =   4920
         Width           =   975
      End
      Begin VB.CommandButton cmdinter 
         Caption         =   "Interacciones"
         Height          =   375
         Index           =   1
         Left            =   -65040
         TabIndex        =   106
         Top             =   5400
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarprod 
         Caption         =   "Medicamentos"
         Height          =   375
         Index           =   1
         Left            =   -65040
         TabIndex        =   99
         Top             =   480
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscargruprod 
         Caption         =   "Grupo Productos"
         Height          =   375
         Index           =   1
         Left            =   -65040
         TabIndex        =   101
         Top             =   1440
         Width           =   1600
      End
      Begin VB.CommandButton cmdgruterapmedicamento 
         Caption         =   "Grupos Terap�uticos      Medicamentos"
         Height          =   615
         Index           =   1
         Left            =   -65040
         TabIndex        =   103
         Top             =   2400
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarmaterial 
         Caption         =   "Productos"
         Height          =   375
         Index           =   1
         Left            =   -65040
         TabIndex        =   100
         Top             =   960
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarprot 
         Caption         =   "Protocolos"
         Height          =   375
         Index           =   1
         Left            =   -65040
         TabIndex        =   102
         Top             =   1920
         Width           =   1600
      End
      Begin VB.CommandButton cmdgruterapmaterial 
         Caption         =   "Grupos Terap�uticos         Productos"
         Height          =   615
         Index           =   1
         Left            =   -65040
         TabIndex        =   104
         Top             =   3120
         Width           =   1600
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6960
         Index           =   4
         Left            =   -74880
         TabIndex        =   98
         Top             =   360
         Width           =   9735
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6435
            Index           =   4
            Left            =   120
            TabIndex        =   97
            Top             =   360
            Width           =   9465
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   2
            stylesets(0).Name=   "Material"
            stylesets(0).BackColor=   12615935
            stylesets(0).Picture=   "FR0126.frx":1110
            stylesets(1).Name=   "Medicamento"
            stylesets(1).BackColor=   16777215
            stylesets(1).Picture=   "FR0126.frx":112C
            SelectTypeRow   =   3
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   16695
            _ExtentY        =   11351
            _StockProps     =   79
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6975
         Index           =   2
         Left            =   -74880
         TabIndex        =   96
         Top             =   360
         Width           =   11340
         Begin TabDlg.SSTab tabTab1 
            Height          =   6375
            Index           =   2
            Left            =   120
            TabIndex        =   94
            TabStop         =   0   'False
            Top             =   360
            Width           =   11055
            _ExtentX        =   19500
            _ExtentY        =   11245
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0126.frx":1148
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(25)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(26)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(27)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(31)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(65)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(0)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(3)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(13)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(17)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(18)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "dtcDateCombo1(3)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "dtcDateCombo1(1)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(46)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(48)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(50)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(51)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(52)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "Command1"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(6)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(7)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(58)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "Frame3"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(28)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(29)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).ControlCount=   24
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0126.frx":1164
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(1)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               DataField       =   "FR43TIPOANEST"
               Height          =   330
               Index           =   29
               Left            =   360
               TabIndex        =   76
               Tag             =   "Tipo Anestesia"
               Top             =   1080
               Width           =   3615
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR43HORAFIN"
               Height          =   330
               Index           =   28
               Left            =   7200
               TabIndex        =   92
               Tag             =   "Hora Fin"
               Top             =   1680
               Width           =   720
            End
            Begin VB.Frame Frame3 
               Caption         =   "Equipo Anestesia"
               Height          =   2415
               Left            =   360
               TabIndex        =   146
               Top             =   3600
               Width           =   6495
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFF00&
                  DataField       =   "SG02COD_ANE"
                  Height          =   330
                  Index           =   53
                  Left            =   240
                  TabIndex        =   77
                  Tag             =   "C�digo Anestesista"
                  Top             =   600
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   55
                  Left            =   1560
                  TabIndex        =   78
                  TabStop         =   0   'False
                  Tag             =   "Apellido Anestesista"
                  Top             =   600
                  Width           =   4485
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_ENF"
                  Height          =   330
                  Index           =   61
                  Left            =   240
                  TabIndex        =   81
                  Tag             =   "C�digo Enfermera"
                  Top             =   1920
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_CIR"
                  Height          =   330
                  Index           =   63
                  Left            =   240
                  TabIndex        =   79
                  Tag             =   "Anestesista 2"
                  Top             =   1200
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   64
                  Left            =   1560
                  TabIndex        =   80
                  TabStop         =   0   'False
                  Tag             =   "Apellido Anestesista 2"
                  Top             =   1200
                  Width           =   4485
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   62
                  Left            =   1560
                  TabIndex        =   82
                  TabStop         =   0   'False
                  Tag             =   "Apellido Enfermera"
                  Top             =   1920
                  Width           =   4485
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Anestesista"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   20
                  Left            =   240
                  TabIndex        =   149
                  Top             =   360
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Enfermera"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   36
                  Left            =   240
                  TabIndex        =   148
                  Top             =   1680
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Anestesista 2"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   41
                  Left            =   240
                  TabIndex        =   147
                  Top             =   960
                  Width           =   1155
               End
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFF00&
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   58
               Left            =   8760
               TabIndex        =   93
               Tag             =   "C�digo Paciente"
               Top             =   5280
               Visible         =   0   'False
               Width           =   1500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               DataField       =   "FR97CODQUIROFANO"
               Height          =   330
               Index           =   7
               Left            =   4920
               TabIndex        =   87
               Tag             =   "C�d. Quir�fano"
               Top             =   480
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   6
               Left            =   5640
               TabIndex        =   88
               TabStop         =   0   'False
               Tag             =   "Descripci�n Quir�fano"
               Top             =   480
               Width           =   4125
            End
            Begin VB.CommandButton Command1 
               Caption         =   "Firmar"
               Height          =   330
               Left            =   6960
               TabIndex        =   86
               Top             =   3000
               Width           =   1155
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   52
               Left            =   7440
               TabIndex        =   85
               TabStop         =   0   'False
               Tag             =   "Apellido 1� Persona"
               Top             =   2520
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD"
               Height          =   330
               Index           =   51
               Left            =   6240
               TabIndex        =   84
               Tag             =   "Firma Digital"
               Top             =   2520
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR43DESINTERVENCION"
               Height          =   810
               Index           =   50
               Left            =   360
               MultiLine       =   -1  'True
               ScrollBars      =   1  'Horizontal
               TabIndex        =   83
               Tag             =   "Descripci�n Intervenci�n"
               Top             =   2520
               Width           =   4815
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR43CODHOJAANEST"
               Height          =   330
               Index           =   48
               Left            =   360
               TabIndex        =   75
               Tag             =   "C�d. Hoja Anestesia"
               Top             =   360
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFF00&
               DataField       =   "FR43HORAINTERVEN"
               Height          =   330
               Index           =   46
               Left            =   7200
               TabIndex        =   90
               Tag             =   "Hora Intervenci�n"
               Top             =   1080
               Width           =   735
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5865
               Index           =   1
               Left            =   -74880
               TabIndex        =   95
               TabStop         =   0   'False
               Top             =   240
               Width           =   10455
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18441
               _ExtentY        =   10345
               _StockProps     =   79
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR43FECINTERVEN"
               Height          =   330
               Index           =   1
               Left            =   4920
               TabIndex        =   89
               Tag             =   "Fecha Intervenci�n"
               Top             =   1080
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16776960
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR43FECFIN"
               Height          =   330
               Index           =   3
               Left            =   4920
               TabIndex        =   91
               Tag             =   "Fecha Fin"
               Top             =   1680
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tipo Anestesia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   360
               TabIndex        =   157
               Top             =   840
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Fin"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   7200
               TabIndex        =   156
               Top             =   1440
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fin Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   4920
               TabIndex        =   155
               Top             =   1440
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Paciente"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   8760
               TabIndex        =   145
               Top             =   5040
               Visible         =   0   'False
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d. Quir�fano"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   4920
               TabIndex        =   144
               Top             =   240
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Firma Digital"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   65
               Left            =   6240
               TabIndex        =   131
               Top             =   2280
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   31
               Left            =   360
               TabIndex        =   130
               Top             =   2280
               Width           =   2145
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hoja Anestesia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   360
               TabIndex        =   129
               Top             =   120
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   26
               Left            =   4920
               TabIndex        =   128
               Top             =   840
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   25
               Left            =   7200
               TabIndex        =   127
               Top             =   840
               Width           =   1575
            End
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6975
         Index           =   1
         Left            =   -74880
         TabIndex        =   62
         Top             =   360
         Width           =   11220
         Begin TabDlg.SSTab tabTab1 
            Height          =   6615
            Index           =   1
            Left            =   120
            TabIndex        =   61
            TabStop         =   0   'False
            Top             =   240
            Width           =   10935
            _ExtentX        =   19288
            _ExtentY        =   11668
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0126.frx":1180
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(52)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(53)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(60)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(1)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(63)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(6)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(19)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "cboDBCombo1(3)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(41)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(47)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(54)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(4)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(5)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(39)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(38)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "Frame1"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "Frame2"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "chkCheck1(12)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(8)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(17)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "Command2"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "Command3"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).ControlCount=   22
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0126.frx":119C
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(2)"
            Tab(1).ControlCount=   1
            Begin VB.CommandButton Command3 
               Caption         =   "Nueva Intervenci�n"
               Height          =   375
               Left            =   6240
               TabIndex        =   160
               Top             =   2280
               Visible         =   0   'False
               Width           =   1935
            End
            Begin VB.CommandButton Command2 
               Caption         =   "Cambiar Intervenci�n"
               Height          =   375
               Left            =   6240
               TabIndex        =   159
               Top             =   1800
               Visible         =   0   'False
               Width           =   1935
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   17
               Left            =   120
               TabIndex        =   39
               TabStop         =   0   'False
               Tag             =   "Cod.Interv."
               Top             =   1320
               Width           =   1005
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FRG3NUMMININT"
               Height          =   330
               Index           =   8
               Left            =   3360
               TabIndex        =   46
               Tag             =   "Dur.Int.|Duraci�n Intervenci�n (min.)"
               Top             =   600
               Width           =   615
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Principal"
               DataField       =   "FRG3INDPRINCIPAL"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   2040
               TabIndex        =   38
               Tag             =   "Intervenci�n Principal?"
               Top             =   720
               Width           =   1335
            End
            Begin VB.Frame Frame2 
               Caption         =   "Equipo Colaborador"
               Height          =   3495
               Left            =   5280
               TabIndex        =   141
               Top             =   3000
               Width           =   5295
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_CCL"
                  Height          =   330
                  Index           =   30
                  Left            =   120
                  TabIndex        =   57
                  Tag             =   "C�digo Cirujano Colaborador"
                  Top             =   480
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   31
                  Left            =   1320
                  TabIndex        =   58
                  TabStop         =   0   'False
                  Tag             =   "Apellido Cirujano Colaborador"
                  Top             =   480
                  Width           =   3885
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_ACL"
                  Height          =   330
                  Index           =   34
                  Left            =   120
                  TabIndex        =   59
                  Tag             =   "C�digo Ayudante Colaborador"
                  Top             =   1080
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   35
                  Left            =   1320
                  TabIndex        =   60
                  TabStop         =   0   'False
                  Tag             =   "Apellido Ayudante Colaborador"
                  Top             =   1080
                  Width           =   3885
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Cirujano"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   62
                  Left            =   120
                  TabIndex        =   143
                  Top             =   240
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Ayudante"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   61
                  Left            =   120
                  TabIndex        =   142
                  Top             =   840
                  Width           =   1455
               End
            End
            Begin VB.Frame Frame1 
               Caption         =   "Equipo Quir�rgico"
               Height          =   3495
               Left            =   120
               TabIndex        =   135
               Top             =   3000
               Width           =   5175
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   26
                  Left            =   1440
                  TabIndex        =   56
                  TabStop         =   0   'False
                  Tag             =   "Apellido Enfermera"
                  Top             =   2880
                  Width           =   3525
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_ENF"
                  Height          =   330
                  Index           =   25
                  Left            =   240
                  TabIndex        =   55
                  Tag             =   "C�digo Enfermera"
                  Top             =   2880
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   22
                  Left            =   1440
                  TabIndex        =   54
                  TabStop         =   0   'False
                  Tag             =   "Apellido Instrumentista"
                  Top             =   2280
                  Width           =   3525
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   24
                  Left            =   1440
                  TabIndex        =   52
                  TabStop         =   0   'False
                  Tag             =   "Apellido Segundo Ayudante"
                  Top             =   1680
                  Width           =   3525
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_SAY"
                  Height          =   330
                  Index           =   23
                  Left            =   240
                  TabIndex        =   51
                  Tag             =   "C�digo Segundo Ayudante"
                  Top             =   1680
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   20
                  Left            =   1440
                  TabIndex        =   50
                  TabStop         =   0   'False
                  Tag             =   "Apellido Primer Ayudante"
                  Top             =   1080
                  Width           =   3525
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_PAY"
                  Height          =   330
                  Index           =   19
                  Left            =   240
                  TabIndex        =   49
                  Tag             =   "C�digo Primer Ayudante"
                  Top             =   1080
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   16
                  Left            =   1440
                  TabIndex        =   48
                  TabStop         =   0   'False
                  Tag             =   "Apellido Cirujano"
                  Top             =   480
                  Width           =   3525
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFF00&
                  DataField       =   "SG02COD_CIR"
                  Height          =   330
                  Index           =   15
                  Left            =   240
                  TabIndex        =   47
                  Tag             =   "C�digo Cirujano"
                  Top             =   480
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_INS"
                  Height          =   330
                  Index           =   21
                  Left            =   240
                  TabIndex        =   53
                  Tag             =   "C�digo Instrumentista"
                  Top             =   2280
                  Width           =   1155
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Enfermera"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   47
                  Left            =   240
                  TabIndex        =   140
                  Top             =   2640
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Segundo Ayudante"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   48
                  Left            =   240
                  TabIndex        =   139
                  Top             =   1440
                  Width           =   1695
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Primer Ayudante"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   54
                  Left            =   240
                  TabIndex        =   138
                  Top             =   840
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Cirujano"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   55
                  Left            =   240
                  TabIndex        =   137
                  Top             =   240
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Instrumentista"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   56
                  Left            =   240
                  TabIndex        =   136
                  Top             =   2040
                  Width           =   1335
               End
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFF00&
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   38
               Left            =   5160
               TabIndex        =   41
               Tag             =   "C�d.Servicio"
               Top             =   1320
               Width           =   500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   39
               Left            =   5760
               TabIndex        =   42
               TabStop         =   0   'False
               Tag             =   "Desc.Servicio"
               Top             =   1320
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   1200
               TabIndex        =   40
               TabStop         =   0   'False
               Tag             =   "Descripci�n Intervenci�n"
               Top             =   1320
               Width           =   3765
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFF00&
               DataField       =   "PR04NUMACTPLAN"
               Height          =   330
               Index           =   4
               Left            =   8880
               TabIndex        =   45
               Tag             =   "Act.Planif."
               Top             =   2520
               Visible         =   0   'False
               Width           =   915
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FRG3CODINTERVENCION"
               Height          =   330
               Index           =   54
               Left            =   120
               TabIndex        =   37
               Tag             =   "C�d.Intervenci�n"
               Top             =   600
               Width           =   1695
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR44CODHOJAQUIRO"
               Height          =   330
               Index           =   47
               Left            =   6240
               TabIndex        =   122
               Tag             =   "C�d. Hoja Quir�fano"
               Top             =   600
               Visible         =   0   'False
               Width           =   1680
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FRG3DESINTERVENCION"
               Height          =   330
               Index           =   41
               Left            =   120
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   43
               Tag             =   "Descripci�n Intervenci�n"
               Top             =   2040
               Width           =   5295
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   6225
               Index           =   2
               Left            =   -74880
               TabIndex        =   63
               TabStop         =   0   'False
               Top             =   120
               Width           =   10215
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18018
               _ExtentY        =   10980
               _StockProps     =   79
            End
            Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
               DataField       =   "FRG3POSIPACIENTE"
               Height          =   330
               Index           =   3
               Left            =   120
               TabIndex        =   44
               Tag             =   "Posici�n|Posici�n del Paciente"
               Top             =   2640
               Width           =   5085
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets.count =   2
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777215
               stylesets(0).Picture=   "FR0126.frx":11B8
               stylesets(1).Name=   "Inactivo"
               stylesets(1).BackColor=   255
               stylesets(1).Picture=   "FR0126.frx":11D4
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns(0).Width=   11165
               Columns(0).Caption=   "Posici�n del Paciente"
               Columns(0).Name =   "Nombre"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               _ExtentX        =   8969
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 0"
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Posici�n del Paciente"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   19
               Left            =   120
               TabIndex        =   158
               Top             =   2400
               Width           =   1860
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Duraci�n Intervenci�n (min.)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   3360
               TabIndex        =   152
               Top             =   360
               Width           =   2430
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dpto.Realizador"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   63
               Left            =   5160
               TabIndex        =   133
               Top             =   1080
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   132
               Top             =   1080
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   60
               Left            =   120
               TabIndex        =   125
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d. Hoja Quir�fano"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   53
               Left            =   6240
               TabIndex        =   124
               Top             =   360
               Visible         =   0   'False
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   52
               Left            =   120
               TabIndex        =   123
               Top             =   1800
               Width           =   2145
            End
         End
      End
      Begin VB.CommandButton cmdbuscarprod 
         Caption         =   "Medicamentos"
         Height          =   375
         Index           =   0
         Left            =   -65040
         TabIndex        =   66
         Top             =   1080
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscargruprod 
         Caption         =   "Grupo Productos"
         Height          =   375
         Index           =   0
         Left            =   -65040
         TabIndex        =   69
         Top             =   2520
         Width           =   1600
      End
      Begin VB.CommandButton cmdgruterapmedicamento 
         Caption         =   "Grupos Terap�uticos      Medicamentos"
         Height          =   615
         Index           =   0
         Left            =   -65040
         TabIndex        =   71
         Top             =   3480
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarmaterial 
         Caption         =   "Productos"
         Height          =   375
         Index           =   0
         Left            =   -65040
         TabIndex        =   67
         Top             =   1560
         Width           =   1600
      End
      Begin VB.CommandButton cmdbuscarprot 
         Caption         =   "Protocolos"
         Height          =   375
         Index           =   0
         Left            =   -65040
         TabIndex        =   70
         Top             =   3000
         Width           =   1600
      End
      Begin VB.CommandButton cmdgruterapmaterial 
         Caption         =   "Grupos Terap�uticos         Productos"
         Height          =   615
         Index           =   0
         Left            =   -65040
         TabIndex        =   72
         Top             =   4200
         Width           =   1600
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6840
         Index           =   3
         Left            =   -74880
         TabIndex        =   65
         Top             =   480
         Width           =   9735
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6315
            Index           =   3
            Left            =   120
            TabIndex        =   64
            Top             =   360
            Width           =   9465
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   2
            stylesets(0).Name=   "Material"
            stylesets(0).BackColor=   12615935
            stylesets(0).Picture=   "FR0126.frx":11F0
            stylesets(1).Name=   "Medicamento"
            stylesets(1).BackColor=   16777215
            stylesets(1).Picture=   "FR0126.frx":120C
            SelectTypeRow   =   3
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   16695
            _ExtentY        =   11139
            _StockProps     =   79
         End
      End
      Begin VB.CommandButton cmdinter 
         Caption         =   "Interacciones"
         Height          =   375
         Index           =   0
         Left            =   -65040
         TabIndex        =   74
         Top             =   6240
         Width           =   1575
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6975
         Index           =   0
         Left            =   120
         TabIndex        =   35
         Top             =   360
         Width           =   11220
         Begin TabDlg.SSTab tabTab1 
            Height          =   6615
            Index           =   0
            Left            =   120
            TabIndex        =   34
            TabStop         =   0   'False
            Top             =   240
            Width           =   10935
            _ExtentX        =   19288
            _ExtentY        =   11668
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0126.frx":1228
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(16)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(15)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(14)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(28)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(29)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(30)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(45)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(10)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(12)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "dtcDateCombo1(2)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "dtcDateCombo1(0)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "tab1"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(2)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "chkCheck1(1)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "chkCheck1(0)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "chkCheck1(2)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(3)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(0)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(9)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(1)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(43)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(44)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "cmdFirmar"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(27)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txtText1(18)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).ControlCount=   25
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0126.frx":1244
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(0)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR44HORAFIN"
               Height          =   330
               Index           =   18
               Left            =   2400
               TabIndex        =   7
               Tag             =   "Hora Fin"
               Top             =   1920
               Width           =   720
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFF00&
               DataField       =   "FR43CODHOJAANEST"
               Height          =   330
               Index           =   27
               Left            =   7560
               TabIndex        =   32
               Tag             =   "C�digo Hoja Anestesia"
               Top             =   1080
               Visible         =   0   'False
               Width           =   1155
            End
            Begin VB.CommandButton cmdFirmar 
               Caption         =   "Firmar"
               Height          =   330
               Left            =   6360
               TabIndex        =   33
               Top             =   2520
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   44
               Left            =   2400
               TabIndex        =   2
               TabStop         =   0   'False
               Tag             =   "Descripci�n Quir�fano"
               Top             =   360
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   43
               Left            =   6960
               TabIndex        =   31
               TabStop         =   0   'False
               Tag             =   "Apellido 1�"
               Top             =   2040
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFF00&
               DataField       =   "FR97CODQUIROFANO"
               Height          =   330
               Index           =   1
               Left            =   1680
               TabIndex        =   1
               Tag             =   "C�d. Quir�fano"
               Top             =   360
               Width           =   600
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD"
               Height          =   330
               Index           =   9
               Left            =   5760
               TabIndex        =   30
               Tag             =   "Firma Digital"
               Top             =   2040
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR44CODHOJAQUIRO"
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   0
               Tag             =   "C�d. Hoja Quir�fano"
               Top             =   360
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR44NUMORDENDIA"
               Height          =   330
               Index           =   3
               Left            =   4200
               TabIndex        =   5
               Tag             =   "N�mero Orden en el D�a"
               Top             =   1320
               Width           =   500
            End
            Begin VB.CheckBox chkCheck1 
               Alignment       =   1  'Right Justify
               Caption         =   "Urg.Absoluta"
               DataField       =   "FR44INDURGENTE"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   3195
               TabIndex        =   10
               Tag             =   "Urg.Absoluta"
               Top             =   2760
               Width           =   1455
            End
            Begin VB.CheckBox chkCheck1 
               Alignment       =   1  'Right Justify
               Caption         =   "Electiva"
               DataField       =   "FR44INDPROGRAMADA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   8
               Tag             =   "Electiva"
               Top             =   2760
               Width           =   1095
            End
            Begin VB.CheckBox chkCheck1 
               Alignment       =   1  'Right Justify
               Caption         =   "Urg.Selectiva"
               DataField       =   "FR44INDIMPREVISTA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   1395
               TabIndex        =   9
               Tag             =   "Urg.Selectiva"
               Top             =   2760
               Width           =   1575
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFF00&
               DataField       =   "FR44HORAINTERV"
               Height          =   330
               Index           =   2
               Left            =   2400
               TabIndex        =   4
               Tag             =   "Hora Intervenci�n"
               Top             =   1320
               Width           =   720
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   6225
               Index           =   0
               Left            =   -74760
               TabIndex        =   36
               TabStop         =   0   'False
               Top             =   240
               Width           =   10215
               _Version        =   131078
               DataMode        =   2
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18018
               _ExtentY        =   10980
               _StockProps     =   79
            End
            Begin TabDlg.SSTab tab1 
               Height          =   2775
               Left            =   120
               TabIndex        =   110
               TabStop         =   0   'False
               Top             =   3480
               Width           =   10215
               _ExtentX        =   18018
               _ExtentY        =   4895
               _Version        =   327681
               Style           =   1
               Tabs            =   2
               TabsPerRow      =   2
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Paciente"
               TabPicture(0)   =   "FR0126.frx":1260
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(11)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(4)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(7)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(8)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "lblLabel1(9)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "lblLabel1(2)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "lblLabel1(5)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "txtText1(11)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "txtText1(12)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "txtText1(10)"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "txtText1(13)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "txtText1(14)"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).Control(12)=   "txtProceso"
               Tab(0).Control(12).Enabled=   0   'False
               Tab(0).Control(13)=   "txtAsistencia"
               Tab(0).Control(13).Enabled=   0   'False
               Tab(0).ControlCount=   14
               TabCaption(1)   =   "Indicativos"
               TabPicture(1)   =   "FR0126.frx":127C
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "lblLabel1(21)"
               Tab(1).Control(1)=   "chkCheck1(11)"
               Tab(1).Control(2)=   "chkCheck1(3)"
               Tab(1).Control(3)=   "chkCheck1(4)"
               Tab(1).Control(4)=   "chkCheck1(5)"
               Tab(1).Control(5)=   "chkCheck1(6)"
               Tab(1).Control(6)=   "chkCheck1(7)"
               Tab(1).Control(7)=   "chkCheck1(8)"
               Tab(1).Control(8)=   "chkCheck1(9)"
               Tab(1).Control(9)=   "chkCheck1(10)"
               Tab(1).Control(10)=   "txtText1(42)"
               Tab(1).Control(11)=   "chkCheck1(13)"
               Tab(1).Control(12)=   "chkCheck1(14)"
               Tab(1).ControlCount=   13
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Isquemia"
                  DataField       =   "FR44INDISQUEMIA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   14
                  Left            =   -72480
                  TabIndex        =   24
                  Tag             =   "Isquemia?"
                  Top             =   1080
                  Width           =   1455
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Instrumental"
                  DataField       =   "FR44INDINSTRUMENTAL"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   13
                  Left            =   -72480
                  TabIndex        =   23
                  Tag             =   "Instrumental?"
                  Top             =   720
                  Width           =   1455
               End
               Begin VB.TextBox txtAsistencia 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Left            =   6000
                  TabIndex        =   14
                  Tag             =   "Asistencia"
                  Top             =   960
                  Width           =   1695
               End
               Begin VB.TextBox txtProceso 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Left            =   4080
                  TabIndex        =   13
                  Tag             =   "Proceso"
                  Top             =   960
                  Width           =   1695
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR44DESOTROS"
                  Height          =   690
                  Index           =   42
                  Left            =   -70440
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   29
                  Tag             =   "Descripci�n otros Indicativos"
                  Top             =   1440
                  Width           =   5295
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Cta. gasas"
                  DataField       =   "FR44INDCUANTAGASAS"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   10
                  Left            =   -72480
                  TabIndex        =   27
                  Tag             =   "Cuenta de Gasas?"
                  Top             =   2160
                  Width           =   1455
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "C.E.C."
                  DataField       =   "FR44INDCEC"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   9
                  Left            =   -72480
                  TabIndex        =   26
                  Tag             =   "C.E.C.?"
                  Top             =   1800
                  Width           =   1455
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Laser"
                  DataField       =   "FR44INDLASER"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   -72480
                  TabIndex        =   25
                  Tag             =   "Laser?"
                  Top             =   1440
                  Width           =   1455
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Rx"
                  DataField       =   "FR44INDRX"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   7
                  Left            =   -74760
                  TabIndex        =   22
                  Tag             =   "Rayos X?"
                  Top             =   2160
                  Width           =   1815
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Laboratorio"
                  DataField       =   "FR44INDMICROSCOPIA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   6
                  Left            =   -74760
                  TabIndex        =   21
                  Tag             =   "Laboratorio?"
                  Top             =   1800
                  Width           =   1815
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Endoscopia"
                  DataField       =   "FR44INDENDOSCOPIA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   5
                  Left            =   -74760
                  TabIndex        =   20
                  Tag             =   "Endoscopia?"
                  Top             =   1440
                  Width           =   1815
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Radioterapia"
                  DataField       =   "FR44INDRADIOTERAP"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   -74760
                  TabIndex        =   19
                  Tag             =   "Radioterapia?"
                  Top             =   1080
                  Width           =   1815
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Anat. patol�gica"
                  DataField       =   "FR44INDANATPATO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   -74760
                  TabIndex        =   18
                  Tag             =   "Anatom�a Patol�gica?"
                  Top             =   720
                  Width           =   1815
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Otros"
                  DataField       =   "FR44INDOTROS"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   -70320
                  TabIndex        =   28
                  Tag             =   "Otros Indicativos?"
                  Top             =   720
                  Width           =   855
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   14
                  Left            =   5880
                  TabIndex        =   17
                  TabStop         =   0   'False
                  Tag             =   "Apellido 2� Paciente"
                  Top             =   1680
                  Width           =   2925
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   13
                  Left            =   2880
                  TabIndex        =   16
                  TabStop         =   0   'False
                  Tag             =   "Apellido 1� Paciente"
                  Top             =   1680
                  Width           =   2925
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFF00&
                  DataField       =   "CI21CODPERSONA"
                  Height          =   330
                  Index           =   10
                  Left            =   240
                  TabIndex        =   11
                  Tag             =   "C�digo Paciente"
                  Top             =   960
                  Width           =   1500
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   12
                  Left            =   240
                  TabIndex        =   15
                  TabStop         =   0   'False
                  Tag             =   "Nombre Paciente"
                  Top             =   1680
                  Width           =   2565
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   11
                  Left            =   2280
                  TabIndex        =   12
                  TabStop         =   0   'False
                  Tag             =   "C�d. Historia Paciente"
                  Top             =   960
                  Width           =   1500
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Asistencia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   5
                  Left            =   6000
                  TabIndex        =   151
                  Top             =   720
                  Width           =   885
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Proceso"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   2
                  Left            =   4080
                  TabIndex        =   150
                  Top             =   720
                  Width           =   705
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Otros Indicativos"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   21
                  Left            =   -70440
                  TabIndex        =   126
                  Top             =   1200
                  Width           =   1575
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Apellido 2�"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   9
                  Left            =   5880
                  TabIndex        =   119
                  Top             =   1440
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Apellido 1�"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   2880
                  TabIndex        =   118
                  Top             =   1440
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   7
                  Left            =   240
                  TabIndex        =   117
                  Top             =   1440
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Persona"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   240
                  TabIndex        =   116
                  Top             =   720
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Historia "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   2280
                  TabIndex        =   115
                  Top             =   720
                  Width           =   975
               End
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR44FECINTERVEN"
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   3
               Tag             =   "Fecha Intervenci�n"
               Top             =   1320
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16776960
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR44FECFIN"
               Height          =   330
               Index           =   2
               Left            =   120
               TabIndex        =   6
               Tag             =   "Fecha Fin"
               Top             =   1920
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Fin"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   2400
               TabIndex        =   154
               Top             =   1680
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fin Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   120
               TabIndex        =   153
               Top             =   1680
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hoja Anestesia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   45
               Left            =   7560
               TabIndex        =   134
               Top             =   840
               Visible         =   0   'False
               Width           =   1335
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d. Quir�fano"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   30
               Left            =   1680
               TabIndex        =   121
               Top             =   120
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Firma Digital"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   29
               Left            =   5760
               TabIndex        =   120
               Top             =   1800
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hoja Quir�fano"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   28
               Left            =   120
               TabIndex        =   114
               Top             =   120
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N�. ord./d�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   4200
               TabIndex        =   113
               Top             =   1080
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   120
               TabIndex        =   112
               Top             =   1080
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Inicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   2400
               TabIndex        =   111
               Top             =   1080
               Width           =   1575
            End
         End
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRedHojQuirof"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRedHojQuirof (FR0126.FRM)                                 *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Redactar Hoja Quir�fano                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim auxHojAnest
Dim auxHojQuirof
Dim auxProcPrinc
Dim auxAsisPrinc
Dim blnInLoad As Boolean
Dim glActuacionPedida

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1





Private Sub cmdbuscarprotesis_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strTerminada As String
Dim rstTerminada As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset

blnInLoad = False

    strTerminada = "SELECT COUNT(*) FROM FR4400 WHERE FR44INDESTHQ>=2 AND FR44CODHOJAQUIRO=" & txtText1(0).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

    strTerminada = "SELECT COUNT(*) FROM FR4300 WHERE FR43INDESTHA>=2 AND FR43CODHOJAANEST=" & txtText1(48).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

cmdbuscarprotesis.Enabled = False

  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    gstrMedicamentoMaterial = "Protesis"
    Call objsecurity.LaunchProcess("FR0166")
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
                    txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "FR44CODHOJAQUIRO,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(0).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If

cmdbuscarprotesis.Enabled = True


End Sub

Private Sub cmdleerlector_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strTerminada As String
Dim rstTerminada As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

blnInLoad = False

    strTerminada = "SELECT COUNT(*) FROM FR4400 WHERE FR44INDESTHQ>=2 AND FR44CODHOJAQUIRO=" & txtText1(0).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

    strTerminada = "SELECT COUNT(*) FROM FR4300 WHERE FR43INDESTHA>=2 AND FR43CODHOJAANEST=" & txtText1(48).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

cmdleerlector(Index).Enabled = False

Load frmLECTOR
Call frmLECTOR.Show(vbModal)

If Index = 0 Then 'material quirofano
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
                    txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "FR44CODHOJAQUIRO,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(0).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & gintprodbuscado(v, 3)
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else 'material anestesia
  If txtText1(48).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
                    txtText1(48).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(48).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & gintprodbuscado(v, 3)
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If

End If

cmdleerlector(Index).Enabled = True


End Sub

Private Sub cmdTerminar_Click()
Dim strInsert, strupdate As String
Dim rstSelect As rdoResultset
Dim strSelect As String
Dim rstDpto As rdoResultset
Dim strDpto As String
Dim strTerminada As String
Dim rstTerminada As rdoResultset
Dim mensaje, servicio
    
blnInLoad = False

  objCW.SetClock (60 * 4)
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False
   
  If tlbToolbar1.Buttons(4).Enabled = True Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  End If
  If tlbToolbar1.Buttons(4).Enabled = True Then
    Exit Sub
  End If
   
  strTerminada = "SELECT COUNT(*) FROM FR4400 WHERE FR44INDESTHQ>=2 AND FR44CODHOJAQUIRO=" & txtText1(0).Text
  Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
  If rstTerminada(0).Value > 0 Then
    MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
    Exit Sub
  End If

  strTerminada = "SELECT COUNT(*) FROM FR4300 WHERE FR43INDESTHA>=2 AND FR43CODHOJAANEST=" & txtText1(48).Text
  Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
  If rstTerminada(0).Value > 0 Then
    MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
    Exit Sub
  End If
    
  mensaje = MsgBox("Esta seguro que desea facturar la hoja.", vbCritical + vbYesNo, "Aviso")
  If mensaje = vbNo Then
    Exit Sub
  End If
  
    
  If txtText1(9).Text = "" Then
    MsgBox "La Hoja de Quir�fano no est� firmada", vbCritical, "Aviso"
    Exit Sub
  ElseIf txtText1(51).Text = "" Then
    MsgBox "La Hoja de Anestesia no est� firmada", vbCritical, "Aviso"
    Exit Sub
  End If
    
cmdTerminar.Enabled = False
Screen.MousePointer = vbHourglass
Me.Enabled = False
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    strDpto = "SELECT AD02CODDPTO FROM FRG300 WHERE FR44CODHOJAQUIRO=" & _
           txtText1(0).Text & " AND FRG3INDPRINCIPAL=-1 "
    Set rstDpto = objApp.rdoConnect.OpenResultset(strDpto)
    'servicio = txtText1(38).Text
    servicio = rstDpto(0).Value
    rstDpto.Close
    Set rstDpto = Nothing

    strInsert = "INSERT INTO FR6500 ("
    strInsert = strInsert & "FR65CODIGO,CI21CODPERSONA,"
    strInsert = strInsert & "FR73CODPRODUCTO,FR65FECHA,"
    strInsert = strInsert & "FR65HORA,AD02CODDPTO,"
    strInsert = strInsert & "FR65CANTIDAD,FR65INDQUIROFANO,"
    strInsert = strInsert & "FR44CODHOJAQUIRO,AD07CODPROCESO,"
    strInsert = strInsert & "AD01CODASISTENCI)"
    strInsert = strInsert & " SELECT FR65CODIGO_SEQUENCE.NEXTVAL,"
    strInsert = strInsert & txtText1(10).Text & ","
    strInsert = strInsert & "FR1800.FR73CODPRODUCTO,"
    strInsert = strInsert & "SYSDATE,TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI')),"
    strInsert = strInsert & servicio & ","
    strInsert = strInsert & "FR1800.FR18CANTCONSUMIDA,-1,"
    'strInsert = strInsert & "DECODE(FR7300.FR73INDFACTDOSIS,-1,FR1800.FR18CANTCONSUMIDA,CEIL(FR1800.FR18CANTCONSUMIDA)),-1,"
    strInsert = strInsert & txtText1(0).Text & ","
    strInsert = strInsert & auxProcPrinc & "," & auxAsisPrinc
    strInsert = strInsert & " FROM FR1800 WHERE FR1800.FR44CODHOJAQUIRO=" & txtText1(0).Text
    strInsert = strInsert & " AND FR1800.FR18CANTCONSUMIDA>0 "
    'strInsert = strInsert & " AND FR1800.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
    
    objApp.rdoConnect.Execute strInsert, 64

    strInsert = "INSERT INTO FR6500 ("
    strInsert = strInsert & "FR65CODIGO,CI21CODPERSONA,"
    strInsert = strInsert & "FR73CODPRODUCTO,FR65FECHA,"
    strInsert = strInsert & "FR65HORA,AD02CODDPTO,"
    strInsert = strInsert & "FR65CANTIDAD,FR65INDQUIROFANO,"
    strInsert = strInsert & "FR43CODHOJAANEST,AD07CODPROCESO,"
    strInsert = strInsert & "AD01CODASISTENCI)"
    strInsert = strInsert & " SELECT FR65CODIGO_SEQUENCE.NEXTVAL,"
    strInsert = strInsert & txtText1(10).Text & ","
    strInsert = strInsert & "FR1700.FR73CODPRODUCTO,"
    strInsert = strInsert & "SYSDATE,TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI')),"
    strInsert = strInsert & servicio & ","
    strInsert = strInsert & "FR1700.FR17CANTIDAD,-1,"
    'strInsert = strInsert & "DECODE(FR7300.FR73INDFACTDOSIS,-1,FR1700.FR17CANTIDAD,CEIL(FR1700.FR17CANTIDAD)),-1,"
    strInsert = strInsert & txtText1(48).Text & ","
    strInsert = strInsert & auxProcPrinc & "," & auxAsisPrinc
    strInsert = strInsert & " FROM FR1700 WHERE FR1700.FR43CODHOJAANEST=" & txtText1(48).Text
    strInsert = strInsert & " AND FR1700.FR17CANTIDAD>0 "
    'strInsert = strInsert & " AND FR1700.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
  
    objApp.rdoConnect.Execute strInsert, 64
  

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    strupdate = "UPDATE FR4400 SET FR44INDESTHQ=2 WHERE FR44CODHOJAQUIRO=" & txtText1(0).Text
    objApp.rdoConnect.Execute strupdate, 64

    strupdate = "UPDATE FR4300 SET FR43INDESTHA=2 WHERE FR43CODHOJAANEST=" & txtText1(48).Text
    objApp.rdoConnect.Execute strupdate, 64

    Call MsgBox("La Hoja de Quir�fano ha sido FACTURADA", vbInformation, "Aviso")
  
Me.Enabled = True
Screen.MousePointer = vbDefault
cmdTerminar.Enabled = True

End Sub

Private Sub Command1_Click()
Dim strupdate As String

  If txtText1(48).Text = "" Then
    Exit Sub
  End If

  Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))

  If txtText1(53).Text = "" Then
    MsgBox "Debe Introducir el Anestesista", vbInformation, "Aviso"
    Exit Sub
  End If

  If txtText1(51).Text = "" Then
    strupdate = "UPDATE FR4300 SET FR43INDESTHA=1 WHERE FR43CODHOJAANEST=" & txtText1(48).Text
    objApp.rdoConnect.Execute strupdate, 64
        
    Call MsgBox("La Hoja de Anestesia ha sido Firmada", vbInformation, "Aviso")
    txtText1(51).SetFocus
    'Call objWinInfo.CtrlSet(txtText1(51), objsecurity.strUser)
    Call objWinInfo.CtrlSet(txtText1(51), txtText1(53).Text)
    Call objWinInfo.DataSave
  
  Else
    MsgBox "La Hoja de Anestesia ya esta firmada.", vbInformation, "Aviso"
  End If


End Sub


Private Sub Form_Activate()
  
If blnInLoad = True Then
  If glActuacionPedida = Empty And gstrLlamador = "" Then
    MsgBox "No ha seleccionado ninguna Actuaci�n", vbCritical, "Aviso"
    Unload Me
    Exit Sub
  End If
End If

If blnInLoad = True Then
  blnInLoad = False
  Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
  Call objWinInfo.DataRefresh
  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
  Call objWinInfo.DataRefresh
  Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
  Call objWinInfo.DataRefresh
  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
  blnInLoad = False
End If

  txtProceso.Text = auxProcPrinc
  txtAsistencia.Text = auxAsisPrinc

End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
 Dim i As Integer
    
    If Index = 3 Then
      If grdDBGrid1(3).Columns(5).Value <> "" Then
        If grdDBGrid1(3).Columns(17).Value <> True Then
          For i = 3 To 11
              grdDBGrid1(3).Columns(i).CellStyleSet "Medicamento"
          Next i
          For i = 13 To 17
              grdDBGrid1(3).Columns(i).CellStyleSet "Medicamento"
          Next i
        Else
          For i = 3 To 11
              grdDBGrid1(3).Columns(i).CellStyleSet "Material"
          Next i
          For i = 13 To 17
              grdDBGrid1(3).Columns(i).CellStyleSet "Material"
          Next i
        End If
      End If
    End If

    If Index = 4 Then
      If grdDBGrid1(4).Columns(5).Value <> "" Then
        If grdDBGrid1(4).Columns(17).Value <> True Then
          For i = 3 To 11
              grdDBGrid1(4).Columns(i).CellStyleSet "Medicamento"
          Next i
          For i = 13 To 17
              grdDBGrid1(4).Columns(i).CellStyleSet "Medicamento"
          Next i
        Else
          For i = 3 To 11
              grdDBGrid1(4).Columns(i).CellStyleSet "Material"
          Next i
          For i = 13 To 17
              grdDBGrid1(4).Columns(i).CellStyleSet "Material"
          Next i
        End If
      End If
    End If

End Sub





Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cmdfirmar_Click()
Dim strupdate As String
Dim strCirujano As String
Dim rstCirujano As rdoResultset

  If txtText1(0).Text = "" Then
    Exit Sub
  End If
  
  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    Exit Sub
  End If

  If txtText1(1) = "" Then
    MsgBox "Debe Introducir el Quir�fano", vbInformation, "Aviso"
    Exit Sub
  End If

  strCirujano = "SELECT SG02COD_CIR FROM FRG300 WHERE FRG3INDPRINCIPAL=-1 AND FR44CODHOJAQUIRO=" & txtText1(0).Text
  Set rstCirujano = objApp.rdoConnect.OpenResultset(strCirujano)
  If rstCirujano.EOF Then
    MsgBox "Debe Introducir el Cirujano en la Intervencion Pricipal", vbInformation, "Aviso"
    Exit Sub
  Else
    If IsNull(rstCirujano(0).Value) Then
      MsgBox "Debe Introducir el Cirujano en la Intervencion Pricipal", vbInformation, "Aviso"
      Exit Sub
    End If
  End If
  rstCirujano.Close
  Set rstCirujano = Nothing

  If txtText1(9).Text = "" Then
    strupdate = "UPDATE FR4400 SET FR44INDESTHQ=1 WHERE FR44CODHOJAQUIRO=" & txtText1(0).Text
    objApp.rdoConnect.Execute strupdate, 64
        
    Call MsgBox("La Hoja de Quir�fano ha sido Firmada", vbInformation, "Aviso")
    txtText1(9).SetFocus
    'Call objWinInfo.CtrlSet(txtText1(9), objsecurity.strUser)
    Call objWinInfo.CtrlSet(txtText1(9), txtText1(15))
    Call objWinInfo.DataSave
    
  Else
    MsgBox "La Hoja de Quir�fano ya esta firmada.", vbInformation, "Aviso"
  End If

End Sub

Private Sub cmdinter_Click(Index As Integer)
Dim rsta As rdoResultset
Dim stra As String
Dim rstprin As rdoResultset
Dim strprin As String
Dim i As Integer
Dim strTerminada As String
Dim rstTerminada As rdoResultset

blnInLoad = False

cmdinter(Index).Enabled = False
        
If Index = 0 Then
  gstrLista = ""
  'se cogen todos los principios activos de los productos que est�n en el grid y se
  'meten en una lista
  grdDBGrid1(3).MoveFirst
  For i = 0 To grdDBGrid1(3).Rows - 1
    stra = "SELECT count(*) FROM FR6400 WHERE FR73CODPRODUCTO=" & _
    grdDBGrid1(3).Columns(5).Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value > 0 Then
      strprin = "SELECT FR68CODPRINCACTIV FROM FR6400 WHERE FR73CODPRODUCTO=" & _
                grdDBGrid1(3).Columns(5).Value
      Set rstprin = objApp.rdoConnect.OpenResultset(strprin)
      If gstrLista = "" Then
        gstrLista = gstrLista & rstprin.rdoColumns(0).Value
      Else
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
      End If
      rstprin.MoveNext
      While Not rstprin.EOF
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
        rstprin.MoveNext
      Wend
      rstprin.Close
      Set rstprin = Nothing
    End If
    rsta.Close
    Set rsta = Nothing
    grdDBGrid1(3).MoveNext
  Next i
Else
  gstrLista = ""
  'se cogen todos los principios activos de los productos que est�n en el grid y se
  'meten en una lista
  grdDBGrid1(4).MoveFirst
  For i = 0 To grdDBGrid1(4).Rows - 1
    stra = "SELECT count(*) FROM FR6400 WHERE FR73CODPRODUCTO=" & _
    grdDBGrid1(4).Columns(5).Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value > 0 Then
      strprin = "SELECT FR68CODPRINCACTIV FROM FR6400 WHERE FR73CODPRODUCTO=" & _
                grdDBGrid1(4).Columns(5).Value
      Set rstprin = objApp.rdoConnect.OpenResultset(strprin)
      If gstrLista = "" Then
        gstrLista = gstrLista & rstprin.rdoColumns(0).Value
      Else
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
      End If
      rstprin.MoveNext
      While Not rstprin.EOF
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
        rstprin.MoveNext
      Wend
      rstprin.Close
      Set rstprin = Nothing
    End If
    rsta.Close
    Set rsta = Nothing
    grdDBGrid1(4).MoveNext
  Next i
End If
  
  If gstrLista <> "" Then
    gstrLista = "(" & gstrLista & ")"
    Call objsecurity.LaunchProcess("FR0163")
  End If

cmdinter(Index).Enabled = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMasterInfo1 As New clsCWForm
  Dim objMasterInfo2 As New clsCWForm
  Dim objMultiInfo3 As New clsCWForm
  Dim objMultiInfo4 As New clsCWForm
  Dim strKey As String
  Dim rsta As rdoResultset
  Dim sqlstr As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
blnInLoad = True


'gActuacionPedida vntdata(1)

Awk1 = gActuacionPedida
If Awk1.NF >= 1 Then
  glActuacionPedida = Awk1.F(1)
End If

Call Crear_Hoja

If (glActuacionPedida = Empty And gstrLlamador <> "") Or glActuacionPedida <> Empty Then
  With objMasterInfo2
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(2)
    Set .grdGrid = grdDBGrid1(1)

    .strName = "Hoja Anestesia"
    .blnAskPrimary = False
    .strTable = "FR4300"
    
    .strWhere = "FR43CODHOJAANEST=" & auxHojAnest
    
    Call .FormAddOrderField("FR43CODHOJAANEST", cwAscending)
    .intAllowance = cwAllowModify
  
    'If gstrLlamador = "frmFirmarHojQuirof" Then
    '  .strWhere = "FR44CODHOJAQUIRO=" & gintcodhojaquiro & " AND SG02COD IS NULL " 'y NO FIRMADA
    '  .intAllowance = cwAllowModify
    'Else
    '  .strWhere = "SG02COD IS NULL "
    'End If
  
    strKey = .strDataBase & .strTable
    'Call .FormCreateFilterWhere(strKey, "Hoja Anestesia")
    'Call .FormAddFilterWhere(strKey, "FR43CODHOJAANEST", "C�digo Hoja Anestesia", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR43FECINTERVEN", "Fecha Intervenci�n", cwDate)
    'Call .FormAddFilterWhere(strKey, "FR43HORAINTERVEN", "Hora Intervenci�n", cwDecimal)
    'Call .FormAddFilterWhere(strKey, "FR43DESINTERVENCION", "Descripci�n Intervenci�n", cwString)
    'Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "Paciente", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "SG02COD_ANE", "Anestesista", cwString)
    'Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "Enfermera", cwString)
    'Call .FormAddFilterWhere(strKey, "SG02COD_CIR", "Cirujano", cwString)
    'Call .FormAddFilterWhere(strKey, "SG02COD", "Firma Digital", cwString)
    'Call .FormAddFilterOrder(strKey, "FR43CODHOJAANEST", "C�digo Hoja Anestesia")
  End With
  
  With objMultiInfo4
    Set .objFormContainer = fraFrame1(4)
    Set .objFatherContainer = fraFrame1(2)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(4)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

    .strName = "Material y Medicacion Anestesia"
    
    .strTable = "FR1700"
    .intAllowance = cwAllowModify + cwAllowDelete
    
    .intCursorSize = 0
    
    'Call .FormAddOrderField("FR17NUMLINEA", cwAscending)
    Call .FormAddOrderField("FR17NUMLINEA", cwAscending)

    Call .FormAddRelation("FR43CODHOJAANEST", txtText1(48))

    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "OTROS CONSUMOS")
    Call .FormAddFilterWhere(strKey, "FR17NUMLINEA", "N�mero de Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR43CODHOJAANEST", "Hoja Anestesia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�digo Unidad Medida", cwString)
    Call .FormAddFilterWhere(strKey, "FR17CANTIDAD", "Cantidad Consumida", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR17INDLEIDO", "Leida?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDBLOQUEO", "Bloqueada?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR17INDESTHA", "Estado Linea?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "FR17NUMLINEA", "N�mero de Linea")

  End With
  
  
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Hoja Quirofano"
    .blnAskPrimary = False
    .strTable = "FR4400"
    
    Call .FormAddOrderField("FR44CODHOJAQUIRO", cwAscending)
  
    .intAllowance = cwAllowModify
    
    'Call .FormAddRelation("FR43CODHOJAANEST", txtText1(48))
    .strWhere = "FR43CODHOJAANEST=" & auxHojAnest
    
    'If gstrLlamador = "frmFirmarHojQuirof" Then
    '  .strWhere = "FR44CODHOJAQUIRO=" & gintcodhojaquiro & " AND SG02COD IS NULL " 'y NO FIRMADA
    '  .intAllowance = cwAllowModify
    'Else
    '  .strWhere = "SG02COD IS NULL "
    'End If
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Hoja Quirofano")
    'Call .FormAddFilterWhere(strKey, "FR44CODHOJAQUIRO", "C�digo Hoja Quir�fano", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR97CODQUIROFANO", "C�digo Quir�fano", cwString)
    'Call .FormAddFilterWhere(strKey, "FR44FECINTERVEN", "Fecha Intervenci�n", cwDate)
    'Call .FormAddFilterWhere(strKey, "FR44HORAINTERV", "Hora Intervenci�n", cwDecimal)
    'Call .FormAddFilterWhere(strKey, "FR44NUMORDENDIA", "N�mero Orden en el D�a", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR44INDPROGRAMADA", "Indicador Programada", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR44INDIMPREVISTA", "Indicador Imprevista", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR44INDURGENTE", "Indicador Urgente", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "SG02COD", "Firma Digital", cwString)
    'Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "Paciente", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR44INDANATPATO", "Indicador Anatom�a Patol�gica", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR44INDRADIOTERAP", "Indicador Radio Terapia", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR44INDENDOSCOPIA", "Indicador Endoscopia", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR44INDMICROSCOPIA", "Indicador Microscop�a", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR44INDRX", "Indicador RX", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR44INDLASER", "Indicador Laser", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR44INDCEC", "Indicador C.E.C.", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR44INDCUANTAGASAS", "Indicador Cuenta Gasas", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR44INDOTROS", "Indicador Otros", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR44DESOTROS", "Descripci�n Indicador Otros", cwString)
    'Call .FormAddFilterOrder(strKey, "FR44CODHOJAQUIRO", "C�digo Hoja Quir�fano")
  End With
    
  
  With objMasterInfo1
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(2)
 
    .strName = "Intervenciones"
    .blnAskPrimary = False
    .strTable = "FRG300"
    
    .intAllowance = cwAllowModify
  
    'Call .FormAddRelation("FR44CODHOJAQUIRO", txtText1(0))
    
    .strWhere = "FR44CODHOJAQUIRO=" & auxHojQuirof
    
    Call .FormAddOrderField("FRG3CODINTERVENCION", cwAscending)
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Intervenciones")
    'Call .FormAddFilterWhere(strKey, "FRG3CODINTERVENCION", "C�digo Intervenci�n", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR44CODHOJAQUIRO", "C�digo Hoja Quir�fano", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FRG3DESINTERVENCION", "Descripci�n Intervenci�n", cwString)
    'Call .FormAddFilterWhere(strKey, "SG02COD_CIR", "Cirujano", cwString)
    'Call .FormAddFilterWhere(strKey, "SG02COD_PAY", "Primer Ayudante", cwString)
    'Call .FormAddFilterWhere(strKey, "SG02COD_SAY", "Segundo Ayudante", cwString)
    'Call .FormAddFilterWhere(strKey, "AD02CODDPTO_CIR", "Departamento Cirujano", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "SG02COD_INS", "Instrumentista", cwString)
    'Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "Enfermera", cwString)
    'Call .FormAddFilterWhere(strKey, "SG02COD_CCL", "Cirujano Colaborador", cwString)
    'Call .FormAddFilterWhere(strKey, "SG02COD_ACL", "Ayudante Colaborador", cwString)
  End With
  
  
  
  With objMultiInfo3
    Set .objFormContainer = fraFrame1(3)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(3)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

    .strName = "Material y Medicacion Quirofano"

    .strTable = "FR1800"

    .intCursorSize = 0

    .intAllowance = cwAllowModify + cwAllowDelete

    Call .FormAddOrderField("FR18NUMLINEA", cwAscending)

    Call .FormAddRelation("FR44CODHOJAQUIRO", txtText1(0))

    .strWhere = "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO " & _
                "FROM FR7300)"

    strKey = .strDataBase & .strTable
    'Call .FormCreateFilterWhere(strKey, "Material y Medicacion")
    'Call .FormAddFilterWhere(strKey, "FR18NUMLINEA", "N�mero de Linea", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR44CODHOJAQUIRO", "Hoja Quir�fano", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR18CANTCONSUMIDA", "Cantidad Consumida", cwDecimal)
    'Call .FormAddFilterWhere(strKey, "FR18INDLEIDO", "Leida?", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR18INDBLOQUEO", "Bloqueada?", cwBoolean)
    'Call .FormAddFilterWhere(strKey, "FR18INDESTHQ", "Estado Linea?", cwBoolean)
    'Call .FormAddFilterOrder(strKey, "FR18NUMLINEA", "N�mero de Linea")

  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo2, cwFormDetail)
    Call .FormAddInfo(objMasterInfo1, cwFormDetail)
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo3, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo4, cwFormMultiLine)

    Call .GridAddColumn(objMultiInfo3, "L�nea", "FR18NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo3, "Hoja Quir�fano", "FR44CODHOJAQUIRO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo3, "C�d.Prod.", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo3, "C�d.Int", "", cwString, 6)
    Call .GridAddColumn(objMultiInfo3, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo3, "Descripci�n Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo3, "F.F", "", cwString, 3)
    Call .GridAddColumn(objMultiInfo3, "Dosis", "", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo3, "U.M", "", cwString, 5)
    Call .GridAddColumn(objMultiInfo3, "Cant.Consumida", "FR18CANTCONSUMIDA", cwDecimal)
    Call .GridAddColumn(objMultiInfo3, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo3, "Leida?", "FR18INDLEIDO", cwBoolean)
    Call .GridAddColumn(objMultiInfo3, "Bloqueada?", "FR18INDBLOQUEO", cwBoolean)
    Call .GridAddColumn(objMultiInfo3, "Estado Linea?", "FR18INDESTHQ", cwBoolean)
    Call .GridAddColumn(objMultiInfo3, "Medicamento/Material", "", cwBoolean)
   
    Call .GridAddColumn(objMultiInfo4, "L�nea", "FR17NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo4, "Hoja Anestesia", "FR43CODHOJAANEST", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo4, "C�d.Prod.", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo4, "C�d.Int", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo4, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo4, "Descripci�n Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo4, "F.F", "", cwString, 3)
    Call .GridAddColumn(objMultiInfo4, "Dosis", "", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo4, "U.M", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo4, "Cant.Consumida", "FR17CANTIDAD", cwDecimal)
    Call .GridAddColumn(objMultiInfo4, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo4, "Leida?", "FR17INDLEIDO", cwBoolean)
    Call .GridAddColumn(objMultiInfo4, "Bloqueada?", "FR17INDBLOQUEO", cwBoolean)
    Call .GridAddColumn(objMultiInfo4, "Estado Linea?", "FR17INDESTHA", cwBoolean)
    Call .GridAddColumn(objMultiInfo4, "Medicamento/Material", "", cwBoolean)
   
    
    Call .FormCreateInfo(objMasterInfo2)
    Call .FormCreateInfo(objMasterInfo1)
    Call .FormCreateInfo(objMasterInfo)
    Call .FormChangeColor(objMultiInfo3)
    Call .FormChangeColor(objMultiInfo4)
     
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(10)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(11), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(12), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(13), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(14), "CI22SEGAPEL")
    .CtrlGetInfo(txtText1(10)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(9)), "SG02COD", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(9)), txtText1(43), "NOMSG02")
    .CtrlGetInfo(txtText1(9)).blnForeign = True
    .CtrlGetInfo(txtText1(9)).blnReadOnly = True
                                                                          
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR97CODQUIROFANO", "SELECT FR97DESQUIROFANO FROM FR9700 WHERE FR97CODQUIROFANO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(44), "FR97DESQUIROFANO")
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), grdDBGrid1(3).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), grdDBGrid1(3).Columns(8), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), grdDBGrid1(3).Columns(11), "FR93CODUNIMEDIDA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), grdDBGrid1(3).Columns(9), "FRH7CODFORMFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), grdDBGrid1(3).Columns(10), "FR73DOSIS")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), grdDBGrid1(3).Columns(7), "FR73REFERENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), grdDBGrid1(3).Columns(17), "FR73INDPRODSAN")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(3).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(3).Columns(8), "FR73DESPRODUCTO")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(3).Columns(11), "FR93CODUNIMEDIDA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(3).Columns(9), "FRH7CODFORMFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(3).Columns(10), "FR73DOSIS")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(3).Columns(7), "FR73REFERENCIA")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(4).Columns(5)), grdDBGrid1(3).Columns(17), "FR73INDPRODSAN")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(15)), "SG02COD_CIR", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(15)), txtText1(16), "NOMSG02")
    .CtrlGetInfo(txtText1(15)).blnForeign = True


    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(19)), "SG02COD_PAY", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(20), "NOMSG02")
    .CtrlGetInfo(txtText1(19)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(21)), "SG02COD_INS", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(21)), txtText1(22), "NOMSG02")
    .CtrlGetInfo(txtText1(21)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(23)), "SG02COD_SAY", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(23)), txtText1(24), "NOMSG02")
    .CtrlGetInfo(txtText1(23)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "SG02COD_ENF", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(26), "NOMSG02")
    .CtrlGetInfo(txtText1(25)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(30)), "SG02COD_CCL", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(30)), txtText1(31), "NOMSG02")
    .CtrlGetInfo(txtText1(30)).blnForeign = True


    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(34)), "SG02COD_ACL", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(34)), txtText1(35), "NOMSG02")
    .CtrlGetInfo(txtText1(34)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(38)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(38)), txtText1(39), "AD02DESDPTO")
    .CtrlGetInfo(txtText1(38)).blnForeign = True
    
    '.CtrlGetInfo(grdDBGrid1(3).Columns(5)).blnReadOnly = True
                                                                                                                  
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "PR04NUMACTPLAN", "SELECT PR0100.PR01CODACTUACION,PR0100.PR01DESCORTA,PR0100.PR01DESCOMPLETA FROM PR0100,PR0300,PR0400 WHERE PR0400.PR04NUMACTPLAN=? AND PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI AND PR0300.PR01CODACTUACION=PR0100.PR01CODACTUACION")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(5), "PR01DESCORTA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(17), "PR01CODACTUACION")
    .CtrlGetInfo(txtText1(4)).blnForeign = True
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(53)), "SG02COD_ANE", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(53)), txtText1(55), "NOMSG02")
    .CtrlGetInfo(txtText1(53)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(61)), "SG02COD_ENF", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(61)), txtText1(62), "NOMSG02")
    .CtrlGetInfo(txtText1(61)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(63)), "SG02COD_CIR", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(63)), txtText1(64), "NOMSG02")
    .CtrlGetInfo(txtText1(63)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(51)), "SG02COD", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 NOMSG02 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(51)), txtText1(52), "NOMSG02")
    .CtrlGetInfo(txtText1(51)).blnForeign = True
    .CtrlGetInfo(txtText1(51)).blnReadOnly = True
      
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "FR97CODQUIROFANO", "SELECT FR97DESQUIROFANO FROM FR9700 WHERE FR97CODQUIROFANO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(6), "FR97DESQUIROFANO")
    .CtrlGetInfo(txtText1(7)).blnForeign = True
      
    .CtrlGetInfo(cboDBCombo1(3)).strSQL = "SELECT ASSESSMENTITEMS.ASSESSMENTITEMDESC FROM ASSESSMENTITEMS,TREATMENTS WHERE ASSESSMENTITEMS.TREATMENTDBOID=TREATMENTS.TREATMENTDBOID AND TREATMENTS.BRANDNAME='Posici�n intraoperatoria' ORDER BY ASSESSMENTITEMS.ASSESSMENTITEMDESC"
      
      
    '.CtrlGetInfo(txtText1(0)).blnInFind = True
    '.CtrlGetInfo(txtText1(1)).blnInFind = True
    '.CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    '.CtrlGetInfo(txtText1(2)).blnInFind = True
    '.CtrlGetInfo(txtText1(3)).blnInFind = True
    '.CtrlGetInfo(chkCheck1(0)).blnInFind = True
    '.CtrlGetInfo(chkCheck1(1)).blnInFind = True
    '.CtrlGetInfo(chkCheck1(2)).blnInFind = True
    '.CtrlGetInfo(txtText1(40)).blnInFind = True
    '.CtrlGetInfo(txtText1(4)).blnInFind = True
    '.CtrlGetInfo(txtText1(5)).blnInFind = True
    '.CtrlGetInfo(txtText1(6)).blnInFind = True
    '.CtrlGetInfo(txtText1(7)).blnInFind = True
    '.CtrlGetInfo(txtText1(8)).blnInFind = True
    '.CtrlGetInfo(txtText1(9)).blnInFind = True
    '.CtrlGetInfo(txtText1(10)).blnInFind = True
    
    .CtrlGetInfo(txtProceso).blnNegotiated = False
    .CtrlGetInfo(txtAsistencia).blnNegotiated = False
    
    .CtrlGetInfo(chkCheck1(12)).blnReadOnly = True
    .CtrlGetInfo(txtText1(10)).blnReadOnly = True
    .CtrlGetInfo(txtText1(38)).blnReadOnly = True
    .CtrlGetInfo(txtText1(7)).blnReadOnly = True
    
    .CtrlGetInfo(txtProceso).blnReadOnly = True
    .CtrlGetInfo(txtAsistencia).blnReadOnly = True
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

grdDBGrid1(3).Columns(6).Width = 700 'c�d.interno
grdDBGrid1(3).Columns(8).Width = 3500  'descripci�n
grdDBGrid1(3).Columns(9).Width = 450   'F.F
grdDBGrid1(3).Columns(10).Width = 900   'dosis
grdDBGrid1(3).Columns(7).Width = 1300 'referencia
grdDBGrid1(3).Columns(12).Width = 1400 'cantidad
grdDBGrid1(3).Columns(11).Width = 750  'U.M
grdDBGrid1(3).Columns(0).Visible = False
grdDBGrid1(3).Columns(0).Width = 0
grdDBGrid1(3).Columns(3).Visible = False
grdDBGrid1(3).Columns(4).Visible = False
grdDBGrid1(3).Columns(5).Visible = False
grdDBGrid1(3).Columns(13).Visible = False
grdDBGrid1(3).Columns(14).Visible = False
grdDBGrid1(3).Columns(15).Visible = False
grdDBGrid1(3).Columns(16).Visible = False
grdDBGrid1(3).Columns(17).Visible = False
grdDBGrid1(3).Columns(12).BackColor = &HFFFF00

grdDBGrid1(4).Columns(6).Width = 700 'c�d.interno
grdDBGrid1(4).Columns(8).Width = 3500  'descripci�n
grdDBGrid1(4).Columns(9).Width = 450   'F.F
grdDBGrid1(4).Columns(10).Width = 900   'dosis
grdDBGrid1(4).Columns(7).Width = 1300 'referencia
grdDBGrid1(4).Columns(12).Width = 1400 'cantidad
grdDBGrid1(4).Columns(11).Width = 750  'U.M
grdDBGrid1(4).Columns(0).Visible = False
grdDBGrid1(4).Columns(0).Width = 0
grdDBGrid1(4).Columns(3).Visible = False
grdDBGrid1(4).Columns(4).Visible = False
grdDBGrid1(4).Columns(5).Visible = False
grdDBGrid1(4).Columns(13).Visible = False
grdDBGrid1(4).Columns(14).Visible = False
grdDBGrid1(4).Columns(15).Visible = False
grdDBGrid1(4).Columns(16).Visible = False
grdDBGrid1(4).Columns(17).Visible = False
grdDBGrid1(4).Columns(12).BackColor = &HFFFF00


'grdDBGrid1(3).RemoveAll
'grdDBGrid1(3).Refresh

    'If gstrLlamador = "frmFirmarHojQuirof" Then
    '  Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
    'End If
Else

End If

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  If glActuacionPedida = Empty And gstrLlamador = "" Then
  Else
    intCancel = objWinInfo.WinExit
  End If
End Sub

Private Sub Form_Unload(intCancel As Integer)
  
  If glActuacionPedida = Empty And gstrLlamador = "" Then
  Else
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
  End If
  
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  Dim strFRH2 As String
  Dim qryFRH2 As rdoQuery
  Dim rdoFRH2 As rdoResultset
  Dim strQuiDispen As String
  
  If strCtrl = "txtText1(10)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "CI2200"

      Set objField = .AddField("CI21CODPERSONA")
      objField.strSmallDesc = "C�digo Paciente"

      Set objField = .AddField("CI22NUMHISTORIA")
      objField.strSmallDesc = "Historia"

      Set objField = .AddField("CI22NOMBRE")
      objField.strSmallDesc = "Nombre"

      Set objField = .AddField("CI22PRIAPEL")
      objField.strSmallDesc = "Apellido 1�"

      Set objField = .AddField("CI22SEGAPEL")
      objField.strSmallDesc = "Apellido 2�"

      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(10), .cllValues("CI21CODPERSONA"))
      End If
    End With
    Set objSearch = Nothing
  End If

  If strCtrl = "txtText1(15)" Or strCtrl = "txtText1(19)" Or _
     strCtrl = "txtText1(21)" Or strCtrl = "txtText1(23)" Or _
     strCtrl = "txtText1(25)" Or strCtrl = "txtText1(30)" Or _
     strCtrl = "txtText1(53)" Or strCtrl = "txtText1(61)" Or _
     strCtrl = "txtText1(63)" Or _
     strCtrl = "txtText1(34)" Or strCtrl = "txtText1(9)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "SG0200"
      .strOrder = "ORDER BY SG02APE1"

      Set objField = .AddField("SG02COD")
      objField.strSmallDesc = "C�digo Usuario"

      Set objField = .AddField("SG02NOM")
      objField.strSmallDesc = "Nombre"

      Set objField = .AddField("SG02APE1")
      objField.strSmallDesc = "Primer Apellido"

      Set objField = .AddField("SG02APE2")
      objField.strSmallDesc = "Segundo Apellido"
      
      Set objField = .AddField("SG02NUMCOLEGIADO")
      objField.strSmallDesc = "Num.Colegiado"
      
      'Set objField = .AddField("SG02NUMCOLEGIADO")
      'objField.strSmallDesc = "Firma"
      
      If .Search Then
        Select Case strCtrl
          Case "txtText1(15)"
            Call objWinInfo.CtrlSet(txtText1(15), .cllValues("SG02COD"))
          Case "txtText1(19)"
            Call objWinInfo.CtrlSet(txtText1(19), .cllValues("SG02COD"))
          Case "txtText1(21)"
            Call objWinInfo.CtrlSet(txtText1(21), .cllValues("SG02COD"))
          Case "txtText1(23)"
            Call objWinInfo.CtrlSet(txtText1(23), .cllValues("SG02COD"))
          Case "txtText1(25)"
            Call objWinInfo.CtrlSet(txtText1(25), .cllValues("SG02COD"))
          Case "txtText1(30)"
            Call objWinInfo.CtrlSet(txtText1(30), .cllValues("SG02COD"))
          Case "txtText1(34)"
            Call objWinInfo.CtrlSet(txtText1(34), .cllValues("SG02COD"))
          Case "txtText1(9)"
            Call objWinInfo.CtrlSet(txtText1(9), .cllValues("SG02COD"))
          Case "txtText1(53)"
            Call objWinInfo.CtrlSet(txtText1(53), .cllValues("SG02COD"))
          Case "txtText1(61)"
            Call objWinInfo.CtrlSet(txtText1(61), .cllValues("SG02COD"))
          Case "txtText1(63)"
            Call objWinInfo.CtrlSet(txtText1(63), .cllValues("SG02COD"))
        End Select
      End If
    End With
    Set objSearch = Nothing
  End If

  If strCtrl = "txtText1(17)" Or strCtrl = "txtText1(32)" Or strCtrl = "txtText1(40)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "AD0200"
      .strOrder = "ORDER BY AD02DESDPTO"

      Set objField = .AddField("AD02CODDPTO")
      objField.strSmallDesc = "C�digo Departamento"

      Set objField = .AddField("AD02DESDPTO")
      objField.strSmallDesc = "Descripci�n Departamento"

      If .Search Then
        Select Case strCtrl
          Case "txtText1(17)"
            Call objWinInfo.CtrlSet(txtText1(17), .cllValues("AD02CODDPTO"))
          Case "txtText1(32)"
            Call objWinInfo.CtrlSet(txtText1(32), .cllValues("AD02CODDPTO"))
          Case "txtText1(40)"
            Call objWinInfo.CtrlSet(txtText1(40), .cllValues("AD02CODDPTO"))
        End Select
      End If
    End With
    Set objSearch = Nothing
  End If

  If strCtrl = "txtText1(38)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "AD0200"
      .strOrder = "ORDER BY AD02DESDPTO"
     .strWhere = " WHERE AD32CODTIPODPTO = 3" & _
                 " AND AD02FECINICIO < (SELECT SYSDATE FROM DUAL)" & _
                 " AND ((AD02FECFIN IS NULL) OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL)))"

      Set objField = .AddField("AD02CODDPTO")
      objField.strSmallDesc = "C�digo Servicio"

      Set objField = .AddField("AD02DESDPTO")
      objField.strSmallDesc = "Descripci�n Servicio"

      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(38), .cllValues("AD02CODDPTO"))
      End If
    End With
    Set objSearch = Nothing
  End If


  If strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "FR9700"
      .strOrder = "ORDER BY FR97DESQUIROFANO"
      
      'Se obtiene el quirofano para dispensacion ficticio
      strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
      Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
      qryFRH2(0) = 30
      Set rdoFRH2 = qryFRH2.OpenResultset()
      If IsNull(rdoFRH2.rdoColumns(0).Value) Then
        strQuiDispen = "999"
      Else
        strQuiDispen = rdoFRH2.rdoColumns(0).Value
      End If
      rdoFRH2.Close
      Set rdoFRH2 = Nothing
      qryFRH2.Close
      Set qryFRH2 = Nothing
      
      .strWhere = " where FR97CODQUIROFANO<>" & strQuiDispen

      Set objField = .AddField("FR97CODQUIROFANO")
      objField.strSmallDesc = "C�digo Quir�fano"

      Set objField = .AddField("FR97DESQUIROFANO")
      objField.strSmallDesc = "Descripci�n Quir�fano"

      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR97CODQUIROFANO"))
      End If
    End With
    Set objSearch = Nothing
  End If

  If strCtrl = "txtText1(7)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "FR9700"
      .strOrder = "ORDER BY FR97DESQUIROFANO"

      Set objField = .AddField("FR97CODQUIROFANO")
      objField.strSmallDesc = "C�digo Quir�fano"

      Set objField = .AddField("FR97DESQUIROFANO")
      objField.strSmallDesc = "Descripci�n Quir�fano"

      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(7), .cllValues("FR97CODQUIROFANO"))
      End If
    End With
    Set objSearch = Nothing
  End If

  If strCtrl = "txtText1(4)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "PR0400"
      .strOrder = "ORDER BY PR04NUMACTPLAN"
      
      .strWhere = "WHERE CI21CODPERSONA=" & txtText1(10)
      .strWhere = .strWhere & " AND AD07CODPROCESO=" & txtProceso.Text
      .strWhere = .strWhere & " AND AD01CODASISTENCI=" & txtAsistencia.Text

      Set objField = .AddField("PR04NUMACTPLAN")
      objField.strSmallDesc = "C�digo Actuaci�n"

      'Set objField = .AddField("AD07CODPROCESO")
      'objField.strSmallDesc = "Proceso"

      'Set objField = .AddField("AD01CODASISTENCI")
      'objField.strSmallDesc = "Asistencia"
      
      Set objField = .AddField("AD02CODDPTO")
      objField.strSmallDesc = "Cod.Dpto.Realizador"

      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(4), .cllValues("PR04NUMACTPLAN"))
        Call objWinInfo.CtrlSet(txtText1(38), .cllValues("AD02CODDPTO"))
      End If
    End With
    Set objSearch = Nothing
  End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)

'  If txtText1(9).Text <> "" Then
'        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
'        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
'        Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
'        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
'        Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
'        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
'        cmdbuscargruprod(0).Enabled = False
'        cmdbuscarprod(0).Enabled = False
'        cmdinter(0).Enabled = False
'
'        Select Case strFormName
'        Case "Hoja Quirofano"
'          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
'        Case "Intervenciones"
'          Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
'        Case "Material y Medicacion"
'          Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
'        End Select
'  Else
'    Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
'    If gstrLlamador = "frmFirmarHojQuirof" Then
'      objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
'    Else
'      objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
'    End If
'    Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
'    objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
'    Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
'    objWinInfo.objWinActiveForm.intAllowance = cwAllowModify + cwAllowDelete
'    cmdbuscargruprod(0).Enabled = True
'    cmdbuscarprod(0).Enabled = True
'    cmdinter(0).Enabled = True
'
' '   Select Case strFormName
'    Case "Hoja Quirofano"
'      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
'    Case "Intervenciones"
'      Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
'    Case "Material y Medicacion"
'      Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
'    End Select
'  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabGeneral1_Click(Index As Integer, PreviousTab As Integer)
    
  Select Case tabGeneral1(1).Tab
    Case 0
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
        txtProceso.Text = auxProcPrinc
        txtAsistencia.Text = auxAsisPrinc
      End If
      Call objWinInfo.CtrlGotFocus
    Case 1
      Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
      If txtText1(0).Text <> "" Then
        objWinInfo.objWinActiveForm.strWhere = "FR44CODHOJAQUIRO=" & txtText1(0).Text
      Else
        objWinInfo.objWinActiveForm.strWhere = "FR44CODHOJAQUIRO IS NULL"
      End If
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
    Case 2
      Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
        objWinInfo.objWinActiveForm.intAllowance = cwAllowModify + cwAllowDelete
      End If
      Call objWinInfo.CtrlGotFocus
    Case 3
      Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
      If txtText1(48).Text <> "" Then
        Call objWinInfo.DataRefresh
        objWinInfo.objWinActiveForm.intAllowance = cwAllowModify + cwAllowDelete
      End If
      Call objWinInfo.CtrlGotFocus
    Case 4
      Call objWinInfo.FormChangeActive(fraFrame1(4), True, True)
      If txtText1(48).Text <> "" Then
        Call objWinInfo.DataRefresh
        objWinInfo.objWinActiveForm.intAllowance = cwAllowModify + cwAllowDelete
      End If
      Call objWinInfo.CtrlGotFocus
  End Select

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim stra As String
Dim rstmin As rdoResultset
Dim strmin As String
Dim rstmed As rdoResultset
Dim strmed As String

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Hoja Quirofano" Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR44CODHOJAQUIRO_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    rsta.Close
    Set rsta = Nothing
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Intervenciones" Then
      If IsNumeric(txtText1(0).Text) Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        sqlstr = "SELECT FRG3CODINTERVENCION_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(54).Text = rsta.rdoColumns(0).Value
        txtText1(47).Text = txtText1(0).Text
        rsta.Close
        Set rsta = Nothing
      End If
      'cuando se crea una nueva intervenci�n hay que rellenar datos por defecto.
      'El equipo quir�rgico,el equipo colaborador y el servicio se introducen s�lo una vez
      'por eso se meten autom�ticamente en el resto de intervenciones aunque luego
      'se pueda modificar.
      If IsNumeric(txtText1(47).Text) Then
        strmin = "SELECT MIN(FRG3CODINTERVENCION) FROM FRG300 WHERE " & _
               "FR44CODHOJAQUIRO=" & txtText1(47).Text
        Set rstmin = objApp.rdoConnect.OpenResultset(strmin)
        If Not rstmin.EOF Then
         If IsNumeric(rstmin.rdoColumns(0).Value) Then
          stra = "SELECT * FROM FRG300 WHERE FR44CODHOJAQUIRO=" & txtText1(47).Text & _
                 " AND FRG3CODINTERVENCION=" & rstmin.rdoColumns(0).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta.rdoColumns("SG02COD_PAY").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_PAY").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(19).Text = rsta.rdoColumns("SG02COD_PAY").Value
              txtText1(20).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_INS").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_INS").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(21).Text = rsta.rdoColumns("SG02COD_INS").Value
              txtText1(22).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_SAY").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_SAY").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(23).Text = rsta.rdoColumns("SG02COD_SAY").Value
              txtText1(24).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_ENF").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_ENF").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(25).Text = rsta.rdoColumns("SG02COD_ENF").Value
              txtText1(26).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_CCL").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_CCL").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(30).Text = rsta.rdoColumns("SG02COD_CCL").Value
              txtText1(31).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_ACL").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_ACL").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(34).Text = rsta.rdoColumns("SG02COD_ACL").Value
              txtText1(35).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_CIR").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_CIR").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(15).Text = rsta.rdoColumns("SG02COD_CIR").Value
              txtText1(16).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
          End If
        rsta.Close
        Set rsta = Nothing
        End If
        End If
        rstmin.Close
        Set rstmin = Nothing
    End If
    ElseIf btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Material y Medicacion Quirofano" Then
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
                  txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea(0).Value) Then
           linea = grdDBGrid1(3).Rows
        Else
           linea = rstlinea(0).Value + grdDBGrid1(3).Rows
        End If
        grdDBGrid1(3).Columns(3).Value = linea
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna hoja de quir�fano", vbInformation
        Exit Sub
      End If
    ElseIf btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Material y Medicacion Anestesia" Then
      If txtText1(48).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
                  txtText1(48).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea(0).Value) Then
           linea = grdDBGrid1(4).Rows
        Else
           linea = rstlinea(0).Value + grdDBGrid1(4).Rows
        End If
        grdDBGrid1(4).Columns(3).Value = linea
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna hoja de anestesia", vbInformation
        Exit Sub
      End If
    Else
      'cuando sea Guardar y Alta Masiva que meta el c�digo autom�ticamente
      If btnButton.Index = 4 And mnuOpcionesOpcion(50).Checked = True _
         And objWinInfo.objWinActiveForm.strName = "Hoja Quirofano" Then
                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
                sqlstr = "SELECT FR44CODHOJAQUIRO_SEQUENCE.nextval FROM dual"
                Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
                Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
                Call objWinInfo.CtrlGotFocus
                Call objWinInfo.CtrlLostFocus
                rsta.Close
                Set rsta = Nothing
      Else
        If btnButton.Index = 8 And objWinInfo.objWinActiveForm.strName = "Intervenciones" And chkCheck1(12) = 1 Then
          MsgBox "No se puede borrar la Intervenci�n Principal", vbCritical, "Aviso"
          Exit Sub
        Else
          Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        End If
      End If
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer
Dim stra As String
Dim rstmin As rdoResultset
Dim strmin As String
Dim rstmed As rdoResultset
Dim strmed As String


  If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "Hoja Quirofano" Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    sqlstr = "SELECT FR44CODHOJAQUIRO_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    rsta.Close
    Set rsta = Nothing
  Else
    If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "Intervenciones" Then
      If IsNumeric(txtText1(0).Text) Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        sqlstr = "SELECT FRG3CODINTERVENCION_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(54).Text = rsta.rdoColumns(0).Value
        txtText1(47).Text = txtText1(0).Text
        rsta.Close
        Set rsta = Nothing
      End If
      'cuando se crea una nueva intervenci�n hay que rellenar datos por defecto.
      'El equipo quir�rgico,el equipo colaborador y el servicio se introducen s�lo una vez
      'por eso se meten autom�ticamente en el resto de intervenciones aunque luego
      'se pueda modificar.
      If IsNumeric(txtText1(47).Text) Then
        strmin = "SELECT MIN(FRG3CODINTERVENCION) FROM FRG300 WHERE " & _
               "FR44CODHOJAQUIRO=" & txtText1(47).Text
        Set rstmin = objApp.rdoConnect.OpenResultset(strmin)
        If Not rstmin.EOF Then
         If IsNumeric(rstmin.rdoColumns(0).Value) Then
          stra = "SELECT * FROM FRG300 WHERE FR44CODHOJAQUIRO=" & txtText1(47).Text & _
                 " AND FRG3CODINTERVENCION=" & rstmin.rdoColumns(0).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta.rdoColumns("SG02COD_PAY").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_PAY").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(19).Text = rsta.rdoColumns("SG02COD_PAY").Value
              txtText1(20).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_INS").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_INS").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(21).Text = rsta.rdoColumns("SG02COD_INS").Value
              txtText1(22).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_SAY").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_SAY").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(23).Text = rsta.rdoColumns("SG02COD_SAY").Value
              txtText1(24).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_ENF").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_ENF").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(25).Text = rsta.rdoColumns("SG02COD_ENF").Value
              txtText1(26).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("FR43CODHOJAANEST").Value) Then
              txtText1(27).Text = rsta.rdoColumns("FR43CODHOJAANEST").Value
              strmed = "SELECT SG02COD_ANE FROM FR4300 WHERE FR43CODHOJAANEST=" & _
                       rsta.rdoColumns("FR43CODHOJAANEST").Value
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(28).Text = rstmed.rdoColumns("SG02COD_ANE").Value
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & txtText1(28).Text & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(29).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_CCL").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_CCL").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(30).Text = rsta.rdoColumns("SG02COD_CCL").Value
              txtText1(31).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("AD02CODDPTO_CCL").Value) Then
              strmed = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & _
                       "'" & rsta.rdoColumns("AD02CODDPTO_CCL").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(32).Text = rsta.rdoColumns("AD02CODDPTO_CCL").Value
              txtText1(33).Text = rstmed.rdoColumns("AD02DESDPTO").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_ACL").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_ACL").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(34).Text = rsta.rdoColumns("SG02COD_ACL").Value
              txtText1(35).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("FRG3HORAINICOL").Value) Then
              txtText1(36).Text = rsta.rdoColumns("FRG3HORAINICOL").Value
            End If
            If Not IsNull(rsta.rdoColumns("FRG3HORAFINCOL").Value) Then
              txtText1(37).Text = rsta.rdoColumns("FRG3HORAFINCOL").Value
            End If
            If Not IsNull(rsta.rdoColumns("SG02COD_CIR").Value) Then
              strmed = "SELECT SG02APE1 FROM SG0200 WHERE SG02COD=" & _
                       "'" & rsta.rdoColumns("SG02COD_CIR").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(15).Text = rsta.rdoColumns("SG02COD_CIR").Value
              txtText1(16).Text = rstmed.rdoColumns("SG02APE1").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("AD02CODDPTO_CIR").Value) Then
              strmed = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & _
                       "'" & rsta.rdoColumns("AD02CODDPTO_CIR").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(17).Text = rsta.rdoColumns("AD02CODDPTO_CIR").Value
              txtText1(18).Text = rstmed.rdoColumns("AD02DESDPTO").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
            If Not IsNull(rsta.rdoColumns("AD02CODDPTO").Value) Then
              strmed = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & _
                       "'" & rsta.rdoColumns("AD02CODDPTO_CIR").Value & "'"
              Set rstmed = objApp.rdoConnect.OpenResultset(strmed)
              txtText1(38).Text = rsta.rdoColumns("AD02CODDPTO").Value
              txtText1(39).Text = rstmed.rdoColumns("AD02DESDPTO").Value
              rstmed.Close
              Set rstmed = Nothing
            End If
          End If
        rsta.Close
        Set rsta = Nothing
        End If
        End If
        rstmin.Close
        Set rstmin = Nothing
    End If
    ElseIf intIndex = 10 And objWinInfo.objWinActiveForm.strName = "Material y Medicacion" Then
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
                  txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea(0).Value) Then
           linea = grdDBGrid1(3).Rows
        Else
           linea = rstlinea(0).Value + grdDBGrid1(3).Rows
        End If
        grdDBGrid1(3).Columns(3).Value = linea
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna hoja de quir�fano", vbInformation
        Exit Sub
      End If
    Else
        'cuando sea Guardar y Alta Masiva que meta el c�digo autom�ticamente
      If intIndex = 40 And mnuOpcionesOpcion(50).Checked = True _
         And objWinInfo.objWinActiveForm.strName = "Hoja Quirofano" Then
                Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
                sqlstr = "SELECT FR44CODHOJAQUIRO_SEQUENCE.nextval FROM dual"
                Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
                Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
                Call objWinInfo.CtrlGotFocus
                Call objWinInfo.CtrlLostFocus
                rsta.Close
                Set rsta = Nothing
      Else
        If intIndex = 60 And objWinInfo.objWinActiveForm.strName = "Intervenciones" And chkCheck1(12) = 1 Then
          MsgBox "No se puede borrar la Intervenci�n Principal", vbCritical, "Aviso"
          Exit Sub
        Else
                Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        End If
      End If
    End If
  End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim strupdate As String
Dim strFRH2 As String
Dim qryFRH2 As rdoQuery
Dim rdoFRH2 As rdoResultset
Dim strQuiDispen As String
Dim strQui As String
Dim rstQui As rdoResultset

Call objWinInfo.CtrlDataChange

If intIndex = 1 Then 'QUIROFANO
  If (txtText1(1).Text <> "" And txtText1(7).Text = "") Or (txtText1(1).Text <> txtText1(7).Text) Then
  
    'Se obtiene el almac�n principal de Farmacia
    strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
    Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
    qryFRH2(0) = 30
    Set rdoFRH2 = qryFRH2.OpenResultset()
    If IsNull(rdoFRH2.rdoColumns(0).Value) Then
      strQuiDispen = "999"
    Else
      strQuiDispen = rdoFRH2.rdoColumns(0).Value
    End If
    rdoFRH2.Close
    Set rdoFRH2 = Nothing
    qryFRH2.Close
    Set qryFRH2 = Nothing

    strQui = "SELECT FR97CODQUIROFANO FROM FR9700" & _
         " WHERE FR97FECINIVIG<(SELECT SYSDATE FROM DUAL) AND " & _
         "((FR97FECFINVIG IS NULL) OR (FR97FECFINVIG>(SELECT SYSDATE FROM DUAL)))" & _
         " AND FR97CODQUIROFANO<>" & strQuiDispen & _
         " AND FR97CODQUIROFANO='" & txtText1(1).Text & "'"
    Set rstQui = objApp.rdoConnect.OpenResultset(strQui)
    If Not rstQui.EOF Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
      strupdate = "UPDATE FR4300 SET FR97CODQUIROFANO='" & txtText1(1).Text & "' WHERE FR43CODHOJAANEST=" & auxHojAnest
      objApp.rdoConnect.Execute strupdate, 64
      Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
      If txtText1(48).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    End If
  End If
End If

If intIndex = 2 Then
    If IsNumeric(txtText1(2).Text) Then
        If txtText1(2).Text > 24 Or txtText1(2).Text < 0 Then
            Call MsgBox("Hora de Intervenci�n fuera de rango", vbInformation, "Aviso")
            txtText1(2).Text = ""
        End If
    End If
End If

If intIndex = 5 Then
    If IsNumeric(txtText1(5).Text) Then
        If txtText1(5).Text > 24 Or txtText1(5).Text < 0 Then
            Call MsgBox("Hora de Final de Anestesia fuera de rango", vbInformation, "Aviso")
            txtText1(5).Text = ""
        End If
    End If
End If

If intIndex = 7 Then
    If IsNumeric(txtText1(7).Text) Then
        If txtText1(7).Text > 24 Or txtText1(7).Text < 0 Then
            Call MsgBox("Hora de Final de Intervenci�n fuera de rango", vbInformation, "Aviso")
            txtText1(7).Text = ""
        End If
    End If
End If
   
End Sub

Private Sub cmdbuscarprod_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strTerminada As String
Dim rstTerminada As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

blnInLoad = False

    strTerminada = "SELECT COUNT(*) FROM FR4400 WHERE FR44INDESTHQ>=2 AND FR44CODHOJAQUIRO=" & txtText1(0).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

    strTerminada = "SELECT COUNT(*) FROM FR4300 WHERE FR43INDESTHA>=2 AND FR43CODHOJAANEST=" & txtText1(48).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

cmdbuscarprod(Index).Enabled = False

If Index = 0 Then
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    gstrMedicamentoMaterial = "Medicamento"
    Call objsecurity.LaunchProcess("FR0166")
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
                    txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "FR44CODHOJAQUIRO,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(0).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtText1(48).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    gstrMedicamentoMaterial = "Medicamento"
    Call objsecurity.LaunchProcess("FR0166")
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
                    txtText1(48).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(48).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdbuscarprod(Index).Enabled = True

End Sub

Private Sub cmdbuscarmaterial_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strTerminada As String
Dim rstTerminada As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

blnInLoad = False

    strTerminada = "SELECT COUNT(*) FROM FR4400 WHERE FR44INDESTHQ>=2 AND FR44CODHOJAQUIRO=" & txtText1(0).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

    strTerminada = "SELECT COUNT(*) FROM FR4300 WHERE FR43INDESTHA>=2 AND FR43CODHOJAANEST=" & txtText1(48).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

cmdbuscarmaterial(Index).Enabled = False
If Index = 0 Then
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    gstrMedicamentoMaterial = "Material"
    Call objsecurity.LaunchProcess("FR0166")
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
                    txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "FR44CODHOJAQUIRO,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(0).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El material sanitario: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtText1(48).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    gstrMedicamentoMaterial = "Material"
    Call objsecurity.LaunchProcess("FR0166")
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
                    txtText1(48).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(48).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El material sanitario: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdbuscarmaterial(Index).Enabled = True
End Sub

Private Sub cmdbuscargruprod_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strTerminada As String
Dim rstTerminada As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

blnInLoad = False

    strTerminada = "SELECT COUNT(*) FROM FR4400 WHERE FR44INDESTHQ>=2 AND FR44CODHOJAQUIRO=" & txtText1(0).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

    strTerminada = "SELECT COUNT(*) FROM FR4300 WHERE FR43INDESTHA>=2 AND FR43CODHOJAANEST=" & txtText1(48).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

cmdbuscargruprod(Index).Enabled = False

If Index = 0 Then
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    Call objsecurity.LaunchProcess("FR0168")
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
                    txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "FR44CODHOJAQUIRO,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(0).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                             Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtText1(48).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    Call objsecurity.LaunchProcess("FR0168")
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
                    txtText1(48).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(48).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                             Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdbuscargruprod(Index).Enabled = True

End Sub

Private Sub cmdbuscarprot_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strTerminada As String
Dim rstTerminada As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

blnInLoad = False

    strTerminada = "SELECT COUNT(*) FROM FR4400 WHERE FR44INDESTHQ>=2 AND FR44CODHOJAQUIRO=" & txtText1(0).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

    strTerminada = "SELECT COUNT(*) FROM FR4300 WHERE FR43INDESTHA>=2 AND FR43CODHOJAANEST=" & txtText1(48).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

cmdbuscarprot(Index).Enabled = False

If Index = 0 Then
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    gstrLlamador = "HojaQuirofano"
    Call objsecurity.LaunchProcess("FR0174")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
                    txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "FR44CODHOJAQUIRO,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(0).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & gintprodbuscado(v, 3)
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtText1(48).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    gstrLlamador = "HojaAnestesia"
    Call objsecurity.LaunchProcess("FR0174")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
                    txtText1(48).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(48).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & gintprodbuscado(v, 3)
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdbuscarprot(Index).Enabled = True
End Sub

Private Sub cmdgruterapmedicamento_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strTerminada As String
Dim rstTerminada As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

blnInLoad = False

    strTerminada = "SELECT COUNT(*) FROM FR4400 WHERE FR44INDESTHQ>=2 AND FR44CODHOJAQUIRO=" & txtText1(0).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

    strTerminada = "SELECT COUNT(*) FROM FR4300 WHERE FR43INDESTHA>=2 AND FR43CODHOJAANEST=" & txtText1(48).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

cmdgruterapmedicamento(Index).Enabled = False

If Index = 0 Then
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    gstrLlamador = "Medicamentos"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
                    txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "FR44CODHOJAQUIRO,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(0).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtText1(48).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    gstrLlamador = "Medicamentos"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
                    txtText1(48).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(48).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdgruterapmedicamento(Index).Enabled = True
End Sub

Private Sub cmdgruterapmaterial_Click(Index As Integer)
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strTerminada As String
Dim rstTerminada As rdoResultset
Dim strInsert As String
Dim IntLinea As Integer
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim strMedida As String

blnInLoad = False

    strTerminada = "SELECT COUNT(*) FROM FR4400 WHERE FR44INDESTHQ>=2 AND FR44CODHOJAQUIRO=" & txtText1(0).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

    strTerminada = "SELECT COUNT(*) FROM FR4300 WHERE FR43INDESTHA>=2 AND FR43CODHOJAANEST=" & txtText1(48).Text
    Set rstTerminada = objApp.rdoConnect.OpenResultset(strTerminada)
    If rstTerminada(0).Value > 0 Then
      MsgBox "La Hoja ya est� FACTURADA", vbCritical, "Aviso"
      Exit Sub
    End If

cmdgruterapmaterial(Index).Enabled = False

If Index = 0 Then
  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    gstrLlamador = "Material Sanitario"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
                    txtText1(0).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strInsert = "INSERT INTO FR1800 ("
          strInsert = strInsert & "FR44CODHOJAQUIRO,FR18NUMLINEA,FR73CODPRODUCTO,FR18CANTCONSUMIDA"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(0).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
Else
  If txtText1(48).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    gstrLlamador = "Material Sanitario"
    Call objsecurity.LaunchProcess("FR0173")
    gstrLlamador = ""
    Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        If noinsertar = True Then
          strlinea = "SELECT MAX(FR17NUMLINEA) FROM FR1700 WHERE FR43CODHOJAANEST=" & _
                    txtText1(48).Text
          Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
          If rstlinea.EOF Then
            IntLinea = 1
          Else
            If IsNull(rstlinea(0).Value) Then
              IntLinea = 1
            Else
              IntLinea = rstlinea(0).Value + 1
            End If
          End If
          rstlinea.Close
          Set rstlinea = Nothing
          strMedida = "NE"
          stra = "SELECT FR93CODUNIMEDIDA" & _
                 " FROM FR7300 WHERE FR73CODPRODUCTO = " & gintprodbuscado(v, 0)
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta(0).Value) Then
              strMedida = rsta(0).Value
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          strInsert = "INSERT INTO FR1700 ("
          strInsert = strInsert & "FR43CODHOJAANEST,FR17NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR17CANTIDAD"
          strInsert = strInsert & ") VALUES ("
          strInsert = strInsert & txtText1(48).Text & ","
          strInsert = strInsert & IntLinea & ","
          strInsert = strInsert & gintprodbuscado(v, 0) & ","
          strInsert = strInsert & "'" & strMedida & "',"
          strInsert = strInsert & "1"
          strInsert = strInsert & ")"
          objApp.rdoConnect.Execute strInsert, 64
        Else
          mensaje = MsgBox("El medicamento: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                    Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
      Screen.MousePointer = vbHourglass
      Me.Enabled = False
      Call objWinInfo.FormChangeActive(fraFrame1(4), False, True)
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    End If
    gintprodtotal = 0
  End If
End If

cmdgruterapmaterial(Index).Enabled = True
End Sub


Private Sub Crear_Hoja()
'glActuacionPedida vntdata(1)
Dim strInsert As String
Dim sqla As String
Dim rsta As rdoResultset
Dim blnFecha As Boolean
Dim strHora As String
Dim stra As String
Dim auxQui
Dim strCuestionario As String
Dim rstCuestionario As rdoResultset
Dim strQuiro As String
Dim rstQuiro As rdoResultset
Dim AuxOrden
Dim AuxDuracion
Dim AuxTipAnes
Dim AuxCarInterv
Dim strFRG300 As String
Dim rstFRG300 As rdoResultset
Dim i As Integer

If gstrLlamador = "frmFirmarHojQuirof" Then
  stra = "SELECT FR43CODHOJAANEST " & _
         "FROM FR4400 WHERE FR44CODHOJAQUIRO= " & gintcodhojaquiro
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    auxHojAnest = rsta(0).Value
  End If
  rsta.Close
  Set rsta = Nothing
  auxHojQuirof = gintcodhojaquiro
  
  sqla = "SELECT AD07CODPROCESO,AD01CODASISTENCI "
  sqla = sqla & " FROM PR0400,FRG300 WHERE PR0400.PR04NUMACTPLAN=FRG300.PR04NUMACTPLAN "
  sqla = sqla & " AND FRG300.FR44CODHOJAQUIRO=" & auxHojQuirof
  Set rsta = objApp.rdoConnect.OpenResultset(sqla)
  If Not rsta.EOF Then
    If Not IsNull(rsta(0).Value) Then
      auxProcPrinc = rsta(0).Value
      txtProceso.Text = auxProcPrinc
    Else
      auxProcPrinc = ""
    End If
    If Not IsNull(rsta(0).Value) Then
      auxAsisPrinc = rsta(1).Value
      txtAsistencia.Text = auxAsisPrinc
    Else
      auxAsisPrinc = ""
    End If
  End If
  rsta.Close
  Set rsta = Nothing
  Exit Sub
ElseIf gstrLlamador = "frmFirmarHojAnest" Then
  stra = "SELECT FR44CODHOJAQUIRO " & _
         "FROM FR4400 WHERE FR43CODHOJAANEST= " & gintcodhojaquiro
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    auxHojQuirof = rsta(0).Value
  End If
  rsta.Close
  Set rsta = Nothing
  auxHojAnest = gintcodhojaquiro
  sqla = "SELECT AD07CODPROCESO,AD01CODASISTENCI "
  sqla = sqla & " FROM PR0400,FRG300 WHERE PR0400.PR04NUMACTPLAN=FRG300.PR04NUMACTPLAN "
  sqla = sqla & " AND FRG300.FR44CODHOJAQUIRO=" & auxHojQuirof
  Set rsta = objApp.rdoConnect.OpenResultset(sqla)
  If Not rsta.EOF Then
    If Not IsNull(rsta(0).Value) Then
      auxProcPrinc = rsta(0).Value
      txtProceso.Text = auxProcPrinc
    Else
      auxProcPrinc = ""
    End If
    If Not IsNull(rsta(0).Value) Then
      auxAsisPrinc = rsta(1).Value
      txtAsistencia.Text = auxAsisPrinc
    Else
      auxAsisPrinc = ""
    End If
  End If
  rsta.Close
  Set rsta = Nothing
  Exit Sub
End If

blnFecha = False


If glActuacionPedida <> Empty Then
  strFRG300 = "SELECT FR44CODHOJAQUIRO FROM FRG300 WHERE PR04NUMACTPLAN=" & glActuacionPedida
  Set rstFRG300 = objApp.rdoConnect.OpenResultset(strFRG300)
  If Not rstFRG300.EOF Then
    auxHojQuirof = rstFRG300(0).Value
    stra = "SELECT FR43CODHOJAANEST " & _
           "FROM FR4400 WHERE FR44CODHOJAQUIRO= " & auxHojQuirof
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not rsta.EOF Then
      auxHojAnest = rsta(0).Value
    End If
    rsta.Close
    Set rsta = Nothing
    
    sqla = "SELECT AD07CODPROCESO,AD01CODASISTENCI "
    sqla = sqla & "FROM PR0400 WHERE PR04NUMACTPLAN=" & glActuacionPedida
    Set rsta = objApp.rdoConnect.OpenResultset(sqla)
    If Not rsta.EOF Then
      If Not IsNull(rsta(0).Value) Then
        auxProcPrinc = rsta(0).Value
        txtProceso.Text = auxProcPrinc
      Else
        auxProcPrinc = ""
      End If
      If Not IsNull(rsta(0).Value) Then
        auxAsisPrinc = rsta(1).Value
        txtAsistencia.Text = auxAsisPrinc
      Else
        auxAsisPrinc = ""
      End If
    End If
    
    Exit Sub
  Else
    'Nueva Hoja
  End If
  rstFRG300.Close
  Set rstFRG300 = Nothing
End If

If glActuacionPedida <> Empty Then
  sqla = "SELECT AD07CODPROCESO,AD01CODASISTENCI "
  sqla = sqla & "FROM PR0400 WHERE PR04NUMACTPLAN=" & glActuacionPedida
  Set rsta = objApp.rdoConnect.OpenResultset(sqla)
  If Not rsta.EOF Then
    If Not IsNull(rsta(0).Value) Then
      auxProcPrinc = rsta(0).Value
      txtProceso.Text = auxProcPrinc
    Else
      auxProcPrinc = ""
    End If
    If Not IsNull(rsta(0).Value) Then
      auxAsisPrinc = rsta(1).Value
      txtAsistencia.Text = auxAsisPrinc
    Else
      auxAsisPrinc = ""
    End If
  End If

  sqla = "SELECT TO_DATE(TO_CHAR(PR04FECPLANIFIC,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_NUMBER(TO_CHAR(PR04FECPLANIFIC,'HH24,MI'))"
  sqla = sqla & ",TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_NUMBER(TO_CHAR(SYSDATE,'HH24,MI')) FROM PR0400 WHERE PR04NUMACTPLAN=" & glActuacionPedida
  Set rsta = objApp.rdoConnect.OpenResultset(sqla)
  If Not IsNull(rsta.rdoColumns(0).Value) Then
    blnFecha = True
    strHora = objGen.ReplaceStr(rsta(1).Value, ",", ".", 1)
  Else
    strHora = objGen.ReplaceStr(rsta(3).Value, ",", ".", 1)
  End If
  rsta.Close
  Set rsta = Nothing
Else
  Exit Sub
End If

    sqla = "SELECT FR43CODHOJAANEST_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqla)
    auxHojAnest = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing

    sqla = "SELECT FR44CODHOJAQUIRO_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqla)
    auxHojQuirof = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
    
If glActuacionPedida <> Empty Then
  strCuestionario = "select PR41RESPUESTA from pr4100,pr0400 "
  strCuestionario = strCuestionario & " where pr4100.PR03NUMACTPEDI=pr0400.PR03NUMACTPEDI"
  strCuestionario = strCuestionario & " and pr0400.PR04NUMACTPLAN=" & glActuacionPedida
  strCuestionario = strCuestionario & " and pr4100.PR41INDAGRUPA=0"
  strCuestionario = strCuestionario & " and pr4100.PR27CODTIPRESPU=2"
  strCuestionario = strCuestionario & " and pr4100.PR40CODPREGUNTA=1043" 'Num.Quir�fano
  Set rstCuestionario = objApp.rdoConnect.OpenResultset(strCuestionario)
  If rstCuestionario.EOF Then
    auxQui = ""
  Else
    If Not IsNull(rstCuestionario.rdoColumns(0).Value) Then
      auxQui = rstCuestionario.rdoColumns(0).Value
    Else
      auxQui = ""
    End If
  End If
  rstCuestionario.Close
  Set rstCuestionario = Nothing
  If auxQui <> "" Then
    strQuiro = "SELECT COUNT(*) FROM FR9700 WHERE FR97CODQUIROFANO='" & Trim(auxQui) & "'"
    Set rstQuiro = objApp.rdoConnect.OpenResultset(strQuiro)
    If rstQuiro(0).Value > 0 Then
      auxQui = rstQuiro(0).Value
    Else
      auxQui = "NULL"
    End If
    rstQuiro.Close
    Set rstQuiro = Nothing
  Else
    auxQui = "NULL"
  End If
''''''''''''
  strCuestionario = "select PR41RESPUESTA from pr4100,pr0400 "
  strCuestionario = strCuestionario & " where pr4100.PR03NUMACTPEDI=pr0400.PR03NUMACTPEDI"
  strCuestionario = strCuestionario & " and pr0400.PR04NUMACTPLAN=" & glActuacionPedida
  strCuestionario = strCuestionario & " and pr4100.PR41INDAGRUPA=0"
  strCuestionario = strCuestionario & " and pr4100.PR27CODTIPRESPU=2"
  strCuestionario = strCuestionario & " and pr4100.PR40CODPREGUNTA=1044" 'Num.Orden
  Set rstCuestionario = objApp.rdoConnect.OpenResultset(strCuestionario)
  If rstCuestionario.EOF Then
    AuxOrden = 0
  Else
    If Not IsNull(rstCuestionario.rdoColumns(0).Value) Then
      If IsNumeric(rstCuestionario.rdoColumns(0).Value) Then
        AuxOrden = CInt(Trim(rstCuestionario.rdoColumns(0).Value))
      Else
        AuxOrden = 0
      End If
      If AuxOrden > 999 Then
        AuxOrden = 0
      End If
    Else
      AuxOrden = 0
    End If
  End If
  rstCuestionario.Close
  Set rstCuestionario = Nothing
''''''''''''
  strCuestionario = "select PR41RESPUESTA from pr4100,pr0400 "
  strCuestionario = strCuestionario & " where pr4100.PR03NUMACTPEDI=pr0400.PR03NUMACTPEDI"
  strCuestionario = strCuestionario & " and pr0400.PR04NUMACTPLAN=" & glActuacionPedida
  strCuestionario = strCuestionario & " and pr4100.PR41INDAGRUPA=0"
  strCuestionario = strCuestionario & " and pr4100.PR27CODTIPRESPU=2"
  strCuestionario = strCuestionario & " and pr4100.PR40CODPREGUNTA=1041" 'Duraci�n Intervenci�n
  Set rstCuestionario = objApp.rdoConnect.OpenResultset(strCuestionario)
  If rstCuestionario.EOF Then
    AuxDuracion = 0
  Else
    If Not IsNull(rstCuestionario.rdoColumns(0).Value) Then
      If IsNumeric(rstCuestionario.rdoColumns(0).Value) Then
        AuxDuracion = CInt(Trim(rstCuestionario.rdoColumns(0).Value))
      Else
        AuxDuracion = 0
      End If
      If AuxDuracion > 99999 Then
        AuxDuracion = 0
      End If
    Else
      AuxDuracion = 0
    End If
  End If
  rstCuestionario.Close
  Set rstCuestionario = Nothing
''''''''''''
  strCuestionario = "select PR41RESPUESTA from pr4100,pr0400 "
  strCuestionario = strCuestionario & " where pr4100.PR03NUMACTPEDI=pr0400.PR03NUMACTPEDI"
  strCuestionario = strCuestionario & " and pr0400.PR04NUMACTPLAN=" & glActuacionPedida
  strCuestionario = strCuestionario & " and pr4100.PR41INDAGRUPA=0"
  strCuestionario = strCuestionario & " and pr4100.PR27CODTIPRESPU=2"
  strCuestionario = strCuestionario & " and pr4100.PR40CODPREGUNTA=1042" 'Tipo Anestesia
  Set rstCuestionario = objApp.rdoConnect.OpenResultset(strCuestionario)
  If rstCuestionario.EOF Then
    AuxTipAnes = "NULL"
  Else
    If Not IsNull(rstCuestionario.rdoColumns(0).Value) Then
      AuxTipAnes = rstCuestionario.rdoColumns(0).Value
    Else
      AuxTipAnes = "NULL"
    End If
  End If
  rstCuestionario.Close
  Set rstCuestionario = Nothing
''''''''''''
  strCuestionario = "select PR41RESPUESTA from pr4100,pr0400 "
  strCuestionario = strCuestionario & " where pr4100.PR03NUMACTPEDI=pr0400.PR03NUMACTPEDI"
  strCuestionario = strCuestionario & " and pr0400.PR04NUMACTPLAN=" & glActuacionPedida
  strCuestionario = strCuestionario & " and pr4100.PR41INDAGRUPA=0"
  strCuestionario = strCuestionario & " and pr4100.PR27CODTIPRESPU=2"
  strCuestionario = strCuestionario & " and pr4100.PR40CODPREGUNTA=1045" 'Caracter Intervenci�n
  Set rstCuestionario = objApp.rdoConnect.OpenResultset(strCuestionario)
  If rstCuestionario.EOF Then
    AuxCarInterv = 0
  Else
    If Not IsNull(rstCuestionario.rdoColumns(0).Value) Then
      If Trim(rstCuestionario.rdoColumns(0).Value) = "Electiva" Then
        AuxCarInterv = 0
      ElseIf Trim(rstCuestionario.rdoColumns(0).Value) = "Urgencia Selectiva" Then
        AuxCarInterv = 1
      ElseIf Trim(rstCuestionario.rdoColumns(0).Value) = "Urgencia Absoluta" Then
        AuxCarInterv = 2
      Else
        AuxCarInterv = 0
      End If
      If AuxDuracion > 3 Then
        AuxCarInterv = 0
      End If
    Else
      AuxCarInterv = 0
    End If
  End If
  rstCuestionario.Close
  Set rstCuestionario = Nothing
  
End If
    

If glActuacionPedida <> Empty Then

  'Hoja de Anestesia
  strInsert = "INSERT INTO FR4300 (FR43CODHOJAANEST,"
  strInsert = strInsert & "FR97CODQUIROFANO,"
  'strInsert = strInsert & "SG02COD_ENF,"
  'strInsert = strInsert & "SG02COD_ANE,"
  'strInsert = strInsert & "SG02COD_CIR,"
  'strInsert = strInsert & "FR43DESINTERVENCION,"
  strInsert = strInsert & "CI21CODPERSONA,"
  strInsert = strInsert & "FR43FECINTERVEN,"
  strInsert = strInsert & "FR43HORAINTERVEN,"
  'strInsert = strInsert & "SG02COD,"
  strInsert = strInsert & "FR43TIPOANEST,"
  strInsert = strInsert & "FR43INDESTHA)"
  strInsert = strInsert & " "
  strInsert = strInsert & "SELECT " & auxHojAnest & ","
  If auxQui = "NULL" Then
    strInsert = strInsert & "NULL,"
  Else
    strInsert = strInsert & "'" & auxQui & "',"
  End If
  strInsert = strInsert & "CI21CODPERSONA,"
  If blnFecha = False Then
    strInsert = strInsert & "TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY'),"
    strInsert = strInsert & strHora & ","
  Else
    strInsert = strInsert & "TO_DATE(TO_CHAR(PR04FECPLANIFIC,'DD/MM/YYYY'),'DD/MM/YYYY'),"
    strInsert = strInsert & strHora & ","
  End If
  If AuxTipAnes = "NULL" Then
    strInsert = strInsert & "NULL,"
  Else
    strInsert = strInsert & "'" & AuxTipAnes & "',"
  End If
  strInsert = strInsert & "0 FROM PR0400 WHERE PR04NUMACTPLAN=" & glActuacionPedida
  objApp.rdoConnect.Execute strInsert, 64
End If

If glActuacionPedida <> Empty Then
  'Hoja de quirofano
  strInsert = "INSERT INTO FR4400 (FR44CODHOJAQUIRO,"
  strInsert = strInsert & "FR97CODQUIROFANO,"
  strInsert = strInsert & "FR44FECINTERVEN,"
  strInsert = strInsert & "FR44HORAINTERV,"
  strInsert = strInsert & "CI21CODPERSONA,"
  strInsert = strInsert & "FR43CODHOJAANEST,"
  strInsert = strInsert & "FR44NUMORDENDIA,"
  strInsert = strInsert & "FR44INDPROGRAMADA,"
  strInsert = strInsert & "FR44INDIMPREVISTA,"
  strInsert = strInsert & "FR44INDURGENTE,"
  'strInsert = strInsert & "FR44INDANATPATO,"
  'strInsert = strInsert & "FR44INDRADIOTERAP,"
  'strInsert = strInsert & "FR44INDENDOSCOPIA,"
  'strInsert = strInsert & "FR44INDMICROSCOPIA,"
  'strInsert = strInsert & "FR44INDRX,"
  'strInsert = strInsert & "FR44INDCEC,"
  'strInsert = strInsert & "FR44INDCUANTAGASAS,"
  'strInsert = strInsert & "FR44INDLASER,"
  'strInsert = strInsert & "FR44INDOTROS,"
  'strInsert = strInsert & "FR44DESOTROS,"
  'strInsert = strInsert & "SG02COD,"
  strInsert = strInsert & "FR44INDESTHQ)"
  strInsert = strInsert & " "
  strInsert = strInsert & "SELECT " & auxHojQuirof & ","
  If auxQui = "NULL" Then
    strInsert = strInsert & "NULL,"
  Else
    strInsert = strInsert & "'" & auxQui & "',"
  End If
  If blnFecha = False Then
    strInsert = strInsert & "TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY'),"
    strInsert = strInsert & strHora & ","
  Else
    strInsert = strInsert & "TO_DATE(TO_CHAR(PR04FECPLANIFIC,'DD/MM/YYYY'),'DD/MM/YYYY'),"
    strInsert = strInsert & strHora & ","
  End If
  strInsert = strInsert & "CI21CODPERSONA,"
  strInsert = strInsert & auxHojAnest & ","
  strInsert = strInsert & AuxOrden & ","
  If AuxCarInterv = 0 Then
    strInsert = strInsert & "-1,"
  Else
    strInsert = strInsert & "NULL,"
  End If
  If AuxCarInterv = 1 Then
    strInsert = strInsert & "-1,"
  Else
    strInsert = strInsert & "NULL,"
  End If
  If AuxCarInterv = 2 Then
    strInsert = strInsert & "-1,"
  Else
    strInsert = strInsert & "NULL,"
  End If
  strInsert = strInsert & "0 FROM PR0400 WHERE PR04NUMACTPLAN=" & glActuacionPedida
  objApp.rdoConnect.Execute strInsert, 64
End If

If glActuacionPedida = Empty Then
  'MsgBox "No ha seleccionado ninguna intervenci�n.", vbCritical, "Aviso"
  'Exit Sub
Else
  For i = 1 To Awk1.NF
    'Intervenciones
    strInsert = "INSERT INTO FRG300 (FRG3CODINTERVENCION,"
    strInsert = strInsert & "FR44CODHOJAQUIRO,"
    strInsert = strInsert & "PR04NUMACTPLAN,"
    strInsert = strInsert & "AD02CODDPTO,"
    'strInsert = strInsert & "FRG3DESINTERVENCION,"
    strInsert = strInsert & "FRG3NUMMININT,"
    strInsert = strInsert & "FRG3INDPRINCIPAL)"
    'strInsert = strInsert & "SG02COD_CIR,"
    'strInsert = strInsert & "SG02COD_PAY,"
    'strInsert = strInsert & "SG02COD_SAY,"
    'strInsert = strInsert & "SG02COD_INS,"
    'strInsert = strInsert & "SG02COD_ENF,"
    'strInsert = strInsert & "SG02COD_CCL,"
    'strInsert = strInsert & "SG02COD_ACL)"
    strInsert = strInsert & " "
    strInsert = strInsert & "SELECT FRG3CODINTERVENCION_SEQUENCE.nextval,"
    strInsert = strInsert & auxHojQuirof & ","
    strInsert = strInsert & "PR04NUMACTPLAN,"
    strInsert = strInsert & "AD02CODDPTO,"
    strInsert = strInsert & AuxDuracion & ","
    If i = 1 Then
      strInsert = strInsert & "-1 "
    Else
      strInsert = strInsert & "0 "
    End If
    strInsert = strInsert & " FROM PR0400 WHERE PR04NUMACTPLAN=" & Awk1.F(i)
    objApp.rdoConnect.Execute strInsert, 64
  Next i
End If

End Sub


