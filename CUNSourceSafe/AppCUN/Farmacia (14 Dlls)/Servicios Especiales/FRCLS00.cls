VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************
Const PRWinDispSerEsp                 As String = "FR0142"
Const PRWinPrepReposicion             As String = "FR0143"
Const PRWinDispUrgencias              As String = "FR0145"
Const PRWinDispSinPRN                 As String = "FR0146"
Const PRWinValNecQuirof               As String = "FR0130"
Const PRWinVerPedPtaNoEnv             As String = "FR0133"
Const PRWinEstPedPtaNoEnv             As String = "FR0134"
Const PRWinValPedPtaFarma             As String = "FR0135"
Const PRWinVerPedPtaNoVal             As String = "FR0136"
Const PRWinBuscarProdDSE              As String = "FR0119"
Const FRRepPreparacion                As String = "FR1361" 'Informe para la prepararci�n de la reposici�n de la unidad
Const FRRepEntrega                    As String = "FR1362" 'Informe para la entrega de la reposici�n de la unidad
Const FRWinDevolucion                 As String = "FR8800"
Const FRWinVerPedQuiNoVal             As String = "FR0148"
Const FRRepPrepQuiro                  As String = "FR1481" 'Informe para la prepararci�n de la reposici�n del quirofano
Const FRRepEntregaQuiro               As String = "FR1482" 'Informe para la entrega de la reposici�n del quirofano
Const FRRepPrepQuiro2                 As String = "FR1483" 'Informe para la prepararci�n de la reposici�n del quirofano
Const FRRepEntregaQuiro2              As String = "FR1484" 'Informe para la entrega de la reposici�n del quirofano
Const FRRepPrepQuiro3                 As String = "FR1485" 'Informe para la prepararci�n de la reposici�n del quirofano
Const FRRepEntregaQuiro3              As String = "FR1486" 'Informe para la entrega de la reposici�n del quirofano
Const FRWinVerTotQuiNoVal             As String = "FR0158"
Const FRRepPrepTotQuiMe               As String = "FR1581" 'Informe para la prepararci�n de la reposici�n del quirofano total,Medicamentos
Const FRRepPrepTotQuiPS               As String = "FR1582" 'Informe para la prepararci�n de la reposici�n del quirofano total,Prod.Sanitarios
Const FRRepPrepTotQuiSIV              As String = "FR1583" 'Informe para la prepararci�n de la reposici�n del quirofano total,Soluciones
Const FRRepRepTotQuiMe                As String = "FR1584" 'Informe para la reposici�n de la reposici�n del quirofano total,Medicamentos
Const FRRepRepTotQuiPS                As String = "FR1585" 'Informe para la reposici�n de la reposici�n del quirofano total,Prod.Sanitarios
Const FRRepRepTotQuiSIV               As String = "FR1586" 'Informe para la reposici�n de la reposici�n del quirofano total,Soluciones
Const FRRepPrepTotQuiProt             As String = "FR1587" 'Informe para la prepararci�n de la reposici�n del quirofano total,Pr�tesis
Const FRRepRepTotQuiProt              As String = "FR1588" 'Informe para la reposici�n de la reposici�n del quirofano total,Pr�tesis
Const FRRepPrepTotAneMe               As String = "FRR181"
Const FRRepPrepTotAnePS               As String = "FRR182"
Const FRRepPrepTotAneSIV              As String = "FRR183"
Const FRRepRepTotAneMe                As String = "FRR184"
Const FRRepRepTotAnePS                As String = "FRR185"
Const FRRepRepTotAneSIV               As String = "FRR186"
Const FRRepPrepTotAneProt             As String = "FRR187"
Const FRRepRepTotAneProt              As String = "FRR188"
Const FRRepRepDispPlanta              As String = "FR1351"
Const PRWinVerPedPtaNoValSec          As String = "FR0336"
Const FRRepPreparacionSec             As String = "FR3361"
Const FRRepEntregaSec                 As String = "FR3362"
Const FRRepRepDispPlantaSec           As String = "FR1353"
Const FRWinPerfilProductos            As String = "FR0144"
Const FRWinCodBarrFarm                As String = "FR8900"



Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objCW = mobjCW
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
  Case PRWinDispSerEsp
      Load frmDispSerEsp
      'Call objsecurity.AddHelpContext(528)
      Call frmDispSerEsp.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDispSerEsp
      Set frmDispSerEsp = Nothing
  Case PRWinPrepReposicion
      Load frmPrepReposicion
      'Call objsecurity.AddHelpContext(528)
      Call frmPrepReposicion.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmPrepReposicion
      Set frmPrepReposicion = Nothing
  Case PRWinDispUrgencias
      Load frmDispUrgencias
      'Call objsecurity.AddHelpContext(528)
      Call frmDispUrgencias.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDispUrgencias
      Set frmDispUrgencias = Nothing
  Case PRWinDispSinPRN
      Load frmDispSinPRN
      'Call objsecurity.AddHelpContext(528)
      Call frmDispSinPRN.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDispSinPRN
      Set frmDispSinPRN = Nothing
  Case PRWinValNecQuirof
      Load frmValNecQuirof
      'Call objsecurity.AddHelpContext(528)
      Call frmValNecQuirof.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmValNecQuirof
      Set frmValNecQuirof = Nothing
  Case PRWinVerPedPtaNoEnv
      Load frmVerPedPtaNoEnv
      'Call objsecurity.AddHelpContext(528)
      Call frmVerPedPtaNoEnv.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmVerPedPtaNoEnv
      Set frmVerPedPtaNoEnv = Nothing
   Case PRWinEstPedPtaNoEnv
      Load FrmEstPedPtaNoEnv
      'Call objsecurity.AddHelpContext(528)
      Call FrmEstPedPtaNoEnv.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmEstPedPtaNoEnv
      Set FrmEstPedPtaNoEnv = Nothing
    Case PRWinValPedPtaFarma
      Load frmValPedPtaFarma
      'Call objsecurity.AddHelpContext(528)
      Call frmValPedPtaFarma.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmValPedPtaFarma
      Set frmValPedPtaFarma = Nothing
    Case PRWinVerPedPtaNoVal
      Load FrmVerPedPtaNoVal
      'Call objsecurity.AddHelpContext(528)
      Call FrmVerPedPtaNoVal.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmVerPedPtaNoVal
      Set FrmVerPedPtaNoVal = Nothing
    Case PRWinBuscarProdDSE
      Load frmBusProductosDSE
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProductosDSE.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusProductosDSE
      Set frmBusProductosDSE = Nothing
    Case FRWinDevolucion
      Load FrmRegDev
      'Call objsecurity.AddHelpContext(528)
      Call FrmRegDev.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmRegDev
      Set FrmRegDev = Nothing
    Case FRWinVerPedQuiNoVal
      Load FrmVerPedQuiNoVal
      'Call objsecurity.AddHelpContext(528)
      Call FrmVerPedQuiNoVal.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmVerPedQuiNoVal
      Set FrmVerPedQuiNoVal = Nothing
    Case FRWinVerTotQuiNoVal
      Load FrmVerTotQuiNoVal
      'Call objsecurity.AddHelpContext(528)
      Call FrmVerTotQuiNoVal.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmVerTotQuiNoVal
      Set FrmVerTotQuiNoVal = Nothing
    Case PRWinVerPedPtaNoValSec
      Load FrmVerPedPtaNoValSec
      'Call objsecurity.AddHelpContext(528)
      Call FrmVerPedPtaNoValSec.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmVerPedPtaNoValSec
      Set FrmVerPedPtaNoValSec = Nothing
    Case FRWinPerfilProductos
      Load FrmPFTProd
      'Call objsecurity.AddHelpContext(528)
      Call FrmPFTProd.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmPFTProd
      Set FrmPFTProd = Nothing
    Case FRWinCodBarrFarm
      Load frmCodBarrDpto
      'Call objsecurity.AddHelpContext(528)
      Call frmCodBarrDpto.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmCodBarrDpto
      Set frmCodBarrDpto = Nothing
      
      
     
  End Select
  Call ERR.Clear

End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 44, 1 To 4) As Variant
       
  aProcess(1, 1) = PRWinDispSerEsp
  aProcess(1, 2) = "Dispensar Servicios Especiales"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinPrepReposicion
  aProcess(2, 2) = "Preparar Reposicion"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PRWinDispUrgencias
  aProcess(3, 2) = "Dispensar Urgencias"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = PRWinDispSinPRN
  aProcess(4, 2) = "Dispensar Sin PRN"
  aProcess(4, 3) = False
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = PRWinValNecQuirof
  aProcess(5, 2) = "Validar Necesidades Quir�fano"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
  
  aProcess(6, 1) = PRWinVerPedPtaNoEnv
  aProcess(6, 2) = "Ver Pedidos de Planta sin Enviar"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = PRWinEstPedPtaNoEnv
  aProcess(7, 2) = "Estudiar Pedido de Planta sin Enviar"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
  
  aProcess(8, 1) = PRWinValPedPtaFarma
  aProcess(8, 2) = "Validar Pedido Farmacia"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow

  aProcess(9, 1) = PRWinVerPedPtaNoVal
  aProcess(9, 2) = "Ver Pedido Planta No Validado"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeWindow
  
  aProcess(10, 1) = PRWinBuscarProdDSE
  aProcess(10, 2) = "Buscar Productos"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow
  
  aProcess(11, 1) = FRRepPreparacion
  aProcess(11, 2) = "Informe Preparaci�n Reposici�n"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeReport
  
  aProcess(12, 1) = FRRepEntrega
  aProcess(12, 2) = "Informe Entrega Reposici�n"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeReport
  
  aProcess(13, 1) = FRWinDevolucion
  aProcess(13, 2) = "Abonos de Productos"
  aProcess(13, 3) = True
  aProcess(13, 4) = cwTypeWindow

  aProcess(14, 1) = FRWinVerPedQuiNoVal
  aProcess(14, 2) = "Validar Pedido Quir�fano"
  aProcess(14, 3) = False
  aProcess(14, 4) = cwTypeWindow

  aProcess(15, 1) = FRRepPrepQuiro
  aProcess(15, 2) = "Inf. Preparaci�n Quir�fano Medicamentos"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeReport
  
  aProcess(16, 1) = FRRepEntregaQuiro
  aProcess(16, 2) = "Inf. Entrega Quir�fano Medicamentos"
  aProcess(16, 3) = False
  aProcess(16, 4) = cwTypeReport

  aProcess(17, 1) = FRRepPrepQuiro2
  aProcess(17, 2) = "Inf. Preparaci�n Quir�fano Prod.Sanitarios"
  aProcess(17, 3) = False
  aProcess(17, 4) = cwTypeReport
  
  aProcess(18, 1) = FRRepEntregaQuiro2
  aProcess(18, 2) = "Inf. Entrega Quir�fano Prod.Sanitarios"
  aProcess(18, 3) = False
  aProcess(18, 4) = cwTypeReport
  
  aProcess(19, 1) = FRRepPrepQuiro3
  aProcess(19, 2) = "Inf. Preparaci�n Quir�fano Soluciones I.V."
  aProcess(19, 3) = False
  aProcess(19, 4) = cwTypeReport
  
  aProcess(20, 1) = FRRepEntregaQuiro3
  aProcess(20, 2) = "Inf. Entrega Quir�fano Soluciones I.V."
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeReport
  
  aProcess(21, 1) = FRWinVerTotQuiNoVal
  aProcess(21, 2) = "Validar Pedido Total Quir�fano"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeWindow

  aProcess(22, 1) = FRRepPrepTotQuiMe
  aProcess(22, 2) = "Inf.Prep.Qui.Medicamentos"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeReport

  aProcess(23, 1) = FRRepPrepTotQuiPS
  aProcess(23, 2) = "Inf.Prep.Qui.Prod.Sanitarios"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeReport

  aProcess(24, 1) = FRRepPrepTotQuiSIV
  aProcess(24, 2) = "Inf.Prep.Qui.Soluciones I.V."
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeReport

  aProcess(25, 1) = FRRepRepTotQuiMe
  aProcess(25, 2) = "Inf.Repo.Qui.Medicamentos"
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeReport
  
  aProcess(26, 1) = FRRepRepTotQuiPS
  aProcess(26, 2) = "Inf.Repo.Qui.Prod.Sanitarios"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeReport
  
  aProcess(27, 1) = FRRepRepTotQuiSIV
  aProcess(27, 2) = "Inf.Repo.Qui.Soluciones I.V."
  aProcess(27, 3) = False
  aProcess(27, 4) = cwTypeReport

  aProcess(28, 1) = FRRepPrepTotAneMe
  aProcess(28, 2) = "Inf.Prep.Ane.Medicamentos"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeReport

  aProcess(29, 1) = FRRepPrepTotAnePS
  aProcess(29, 2) = "Inf.Prep.Ane.Prod.Sanitarios"
  aProcess(29, 3) = False
  aProcess(29, 4) = cwTypeReport

  aProcess(30, 1) = FRRepPrepTotAneSIV
  aProcess(30, 2) = "Inf.Prep.Ane.Soluciones I.V."
  aProcess(30, 3) = False
  aProcess(30, 4) = cwTypeReport

  aProcess(31, 1) = FRRepRepTotAneMe
  aProcess(31, 2) = "Inf.Repo.Ane.Medicamentos"
  aProcess(31, 3) = False
  aProcess(31, 4) = cwTypeReport
  
  aProcess(32, 1) = FRRepRepTotAnePS
  aProcess(32, 2) = "Inf.Repo.Ane.Prod.Sanitarios"
  aProcess(32, 3) = False
  aProcess(32, 4) = cwTypeReport
  
  aProcess(33, 1) = FRRepRepTotAneSIV
  aProcess(33, 2) = "Inf.Repo.Ane.Soluciones I.V."
  aProcess(33, 3) = False
  aProcess(33, 4) = cwTypeReport

  aProcess(34, 1) = FRRepPrepTotAneProt
  aProcess(34, 2) = "Inf.Prep.Ane.Pr�tesis"
  aProcess(34, 3) = False
  aProcess(34, 4) = cwTypeReport
  
  aProcess(35, 1) = FRRepRepTotAneProt
  aProcess(35, 2) = "Inf.Repo.Ane.Pr�tesis"
  aProcess(35, 3) = False
  aProcess(35, 4) = cwTypeReport

  aProcess(36, 1) = FRRepPrepTotQuiProt
  aProcess(36, 2) = "Inf.Prep.Qui.Pr�tesis"
  aProcess(36, 3) = False
  aProcess(36, 4) = cwTypeReport

  aProcess(37, 1) = FRRepRepTotQuiProt
  aProcess(37, 2) = "Inf.Repo.Qui.Pr�tesis"
  aProcess(37, 3) = False
  aProcess(37, 4) = cwTypeReport

  aProcess(38, 1) = FRRepRepDispPlanta
  aProcess(38, 2) = "Listado de Dispensacion Plantas Dpto./Fecha"
  aProcess(38, 3) = False
  aProcess(38, 4) = cwTypeReport
  
  aProcess(39, 1) = PRWinVerPedPtaNoValSec
  aProcess(39, 2) = "Ver Pedido Planta/Seccion No Validado"
  aProcess(39, 3) = False
  aProcess(39, 4) = cwTypeWindow
  
  aProcess(40, 1) = FRRepPreparacionSec
  aProcess(40, 2) = "Informe Preparaci�n Reposici�n por Seccion"
  aProcess(40, 3) = False
  aProcess(40, 4) = cwTypeReport
  
  aProcess(41, 1) = FRRepEntregaSec
  aProcess(41, 2) = "Informe Entrega Reposici�n por Seccion"
  aProcess(41, 3) = False
  aProcess(41, 4) = cwTypeReport
  
  aProcess(42, 1) = FRRepRepDispPlantaSec
  aProcess(42, 2) = "Listado de Disp. Plantas Dpto./Sec./Fecha"
  aProcess(42, 3) = False
  aProcess(42, 4) = cwTypeReport
  
  aProcess(43, 1) = FRWinPerfilProductos
  aProcess(43, 2) = "Perfil de Productos"
  aProcess(43, 3) = True
  aProcess(43, 4) = cwTypeWindow

  aProcess(44, 1) = FRWinCodBarrFarm
  aProcess(44, 2) = "C�digos de barras de farmacia"
  aProcess(44, 3) = True
  aProcess(44, 4) = cwTypeWindow
End Sub
