VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "msrdc20.ocx"
Begin VB.Form FrmPFTProd 
   Caption         =   "B�squeda de Productos de Farmacia Dispensados a Pacientes"
   ClientHeight    =   6945
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6945
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Frame fraOpcion 
      Caption         =   "Seleccione opci�n"
      ForeColor       =   &H00FF0000&
      Height          =   615
      Left            =   240
      TabIndex        =   11
      Top             =   1080
      Width           =   3735
      Begin VB.OptionButton optKits 
         Caption         =   "Fabricaci�n"
         Height          =   255
         Left            =   2280
         TabIndex        =   14
         Top             =   240
         Width           =   1215
      End
      Begin VB.OptionButton optServicio 
         Caption         =   "Servicio"
         Height          =   255
         Left            =   1200
         TabIndex        =   13
         Top             =   240
         Width           =   1215
      End
      Begin VB.OptionButton optPaciente 
         Caption         =   "Paciente"
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Value           =   -1  'True
         Width           =   1215
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   495
      Left            =   10560
      TabIndex        =   6
      Top             =   480
      Width           =   1095
   End
   Begin SSCalendarWidgets_A.SSDateCombo dcboHasta 
      Height          =   285
      Left            =   7440
      TabIndex        =   4
      Top             =   600
      Width           =   1455
      _Version        =   65537
      _ExtentX        =   2566
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16776960
   End
   Begin SSCalendarWidgets_A.SSDateCombo dcboDesde 
      Height          =   285
      Left            =   5880
      TabIndex        =   3
      Top             =   600
      Width           =   1455
      _Version        =   65537
      _ExtentX        =   2566
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16776960
   End
   Begin MSRDC.MSRDC MSRDC1 
      Height          =   330
      Left            =   4440
      Top             =   120
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   582
      _Version        =   327681
      Options         =   0
      CursorDriver    =   1
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   3
      LockType        =   1
      QueryType       =   0
      Prompt          =   3
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Bindings        =   "FR0144.frx":0000
      Height          =   6480
      Left            =   120
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1800
      Width           =   11655
      _Version        =   131078
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      _ExtentX        =   20558
      _ExtentY        =   11430
      _StockProps     =   79
      Caption         =   "PERFIL DE PRODUCTOS"
   End
   Begin VB.CommandButton cmdBuscar 
      BackColor       =   &H00FF0000&
      Caption         =   "BUSCAR"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   9000
      TabIndex        =   5
      Top             =   480
      Width           =   1335
   End
   Begin VB.TextBox txtCodigoProducto 
      BackColor       =   &H00FFFF00&
      ForeColor       =   &H80000012&
      Height          =   285
      Left            =   240
      MaxLength       =   6
      TabIndex        =   2
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label Label4 
      Caption         =   "Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7440
      TabIndex        =   10
      Top             =   240
      Width           =   735
   End
   Begin VB.Label Label3 
      Caption         =   "Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5880
      TabIndex        =   9
      Top             =   240
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Descripci�n Producto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1800
      TabIndex        =   7
      Top             =   240
      Width           =   2055
   End
   Begin VB.Label lblDescripcionProducto 
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1800
      TabIndex        =   0
      Top             =   600
      Width           =   3975
   End
   Begin VB.Label Label1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "C�digo Producto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   1455
   End
End
Attribute VB_Name = "FrmPFTProd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intClaveCodigo As Long
Private Sub Obtener_Producto()
   Dim mensaje As String
   Dim SQL As String
   Dim qryFTP As rdoQuery
   Dim rstFTP As rdoResultset
   
'   If txtCodigoProducto.Text = "" Then
'     mensaje = MsgBox("No hay ning�n producto seleccionado.", vbInformation, "Aviso")
'     txtCodigoProducto.SetFocus
'     Exit Sub
'   End If
'
     SQL = "SELECT fr7300.fr73desproducto,fr7300.fr73codproducto from fr7300 "
     SQL = SQL & " where fr7300.fr73codintfar = ?"
'
    Set qryFTP = objApp.rdoConnect.CreateQuery("", SQL)
      qryFTP(0) = txtCodigoProducto.Text
      Set rstFTP = qryFTP.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If rstFTP.EOF = True Then
      mensaje = MsgBox("No existe el producto seleccionado.", vbInformation, "Aviso")
      lblDescripcionProducto = ""
      txtCodigoProducto.SetFocus
      Exit Sub
    End If
     lblDescripcionProducto = rstFTP(0)
     intClaveCodigo = rstFTP(1)

End Sub
Private Sub cmdBuscar_Click()
    Screen.MousePointer = vbHourglass
    Dim SQL As String
    Dim qryFTP As rdoQuery
    Dim rstFTP As rdoResultset
    If optPaciente.Value = True Then ' b�squeda por paciente
     SQL = "SELECT ci2200.ci22numhistoria Historia, ci2200.ci22nombre ||' '||ci2200.ci22priapel ||' '|| "
     SQL = SQL & " ci2200.ci22segapel Nombre,"
     SQL = SQL & " to_char(FR6500.FR65FECHA,'DD/MM/YYYY HH24:MI') FechaFact,"
     SQL = SQL & " FR6500.FR65cantidad Cant,"
     SQL = SQL & " fr6500.fr66codpeticion Peticion,"
     SQL = SQL & " fr6500.pr62codhojaquir HojaQuir,"
     SQL = SQL & " DECODE(fr6500.fr65indimputquir,0,'ANEST',-1,'QUIR',NULL,"
     SQL = SQL & " decode(fr6600.fr66indom||fr6600.fr66indcesta,-1||null ,'OM',0||-1,'CESTA',null||null,'','PRN'),'') Origen,"
     SQL = SQL & " to_char(FR6600.FR66fecdispen,'DD/MM/YYYY HH24:MI') FechaDispen,"
     SQL = SQL & " fr6600.ad02coddpto||'-'||ad0200.ad02desdpto Dpto,"
     SQL = SQL & " fr6500.ad07codproceso Proceso,"
     SQL = SQL & " fr6500.ad01codasistenci Asistencia,"
     SQL = SQL & " fr6500.sg02cod_add||'-'||"
     SQL = SQL & " sg0200.sg02nom||' '||sg0200.sg02ape1||' '||sg0200.sg02ape2 Usuario,"
     SQL = SQL & " fr7300.fr73desproducto Descripci�n"
     SQL = SQL & " from fr6500,ci2200,fr6600,sg0200,ad0200,fr7300"
     SQL = SQL & " where fr6500.ci21codpersona = ci2200.ci21codpersona and "
     SQL = SQL & " fr6500.fr66codpeticion = fr6600.fr66codpeticion(+) and "
     SQL = SQL & " fr6500.sg02cod_add = sg0200.sg02cod and"
     SQL = SQL & " fr6600.ad02coddpto = ad0200.ad02coddpto(+) and"
     SQL = SQL & " fr6500.fr73codproducto = fr7300.fr73codproducto and"
     SQL = SQL & " fr6500.fr73codproducto = ? and"
     SQL = SQL & " fr6500.fr65fecha >= TO_DATE(?,'DD/MM/YYYY') and"
     SQL = SQL & " fr6500.fr65fecha <= TO_DATE(?,'DD/MM/YYYY')"
     SQL = SQL & " Order By fr6500.ci21codpersona,fr6500.fr65fecha desc"
    ElseIf optServicio.Value = True Then ' b�squeda por servicio
     SQL = "SELECT frk100.ad02coddpto||'-'||ad0200.ad02desdpto Dpto,"
     SQL = SQL & " to_char(FRk100.FRk1fecmov,'DD/MM/YYYY HH24:MI') Fecha,"
     SQL = SQL & " frk100.frk1cantidad Cant,fr5500.fr55codnecesunid Peticion,"
     SQL = SQL & " to_char(FR5500.FR55fecdispen,'DD/MM/YYYY HH24:MI') FechaDispen,"
     SQL = SQL & " fr5500.sg02cod_add||'-'||"
     SQL = SQL & " sg0200.sg02nom||' '||sg0200.sg02ape1||' '||sg0200.sg02ape2 Usuario"
     SQL = SQL & " from frk100,ad0200,fr5500,sg0200"
     SQL = SQL & " where frk100.ad02coddpto = ad0200.ad02coddpto and"
     SQL = SQL & " frk100.frk1codnecesunid = fr5500.fr55codnecesunid(+) and "
     SQL = SQL & " fr5500.sg02cod_add = sg0200.sg02cod(+) and"
     SQL = SQL & " frk100.fr73codproducto = ? and"
     SQL = SQL & " frk100.frk1fecmov >= TO_DATE(?,'DD/MM/YYYY') and"
     SQL = SQL & " frk100.frk1fecmov <= TO_DATE(?,'DD/MM/YYYY')"
     SQL = SQL & " Order By frk100.ad02coddpto"
    Else ' b�squeda por Fabricaci�n
     SQL = "select to_char(FR8000.FR80fecmovimiento,'DD/MM/YYYY HH24:MI') Fecha, "
     SQL = SQL & " fr8000.fr80unisalen Cant"
     SQL = SQL & " from fr8000"
     SQL = SQL & " where fr8000.fr73codproducto = ? and"
     SQL = SQL & " fr8000.fr80fecmovimiento >= TO_DATE(?,'DD/MM/YYYY') and"
     SQL = SQL & " fr8000.fr80fecmovimiento <= TO_DATE(?,'DD/MM/YYYY') and"
     SQL = SQL & " fr8000.fr90codtipmov = 1 and"
     SQL = SQL & " fr8000.fr04codalmacen_ori = 0"
    ' mostrando los KIT,s a los que pertenece
'      SQL = "select to_char(FR8000.FR80fecmovimiento,'DD/MM/YYYY HH24:MI') Fecha,"
'      SQL = SQL & " fr8000.fr80unisalen Cant,"
'      SQL = SQL & " fr7300.fr73codproducto || '-' ||fr7300.fr73desproducto Descr"
'      SQL = SQL & " from fr8000,fr7300,fr7100,fr6900"
'      SQL = SQL & " where fr8000.fr73codproducto = ? and"
'      SQL = SQL & " fr8000.fr80fecmovimiento >= TO_DATE(?,'DD/MM/YYYY') and"
'      SQL = SQL & " fr8000.fr80fecmovimiento <= TO_DATE(?,'DD/MM/YYYY') and"
'      SQL = SQL & " fr8000.fr90codtipmov = 1 and"
'      SQL = SQL & " fr8000.fr04codalmacen_ori = 0 and"
'      SQL = SQL & " fr8000.fr73codproducto = fr7100.fr73codproducto and"
'      SQL = SQL & " fr7100.fr69codprocfabric = fr6900.fr69codprocfabric and"
'      SQL = SQL & " fr6900.fr73codproducto = fr7300.fr73codproducto"
    End If
    Set qryFTP = objApp.rdoConnect.CreateQuery("", SQL)
          qryFTP(0) = intClaveCodigo
          qryFTP(1) = Format(dcboDesde.Text, "dd/mm/yyyy")
          qryFTP(2) = Format(DateAdd("d", 1, dcboHasta.Text), "dd/mm/yyyy")
     Set rstFTP = qryFTP.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

'     Set MSRDC1.Resultset = rstFTP
'     Screen.MousePointer = vbDefault
     
     SSDBGrid1.Redraw = False
     SSDBGrid1.Reset
     Set MSRDC1.Resultset = rstFTP
     If optPaciente.Value = True Then ' b�squeda por paciente
       SSDBGrid1.Columns(0).Width = 900
       SSDBGrid1.Columns(1).Width = 3300
       SSDBGrid1.Columns(2).Width = 1600
       SSDBGrid1.Columns(3).Width = 700
       SSDBGrid1.Columns(4).Width = 900
       SSDBGrid1.Columns(5).Width = 950
       SSDBGrid1.Columns(6).Width = 900
       SSDBGrid1.Columns(7).Width = 1600
       SSDBGrid1.Columns(8).Width = 2000
       SSDBGrid1.Columns(9).Width = 1600
       SSDBGrid1.Columns(10).Width = 1600
       SSDBGrid1.Columns(11).Width = 3300
       SSDBGrid1.Columns(12).Width = 3500
     ElseIf optServicio.Value = True Then ' b�squeda por servicio
       SSDBGrid1.Columns(0).Width = 2500
       SSDBGrid1.Columns(1).Width = 1600
       SSDBGrid1.Columns(2).Width = 700
       SSDBGrid1.Columns(3).Width = 900
       SSDBGrid1.Columns(4).Width = 1600
       SSDBGrid1.Columns(5).Width = 3300
     Else
       SSDBGrid1.Columns(0).Width = 1600
       SSDBGrid1.Columns(1).Width = 700
'       SSDBGrid1.Columns(2).Width = 3500
     End If
     SSDBGrid1.Redraw = True
     Screen.MousePointer = vbDefault
     txtCodigoProducto.SetFocus

'
End Sub

Private Sub cmdSalir_Click()
   Unload Me
End Sub

Private Sub Form_Load()
    cmdBuscar.Enabled = False
End Sub

Private Sub txtCodigoProducto_Change()
'  If txtCodigoProducto.Text <> "" Then
   If Len(txtCodigoProducto.Text) = 6 Then
    cmdBuscar.Enabled = True
    cmdBuscar.SetFocus
    Obtener_Producto
  Else
    cmdBuscar.Enabled = False
   End If
End Sub
