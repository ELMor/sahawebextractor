VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Begin VB.Form FrmVerTotQuiNoVal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. PEDIR SERVICIO FARMACIA. Ver Pedido Quir�fano no Dispensado. "
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0158.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Quirofano"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Index           =   1
      Left            =   120
      TabIndex        =   9
      Top             =   2760
      Visible         =   0   'False
      Width           =   11340
      Begin TabDlg.SSTab tabTab1 
         Height          =   990
         Index           =   0
         Left            =   120
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   360
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   1746
         _Version        =   327681
         TabOrientation  =   3
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   1
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0158.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblLabel1(10)"
         Tab(0).Control(1)=   "txttext1(6)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0158.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "AG11DESRECURSO"
            Height          =   330
            Index           =   6
            Left            =   -74880
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Descripci�n Qui."
            Top             =   360
            Width           =   5175
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   585
            Index           =   2
            Left            =   120
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   120
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   1032
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Quirofano"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   -74880
            TabIndex        =   13
            Top             =   120
            Width           =   1905
         End
      End
      Begin SSDataWidgets_B.SSDBGrid grdAux 
         Height          =   1455
         Index           =   1
         Left            =   120
         TabIndex        =   14
         Top             =   1560
         Visible         =   0   'False
         Width           =   10170
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   28
         stylesets.count =   1
         stylesets(0).Name=   "Bloqueada"
         stylesets(0).ForeColor=   16777215
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "FR0158.frx":0044
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   28
         Columns(0).Width=   2566
         Columns(0).Caption=   "frk3codnecesunid"
         Columns(0).Name =   "frk3codnecesunid"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2487
         Columns(1).Caption=   "fr97codquirofano"
         Columns(1).Name =   "fr97codquirofano"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   2328
         Columns(2).Caption=   "fr26codestpetic"
         Columns(2).Name =   "fr26codestpetic"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   2037
         Columns(3).Caption=   "frk3indquiane"
         Columns(3).Name =   "frk3indquiane"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   2064
         Columns(4).Caption=   "frk4numlinea"
         Columns(4).Name =   "frk4numlinea"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   2514
         Columns(5).Caption=   "frk5codnecesval"
         Columns(5).Name =   "frk5codnecesval"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   2328
         Columns(6).Caption=   "fr73codproducto"
         Columns(6).Name =   "fr73codproducto"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   2514
         Columns(7).Caption=   "frk5cantnecesquir"
         Columns(7).Name =   "frk5cantnecesquir"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   2540
         Columns(8).Caption=   "frk5cantsumifarm"
         Columns(8).Name =   "frk5cantsumifarm"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   2064
         Columns(9).Caption=   "frk5indquiane"
         Columns(9).Name =   "frk5indquiane"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   1746
         Columns(10).Caption=   "frk5indfac"
         Columns(10).Name=   "frk5indfac"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   1746
         Columns(11).Caption=   "frk5indbloq"
         Columns(11).Name=   "frk5indbloq"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(12).Width=   2117
         Columns(12).Caption=   "frk5indtratado"
         Columns(12).Name=   "frk5indtratado"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(13).Width=   2275
         Columns(13).Caption=   "frk5cantultfarm"
         Columns(13).Name=   "frk5cantultfarm"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(14).Width=   1879
         Columns(14).Caption=   "frh9ubicacion"
         Columns(14).Name=   "frh9ubicacion"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   1905
         Columns(15).Caption=   "fr73codintfar"
         Columns(15).Name=   "fr73codintfar"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         Columns(16).Width=   2090
         Columns(16).Caption=   "fr73referencia"
         Columns(16).Name=   "fr73referencia"
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).FieldLen=   256
         Columns(17).Width=   2487
         Columns(17).Caption=   "fr73desproducto"
         Columns(17).Name=   "fr73desproducto"
         Columns(17).DataField=   "Column 17"
         Columns(17).DataType=   8
         Columns(17).FieldLen=   256
         Columns(18).Width=   1482
         Columns(18).Caption=   "fr73dosis"
         Columns(18).Name=   "fr73dosis"
         Columns(18).DataField=   "Column 18"
         Columns(18).DataType=   8
         Columns(18).FieldLen=   256
         Columns(19).Width=   2143
         Columns(19).Caption=   "frh7codformfar"
         Columns(19).Name=   "frh7codformfar"
         Columns(19).DataField=   "Column 19"
         Columns(19).DataType=   8
         Columns(19).FieldLen=   256
         Columns(20).Width=   2540
         Columns(20).Caption=   "fr93codunimedida"
         Columns(20).Name=   "fr93codunimedida"
         Columns(20).DataField=   "Column 20"
         Columns(20).DataType=   8
         Columns(20).FieldLen=   256
         Columns(21).Width=   2275
         Columns(21).Caption=   "fr73tamenvase"
         Columns(21).Name=   "fr73tamenvase"
         Columns(21).DataField=   "Column 21"
         Columns(21).DataType=   8
         Columns(21).FieldLen=   256
         Columns(22).Width=   2223
         Columns(22).Caption=   "fr73indprodsan"
         Columns(22).Name=   "fr73indprodsan"
         Columns(22).DataField=   "Column 22"
         Columns(22).DataType=   8
         Columns(22).FieldLen=   256
         Columns(23).Width=   1879
         Columns(23).Caption=   "fr73indinfiv"
         Columns(23).Name=   "fr73indinfiv"
         Columns(23).DataField=   "Column 23"
         Columns(23).DataType=   8
         Columns(23).FieldLen=   256
         Columns(24).Width=   2223
         Columns(24).Caption=   "fr73indmatimp"
         Columns(24).Name=   "fr73indmatimp"
         Columns(24).DataField=   "Column 24"
         Columns(24).DataType=   8
         Columns(24).FieldLen=   256
         Columns(25).Width=   1429
         Columns(25).Caption=   "Ac.Ped."
         Columns(25).Name=   "Ac.Ped."
         Columns(25).DataField=   "Column 25"
         Columns(25).DataType=   8
         Columns(25).FieldLen=   256
         Columns(26).Width=   1429
         Columns(26).Caption=   "Ac.Disp."
         Columns(26).Name=   "Ac.Disp."
         Columns(26).DataField=   "Column 26"
         Columns(26).DataType=   8
         Columns(26).FieldLen=   256
         Columns(27).Width=   1402
         Columns(27).Caption=   "Ac.Ori."
         Columns(27).Name=   "Ac.Ori."
         Columns(27).DataField=   "Column 27"
         Columns(27).DataType=   8
         Columns(27).FieldLen=   256
         _ExtentX        =   17939
         _ExtentY        =   2566
         _StockProps     =   79
      End
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   9960
      Top             =   7560
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdBloquear 
      Caption         =   "NO DISPENSAR"
      Height          =   375
      Left            =   6720
      TabIndex        =   3
      Top             =   7560
      Width           =   1695
   End
   Begin VB.CommandButton cmdValidar 
      Caption         =   "Dispensar"
      Height          =   375
      Left            =   3480
      TabIndex        =   2
      Top             =   7560
      Width           =   1935
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6975
      Left            =   0
      TabIndex        =   4
      Top             =   480
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   12303
      _Version        =   327681
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Necesidades Quir�fano"
      TabPicture(0)   =   "FR0158.frx":0060
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(2)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Necesidades Anestesia"
      TabPicture(1)   =   "FR0158.frx":007C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(0)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.Frame fraFrame1 
         Caption         =   "Necesidades Quir�fano"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   6480
         Index           =   2
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   11535
         Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
            Height          =   6075
            Index           =   1
            Left            =   120
            TabIndex        =   6
            Top             =   360
            Width           =   11280
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   16
            stylesets.count =   4
            stylesets(0).Name=   "SolIV"
            stylesets(0).BackColor=   12632256
            stylesets(0).Picture=   "FR0158.frx":0098
            stylesets(1).Name=   "Protesis"
            stylesets(1).BackColor=   9240575
            stylesets(1).Picture=   "FR0158.frx":00B4
            stylesets(2).Name=   "Medicamento"
            stylesets(2).BackColor=   16777215
            stylesets(2).Picture=   "FR0158.frx":00D0
            stylesets(3).Name=   "ProdSani"
            stylesets(3).BackColor=   12615935
            stylesets(3).Picture=   "FR0158.frx":00EC
            SelectTypeRow   =   3
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns.Count   =   16
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�d. Prod"
            Columns(0).Name =   "C�d. Prod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   12632256
            Columns(1).Width=   1561
            Columns(1).Caption=   "Ubicaci�n"
            Columns(1).Name =   "Ubicaci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   12632256
            Columns(2).Width=   1138
            Columns(2).Caption=   "C�d.Int."
            Columns(2).Name =   "C�d.Int."
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(2).HasBackColor=   -1  'True
            Columns(2).BackColor=   12632256
            Columns(3).Width=   1561
            Columns(3).Caption=   "Referencia"
            Columns(3).Name =   "Referencia"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(3).HasBackColor=   -1  'True
            Columns(3).BackColor=   12632256
            Columns(4).Width=   3519
            Columns(4).Caption=   "Producto"
            Columns(4).Name =   "Producto"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Locked=   -1  'True
            Columns(4).HasBackColor=   -1  'True
            Columns(4).BackColor=   12632256
            Columns(5).Width=   741
            Columns(5).Caption=   "FF"
            Columns(5).Name =   "FF"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Locked=   -1  'True
            Columns(5).HasBackColor=   -1  'True
            Columns(5).BackColor=   12632256
            Columns(6).Width=   1349
            Columns(6).Caption=   "D�sis"
            Columns(6).Name =   "D�sis"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).Locked=   -1  'True
            Columns(6).HasBackColor=   -1  'True
            Columns(6).BackColor=   12632256
            Columns(7).Width=   847
            Columns(7).Caption=   "U.M."
            Columns(7).Name =   "U.M."
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(7).Locked=   -1  'True
            Columns(7).HasBackColor=   -1  'True
            Columns(7).BackColor=   12632256
            Columns(8).Width=   1482
            Columns(8).Caption=   "Tam Env"
            Columns(8).Name =   "Tam Env"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(8).Locked=   -1  'True
            Columns(8).HasBackColor=   -1  'True
            Columns(8).BackColor=   12632256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "Tratado"
            Columns(9).Name =   "Tratado"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(9).Locked=   -1  'True
            Columns(9).HasBackColor=   -1  'True
            Columns(9).BackColor=   12632256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "Prod.San."
            Columns(10).Name=   "Prod.San."
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(10).Locked=   -1  'True
            Columns(10).HasBackColor=   -1  'True
            Columns(10).BackColor=   12632256
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "Inf.I.V."
            Columns(11).Name=   "Inf.I.V."
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(11).Locked=   -1  'True
            Columns(11).HasBackColor=   -1  'True
            Columns(11).BackColor=   12632256
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "Pr�tesis"
            Columns(12).Name=   "Pr�tesis"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(12).Locked=   -1  'True
            Columns(12).HasBackColor=   -1  'True
            Columns(12).BackColor=   12632256
            Columns(13).Width=   1746
            Columns(13).Caption=   "Cant.Ped."
            Columns(13).Name=   "Cant.Ped."
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(13).Locked=   -1  'True
            Columns(13).HasBackColor=   -1  'True
            Columns(13).BackColor=   12632256
            Columns(14).Width=   1826
            Columns(14).Caption=   "Cant.Disp."
            Columns(14).Name=   "Cant.Disp."
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(14).HasBackColor=   -1  'True
            Columns(14).BackColor=   16776960
            Columns(15).Width=   1535
            Columns(15).Caption=   "Cant.Ori."
            Columns(15).Name=   "Cant.Ori."
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            Columns(15).Locked=   -1  'True
            Columns(15).HasBackColor=   -1  'True
            Columns(15).BackColor=   12632256
            _ExtentX        =   19897
            _ExtentY        =   10716
            _StockProps     =   79
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Necesidades Anestesia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   6480
         Index           =   0
         Left            =   -74880
         TabIndex        =   7
         Top             =   360
         Width           =   11535
         Begin SSDataWidgets_B.SSDBGrid auxDBGrid1 
            Height          =   6075
            Index           =   0
            Left            =   120
            TabIndex        =   8
            Top             =   360
            Width           =   11280
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   16
            stylesets.count =   4
            stylesets(0).Name=   "SolIV"
            stylesets(0).BackColor=   12632256
            stylesets(0).Picture=   "FR0158.frx":0108
            stylesets(1).Name=   "Medicamento"
            stylesets(1).BackColor=   16777215
            stylesets(1).Picture=   "FR0158.frx":0124
            stylesets(2).Name=   "Protesis"
            stylesets(2).BackColor=   9240575
            stylesets(2).Picture=   "FR0158.frx":0140
            stylesets(3).Name=   "ProdSani"
            stylesets(3).BackColor=   12615935
            stylesets(3).Picture=   "FR0158.frx":015C
            SelectTypeRow   =   3
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns.Count   =   16
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�d. Prod"
            Columns(0).Name =   "C�d. Prod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   12632256
            Columns(1).Width=   1482
            Columns(1).Caption=   "Ubicaci�n"
            Columns(1).Name =   "Ubicaci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   12632256
            Columns(2).Width=   1138
            Columns(2).Caption=   "C�d.Int."
            Columns(2).Name =   "C�d.Int."
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(2).HasBackColor=   -1  'True
            Columns(2).BackColor=   12632256
            Columns(3).Width=   1614
            Columns(3).Caption=   "Referencia"
            Columns(3).Name =   "Referencia"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(3).HasBackColor=   -1  'True
            Columns(3).BackColor=   12632256
            Columns(4).Width=   3519
            Columns(4).Caption=   "Producto"
            Columns(4).Name =   "Producto"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Locked=   -1  'True
            Columns(4).HasBackColor=   -1  'True
            Columns(4).BackColor=   12632256
            Columns(5).Width=   794
            Columns(5).Caption=   "FF"
            Columns(5).Name =   "FF"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Locked=   -1  'True
            Columns(5).HasBackColor=   -1  'True
            Columns(5).BackColor=   12632256
            Columns(6).Width=   1085
            Columns(6).Caption=   "D�sis"
            Columns(6).Name =   "D�sis"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).Locked=   -1  'True
            Columns(6).HasBackColor=   -1  'True
            Columns(6).BackColor=   12632256
            Columns(7).Width=   900
            Columns(7).Caption=   "U.M."
            Columns(7).Name =   "U.M."
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(7).Locked=   -1  'True
            Columns(7).HasBackColor=   -1  'True
            Columns(7).BackColor=   12632256
            Columns(8).Width=   1482
            Columns(8).Caption=   "Tam Env"
            Columns(8).Name =   "Tam Env"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(8).Locked=   -1  'True
            Columns(8).HasBackColor=   -1  'True
            Columns(8).BackColor=   12632256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "Tratado"
            Columns(9).Name =   "Tratado"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(9).Locked=   -1  'True
            Columns(9).HasBackColor=   -1  'True
            Columns(9).BackColor=   12632256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "Prod.San."
            Columns(10).Name=   "Prod.San."
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(10).Locked=   -1  'True
            Columns(10).HasBackColor=   -1  'True
            Columns(10).BackColor=   12632256
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "Inf.I.V."
            Columns(11).Name=   "Inf.I.V."
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(11).Locked=   -1  'True
            Columns(11).HasBackColor=   -1  'True
            Columns(11).BackColor=   12632256
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "Pr�tesis"
            Columns(12).Name=   "Pr�tesis"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(12).Locked=   -1  'True
            Columns(12).HasBackColor=   -1  'True
            Columns(12).BackColor=   12632256
            Columns(13).Width=   1693
            Columns(13).Caption=   "Cant.Ped."
            Columns(13).Name=   "Cant.Ped."
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(13).Locked=   -1  'True
            Columns(13).HasBackColor=   -1  'True
            Columns(13).BackColor=   12632256
            Columns(14).Width=   1693
            Columns(14).Caption=   "Cant.Disp."
            Columns(14).Name=   "Cant.Disp."
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(14).HasBackColor=   -1  'True
            Columns(14).BackColor=   16776960
            Columns(15).Width=   1693
            Columns(15).Caption=   "Cant.Ori."
            Columns(15).Name=   "Cant.Ori."
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            Columns(15).Locked=   -1  'True
            Columns(15).HasBackColor=   -1  'True
            Columns(15).BackColor=   12632256
            _ExtentX        =   19897
            _ExtentY        =   10716
            _StockProps     =   79
         End
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmVerTotQuiNoVal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FrmVerTotQuiNoVal (FR0158.FRM)                               *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Ver Pedido Planta no Validado                           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim blnInload As Boolean
Dim blnDispQui As Boolean
Dim blnDispAne As Boolean

Dim mblnGrabar As Boolean
Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strFR04 As String
  Dim qryFR04 As rdoQuery
  Dim rdoFR04 As rdoResultset
  Dim strAlmDes As String
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  
  'If gstrEst = "12" Then
  '  If SSTab1.Tab = "0" Then 'quirofano
  '    strWhere = "{FRK501J.FRK5INDQUIANE}=0 AND {FRK501J.FR26CODESTPETIC}=12 AND {FRK501J.FRK5INDTRATADO}=0"
  '  Else
  '    strWhere = "{FRK501J.FRK5INDQUIANE}=1 AND {FRK501J.FR26CODESTPETIC}=12 AND {FRK501J.FRK5INDTRATADO}=0"
  '  End If
  'ElseIf gstrEst = "9" Then
  '  If SSTab1.Tab = "0" Then 'quirofano
  '    strWhere = "{FRK501J.FRK5INDQUIANE}=0 AND {FRK501J.FR26CODESTPETIC}=9 AND {FRK501J.FRK5INDTRATADO}=1"
  '  Else
  '    strWhere = "{FRK501J.FRK5INDQUIANE}=1 AND {FRK501J.FR26CODESTPETIC}=9 AND {FRK501J.FRK5INDTRATADO}=1"
  '  End If
  'ElseIf gstrEst = "12,9" Then
  '  If SSTab1.Tab = "0" Then 'quirofano
  '    strWhere = "{FRK501J.FRK5INDQUIANE}=0 AND ({FRK501J.FR26CODESTPETIC}=12 OR {FRK501J.FR26CODESTPETIC}=9) AND ({FRK501J.FRK5INDTRATADO}=0 OR {FRK501J.FRK5INDTRATADO}=1)"
  '  Else
  '    strWhere = "{FRK501J.FRK5INDQUIANE}=1 AND ({FRK501J.FR26CODESTPETIC}=12 OR {FRK501J.FR26CODESTPETIC}=9) AND ({FRK501J.FRK5INDTRATADO}=0 OR {FRK501J.FRK5INDTRATADO}=1)"
  '  End If
  'Else
  '  Exit Sub
  'End If
             
  If gstrEst = "12" Then
    If SSTab1.Tab = "0" Then 'quirofano
      strWhere = "{FRK500.FRK5INDQUIANE}=0 AND {FRK300.FR26CODESTPETIC}=12 AND {FRK500.FRK5INDTRATADO}=0 AND {FRK500.FRK3CODNECESUNID}={FRK300.FRK3CODNECESUNID}"
    Else
      strWhere = "{FRK500.FRK5INDQUIANE}=1 AND {FRK300.FR26CODESTPETIC}=12 AND {FRK500.FRK5INDTRATADO}=0 AND {FRK500.FRK3CODNECESUNID}={FRK300.FRK3CODNECESUNID}"
    End If
  ElseIf gstrEst = "9" Then
    If SSTab1.Tab = "0" Then 'quirofano
      strWhere = "{FRK500.FRK5INDQUIANE}=0 AND {FRK300.FR26CODESTPETIC}=9 AND {FRK500.FRK5INDTRATADO}=1 AND {FRK500.FRK3CODNECESUNID}={FRK300.FRK3CODNECESUNID}"
    Else
      strWhere = "{FRK500.FRK5INDQUIANE}=1 AND {FRK300.FR26CODESTPETIC}=9 AND {FRK500.FRK5INDTRATADO}=1 AND {FRK500.FRK3CODNECESUNID}={FRK300.FRK3CODNECESUNID}"
    End If
  ElseIf gstrEst = "12,9" Then
    If SSTab1.Tab = "0" Then 'quirofano
      strWhere = "{FRK500.FRK5INDQUIANE}=0 AND ({FRK300.FR26CODESTPETIC}=12 OR {FRK300.FR26CODESTPETIC}=9) AND ({FRK500.FRK5INDTRATADO}=0 OR {FRK500.FRK5INDTRATADO}=1) AND {FRK500.FRK3CODNECESUNID}={FRK300.FRK3CODNECESUNID}"
    Else
      strWhere = "{FRK500.FRK5INDQUIANE}=1 AND ({FRK300.FR26CODESTPETIC}=12 OR {FRK300.FR26CODESTPETIC}=9) AND ({FRK500.FRK5INDTRATADO}=0 OR {FRK500.FRK5INDTRATADO}=1) AND {FRK500.FRK3CODNECESUNID}={FRK300.FRK3CODNECESUNID}"
    End If
  Else
    Exit Sub
  End If
             
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp3
  CrystalReport1.Action = 1

Err_imp3:

End Sub

Private Sub auxDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim j As Integer

    If auxDBGrid1(Index).Columns("Inf.I.V.").Value = "-1" Then
      For j = 0 To 15
        If j <> 14 Then
          auxDBGrid1(Index).Columns(j).CellStyleSet "SolIV"
        End If
      Next j
    ElseIf auxDBGrid1(Index).Columns("Pr�tesis").Value = "-1" Then
      For j = 0 To 15
        If j <> 14 Then
          auxDBGrid1(Index).Columns(j).CellStyleSet "Protesis"
        End If
      Next j
    ElseIf auxDBGrid1(Index).Columns("Prod.San.").Value = "-1" Then
      For j = 0 To 15
        If j <> 14 Then
          auxDBGrid1(Index).Columns(j).CellStyleSet "ProdSani"
        End If
      Next j
    Else
      For j = 0 To 15
        If j <> 14 Then
          auxDBGrid1(Index).Columns(j).CellStyleSet "Medicamento"
        End If
      Next j
    End If

End Sub

Private Sub Form_Activate()

  If blnInload Then
    Me.MousePointer = vbHourglass
    Me.Enabled = False
    If gstrQA = "0" Then
      SSTab1.TabEnabled(1) = False
      SSTab1.Tab = 0
    Else
      SSTab1.TabEnabled(0) = False
      SSTab1.Tab = 1
    End If
    Call Actualizar_Grid
    blnInload = False
    Me.MousePointer = vbDefault
    Me.Enabled = True
  End If

End Sub


Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  
  mblnGrabar = True
  blnCancel = True
 
End Sub


Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cmdbloquear_Click()
Dim qryupdate As rdoQuery
Dim strupdate As String
Dim nTotalSelRows, i, intMsg, intGrid As Integer
Dim bkmrk As Variant
    
  If gstrEst = "5" Then
    Exit Sub
  End If
    
  intMsg = MsgBox("�Est� seguro que NO desea DISPENSAR estos productos?", vbQuestion + vbYesNo)
  If intMsg = vbNo Then
    Exit Sub
  End If
  
  If SSTab1.Tab = 0 Then
    intGrid = 1
  Else
    intGrid = 0
  End If
    
  cmdBloquear.Enabled = False
  Screen.MousePointer = vbHourglass
  Me.Enabled = False
    nTotalSelRows = auxDBGrid1(intGrid).SelBookmarks.Count
    If nTotalSelRows > 0 Then
      For i = 0 To nTotalSelRows - 1
        bkmrk = auxDBGrid1(intGrid).SelBookmarks(i)
        If SSTab1.Tab = 0 Then
          strupdate = "UPDATE FRK500 SET FRK5INDBLOQ=? WHERE FR73CODPRODUCTO=? "
          If gstrEst = "12" Then
            strupdate = strupdate & " AND FRK5INDTRATADO=0 "
          ElseIf gstrEst = "9" Then
            strupdate = strupdate & " AND FRK5INDTRATADO=1 "
          Else
            strupdate = strupdate & " AND FRK5INDTRATADO IN (0,1) "
          End If
          strupdate = strupdate & " AND FRK5INDBLOQ<>? and FRK5INDQUIANE=?"
          Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
          qryupdate(0) = -1
          qryupdate(1) = auxDBGrid1(intGrid).Columns("C�d. Prod").CellValue(bkmrk)
          qryupdate(2) = -1
          qryupdate(3) = 0
          qryupdate.Execute
        Else
          strupdate = "UPDATE FRK500 SET FRK5INDBLOQ=? WHERE FR73CODPRODUCTO=? "
          If gstrEst = "12" Then
            strupdate = strupdate & " AND FRK5INDTRATADO=0 "
          ElseIf gstrEst = "9" Then
            strupdate = strupdate & " AND FRK5INDTRATADO=1 "
          Else
            strupdate = strupdate & " AND FRK5INDTRATADO IN (0,1) "
          End If
          strupdate = strupdate & " AND FRK5INDBLOQ<>? and FRK5INDQUIANE=?"
          Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
          qryupdate(0) = -1
          qryupdate(1) = auxDBGrid1(intGrid).Columns("C�d. Prod").CellValue(bkmrk)
          qryupdate(2) = -1
          qryupdate(3) = 1
          qryupdate.Execute
        End If
      Next i
      
      Call Actualizar_Peticiones
      Call Actualizar_Grid
      
      Screen.MousePointer = vbDefault
      MsgBox "Terminado", vbInformation, "Aviso"
    Else
      Screen.MousePointer = vbDefault
      MsgBox "Debe seleccionar los productos que no desea dispensar", vbInformation, "Aviso"
    End If
  
  Me.Enabled = True
  Screen.MousePointer = vbDefault
  cmdBloquear.Enabled = True

End Sub


Private Sub cmdvalidar_Click()

cmdValidar.Enabled = False
Me.Enabled = False
Screen.MousePointer = vbHourglass

  objCW.SetClock (60 * 4)
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False

If SSTab1.Tab = 0 Then
  If blnDispQui = False Then
      cmdValidar.Enabled = False
      'Call Dispensar
      Call DispAcumulado
      cmdValidar.Enabled = True
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  Else
    Screen.MousePointer = vbDefault
    MsgBox "Ya ha dispensado Quir�fano.", vbInformation, "Aviso"
  End If
Else
  If blnDispAne = False Then
      cmdValidar.Enabled = False
      'Call Dispensar
      Call DispAcumulado
      cmdValidar.Enabled = True
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.DataRefresh
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  Else
    Screen.MousePointer = vbDefault
    MsgBox "Ya ha dispensado Anestesia.", vbInformation, "Aviso"
  End If
End If

Screen.MousePointer = vbDefault
cmdValidar.Enabled = True
Me.Enabled = True
cmdBloquear.Enabled = False
MsgBox "Necesidades Dispensadas.", vbInformation, "Aviso"

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim objMasterInfo As New clsCWForm
Dim strKey As String
Dim i As Integer

  objCW.SetClock (60 * 4)
  objCW.SetClockEnable False
  objCW.blnAutoDisconnect = False

blnInload = True
blnDispQui = False
blnDispAne = False

Set objWinInfo = New clsCWWin
Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                            Me, tlbToolbar1, stbStatusBar1, _
                            cwWithAll)

  With objMasterInfo
    .strName = "Detalle Necesidad Quirofano"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    .intFormModel = cwWithGrid + cwWithTab + cwWithKeys
    .strTable = "AG1100"

    .intCursorSize = 0
    .intAllowance = cwAllowReadOnly
    .strWhere = "AG14CODTIPRECU=33"
    
    If gstrQA = "0" Then
      Call .objPrinter.Add("FR1583", "Prep.Qui.Soluciones I.V.")
      Call .objPrinter.Add("FR1587", "Prep.Qui.Pr�tesis")
      Call .objPrinter.Add("FR1582", "Prep.Qui.Prod.Sanitarios")
      Call .objPrinter.Add("FR1581", "Prep.Qui.Medicamentos")
      
      Call .objPrinter.Add("FR1586", "Repo.Qui.Soluciones I.V.")
      Call .objPrinter.Add("FR1588", "Repo.Qui.Pr�tesis")
      Call .objPrinter.Add("FR1585", "Repo.Qui.Prod.Sanitarios")
      Call .objPrinter.Add("FR1584", "Repo.Qui.Medicamentos")
    Else
      Call .objPrinter.Add("FRR183", "Prep.Ane.Soluciones I.V.")
      Call .objPrinter.Add("FRR187", "Prep.Ane.Pr�tesis")
      Call .objPrinter.Add("FRR182", "Prep.Ane.Prod.Sanitarios")
      Call .objPrinter.Add("FRR181", "Prep.Ane.Medicamentos")
      
      Call .objPrinter.Add("FRR186", "Repo.Ane.Soluciones I.V.")
      Call .objPrinter.Add("FRR188", "Repo.Ane.Pr�tesis")
      Call .objPrinter.Add("FRR185", "Repo.Ane.Prod.Sanitarios")
      Call .objPrinter.Add("FRR184", "Repo.Ane.Medicamentos")
    End If

    Call .FormAddOrderField("AG11DESRECURSO", cwDescending)
    strKey = .strDataBase & .strTable
  End With

    'With objMultiInfo2
    '    .strName = "Detalle Necesidad Anestesia"
    '    Set .objFormContainer = fraFrame1(0)
    '    Set .objFatherContainer = Nothing
    '    Set .tabMainTab = Nothing
    '    Set .grdGrid = grdDBGrid1(0)
    '    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    '    .strTable = "FRK502J"
    '    .intCursorSize = 0
    '    Select Case gstrEst
    '    Case "3" 'Enviada
    '      .intAllowance = cwAllowAll
    '      .strWhere = "FR26CODESTPETIC=3 AND NVL(FRK5INDTRATADO,0)=0 "
    '    Case "5" 'Dispensada Total
    '      .intAllowance = cwAllowReadOnly
    '     .strWhere = "FR26CODESTPETIC=5 AND FRK5INDTRATADO=2 "
    '   Case "8" 'Anulada
    '     .intAllowance = cwAllowReadOnly
    '    Case "9" 'Dispensada Parcial
    '      .intAllowance = cwAllowAll
    '      .strWhere = "FR26CODESTPETIC=9 AND FRK5INDTRATADO=1 "
    '    Case "3,9" 'Dispensada Parcial
    '      .intAllowance = cwAllowAll
    '      .strWhere = "FR26CODESTPETIC IN (3,9) AND (FRK5INDTRATADO=0 OR  FRK5INDTRATADO=1) "
    '    Case Else
    '      .intAllowance = cwAllowReadOnly
    '    End Select
    '    If .strWhere <> "" Then
    '      .strWhere = .strWhere & " AND FRK5INDQUIANE=1" 'anestesia
    '    Else
    '      .strWhere = .strWhere & "FRK5INDQUIANE=1" 'anestesia
    '    End If
    '    Call .objPrinter.Add("FRR181", "Prep.Ane.Medicamentos")
    '    Call .objPrinter.Add("FRR182", "Prep.Ane.Prod.Sanitarios")
    '    Call .objPrinter.Add("FRR183", "Prep.Ane.Soluciones I.V.")
    '    Call .objPrinter.Add("FRR187", "Prep.Ane.Pr�tesis")
    '    Call .objPrinter.Add("FRR184", "Repo.Ane.Medicamentos")
    '    Call .objPrinter.Add("FRR185", "Repo.Ane.Prod.Sanitarios")
    '    Call .objPrinter.Add("FRR186", "Repo.Ane.Soluciones I.V.")
    '    Call .objPrinter.Add("FRR188", "Repo.Ane.Pr�tesis")
    '
    '    Call .FormAddOrderField("nvl(FR73INDINFIV,0)", cwDescending)
    '    Call .FormAddOrderField("nvl(FR73INDMATIMP,0)", cwDescending)
    '    Call .FormAddOrderField("nvl(FR73INDPRODSAN,0)", cwDescending)
    '    Call .FormAddOrderField("FRH9UBICACION", cwAscending)
    '    Call .FormAddOrderField("FR73DESPRODUCTO", cwAscending)
    '    strKey = .strDataBase & .strTable
    'End With

    With objWinInfo
        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        
        'Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
        'Call .GridAddColumn(objMultiInfo, "C�d. Prod", "FR73CODPRODUCTO", cwNumeric, 9)
        'Call .GridAddColumn(objMultiInfo, "Ubicaci�n", "FRH9UBICACION", cwString, 10)
        'Call .GridAddColumn(objMultiInfo, "C�d.Int.", "FR73CODINTFAR", cwString, 6)
        'Call .GridAddColumn(objMultiInfo, "Referencia", "FR73REFERENCIA", cwString, 15)
        'Call .GridAddColumn(objMultiInfo, "Producto", "FR73DESPRODUCTO", cwNumeric, 7)
        'Call .GridAddColumn(objMultiInfo, "FF", "FRH7CODFORMFAR", cwString, 5)
        'Call .GridAddColumn(objMultiInfo, "D�sis", "FR73DOSIS", cwDecimal, 9)
        'Call .GridAddColumn(objMultiInfo, "U.M.", "FR93CODUNIMEDIDA", cwString, 5)
        'Call .GridAddColumn(objMultiInfo, "Tam Env", "FR73TAMENVASE", cwDecimal, 10)
        'Call .GridAddColumn(objMultiInfo, "Tratado", "FRK5INDTRATADO", cwBoolean)
        'Call .GridAddColumn(objMultiInfo, "Prod.San.", "FR73INDPRODSAN", cwBoolean)
        'Call .GridAddColumn(objMultiInfo, "Inf.I.V.", "FR73INDINFIV", cwBoolean)
        'Call .GridAddColumn(objMultiInfo, "Pr�tesis", "FR73INDMATIMP", cwBoolean)
        'Call .GridAddColumn(objMultiInfo, "Cantidad", "CANTNECESQUIR", cwNumeric, 11)
        'Call .GridAddColumn(objMultiInfo, "Cantidad Disp", "CANTSUMIFARM", cwNumeric, 11)
        'Call .GridAddColumn(objMultiInfo, "Cant.Disp.", "")
        'Call .GridAddColumn(objMultiInfo, "Cant.Nec.", "")

        Call .FormCreateInfo(objMasterInfo)
        'Call .FormCreateInfo(objMultiInfo2)

        ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        '.CtrlGetInfo(grdDBGrid1(1).Columns(4)).intKeyNo = 1
        'Se indica que campos son obligatorios y cuales son clave primaria

        'Call .FormChangeColor(objMultiInfo)
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(12)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(13)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(14)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(15)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(16)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnReadOnly = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(18)).blnNegotiated = False
        '.CtrlGetInfo(grdDBGrid1(1).Columns(18)).blnMandatory = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(19)).blnReadOnly = True
        
        Call .WinRegister
        Call .WinStabilize
    End With

    'grdDBGrid1(1).Columns("C�d. Prod").Visible = False
    'grdDBGrid1(1).Columns("Ubicaci�n").Width = 850
    'grdDBGrid1(1).Columns("C�d.Int.").Width = 800
    'grdDBGrid1(1).Columns("Referencia").Width = 850
    'grdDBGrid1(1).Columns("Producto").Width = 2000
    'grdDBGrid1(1).Columns("FF").Width = 850
    'grdDBGrid1(1).Columns("D�sis").Width = 850
    'grdDBGrid1(1).Columns("U.M.").Width = 850
    'grdDBGrid1(1).Columns("Tam Env").Width = 850
    'grdDBGrid1(1).Columns("Tratado").Visible = False
    'grdDBGrid1(1).Columns("Prod.San.").Visible = False
    'grdDBGrid1(1).Columns("Inf.I.V.").Visible = False
    'grdDBGrid1(1).Columns("Pr�tesis").Visible = False
    'grdDBGrid1(1).Columns("Cantidad").Visible = False
    'grdDBGrid1(1).Columns("Cantidad Disp").Visible = False

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

Private Function FillStr(ByVal strBuffer As String)
  FillStr = strBuffer & String(128 - Len(strBuffer), vbNullChar)
End Function



Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
Dim strWhere As String
Dim strFilter As String

  If gstrQA = "0" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Select Case intReport
      Case 1
        Call Imprimir("FR1583.RPT", 0)
      Case 2
        Call Imprimir("FR1587.RPT", 0)
      Case 3
        Call Imprimir("FR1582.RPT", 0)
      Case 4
        Call Imprimir("FR1581.RPT", 0)
      Case 5
        Call Imprimir("FR1586.RPT", 0)
      Case 6
        Call Imprimir("FR1588.RPT", 0)
      Case 7
        Call Imprimir("FR1585.RPT", 0)
      Case 8
        Call Imprimir("FR1584.RPT", 0)
      End Select
    End If
    Set objPrinter = Nothing
  Else
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Select Case intReport
      Case 1
        Call Imprimir("FRR183.RPT", 0)
      Case 2
        Call Imprimir("FRR187.RPT", 0)
      Case 3
        Call Imprimir("FRR182.RPT", 0)
      Case 4
        Call Imprimir("FRR181.RPT", 0)
      Case 5
        Call Imprimir("FRR186.RPT", 0)
      Case 6
        Call Imprimir("FRR188.RPT", 0)
      Case 7
        Call Imprimir("FRR185.RPT", 0)
      Case 8
        Call Imprimir("FRR184.RPT", 0)
      End Select
    End If
    Set objPrinter = Nothing
  End If

End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  
  If btnButton.Index = 30 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  End If

  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  
End Sub


Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 40 'Guardar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  If intIndex = 10 Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
  Else
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  End If
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub


Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub



Private Sub Actualizar_Peticiones()
Dim strSelect As String
Dim qrySelect As rdoQuery
Dim auxcodproducto As Currency
Dim rdoSelect As rdoResultset
Dim sqlstr As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim strupdate As String
Dim auxContQui As Currency
Dim auxContAne As Currency
  

  If SSTab1.Tab = 0 Then
      If gstrEst = "12" Then
        strSelect = "SELECT * FROM FRK300 WHERE FR26CODESTPETIC=12"
        strSelect = strSelect & " AND FRK3INDQUIANE=0"
        Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
        Set rdoSelect = qrySelect.OpenResultset(strSelect)
      ElseIf gstrEst = "9" Then
        strSelect = "SELECT * FROM FRK300 WHERE FR26CODESTPETIC=9"
        strSelect = strSelect & " AND FRK3INDQUIANE=0"
        Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
        Set rdoSelect = qrySelect.OpenResultset(strSelect)
      ElseIf gstrEst = "12,9" Then
        strSelect = "SELECT * FROM FRK300 WHERE FR26CODESTPETIC IN (12,9)"
        strSelect = strSelect & " AND FRK3INDQUIANE=0"
        Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
        Set rdoSelect = qrySelect.OpenResultset(strSelect)
      End If
      While Not rdoSelect.EOF
        sqlstr = "SELECT count(*) from FRK500 where " & _
                 " FRK3CODNECESUNID=? and FRK5INDBLOQ=0 " & _
                 " AND FRK5INDQUIANE=0 and FRK5INDTRATADO<2"
        Set qrya = objApp.rdoConnect.CreateQuery("", sqlstr)
        qrya(0) = rdoSelect("FRK3CODNECESUNID").Value
        Set rsta = qrya.OpenResultset(sqlstr)
        auxContQui = rsta(0).Value
        rsta.Close
        Set rsta = Nothing
        If auxContQui > 0 Then
          strupdate = "UPDATE FRK300 SET FR26CODESTPETIC=9 WHERE "
          strupdate = strupdate & " FRK3CODNECESUNID=" & rdoSelect("FRK3CODNECESUNID").Value
          objApp.rdoConnect.Execute strupdate, 64
        Else
          strupdate = "UPDATE FRK300 SET FR26CODESTPETIC=5 WHERE "
          strupdate = strupdate & " FRK3CODNECESUNID=" & rdoSelect("FRK3CODNECESUNID").Value
          objApp.rdoConnect.Execute strupdate, 64
        End If
        rdoSelect.MoveNext
      Wend
      rdoSelect.Close
      Set rdoSelect = Nothing
  Else
      If gstrEst = "12" Then
        strSelect = "SELECT * FROM FRK300 WHERE FR26CODESTPETIC=12"
        strSelect = strSelect & " AND FRK3INDQUIANE=-1"
        Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
        Set rdoSelect = qrySelect.OpenResultset(strSelect)
      ElseIf gstrEst = "9" Then
        strSelect = "SELECT * FROM FRK300 WHERE FR26CODESTPETIC=9"
        strSelect = strSelect & " AND FRK3INDQUIANE=-1"
        Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
        Set rdoSelect = qrySelect.OpenResultset(strSelect)
      Else
        strSelect = "SELECT * FROM FRK300 WHERE FR26CODESTPETIC IN (12,9)"
        strSelect = strSelect & " AND FRK3INDQUIANE=-1"
        Set qrySelect = objApp.rdoConnect.CreateQuery("", strSelect)
        Set rdoSelect = qrySelect.OpenResultset(strSelect)
      End If
      While Not rdoSelect.EOF
        sqlstr = "SELECT count(*) from FRK500 where " & _
                 " FRK3CODNECESUNID=? and FRK5INDBLOQ=0 " & _
                 " AND FRK5INDQUIANE=1 and FRK5INDTRATADO<2"
        Set qrya = objApp.rdoConnect.CreateQuery("", sqlstr)
        qrya(0) = rdoSelect("FRK3CODNECESUNID").Value
        Set rsta = qrya.OpenResultset(sqlstr)
        auxContAne = rsta(0).Value
        rsta.Close
        Set rsta = Nothing
        If auxContAne > 0 Then
          strupdate = "UPDATE FRK300 SET FR26CODESTPETIC=9 WHERE "
          strupdate = strupdate & " FRK3CODNECESUNID=" & rdoSelect("FRK3CODNECESUNID").Value
          objApp.rdoConnect.Execute strupdate, 64
        Else
          strupdate = "UPDATE FRK300 SET FR26CODESTPETIC=5 WHERE "
          strupdate = strupdate & " FRK3CODNECESUNID=" & rdoSelect("FRK3CODNECESUNID").Value
          objApp.rdoConnect.Execute strupdate, 64
        End If
        rdoSelect.MoveNext
      Wend
      rdoSelect.Close
      Set rdoSelect = Nothing
  End If

End Sub

Private Sub Actualizar_Grid()
Dim strSelect As String
Dim qrya As rdoQuery
Dim rsta As rdoResultset
Dim mvntAcumulado As Variant
Dim mvntAcumuladoDisp As Variant
Dim mvntAcumuladoOri As Variant
Dim mvntProducto As Variant
Dim i As Integer
Dim strlinea As String
Dim IntGridAct As Integer

If SSTab1.Tab = 0 Then
  IntGridAct = 1
Else
  IntGridAct = 0
End If

Me.Enabled = False
Screen.MousePointer = vbHourglass
  
strSelect = "SELECT FRK300.FRK3CODNECESUNID"
strSelect = strSelect & ",FRK300.FR97CODQUIROFANO"
strSelect = strSelect & ",FRK300.FR26CODESTPETIC"
strSelect = strSelect & ",FRK300.FRK3INDQUIANE"

strSelect = strSelect & ",FRK500.FRK4NUMLINEA"
'strSelect = strSelect & ",FRK500.FRK3CODNECESUNID"
strSelect = strSelect & ",FRK500.FRK5CODNECESVAL"
strSelect = strSelect & ",FRK500.FR73CODPRODUCTO"
'strSelect = strSelect & ",FRK500.FR93CODUNIMEDIDA"
strSelect = strSelect & ",FRK500.FRK5CANTNECESQUIR"
'strSelect = strSelect & ",FRK500.SG02COD_VAL"
'strSelect = strSelect & ",FRK500.FRK5FECVALIDACION"
'strSelect = strSelect & ",FRK500.FR93CODUNIMEDIDA_SUM"
strSelect = strSelect & ",FRK500.FRK5CANTSUMIFARM"
strSelect = strSelect & ",FRK500.FRK5INDQUIANE"
strSelect = strSelect & ",FRK500.FRK5INDFAC"
strSelect = strSelect & ",FRK500.FRK5INDBLOQ"
strSelect = strSelect & ",FRK500.FRK5INDTRATADO"
strSelect = strSelect & ",FRK500.FRK5CANTULTFARM"

strSelect = strSelect & ",FR7300.FRH9UBICACION"
strSelect = strSelect & ",FR7300.FR73CODINTFAR"
strSelect = strSelect & ",FR7300.FR73REFERENCIA"
strSelect = strSelect & ",FR7300.FR73DESPRODUCTO"
strSelect = strSelect & ",FR7300.FR73DOSIS"
strSelect = strSelect & ",FR7300.FRH7CODFORMFAR"
strSelect = strSelect & ",FR7300.FR93CODUNIMEDIDA"
strSelect = strSelect & ",FR7300.FR73TAMENVASE"
strSelect = strSelect & ",FR7300.FR73INDPRODSAN"
strSelect = strSelect & ",FR7300.FR73INDINFIV"
strSelect = strSelect & ",FR7300.FR73INDMATIMP"

strSelect = strSelect & " FROM FRK300,FRK500,FR7300"
strSelect = strSelect & " WHERE FRK300.FRK3CODNECESUNID=FRK500.FRK3CODNECESUNID"
strSelect = strSelect & " AND FRK500.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"

Select Case gstrEst
Case "12" 'Pendiente
  strSelect = strSelect & " AND FRK300.FR26CODESTPETIC=12 AND (FRK500.FRK5INDTRATADO=0 OR FRK500.FRK5INDTRATADO IS NULL) "
  strSelect = strSelect & " AND FRK500.FRK5INDBLOQ=0"
Case "5" 'Dispensada Total
  strSelect = strSelect & " AND FRK300.FR26CODESTPETIC=5 AND FRK500.FRK5INDTRATADO=2 "
  strSelect = strSelect & " AND FRK500.FRK5INDBLOQ=0"
Case "8" 'Anulada
  strSelect = strSelect & " AND FRK500.FRK5INDBLOQ=-1"
Case "9" 'Dispensada Parcial
  strSelect = strSelect & " AND FRK300.FR26CODESTPETIC=9 AND (FRK500.FRK5INDTRATADO=0 OR FRK500.FRK5INDTRATADO=1) "
  strSelect = strSelect & " AND FRK500.FRK5INDBLOQ=0"
Case "12,9" 'Dispensada Parcial
  strSelect = strSelect & " AND FRK300.FR26CODESTPETIC IN (12,9) AND (FRK500.FRK5INDTRATADO=0 OR FRK500.FRK5INDTRATADO=1) "
  strSelect = strSelect & " AND FRK500.FRK5INDBLOQ=0"
Case Else
End Select

If gstrQA = "0" Then
  strSelect = strSelect & " AND FRK5INDQUIANE=0" 'Quirofano
  strSelect = strSelect & " AND FRK3INDQUIANE=0" 'Quirofano
Else
  strSelect = strSelect & " AND FRK5INDQUIANE=1" 'Anestesia
  strSelect = strSelect & " AND FRK3INDQUIANE=-1" 'Anestesia
End If

strSelect = strSelect & " ORDER BY NVL(FR7300.FR73INDINFIV,0)"
strSelect = strSelect & " ,NVL(FR7300.FR73INDMATIMP,0)"
strSelect = strSelect & " ,NVL(FR7300.FR73INDPRODSAN,0)"
strSelect = strSelect & " ,FR7300.FRH9UBICACION"
strSelect = strSelect & " ,FR7300.FR73DESPRODUCTO"
strSelect = strSelect & " ,FR7300.FR73CODPRODUCTO"

Set qrya = objApp.rdoConnect.CreateQuery("", strSelect)
Set rsta = qrya.OpenResultset()
'grdAux(1).Redraw = False
grdAux(1).RemoveAll
While Not rsta.EOF
  strlinea = rsta("FRK3CODNECESUNID").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR97CODQUIROFANO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR26CODESTPETIC").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK3INDQUIANE").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK4NUMLINEA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK5CODNECESVAL").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK5CANTNECESQUIR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK5CANTSUMIFARM").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK5INDQUIANE").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK5INDFAC").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK5INDBLOQ").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK5INDTRATADO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRK5CANTULTFARM").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRH9UBICACION").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73CODINTFAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73REFERENCIA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73DESPRODUCTO").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73DOSIS").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FRH7CODFORMFAR").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73TAMENVASE").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73INDPRODSAN").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73INDINFIV").Value & Chr(vbKeyTab)
  strlinea = strlinea & rsta("FR73INDMATIMP").Value
  strlinea = strlinea & "" & Chr(vbKeyTab)
  strlinea = strlinea & "" & Chr(vbKeyTab)
  strlinea = strlinea & "" & Chr(vbKeyTab)
  grdAux(1).AddItem strlinea
  rsta.MoveNext
Wend
qrya.Close
Set qrya = Nothing
'grdAux(1).Update
'grdAux(1).Redraw = True


  auxDBGrid1(IntGridAct).RemoveAll
  auxDBGrid1(IntGridAct).Redraw = False
  If grdAux(1).Rows > 0 Then
    mvntAcumulado = 0
    mvntAcumuladoDisp = 0
    mvntAcumuladoOri = 0
    'grdAux(1).Redraw = False
    grdAux(1).MoveFirst
    For i = 0 To grdAux(1).Rows - 1
      If grdAux(1).Columns("frk5cantnecesquir").Value = "" Then 'cant.ped.
        grdAux(1).Columns("frk5cantnecesquir").Value = 0
      End If
      If grdAux(1).Columns("frk5cantsumifarm").Value = "" Then 'cant.disp.
        grdAux(1).Columns("frk5cantsumifarm").Value = 0
      End If
      If grdAux(1).Columns("frk5cantultfarm").Value = "" Then 'cant.disp.
        grdAux(1).Columns("frk5cantultfarm").Value = 0
      End If
      mvntAcumulado = mvntAcumulado + grdAux(1).Columns("frk5cantsumifarm").Value
      mvntAcumuladoDisp = mvntAcumuladoDisp + grdAux(1).Columns("frk5cantultfarm").Value
      mvntAcumuladoOri = mvntAcumuladoOri + grdAux(1).Columns("frk5cantnecesquir").Value
      mvntProducto = grdAux(1).Columns("fr73codproducto").Value 'Cod.Prod.
      grdAux(1).MoveNext
      If mvntProducto <> grdAux(1).Columns("fr73codproducto").Value Then
        grdAux(1).MovePrevious
        grdAux(1).Columns("Ac.Ped.").Value = mvntAcumulado 'Ac.Ped.
        grdAux(1).Columns("Ac.Disp.").Value = mvntAcumuladoDisp 'Ac.Disp.
        grdAux(1).Columns("Ac.Ori.").Value = mvntAcumuladoOri
        
        auxDBGrid1(IntGridAct).AddNew
        auxDBGrid1(IntGridAct).Columns("C�d. Prod").Value = grdAux(1).Columns("fr73codproducto").Value
        auxDBGrid1(IntGridAct).Columns("Ubicaci�n").Value = grdAux(1).Columns("frh9ubicacion").Value
        auxDBGrid1(IntGridAct).Columns("C�d.Int.").Value = grdAux(1).Columns("fr73codintfar").Value
        auxDBGrid1(IntGridAct).Columns("Referencia").Value = grdAux(1).Columns("fr73referencia").Value
        auxDBGrid1(IntGridAct).Columns("Producto").Value = grdAux(1).Columns("fr73desproducto").Value
        auxDBGrid1(IntGridAct).Columns("FF").Value = grdAux(1).Columns("frh7codformfar").Value
        auxDBGrid1(IntGridAct).Columns("D�sis").Value = grdAux(1).Columns("fr73dosis").Value
        auxDBGrid1(IntGridAct).Columns("U.M.").Value = grdAux(1).Columns("fr93codunimedida").Value
        auxDBGrid1(IntGridAct).Columns("Tam Env").Value = grdAux(1).Columns("fr73tamenvase").Value
        auxDBGrid1(IntGridAct).Columns("Tratado").Value = grdAux(1).Columns("frk5indtratado").Value
        auxDBGrid1(IntGridAct).Columns("Prod.San.").Value = grdAux(1).Columns("fr73indprodsan").Value
        auxDBGrid1(IntGridAct).Columns("Inf.I.V.").Value = grdAux(1).Columns("fr73indinfiv").Value
        auxDBGrid1(IntGridAct).Columns("Pr�tesis").Value = grdAux(1).Columns("fr73indmatimp").Value
        auxDBGrid1(IntGridAct).Columns("Cant.Ped.").Value = grdAux(1).Columns("Ac.Ped.").Value
        'auxDBGrid1(IntGridAct).Columns("Cant.Disp.").Value = grdAux(1).Columns("Ac.Disp.").Value
        If grdAux(1).Columns("fr26codestpetic").Value = 12 Then 'Pendientes
          auxDBGrid1(IntGridAct).Columns("Cant.Disp.").Value = grdAux(1).Columns("Ac.Ped.").Value
        Else  'estado=9 dispensadas parciales
          auxDBGrid1(IntGridAct).Columns("Cant.Disp.").Value = 0
        End If
        auxDBGrid1(IntGridAct).Columns("Cant.Ori.").Value = grdAux(1).Columns("Ac.Ori.").Value
        auxDBGrid1(IntGridAct).Update
        
        mvntAcumulado = 0
        mvntAcumuladoDisp = 0
        mvntAcumuladoOri = 0
        grdAux(1).MoveNext
      End If
    Next i
    
    grdAux(1).Columns("Ac.Ped.").Value = mvntAcumulado
    grdAux(1).Columns("Ac.Disp.").Value = mvntAcumuladoDisp
    grdAux(1).Columns("Ac.Ori.").Value = mvntAcumuladoOri
    
    auxDBGrid1(IntGridAct).AddNew
    auxDBGrid1(IntGridAct).Columns("C�d. Prod").Value = grdAux(1).Columns("fr73codproducto").Value
    auxDBGrid1(IntGridAct).Columns("Ubicaci�n").Value = grdAux(1).Columns("frh9ubicacion").Value
    auxDBGrid1(IntGridAct).Columns("C�d.Int.").Value = grdAux(1).Columns("fr73codintfar").Value
    auxDBGrid1(IntGridAct).Columns("Referencia").Value = grdAux(1).Columns("fr73referencia").Value
    auxDBGrid1(IntGridAct).Columns("Producto").Value = grdAux(1).Columns("fr73desproducto").Value
    auxDBGrid1(IntGridAct).Columns("FF").Value = grdAux(1).Columns("frh7codformfar").Value
    auxDBGrid1(IntGridAct).Columns("D�sis").Value = grdAux(1).Columns("fr73dosis").Value
    auxDBGrid1(IntGridAct).Columns("U.M.").Value = grdAux(1).Columns("fr93codunimedida").Value
    auxDBGrid1(IntGridAct).Columns("Tam Env").Value = grdAux(1).Columns("fr73tamenvase").Value
    auxDBGrid1(IntGridAct).Columns("Tratado").Value = grdAux(1).Columns("frk5indtratado").Value
    auxDBGrid1(IntGridAct).Columns("Prod.San.").Value = grdAux(1).Columns("fr73indprodsan").Value
    auxDBGrid1(IntGridAct).Columns("Inf.I.V.").Value = grdAux(1).Columns("fr73indinfiv").Value
    auxDBGrid1(IntGridAct).Columns("Pr�tesis").Value = grdAux(1).Columns("fr73indmatimp").Value
    auxDBGrid1(IntGridAct).Columns("Cant.Ped.").Value = grdAux(1).Columns("Ac.Ped.").Value
    'auxDBGrid1(IntGridAct).Columns("Cant.Disp.").Value = grdAux(1).Columns("Ac.Disp.").Value
    If grdAux(1).Columns("fr26codestpetic").Value = 12 Then 'Pendientes
      auxDBGrid1(IntGridAct).Columns("Cant.Disp.").Value = grdAux(1).Columns("Ac.Ped.").Value
    Else  'estado=9 dispensadas parciales
      auxDBGrid1(IntGridAct).Columns("Cant.Disp.").Value = 0
    End If
    auxDBGrid1(IntGridAct).Columns("Cant.Ori.").Value = grdAux(1).Columns("Ac.Ori.").Value
    auxDBGrid1(IntGridAct).Update
    
    grdAux(1).MoveFirst
    'grdAux(1).Redraw = True
  End If
  auxDBGrid1(IntGridAct).MoveFirst
  auxDBGrid1(IntGridAct).Redraw = True
  
  Screen.MousePointer = vbDefault
  Me.Enabled = True

End Sub

Private Sub DispAcumulado()
Dim i As Integer
Dim resto As Double
Dim codigo As Variant
Dim strupdate As String
Dim qryupdate As rdoQuery
Dim strAlmDes As String
Dim strFRH2 As String
Dim qryFRH2 As rdoQuery
Dim rdoFRH2 As rdoResultset
Dim strAlmFar As String
Dim strIns As String
Dim strIns80 As String
Dim strIns35 As String
Dim IntGridAct As Integer
Dim strQuiDispen As String
Dim strQui As String
Dim qryQui As rdoQuery
Dim rdoQui As rdoResultset
Dim auxcantmov As String
Dim strUpdCanti As String
Dim qryUpdCanti As rdoQuery
Dim strUpdTrat As String
Dim qryUpdTrat As rdoQuery

  If SSTab1.Tab = 0 Then
    IntGridAct = 1
  Else
    IntGridAct = 0
  End If

  If grdAux(1).Rows > 0 Then
    Me.Enabled = False
    Screen.MousePointer = vbHourglass
    
    auxDBGrid1(IntGridAct).Redraw = False
    auxDBGrid1(IntGridAct).MoveFirst
    'Datos correctos
    For i = 0 To auxDBGrid1(IntGridAct).Rows - 1
      If IsNumeric(auxDBGrid1(IntGridAct).Columns("Cant.Disp.").Value) = False Then
        Me.Enabled = True
        Screen.MousePointer = vbDefault
        auxDBGrid1(IntGridAct).Redraw = True
        Call MsgBox("Datos Incorrectos", vbInformation, "Aviso")
        auxDBGrid1(IntGridAct).Col = 10
        Exit Sub
      End If
      If CCur(auxDBGrid1(IntGridAct).Columns("Cant.Disp.").Value) > CCur(auxDBGrid1(IntGridAct).Columns("Cant.Ped.").Value) Then
        Me.Enabled = True
        Screen.MousePointer = vbDefault
        auxDBGrid1(IntGridAct).Redraw = True
        Call MsgBox("No se puede dispensar mas de lo que se ha pedido", vbInformation, "Aviso")
        auxDBGrid1(IntGridAct).Col = 10
        Exit Sub
      End If
      auxDBGrid1(IntGridAct).Columns("Cant.Disp.").Value = CCur(auxDBGrid1(IntGridAct).Columns("Cant.Disp.").Value)
      auxDBGrid1(IntGridAct).MoveNext
    Next i
    
    'Se obtiene el almac�n de quirofano
    strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
    Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
    qryFRH2(0) = 30
    Set rdoFRH2 = qryFRH2.OpenResultset()
    If IsNull(rdoFRH2.rdoColumns(0).Value) Then
      strQuiDispen = "999"
    Else
      strQuiDispen = rdoFRH2.rdoColumns(0).Value
    End If
    rdoFRH2.Close
    Set rdoFRH2 = Nothing
    qryFRH2.Close
    Set qryFRH2 = Nothing

    strQui = "SELECT FRH2PARAMGEN FROM FRH200 "
    strQui = strQui & " WHERE FRH2CODPARAMGEN=?"
    Set qryQui = objApp.rdoConnect.CreateQuery("", strQui)
    If SSTab1.Tab = 0 Then
      qryQui(0) = 31
    Else
      qryQui(0) = 32
    End If
    Set rdoQui = qryQui.OpenResultset()
    If SSTab1.Tab = 0 Then
      'strAlmDes = rdoQui("FR04CODALMACEN_QUI").Value
      If Not rdoQui.EOF Then
        strAlmDes = rdoQui("FRH2PARAMGEN").Value '213
      End If
    Else
      'strAlmDes = rdoQui("FR04CODALMACEN_ANE").Value
      If Not rdoQui.EOF Then
        strAlmDes = rdoQui("FRH2PARAMGEN").Value '223
      End If
    End If
    rdoQui.Close
    Set rdoQui = Nothing
    qryQui.Close
    Set qryQui = Nothing
             
    'Se obtiene el almac�n principal de Farmacia
    strFRH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
    Set qryFRH2 = objApp.rdoConnect.CreateQuery("", strFRH2)
    qryFRH2(0) = 28
    Set rdoFRH2 = qryFRH2.OpenResultset()
    If IsNull(rdoFRH2.rdoColumns(0).Value) Then
      strAlmFar = "0"
    Else
      strAlmFar = rdoFRH2.rdoColumns(0).Value
    End If
    qryFRH2.Close
    Set qryFRH2 = Nothing
    Set rdoFRH2 = Nothing
    
    strupdate = "UPDATE FRK500 SET FRK5CANTULTFARM=TO_NUMBER(?) WHERE FRK5CODNECESVAL=? AND FRK4NUMLINEA=?"
    Set qryupdate = objApp.rdoConnect.CreateQuery("", strupdate)
    grdAux(1).MoveLast
    auxDBGrid1(IntGridAct).MoveLast
    codigo = 0
    resto = 0
    For i = 0 To grdAux(1).Rows - 1
      If codigo <> grdAux(1).Columns("fr73codproducto").Value Then
        grdAux(1).Columns("Ac.Disp.").Value = auxDBGrid1(IntGridAct).Columns("Cant.Disp.").Value
        grdAux(1).Columns("frk5cantultfarm").Value = grdAux(1).Columns("Ac.Disp.").Value
        codigo = grdAux(1).Columns("fr73codproducto").Value
        If CDec(grdAux(1).Columns("frk5cantultfarm").Value) > CDec(grdAux(1).Columns("frk5cantsumifarm").Value) Then
          resto = grdAux(1).Columns("frk5cantultfarm").Value - grdAux(1).Columns("frk5cantsumifarm").Value
          grdAux(1).Columns("frk5cantultfarm").Value = grdAux(1).Columns("frk5cantsumifarm").Value
        Else
          resto = 0
        End If
        qryupdate(0) = grdAux(1).Columns("frk5cantultfarm").Value
        qryupdate(1) = grdAux(1).Columns("frk5codnecesval").Value
        qryupdate(2) = grdAux(1).Columns("frk4numlinea").Value
        qryupdate.Execute
        
        If CCur(grdAux(1).Columns("frk5cantultfarm").Value) > 0 Then
          'RealizarApuntes
          auxcantmov = objGen.ReplaceStr(grdAux(1).Columns("frk5cantultfarm").Value, ",", ".", 1)
          'Se realiza el insert en FR8000: salidas de alamac�n
          strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,FR90CODTIPMOV,"
          strIns80 = strIns80 & "FR80FECMOVIMIENTO,FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES ("
          strIns80 = strIns80 & "FR80NUMMOV_SEQUENCE.nextval," & strAlmFar & "," & strAlmDes & ",6,SYSDATE,"
          strIns80 = strIns80 & grdAux(1).Columns("fr73codproducto").Value & "," & auxcantmov & ",1,'NE')"
          objApp.rdoConnect.Execute strIns80, 64
          'Se realiza el insert en FR3500: entradas de almac�n
          strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,"
          strIns35 = strIns35 & "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,"
          strIns35 = strIns35 & "FR93CODUNIMEDIDA) VALUES (" & "FR35CODMOVIMIENTO_SEQUENCE.nextval" & ","
          strIns35 = strIns35 & strAlmFar & "," & strAlmDes & ",6,SYSDATE," & grdAux(1).Columns("fr73codproducto").Value & "," & auxcantmov & ",1,'NE')"
          objApp.rdoConnect.Execute strIns35, 64
        End If
        auxDBGrid1(IntGridAct).MovePrevious
      Else
        grdAux(1).Columns("frk5cantultfarm").Value = resto
        If CDec(grdAux(1).Columns("frk5cantultfarm").Value) > CDec(grdAux(1).Columns("frk5cantsumifarm").Value) Then
          resto = grdAux(1).Columns("frk5cantultfarm").Value - grdAux(1).Columns("frk5cantsumifarm").Value
          grdAux(1).Columns("frk5cantultfarm").Value = grdAux(1).Columns("frk5cantsumifarm").Value
        Else
          resto = 0
        End If
        qryupdate(0) = grdAux(1).Columns("frk5cantultfarm").Value
        qryupdate(1) = grdAux(1).Columns("frk5codnecesval").Value
        qryupdate(2) = grdAux(1).Columns("frk4numlinea").Value
        qryupdate.Execute
      
        If CCur(grdAux(1).Columns("frk5cantultfarm").Value) > 0 Then
          'RealizarApuntes
          auxcantmov = objGen.ReplaceStr(grdAux(1).Columns("frk5cantultfarm").Value, ",", ".", 1)
          'Se realiza el insert en FR8000: salidas de alamac�n
          strIns80 = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,FR90CODTIPMOV,"
          strIns80 = strIns80 & "FR80FECMOVIMIENTO,FR73CODPRODUCTO,FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES ("
          strIns80 = strIns80 & "FR80NUMMOV_SEQUENCE.nextval," & strAlmFar & "," & strAlmDes & ",6,SYSDATE,"
          strIns80 = strIns80 & grdAux(1).Columns("fr73codproducto").Value & "," & auxcantmov & ",1,'NE')"
          objApp.rdoConnect.Execute strIns80, 64
          'Se realiza el insert en FR3500: entradas de almac�n
          strIns35 = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI,FR04CODALMACEN_DES,"
          strIns35 = strIns35 & "FR90CODTIPMOV,FR35FECMOVIMIENTO,FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,"
          strIns35 = strIns35 & "FR93CODUNIMEDIDA) VALUES (" & "FR35CODMOVIMIENTO_SEQUENCE.nextval" & ","
          strIns35 = strIns35 & strAlmFar & "," & strAlmDes & ",6,SYSDATE," & grdAux(1).Columns("fr73codproducto").Value & "," & auxcantmov & ",1,'NE')"
          objApp.rdoConnect.Execute strIns35, 64
        End If
      End If
      grdAux(1).MovePrevious
    Next i
    Me.Enabled = True
    Screen.MousePointer = vbDefault
    'grdAux(1).Redraw = True
    auxDBGrid1(IntGridAct).Redraw = True
  End If
    
  Me.Enabled = False
  Screen.MousePointer = vbHourglass
    
  If SSTab1.Tab = 0 Then
    Call Imprimir("FR1586.RPT", 1)
    Call Imprimir("FR1588.RPT", 1)
    Call Imprimir("FR1585.RPT", 1)
    Call Imprimir("FR1584.RPT", 1)
  Else
    Call Imprimir("FRR186.RPT", 1)
    Call Imprimir("FRR188.RPT", 1)
    Call Imprimir("FRR185.RPT", 1)
    Call Imprimir("FRR184.RPT", 1)
  End If
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  strUpdCanti = "UPDATE FRK500 SET FRK5CANTSUMIFARM=FRK5CANTSUMIFARM-FRK5CANTULTFARM,FRK5CANTULTFARM=? "
  If gstrQA = "0" Then
    strUpdCanti = strUpdCanti & " WHERE FRK5INDQUIANE=0" 'Quirofano
  Else
    strUpdCanti = strUpdCanti & " WHERE FRK5INDQUIANE=1" 'Anestesia
  End If
  Select Case gstrEst
  Case "12" 'Pendiente
    If gstrQA = "0" Then
      strUpdCanti = strUpdCanti & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=12 AND FRK3INDQUIANE=0)" 'Quirofano
    Else
      strUpdCanti = strUpdCanti & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=12 AND FRK3INDQUIANE=-1)" 'Anestesia
    End If
  Case "5" 'Dispensada Total
    strUpdCanti = strUpdCanti & "1=0"
  Case "8" 'Anulada
    strUpdCanti = strUpdCanti & "1=0"
  Case "9" 'Dispensada Parcial
    If gstrQA = "0" Then
      strUpdCanti = strUpdCanti & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=9 AND FRK3INDQUIANE=0)" 'Quirofano
    Else
      strUpdCanti = strUpdCanti & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=9 AND FRK3INDQUIANE=-1)" 'Anestesia
    End If
  Case "12,9" 'Dispensada Parcial
    If gstrQA = "0" Then
      strUpdCanti = strUpdCanti & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC IN (12,9) AND FRK3INDQUIANE=0)" 'Quirofano
    Else
      strUpdCanti = strUpdCanti & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC IN (12,9) AND FRK3INDQUIANE=-1)" 'Anestesia
    End If
  Case Else
    strUpdCanti = strUpdCanti & "1=0"
  End Select
  Set qryUpdCanti = objApp.rdoConnect.CreateQuery("", strUpdCanti)
  qryUpdCanti(0) = 0
  qryUpdCanti.Execute
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Select Case gstrEst
  Case "12" 'Pendiente
      strUpdTrat = "UPDATE FRK500 SET FRK5INDTRATADO=? "
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=0 AND FRK5INDBLOQ=0" 'Quirofano
      Else
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=1 AND FRK5INDBLOQ=0" 'Anestesia
      End If
      strUpdTrat = strUpdTrat & " AND FRK5INDTRATADO=0 "
      strUpdTrat = strUpdTrat & " AND FRK5CANTSUMIFARM=FRK5CANTULTFARM"
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=12 AND FRK3INDQUIANE=0)"  'Quirofano
      Else
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=12 AND FRK3INDQUIANE=-1)"  'Anestesia
      End If
      Set qryUpdTrat = objApp.rdoConnect.CreateQuery("", strUpdTrat)
      qryUpdTrat(0) = 2
      qryUpdTrat.Execute
    
      strUpdTrat = "UPDATE FRK500 SET FRK5INDTRATADO=? "
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=0 AND FRK5INDBLOQ=0" 'Quirofano
      Else
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=1 AND FRK5INDBLOQ=0" 'Anestesia
      End If
      strUpdTrat = strUpdTrat & " AND FRK5INDTRATADO=0 "
      strUpdTrat = strUpdTrat & " AND FRK5CANTSUMIFARM>FRK5CANTULTFARM"
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=12 AND FRK3INDQUIANE=0)"  'Quirofano
      Else
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=12 AND FRK3INDQUIANE=-1)"  'Anestesia
      End If
      Set qryUpdTrat = objApp.rdoConnect.CreateQuery("", strUpdTrat)
      qryUpdTrat(0) = 1
      qryUpdTrat.Execute
  Case "5" 'Dispensada Total
  Case "8" 'Anulada
  Case "9" 'Dispensada Parcial
      strUpdTrat = "UPDATE FRK500 SET FRK5INDTRATADO=? "
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=0 AND FRK5INDBLOQ=0" 'Quirofano
      Else
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=1 AND FRK5INDBLOQ=0" 'Anestesia
      End If
      strUpdTrat = strUpdTrat & " AND FRK5INDTRATADO=1 "
      strUpdTrat = strUpdTrat & " AND FRK5CANTSUMIFARM=FRK5CANTULTFARM"
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=9 AND FRK3INDQUIANE=0)"  'Quirofano
      Else
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=9 AND FRK3INDQUIANE=-1)"  'Anestesia
      End If
      Set qryUpdTrat = objApp.rdoConnect.CreateQuery("", strUpdTrat)
      qryUpdTrat(0) = 2
      qryUpdTrat.Execute
    
      strUpdTrat = "UPDATE FRK500 SET FRK5INDTRATADO=? "
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=0 AND FRK5INDBLOQ=0" 'Quirofano
      Else
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=1 AND FRK5INDBLOQ=0" 'Anestesia
      End If
      strUpdTrat = strUpdTrat & " AND FRK5INDTRATADO=0 "
      strUpdTrat = strUpdTrat & " AND FRK5CANTSUMIFARM>FRK5CANTULTFARM"
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=9 AND FRK3INDQUIANE=0)"  'Quirofano
      Else
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC=9 AND FRK3INDQUIANE=-1)"  'Anestesia
      End If
      Set qryUpdTrat = objApp.rdoConnect.CreateQuery("", strUpdTrat)
      qryUpdTrat(0) = 1
      qryUpdTrat.Execute
  Case "12,9" 'Dispensada Parcial
      strUpdTrat = "UPDATE FRK500 SET FRK5INDTRATADO=? "
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=0 AND FRK5INDBLOQ=0" 'Quirofano
      Else
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=1 AND FRK5INDBLOQ=0" 'Anestesia
      End If
      strUpdTrat = strUpdTrat & " AND FRK5INDTRATADO<>2 "
      strUpdTrat = strUpdTrat & " AND FRK5CANTSUMIFARM=FRK5CANTULTFARM"
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC IN (12,9) AND FRK3INDQUIANE=0)"  'Quirofano
      Else
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC IN (12,9) AND FRK3INDQUIANE=-1)"  'Anestesia
      End If
      Set qryUpdTrat = objApp.rdoConnect.CreateQuery("", strUpdTrat)
      qryUpdTrat(0) = 2
      qryUpdTrat.Execute
    
      strUpdTrat = "UPDATE FRK500 SET FRK5INDTRATADO=? "
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=0 AND FRK5INDBLOQ=0" 'Quirofano
      Else
        strUpdTrat = strUpdTrat & " WHERE FRK5INDQUIANE=1 AND FRK5INDBLOQ=0" 'Anestesia
      End If
      strUpdTrat = strUpdTrat & " AND FRK5INDTRATADO=0 "
      strUpdTrat = strUpdTrat & " AND FRK5CANTSUMIFARM>FRK5CANTULTFARM"
      If gstrQA = "0" Then
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC IN (12,9) AND FRK3INDQUIANE=0)"  'Quirofano
      Else
        strUpdTrat = strUpdTrat & " AND FRK3CODNECESUNID IN (SELECT FRK3CODNECESUNID FROM FRK300 WHERE FR26CODESTPETIC IN (12,9) AND FRK3INDQUIANE=-1)"  'Anestesia
      End If
      Set qryUpdTrat = objApp.rdoConnect.CreateQuery("", strUpdTrat)
      qryUpdTrat(0) = 1
      qryUpdTrat.Execute
  Case Else
  End Select
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
  Call Actualizar_Peticiones
  
  Call Actualizar_Grid

  Me.Enabled = True
  Screen.MousePointer = vbDefault
  Call MsgBox("Se ha realizado la Dispensaci�n", vbExclamation, "Aviso")
  
  Exit Sub

End Sub
