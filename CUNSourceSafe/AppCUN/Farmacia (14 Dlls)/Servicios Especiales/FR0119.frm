VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmBusProductosDSE 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA.Productos"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.TextBox txtinterno 
      Height          =   315
      Left            =   5040
      MaxLength       =   6
      TabIndex        =   24
      ToolTipText     =   "C�digo Producto"
      Top             =   840
      Width           =   1095
   End
   Begin VB.Frame frame2 
      Caption         =   "Medicamentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3495
      Left            =   120
      TabIndex        =   22
      Top             =   2280
      Width           =   8145
      Begin SSDataWidgets_B.SSDBGrid Gridmed 
         Height          =   3015
         Left            =   120
         TabIndex        =   23
         Top             =   360
         Width           =   7935
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   16
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowHeight       =   423
         Columns.Count   =   16
         Columns(0).Width=   2090
         Columns(0).Caption=   "Codigo"
         Columns(0).Name =   "Codigo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1588
         Columns(1).Caption=   "Interno"
         Columns(1).Name =   "Interno"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   2223
         Columns(2).Caption=   "Referencia"
         Columns(2).Name =   "Referencia"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3889
         Columns(3).Caption=   "Descripci�n"
         Columns(3).Name =   "Descripci�n"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1217
         Columns(4).Caption=   "F.F"
         Columns(4).Name =   "FF"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   1508
         Columns(5).Caption=   "Dosis"
         Columns(5).Name =   "Dosis"
         Columns(5).Alignment=   1
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   1482
         Columns(6).Caption=   "U.M"
         Columns(6).Name =   "U.M"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   2910
         Columns(7).Caption=   "Cod.Grp.Terap�utico"
         Columns(7).Name =   "Cod.Grp.Terapeutico"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   4471
         Columns(8).Caption=   "Grupo Terap�utico"
         Columns(8).Name =   "Grupo Terapeutico"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   1561
         Columns(9).Caption=   "Volumen"
         Columns(9).Name =   "Volumen"
         Columns(9).Alignment=   1
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   1138
         Columns(10).Caption=   "V�a"
         Columns(10).Name=   "V�a"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   2408
         Columns(11).Caption=   "Estupefaciente?"
         Columns(11).Name=   "Estupefaciente?"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   11
         Columns(11).FieldLen=   256
         Columns(11).Style=   2
         Columns(12).Width=   1984
         Columns(12).Caption=   "Psicotr�pico?"
         Columns(12).Name=   "Psicotropico?"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   11
         Columns(12).FieldLen=   256
         Columns(12).Style=   2
         Columns(13).Width=   2514
         Columns(13).Caption=   "Uso Hospitalario?"
         Columns(13).Name=   "Uso Hospitalario?"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   11
         Columns(13).FieldLen=   256
         Columns(13).Style=   2
         Columns(14).Width=   3466
         Columns(14).Caption=   "Diagn�stico Hospitalario?"
         Columns(14).Name=   "Diagnostico Hospitalario?"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   11
         Columns(14).FieldLen=   256
         Columns(14).Style=   2
         Columns(15).Width=   5477
         Columns(15).Caption=   "Indicaciones"
         Columns(15).Name=   "Indicaciones"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         _ExtentX        =   13996
         _ExtentY        =   5318
         _StockProps     =   79
      End
   End
   Begin VB.Frame frame3 
      Caption         =   "Principios Activos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   2160
      Left            =   120
      TabIndex        =   20
      Top             =   5880
      Width           =   8145
      Begin SSDataWidgets_B.SSDBGrid gridprincactiv 
         Height          =   1695
         Left            =   120
         TabIndex        =   21
         Top             =   360
         Width           =   7935
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   5
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Caption=   "Cod Principio"
         Columns(0).Name =   "Cod Principio"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4763
         Columns(1).Caption=   "Principio Activo"
         Columns(1).Name =   "Principio Activo"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Codigo Producto"
         Columns(2).Name =   "Codigo Producto"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   2302
         Columns(3).Caption=   "Dosis"
         Columns(3).Name =   "Dosis"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1535
         Columns(4).Caption=   "U.M"
         Columns(4).Name =   "U.M"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   13996
         _ExtentY        =   2990
         _StockProps     =   79
      End
   End
   Begin VB.CheckBox chkVisible 
      Caption         =   "Visible"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6600
      TabIndex        =   19
      ToolTipText     =   "Estupefaciente"
      Top             =   600
      Value           =   1  'Checked
      Width           =   1935
   End
   Begin VB.CheckBox chkestupefaciente 
      Caption         =   "Estupefaciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6600
      TabIndex        =   18
      ToolTipText     =   "Estupefaciente"
      Top             =   960
      Width           =   1935
   End
   Begin VB.CheckBox chkpsicotropico 
      Caption         =   "Psicotr�pico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6600
      TabIndex        =   17
      ToolTipText     =   "Psicotr�pico"
      Top             =   1680
      Width           =   1575
   End
   Begin VB.CheckBox chkusohospitalario 
      Caption         =   "Uso Hospitalario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6600
      TabIndex        =   16
      ToolTipText     =   "Uso Hospitalario"
      Top             =   1320
      Width           =   1815
   End
   Begin VB.CheckBox chkdiagnostico 
      Caption         =   "Diagn�stico Hospitalario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6600
      TabIndex        =   15
      ToolTipText     =   "Diagn�stico Hospital"
      Top             =   2040
      Width           =   2415
   End
   Begin VB.Frame Frame1 
      Caption         =   "Medicamentos Seleccionados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   4935
      Left            =   8880
      TabIndex        =   12
      Top             =   2400
      Width           =   3015
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Height          =   4455
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   2775
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   8
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowHeight       =   423
         Columns.Count   =   8
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4207
         Columns(1).Caption=   "Medicamento"
         Columns(1).Name =   "Medicamento"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "CodInt"
         Columns(2).Name =   "CodInt"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "UM"
         Columns(3).Name =   "UM"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FF"
         Columns(4).Name =   "FF"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "Vol"
         Columns(5).Name =   "Vol"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "Via"
         Columns(6).Name =   "Via"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "Dosis"
         Columns(7).Name =   "Dosis"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   4895
         _ExtentY        =   7858
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmda�adir 
      Caption         =   "->"
      Height          =   255
      Left            =   8400
      TabIndex        =   14
      Top             =   3840
      Width           =   375
   End
   Begin VB.CheckBox chkproducto 
      Caption         =   "�Nombre Comercial?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C000C0&
      Height          =   255
      Left            =   9240
      TabIndex        =   11
      ToolTipText     =   "Medicamento?"
      Top             =   1440
      Value           =   1  'Checked
      Width           =   2295
   End
   Begin VB.CheckBox chkprincipioactivo 
      Caption         =   "�Principio Activo?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C000C0&
      Height          =   255
      Left            =   9240
      TabIndex        =   10
      ToolTipText     =   "Principio Activo?"
      Top             =   1800
      Width           =   1935
   End
   Begin VB.CommandButton cmdfiltrar 
      Caption         =   "Filtrar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9240
      TabIndex        =   9
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox txtdescgrupo 
      Height          =   315
      Left            =   2520
      TabIndex        =   5
      Top             =   1560
      Width           =   3855
   End
   Begin VB.TextBox txtcodgrupo 
      Height          =   315
      Left            =   240
      TabIndex        =   4
      Top             =   1560
      Width           =   2055
   End
   Begin VB.TextBox txtdescripcion 
      Height          =   315
      Left            =   240
      TabIndex        =   3
      Top             =   840
      Width           =   4575
   End
   Begin VB.CommandButton cdmtraer 
      Caption         =   "Traer Medicamentos"
      Height          =   375
      Left            =   9240
      TabIndex        =   2
      Top             =   7560
      Width           =   1695
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label Label2 
      Caption         =   "C�d. Producto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5040
      TabIndex        =   25
      Top             =   600
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Desc. Grupo Terape�tico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2520
      TabIndex        =   8
      Top             =   1320
      Width           =   2295
   End
   Begin VB.Label lblcodgrupo 
      Caption         =   "C�d. Grupo Terape�tico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   1320
      Width           =   2295
   End
   Begin VB.Label lbldescripcion 
      Caption         =   "Descripci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   600
      Width           =   1455
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmBusProductosDSE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmBusProductosDSE(FR0119.FRM)                                   *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: buscar productos                                        *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1


Private Sub cmda�adir_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
    
cmda�adir.Enabled = False
mintNTotalSelRows = Gridmed.SelBookmarks.Count
For mintisel = 0 To mintNTotalSelRows - 1
  mvarBkmrk = Gridmed.SelBookmarks(mintisel)
  'c�d.producto,descripci�n,interno,UM,FF,volumen,V�a,Dosis
  SSDBGrid1.AddItem Gridmed.Columns(0).CellValue(mvarBkmrk) & Chr(vbKeyTab) & _
                    Gridmed.Columns(3).CellValue(mvarBkmrk) & Chr(vbKeyTab) & _
                    Gridmed.Columns(1).CellValue(mvarBkmrk) & Chr(vbKeyTab) & _
                    Gridmed.Columns(6).CellValue(mvarBkmrk) & Chr(vbKeyTab) & _
                    Gridmed.Columns(4).CellValue(mvarBkmrk) & Chr(vbKeyTab) & _
                    Gridmed.Columns(9).CellValue(mvarBkmrk) & Chr(vbKeyTab) & _
                    Gridmed.Columns(10).CellValue(mvarBkmrk) & Chr(vbKeyTab) & _
                    Gridmed.Columns(5).CellValue(mvarBkmrk)
Next mintisel

cmda�adir.Enabled = True
End Sub


Private Sub Gridmed_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
'actualizar el grid de principios activos
Dim rsta As rdoResultset
Dim stra As String

gridprincactiv.RemoveAll
If IsNumeric(Gridmed.Columns(0).Value) Then
    stra = "SELECT FR6400.*,FR6800.FR68DESPRINCACTIV FROM FR6400,FR6800 WHERE " & _
           "FR6400.FR73CODPRODUCTO=" & Gridmed.Columns(0).Value & _
           " AND FR6400.FR68CODPRINCACTIV=FR6800.FR68CODPRINCACTIV" & " ORDER BY FR6800.FR68DESPRINCACTIV ASC"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    While Not rsta.EOF
        gridprincactiv.AddItem rsta.rdoColumns("FR68CODPRINCACTIV").Value & Chr(vbKeyTab) & _
                     rsta.rdoColumns("FR68DESPRINCACTIV").Value & Chr(vbKeyTab) & _
                     rsta.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                     rsta.rdoColumns("FR64DOSIS").Value & Chr(vbKeyTab) & _
                     rsta.rdoColumns("FR93CODUNIMEDIDA").Value
    rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
End If
End Sub

Private Sub SSDBGrid1_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = 0
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cdmtraer_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mensaje As String

cdmtraer.Enabled = False
If SSDBGrid1.Rows > 0 Then
  'Guardamos el n�mero de filas seleccionadas
  mintNTotalSelRows = SSDBGrid1.Rows
  ReDim gintprodbuscado(mintNTotalSelRows, 8)
  gintprodtotal = mintNTotalSelRows
  For mintisel = 0 To mintNTotalSelRows - 1
    gblnSelProd = True
      'Guardamos el n�mero de fila que est� seleccionada
       gintprodbuscado(mintisel, 0) = SSDBGrid1.Columns("C�digo").Value
       gintprodbuscado(mintisel, 1) = SSDBGrid1.Columns("CodInt").Value
       gintprodbuscado(mintisel, 2) = SSDBGrid1.Columns("Medicamento").Value
       gintprodbuscado(mintisel, 3) = SSDBGrid1.Columns("Dosis").Value 'dosis
       gintprodbuscado(mintisel, 4) = SSDBGrid1.Columns("UM").Value 'unidad medida
       gintprodbuscado(mintisel, 5) = SSDBGrid1.Columns("FF").Value 'forma
       gintprodbuscado(mintisel, 6) = SSDBGrid1.Columns("Vol").Value 'volumen
       gintprodbuscado(mintisel, 7) = SSDBGrid1.Columns("Via").Value 'v�a
       SSDBGrid1.MoveNext
  Next mintisel
  cdmtraer.Enabled = True
  Unload Me
Else
  mensaje = MsgBox("No ha seleccionado ning�n medicamento", vbInformation, "Aviso")
  cdmtraer.Enabled = True
End If

End Sub

Private Sub cmdFiltrar_Click()
Dim strClausulaWhere As String
Dim rsta As rdoResultset
Dim stra As String
Dim listacodprincipios As String
Dim listaproductos As String
Dim rdoQ As rdoQuery
Dim strclausulafiltro As String
Dim rstclausulafiltro As rdoResultset
Dim FF As String
Dim Dosis As String
Dim Volumen As String
Dim Via As String
Dim IndEstupefaci As Integer
Dim IndPsicot As Integer
Dim IndUsoHosp As String
Dim IndDiagHosp As String
Dim Indicaciones As String
Dim codgrpterap As String
Dim descgrpterap As String

cmdfiltrar.Enabled = False


'*************************FORM_LOAD*************************************
If gintbuscargruprod = 1 Then
    'strClausulaWhere = "FR73CODPRODUCTO=" & frmBusGrpProd.grdDBGrid1(0).Columns(3).Value
Else
    If gstrLlamadorProd = "Principio Activo" Then
        strClausulaWhere = "fr73indprinact=-1" & _
         " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))"

    ElseIf gstrLlamador = "frmRegDev" Then
      strClausulaWhere = "-1=0 " & _
        " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))"

    Else
        strClausulaWhere = "-1=0 " & _
         " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))"

    End If
End If

'***********************************************************************


If chkprincipioactivo.Value = 0 And chkproducto.Value = 0 Then
    Call MsgBox("Debe especificar b�squeda por Medicamento o Principio Activo.", vbInformation, "Aviso")
End If

'En txtdescripcion se mete un producto
    If gintbuscargruprod = 1 Then
       'strclausulawhere = "FR73CODPRODUCTO=" & frmBusGrpProd.grdDBGrid1(0).Columns(3).Value
    Else
       If gstrLlamadorProd = "Principio Activo" Then
          strClausulaWhere = "fr73indprinact=-1" & _
          " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))"

       Else
          strClausulaWhere = "-1=-1 " & _
          " AND TRUNC(SYSDATE) BETWEEN TO_DATE(TO_CHAR(FR73FECINIVIG,'DD/MM/YYYY'),'DD/MM/YYYY') AND NVL(TO_DATE(TO_CHAR(FR73FECFINVIG,'DD/MM/YYYY'),'DD/MM/YYYY'),TO_DATE('31/12/9999','DD/MM/YYYY'))"

       End If
    End If
    'If txtdescripcion.Text <> "" Then
    '   strclausulawhere = strclausulawhere & " AND UPPER(FR73DESPRODUCTO) like UPPER('" & txtdescripcion.Text & "%')"
    'End If
    If txtcodgrupo.Text <> "" Then
            strClausulaWhere = strClausulaWhere & " AND UPPER(FR00CODGRPTERAP) like UPPER('" & txtcodgrupo.Text & "%')"
    End If
    If txtdescgrupo.Text <> "" Then
            strClausulaWhere = strClausulaWhere & " AND UPPER(FR00DESGRPTERAP) like UPPER('" & txtdescgrupo.Text & "%')"
    End If
    If chkestupefaciente.Value = 0 Then
        'strClausulaWhere = strClausulaWhere & " AND (FR73INDESTUPEFACI=0 OR FR73INDESTUPEFACI IS NULL)"
    Else
        strClausulaWhere = strClausulaWhere & " AND FR73INDESTUPEFACI=-1"
    End If
    If chkpsicotropico.Value = 0 Then
            'strClausulaWhere = strClausulaWhere & " AND (FR73INDPSICOT=0 OR FR73INDPSICOT IS NULL)"
    Else
            strClausulaWhere = strClausulaWhere & " AND FR73INDPSICOT=-1"
    End If
    If chkdiagnostico.Value = 0 Then
            'strClausulaWhere = strClausulaWhere & " AND (FR73INDDIAGHOSP=0 OR FR73INDDIAGHOSP IS NULL)"
    Else
            strClausulaWhere = strClausulaWhere & " AND FR73INDDIAGHOSP=-1"
    End If
    If chkusohospitalario.Value = 0 Then
            'strClausulaWhere = strClausulaWhere & " AND (FR73INDUSOHOSP=0 OR FR73INDUSOHOSP IS NULL)"
    Else
            strClausulaWhere = strClausulaWhere & " AND FR73INDUSOHOSP=-1"
    End If

'En txtdescripcion se mete un principio activo y se buscan productos que contengan
'ese principio
If chkprincipioactivo.Value = 1 Then
 If txtDescripcion.Text <> "" Then
 
Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
Rem =====================================================================
    'stra = "SELECT FR68CODPRINCACTIV FROM FR6800 WHERE " & _
    '       "UPPER(FR68DESPRINCACTIV) like UPPER('" & txtdescripcion.Text & "%')"
    stra = "SELECT FR68CODPRINCACTIV FROM FR6800 WHERE " & _
           "UPPER(FR68DESPRINCACTIV) like UPPER(?)"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", stra)
    rdoQ(0) = "%" & txtDescripcion.Text & "%"
    Set rsta = rdoQ.OpenResultset(stra)
    
    If listacodprincipios = "" And Not rsta.EOF Then
        listacodprincipios = listacodprincipios & rsta.rdoColumns(0).Value
        rsta.MoveNext
    End If
    While Not rsta.EOF
        listacodprincipios = listacodprincipios & "," & rsta.rdoColumns(0).Value
        rsta.MoveNext
    Wend
    If listacodprincipios <> "" Then
        'bind variables
        'stra = "SELECT FR73CODPRODUCTO FROM FR6400 WHERE " & _
        '      "FR68CODPRINCACTIV IN (" & listacodprincipios & ")"
        'Set rsta = objApp.rdoConnect.OpenResultset(stra)
        stra = "SELECT FR73CODPRODUCTO FROM FR6400 WHERE " & _
              "FR68CODPRINCACTIV IN (" & listacodprincipios & ")"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", stra)
        'rdoQ(0) = listacodprincipios
        On Error GoTo ErrorLista
        Set rsta = rdoQ.OpenResultset(stra)
        If listaproductos = "" And Not rsta.EOF Then
            listaproductos = listaproductos & rsta.rdoColumns(0).Value
            rsta.MoveNext
        End If
        While Not rsta.EOF
            listaproductos = listaproductos & "," & rsta.rdoColumns(0).Value
            rsta.MoveNext
        Wend
    End If
 End If
End If

If chkproducto = 1 And chkprincipioactivo = 1 And listaproductos <> "" Then
    If txtDescripcion.Text <> "" Then
        strClausulaWhere = strClausulaWhere & _
        " AND (UPPER(FR73DESPRODUCTO) like UPPER('%" & txtDescripcion.Text & "%')" & _
              " OR " & _
              "FR73CODPRODUCTO IN (" & listaproductos & ")" & ")"
    End If
End If
If chkproducto = 1 And chkprincipioactivo = 1 And listaproductos = "" Then
    If txtDescripcion.Text <> "" Then
        strClausulaWhere = strClausulaWhere & _
        " AND UPPER(FR73DESPRODUCTO) like UPPER('%" & txtDescripcion.Text & "%')"
    End If
End If
If chkproducto = 1 And chkprincipioactivo = 0 Then
    If txtDescripcion.Text <> "" Then
        strClausulaWhere = strClausulaWhere & _
        " AND UPPER(FR73DESPRODUCTO) like UPPER('%" & txtDescripcion.Text & "%')"
    End If
End If
If chkproducto = 0 And chkprincipioactivo = 1 And listaproductos <> "" Then
    If txtDescripcion.Text <> "" Then
        strClausulaWhere = strClausulaWhere & _
        " AND FR73CODPRODUCTO IN (" & listaproductos & ")"
    End If
End If
If chkproducto = 0 And chkprincipioactivo = 1 And listaproductos = "" Then
    If txtDescripcion.Text <> "" Then
        strClausulaWhere = strClausulaWhere & _
        " AND FR73CODPRODUCTO IS NULL"
    End If
End If

If chkVisible = 0 Then
  If strClausulaWhere <> "" Then
    strClausulaWhere = strClausulaWhere & _
    " AND FR73INDVISIBLE=0"
  Else
    strClausulaWhere = strClausulaWhere & _
    " FR73INDVISIBLE=0"
  End If
Else
  If strClausulaWhere <> "" Then
    strClausulaWhere = strClausulaWhere & _
    " AND FR73INDVISIBLE=-1"
  Else
    strClausulaWhere = strClausulaWhere & _
    " FR73INDVISIBLE=-1"
  End If
End If

If txtinterno.Text <> "" Then
  strClausulaWhere = strClausulaWhere & " AND  FR73CODINTFAR=" & "'" & txtinterno.Text & "'"
End If

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

strclausulafiltro = "SELECT * FROM FR7300J WHERE " & strClausulaWhere & " ORDER BY FR73DESPRODUCTO ASC"
Set rstclausulafiltro = objApp.rdoConnect.OpenResultset(strclausulafiltro)
Gridmed.RemoveAll
gridprincactiv.RemoveAll

While Not rstclausulafiltro.EOF
    If IsNull(rstclausulafiltro.rdoColumns("FRH7CODFORMFAR").Value) Then
      FF = ""
    Else
      FF = rstclausulafiltro.rdoColumns("FRH7CODFORMFAR").Value
    End If
    If IsNull(rstclausulafiltro.rdoColumns("FR73DOSIS").Value) Then
      Dosis = ""
    Else
      Dosis = rstclausulafiltro.rdoColumns("FR73DOSIS").Value
    End If
    If IsNull(rstclausulafiltro.rdoColumns("FR73VOLUMEN").Value) Then
      Volumen = ""
    Else
      Volumen = rstclausulafiltro.rdoColumns("FR73VOLUMEN").Value
    End If
    If IsNull(rstclausulafiltro.rdoColumns("FR34CODVIA").Value) Then
      Via = ""
    Else
      Via = rstclausulafiltro.rdoColumns("FR34CODVIA").Value
    End If
    If IsNull(rstclausulafiltro.rdoColumns("FR73INDESTUPEFACI").Value) Then
      IndEstupefaci = 0
    Else
      IndEstupefaci = rstclausulafiltro.rdoColumns("FR73INDESTUPEFACI").Value
    End If
    If IsNull(rstclausulafiltro.rdoColumns("FR73INDPSICOT").Value) Then
      IndPsicot = 0
    Else
      IndPsicot = rstclausulafiltro.rdoColumns("FR73INDPSICOT").Value
    End If
    If IsNull(rstclausulafiltro.rdoColumns("FR73INDUSOHOSP").Value) Then
      IndUsoHosp = 0
    Else
      IndUsoHosp = rstclausulafiltro.rdoColumns("FR73INDUSOHOSP").Value
    End If
    If IsNull(rstclausulafiltro.rdoColumns("FR73INDDIAGHOSP").Value) Then
      IndDiagHosp = 0
    Else
      IndDiagHosp = rstclausulafiltro.rdoColumns("FR73INDDIAGHOSP").Value
    End If
    If IsNull(rstclausulafiltro.rdoColumns("FR73INDICACIONES").Value) Then
      Indicaciones = ""
    Else
      Indicaciones = rstclausulafiltro.rdoColumns("FR73INDICACIONES").Value
    End If
    If IsNull(rstclausulafiltro.rdoColumns("FR00CODGRPTERAP").Value) Then
      codgrpterap = ""
    Else
      codgrpterap = rstclausulafiltro.rdoColumns("FR00CODGRPTERAP").Value
    End If
    If IsNull(rstclausulafiltro.rdoColumns("FR00DESGRPTERAP").Value) Then
      descgrpterap = ""
    Else
      descgrpterap = rstclausulafiltro.rdoColumns("FR00DESGRPTERAP").Value
    End If
    
    Gridmed.AddItem rstclausulafiltro.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                   rstclausulafiltro.rdoColumns("FR73CODINTFAR").Value & Chr(vbKeyTab) & _
                   rstclausulafiltro.rdoColumns("FR73REFERENCIA").Value & Chr(vbKeyTab) & _
                   rstclausulafiltro.rdoColumns("FR73DESPRODUCTO").Value & Chr(vbKeyTab) & _
                   FF & Chr(vbKeyTab) & _
                   Dosis & Chr(vbKeyTab) & _
                   rstclausulafiltro.rdoColumns("FR93CODUNIMEDIDA").Value & Chr(vbKeyTab) & _
                   codgrpterap & Chr(vbKeyTab) & _
                   descgrpterap & Chr(vbKeyTab) & _
                   Volumen & Chr(vbKeyTab) & _
                   Via & Chr(vbKeyTab) & _
                   IndEstupefaci & Chr(vbKeyTab) & _
                   IndPsicot & Chr(vbKeyTab) & _
                   IndUsoHosp & Chr(vbKeyTab) & _
                   IndDiagHosp & Chr(vbKeyTab) & _
                   Indicaciones
rstclausulafiltro.MoveNext
Wend
rstclausulafiltro.Close
Set rstclausulafiltro = Nothing

'se cargan los principios activos del primer registro del grid de medicamentos
If IsNumeric(Gridmed.Columns(0).Value) Then
    stra = "SELECT FR6400.*,FR6800.FR68DESPRINCACTIV FROM FR6400,FR6800 WHERE " & _
           "FR6400.FR73CODPRODUCTO=" & Gridmed.Columns(0).Value & _
           " AND FR6400.FR68CODPRINCACTIV=FR6800.FR68CODPRINCACTIV"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    While Not rsta.EOF
        gridprincactiv.AddItem rsta.rdoColumns("FR68CODPRINCACTIV").Value & Chr(vbKeyTab) & _
                     rsta.rdoColumns("FR68DESPRINCACTIV").Value & Chr(vbKeyTab) & _
                     rsta.rdoColumns("FR73CODPRODUCTO").Value & Chr(vbKeyTab) & _
                     rsta.rdoColumns("FR64DOSIS").Value & Chr(vbKeyTab) & _
                     rsta.rdoColumns("FR93CODUNIMEDIDA").Value
    rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
End If

cmdfiltrar.Enabled = True
Exit Sub

ErrorLista:
If ERR > 0 Then
MsgBox "Error # " & Str(ERR.Number) & ERR.Description, vbInformation, Me.Caption
cmdfiltrar.Enabled = True
End If

End Sub

Private Sub Form_Activate()
Call Inicializar_Toolbar
Call Inicializar_Grid
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  

If gstrLlamador = "frmRegDev" Then
      chkestupefaciente.Visible = False
      chkusohospitalario.Visible = False
      chkpsicotropico.Visible = False
      chkdiagnostico.Visible = False
      chkproducto.Visible = False
      chkprincipioactivo.Visible = False
      'fraFrame1(1).Height = 5655
      'tabTab1(0).Height = 5175
      'grdDBGrid1(2).Height = 5145
      Gridmed.SelectTypeRow = ssSelectionTypeMultiSelectRange
End If
    
Gridmed.Columns(0).Visible = False 'c�d.producto
gridprincactiv.Columns(0).Visible = False
gridprincactiv.Columns(2).Visible = False

Gridmed.Columns(1).Width = 700
Gridmed.Columns(2).Width = 1200
Gridmed.Columns(3).Width = 3300
Gridmed.Columns(4).Width = 600
Gridmed.Columns(5).Width = 600
Gridmed.Columns(6).Width = 600
Gridmed.Columns(7).Width = 1700
Gridmed.Columns(8).Width = 3000
Gridmed.Columns(9).Width = 750
Gridmed.Columns(10).Width = 600
Gridmed.Columns(11).Width = 1100
Gridmed.Columns(12).Width = 1100
Gridmed.Columns(13).Width = 1500
Gridmed.Columns(14).Width = 2200
Gridmed.Columns(15).Width = 15000

gridprincactiv.Columns(1).Width = 5700
gridprincactiv.Columns(3).Width = 1000
gridprincactiv.Columns(4).Width = 800
  
End Sub






' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Select Case btnButton.Index
  Case 30:
          Unload Me
End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
If intIndex = 100 Then
  Unload Me
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   tlbToolbar1.Buttons.Item(4).Enabled = False
   tlbToolbar1.Buttons.Item(6).Enabled = False
   tlbToolbar1.Buttons.Item(8).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(21).Enabled = False
   tlbToolbar1.Buttons.Item(22).Enabled = False
   tlbToolbar1.Buttons.Item(23).Enabled = False
   tlbToolbar1.Buttons.Item(24).Enabled = False
   tlbToolbar1.Buttons.Item(26).Enabled = False
   
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuDatosOpcion(80).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuRegistroOpcion(40).Enabled = False
   mnuRegistroOpcion(50).Enabled = False
   mnuRegistroOpcion(60).Enabled = False
   mnuRegistroOpcion(70).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub
Private Sub Inicializar_Grid()
Dim i As Integer

For i = 0 To 15
  Gridmed.Columns(i).Locked = True
Next i
For i = 0 To 4
  gridprincactiv.Columns(i).Locked = True
Next i
For i = 0 To 1
  SSDBGrid1.Columns(i).Locked = True
Next i

End Sub














