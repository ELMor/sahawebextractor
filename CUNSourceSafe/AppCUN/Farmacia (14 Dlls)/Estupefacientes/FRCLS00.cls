VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************
Const PRWinMonPetEstup                As String = "FR0103"
Const PRWinDispEstupef                As String = "FR0107"
Const PRWinEstuVerPedidoFarmacia      As String = "FR0108"
Const PRWinMonLibEstupef              As String = "FR0109"
Const PRWinEstuVerOMPRN               As String = "FR0190"
Const PRWinModLinea                   As String = "FR0197"
Const PRWinVerPerfilFTPEstupefac      As String = "FR0198"
Const PRWinAlergiasPacienteEstupefac  As String = "FR0199"
Const PRWinBusEstupefacientes         As String = "FR0360"
Const PRWinConsultarPeticionesEstupef As String = "FR0104"
Const PRWinConcatenarInsAdminEstupef  As String = "FR0102"


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case PRWinMonPetEstup
      Load frmMonPetEstup
      'Call objsecurity.AddHelpContext(528)
      Call frmMonPetEstup.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMonPetEstup
      Set frmMonPetEstup = Nothing
     Case PRWinDispEstupef
      Load FrmDispEstupef
      'Call objsecurity.AddHelpContext(528)
      Call FrmDispEstupef.Show(vbModal)
      'Call FrmDispEstupef.RemoveHelpContext
      Unload FrmDispEstupef
      Set FrmDispEstupef = Nothing
     Case PRWinEstuVerPedidoFarmacia
      Load FrmEstuVerPedidoFarmacia
      'Call objsecurity.AddHelpContext(528)
      Call FrmEstuVerPedidoFarmacia.Show(vbModal)
      'Call FrmEstuVerPedidoFarmacia.RemoveHelpContext
      Unload FrmEstuVerPedidoFarmacia
      Set FrmEstuVerPedidoFarmacia = Nothing
    Case PRWinMonLibEstupef
     Load FrmMonLibEstupef
     'Call objsecurity.AddHelpContext(528)
     Call FrmMonLibEstupef.Show(vbModal)
     'Call FrmDispEstupef.RemoveHelpContext
     Unload FrmMonLibEstupef
     Set FrmMonLibEstupef = Nothing
    Case PRWinEstuVerOMPRN
      Load frmEstuVerOMPRN
      'Call objsecurity.AddHelpContext(528)
      Call frmEstuVerOMPRN.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmEstuVerOMPRN
      Set frmEstuVerOMPRN = Nothing
    Case PRWinModLinea
      Load frmModLinea
      'Call objsecurity.AddHelpContext(528)
      Call frmModLinea.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmModLinea
      Set frmModLinea = Nothing
    Case PRWinVerPerfilFTPEstupefac
      Load frmVerPerfilFTPEstupefac
      'Call objsecurity.AddHelpContext(528)
      Call frmVerPerfilFTPEstupefac.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerPerfilFTPEstupefac
      Set frmVerPerfilFTPEstupefac = Nothing
    Case PRWinAlergiasPacienteEstupefac
      Load frmAlergiasPacienteEstupefac
      'Call objsecurity.AddHelpContext(528)
      Call frmAlergiasPacienteEstupefac.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmAlergiasPacienteEstupefac
      Set frmAlergiasPacienteEstupefac = Nothing
    Case PRWinBusEstupefacientes
      Load frmBusEstupefacientes
      'Call objsecurity.AddHelpContext(528)
      Call frmBusEstupefacientes.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusEstupefacientes
      Set frmBusEstupefacientes = Nothing
    Case PRWinConsultarPeticionesEstupef
      Load frmConsultarPeticionesEstupef
      'Call objsecurity.AddHelpContext(528)
      Call frmConsultarPeticionesEstupef.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmConsultarPeticionesEstupef
      Set frmConsultarPeticionesEstupef = Nothing
    Case PRWinConcatenarInsAdminEstupef
      Load frmConcatenarInsAdminEstupef
      'Call objsecurity.AddHelpContext(528)
      Call frmConcatenarInsAdminEstupef.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmConcatenarInsAdminEstupef
      Set frmConcatenarInsAdminEstupef = Nothing
      
  End Select
  Call err.Clear

End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 13, 1 To 4) As Variant
  aProcess(1, 1) = PRWinMonPetEstup
  aProcess(1, 2) = "Monitorizar Peticiones Estupefacientes"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(3, 1) = PRWinDispEstupef
  aProcess(3, 2) = "Dispensar Estupefacientes"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = PRWinEstuVerPedidoFarmacia
  aProcess(4, 2) = "Visualizar Pedido de Farmacia"
  aProcess(4, 3) = False
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = PRWinMonLibEstupef
  aProcess(5, 2) = "Monitorizar Libro Estupefacientes"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
  
  aProcess(6, 1) = PRWinEstuVerOMPRN
  aProcess(6, 2) = "Ver OM/PRN"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = PRWinModLinea
  aProcess(7, 2) = "Modificar Linea"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
  
  aProcess(8, 1) = PRWinVerPerfilFTPEstupefac
  aProcess(8, 2) = "Perfil FTP"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = PRWinAlergiasPacienteEstupefac
  aProcess(9, 2) = "Alergias"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeWindow
  
  aProcess(10, 1) = PRWinBusEstupefacientes
  aProcess(10, 2) = "Buscar Estupefacientes"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow
  
  aProcess(11, 1) = PRWinConsultarPeticionesEstupef
  aProcess(11, 2) = "Consultar Peticiones Estupefacientes"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow
  
  aProcess(12, 1) = PRWinConcatenarInsAdminEstupef
  aProcess(12, 2) = "Instrucciones de Administraci�n"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow
  
End Sub
