VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmVerPerfilFTPEstupefac 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMINISTRAR MEDICAMENTOS. Consultar Perfil Farmacoterap�utico."
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   10680
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   10680
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   10680
      _ExtentX        =   18838
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame5 
      Caption         =   "Intervalo"
      ForeColor       =   &H00C00000&
      Height          =   1935
      Left            =   9840
      TabIndex        =   37
      Top             =   600
      Width           =   1935
      Begin SSCalendarWidgets_A.SSDateCombo dtcinicio 
         Height          =   330
         Left            =   120
         TabIndex        =   19
         Tag             =   "Fecha Inicio"
         ToolTipText     =   "Fecha Inicio"
         Top             =   600
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2999
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcfin 
         Height          =   330
         Left            =   120
         TabIndex        =   20
         Tag             =   "Fecha Fin"
         ToolTipText     =   "Fecha Fin"
         Top             =   1320
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2999
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Fin"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   39
         Top             =   1080
         Width           =   705
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Inicio"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   38
         Top             =   360
         Width           =   870
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Origen "
      ForeColor       =   &H00C00000&
      Height          =   1695
      Left            =   8160
      TabIndex        =   32
      Top             =   600
      Width           =   1455
      Begin VB.OptionButton Option3 
         Caption         =   "Anestesia"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   17
         Top             =   1200
         Width           =   1215
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Quir�fano"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   16
         Top             =   960
         Width           =   1215
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Todos"
         Height          =   195
         Index           =   5
         Left            =   120
         TabIndex        =   18
         Top             =   1440
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Cesta"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   15
         Top             =   720
         Width           =   975
      End
      Begin VB.OptionButton Option3 
         Caption         =   "PRN"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   14
         Top             =   480
         Width           =   975
      End
      Begin VB.OptionButton Option3 
         Caption         =   "OM"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Productos"
      ForeColor       =   &H00C00000&
      Height          =   1935
      Left            =   6000
      TabIndex        =   31
      Top             =   600
      Width           =   1935
      Begin VB.TextBox txtproducto 
         Height          =   330
         Left            =   120
         MaxLength       =   6
         TabIndex        =   12
         Tag             =   "C�digo Producto"
         ToolTipText     =   "C�digo Producto"
         Top             =   1440
         Width           =   1335
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Todos"
         Height          =   255
         Index           =   2
         Left            =   360
         TabIndex        =   11
         Top             =   840
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Material"
         Height          =   255
         Index           =   1
         Left            =   360
         TabIndex        =   10
         Top             =   600
         Width           =   1095
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Medicamentos"
         Height          =   375
         Index           =   0
         Left            =   360
         TabIndex        =   9
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo Producto"
         Height          =   195
         Index           =   4
         Left            =   120
         TabIndex        =   40
         Top             =   1200
         Width           =   1185
      End
   End
   Begin VB.CommandButton cmdfiltrar 
      Caption         =   "FILTRAR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   26
      Top             =   2400
      Width           =   1215
   End
   Begin VB.Frame Frame2 
      Caption         =   "B�squeda por "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   2175
      Left            =   120
      TabIndex        =   25
      Top             =   480
      Width           =   5655
      Begin VB.TextBox Text1 
         DataField       =   "CI22NOMBRE"
         Height          =   330
         Index           =   2
         Left            =   1560
         Locked          =   -1  'True
         TabIndex        =   1
         Tag             =   "Paciente"
         ToolTipText     =   "Paciente"
         Top             =   600
         Width           =   4020
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         DataField       =   "CI22NUMHISTORIA"
         Height          =   330
         Index           =   1
         Left            =   120
         MaxLength       =   7
         TabIndex        =   0
         Tag             =   "Historia"
         ToolTipText     =   "Historia"
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdprocasis 
         Caption         =   "Buscar Proceso/Asistencia"
         Height          =   495
         Left            =   120
         TabIndex        =   2
         Top             =   1080
         Width           =   1695
      End
      Begin VB.TextBox txtproceso 
         Alignment       =   1  'Right Justify
         DataField       =   "CI22NUMHISTORIA"
         Height          =   330
         Left            =   2160
         MaxLength       =   10
         TabIndex        =   3
         Tag             =   "Proceso"
         ToolTipText     =   "Proceso"
         Top             =   1200
         Width           =   1400
      End
      Begin VB.TextBox txtasistencia 
         Alignment       =   1  'Right Justify
         DataField       =   "CI22NUMHISTORIA"
         Height          =   330
         Left            =   3840
         MaxLength       =   10
         TabIndex        =   4
         Tag             =   "Asistencia"
         ToolTipText     =   "Asistencia"
         Top             =   1200
         Width           =   1400
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Proc/Asist"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   3
         Left            =   4080
         TabIndex        =   8
         Top             =   1800
         Width           =   1335
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Asistencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   2
         Left            =   2640
         TabIndex        =   7
         Top             =   1800
         Width           =   1215
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Proceso"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   1
         Left            =   1320
         TabIndex        =   6
         Top             =   1800
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   1800
         Value           =   -1  'True
         Width           =   1095
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Paciente"
         Height          =   195
         Index           =   17
         Left            =   1560
         TabIndex        =   36
         Top             =   360
         Width           =   630
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         Height          =   195
         Index           =   21
         Left            =   120
         TabIndex        =   35
         Top             =   360
         Width           =   525
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Proceso"
         Height          =   195
         Index           =   2
         Left            =   2160
         TabIndex        =   34
         Top             =   960
         Width           =   585
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Asistencia"
         Height          =   195
         Index           =   3
         Left            =   3840
         TabIndex        =   33
         Top             =   960
         Width           =   720
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Perfil Farmacoterap�utico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5295
      Index           =   0
      Left            =   120
      TabIndex        =   21
      Top             =   2760
      Width           =   11745
      Begin VB.Frame FrameProcAsis 
         Caption         =   "Seleccionar Proceso y Asistencia"
         ForeColor       =   &H00C00000&
         Height          =   3495
         Left            =   2040
         TabIndex        =   27
         Top             =   240
         Visible         =   0   'False
         Width           =   7695
         Begin VB.CommandButton cmdcancelar 
            Caption         =   "Cancelar"
            Height          =   375
            Left            =   4080
            TabIndex        =   30
            Top             =   3000
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.CommandButton cmdaceptar 
            Caption         =   "Aceptar"
            Height          =   375
            Left            =   2880
            TabIndex        =   29
            Top             =   3000
            Visible         =   0   'False
            Width           =   975
         End
         Begin SSDataWidgets_B.SSDBGrid Grid2 
            Height          =   2655
            Index           =   0
            Left            =   120
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   240
            Visible         =   0   'False
            Width           =   7410
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   5
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns.Count   =   5
            Columns(0).Width=   2302
            Columns(0).Caption=   "Proceso"
            Columns(0).Name =   "Proceso"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2302
            Columns(1).Caption=   "Asistencia"
            Columns(1).Name =   "Asistencia"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   1588
            Columns(2).Caption=   "Cama"
            Columns(2).Name =   "Cama"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   2990
            Columns(3).Caption=   "Inicio"
            Columns(3).Name =   "Inicio"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   7
            Columns(3).FieldLen=   256
            Columns(4).Width=   2990
            Columns(4).Caption=   "Fin"
            Columns(4).Name =   "Fin"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   7
            Columns(4).FieldLen=   256
            _ExtentX        =   13070
            _ExtentY        =   4683
            _StockProps     =   79
            Caption         =   "PROCESOS / ASISTENCIAS"
         End
      End
      Begin SSDataWidgets_B.SSDBGrid Grid1 
         Height          =   4815
         Index           =   0
         Left            =   120
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   360
         Width           =   11490
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   20
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   20
         Columns(0).Width=   1561
         Columns(0).Caption=   "Origen"
         Columns(0).Name =   "Origen"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1773
         Columns(1).Caption=   "Historia"
         Columns(1).Name =   "Historia"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   2170
         Columns(2).Caption=   "Fecha"
         Columns(2).Name =   "Fecha"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   2143
         Columns(3).Caption=   "Interno"
         Columns(3).Name =   "Interno"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3466
         Columns(4).Caption=   "Producto"
         Columns(4).Name =   "Producto"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Caption=   "Cantidad"
         Columns(5).Name =   "Cantidad"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   1879
         Columns(6).Caption=   "Dosis"
         Columns(6).Name =   "Dosis"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   1693
         Columns(7).Caption=   "U.M"
         Columns(7).Name =   "UM"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   1773
         Columns(8).Caption=   "V�a"
         Columns(8).Name =   "V�a"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Caption=   "Int_dil"
         Columns(9).Name =   "Interno_dil"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Caption=   "Producto_dil"
         Columns(10).Name=   "Producto_dil"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   3200
         Columns(11).Caption=   "Cant_dil"
         Columns(11).Name=   "Cantidad_dil"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(12).Width=   3200
         Columns(12).Caption=   "Int_mez"
         Columns(12).Name=   "Interno_mez"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(13).Width=   3200
         Columns(13).Caption=   "Producto_mez"
         Columns(13).Name=   "Producto_mez"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(14).Width=   3200
         Columns(14).Caption=   "Cant_mez"
         Columns(14).Name=   "Cantidad_mez"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   3200
         Columns(15).Caption=   "Proceso"
         Columns(15).Name=   "Proceso"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         Columns(16).Width=   3200
         Columns(16).Caption=   "Asistencia"
         Columns(16).Name=   "Asistencia"
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).FieldLen=   256
         Columns(17).Width=   1244
         Columns(17).Caption=   "I.Cient?"
         Columns(17).Name=   "I.C"
         Columns(17).DataField=   "Column 17"
         Columns(17).DataType=   11
         Columns(17).FieldLen=   256
         Columns(17).Style=   2
         Columns(18).Width=   2037
         Columns(18).Caption=   "Dpto.Cargo"
         Columns(18).Name=   "Dpto.Cargo"
         Columns(18).DataField=   "Column 18"
         Columns(18).DataType=   8
         Columns(18).FieldLen=   256
         Columns(19).Width=   3200
         Columns(19).Caption=   "Departamento Cargo"
         Columns(19).Name=   "Departamento"
         Columns(19).DataField=   "Column 19"
         Columns(19).DataType=   8
         Columns(19).FieldLen=   256
         _ExtentX        =   20267
         _ExtentY        =   8493
         _StockProps     =   79
         Caption         =   "PERFIL FARMACOTERAP�UTICO"
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   22
      Top             =   8055
      Width           =   10680
      _ExtentX        =   18838
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmVerPerfilFTPEstupefac"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: FR0198.FRM                                                   *
'* AUTOR: IRENE V�ZWUEZ MARTINEZ                                        *
'* FECHA: ABRIL DEL 2000                                                *
'* DESCRIPCI�N: Consulta Perfil FTP Estupefacientes                     *
'* ARGUMENTOS:                                                          *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cmdaceptar_Click()
txtasistencia.Text = Grid2(0).Columns("Asistencia").Value
txtproceso.Text = Grid2(0).Columns("Proceso").Value
Grid2(0).Visible = False
FrameProcAsis.Visible = False
cmdaceptar.Visible = False
cmdcancelar.Visible = False
Grid2(0).RemoveAll
End Sub

Private Sub cmdcancelar_Click()
  Grid2(0).RemoveAll
  Grid2(0).Visible = False
  FrameProcAsis.Visible = False
  cmdaceptar.Visible = False
  cmdcancelar.Visible = False
End Sub

Private Sub cmdfiltrar_Click()
Dim strWhere As String
Dim stra As String
Dim rsta As rdoResultset
Dim strpersona As String
Dim rstpersona As rdoResultset
Dim rstprod As rdoResultset
Dim strprod  As String
Dim strdpto As String
Dim rstdpto As rdoResultset
Dim strdepartamento As String
Dim origen As String
Dim strproddil As String
Dim rstproddil As rdoResultset
Dim prod_dil As String
Dim descprod_dil As String
Dim cant_dil As String
Dim strprodmez As String
Dim rstprodmez As rdoResultset
Dim prod_mez As String
Dim descprod_mez As String
Dim cant_mez As String
Dim dpto As String
Dim UM As String

Screen.MousePointer = vbHourglass
cmdfiltrar.Enabled = False


'Medicamentos,Material,Todos
If Option2(0).Value = True Then 'Medicamentos
  strWhere = strWhere & " AND (FR7300.FR73INDPRODSAN=0 OR FR7300.FR73INDPRODSAN IS NULL)"
Else
  If Option2(1).Value = True Then 'Material
    strWhere = strWhere & " AND (FR7300.FR73INDPRODSAN=-1)"
  End If
End If

If Trim(txtproducto.Text) <> "" Then
  strprod = "SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73CODINTFAR=" & "'" & txtproducto.Text & "'"
  Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
  If Not rstprod.EOF Then
    strWhere = strWhere & " AND FR6500.FR73CODPRODUCTO=" & rstprod.rdoColumns("FR73CODPRODUCTO").Value
  End If
  rstprod.Close
  Set rstprod = Nothing
End If

'Origen :OM.PRN,Cesta,Quir�fano,Anestesia
If Option3(0).Value = True Then  'OM
  strWhere = strWhere & " AND FR6600.FR66INDOM=-1"
Else
  If Option3(1).Value = True Then 'PRN
    strWhere = strWhere & " AND (FR6600.FR66INDOM=0 OR FR6600.FR66INDOM IS NULL) AND " & _
               "(FR66INDCESTA=0 OR FR66INDCESTA IS NULL) AND " & _
               "(FR6500.FR65INDQUIROFANO=0 OR FR6500.FR65INDQUIROFANO IS NULL)"

  Else
    If Option3(2).Value = True Then 'Cesta
      strWhere = strWhere & " AND (FR6600.FR66INDOM=0 OR FR6600.FR66INDOM IS NULL) AND " & _
                 "(FR66INDCESTA=-1)"
    Else
        If Option3(3).Value = True Then 'Quir�fano
          strWhere = strWhere & " AND (FR6500.FR65INDQUIROFANO=-1) AND " & _
                 "(FR6500.PR62CODHOJAQUIR IS NOT NULL) AND (FR6500.FR65INDIMPUTQUIR=-1)"
        Else
          If Option3(4).Value = True Then 'Anestesia
            strWhere = strWhere & " AND (FR6500.FR65INDQUIROFANO=-1) AND " & _
                 "(FR6500.PR62CODHOJAQUIR IS NOT NULL) AND (FR6500.FR65INDIMPUTQUIR=0)"
          End If
        End If
    End If
  End If
End If

If Option1(0).Value = True Then 'Historia
  If IsNumeric(Text1(1).Text) Then
    strpersona = "SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
    Set rstpersona = objApp.rdoConnect.OpenResultset(strpersona)
    If Not rstpersona.EOF Then
      strWhere = strWhere & " AND FR6500.CI21CODPERSONA=" & rstpersona.rdoColumns("CI21CODPERSONA").Value
    End If
    rstpersona.Close
    Set rstpersona = Nothing
  Else
    Grid1(0).RemoveAll
    cmdfiltrar.Enabled = True
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
Else
  If Option1(3).Value = True Then 'Proceso/Asistencia
      If IsNumeric(txtproceso.Text) And IsNumeric(txtasistencia.Text) Then
          strWhere = strWhere & " AND FR6500.AD01CODASISTENCI=" & txtasistencia.Text & _
                   " AND FR6500.AD07CODPROCESO=" & txtproceso.Text
      Else
        Grid1(0).RemoveAll
        cmdfiltrar.Enabled = True
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
  Else
    If Option1(1).Value = True Then 'Proceso
      If IsNumeric(txtproceso.Text) Then
        strWhere = strWhere & " AND FR6500.AD07CODPROCESO=" & txtproceso.Text
      Else
        Grid1(0).RemoveAll
        cmdfiltrar.Enabled = True
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
    Else
      If Option1(2).Value = True Then  'Asistencia
        If IsNumeric(txtasistencia.Text) Then
          strWhere = strWhere & " AND FR6500.AD01CODASISTENCI=" & txtasistencia.Text
        Else
          Grid1(0).RemoveAll
          cmdfiltrar.Enabled = True
          Screen.MousePointer = vbDefault
          Exit Sub
        End If
      End If
    End If
  End If
End If

'fecha inicio/fecha fin
If IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
  strWhere = strWhere & " AND TRUNC(FR6500.FR65FECHA) BETWEEN " & _
           "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
           "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
End If
If IsDate(dtcinicio.Date) And Not IsDate(dtcfin.Date) Then
  strWhere = strWhere & " AND TRUNC(FR6500.FR65FECHA) BETWEEN " & _
           "TO_DATE('" & dtcinicio.Date & "','DD/MM/YYYY') AND " & _
           "TO_DATE('31/12/9999','DD/MM/YYYY')"
End If
If Not IsDate(dtcinicio.Date) And IsDate(dtcfin.Date) Then
  strWhere = strWhere & " AND TRUNC(FR6500.FR65FECHA) BETWEEN " & _
           "TO_DATE('01/01/1900','DD/MM/YYYY') AND " & _
           "TO_DATE('" & dtcfin.Date & "','DD/MM/YYYY')"
End If



Grid1(0).RemoveAll
stra = "SELECT FR6500.*,FR7300.FR73CODINTFAR,FR7300.FR73DESPRODUCTO,"
stra = stra & "CI2200.CI22NUMHISTORIA,FR6600.FR66INDCESTA,FR6600.FR66INDOM,FR6500.FR65INDQUIROFANO,FR6500.FR65INDIMPUTQUIR "
stra = stra & " FROM FR6500,FR7300,CI2200,FR6600 "
stra = stra & " WHERE FR6500.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO"
stra = stra & " AND FR6500.FR66CODPETICION=FR6600.FR66CODPETICION(+)"
stra = stra & " AND FR6500.CI21CODPERSONA=CI2200.CI21CODPERSONA" & strWhere
stra = stra & " ORDER BY FR6500.FR65FECHA DESC"

Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
  'U.M
  If IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
    UM = ""
  Else
    UM = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
  End If
  'departamento de cargo
  If Not IsNull(rsta.rdoColumns("AD02CODDPTO_CRG").Value) Then
    strdpto = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rsta.rdoColumns("AD02CODDPTO_CRG").Value
    Set rstdpto = objApp.rdoConnect.OpenResultset(strdpto)
    If Not IsNull(rstdpto.rdoColumns("AD02DESDPTO").Value) Then
      strdepartamento = rstdpto.rdoColumns("AD02DESDPTO").Value
    Else
      strdepartamento = ""
    End If
    rstdpto.Close
    Set rstdpto = Nothing
  End If
  'origen
  If rsta.rdoColumns("FR66INDOM").Value = -1 Then
    origen = "OM"
  End If
  If (rsta.rdoColumns("FR66INDOM").Value = 0 Or IsNull(rsta.rdoColumns("FR66INDOM").Value)) Then
     If (rsta.rdoColumns("FR66INDCESTA").Value = 0 Or IsNull(rsta.rdoColumns("FR66INDCESTA").Value)) Then
      If (rsta.rdoColumns("FR65INDQUIROFANO").Value = 0 Or IsNull(rsta.rdoColumns("FR65INDQUIROFANO").Value)) Then
         origen = "PRN"
      End If
     End If
     If (rsta.rdoColumns("FR66INDCESTA").Value = -1) Then
        origen = "Cesta"
     End If
  End If
  If (rsta.rdoColumns("FR65INDIMPUTQUIR").Value = -1) _
     And Not IsNull(rsta.rdoColumns("PR62CODHOJAQUIR").Value) Then
        origen = "Quir�fano"
  Else
    If (rsta.rdoColumns("FR65INDIMPUTQUIR").Value = 0) _
       And Not IsNull(rsta.rdoColumns("PR62CODHOJAQUIR").Value) Then
          origen = "Anestesia"
    End If
  End If
  
  'producto diluyente
  If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value) Then
    strproddil = "SELECT FR73CODINTFAR,FR73DESPRODUCTO FROM FR7300 WHERE "
    strproddil = strproddil & " FR73CODPRODUCTO=" & rsta.rdoColumns("FR73CODPRODUCTO_DIL").Value
    Set rstproddil = objApp.rdoConnect.OpenResultset(strproddil)
    If Not rstproddil.EOF Then
      prod_dil = rstproddil.rdoColumns("FR73CODINTFAR").Value
      descprod_dil = rstproddil.rdoColumns("FR73DESPRODUCTO").Value
    Else
      prod_dil = ""
      descprod_dil = ""
    End If
    rstproddil.Close
    Set rstproddil = Nothing
    If Not IsNull(rsta.rdoColumns("FR65CANTIDADDIL").Value) Then
      cant_dil = rsta.rdoColumns("FR65CANTIDADDIL").Value
    Else
      cant_dil = ""
    End If
  Else
    prod_dil = ""
    descprod_dil = ""
    cant_dil = ""
  End If
  
  'producto mezcla
  If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO_2").Value) Then
    strprodmez = "SELECT FR73CODINTFAR,FR73DESPRODUCTO FROM FR7300 WHERE "
    strprodmez = strprodmez & " FR73CODPRODUCTO=" & rsta.rdoColumns("FR73CODPRODUCTO_2").Value
    Set rstprodmez = objApp.rdoConnect.OpenResultset(strprodmez)
    If Not rstprodmez.EOF Then
      prod_mez = rstprodmez.rdoColumns("FR73CODINTFAR").Value
      descprod_mez = rstprodmez.rdoColumns("FR73DESPRODUCTO").Value
    Else
      prod_mez = ""
      descprod_mez = ""
    End If
    rstprodmez.Close
    Set rstprodmez = Nothing
    If Not IsNull(rsta.rdoColumns("FR65DOSIS_2").Value) Then
      cant_mez = rsta.rdoColumns("FR65DOSIS_2").Value
    Else
      cant_mez = ""
    End If
  Else
    prod_mez = ""
    descprod_mez = ""
    cant_mez = ""
  End If
  
    
  Grid1(0).AddItem origen & Chr(vbKeyTab) & _
                   rsta.rdoColumns("CI22NUMHISTORIA").Value & Chr(vbKeyTab) & _
                   rsta.rdoColumns("FR65FECHA").Value & Chr(vbKeyTab) & _
                   rsta.rdoColumns("FR73CODINTFAR").Value & Chr(vbKeyTab) & _
                   rsta.rdoColumns("FR73DESPRODUCTO").Value & Chr(vbKeyTab) & _
                   rsta.rdoColumns("FR65CANTIDAD").Value & Chr(vbKeyTab) & _
                   rsta.rdoColumns("FR65DOSIS").Value & Chr(vbKeyTab) & _
                   UM & Chr(vbKeyTab) & _
                   rsta.rdoColumns("FR34CODVIA").Value & Chr(vbKeyTab) & _
                   prod_dil & Chr(vbKeyTab) & _
                   descprod_dil & Chr(vbKeyTab) & _
                   cant_dil & Chr(vbKeyTab) & _
                   prod_mez & Chr(vbKeyTab) & _
                   descprod_mez & Chr(vbKeyTab) & _
                   cant_mez & Chr(vbKeyTab) & _
                   rsta.rdoColumns("AD07CODPROCESO").Value & Chr(vbKeyTab) & _
                   rsta.rdoColumns("AD01CODASISTENCI").Value & Chr(vbKeyTab) & _
                   rsta.rdoColumns("FR65INDINTERCIENT").Value & Chr(vbKeyTab) & _
                   rsta.rdoColumns("AD02CODDPTO_CRG").Value & Chr(vbKeyTab) & _
                   strdepartamento
rsta.MoveNext
Wend

cmdfiltrar.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub cmdprocasis_Click()
Dim stra  As String
Dim rsta As rdoResultset
Dim i As Integer
Dim fechafin As Date
Dim varfechafin
Dim varfechainicio
Dim rstpaciente As rdoResultset
Dim strpaciente As String

Screen.MousePointer = vbHourglass
cmdprocasis.Enabled = False

FrameProcAsis.Visible = True
FrameProcAsis.ZOrder (0)
Grid2(0).Visible = True
cmdaceptar.Visible = True
cmdcancelar.Visible = True

For i = 0 To 4
  Grid2(0).Columns(i).Locked = True
Next i
'se busca el c�digo del paciente
If IsNumeric(Text1(1).Text) Then
  strpaciente = "SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
  Set rstpaciente = objApp.rdoConnect.OpenResultset(strpaciente)
  If Not rstpaciente.EOF Then
    'se carga el grid de procesos y asistencias del paciente
    'stra = "SELECT AD07CODPROCESO,AD01CODASISTENCI,AD08FECINICIO,TO_DATE(TO_CHAR(AD08FECFIN,'DD/MM/YYYY'),'DD/MM/YYYY') FROM AD0803J WHERE " & _
           "AD01CODASISTENCI IN (SELECT AD01CODASISTENCI FROM AD0100 " & _
                              " WHERE TO_DATE(TO_CHAR(AD01FECINICIO,'DD/MM/YYYY'),'DD/MM/YYYY')<=(SELECT TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY') FROM DUAL))" & _
           " AND CI21CODPERSONA=" & rstpaciente.rdoColumns("CI21CODPERSONA").Value
    stra = "SELECT AD0800.AD07CODPROCESO,AD0800.AD01CODASISTENCI,AD0800.AD08FECINICIO,"
    stra = stra & "TO_DATE(TO_CHAR(AD0800.AD08FECFIN,'DD/MM/YYYY HH24:MI'),'DD/MM/YYYY HH24:MI')"
    stra = stra & " FROM AD0800, AD0100"
    stra = stra & " WHERE AD0800.AD01CODASISTENCI = AD0100.AD01CODASISTENCI"
    stra = stra & " AND  AD0800.AD34CODESTADO = 1"
    stra = stra & " AND AD0100.CI21CODPERSONA=" & rstpaciente.rdoColumns("CI21CODPERSONA").Value
    stra = stra & " ORDER BY AD0800.AD08FECINICIO DESC"
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    While Not rsta.EOF
      If IsNull(rsta.rdoColumns(3).Value) Then
        varfechafin = ""
      Else
        varfechafin = rsta.rdoColumns(3).Value
      End If
      Grid2(0).AddItem rsta.rdoColumns("AD07CODPROCESO").Value & Chr(vbKeyTab) & _
                       rsta.rdoColumns("AD01CODASISTENCI").Value & Chr(vbKeyTab) & _
                       Chr(vbKeyTab) & _
                       rsta.rdoColumns("AD08FECINICIO").Value & Chr(vbKeyTab) & varfechafin
    rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
  End If
  rstpaciente.Close
  Set rstpaciente = Nothing
End If

cmdprocasis.Enabled = True
Screen.MousePointer = vbDefault
End Sub

Private Sub Form_Activate()
Call Inicializar_Toolbar
Call Inicializar_Grid
End Sub



Private Sub Grid2_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim selectCama As String
Dim rdoCama As rdoResultset

If Index = 0 Then
  selectCama = "SELECT GCFN06(AD15CODCAMA) FROM AD1500 WHERE "
  selectCama = selectCama & " AD07CODPROCESO="
  selectCama = selectCama & Grid2(0).Columns("Proceso").Value
  selectCama = selectCama & " AND AD01CODASISTENCI="
  selectCama = selectCama & Grid2(0).Columns("Asistencia").Value
  selectCama = selectCama & " AND AD14CODESTCAMA=2"
  Set rdoCama = objApp.rdoConnect.OpenResultset(selectCama)
  If Not rdoCama.EOF Then
    Grid2(0).Columns("Cama").Value = rdoCama(0).Value
  Else
    Grid2(0).Columns("Cama").Value = ""
  End If
  rdoCama.Close
  Set rdoCama = Nothing
End If
End Sub



Private Sub Text1_Change(Index As Integer)
If Index = 1 Then
  txtproceso.Text = ""
  txtasistencia.Text = ""
  Grid1(0).RemoveAll
  Text1(2).Text = ""
End If
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim strpaciente As String

If KeyAscii = 13 Then
  If IsNumeric(Text1(1).Text) Then
      stra = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI22NUMHISTORIA=" & Text1(1).Text
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      'If Not IsNull(rsta.rdoColumns("CI22NUMHISTORIA").Value) Then
      '  Text1(1).Text = rsta.rdoColumns("CI22NUMHISTORIA").Value
      'End If
      If Not IsNull(rsta.rdoColumns("CI22NOMBRE").Value) Then
        strpaciente = rsta.rdoColumns("CI22NOMBRE").Value
      End If
      If Not IsNull(rsta.rdoColumns("CI22PRIAPEL").Value) Then
        strpaciente = strpaciente & " " & rsta.rdoColumns("CI22PRIAPEL").Value
      End If
      If Not IsNull(rsta.rdoColumns("CI22SEGAPEL").Value) Then
        strpaciente = strpaciente & " " & rsta.rdoColumns("CI22SEGAPEL").Value
      End If
      Text1(2).Text = strpaciente
      rsta.Close
      Set rsta = Nothing
  End If
End If
End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String
    Dim stra As String
    Dim rsta As rdoResultset
    Dim strpaciente As String
  
    Set objWinInfo = New clsCWWin
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)

If IsNumeric(glngPaciente) Then
  stra = "SELECT CI22NUMHISTORIA,CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI21CODPERSONA=" & glngPaciente
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not IsNull(rsta.rdoColumns("CI22NUMHISTORIA").Value) Then
    Text1(1).Text = rsta.rdoColumns("CI22NUMHISTORIA").Value
  End If
  If Not IsNull(rsta.rdoColumns("CI22NOMBRE").Value) Then
    strpaciente = rsta.rdoColumns("CI22NOMBRE").Value
  End If
  If Not IsNull(rsta.rdoColumns("CI22PRIAPEL").Value) Then
    strpaciente = strpaciente & " " & rsta.rdoColumns("CI22PRIAPEL").Value
  End If
  If Not IsNull(rsta.rdoColumns("CI22SEGAPEL").Value) Then
    strpaciente = strpaciente & " " & rsta.rdoColumns("CI22SEGAPEL").Value
  End If
  Text1(2).Text = strpaciente
  rsta.Close
  Set rsta = Nothing
  Text1(1).Locked = True
  Text1(2).Locked = True
End If

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  '  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
   ' intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    'intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
   ' Call objWinInfo.WinDeRegister
   ' Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)

End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Select Case btnButton.Index
  Case 21:
          Call Grid1(0).MoveFirst
  Case 22:
          Call Grid1(0).MovePrevious
  Case 23:
          Call Grid1(0).MoveNext
  Case 24:
          Call Grid1(0).MoveLast
  Case 26:
          Call Inicializar_Grid
          Call Grid1(0).Refresh
  Case 30:
          Unload Me
End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
If intIndex = 100 Then
  Unload Me
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
Select Case intIndex
  Case 40:
          Call Grid1(0).MoveFirst
  Case 50:
          Call Grid1(0).MovePrevious
  Case 60:
          Call Grid1(0).MoveNext
  Case 70:
          Call Grid1(0).MoveLast
End Select
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  If intIndex = 10 Then
          Call Inicializar_Grid
          Call Grid1(0).Refresh
  End If
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    'Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    'Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub
Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   tlbToolbar1.Buttons.Item(4).Enabled = False
   tlbToolbar1.Buttons.Item(6).Enabled = False
   tlbToolbar1.Buttons.Item(8).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuDatosOpcion(80).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub

Private Sub Inicializar_Grid()
Dim i As Integer

For i = 0 To 19
  Grid1(0).Columns(i).Locked = True
Next i

Grid1(0).Columns(1).Width = 900  'historia
Grid1(0).Columns(2).Width = 1100 'fecha
Grid1(0).Columns(3).Width = 900  'interno
Grid1(0).Columns(4).Width = 3300 'producto
Grid1(0).Columns(5).Width = 800  'cantidad
Grid1(0).Columns(6).Width = 800  'dosis
Grid1(0).Columns(7).Width = 700  'UM
Grid1(0).Columns(8).Width = 800  'v�a
Grid1(0).Columns(9).Width = 900  'interno_dil
Grid1(0).Columns(10).Width = 3300 'producto_dil
Grid1(0).Columns(11).Width = 800  'cantidad_dil
Grid1(0).Columns(12).Width = 900  'interno_mez
Grid1(0).Columns(13).Width = 3300 'producto_mez
Grid1(0).Columns(14).Width = 850  'cantidad_mez
Grid1(0).Columns(15).Width = 1100  'proceso
Grid1(0).Columns(16).Width = 1100  'asistencia
Grid1(0).Columns(18).Visible = False 'c�d.dpto.cargo

End Sub

