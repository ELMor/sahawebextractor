VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmBusGrpProdPRD 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Grupos de Medicamentos"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdtraerprod 
      Caption         =   "Traer Medicamentos"
      Height          =   375
      Left            =   10320
      TabIndex        =   17
      Top             =   5880
      Width           =   1575
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Grupo de Medicamentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Index           =   1
      Left            =   480
      TabIndex        =   8
      Top             =   480
      Width           =   11100
      Begin TabDlg.SSTab tabTab1 
         Height          =   2295
         Index           =   0
         Left            =   240
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   360
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblLabel1(14)"
         Tab(0).Control(1)=   "lblLabel1(28)"
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(3)=   "lblLabel1(2)"
         Tab(0).Control(4)=   "lblLabel1(0)"
         Tab(0).Control(5)=   "dtcDateCombo1(1)"
         Tab(0).Control(6)=   "dtcDateCombo1(0)"
         Tab(0).Control(7)=   "txtText1(0)"
         Tab(0).Control(8)=   "txtText1(1)"
         Tab(0).Control(9)=   "txtText1(2)"
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   2
            Left            =   -70080
            TabIndex        =   4
            Tag             =   "C�digo Servicio"
            Top             =   1680
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR41DESGRUPPROD"
            Height          =   690
            Index           =   1
            Left            =   -72840
            MultiLine       =   -1  'True
            TabIndex        =   1
            Tag             =   "Descripci�n Grupo Producto"
            Top             =   360
            Width           =   7860
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR41CODGRUPPROD"
            Height          =   330
            Index           =   0
            Left            =   -74760
            TabIndex        =   0
            Tag             =   "C�digo Grupo Producto"
            Top             =   360
            Width           =   1140
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2265
            Index           =   2
            Left            =   0
            TabIndex        =   11
            Top             =   0
            Width           =   10575
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18653
            _ExtentY        =   3995
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR41FECINIVIGGP"
            Height          =   330
            Index           =   0
            Left            =   -74760
            TabIndex        =   2
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   1680
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR41FECFINVIGGP"
            Height          =   330
            Index           =   1
            Left            =   -72360
            TabIndex        =   3
            Tag             =   "Fecha Fin Vigencia"
            Top             =   1680
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -70080
            TabIndex        =   18
            Top             =   1440
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   -74760
            TabIndex        =   16
            Top             =   1440
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   -72360
            TabIndex        =   15
            Top             =   1440
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Grupo Prod"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   -74760
            TabIndex        =   14
            Top             =   120
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Grupo Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   -72840
            TabIndex        =   12
            Top             =   120
            Width           =   2535
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Medicamentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4680
      Index           =   0
      Left            =   480
      TabIndex        =   6
      Top             =   3360
      Width           =   9735
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4185
         Index           =   0
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   9480
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   16722
         _ExtentY        =   7382
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   5
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmBusGrpProdPRD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmBusGrpProdPOM (FR0167.FRM)                                *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: buscar Grupo Medicamentos                               *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim blnnoactivate As Boolean

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cmdtraerprod_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset
Dim strinsert As String
Dim rstrest As rdoResultset
Dim strrest As String

cmdtraerprod.Enabled = False
If objWinInfo.objWinActiveForm.strName = "Productos" Then
'Guardamos el n�mero de filas seleccionadas
mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
ReDim gintprodbuscado(mintNTotalSelRows, 9)
gintprodtotal = mintNTotalSelRows
For mintisel = 0 To mintNTotalSelRows - 1
    'Guardamos el n�mero de fila que est� seleccionada
     mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
     gintprodbuscado(mintisel, 0) = "" & grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk) & "" 'c�digo
     gintprodbuscado(mintisel, 1) = grdDBGrid1(0).Columns(5).CellValue(mvarBkmrk) 'c�d.interno
     gintprodbuscado(mintisel, 2) = grdDBGrid1(0).Columns(7).CellValue(mvarBkmrk) 'descripci�n
     
     gintprodbuscado(mintisel, 3) = grdDBGrid1(0).Columns(10).CellValue(mvarBkmrk)
     gintprodbuscado(mintisel, 4) = grdDBGrid1(0).Columns(11).CellValue(mvarBkmrk)
     gintprodbuscado(mintisel, 5) = grdDBGrid1(0).Columns(6).CellValue(mvarBkmrk)
     gintprodbuscado(mintisel, 6) = grdDBGrid1(0).Columns(14).CellValue(mvarBkmrk)
     gintprodbuscado(mintisel, 7) = grdDBGrid1(0).Columns(15).CellValue(mvarBkmrk)
     gintprodbuscado(mintisel, 8) = grdDBGrid1(0).Columns(8).CellValue(mvarBkmrk)
     gintprodbuscado(mintisel, 9) = grdDBGrid1(0).Columns(9).CellValue(mvarBkmrk)
     '--------------------------------------------------------------------------------
'     'para cada producto hay que mirar si es de uso restringido
'       stra = "SELECT FR73INDUSOREST FROM FR7300 WHERE FR73CODPRODUCTO=" & _
'               grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk)
'       Set rsta = objApp.rdoConnect.OpenResultset(stra)
'       If rsta.rdoColumns(0).Value = -1 Then
'          'si es de uso restringido se llama a la pantalla de restricciones,pero antes
'          'se vuelca el contenido de FRA900 en FRA400
'              stra = "SELECT * FROM FRA900 WHERE FR73CODPRODUCTO=" & _
'                     grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk)
'              Set rsta = objApp.rdoConnect.OpenResultset(stra)
'              While Not rsta.EOF
'                  strrest = "SELECT FRA9CODREST FROM FRA400 WHERE FR73CODPRODUCTO=" & _
'                            grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk) & " AND " & _
'                            "FR66CODPETICION=" & glngcodpeticion & " AND " & _
'                            "FRA9CODREST=" & rsta.rdoColumns("FRA9CODREST").Value
'                  Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
'                  If rstrest.EOF Then
'                    strinsert = "INSERT INTO FRA400(FR73CODPRODUCTO,FR66CODPETICION," & _
'                              "FRA9CODREST,FRA4DESCREST,FRA4INDSELECCIONADO) VALUES(" & _
'                              grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk) & "," & _
'                              glngcodpeticion & "," & _
'                              rsta.rdoColumns("FRA9CODREST").Value & "," & _
'                              "'" & rsta.rdoColumns("FRA9DESREST").Value & "'" & "," & _
'                              "0" & ")"
'                    objApp.rdoConnect.Execute strinsert, 64
'                    objApp.rdoConnect.Execute "Commit", 64
'                  End If
'                  rstrest.Close
'                  Set rstrest = Nothing
'              rsta.MoveNext
'              Wend
'              rsta.Close
'              Set rsta = Nothing
'              glngcodprod = grdDBGrid1(0).Columns(4).CellValue(mvarBkmrk)
'              Call objsecurity.LaunchProcess("FR0182")
'       End If
     '--------------------------------------------------------------------------------
Next mintisel
cmdtraerprod.Enabled = True
Unload Me
Else
mensaje = MsgBox("No ha seleccionado ning�n medicamento", vbInformation, "Aviso")
cmdtraerprod.Enabled = True
End If
End Sub

Private Sub Form_Activate()
If blnnoactivate = False Then
  blnnoactivate = True
  'se va al primer registro para refrescar el grid
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
 End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Grupos de Productos"
      
    .strTable = "FR4100"
    .intAllowance = cwAllowReadOnly
    If gstrLlamadorProd = "frmPedirPrn" Then
    '  .strWhere = "FR41FECINIVIGGP<(SELECT SYSDATE FROM DUAL) AND " & _
    '     "((FR41FECFINVIGGP IS NULL) OR (FR41FECFINVIGGP>(SELECT SYSDATE FROM DUAL))) AND " & _
    '     " FRI7CODTIPGRP = 3 AND AD02CODDPTO=" & frmPedirPRN.txtText1(16).Text
    Else
      .strWhere = "FR41FECINIVIGGP<(SELECT SYSDATE FROM DUAL) AND " & _
         "((FR41FECFINVIGGP IS NULL) OR (FR41FECFINVIGGP>(SELECT SYSDATE FROM DUAL))) "
    End If
    Call .FormAddOrderField("FR41CODGRUPPROD", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Grupos de Medicamentos")
    Call .FormAddFilterWhere(strKey, "FR41CODGRUPPROD", "C�digo Grupo Medicamento", cwString)
    Call .FormAddFilterWhere(strKey, "FR41DESGRUPPROD", "Descripci�n Grupo Medicamento", cwString)
    Call .FormAddFilterWhere(strKey, "FR41FECINIVIGGP", "Fecha Inicio Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR41FECFINVIGGP", "Fecha Fin Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)

    Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "C�digo Grupo Medicamento")
    Call .FormAddFilterOrder(strKey, "FR41DESGRUPPROD", "Descripci�n Grupo Medicamento")
    Call .FormAddFilterOrder(strKey, "FR41FECINIVIGGP", "Fecha Inicio Vigencia")
    Call .FormAddFilterOrder(strKey, "FR41FECFINVIGGP", "Fecha Fin Vigencia")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Servicio")


  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Productos"
    
    '.strTable = "FR7309J"
    .strTable = "FR7315J"
    .intAllowance = cwAllowReadOnly
    
    If gstrLlamadorProd = "frmDefProtocolosGM" Then
      '.strWhere = "(FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 " & _
                  "WHERE (FR73INDMATIMP=0 OR FR73INDMATIMP IS NULL )" & _
                  " and (FR73INDPRODSAN=0 OR FR73INDPRODSAN IS NULL ))) "
       .strWhere = "(FR73INDMATIMP=0 OR FR73INDMATIMP IS NULL )" & _
                  " and (FR73INDPRODSAN=0 OR FR73INDPRODSAN IS NULL)"
    End If
    
    Call .FormAddOrderField("FR73DESPRODUCTO", cwAscending)
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
    Call .FormAddRelation("FR41CODGRUPPROD", txtText1(0))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Medicamentos")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Medicamento", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR00CODGRPTERAP", "C�digo Grupo Terape�tico", cwString)
    Call .FormAddFilterWhere(strKey, "FR73DOSIS", "Dosis", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "Unidad de Medida", cwString)
    Call .FormAddFilterWhere(strKey, "FR73VOLUMEN", "Volumen", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR34CODVIA", "C�digo V�a", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73INDESTUPEFACI", "Estupefaciente?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDPSICOT", "Psicotr�pico", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDUSOHOSP", "Uso Hospitalario", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDDIAGHOSP", "Diagnostico Hospitalario", cwBoolean)
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
  
    Call .GridAddColumn(objMultiInfo, "Grupo", "FR41CODGRUPPROD", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "C�d.Medicamento", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�d. Interno", "FR73CODINTFAR", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Referencia", "FR73REFERENCIA", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Descripci�n", "FR73DESPRODUCTO", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Tam Env", "FR73TAMENVASE", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "F.F", "FRH7CODFORMFAR", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR73DOSIS", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "U.M", "FR93CODUNIMEDIDA", cwString, 9)
    Call .GridAddColumn(objMultiInfo, "Grupo Terape�tico", "FR00CODGRPTERAP", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Desc.Grupo Terape�tico", "", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Volumen", "FR73VOLUMEN", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Estupefaciente?", "FR73INDESTUPEFACI", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Psicotr�pico?", "FR73INDPSICOT", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Uso Hospitalario?", "FR73INDUSOHOSP", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Diagnostico Hospitalario?", "FR73INDDIAGHOSP", cwBoolean)
    
   
    
    Call .FormCreateInfo(objMasterInfo)

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnInFind = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(13)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(10)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(14)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(15)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(16)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(17)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(18)).blnInFind = True
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(8), "FR73TAMENVASE")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), "FR00CODGRPTERAP", "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), grdDBGrid1(0).Columns(13), "FR00DESGRPTERAP")
    grdDBGrid1(0).Columns(3).Visible = False
    grdDBGrid1(0).Columns(4).Visible = False
    grdDBGrid1(0).Columns(12).Visible = False
    grdDBGrid1(0).Columns(13).Visible = False
    grdDBGrid1(0).Columns(14).Visible = False
    grdDBGrid1(0).Columns(15).Visible = False
    grdDBGrid1(0).Columns(16).Visible = False
    grdDBGrid1(0).Columns(17).Visible = False
    grdDBGrid1(0).Columns(18).Visible = False
    grdDBGrid1(0).Columns(19).Visible = False
    'grdDBGrid1(0).Columns(5).Visible = False
    grdDBGrid1(0).Columns(5).Width = 700
    grdDBGrid1(0).Columns(6).Width = 1200
    grdDBGrid1(0).Columns(7).Width = 3500
    grdDBGrid1(0).Columns(8).Width = 800
    grdDBGrid1(0).Columns(9).Width = 500
    grdDBGrid1(0).Columns(10).Width = 700
    grdDBGrid1(0).Columns(12).Width = 1500
    Call .WinRegister
    Call .WinStabilize
    
  End With

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strFormName = "Grupos de Productos" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     
     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Servicio"
         
     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Servicio"
     
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub



Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


