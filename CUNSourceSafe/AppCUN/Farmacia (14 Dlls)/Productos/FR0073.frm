VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmBusProductosPRD 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA.Productos"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   30
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CheckBox chkmedicamento 
      Caption         =   "Medicamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1080
      TabIndex        =   54
      Top             =   1920
      Value           =   1  'Checked
      Width           =   1575
   End
   Begin VB.CheckBox chksolucion 
      Caption         =   "Soluci�n Intravenosa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2760
      TabIndex        =   53
      Top             =   1920
      Width           =   2175
   End
   Begin VB.CheckBox chkvisible 
      Caption         =   "Visible"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6000
      TabIndex        =   52
      Top             =   960
      Width           =   975
   End
   Begin VB.CheckBox chkproducto 
      Caption         =   "�Nombre Comercial?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C000C0&
      Height          =   255
      Left            =   9120
      TabIndex        =   8
      ToolTipText     =   "�Nombre Comercial?"
      Top             =   1440
      Width           =   2175
   End
   Begin VB.CheckBox chkprincipioactivo 
      Caption         =   "�Principio Activo?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C000C0&
      Height          =   255
      Left            =   9120
      TabIndex        =   9
      Top             =   1800
      Width           =   1935
   End
   Begin VB.CommandButton cmdfiltrar 
      Caption         =   "Filtrar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9120
      TabIndex        =   7
      Top             =   960
      Width           =   1215
   End
   Begin VB.CheckBox chkdiagnostico 
      Caption         =   "Diagn�stico Hospitalario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6000
      TabIndex        =   6
      Top             =   2040
      Width           =   2415
   End
   Begin VB.CheckBox chkusohospitalario 
      Caption         =   "Uso Hospitalario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6000
      TabIndex        =   4
      Top             =   1320
      Width           =   1815
   End
   Begin VB.CheckBox chkpsicotropico 
      Caption         =   "Psicotr�pico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6000
      TabIndex        =   5
      Top             =   1680
      Width           =   1575
   End
   Begin VB.TextBox txtdescgrupo 
      Height          =   315
      Left            =   2400
      TabIndex        =   2
      ToolTipText     =   "Descripci�n Grupo Terape�tico"
      Top             =   1560
      Width           =   3015
   End
   Begin VB.TextBox txtcodgrupo 
      Height          =   315
      Left            =   120
      TabIndex        =   1
      ToolTipText     =   "C�d. Grupo Terape�tico"
      Top             =   1560
      Width           =   2055
   End
   Begin VB.CheckBox chkestupefaciente 
      Caption         =   "Estupefaciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6000
      TabIndex        =   3
      Top             =   600
      Width           =   1935
   End
   Begin VB.TextBox txtdescripcion 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      ToolTipText     =   "DEscripci�n"
      Top             =   840
      Width           =   5295
   End
   Begin VB.CommandButton cdmtraer 
      Caption         =   "Traer Medicamentos"
      Height          =   375
      Left            =   9120
      TabIndex        =   32
      Top             =   6600
      Width           =   1695
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Medicamentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Index           =   1
      Left            =   120
      TabIndex        =   31
      Top             =   2280
      Width           =   11580
      Begin TabDlg.SSTab tabTab1 
         Height          =   3015
         Index           =   0
         Left            =   120
         TabIndex        =   33
         TabStop         =   0   'False
         Top             =   360
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   5318
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0073.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblLabel1(22)"
         Tab(0).Control(1)=   "lblLabel1(28)"
         Tab(0).Control(2)=   "lblLabel1(14)"
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(4)=   "lblLabel1(1)"
         Tab(0).Control(5)=   "lblLabel1(3)"
         Tab(0).Control(6)=   "lblLabel1(4)"
         Tab(0).Control(7)=   "lblLabel1(5)"
         Tab(0).Control(8)=   "lblLabel1(7)"
         Tab(0).Control(9)=   "lblLabel1(8)"
         Tab(0).Control(10)=   "lblLabel1(2)"
         Tab(0).Control(11)=   "lblLabel1(6)"
         Tab(0).Control(12)=   "lblLabel1(9)"
         Tab(0).Control(13)=   "txtText1(22)"
         Tab(0).Control(14)=   "txtText1(1)"
         Tab(0).Control(15)=   "txtText1(0)"
         Tab(0).Control(16)=   "txtText1(2)"
         Tab(0).Control(17)=   "txtText1(3)"
         Tab(0).Control(18)=   "chkCheck1(0)"
         Tab(0).Control(19)=   "chkCheck1(1)"
         Tab(0).Control(20)=   "chkCheck1(2)"
         Tab(0).Control(21)=   "chkCheck1(3)"
         Tab(0).Control(22)=   "txtText1(5)"
         Tab(0).Control(23)=   "txtText1(6)"
         Tab(0).Control(24)=   "txtText1(7)"
         Tab(0).Control(25)=   "txtText1(9)"
         Tab(0).Control(26)=   "txtText1(10)"
         Tab(0).Control(27)=   "txtText1(4)"
         Tab(0).Control(28)=   "txtText1(8)"
         Tab(0).Control(29)=   "txtText1(11)"
         Tab(0).ControlCount=   30
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0073.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            DataField       =   "FRH7CODFORMFAR"
            Height          =   330
            Index           =   11
            Left            =   -66960
            TabIndex        =   17
            Tag             =   "FF"
            Top             =   2040
            Width           =   860
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   8
            Left            =   -68520
            TabIndex        =   16
            Tag             =   "Tama�o"
            Top             =   1920
            Width           =   860
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR73REFERENCIA"
            Height          =   330
            Index           =   4
            Left            =   -67080
            TabIndex        =   14
            Tag             =   "Referencia"
            Top             =   1320
            Width           =   860
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR34CODVIA"
            Height          =   330
            Index           =   10
            Left            =   -68520
            TabIndex        =   21
            Tag             =   "V�a"
            Top             =   1320
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR73VOLUMEN"
            Height          =   330
            Index           =   9
            Left            =   -69840
            TabIndex        =   20
            Tag             =   "Volumen"
            Top             =   1320
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR93CODUNIMEDIDA"
            Height          =   330
            Index           =   7
            Left            =   -71520
            TabIndex        =   19
            Tag             =   "UM"
            Top             =   1920
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR73DOSIS"
            Height          =   330
            Index           =   6
            Left            =   -73080
            TabIndex        =   18
            Tag             =   "Dosis "
            Top             =   1920
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR73INDICACIONES"
            Height          =   330
            Index           =   5
            Left            =   -74760
            TabIndex        =   26
            Tag             =   "Indicaciones"
            Top             =   1920
            Width           =   1020
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Diagn�stico Hospitalario"
            DataField       =   "FR73INDDIAGHOSP"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   -68640
            TabIndex        =   25
            Tag             =   "Diagn�stico Hospitalario?"
            Top             =   2400
            Width           =   3255
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Uso Hospitalario"
            DataField       =   "FR73INDUSOHOSP"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   -72720
            TabIndex        =   24
            Tag             =   "Uso Hospitalario?"
            Top             =   2400
            Width           =   1935
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Psicotr�pico"
            DataField       =   "FR73INDPSICOT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -70440
            TabIndex        =   23
            Tag             =   "Psicotr�pico?"
            Top             =   2400
            Width           =   2415
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Estupefaciente"
            DataField       =   "FR73INDESTUPEFACI"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -74640
            TabIndex        =   22
            Tag             =   "Estupefaciente?"
            Top             =   2400
            Width           =   1815
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR00DESGRPTERAP"
            Height          =   330
            Index           =   3
            Left            =   -72480
            TabIndex        =   13
            Tag             =   "Descripci�n Grupo Terape�tico"
            Top             =   1320
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR00CODGRPTERAP"
            Height          =   330
            Index           =   2
            Left            =   -74760
            TabIndex        =   11
            Tag             =   "C�digo Grupo Terape�tico"
            Top             =   1320
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            Index           =   0
            Left            =   -74880
            TabIndex        =   10
            Tag             =   "C�digo Medicamento"
            Top             =   360
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73DESPRODUCTO"
            Height          =   570
            Index           =   1
            Left            =   -72120
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   15
            Tag             =   "Descripci�n Medicamento"
            Top             =   360
            Width           =   6420
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR73CODINTFAR"
            Height          =   330
            Index           =   22
            Left            =   -73560
            TabIndex        =   12
            Tag             =   "C�digo Interno"
            Top             =   360
            Width           =   860
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   34
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2985
            Index           =   2
            Left            =   0
            TabIndex        =   35
            TabStop         =   0   'False
            Top             =   0
            Width           =   11175
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19711
            _ExtentY        =   5265
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            Caption         =   "FF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   -66960
            TabIndex        =   51
            Top             =   1800
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "TAMA�O"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   -68520
            TabIndex        =   50
            Top             =   1680
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   -67080
            TabIndex        =   49
            Top             =   1080
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "V�a"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   -68520
            TabIndex        =   48
            Top             =   1080
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Volumen"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   -69840
            TabIndex        =   47
            Top             =   1080
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Unidad de Medida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   -71520
            TabIndex        =   43
            Top             =   1680
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dosis Usual"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   -73080
            TabIndex        =   42
            Top             =   1680
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Indicaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   -74760
            TabIndex        =   41
            Top             =   1680
            Width           =   2175
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Desc.Grupo Terape�tico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -72480
            TabIndex        =   40
            Top             =   1080
            Width           =   2175
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Grupo Terape�tico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -74760
            TabIndex        =   39
            Top             =   1080
            Width           =   2175
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Medicamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   -72120
            TabIndex        =   38
            Top             =   120
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   -74880
            TabIndex        =   37
            Top             =   120
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Interno"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   22
            Left            =   -73560
            TabIndex        =   36
            Top             =   120
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Principios Activos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Index           =   0
      Left            =   120
      TabIndex        =   28
      Top             =   5880
      Width           =   8655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1635
         Index           =   0
         Left            =   120
         TabIndex        =   29
         Top             =   360
         Width           =   8385
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   14790
         _ExtentY        =   2884
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   27
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Desc. Grupo Terape�tico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2400
      TabIndex        =   46
      Top             =   1320
      Width           =   2295
   End
   Begin VB.Label lblcodgrupo 
      Caption         =   "C�d. Grupo Terape�tico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   45
      Top             =   1320
      Width           =   2295
   End
   Begin VB.Label lbldescripcion 
      Caption         =   "Descripci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   44
      Top             =   600
      Width           =   1455
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmBusProductosPRD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmBusProductosPOM(FR0073.FRM)                                *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: buscar productos                                        *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim strfrmRedactarOM As String
Dim blnnoactivate As Boolean


Private Sub chkmedicamento_Click()
If chkmedicamento.Value = 0 Then
  chkmedicamento.Value = 1
End If

End Sub

Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub

Private Sub cdmtraer_Click()
Dim mintisel As Integer
Dim mintNTotalSelRows As Integer
Dim mvarBkmrk As Variant
Dim mensaje As String
Dim rsta As rdoResultset
Dim stra As String
Dim strinsert As String
Dim rstrest As rdoResultset
Dim strrest As String

cdmtraer.Enabled = False
If objWinInfo.objWinActiveForm.strName = "Productos" Then
  If gstrLlamadorProd = "frmDefProtocolosmed2" Or gstrLlamadorProd = "frmDefProtocolossoldil" Or gstrLlamadorProd = "frmDefProtocolossolFlui" Or gstrLlamadorProd = "frmDefProtocolosFlui" Then
    ReDim gintprodbuscado(1, 1)
    gintprodtotal = 1
    gintprodbuscado(0, 0) = "" & grdDBGrid1(2).Columns(1).CellValue(mvarBkmrk) & "" ''c�digo
  Else
    mintNTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
    ReDim gintprodbuscado(mintNTotalSelRows, 9)
    'Guardamos el n�mero de filas seleccionadas
    gintprodtotal = mintNTotalSelRows
    For mintisel = 0 To mintNTotalSelRows - 1
      'Guardamos el n�mero de fila que est� seleccionada
       mvarBkmrk = grdDBGrid1(2).SelBookmarks(mintisel)
       gintprodbuscado(mintisel, 0) = "" & grdDBGrid1(2).Columns(1).CellValue(mvarBkmrk) & "" ''c�digo
       gintprodbuscado(mintisel, 1) = grdDBGrid1(2).Columns(3).CellValue(mvarBkmrk) ''c�d.interno
       gintprodbuscado(mintisel, 2) = grdDBGrid1(2).Columns(6).CellValue(mvarBkmrk) ''descripci�n
       gintprodbuscado(mintisel, 3) = grdDBGrid1(2).Columns(8).CellValue(mvarBkmrk) ''dosis
       gintprodbuscado(mintisel, 4) = grdDBGrid1(2).Columns(9).CellValue(mvarBkmrk) ''unidad medida
       gintprodbuscado(mintisel, 5) = grdDBGrid1(2).Columns(7).CellValue(mvarBkmrk) ''forma
       gintprodbuscado(mintisel, 6) = grdDBGrid1(2).Columns(10).CellValue(mvarBkmrk) ''volumen
       gintprodbuscado(mintisel, 7) = grdDBGrid1(2).Columns(11).CellValue(mvarBkmrk) ''v�a
       gintprodbuscado(mintisel, 8) = grdDBGrid1(2).Columns(5).CellValue(mvarBkmrk) ''referencia
       '----------------------------------------------------------------------------------
       'para cada producto hay que mirar si es de uso restringido
       'stra = "SELECT FR73INDUSOREST FROM FR7300 WHERE FR73CODPRODUCTO=" & _
       '        grdDBGrid1(2).Columns(1).CellValue(mvarBkmrk)
       'Set rsta = objApp.rdoConnect.OpenResultset(stra)
       'If rsta.rdoColumns(0).Value = -1 Then
       '   'si es de uso restringido se llama a la pantalla de restricciones,pero antes
       '   'se vuelca el contenido de FRA900 en FRA400
       '       stra = "SELECT * FROM FRA900 WHERE FR73CODPRODUCTO=" & _
       '              grdDBGrid1(2).Columns(1).CellValue(mvarBkmrk)
       '       Set rsta = objApp.rdoConnect.OpenResultset(stra)
       '       While Not rsta.EOF
       '           strrest = "SELECT FRA9CODREST FROM FRA400 WHERE FR73CODPRODUCTO=" & _
       '                     grdDBGrid1(2).Columns(1).CellValue(mvarBkmrk) & " AND " & _
       '                     "FR66CODPETICION=" & glngcodpeticion & " AND " & _
       '                     "FRA9CODREST=" & rsta.rdoColumns("FRA9CODREST").Value
       '           Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
       '           If rstrest.EOF Then
       '             strinsert = "INSERT INTO FRA400(FR73CODPRODUCTO,FR66CODPETICION," & _
       '                       "FRA9CODREST,FRA4DESCREST,FRA4INDSELECCIONADO) VALUES(" & _
       '                       grdDBGrid1(2).Columns(1).CellValue(mvarBkmrk) & "," & _
       '                       glngcodpeticion & "," & _
       '                       rsta.rdoColumns("FRA9CODREST").Value & "," & _
       '                       "'" & rsta.rdoColumns("FRA9DESREST").Value & "'" & "," & _
       '                       "0" & ")"
       '             objApp.rdoConnect.Execute strinsert, 64
       '             objApp.rdoConnect.Execute "Commit", 64
       '           End If
       '           rstrest.Close
       '           Set rstrest = Nothing
       '       rsta.MoveNext
       '       Wend
       '       rsta.Close
       '       Set rsta = Nothing
       '       glngcodprod = grdDBGrid1(2).Columns(1).CellValue(mvarBkmrk)
       '       Call objsecurity.LaunchProcess("FR0182")
       'End If
       '---------------------------------------------------------------------------------
    Next mintisel
  End If
cdmtraer.Enabled = True
Unload Me

Else
mensaje = MsgBox("No ha seleccionado ning�n medicamento", vbInformation, "Aviso")
cdmtraer.Enabled = True
End If

End Sub

Private Sub cmdfiltrar_Click()
Dim strclausulawhere As String
Dim rsta As rdoResultset
Dim stra As String
Dim listacodprincipios As String
Dim listaproductos As String
Dim rdoQ As rdoQuery

cmdfiltrar.Enabled = False

If chkprincipioactivo.Value = 0 And chkproducto.Value = 0 Then
    Call MsgBox("Debe especificar b�squeda por Nombre Comercial o Principio Activo.", vbInformation, "Aviso")
    cmdfiltrar.Enabled = True
    Exit Sub
End If

    If gstrLlamadorProd = "Principio Activo" Then
       strclausulawhere = "fr73indprinact=-1"
    Else
       strclausulawhere = "-1=-1 "
    End If

    'If gstrLlamadorProd = "frmPedirPrn" Then
    '  strclausulawhere = strclausulawhere & " and FR73INDPRODSAN=-1"
    'End If
    If gstrLlamadorProd = "frmDefProtocolosMed" Then
      strclausulawhere = strclausulawhere & strfrmRedactarOM
    End If
    If gstrLlamadorProd = "frmDefProtocolossoldil" Then
    'infusion intravenosa
      strclausulawhere = strclausulawhere & " AND FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73INDINFIV=-1)" & strfrmRedactarOM
    End If
'''''''''''''''''''''''''''''''''''''''''''''''''''
    If gstrLlamadorProd = "frmDefProtocolosFlui" Then
      strclausulawhere = strclausulawhere & strfrmRedactarOM
    End If
    If gstrLlamadorProd = "frmDefProtocolossolFlui" Then
    'Fluidoterapia
      strclausulawhere = strclausulawhere & " AND FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73INDINFIV=-1)" & strfrmRedactarOM
    End If
    If gstrLlamadorProd = "frmDefProtocolosEstu" Then
    'Estupefaciente
      strclausulawhere = strclausulawhere & " AND FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73INDESTUPEFACI=-1)" & strfrmRedactarOM
    End If
    If gstrLlamadorProd = "frmDefProtocolosFoMa" Then
    'Form.Magistral
      strclausulawhere = strclausulawhere & " AND FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73INDFABRICABLE=-1)" & strfrmRedactarOM
    End If
'''''''''''''''''''''''''''''''''''''''''''''''''''
    
    'If txtdescripcion.Text <> "" Then
    '   strclausulawhere = strclausulawhere & " AND UPPER(FR73DESPRODUCTO) like UPPER('" & txtdescripcion.Text & "%')"
    'End If
    If txtcodgrupo.Text <> "" Then
            strclausulawhere = strclausulawhere & " AND UPPER(FR00CODGRPTERAP) like UPPER('" & txtcodgrupo.Text & "%')"
    End If
    If txtdescgrupo.Text <> "" Then
            strclausulawhere = strclausulawhere & " AND UPPER(FR00DESGRPTERAP) like UPPER('" & txtdescgrupo.Text & "%')"
    End If
    If chkestupefaciente.Value = 0 Then
        strclausulawhere = strclausulawhere & " AND (FR73INDESTUPEFACI=0 OR FR73INDESTUPEFACI IS NULL)"
    Else
        strclausulawhere = strclausulawhere & " AND FR73INDESTUPEFACI=-1"
    End If
    If chkpsicotropico.Value = 0 Then
            strclausulawhere = strclausulawhere & " AND (FR73INDPSICOT=0 OR FR73INDPSICOT IS NULL)"
    Else
            strclausulawhere = strclausulawhere & " AND FR73INDPSICOT=-1"
    End If
    'If chkdiagnostico.Value = 0 Then
    '        strclausulawhere = strclausulawhere & " AND (FR73INDDIAGHOSP=0 OR FR73INDDIAGHOSP IS NULL)"
    'Else
    '        strclausulawhere = strclausulawhere & " AND FR73INDDIAGHOSP=-1"
    'End If
    'If chkusohospitalario.Value = 0 Then
    '        strclausulawhere = strclausulawhere & " AND (FR73INDUSOHOSP=0 OR FR73INDUSOHOSP IS NULL)"
    'Else
    '        strclausulawhere = strclausulawhere & " AND FR73INDUSOHOSP=-1"
    'End If
    If chksolucion.Value = 0 Then
            strclausulawhere = strclausulawhere & " AND (FR73INDINFIV=0 OR FR73INDINFIV IS NULL)"
    Else
            strclausulawhere = strclausulawhere & " AND FR73INDINFIV=-1"
    End If
    

'En txtdescripcion se mete un principio activo y se buscan productos que contengan
'ese principio
If chkprincipioactivo.Value = 1 Then
 If txtdescripcion.Text <> "" Then
    'bind variables
    'stra = "SELECT FR68CODPRINCACTIV FROM FR6800 WHERE " & _
           "UPPER(FR68DESPRINCACTIV) like UPPER('" & txtdescripcion.Text & "%')"
    stra = "SELECT FR68CODPRINCACTIV FROM FR6800 WHERE " & _
           "UPPER(FR68DESPRINCACTIV) like UPPER(?)"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", stra)
    rdoQ(0) = txtdescripcion.Text & "%"
    Set rsta = rdoQ.OpenResultset(stra)
    If listacodprincipios = "" And Not rsta.EOF Then
        listacodprincipios = listacodprincipios & rsta.rdoColumns(0).Value
        rsta.MoveNext
    End If
    While Not rsta.EOF
        listacodprincipios = listacodprincipios & "," & rsta.rdoColumns(0).Value
        rsta.MoveNext
    Wend
    If listacodprincipios <> "" Then
        'bind variables
        'stra = "SELECT FR73CODPRODUCTO FROM FR6400 WHERE " & _
              "FR68CODPRINCACTIV IN (" & listacodprincipios & ")"
        stra = "SELECT FR73CODPRODUCTO FROM FR6400 WHERE " & _
              "FR68CODPRINCACTIV IN (?)"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", stra)
        rdoQ(0) = listacodprincipios
        Set rsta = rdoQ.OpenResultset(stra)
        If listaproductos = "" And Not rsta.EOF Then
            listaproductos = listaproductos & rsta.rdoColumns(0).Value
            rsta.MoveNext
        End If
        While Not rsta.EOF
            listaproductos = listaproductos & "," & rsta.rdoColumns(0).Value
            rsta.MoveNext
        Wend
    End If
 End If
End If

If chkproducto = 1 And chkprincipioactivo = 1 And listaproductos <> "" Then
    If txtdescripcion.Text <> "" Then
        strclausulawhere = strclausulawhere & _
        " AND (UPPER(FR73DESPRODUCTO) like UPPER('" & txtdescripcion.Text & "%')" & _
              " OR " & _
              "FR73CODPRODUCTO IN (" & listaproductos & ")" & ")"
    End If
End If
If chkproducto = 1 And chkprincipioactivo = 1 And listaproductos = "" Then
    If txtdescripcion.Text <> "" Then
        strclausulawhere = strclausulawhere & _
        " AND UPPER(FR73DESPRODUCTO) like UPPER('" & txtdescripcion.Text & "%')"
    End If
End If
If chkproducto = 1 And chkprincipioactivo = 0 Then
    If txtdescripcion.Text <> "" Then
        strclausulawhere = strclausulawhere & _
        " AND UPPER(FR73DESPRODUCTO) like UPPER('" & txtdescripcion.Text & "%')"
    End If
End If
If chkproducto = 0 And chkprincipioactivo = 1 And listaproductos <> "" Then
    If txtdescripcion.Text <> "" Then
        strclausulawhere = strclausulawhere & _
        " AND FR73CODPRODUCTO IN (" & listaproductos & ")"
    End If
End If
If chkproducto = 0 And chkprincipioactivo = 1 And listaproductos = "" Then
    If txtdescripcion.Text <> "" Then
        strclausulawhere = strclausulawhere & _
        " AND FR73CODPRODUCTO IS NULL"
    End If
End If

If chkvisible = 0 Then
  strclausulawhere = strclausulawhere & " AND FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73INDVISIBLE=0 OR FR73INDVISIBLE IS NULL)"
End If

Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
objWinInfo.objWinActiveForm.strWhere = strclausulawhere
Call objWinInfo.DataRefresh

cmdfiltrar.Enabled = True
End Sub

Private Sub Form_Activate()
If blnnoactivate = False Then
   blnnoactivate = True
  'se va al primer registro para refrescar el grid
  Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
  Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
  Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
  'If gstrLlamadorProd = "frmPedirPrn" Then
  '  chkestupefaciente.Visible = False
  '  chkusohospitalario.Visible = False
  '  chkpsicotropico.Visible = False
  '  chkdiagnostico.Visible = False
  '  chkproducto.Visible = False
  '  chkprincipioactivo.Visible = False
  '  chksolucion.Visible = False
  ' chkmedicamento.Visible = False
  ' chkproducto.Value = 1
  'Else
    chkusohospitalario.Visible = False
    chkpsicotropico.Visible = False
    chkdiagnostico.Visible = False
    chkmedicamento.Left = 6000
    chksolucion.Left = 6000
    chkestupefaciente.Left = 6000
    chkvisible.Left = 6000
    chkmedicamento.Top = 600
    chksolucion.Top = 960
    chkestupefaciente.Top = 1320
    chkvisible.Top = 1680
    chkmedicamento.Visible = True
    chksolucion.Visible = True
    chkmedicamento.Value = 1
  'End If
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Productos"
      
    .strTable = "FR7300J" 'vista de productos con la descripci�n del grupoterape�tico
    .intAllowance = cwAllowReadOnly
    
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'WHERE
    If gstrLlamadorProd = "Principio Activo" Then
        .strWhere = "fr73indprinact=-1"
    Else
        .strWhere = "-1=-1 "
    End If
    
    strfrmRedactarOM = " and (FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 "
    strfrmRedactarOM = strfrmRedactarOM & "WHERE (FR73INDMATIMP=0 OR FR73INDMATIMP IS NULL )" & _
                       " and (FR73INDPRODSAN=0 OR FR73INDPRODSAN IS NULL ))) "
    
    If gstrLlamadorProd = "frmDefProtocolosMed" Then
      .strWhere = .strWhere & strfrmRedactarOM
    End If
    'If gstrLlamadorProd = "frmPedirPrn" Then
    '  .strWhere = .strWhere & " and FR73INDPRODSAN=-1"
    'End If
    If gstrLlamadorProd = "frmDefProtocolosmed2" Then
      grdDBGrid1(2).SelectTypeRow = ssSelectionTypeSingleSelect
      .strWhere = .strWhere & strfrmRedactarOM
    End If
    If gstrLlamadorProd = "frmDefProtocolossoldil" Then
      Me.Caption = "FARMACIA.Soluciones para diluir"
      fraframe1(1).Caption = "Soluciones para diluir"
      grdDBGrid1(2).SelectTypeRow = ssSelectionTypeSingleSelect
      .strWhere = .strWhere & " AND FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73INDINFIV=-1)" & strfrmRedactarOM
    End If
    If gstrLlamadorProd = "frmDefProtocolosFlui" Then
      grdDBGrid1(2).SelectTypeRow = ssSelectionTypeSingleSelect
      .strWhere = .strWhere & strfrmRedactarOM
    End If
    If gstrLlamadorProd = "frmDefProtocolossolFlui" Then
    'Fluidoterapia
      Me.Caption = "FARMACIA.Flu�doterapia"
      fraframe1(1).Caption = "Soluciones Intravenosas"
      grdDBGrid1(2).SelectTypeRow = ssSelectionTypeSingleSelect
      .strWhere = .strWhere & " AND FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73INDINFIV=-1)" & strfrmRedactarOM
    End If
    If gstrLlamadorProd = "frmDefProtocolosEstu" Then
    'Estupefaciente
      Me.Caption = "FARMACIA.Estupefaciente"
      fraframe1(1).Caption = "Estupefacientes"
      chkestupefaciente.Value = 1
      chkestupefaciente.Visible = False
      .strWhere = .strWhere & " AND FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73INDESTUPEFACI=-1)" & strfrmRedactarOM
    End If
    If gstrLlamadorProd = "frmDefProtocolosFoMa" Then
    'Form.Magistral
      Me.Caption = "FARMACIA.F�rmulas Magistrales"
      fraframe1(1).Caption = "F�rmulas Magistrales"
      .strWhere = .strWhere & " AND FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73INDFABRICABLE=-1)" & strfrmRedactarOM
    End If

    chkvisible.Value = 1
    chkproducto.Value = 1
    .strWhere = .strWhere & " AND FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73INDVISIBLE=-1)"

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Call .FormAddOrderField("FR73DESPRODUCTO", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Productos")
    Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n Medicamento", cwString)
    Call .FormAddFilterWhere(strKey, "FR00CODGRPTERAP", "C�d.Grupo Terape�tico", cwString)
    Call .FormAddFilterWhere(strKey, "FR00DESGRPTERAP", "Desc.Grupo Terape�tico", cwString)
    Call .FormAddFilterWhere(strKey, "FR73DOSIS", "Dosis", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "U.M", cwString)
    Call .FormAddFilterWhere(strKey, "FR73VOLUMEN", "Volumen", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR34CODVIA", "C�digo V�a", cwString)
    Call .FormAddFilterWhere(strKey, "FR73INDESTUPEFACI", "Estupefaciente?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDPSICOT", "Psicotr�pico?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDUSOHOSP", "Uso Hospitalario?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR73INDDIAGHOSP", "Diagn�stico Hospitalario?", cwBoolean)
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = fraframe1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Principios Activos"
    
    .strTable = "FR6400"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR68CODPRINCACTIV", cwAscending)
    Call .FormAddRelation("FR73CODPRODUCTO", txtText1(0))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Principios Activos")
    Call .FormAddFilterWhere(strKey, "FR68CODPRINCACTIV", "C�d. Principio Activo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR64DOSIS", "Dosis", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�d. Unidad de Medida", cwString)
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�digo Principio", "FR68CODPRINCACTIV", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Descripci�n", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "C�d.Medicamento", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR64DOSIS", cwNumeric, 6)
    Call .GridAddColumn(objMultiInfo, "Unidad Medida", "FR93CODUNIMEDIDA", cwString, 5)
    
    Call .FormCreateInfo(objMasterInfo)

    Call .FormChangeColor(objMultiInfo)
        

    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), "FR68CODPRINCACTIV", "SELECT * FROM FR6800 WHERE FR68CODPRINCACTIV = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(4), "FR68DESPRINCACTIV")
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    
    grdDBGrid1(0).Columns(5).Visible = False
    
    grdDBGrid1(0).Columns(3).Width = 700
    grdDBGrid1(0).Columns(4).Width = 4800
    
    Call .WinRegister
    Call .WinStabilize
    
    grdDBGrid1(2).Columns(1).Visible = False
    grdDBGrid1(2).Columns(2).Visible = False
    grdDBGrid1(2).Columns(4).Visible = False
    
    grdDBGrid1(2).Columns(3).Width = 1000
    grdDBGrid1(2).Columns(5).Width = 1200
    grdDBGrid1(2).Columns(6).Width = 5500
    grdDBGrid1(2).Columns(7).Width = 600
    grdDBGrid1(2).Columns(8).Width = 800
    grdDBGrid1(2).Columns(9).Width = 800
    grdDBGrid1(2).Columns(10).Width = 1300
    grdDBGrid1(2).Columns(11).Width = 1300
    grdDBGrid1(2).Columns(12).Width = 1500
    grdDBGrid1(2).Columns(13).Width = 1300
    
    'grdDBGrid1(2).Columns(13).Position
    
  End With
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
  
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub





' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
If btnButton.Index <> 3 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
If intIndex <> 20 Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


