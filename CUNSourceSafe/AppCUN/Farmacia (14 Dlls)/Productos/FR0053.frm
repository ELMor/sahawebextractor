VERSION 5.00
Begin VB.Form frmBuscarPrincipiosActivos 
   Caption         =   "MANTENIMIENTO FARMACIA. Selecci�n de Principios Activos"
   ClientHeight    =   3615
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10395
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3615
   ScaleWidth      =   10395
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdaceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   4320
      TabIndex        =   2
      Top             =   2520
      Width           =   1935
   End
   Begin VB.Frame Frame1 
      Caption         =   "Introduzca el nombre del principio activo "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   10215
      Begin VB.CommandButton cmdprincipios 
         Caption         =   "Principios Activos"
         Height          =   375
         Left            =   8040
         TabIndex        =   3
         Top             =   720
         Width           =   1935
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Left            =   120
         MaxLength       =   50
         TabIndex        =   1
         Top             =   720
         Width           =   7695
      End
   End
End
Attribute VB_Name = "frmBuscarPrincipiosActivos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdaceptar_Click()
Dim rsta As rdoResultset
Dim stra As String
Dim rstsin As rdoResultset
Dim strsin As String
Dim principio As String
Dim mensaje As Integer
Dim rdoQ As rdoQuery

cmdaceptar.Enabled = False
If Text1.Text <> "" Then
principio = UCase(Text1.Text)
'bind variables
stra = "SELECT count(*) FROM FR6800 WHERE UPPER(FR68DESPRINCACTIV)=?"
Set rdoQ = objApp.rdoConnect.CreateQuery("", stra)
rdoQ(0) = principio
Set rsta = rdoQ.OpenResultset(stra)
If rsta.rdoColumns(0).Value = 0 Then
  'bind variables
  strsin = "SELECT count(*) FROM FRH400 WHERE UPPER(FRH4DESSINONIMO)=?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", strsin)
  rdoQ(0) = principio
  Set rstsin = rdoQ.OpenResultset(strsin)
  If rstsin.rdoColumns(0).Value = 0 Then
     mensaje = MsgBox("El principio activo no existe. �Desea buscar otro?", vbYesNo, "Aviso")
     If mensaje = 6 Then 'S�
        Text1.Text = ""
        cmdaceptar.Enabled = True
        Exit Sub
     End If
  Else
    'bind variables
     strsin = "SELECT FR68CODPRINCACTIV FROM FRH400 WHERE UPPER(FRH4DESSINONIMO)=?"
     Set rdoQ = objApp.rdoConnect.CreateQuery("", strsin)
     rdoQ(0) = principio
     Set rstsin = rdoQ.OpenResultset(strsin)
     'bind variables
     'stra = "SELECT FR68CODPRINCACTIV,FR68DESPRINCACTIV FROM FR6800 " & _
     '    "WHERE FR68CODPRINCACTIV=" & rstsin.rdoColumns(0).Value
     stra = "SELECT FR68CODPRINCACTIV,FR68DESPRINCACTIV FROM FR6800 " & _
         "WHERE FR68CODPRINCACTIV=?"
     Set rdoQ = objApp.rdoConnect.CreateQuery("", stra)
     rdoQ(0) = rstsin.rdoColumns(0).Value
     Set rsta = rdoQ.OpenResultset(stra)
     ReDim gintpricipioactivobuscado(0, 2)
     gintpricipioactivototal = 1
     gintpricipioactivobuscado(0, 0) = rsta.rdoColumns("FR68CODPRINCACTIV").Value
     gintpricipioactivobuscado(0, 1) = rsta.rdoColumns("FR68DESPRINCACTIV").Value
     'gintpricipioactivobuscado(0, 2) = rsta.rdoColumns("FR68DOSIS").Value
     'gintpricipioactivobuscado(0, 3) = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
  End If
  rstsin.Close
  Set rstsin = Nothing
Else
  'bind variables
  'stra = "SELECT FR68CODPRINCACTIV,FR68DESPRINCACTIV FROM FR6800 " & _
  '       "WHERE UPPER(FR68DESPRINCACTIV)='" & principio & "'"
  stra = "SELECT FR68CODPRINCACTIV,FR68DESPRINCACTIV FROM FR6800 " & _
         "WHERE UPPER(FR68DESPRINCACTIV)=?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", stra)
  rdoQ(0) = principio
  Set rsta = rdoQ.OpenResultset(stra)
  ReDim gintpricipioactivobuscado(0, 2)
  gintpricipioactivototal = 1
  gintpricipioactivobuscado(0, 0) = rsta.rdoColumns("FR68CODPRINCACTIV").Value
  gintpricipioactivobuscado(0, 1) = rsta.rdoColumns("FR68DESPRINCACTIV").Value
  'gintpricipioactivobuscado(0, 2) = rsta.rdoColumns("FR68DOSIS").Value
  'gintpricipioactivobuscado(0, 3) = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
End If
rsta.Close
Set rsta = Nothing
End If
cmdaceptar.Enabled = True

Unload Me
End Sub

Private Sub cmdprincipios_Click()
  cmdprincipios.Enabled = False
  Call objsecurity.LaunchProcess("FR0066")
  cmdprincipios.Enabled = True
End Sub

Private Sub Form_Activate()
If gintprincact = 1 Then
    gintprincact = 0
    Unload Me
End If
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
' PASA A MAYUSCULAS
'   KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
