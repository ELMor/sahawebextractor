VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmConsultaCompleta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Consulta no respondida"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdcorreo 
      Caption         =   "Enviar Respuesta por Correo"
      Height          =   375
      Left            =   5040
      TabIndex        =   31
      Top             =   7440
      Width           =   2295
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Consulta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6615
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   600
      Width           =   11685
      Begin TabDlg.SSTab tabTab1 
         Height          =   5940
         Index           =   0
         Left            =   120
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   480
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   10478
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR8207.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(4)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(7)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "SSTab1"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR8207.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin TabDlg.SSTab SSTab1 
            Height          =   4095
            Left            =   240
            TabIndex        =   21
            Top             =   960
            Width           =   10695
            _ExtentX        =   18865
            _ExtentY        =   7223
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            Tab             =   1
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Consulta"
            TabPicture(0)   =   "FR8207.frx":0038
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "lblLabel1(4)"
            Tab(0).Control(1)=   "lblLabel1(5)"
            Tab(0).Control(2)=   "lblLabel1(1)"
            Tab(0).Control(3)=   "dtcDateCombo1(1)"
            Tab(0).Control(4)=   "txtText1(9)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(8)"
            Tab(0).Control(6)=   "txtText1(1)"
            Tab(0).ControlCount=   7
            TabCaption(1)   =   "Respuesta"
            TabPicture(1)   =   "FR8207.frx":0054
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "lblLabel1(7)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "lblLabel1(6)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "lblLabel1(2)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "lblLabel1(11)"
            Tab(1).Control(3).Enabled=   0   'False
            Tab(1).Control(4)=   "lblLabel1(8)"
            Tab(1).Control(4).Enabled=   0   'False
            Tab(1).Control(5)=   "dtcDateCombo1(0)"
            Tab(1).Control(5).Enabled=   0   'False
            Tab(1).Control(6)=   "txtText1(11)"
            Tab(1).Control(6).Enabled=   0   'False
            Tab(1).Control(7)=   "txtText1(10)"
            Tab(1).Control(7).Enabled=   0   'False
            Tab(1).Control(8)=   "txtText1(5)"
            Tab(1).Control(8).Enabled=   0   'False
            Tab(1).Control(9)=   "txtText1(6)"
            Tab(1).Control(9).Enabled=   0   'False
            Tab(1).Control(10)=   "txtText1(2)"
            Tab(1).Control(10).Enabled=   0   'False
            Tab(1).Control(11)=   "txtText1(3)"
            Tab(1).Control(11).Enabled=   0   'False
            Tab(1).Control(12)=   "cmdrespuesta"
            Tab(1).Control(12).Enabled=   0   'False
            Tab(1).ControlCount=   13
            Begin VB.CommandButton cmdrespuesta 
               Caption         =   "Buscar Respuesta"
               Height          =   375
               Left            =   7800
               TabIndex        =   30
               Top             =   3000
               Width           =   1695
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   3
               Left            =   720
               TabIndex        =   11
               TabStop         =   0   'False
               Tag             =   "Servicio que responde"
               Top             =   3480
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   2
               Left            =   240
               TabIndex        =   10
               Tag             =   "C�digo Servicio que responde"
               Top             =   3480
               Width           =   400
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               ForeColor       =   &H00000000&
               Height          =   330
               Index           =   6
               Left            =   1560
               TabIndex        =   9
               TabStop         =   0   'False
               Tag             =   "Persona que responde"
               Top             =   2640
               Width           =   4560
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "SG02COD"
               Height          =   330
               Index           =   5
               Left            =   240
               TabIndex        =   8
               Tag             =   "C�d.Persona que responde"
               Top             =   2640
               Width           =   1200
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR48DESRESPUESTA"
               Height          =   1410
               Index           =   10
               Left            =   2400
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   6
               Tag             =   "Respuesta"
               Top             =   960
               Width           =   8040
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR48VALORRESP"
               Height          =   330
               Index           =   11
               Left            =   240
               TabIndex        =   7
               Tag             =   "Valoraci�n Respuesta"
               Top             =   1680
               Width           =   300
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR48DESCONSULTA"
               Height          =   1650
               Index           =   1
               Left            =   -72600
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   2
               Tag             =   "Consulta"
               Top             =   840
               Width           =   8040
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "SG02COD_PRE"
               Height          =   330
               Index           =   8
               Left            =   -74640
               TabIndex        =   3
               Tag             =   "C�d.Persona que consulta"
               Top             =   3000
               Width           =   1200
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   9
               Left            =   -73320
               TabIndex        =   4
               TabStop         =   0   'False
               Tag             =   "Persona que responde"
               Top             =   3000
               Width           =   4560
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR48FECCONSULTA"
               Height          =   330
               Index           =   1
               Left            =   -74640
               TabIndex        =   1
               Tag             =   "Fecha Consulta"
               Top             =   840
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR48FECRESPUESTA"
               Height          =   330
               Index           =   0
               Left            =   240
               TabIndex        =   5
               Tag             =   "Fecha Respuesta"
               Top             =   960
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Servicio que responde"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   240
               TabIndex        =   29
               Top             =   3240
               Width           =   1920
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Persona que responde"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   11
               Left            =   240
               TabIndex        =   28
               Top             =   2400
               Width           =   1920
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Respuesta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   240
               TabIndex        =   27
               Top             =   720
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Respuesta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   2400
               TabIndex        =   26
               Top             =   720
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Valoraci�n Respuesta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   7
               Left            =   240
               TabIndex        =   25
               Top             =   1440
               Width           =   1875
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Consulta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   -72600
               TabIndex        =   24
               Top             =   600
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Consulta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   -74640
               TabIndex        =   23
               Top             =   600
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Persona que pregunta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   -74640
               TabIndex        =   22
               Top             =   2760
               Width           =   1890
            End
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   7
            Left            =   1440
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "Producto"
            Top             =   5400
            Width           =   8640
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            Index           =   4
            Left            =   240
            TabIndex        =   12
            Tag             =   "C�d.Producto"
            Top             =   5400
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR48CODLOGACTCIM"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   0
            Tag             =   "C�digo Consulta"
            Top             =   480
            Width           =   1080
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6225
            Index           =   0
            Left            =   -74760
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   240
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   10980
            _StockProps     =   79
            Caption         =   "CONSULTAS"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   240
            TabIndex        =   20
            Top             =   5160
            Width           =   1260
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Consulta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   18
            Top             =   240
            Width           =   1455
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   19
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmConsultaCompleta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmConsultaCompleta(FR0207.FRM)                              *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: ABRIL DE 1999                                                 *
'* DESCRIPCION: Consulta Completa                                       *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


Private Sub cmdcorreo_Click()
cmdcorreo.Enabled = False
    'respuesta a la consulta
    frmEnviarRespCorreo.txtText1(8).Text = txtText1(10).Text
    Call objsecurity.LaunchProcess("FR0206")
cmdcorreo.Enabled = True
End Sub

Private Sub cmdrespuesta_Click()
Dim strfec As String
Dim rstfec As rdoResultset

cmdrespuesta.Enabled = False
If txtText1(4).Text <> "" Then
    intproductoconsulta = txtText1(4).Text
    strproductoconsulta = txtText1(7).Text
    Call objsecurity.LaunchProcess("FR0203")
    'se trae del buscador la descripci�n BIM
    If intcodBIM <> "" Then
        txtText1(10).Text = intcodBIM
        strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
        Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
        Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstfec.rdoColumns(0).Value)
        rstfec.Close
        Set rstfec = Nothing
        Call objWinInfo.CtrlSet(txtText1(5), objsecurity.strUser)
        objWinInfo.objWinActiveForm.blnChanged = True
        tlbToolbar1.Buttons(4).Enabled = True
    End If
Else
    Call MsgBox("No ha introducido ning�n producto para realizar la b�squeda", vbInformation, "Aviso")
End If
cmdrespuesta.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String

  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Registrar Consulta"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "FR4800"
    .strWhere = "FR48CODLOGACTCIM=" & intconsulta
    
    Call .FormAddOrderField("FR48CODLOGACTCIM", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Registrar Consulta")
    Call .FormAddFilterWhere(strKey, "FR48CODLOGACTCIM", "C�digo Consulta", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR48FECCONSULTA", "Fecha Consulta", cwDate)
    Call .FormAddFilterWhere(strKey, "FR48DESCONSULTA", "Consulta", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_PRE", "C�digo Persona que pregunta", cwString)
    Call .FormAddFilterWhere(strKey, "FR48FECRESPUESTA", "Fecha Respuesta", cwDate)
    Call .FormAddFilterWhere(strKey, "FR48DESRESPUESTA", "Respuesta", cwString)
    Call .FormAddFilterWhere(strKey, "FR48VALORRESP", "Valoraci�n respuesta", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD", "C�digo Persona que responde", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Dpto que responde", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "FR48CODLOGACTCIM", "C�digo Consulta")
    Call .FormAddFilterOrder(strKey, "FR48FECCONSULTA", "Fecha Consulta")
    Call .FormAddFilterOrder(strKey, "FR48FECRESPUESTA", "Fecha Respuesta")
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)


    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(5)).blnForeign = True
    .CtrlGetInfo(txtText1(4)).blnForeign = True
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True
    .CtrlGetInfo(txtText1(4)).blnReadOnly = True
    .CtrlGetInfo(txtText1(8)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(7), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "SG02COD_PRE", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(6), "SG02APE1")
    
    Call .WinRegister
    Call .WinStabilize
  End With

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim objSearch As clsCWSearch
  
  If strFormName = "Registrar Consulta" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strOrder = "ORDER BY AD02CODDPTO ASC"
     
     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�d. Servicio"
         
     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Desc. Servicio"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
  If strFormName = "Registrar Consulta" And strCtrl = "txtText1(4)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
         
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"
     
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo Interno"
         
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n Producto"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(4), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
  
  If strFormName = "Registrar Consulta" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "SG0200"
     .strOrder = "ORDER BY SG02COD ASC"
     
     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�d.Persona"
     
     Set objField = .AddField("SG02NOM")
     objField.strSmallDesc = "Nombre"
         
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Apellido 1�"
     
     Set objField = .AddField("SG02APE2")
     objField.strSmallDesc = "Apellido 2�"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
  If strFormName = "Registrar Consulta" And strCtrl = "txtText1(5)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "SG0200"
     .strOrder = "ORDER BY SG02COD ASC"
     
     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�d.Persona"
     
     Set objField = .AddField("SG02NOM")
     objField.strSmallDesc = "Nombre"
         
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Apellido 1�"
     
     Set objField = .AddField("SG02APE2")
     objField.strSmallDesc = "Apellido 2�"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(5), .cllValues("SG02COD"))
     End If
   End With
   Set objSearch = Nothing
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstfec As rdoResultset
Dim strfec As String

  If btnButton.Index = 2 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR48CODLOGACTCIM_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0).Text = rsta.rdoColumns(0).Value
    SendKeys ("{TAB}")
    strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    Call objWinInfo.CtrlSet(dtcDateCombo1(1), rstfec.rdoColumns(0).Value)
    SendKeys ("{TAB}")
    rstfec.Close
    Set rstfec = Nothing
    rsta.Close
    Set rsta = Nothing
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If

End Sub
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstfec As rdoResultset
Dim strfec As String

  If intIndex = 10 Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    sqlstr = "SELECT FR48CODLOGACTCIM_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0).Text = rsta.rdoColumns(0).Value
    SendKeys ("{TAB}")
    strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    Call objWinInfo.CtrlSet(dtcDateCombo1(1), rstfec.rdoColumns(0).Value)
    SendKeys ("{TAB}")
    rstfec.Close
    Set rstfec = Nothing
    rsta.Close
    Set rsta = Nothing
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)
Dim strfec As String
Dim rstfec As rdoResultset

If Index = 10 Then
        strfec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
        Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
        Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstfec.rdoColumns(0).Value)
        rstfec.Close
        Set rstfec = Nothing
        Call objWinInfo.CtrlSet(txtText1(5), objsecurity.strUser)
End If
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim rstfec As rdoResultset
Dim strfec As String
Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

