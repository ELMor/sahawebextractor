Attribute VB_Name = "Module1"
Option Explicit
Public objCW As Object    ' referencia al objeto CodeWizard
Public objApp As Object
Public objPipe As Object
Public objGen As Object
Public objEnv As Object
Public objError As Object
Public objMouse As Object
Public objSecurity As Object
Public objMRes As Object

Public Const cSALIR = 1
Public Const cENGRANAJES = 2
Public Const cMARTILLO = 3
Public Const cGUARDAR = 4
Public Const cNUEVO = 6

Public gblCodPeticion As Long
Public Sub Main()

End Sub
Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Set objSecurity = objCW.objSecurity
  Set objMRes = CreateObject("CodeWizard.clsCWMRES")
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub
Public Sub ExitApp()
     objCW = Nothing
End Sub

Public Function strFecha_Sistema() As String
  Dim rs As rdoResultset
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY') from dual")
  strFecha_Sistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing
End Function

Public Function fNextClaveSeq(campo$) As Long
'obtiene el siguiente c�digo a utilizar en un registro nuevo utilizando los sequences
    
    Dim sql$, rsMaxClave As rdoResultset
    
    sql = "SELECT " & campo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(sql)
    fNextClaveSeq = rsMaxClave(0)
    rsMaxClave.Close
    
End Function
