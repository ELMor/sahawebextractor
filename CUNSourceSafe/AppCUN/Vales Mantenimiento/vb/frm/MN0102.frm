VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmPeticionDepartamento 
   Caption         =   "Peticiones de un Departamento"
   ClientHeight    =   7155
   ClientLeft      =   1395
   ClientTop       =   885
   ClientWidth     =   9420
   LinkTopic       =   "Form1"
   ScaleHeight     =   7155
   ScaleWidth      =   9420
   Begin VB.CommandButton cmdPeticion 
      Caption         =   "&Petici�n a Mantenimiento"
      Height          =   615
      Left            =   5040
      TabIndex        =   9
      Top             =   6480
      Width           =   2175
   End
   Begin VB.CommandButton cmdVer 
      Caption         =   "&Ver la petici�n seleccionada"
      Height          =   615
      Left            =   2400
      TabIndex        =   8
      Top             =   6480
      Width           =   2175
   End
   Begin VB.CommandButton cmdSalir 
      Height          =   735
      Left            =   8160
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Salir"
      Top             =   240
      Width           =   735
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   5175
      Left            =   360
      TabIndex        =   6
      Top             =   1200
      Width           =   8775
      _Version        =   131078
      DataMode        =   2
      FieldSeparator  =   ";"
      Col.Count       =   3
      AllowUpdate     =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2117
      Columns(0).Caption=   "Cod. Petici�n"
      Columns(0).Name =   "Cod. Petici�n"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1931
      Columns(1).Caption=   "Fecha"
      Columns(1).Name =   "Fecha"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   10874
      Columns(2).Caption=   "Desperfecto"
      Columns(2).Name =   "Desperfecto"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   15478
      _ExtentY        =   9128
      _StockProps     =   79
   End
   Begin SSDataWidgets_B.SSDBCombo ssdbDepartamento 
      Height          =   375
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   2775
      DataFieldList   =   "Column 1"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldSeparator  =   ";"
      ForeColorEven   =   0
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   2
      Columns(0).Width=   3969
      Columns(0).Caption=   "Departamento"
      Columns(0).Name =   "Departamento"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Cod. Departamento"
      Columns(1).Name =   "Cod. Departamento"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4895
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   0
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateDesde 
      Height          =   375
      Left            =   3480
      TabIndex        =   1
      Top             =   480
      Width           =   1935
      _Version        =   65537
      _ExtentX        =   3413
      _ExtentY        =   661
      _StockProps     =   93
      BackColor       =   -2147483639
      DefaultDate     =   ""
      ShowCentury     =   -1  'True
      AllowEdit       =   0   'False
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateHasta 
      Height          =   375
      Left            =   5760
      TabIndex        =   2
      Top             =   480
      Width           =   1935
      _Version        =   65537
      _ExtentX        =   3413
      _ExtentY        =   661
      _StockProps     =   93
      BackColor       =   -2147483639
      DefaultDate     =   ""
      ShowCentury     =   -1  'True
      AllowEdit       =   0   'False
      StartofWeek     =   2
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   840
      Top             =   5880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   1
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MN0102.frx":0000
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label3 
      Caption         =   "Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5760
      TabIndex        =   5
      Top             =   240
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3480
      TabIndex        =   4
      Top             =   240
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "Departamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   3
      Top             =   240
      Width           =   1215
   End
End
Attribute VB_Name = "frmPeticionDepartamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim blnPrimeraVez As Boolean

Private Sub cmdPeticion_Click()
  Call objSecurity.LaunchProcess("MN0101")
  SSDBGrid1.RemoveAll
  Call CargarGrid(ssdbDepartamento.Columns(1).Value, SSDateDesde.Text, SSDateHasta.Text)
End Sub

Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub cmdVer_Click()
  Call objSecurity.LaunchProcess("MN0101", SSDBGrid1.Columns(0).Value)
End Sub

Private Sub Form_Load()
Dim dpt%, DesDpto$
blnPrimeraVez = True
Set cmdSalir.Picture = ImageList1.ListImages.Item(cSALIR).Picture
    Call CargarComboDepartamentos
    dpt = ObtenerDepartamento(DesDpto)
      If dpt <> 0 Then
        ssdbDepartamento.Columns(1).Value = dpt
        ssdbDepartamento = DesDpto
      Else
        ssdbDepartamento.Columns(1).Value = dpt
        ssdbDepartamento = ""
      End If
    SSDateHasta.Date = strFecha_Sistema
    SSDateDesde.Date = DateAdd("m", -2, strFecha_Sistema)
    Call CargarGrid(dpt, SSDateDesde.Text, SSDateHasta.Text)
End Sub
Private Sub CargarComboDepartamentos()
  Dim str$, ww$
  Dim rd As rdoResultset
  Dim qy As rdoQuery
  str = "SELECT AD02CODDPTO, AD02DESDPTO FROM AD0200 ORDER BY AD02DESDPTO ASC"
    Set rd = objApp.rdoConnect.OpenResultset(str, rdOpenKeyset, rdConcurReadOnly)
    While Not rd.EOF
        ww = rd!AD02DESDPTO & "; " & rd!AD02CODDPTO
        ssdbDepartamento.AddItem ww
        rd.MoveNext
    Wend
    rd.Close
   ssdbDepartamento.Columns(1).Value = Null
End Sub


Private Function ObtenerDepartamento(DesDpto As String) As Integer
Dim str$
Dim rd As rdoResultset
Dim qy As rdoQuery

  str = "SELECT AD0300.AD02CODDPTO, AD0200.AD02DESDPTO " & _
  "FROM AD0300, AD0200 WHERE AD0300.SG02COD=? AND " & _
  "AD0300.AD02CODDPTO = AD0200.AD02CODDPTO"
  Set qy = objApp.rdoConnect.CreateQuery("", str)
  qy(0) = objSecurity.strUser
  Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
  If rd.RowCount > 0 Then
    DesDpto = rd!AD02DESDPTO
    ObtenerDepartamento = rd!AD02CODDPTO
  Else
    ObtenerDepartamento = 0
  End If
  rd.Close
  qy.Close
End Function

Private Sub CargarGrid(dpt%, desde As String, hasta As String)
  Dim str$
  Dim rd As rdoResultset
  Dim qy As rdoQuery
  Dim ww As String
  Me.MousePointer = vbHourglass
  str = "SELECT * FROM MN6100 WHERE AD02CODDPTO=? " & _
  "AND MN61FECADD < TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
  "AND MN61FECADD > TO_DATE(?,'DD/MM/YYYY') " & _
  "ORDER BY MN61FECADD DESC"
  Set qy = objApp.rdoConnect.CreateQuery("", str)
    qy(0) = dpt
    qy(1) = hasta + " 23:59:59"
    qy(2) = desde
  Set rd = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
  Do While Not rd.EOF
    ww = CStr(rd!MN61CODPETICION) + ";" + Format$(rd!MN61FECADD, "DD/MM/YYYY") + ";" + rd!MN61DESPERFECTO
    SSDBGrid1.AddItem ww
    rd.MoveNext
  Loop
  Me.MousePointer = vbDefault
End Sub

Private Sub SSDateDesde_Change()
  SSDBGrid1.RemoveAll
  Call CargarGrid(ssdbDepartamento.Columns(1).Value, SSDateDesde.Text, SSDateHasta.Text)
End Sub

Private Sub SSDateDesde_CloseUp()
  SSDBGrid1.RemoveAll
  Call CargarGrid(ssdbDepartamento.Columns(1).Value, SSDateDesde.Text, SSDateHasta.Text)
End Sub

Private Sub SSDateHasta_Change()
  SSDBGrid1.RemoveAll
  Call CargarGrid(ssdbDepartamento.Columns(1).Value, SSDateDesde.Text, SSDateHasta.Text)
End Sub

Private Sub SSDateHasta_CloseUp()
  SSDBGrid1.RemoveAll
  Call CargarGrid(ssdbDepartamento.Columns(1).Value, SSDateDesde.Text, SSDateHasta.Text)
End Sub

Private Sub ssdbDepartamento_CloseUp()
  SSDBGrid1.RemoveAll
  Call CargarGrid(ssdbDepartamento.Columns(1).Value, SSDateDesde.Text, SSDateHasta.Text)
End Sub

Private Sub ssdbDepartamento_KeyUp(KeyCode As Integer, Shift As Integer)
  SSDBGrid1.RemoveAll
  Call CargarGrid(ssdbDepartamento.Columns(1).Value, SSDateDesde.Text, SSDateHasta.Text)
End Sub

Private Sub ssdbDepartamento_LostFocus()
  If Not blnPrimeraVez Then
    SSDBGrid1.RemoveAll
    Call CargarGrid(ssdbDepartamento.Columns(1).Value, SSDateDesde.Text, SSDateHasta.Text)
  End If
End Sub

Private Sub SSDBGrid1_DblClick()
  Call objSecurity.LaunchProcess("MN0101", SSDBGrid1.Columns(0).Value)
End Sub


