VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmRestricciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AGENDA. Restricciones"
   ClientHeight    =   5730
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11295
   ControlBox      =   0   'False
   Icon            =   "AG1005.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   5730
   ScaleWidth      =   11295
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Restricciones por Recurso"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4785
      Index           =   0
      Left            =   105
      TabIndex        =   7
      Top             =   555
      Width           =   5250
      Begin TabDlg.SSTab tabTab1 
         Height          =   4230
         Index           =   0
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   375
         Width           =   5025
         _ExtentX        =   8864
         _ExtentY        =   7461
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AG1005.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(3)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(7)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cboSSDBCombo1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "cboSSDBCombo1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(4)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(5)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(6)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(7)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(8)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(9)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "chkExclusivo(0)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "Frame1"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).ControlCount=   18
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AG1005.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.Frame Frame1 
            Caption         =   "Busqueda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1215
            Left            =   3720
            TabIndex        =   23
            Top             =   480
            Width           =   1215
            Begin VB.OptionButton optbus 
               Caption         =   "1 Apell"
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   25
               Top             =   720
               Width           =   975
            End
            Begin VB.OptionButton optbus 
               Caption         =   "Codigo"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   24
               Top             =   360
               Value           =   -1  'True
               Width           =   975
            End
         End
         Begin VB.CheckBox chkExclusivo 
            Caption         =   "Exclusivo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   180
            TabIndex        =   4
            Top             =   3240
            Width           =   1455
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   9
            Left            =   3720
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   480
            Visible         =   0   'False
            Width           =   945
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   8
            Left            =   420
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   3705
            Visible         =   0   'False
            Width           =   1185
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   7
            Left            =   3885
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   3690
            Visible         =   0   'False
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   6
            Left            =   2580
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   3705
            Visible         =   0   'False
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   5
            Left            =   3270
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   3690
            Visible         =   0   'False
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   4
            Left            =   1830
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   3675
            Visible         =   0   'False
            Width           =   465
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   3
            Left            =   2835
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   3270
            Visible         =   0   'False
            Width           =   810
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   2
            Left            =   2115
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   3285
            Visible         =   0   'False
            Width           =   555
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   1
            Left            =   2280
            MaxLength       =   10
            TabIndex        =   2
            Tag             =   "Valor Hasta"
            Top             =   1830
            Width           =   2010
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   0
            Left            =   180
            TabIndex        =   1
            Tag             =   "Valor Desde"
            Top             =   1830
            Width           =   1785
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3615
            Index           =   0
            Left            =   -74895
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   495
            Width           =   4590
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   8096
            _ExtentY        =   6376
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "AG16CODTIPREST"
            Height          =   330
            Index           =   1
            Left            =   180
            TabIndex        =   0
            Tag             =   "Tipo de Restricci�n"
            Top             =   750
            Width           =   3375
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   7
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   5953
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "TipoDato"
            Columns(2).Name =   "TipoDato"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "Tabla"
            Columns(3).Name =   "Tabla"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "ColCod"
            Columns(4).Name =   "ColCod"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "ColDes"
            Columns(5).Name =   "ColDes"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "FechaBaja"
            Columns(6).Name =   "FechaBaja"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            _ExtentX        =   5953
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            Height          =   330
            Index           =   0
            Left            =   180
            TabIndex        =   3
            Tag             =   "Inclusi�n/Exclusi�n"
            Top             =   2640
            Width           =   2160
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            FieldDelimiter  =   "'"
            FieldSeparator  =   ","
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   3810
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            _ExtentX        =   3810
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Indicador Inclusi�n/Exclusi�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   180
            TabIndex        =   13
            Top             =   2280
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Valor Hasta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   2325
            TabIndex        =   8
            Top             =   1575
            Width           =   1005
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Valor Desde"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   180
            TabIndex        =   11
            Top             =   1575
            Width           =   1050
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Restricci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   180
            TabIndex        =   10
            Top             =   480
            Width           =   1680
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   9
      Top             =   5445
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin ComctlLib.TreeView TreeView1 
      Height          =   4635
      Index           =   0
      Left            =   5400
      TabIndex        =   14
      Top             =   720
      Width           =   5805
      _ExtentX        =   10239
      _ExtentY        =   8176
      _Version        =   327682
      HideSelection   =   0   'False
      Indentation     =   265
      Style           =   7
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRestricciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim strPadre As String
Dim strActual As String
Dim intMaxLevels As Integer
Dim lngNumSecuencia As Long
Private Function CrearArbolExclusivosFranjas(ByVal strNivelSup As String, ByVal strCodFranja As String) As Long
  Dim rdoRestric As rdoResultset
  Dim rdoNuevoRestric As rdoResultset
  Dim strSql As String
  Dim rdoConsulta As rdoQuery
  Dim rdoConsulta2 As rdoQuery
  Dim strNuevoNivelSup As String
  Dim vntNuevoCodRest As Variant

'  strSql = "SELECT  * FROM " _
'      & objEnv.GetValue("Database") & "AG1200 WHERE  AG1200.AG11CODRECURSO=" & Val(frmFranjas.txtText1(12).Text) _
'         & " AND AG1200.AG07CODPERFIL=" & Val(frmFranjas.txtText1(11).Text) _
'         & " AND AG1200.AG04CODFRANJA=" & Val(frmFranjas.txtText1(0).Text) _
'         & " And AG1200.AG12NUMRESFRJA = " & strNivelSup
  strSql = "SELECT  * FROM " _
      & objEnv.GetValue("Database") & "AG1200 WHERE  AG1200.AG11CODRECURSO=?" _
         & " AND AG1200.AG07CODPERFIL=?" _
         & " AND AG1200.AG04CODFRANJA=?" _
         & " And AG1200.AG12NUMRESFRJA = ?"
'  Set rdoRestric = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
  
    Set rdoConsulta2 = objApp.rdoConnect.CreateQuery("", strSql)
    rdoConsulta2(0) = Val(frmFranjas.txtText1(12).Text)
    rdoConsulta2(1) = Val(frmFranjas.txtText1(11).Text)
    rdoConsulta2(2) = Val(frmFranjas.txtText1(0).Text)
    rdoConsulta2(3) = strNivelSup
    Set rdoRestric = rdoConsulta2.OpenResultset()
  
  
  If Not rdoRestric.EOF Then
    strSql = "SELECT  * FROM " _
        & objEnv.GetValue("Database") & "AG1200 WHERE  AG1200.AG11CODRECURSO=0" _
         & " AND AG1200.AG07CODPERFIL=0 " _
         & " AND AG1200.AG04CODFRANJA=0" _
         & " And AG1200.AG12NUMRESFRJA =0"
    Set rdoNuevoRestric = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
    rdoNuevoRestric.AddNew
    rdoNuevoRestric("AG11CODRECURSO") = rdoRestric("AG11CODRECURSO")
    rdoNuevoRestric("AG07CODPERFIL") = rdoRestric("AG07CODPERFIL")
    rdoNuevoRestric("AG04CODFRANJA") = strCodFranja
         
    rdoNuevoRestric("AG16CODTIPREST") = rdoRestric("AG16CODTIPREST")
    rdoNuevoRestric("AG12VALDESDERES") = rdoRestric("AG12VALDESDERES")
    rdoNuevoRestric("AG12INDINCEXCL") = rdoRestric("AG12INDINCEXCL")
    rdoNuevoRestric("AG12VALHASTARES") = rdoRestric("AG12VALHASTARES")
    rdoNuevoRestric("AG12NIVELJERAR") = rdoRestric("AG12NIVELJERAR")
    If rdoRestric("AG12NIVELSUPER") = 0 Then
      rdoNuevoRestric("AG12NIVELSUPER") = rdoRestric("AG12NIVELSUPER")
    Else
      rdoNuevoRestric("AG12NIVELSUPER") = CrearArbolExclusivosFranjas(rdoRestric("AG12NIVELSUPER"), strCodFranja)
    End If
    rdoNuevoRestric("AG12FECADD") = objGen.GetDBDateTime
    rdoNuevoRestric("AG12FECUPD") = objGen.GetDBDateTime
    strSql = "SELECT AG12NUMRESFRJA, AG11CODRECURSO, AG12NIVELSUPER, AG12VALDESDERES," _
         & " AG12VALHASTARES, AG16CODTIPREST, AG07CODPERFIL, AG04CODFRANJA FROM " _
         & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO= ? " _
         & " AND AG07CODPERFIL= ? AND AG04CODFRANJA= ? " _
         & " order by  AG12NUMRESFRJA desc"
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
    rdoConsulta(0) = rdoRestric("AG11CODRECURSO")
    rdoConsulta(1) = rdoRestric("AG07CODPERFIL")
    rdoConsulta(2) = strCodFranja
    vntNuevoCodRest = GetNewCode(rdoConsulta)
    CrearArbolExclusivosFranjas = vntNuevoCodRest
    rdoNuevoRestric("AG12NUMRESFRJA") = vntNuevoCodRest
    rdoNuevoRestric.Update
    rdoConsulta.Close
    Set rdoConsulta = Nothing
    rdoNuevoRestric.Close
    Set rdoNuevoRestric = Nothing
  End If
rdoRestric.Close
rdoConsulta2.Close
End Function
Private Function CrearArbolExclusivosRecurso(ByVal strNivelSup As String, ByVal strCodRecurso As String) As Long
  Dim rdoRestric As rdoResultset
  Dim rdoNuevoRestric As rdoResultset
  Dim strSql As String
  Dim rdoConsulta As rdoQuery
  Dim rdoConsulta1 As rdoQuery
  Dim strNuevoNivelSup As String
  Dim vntNuevoCodRest As Variant

'  strSql = "SELECT  * FROM " _
'      & objEnv.GetValue("Database") & "AG1300 WHERE  AG1300.AG11CODRECURSO=" & Val(frmRecurso.txtText1(0)) _
'         & " And AG1300.AG13NUMRESTREC = " & strNivelSup
  strSql = "SELECT  * FROM " _
      & objEnv.GetValue("Database") & "AG1300 WHERE  AG1300.AG11CODRECURSO=?" _
         & " And AG1300.AG13NUMRESTREC = ?"
         
    Set rdoConsulta1 = objApp.rdoConnect.CreateQuery("", strSql)
    rdoConsulta1(0) = Val(frmRecurso.txtText1(0))
    rdoConsulta1(1) = strNivelSup
    Set rdoRestric = rdoConsulta1.OpenResultset()
'  Set rdoRestric = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
  If Not rdoRestric.EOF Then
    strSql = "SELECT  * FROM " _
        & objEnv.GetValue("Database") & "AG1300 WHERE  AG1300.AG11CODRECURSO=0" _
    
    Set rdoNuevoRestric = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
    rdoNuevoRestric.AddNew
    rdoNuevoRestric("AG11CODRECURSO") = strCodRecurso
         
    rdoNuevoRestric("AG16CODTIPREST") = rdoRestric("AG16CODTIPREST")
    rdoNuevoRestric("AG13VALDESDERES") = rdoRestric("AG13VALDESDERES")
    rdoNuevoRestric("AG13INDINCEXCL") = rdoRestric("AG13INDINCEXCL")
    If Not IsNull(rdoRestric("AG13VALHASTARES")) Then
      rdoNuevoRestric("AG13VALHASTARES") = rdoRestric("AG13VALHASTARES")
    End If
    rdoNuevoRestric("AG13NIVELJERAR") = rdoRestric("AG13NIVELJERAR")
    If rdoRestric("AG13NIVELSUPER") = 0 Then
      rdoNuevoRestric("AG13NIVELSUPER") = rdoRestric("AG13NIVELSUPER")
    Else
      rdoNuevoRestric("AG13NIVELSUPER") = CrearArbolExclusivosRecurso(rdoRestric("AG13NIVELSUPER"), strCodRecurso)
    End If
    rdoNuevoRestric("AG13FECADD") = objGen.GetDBDateTime
    rdoNuevoRestric("AG13FECUPD") = objGen.GetDBDateTime
    strSql = "select  AG13NUMRESTREC , AG11CODRECURSO from AG1300 " _
         & "where AG11CODRECURSO = ? " _
         & " order by  AG13NUMRESTREC desc"
        
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
    rdoConsulta(0) = strCodRecurso
    vntNuevoCodRest = GetNewCode(rdoConsulta)
    CrearArbolExclusivosRecurso = vntNuevoCodRest
    rdoNuevoRestric("AG13NUMRESTREC") = vntNuevoCodRest
    rdoNuevoRestric.Update
    rdoConsulta.Close
    Set rdoConsulta = Nothing
    rdoNuevoRestric.Close
    Set rdoNuevoRestric = Nothing
  End If
  rdoRestric.Close
  rdoConsulta1.Close
End Function

'Procedimiento que crea nuevas restricciones
'en resursos o franjas de un recurso
Private Function CrearRestricExclusivosFranjas() As Boolean
  Dim rdoRestric As rdoResultset
  Dim rdoNuevoRestric As rdoResultset
  Dim strSql As String
  Dim rdoConsulta As rdoQuery
  Dim rdoConsulta1 As rdoQuery
  Dim lngNivelSuperior As Long

  On Error GoTo ErrRestric
  CrearRestricExclusivosFranjas = True
'    strSql = "SELECT  * FROM " _
'        & objEnv.GetValue("Database") & "AG0400 WHERE  AG0400.AG11CODRECURSO=" & Val(frmFranjas.txtText1(12).Text) _
'         & " AND AG0400.AG07CODPERFIL=" & Val(frmFranjas.txtText1(11).Text) _
'         & " AND AG0400.AG04CODFRANJA<> " & Val(txtText1(5).Text)
    strSql = "SELECT  * FROM " _
        & objEnv.GetValue("Database") & "AG0400 WHERE  AG0400.AG11CODRECURSO=?" _
         & " AND AG0400.AG07CODPERFIL=?" _
         & " AND AG0400.AG04CODFRANJA<>? "
    
    Set rdoConsulta1 = objApp.rdoConnect.CreateQuery("", strSql)
    rdoConsulta1(0) = Val(frmFranjas.txtText1(12).Text)
    rdoConsulta1(1) = Val(frmFranjas.txtText1(11).Text)
    rdoConsulta1(2) = Val(txtText1(5).Text)
    Set rdoRestric = rdoConsulta1.OpenResultset()
    
'    Set rdoRestric = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
    strSql = "SELECT  * FROM " _
        & objEnv.GetValue("Database") & "AG1200 WHERE  AG1200.AG11CODRECURSO=0" _
         & " AND AG1200.AG07CODPERFIL=0 " _
         & " AND AG1200.AG04CODFRANJA=0" _
         & " And AG1200.AG12NUMRESFRJA =0"
    Set rdoNuevoRestric = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
    
    If strPadre = "Root" Then
      Do While Not rdoRestric.EOF
         rdoNuevoRestric.AddNew
         rdoNuevoRestric("AG11CODRECURSO") = rdoRestric("AG11CODRECURSO")
         rdoNuevoRestric("AG07CODPERFIL") = rdoRestric("AG07CODPERFIL")
         rdoNuevoRestric("AG04CODFRANJA") = rdoRestric("AG04CODFRANJA")
         strSql = "SELECT AG12NUMRESFRJA, AG11CODRECURSO, AG12NIVELSUPER, AG12VALDESDERES," _
             & " AG12VALHASTARES, AG16CODTIPREST, AG07CODPERFIL, AG04CODFRANJA FROM " _
             & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO= ? " _
             & " AND AG07CODPERFIL= ? AND AG04CODFRANJA= ? " _
             & " order by  AG12NUMRESFRJA desc"
         Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
         rdoConsulta(0) = rdoRestric("AG11CODRECURSO")
         rdoConsulta(1) = rdoRestric("AG07CODPERFIL")
         rdoConsulta(2) = rdoRestric("AG04CODFRANJA")
         vntNuevoCod = GetNewCode(rdoConsulta)
         rdoNuevoRestric("AG12NUMRESFRJA") = vntNuevoCod
         rdoNuevoRestric("AG16CODTIPREST") = cboSSDBCombo1(1).Value
         If Not IsNull(txtText1(0)) Then
           rdoNuevoRestric("AG12VALDESDERES") = txtText1(0)
         End If
         If cboSSDBCombo1(0).Value = 0 Then
           rdoNuevoRestric("AG12INDINCEXCL") = 1
         Else
           rdoNuevoRestric("AG12INDINCEXCL") = 0
         End If
         If Not IsNull(txtText1(1)) Then
           rdoNuevoRestric("AG12VALHASTARES") = txtText1(1)
         End If
         rdoNuevoRestric("AG12NIVELJERAR") = txtText1(2)
         rdoNuevoRestric("AG12NIVELSUPER") = 0
         rdoNuevoRestric("AG12FECADD") = objGen.GetDBDateTime
         rdoNuevoRestric("AG12FECUPD") = objGen.GetDBDateTime
         rdoNuevoRestric.Update
         rdoRestric.MoveNext
      Loop
      rdoRestric.Close
    Else
      Do While Not rdoRestric.EOF
           rdoNuevoRestric.AddNew
           rdoNuevoRestric("AG11CODRECURSO") = Val(txtText1(4))
           rdoNuevoRestric("AG07CODPERFIL") = Val(txtText1(6))
           rdoNuevoRestric("AG04CODFRANJA") = Val(txtText1(7))
           rdoNuevoRestric("AG16CODTIPREST") = cboSSDBCombo1(1).Value
           If Not IsNull(txtText1(0)) Then
             rdoNuevoRestric("AG12VALDESDERES") = txtText1(0)
           End If
           If cboSSDBCombo1(0).Value = 0 Then
             rdoNuevoRestric("AG12INDINCEXCL") = 1
           Else
             rdoNuevoRestric("AG12INDINCEXCL") = 0
           End If
           If Not IsNull(txtText1(1)) Then
             rdoNuevoRestric("AG12VALHASTARES") = txtText1(1)
           End If
           rdoNuevoRestric("AG12NIVELJERAR") = Val(txtText1(2))
           lngNivelSuperior = CrearArbolExclusivosFranjas(txtText1(3), rdoRestric("AG04CODFRANJA"))
           If lngNivelSuperior > 0 Then
             rdoNuevoRestric("AG12NIVELSUPER") = lngNivelSuperior
           Else
              rdoNuevoRestric.CancelUpdate
              CrearRestricExclusivosFranjas = False
              Exit Do
           End If
           rdoNuevoRestric("AG12FECADD") = objGen.GetDBDateTime
           rdoNuevoRestric("AG12FECUPD") = objGen.GetDBDateTime
           strSql = "SELECT AG12NUMRESFRJA, AG11CODRECURSO, AG12NIVELSUPER, AG12VALDESDERES," _
             & " AG12VALHASTARES, AG16CODTIPREST, AG07CODPERFIL, AG04CODFRANJA FROM " _
             & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO= ? " _
             & " AND AG07CODPERFIL= ? AND AG04CODFRANJA= ? " _
             & " order by  AG12NUMRESFRJA desc"
          
           Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
           rdoConsulta(0) = rdoRestric("AG11CODRECURSO")
           rdoConsulta(1) = rdoRestric("AG07CODPERFIL")
           rdoConsulta(2) = rdoRestric("AG04CODFRANJA")
           vntNuevoCod = GetNewCode(rdoConsulta)
           rdoNuevoRestric("AG12NUMRESFRJA") = vntNuevoCod
           rdoNuevoRestric.Update
           rdoRestric.MoveNext
       Loop
       rdoRestric.Close
    End If
    Set rdoRestric = Nothing
    Set rdoNuevoRestric = Nothing
    Exit Function
ErrRestric:
  MsgBox Err.Number & " - " & Err.Description, vbExclamation, "Agenda"
  CrearRestricExclusivosFranjas = False

End Function

Private Function CrearRestricExclusivosRecurso() As Boolean
  Dim rdoRestric As rdoResultset
  Dim rdoNuevoRestric As rdoResultset
  Dim strSql As String
  Dim rdoConsulta As rdoQuery
  Dim rdoConsulta1 As rdoQuery
  Dim lngNivelSuperior As Long

  On Error GoTo ErrRestric
  CrearRestricExclusivosRecurso = True
'    strSql = "SELECT  * FROM " _
'        & objEnv.GetValue("Database") & "AG1100 WHERE  AG1100.AG11CODRECURSO<>" & Val(frmRecurso.txtText1(0)) & " AND AG1100.AG14CODTIPRECU=" & frmRecurso.cboSSDBCombo1(1).Value
    strSql = "SELECT  * FROM " _
        & objEnv.GetValue("Database") & "AG1100 WHERE  AG1100.AG11CODRECURSO<>?  AND AG1100.AG14CODTIPRECU=?"
'    Set rdoRestric = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
  
    Set rdoConsulta1 = objApp.rdoConnect.CreateQuery("", strSql)
    rdoConsulta1(0) = Val(frmRecurso.txtText1(0))
    rdoConsulta1(1) = frmRecurso.cboSSDBCombo1(1).Value
    Set rdoRestric = rdoConsulta1.OpenResultset()
    
    strSql = "SELECT  * FROM " _
        & objEnv.GetValue("Database") & "AG1300 WHERE    AG1300.AG11CODRECURSO=0"
    Set rdoNuevoRestric = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
    If strPadre = "Root" Then
      Do While Not rdoRestric.EOF
         rdoNuevoRestric.AddNew
         rdoNuevoRestric("AG11CODRECURSO") = rdoRestric("AG11CODRECURSO")
         strSql = "select  AG13NUMRESTREC , AG11CODRECURSO from AG1300 " _
           & "where AG11CODRECURSO = ? " _
          & " order by  AG13NUMRESTREC desc"
          
         Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
         rdoConsulta(0) = rdoRestric("AG11CODRECURSO")
         vntNuevoCod = GetNewCode(rdoConsulta)
         rdoNuevoRestric("AG13NUMRESTREC") = vntNuevoCod
         rdoNuevoRestric("AG16CODTIPREST") = cboSSDBCombo1(1).Value
         If Not IsNull(txtText1(0)) Then
           rdoNuevoRestric("AG13VALDESDERES") = txtText1(0)
         End If
         If cboSSDBCombo1(0).Value = 0 Then
           rdoNuevoRestric("AG13INDINCEXCL") = 1
         Else
           rdoNuevoRestric("AG13INDINCEXCL") = 0
         End If
         If Not IsNull(txtText1(1)) Then
           rdoNuevoRestric("AG13VALHASTARES") = txtText1(1)
         End If
         rdoNuevoRestric("AG13NIVELJERAR") = Val(txtText1(2))
         rdoNuevoRestric("AG13NIVELSUPER") = 0
         rdoNuevoRestric("AG13FECADD") = objGen.GetDBDateTime
         rdoNuevoRestric("AG13FECUPD") = objGen.GetDBDateTime
         rdoNuevoRestric.Update
         rdoRestric.MoveNext
      Loop
      rdoRestric.Close
    Else
      Do While Not rdoRestric.EOF
           rdoNuevoRestric.AddNew
           rdoNuevoRestric("AG11CODRECURSO") = rdoRestric("AG11CODRECURSO")
           rdoNuevoRestric("AG16CODTIPREST") = cboSSDBCombo1(1).Value
           If Not IsNull(txtText1(0)) Then
             rdoNuevoRestric("AG13VALDESDERES") = txtText1(0)
           End If
           If cboSSDBCombo1(0).Value = 0 Then
             rdoNuevoRestric("AG13INDINCEXCL") = 1
           Else
             rdoNuevoRestric("AG13INDINCEXCL") = 0
           End If
           If Not IsNull(txtText1(1)) Then
             rdoNuevoRestric("AG13VALHASTARES") = txtText1(1)
           End If
           rdoNuevoRestric("AG13NIVELJERAR") = Val(txtText1(2))
           lngNivelSuperior = CrearArbolExclusivosRecurso(txtText1(3), rdoRestric("AG11CODRECURSO"))
           If lngNivelSuperior > 0 Then
             rdoNuevoRestric("AG13NIVELSUPER") = lngNivelSuperior
           Else
              rdoNuevoRestric.CancelUpdate
              CrearRestricExclusivosRecurso = False
              Exit Do
           End If
           rdoNuevoRestric("AG13FECADD") = objGen.GetDBDateTime
           rdoNuevoRestric("AG13FECUPD") = objGen.GetDBDateTime
           strSql = "select  AG13NUMRESTREC , AG11CODRECURSO from AG1300 " _
             & "where AG11CODRECURSO = ? " _
            & " order by  AG13NUMRESTREC desc"
          
           Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
           rdoConsulta(0) = rdoRestric("AG11CODRECURSO")
           vntNuevoCod = GetNewCode(rdoConsulta)
           rdoNuevoRestric("AG13NUMRESTREC") = vntNuevoCod
           rdoNuevoRestric.Update
           rdoRestric.MoveNext
       Loop
       rdoRestric.Close
    End If
    Set rdoRestric = Nothing
    Set rdoNuevoRestric = Nothing
    Exit Function
ErrRestric:
  MsgBox Err.Number & " - " & Err.Description, vbExclamation, "Agenda"
  CrearRestricExclusivosRecurso = False
End Function

'Procedimiento que borra aquellos registros cuyo campo Nivel
'Superior sea igual al par�metro pasado as este procedmiento
Sub BorradoNivelInf(lngNivelSup As Long)
  Dim rdoNivelInf As rdoResultset
  Dim strSql As String
  Dim qryConsulta As rdoQuery
  
  'Si el tipo de restricci�n es de Recurso selecciono regsitros
  'de la tabla AG1300, sino de la tabla de restriciones por
  'franja (AG1200)
'  If intTipoRest = agRestPorRecurso Then
'    strSql = "SELECT  AG13NUMRESTREC FROM " _
'        & objEnv.GetValue("Database") & "AG1300 WHERE AG11CODRECURSO=" & Val(frmRecurso.txtText1(0).Text) _
'        & " AND AG13NIVELSUPER=" & lngNivelSup
'  Else
'    strSql = "SELECT  AG12NUMRESFRJA FROM " _
'      & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO=" & Val(frmFranjas.txtText1(12).Text) _
'      & " AND AG07CODPERFIL=" & Val(frmFranjas.txtText1(11).Text) _
'      & " AND AG04CODFRANJA=" & Val(frmFranjas.txtText1(0).Text) _
'      & " AND AG12NIVELSUPER=" & lngNivelSup
'  End If
  If intTipoRest = agRestPorRecurso Then
    strSql = "SELECT  AG13NUMRESTREC FROM " _
        & objEnv.GetValue("Database") & "AG1300 WHERE AG11CODRECURSO=?" _
        & " AND AG13NIVELSUPER=?"
  Else
    strSql = "SELECT  AG12NUMRESFRJA FROM " _
      & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO=?" _
      & " AND AG07CODPERFIL=?" _
      & " AND AG04CODFRANJA=?" _
      & " AND AG12NIVELSUPER=?"
  End If
  
    Set qryConsulta = objApp.rdoConnect.CreateQuery("", strSql)
  
  'Abro el cursor y borro todos sus  registros
  If intTipoRest = agRestPorRecurso Then
    qryConsulta(0) = Val(frmRecurso.txtText1(0).Text)
    qryConsulta(1) = lngNivelSup
  Else
    qryConsulta(0) = Val(frmFranjas.txtText1(12).Text)
    qryConsulta(1) = Val(frmFranjas.txtText1(11).Text)
    qryConsulta(2) = Val(frmFranjas.txtText1(0).Text)
    qryConsulta(3) = lngNivelSup
  End If
    Set rdoNivelInf = qryConsulta.OpenResultset()
'  Set rdoNivelInf = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
  Do While Not rdoNivelInf.EOF
     rdoNivelInf.Delete
     rdoNivelInf.MoveNext
  Loop
  rdoNivelInf.Close
  Set rdoNivelInf = Nothing
  qryConsulta.Close
End Sub
'Procedimiento que carga la lista de valores con los valores del
'tipo de restricci�n
Sub CargarValoresTipo()
  Dim objField As clsCWFieldSearch
  
  Set objSearch = New clsCWSearch
  With objSearch
   .strTable = "AG1700"
   .strWhere = "WHERE AG16CODTIPREST=" & cboSSDBCombo1(1).Value
   .strOrder = "ORDER BY AG17DESVALTIRE"
  
   'Campos de la lista de  valores
   Set objField = .AddField("AG17VALDESDETRE")
   objField.strSmallDesc = "Valor Desde"
   Set objField = .AddField("AG17VALHASTATRE")
   objField.strSmallDesc = "Valor Hasta"
   Set objField = .AddField("AG17DESVALTIRE")
   objField.strSmallDesc = "Descripci�n                          "
      
   'Saco los valores en forma de Grid
   If .ViewSelect Then
     objWinInfo.CtrlGetInfo(txtText1(0)).blnReadOnly = False
     objWinInfo.CtrlGetInfo(txtText1(1)).blnReadOnly = False
     
     Call objWinInfo.CtrlSet(txtText1(0), .cllValues("AG17VALDESDETRE"))
     Call objWinInfo.CtrlSet(txtText1(1), .cllValues("AG17VALHASTATRE"))
    
     Call objWinInfo.CtrlSet(txtText1(8), .cllValues("AG17DESVALTIRE"))
     objWinInfo.CtrlGetInfo(txtText1(0)).blnReadOnly = True
     objWinInfo.CtrlGetInfo(txtText1(1)).blnReadOnly = True

   End If
  End With
  Set objSearch = Nothing
End Sub

'Procedimiento que carga una lista de valores con los datos de
'una tabla. Para ello se le pasa el nombre de la tabla,
'una colecci�n de columnas clave y una columna descripci�n

Sub CargarTablaAsoc(strTablaAsoc As String, cllColCod As Collection, strColDes As String)
  Dim objField As clsCWFieldSearch
  Dim strColumna As Variant
  Dim strCodusu As String
  Dim strApell As String
  
  Set objSearch = New clsCWSearch
  With objSearch
   .strTable = strTablaAsoc
   If strTablaAsoc = "SG0200" Then
     Frame1.Visible = True
     If optbus(0).Value = True Then
       strCodusu = UCase(InputBox("Introduce Codigo de Usuario:", "BUSQUEDA POR CODIGO DE USUARIO"))
       .strWhere = " WHERE SG02COD LIKE '" & strCodusu & "%'"
     ElseIf optbus(1).Value = True Then
       strApell = UCase(InputBox("Introduce el Primer Apellido:", "BUSQUEDA POR PRIMER APELLIDO"))
       .strWhere = " WHERE SG02APE1 LIKE '" & strApell & "%'"
     End If
   Else
   Frame1.Visible = False
   
       
     .strWhere = ""
   End If
   
   .strOrder = ""
   For Each strColumna In cllColCod
     
      Set objField = .AddField(strColumna)
   Next
   
   Set objField = .AddField(strColDes)
      
   If .ViewSelect Then
     objWinInfo.CtrlGetInfo(txtText1(0)).blnReadOnly = False
     

     For Each strColumna In cllColCod
       Call objWinInfo.CtrlSet(txtText1(0), objWinInfo.CtrlGet(txtText1(0)) & .cllValues(strColumna))
     Next
     
     Call objWinInfo.CtrlSet(txtText1(8), .cllValues(strColDes))
     

   End If
  objWinInfo.CtrlGetInfo(txtText1(0)).blnReadOnly = True
  End With
  Set objSearch = Nothing
End Sub

'Funci�n que devuelve el Tipo de Restricci�n (la descripi�n)
'de un nodo del arbol de jerarquias de restricciones.
'Se le pasa como par�metro todo el texto que hay en el nodo
'del Tree View

Function NodeGetType(strTextNode As String) As String
  On Error Resume Next
  Dim intContador As Integer

  For intContador = Len(strTextNode) To 1 Step -1
    If Mid(strTextNode, intContador, 1) = "(" Then
      Exit For
    End If
  Next

  NodeGetType = Mid(strTextNode, 1, intContador - 1)

End Function

'Funci�n que devuelve True si el tipo de restrici�n del nodo que
'se le pasa como argumento esta reptido en esa condici�n

Function RepeatType(ByVal nodNode As ComctlLib.Node) As Boolean
  Dim nodDummy As Node
  
  RepeatType = False
  Set nodDummy = nodNode

  Do While treeGetLevel(nodDummy) > 1
    If Trim(NodeGetType(nodDummy.Text)) = Trim(cboSSDBCombo1(1).Text) Then
      RepeatType = True
    End If
    Set nodDummy = nodDummy.Parent
  Loop

End Function



'Funci�n que inserta el nuevo nodo en le arbol y genera el n�mero
'de secuencia de la restricci�n. Adem�s devuelve le nivel de
'jerarquia del nodo. Recibe como parametro el nombre del nodo
'padre
Public Function A�adirNodo(strPadreActual As String) As Long
  Dim nodNodo As Node
  Dim rdoQueryCodNivel As rdoQuery
  Dim rdoConsulta As rdoQuery
  Dim rdoCodNivel As rdoResultset
  Dim strSql As String
  Dim strKeyActual As String
 'Variable que contendra los dos primeros caracteres del Nivel
 'de Jerarquia
  Dim strN1 As String
 'Variable que contendra los dos caracteres del medio del Nivel
 'de Jerarquia
  Dim strN2 As String
 'Variable que contendra los dos �ltimos caracteres del Nivel
 'de Jerarquia
  Dim strN3 As String
 'Variable que contendra el texto asociado al nodo del arbol
  Dim strNodeText As String
  
  ' Abrimos cursor
'  If intTipoRest = agRestPorRecurso Then
'    strSql = "select  AG13NUMRESTREC , AG11CODRECURSO from AG1300 " _
'           & "where AG11CODRECURSO =" & Val(txtText1(4)) _
'          & " order by  AG13NUMRESTREC desc"
'  Else
'    strSql = "SELECT AG12NUMRESFRJA, AG11CODRECURSO, AG12NIVELSUPER, AG12VALDESDERES," _
'      & " AG12VALHASTARES, AG16CODTIPREST, AG07CODPERFIL, AG04CODFRANJA FROM " _
'      & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO=" & Val(frmFranjas.txtText1(12).Text) _
'      & " AND AG07CODPERFIL=" & Val(frmFranjas.txtText1(11).Text) _
'      & " AND AG04CODFRANJA=" & Val(frmFranjas.txtText1(0).Text) _
'      & " order by  AG12NUMRESFRJA desc"
'  End If
  If intTipoRest = agRestPorRecurso Then
    strSql = "select  AG13NUMRESTREC , AG11CODRECURSO from AG1300 " _
           & "where AG11CODRECURSO = ? " _
          & " order by  AG13NUMRESTREC desc"
  Else
    strSql = "SELECT AG12NUMRESFRJA, AG11CODRECURSO, AG12NIVELSUPER, AG12VALDESDERES," _
      & " AG12VALHASTARES, AG16CODTIPREST, AG07CODPERFIL, AG04CODFRANJA FROM " _
      & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO= ? " _
      & " AND AG07CODPERFIL= ? AND AG04CODFRANJA= ? " _
      & " order by  AG12NUMRESFRJA desc"
  End If

  ' Empieza transacci�n
    Call objApp.rdoConnect.BeginTrans
    'Obtengo el nuevo c�digo
    
  'LAS 13.4.99
  Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
  If intTipoRest = agRestPorRecurso Then
        rdoConsulta(0) = Val(txtText1(4))
  Else
        rdoConsulta(0) = Val(frmFranjas.txtText1(12).Text)
        rdoConsulta(1) = Val(frmFranjas.txtText1(11).Text)
        rdoConsulta(2) = Val(frmFranjas.txtText1(0).Text)
  End If
    
  vntNuevoCod = GetNewCode(rdoConsulta)
  rdoConsulta.Close
    
    'Paso el nuevo c�digo al campo del cursor y a la textbox
    Call objWinInfo.CtrlStabilize(txtText1(5), vntNuevoCod)
    strKeyActual = "N" & vntNuevoCod
 
  If strPadreActual = "Root" Then
'    If intTipoRest = agRestPorRecurso Then
'      strSql = "SELECT AG13NIVELJERAR FROM AG1300 WHERE AG11CODRECURSO=" & Val(txtText1(4)) & " ORDER BY AG13NIVELJERAR DESC"
'    Else
'      strSql = "SELECT AG12NIVELJERAR FROM AG1200 " _
'         & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO=" & Val(txtText1(4).Text) _
'         & " AND AG07CODPERFIL=" & Val(txtText1(6).Text) _
'        & " AND AG04CODFRANJA=" & Val(txtText1(7).Text) _
'        & " ORDER BY AG12NIVELJERAR DESC"
'    End If
    If intTipoRest = agRestPorRecurso Then
      strSql = "SELECT AG13NIVELJERAR FROM AG1300 WHERE AG11CODRECURSO=? ORDER BY AG13NIVELJERAR DESC"
    Else
      strSql = "SELECT AG12NIVELJERAR FROM AG1200 " _
         & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO=?" _
         & " AND AG07CODPERFIL=?" _
        & " AND AG04CODFRANJA=?" _
        & " ORDER BY AG12NIVELJERAR DESC"
    End If
    
    Set rdoQueryCodNivel = objApp.rdoConnect.CreateQuery("", strSql)
   'Establezco el n� de registros a recuperar a 1
    rdoQueryCodNivel.MaxRows = 1
    
  If intTipoRest = agRestPorRecurso Then
        rdoQueryCodNivel(0) = Val(txtText1(4))
  Else
        rdoQueryCodNivel(0) = Val(txtText1(4).Text)
        rdoQueryCodNivel(1) = Val(txtText1(6).Text)
        rdoQueryCodNivel(2) = Val(txtText1(7).Text)
  End If
    
    
    Set rdoCodNivel = rdoQueryCodNivel.OpenResultset(rdOpenKeyset)
    If rdoCodNivel.RowCount = 0 Then
      A�adirNodo = 10000
    Else
      strN1 = Left(Format(rdoCodNivel(0), "000000"), 2)
      strN2 = Right(Left(Format(rdoCodNivel(0), "000000"), 4), 2)
      strN3 = Right(Format(rdoCodNivel(0), "000000"), 2)
      A�adirNodo = Val(Str(Val(strN1) + 1) & "0000")
     End If
   rdoCodNivel.Close
   rdoQueryCodNivel.Close
    
    
  Else
'    If intTipoRest = agRestPorRecurso Then
'      strSql = "SELECT  AG13NIVELJERAR FROM " _
'          & objEnv.GetValue("Database") & "AG1300 WHERE AG11CODRECURSO=" & Val(frmRecurso.txtText1(0).Text) _
'          & " AND AG13NIVELSUPER=" & Val(Mid(strPadreActual, 2)) _
'          & " ORDER BY AG13NIVELJERAR DESC"
'  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
'  Rem =====================================================================
'    Else
'      strSql = "SELECT  AG12NIVELJERAR FROM " _
'        & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO=" & Val(frmFranjas.txtText1(12).Text) _
'        & " AND AG07CODPERFIL=" & Val(frmFranjas.txtText1(11).Text) _
'        & " AND AG04CODFRANJA=" & Val(frmFranjas.txtText1(0).Text) _
'        & " AND AG12NIVELSUPER=" & Val(Mid(strPadreActual, 2)) _
'        & " ORDER BY AG12NIVELJERAR DESC"
'    End If
    If intTipoRest = agRestPorRecurso Then
      strSql = "SELECT  AG13NIVELJERAR FROM " _
          & objEnv.GetValue("Database") & "AG1300 WHERE AG11CODRECURSO=?" _
          & " AND AG13NIVELSUPER=?" _
          & " ORDER BY AG13NIVELJERAR DESC"
    Else
      strSql = "SELECT  AG12NIVELJERAR FROM " _
        & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO=?" _
        & " AND AG07CODPERFIL=?" _
        & " AND AG04CODFRANJA=?" _
        & " AND AG12NIVELSUPER=?" _
        & " ORDER BY AG12NIVELJERAR DESC"
    End If
    Set rdoQueryCodNivel = objApp.rdoConnect.CreateQuery("", strSql)

   'Establezco el n� de registros a recuperar a 1
    rdoQueryCodNivel.MaxRows = 1
    If intTipoRest = agRestPorRecurso Then
        rdoQueryCodNivel(0) = Val(frmRecurso.txtText1(0).Text)
        rdoQueryCodNivel(1) = Val(Mid(strPadreActual, 2))
    Else
        rdoQueryCodNivel(0) = Val(frmFranjas.txtText1(12).Text)
        rdoQueryCodNivel(1) = Val(frmFranjas.txtText1(11).Text)
        rdoQueryCodNivel(2) = Val(frmFranjas.txtText1(0).Text)
        rdoQueryCodNivel(3) = Val(Mid(strPadreActual, 2))
    End If
    
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
    
    Set rdoCodNivel = rdoQueryCodNivel.OpenResultset(rdOpenKeyset)
    If rdoCodNivel.EOF Then
        If intTipoRest = agRestPorRecurso Then
          strSql = "SELECT  AG13NIVELJERAR FROM " _
               & objEnv.GetValue("Database") & "AG1300 WHERE AG11CODRECURSO=" & Val(frmRecurso.txtText1(0).Text) _
               & " AND AG13NUMRESTREC=" & Val(Mid(strPadreActual, 2))
        Else
          strSql = "SELECT  AG12NIVELJERAR FROM " _
            & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO=" & Val(frmFranjas.txtText1(12).Text) _
            & " AND AG07CODPERFIL=" & Val(frmFranjas.txtText1(11).Text) _
            & " AND AG04CODFRANJA=" & Val(frmFranjas.txtText1(0).Text) _
            & " AND AG12NUMRESFRJA=" & Val(Mid(strPadreActual, 2))
        End If
        If objGen.NodeGetLevel(TreeView1(0).SelectedItem) = 2 Then
          A�adirNodo = Val(Left(Format(GetTableColumn(strSql)(1), "000000"), 2) & "0100")
        Else
          A�adirNodo = Val(Left(Format(GetTableColumn(strSql)(1), "000000"), 4) & "01")
        End If
      
    Else
      strN1 = Left(Format(rdoCodNivel(0), "000000"), 2)
      strN2 = Right(Left(Format(rdoCodNivel(0), "000000"), 4), 2)
      strN3 = Right(Format(rdoCodNivel(0), "000000"), 2)
    
    'Creo el nivel de jerarquia
      If strN3 = "00" Then
        A�adirNodo = Val(strN1 & Format(Str(Val(strN2) + 1), "00") & strN3)
      Else
        A�adirNodo = Val(strN1 & strN2 & Format(Str(Val(strN2) + 1), "00"))
      End If
    End If
    rdoCodNivel.Close
    Set rdoCodNivel = Nothing
    rdoQueryCodNivel.Close
    Set rdoQueryCodNivel = Nothing
  End If
 'Obtengo el texto del nodo
  strNodeText = cboSSDBCombo1(1).Text & " ( " & txtText1(0).Text & " - " & Trim(txtText1(1).Text & " " & txtText1(8).Text) & " )"
 'Creo el nodo
  Set nodNodo = TreeView1(0).Nodes.Add(strPadreActual, tvwChild, strKeyActual, strNodeText)
  If cboSSDBCombo1(0).Value = agExclusion Then
    nodNodo.Image = "i28"
  Else
    nodNodo.Image = "i27"
  End If
  
  nodNodo.EnsureVisible
  nodNodo.Sorted = False
  Set nodNodo = Nothing
End Function

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  Dim nodNode As Node
  
  Call objApp.SplashOn
  
  If intTipoRest = agRestPorRecurso Then
    Call CargarTreeView(intTipoRest, Me, 0, Val(frmRecurso.txtText1(0)))
  End If
  If intTipoRest = agRestPorFranja Then
    lngCodFranja = Val(frmFranjas.txtText1(0))
    TreeView1(0).Height = 2300
    TreeView1(0).Top = 720 + 2315
    Load TreeView1(1)
    TreeView1(1).Left = TreeView1(0).Left
    TreeView1(1).Width = TreeView1(0).Width
    TreeView1(1).Top = 720
    TreeView1(1).Height = 2280
    TreeView1(1).Visible = True
    
    Call CargarTreeView(agRestPorRecurso, Me, 1, lngCodRecurso)
    Call CargarTreeView(intTipoRest, Me, 0, lngCodRecurso, lngCodPerfil, lngCodFranja)
    
  End If

  
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
   'Introducir la documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Mantenimiento de Restricciones"
    .cwDAT = "30-09-97"
    .cwAUT = "I�aki Gabiola"
    .cwDES = "Esta ventana permite mantener las diferentes restricciones de recursos como de franjas"
    .cwUPD = "30-09-97 - I�aki Gabiola - Creaci�n del m�dulo"
    .cwEVT = ""
  End With
   ' Definici�n de la Tabla
  
  
  With objDetailInfo
    
    .strName = "Restriccion"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strDataBase = objEnv.GetValue("Database")
    .blnHasMaint = True
    If intTipoRest = agRestPorRecurso Then
      .strTable = "AG1300"
      .blnAskPrimary = False
      .strWhere = "AG1300.AG11CODRECURSO=" & Val(frmRecurso.txtText1(0)) & " AND AG1300.AG13NIVELSUPER=0"
      ' Definici�n del orden de clasificaci�n
      Call .FormAddOrderField("AG16CODTIPREST", cwAscending)
      Call .FormAddOrderField("AG13NUMRESTREC", cwAscending)
      fraFrame1(0).Caption = "Restricciones por Recurso"
      Me.Caption = "AGENDA. Restricciones del Recurso: " & frmRecurso.txtText1(1).Text
      Me.HelpContextID = 15
      
      txtText1(0).DataField = "AG13VALDESDERES"
      txtText1(1).DataField = "AG13VALHASTARES"
      txtText1(2).DataField = "AG13NIVELJERAR"
      txtText1(3).DataField = "AG13NIVELSUPER"
      txtText1(4).DataField = "AG11CODRECURSO"
      txtText1(5).DataField = "AG13NUMRESTREC"
      
      cboSSDBCombo1(0).DataField = "AG13INDINCEXCL"
      
    Else
      .strTable = "AG1200"
      .blnAskPrimary = False
      .strWhere = "AG1200.AG11CODRECURSO=" & Val(frmFranjas.txtText1(12).Text) _
         & " AND AG1200.AG07CODPERFIL=" & Val(frmFranjas.txtText1(11).Text) _
         & " AND AG1200.AG04CODFRANJA=" & Val(frmFranjas.txtText1(0).Text) _
         & " AND AG1200.AG12NIVELSUPER=0"

     
      ' Definici�n del orden de clasificaci�n
      Call .FormAddOrderField("AG16CODTIPREST", cwAscending)
      Call .FormAddOrderField("AG12NUMRESFRJA", cwAscending)
      fraFrame1(0).Caption = "Restricciones por Franja"
      Me.Caption = "AGENDA. Restricciones del Recurso: " & frmRecurso.txtText1(1).Text _
            & " Perfil : " & frmPerfiles.txtText1(3).Text
      Me.HelpContextID = 20
      
      txtText1(0).DataField = "AG12VALDESDERES"
      txtText1(1).DataField = "AG12VALHASTARES"
      txtText1(2).DataField = "AG12NIVELJERAR"
      txtText1(3).DataField = "AG12NIVELSUPER"
      txtText1(6).DataField = "AG07CODPERFIL"
      txtText1(5).DataField = "AG12NUMRESFRJA"
      txtText1(7).DataField = "AG04CODFRANJA"
      txtText1(4).DataField = "AG11CODRECURSO"
      
      cboSSDBCombo1(0).DataField = "AG12INDINCEXCL"
    End If
    
    cboSSDBCombo1(0).AddItem ("'0','INCLUSI�N'")
    cboSSDBCombo1(0).AddItem ("'1','EXCLUSI�N'")

    
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    ' Definici�n de los objetos del form
    Call .FormCreateInfo(objDetailInfo)
        
        
    .CtrlGetInfo(txtText1(2)).blnInGrid = False
    .CtrlGetInfo(txtText1(3)).blnInGrid = False
    .CtrlGetInfo(txtText1(4)).blnInGrid = False
    .CtrlGetInfo(txtText1(5)).blnInGrid = False
    .CtrlGetInfo(txtText1(6)).blnInGrid = False
    .CtrlGetInfo(txtText1(7)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(0)).blnReadOnly = True
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True
    
    If intTipoRest = agRestPorRecurso Then
      ' Propiedades del campo clave para asignaci�n autom�tica
      .CtrlGetInfo(txtText1(4)).blnValidate = False
      .CtrlGetInfo(txtText1(5)).blnValidate = False
      .CtrlGetInfo(cboSSDBCombo1(1)).strSql = "SELECT AG16CODTIPREST, AG16DESTIPREST, AG18CODTIPDATO, AG16TABLAVALOR, AG16COLCODVALO, AG16COLDESVALO  FROM " & objEnv.GetValue("Database") & "AG1600 WHERE AG16CODTIPREST IN (SELECT AG16CODTIPREST FROM " & objEnv.GetValue("Database") & "AG1500 WHERE AG14CODTIPRECU=" & frmRecurso.cboSSDBCombo1(1).Value & ")"
      .CtrlGetInfo(txtText1(4)).vntDefaultValue = Val(frmRecurso.txtText1(0).Text)
      .CtrlGetInfo(txtText1(6)).blnNegotiated = False
      .CtrlGetInfo(txtText1(7)).blnNegotiated = False
    
    Else
      .CtrlGetInfo(cboSSDBCombo1(1)).strSql = "SELECT AG16CODTIPREST, AG16DESTIPREST, AG18CODTIPDATO, AG16TABLAVALOR, AG16COLCODVALO, AG16COLDESVALO  FROM " & objEnv.GetValue("Database") & "AG1600 WHERE AG16CODTIPREST IN (SELECT AG16CODTIPREST FROM " & objEnv.GetValue("Database") & "AG1500 WHERE AG14CODTIPRECU=" & frmRecurso.cboSSDBCombo1(1).Value & ")"
      .CtrlGetInfo(txtText1(4)).blnValidate = False
      .CtrlGetInfo(txtText1(5)).blnValidate = False
      .CtrlGetInfo(txtText1(6)).blnValidate = False
      .CtrlGetInfo(txtText1(7)).blnValidate = False
      .CtrlGetInfo(txtText1(4)).vntDefaultValue = Val(frmFranjas.txtText1(12).Text)
      .CtrlGetInfo(txtText1(7)).vntDefaultValue = Val(frmFranjas.txtText1(0).Text)
      .CtrlGetInfo(txtText1(6)).vntDefaultValue = Val(frmFranjas.txtText1(11).Text)

    End If
    .CtrlGetInfo(txtText1(2)).blnValidate = False
   
   'Para elnombre de tabla del tipo de restricci�n
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(1)), "AG16CODTIPREST", "SELECT  AG16CODTIPREST, AG16TABLAVALOR  FROM AG1600 WHERE AG16CODTIPREST = ?", True)
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(1)), txtText1(9), "AG16TABLAVALOR")

    Call .WinRegister
    Call .WinStabilize
  End With
  Call objApp.SplashOff
  objWinInfo.DataMoveFirst

  If cboSSDBCombo1(1).Rows >= 4 Then
   intMaxLevels = 4
  Else
    intMaxLevels = cboSSDBCombo1(1).Rows + 1
  End If
  If cboSSDBCombo1(1).Rows = 0 Then
    objWinInfo.objWinMainForm.intAllowance = cwAllowReadOnly
    objWinInfo.WinPrepareScr
  End If
  strPadre = "Root"
  If cboSSDBCombo1(1).Columns("Tabla").Text <> "SG0200" Then
    Frame1.Visible = False
  End If
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  
  'If intIndex = 0 Then
  '  grdDBGrid1(0).Columns(1).Text = GetTableColumn("SELECT  AG16DESTIPREST  FROM " & objEnv.GetValue("Database") & "AG1600 WHERE AG16CODTIPREST =" & grdDBGrid1(0).Columns(1).Value)("AG16DESTIPREST")
  'End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
 If Me.ActiveControl.Name <> "TreeView1(0)" Then
  If Me.ActiveControl.DataField = "AG16CODTIPREST" Then
    Me.MousePointer = vbHourglass
     'Versi�n EXE
    'Load frmTipRest
    'Call frmTipRest.Show(vbModal)
    'Unload frmTipRest
    'Set frmTipRest = Nothing
    
     'Versi�n DLL
    lngCodTipRec = frmRecurso.cboSSDBCombo1(1).Value
    Call objSecurity.LaunchProcess(agTipoRecurso)
    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)))
    lngCodTipRec = 0
    Me.MousePointer = vbDefault
  End If
 End If
End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
  Dim rdoNivelBorrado As rdoResultset
  Dim qryConsulta As rdoQuery
  Dim strSql As String
  Dim intNumNivel As Integer
  'Despues de borrar el regsitro hay que borrar todos sus
  'descendientes
  If Not blnError Then
    intNumNivel = treeGetLevel(TreeView1(0).SelectedItem)
'    If intTipoRest = agRestPorRecurso Then
'      strSql = "SELECT  AG13NUMRESTREC FROM " _
'          & objEnv.GetValue("Database") & "AG1300 WHERE AG11CODRECURSO=" & Val(frmRecurso.txtText1(0).Text) _
'          & " AND AG13NIVELSUPER=" & lngNumSecuencia
'    Else
'      strSql = "SELECT  AG12NUMRESFRJA FROM " _
'        & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO=" & Val(frmFranjas.txtText1(12).Text) _
'        & " AND AG07CODPERFIL=" & Val(frmFranjas.txtText1(11).Text) _
'        & " AND AG04CODFRANJA=" & Val(frmFranjas.txtText1(0).Text) _
'        & " AND AG12NIVELSUPER=" & lngNumSecuencia
'    End If
    If intTipoRest = agRestPorRecurso Then
      strSql = "SELECT  AG13NUMRESTREC FROM " _
          & objEnv.GetValue("Database") & "AG1300 WHERE AG11CODRECURSO=?" _
          & " AND AG13NIVELSUPER=?"
    Else
      strSql = "SELECT  AG12NUMRESFRJA FROM " _
        & objEnv.GetValue("Database") & "AG1200 WHERE AG11CODRECURSO=?" _
        & " AND AG07CODPERFIL=?" _
        & " AND AG04CODFRANJA=?" _
        & " AND AG12NIVELSUPER=?"
    End If
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
    Set qryConsulta = objApp.rdoConnect.CreateQuery("", strSql)
    If intTipoRest = agRestPorRecurso Then
        qryConsulta(0) = Val(frmRecurso.txtText1(0).Text)
        qryConsulta(1) = lngNumSecuencia
    Else
        qryConsulta(0) = Val(frmFranjas.txtText1(12).Text)
        qryConsulta(1) = Val(frmFranjas.txtText1(11).Text)
        qryConsulta(2) = Val(frmFranjas.txtText1(0).Text)
        qryConsulta(3) = lngNumSecuencia
    End If
    Set rdoNivelBorrado = qryConsulta.OpenResultset(rdOpenKeyset)
    
'    Set rdoNivelBorrado = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
    Do While Not rdoNivelBorrado.EOF
       'Si el nodo es de nivel 1 hay que borrar sus descendientes
       If intNumNivel = 1 Then
          Call BorradoNivelInf(rdoNivelBorrado(0))
       End If
       rdoNivelBorrado.Delete
       rdoNivelBorrado.MoveNext
    Loop
    rdoNivelBorrado.Close
    Set rdoNivelBorrado = Nothing
    'Borro el nodo del arbol
    Call TreeView1(0).Nodes.Remove("N" & lngNumSecuencia)
    'Valido las eliminaciones
    objApp.rdoConnect.CommitTrans
    objWinInfo.DataRefresh
  Else
    'Deshago los cambios
    objApp.rdoConnect.RollbackTrans
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  'Si el tipo e restricci�n tiene asociado una tabla no dejo
  'cambiar el campo desde y hasta
  'If txtText1(9).Text <> "" Then
  '  objWinInfo.CtrlGetInfo(txtText1(0)).blnReadOnly = True
  '  objWinInfo.CtrlGetInfo(txtText1(1)).blnReadOnly = True
  'Else
  '  objWinInfo.CtrlGetInfo(txtText1(0)).blnReadOnly = False
  '  objWinInfo.CtrlGetInfo(txtText1(1)).blnReadOnly = False
  'End If
  'Call objWinInfo.WinPrepareScr
End Sub

Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim rdoValores As rdoResultset
  Dim strSql As String
  Dim blnDentroRango As Boolean
  Dim strMensaje As String
  Dim qryConsulta As rdoQuery
  
  'Comprobaci�n de repeticion de tipo de restricci�n en una misma
  'condici�n
  If RepeatType(TreeView1(0).SelectedItem) Then
    Call objError.SetError(cwCodeMsg, "El Tipo de Restricci�n ya se encuentra elegido en esta condici�n")
    Call objError.Raise
    blnCancel = True
    Exit Sub
  End If

  If cboSSDBCombo1(1).Columns("FechaBaja").Text <> "" Then
    Call objError.SetError(cwCodeMsg, "El Tipo de Restricci�n elegido est� dado de baja")
    Call objError.Raise
    blnCancel = True
    Exit Sub
  End If

  'Si el tipo de restricci�n no tiene asociado ninguna tabla
  'entonces valido los valores desde y hasta con los valores
  'permitidos
  If txtText1(9) = "" Then
    'Comprobaci�n campo Desde menor que campo Hasta
    If IsNumeric(txtText1(0)) And IsNumeric(txtText1(1)) Then
     If Val(txtText1(0)) > Val(txtText1(1)) Then
        Call objError.SetError(cwCodeMsg, "El valor Desde es mayor que el valor Hasta")
        Call objError.Raise
        blnCancel = True
        Exit Sub
      End If
    End If
  
    'Comprobaci�n de que le valor desde y hasta esten dentro de los
    'valores admitidos
    blnDentroRango = False
'    strSql = "SELECT AG16CODTIPREST,AG17VALTIRES,AG17VALDESDETRE, " _
'         & " AG17VALHASTATRE,AG17DESVALTIRE FROM AG1700 " _
'         & " WHERE AG16CODTIPREST=" & cboSSDBCombo1(1).Value
    strSql = "SELECT AG16CODTIPREST,AG17VALTIRES,AG17VALDESDETRE, " _
         & " AG17VALHASTATRE,AG17DESVALTIRE FROM AG1700 " _
         & " WHERE AG16CODTIPREST=?"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
  
    Set qryConsulta = objApp.rdoConnect.CreateQuery("", strSql)
        qryConsulta(0) = cboSSDBCombo1(1).Value
    Set rdoValores = qryConsulta.OpenResultset()

'    Set rdoValores = objApp.rdoConnect.OpenResultset(strSql, rdOpenForwardOnly)
    
    Do While Not rdoValores.EOF
      
      If Not IsNull(rdoValores("AG17VALHASTATRE")) Then
        
        If Val(rdoValores("AG17VALDESDETRE")) <= Val(txtText1(0)) And _
            Val(rdoValores("AG17VALHASTATRE")) >= Val(txtText1(1)) Then
 
           blnDentroRango = True
        End If
      
      End If
      
      If IsNull(rdoValores("AG17VALHASTATRE")) Then
        If Val(rdoValores("AG17VALDESDETRE")) <= Val(txtText1(0)) Then
           blnDentroRango = True
        End If
      End If
      rdoValores.MoveNext
    
    Loop
    If cboSSDBCombo1(0).Text <> "" Then
      If cboSSDBCombo1(0).Value = 0 Then
        If Not blnDentroRango Then
          Call objError.SetError(cwCodeMsg, "Los valores Desde y Hasta no est�n dentro de los l�mites permitidos")
          Call objError.Raise
          blnCancel = True
          Exit Sub
        End If
      End If
    End If
  End If
  
  If chkExclusivo(0).Value = 1 Then
    If intTipoRest = agRestPorFranja Then
      strMensaje = "Esta acci�n provocara que se generen nuevas restricciones en las dem�s franjas. �Desea continuar?"
    Else
      strMensaje = "Esta acci�n provocara que se generen nuevas restricciones en los dem�s recursos del mismo tipo de recurso. �Desea continuar?"
    End If
    If MsgBox(strMensaje, vbExclamation + vbYesNoCancel, "Agenda") <> vbYes Then
      blnCancel = True
    End If
  End If
  
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    
  If Not blnError Then
    If chkExclusivo(0).Value = 1 Then
      If intTipoRest = agRestPorRecurso Then
        If CrearRestricExclusivosRecurso Then
          objApp.rdoConnect.CommitTrans
        Else
          objApp.rdoConnect.RollbackTrans
        End If
      Else
        If CrearRestricExclusivosFranjas Then
          objApp.rdoConnect.CommitTrans
        Else
          objApp.rdoConnect.RollbackTrans
        End If
      End If
    Else
      objApp.rdoConnect.CommitTrans
    End If
  Else
    objApp.rdoConnect.RollbackTrans
  End If
End Sub


Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
 'Obtengo el n�mero de secuencia del registro a borrar para
 'poder utilizarlo para poder borrar sus descendientes
  If intTipoRest = agRestPorRecurso Then
    lngNumSecuencia = objWinInfo.objWinActiveForm.rdoCursor("AG13NUMRESTREC")
  Else
    lngNumSecuencia = objWinInfo.objWinActiveForm.rdoCursor("AG12NUMRESFRJA")
  End If
 'Empiezo una transacci�n por si hay errores
  Call objApp.rdoConnect.BeginTrans
  objSecurity.RegSession
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
 Dim strNodeText As String
 With objWinInfo
  'Si estoy a�adiendo un nuevo regsitro a�ado un nuevo nodo
   If .intWinStatus = cwModeSingleAddRest Then
  
     Call .CtrlStabilize(txtText1(2), A�adirNodo(strPadre))
     If strPadre = "Root" Then
       Call .CtrlStabilize(txtText1(3), 0)
     Else
       Call .CtrlStabilize(txtText1(3), Mid(strPadre, 2))
     End If
   End If
  'Si estoy editando (modificando) mofifico el texto del nodo
   If .intWinStatus = cwModeSingleEdit Then
     strNodeText = cboSSDBCombo1(1).Text & " ( " & txtText1(0).Text & " - " & Trim(txtText1(1).Text & " " & txtText1(8).Text) & " )"

     If intTipoRest = agRestPorRecurso Then
       TreeView1(0).Nodes("N" & .objWinActiveForm.rdoCursor("AG13NUMRESTREC")).Text = strNodeText
       If cboSSDBCombo1(0).Value = agExclusion Then
         TreeView1(0).Nodes("N" & .objWinActiveForm.rdoCursor("AG13NUMRESTREC")).Image = "i28"
       Else
         TreeView1(0).Nodes("N" & .objWinActiveForm.rdoCursor("AG13NUMRESTREC")).Image = "i27"
       End If
     Else
        TreeView1(0).Nodes("N" & .objWinActiveForm.rdoCursor("AG12NUMRESFRJA")).Text = strNodeText
       If cboSSDBCombo1(0).Value = agExclusion Then
         TreeView1(0).Nodes("N" & .objWinActiveForm.rdoCursor("AG12NUMRESFRJA")).Image = "i28"
       Else
         TreeView1(0).Nodes("N" & .objWinActiveForm.rdoCursor("AG12NUMRESFRJA")).Image = "i27"
       End If
    
   End If
  End If
  End With
End Sub





' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub









' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
  If cboSSDBCombo1(intIndex).Text <> "" Then
    Call objWinInfo.CtrlDataChange
  End If
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)
  Dim rdoValores As rdoResultset
  Dim strSql As String
  Dim blnListaValores As Boolean
  Dim blnDentroRango As Boolean
  
  Call objWinInfo.CtrlDataChange
  'Si cambio de Tipo de Restricci�n miro si tiene tabla asociada
  'o tiene valores del tipo 1-De Pie, 2-Sentado....
  If intIndex = 1 Then
    
    Call objWinInfo.CtrlSet(txtText1(0), "")
    
    Call objWinInfo.CtrlSet(txtText1(1), "")

    'Miro si tiene tabla asociada y en caso afirmativo saco una
    'lista de valores con los valores de esa tabla
    If cboSSDBCombo1(1).Columns("Tabla").Text <> "" Then
        
        
      'objWinInfo.CtrlGetInfo(txtText1(0)).blnReadOnly = True
      'objWinInfo.CtrlGetInfo(txtText1(1)).blnReadOnly = True
      With cboSSDBCombo1(1)
        Call CargarTablaAsoc(.Columns("Tabla").Text, SearchFields(.Columns("ColCod").Text), .Columns("ColDes").Text)
      End With
      'Call objWinInfo.FormChangeColor(objWinInfo.objWinActiveForm)
      'Call objWinInfo.WinPrepareScr
      Exit Sub
    Else
      'objWinInfo.CtrlGetInfo(txtText1(0)).blnReadOnly = False
      'objWinInfo.CtrlGetInfo(txtText1(1)).blnReadOnly = False

    End If
   'Miro si es una tipo de restricci�n con valores como 1-De Pie,
   '2-Sentado... , en cuyo caso activo la variable blnListaValores
   'y saco una lista de valores con esos valores
    blnListaValores = True
    
    '** Comentado para que saque todos los valores tanto numericos
    '** como cadena
    
    'strSql = "SELECT AG16CODTIPREST,AG17VALTIRES,AG17VALDESDETRE, " _
    '   & " AG17VALHASTATRE,AG17DESVALTIRE FROM AG1700 " _
    '   & " WHERE AG16CODTIPREST=" & cboSSDBCombo1(1).Value
    'Set rdoValores = objApp.rdoConnect.OpenResultset(strSql, rdOpenForwardOnly)
    'Do While Not rdoValores.EOF
      
    '  If Not IsNull(rdoValores("AG17VALHASTATRE")) Then
    '    blnListaValores = False
    '    Exit Do
    '  End If
      
    '  rdoValores.MoveNext
    
    'Loop
    'Set rdoValores = Nothing
    
    If blnListaValores Then
       Call CargarValoresTipo
    End If
    Call objWinInfo.FormChangeColor(objWinInfo.objWinActiveForm)
    Call objWinInfo.WinPrepareScr

  End If
End Sub


Private Sub TreeView1_NodeClick(intIndex As Integer, ByVal Node As ComctlLib.Node)
  
 'Cunado se hace un click en un nodo primero comprobamos su
 'nivel para permitir a�adir mas regsitros.
 'Despues cargamos el cursor del Form con los hijos del nodo
 'seleccionado
  If intIndex = 0 Then
  With objWinInfo
    If treeGetLevel(Node) >= intMaxLevels Then
      .objWinActiveForm.intAllowance = cwAllowReadOnly
      
    Else
      .objWinActiveForm.intAllowance = cwAllowAll
    End If
    
    strPadre = Node.Key
    If strPadre = "Root" Then
      If intTipoRest = agRestPorRecurso Then
        .objWinActiveForm.strWhere = "AG1300.AG11CODRECURSO=" & Val(frmRecurso.txtText1(0)) & " AND AG1300.AG13NIVELSUPER=0"
      Else
        .objWinActiveForm.strWhere = "AG1200.AG11CODRECURSO=" & Val(frmFranjas.txtText1(12).Text) _
            & " AND AG1200.AG07CODPERFIL=" & Val(frmFranjas.txtText1(11).Text) _
            & " AND AG1200.AG04CODFRANJA=" & Val(frmFranjas.txtText1(0).Text) _
            & " AND AG1200.AG12NIVELSUPER=0"
        End If
    Else
      If intTipoRest = agRestPorRecurso Then
        .objWinActiveForm.strWhere = "AG1300.AG11CODRECURSO=" & Val(frmRecurso.txtText1(0)) & " AND AG1300.AG13NIVELSUPER=" & Val(Mid(strPadre, 2))
      Else
        .objWinActiveForm.strWhere = "AG1200.AG11CODRECURSO=" & Val(frmFranjas.txtText1(12).Text) _
          & " AND AG1200.AG07CODPERFIL=" & Val(frmFranjas.txtText1(11).Text) _
          & " AND AG1200.AG04CODFRANJA=" & Val(frmFranjas.txtText1(0).Text) _
          & " AND AG1200.AG12NIVELSUPER=" & Val(Mid(strPadre, 2))
      End If
    End If
    .DataRefresh
   End With
   End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


