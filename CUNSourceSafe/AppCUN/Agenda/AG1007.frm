VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Begin VB.Form frmFranjas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AGENDA. Franjas"
   ClientHeight    =   8340
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   11625
   ControlBox      =   0   'False
   HelpContextID   =   18
   Icon            =   "AG1007.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   8340
   ScaleWidth      =   11625
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Tipos de Actuaci�n"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4290
      Index           =   1
      Left            =   120
      TabIndex        =   32
      Tag             =   "Mantenimiento de Tipos de Actuaci�n por Franja "
      Top             =   3780
      Width           =   11340
      Begin TabDlg.SSTab tabTab1 
         Height          =   3810
         Index           =   1
         Left            =   135
         TabIndex        =   33
         TabStop         =   0   'False
         Top             =   420
         Width           =   11025
         _ExtentX        =   19447
         _ExtentY        =   6720
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AG1007.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(7)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(16)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(17)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(11)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "cboSSDBCombo1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtcDateCombo1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtcDateCombo1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(16)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(13)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(18)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(14)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(15)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(17)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(5)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).ControlCount=   17
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AG1007.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(3)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG01NUMHORANT"
            Height          =   330
            HelpContextID   =   40101
            Index           =   5
            Left            =   2520
            TabIndex        =   23
            Tag             =   "Horas Antelaci�n"
            Top             =   1665
            Width           =   1515
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Index           =   17
            Left            =   1762
            TabIndex        =   58
            TabStop         =   0   'False
            Tag             =   "Descripci�n Actuaci�n"
            Top             =   930
            Width           =   4890
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG04CODFRANJA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   15
            Left            =   4695
            TabIndex        =   55
            TabStop         =   0   'False
            Tag             =   "C�digo Franja"
            Text            =   "C�d. Fra"
            Top             =   480
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG07CODPERFIL"
            Height          =   330
            HelpContextID   =   40101
            Index           =   14
            Left            =   3870
            TabIndex        =   54
            TabStop         =   0   'False
            Tag             =   "C�digo Pefil"
            Text            =   "C�d. Perf"
            Top             =   480
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG01NUMASIGADM"
            Height          =   330
            HelpContextID   =   40101
            Index           =   18
            Left            =   255
            TabIndex        =   22
            Tag             =   "Asignaciones Admitidas"
            Top             =   1665
            Width           =   1990
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG11CODRECURSO"
            Height          =   330
            HelpContextID   =   40101
            Index           =   13
            Left            =   2880
            TabIndex        =   41
            TabStop         =   0   'False
            Tag             =   "C�digo Recurso"
            Text            =   "C�d. Rec"
            Top             =   480
            Visible         =   0   'False
            Width           =   810
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            HelpContextID   =   30101
            Index           =   16
            Left            =   255
            Locked          =   -1  'True
            TabIndex        =   19
            Tag             =   "C�digo Actuaci�n"
            Top             =   930
            Width           =   1065
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG01FECINVGACF"
            Height          =   330
            Index           =   0
            Left            =   255
            TabIndex        =   24
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   2565
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   1
            Left            =   -74910
            TabIndex        =   34
            TabStop         =   0   'False
            Top             =   90
            Width           =   8655
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15266
            _ExtentY        =   5054
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3315
            Index           =   3
            Left            =   -74880
            TabIndex        =   38
            Top             =   360
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18865
            _ExtentY        =   5847
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG01FECFIVGACF"
            Height          =   330
            Index           =   1
            Left            =   2550
            TabIndex        =   25
            Tag             =   "Fecha Fin Vigencia"
            Top             =   2565
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   1
            Left            =   6960
            TabIndex        =   20
            Tag             =   "Departamento"
            Top             =   930
            Width           =   3375
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1905
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   5953
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            _ExtentX        =   5953
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Horas Antelaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   2520
            TabIndex        =   59
            Top             =   1440
            Width           =   1470
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Asignaciones Admitidas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   255
            TabIndex        =   53
            Top             =   1425
            Width           =   2010
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   6960
            TabIndex        =   52
            Top             =   690
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   2565
            TabIndex        =   39
            Top             =   2310
            Width           =   1650
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   1770
            TabIndex        =   37
            Top             =   690
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   255
            TabIndex        =   36
            Top             =   2310
            Width           =   1860
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Actuaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   255
            TabIndex        =   35
            Top             =   690
            Width           =   870
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Franjas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Index           =   0
      Left            =   120
      TabIndex        =   27
      Tag             =   "Mantenimiento de Franjas"
      Top             =   480
      Width           =   11340
      Begin TabDlg.SSTab tabTab1 
         Height          =   2775
         HelpContextID   =   90001
         Index           =   0
         Left            =   150
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   360
         Width           =   11010
         _ExtentX        =   19420
         _ExtentY        =   4895
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AG1007.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(4)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(3)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(9)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(10)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(12)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(13)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(14)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cboSSDBCombo1(6)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "cboSSDBCombo1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtcDateCombo1(2)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(10)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "fraFrame1(3)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(2)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(3)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(4)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(12)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(11)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "cmdCommand1(0)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(6)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(7)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(20)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "updUpDown1(1)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).ControlCount=   27
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AG1007.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin ComCtl2.UpDown updUpDown1 
            Height          =   330
            Index           =   1
            Left            =   5175
            TabIndex        =   15
            Top             =   1350
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   582
            _Version        =   327681
            Value           =   1
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtText1(6)"
            BuddyDispid     =   196610
            BuddyIndex      =   6
            OrigLeft        =   5220
            OrigTop         =   1335
            OrigRight       =   5460
            OrigBottom      =   1680
            Max             =   30000
            Min             =   1
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   20
            Left            =   7815
            TabIndex        =   56
            TabStop         =   0   'False
            Top             =   1695
            Visible         =   0   'False
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG04INTERVCITA"
            Height          =   330
            Index           =   7
            Left            =   5730
            Locked          =   -1  'True
            TabIndex        =   16
            Tag             =   "Intervalo Cita"
            Top             =   1365
            Width           =   1155
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG04NUMCITADMI"
            Height          =   330
            Index           =   6
            Left            =   4140
            TabIndex        =   14
            Tag             =   "Citas Admitidas"
            Top             =   1350
            Width           =   1035
         End
         Begin VB.CommandButton cmdCommand1 
            Caption         =   "Re&stricciones"
            Enabled         =   0   'False
            Height          =   435
            Index           =   0
            Left            =   9000
            TabIndex        =   18
            Top             =   2100
            Width           =   1770
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG07CODPERFIL"
            Height          =   330
            Index           =   11
            Left            =   9840
            TabIndex        =   49
            Tag             =   "C�digo Perfil"
            Text            =   "C�d.Perf"
            Top             =   1230
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG11CODRECURSO"
            Height          =   330
            Index           =   12
            Left            =   8940
            TabIndex        =   48
            Tag             =   "C�digo Recurso"
            Text            =   "C�d.Rec."
            Top             =   1245
            Visible         =   0   'False
            Width           =   750
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG04HORFIFRJMM"
            Height          =   330
            Index           =   4
            Left            =   3240
            MaxLength       =   2
            TabIndex        =   4
            Tag             =   "Minutos Fin"
            Top             =   660
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG04HORFIFRJHH"
            Height          =   330
            Index           =   3
            Left            =   2760
            MaxLength       =   2
            TabIndex        =   3
            Tag             =   "Hora Fin"
            Top             =   660
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG04HORINFRJMM"
            Height          =   330
            Index           =   2
            Left            =   2040
            MaxLength       =   2
            TabIndex        =   2
            Tag             =   "Minutos Inicio"
            Top             =   660
            Width           =   390
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Aplicable a"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1500
            Index           =   3
            Left            =   150
            TabIndex        =   43
            Top             =   1140
            Width           =   3855
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Lunes"
               DataField       =   "AG04INDLUNFRJA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   0
               Left            =   210
               TabIndex        =   7
               Tag             =   "Lunes"
               Top             =   300
               Width           =   945
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Martes"
               DataField       =   "AG04INDMARFRJA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   1
               Left            =   210
               TabIndex        =   8
               Tag             =   "Martes "
               Top             =   660
               Width           =   945
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Mi�rcoles"
               DataField       =   "AG04INDMIEFRJA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   2
               Left            =   210
               TabIndex        =   9
               Tag             =   "Mi�rcoles "
               Top             =   1020
               Width           =   1305
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Jueves"
               DataField       =   "AG04INDJUEFRJA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   3
               Left            =   1515
               TabIndex        =   10
               Tag             =   "Jueves"
               Top             =   300
               Width           =   945
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Viernes"
               DataField       =   "AG04INDVIEFRJA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   4
               Left            =   1515
               TabIndex        =   11
               Tag             =   "Viernes "
               Top             =   660
               Width           =   1065
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "S�bado"
               DataField       =   "AG04INDSABFRJA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   5
               Left            =   2625
               TabIndex        =   12
               Tag             =   "S�bado "
               Top             =   300
               Width           =   1065
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Domingo"
               DataField       =   "AG04INDDOMFRJA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   6
               Left            =   2640
               TabIndex        =   13
               Tag             =   "Domingo"
               Top             =   645
               Width           =   1185
            End
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG05NUMINCIDEN"
            Height          =   330
            Index           =   10
            Left            =   7755
            TabIndex        =   42
            Tag             =   "N� de incidencia"
            Text            =   "N�Incidencia"
            Top             =   1245
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG04HORINFRJHH"
            Height          =   330
            Index           =   1
            Left            =   1530
            MaxLength       =   2
            TabIndex        =   1
            Tag             =   "Hora Inicio"
            Top             =   660
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG04CODFRANJA"
            Height          =   330
            Index           =   0
            Left            =   150
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "C�digo Franja"
            Top             =   660
            Width           =   1150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2280
            Index           =   0
            Left            =   -74880
            TabIndex        =   26
            Top             =   360
            Width           =   10740
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18944
            _ExtentY        =   4022
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG04FECBAJA"
            Height          =   330
            Index           =   2
            Left            =   8985
            TabIndex        =   6
            Tag             =   "Fecha Baja "
            Top             =   660
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "PR12CODACTIVIDAD"
            Height          =   330
            Index           =   0
            Left            =   3960
            TabIndex        =   5
            Tag             =   "Tipo Actividad"
            Top             =   660
            Width           =   3015
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AutoRestore     =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   5318
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "Planificable"
            Columns(2).Name =   "Planificable"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   5318
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "AG04MODASIGCITA"
            Height          =   330
            Index           =   6
            Left            =   4140
            TabIndex        =   17
            Tag             =   "Modo de Asignaci�n"
            Top             =   2040
            Width           =   3255
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            FieldDelimiter  =   "'"
            FieldSeparator  =   ","
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   5741
            Columns(1).Caption=   "Modo de Asignaci�n"
            Columns(1).Name =   "Modo de Asignaci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5741
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Modo de Asignaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   4140
            TabIndex        =   57
            Top             =   1800
            Width           =   1740
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Intervalo Cita"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   5715
            TabIndex        =   51
            Top             =   1125
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Citas Admitidas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   4140
            TabIndex        =   50
            Top             =   1125
            Width           =   1305
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Actividad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   3945
            TabIndex        =   47
            Top             =   415
            Width           =   810
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   ":"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   3165
            TabIndex        =   46
            Top             =   705
            Width           =   75
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   2760
            TabIndex        =   45
            Top             =   415
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   ":"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   1950
            TabIndex        =   44
            Top             =   705
            Width           =   75
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Baja"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   9015
            TabIndex        =   40
            Top             =   415
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   1530
            TabIndex        =   30
            Top             =   415
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   165
            TabIndex        =   29
            Top             =   415
            Width           =   600
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   28
      Top             =   8055
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmFranjas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Rem bind
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Public blnFlag As Boolean
Dim intGranoTiempo As Integer

Public Function GetUserName(strUser As String) As String
  Dim strSql As String
  Dim cllUserData As New Collection
  
  strSql = "SELECT SG02NOM, SG02APE1, SG02APE2 FROM SG0200 WHERE SG02COD=" & objGen.ValueToSQL(strUser, cwString)
  Set cllUserData = GetTableColumn(strSql)
  If cllUserData("SG02NOM") <> "" Then
    GetUserName = Trim(cllUserData("SG02NOM")) & " " & Trim(cllUserData("SG02APE1")) & " " & Trim(cllUserData("SG02APE2"))
  Else
    GetUserName = ""
  End If
End Function
' Funci�n que devuelve True si hay solapamiento entre las dos
' franjas que se comparan. Si no hay solapamiento devuelve False.
' Se pasan 8 parametros

' - intHIniCursor : Hora Inicio del registro apuntado por el cursor
' - intMIniCursor : Minutos Inicio del registro apuntado por el cursor
' - intHFinCursor : Hora Fin del registro apuntado por el cursor
' - intMFinCursor : Minutos Fin del registro apuntado por el cursor
' - intHIniComp : Hora Inicio del registro a comparar
' - intMIniComp : Minutos Inicio del registro a comparar
' - intHFinComp : Hora Fin del registro apuntado a comparar
' - intMFinComp : Minutos Fin del registro apuntado a comparar


Public Function Solapamiento(intHIniCursor As Integer, intMIniCursor As Integer, _
                             intHFinCursor As Integer, intMFinCursor As Integer, _
                             intHIniComp As Integer, intMIniComp As Integer, _
                             intHFinComp As Integer, intMFinComp As Integer) As Boolean
  
  Dim dteInicioCursor As Date
  Dim dteFinCursor As Date
  Dim dteInicioComp As Date
  Dim dteFinComp As Date

  dteInicioCursor = CDate(intHIniCursor & ":" & intMIniCursor)
  dteFinCursor = CDate(intHFinCursor & ":" & intMFinCursor)
  dteInicioComp = CDate(intHIniComp & ":" & intMIniComp)
  dteFinComp = CDate(intHFinComp & ":" & intMFinComp)
  
  If dteInicioComp >= dteFinCursor Or dteFinComp <= dteInicioCursor Then
    Solapamiento = False
  Else
    Solapamiento = True
  End If

End Function



' Procedimiento que devuelve el c�digo de motivo de incidencia
' en la variable global intCodMotInci si hay disminuci�n de
' disponibilidad, si no hay devuelve 0 en intCodMotInci

'Posibles valores en la tabla de motivos de incidencia (AG0600)

'AG06CODMOTINCI AG06DESMOTINCI                           AG06INDINCDISM
'-------------- ---------------------------------------- --------------
'            10 Cambio en periodo de vigencia                         1
'            20 Cambio a festivo de un d�a especial                   1                                   2
'            30 Nuevo per�odo de vigencia                             1
'            40 Nuevo d�a especial                                    1
'            50 Borrado de d�a especial no festivo                       1
'            60 Borrado de per�odo de vigencia                        1
'            70 Disminuci�n de Recursos                               1
'            80 Borrado de Recurso                                    1
'            90 Nuevo Periodo de Vigencia de un Perfil                                    1
'           100 Borrado de Actuaci�n de una franja
'           110 Borrado de una Franja con Actividad Planificable
'           120 Cambio en un d�a aplicable
'           140 Cambio en Tipo de Actividad de una Franja

Sub Disminucion_Franjas()
  Dim strSql As String
  Dim rdoActividad As rdoResultset
  Dim rdoConsulta As rdoQuery
' Inicializo intCodMotInci
  intCodMotInci = 0
 
  With objWinInfo.objWinActiveForm
   
  ' Comprobaci�n Hora Inicio
    If .rdoCursor("AG04HORINFRJHH") <> Val(txtText1(1).Text) Then
      intCodMotInci = 110
    End If
    If .rdoCursor("AG04HORINFRJMM") <> Val(txtText1(2).Text) Then
      intCodMotInci = 110
    End If
  ' Comprobaci�n Hora Fin
    If .rdoCursor("AG04HORFIFRJHH") <> Val(txtText1(3).Text) Then
      intCodMotInci = 110
    End If
    If .rdoCursor("AG04HORFIFRJMM") <> Val(txtText1(4).Text) Then
      intCodMotInci = 110
    End If
 
  ' Comprobaci�n cambio dias aplicables
   
    If cboSSDBCombo1(0).Text = "" Or txtText1(20).Text = "-1" Then
    ' Lunes
      If .rdoCursor("AG04INDLUNFRJA") <> -chkCheck1(0).Value _
                    And chkCheck1(0).Value = 0 Then
        intCodMotInci = 120
      End If
    ' Martes
      If .rdoCursor("AG04INDMARFRJA") <> -chkCheck1(1).Value _
                    And chkCheck1(1).Value = 0 Then
        intCodMotInci = 120
      End If
    ' Mi�rcoles
      If .rdoCursor("AG04INDMIEFRJA") <> -chkCheck1(2).Value _
                  And chkCheck1(2).Value = 0 Then
        intCodMotInci = 120
      End If
    ' Jueves
      If .rdoCursor("AG04INDJUEFRJA") <> -chkCheck1(3).Value _
                  And chkCheck1(3).Value = 0 Then
        intCodMotInci = 120
      End If
    ' Viernes
      If .rdoCursor("AG04INDVIEFRJA") <> -chkCheck1(4).Value _
                  And chkCheck1(4).Value = 0 Then
        intCodMotInci = 120
      End If
    ' S�bado
      If .rdoCursor("AG04INDSABFRJA") <> -chkCheck1(5).Value _
                  And chkCheck1(5).Value = 0 Then
        intCodMotInci = 120
      End If
    ' Domingo
      If .rdoCursor("AG04INDDOMFRJA") <> -chkCheck1(6).Value _
                  And chkCheck1(6).Value = 0 Then
        intCodMotInci = 120
      End If
    End If
    
    If txtText1(20).Text = "0" Then
    ' Lunes
      If .rdoCursor("AG04INDLUNFRJA") <> -chkCheck1(0).Value _
                    And chkCheck1(0).Value = 1 Then
        intCodMotInci = 120
      End If
    ' Martes
      If .rdoCursor("AG04INDMARFRJA") <> -chkCheck1(1).Value _
                    And chkCheck1(1).Value = 1 Then
        intCodMotInci = 120
      End If
    ' Mi�rcoles
      If .rdoCursor("AG04INDMIEFRJA") <> -chkCheck1(2).Value _
                  And chkCheck1(2).Value = 1 Then
        intCodMotInci = 120
      End If
    ' Jueves
      If .rdoCursor("AG04INDJUEFRJA") <> -chkCheck1(3).Value _
                  And chkCheck1(3).Value = 1 Then
        intCodMotInci = 120
      End If
    ' Viernes
      If .rdoCursor("AG04INDVIEFRJA") <> -chkCheck1(4).Value _
                  And chkCheck1(4).Value = 1 Then
        intCodMotInci = 120
      End If
    ' S�bado
      If .rdoCursor("AG04INDSABFRJA") <> -chkCheck1(5).Value _
                  And chkCheck1(5).Value = 1 Then
        intCodMotInci = 120
      End If
    ' Domingo
      If .rdoCursor("AG04INDDOMFRJA") <> -chkCheck1(6).Value _
                  And chkCheck1(6).Value = 1 Then
        intCodMotInci = 120
      End If
    End If
    
    
    'Cambio en Tipo Actividad
  '  If .rdoCursor("PR12CODACTIVIDAD") <> cboSSDBCombo1(0).Columns(0).Value Then
  '    intCodMotInci = 140
  '  End If
    If txtText1(20).Text = "0" And Not IsNull(.rdoCursor("PR12CODACTIVIDAD")) Then
'      strSql = "SELECT PR12INDPLANIFIC FROM " & objEnv.GetValue("Database") & "PR1200 WHERE PR12CODACTIVIDAD =" & .rdoCursor("PR12CODACTIVIDAD")
      strSql = "SELECT PR12INDPLANIFIC FROM " & objEnv.GetValue("Database") & "PR1200 WHERE PR12CODACTIVIDAD =?"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
            rdoConsulta(0) = .rdoCursor("PR12CODACTIVIDAD")
            Set rdoActividad = rdoConsulta.OpenResultset()
'      Set rdoActividad = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
      If Not rdoActividad.EOF Then
        If rdoActividad(0) = -1 Then
           intCodMotInci = 140
        End If
        rdoActividad.Close
        rdoConsulta.Close
      End If
    End If
  End With
  Set rdoActividad = Nothing
  
  'Baja l�gica de una franja
  If dtcDateCombo1(2).Text <> "" Then
    intCodMotInci = 110
  End If
End Sub



Private Sub cboSSDBCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange

  If intIndex = 0 And blnFlag = True Then
    
    If txtText1(20).Text = "0" Then
      With objWinInfo
        'N�mero Horas Antelaci�n para Asignaci�n
        'Call objWinInfo.CtrlSet(txtText1(5), "")
        '.CtrlGetInfo(txtText1(5)).blnReadOnly = True
        '.CtrlGetInfo(txtText1(5)).blnMandatory = False
        
        Call objWinInfo.CtrlSet(txtText1(6), "")
        .CtrlGetInfo(txtText1(6)).blnReadOnly = True
        .CtrlGetInfo(txtText1(6)).blnMandatory = False
        
       ' Call objWinInfo.CtrlSet(txtText1(7), "")
       ' .CtrlGetInfo(txtText1(7)).blnReadOnly = True
       ' .CtrlGetInfo(txtText1(7)).blnMandatory = False
        
        'Call objWinInfo.CtrlSet(txtText1(8), "")
        '.CtrlGetInfo(txtText1(8)).blnReadOnly = True
        
       ' Call objWinInfo.CtrlSet(txtText1(9), "")
       ' .CtrlGetInfo(txtText1(9)).blnReadOnly = True
        fraFrame1(1).Enabled = False
        '.cllWinForms("fraFrame1(1)").intAllowance = cwAllowReadOnly
        '.cllWinForms("fraFrame1(2)").intAllowance = cwAllowReadOnly
        Call .WinPrepareScr
        Call .FormChangeColor(.objWinActiveForm)
      End With
      'updUpDown1(0).Enabled = False
      updUpDown1(1).Enabled = False
      
      If objWinInfo.intWinStatus = cwModeSingleEdit Then
        cmdCommand1(0).Enabled = False
      End If
    Else
      With objWinInfo
        '.CtrlGetInfo(txtText1(5)).blnReadOnly = False
        '.CtrlGetInfo(txtText1(5)).blnMandatory = True
        .CtrlGetInfo(txtText1(6)).blnReadOnly = False
        .CtrlGetInfo(txtText1(6)).blnMandatory = True
      '  .CtrlGetInfo(txtText1(7)).blnReadOnly = False
      '  If frmRecurso.cboSSDBCombo1(3).Value <> 3 Then
      '    .CtrlGetInfo(txtText1(7)).blnMandatory = False
      '  Else
      '    .CtrlGetInfo(txtText1(7)).blnMandatory = True
      '  End If
       ' .CtrlGetInfo(txtText1(8)).blnReadOnly = False
       ' .CtrlGetInfo(txtText1(9)).blnReadOnly = False
        fraFrame1(1).Enabled = True
       ' .cllWinForms("fraFrame1(1)").intAllowance = cwAllowAll
        '.cllWinForms("fraFrame1(2)").intAllowance = cwAllowAll
        Call .WinPrepareScr
        Call .FormChangeColor(.objWinActiveForm)
      End With
      'updUpDown1(0).Enabled = True
      updUpDown1(1).Enabled = True
      
      If objWinInfo.intWinStatus = cwModeSingleEdit Then
        cmdCommand1(0).Enabled = True
      End If
  End If
  cboSSDBCombo1(0).SetFocus
End If
      

End Sub


Private Sub cboSSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
  If intIndex = 0 And KeyCode = 46 Then
   
    Call objWinInfo.CtrlSet(cboSSDBCombo1(intIndex), "")
    With objWinInfo
      '.CtrlGetInfo(txtText1(5)).blnReadOnly = False
      '.CtrlGetInfo(txtText1(5)).blnMandatory = True
      .CtrlGetInfo(txtText1(6)).blnReadOnly = False
      .CtrlGetInfo(txtText1(6)).blnMandatory = True
     ' .CtrlGetInfo(txtText1(7)).blnReadOnly = False
     ' If frmRecurso.cboSSDBCombo1(3).Value <> 3 Then
     '  .CtrlGetInfo(txtText1(7)).blnMandatory = False
     ' Else
     '   .CtrlGetInfo(txtText1(7)).blnMandatory = True
     ' End If
     ' .CtrlGetInfo(txtText1(8)).blnReadOnly = False
     ' .CtrlGetInfo(txtText1(9)).blnReadOnly = False
      fraFrame1(1).Enabled = True
     ' .cllWinForms("fraFrame1(1)").intAllowance = cwAllowAll
      '.cllWinForms("fraFrame1(2)").intAllowance = cwAllowAll
      Call .WinPrepareScr
      Call .FormChangeColor(.objWinActiveForm)
    End With
    'updUpDown1(0).Enabled = True
    updUpDown1(1).Enabled = True
    
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
      cmdCommand1(0).Enabled = True
    End If
    cboSSDBCombo1(0).SetFocus

  End If
End Sub

Private Sub cmdCommand1_Click(intIndex As Integer)
  'Cargamos el form de restriciones
  If objWinInfo.objWinActiveForm.blnChanged = True Then
    objWinInfo.DataRefresh
  End If
  
  intTipoRest = agRestPorFranja
 'Versi�n DLL
  Call objSecurity.LaunchProcess(agRestricciones)
 
 'Versi�n EXE
  'Load frmRestricciones
  'Call frmRestricciones.Show(vbModal)
  'Unload frmRestricciones
  'Set frmRestricciones = Nothing

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strSql As String
  Dim rdoParametro As rdoResultset

' **********************************
' Declaraci�n de variables
' **********************************
  
' Form maestro
  Dim objMasterInfo As New clsCWForm
' Form detalle 1
  Dim objDetailInfo1 As New clsCWForm
' Form detalle 2
'  Dim objDetailInfo2 As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
' **********************************
' Fin declaraci�n de variables
' **********************************
  blnRollback = False
' Se visualiza el formulario de splash
  Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
' Documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Mantenimiento de Franjas"
    .cwDAT = "29-07-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite mantener las franjas, actuaciones por franja, restricciones por franja y permisos por franja"
    
    .cwUPD = "29-07-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
' Declaraci�n de las caracter�sticas del form maestro
  With objMasterInfo
   ' Asigno nombre al frame
    .strName = "Franjas"

  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
  ' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG0400"
    .strWhere = "AG0400.AG11CODRECURSO=" & lngCodRecurso & " AND AG0400.AG07CODPERFIL=" & lngCodPerfil
    .blnAskPrimary = False
  ' Asignaci�n del nivel de acceso del usuario al formulario
    If IsDate(frmPerfiles.dtcDateCombo1(3).Date) Then
      If DateDiff("d", frmPerfiles.dtcDateCombo1(3).Date, CDate(Format(objGen.GetDBDateTime, "dd/mm/yyyy"))) > 0 Then
        .intAllowance = cwAllowReadOnly
      Else
        .intAllowance = cwAllowAll
      End If
    End If
    If IsDate(frmPerfiles.dtcDateCombo1(2).Date) Then
      If DateDiff("d", frmPerfiles.dtcDateCombo1(2).Date, CDate(Format(objGen.GetDBDateTime, "dd/mm/yyyy"))) > 0 Then
        .intAllowance = cwAllowReadOnly
      Else
        .intAllowance = cwAllowAll
      End If
    End If
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
  ' Definici�n del impreso
    Call .objPrinter.Add("AG0007", "Listado 1 de Franjas por Perfil")
  
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG04HORINFRJHH", cwAscending)
    Call .FormAddOrderField("AG04HORINFRJMM", cwAscending)
    Call .FormAddOrderField("AG04HORFIFRJHH", cwAscending)
    Call .FormAddOrderField("AG04HORFIFRJMM", cwAscending)
    Call .FormAddOrderField("AG04CODFRANJA", cwAscending)
  ' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla Franjas")
    Call .FormAddFilterWhere(strKey, "AG04CODFRANJA", "C�digo Franja", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG04INDLUNFRJA", "Indicador Lunes Aplicable", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG04INDMARFRJA", "Indicador Martes Aplicable", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG04INDMIEFRJA", "Indicador Mi�rcoles Aplicable", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG04INDJUEFRJA", "Indicador Jueves Aplicable", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG04INDVIEFRJA", "Indicador Viernes Aplicable", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG04INDSABFRJA", "Indicador Sabado Aplicable", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG04INDDOMFRJA", "Indicador Domingo Aplicable", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG04HORINFRJHH", "Hora Inicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG04HORINFRJMM", "Minutos  Inicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG04HORFIFRJHH", "Hora Fin", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG04HORFIFRJMM", "Minutos  Inicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR12CODACTIVIDAD", "C�digo Actividad", cwNumeric)
    
  ' Creaci�n de los criterios de ordenaci�n
    Call .FormAddFilterOrder(strKey, "AG04CODFRANJA", "C�digo Franja")
    Call .FormAddFilterOrder(strKey, "PR12CODACTIVIDAD", "C�digo Actividad")
   
  End With
  
' Declaraci�n de las caracter�sticas del form Tipos de Actuaci�n
  With objDetailInfo1
    ' Asigno nombre al frame
    .strName = "Tipos_Actuaci�n"
  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
  ' Asignaci�n del contenedor(frame)padre del form
    Set .objFatherContainer = fraFrame1(0)
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(1)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(3)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG0100"
    .intAllowance = objMasterInfo.intAllowance
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("PR01CODACTUACION", cwAscending)
  ' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("AG11CODRECURSO", txtText1(12))
    Call .FormAddRelation("AG07CODPERFIL", txtText1(11))
    Call .FormAddRelation("AG04CODFRANJA", txtText1(0))
  
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
  ' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Actuaciones")
    Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo Actuaci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Departamento", cwNumeric)
    
  ' Creaci�n de los criterios de ordenaci�n
    Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo Actuaci�n")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Departamento")

  End With
   
' Declaraci�n de las caracter�sticas del form detalle2
'  With objDetailInfo2
   ' Asigno nombre al frame
'    .strName = "Permisos"
  ' Asignaci�n del contenedor(frame)del form
'    Set .objFormContainer = fraFrame1(2)
  ' Asignaci�n del contenedor(frame)padre del form
'    Set .objFatherContainer = fraFrame1(0)
'  ' Asignaci�n del objeto tab del form
'    Set .tabMainTab = tabTab1(2)
  ' Asignaci�n del objeto grid del form
'    Set .grdGrid = grdDBGrid1(4)
  ' Asignaci�n de la tabla asociada al form
'    .strDataBase = objEnv.GetValue("Database")
'    .strTable = "AG1000"
'    .blnAskPrimary = False
  ' M�todo de ordenacion del form
'    Call .FormAddOrderField("AG10NUMPERMISO", cwAscending)
  ' Campo de relaci�n entre el form padre e hijo
'    Call .FormAddRelation("AG11CODRECURSO", txtText1(12))
'    Call .FormAddRelation("AG07CODPERFIL", txtText1(11))
'    Call .FormAddRelation("AG04CODFRANJA", txtText1(0))
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
'    strKey = .strDataBase & .strTable

'  End With
  
' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
    
  ' Se a�aden los formularios a la ventana
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo1, cwFormDetail)
'    Call .FormAddInfo(objDetailInfo2, cwFormDetail)
  ' Se obtiene informaci�n de las caracter�sticas de
  ' los controles del formulario
    Call .FormCreateInfo(objMasterInfo)
        
    .CtrlGetInfo(txtText1(7)).blnReadOnly = True
  
  ' Campos que intervienen en busquedas
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(4)).blnInFind = True
    .CtrlGetInfo(chkCheck1(5)).blnInFind = True
    .CtrlGetInfo(chkCheck1(6)).blnInFind = True

    
    .CtrlGetInfo(txtText1(16)).blnInFind = True
  
    '.CtrlGetInfo(txtText1(22)).blnInFind = True
    '.CtrlGetInfo(txtText1(23)).blnInFind = True
    '.CtrlGetInfo(txtText1(25)).blnInFind = True
    '.CtrlGetInfo(txtText1(29)).blnInFind = True
  
  '  Rellenamos las DBCombo
     .CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT PR12CODACTIVIDAD, PR12DESACTIVIDAD, PR12INDPLANIFIC FROM " & objEnv.GetValue("Database") & "PR1200 ORDER BY PR12DESACTIVIDAD"
     .CtrlGetInfo(cboSSDBCombo1(1)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02INDRESPONPROC=-1 ORDER BY AD02DESDPTO"
     '.CtrlGetInfo(cboSSDBCombo1(2)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02INDRESPONPROC=-1 ORDER BY AD02DESDPTO"

     cboSSDBCombo1(6).AddItem ("'1','POR CANTIDAD'")
     cboSSDBCombo1(6).AddItem ("'2','SECUENCIAL'")
     cboSSDBCombo1(6).AddItem ("'3','INTERVALOS PREDETERMINADOS'")
     
  'Campos relcacionados entre si
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "PR01CODACTUACION", "SELECT PR01CODACTUACION, PR01DESCORTA FROM " & objEnv.GetValue("Database") & "PR0100 WHERE PR01CODACTUACION = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(17), "PR01DESCORTA")

      Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "PR12CODACTIVIDAD", "SELECT PR12INDPLANIFIC FROM " & objEnv.GetValue("Database") & "PR1200 WHERE PR12CODACTIVIDAD = ?", True)
      Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtText1(20), "PR12INDPLANIFIC")
   
   ' Eliminamos campos del grid
     .CtrlGetInfo(txtText1(10)).blnInGrid = False
     .CtrlGetInfo(txtText1(11)).blnInGrid = False
     .CtrlGetInfo(txtText1(12)).blnInGrid = False
     .CtrlGetInfo(txtText1(13)).blnInGrid = False
     .CtrlGetInfo(txtText1(14)).blnInGrid = False
     .CtrlGetInfo(txtText1(15)).blnInGrid = False
     '.CtrlGetInfo(txtText1(24)).blnInGrid = False
     '.CtrlGetInfo(txtText1(26)).blnInGrid = False
     '.CtrlGetInfo(txtText1(27)).blnInGrid = False
     '.CtrlGetInfo(txtText1(28)).blnInGrid = False
  
 ' Propiedades del campo clave para asignaci�n autom�tica
    .CtrlGetInfo(txtText1(0)).blnValidate = False
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
  
    '.CtrlGetInfo(txtText1(24)).blnValidate = False
    '.CtrlGetInfo(txtText1(24)).blnInGrid = False
  
 'Campos con lista de valores
    .CtrlGetInfo(txtText1(16)).blnForeign = True
    '.CtrlGetInfo(txtText1(21)).blnForeign = True

    '.CtrlGetInfo(txtText1(21)).blnNegotiated = False
  
  ' Validaciones de  hora inicio y hora fin
    'Hora entre 0 y 23
     With .CtrlGetInfo(txtText1(1))
       .vntMinValue = 0
       .vntMaxValue = 23
     End With
    'Hora entre 0 y 23
     With .CtrlGetInfo(txtText1(1))
       .vntMinValue = 0
       .vntMaxValue = 23
     End With
    'Minutos entre 0 y 59
     With .CtrlGetInfo(txtText1(2))
       .vntMinValue = 0
       .vntMaxValue = 59
     End With
    'Hora entre 0 y 23
     With .CtrlGetInfo(txtText1(3))
       .vntMinValue = 0
       .vntMaxValue = 23
     End With
     'Minutos entre 0 y 59
     With .CtrlGetInfo(txtText1(4))
       .vntMinValue = 0
       .vntMaxValue = 59
     End With
    'Hora entre 0 y 23
    ' With .CtrlGetInfo(txtText1(8))
    '   .vntMinValue = 0
    '   .vntMaxValue = 23
    ' End With
     'Minutos entre 0 y 59
    ' With .CtrlGetInfo(txtText1(9))
    '   .vntMinValue = 0
    '   .vntMaxValue = 59
    ' End With
    'Horas Antelaci�n entre 1 y 100
     With .CtrlGetInfo(txtText1(5))
       .vntMinValue = 1
       .vntMaxValue = 99
     End With
     
   'Valor por defecto de la fecha inicio vigencia
    .CtrlGetInfo(dtcDateCombo1(0)).vntDefaultValue = Format(objGen.GetDBDateTime, "DD/MM/YYYY")
  
  
    'cboSSDBCombo1(3).AddItem ("'0','SIN PERMISO'")
    'cboSSDBCombo1(3).AddItem ("'1','PERMISO ASIGNACI�N'")
    'cboSSDBCombo1(4).AddItem ("'0','SIN PERMISO'")
    'cboSSDBCombo1(4).AddItem ("'1','PERMISO ASIGNACI�N'")
    'cboSSDBCombo1(5).AddItem ("'0','SIN PERMISO'")
    'cboSSDBCombo1(5).AddItem ("'1','PERMISO ASIGNACI�N'")

     
  ' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
  ' Se estabiliza la ventana configurando las propiedades
  ' de los controles
    Call .WinStabilize
  
  End With
  
' Se oculta el formulario de splash
  Call objApp.SplashOff
' Me posiciono en el primer registro
  Call objWinInfo.DataMoveFirst
'Pongo en el Caption de la ventana el recurso y el perfil
  Me.Caption = Me.Caption & " del Recurso: " & frmPerfiles.txtText1(1) _
             & "     Perfil: " & frmPerfiles.txtText1(3)

  
  blnFlag = True
  If txtText1(20) = "-1" Or txtText1(20).Text = "" Then
    With objWinInfo
      '.CtrlGetInfo(txtText1(5)).blnReadOnly = False
      '.CtrlGetInfo(txtText1(5)).blnMandatory = True
      .CtrlGetInfo(txtText1(6)).blnReadOnly = False
      .CtrlGetInfo(txtText1(6)).blnMandatory = True
      '.CtrlGetInfo(txtText1(8)).blnReadOnly = False
      '.CtrlGetInfo(txtText1(9)).blnReadOnly = False
      fraFrame1(1).Enabled = True
      Call .WinPrepareScr
      Call .FormChangeColor(.objWinActiveForm)
    End With
    'updUpDown1(0).Enabled = True
    updUpDown1(1).Enabled = True
    
    fraFrame1(1).Enabled = True
    cmdCommand1(0).Enabled = True
  Else
    With objWinInfo
      
      'Call objWinInfo.CtrlSet(txtText1(5), "")
      '.CtrlGetInfo(txtText1(5)).blnReadOnly = True
      '.CtrlGetInfo(txtText1(5)).blnMandatory = False
      
      Call objWinInfo.CtrlSet(txtText1(6), "")
      .CtrlGetInfo(txtText1(6)).blnReadOnly = True
      .CtrlGetInfo(txtText1(6)).blnMandatory = False
      
      
      'Call objWinInfo.CtrlSet(txtText1(8), "")
      '.CtrlGetInfo(txtText1(8)).blnReadOnly = True
      
      'Call objWinInfo.CtrlSet(txtText1(9), "")
      '.CtrlGetInfo(txtText1(9)).blnReadOnly = True
      fraFrame1(1).Enabled = False
      
      Call .WinPrepareScr
      Call .FormChangeColor(.objWinActiveForm)
    End With
    'updUpDown1(0).Enabled = False
    updUpDown1(1).Enabled = False
    
  
    fraFrame1(1).Enabled = False
    cmdCommand1(0).Enabled = False
  End If
  
  'txtText1(21).Locked = True
  'txtText1(21).BackColor = objApp.objUserColor.lngReadOnly
  '*******Habra que cogerlo de la BD
  strSql = "SELECT AG25GRANOTIEMPO FROM AG2500"
  Set rdoParametro = objApp.rdoConnect.OpenResultset(strSql)
  If Not rdoParametro.EOF Then
   intGranoTiempo = rdoParametro(0)
   rdoParametro.Close
   Set rdoParametro = Nothing
  Else
    intGranoTiempo = 10
  End If
  '***********************
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub




Private Sub grdDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  'If intIndex = 0 Then
  '  If grdDBGrid1(0).Columns(5).Text <> "" Then
  '    grdDBGrid1(0).Columns(5).Text = GetTableColumn("SELECT  PR12DESACTIVIDAD FROM " & objEnv.GetValue("Database") & "PR1200  WHERE PR12CODACTIVIDAD =" & grdDBGrid1(0).Columns(5).Value)("PR12DESACTIVIDAD")
  '  End If
  'End If
  'If intIndex = 4 Then
    'Select Case grdDBGrid1(4).Columns("Horas Normales").Text
    '  Case "0"
    '    grdDBGrid1(4).Columns("Horas Normales").Text = "SIN PERMISO"
    '  Case "1"
    '    grdDBGrid1(4).Columns("Horas Normales").Text = "PERMISO ASIGNACI�N"
    'End Select
    'Select Case grdDBGrid1(4).Columns("Horas de Reserva").Text
    '  Case "0"
    '    grdDBGrid1(4).Columns("Horas de Reserva").Text = "SIN PERMISO"
    '  Case "1"
    '    grdDBGrid1(4).Columns("Horas de Reserva").Text = "PERMISO ASIGNACI�N"
    'End Select
    'Select Case grdDBGrid1(4).Columns("Horas de Sobrecarga").Text
    '  Case "0"
    '    grdDBGrid1(4).Columns("Horas de Sobrecarga").Text = "SIN PERMISO"
    '  Case "1"
    '    grdDBGrid1(4).Columns("Horas de Sobrecarga").Text = "PERMISO ASIGNACI�N"
    'End Select
  'End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Tipos_Actuaci�n" Then
    
    If strCtrl = "txtText1(16)" Then
      Set objSearch = New clsCWSearch
      With objSearch
       .strTable = "AG0501J"
       If cboSSDBCombo1(0).Text = "" Then
         .strWhere = "WHERE AG14CODTIPRECU=" & frmRecurso.cboSSDBCombo1(1).Value
       Else
         .strWhere = "WHERE AG14CODTIPRECU=" & frmRecurso.cboSSDBCombo1(1).Value & " AND PR12CODACTIVIDAD=" & cboSSDBCombo1(0).Value
       End If
       .strWhere = .strWhere & " and (PR01FECFIN IS NULL OR PR01FECFIN > " & objGen.ValueToSQL(objGen.GetDBDateTime, cwDate) & ")"
       .strOrder = ""
      
       Set objField = .AddField("PR01CODACTUACION")
       objField.strSmallDesc = "C�digo Actuaci�n"
       Set objField = .AddField("PR01DESCORTA")
       objField.strSmallDesc = "Descripci�n Actuaci�n                          "
       Set objField = .AddField("AD02CODDPTO")
       objField.strSmallDesc = "C�digo Depto."
       
       If .Search Then
         
         Call objWinInfo.CtrlSet(txtText1(16), .cllValues("PR01CODACTUACION"))
         Call objWinInfo.CtrlSearchLinked(objWinInfo.CtrlGetInfo(txtText1(16)))
       End If
      End With
     End If
     Set objSearch = Nothing
      
   End If
 ' If strFormName = "Permisos" Then
    
 '   If strCtrl = "txtText1(21)" Then
 '     Set objSearch = New clsCWSearch
 '     With objSearch
 '      .strTable = "SG0200"
 '      .strWhere = ""
 '      .strOrder = ""
      
 '      Set objField = .AddField("SG02COD")
 '      objField.strSmallDesc = "C�digo Usuario"
 '      Set objField = .AddField("SG01COD")
 '      objField.strSmallDesc = "C�digo Grupo"
 '      Set objField = .AddField("SG02NOM")
 '      objField.strSmallDesc = "Nombre"
 '      Set objField = .AddField("SG02APE1")
 '      objField.strSmallDesc = "Primer Apellido"
 '      Set objField = .AddField("SG02APE2")
 '      objField.strSmallDesc = "Segundo Apellido"
       
 '      If .Search Then
         
 '        Call objWinInfo.CtrlSet(txtText1(29), .cllValues("SG02COD"))
 '        Call objWinInfo.CtrlSet(txtText1(21), Trim(.cllValues("SG02NOM")) & " " & Trim(.cllValues("SG02APE1")) & " " & Trim(.cllValues("SG02APE2")))
        
 '        txtText1(21).SetFocus
 '      End If
 '     End With
 '    End If
 '    Set objSearch = Nothing
      
 '  End If

End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
' Si el form que se activa es franjas habilito el bot�n
' de restricciones y el Updown button, si no los deshabilito
  If strFormName = "Franjas" Then
    cmdCommand1(0).Enabled = True
    'updUpDown1(0).Enabled = True
    updUpDown1(1).Enabled = True
    
  Else
    cmdCommand1(0).Enabled = False
    'updUpDown1(0).Enabled = False
    updUpDown1(1).Enabled = False
    
  End If

End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
  'Si estamos en le form Franjas y estamos a�adiendo un nuevo
  'registro habilitamos el bot�n Upd
  If strFormName = "Franjas" Then
    If intNewStatus = cwModeSingleEmpty Then
      'updUpDown1(0).Enabled = False
      updUpDown1(1).Enabled = False
     
      cmdCommand1(0).Enabled = False
    Else
      cmdCommand1(0).Enabled = True
      'updUpDown1(0).Enabled = True
      updUpDown1(1).Enabled = True
     
    End If
    If intNewStatus = cwModeSingleAddRest Then
      cmdCommand1(0).Enabled = False
      With objWinInfo
        '.CtrlGetInfo(txtText1(5)).blnReadOnly = False
        '.CtrlGetInfo(txtText1(5)).blnMandatory = True
        .CtrlGetInfo(txtText1(6)).blnReadOnly = False
        .CtrlGetInfo(txtText1(6)).blnMandatory = True
        '.CtrlGetInfo(txtText1(8)).blnReadOnly = False
        '.CtrlGetInfo(txtText1(9)).blnReadOnly = False
        fraFrame1(1).Enabled = True
        Call .WinPrepareScr
        Call .FormChangeColor(.objWinActiveForm)
      End With

    End If
    
  End If
 
  If strFormName = "Tipos_Actuaci�n" Then
     If objWinInfo.intWinStatus = cwModeSingleAddKey Then
       objWinInfo.CtrlGotFocus
       txtText1(16).Locked = True
     End If
  End If
End Sub


Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
'******************************************************************
' Campos por defecto del form Tipos de Actuaci�n
'******************************************************************
  If strFormName = "Tipos_Actuaci�n" Then
   'C�digo de Recurso
    txtText1(13).Text = txtText1(12).Text
   'C�digo de Perfil
    txtText1(14).Text = txtText1(11).Text
   'C�digo de Franja
    txtText1(15).Text = txtText1(0).Text
 
  End If

'******************************************************************
' Campos por defecto del form Franjas
'******************************************************************
  If strFormName = "Franjas" Then
   'C�digo de Perfil
    txtText1(11).Text = lngCodPerfil
   'C�digo de Recurso
    txtText1(12).Text = lngCodRecurso
    cboSSDBCombo1(6).Value = frmRecurso.cboSSDBCombo1(3).Value
  End If
    
  'If strFormName = "Permisos" Then
   'C�digo de Recurso
  '  txtText1(28).Text = txtText1(12).Text
   'C�digo de Perfil
  '  txtText1(27).Text = txtText1(11).Text
   'C�digo de Franja
  '  txtText1(26).Text = txtText1(0).Text
 
  '  txtText1(21).Text = ""
  'End If

End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
  If blnRollback = True And blnError = False Then
    Call objApp.rdoConnect.RollbackTrans
    blnRollback = False
  Else
    Call objApp.rdoConnect.CommitTrans
  End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  If strFormName = "Franjas" Then
    txtText1(2).Text = Format(txtText1(2).Text, "00")
    txtText1(4).Text = Format(txtText1(4).Text, "00")
    If txtText1(20) = "-1" Or txtText1(20).Text = "" Then
      With objWinInfo
        '.CtrlGetInfo(txtText1(5)).blnReadOnly = False
        '.CtrlGetInfo(txtText1(5)).blnMandatory = True
        .CtrlGetInfo(txtText1(6)).blnReadOnly = False
        .CtrlGetInfo(txtText1(6)).blnMandatory = True
        '.CtrlGetInfo(txtText1(8)).blnReadOnly = False
        '.CtrlGetInfo(txtText1(9)).blnReadOnly = False
        
        Call .WinPrepareScr
        Call .FormChangeColor(.objWinActiveForm)
      End With
      'updUpDown1(0).Enabled = True
      updUpDown1(1).Enabled = True
      
      fraFrame1(1).Enabled = True
      If objWinInfo.intWinStatus = cwModeSingleEdit Then
        cmdCommand1(0).Enabled = True
      Else
        cmdCommand1(0).Enabled = False
      End If
    Else
      With objWinInfo
        
        'Call objWinInfo.CtrlSet(txtText1(5), "")
        '.CtrlGetInfo(txtText1(5)).blnReadOnly = True
        '.CtrlGetInfo(txtText1(5)).blnMandatory = False
        
        Call objWinInfo.CtrlSet(txtText1(6), "")
        .CtrlGetInfo(txtText1(6)).blnReadOnly = True
        .CtrlGetInfo(txtText1(6)).blnMandatory = False
        
        
       ' Call objWinInfo.CtrlSet(txtText1(8), "")
       ' .CtrlGetInfo(txtText1(8)).blnReadOnly = True
        
       ' Call objWinInfo.CtrlSet(txtText1(9), "")
       ' .CtrlGetInfo(txtText1(9)).blnReadOnly = True
        
        Call .WinPrepareScr
        Call .FormChangeColor(.objWinActiveForm)
      End With
      'updUpDown1(0).Enabled = False
      updUpDown1(1).Enabled = False
      
  
      fraFrame1(1).Enabled = False
      cmdCommand1(0).Enabled = False
    End If
  End If
  
 ' If strFormName = "Permisos" Then
 '   If txtText1(29) <> "" Then
 '     txtText1(21) = GetUserName(txtText1(29))
 '   Else
 '     txtText1(21) = ""
 '   End If
 ' End If
End Sub

' Validaciones del programa
Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)

  Dim intContador As Integer
  Dim rdoFranja As rdoResultset
  Dim strSql As String
  Dim strVar As String
  Dim vntCodFranja As Variant
  Dim blnDiaAplicable As Boolean
  Dim intPos As Integer
  Dim rdoRestricciones As rdoResultset
  Dim rdoActuaciones As rdoResultset
  Dim rdoConsulta As rdoQuery
  
'******************************************************************
'******************************************************************
' Validaciones del form Franjas
'******************************************************************
'******************************************************************
  
  If strFormName = "Franjas" Then
    
'******************************************************************
    'Comprobaci�n Hora Fin > Hora Inicio
'******************************************************************
    If Val(txtText1(1)) > Val(txtText1(3)) Then
     'Hora Inicio mayor que hora fin
      blnCancel = True
      Call objError.SetError(cwCodeMsg, "La hora de inicio es mayor que la hora fin")
      Call objError.Raise
      Exit Sub
    Else
      If Val(txtText1(1)) = Val(txtText1(3)) Then
        If Val(txtText1(2)) >= Val(txtText1(4)) Then
          'Hora Inicio mayor que hora fin
           blnCancel = True
           Call objError.SetError(cwCodeMsg, "La hora de inicio es mayor o igual que la hora fin")
           Call objError.Raise
           Exit Sub
        End If
      End If
    End If
        
    'Comprobamos que no hay solapamiento entre franjas
       
    If Val(txtText1(1)) >= 0 And Val(txtText1(1)) < 24 And _
       Val(txtText1(3)) >= 0 And Val(txtText1(3)) < 24 And _
       Val(txtText1(2)) >= 0 And Val(txtText1(2)) < 60 And _
       Val(txtText1(4)) >= 0 And Val(txtText1(4)) < 60 Then
      With objWinInfo.objWinActiveForm
      'Si estoy editando un registro ya creado guardo su c�digo
      'para luego no comparlo condigo mismo
       If objWinInfo.intWinStatus = cwModeSingleEdit Then
         vntCodFranja = .rdoCursor("AG04CODFRANJA")
       End If
      
      'Abro el cursor
'       strSql = "SELECT AG04CODFRANJA, AG04INDLUNFRJA, AG04INDMARFRJA," _
'              & " AG04INDMIEFRJA, AG04INDJUEFRJA, AG04INDVIEFRJA, " _
'              & " AG04INDSABFRJA, AG04INDDOMFRJA, AG04HORINFRJHH, " _
'              & " AG04HORINFRJMM, AG04HORFIFRJHH, AG04HORFIFRJMM FROM " _
'            & objEnv.GetValue("Database") & "AG0400 WHERE AG11CODRECURSO=" _
'            & Val(txtText1(12)) & " AND AG07CODPERFIL=" _
'            & Val(txtText1(11)) & " AND AG04FECBAJA='' ORDER BY 1"
            
       strSql = "SELECT AG04CODFRANJA, AG04INDLUNFRJA, AG04INDMARFRJA," _
              & " AG04INDMIEFRJA, AG04INDJUEFRJA, AG04INDVIEFRJA, " _
              & " AG04INDSABFRJA, AG04INDDOMFRJA, AG04HORINFRJHH, " _
              & " AG04HORINFRJMM, AG04HORFIFRJHH, AG04HORFIFRJMM FROM " _
            & objEnv.GetValue("Database") & "AG0400 WHERE AG11CODRECURSO=?" _
            & " AND AG07CODPERFIL=? AND AG04FECBAJA='' ORDER BY 1"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
            rdoConsulta(0) = Val(txtText1(12))
            rdoConsulta(1) = Val(txtText1(11))
            Set rdoFranja = rdoConsulta.OpenResultset()
       
'       Set rdoFranja = objApp.rdoConnect.OpenResultset(strSql, rdOpenForwardOnly, rdConcurReadOnly)
      'Miro si el primer registro del recordset coincide con el
      'registro que acabo de modificar. Si es as�, paso al siguiente
      'registro
    
       If objWinInfo.intWinStatus = cwModeSingleEdit And Not rdoFranja.EOF Then
         If Val(rdoFranja("AG04CODFRANJA")) = vntCodFranja Then
           rdoFranja.MoveNext
         End If
       End If
     
      'Recorro el cursor hasta el final de el cursor o hasta que
      'encuentre alg�n solapamiento de franjas
       While Not rdoFranja.EOF And Not blnCancel
     
       'Miro si hay dias aplicables coincidentes entre los registro a comparar
     
         For intContador = 0 To 6
           If rdoFranja(intContador + 1) = -1 And chkCheck1(intContador).Value = 1 Then
           'Comprobaci�n solapamiento de horas de franjas
             If Solapamiento(rdoFranja("AG04HORINFRJHH"), rdoFranja("AG04HORINFRJMM"), _
                      rdoFranja("AG04HORFIFRJHH"), rdoFranja("AG04HORFIFRJMM"), _
                      Val(txtText1(1)), Val(txtText1(2)), Val(txtText1(3)), Val(txtText1(4))) Then
       
             'Solapamiento
               blnCancel = True
               Call objError.SetError(cwCodeMsg, "Alguna hora se solapa con la franja cuyo c�digo es  " & rdoFranja("AG04CODFRANJA") & " el " & DiaSemana(intContador))
               Call objError.Raise
               Exit Sub
             End If
           End If
         Next intContador
                
        'No solapamiento
        rdoFranja.MoveNext
      
       'Miro si el registro del recordset coincide con el
       'registro que acabo de modificar. Si es as�, paso al siguiente
      'registro
        If Not rdoFranja.EOF And objWinInfo.intWinStatus = cwModeSingleEdit Then
          If Val(rdoFranja("AG04CODFRANJA")) = vntCodFranja Then
            rdoFranja.MoveNext
          End If
        End If
       
      Wend

     End With
   End If
   
   
   rdoFranja.Close
   rdoConsulta.Close

   
  'Comprobaci�n Hora Overbooking
   'If txtText1(8) <> "" And txtText1(9) <> "" Then
   '  If Val(txtText1(8)) >= 0 And Val(txtText1(8)) < 24 And _
   '    Val(txtText1(9)) >= 0 And Val(txtText1(9)) < 59 Then

   '    If Not Solapamiento(Val(txtText1(1)), Val(txtText1(2)), Val(txtText1(3)), Val(txtText1(4)), _
   '            Val(txtText1(8)), Val(txtText1(9)), Val(txtText1(8)), Val(txtText1(9))) Then
   
   '      blnCancel = True
   '      Call objError.SetError(cwCodeMsg, "La Hora de Overbooking no esta en entre la Hora Inicio y Hora Fin")
   '      Call objError.Raise
    '     Exit Sub
    '   End If
    ' End If
   'End If
  'Comprobaci�n de por lo menos se aplica a alg�n d�a
  
  blnDiaAplicable = False
  For intPos = 0 To 6
    If chkCheck1(intPos).Value = 1 Then
      blnDiaAplicable = True
      Exit For
    End If
  Next intPos
  If Not blnDiaAplicable Then
     blnCancel = True
     Call objError.SetError(cwCodeMsg, "No hay ning�n d�a aplicable seleccionado.")
     Call objError.Raise
     Exit Sub
  End If
  
 'Comprobaci�n de que el tipo de actividad elegido puede tener
 'asociados tipos de actuaci�n
  If txtText1(20) = "0" Then
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
      
      If objGen.GetRowCount(objWinInfo.cllWinForms("fraFrame1(1)").rdoCursor) > 0 Then
    
        blnCancel = True
        Call objError.SetError(cwCodeMsg, "El Tipo de Actividad elegido no puede tener asocidado ning�n tipo de Actuaci�n.")
        Call objError.Raise
        Exit Sub
      End If
      
    End If
  End If

 'Comprobaci�n de que el tipo de actividad elegido puede tener
 'asociados restricciones
  If objWinInfo.intWinStatus = cwModeSingleEdit Then
    If objWinInfo.objWinActiveForm.rdoCursor("PR12CODACTIVIDAD") <> cboSSDBCombo1(0).Value And txtText1(20).Text = "0" Then
'      strSql = "SELECT AG1200.AG11CODRECURSO FROM " & objEnv.GetValue("Database") _
'         & "AG1200 WHERE AG1200.AG11CODRECURSO=" & Val(frmFranjas.txtText1(12).Text) _
'         & " AND AG1200.AG07CODPERFIL=" & Val(frmFranjas.txtText1(11).Text) _
'         & " AND AG1200.AG04CODFRANJA=" & Val(frmFranjas.txtText1(0).Text) _
'         & " AND AG1200.AG12NIVELSUPER=0"
      strSql = "SELECT AG1200.AG11CODRECURSO FROM " & objEnv.GetValue("Database") _
         & "AG1200 WHERE AG1200.AG11CODRECURSO=?" _
         & " AND AG1200.AG07CODPERFIL=?" _
         & " AND AG1200.AG04CODFRANJA=?" _
         & " AND AG1200.AG12NIVELSUPER=0"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
        Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
        rdoConsulta(0) = Val(frmFranjas.txtText1(12).Text)
        rdoConsulta(1) = Val(frmFranjas.txtText1(11).Text)
        rdoConsulta(2) = Val(frmFranjas.txtText1(0).Text)
        Set rdoRestricciones = rdoConsulta.OpenResultset()

  
  
'      Set rdoRestricciones = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset)
      If objGen.GetRowCount(rdoRestricciones) > 0 Then
    
        blnCancel = True
        Call objError.SetError(cwCodeMsg, "El Tipo de Actividad elegido no puede tener asocidado ninguna restricci�n. Borre primero las restricciones.")
        Call objError.Raise
        Exit Sub
      End If
   rdoRestricciones.Close
   rdoConsulta.Close
    End If
  End If
  'Comprobaci�n de que el n�mero de minutos de la franja sea
  'mayor o igual al grano de tiempo establecido en los par�metros
  If IsDate(Format(txtText1(1) & ":" & txtText1(2), "HH:NN")) And IsDate(Format(txtText1(3) & ":" & txtText1(4), "HH:NN")) Then
   If DateDiff("n", CDate(Format(txtText1(1) & ":" & txtText1(2), "HH:NN")), CDate(Format(txtText1(3) & ":" & txtText1(4), "HH:NN"))) < intGranoTiempo Then
    Call objError.SetError(cwCodeMsg, "El n�mero de minutos de la Franja debe ser mayor o igual al Intervalo de Tiempo establecido en los Par�metros")
    Call objError.Raise
    Exit Sub
   End If
  End If
  'Comprobaci�n de que el n�mero de minutos del tiempo duraci�n actuaciones
  'mayor o igual al grano de tiempo establecido en los par�metros
  If txtText1(7) <> "" Then
   If Val(txtText1(7)) < intGranoTiempo Then
    Call objError.SetError(cwCodeMsg, "El n�mero de minutos del Intervalo de Cita debe ser mayor o igual al Intervalo de Tiempo establecido en los Par�metros")
    Call objError.Raise
    Exit Sub
   End If
  End If
  'Miramos si hay disminuci�n de disponibilidad
   If objWinInfo.intWinStatus = cwModeSingleEdit Then
     Call Disminucion_Franjas
   End If
 End If
  
'******************************************************************
'******************************************************************
' Validaciones del form Tipos de Actuaci�n
'******************************************************************
'******************************************************************

  If strFormName = "Tipos_Actuaci�n" Then
   
   ' Validaci�n de Fecha Fin mayor que Fecha Inicio
    If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
         Call objError.SetError(cwCodeMsg, "La Fecha Fin Vigencia es  menor que Fecha Inicial Vigencia")
         Call objError.Raise
         blnCancel = True
         Exit Sub
      End If
         
    End If
  
   'Validar que el N�mero Asignaciones Admitidas sea menor � igual que el
   'N�mero Citas Admitidas de la franja correspondiente
   With objWinInfo
     If Val(.CtrlGet(txtText1(18))) > Val(.CtrlGet(txtText1(6))) Then
       Call objError.SetError(cwCodeMsg, "El numero de Asignaciones Admitidas no puede ser mayor que el de Citas Admitidas")
       Call objError.Raise
       blnCancel = True
       Exit Sub
     End If
   
    'If DateDiff("n", CDate(Val(txtText1(1)) & ":" & Val(txtText1(2))), CDate(Val(txtText1(3)) & ":" & Val(txtText1(4)))) < Val(txtText1(19)) Then
    '   Call objError.SetError(cwCodeMsg, "El Tiempo de Asignaci�n excede el Tiempo de Franja.")
    '   Call objError.Raise
    '   blnCancel = True
    '   Exit Sub
    ' End If
   End With
 
 
 
 
 
   'Validar que la Actuaci�n introducida pertenezca al Tipo de
   'Actividad de la franja.
   If cboSSDBCombo1(0).Text <> "" Then
'     strSql = "SELECT PR01CODACTUACION FROM PR0100 WHERE PR01CODACTUACION=" & Val(txtText1(16)) _
'       & " AND PR12CODACTIVIDAD=" & objWinInfo.cllWinForms("fraFrame1(0)").rdoCursor("PR12CODACTIVIDAD")
     strSql = "SELECT PR01CODACTUACION FROM PR0100 WHERE PR01CODACTUACION=?" _
       & " AND PR12CODACTIVIDAD=?"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
        Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
        rdoConsulta(0) = Val(txtText1(16))
        rdoConsulta(1) = objWinInfo.cllWinForms("fraFrame1(0)").rdoCursor("PR12CODACTIVIDAD")
        Set rdoFranja = rdoConsulta.OpenResultset()
     
'     Set rdoFranja = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset)
     If rdoFranja.EOF Then
       Call objError.SetError(cwCodeMsg, "La Actividad elegida no corresponde con el Tipo de Actividad de la Franja")
       Call objError.Raise
       blnCancel = True
     End If
        rdoFranja.Close
        rdoConsulta.Close
   End If
   
   'Validar que la actuaci�n introducida pueda realizarse por
   'el tipo de recurso actual
   Dim strWhere As String
   
   strSql = "Select * from AG0501J "
  
   If cboSSDBCombo1(0).Text = "" Then
'     strWhere = " WHERE AG14CODTIPRECU=" & frmRecurso.cboSSDBCombo1(1).Value
     strWhere = " WHERE AG14CODTIPRECU=?"
   Else
'     strWhere = " WHERE AG14CODTIPRECU=" & frmRecurso.cboSSDBCombo1(1).Value & " AND PR12CODACTIVIDAD=" & cboSSDBCombo1(0).Value
     strWhere = " WHERE AG14CODTIPRECU=? AND PR12CODACTIVIDAD=?"
   End If
   
'   strWhere = strWhere & " and PR01CODACTUACION=" & Val(txtText1(16))
   strWhere = strWhere & " and PR01CODACTUACION=?"
   strSql = strSql & strWhere
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
        Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
   If cboSSDBCombo1(0).Text = "" Then
        rdoConsulta(0) = frmRecurso.cboSSDBCombo1(1).Value
        rdoConsulta(1) = Val(txtText1(16))
    Else
        rdoConsulta(0) = frmRecurso.cboSSDBCombo1(1).Value
        rdoConsulta(1) = cboSSDBCombo1(0).Value
        rdoConsulta(2) = Val(txtText1(16))
   End If
        Set rdoFranja = rdoConsulta.OpenResultset()
  
'   Set rdoFranja = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset)
   If rdoFranja.EOF Then
     Call objError.SetError(cwCodeMsg, "La Actuaci�n introducida no se puede realizar por el tipo de recurso actual.")
     Call objError.Raise
     blnCancel = True
   Else
     'Validar que la actuaci�n introducida no est� dada de baja
'     strSql = "Select PR01FECFIN FROM PR0100 WHERE PR01CODACTUACION=" & rdoFranja("PR01CODACTUACION")
     strSql = "Select PR01FECFIN FROM PR0100 WHERE PR01CODACTUACION=?"
     strVar = rdoFranja("PR01CODACTUACION")
     rdoFranja.Close
     rdoConsulta.Close
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
        Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
        rdoConsulta(0) = strVar
        Set rdoFranja = rdoConsulta.OpenResultset()
  
'     Set rdoFranja = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset)
     If Not IsNull(rdoFranja("PR01FECFIN")) Then
       If CDate(rdoFranja("PR01FECFIN")) < CDate(objGen.GetDBDateTime) Then
         Call objError.SetError(cwCodeMsg, "La Actuaci�n introducida est� dada de baja.")
         Call objError.Raise
         blnCancel = True
       End If
     End If
   End If
'   Set rdoFranja = Nothing
   rdoFranja.Close
   rdoConsulta.Close

   'Validar que si la actuaci�n introducida tiene tiempo de
   'antelaci�n definido, exista al menos otra actuaci�n definida
   'en la misma franja y vigente sin el tpo de antelaci�n definido.
   If txtText1(5).Text <> "" Then
      strSql = "SELECT * FROM AG0100 WHERE AG11CODRECURSO=?" _
         & " AND AG07CODPERFIL=?" _
         & " AND AG04CODFRANJA=?" _
         & " AND AG01NUMHORANT IS NULL" _
         & " AND ( AG01FECFIVGACF IS NULL OR TO_DATE(TO_CHAR(AG01FECFIVGACF,'DD-MM-YYYY'),'DD-MM-YYYY') > ? )"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
        Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
        rdoConsulta(0) = Val(txtText1(13).Text)
        rdoConsulta(1) = Val(txtText1(14).Text)
        rdoConsulta(2) = Val(txtText1(15).Text)
        rdoConsulta(3) = Format(objGen.ValueToSQL(objGen.GetDBDateTime, cwDate), "DD-MM-YYYY")
        Set rdoActuaciones = rdoConsulta.OpenResultset()
      
'      Set rdoActuaciones = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset)
      If rdoActuaciones.RowCount = 0 Then
         Call objError.SetError(cwCodeMsg, "Para definir una actuaci�n con Horas de Antelaci�n debe existir al menos una actuaci�n vigente sin Horas de Antelaci�n.")
         Call objError.Raise
         blnCancel = True
      End If
   End If
   
 End If
  
End Sub


Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'******************************************************************
' Compruebo si el usuarario ha cancelado o no la incidencia para
' validar los cambios o anularlos
'******************************************************************
  If blnRollback = True And blnError = False Then
    Call objApp.rdoConnect.RollbackTrans
    blnRollback = False
  Else
    Call objApp.rdoConnect.CommitTrans
  End If
End Sub


Private Sub objWinInfo_cwPreChangeForm(ByVal strFormName As String)
  If strFormName = "Tipos_Actuaci�n" Then
    If objWinInfo.intWinStatus = cwModeSingleEmpty And txtText1(16) <> "" Then
      txtText1(16) = ""
      txtText1(17) = ""
      cboSSDBCombo1(1).Text = ""
    End If
  End If
End Sub

Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
'******************************************************************
'******************************************************************
'  Comprobaciones en el form Franjas antes del borrado
'******************************************************************
'******************************************************************

  If strFormName = "Franjas" Then
'******************************************************************
  'Comprobaci�n de que no tenga ning�n Tipo de Actuaci�n
  'asociado
'******************************************************************
    If objGen.GetRowCount(objWinInfo.cllWinForms("fraFrame1(1)").rdoCursor) > 0 Then
      Call objError.SetError(cwCodeMsg, "No se puede borrar una Frnaja que tiene asociado algun Tipo de Actuaci�n.")
      Call objError.Raise
      blnCancel = True
      Exit Sub
    End If
    
    If txtText1(20) = "-1" Then
      intCodMotInci = 110
    ' Iniciamos la transacci�n
      Call objApp.rdoConnect.BeginTrans
      objSecurity.RegSession
    ' Carga de la ventana Incidencias
      Call AddInciden(intCodMotInci, False)
      blnCancel = blnRollback
    End If
  End If
   
'******************************************************************
'******************************************************************
'  Comprobaciones en el form Actuaciones antes del borrado
'******************************************************************
'******************************************************************
   If strFormName = "Tipos_Actuaci�n" Then
      intCodMotInci = 100
    ' Iniciamos la transacci�n
      Call objApp.rdoConnect.BeginTrans
      objSecurity.RegSession
    ' Carga de la ventana Incidencias
      Call AddInciden(intCodMotInci, False)

    End If
End Sub


Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
' Variable para las sentencia Sql que abrira el cursor
' para dar de alta la incidencia
  Dim strSql As String
  Dim rdoConsulta As rdoQuery
' Variable que guardara el campo n� de incidencia del form activo
  Dim strCampoNumIncidencia As String
  
'******************************************************************
'******************************************************************
' Actuaciones en el form Franjas antes de la actualizaci�n
'******************************************************************
'******************************************************************

  If strFormName = "Franjas" Then

'******************************************************************
    'Si estamosa�adiendo un nuevo registro obtenemos el nuevo
    'c�digo
'******************************************************************
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
'LAS 13.4.99
'      strSql = "SELECT MAX(AG04CODFRANJA) FROM " & objEnv.GetValue("Database") & "AG0400 " _
'             & " WHERE AG11CODRECURSO=" & Val(txtText1(12)) _
'             & " AND AG07CODPERFIL=" & Val(txtText1(11))
      strSql = "SELECT MAX(AG04CODFRANJA) FROM " & objEnv.GetValue("Database") & "AG0400 " _
             & " WHERE AG11CODRECURSO= ? AND AG07CODPERFIL= ? "
             '& " ORDER BY AG04CODFRANJA DESC"
     ' Empieza transacci�n
      Call objApp.rdoConnect.BeginTrans
      objSecurity.RegSession
     'Obtengo el nuevo c�digo
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
    
    rdoConsulta(0) = Val(txtText1(12))
    rdoConsulta(1) = Val(txtText1(11))
    
    vntNuevoCod = GetNewCode(rdoConsulta)
    rdoConsulta.Close
'      vntNuevoCod = GetNewCode(strSql)
     'Paso el nuevo c�digo al campo del cursor y a la textbox
      Call objWinInfo.CtrlStabilize(txtText1(0), vntNuevoCod)
    End If
   
   
   
'******************************************************************
   'Si estamos en modo edici�n puede que haya alguna incidencia al
   'grabar los cambios
'******************************************************************
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
      strCampoNumIncidencia = "AG05NUMINCIDEN"
  
    ' Miramos si hay disminuci�n
      If intCodMotInci <> 0 Then
      
      ' Iniciamos la transacci�n
        Call objApp.rdoConnect.BeginTrans
      
      ' Carga de la ventana Incidencias
        Call AddInciden(intCodMotInci, False)
      ' Asignamos el n�mero de incidencia al campo del form activo
        If blnActive And Not blnRollback Then
          objWinInfo.objWinActiveForm.rdoCursor(strCampoNumIncidencia) = vntNuevoCod
        End If
     End If
   End If
End If
  
  'If strFormName = "Permisos" Then

'******************************************************************
    'Si estamos a�adiendo un nuevo registro obtenemos el nuevo
    'c�digo
'******************************************************************
  '  If objWinInfo.intWinStatus = cwModeSingleAddRest Then
  '    strSql = "SELECT MAX(AG10NUMPERMISO) FROM " & objEnv.GetValue("Database") & "AG1000 " _
  '           & " WHERE AG11CODRECURSO=" & Val(txtText1(12)) _
  '           & " AND AG07CODPERFIL=" & Val(txtText1(11)) _
  '           & " AND AG04CODFRANJA=" & Val(txtText1(0))
     ' Empieza transacci�n
  '    Call objApp.rdoConnect.BeginTrans
     'Obtengo el nuevo c�digo
 '     vntNuevoCod = GetNewCode(strSql)
     'Paso el nuevo c�digo al campo del cursor y a la textbox
 '     Call objWinInfo.CtrlStabilize(txtText1(24), vntNuevoCod)
 '   End If
 ' End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "Franjas" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(True)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_Change(intIndex As Integer)
   
   Call objWinInfo.CtrlDataChange
   

End Sub

Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
  If intIndex = 6 Then
    On Error GoTo Error
    If Val(cboSSDBCombo1(6).Value) = 3 Then
      If Val(txtText1(6)) > 0 Then
       txtText1(7) = CInt(DateDiff("n", CDate(Format(txtText1(1) & ":" & txtText1(2), "HH:NN")), CDate(Format(txtText1(3) & ":" & txtText1(4), "HH:NN"))) / Val(txtText1(6)))
     End If
    Else
      txtText1(7) = ""
   End If
  End If
Error:
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 0 Then

    If cboSSDBCombo1(0).Columns(2).Value = 0 Then
      fraFrame1(1).Enabled = False
    Else
      fraFrame1(1).Enabled = True
    End If
  End If
  If intIndex = 6 Then
    On Error GoTo Error
    If Val(cboSSDBCombo1(6).Value) = 3 Then
      If Val(txtText1(6)) > 0 Then
       txtText1(7) = CInt(DateDiff("n", CDate(Format(txtText1(1) & ":" & txtText1(2), "HH:NN")), CDate(Format(txtText1(3) & ":" & txtText1(4), "HH:NN"))) / Val(txtText1(6)))
     End If
    Else
      txtText1(7) = ""
 
   End If
  End If
Error:
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
'******************************************************************
  'Si la textbox actual es la de c�digo de actuaci�n obtengo la descripci�n y
  'relleno la combo de los departamentos con los departamentos que puedean realizar
  'esa actuaci�n
'******************************************************************
  If intIndex = 16 Then
    'Si el contenido del campo c�d. actuaci�n es diferente de Null
    If txtText1(intIndex) <> "" Then
      'Obtengo la descripci�n de la actuaci�n
      Call objWinInfo.CtrlSearchLinked(objWinInfo.CtrlGetInfo(txtText1(intIndex)))
      'Relleno la combo
     ' objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02CODDPTO IN (SELECT AD02CODDPTO FROM " & objEnv.GetValue("Database") & "PR0200 WHERE PR01CODACTUACION=" & txtText1(16) & ")  ORDER BY AD02DESDPTO"
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02CODDPTO IN (SELECT AD02CODDPTO FROM " & objEnv.GetValue("Database") & "AG0501J WHERE AG14CODTIPRECU=" & frmRecurso.cboSSDBCombo1(1).Value & " AND PR01CODACTUACION=" & txtText1(16) & ")  ORDER BY AD02DESDPTO"
      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)))
      If cboSSDBCombo1(1).Rows = 0 Then
        objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02CODDPTO IN (SELECT AD02CODDPTO FROM " & objEnv.GetValue("Database") & "PR0200 WHERE  PR01CODACTUACION=" & txtText1(16) & ")  ORDER BY AD02DESDPTO"
        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)))
      End If
    End If
  End If

End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 6 Or intIndex = 1 _
    Or intIndex = 2 Or intIndex = 3 Or intIndex = 4 _
                                        Then
   On Error GoTo Error
   If Val(cboSSDBCombo1(6).Value) = 3 Then
     If Val(txtText1(6)) > 0 Then
       txtText1(7) = CInt(DateDiff("n", CDate(Format(txtText1(1) & ":" & txtText1(2), "HH:NN")), CDate(Format(txtText1(3) & ":" & txtText1(4), "HH:NN"))) / Val(txtText1(6)))
     End If
   End If
  End If
Error:

End Sub

