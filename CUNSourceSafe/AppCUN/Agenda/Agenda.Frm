VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmAgenda 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Agenda"
   ClientHeight    =   1020
   ClientLeft      =   2175
   ClientTop       =   2895
   ClientWidth     =   3060
   ClipControls    =   0   'False
   Icon            =   "Agenda.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1020
   ScaleWidth      =   3060
   Begin Crystal.CrystalReport crCrystalReport1 
      Left            =   1680
      Top             =   120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog1 
      Left            =   855
      Top             =   225
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
      HelpFile        =   "csc.hlp"
   End
   Begin ComctlLib.ImageList imlImageList1 
      Left            =   135
      Top             =   135
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Tipos de Recursos"
         Index           =   10
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Tipos de restricción"
         Index           =   20
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Calendarios"
         Index           =   40
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Recursos"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Consulta Agenda Recursos"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Códigos de Incidencia"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Dietario Recursos"
         Index           =   80
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Mant. Parámetros"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   199
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   200
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmAgenda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Rem bind ok
Option Explicit


Private Sub Form_Load()
  Dim strEntorno1 As String
  Dim strEntorno2 As String
  Call InitApp
 
  With objApp
  
    Call .Register(App, Screen)
    
    Set .frmFormMain = Me
    Set .imlImageList = imlImageList1
    Set .crCrystal = crCrystalReport1
    Set .dlgCommon = dlgCommonDialog1
    
  End With
  
  strEntorno1 = "Entorno de Desarrollo"
  strEntorno2 = "Entorno de Explotación"
  
  With objEnv
    Call .AddEnv(strEntorno1)
    Call .AddValue(strEntorno1, "DataBase", "")
  
    Call .AddEnv(strEntorno2)
    Call .AddValue(strEntorno2, "DataBase", "")
  
  End With
  
  objApp.blnUseRegistry = True
  
  objApp.strPassword = "BGPGOZ"
  objApp.blnAskPassword = False
  
  If Not objApp.CreateInfo Then
    Call ExitApp
  End If
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, intUnloadMode As Integer)
  Call ExitApp
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call objApp.HelpContext
    Case 20
      Call objApp.About
  End Select
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
     Case 10
      Load frmTipoRecu
      Call frmTipoRecu.Show(vbModal)
      Unload frmTipoRecu
      Set frmTipoRecu = Nothing
     Case 20
      Load frmTipRest
      Call frmTipRest.Show(vbModal)
      Unload frmTipRest
      Set frmTipRest = Nothing
     Case 40
      Load frmCalendarios
      Call frmCalendarios.Show(vbModal)
      Unload frmCalendarios
      Set frmCalendarios = Nothing
     Case 50
      Load frmRecurso
      Call frmRecurso.Show(vbModal)
      Unload frmRecurso
      Set frmRecurso = Nothing
     Case 60
      Load frmAgendaRecurso
      Call frmAgendaRecurso.Show(vbModal)
      Unload frmAgendaRecurso
      Set frmAgendaRecurso = Nothing
     Case 70
       Call CodigosIncidencia
     Case 73
   '     Call DiasFestivos
     Case 80
      Load frmDietario
      Call frmDietario.Show(vbModal)
      Unload frmDietario
      Set frmDietario = Nothing
     Case 90
       Call Parametros
  
     Case 200
       Unload Me
  End Select
End Sub

