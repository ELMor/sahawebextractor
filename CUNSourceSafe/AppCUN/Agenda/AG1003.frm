VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmCalendarios 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "AGENDA. Calendarios"
   ClientHeight    =   7860
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   11625
   ControlBox      =   0   'False
   HelpContextID   =   3
   Icon            =   "AG1003.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7860
   ScaleWidth      =   11625
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   25
      Top             =   0
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "D�as Especiales"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Index           =   2
      Left            =   6480
      TabIndex        =   32
      Top             =   3000
      Width           =   4980
      Begin TabDlg.SSTab tabTab1 
         Height          =   4020
         Index           =   2
         Left            =   120
         TabIndex        =   33
         TabStop         =   0   'False
         Top             =   375
         Width           =   4695
         _ExtentX        =   8281
         _ExtentY        =   7091
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AG1003.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(9)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "dtcDateCombo1(4)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "dtcDateCombo1(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "chkCheck1(9)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(4)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(11)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(22)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(5)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AG1003.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(4)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   5
            Left            =   2370
            Locked          =   -1  'True
            TabIndex        =   47
            TabStop         =   0   'False
            Tag             =   "D�a Semana"
            Top             =   975
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG05NUMINCIDEN"
            Height          =   330
            Index           =   22
            Left            =   3000
            TabIndex        =   45
            TabStop         =   0   'False
            Tag             =   "C�digo Departamento"
            Text            =   "N�Incidencia"
            Top             =   360
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG02CODCALENDA"
            Height          =   330
            Index           =   11
            Left            =   1440
            TabIndex        =   43
            TabStop         =   0   'False
            Tag             =   "C�digo Departamento"
            Text            =   "C�d. Cal."
            Top             =   360
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG03DESDIAESPE"
            Height          =   330
            Index           =   4
            Left            =   240
            TabIndex        =   16
            Tag             =   "Descripci�n D�a Especial|Descripci�n"
            Top             =   1680
            Width           =   3930
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Festivo"
            DataField       =   "AG03INDFESTIVO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   9
            Left            =   3150
            TabIndex        =   18
            Tag             =   "Indicador Festivo|Indicador"
            Top             =   2670
            Width           =   945
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG03FECDIAESPE"
            Height          =   330
            Index           =   2
            Left            =   240
            TabIndex        =   15
            Tag             =   "Fecha D�a Especial|Fecha"
            Top             =   975
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   2
            Left            =   -74910
            TabIndex        =   34
            TabStop         =   0   'False
            Top             =   90
            Width           =   8655
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15266
            _ExtentY        =   5054
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3315
            Index           =   4
            Left            =   -74900
            TabIndex        =   38
            Top             =   480
            Width           =   4500
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   7937
            _ExtentY        =   5847
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG03FECBAJA"
            Height          =   330
            Index           =   4
            Left            =   240
            TabIndex        =   17
            Tag             =   "Fecha Baja D�a Especial|Fecha Baja"
            Top             =   2640
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Baja"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   240
            TabIndex        =   46
            Top             =   2400
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   240
            TabIndex        =   36
            Top             =   720
            Width           =   540
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   240
            TabIndex        =   35
            Top             =   1440
            Width           =   1020
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Per�odos de Vigencia"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Index           =   1
      Left            =   120
      TabIndex        =   26
      Top             =   3000
      Width           =   6420
      Begin TabDlg.SSTab tabTab1 
         Height          =   4020
         Index           =   1
         Left            =   120
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   360
         Width           =   6135
         _ExtentX        =   10821
         _ExtentY        =   7091
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AG1003.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(7)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "dtcDateCombo1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "fraFrame1(3)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(10)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(21)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).ControlCount=   11
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(3)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG05NUMINCIDEN"
            Height          =   330
            HelpContextID   =   40101
            Index           =   21
            Left            =   4095
            TabIndex        =   44
            TabStop         =   0   'False
            Tag             =   "C�digo Departamento"
            Text            =   "N�Incidencia"
            Top             =   495
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG02CODCALENDA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   10
            Left            =   2865
            TabIndex        =   42
            TabStop         =   0   'False
            Tag             =   "C�digo Departamento"
            Text            =   "C�d. Cal."
            Top             =   480
            Visible         =   0   'False
            Width           =   1150
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "D�as Festivos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1695
            Index           =   3
            Left            =   120
            TabIndex        =   40
            Top             =   2160
            Width           =   5700
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Domingo"
               DataField       =   "AG08INDDOMFEST"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   8
               Left            =   3840
               TabIndex        =   14
               Tag             =   "Domingo Festivo|Indicador Festivo"
               Top             =   840
               Width           =   1185
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "S�bado"
               DataField       =   "AG08INDSABFEST"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   7
               Left            =   3840
               TabIndex        =   13
               Tag             =   "S�bado Festivo|Indicador Festivo"
               Top             =   480
               Width           =   1065
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Viernes"
               DataField       =   "AG08INDVIEFEST"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   6
               Left            =   2280
               TabIndex        =   12
               Tag             =   "Viernes Festivo|Indicador Festivo"
               Top             =   840
               Width           =   1065
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Jueves"
               DataField       =   "AG08INDJUEFEST"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   5
               Left            =   2280
               TabIndex        =   11
               Tag             =   "Jueves Festivo|Indicador Festivo"
               Top             =   480
               Width           =   945
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Mi�rcoles"
               DataField       =   "AG08INDMIEFEST"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   4
               Left            =   600
               TabIndex        =   10
               Tag             =   "Mi�rcoles Festivo|Indicador Festivo"
               Top             =   1200
               Width           =   1305
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Martes"
               DataField       =   "AG08INDMARFEST"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   3
               Left            =   600
               TabIndex        =   9
               Tag             =   "Martes Festivo|Indicador Festivo"
               Top             =   840
               Width           =   945
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Lunes"
               DataField       =   "AG08INDLUNFEST"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   2
               Left            =   600
               TabIndex        =   8
               Tag             =   "Lunes Festivo|Indicador Festivo"
               Top             =   480
               Width           =   945
            End
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG08DESPERVIGE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   1800
            TabIndex        =   5
            Tag             =   "Descripci�n Per�odo|Descripci�n"
            Top             =   930
            Width           =   4050
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H0000FFFF&
            DataField       =   "AG08CODPERVIGE"
            Height          =   330
            HelpContextID   =   30101
            Index           =   2
            Left            =   120
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "C�digo Per�odo Vigencia|C�digo"
            Top             =   930
            Width           =   1470
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG08FECINIPERI"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   6
            Tag             =   "Fecha Desde"
            Top             =   1680
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   1
            Left            =   -74910
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   90
            Width           =   8655
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15266
            _ExtentY        =   5054
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3315
            Index           =   3
            Left            =   -74900
            TabIndex        =   37
            Top             =   480
            Width           =   5940
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   10477
            _ExtentY        =   5847
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG08FECFINPERI"
            Height          =   330
            Index           =   1
            Left            =   3960
            TabIndex        =   7
            Tag             =   "Fecha Hasta"
            Top             =   1680
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Hasta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   3960
            TabIndex        =   39
            Top             =   1440
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   1800
            TabIndex        =   31
            Top             =   690
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Desde"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   120
            TabIndex        =   30
            Top             =   1440
            Width           =   1140
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   29
            Top             =   690
            Width           =   600
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Calendarios"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2325
      Index           =   0
      Left            =   120
      TabIndex        =   21
      Top             =   480
      Width           =   11340
      Begin TabDlg.SSTab tabTab1 
         Height          =   1815
         HelpContextID   =   90001
         Index           =   0
         Left            =   135
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   375
         Width           =   11010
         _ExtentX        =   19420
         _ExtentY        =   3201
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AG1003.frx":0060
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(4)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "dtcDateCombo1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkCheck1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).ControlCount=   7
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AG1003.frx":007C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Calendario principal"
            DataField       =   "AG02INDCALPRIN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   6480
            TabIndex        =   2
            Tag             =   "Indicador Calendario Principal|Indicador"
            Top             =   840
            Width           =   2085
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG02DESCALENDA"
            Height          =   330
            Index           =   1
            Left            =   1890
            TabIndex        =   1
            Tag             =   "Descripci�n Calendario|Descripci�n"
            Top             =   855
            Width           =   4230
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H0000FFFF&
            DataField       =   "AG02CODCALENDA"
            Height          =   330
            Index           =   0
            Left            =   480
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "C�digo Calendario|C�digo"
            Top             =   855
            Width           =   1150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1395
            Index           =   0
            Left            =   -74880
            TabIndex        =   20
            Top             =   360
            Width           =   10740
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18944
            _ExtentY        =   2461
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AG02FECBAJA"
            Height          =   330
            Index           =   3
            Left            =   8775
            TabIndex        =   3
            Tag             =   "Fecha Baja Calendario|Fecha Baja"
            Top             =   855
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Baja"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   8760
            TabIndex        =   41
            Top             =   600
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   1890
            TabIndex        =   24
            Top             =   600
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   465
            TabIndex        =   23
            Top             =   600
            Width           =   600
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   22
      Top             =   7575
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmCalendarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Rem bind repasar
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

' Procedimiento que devuelve el c�digo de motivo de incidencia
' en la variable global intCodMotInci si hay disminuci�n de
' disponibilidad, si no hay devuelve 0 en intCodMotInci

'Posibles valores en la tabla de motivos de incidencia (AG0600)

'AG06CODMOTINCI AG06DESMOTINCI                           AG06INDINCDISM
'-------------- ---------------------------------------- --------------
'            10 Cambio en periodo de vigencia                         1
'            20 Cambio a festivo de un d�a especial                   1                                   2
'            30 Nuevo per�odo de vigencia                             1
'            40 Nuevo d�a especial                                    1
'            50 Borrado de d�a especial no festivo                       1
'            60 Borrado de per�odo de vigencia                        1
'            70 Disminuci�n de Recursos                               1
'            80 Borrado de Recurso                                    1
'            90 Cambio en Perfil Gen�rico                               1
'           100 Cambio en Fecha Baja                                    1

Sub Disminucion_Periodos()
' Inicializo intCodMotInci
  intCodMotInci = 0
 
  With objWinInfo.objWinActiveForm
  'Nuevo Periodo
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        intCodMotInci = 30
        Exit Sub
    End If
  ' Comprobaci�n Fecha Inicio
    If CDate(.rdoCursor("AG08FECINIPERI")) <> dtcDateCombo1(0).Date Then
      intCodMotInci = 10 ' Para Probar
    End If
  ' Comprobaci�n Fecha Fin
    If CDate(.rdoCursor("AG08FECFINPERI")) <> dtcDateCombo1(1).Date Then
      intCodMotInci = 10 ' Para Probar
    End If
  ' Comprobaci�n cambio dias festivos
  ' Lunes
    If -chkCheck1(2).Value <> Val(.rdoCursor("AG08INDLUNFEST")) Then
      intCodMotInci = 10
    End If
  ' Martes
    If -chkCheck1(3).Value <> Val(.rdoCursor("AG08INDMARFEST")) Then
      intCodMotInci = 10
    End If
  ' Mi�rcoles
    If -chkCheck1(4).Value <> Val(.rdoCursor("AG08INDMIEFEST")) Then
      intCodMotInci = 10
    End If
  ' Jueves
    If -chkCheck1(5).Value <> Val(.rdoCursor("AG08INDJUEFEST")) Then
      intCodMotInci = 10
    End If
  ' Viernes
    If -chkCheck1(6).Value <> Val(.rdoCursor("AG08INDVIEFEST")) Then
      intCodMotInci = 10
    End If
  ' Sabado
    If -chkCheck1(7).Value <> Val(.rdoCursor("AG08INDSABFEST")) Then
      intCodMotInci = 10
    End If
  ' Domingo
    If -chkCheck1(8).Value <> Val(.rdoCursor("AG08INDDOMFEST")) Then
      intCodMotInci = 10
    End If
  End With
End Sub

' Procedimiento que devuelve el c�digo de motivo de incidencia
' en la variable global intCodMotInci si hay disminuci�n de
' disponibilidad, si no hay devuelve 0 en intCodMotInci

Sub Disminucion_DiasEspeciales()
' Inicializo intCodMotInci
  intCodMotInci = 0
  With objWinInfo.objWinActiveForm
  ' Si se a�ade un nuevo registro
    If objGen.GetRowCount(.rdoCursor) = 0 Then
        intCodMotInci = 40
        Exit Sub
    End If
  ' Comprobaci�n cambio dias festivos
    If -chkCheck1(9).Value <> Val(.rdoCursor("AG03INDFESTIVO")) And chkCheck1(9).Value = 1 Then
      intCodMotInci = 20 ' Para Probar
    End If
  End With
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form maestro
  Dim objMasterInfo As New clsCWForm
' Form detalle 1
  Dim objDetailInfo1 As New clsCWForm
' Form detalle 2
  Dim objDetailInfo2 As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
  Dim intMode As Integer
' **********************************
' Fin declaraci�n de variables
' **********************************
  blnRollback = False
' Se visualiza el formulario de splash
  Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  If Val(vntCod) = 0 Then
    intMode = cwModeSingleEmpty
  Else
    intMode = cwModeSingleEdit
  End If
  Call objWinInfo.WinCreateInfo(intMode, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
' Documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Mantenimiento de Calendarios"
    .cwDAT = "11-07-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite mantener los diferentes calendarios de la CUN"
    
    .cwUPD = "11-07-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
' Declaraci�n de las caracter�sticas del form maestro
  With objMasterInfo
   ' Asigno nombre al frame
    .strName = "Calendario"

  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
  ' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG0200"
    .blnAskPrimary = False
    If Val(vntCod) > 0 Then
      .strInitialWhere = "AG02CODCALENDA=" & Val(vntCod)
    End If
  ' Definici�n del impreso
    Call .objPrinter.Add("AG0001", "Listado 1 de Calendarios")

  ' Asignaci�n del nivel de acceso del usuario al formulario
  '  .intAllowance = cwAllowReadOnly
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG02CODCALENDA", cwAscending)
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
  
  ' Creaci�n de los filtros de busqueda
    
    Call .FormCreateFilterWhere(strKey, "Tabla de Calendarios")
    Call .FormAddFilterWhere(strKey, "AG02CODCALENDA", "C�digo  Calendario", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG02DESCALENDA", "Descripci�n Calendario", cwString)
    Call .FormAddFilterWhere(strKey, "AG02INDCALPRIN", "Indicador Calendario Principal", cwString)
    
  ' Creaci�n de los criterios de ordenaci�n
    Call .FormAddFilterOrder(strKey, "AG02CODCALENDA", "C�digo")
    Call .FormAddFilterOrder(strKey, "AG02DESCALENDA", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "AG02INDCALPRIN", "Indicador Calendario Principal")
  
  End With
  
' Declaraci�n de las caracter�sticas del form detalle1
  With objDetailInfo1
    ' Asigno nombre al frame
    .strName = "Per�odo_Vigencia"
  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
  ' Asignaci�n del contenedor(frame)padre del form
    Set .objFatherContainer = fraFrame1(0)
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(1)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(3)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG0800"
    .blnAskPrimary = False
      
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG02CODCALENDA", cwAscending)
    Call .FormAddOrderField("AG08CODPERVIGE", cwAscending)
  ' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("AG02CODCALENDA", txtText1(0))
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
  ' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Periodos de Vigencia")
    Call .FormAddFilterWhere(strKey, "AG08DESPERVIGE", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AG08INDLUNFEST", "Indicador Lunes Festivo", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG08INDMARFEST", "Indicador Martes Festivo", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG08INDMIEFEST", "Indicador Mi�rcoles Festivo", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG08INDJUEFEST", "Indicador Jueves Festivo", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG08INDVIEFEST", "Indicador Viernes Festivo", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG08INDSABFEST", "Indicador Sabado Festivo", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG08INDDOMFEST", "Indicador Domingo Festivo", cwBoolean)
    Call .FormAddFilterWhere(strKey, "AG08FECINIPERI", "Fecha Desde", cwDate)
    Call .FormAddFilterWhere(strKey, "AG08FECFINPERI", "Fecha Hasta", cwDate)
    
  ' Creaci�n de los criterios de ordenaci�n
    Call .FormAddFilterOrder(strKey, "AG08DESPERVIGE", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "AG08FECINIPERI", "Fecha Desde")
    Call .FormAddFilterOrder(strKey, "AG08FECFINPERI", "Fecha Hasta")

  End With
   
' Declaraci�n de las caracter�sticas del form detalle2
  With objDetailInfo2
   ' Asigno nombre al frame
    .strName = "D�a_Especial"
  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(2)
  ' Asignaci�n del contenedor(frame)padre del form
    Set .objFatherContainer = fraFrame1(0)
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(2)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(4)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG0300"
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG03FECDIAESPE", cwAscending)
  ' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("AG02CODCALENDA", txtText1(0))
  ' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
  ' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de D�as Especiales")
    Call .FormAddFilterWhere(strKey, "AG03FECDIAESPE", "Fecha D�a Especial", cwString)
    Call .FormAddFilterWhere(strKey, "AG03DESDIAESPE", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "AG03INDFESTIVO", "Indicador Festivo", cwBoolean)
   
  ' Creaci�n de los criterios de ordenaci�n
    
    Call .FormAddFilterOrder(strKey, "AG03FECDIAESPE", "Fecha D�a Especial")
    Call .FormAddFilterOrder(strKey, "AG03DESPDIAESPE", "Descripci�n")

  End With
  
' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
    
  ' Se a�aden los formularios a la ventana
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo1, cwFormDetail)
    Call .FormAddInfo(objDetailInfo2, cwFormDetail)
  ' Se obtiene informaci�n de las caracter�sticas de
  ' los controles del formulario
    Call .FormCreateInfo(objMasterInfo)
        
  ' Campos que intervienen en busquedas
    
    'Form Calendario
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    'Form Periodos de Vigencia
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(4)).blnInFind = True
    .CtrlGetInfo(chkCheck1(5)).blnInFind = True
    .CtrlGetInfo(chkCheck1(6)).blnInFind = True
    .CtrlGetInfo(chkCheck1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(8)).blnInFind = True
    
    'Form D�as Especiales
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(chkCheck1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnForeign = False
  
  ' Propiedades del campo clave calendario para asignaci�n autom�tica
    .CtrlGetInfo(txtText1(0)).blnValidate = False
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
  
  ' Propiedades del campo clave periodo vigencia para asignaci�n autom�tica
    .CtrlGetInfo(txtText1(2)).blnValidate = False
    .CtrlGetInfo(txtText1(2)).blnInGrid = False

  
   ' Eliminamos campos del grid
   
     .CtrlGetInfo(txtText1(2)).blnInGrid = False
     .CtrlGetInfo(txtText1(10)).blnInGrid = False
     .CtrlGetInfo(txtText1(11)).blnInGrid = False
 
     .CtrlGetInfo(txtText1(21)).blnInGrid = False
     .CtrlGetInfo(txtText1(22)).blnInGrid = False
  
  ' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
  ' Se estabiliza la ventana configurando las propiedades
  ' de los controles
    Call .WinStabilize
  
  End With
  txtText1(5).BackColor = objApp.objUserColor.lngReadOnly
' Se oculta el formulario de splash
  Call objApp.SplashOff

'Inicializo variables
  blnRefrescar = False
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
  If intNewStatus = cwModeSingleAddRest Then
    objWinInfo.CtrlGetInfo(chkCheck1(0)).blnReadOnly = False
  End If

End Sub

Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
 'Refrescar por cambio de calendario principal
  If strFormName = "Calendario" And blnRefrescar = True Then
    blnRefrescar = False
    objWinInfo.DataRefresh
  End If
End Sub

' Introducci�n de los impresos
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "Calendario" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(True)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)

  Dim blnCalPrincipal As Boolean
  Dim rdoCalendario As rdoResultset
  Dim rdoQueryCal As rdoQuery
  Dim strSql As String
  
  intCancel = objWinInfo.WinExit

'******************************************************************
' Compruebo si hay alg�n calendario principal, si no lo hay saco
' un mensaje y vuelvo al form
'******************************************************************
 
  'Abro el cursor
   strSql = "SELECT AG02CODCALENDA FROM " & objEnv.GetValue("Database") & "AG0200 WHERE AG02INDCALPRIN=-1 AND AG02FECBAJA IS NULL"
   Set rdoQueryCal = objApp.rdoConnect.CreateQuery("", strSql)
   rdoQueryCal.MaxRows = 1
   Set rdoCalendario = rdoQueryCal.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
 
 ' Si no hay calendario principal saco un mensaje de error
   If objGen.GetRowCount(rdoCalendario) = 0 Then
     Call objError.SetError(cwCodeMsg, "No existe ning�n calendario principal")
     Call objError.Raise
     'Cancelo el Query_Unload
     intCancel = 1
   End If
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
' Si se a�ade un nuevo registro de per�odo de vigencia
' o d�a especial le paso el c�digo del calendario
 If strFormName = "Per�odo_Vigencia" Then
   txtText1(10).Text = txtText1(0).Text
 End If
 If strFormName = "D�a_Especial" Then
   txtText1(11).Text = txtText1(0).Text
 End If
  
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  'Si es el calendario principal desactivo el indicador y la
  'fecha de baja
  If strFormName = "Calendario" Then
    If chkCheck1(0).Value = 1 Then
      objWinInfo.CtrlGetInfo(chkCheck1(0)).blnReadOnly = True
      objWinInfo.CtrlGetInfo(dtcDateCombo1(3)).blnReadOnly = True
    Else
      objWinInfo.CtrlGetInfo(chkCheck1(0)).blnReadOnly = False
      objWinInfo.CtrlGetInfo(dtcDateCombo1(3)).blnReadOnly = False
    End If
  End If
  If strFormName = "D�a_Especial" And objWinInfo.intWinStatus <> cwModeSingleEmpty Then
    If IsDate(dtcDateCombo1(2).Date) Then
      txtText1(5) = UCase(DiaSemana(dtcDateCombo1(2).Day(dtcDateCombo1(2).Date).DayofWeek - 1))
    End If
  End If

End Sub

' Validaciones del programa
Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)
  
  Dim blnCalPrincipal As Boolean
  Dim rdoCalendario As rdoResultset
  Dim strSql As String
  Dim vntCodigo As Variant
  Dim rdoPeriodo As rdoResultset
  Dim blnInPeriod As Boolean
  Dim rdoConsulta1 As rdoQuery
  Dim rdoConsulta2 As rdoQuery
  
  
'******************************************************************
'******************************************************************
' Validaciones del form Calendario
'******************************************************************
'******************************************************************
  
  If strFormName = "Calendario" Then
    
'******************************************************************
   ' Comprobaci�n de calendario principal �nico
'******************************************************************
     
     If chkCheck1(0).Value = 1 Then
      'Abrimos el cursor
      strSql = "SELECT AG02CODCALENDA, AG02INDCALPRIN FROM " _
        & objEnv.GetValue("Database") & "AG0200 WHERE AG02INDCALPRIN = -1  AND AG02FECBAJA IS  NULL"
      Set rdoCalendario = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset)
      If objGen.GetRowCount(rdoCalendario) > 0 Then
        Call objError.SetError(cwCodeQuery, "Ya existe un Calendario Principal. �Desea continuar?")
        If objError.Raise = vbNo Then
           blnCancel = True
           Exit Sub
        End If
      End If
    End If
    If objWinInfo.CtrlGet(chkCheck1(0)) = 1 And dtcDateCombo1(3).Date <> "" Then
      Call objError.SetError(cwCodeMsg, "El Calendario Principal no se puede dar de baja")
      Call objError.Raise
      blnCancel = True
    End If

  
  End If


'******************************************************************
'******************************************************************
' Validaciones del form Per�odo de Vigencia
'******************************************************************
'******************************************************************
  
  If strFormName = "Per�odo_Vigencia" Then
   
'******************************************************************
   ' Validaci�n de Fecha Fin mayor que Fecha Inicio
'******************************************************************
    If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
         Call objError.SetError(cwCodeMsg, "La Fecha Fin es  menor que Fecha Inicial")
         Call objError.Raise
         blnCancel = True
         Exit Sub
      End If
    End If

'******************************************************************
  'Validaci�n de solapamiento de fechas
'******************************************************************
    
'    strSql = "select AG08CODPERVIGE,AG08FECINIPERI,AG08FECFINPERI from AG0800 " _
'         & " where AG02CODCALENDA=" & Val(txtText1(10).Text)
    strSql = "select AG08CODPERVIGE,AG08FECINIPERI,AG08FECFINPERI from AG0800 " _
         & " where AG02CODCALENDA=?"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Set rdoConsulta1 = objApp.rdoConnect.CreateQuery("", strSql)
  rdoConsulta1(0) = Val(objWinInfo.CtrlGet(txtText1(0)))
'  Set rdoCalendario = rdoConsulta.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
  
    Set rdoPeriodo = rdoConsulta1.OpenResultset(strSql)
   'Si estamos editando guardamos el c�digo de periodo
   'para no compararlo consigo mismo
    If objWinInfo.intWinStatus = cwModeSingleEdit And Not rdoPeriodo.EOF Then
      vntCodigo = objWinInfo.objWinActiveForm.rdoCursor("AG08CODPERVIGE")
      If rdoPeriodo("AG08CODPERVIGE") = vntCodigo Then
        rdoPeriodo.MoveNext
      End If
    End If
   'Recorremos el cursor hasta el final o hasta que encontremos
   'solapamiento
  
    
    While Not rdoPeriodo.EOF And Not blnCancel
      
      'Miramos si hay solapamiento
      If dtcDateCombo1(0).Date <= CDate(rdoPeriodo("AG08FECFINPERI")) Then
        If dtcDateCombo1(1).Date >= CDate(rdoPeriodo("AG08FECINIPERI")) Then
          'Sacamos mensaje de error
          Call objError.SetError(cwCodeMsg, "No se han podido actualizar los datos." & Chr$(13) & "Existe solapamiento de fechas.")
          Call objError.Raise
         'Cancelamos el alta o modificaci�n
          blnCancel = True
          Exit Sub
        End If
      End If
      rdoPeriodo.MoveNext
     'Si estamos editando miramos si el registro a comparar es
     'el actual para saltarlo
      If objWinInfo.intWinStatus = cwModeSingleEdit And Not rdoPeriodo.EOF Then
        If rdoPeriodo("AG08CODPERVIGE") = vntCodigo Then
          rdoPeriodo.MoveNext
        End If
      End If
    Wend
  
'******************************************************************
  ' Comprobaci�n disminuci�n de disponibilidad
'******************************************************************
    'Call Disminucion_Periodos
  
  End If
  
   

'******************************************************************
'******************************************************************
' Validaciones del form D�as Especiales
'******************************************************************
'******************************************************************
  
  If strFormName = "D�a_Especial" Then

    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
      blnInPeriod = False
'      strSql = "SELECT AG08CODPERVIGE,AG08FECINIPERI,AG08FECFINPERI FROM AG0800 WHERE AG02CODCALENDA=" & Val(objWinInfo.CtrlGet(txtText1(0)))
  
      strSql = "SELECT AG08CODPERVIGE,AG08FECINIPERI,AG08FECFINPERI " _
            & " FROM AG0800 WHERE AG02CODCALENDA=?"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Set rdoConsulta2 = objApp.rdoConnect.CreateQuery("", strSql)
  rdoConsulta2(0) = Val(objWinInfo.CtrlGet(txtText1(0)))
  Set rdoCalendario = rdoConsulta2.OpenResultset(strSql)

  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
'      Set rdoCalendario = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
      If Not rdoCalendario.EOF Then
        Do While Not rdoCalendario.EOF
          If dtcDateCombo1(2).Date >= CDate(rdoCalendario(1)) And dtcDateCombo1(2).Date <= CDate(rdoCalendario(2)) Then
            blnInPeriod = True
            GoTo Continuar
          Else
            rdoCalendario.MoveNext
          End If
        Loop
      Else
        blnInPeriod = False
      End If
Continuar:
      If Not blnInPeriod Then
        Call objError.SetError(cwCodeMsg, "El D�a Especial introducido no est� dentro de ning�n Per�odo de Vigencia.")
        Call objError.Raise
        'Cancelamos el alta o modificaci�n
        blnCancel = True
        Exit Sub
      End If
    End If
'******************************************************************
  ' Comprobaci�n disminuci�n de disponibilidad
'******************************************************************
    Call Disminucion_DiasEspeciales
  
  End If

End Sub


Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'******************************************************************
' Compruebo si el usuarario ha cancelado o no la incidencia para
' validar los cambios o anularlos
'******************************************************************
  If blnRollback = True Or blnError = True Then
    Call objApp.rdoConnect.RollbackTrans
    blnRollback = False
  Else
    Call objApp.rdoConnect.CommitTrans
  End If
  
  'Si se ha cambiado o creado el calendario principal refresco el
  'cursor para que le anterior cal.principal se refresque
  If strFormName = "Calendario" And chkCheck1(0).Value = 1 Then
    blnRefrescar = True
  End If
  
End Sub

Private Sub objWinInfo_cwPreChangeForm(ByVal strFormName As String)
 'Si el Calendario est� dado de baja no dejo a�adir o borrar
 'Periodos de Vigencia y D�as Espceiales
  If strFormName = "Calendario" And objWinInfo.intWinStatus <> cwModeSingleEmpty Then
    If Not IsNull(objWinInfo.cllWinForms("fraFrame1(0)").rdoCursor("AG02FECBAJA")) Then
      If CDate(objWinInfo.cllWinForms("fraFrame1(0)").rdoCursor("AG02FECBAJA")) <= CDate(objGen.GetDBDateTime) Then
        objWinInfo.cllWinForms("fraFrame1(1)").intAllowance = cwAllowReadOnly
        objWinInfo.cllWinForms("fraFrame1(2)").intAllowance = cwAllowReadOnly
      Else
        objWinInfo.cllWinForms("fraFrame1(1)").intAllowance = cwAllowAll
        objWinInfo.cllWinForms("fraFrame1(2)").intAllowance = cwAllowAll
      End If
    End If
  End If

End Sub


Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
  
  Dim rdoRecurso As rdoResultset
  Dim strSql As String
  Dim QyConsulta As rdoQuery
  
'******************************************************************
'******************************************************************
' Comprobaciones del form Calendario
'******************************************************************
'******************************************************************
  
  If strFormName = "Calendario" Then
    'Comprobaci�n de que no tenga ning�n periodo o d�a especial
    'asociado
    If objGen.GetRowCount(objWinInfo.cllWinForms("fraFrame1(1)").rdoCursor) > 0 Then
      Call objError.SetError(cwCodeMsg, "No se puede borrar un Calendario que tiene asociado alg�n Periodo de Vigencia")
      Call objError.Raise
      blnCancel = True
      Exit Sub
    End If
    If objGen.GetRowCount(objWinInfo.cllWinForms("fraFrame1(2)").rdoCursor) > 0 Then
      Call objError.SetError(cwCodeMsg, "No se puede borrar un Calendario que tiene asociado alg�n D�a Especial")
      Call objError.Raise
      blnCancel = True
      Exit Sub
    End If
   
'******************************************************************
    'Comprobamos que ning�n recurso tenga asociado este calendario
'******************************************************************
    'Abrimos el cursor
'    strSql = "SELECT AG11CODRECURSO FROM " & objEnv.GetValue("Database") & "AG1100 " _
'          & " WHERE AG02CODCALENDA=" & Val(txtText1(0))
    strSql = "SELECT AG11CODRECURSO FROM " & objEnv.GetValue("Database") & "AG1100 " _
          & " WHERE AG02CODCALENDA=?"
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", strSql)
    QyConsulta(0) = Val(txtText1(0))
    Set rdoRecurso = QyConsulta.OpenResultset(strSql)
    
    'Si hay algun registro en el cursor sacamos mensaje de error
    If objGen.GetRowCount(rdoRecurso) > 0 Then
      Call objError.SetError(cwCodeMsg, "Este calendario lo tiene asignado alg�n recurso")
      Call objError.Raise
      blnCancel = True
      Exit Sub
    End If
'******************************************************************
    'Comprobamos que no se borra el calendario principal
'******************************************************************
    If objWinInfo.objWinActiveForm.rdoCursor("AG02INDCALPRIN") = -1 Then
      Call objError.SetError(cwCodeMsg, "El calendario principal no se puede borrar")
      Call objError.Raise
      blnCancel = True
      Exit Sub
    End If
    
    Exit Sub
  End If
  
'******************************************************************
  ' Comprobaci�n disminuci�n de disponibilidad
'******************************************************************
  
  intCodMotInci = 0
 'Borrado de Periodo de Vigencia
  If strFormName = "Per�odo_Vigencia" Then
    intCodMotInci = 60
    ' Carga de la ventana Incidencias
    Call AddInciden(intCodMotInci, False)
    blnCancel = blnRollback
    strSql = "DELETE " _
            & " FROM AG0300 WHERE AG02CODCALENDA= " _
            & Val(txtText1(0)) _
            & " AND AG03FECDIAESPE BETWEEN " _
            & objGen.ValueToSQL(dtcDateCombo1(0).Date, cwDate) _
            & " AND " & objGen.ValueToSQL(dtcDateCombo1(1).Date, cwDate)
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
  Rem =====================================================================
            
    Call objApp.rdoConnect.Execute(strSql)
  End If
 
 'Borrado de un d�a no festivo
  If strFormName = "D�a_Especial" Then
    If objWinInfo.objWinActiveForm.rdoCursor("AG03INDFESTIVO").Value = 0 Then
      intCodMotInci = 50
    ' Carga de la ventana Incidencias
      Call AddInciden(intCodMotInci, False)
      blnCancel = blnRollback
    
    End If
  End If
  
  
  
End Sub



Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
' Variable para las sentencia Sql que abrira el cursor
' para dar de alta la incidencia
  Dim strSql As String
  Dim rdoConsulta As rdoQuery
' Variable que guardara el campo n� de incidencia del form activo
  Dim strCampoNumIncidencia As String
' Variable para el resultset para obtener el nuevo c�digo de recurso
  Dim rdoCalendario As rdoResultset
  Dim rdoDiaEspecial As rdoResultset
'******************************************************************
  ' Comprobaci�nes del form Calendario
'******************************************************************
  
  If strFormName = "Calendario" Then
 
    
    ' Empieza transacci�n
    Call objApp.rdoConnect.BeginTrans
    objSecurity.RegSession
    With objWinInfo
      If .intWinStatus = cwModeSingleAddRest Then
      ' Abrimos cursor
        strSql = "select MAX(AG02CODCALENDA) from AG0200 "
    
'        vntNuevoCod = GetNewCode(strSql)
'LAS 13.4.99
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
         vntNuevoCod = GetNewCode(rdoConsulta)
    rdoConsulta.Close
        
        
        Call .CtrlStabilize(txtText1(0), vntNuevoCod)
      
      
      End If
    End With
   
   'Si es calendario Principal borramos el otro cal principal
    If chkCheck1(0).Value = 1 And dtcDateCombo1(3).Date = "" Then
      strSql = "select AG02CODCALENDA, AG02INDCALPRIN from AG0200 where AG02INDCALPRIN=-1"
      Set rdoCalendario = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
     
      If objGen.GetRowCount(rdoCalendario) > 0 Then
        While Not rdoCalendario.EOF
          rdoCalendario.Edit
          rdoCalendario("AG02INDCALPRIN") = 0
          rdoCalendario.Update
          rdoCalendario.MoveNext
        Wend
      End If
    End If
  
   Exit Sub
 End If
    
     
    
 

'******************************************************************
  ' Comprobaci�nes del form Per�odo_Vigencia
'******************************************************************

  If strFormName = "Per�odo_Vigencia" Then
  ' Asignamos el campo n� de incidencia
    strCampoNumIncidencia = "AG05NUMINCIDEN"
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
'      strSql = "SELECT MAX(AG08CODPERVIGE) FROM " & objEnv.GetValue("Database") & "AG0800 " _
'             & " WHERE AG02CODCALENDA=" & Val(txtText1(0)) '& " ORDER BY AG08CODPERVIGE DESC"
      strSql = "SELECT MAX(AG08CODPERVIGE) FROM " & objEnv.GetValue("Database") & "AG0800 " _
             & " WHERE AG02CODCALENDA= ? "  '& " ORDER BY AG08CODPERVIGE DESC"
     ' Empieza transacci�n
      Call objApp.rdoConnect.BeginTrans
      objSecurity.RegSession
     ' Obtengo un c�digo nuevo
      
'      vntNuevoCod = GetNewCode(strSql)
'LAS 13.4.1999
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql)
        rdoConsulta(0) = Val(txtText1(0))
         vntNuevoCod = GetNewCode(rdoConsulta)
    rdoConsulta.Close
      
     ' Asigno el c�digo al registro y a la textbox
      If Val(vntNuevoCod) > 0 Then
        Call objWinInfo.CtrlStabilize(txtText1(2), vntNuevoCod)
      End If

      strSql = "SELECT AG02CODCALENDA, AG03FECDIAESPE, AG03DESDIAESPE, AG03INDFESTIVO, AG03FECBAJA, AG05NUMINCIDEN " _
            & " FROM AG0300 WHERE AG02CODCALENDA= " _
            & Val(GetTableColumn("SELECT AG02CODCALENDA FROM AG0200 WHERE AG02INDCALPRIN=-1")("AG02CODCALENDA")) _
            & " AND AG03FECDIAESPE BETWEEN " _
            & objGen.ValueToSQL(dtcDateCombo1(0).Date, cwDate) _
            & " AND " & objGen.ValueToSQL(dtcDateCombo1(1).Date, cwDate)
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
  Rem =====================================================================
      Set rdoCalendario = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
      strSql = "SELECT AG02CODCALENDA, AG03FECDIAESPE, AG03DESDIAESPE, AG03INDFESTIVO, AG03FECBAJA, AG05NUMINCIDEN " _
            & " FROM AG0300 WHERE AG02CODCALENDA=0"
            
      Set rdoDiaEspecial = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)

      Do While Not rdoCalendario.EOF
        rdoDiaEspecial.AddNew
        rdoDiaEspecial("AG02CODCALENDA") = Val(txtText1(0))
        rdoDiaEspecial("AG03FECDIAESPE") = rdoCalendario("AG03FECDIAESPE")
        rdoDiaEspecial("AG03DESDIAESPE") = rdoCalendario("AG03DESDIAESPE")
        rdoDiaEspecial("AG03INDFESTIVO") = rdoCalendario("AG03INDFESTIVO")
        If Not IsNull(rdoCalendario("AG03FECBAJA")) Then rdoDiaEspecial("AG03FECBAJA") = rdoCalendario("AG03FECBAJA")
        'rdoDiaEspecial("AG05NUMINCIDEN") = rdoCalendario("AG05NUMINCIDEN")
        On Error GoTo DiaError
        rdoDiaEspecial.Update
        rdoCalendario.MoveNext
      Loop
    End If
  End If
 
'******************************************************************
 'Si el form actual es D�a especial asigno a strCampoNumIncidencia
 'su campo n�mero de incidencia
'******************************************************************
  If strFormName = "D�a_Especial" Then
    strCampoNumIncidencia = "AG05NUMINCIDEN"
    If dtcDateCombo1(4).Text <> "" And chkCheck1(9).Value = 0 Then
      intCodMotInci = 50
    End If
  End If
  
  
'******************************************************************
' Miramos si hay disminuci�n de disponibilidad
'******************************************************************
  If intCodMotInci <> 0 Then
      
  ' Iniciamos la transacci�n
    Call objApp.rdoConnect.BeginTrans
   objSecurity.RegSession
  ' Carga de la ventana Incidencias
    Call AddInciden(intCodMotInci, False)
    If Not blnRollback And blnActive Then
    ' Asignamos el n�mero de incidencia al campo del form activo
      objWinInfo.objWinActiveForm.rdoCursor(strCampoNumIncidencia) = vntNuevoCod
    End If
  End If
  Exit Sub
  
DiaError:
  rdoDiaEspecial.CancelUpdate
  rdoDiaEspecial.Close
  strSql = "SELECT AG02CODCALENDA, AG03FECDIAESPE, AG03DESDIAESPE, AG03INDFESTIVO, AG03FECBAJA, AG05NUMINCIDEN " _
      & " FROM AG0300 WHERE AG02CODCALENDA=0"
            
  Set rdoDiaEspecial = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
  Resume Next
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
  If intIndex = 2 Then
    If IsDate(dtcDateCombo1(2).Date) Then
      txtText1(5) = UCase(DiaSemana(dtcDateCombo1(2).Day(dtcDateCombo1(2).Date).DayofWeek - 1))
    End If
  End If

End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange

End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 2 Then
    If IsDate(dtcDateCombo1(2).Date) Then
      txtText1(5) = UCase(DiaSemana(dtcDateCombo1(2).Day(dtcDateCombo1(2).Date).DayofWeek - 1))
    End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  'Si es Calendario Principal desactivo el campo fecha baja
  If chkCheck1(0).Value = 1 Then
    dtcDateCombo1(3).Date = ""
    dtcDateCombo1(3).Enabled = False
  Else
    dtcDateCombo1(3).Enabled = True
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
