VERSION 5.00
Begin VB.Form frmInstall 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CodeWizard Library"
   ClientHeight    =   2550
   ClientLeft      =   3750
   ClientTop       =   3765
   ClientWidth     =   6150
   ClipControls    =   0   'False
   FillColor       =   &H8000000F&
   FontTransparent =   0   'False
   ForeColor       =   &H00C0C0C0&
   Icon            =   "Install.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   Picture         =   "Install.frx":030A
   ScaleHeight     =   2550
   ScaleWidth      =   6150
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   3491
      TabIndex        =   2
      ToolTipText     =   "Cierra el programa"
      Top             =   2025
      Width           =   1500
   End
   Begin VB.CommandButton cmdInstall 
      Caption         =   "&Verificar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1159
      TabIndex        =   0
      ToolTipText     =   "Verifica la versi�n de CodeWizard"
      Top             =   2025
      Width           =   1500
   End
   Begin VB.Label lblOk 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   14.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   5715
      TabIndex        =   6
      Top             =   945
      Width           =   255
   End
   Begin VB.Label lblVersion 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   1
      Left            =   2385
      TabIndex        =   5
      Top             =   1005
      Width           =   60
   End
   Begin VB.Label lblOk 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   14.25
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   5715
      TabIndex        =   4
      Top             =   495
      Width           =   255
   End
   Begin VB.Label lblVersion 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   0
      Left            =   2385
      TabIndex        =   3
      Top             =   555
      Width           =   60
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "Bienvenido al centro de control de CodeWizard Library"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   240
      Left            =   495
      TabIndex        =   1
      Top             =   90
      Width           =   5265
   End
End
Attribute VB_Name = "frmInstall"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub cmdCancel_Click()
  Unload Me
End Sub


Private Sub cmdInstall_Click()
  Dim intObject As Integer
  
  On Error GoTo InitError
  
cwServer0:
  intObject = 0
  Dim objCW As Object
  Set objCW = CreateObject("CodeWizard.clsCW")
  objCW.objApp.blnUseRegistry = False
  lblVersion(0) = objCW.objApp.GetCWVersion
  lblOk(0).Left = lblVersion(0).Left + lblVersion(0).Width + 100
  lblOk(0).Visible = True
  Set objCW = Nothing
  
cwServer1:
  intObject = 1
  Dim objSec As Object
  Set objSec = CreateObject("CWSecure.clsCWSecure")
  lblVersion(1) = objSec.GetCWSecureVersion
  lblOk(1).Left = lblVersion(1).Left + lblVersion(1).Width + 100
  lblOk(1).Visible = True
  Set objSec = Nothing
  
  cmdInstall.Enabled = False
  Exit Sub

InitError:
  Select Case intObject
    Case 0
      lblVersion(0) = "Error en la conexi�n con CodeWizard"
    Case 1
      lblVersion(1) = "Error en la conexi�n con CWSecure"
  End Select
  lblOk(intObject).Visible = False
  If intObject = 0 Then
    Resume cwServer1
  End If
End Sub

Private Sub Form_Load()
  lblOk(0).Visible = False
  lblOk(1).Visible = False
End Sub
