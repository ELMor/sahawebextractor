VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWForm
' Coded by SYSECA Bilbao
' **********************************************************************************


' tipo de mantenimiento
Public Enum cwFormType
  cwFormDetail = 0
  cwFormMultiLine = 1
End Enum

' modelo de formulario
Public Enum cwFormModel
  cwWithGrid = 1
  cwWithTab = 2
  cwWithKeys = 4
  cwWithoutGrid = 0
  cwWithoutTab = 0
  cwWithoutKeys = 0
End Enum

' permisos del formulario
Public Enum cwAllowType
  cwAllowAdd = 1
  cwAllowModify = 2
  cwAllowDelete = 4
  cwAllowAll = 7
  cwAllowReadOnly = 0
End Enum

Public Enum cwOrderDirection
  cwAscending = False
  cwDescending = True
End Enum
  

Public strName            As String
Public intFormType        As cwFormType           ' tipo de formulario
Public intFormModel       As cwFormModel
Public cllControls        As New Collection
Public blnDirection       As Boolean
Public intCursorSize      As Integer
Public intAllowance       As cwAllowType
Public blnPKAutomatic     As Boolean
Public blnChanged         As Boolean
Public blnFilterOn        As Boolean
Public blnHasMaint        As Boolean              ' �tiene mantenimiento?
Public blnAskPrimary      As Boolean
Public strDataBase        As String
Public strTable           As String
Public strColumns         As String
Public strWhere           As String
Public strInitialWhere    As String
Public cllOrderBy         As New Collection
Public cllRelations       As New Collection
Public blnMasive          As Boolean
Public blnMasiveOn        As Boolean
Public rdoStatement       As rdoPreparedStatement
Public rdoCursor          As rdoResultset
Public blnInFirstReg      As Boolean
Public blnInLastReg       As Boolean
Public grdGrid            As Object
Public tabMainTab         As Object
Public blnForzeUpdate     As Boolean
Public objFormContainer   As Object
Public objFatherContainer As Object
Public cllContainers      As New Collection
Public objFilter          As New clsCWFilter
Public intSearchTop       As Integer              ' posici�n Y de la ventana de localizar
Public intSearchLeft      As Integer              ' posici�n X de la ventana de localizar
Public objPrinter         As New clsCWPrinter
Public intMaxFilterCursor As Integer
Public blnEnabled         As Boolean
Public intRestrictions    As Integer
'Oscar
'Public strHint            As String                 'texto a poner tras "SELECT "

Private Sub Class_Initialize()
  intFormModel = cwWithGrid + cwWithTab + cwWithKeys
  intCursorSize = cwCursorSizeDef
  intAllowance = cwAllowAll
  blnAskPrimary = True
  blnMasive = True
  blnPKAutomatic = True
  intSearchLeft = -1
  intSearchTop = -1
  objPrinter.Selected = 0
  intMaxFilterCursor = cwCursorSizeFilter
  blnEnabled = True
End Sub


' a�ade un objeto order de tipo field al formulario
Public Sub FormAddOrderField(ByVal strField As String, _
                             ByVal blnDirec As cwOrderDirection)
  Dim objOrder As New clsCWOrder
    
  On Error GoTo cwIntError
  
  'Salva: objOrder.strFieldName = strTable & "." & strField
  objOrder.strFieldName = strField
  objOrder.blnDirection = blnDirec
  Call cllOrderBy.Add(objOrder)
  Set objOrder = Nothing
  Exit Sub

cwIntError:
  Call objError.InternalError(Me, "FormAddOrderField", strField)
End Sub


' a�ade un objeto order de tipo function al formulario
Public Sub FormAddOrderFunction(ByVal intFunction As Integer, _
                                ByRef aFields() As String, _
                                ByVal blnDirec As cwOrderDirection)
  Dim objOrder As New clsCWOrder
  Dim lngInd       As Long
  
  On Error GoTo cwIntError
  
  objOrder.intFunction = intFunction
  For lngInd = LBound(aFields) To UBound(aFields)
    Call objOrder.cllFields.Add(strTable & "." & aFields(lngInd))
  Next
  objOrder.blnDirection = blnDirec
  Call cllOrderBy.Add(objOrder)
  Set objOrder = Nothing
  Exit Sub

cwIntError:
  Call objError.InternalError(Me, "FormAddOrderFunction", intFunction)
End Sub


' a�ade un objeto relation al formulario
Public Sub FormAddRelation(ByVal strField As String, _
                           ByVal objControl As Object)
  Dim objRelation As New clsCWRelation
  
  On Error GoTo cwIntError
  
  'objRelation.strFieldDetail = strTable & "." & strField
  objRelation.strFieldDetail = strField
  
  Set objRelation.objCtrlMaster = objControl
  Call cllRelations.Add(objRelation)
  Set objRelation = Nothing
  Exit Sub

cwIntError:
  Call objError.InternalError(Me, "FormAddRelation", strField)
End Sub


' a�ade campos a la ORDER BY de filtros
Public Sub FormAddFilterOrder(ByVal strTableName As String, _
                              ByVal strColumn As String, _
                              ByVal strDescription As String)
  Dim objFilterOrder As New clsCWFilterOrder
      
  On Error GoTo cwIntError
  
  With objFilterOrder
    'Salva:.strFieldName = IIf(objApp.blnRemoveOrderTable, "", strTableName & ".") & strColumn
    .strFieldName = strColumn
    .strFieldDesc = strDescription
    .blnActive = False
    .blnOrderBy = False
  End With
  Call objFilter.cllOrderBy.Add(objFilterOrder)
  Set objFilterOrder = Nothing
  
  With objFilterOrder
    'Salva:.strFieldName = IIf(objApp.blnRemoveOrderTable, "", strTableName & ".") & strColumn
    .strFieldName = strColumn
    .strFieldDesc = strDescription
    .blnActive = False
    .blnOrderBy = False
  End With
  Call objPrinter.objFilter.cllOrderBy.Add(objFilterOrder)
  Set objFilterOrder = Nothing
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "FormAddFilterOrder")
End Sub


' crea la informaci�n sobre la WHERE de filtros
Public Sub FormCreateFilterWhere(ByVal strTableName As String, _
                                 ByVal strTableDesc As String, _
                                 Optional ByVal strFatherName As String = "", _
                                 Optional ByVal strNameColumn As String = "", _
                                 Optional ByVal strFatherColumn As String = "")
  Dim objTables As New clsCWFilterTables
  
  On Error GoTo cwIntError
  
  With objTables
    .strTableName = strTableName
    .strTableDesc = strTableDesc
    .strFatherName = strFatherName
    .strNameColumn = strNameColumn
    .strFatherColumn = strFatherColumn
  End With
  Call objFilter.cllFilterTables.Add(objTables, objTables.strTableName)
  Set objTables = Nothing
  
  With objTables
    .strTableName = strTableName
    .strTableDesc = strTableDesc
    .strFatherName = strFatherName
    .strNameColumn = strNameColumn
    .strFatherColumn = strFatherColumn
  End With
  Call objPrinter.objFilter.cllFilterTables.Add(objTables, objTables.strTableName)
  Set objTables = Nothing
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "FormCreateFilterWhere")
End Sub


' a�ade campos a la WHERE de filtros
Public Sub FormAddFilterWhere(ByVal strTableName As String, _
                              ByVal strColumn As String, _
                              ByVal strDescription As String, _
                              ByVal intType As cwDataType, _
                              Optional ByVal strSQL As String = "")
  Dim objFields As New clsCWFilterFields
  
  On Error GoTo cwIntError
  
  With objFields
    '====================================================
     '.strFieldName = strColumn
    '18/6/98
    .strFieldName = strTableName & "." & strColumn
   '========================================================
    .strFieldDesc = IIf(objGen.IsStrEmpty(strDescription), strColumn, strDescription)
    .intFieldType = intType
    .strSQL = strSQL
    Call objGen.RemoveCollection(.cllValues)
    Call objFilter.cllFilterTables(strTableName).cllChilds.Add(objFields, .strFieldName)
  End With
  Set objFields = Nothing
  
  With objFields
    'Salva: .strFieldName = strTableName & "." & strColumn
    .strFieldName = strColumn
    .strFieldDesc = IIf(objGen.IsStrEmpty(strDescription), strColumn, strDescription)
    .intFieldType = intType
    .strSQL = strSQL
    Call objGen.RemoveCollection(.cllValues)
    Call objPrinter.objFilter.cllFilterTables(strTableName).cllChilds.Add(objFields, .strFieldName)
  End With
  Set objFields = Nothing
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "FormAddFilterWhere")
End Sub


' determina las capacidades de un formulario
Friend Function FormGetCaps(ByVal intFormCaps As cwFormModel) As Boolean
  FormGetCaps = (intFormModel And intFormCaps) > 0
End Function


Friend Function FormGetAllowance() As Integer
  Dim intAllow As Integer
  
  If objGen.IsAnd(intAllowance, cwAllowAdd) And Not objGen.IsAnd(intRestrictions, cwRestrictTableInsert) Then
    intAllow = cwAllowAdd
  End If
  If objGen.IsAnd(intAllowance, cwAllowModify) And Not objGen.IsAnd(intRestrictions, cwRestrictTableUpdate) Then
    intAllow = intAllow + cwAllowModify
  End If
  If objGen.IsAnd(intAllowance, cwAllowDelete) And Not objGen.IsAnd(intRestrictions, cwRestrictTableDelete) Then
    intAllow = intAllow + cwAllowDelete
  End If
  FormGetAllowance = intAllow
End Function


Friend Function FormGetEnabled() As Integer
  FormGetEnabled = blnEnabled And Not objGen.IsAnd(intRestrictions, cwRestrictTableSelect)
End Function

