VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmCWView 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Seleccionar"
   ClientHeight    =   5205
   ClientLeft      =   2400
   ClientTop       =   2295
   ClientWidth     =   8730
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "CWFRM05.frx":0000
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5421.875
   ScaleMode       =   0  'Usuario
   ScaleWidth      =   8730
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8730
      _ExtentX        =   15399
      _ExtentY        =   741
      ButtonWidth     =   635
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      Begin VB.ComboBox cboSearch 
         Height          =   315
         Left            =   4635
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   45
         Width           =   2040
      End
      Begin VB.TextBox txtSearch 
         Height          =   330
         Left            =   2925
         MaxLength       =   10
         TabIndex        =   2
         Top             =   45
         Width           =   1635
      End
   End
   Begin VB.PictureBox picDummy 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   330
      Left            =   0
      ScaleHeight     =   330
      ScaleWidth      =   8730
      TabIndex        =   4
      Top             =   420
      Width           =   8730
      Begin VB.Label lblColumn 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   " xxxx"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   1125
         TabIndex        =   6
         Top             =   45
         Width           =   435
      End
      Begin VB.Label lblSearch 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Localizar por:"
         Height          =   195
         Left            =   135
         TabIndex        =   5
         Top             =   45
         Width           =   945
      End
   End
   Begin SSDataWidgets_B.SSDBGrid grdGrid 
      Align           =   1  'Align Top
      Height          =   3285
      Left            =   0
      TabIndex        =   0
      Top             =   750
      Width           =   8730
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   0
      AllowRowSizing  =   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   3
      MaxSelectedRows =   200
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      UseDefaults     =   0   'False
      _ExtentX        =   15399
      _ExtentY        =   5794
      _StockProps     =   79
      ForeColor       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Datos"
      Index           =   10
      Begin VB.Menu optDatos 
         Caption         =   "S&eleccionar y salir"
         Index           =   10
      End
      Begin VB.Menu optDatos 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu optDatos 
         Caption         =   "&Salir"
         Index           =   30
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Index           =   20
      Begin VB.Menu optRegistro 
         Caption         =   "&Primero"
         Index           =   10
      End
      Begin VB.Menu optRegistro 
         Caption         =   "&Anterior"
         Index           =   20
      End
      Begin VB.Menu optRegistro 
         Caption         =   "&Siguiente"
         Index           =   30
      End
      Begin VB.Menu optRegistro 
         Caption         =   "&�ltimo"
         Index           =   40
      End
      Begin VB.Menu optRegistro 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu optRegistro 
         Caption         =   "M�s datos"
         Index           =   60
      End
      Begin VB.Menu optRegistro 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu optRegistro 
         Caption         =   "&Buscar desde el comienzo"
         Index           =   80
      End
      Begin VB.Menu optRegistro 
         Caption         =   "Buscar &desde el registro actual"
         Index           =   90
      End
   End
End
Attribute VB_Name = "frmCWView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWView
' Coded by SYSECA Bilbao
' **********************************************************************************


Private Enum cwMove
  cwMoveFirst = 0
  cwMovePrevious = 1
  cwMoveNext = 2
  cwMoveLast = 3
End Enum


Public Enum cwSelectType
  cwNoneSelect = 0
  cwSingleSelect = 1
  cwMultiSelect = 2
End Enum


Dim mintLeft   As Integer
Dim mintTop    As Integer
Dim mintWidth  As Integer
Dim mintHeight As Integer
Dim mintColumn As Integer
Dim mlngRow    As Long


Public objZoom           As frmCWSearch
Public strZoom           As String
Public strZoomName       As String
Public cllColumns        As Collection
Public blnSelected       As Boolean
Public cllFields         As Collection
Public strSQL            As String
Public intSelectType     As cwSelectType
Public blnSearchCaptions As Boolean
Public lngMaxSize        As Long


Private Sub Form_Load()
  mintLeft = -1
  mintTop = -1
  mintWidth = -1
  mintHeight = -1
  lngMaxSize = cwCursorSizeView
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Call objGen.RemoveCollection(cllColumns)
End Sub

Private Sub optDatos_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call SelectAndExit
    Case 30
      Call OnlyExit
  End Select
End Sub

Private Sub optRegistro_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call MoveReg(cwMoveFirst)
    Case 20
      Call MoveReg(cwMovePrevious)
    Case 30
      Call MoveReg(cwMoveNext)
    Case 40
      Call MoveReg(cwMoveLast)
    Case 60
      Call ProcessZoom
    Case 80
      Call SearchReg(True)
    Case 90
      Call SearchReg(False)
  End Select
End Sub

Private Sub tlbToolbar1_ButtonClick(ByVal btnButton As ComctlLib.Button)
  Select Case btnButton.Index
    Case 2
      Call MoveReg(cwMoveFirst)
    Case 3
      Call MoveReg(cwMovePrevious)
    Case 4
      Call MoveReg(cwMoveNext)
    Case 5
      Call MoveReg(cwMoveLast)
    Case 7
      Call ProcessZoom
    Case 9
      Call SearchReg(True)
    Case 10
      Call SearchReg(False)
    Case 12
      Call SelectAndExit
    Case 14
      Call OnlyExit
  End Select
End Sub

Private Sub grdGrid_DblClick()
  If tlbToolbar1.Buttons(12).Enabled Then
    If mlngRow = grdGrid.AddItemRowIndex(grdGrid.Bookmark) Then
      Call SelectAndExit
    End If
  End If
End Sub

Private Sub grdGrid_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              Y As Single)
  Dim blnActive As Boolean
  
  mintColumn = grdGrid.ColContaining(x, Y)
  If mintColumn > -1 Then
    blnActive = (grdGrid.Columns(mintColumn).style <> ssStyleCheckBox)
    lblColumn.Caption = grdGrid.Columns(mintColumn).Caption
  End If
  txtSearch.Enabled = blnActive
  txtSearch.BackColor = IIf(blnActive, vbWindowBackground, vbButtonFace)
  cboSearch.Enabled = blnActive
  cboSearch.BackColor = IIf(blnActive, vbWindowBackground, vbButtonFace)
  tlbToolbar1.Buttons(9).Enabled = blnActive
  tlbToolbar1.Buttons(10).Enabled = blnActive
  lblColumn.Visible = blnActive
  lblSearch.Visible = blnActive
  mlngRow = grdGrid.AddItemRowIndex(grdGrid.Bookmark)
End Sub

Private Sub OnlyExit()
  Call Me.Hide
End Sub

Private Sub SelectAndExit()
  Dim vntSelected As Variant
  Dim objElement  As Object
  Dim intColumn   As Integer
  Dim objColumn   As Object
  
  On Error GoTo cwIntError
 
  With grdGrid
    .Redraw = False
    If .SelBookmarks.Count > 0 Then
      For Each vntSelected In .SelBookmarks
        .Bookmark = vntSelected
        For Each objElement In cllColumns
          intColumn = 0
          For Each objColumn In .Columns
            If UCase(objColumn.Name) = UCase(objElement.strCaption) Then
              intColumn = .ColPosition(objColumn.Position)
              Exit For
            End If
          Next
          Call objElement.cllValues.Add(.Columns(intColumn).Text)
        Next
      Next
    Else
      For Each objElement In cllColumns
        intColumn = 0
        For Each objColumn In .Columns
          If UCase(objColumn.Name) = UCase(objElement.strCaption) Then
            intColumn = .ColPosition(objColumn.Position)
            Exit For
          End If
        Next
        Call objElement.cllValues.Add(.Columns(intColumn).Text)
      Next
    End If
  End With
  blnSelected = True
  Call Me.Hide
  Exit Sub

cwIntError:
  Call objError.InternalError(Me, "AcceptClick")
End Sub

Private Sub MoveReg(ByVal intMove As cwMove)
  Dim intCol As Integer
  
  Call objMouse.BusyOn
  intCol = grdGrid.Col
  Select Case intMove
    Case cwMoveFirst
      Call grdGrid.MoveFirst
    Case cwMovePrevious
      Call grdGrid.MovePrevious
    Case cwMoveNext
      Call grdGrid.MoveNext
    Case cwMoveLast
      Call grdGrid.MoveLast
  End Select
  grdGrid.Col = intCol
  Call grdGrid.DoClick
  Call objMouse.BusyOff
End Sub

Private Sub SearchReg(ByVal blnFirst As Boolean)
  Dim intCol    As Integer
  Dim lngRow    As Long
  Dim strText   As String
  Dim strValue  As String
  Dim objColumn As Column
  Dim blnFound  As Boolean
  Dim intMode   As Integer
  Dim intLen    As Integer
  
  strText = Trim(UCase(txtSearch))
  If Not objGen.IsStrEmpty(strText) Then
    With grdGrid
      intCol = .Col
      Call objMouse.BusyOn
      If blnFirst Then
        lngRow = 0
      Else
        lngRow = .AddItemRowIndex(.Bookmark) + 1
      End If
      Set objColumn = .Columns(mintColumn)
      intMode = cboSearch.ListIndex
      intLen = Len(strText)
      Do While lngRow < .Rows
        strValue = objColumn.CellText(grdGrid.AddItemBookmark(lngRow))
        Select Case intMode
          Case 0
            blnFound = (Left(UCase(strValue), intLen) = strText)
          Case 1
            blnFound = (InStr(UCase(strValue), strText) > 0)
          Case 2
            blnFound = (UCase(strValue) = strText)
          Case 3
            blnFound = (InStr(UCase(strValue), strText) = 0)
          Case 4
            blnFound = (UCase(strValue) <> strText)
        End Select
        If blnFound Then
          .Bookmark = grdGrid.AddItemBookmark(lngRow)
          Exit Do
        Else
          lngRow = lngRow + 1
        End If
      Loop
      Set objColumn = Nothing
      Call objMouse.BusyOff
      If Not blnFound Then
        With objError
          Call .SetError(cwCodeMsg, msgUsrSearchNotFound)
          Call .Raise
        End With
      End If
      .Col = intCol
      Call .DoClick
    End With
  End If
End Sub

Public Function Raise() As Boolean
  Dim objColumn    As Object
  Dim objForm      As clsCWForm
  Dim objCtrl      As clsCWCtrl
  Dim blnVisible   As Boolean
  Dim strCaption   As String
  Dim intType      As Integer
  Dim intSize      As Integer
  Dim rdoCursor    As rdoResultset
  Dim rdoColumna   As rdoColumn
  Dim lngInd       As Long
  Dim strBuffer    As String
  Dim strDelim     As String
  Dim strSepar     As String
  Dim strColumn    As String
  Dim intKeyNo     As Integer
  Dim blnMandatory As Boolean
  Dim lngColor     As Long
  Dim blnReadOnly  As Boolean
  Dim intMouse     As Integer
  
  On Error GoTo cwIntError

  Call objMouse.BusyOn

  Set rdoCursor = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly, rdAsyncEnable)
  
  Call objGen.WaitCursor(rdoCursor, True)
  
  If rdoCursor.RowCount > 0 Then
    Me.Left = IIf(mintLeft <> -1, mintLeft, Me.Left)
    Me.Top = IIf(mintTop <> -1, mintTop, Me.Top)
    Me.Width = IIf(mintWidth <> -1, mintWidth, Me.Width)
    Me.Height = IIf(mintHeight <> -1, mintHeight, Me.Height)
    
    Select Case intSelectType
      Case cwSingleSelect
        grdGrid.SelectTypeRow = ssSelectionTypeSingleSelect
      Case cwMultiSelect
        grdGrid.SelectTypeRow = ssSelectionTypeMultiSelectRange
     Case cwNoneSelect
        grdGrid.SelectTypeRow = ssSelectionTypeNone
    End Select
      
    grdGrid.Height = Me.ScaleHeight - tlbToolbar1.Height - picDummy.Height
      
    optDatos(10).Enabled = (intSelectType <> cwNoneSelect)
      
    tlbToolbar1.ImageList = objApp.imlImageList
    With tlbToolbar1.Buttons
      Call .Add(, , , tbrSeparator)
      .Add(, cwToolBarButtonFirst, , tbrDefault, cwImageFirst).ToolTipText = msgToolTipFirst
      .Add(, cwToolBarButtonPrevious, , tbrDefault, cwImagePrevious).ToolTipText = msgToolTipPrevious
      .Add(, cwToolBarButtonNext, , tbrDefault, cwImageNext).ToolTipText = msgToolTipNext
      .Add(, cwToolBarButtonLast, , tbrDefault, cwImageLast).ToolTipText = msgToolTipLast
      Call .Add(, , , tbrSeparator)
      .Add(, , , tbrDefault, cwImageZoom).ToolTipText = strZoom
      Call .Add(, , , tbrSeparator)
      .Add(, , , tbrDefault, cwImageFind).ToolTipText = msgToolTipSearchFirst
      .Add(, , , tbrDefault, cwImageFindNext).ToolTipText = msgToolTipSearchNext
      .Add(, , , tbrPlaceholder).Width = 4000
      With .Add(, , , tbrDefault, cwImageExecute)
        .ToolTipText = msgToolTipSelect
        .Enabled = (intSelectType <> cwNoneSelect)
      End With
      Call .Add(, , , tbrSeparator)
      .Add(, , , tbrDefault, cwImageExit).ToolTipText = msgToolTipExit
    End With
    
    With cboSearch
      Call .AddItem(msgFilterComienza)
      Call .AddItem(msgFilterContiene)
      Call .AddItem(msgFilterIgual)
      Call .AddItem(msgFilterNoContiene)
      Call .AddItem(msgFilterDistinto)
      .ListIndex = 0
    End With
  
    lblSearch.Visible = False
    lblColumn.Visible = False
    txtSearch.Enabled = False
    txtSearch.BackColor = vbButtonFace
    cboSearch.Enabled = False
    cboSearch.BackColor = vbButtonFace
    tlbToolbar1.Buttons(9).Enabled = False
    tlbToolbar1.Buttons(10).Enabled = False
  
    If Not objZoom Is Nothing Then
      optRegistro(60).Caption = strZoom
      tlbToolbar1.Buttons(7).Enabled = True
    Else
      tlbToolbar1.Buttons(7).Enabled = False
      optRegistro(60).Visible = False
      optRegistro(70).Visible = False
    End If
    
    If blnSearchCaptions Then
      Set objForm = objApp.objActiveWin.objWinActiveForm
    End If
    
    For lngInd = 0 To rdoCursor.rdoColumns.Count - 1
      Call grdGrid.Columns.Add(lngInd)
      
      Set objColumn = grdGrid.Columns(lngInd)
      
      blnVisible = True
      strCaption = rdoCursor.rdoColumns(lngInd).Name
      intType = cwString
      intSize = 10
      intKeyNo = 0
      blnMandatory = False
      blnReadOnly = False
    
      If blnSearchCaptions Then
        For Each objCtrl In objForm.cllControls
          If UCase(objApp.objActiveWin.CtrlGetField(objCtrl.objControl)) = UCase(strCaption) Then
            With objCtrl
              blnVisible = .blnInGrid And objApp.objActiveWin.CtrlAllowView(.objControl)
              strCaption = .strSmallDesc
              intSize = .intSize
              intType = .intType
              intKeyNo = .intKeyNo
              blnMandatory = .blnMandatory
              blnReadOnly = objApp.objActiveWin.CtrlIsReadOnly(.objControl)
            End With
            Exit For
          End If
        Next
      Else
        strColumn = rdoCursor.rdoColumns(lngInd).SourceColumn
        If Not cllFields Is Nothing Then
          strCaption = cllFields(strColumn).strSmallDesc
          intSize = cllFields(strColumn).intSize
          intType = cllFields(strColumn).intType
          blnVisible = cllFields(strColumn).blnInGrid
          intKeyNo = cllFields(strColumn).intKeyNo
          blnMandatory = cllFields(strColumn).blnMandatory
          blnReadOnly = cllFields(strColumn).blnReadOnly
        End If
      End If
      With objColumn
        .Locked = True
        .Visible = blnVisible
        .Name = rdoCursor.rdoColumns(lngInd).Name
        If blnVisible Then
          .Caption = strCaption
          .Width = objGen.GetMax(intSize, Len(strCaption)) * cwCharWidth
          Select Case intType
            Case cwBoolean
             .style = ssStyleCheckBox
            Case Else
          End Select
          If intKeyNo > 0 Then
            lngColor = objApp.objUserColor.lngKey
          ElseIf blnReadOnly Then
            lngColor = objApp.objUserColor.lngReadOnly
          ElseIf blnMandatory Then
            lngColor = objApp.objUserColor.lngMandatory
          Else
            lngColor = objApp.objUserColor.lngNormal
          End If
          .BackColor = lngColor
        End If
      End With
    Next
    
    If blnSearchCaptions Then
      Set objForm = Nothing
    End If
      
    Call objGen.WaitCursor(rdoCursor, False)
      
    If rdoCursor.RowCount >= lngMaxSize - 1 Then
      Call objMouse.BusyOff
      With objError
        Call .SetError(cwCodeMsg, msgUsrViewMaxSizeII, _
                       Format(lngMaxSize, cwMaskNumber))
        Call .Raise
      End With
    Else
      grdGrid.Redraw = False
      
      strDelim = objGen.GetDelSep(grdGrid, True)
      strSepar = objGen.GetDelSep(grdGrid, False)
      Do While Not rdoCursor.EOF
        strBuffer = ""
        For Each rdoColumna In rdoCursor.rdoColumns
          strBuffer = strBuffer & _
                      strDelim & rdoColumna.Value & strDelim & strSepar
        Next
        strBuffer = Left(strBuffer, Len(strBuffer) - Len(strSepar))
        Call grdGrid.AddItem(strBuffer)
        Call rdoCursor.MoveNext
      Loop
      
      grdGrid.Redraw = True
      Me.Caption = Me.Caption & " - " & Format(rdoCursor.RowCount, cwMaskNumber) & msgRegistros
      
      Call objMouse.BusyOff
    
      intMouse = Screen.MousePointer
      Screen.MousePointer = vbArrow
      Call Me.Show(vbModal)
      Screen.MousePointer = intMouse
      
      Raise = blnSelected
    End If
  Else
    Call objMouse.BusyOff

    With objError
      Call .SetError(cwCodeMsg, msgUsrFilterNotFound)
      Call .Raise
    End With
  End If
  
  Call rdoCursor.Close
  Set rdoCursor = Nothing
  
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "Raise", strSQL)
End Function

Private Sub ProcessZoom()
  Dim cllValues As New Collection
  Dim objColumn As Column
  
  For Each objColumn In grdGrid.Columns
    Call cllValues.Add(objColumn.Value, objColumn.Name)
  Next
  
  If Not (objZoom Is Nothing) Then
    Call objZoom.ZoomData(cllValues)
  End If
  
  Call objGen.RemoveCollection(cllValues)
End Sub
