VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWMouse"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWMouse
' Coded by SYSECA Bilbao
' **********************************************************************************


Dim mcllPointer As New Collection


Public Sub BusyOn()
  Dim intPointer As Integer

  intPointer = Screen.MousePointer
  If mcllPointer.Count = 0 Then
    Screen.MousePointer = vbHourglass
  End If
  Call mcllPointer.Add(intPointer)
  objCW.SetClockEnable (True)
End Sub

Public Sub BusyOff()
  Dim intPointer As Integer

  intPointer = mcllPointer(mcllPointer.Count)
  Call mcllPointer.Remove(mcllPointer.Count)
  If mcllPointer.Count = 0 Then
    Screen.MousePointer = intPointer
  End If
  objCW.SetClockEnable (True)
End Sub
