VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmCWGetDate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " "
   ClientHeight    =   2625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4650
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "CWFRM16.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2625
   ScaleWidth      =   4650
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   3015
      TabIndex        =   2
      Top             =   540
      Width           =   1545
   End
   Begin VB.CommandButton cmdAccept 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   3015
      TabIndex        =   1
      Top             =   45
      Width           =   1545
   End
   Begin SSCalendarWidgets_A.SSMonth moncSSMonth 
      Height          =   2535
      Left            =   45
      TabIndex        =   0
      Top             =   45
      Width           =   2895
      _Version        =   65537
      _ExtentX        =   5106
      _ExtentY        =   4471
      _StockProps     =   76
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      StartofWeek     =   2
      ShowSelectedDate=   0   'False
      ShowCentury     =   -1  'True
   End
End
Attribute VB_Name = "frmCWGetDate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWGetDate
' Coded by SYSECA Bilbao
' **********************************************************************************


Public blnAccept As Boolean


Private Sub cmdAccept_Click()
  blnAccept = True
  Call Me.Hide
End Sub

Private Sub cmdCancel_Click()
  blnAccept = False
  Call Me.Hide
End Sub

Public Sub Prepare(ByVal strDate As String)
  Dim datDate As Date
  
  If IsDate(strDate) Then
    datDate = CDate(strDate)
    If datDate >= moncSSMonth.MinDate And datDate <= moncSSMonth.MaxDate Then
      moncSSMonth.Date = datDate
    End If
  End If
End Sub

Private Sub Form_Load()
  ' forzar cambio
  moncSSMonth.Date = moncSSMonth.Date
End Sub

Private Sub moncSSMonth_DblClick()
  cmdAccept.Value = True
End Sub

Private Sub moncSSMonth_FocusChange(FocusDate As String, OldFocusDate As String, MonthNum As Integer, YearNum As Integer, DayNum As Integer)
  moncSSMonth.Date = FocusDate
End Sub

Private Sub moncSSMonth_SelChanged(SelDate As String, OldSelDate As String, Selected As Integer)
  Me.Caption = Format(SelDate, "D \d\e MMMM \d\e YYYY")
End Sub
