VERSION 5.00
Begin VB.Form frmCWEncript 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGURIDAD. Algoritmo de Encriptación"
   ClientHeight    =   1785
   ClientLeft      =   2835
   ClientTop       =   3480
   ClientWidth     =   4740
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   86
   Icon            =   "SGFRM04.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1054.637
   ScaleMode       =   0  'User
   ScaleWidth      =   4450.603
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtSource 
      Height          =   345
      IMEMode         =   3  'DISABLE
      Left            =   1575
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   180
      Width           =   3000
   End
   Begin VB.TextBox txtTarget 
      BackColor       =   &H8000000F&
      Height          =   345
      IMEMode         =   3  'DISABLE
      Left            =   1575
      Locked          =   -1  'True
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   630
      Width           =   3000
   End
   Begin VB.CommandButton cmdAccept 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   860
      TabIndex        =   2
      Top             =   1260
      Width           =   1400
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2480
      TabIndex        =   3
      Top             =   1260
      Width           =   1400
   End
   Begin VB.Label lblLabels 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Texto a Encriptar"
      Height          =   195
      Index           =   1
      Left            =   165
      TabIndex        =   5
      Top             =   225
      Width           =   1215
   End
   Begin VB.Label lblLabels 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Texto Encriptado"
      Height          =   195
      Index           =   2
      Left            =   165
      TabIndex        =   4
      Top             =   720
      Width           =   1215
   End
End
Attribute VB_Name = "frmCWEncript"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWEncript
' Coded by Manu Roibal
' **********************************************************************************


Private Sub Form_Load()
  Call EnabledOk
End Sub

Private Sub cmdAccept_Click()
  Dim objEncript As New clsCWMRES
  
  txtTarget = objEncript.Encript(txtSource)
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub txtSource_Change()
  Call EnabledOk
End Sub

Private Sub EnabledOk()
  cmdAccept.Enabled = Not objGen.IsStrEmpty(txtSource)
End Sub

