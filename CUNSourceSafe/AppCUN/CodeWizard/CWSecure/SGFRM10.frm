VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmCWPurgeDelega 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGURIDAD. Eliminaci�n de Delegaciones"
   ClientHeight    =   1560
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4575
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   53
   Icon            =   "SGFRM10.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1560
   ScaleWidth      =   4575
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo 
      Height          =   375
      Left            =   90
      TabIndex        =   0
      Top             =   630
      Width           =   2535
      _Version        =   65537
      _ExtentX        =   4471
      _ExtentY        =   661
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      ShowCentury     =   -1  'True
      StartofWeek     =   2
   End
   Begin VB.CommandButton cmdAccept 
      Caption         =   "&Borrar"
      Default         =   -1  'True
      Height          =   375
      Left            =   3015
      TabIndex        =   1
      Top             =   135
      Width           =   1400
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3015
      TabIndex        =   2
      Top             =   630
      Width           =   1400
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Si la fecha se deja en blanco se borran todas las delegaciones"
      Height          =   195
      Index           =   0
      Left            =   45
      TabIndex        =   4
      Top             =   1260
      Width           =   4425
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Borrar todas las delegaciones cuya fecha de vigencia es anterior a..."
      Height          =   390
      Index           =   2
      Left            =   90
      TabIndex        =   3
      Top             =   135
      Width           =   2610
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmCWPurgeDelega"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWPurgeDelega
' Coded by Manu Roibal
' **********************************************************************************


Const cwSpyMsgDeleteAll       As String = "�Est� seguro de que desea borrar todas las Delegaciones? (%1 Registros)"
Const cwSpyMsgDeleteSelective As String = "�Est� seguro de que desea borrar las Delegaciones anteriores al %1? (%2 Registros)"
Const cwSpyMsgDeleteError     As String = "Se ha producido un error al borrar las Delegaciones" & vbCrLf & vbCrLf & "%1 - %2"
Const cwSpyMsgDeleteOk        As String = "Las Delegaciones se borraron con �xito"
Const cwSpyMsgDeleteNoRegs    As String = "No hay ninguna Delegaci�n para borrar"


Private Sub cmdAccept_Click()
  Dim strSQL    As String
  Dim strWhere  As String
  Dim rdoCursor As rdoResultset
  Dim lngRegs   As Long
  
  If Not objGen.IsStrEmpty(dtcDateCombo.Text) Then
    strWhere = "WHERE sg05fecfin < " & objGen.ValueToSQL(dtcDateCombo.Text, cwDate)
  End If
  
  strSQL = "SELECT Count(*) FROM sg0500 " & strWhere
  Set rdoCursor = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
  lngRegs = rdoCursor(0).Value
  Call rdoCursor.Close
  Set rdoCursor = Nothing
  
  If lngRegs = 0 Then
    Call objError.SetError(cwCodeMsg, cwSpyMsgDeleteNoRegs)
    Call objError.Raise
  Else
    If Not objGen.IsStrEmpty(dtcDateCombo.Text) And IsDate(dtcDateCombo.Text) Then
      Call objError.SetError(cwCodeQuery, cwSpyMsgDeleteSelective, Format(dtcDateCombo.Date, "dddd d \d\e mmmm \d\e yyyy"), lngRegs)
    Else
      Call objError.SetError(cwCodeQuery, cwSpyMsgDeleteAll, lngRegs)
    End If
    
    If (objError.Raise = vbYes) Then
      strSQL = "DELETE FROM sg0500 " & strWhere
  
      On Error Resume Next
      Call objApp.rdoConnect.Execute(strSQL)
      If Err.Number > 0 Then
        Call objError.SetError(cwCodeMsg, cwSpyMsgDeleteError, Err.Number, Err.Description)
        Call objError.Raise
      Else
        Call objError.SetError(cwCodeMsg, cwSpyMsgDeleteOk)
        Call objError.Raise
        Unload Me
      End If
    End If
  End If
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

