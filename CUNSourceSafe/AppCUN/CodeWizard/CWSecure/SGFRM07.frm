VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmCWPrnTabCol 
   Caption         =   "SEGURIDAD. Tablas y Columnas para Informes Externos"
   ClientHeight    =   7215
   ClientLeft      =   735
   ClientTop       =   2355
   ClientWidth     =   11415
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   38
   Icon            =   "SGFRM07.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7215
   ScaleWidth      =   11415
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   741
      ButtonWidth     =   635
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   327682
   End
   Begin SSDataWidgets_B.SSDBGrid grdGrid 
      Height          =   4515
      Left            =   4410
      TabIndex        =   1
      Top             =   630
      Width           =   6765
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   0
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   0
      BackColorEven   =   -2147483643
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      _ExtentX        =   11933
      _ExtentY        =   7964
      _StockProps     =   79
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picSplitter 
      BorderStyle     =   0  'None
      Height          =   4290
      Left            =   4005
      MousePointer    =   9  'Size W E
      ScaleHeight     =   4290
      ScaleWidth      =   45
      TabIndex        =   2
      Top             =   675
      Width           =   50
   End
   Begin ComctlLib.TreeView tvwTables 
      Height          =   4485
      Left            =   45
      TabIndex        =   0
      Top             =   675
      Width           =   3660
      _ExtentX        =   6456
      _ExtentY        =   7911
      _Version        =   327682
      HideSelection   =   0   'False
      Indentation     =   353
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin ComctlLib.StatusBar stbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   6930
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Datos"
      Begin VB.Menu optData 
         Caption         =   "&Guardar"
         Index           =   10
         Shortcut        =   ^G
      End
      Begin VB.Menu optData 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu optData 
         Caption         =   "&Imprimir"
         Index           =   30
         Shortcut        =   ^P
      End
      Begin VB.Menu optData 
         Caption         =   "-"
         Index           =   40
      End
      Begin VB.Menu optData 
         Caption         =   "&Salir"
         Index           =   50
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edici�n"
      Begin VB.Menu optEdit 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu optEdit 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu optEdit 
         Caption         =   "&Marcar todos"
         Index           =   30
         Shortcut        =   ^E
      End
      Begin VB.Menu optEdit 
         Caption         =   "&Desmarcar todos"
         Index           =   40
         Shortcut        =   ^D
      End
   End
   Begin VB.Menu mnuRegister 
      Caption         =   "&Registro"
      Begin VB.Menu optRegister 
         Caption         =   "&Primero"
         Index           =   10
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Anterior"
         Index           =   20
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Siguiente"
         Index           =   30
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Ultimo"
         Index           =   40
      End
      Begin VB.Menu optRegister 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu optRegister 
         Caption         =   "Lista de &Valores"
         Index           =   60
      End
      Begin VB.Menu optRegister 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Refrescar Registros"
         Index           =   80
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&?"
      Begin VB.Menu optHelp 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu optHelp 
         Caption         =   "&Acerca de..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmCWPrnTabCol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWPrnTabCol
' Coded by Manu Roibal
' **********************************************************************************


Dim WithEvents objTree As clsCWTree
Attribute objTree.VB_VarHelpID = -1


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objSource As New clsCWTreeSource
  Dim objLevel  As New clsCWTreeLevel
  
  On Error GoTo cwIntError
  
  Call objApp.SplashOn
  
  Set objTree = New clsCWTree
  
  With objTree
    Set .frmForm = Me
    Set .grdGrid = grdGrid
    Set .picSplitter = picSplitter
    Set .tvwTables = tvwTables
    Set .tlbToolbar = tlbToolbar
    Set .stbStatusBar = stbStatusBar
  
  
    ' datos del nivel 1
    With objLevel
      .intType = cwTreeLevelTypeNothing
      .strText = "COLUMNAS"
      Set .objSource = Nothing
    End With
    Call objTree.TreeAddLevel(objLevel)
    Set objLevel = Nothing
    
    ' datos del nivel 2
    With objLevel
      .intType = cwTreeLevelTypeSQL
      .strText = "SELECT sg00id, sg00des FROM sg0000 ORDER BY sg00des"
      Set .objSource = Nothing
    End With
    Call objTree.TreeAddLevel(objLevel)
    Set objLevel = Nothing

    ' datos del nivel 3
    With objLevel
      .intType = cwTreeLevelTypeTables
      .strText = ""
      Set .objSource = Nothing
    End With
    Call objTree.TreeAddLevel(objLevel)
    Set objLevel = Nothing
    
    ' datos del nivel 4
    With objSource
      .strCrossSQL = "SELECT sg04cod, sg09cod, sg10cod FROM sg1500 WHERE sg09cod = ? AND sg10cod = ?"
      .strCrossTable = "SG1500"
      .strText = "sg0400"
      .intType = cwTreeSourceTypeSQL
      .strWhere = "WHERE sg17cod = " & cwTypeExternalReport
      Call .AddField("sg04cod", "", cwVariant, False, True, True, 0, "")
      Call .AddField("sg04des", "", cwVariant, False, True, True, 0, "")
      Call .AddField("", "Asignar", cwBoolean, False, False, True, 0, "")
      Call .CheckPositions(3)
      Call .LoadDictionary
    End With
    With objLevel
      .intType = cwTreeLevelTypeColumns
      .strText = ""
      Set .objSource = objSource
    End With
    Call objTree.TreeAddLevel(objLevel)
    Set objSource = Nothing
    Set objLevel = Nothing
    
    Call objTree.TreeCreateInfo
  End With
  
cwResError:
  Call objApp.SplashOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "Load")
  Resume cwResError
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Cancel = objTree.treeExit
End Sub

Private Sub Form_Resize()
  Call objTree.treeResize
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de clsCWTree
' -----------------------------------------------
Private Sub objTree_cwQueryUpdateRow(ByVal intLevel As Integer, ByVal cllGridValues As Collection, blnSave As Boolean, blnCancel As Boolean)
  If intLevel = 4 Then
    blnSave = (cllGridValues(3) <> 0)
  End If
End Sub

Private Sub objTree_cwReadCross(ByVal intLevel As Integer, aParameters() As String, ByVal cllParents As Collection)
  If intLevel = 4 Then
    aParameters(1) = cllParents(2)
    aParameters(2) = cllParents(1)
  End If
End Sub

Private Sub objTree_cwReadRow(ByVal intLevel As Integer, aGridValues() As String, ByVal cllCross As Collection, ByVal cllCursor As Collection, ByVal blnExist As Boolean)
  If intLevel = 4 Then
    aGridValues(1) = cllCursor(1)
    aGridValues(2) = cllCursor(2)
    aGridValues(3) = IIf(blnExist, -1, 0)
  End If
End Sub

Private Sub objTree_cwUpdateRow(ByVal intLevel As Integer, ByVal cllGridValues As Collection, ByVal cllParents As Collection, rdoCursor As RDO.rdoResultset)
  If intLevel = 4 Then
    rdoCursor(0) = cllGridValues(1)
    rdoCursor(1) = cllParents(2)
    rdoCursor(2) = cllParents(1)
  End If
End Sub

Private Sub objTree_cwQueryReadLevel(ByVal intLevel As Integer, ByVal strValue As String, ByVal cllParents As Collection, blnCancel As Boolean)
  If intLevel = 3 Then
    If Not (strValue Like (cllParents(1) & "####")) Then
      blnCancel = True
    End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Grid
' -----------------------------------------------
Private Sub grdGrid_Change()
  Call objTree.treeDataChanged
End Sub

Private Sub grdGrid_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
  Call objTree.treeRowColChange(LastRow, LastCol)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del men�
' -----------------------------------------------
Private Sub optData_Click(intIndex As Integer)
  Call objTree.treeProcess(cwTreeProcessData, intIndex)
End Sub

Private Sub optEdit_Click(intIndex As Integer)
  Call objTree.treeProcess(cwTreeProcessEdit, intIndex)
End Sub

Private Sub optHelp_Click(intIndex As Integer)
  Call objTree.treeProcess(cwTreeProcessHelp, intIndex)
End Sub

Private Sub optRegister_Click(intIndex As Integer)
  Call objTree.treeProcess(cwTreeProcessRegister, intIndex)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Splitter
' -----------------------------------------------
Private Sub picSplitter_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objTree.treeMoving(cwTreeMouseDown, Button, Shift, X, Y)
End Sub

Private Sub picSplitter_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objTree.treeMoving(cwTreeMouseUp, Button, Shift, X, Y)
End Sub

Private Sub picSplitter_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objTree.treeMoving(cwTreeMouseMove, Button, Shift, X, Y)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la ToolBar
' -----------------------------------------------
Private Sub tlbtoolbar_ButtonClick(ByVal btnButton As ComctlLib.Button)
  Call objTree.treeProcess(cwTreeProcessToolBar, btnButton.Index)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la StatusBar
' -----------------------------------------------
Private Sub stbStatusBar_PanelDblClick(ByVal panPanel As ComctlLib.Panel)
  Call objTree.treeProcess(cwTreeProcessStatusBar, panPanel.Index)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del TreeView
' -----------------------------------------------
Private Sub tvwTables_Collapse(ByVal nodNode As ComctlLib.Node)
  Call objTree.treeSelected(cwTreeCollapse, nodNode)
End Sub

Private Sub tvwTables_Expand(ByVal nodNode As ComctlLib.Node)
  Call objTree.treeSelected(cwTreeExpand, nodNode)
End Sub

Private Sub tvwTables_NodeClick(ByVal nodNode As ComctlLib.Node)
  Call objTree.treeSelected(cwTreeSelect, nodNode)
End Sub

