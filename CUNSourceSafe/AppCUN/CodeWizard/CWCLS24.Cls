VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLinked"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWLinked
' Coded by SYSECA Bilbao
' **********************************************************************************


Public blnHasLinked      As Boolean
Public strLinkedColumn   As String               ' almacena la columna relactionada con la FK
Public cllLinkedControls As New Collection       ' almacena la colecci�n de controles asociados
Public cllLinkedColumns  As New Collection       ' almacena la colecci�n de columnas asociadas
Public rdoLinkedQuery    As rdoQuery             ' una query para las b�squedas
Public rdoLinkedCursor   As rdoResultset         ' el cursor de datos
Public strDesColumn      As String               ' Salva: columna-descripci�n para el grid
Public blnDesInGrid      As Boolean              ' Salva: para que no aparezca la descripci�n en el grid
Public blnInFind         As Boolean              ' Salva: para introducir la descripci�n en la b�squeda

