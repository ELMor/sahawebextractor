VERSION 5.00
Begin VB.Form frmCWPrinter 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Seleccionar Informe o Listado"
   ClientHeight    =   2640
   ClientLeft      =   705
   ClientTop       =   2070
   ClientWidth     =   7365
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "CWFRM09.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2640
   ScaleWidth      =   7365
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSelect 
      Caption         =   "Seleccionar..."
      Height          =   375
      Left            =   5715
      TabIndex        =   8
      Top             =   630
      Width           =   1545
   End
   Begin VB.ComboBox cboDestination 
      Height          =   315
      ItemData        =   "CWFRM09.frx":000C
      Left            =   3690
      List            =   "CWFRM09.frx":0016
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   360
      Width           =   1770
   End
   Begin VB.TextBox txtCopies 
      Height          =   330
      Left            =   3690
      MaxLength       =   3
      TabIndex        =   3
      Top             =   1080
      Width           =   555
   End
   Begin VB.ListBox lstReports 
      Height          =   2400
      Left            =   90
      TabIndex        =   1
      Top             =   135
      Width           =   3435
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   5715
      TabIndex        =   7
      Top             =   1665
      Width           =   1545
   End
   Begin VB.CommandButton cmdSetup 
      Caption         =   "C&onfiguraci�n..."
      Height          =   375
      Left            =   5715
      TabIndex        =   6
      Top             =   1125
      Width           =   1545
   End
   Begin VB.CommandButton cmdAccept 
      Caption         =   "&Imprimir"
      Default         =   -1  'True
      Height          =   375
      Left            =   5715
      TabIndex        =   5
      Top             =   135
      Width           =   1545
   End
   Begin VB.Label lblDestination 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Destino"
      Height          =   195
      Left            =   3690
      TabIndex        =   4
      Top             =   135
      Width           =   540
   End
   Begin VB.Label lblCopies 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "N�mero de copias"
      Height          =   195
      Left            =   3690
      TabIndex        =   0
      Top             =   855
      Width           =   1290
   End
End
Attribute VB_Name = "frmCWPrinter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWPrinter
' Coded by SYSECA Bilbao
' **********************************************************************************


Const cwMsgPrintSelectError As String = "Se ha realizado una selecci�n que puede no ser v�lida para el informe seleccionado. �Desea continuar?"


Dim mcllNames      As New Collection
Dim mobjPrinter    As clsCWPrinter
Dim mstrSelectName As String
Dim mstrFilterName As String


Private Sub Form_Load()
  cmdAccept.Enabled = False
  cmdSelect.Enabled = False
  txtCopies.Enabled = False
  lblCopies.Enabled = False
  cboDestination.Enabled = False
  lblDestination.Enabled = False
End Sub

Public Sub LoadReports(ByVal objPrinter As clsCWPrinter)
  Dim lngInd As Long
  Dim intPos As Integer
  
  On Error GoTo cwIntError
  
  Set mobjPrinter = objPrinter
  
  With mobjPrinter
    For lngInd = 1 To .Count
      If .Enabled(lngInd) Then
        Call lstReports.AddItem(.Description(lngInd))
        Call mcllNames.Add(.Name(lngInd))
        If lngInd = .Selected Then
          intPos = mcllNames.Count
        End If
      End If
    Next
    On Error Resume Next
    lstReports.ListIndex = intPos - 1
    .Selected = 0
  End With
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "LoadReports")
End Sub

Private Sub lstReports_Click()
  Dim intPos As Integer
  
  With mobjPrinter
    intPos = .GetPos(mcllNames(lstReports.ListIndex + 1))
    txtCopies = .Copies(intPos)
    cboDestination.ListIndex = .Destination(intPos)
    mstrSelectName = .Name(intPos)
  End With
End Sub

Private Sub lstReports_DblClick()
  cmdAccept.Value = True
End Sub

Private Sub cmdAccept_Click()
  Dim blnOk   As Boolean
  Dim strName As String
  
  strName = mcllNames(lstReports.ListIndex + 1)
  If objSecurity.HasProcess(strName) Then
    If mstrSelectName <> mstrFilterName And Not objGen.IsStrEmpty(mstrFilterName) Then
      Call objError.SetError(cwCodeQuery, cwMsgPrintSelectError)
      blnOk = (objError.Raise = vbYes)
    Else
      blnOk = True
    End If
 
    If blnOk Then
      With mobjPrinter
        .Selected = .GetPos(strName)
        Call .SetValues(CInt(txtCopies), cboDestination.ListIndex)
      End With
      Call Hide
    End If
  End If
End Sub

Private Sub cmdSelect_Click()
  Dim frmFilter As New frmCWFilter
  
  On Error GoTo cwIntError
  
  Load frmFilter
  
  With frmFilter
    Set .objFilter = mobjPrinter.objFilter
    .blnInternal = False
    Call .Prepare
    Call .Show(vbModal)
    If .blnCancel Then
      mstrFilterName = ""
    Else
      With mobjPrinter
        mstrFilterName = .Name(.GetPos(mcllNames(lstReports.ListIndex + 1)))
      End With
    End If
  End With
  
  Unload frmFilter
  Set frmFilter = Nothing
  DoEvents
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "SelectClick")
End Sub

Private Sub cmdSetup_Click()
  objApp.dlgCommon.Flags = cdlPDHidePrintToFile Or cdlPDNoPageNums Or _
                           cdlPDNoSelection Or cdlPDPrintSetup
  Call objApp.dlgCommon.ShowPrinter
End Sub

Private Sub cmdCancel_Click()
  mobjPrinter.Selected = 0
  Call Hide
End Sub

Private Sub cboDestination_Click()
  cmdAccept.Enabled = True
  With mobjPrinter.objFilter
    cmdSelect.Enabled = (.cllFilterTables.Count > 0 Or .cllOrderBy.Count > 0)
  End With
  cboDestination.Enabled = True
  lblDestination.Enabled = True
  txtCopies.Enabled = (cboDestination.ListIndex <> crptToWindow)
  lblCopies.Enabled = (cboDestination.ListIndex <> crptToWindow)
End Sub

Private Sub txtCopies_KeyPress(intKeyAscii As Integer)
  If (intKeyAscii < vbKey0 Or intKeyAscii > vbKey9) And intKeyAscii <> vbKeyBack Then
    intKeyAscii = 0
  End If
End Sub
