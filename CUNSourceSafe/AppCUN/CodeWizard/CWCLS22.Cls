VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWCatalog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWCatalog
' Coded by SYSECA Bilbao
' **********************************************************************************
' ATENCI�N
' Ha habido que parchear el documento de declaraciones del
' API de ODBC de Microsoft cambiando las sentencias DECLARE
' de SQLPrimaryKeys, SQLTables y SQLColumns de la siguiente forma:
'
' los argumentos definidos mediante <x As Any> se han
' cambiado por <ByVal x As String> ya que un par�metro
' de tipo string siempre debe ser pasado por valor para
' forzar un puntero largo con conversi�n unicode-ansi
'
' Toma cantada de MS !!! Parece que no leen sus propios manuales
' He estado buscando el fallo durante 2 intensos d�as traceando
' las tripas del driver de ODBC... :-)
'
' Manu


Dim mcllSize      As New Collection
Dim mcllDecs      As New Collection
Dim mcllMandatory As New Collection
Dim mcllPrimary   As New Collection
Dim mcllType      As New Collection
Dim mcllRDOType   As New Collection
Dim mcllSmall     As New Collection
Dim mcllBig       As New Collection


Public strTableDesc As String


Friend Sub CreateInfo(ByVal strTable As String)
  Dim hStmt           As Long
  Dim lngLen          As Long
  Dim nRetCode        As Integer
  Dim strCursorColumn As String * 129
  Dim strDescription  As String * 255
  Dim strColumn       As String
  Dim intDataType     As Integer
  Dim intVBDataType   As Integer
  Dim intScale        As Integer
  Dim intNullable     As Integer
  Dim lngPrecision    As Long
  Dim lngLength       As Long
  Dim intPrimary      As Integer
  Dim intSize         As Integer
  Dim strSmall        As String
  Dim strBig          As String
  Dim intSpecialChar  As Integer
  Dim strBuffer       As String
  Dim strSchema       As String
  Dim rdoCursor       As rdoResultset
  Dim strSQL          As String
  Dim strDBMS         As String * 255
    
  On Error Resume Next
  
  ' Salva: Tratamos de coger el ususario de la tabla por si es un sin�nimo, ya que
  ' SQLPrimaryKeys falla con estos
  nRetCode = SQLGetInfoString(objApp.rdoConnect.hdbc, SQL_DBMS_NAME, strDBMS, 254, intSize)
  strSchema = StrConv(Mid$(strDBMS, 1, intSize), vbUpperCase)
    
  If strSchema = "ORACLE" Then
    strSQL = "SELECT TABLE_OWNER FROM USER_SYNONYMS WHERE SYNONYM_NAME='" & UCase(strTable) & "'"
    Set rdoCursor = objApp.rdoConnect.OpenResultset(strSQL, rdOpenStatic, rdConcurReadOnly)
    'Salva: Pasamos a querys. Set rdoCursor = objApp.rdoConnect.OpenResultset(strSQL, rdOpenStatic, rdConcurReadOnly)
    strSchema = rdoCursor.rdoColumns(0).Value
    rdoCursor.Close
  End If

  Call SQLAllocStmt(objApp.rdoConnect.hdbc, hStmt)
  nRetCode = SQLTables(hStmt, _
                       vbNullString, 0, _
                       vbNullString, 0, _
                       UCase(strTable), SQL_NTS, _
                       "TABLE", SQL_NTS)
  Do While nRetCode = SQL_SUCCESS
    nRetCode = SQLFetch(hStmt)
    If nRetCode = SQL_SUCCESS Then
      Call SQLGetData(hStmt, 5, SQL_C_CHAR, strDescription, 255, lngLen)
      If lngLen < 0 Then
        strTableDesc = ""
      Else
        strTableDesc = Left(strDescription, lngLen)
      End If
    End If
  Loop
  Call SQLFreeStmt(hStmt, SQL_CLOSE)
 
  Call SQLAllocStmt(objApp.rdoConnect.hdbc, hStmt)
  nRetCode = SQLColumns(hStmt, _
                        vbNullString, 0, _
                        vbNullString, 0, _
                        UCase(strTable), SQL_NTS, _
                        vbNullString, 0)
  Do While nRetCode = SQL_SUCCESS
    nRetCode = SQLFetch(hStmt)
    If nRetCode = SQL_SUCCESS Then
      Call SQLGetData(hStmt, 4, SQL_C_CHAR, strCursorColumn, 129, lngLen)
      strColumn = UCase(Left(strCursorColumn, lngLen))
      
      Call SQLGetNumericData(hStmt, 5, SQL_C_SSHORT, intDataType, 0, lngLen)
      Call SQLGetNumericData(hStmt, 7, SQL_C_SLONG, lngPrecision, 0, lngLen)
      Call SQLGetNumericData(hStmt, 8, SQL_C_SLONG, lngLength, 0, lngLen)
      'OSCAR: las fechas tienen 17 en vez de 16 caracteres de longitud
      If intDataType = SQL_TIMESTAMP Then lngLength = lngLength + 1
      
      Call SQLGetNumericData(hStmt, 9, SQL_C_SSHORT, intScale, 0, lngLen)
      
      Call mcllRDOType.Add(intDataType, strColumn)
      
      intVBDataType = objGen.Rdo2Vb(intDataType)
      Select Case intVBDataType
        Case cwNumeric, cwDecimal
          intVBDataType = IIf(intScale > 0 Or lngLength > 15, cwDecimal, cwNumeric)
          Call mcllSize.Add(lngPrecision + IIf(intScale > 0, 1, 0), strColumn)
          Call mcllDecs.Add(intScale, strColumn)
        Case Else
          Call mcllSize.Add(lngLength, strColumn)
          Call mcllDecs.Add(0, strColumn)
      End Select
      Call mcllType.Add(intVBDataType, strColumn)
      
      Call SQLGetNumericData(hStmt, 11, SQL_C_SSHORT, intNullable, 0, lngLen)
      Call mcllMandatory.Add(intNullable = 0, strColumn)
      
      Call SQLGetData(hStmt, 12, SQL_C_CHAR, strDescription, 255, lngLen)
      
      strSmall = ""
      strBig = ""
      If lngLen < 0 Then
        strBuffer = ""
      Else
        strBuffer = Left(strDescription, lngLen)
      End If
      intSpecialChar = InStr(strBuffer, msgCharDelimiter)
      If intSpecialChar > 0 Then
        strSmall = Mid(strBuffer, 1, intSpecialChar - 1)
        strBig = Mid(strBuffer, intSpecialChar + 1)
      Else
        strSmall = strBuffer
      End If
      strBig = IIf(objGen.IsStrEmpty(strBig), strSmall, strBig)
      
      Call mcllSmall.Add(strSmall, strColumn)
      Call mcllBig.Add(strBig, strColumn)
    
      Call mcllPrimary.Add(0, strColumn)
    End If
  Loop
  Call SQLFreeStmt(hStmt, SQL_CLOSE)
  
  Call SQLAllocStmt(objApp.rdoConnect.hdbc, hStmt)
  nRetCode = SQLPrimaryKeys(hStmt, _
                            vbNullString, 0, _
                            strSchema, LenB(strSchema), _
                            UCase(strTable), SQL_NTS)
  Do While nRetCode = SQL_SUCCESS
    nRetCode = SQLFetch(hStmt)
    If nRetCode = SQL_SUCCESS Then
      Call SQLGetData(hStmt, 4, SQL_C_CHAR, strCursorColumn, 129, lngLen)
      strColumn = UCase(Left(strCursorColumn, lngLen))
      Call SQLGetNumericData(hStmt, 5, SQL_C_SSHORT, intPrimary, 0, lngLen)
      Call mcllPrimary.Remove(strColumn)
      Call mcllPrimary.Add(intPrimary, strColumn)
    End If
  Loop
  Call SQLFreeStmt(hStmt, SQL_CLOSE)
End Sub

Private Sub Class_Terminate()
  On Error Resume Next
  With objGen
    Call .RemoveCollection(mcllType)
    Call .RemoveCollection(mcllSize)
    Call .RemoveCollection(mcllMandatory)
    Call .RemoveCollection(mcllPrimary)
  End With
End Sub

Friend Function Primary(ByVal strColumn As String) As Integer
  On Error Resume Next
  Primary = mcllPrimary(UCase(strColumn))
End Function

Friend Function Size(ByVal strColumn As String) As Integer
  On Error Resume Next
  Size = mcllSize(UCase(strColumn))
End Function

Friend Function Decs(ByVal strColumn As String) As Integer
  On Error Resume Next
  Decs = mcllDecs(UCase(strColumn))
End Function

Friend Function Mandatory(ByVal strColumn As String) As Boolean
  On Error Resume Next
  Mandatory = mcllMandatory(UCase(strColumn))
End Function

Friend Function VBType(ByVal strColumn As String) As Integer
  On Error Resume Next
  VBType = mcllType(UCase(strColumn))
End Function

Friend Function RDOType(ByVal strColumn As String) As Integer
  On Error Resume Next
  RDOType = mcllRDOType(UCase(strColumn))
End Function

Friend Function SmallComment(ByVal strColumn As String) As String
  On Error Resume Next
  SmallComment = mcllSmall(UCase(strColumn))
End Function

Friend Function BigComment(ByVal strColumn As String) As String
  On Error Resume Next
  BigComment = mcllBig(UCase(strColumn))
End Function

Friend Function IsColumn(ByVal strColumn As String) As Boolean
  Dim intDummy As Integer
  
  On Error Resume Next
  intDummy = mcllSize(UCase(strColumn))
  IsColumn = (Err.Number = 0)
End Function

Friend Sub CrearInformacion(ByVal strTable As String)
    
    Dim i As Long
    Dim Tablas(20) As String
    Dim NumTablas As Integer
    
    NumTablas = GetTables(strTable, Tablas())

    For i = 0 To NumTablas
        Call CreateInfo(Tablas(i))
    Next i

End Sub

Private Function LoopBlankSpaces(str As String) As Long

    Dim j As Long

    j = 1
    Do While Asc(Mid$(str, j, 1)) = 32 And j < Len(str)
        j = j + 1
    Loop

    LoopBlankSpaces = j
    
End Function

Private Function LoopAlias(str As String) As Long
    
    Dim j As Long

    j = LoopBlankSpaces(str)

    Do While Asc(Mid$(str, j, 1)) <> 44 And j < Len(str)
        j = j + 1
    Loop

    If j = Len(str) Then
        j = j + 1
    End If
    
    LoopAlias = j

End Function

Public Function GetTables(strTable As String, Cad() As String) As Integer
    
    Dim i As Long
    Dim str As String
    Dim j As Long
    Dim k As Long
       
    k = 0
    i = LoopBlankSpaces(strTable)
                      
    Do While i <= Len(strTable)
        If Asc(Mid$(strTable, i, 1)) <> 44 Then
            If Asc(Mid$(strTable, i, 1)) <> 32 Then
                str = str & Mid$(strTable, i, 1)
                i = i + 1
            Else
                i = i + LoopAlias(Mid$(strTable, i + 1, Len(strTable) - i))
            End If
            
        Else
            Cad(k) = str
            k = k + 1
            str = ""
            i = i + LoopBlankSpaces(Mid$(strTable, i + 1, Len(strTable) - i))
        End If
    Loop
    
    Cad(k) = str
    
    GetTables = k
  

End Function

