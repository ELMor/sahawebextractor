VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWFilterTables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Class clsCWFilterTables
' Coded by SYSECA Bilbao
' **********************************************************************************


' esta estructura identifica a *cada* tabla
' de datos que se incluye en el filtro
' suministrando datos sobre la misma


Public strTableName    As String           ' el nombre de la tabla
Public strTableDesc    As String           ' descripci�n de la tabla
Public strFatherName   As String           ' el nombre de su tabla padre
Public cllChilds       As New Collection   ' colecci�n de campos de la tabla
Public strNameColumn   As String           ' campo de la tabla actual en la relaci�n tabla padre-hija
Public strFatherColumn As String           ' campo de la tabla padre en la relaci�n tabla padre-hija

