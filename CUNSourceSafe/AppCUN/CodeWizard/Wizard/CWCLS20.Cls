VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWError
' Coded by Manu Roibal
' **********************************************************************************


' constantes para los mensajes de usuario
Public Enum cwMsgType
  cwInternalMsg = 1
  cwCodeMsg = 2
  cwCodeQuery = 3
  cwCodeQueryCancel = 4
End Enum


Public strDescription As String
Public strMoreData    As String
Public strTitle       As String
Public lngWinType     As Long


Public Sub SetError(ByVal cwType As cwMsgType, _
                    ByVal strMessage As String, _
                    ParamArray aMore() As Variant)
  Dim intFound  As Integer
  Dim strSearch As String
  Dim lngInd    As Long
  Dim intLabel  As Integer
  
  Select Case cwType
    Case cwInternalMsg
      strTitle = "Error"
      lngWinType = vbExclamation + vbOKOnly + vbDefaultButton1
    Case cwCodeMsg
      strTitle = "Aviso"
      lngWinType = vbInformation + vbOKOnly + vbDefaultButton1
    Case cwCodeQuery
      strTitle = "Pregunta"
      lngWinType = vbQuestion + vbYesNo + vbDefaultButton2
    Case cwCodeQueryCancel
      strTitle = "Pregunta"
      lngWinType = vbQuestion + vbYesNoCancel + vbDefaultButton1
  End Select
  
  intLabel = 1
  For lngInd = LBound(aMore) To UBound(aMore)
    strSearch = "%" & intLabel
    intFound = InStr(strMessage, strSearch)
    If intFound > 0 Then
      strMessage = Mid(strMessage, 1, intFound - 1) & _
                   aMore(lngInd) & _
                   Mid(strMessage, intFound + Len(strSearch))
    End If
    intLabel = intLabel + 1
  Next
  
  strDescription = strMessage
  
  strMoreData = "Mensaje de CodeWizard" & vbCrLf & strMessage
  
  If Err.Number > 0 Then
    strMoreData = strMoreData & vbCrLf & vbCrLf & "Error de c�digo" & vbCrLf & _
                  Err.Number & " - " & Err.Description & vbCrLf & _
                  "Origen " & Err.Source & vbCrLf & " "
  End If
  
  If rdoErrors.Count > 0 Then
    With rdoErrors(0)
      If .Number <> 0 Then
        strMoreData = strMoreData & vbCrLf & vbCrLf & "Error de Base de Datos" & vbCrLf & _
                      "Error nativo " & .Number & ", " & .Description & vbCrLf & _
                      "Estado de SQL " & .SQLState & ", " & .SQLRetcode & vbCrLf & _
                      "Origen " & .Source
      End If
    End With
  End If

End Sub

' produce un error interno de CodeWizard
Public Function InternalError(ByVal objObject As Object, _
                              ByVal strProcedure As String, _
                              Optional ByVal strParam As String = "") As Long
  If objGen.IsStrEmpty(strParam) Then
    strParam = msgNotAvailable
  End If
  Call SetError(cwInternalMsg, msgInternal, _
                TypeName(objObject), strProcedure, strParam)
  InternalError = Raise
End Function

' muestra un mensaje en la pantalla
Public Function Raise() As Long
  Dim frmMsgBox  As New frmCWMsgBox
  Dim intPointer As Integer
  
  Load frmMsgBox
  
  With frmMsgBox
    .strTitle = strTitle
    .lngMode = lngWinType
    .strMsg = strDescription
    .strMore = strMoreData
    Call .Prepare
    intPointer = Screen.MousePointer
    Screen.MousePointer = vbArrow
    Call .Show(vbModal)
    Screen.MousePointer = intPointer
    Raise = .lngAnswer
  End With
  
  Unload frmMsgBox
  Set frmMsgBox = Nothing
  
  ' dar tiempo a que se oculte la ventana
  DoEvents
End Function

