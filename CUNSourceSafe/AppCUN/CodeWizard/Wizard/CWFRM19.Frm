VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCWProperties 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Propiedades"
   ClientHeight    =   5910
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5370
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "CWFRM19.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5910
   ScaleWidth      =   5370
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAccept 
      Cancel          =   -1  'True
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   330
      Left            =   4140
      TabIndex        =   4
      Top             =   5490
      Width           =   1140
   End
   Begin TabDlg.SSTab tabProperties 
      Height          =   5325
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   5190
      _ExtentX        =   9155
      _ExtentY        =   9393
      _Version        =   327681
      Style           =   1
      Tabs            =   1
      TabHeight       =   520
      ShowFocusRect   =   0   'False
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "CWFRM19.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "imgIcon"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblName"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblType"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblApplication"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblOwner"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblLabel(0)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblLabel(1)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblLabel(2)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblLabel(3)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblLabel(4)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lblLabel(5)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "lblDatDesactive"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "lblDatActive"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "lblInternalName"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "lblLabel(6)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "lblLabel(7)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "lblLabel(8)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "lblLabel(9)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "lblUser"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "lblInternalUser"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "lblDatlUserAct"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "lblDatUserDes"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "fraLine1(0)"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "fraLine1(1)"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "fraLine1(2)"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).ControlCount=   25
      Begin VB.Frame fraLine1 
         Height          =   60
         Index           =   2
         Left            =   225
         TabIndex        =   3
         Top             =   3645
         Width           =   4740
      End
      Begin VB.Frame fraLine1 
         Height          =   60
         Index           =   1
         Left            =   225
         TabIndex        =   2
         Top             =   2430
         Width           =   4740
      End
      Begin VB.Frame fraLine1 
         Height          =   60
         Index           =   0
         Left            =   225
         TabIndex        =   1
         Top             =   1125
         Width           =   4740
      End
      Begin VB.Label lblDatUserDes 
         Caption         =   "Fecha de Desactivación del  Usuario"
         Height          =   195
         Left            =   1485
         TabIndex        =   25
         Top             =   4815
         Width           =   3435
      End
      Begin VB.Label lblDatlUserAct 
         Caption         =   "Fecha de Activación del  Usuario"
         Height          =   195
         Left            =   1485
         TabIndex        =   24
         Top             =   4500
         Width           =   3435
      End
      Begin VB.Label lblInternalUser 
         Caption         =   "Nombre interno del Usuario"
         Height          =   195
         Left            =   1485
         TabIndex        =   23
         Top             =   4185
         Width           =   3435
      End
      Begin VB.Label lblUser 
         Caption         =   "Nombre del Usuario"
         Height          =   195
         Left            =   1485
         TabIndex        =   22
         Top             =   3870
         Width           =   3435
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre Interno:"
         Height          =   195
         Index           =   9
         Left            =   270
         TabIndex        =   21
         Top             =   4185
         Width           =   1140
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Usuario:"
         Height          =   195
         Index           =   8
         Left            =   270
         TabIndex        =   20
         Top             =   3870
         Width           =   585
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Activación:"
         Height          =   195
         Index           =   7
         Left            =   270
         TabIndex        =   19
         Top             =   4500
         Width           =   795
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Desactivación:"
         Height          =   195
         Index           =   6
         Left            =   270
         TabIndex        =   18
         Top             =   4815
         Width           =   1065
      End
      Begin VB.Label lblInternalName 
         Caption         =   "Nombre interno del Objeto"
         Height          =   195
         Left            =   1485
         TabIndex        =   17
         Top             =   2655
         Width           =   3435
      End
      Begin VB.Label lblDatActive 
         Caption         =   "Fecha de Activación del Objeto"
         Height          =   195
         Left            =   1485
         TabIndex        =   16
         Top             =   2970
         Width           =   3435
      End
      Begin VB.Label lblDatDesactive 
         Caption         =   "Fecha de Desactivación del Objeto"
         Height          =   195
         Left            =   1485
         TabIndex        =   15
         Top             =   3285
         Width           =   3435
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Desactivación:"
         Height          =   195
         Index           =   5
         Left            =   270
         TabIndex        =   14
         Top             =   3285
         Width           =   1065
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Activación:"
         Height          =   195
         Index           =   4
         Left            =   270
         TabIndex        =   13
         Top             =   2970
         Width           =   795
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre Interno:"
         Height          =   195
         Index           =   3
         Left            =   270
         TabIndex        =   12
         Top             =   2655
         Width           =   1140
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Propietario:"
         Height          =   195
         Index           =   2
         Left            =   270
         TabIndex        =   11
         Top             =   2070
         Width           =   795
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Aplicación:"
         Height          =   195
         Index           =   1
         Left            =   270
         TabIndex        =   10
         Top             =   1710
         Width           =   780
      End
      Begin VB.Label lblLabel 
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo:"
         Height          =   195
         Index           =   0
         Left            =   270
         TabIndex        =   9
         Top             =   1350
         Width           =   360
      End
      Begin VB.Label lblOwner 
         Caption         =   "Propietario del Objeto"
         Height          =   195
         Left            =   1170
         TabIndex        =   8
         Top             =   2070
         Width           =   3750
      End
      Begin VB.Label lblApplication 
         Caption         =   "Aplicación del Objeto"
         Height          =   195
         Left            =   1170
         TabIndex        =   7
         Top             =   1710
         Width           =   3750
      End
      Begin VB.Label lblType 
         Caption         =   "Tipo del Objeto"
         Height          =   195
         Left            =   1170
         TabIndex        =   6
         Top             =   1350
         Width           =   3750
      End
      Begin VB.Label lblName 
         Caption         =   "Nombre del Objeto"
         Height          =   195
         Left            =   990
         TabIndex        =   5
         Top             =   585
         Width           =   3975
      End
      Begin VB.Image imgIcon 
         Height          =   480
         Left            =   270
         Picture         =   "CWFRM19.frx":0028
         Top             =   495
         Width           =   480
      End
   End
End
Attribute VB_Name = "frmCWProperties"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWProperties
' Coded by Manu Roibal
' **********************************************************************************


Public Sub Prepare(ByVal objProcess As clsCWUserProcess, _
                   ByVal picIcon As IImage)
  lblName = objProcess.strProcessDesc
  
  Select Case objProcess.intProcessType
    Case cwTypeWindow
      lblType = "Ventana"
    Case cwTypeRoutine
      lblType = "Rutina"
    Case cwTypeReport
      lblType = "Listado"
    Case cwTypeExternalReport
      lblType = "Listado Externo"
    Case cwTypeApplication
      lblType = "Aplicación"
  End Select
  
  lblApplication = objProcess.strAppDesc
  lblOwner = objProcess.strRolDesc
  lblInternalName = objProcess.strProcess
  
  If IsDate(objProcess.vntProcessFecAct) Then
    lblDatActive = Format(CDate(objProcess.vntProcessFecAct), "Long Date")
  Else
    lblDatActive = "No determinada"
  End If
    
  If IsDate(objProcess.vntProcessFecDes) Then
    lblDatDesactive = Format(CDate(objProcess.vntProcessFecDes), "Long Date")
  Else
    lblDatDesactive = "No determinada"
  End If
  
  lblUser = objSecurity.strFullName
  lblInternalUser = objSecurity.strUser
  
  If IsDate(objSecurity.strFecAct) Then
    lblDatlUserAct = Format(CDate(objSecurity.strFecAct), "Long Date")
  Else
    lblDatlUserAct = "No determinada"
  End If
    
  If IsDate(objSecurity.strFecDes) Then
    lblDatUserDes = Format(CDate(objSecurity.strFecDes), "Long Date")
  Else
    lblDatUserDes = "No determinada"
  End If
  
  Set imgIcon.Picture = picIcon.Picture
End Sub


Private Sub cmdAccept_Click()
  Call Me.Hide
End Sub

