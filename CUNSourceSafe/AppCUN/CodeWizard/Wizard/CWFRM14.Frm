VERSION 5.00
Begin VB.Form frmCWSelectDir 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Seleccionar"
   ClientHeight    =   2985
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4740
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   4740
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.DriveListBox drvDocum 
      Height          =   315
      Left            =   90
      TabIndex        =   3
      Top             =   2565
      Width           =   2895
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3105
      TabIndex        =   2
      Top             =   630
      Width           =   1545
   End
   Begin VB.CommandButton cmdAccept 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   3105
      TabIndex        =   1
      Top             =   90
      Width           =   1545
   End
   Begin VB.DirListBox dirDocum 
      Height          =   2340
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   2895
   End
End
Attribute VB_Name = "frmCWSelectDir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWDocDirectory
' Coded by Manu Roibal
' **********************************************************************************


Public strDirectory As String


Private Sub cmdAccept_Click()
  strDirectory = dirDocum.Path & "\"
  Me.Hide
End Sub

Private Sub cmdCancel_Click()
  strDirectory = ""
  Me.Hide
End Sub

Private Sub drvDocum_Change()
  Dim oldPath As String
  
  On Error Resume Next
  oldPath = dirDocum.Path
  dirDocum.Path = drvDocum.Drive
  If Err.Number > 0 Then
    Call objError.SetError(cwCodeMsg, msgUnitNotReady, drvDocum.Drive)
    Call objError.Raise
    drvDocum.Drive = oldPath
  End If
End Sub

