VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWPipeLine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWPipeLine
' Coded by Manu Roibal
' **********************************************************************************


' esta clase implementa un pipeline del mismo estilo
' que los implementados por la arquitectura x86


Dim mcllKeys As New Collection


Public Sub PipeSet(ByVal strKey As String, _
                   ByVal vntValue As Variant)
  
  On Error GoTo cwIntError
  
  strKey = UCase(strKey)
  If PipeExist(strKey) Then
    Call PipeRemove(strKey)
  End If
  Call mcllKeys.Add(vntValue, strKey)
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "PipeSet", strKey)
End Sub

Public Function PipeGet(ByVal strKey As String) As Variant
  
  On Error GoTo cwIntError
  
  PipeGet = mcllKeys(UCase(strKey))
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "PipeGet", strKey)
End Function

Public Sub PipeRemove(ByVal strKey As String)
  
  On Error GoTo cwIntError
  
  Call mcllKeys.Remove(UCase(strKey))
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "PipeRemove", strKey)
End Sub

Public Function PipeExist(ByVal strKey As String) As Boolean
  Dim strPrueba As String
  
  On Error Resume Next
  strPrueba = mcllKeys(UCase(strKey))
  PipeExist = (Err.Number = 0)
End Function
