VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWFilterOrder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Class clsCWFilterOrder
' Coded by SYSECA Bilbao
' **********************************************************************************


' esta estructura identifica a cada control
' de datos que se incluye en la 'order by'
' suministrando datos sobre el mismo


Public strFieldName As String       ' el nombre del campo de la order
Public strFieldDesc As String       ' la descripción del campo
Public blnActive    As Boolean      ' entra o no en la order
Public blnOrderBy   As Boolean      ' false = ASC, true = DESC

