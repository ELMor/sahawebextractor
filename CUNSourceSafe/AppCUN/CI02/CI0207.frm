VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Begin VB.Form frmCitasRecurso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Citas por recurso"
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdHF 
      Caption         =   "&H. Filiacion"
      Height          =   375
      Left            =   10920
      TabIndex        =   32
      Top             =   1080
      Width           =   915
   End
   Begin VB.CommandButton cmdRecitacion 
      Caption         =   "&Recitaci�n"
      Height          =   375
      Left            =   10920
      TabIndex        =   31
      Top             =   7440
      Width           =   945
   End
   Begin VB.CommandButton cmdOverbooking 
      Caption         =   "Overboo&k."
      Height          =   375
      Index           =   1
      Left            =   10920
      TabIndex        =   30
      Top             =   6600
      Width           =   915
   End
   Begin VB.CommandButton cmdPendRecitar 
      Caption         =   "&Pend. Recitar"
      Height          =   495
      Left            =   10920
      TabIndex        =   29
      Top             =   1620
      Width           =   915
   End
   Begin VB.CommandButton cmdOverbooking 
      Caption         =   "Over&book."
      Height          =   375
      Index           =   0
      Left            =   10920
      TabIndex        =   28
      Top             =   3180
      Width           =   915
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "Ci&tar"
      Height          =   375
      Index           =   1
      Left            =   10920
      TabIndex        =   27
      Top             =   6180
      Width           =   945
   End
   Begin VB.CommandButton cmdDatosAct 
      Caption         =   "Dat&os Act."
      Height          =   375
      Index           =   1
      Left            =   10920
      TabIndex        =   26
      Top             =   5760
      Width           =   945
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "An&ular"
      Height          =   375
      Index           =   1
      Left            =   10920
      TabIndex        =   25
      Top             =   7020
      Width           =   945
   End
   Begin VB.CheckBox chkVerTodas 
      Alignment       =   1  'Right Justify
      Caption         =   "Ver act. citadas, en cola, realiz�ndose y realizadas"
      Height          =   195
      Left            =   120
      TabIndex        =   24
      Top             =   960
      Width           =   3915
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "Consu&ltar"
      Height          =   375
      Left            =   7320
      TabIndex        =   23
      Top             =   120
      Width           =   855
   End
   Begin VB.CommandButton cmdAgendaRec 
      Caption         =   "Age&nda"
      Enabled         =   0   'False
      Height          =   375
      Left            =   7320
      TabIndex        =   16
      Top             =   840
      Width           =   855
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10920
      TabIndex        =   13
      Top             =   120
      Width           =   915
   End
   Begin VB.CommandButton cmdVisionGlobal 
      Caption         =   "V. &Global"
      Height          =   375
      Left            =   10920
      TabIndex        =   12
      Top             =   600
      Width           =   915
   End
   Begin VB.CommandButton cmdDatosAct 
      Caption         =   "&Datos Act."
      Height          =   375
      Index           =   0
      Left            =   10920
      TabIndex        =   11
      Top             =   2220
      Width           =   945
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "&Anular"
      Height          =   375
      Index           =   0
      Left            =   10920
      TabIndex        =   10
      Top             =   3660
      Width           =   945
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Citar"
      Height          =   375
      Index           =   0
      Left            =   10920
      TabIndex        =   9
      Top             =   2700
      Width           =   945
   End
   Begin VB.CommandButton cmdImprimirCarta 
      Caption         =   "I&mprimir Carta"
      Height          =   555
      Left            =   10920
      TabIndex        =   8
      Top             =   4140
      Width           =   945
   End
   Begin VB.CommandButton cmdHojaPacientes 
      Caption         =   "Imprimir Citas P&endientes"
      Height          =   675
      Left            =   10920
      TabIndex        =   7
      Top             =   7920
      Width           =   945
   End
   Begin VB.Frame Frame1 
      Caption         =   "Listados"
      ForeColor       =   &H00FF0000&
      Height          =   1215
      Left            =   8280
      TabIndex        =   2
      Top             =   0
      Width           =   2535
      Begin VB.CommandButton cmdImprimirListados 
         Caption         =   "&Imprimir"
         Height          =   375
         Left            =   1680
         TabIndex        =   6
         Top             =   780
         Width           =   795
      End
      Begin VB.OptionButton optDiario 
         Caption         =   "Diario"
         Height          =   255
         Left            =   180
         TabIndex        =   5
         Top             =   780
         Width           =   1395
      End
      Begin VB.OptionButton optDSolicitante 
         Caption         =   "Diario con solicitante"
         Height          =   195
         Left            =   180
         TabIndex        =   4
         Top             =   540
         Width           =   1815
      End
      Begin VB.OptionButton optFechas 
         Caption         =   "En un rango de fechas"
         Height          =   255
         Left            =   180
         TabIndex        =   3
         Top             =   240
         Value           =   -1  'True
         Width           =   1995
      End
   End
   Begin VB.CommandButton cmdVolverACitadas 
      Caption         =   "&Volver a Citadas"
      Height          =   495
      Left            =   10920
      TabIndex        =   1
      Top             =   5220
      Width           =   945
   End
   Begin SSDataWidgets_B.SSDBGrid grdCitadas 
      Height          =   3495
      Left            =   60
      TabIndex        =   14
      Top             =   1320
      Width           =   10755
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   18971
      _ExtentY        =   6165
      _StockProps     =   79
      Caption         =   "Actuaciones citadas"
      ForeColor       =   8388608
      BackColor       =   12632256
   End
   Begin SSDataWidgets_B.SSDBGrid grdPendientes 
      Height          =   3630
      Left            =   60
      TabIndex        =   15
      Top             =   4920
      Width           =   10755
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   18971
      _ExtentY        =   6403
      _StockProps     =   79
      Caption         =   "Actuaciones pendientes de recitar"
      ForeColor       =   8388608
      BackColor       =   12632256
   End
   Begin ComctlLib.ListView lvwRec 
      Height          =   1155
      Left            =   4260
      TabIndex        =   17
      Top             =   60
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   2037
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin SSDataWidgets_B.SSDBCombo cboDptos 
      Height          =   315
      Left            =   600
      TabIndex        =   0
      Top             =   60
      Width           =   3015
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5318
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSCalendarWidgets_A.SSDateCombo dcboFecDesde 
      Height          =   330
      Left            =   600
      TabIndex        =   18
      Tag             =   "Fecha superior de las citas"
      Top             =   480
      Width           =   1530
      _Version        =   65537
      _ExtentX        =   2699
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dcboFecHasta 
      Height          =   330
      Left            =   2700
      TabIndex        =   19
      Tag             =   "Fecha superior de las citas"
      Top             =   480
      Width           =   1530
      _Version        =   65537
      _ExtentX        =   2699
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Dpto:"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Width           =   435
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Hasta:"
      Height          =   195
      Index           =   1
      Left            =   2220
      TabIndex        =   21
      Top             =   600
      Width           =   465
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Desde:"
      Height          =   195
      Index           =   2
      Left            =   60
      TabIndex        =   20
      Top             =   600
      Width           =   510
   End
End
Attribute VB_Name = "frmCitasRecurso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intDptoSel%, strRecSel$, strAllRec$
Dim strMeCaption$
Dim strTexto As String


Private Sub cboDptos_Click()
    If intDptoSel <> cboDptos.Columns("Cod").Text Then Call pVaciarGrids
    intDptoSel = cboDptos.Columns("Cod").Text
    Call pCargarRecursos(intDptoSel)
    Call pCaption
End Sub

Private Sub cmdAgendaRec_Click()
    Call objPipe.PipeSet("DPTO", cboDptos.Text)
    Call objPipe.PipeSet("COD_REC", lvwRec.SelectedItem.Tag)
    Call objPipe.PipeSet("REC", lvwRec.SelectedItem.Text)
    Screen.MousePointer = vbHourglass
    Call objSecurity.LaunchProcess("AG1008")
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdAnular_Click(Index As Integer)
    Call pAnularAct
End Sub

Private Sub cmdCitar_Click(Index As Integer)
    Call pCitar
End Sub

Private Sub cmdConsultar_Click()
    Dim i%
    If strRecSel <> "" Then
        Call pCargarActCitadas(-1)
        Call pCargarActPendRecitar(-1)
    Else
        MsgBox "No se ha seleccionado ning�n recurso.", vbExclamation, strMeCaption
    End If
End Sub

Private Sub cmdHF_Click()
    Dim grdGrid As SSDBGrid
    Dim i%, strCodPers$
    
    'se mira si hay aguna actuaci�n seleccionada
    If grdCitadas.SelBookmarks.Count > 0 Then
        Set grdGrid = grdCitadas
    ElseIf grdPendientes.SelBookmarks.Count > 0 Then
        Set grdGrid = grdPendientes
    Else
        MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, strMeCaption
        Exit Sub
    End If
    
    'si hay m�s de una actuaci�n seleccionada se mira que corresponda al mismo paciente
    strCodPers = grdGrid.Columns("CodPersona").CellValue(grdGrid.SelBookmarks(0))
    If grdGrid.SelBookmarks.Count > 1 Then
        For i = 1 To grdGrid.SelBookmarks.Count - 1
            If strCodPers <> grdGrid.Columns("CodPersona").CellValue(grdGrid.SelBookmarks(i)) Then
                MsgBox "Debe seleccionar un �nico Paciente.", vbExclamation, strMeCaption
                Exit Sub
            End If
        Next i
    End If
    
    'se accede a la Hoja de filiacion
    Call objSecurity.LaunchProcess("CI4017", strCodPers)
End Sub

Private Sub cmdHojaPacientes_Click()
    Dim strWhere$
    If Len(strRecSel) > 4 Then
        strWhere = "PR0435j.AG11CODRECURSO IN (" & strAllRec & ")"
    Else
        strWhere = "PR0435j.AG11CODRECURSO = " & strRecSel
    End If
'            & " AND PR0432j.CI01FECCONCERT BETWEEN TO_DATE('" & dcboFecDesde.Text & "' ,'DD/MM/YYYY')" _
'            & " AND TO_DATE('" & CStr(DateAdd("d", 1, CDate(dcboFecHasta.Text))) & "','DD/MM/YYYY')"
    Call Imprimir_API(strWhere, "Pendientes.rpt")
End Sub

Private Sub cmdImprimirCarta_Click()
    Dim i%, msg$, n%
    
    For i = 0 To grdCitadas.SelBookmarks.Count - 1
        If grdCitadas.Columns("Recita").CellText(grdCitadas.SelBookmarks(i)) = 1 Then
            n = n + 1
        End If
    Next
    If n = 0 Then
        msg = "No se ha seleccionado ninguna Actuaci�n que haya sido Recitada."
        MsgBox msg, vbExclamation, strMeCaption
        Exit Sub
    Else
        If n <> grdCitadas.SelBookmarks.Count Then
            msg = "Alguna de las Actuaciones selecionadas no ha sido Recitada y por tanto"
            msg = msg & " no se va a imprimir la carta correspondiente."
            MsgBox msg, vbInformation, strMeCaption
        End If
        msg = ""
        msg = InputBox("Introduzca el Texto de la Carta:", "Motivo de Recita", strTexto)
        If msg = "" Then
            msg = "No se ha se�alado ning�n motivo de Recita."
            MsgBox msg, vbExclamation, strMeCaption
            Exit Sub
        End If
        strTexto = msg
        For i = 0 To grdCitadas.SelBookmarks.Count - 1
            If grdCitadas.Columns("Recita").CellText(grdCitadas.SelBookmarks(i)) = 1 Then
                Call pImprimirCartas(grdCitadas.Columns("NumActPlan").CellValue(grdCitadas.SelBookmarks(i)), msg)
            End If
        Next i
    End If
End Sub

Private Sub cmdImprimirListados_Click()
    Dim strWhere$, Formula(1, 2)
    
    Call pCaption
    Screen.MousePointer = vbHourglass
    strWhere = " PR0409J.AD02CODDPTO= " & intDptoSel _
            & " AND CI01FECCONCERT >= TO_DATE('" & dcboFecDesde.Text & "','DD/MM/YYYY')" _
            & " AND CI01FECCONCERT < TO_DATE('" & DateAdd("d", 1, CDate(dcboFecHasta.Text)) & " ','DD/MM/YYYY')" _
            & " AND PR0409J.AG11CODRECURSO IN (" & strRecSel & ")"
    Formula(1, 1) = "fecha"
    Formula(1, 2) = "date(" & Year(dcboFecDesde.Text) & "," & Month(dcboFecDesde.Text) & "," & Day(dcboFecDesde.Text) & ")"
    If optDiario.Value = True Then Call Imprimir_API(strWhere, "CI01R.rpt", Formula)
    If optDSolicitante.Value = True Then Call Imprimir_API(strWhere, "CI03R.rpt", Formula)
    If optFechas.Value = True Then Call Imprimir_API(strWhere, "CI024.rpt", Formula)
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdDatosAct_Click(Index As Integer)
    Call pDatosAct
End Sub

Private Sub pRecitacion()
    Dim i%, strAux$
    Dim strFecha$, strHora$, strCodRec$, strObserv$
    Dim strFecCita$, strCodRecCita$

    Select Case grdPendientes.SelBookmarks.Count
    Case 0: Exit Sub
        MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, strMeCaption
        Exit Sub
    Case 1
        Call objPipe.PipeSet("CI0208_FECHA", Format(grdPendientes.Columns("Fecha Cita").CellValue(grdPendientes.SelBookmarks(0)), "dd/mm/yyyy"))
        Call objPipe.PipeSet("CI0208_HORA", Format(grdPendientes.Columns("Fecha Cita").CellValue(grdPendientes.SelBookmarks(0)), "hh:mm"))
        Call objPipe.PipeSet("CI0208_AG11CODRECURSO", grdPendientes.Columns("CodRecurso").CellValue(grdPendientes.SelBookmarks(0)))
    Case Else
        'Si la selecci�n comprende un �nico recurso, se pasa este en el Pipe para que _
        pueda ser cambiado. Si son varios recursos, no se podr�n cambiar y s�lo se _
        podr� cambiar la fecha
        strAux = grdPendientes.Columns("CodRecurso").CellValue(grdPendientes.SelBookmarks(0))
        For i = 1 To grdPendientes.SelBookmarks.Count - 1
            If strAux <> grdPendientes.Columns("CodRecurso").CellValue(grdPendientes.SelBookmarks(i)) Then
                Exit For
            End If
        Next i
        If i = grdPendientes.SelBookmarks.Count Then 'mismo recurso
            Call objPipe.PipeSet("CI0208_FECHA", Format(grdPendientes.Columns("Fecha Cita").CellValue(grdPendientes.SelBookmarks(0)), "dd/mm/yyyy"))
            Call objPipe.PipeSet("CI0208_AG11CODRECURSO", grdPendientes.Columns("CodRecurso").CellValue(grdPendientes.SelBookmarks(0)))
        Else 'recursos distintos
            Call objPipe.PipeSet("CI0208_FECHA", Format(grdPendientes.Columns("Fecha Cita").CellValue(grdPendientes.SelBookmarks(0)), "dd/mm/yyyy"))
        End If
    End Select

    frmRecitacion.Show vbModal
    Set frmRecitacion = Nothing
    If Not objPipe.PipeExist("CI0208_FECHA") Then Exit Sub 'se ha cancelado la operaci�n

    strFecha = objPipe.PipeGet("CI0208_FECHA")
    Call objPipe.PipeRemove("CI0208_FECHA")
    If objPipe.PipeExist("CI0208_HORA") Then
        strHora = objPipe.PipeGet("CI0208_HORA")
        Call objPipe.PipeRemove("CI0208_HORA")
    End If
    If objPipe.PipeExist("CI0208_AG11CODRECURSO") Then
        strCodRec = objPipe.PipeGet("CI0208_AG11CODRECURSO")
        Call objPipe.PipeRemove("CI0208_AG11CODRECURSO")
    End If
    If objPipe.PipeExist("CI0208_CI01OBSERV") Then
        strObserv = objPipe.PipeGet("CI0208_CI01OBSERV")
        Call objPipe.PipeRemove("CI0208_CI01OBSERV")
    End If

    Screen.MousePointer = vbHourglass
    objApp.BeginTrans
    On Error GoTo Canceltrans:
    For i = 0 To grdPendientes.SelBookmarks.Count - 1
        strFecCita = strFecha & " " & IIf(strHora = "", Format(grdPendientes.Columns("Fecha Cita").CellValue(grdPendientes.SelBookmarks(i)), "hh:mm"), strHora)
        strCodRecCita = IIf(strCodRec = "", grdPendientes.Columns("CodRecurso").CellValue(grdPendientes.SelBookmarks(i)), strCodRec)
        Call pRecitarManual(grdPendientes.Columns("NumActPlan").CellValue(grdPendientes.SelBookmarks(i)), _
                grdPendientes.Columns("CodAct").CellValue(grdPendientes.SelBookmarks(i)), _
                grdPendientes.Columns("NSol").CellValue(grdPendientes.SelBookmarks(i)), _
                grdPendientes.Columns("NCita").CellValue(grdPendientes.SelBookmarks(i)) + 1, _
                strFecCita, strCodRecCita, strObserv)
    Next i
    objApp.CommitTrans
    grdPendientes.DeleteSelected
    Call pCargarActCitadas(-1)
    Screen.MousePointer = vbDefault
    Exit Sub

Canceltrans:
    objApp.RollbackTrans
    Screen.MousePointer = vbDefault
    strAux = "Se ha producido un error al realizar la recitaci�n." & Chr$(13) & Error
    MsgBox strAux, vbExclamation, strMeCaption
End Sub

Private Sub cmdOverbooking_Click(Index As Integer)
    Call pOverbooking
End Sub

Private Sub cmdPendRecitar_Click()
    Dim sql$, qryPR04 As rdoQuery, qryCI01 As rdoQuery, i%
    Dim strCancel$
    
    If grdCitadas.SelBookmarks.Count = 0 Then
        MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, strMeCaption
        Exit Sub
    End If
    
    'se comprueba que todas las actuaciones seleccionadas pueden ser pasadas a
    'pendientes de recitar (estado citada)
    For i = 0 To grdCitadas.SelBookmarks.Count - 1
        If grdCitadas.Columns("Estado").CellText(grdCitadas.SelBookmarks(i)) <> "Citada" Then
            sql = "Alguna de las actuaciones seleccionadas ya ha entrado en cola,"
            sql = sql & " est� realiz�ndose o ha sido realizada y no puede dejarse"
            sql = sql & " pendiente de recitar."
            sql = sql & Chr$(13)
            sql = sql & "Seleccione s�lo actuaciones en estado 'Citada'."
            MsgBox sql, vbExclamation, strMeCaption
            Exit Sub
        End If
    Next i
    
    sql = "Indique el motivo por el que se van a dejar pendientes de recitar las actuaciones seleccionadas:"
    strCancel = Trim$(InputBox(sql, "Dejar Pendiente de Recitar"))
    If strCancel = "" Then
        sql = "La operaci�n NO se ha realizado: hace falta indicar un motivo."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    
    'se pasan las actuaciones a pendientes a recitar
    sql = "UPDATE PR0400 SET PR37CODESTADO = ?"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND PR37CODESTADO = ?"
    Set qryPR04 = objApp.rdoConnect.CreateQuery("", sql)
    qryPR04(0) = constESTACT_PLANIFICADA
    qryPR04(2) = constESTACT_CITADA
    
    sql = "UPDATE CI0100 SET CI01SITCITA = ?, CI01OBSERV = ?"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND CI01SITCITA = ?"
    Set qryCI01 = objApp.rdoConnect.CreateQuery("", sql)
    qryCI01(0) = constESTCITA_PENDRECITAR
    qryCI01(1) = Left$(strCancel, 255)
    qryCI01(3) = constESTCITA_CITADA
    
    Screen.MousePointer = vbHourglass
    objApp.BeginTrans
    On Error GoTo Canceltrans
    For i = 0 To grdCitadas.SelBookmarks.Count - 1
        qryPR04(1) = grdCitadas.Columns("NumActPlan").CellText(grdCitadas.SelBookmarks(i))
        qryPR04.Execute
        qryCI01(2) = grdCitadas.Columns("NumActPlan").CellText(grdCitadas.SelBookmarks(i))
        qryCI01.Execute
    Next i
    objApp.CommitTrans
    grdCitadas.DeleteSelected
    Call pCargarActPendRecitar(-1)
    Screen.MousePointer = vbDefault
    Exit Sub
    
Canceltrans:
    objApp.RollbackTrans
    Screen.MousePointer = vbDefault
    sql = "Se ha producido un error al pasar las actuaciones a pendientes de recitar."
    sql = sql & Chr$(13) & Error
    MsgBox sql, vbExclamation, strMeCaption
End Sub

Private Sub cmdRecitacion_Click()
    Call pRecitacion
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdVisionGlobal_Click()
    Dim grdGrid As SSDBGrid
    Dim i%, strCodPers$
    
    'se mira si hay aguna actuaci�n seleccionada
    If grdCitadas.SelBookmarks.Count > 0 Then
        Set grdGrid = grdCitadas
    ElseIf grdPendientes.SelBookmarks.Count > 0 Then
        Set grdGrid = grdPendientes
    Else
        MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, strMeCaption
        Exit Sub
    End If
    
    'si hay m�s de una actuaci�n seleccionada se mira que corresponda al mismo paciente
    strCodPers = grdGrid.Columns("CodPersona").CellValue(grdGrid.SelBookmarks(0))
    If grdGrid.SelBookmarks.Count > 1 Then
        For i = 1 To grdGrid.SelBookmarks.Count - 1
            If strCodPers <> grdGrid.Columns("CodPersona").CellValue(grdGrid.SelBookmarks(i)) Then
                MsgBox "Debe seleccionar un �nico Paciente.", vbExclamation, strMeCaption
                Exit Sub
            End If
        Next i
    End If
    
    'se accede a la Visi�n Global
    Dim vntData()
    ReDim vntData(1 To 2) As Variant
    vntData(1) = strCodPers
    vntData(2) = 2
    Call objSecurity.LaunchProcess("HC02", vntData())
End Sub

Private Sub cmdVolverACitadas_Click()
    Dim sql$, qryPR04 As rdoQuery, qryCI01 As rdoQuery, i%
    
    If grdPendientes.SelBookmarks.Count = 0 Then
        MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, strMeCaption
        Exit Sub
    End If
        
    sql = "�Desea Ud. volver a citadas las actuaciones seleccionadas?"
    If MsgBox(sql, vbQuestion + vbYesNo, strMeCaption) = vbNo Then Exit Sub

    'se vuelven a citar las actuaciones
    sql = "UPDATE PR0400 SET PR37CODESTADO = ?"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    Set qryPR04 = objApp.rdoConnect.CreateQuery("", sql)
    qryPR04(0) = constESTACT_CITADA
    
    sql = "UPDATE CI0100 SET CI01SITCITA = ?, CI01OBSERV = NULL"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND CI01SITCITA = ?"
    Set qryCI01 = objApp.rdoConnect.CreateQuery("", sql)
    qryCI01(0) = constESTCITA_CITADA
    qryCI01(2) = constESTCITA_PENDRECITAR
        
    Screen.MousePointer = vbHourglass
    objApp.BeginTrans
    On Error GoTo Canceltrans
    For i = 0 To grdPendientes.SelBookmarks.Count - 1
        qryPR04(1) = grdPendientes.Columns("NumActPlan").CellText(grdPendientes.SelBookmarks(i))
        qryPR04.Execute
        qryCI01(1) = grdPendientes.Columns("NumActPlan").CellText(grdPendientes.SelBookmarks(i))
        qryCI01.Execute
    Next
    objApp.CommitTrans
    grdPendientes.DeleteSelected
    Call pCargarActCitadas(-1)
    Screen.MousePointer = vbDefault
    Exit Sub
    
Canceltrans:
    objApp.RollbackTrans
    Screen.MousePointer = vbDefault
    sql = "Se ha producido un error al pasar las actuaciones a pendientes de recitar."
    sql = sql & Chr$(13) & Error
    MsgBox sql, vbExclamation, strMeCaption

    
        
    
    
End Sub

Private Sub dcboFecDesde_Change()
    On Error Resume Next
    If dcboFecDesde.Date <> "" Then
        If CDate(dcboFecHasta.Date) < CDate(dcboFecDesde.Date) Then
            dcboFecHasta.Date = dcboFecDesde.Date
            If Err > 0 Then dcboFecDesde.Date = dcboFecHasta.Date
        End If
    End If
End Sub

Private Sub dcboFecDesde_Click()
    On Error Resume Next
    If dcboFecDesde.Date <> "" Then
        If CDate(dcboFecHasta.Date) < CDate(dcboFecDesde.Date) Then
            dcboFecHasta.Date = dcboFecDesde.Date
            If Err > 0 Then dcboFecDesde.Date = dcboFecHasta.Date
        End If
    End If
End Sub

Private Sub dcboFecHasta_Change()
    On Error Resume Next
    If dcboFecHasta.Date <> "" Then
        If CDate(dcboFecHasta.Date) < CDate(dcboFecDesde.Date) Then
            dcboFecDesde.Date = dcboFecHasta.Date
            If Err > 0 Then dcboFecHasta.Date = dcboFecDesde.Date
        End If
    End If
End Sub

Private Sub dcboFecHasta_Click()
    On Error Resume Next
    If dcboFecHasta.Date <> "" Then
        If CDate(dcboFecHasta.Date) < CDate(dcboFecDesde.Date) Then
            dcboFecDesde.Date = dcboFecHasta.Date
            If Err > 0 Then dcboFecHasta.Date = dcboFecDesde.Date
        End If
    End If
End Sub

Private Sub Form_Load()
    Call pFormatearControles
    Call pCargarDptos
    Call pCargarRecursos(intDptoSel)
End Sub

Private Sub grdCitadas_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub grdCitadas_Click()
    grdPendientes.SelBookmarks.RemoveAll
End Sub

Private Sub grdCitadas_HeadClick(ByVal ColIndex As Integer)
    If grdCitadas.Rows > 0 Then Call pCargarActCitadas(ColIndex)
End Sub

Private Sub grdPendientes_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub grdPendientes_HeadClick(ByVal ColIndex As Integer)
    If grdPendientes.Rows > 0 Then Call pCargarActPendRecitar(ColIndex)
End Sub

Private Sub grdPendientes_Click()
    grdCitadas.SelBookmarks.RemoveAll
End Sub

Private Sub lvwRec_Click()
    Dim i%, cont%
    
    Call pVaciarGrids
 
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    If lvwRec.ListItems(1).Selected Then
        For i = 2 To lvwRec.ListItems.Count
            lvwRec.ListItems(i).Selected = False
        Next i
    End If
    
    'se habilita el acceso a la agenda del recurso (s�lo para un �nico recurso seleccionado)
    For i = 1 To lvwRec.ListItems.Count
        If lvwRec.ListItems(i).Selected Then
            If i = 1 Then Exit For 'TODOS
            cont = cont + 1
        End If
    Next i
    If cont <> 1 Then cmdAgendaRec.Enabled = False Else cmdAgendaRec.Enabled = True
    
    'Se muestra en el Caption los recursos seleccionados
    Call pCaption
End Sub

Private Function fEstado(intEstado%, Optional strFecEntrCola$)
    Select Case intEstado
    Case constESTACT_CITADA
        If strFecEntrCola = "" Then fEstado = "Citada" Else fEstado = "En cola"
    Case constESTACT_REALIZANDOSE
        fEstado = "Realiz�ndose"
    Case constESTACT_REALIZADA
        fEstado = "Realizada"
    End Select
End Function

Private Sub pDatosAct()
'**************************************************************************************
'*  Muestra los datos de la actuaci�n seleccionada
'**************************************************************************************
    Dim grdGrid As SSDBGrid, sql$, lngNAPlan&
    
    If grdCitadas.SelBookmarks.Count > 0 Then
        Set grdGrid = grdCitadas
    ElseIf grdPendientes.SelBookmarks.Count > 0 Then
        Set grdGrid = grdPendientes
    Else
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    If grdGrid.SelBookmarks.Count = 1 Then
        lngNAPlan = grdGrid.Columns("NumActPlan").CellValue(grdGrid.SelBookmarks(0))
    Else
        sql = "Debe Ud. seleccionar una �nica Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    Call objSecurity.LaunchProcess("PR0510", lngNAPlan)
End Sub

Private Sub pAnularAct()
'**************************************************************************************
'*  Anula una actuaci�n planificada cambiando su estado a anulada
'**************************************************************************************
    Dim grdGrid As SSDBGrid, i%
    Dim sql$, qryPR04 As rdoQuery, qryCI01 As rdoQuery
    Dim strCancel$, lngNumActPlan&
    
    'se mira si existe alguna actuaci�n seleccionada
    If grdCitadas.SelBookmarks.Count > 0 Then
        Set grdGrid = grdCitadas
        'se comprueba que todas las actuaciones seleccionadas pueden ser anuladas (estado citada)
        For i = 0 To grdGrid.SelBookmarks.Count - 1
            If grdGrid.Columns("Estado").CellText(grdGrid.SelBookmarks(i)) <> "Citada" Then
                sql = "Alguna de las actuaciones seleccionadas ya ha entrado en cola,"
                sql = sql & " est� realiz�ndose o ha sido realizada y no puede anularse."
                sql = sql & Chr$(13)
                sql = sql & "Seleccione s�lo actuaciones en estado 'Citada'."
                MsgBox sql, vbExclamation, strMeCaption
                Exit Sub
            End If
        Next i
    ElseIf grdPendientes.SelBookmarks.Count > 0 Then
        Set grdGrid = grdPendientes
        'todas las actuaciones pueden ser anuladas
    Else
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
    
    Select Case grdGrid.SelBookmarks.Count
    Case 1
        sql = "Si Ud. anula la actuaci�n ya no estar� disponible para ser realizada." & Chr$(13)
        sql = sql & "Indique el motivo por el que se va a anular la actuaci�n:"
    Case Is > 1
        sql = "Si Ud. anula las actuaciones ya no estar�n disponibles para ser realizadas." & Chr$(13)
        sql = sql & "Indique el motivo por el que se van a anular las actuaciones:"
    End Select
    strCancel = Trim$(InputBox(sql, "Anular Actuaci�n"))
    If strCancel = "" Then
        sql = "La actuaci�n NO ha sido anulada: hace falta indicar un motivo."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If
        
    'Querys para la anulaci�n de la actuaci�n y la cita
    sql = "UPDATE PR0400 SET PR04DESMOTCAN = ?,"
    sql = sql & " PR37CODESTADO = " & constESTACT_CANCELADA & ","
    sql = sql & " PR04FECCANCEL = SYSDATE,"
    sql = sql & " PR04FECENTRCOLA = NULL,"
    sql = sql & " PR04FECINIACT = NULL,"
    sql = sql & " PR04FECFINACT = NULL"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    Set qryPR04 = objApp.rdoConnect.CreateQuery("", sql)
    
    sql = "UPDATE CI0100 SET CI01SITCITA = '" & constESTCITA_ANULADA & "'"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND CI01SITCITA IN ('" & constESTCITA_CITADA & "','" & constESTCITA_PENDRECITAR & "')"
    Set qryCI01 = objApp.rdoConnect.CreateQuery("", sql)

    Screen.MousePointer = vbHourglass
    On Error GoTo Canceltrans
    objApp.BeginTrans
    For i = 0 To grdGrid.SelBookmarks.Count - 1
        lngNumActPlan = grdGrid.Columns("NumActPlan").CellText(grdGrid.SelBookmarks(i))
        
        'anular la actuaci�n
        If Len(strCancel) > 50 Then qryPR04(0) = Left$(strCancel, 50) Else qryPR04(0) = strCancel
        qryPR04(1) = lngNumActPlan
        qryPR04.Execute
        
        'anular la cita
        qryCI01(0) = lngNumActPlan
        qryCI01.Execute
        
        'anular las actuaciones y citas asociadas
        Call pAnularAsociadas(lngNumActPlan)
    Next i
    qryPR04.Close
    qryCI01.Close
    objApp.CommitTrans
    'se elimina la actuaci�n del Grid
    grdGrid.DeleteSelected
    Screen.MousePointer = vbDefault

    Exit Sub
    
Canceltrans:
    objApp.RollbackTrans
    Screen.MousePointer = vbDefault
    sql = "Se ha producido un error al realizar la anulaci�n." & Chr$(13) & Error
    MsgBox sql, vbExclamation, strMeCaption
End Sub

Private Sub pCaption()
    Dim i%, msg$

    'Se muestra en el Caption del formulario los recursos seleccionados
    strRecSel = ""
    For i = 1 To lvwRec.ListItems.Count
        If lvwRec.ListItems(i).Selected Then
            strRecSel = strRecSel & lvwRec.ListItems(i).Tag & ","
            msg = msg & lvwRec.ListItems(i).Text & ", "
            If i = 1 Then
                strRecSel = strAllRec
                Exit For
            End If
        End If
    Next i
    If msg <> "" Then
        If Right$(strRecSel, 1) = "," Then strRecSel = Left$(strRecSel, Len(strRecSel) - 1)
        msg = Left$(msg, Len(msg) - 2)
        msg = strMeCaption & ". Dpto: " & cboDptos.Text & ". Recursos: " & msg
    Else
        msg = strMeCaption & ". Dpto: " & cboDptos.Text
    End If
    Me.Caption = msg
End Sub

Private Sub pCargarActCitadas(intCol%)
    Dim sql$, qry As rdoQuery, rs As rdoResultset, qry1 As rdoQuery, rs1 As rdoResultset
    Static sqlOrder$
    Dim strFecEntrCola$, intRecita%
    
    Screen.MousePointer = vbHourglass
    
    'Query para ver si la Cita tiene registros de recita
    sql = "SELECT COUNT(PR04NUMACTPLAN) FROM CI0100"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND CI01SITCITA = ?"
    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
    qry1(1) = constESTCITA_RECITADA
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY PR0400.PR37CODESTADO, TRUNC(NVL(PR04FECENTRCOLA,TO_DATE('1/1/1900','DD/MM/YYYY'))), CI01FECCONCERT, PAC" 'Opci�n por defecto
    Else
        Select Case UCase(grdCitadas.Columns(intCol).Caption)
        Case UCase("Fecha Cita"): sqlOrder = " ORDER BY CI01FECCONCERT, PAC"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, CI01FECCONCERT"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PAC, CI01FECCONCERT"
        Case UCase("Actuaci�n"): sqlOrder = " ORDER BY PR01DESCORTA, CI01FECCONCERT"
        Case UCase("Recurso"): sqlOrder = " ORDER BY AG11DESRECURSO, CI01FECCONCERT, PAC"
        Case UCase("Estado"): sqlOrder = " ORDER BY PR0400.PR37CODESTADO, TRUNC(NVL(PR04FECENTRCOLA,TO_DATE('1/1/1900','DD/MM/YYYY'))), CI01FECCONCERT, PAC"
        Case Else: Exit Sub
        End Select
    End If
    
    grdCitadas.RemoveAll
    grdCitadas.ScrollBars = ssScrollBarsNone

    sql = "SELECT CI2200.CI21CODPERSONA, CI2200.CI22NUMHISTORIA,"
    sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC,"
    sql = sql & " PR0400.PR04NUMACTPLAN, PR0400.PR37CODESTADO, PR04FECENTRCOLA,"
    sql = sql & " PR0400.PR01CODACTUACION, PR0100.PR01DESCORTA, PR0100.PR12CODACTIVIDAD,"
    sql = sql & " CI0100.CI01FECCONCERT, DECODE(CI01INDASIG,'0','1','0') CI01INDASIG,"
    sql = sql & " AG1100.AG11CODRECURSO, AG1100.AG11DESRECURSO"
    sql = sql & " FROM PR0400, PR0100, CI2200, AG1100, CI0100"
    sql = sql & " WHERE CI0100.CI01FECCONCERT BETWEEN TO_DATE(?,'DD/MM/YYYY') AND TO_DATE(?,'DD/MM/YYYY')"
    sql = sql & " AND CI0100.CI01SITCITA = ?"
    sql = sql & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
    If chkVerTodas.Value = 0 Then
        grdCitadas.Caption = "Actuaciones citadas"
        sql = sql & " AND PR0400.PR37CODESTADO IN (" & constESTACT_CITADA & ")"
        sql = sql & " AND PR04FECENTRCOLA IS NULL"
    Else
        grdCitadas.Caption = "Actuaciones citadas, en cola, realiz�ndose y realizadas"
        sql = sql & " AND PR0400.PR37CODESTADO IN (" & constESTACT_CITADA & "," & constESTACT_REALIZANDOSE & "," & constESTACT_REALIZADA & ")"
    End If
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    sql = sql & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    sql = sql & " AND AG1100.AD02CODDPTO = ?"
    sql = sql & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format(dcboFecDesde.Text, "dd/mm/yyyy")
    qry(1) = Format(DateAdd("d", 1, dcboFecHasta.Text), "dd/mm/yyyy")
    qry(2) = constESTCITA_CITADA
    qry(3) = cboDptos.Columns("Cod").Text
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        If Not IsNull(rs!PR04FECENTRCOLA) Then strFecEntrCola = rs!PR04FECENTRCOLA Else strFecEntrCola = ""
        qry1(0) = rs!PR04NUMACTPLAN
        Set rs1 = qry1.OpenResultset()
        If rs1(0) > 0 Then intRecita = 1 Else intRecita = 0
        grdCitadas.AddItem rs!CI21CODPERSONA & Chr(9) & _
                            rs!PR04NUMACTPLAN & Chr(9) & _
                            rs!PR01CODACTUACION & Chr(9) & _
                            rs!PR12CODACTIVIDAD & Chr(9) & _
                            Format(rs!CI01FECCONCERT, "dd/mm/yyyy hh:mm") & Chr(9) & _
                            rs!CI22NUMHISTORIA & Chr(9) & _
                            rs!PAC & Chr(9) & _
                            rs!PR01DESCORTA & Chr(9) & _
                            rs!AG11DESRECURSO & Chr(9) & _
                            rs!AG11CODRECURSO & Chr(9) & _
                            fEstado(rs!PR37CODESTADO, strFecEntrCola) & Chr(9) & _
                            intRecita & Chr(9) & _
                            rs!CI01INDASIG
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    grdCitadas.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarActPendRecitar(intCol%)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Static sqlOrder$
    
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY CI01FECCONCERT, PAC" 'Opci�n por defecto
    Else
        Select Case UCase(grdPendientes.Columns(intCol).Caption)
        Case UCase("Fecha Cita"): sqlOrder = " ORDER BY CI01FECCONCERT, PAC"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, CI01FECCONCERT"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PAC, CI01FECCONCERT"
        Case UCase("Actuaci�n"): sqlOrder = " ORDER BY PR01DESCORTA, CI01FECCONCERT"
        Case UCase("Recurso"): sqlOrder = " ORDER BY AG11DESRECURSO, CI01FECCONCERT, PAC"
        Case UCase("Fecha Pend."): sqlOrder = " ORDER BY CI01FECUPD, CI01FECCONCERT, PAC"
        Case UCase("Usuario Pend."): sqlOrder = " ORDER BY USERUPD, CI01FECCONCERT, PAC"
        Case Else: Exit Sub
        End Select
    End If
    
    grdPendientes.RemoveAll
    grdPendientes.ScrollBars = ssScrollBarsNone

    sql = "SELECT /*+ ORDERED INDEX(CI0100 CI0103)*/"
    sql = sql & " CI2200.CI21CODPERSONA, CI2200.CI22NUMHISTORIA,"
    sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC,"
    sql = sql & " PR0400.PR04NUMACTPLAN,"
    sql = sql & " PR0400.PR01CODACTUACION, PR0100.PR01DESCORTA, PR0100.PR12CODACTIVIDAD,"
    sql = sql & " CI0100.CI31NUMSOLICIT, CI0100.CI01NUMCITA, CI0100.CI01FECCONCERT,"
    sql = sql & " AG1100.AG11CODRECURSO, AG1100.AG11DESRECURSO,"
    sql = sql & " CI01FECUPD, DECODE(CI0100.SG02COD_UPD,'?????',CI0100.SG02COD_UPD, SG02APE1||' '||SG02APE2||', '||SG02NOM) USERUPD,"
    sql = sql & " CI0100.CI01OBSERV"
    sql = sql & " FROM CI0100, SG0200, PR0400, PR0100, CI2200, AG1100"
    sql = sql & " WHERE CI0100.CI01SITCITA = ?" 'Para todas las fechas
    sql = sql & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
    sql = sql & " AND SG0200.SG02COD (+)= CI0100.SG02COD_UPD"
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
    sql = sql & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    sql = sql & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    sql = sql & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    sql = sql & " AND AG1100.AD02CODDPTO = ?"
    sql = sql & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = constESTCITA_PENDRECITAR
    qry(1) = cboDptos.Columns("Cod").Text
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        grdPendientes.AddItem (rs!CI21CODPERSONA & Chr(9) & _
                    rs!PR04NUMACTPLAN & Chr(9) & _
                    rs!PR01CODACTUACION & Chr(9) & _
                    rs!PR12CODACTIVIDAD & Chr(9) & _
                    Format(rs!CI01FECCONCERT, "dd/mm/yyyy hh:mm") & Chr(9) & _
                    rs!CI22NUMHISTORIA & Chr(9) & _
                    rs!PAC & Chr(9) & _
                    rs!PR01DESCORTA & Chr(9) & _
                    rs!AG11DESRECURSO & Chr(9) & _
                    rs!AG11CODRECURSO & Chr(9) & _
                    rs!CI31NUMSOLICIT & Chr(9) & _
                    rs!CI01NUMCITA & Chr(9) & _
                    Format(rs!CI01FECUPD, "dd/mm/yyyy hh:mm") & Chr(9) & _
                    rs!USERUPD & Chr(9) & _
                    rs!CI01OBSERV)
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    grdPendientes.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarDptos()
    Dim sql$, rs As rdoResultset, qry As rdoQuery
    
    sql = "SELECT AD0200.AD02CODDPTO, AD0200.AD02DESDPTO"
    sql = sql & " FROM AD0200, AD0300"
    sql = sql & " WHERE AD0300.SG02COD = ?"
    sql = sql & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN, TO_DATE('31/12/9999','DD/MM/YYYY'))"
    sql = sql & " AND AD0200.AD02CODDPTO = AD0300.AD02CODDPTO "
    sql = sql & " AND SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN, TO_DATE('31/12/9999','DD/MM/YYYY'))"
    sql = sql & " ORDER BY AD0200.AD02DESDPTO"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objSecurity.strUser
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboDptos.AddItem rs(0) & Chr$(9) & rs(1)
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    cboDptos.MoveFirst
    cboDptos.Text = cboDptos.Columns("Desig").Text
    intDptoSel = cboDptos.Columns("Cod").Text
End Sub

Public Sub pCargarRecursos(intDptoSel%)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim item As ListItem
    
    'se cargan los recursos del Dpto. seleccionado
    lvwRec.ListItems.Clear
    
    strAllRec = ""
    sql = "SELECT AG11CODRECURSO, AG11DESRECURSO"
    sql = sql & " FROM AG1100"
    sql = sql & " WHERE AD02CODDPTO = ?"
    sql = sql & " AND SYSDATE BETWEEN AG11FECINIVREC AND NVL(AG11FECFINVREC,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    sql = sql & " ORDER BY AG11ORDEN, AG11DESRECURSO"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        Set item = lvwRec.ListItems.Add(, , "TODOS")
        item.Tag = 0
        item.Selected = True
        cmdAgendaRec.Enabled = False
        Do While Not rs.EOF
            strAllRec = strAllRec & rs!AG11CODRECURSO & ","
            Set item = lvwRec.ListItems.Add(, , rs!AG11DESRECURSO)
            item.Tag = rs!AG11CODRECURSO
            rs.MoveNext
        Loop
        strAllRec = Left$(strAllRec, Len(strAllRec) - 1)
    End If
    rs.Close
    qry.Close
End Sub

Private Sub pCitar()
    Dim strNAPlan$, grdGrid As SSDBGrid, i%, sql$
    Dim strCodPers$
    
    'se mira si hay alguna actuaci�n seleccionada
    If grdCitadas.SelBookmarks.Count > 0 Then
        Set grdGrid = grdCitadas
        'se comprueba que todas las actuaciones seleccionadas puedan ser citadas/recitadas (estado citada)
        For i = 0 To grdGrid.SelBookmarks.Count - 1
            If grdGrid.Columns("Estado").CellText(grdGrid.SelBookmarks(i)) <> "Citada" Then
                sql = "Alguna de las actuaciones seleccionadas ya ha entrado en cola,"
                sql = sql & " est� realiz�ndose o ha sido realizada y no puede anularse."
                sql = sql & Chr$(13)
                sql = sql & "Seleccione s�lo actuaciones en estado 'Citada'."
                MsgBox sql, vbExclamation, strMeCaption
                Exit Sub
            End If
        Next i
    ElseIf grdPendientes.SelBookmarks.Count > 0 Then
        Set grdGrid = grdPendientes
        'siempre se pueden citar/recitar
    Else
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If

    'si hay m�s de una actuaci�n seleccionada se mira que correspondan al mismo paciente
    strCodPers = grdGrid.Columns("CodPersona").CellValue(grdGrid.SelBookmarks(0))
    If grdGrid.SelBookmarks.Count > 1 Then
        For i = 1 To grdGrid.SelBookmarks.Count - 1
            If strCodPers <> grdGrid.Columns("CodPersona").CellValue(grdGrid.SelBookmarks(i)) Then
                MsgBox "Debe seleccionar un �nico Paciente.", vbExclamation, strMeCaption
                Exit Sub
            End If
        Next i
    End If

    'se pasa a citar o recitar las actuaciones
    For i = 0 To grdGrid.SelBookmarks.Count - 1
        strNAPlan = strNAPlan & grdGrid.Columns("NumActPlan").CellValue(grdGrid.SelBookmarks(i)) & ","
    Next i
    Call pCitarActuaciones(strNAPlan)
    Call pCargarActCitadas(-1)
    Call pCargarActPendRecitar(-1)
End Sub


Private Sub pOverbooking()
    Dim strNAPlan$, grdGrid As SSDBGrid, i%, sql$
    Dim strCodPers$
    
    'se mira si hay alguna actuaci�n seleccionada
    If grdCitadas.SelBookmarks.Count > 0 Then
        Set grdGrid = grdCitadas
        'se comprueba que todas las actuaciones seleccionadas puedan ser citadas/recitadas (estado citada)
        For i = 0 To grdGrid.SelBookmarks.Count - 1
            If grdGrid.Columns("Estado").CellText(grdGrid.SelBookmarks(i)) <> "Citada" Then
                sql = "Alguna de las actuaciones seleccionadas ya ha entrado en cola,"
                sql = sql & " est� realiz�ndose o ha sido realizada y no puede anularse."
                sql = sql & Chr$(13)
                sql = sql & "Seleccione s�lo actuaciones en estado 'Citada'."
                MsgBox sql, vbExclamation, strMeCaption
                Exit Sub
            End If
        Next i
    ElseIf grdPendientes.SelBookmarks.Count > 0 Then
        Set grdGrid = grdPendientes
        'siempre se pueden citar/recitar
    Else
        sql = "No se ha seleccionado ninguna Actuaci�n."
        MsgBox sql, vbExclamation, strMeCaption
        Exit Sub
    End If

''    'si hay m�s de una actuaci�n seleccionada se mira que correspondan al mismo paciente
''    strCodPers = grdGrid.Columns("CodPersona").CellValue(grdGrid.SelBookmarks(0))
''    If grdGrid.SelBookmarks.Count > 1 Then
''        For i = 1 To grdGrid.SelBookmarks.Count - 1
''            If strCodPers <> grdGrid.Columns("CodPersona").CellValue(grdGrid.SelBookmarks(i)) Then
''                MsgBox "Debe seleccionar un �nico Paciente.", vbExclamation, strMeCaption
''                Exit Sub
''            End If
''        Next i
''    End If

    'se pasa a citar o recitar las actuaciones
    For i = 0 To grdGrid.SelBookmarks.Count - 1
        strNAPlan = strNAPlan & grdGrid.Columns("NumActPlan").CellValue(grdGrid.SelBookmarks(i)) & ","
    Next i
    Call pCitarOverbooking(strNAPlan)
    Call pCargarActCitadas(-1)
    Call pCargarActPendRecitar(-1)
End Sub

Private Sub pFormatearControles()
    strMeCaption = "Citas por recurso"
    dcboFecDesde.BackColor = objApp.objUserColor.lngMandatory
    dcboFecHasta.BackColor = objApp.objUserColor.lngMandatory
    cboDptos.BackColor = objApp.objUserColor.lngMandatory
    With grdCitadas
        .Columns(0).Caption = "CodPersona"
        .Columns(0).Visible = False
        .Columns(1).Caption = "NumActPlan"
        .Columns(1).Visible = False
        .Columns(2).Caption = "CodAct"
        .Columns(2).Visible = False
        .Columns(3).Caption = "TipoAct"
        .Columns(3).Visible = False
        .Columns(4).Caption = "Fecha Cita"
        .Columns(4).Width = 1450
        .Columns(5).Caption = "N� Hist."
        .Columns(5).Width = 700
        .Columns(5).Alignment = ssCaptionAlignmentRight
        .Columns(6).Caption = "Paciente"
        .Columns(6).Width = 3000
        .Columns(7).Caption = "Actuaci�n"
        .Columns(7).Width = 2000
        .Columns(8).Caption = "Recurso"
        .Columns(8).Width = 1600
        .Columns(9).Caption = "CodRecurso"
        .Columns(9).Visible = False
        .Columns(10).Caption = "Estado"
        .Columns(10).Width = 1100
        .Columns(11).Caption = "Recita"
        .Columns(11).style = ssStyleCheckBox
        .Columns(11).Width = 600
        .Columns(12).Name = "Overbooking"
        .Columns(12).Caption = "Cita manual"
        .Columns(12).style = ssStyleCheckBox
        .Columns(12).Width = 1000
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    With grdPendientes
        .Columns(0).Caption = "CodPersona"
        .Columns(0).Visible = False
        .Columns(1).Caption = "NumActPlan"
        .Columns(1).Visible = False
        .Columns(2).Caption = "CodAct"
        .Columns(2).Visible = False
        .Columns(3).Caption = "TipoAct"
        .Columns(3).Visible = False
        .Columns(4).Caption = "Fecha Cita"
        .Columns(4).Width = 1450
        .Columns(5).Caption = "N� Hist."
        .Columns(5).Width = 700
        .Columns(5).Alignment = ssCaptionAlignmentRight
        .Columns(6).Caption = "Paciente"
        .Columns(6).Width = 3000
        .Columns(7).Caption = "Actuaci�n"
        .Columns(7).Width = 2000
        .Columns(8).Caption = "Recurso"
        .Columns(8).Width = 1800
        .Columns(9).Caption = "CodRecurso"
        .Columns(9).Visible = False
        .Columns(10).Caption = "NSol"
        .Columns(10).Visible = False
        .Columns(11).Caption = "NCita"
        .Columns(11).Visible = False
        .Columns(12).Caption = "Fecha Pend."
        .Columns(12).Width = 1450
        .Columns(13).Caption = "Usuario Pend."
        .Columns(13).Width = 2000
        .Columns(14).Caption = "Observaciones Pend."
        .Columns(14).Width = 5000
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    lvwRec.ColumnHeaders.Add , , , 2000
End Sub

Private Sub pImprimirCartas(lngActPlan&, strMensaje$)
    Dim formulas(1, 2)
    formulas(1, 1) = "motivo"
    formulas(1, 2) = "'" & strMensaje & "'"
    Call Imprimir_API("PR0465J.PR04NUMACTPLAN = " & lngActPlan, "CARTAS.RPT", formulas)
End Sub

Private Sub pRecitarManual(lngNAPlan&, strCodAct$, strNSol$, intNCita%, strFechaCita$, strCodRec$, strObserv$)
    Dim sql$, qry As rdoQuery, rs As rdoResultset

    On Error Resume Next
    
    'Se actualiza el estado de la cita antigua a RECITADA
    sql = "UPDATE CI0100 SET CI01SITCITA = '" & constESTCITA_RECITADA & "'"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND (CI01SITCITA = '" & constESTCITA_CITADA & "'"
    sql = sql & " OR CI01SITCITA = '" & constESTCITA_PENDRECITAR & "')"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNAPlan
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Sub

    'PR0400: estado de la actuaci�n
    sql = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNAPlan
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Sub
    
    'CI0100: a�adir cita
    sql = "INSERT INTO CI0100 (CI31NUMSOLICIT, CI01NUMCITA, AG11CODRECURSO, PR04NUMACTPLAN,"
    sql = sql & " CI01FECCONCERT, CI01SITCITA, CI01INDRECORDA, CI01TIPRECORDA, CI01NUMENVRECO,"
    sql = sql & " CI01INDLISESPE, CI01INDASIG, CI01OBSERV)"
    sql = sql & " VALUES (?, ?, ?, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'), ?, -1, 1, 1, 0, -1, ?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNSol
    qry(1) = intNCita
    qry(2) = strCodRec
    qry(3) = lngNAPlan
    qry(4) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
    qry(5) = constESTCITA_CITADA
    If strObserv <> "" Then qry(6) = strObserv Else qry(6) = Null
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Sub

    'CI1500: a�adir fase citada
    sql = "INSERT INTO CI1500 (CI31NUMSOLICIT, CI01NUMCITA, CI15FECCONCPAC, CI15NUMFASECITA,"
    sql = sql & " CI15DESFASECITA, CI15NUMDIASPAC, CI15NUMHORAPAC,"
    sql = sql & " CI15NUMMINUPAC)"
    sql = sql & " SELECT ?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI'), PR05NUMFASE,"
    sql = sql & " PR05DESFASE, FLOOR(PR05NUMOCUPACI/1440), FLOOR(MOD(PR05NUMOCUPACI,1440)/60),"
    sql = sql & " MOD(MOD(PR05NUMOCUPACI,1440),60)"
    sql = sql & " FROM PR0500"
    sql = sql & " WHERE PR01CODACTUACION = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNSol
    qry(1) = intNCita
    qry(2) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
    qry(3) = strCodAct
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Sub

    'CI2700: A�adir recurso citado
    sql = "INSERT INTO CI2700 (CI31NUMSOLICIT, CI01NUMCITA, CI15NUMFASECITA, AG11CODRECURSO,"
    sql = sql & " CI27FECOCUPREC, CI27NUMDIASREC, CI27NUMHORAREC, CI27NUMMINUREC)"
    sql = sql & " SELECT ?, ?, PR05NUMFASE, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'),"
    sql = sql & " FLOOR(PR13NUMTIEMPREC/1440), FLOOR(MOD(PR13NUMTIEMPREC,1440)/60),"
    sql = sql & " MOD(MOD(PR13NUMTIEMPREC,1440),60)"
    sql = sql & " FROM PR1300"
    sql = sql & " WHERE PR01CODACTUACION = ?"
    sql = sql & " AND PR13INDPREFEREN = -1"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNSol
    qry(1) = intNCita
    qry(2) = strCodRec
    qry(3) = Format(strFechaCita, "dd/mm/yyyy hh:mm")
    qry(4) = strCodAct
    qry.Execute
    qry.Close
    If Err > 0 Then Exit Sub
    
    On Error GoTo 0
End Sub

Private Sub pVaciarGrids()
    grdPendientes.RemoveAll
    grdCitadas.RemoveAll
End Sub

Private Sub pVolverCitada(lngNAPlan$)
    Dim sql$, qry As rdoQuery
    
    sql = "UPDATE CI0100 SET CI01SITCITA = ?"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    sql = sql & " AND CI01SITCITA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = constESTCITA_CITADA
    qry(1) = lngNAPlan
    qry(2) = constESTCITA_PENDRECITAR
    qry.Execute
    qry.Close
    sql = "UPDATE PR0400 SET PR37CODESTADO = ?"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = constESTACT_CITADA
    qry(1) = lngNAPlan
    qry.Execute
    qry.Close
End Sub
