Attribute VB_Name = "modLlamadas"
Option Explicit

Sub MostrarCitasPac(nPers As Long)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
  ' Primero de muestran los datos del paciente
  sql = "SELECT CI2200.CI22NumHistoria, CI2200.CI22FecNacim, " _
      & "CI2200.CI22PriApel||' '||CI2200.CI22SegApel||', '||CI2200.CI22Nombre " _
      & "FROM CI2200 WHERE " _
      & "CI2200.CI21CodPersona = ? "
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nPers
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    Screen.MousePointer = 0
    MsgBox "No se ha encontrado al paciente.", vbInformation, "Citas del paciente"
    Exit Sub
  End If
  Load frmCitasPaciente
  With frmCitasPaciente
    .nPers = nPers
    .txtHistoria.BackColor = reColor.constGris
    .txtHistoria.Locked = True
    If IsNull(rdo(0)) Then .txtHistoria.Text = "" Else .txtHistoria.Text = rdo(0)
    If IsNull(rdo(1)) Then .txtFNacim.Text = "" Else .txtFNacim.Text = rdo(1)
    .txtPaciente.Text = rdo(2)
    .cmdBuscar.Enabled = False
    .cmdSalir.TabIndex = 0
    Screen.MousePointer = vbHourglass
    Call .pSinCitar(-1)
    Call .pCitadas(-1)
    Call .pPendientes(-1)
    Call .pMensajes
    Screen.MousePointer = vbDefault
    .Show vbModal
  End With
End Sub

Sub MostrarCitasRecurso(intCR As Integer, intCDpto As Integer)
Dim inti As Integer
Dim item As ListItem
Screen.MousePointer = vbHourglass
Load frmCitasRecurso
Do While frmCitasRecurso.cboDptos.Columns(0).Value <> intCDpto
    frmCitasRecurso.cboDptos.MoveNext
    inti = inti + 1
    If inti > 300 Then Exit Do
Loop
frmCitasRecurso.cboDptos.Text = frmCitasRecurso.cboDptos.Columns(1).Value
frmCitasRecurso.pCargarRecursos (frmCitasRecurso.cboDptos.Columns(0).Value)
With frmCitasRecurso
    .lvwRec.ListItems(1).Selected = False
    For Each item In .lvwRec.ListItems
    If item.Tag = intCR Then
        .lvwRec.ListItems(item.Index).Selected = True
        Exit For
    End If
    Next item
    Screen.MousePointer = Default
    .Show vbModal
End With
End Sub
