VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmCodigosPostales 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "C�digos postales"
   ClientHeight    =   5760
   ClientLeft      =   1785
   ClientTop       =   1575
   ClientWidth     =   5025
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5760
   ScaleMode       =   0  'Usuario
   ScaleWidth      =   76.137
   Begin VB.TextBox txtprovincia 
      Height          =   285
      Left            =   840
      TabIndex        =   10
      Top             =   60
      Width           =   2355
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Height          =   285
      Left            =   3720
      TabIndex        =   7
      Top             =   60
      Width           =   1230
   End
   Begin VB.TextBox txtcalle 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   840
      MaxLength       =   40
      TabIndex        =   4
      Top             =   720
      Width           =   4095
   End
   Begin VB.TextBox txtlocalidaddir 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   840
      MaxLength       =   30
      TabIndex        =   3
      Top             =   375
      Width           =   4095
   End
   Begin VB.CommandButton cmdcancelar 
      Caption         =   "&Cancelar"
      Height          =   555
      Left            =   3780
      TabIndex        =   1
      Top             =   5160
      Width           =   1155
   End
   Begin VB.CommandButton cmdaceptar 
      Caption         =   "&Aceptar"
      Height          =   555
      Left            =   120
      TabIndex        =   0
      Top             =   5160
      Width           =   1155
   End
   Begin ComctlLib.ListView lvwCodPostal 
      Height          =   4035
      Left            =   60
      TabIndex        =   2
      Top             =   1020
      Width           =   4875
      _ExtentX        =   8599
      _ExtentY        =   7117
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Calle"
         Object.Width           =   5239
      EndProperty
      BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   1
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Localidad"
         Object.Width           =   5239
      EndProperty
      BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Alignment       =   2
         SubItemIndex    =   2
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Cod Postal"
         Object.Width           =   2328
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo cboprovdir 
      Height          =   285
      Left            =   840
      TabIndex        =   5
      Top             =   60
      Width           =   2385
      DataFieldList   =   "Column 1"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "cod"
      Columns(0).Name =   "cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   3200
      Columns(1).Caption=   "des"
      Columns(1).Name =   "des"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      _ExtentX        =   4207
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCalle 
      AutoSize        =   -1  'True
      Caption         =   "Calle:"
      Height          =   195
      Left            =   60
      TabIndex        =   9
      Top             =   720
      Width           =   390
   End
   Begin VB.Label lblLocalidad 
      AutoSize        =   -1  'True
      Caption         =   "Localidad:"
      Height          =   195
      Left            =   60
      TabIndex        =   8
      Top             =   420
      Width           =   735
   End
   Begin VB.Label lblLabel 
      AutoSize        =   -1  'True
      Caption         =   "Provincia:"
      Height          =   195
      Index           =   22
      Left            =   60
      TabIndex        =   6
      Top             =   120
      Width           =   705
   End
End
Attribute VB_Name = "frmCodigosPostales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strCalle
Dim intTipPobla As Integer

Dim lngCodProv As Long
Dim strLocalidad As String
Dim Item As ListItem



Private Sub pcargarcombos(sql As String, con As SSDBCombo)
    
Dim rs As rdoResultset
Set rs = objApp.rdoConnect.OpenResultset(sql)
    Do While Not rs.EOF
    con.AddItem rs(0) & Chr(9) & rs(1)
    rs.MoveNext
   Loop
End Sub

Private Sub pSelecionar()
Dim sql As String
Dim rs As rdoResultset
Dim vntData(0 To 3)
Dim I As Integer
'Dim item As ListItem
    If fseleccion(I) Then
        
       

        If txtcalle.Visible = True Then
           
            vntData(0) = UCase(txtlocalidaddir.Text)
          
            If lvwCodPostal.ListItems(I) <> UCase(txtlocalidaddir.Text) Then
                vntData(1) = lvwCodPostal.ListItems(I)
                Call objPipe.PipeSet("CALLE", vntData(1))
            End If
            If Item.SubItems(1) <> "" Then
                vntData(2) = Item.SubItems(1)
                Call objPipe.PipeSet("CODPOSTAL", vntData(2))
            End If
            
        Else 'se entra por localidad
'            If blnNuevaProv Then
'                vntData(0) = UCase(txtlocalidaddir.Text)
'            Else
                vntData(0) = lvwCodPostal.ListItems(I)
'            End If
            Call objPipe.PipeSet("LOCALIDAD", vntData(0))
            sql = "SELECT COUNT(*) CI07CODPOSTAL FROM CI0700 WHERE CI07DESPOBLA='" & strLocalidad & "' AND CI26CODPROVI=" & lngCodProv
            Set rs = objApp.rdoConnect.OpenResultset(sql)
            If rs(0) = 1 Then
                sql = "SELECT CI07CODPOSTAL FROM CI0700 WHERE CI07DESPOBLA='" & strLocalidad & "' AND CI26CODPROVI=" & lngCodProv
                Set rs = objApp.rdoConnect.OpenResultset(sql)
                vntData(2) = rs(0)
                Call objPipe.PipeSet("CODPOSTAL", vntData(2))
            End If
        End If
        vntData(3) = cboprovdir.Columns(0).Value
        Call objPipe.PipeSet("PROVINCIA", vntData(3))
        
        Unload Me
    End If

End Sub

Private Sub cmdSalir_Click()
    Unload Me
    
End Sub


Private Sub cboprovdir_Change()
    lngCodProv = cboprovdir.Columns(0).Value
    Call objPipe.PipeSet("PROVINCIA", lngCodProv)
End Sub

Private Sub cboprovdir_Click()
    lngCodProv = cboprovdir.Columns(0).Value
    Call objPipe.PipeSet("PROVINCIA", lngCodProv)
End Sub

Private Sub cmdAceptar_Click()
Call pSelecionar
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub


Private Sub cmdConsultar_Click()
Dim I As Integer
Dim sql As String
Dim rs As rdoResultset
'Dim item As ListItem
lvwCodPostal.ListItems.Clear
lngCodProv = cboprovdir.Columns(0).Value
strLocalidad = UCase(txtlocalidaddir.Text)
If Not (txtcalle.Visible) Then ' Esta selecionando por localidad
    If Trim(strLocalidad) <> "" Then
        sql = "SELECT DISTINCT CI07DESPOBLA FROM CI0700 WHERE CI26CODPROVI =" & lngCodProv & " AND " _
        & "     CI07DESPOBLA LIKE '%" & strLocalidad & "%' "
    Else
        sql = "SELECT distinct CI07DESPOBLA FROM CI0700 WHERE CI26CODPROVI =" & lngCodProv
    End If
        
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    If rs.EOF Then
        
        MsgBox "No se tiene codificado la localidad", vbExclamation, Me.Caption
        Exit Sub
    End If
    Do While Not rs.EOF
        Set Item = lvwCodPostal.ListItems.Add(, , rs(0))
'        If rs(2) <> 0 Then
'            If Not IsNull(rs(1)) Then Item.SubItems(1) = rs(1)
'        Else
'            Item.SubItems(1) = ""
'        End If
        rs.MoveNext
    Loop
    rs.Close
Else 'se seleciona por calle
strCalle = UCase(txtcalle.Text)
    If Trim(strCalle) <> "" Then
        sql = "SELECT DISTINCT CI07CALLE, CI07CODPOSTAL FROM CI0700 WHERE CI07CALLE LIKE '%" & strCalle & "%' " _
        & " AND CI26CODPROVI =" & lngCodProv & " AND CI07DESPOBLA='" & strLocalidad & "'"
    Else
        sql = "SELECT CI07CALLE,CI07CODPOSTAL FROM CI0700 WHERE CI07DESPOBLA='" & strLocalidad & "' AND CI26CODPROVI=" & lngCodProv
        Set rs = objApp.rdoConnect.OpenResultset(sql)
        If rs.EOF Then
            MsgBox "No se tiene codificado la calle", vbExclamation, Me.Caption
            Exit Sub
        End If
        If Trim(rs(0)) <> "" Then
            Do While Not rs.EOF
                Set Item = lvwCodPostal.ListItems.Add(, , rs(0))
                Item.SubItems(1) = rs(1)
                rs.MoveNext
                
            Loop
            Exit Sub
        Else
            Do While Not rs.EOF
                Set Item = lvwCodPostal.ListItems.Add(, , strLocalidad)
                Item.SubItems(1) = rs(1)
            rs.MoveNext
            Loop
            Exit Sub
        End If
    End If
        Set rs = objApp.rdoConnect.OpenResultset(sql)
        If rs.EOF Then
            MsgBox "No se tiene codificado el C�digo Postal", vbExclamation, Me.Caption
        Exit Sub
        End If
        Do While Not rs.EOF
            Set Item = lvwCodPostal.ListItems.Add(, , rs(0))
            If Not IsNull(rs(1)) Then
                Item.SubItems(1) = rs(1)
            Else
                Item.SubItems(1) = ""
            End If
            rs.MoveNext
        Loop
        rs.Close
    
End If
End Sub

Private Sub Form_Load()
Dim strCodAyuda As String
Dim sql As String
Dim strProv As String
If objPipe.PipeExist("CODPROVINCIA") Then
    
    sql = "SELECT CI26CODPROVI,CI26DESPROVI FROM CI2600 ORDER BY CI26DESPROVI"
    Call pcargarcombos(sql, cboprovdir)
    cboprovdir.MoveFirst
    strProv = objPipe.PipeGet("CODPROVINCIA")
    Do While cboprovdir.Columns(1).Value <> strProv
        strCodAyuda = cboprovdir.Columns(1).Value
        lngCodProv = cboprovdir.Columns(0).Value
        cboprovdir.MoveNext
        If strCodAyuda = cboprovdir.Columns(1).Value Then Exit Do
    Loop
    cboprovdir.Text = cboprovdir.Columns(1).Value
    Call objPipe.PipeRemove("CODPROVINCIA")
End If
If objPipe.PipeExist("LOCALIDAD") Then
    strLocalidad = objPipe.PipeGet("LOCALIDAD")
    txtlocalidaddir.Text = strLocalidad
Call objPipe.PipeRemove("LOCALIDAD")
End If
If objPipe.PipeExist("CALLE") Then
    lvwCodPostal.ColumnHeaders.Remove (2)
   strCalle = objPipe.PipeGet("CALLE")
   txtprovincia.Visible = True
   txtprovincia.BackColor = &HC0C0C0
   txtlocalidaddir.BackColor = &HC0C0C0
   txtprovincia.Text = cboprovdir.Columns(1).Text
   cboprovdir.Visible = False
   txtprovincia.Locked = True
   txtcalle.Visible = True
   txtcalle.Text = strCalle
   cboprovdir.Enabled = False
    txtlocalidaddir.Locked = True
    Call objPipe.PipeRemove("CALLE")
Else
     txtprovincia.Visible = False
     
     cboprovdir.Visible = True
   'txtprovincia.Locked = True
    cboprovdir.Enabled = True
    txtlocalidaddir.BackColor = &HFFFFFF
    txtlocalidaddir.Locked = False
    lvwCodPostal.Top = 720
   lvwCodPostal.Height = 4395
   lvwCodPostal.ColumnHeaders.Remove (1)
   txtcalle.Visible = False
   lvwCodPostal.Height = 4035
   lblCalle.Visible = False
End If
lvwCodPostal.ColumnHeaders.Add , , , 2000

'End If

'End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    txtcalle.Visible = False
    lvwCodPostal.Height = 4035
    
End Sub

Private Sub lvwcodpostal_DblClick()
Call pSelecionar
End Sub
Private Function fseleccion(I As Integer) As Boolean
'Dim i As Integer
    For I = 1 To lvwCodPostal.ListItems.Count
        If lvwCodPostal.ListItems(I).Selected Then
            fseleccion = True
            Exit Function
        End If
    Next
End Function

Private Sub lvwCodPostal_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    Call pSelecionar
End If
End Sub





Private Sub txtcalle_GotFocus()
txtcalle.SelStart = 0
txtcalle.SelLength = Len(Trim(txtlocalidaddir.Text))

End Sub

Private Sub txtcalle_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 And txtcalle.Visible = True Then cmdConsultar_Click
End Sub

Private Sub txtlocalidaddir_GotFocus()
txtlocalidaddir.SelStart = 0
txtlocalidaddir.SelLength = Len(Trim(txtlocalidaddir.Text))

End Sub

Private Sub txtlocalidaddir_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And txtcalle.Visible = False Then cmdConsultar_Click
    
End Sub

Private Sub txtprovincia_GotFocus()
txtprovincia.SelStart = 0
txtprovincia.SelLength = Len(Trim(txtprovincia.Text))

End Sub
