Attribute VB_Name = "modConstantes"
Option Explicit

'Constantes de los estados de las actuaciones
Public Const constESTACT_PLANIFICADA = 1
Public Const constESTACT_CITADA = 2
Public Const constESTACT_REALIZANDOSE = 3
Public Const constESTACT_REALIZADA = 4
Public Const constESTACT_INFORMADA = 5
Public Const constESTACT_CANCELADA = 6

'Constantes de los estados de las citas
Public Const constESTCITA_CITADA = 1
Public Const constESTCITA_ANULADA = 2
Public Const constESTCITA_RECITADA = 3
Public Const constESTCITA_PENDRECITAR = 4
Public Const constESTCITA_RESERVADA = 5

'Constantes para los tipos de actividades
Public Const constACTIV_CONSULTA = 201
Public Const constACTIV_PRUEBA = 203
Public Const constACTIV_INTERVENCION = 207
Public Const constACTIV_HOSPITALIZACION = 209
Public Const constACTIV_CONTCONSULTA = 214
Public Const constACTIV_INFORME = 215

'Constantes de los Departamentos
Public Const constDPTO_QUIROFANO = 213
Public Const constDPTO_ANESTESIA = 223
Public Const constDPTO_URGENCIAS = 216
Public Const constDPTO_RAYOS = 209
Public Const constDPTO_REHABILITACION = 211
Public Const constDPTO_ADMISION = 3


' Colores
Public Enum reColor
  constAzulClaro = 16776960
  constNegro = 0
  constBlanco = 16777215
  constAmarilloClaro = 12648447
  constAmarilloOscuro = 65535
  constVerde = 9895830
  constVerdeOscuro = 46080
  constRojo = 255
  constRojoClaro = 45055
  constAzul = 16711680
  constCyan = &HFFFF80
  constGris = &HC0C0C0
  constMorado = 9830600
  constLila = 16711935
End Enum


