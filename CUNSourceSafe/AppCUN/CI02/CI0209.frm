VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Begin VB.Form frmActDepartamento 
   Caption         =   "Actuaciones por Departamento."
   ClientHeight    =   8595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   ScaleHeight     =   8595
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cdmImpPlan 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   3240
      TabIndex        =   18
      Top             =   8160
      Width           =   1335
   End
   Begin VB.CommandButton cmdVG 
      Caption         =   "&Vision Global"
      Height          =   375
      Left            =   1680
      TabIndex        =   16
      Top             =   8160
      Width           =   1335
   End
   Begin VB.CommandButton cmdDatosAct 
      Caption         =   "&Datos Actuaci�n"
      Height          =   375
      Left            =   120
      TabIndex        =   15
      Top             =   8160
      Width           =   1335
   End
   Begin ComctlLib.TreeView tvwGrupos 
      Height          =   1635
      Left            =   3840
      TabIndex        =   14
      Top             =   0
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   2884
      _Version        =   327682
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   11015
      TabIndex        =   12
      Top             =   1200
      Width           =   820
   End
   Begin VB.CheckBox chkSFec 
      Caption         =   "Sin Fecha"
      Height          =   255
      Left            =   2520
      TabIndex        =   11
      Top             =   1320
      Width           =   1095
   End
   Begin VB.CheckBox chkRea 
      Caption         =   "Realizadas"
      Height          =   255
      Left            =   2520
      TabIndex        =   10
      Top             =   960
      Width           =   1095
   End
   Begin VB.CheckBox chkPlan 
      Caption         =   "Planificadas"
      Height          =   255
      Left            =   2520
      TabIndex        =   9
      Top             =   360
      Value           =   1  'Checked
      Width           =   1215
   End
   Begin VB.CheckBox chkCit 
      Caption         =   "Citadas"
      Height          =   255
      Left            =   2520
      TabIndex        =   8
      Top             =   680
      Value           =   1  'Checked
      Width           =   975
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "Consu&ltar"
      Height          =   375
      Left            =   11015
      TabIndex        =   0
      Top             =   120
      Width           =   820
   End
   Begin ComctlLib.ListView lvwAct 
      Height          =   1635
      Left            =   7800
      TabIndex        =   1
      Top             =   0
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   2884
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Actuaciones"
         Object.Width           =   3528
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo cboDptos 
      Height          =   315
      Left            =   540
      TabIndex        =   2
      Top             =   0
      Width           =   3135
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5530
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSCalendarWidgets_A.SSDateCombo dcboFecDesde 
      Height          =   330
      Left            =   540
      TabIndex        =   3
      Tag             =   "Fecha superior de las citas"
      Top             =   480
      Width           =   1530
      _Version        =   65537
      _ExtentX        =   2699
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dcboFecHasta 
      Height          =   330
      Left            =   540
      TabIndex        =   4
      Tag             =   "Fecha superior de las citas"
      Top             =   900
      Width           =   1530
      _Version        =   65537
      _ExtentX        =   2699
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin SSDataWidgets_B.SSDBGrid grdAct 
      Height          =   6375
      Left            =   0
      TabIndex        =   13
      Top             =   1680
      Width           =   11850
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   0
      stylesets.count =   1
      stylesets(0).Name=   "Editable"
      stylesets(0).BackColor=   16777215
      stylesets(0).Picture=   "CI0209.frx":0000
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20902
      _ExtentY        =   11245
      _StockProps     =   79
      Caption         =   "Actuaciones "
      ForeColor       =   8388608
      BackColor       =   12632256
   End
   Begin SSCalendarWidgets_A.SSDateCombo dcboFecPlan 
      Height          =   330
      Left            =   1680
      TabIndex        =   17
      Tag             =   "Fecha superior de las citas"
      Top             =   2280
      Width           =   1530
      _Version        =   65537
      _ExtentX        =   2699
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Desde:"
      Height          =   195
      Index           =   2
      Left            =   0
      TabIndex        =   7
      Top             =   540
      Width           =   510
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Hasta:"
      Height          =   195
      Index           =   1
      Left            =   0
      TabIndex        =   6
      Top             =   1020
      Width           =   465
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Dpto."
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   60
      Width           =   390
   End
End
Attribute VB_Name = "frmActDepartamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strHora_Ant As String

Private Sub cdmImpPlan_Click()
    Dim strWhere$, Formula(1, 2)
    Dim strActSel$
    Dim item As ListItem
    
    For Each item In lvwAct.ListItems
        If item.Selected = True Then strActSel = strActSel & item.Tag & ","
    Next item
    If Len(strActSel) > 1 Then
        strActSel = Left(strActSel, Len(strActSel) - 1)
    Else
        MsgBox "Seleccione las Actuaciones para imprimir", vbExclamation
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    If chkRea.Value = 1 Then
        strWhere = " PR0400.AD02CODDPTO= " & cboDptos.Columns(0).Value _
            & " AND PR0400.PR04FECINIACT > TO_DATE('" & dcboFecDesde.Text & "','DD/MM/YYYY')" _
            & " AND PR0400.PR04FECINIACT < TO_DATE('" & DateAdd("d", 1, CDate(dcboFecHasta.Text)) & " ','DD/MM/YYYY')" _
            & " AND PR0400.PR01CODACTUACION IN (" & strActSel & ")" _
            & " AND PR0400.PR37CODESTADO = 4"
        Call Imprimir_API(strWhere, "CI02REALIZADAS.rpt")
    ElseIf chkSFec.Value = 1 Then
        strWhere = " PR0400.AD02CODDPTO= " & cboDptos.Columns(0).Value _
                & " AND PR0400.PR04FECPLANIFIC IS NULL " _
                & " AND PR0400.PR01CODACTUACION IN (" & strActSel & ")" _
                & " AND PR0400.PR37CODESTADO = 1"
        Call Imprimir_API(strWhere, "CI02PLANIFICADAS.rpt")
    ElseIf chkPlan.Value = 1 And chkCit.Value = 0 Then
        strWhere = " PR0400.AD02CODDPTO= " & cboDptos.Columns(0).Value _
                & " AND PR0400.PR04FECPLANIFIC > TO_DATE('" & dcboFecDesde.Text & "','DD/MM/YYYY')" _
                & " AND PR0400.PR04FECPLANIFIC < TO_DATE('" & DateAdd("d", 1, CDate(dcboFecHasta.Text)) & " ','DD/MM/YYYY')" _
                & " AND PR0400.PR01CODACTUACION IN (" & strActSel & ")" _
                & " AND PR0400.PR37CODESTADO = 1"
        Call Imprimir_API(strWhere, "CI02PLANIFICADAS.rpt")
    ElseIf chkCit.Value = 1 And chkPlan = 0 Then
        strWhere = " PR0400.AD02CODDPTO= " & cboDptos.Columns(0).Value _
            & " AND CI0100.CI01FECCONCERT > TO_DATE('" & dcboFecDesde.Text & "','DD/MM/YYYY')" _
            & " AND CI0100.CI01FECCONCERT < TO_DATE('" & DateAdd("d", 1, CDate(dcboFecHasta.Text)) & " ','DD/MM/YYYY')" _
            & " AND PR0400.PR01CODACTUACION IN (" & strActSel & ")" _
            & " AND PR0400.PR37CODESTADO = 2"
        Call Imprimir_API(strWhere, "CI02CITADAS.rpt")
    ElseIf chkCit.Value = 1 And chkPlan.Value = 1 Then
        strWhere = " PR0468J.DPTOREA = " & cboDptos.Columns(0).Value _
            & " AND PR0468J.FECHAENTERA > TO_DATE('" & dcboFecDesde.Text & "','DD/MM/YYYY')" _
            & " AND PR0468J.FECHAENTERA < TO_DATE('" & DateAdd("d", 1, CDate(dcboFecHasta.Text)) & " ','DD/MM/YYYY')" _
            & " AND PR0468J.PR01CODACTUACION IN (" & strActSel & ")"
        Call Imprimir_API(strWhere, "CI02CITPLAN.rpt")
    End If
    

    Screen.MousePointer = vbDefault

End Sub

Private Sub chkCit_Click()
If chkCit.Value = 1 Then
    chkSFec.Value = 0
    chkRea.Value = 0
End If
End Sub

Private Sub chkPlan_Click()
If chkPlan.Value = 1 Then
    chkSFec.Value = 0
    chkRea.Value = 0
End If
End Sub

Private Sub chkRea_Click()
If chkRea.Value = 1 Then
    chkSFec.Value = 0
    chkPlan.Value = 0
    chkCit.Value = 0
End If
End Sub

Private Sub chkSFec_Click()
If chkSFec.Value = 1 Then
    chkCit.Value = 0
    chkPlan.Value = 0
    chkRea.Value = 0
End If
End Sub

Private Sub cmdConsultar_Click()
If blnComprobaciones Then pLlenarGrid (-1)
End Sub

Private Sub cmdDatosAct_Click()
If grdAct.SelBookmarks.Count = 0 Then
    MsgBox "No hay ninguna Actuacion seleccionada", vbExclamation, Me.Caption
Else
  Call objSecurity.LaunchProcess("PR0510", grdAct.Columns("PR04NUMACTPLAN").CellValue(grdAct.SelBookmarks(0)))
End If
End Sub





Private Sub cmdVG_Click()
If grdAct.SelBookmarks.Count = 0 Then
    MsgBox "No hay ninguna Actuacion seleccionada", vbExclamation, Me.Caption
Else
    Me.MousePointer = vbHourglass
    ReDim vntData(1) As Variant
    ReDim vntData(1 To 2) As Variant
    vntData(1) = grdAct.Columns("CI21CODPERSONA").CellValue(grdAct.SelBookmarks(0))
    vntData(2) = 2
    Call objSecurity.LaunchProcess("HC02", vntData())
    Me.MousePointer = vbDefault
End If

End Sub

Private Sub Command1_Click()
Unload Me
End Sub





Private Sub dcboFecDesde_Change()
If DateDiff("d", dcboFecDesde.Text, dcboFecHasta.Text) < 0 Then
   dcboFecHasta.Text = dcboFecDesde.Text
End If
    
End Sub

Private Sub dcboFecDesde_CloseUp()
If DateDiff("d", dcboFecDesde.Text, dcboFecHasta.Text) < 0 Then
    dcboFecHasta.Text = dcboFecDesde.Text
End If
End Sub

Private Sub dcboFecHasta_Change()
If DateDiff("d", dcboFecDesde.Text, dcboFecHasta.Text) < 0 Then
    MsgBox "El intervalo de fechas no es valido", vbExclamation, Me.Caption
    dcboFecHasta.Text = dcboFecDesde.Text
End If
End Sub

Private Sub dcboFecHasta_CloseUp()
If DateDiff("d", dcboFecDesde.Text, dcboFecHasta.Text) < 0 Then
    MsgBox "El intervalo de fechas no es valido", vbExclamation, Me.Caption
    dcboFecHasta.Text = dcboFecDesde.Text
End If
End Sub

Private Sub dcboFecPlan_CloseUp()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strFecha As String

If Not grdAct.Columns("Fecha").Locked Then
    strFecha = fAhora
    If CDate(dcboFecPlan.Text) < CDate(Format(strFecha, "DD/MM/YYYY")) Then
        MsgBox "No puede asignar una fecha de planificacion menor a la fecha actual", vbExclamation, Me.Caption
        Exit Sub
    End If
    
    sql = "UPDATE PR0400 SET PR04FECPLANIFIC = TO_DATE(?,'DD/MM/YYYY HH24:MI')"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Format(dcboFecPlan.Text & " " & grdAct.Columns("Hora").Value, "dd/mm/yyyy hh:mm")
        qry(1) = grdAct.Columns("PR04NUMACTPLAN").Value
    qry.Execute
    If qry.RowsAffected = 1 Then
        grdAct.Columns("Fecha").Value = Format(dcboFecPlan.Text, "dd/mm/yyyy")
        grdAct.Update
    Else
        MsgBox "No se ha podido actualizar", vbExclamation, Me.Caption
    End If
End If
End Sub

Private Sub Form_Load()

pLlenarCombo
pformateargrid
grdAct.Columns("Fecha").DropDownHwnd = dcboFecPlan.hWnd
End Sub

Private Sub pLlenarCombo()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

sql = "SELECT AD0200.AD02CODDPTO, AD02DESDPTO FROM AD0200,AD0300 "
sql = sql & " WHERE AD0300.AD02CODDPTO = AD0200.AD02CODDPTO"
sql = sql & " AND AD0300.SG02COD = ? "
sql = sql & " AND (AD0300.AD03FECFIN IS NULL OR AD0300.AD03FECFIN <= SYSDATE)"
sql = sql & " ORDER BY AD02DESDPTO"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objSecurity.strUser
Set rs = qry.OpenResultset()
While Not rs.EOF
    cboDptos.AddItem rs(0) & Chr$(9) & rs(1)
    rs.MoveNext
Wend

End Sub


Private Sub cboDptos_CloseUp()
grdAct.RemoveAll
pCargarGrupos
End Sub
Private Sub cboDptos_KeyPress(KeyAscii As Integer)
pCargarGrupos
End Sub
Private Sub pCargarGrupos()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim Nodo As Node
Dim strTipGrup As String

tvwGrupos.Nodes.Clear
lvwAct.ListItems.Clear
'NODO PRINCIPAL PARA SACAR TODAS LAS ACTUACIONES
Set Nodo = tvwGrupos.Nodes.Add(, , "TODOS", "TODOS")
'TIPOS DE ACTUACIONES
sql = "SELECT PR12CODACTIVIDAD,PR12DESACTIVIDAD FROM PR1200 "
sql = sql & " WHERE PR12INDPLANIFIC = -1 "
sql = sql & "ORDER BY PR12DESACTIVIDAD"
Set rs = objApp.rdoConnect.OpenResultset(sql)
While Not rs.EOF
    Set Nodo = tvwGrupos.Nodes.Add(, , "A" & rs!PR12CODACTIVIDAD, rs!PR12DESACTIVIDAD)
    rs.MoveNext
Wend
rs.Close

sql = "SELECT PR1500.PR15CODTIPGRUPO,PR15DESTIPGRUPO, PR1600.PR16CODGRUPO, "
sql = sql & " PR16DESGRUPO FROM PR1500,PR1600,PR6900"
sql = sql & " WHERE PR6900.AD02CODDPTO = ? "
sql = sql & " AND PR6900.PR16CODGRUPO = PR1600.PR16CODGRUPO"
sql = sql & " AND PR1600.PR15CODTIPGRUPO = PR1500.PR15CODTIPGRUPO"
sql = sql & " ORDER BY PR15DESTIPGRUPO,PR16DESGRUPO"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = cboDptos.Columns(0).Value
Set rs = qry.OpenResultset()

While Not rs.EOF
    If strTipGrup <> rs!PR15CODTIPGRUPO Then
        strTipGrup = rs!PR15CODTIPGRUPO
        Set Nodo = tvwGrupos.Nodes.Add(, , "T" & rs!PR15CODTIPGRUPO, rs!PR15DESTIPGRUPO)
    End If
    Set Nodo = tvwGrupos.Nodes.Add("T" & rs!PR15CODTIPGRUPO, tvwChild, "G" & rs!PR16CODGRUPO, rs!PR16DESGRUPO)
    rs.MoveNext
Wend
rs.Close
qry.Close
tvwGrupos.Nodes.item(1).Selected = True
End Sub



Private Sub grdAct_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

If ColIndex = 3 Then
    If Not IsDate(grdAct.Columns("Hora").Value) Then
        MsgBox "La hora no es correcta, introduzcala con formato HH:MM", vbExclamation, Me.Caption
        grdAct.Columns("Hora").Value = strHora_Ant
        Exit Sub
    End If
    sql = "UPDATE PR0400 SET PR04FECPLANIFIC = TO_DATE(?,'DD/MM/YYYY HH24:MI')"
    sql = sql & " WHERE PR04NUMACTPLAN = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Format(dcboFecPlan.Text & " " & grdAct.Columns("Hora").Value, "dd/mm/yyyy hh:mm")
        qry(1) = grdAct.Columns("PR04NUMACTPLAN").Value
    qry.Execute
    qry.Close
     
End If
If ColIndex = 14 Then
    sql = "UPDATE PR0800 SET PR08DESMOTPET = ? WHERE PR03NUMACTPEDI = ( SELECT "
    sql = sql & "PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ?) "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
          qry(0) = grdAct.Columns("Observaciones a la prueba").Value
          qry(1) = grdAct.Columns("PR04NUMACTPLAN").Value
    qry.Execute
    qry.Close
End If

End Sub

Private Sub grdAct_Click()
    strHora_Ant = grdAct.Columns("Hora").Value
    If grdAct.Columns("Estado").Text = "Planificada" And Trim(grdAct.Columns("PR04FECENTRCOLA").Text) = "" Then
         grdAct.Columns("Fecha").Locked = False
         grdAct.Columns("Hora").Locked = False
         grdAct.Columns("Observaciones a la prueba").Locked = False
    Else
         grdAct.Columns("Fecha").Locked = True
         grdAct.Columns("Hora").Locked = True
         grdAct.Columns("Observaciones a la prueba").Locked = True
    End If
End Sub

Private Sub grdAct_HeadClick(ByVal ColIndex As Integer)
pLlenarGrid (ColIndex)
End Sub

Private Sub grdAct_RowLoaded(ByVal Bookmark As Variant)
If grdAct.Columns("Estado").Text = "Planificada" And Trim(grdAct.Columns("PR04FECENTRCOLA").Text) = "" Then
    Call grdAct.Columns("Fecha").CellStyleSet("Editable", grdAct.Row)
    Call grdAct.Columns("Hora").CellStyleSet("Editable", grdAct.Row)
    Call grdAct.Columns("Observaciones a la prueba").CellStyleSet("Editable", grdAct.Row)
End If
End Sub

Private Sub tvwGrupos_NodeClick(ByVal Node As ComctlLib.Node)
Call pCargarActuaciones(Node.Key)
End Sub

Private Sub pCargarActuaciones(strCod As String)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim item As ListItem

lvwAct.ListItems.Clear

If strCod <> "TODOS" Then
    If Left(strCod, 1) = "T" Then
        sql = "SELECT PR01CODACTUACION,PR01DESCORTA FROM PR0100 "
        sql = sql & " WHERE PR01CODACTUACION IN (SELECT PR01CODACTUACION "
        sql = sql & " FROM PR1700 WHERE PR16CODGRUPO IN (SELECT PR16CODGRUPO "
        sql = sql & " FROM PR1600 WHERE PR15CODTIPGRUPO = ? ))"
    ElseIf Left(strCod, 1) = "A" Then
        sql = "SELECT PR0100.PR01CODACTUACION,PR01DESCORTA FROM PR0100, PR0200 "
        sql = sql & " WHERE PR12CODACTIVIDAD = ? "
        sql = sql & " AND PR0200.AD02CODDPTO =  " & cboDptos.Columns(0).Value
        sql = sql & " AND PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION"
        sql = sql & " AND SYSDATE < NVL(PR01FECFIN,'01/01/2100')"
    Else
        sql = "SELECT PR01CODACTUACION,PR01DESCORTA FROM PR0100 "
        sql = sql & " WHERE PR01CODACTUACION IN (SELECT PR01CODACTUACION "
        sql = sql & " FROM PR1700 WHERE PR16CODGRUPO = ? )"
    End If
    sql = sql & " ORDER BY PR01DESCORTA "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Right(strCod, Len(strCod) - 1)
Else
    sql = "SELECT PR0100.PR01CODACTUACION,PR01DESCORTA "
    sql = sql & " FROM PR0100,PR0200 WHERE "
    sql = sql & " PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0200.AD02CODDPTO = ? "
    sql = sql & " ORDER BY PR01DESCORTA "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = cboDptos.Columns(0).Value
End If
Set rs = qry.OpenResultset()
While Not rs.EOF
    Set item = lvwAct.ListItems.Add(, , rs(1))
    item.Tag = rs(0)
    rs.MoveNext
Wend

End Sub

Private Sub pformateargrid()
With grdAct
    .AllowUpdate = True
    .Columns(0).Caption = "CI21CODPERSONA"
    .Columns(0).Visible = False
    .Columns(0).Locked = True
    .Columns(1).Caption = "FechaEntera"
    .Columns(1).Visible = False
    .Columns(1).Locked = True
    .Columns(2).Caption = "Fecha"
    .Columns(2).Width = 1232
    .Columns(2).Locked = True
    .Columns(3).Caption = "Hora"
    .Columns(3).Width = 670
    .Columns(3).Locked = True
    .Columns(4).Caption = "Estado"
    .Columns(4).Width = 700
    .Columns(4).Locked = True
    .Columns(5).Caption = "PR04NUMACTPLAN"
    .Columns(5).Visible = False
    .Columns(5).Locked = True
    .Columns(6).Caption = "Actuaci�n"
    .Columns(6).Width = 1875
    .Columns(6).Locked = True
    .Columns(7).Caption = "Recurso"
    .Columns(7).Width = 1380
    .Columns(7).Locked = True
    .Columns(8).Caption = "Historia"
    .Columns(8).Width = 700
    .Columns(8).Locked = True
    .Columns(9).Caption = "Paciente"
    .Columns(9).Width = 2580
    .Columns(9).Locked = True
    .Columns(10).Caption = "cDptoSol"
    .Columns(10).Visible = False
    .Columns(10).Locked = True
    .Columns(11).Caption = "Dpto Solict."
    .Columns(11).Width = 1395
    .Columns(11).Locked = True
    .Columns(12).Caption = "cDrSol"
    .Columns(12).Visible = False
    .Columns(12).Locked = True
    .Columns(13).Caption = "Dr. Solicit."
    .Columns(13).Width = 1530
    .Columns(13).Locked = True
    .Columns(14).Caption = "Observaciones a la prueba"
    .Columns(14).Width = 3000
    .Columns(14).Locked = True
    .Columns(15).Caption = "PR04FECENTRCOLA"
    .Columns(15).Visible = False
    .Columns(15).Locked = True
End With

End Sub

Private Sub pLlenarGrid(intCol As Integer)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strActuaciones As String
Dim item As ListItem
Dim SqlAct As String
Dim strQry1 As String
Dim strOrder As String
Dim I As Integer
Dim strUnion As String


Screen.MousePointer = vbHourglass
grdAct.RemoveAll
strActuaciones = ""
Select Case intCol
    Case -1, 1
        strOrder = " ORDER BY FechaEntera, PR01DESCORTA"
    Case 2
        strOrder = " ORDER BY FechaEntera, PR01DESCORTA"
    Case 3
        strOrder = " ORDER BY HORA, PR01DESCORTA"
    Case 4
        strOrder = " ORDER BY PR37DESESTADO, FechaEntera"
    Case 6
        strOrder = " ORDER BY PR01DESCORTA, FechaEntera"
    Case 7
        strOrder = " ORDER BY REC, FechaEntera"
    Case 8
        strOrder = " ORDER BY CI22NUMHISTORIA, FechaEntera"
    Case 9
        strOrder = " ORDER BY PACIENTE,FechaEntera"
    Case 11
        strOrder = " ORDER BY AD02CODDPTO, FechaEntera"
    Case 12
        strOrder = " ORDER BY SG02COD, FechaEntera "
    Case Else
        strOrder = " ORDER BY FechaEntera, PR01DESCORTA"
End Select
For Each item In lvwAct.ListItems
    If item.Selected = True Then strActuaciones = strActuaciones & item.Tag & ","
Next item
If Len(strActuaciones) > 1 Then strActuaciones = Left(strActuaciones, Len(strActuaciones) - 1)
If strActuaciones <> "" Then
    SqlAct = " AND PR0400.PR01CODACTUACION IN ( " & strActuaciones & ")"
Else
    If tvwGrupos.SelectedItem.Key <> "TODOS" Then
        If Left(tvwGrupos.SelectedItem.Key, 1) = "T" Then
            SqlAct = "AND PR0400.PR01CODACTUACION IN (SELECT PR01CODACTUACION "
            SqlAct = SqlAct & " FROM PR1700 WHERE PR16CODGRUPO IN (SELECT PR16CODGRUPO "
            SqlAct = SqlAct & " FROM PR1600 WHERE PR15CODTIPGRUPO = ? ))"
            strQry1 = Right(tvwGrupos.SelectedItem.Key, Len(tvwGrupos.SelectedItem.Key) - 1)
        ElseIf Left(tvwGrupos.SelectedItem.Key, 1) = "A" Then
            SqlAct = "AND PR0400.PR01CODACTUACION IN (SELECT PR0100.PR01CODACTUACION "
            SqlAct = SqlAct & "FROM PR0100, PR0200 "
            SqlAct = SqlAct & " WHERE PR12CODACTIVIDAD = ? "
            SqlAct = SqlAct & " AND PR0200.AD02CODDPTO =  " & cboDptos.Columns(0).Value
            SqlAct = SqlAct & " AND PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION"
            SqlAct = SqlAct & " AND SYSDATE < NVL(PR01FECFIN,'01/01/2100'))"
            strQry1 = Right(tvwGrupos.SelectedItem.Key, Len(tvwGrupos.SelectedItem.Key) - 1)
        Else
            SqlAct = "AND PR0400.PR01CODACTUACION IN (SELECT PR01CODACTUACION "
            SqlAct = SqlAct & " FROM PR1700 WHERE PR16CODGRUPO = ? )"
            strQry1 = Right(tvwGrupos.SelectedItem.Key, Len(tvwGrupos.SelectedItem.Key) - 1)
        End If
    Else
        SqlAct = "AND PR0400.PR01CODACTUACION IN (SELECT PR01CODACTUACION "
        SqlAct = SqlAct & " FROM PR0200 WHERE AD02CODDPTO = ?) "
        strQry1 = cboDptos.Columns(0).Value
    End If
End If

If chkCit.Value = 1 Then 'sacamos las citadas
    sql = "SELECT PR01DESCORTA, PR37DESESTADO, CI01FECCONCERT FECHAENTERA, "
    sql = sql & " TO_CHAR(CI01FECCONCERT,'DD/MM/YYYY') FECHA, TO_CHAR(CI01FECCONCERT,'HH24:MI') HORA,"
    sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE, CI22NUMHISTORIA, "
    sql = sql & " PR0400.PR04NUMACTPLAN, CI2200.CI21CODPERSONA, AG11DESRECURSO REC, "
    sql = sql & " PR0900.AD02CODDPTO, AD02DESDPTO, PR0900.SG02COD, SG02TXTFIRMA,PR08DESMOTPET, PR04FECENTRCOLA"
    sql = sql & " FROM CI0100,PR0400,CI2200, PR0100, PR3700, AG1100, "
    sql = sql & " PR0300, PR0900, PR0800,AD0200, SG0200 "
    sql = sql & " WHERE CI01FECCONCERT > = TO_DATE(?,'DD/MM/YYYY HH24:MI')"
    sql = sql & " AND CI01FECCONCERT < = TO_DATE(?,'DD/MM/YYYY HH24:MI')"
    sql = sql & " AND CI01SITCITA = '" & constESTCITA_CITADA & "'"
    sql = sql & " AND PR0400.AD02CODDPTO = ? "
    sql = sql & " AND CI0100.AG11CODRECURSO = AG1100.AG11CODRECURSO "
    sql = sql & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN "
    sql = sql & " AND PR0400.PR37CODESTADO =  " & constESTACT_CITADA
    sql = sql & " AND PR0400.PR37CODESTADO = PR3700.PR37CODESTADO"
    sql = sql & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA "
    sql = sql & " " & SqlAct
    sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0800.PR03NUMACTPEDI"
    sql = sql & " AND PR0300.PR09NUMPETICION = PR0900.PR09NUMPETICION"
    sql = sql & " AND PR0900.AD02CODDPTO = AD0200.AD02CODDPTO"
    sql = sql & " AND PR0900.SG02COD = SG0200.SG02COD"
    strUnion = " UNION "
End If
If chkPlan.Value = 1 Then
    sql = sql & strUnion & " SELECT PR01DESCORTA, PR37DESESTADO, PR04FECPLANIFIC FECHAENTERA, "
    sql = sql & " TO_CHAR(PR04FECPLANIFIC,'DD/MM/YYYY') FECHA, TO_CHAR(PR04FECPLANIFIC,'HH24:MI') HORA,"
    sql = sql & "CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE, CI22NUMHISTORIA, "
    sql = sql & " PR0400.PR04NUMACTPLAN, CI2200.CI21CODPERSONA, ' ' REC,"
    sql = sql & " PR0900.AD02CODDPTO, AD02DESDPTO, PR0900.SG02COD, "
    sql = sql & " SG02TXTFIRMA, PR08DESMOTPET, PR04FECENTRCOLA"
    sql = sql & " FROM PR0400,CI2200, PR0100, PR3700, PR0300, PR0900, PR0800,AD0200, SG0200 "
    sql = sql & " WHERE PR04FECPLANIFIC > = TO_DATE(?,'DD/MM/YYYY HH24:MI')"
    sql = sql & " AND PR04FECPLANIFIC < = TO_DATE(?,'DD/MM/YYYY HH24:MI')"
    sql = sql & " AND PR0400.PR37CODESTADO =  " & constESTACT_PLANIFICADA
    sql = sql & " AND PR0400.AD02CODDPTO = ? "
    sql = sql & " AND PR0400.PR37CODESTADO = PR3700.PR37CODESTADO"
    sql = sql & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
    sql = sql & " " & SqlAct
    sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0800.PR03NUMACTPEDI"
    sql = sql & " AND PR0300.PR09NUMPETICION = PR0900.PR09NUMPETICION"
    sql = sql & " AND PR0900.AD02CODDPTO = AD0200.AD02CODDPTO"
    sql = sql & " AND PR0900.SG02COD = SG0200.SG02COD"
    strUnion = " UNION "
End If
If chkRea.Value = 1 Then
    sql = sql & strUnion & "SELECT /*+ ORDERED INDEX(PR0400 PR0408) (CI0100 CI0105)*/"
    sql = sql & " PR01DESCORTA, PR37DESESTADO, PR04FECINIACT FECHAENTERA, "
    sql = sql & " TO_CHAR(PR04FECINIACT,'DD/MM/YYYY') FECHA, TO_CHAR(PR04FECINIACT,'HH24:MI') HORA,"
    sql = sql & "CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE, CI22NUMHISTORIA, "
    sql = sql & " PR0400.PR04NUMACTPLAN, CI2200.CI21CODPERSONA, AG11DESRECURSO REC,"
    sql = sql & " PR0900.AD02CODDPTO, AD02DESDPTO, PR0900.SG02COD, SG02TXTFIRMA, "
    sql = sql & " PR08DESMOTPET, PR04FECENTRCOLA"
    sql = sql & " FROM PR0400,CI0100,PR0300,PR0900,PR0800,PR0100,CI2200,PR3700,AG1100,"
    sql = sql & " AD0200 , SG0200"
    sql = sql & " WHERE PR04FECINIACT > = TO_DATE(?,'DD/MM/YYYY HH24:MI')"
    sql = sql & " AND PR04FECINIACT < = TO_DATE(?,'DD/MM/YYYY HH24:MI')"
    sql = sql & " AND PR0400.PR37CODESTADO = " & constESTACT_REALIZADA
    sql = sql & " AND PR0400.AD02CODDPTO = ? "
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN(+) "
    sql = sql & " AND CI01SITCITA (+) = '" & constESTCITA_CITADA & "'"
    sql = sql & " AND CI0100.AG11CODRECURSO  = AG1100.AG11CODRECURSO(+) "
    sql = sql & " AND PR0400.PR37CODESTADO = PR3700.PR37CODESTADO"
    sql = sql & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
    sql = sql & " " & SqlAct
    sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0800.PR03NUMACTPEDI"
    sql = sql & " AND PR0300.PR09NUMPETICION = PR0900.PR09NUMPETICION"
    sql = sql & " AND PR0900.AD02CODDPTO = AD0200.AD02CODDPTO"
    sql = sql & " AND PR0900.SG02COD = SG0200.SG02COD"
    strUnion = " UNION "
End If
If chkSFec.Value = 1 Then
    sql = " SELECT PR01DESCORTA, PR37DESESTADO, ' ' FECHAENTERA, "
    sql = sql & " ' 'FECHA, ' ' HORA,"
    sql = sql & "CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE, CI22NUMHISTORIA, "
    sql = sql & " PR0400.PR04NUMACTPLAN, CI2200.CI21CODPERSONA, ' ' REC,"
    sql = sql & " PR0900.AD02CODDPTO, AD02DESDPTO, PR0900.SG02COD, "
    sql = sql & " SG02TXTFIRMA, PR08DESMOTPET, PR04FECENTRCOLA"
    sql = sql & " FROM PR0400,CI2200, PR0100, PR3700, PR0300, PR0900, PR0800,AD0200, SG0200 "
    sql = sql & " WHERE PR04FECPLANIFIC IS NULL"
    sql = sql & " AND PR0400.PR37CODESTADO =  " & constESTACT_PLANIFICADA
    sql = sql & " AND PR0400.AD02CODDPTO = ? "
    sql = sql & " AND PR0400.PR37CODESTADO = PR3700.PR37CODESTADO"
    sql = sql & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
    sql = sql & " " & SqlAct
    sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0800.PR03NUMACTPEDI"
    sql = sql & " AND PR0300.PR09NUMPETICION = PR0900.PR09NUMPETICION"
    sql = sql & " AND PR0900.AD02CODDPTO = AD0200.AD02CODDPTO"
    sql = sql & " AND PR0900.SG02COD = SG0200.SG02COD"
End If
sql = sql & strOrder
Set qry = objApp.rdoConnect.CreateQuery("", sql)
I = 0
    If chkPlan.Value = 1 Then
        qry(I) = Format(dcboFecDesde.Text & " 00:00", "dd/mm/yyyy hh:mm")
        I = I + 1
        qry(I) = Format(dcboFecHasta.Text & " 23:59", "dd/mm/yyyy hh:mm")
        I = I + 1
        qry(I) = cboDptos.Columns(0).Value
        I = I + 1
        If strActuaciones = "" Then
            qry(I) = strQry1
            I = I + 1
        End If
    End If
    If chkCit.Value = 1 Then
        qry(I) = Format(dcboFecDesde.Text & " 00:00", "dd/mm/yyyy hh:mm")
        I = I + 1
        qry(I) = Format(dcboFecHasta.Text & " 23:59", "dd/mm/yyyy hh:mm")
        I = I + 1
        qry(I) = cboDptos.Columns(0).Value
        I = I + 1
        If strActuaciones = "" Then
            qry(I) = strQry1
            I = I + 1
        End If
    End If
    If chkRea.Value = 1 Then
        qry(I) = Format(dcboFecDesde.Text & " 00:00", "dd/mm/yyyy hh:mm")
        I = I + 1
        qry(I) = Format(dcboFecHasta.Text & " 23:59", "dd/mm/yyyy hh:mm")
        I = I + 1
        qry(I) = cboDptos.Columns(0).Value
        I = I + 1
        If strActuaciones = "" Then
            qry(I) = strQry1
            I = I + 1
        End If
    End If
    If chkSFec.Value = 1 Then
        qry(I) = cboDptos.Columns(0).Value
        I = I + 1
        If strActuaciones = "" Then
            qry(I) = strQry1
            I = I + 1
        End If
    End If
Set rs = qry.OpenResultset
While Not rs.EOF
    grdAct.AddItem rs!CI21CODPERSONA & Chr$(9) _
                & Format(rs!fechaENTERA, "dd/mm/yyyy hh:mm") & Chr$(9) _
                & Format(rs!fecha, "dd/mm/yyyy") & Chr$(9) _
                & Format(rs!hora, "hh:mm") & Chr$(9) _
                & rs!PR37DESESTADO & Chr$(9) _
                & rs!PR04NUMACTPLAN & Chr$(9) _
                & rs!PR01DESCORTA & Chr$(9) _
                & rs!REC & Chr$(9) _
                & rs!CI22NUMHISTORIA & Chr$(9) _
                & rs!PACIENTE & Chr$(9) _
                & rs!AD02CODDPTO & Chr$(9) _
                & rs!AD02DESDPTO & Chr$(9) _
                & rs!SG02COD & Chr$(9) _
                & rs!SG02TXTFIRMA & Chr$(9) _
                & rs!PR08DESMOTPET & Chr$(9) _
                & rs!PR04FECENTRCOLA
    rs.MoveNext
    
Wend
Screen.MousePointer = vbDefault
End Sub

Private Function blnComprobaciones() As Boolean
Dim item As ListItem
Dim intNS As Integer

blnComprobaciones = True
If Not IsObject(tvwGrupos.SelectedItem) Then
    blnComprobaciones = False
    Exit Function
End If


For Each item In lvwAct.ListItems
  If item.Selected = True Then intNS = intNS + 1
Next item
If intNS <> 1 Then
      If DateDiff("d", dcboFecDesde.Text, dcboFecHasta.Text) > 7 Then
          MsgBox "Para ver las realizadas un rango mayor de 7 dias debe seleccionar una �nica prueba", vbExclamation, Me.Caption
          blnComprobaciones = False
          Exit Function
      End If
 End If

End Function
