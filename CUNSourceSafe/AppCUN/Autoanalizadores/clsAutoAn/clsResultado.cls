VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsResultado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private MiStrResultado As String
Private MiDesResultado As String 'designacion
Private MiStrEstado As constEstado
Private MiStrProperty1 As String '9
Private MiStrProperty2 As String '11
Private MiStrProperty3 As String
Private MiDblRefMin As Double
Private MiDblRefMax As Double
Private MiStrRefMinMax As String
Private MiStrCodAct As String

Public codResultado As String 'codigo de resultado
Public intFila As Integer     'nfila en el grid
Public intCodUnidad As Integer
Public Actuacion As Object
Public BkMrk As Variant
'variables locales que contienen valores de propiedad

Public Sub ElegirFila()
Dim i%
Dim Selected As Boolean
  
  With frmAutoan.ssdgPruebas
    For i = 0 To .SelBookmarks.Count - 1
      If CStr(.Row) = CStr(.SelBookmarks(i)) Then
        'Selected = True
      End If
    Next
    If Selected = False Then
      .SelBookmarks.Add intFila
    Else
      .SelBookmarks.Remove intFila
    End If
  End With
End Sub

Public Sub QuitarResultado()
  
End Sub

Public Sub EscribirEnColumna(code As constColumnaGridResultados, valor As String)
Dim columna As Column

  With frmAutoan.ssdgPruebas
    For Each columna In .Columns
      If columna.TagVariant = code Then
'        .MoveFirst
'        .MoveRecords intFila
        .Bookmark = intFila 'BkMrk '
        columna.Text = valor
        Exit For
      End If
    Next
  End With
End Sub

Public Property Let strCodAct(val As String)
  MiStrCodAct = val
End Property

Public Sub ResetResultado()
  MiStrResultado = ""
End Sub

Public Property Let strResultado(val As String)   'resultado numerico/valor
Dim Sql$
Dim Qry As rdoQuery
Dim Rs As rdoResultset
Dim FechaHora As String
Dim strResponsable As String
On Error GoTo Autoanerror

  'Call EscribirEnColumna(cteResultadoCODACTUACIONINVISIBLE, Actuacion.strCodAct)
  'Call EscribirEnColumna(ctePruebaCODMUESTRAINVISIBLE, Actuacion.strCodMuestra)
  
  Sql = "SELECT historia from RESULTADOASISTENCIA" _
  & " WHERE Historia = ? and Caso = ? " _
  & " and Secuencia = ? and NRepeticion = ? " _
  & " and cResultado = ? "
  Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
  With Actuacion
    Qry(0) = .Muestra.lngHistoria
    Qry(1) = Actuacion.strCaso
    Qry(2) = Actuacion.strSecuencia
    Qry(3) = Actuacion.intRepeticion
    Qry(4) = codResultado
  End With
  Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
  'Qry.Close
  FechaHora = objAutoan.fFechaHoraActual
  If Actuacion.lngNumLista = 0 Then
    strResponsable = ""
  Else
    strResponsable = objAutoan.ColListasDeTrabajo(CStr(Actuacion.lngNumLista)).strResponsableLista
  End If
  If Rs.EOF Then
    Sql = "INSERT INTO ResultadoAsistencia" _
    & " (historia, caso, secuencia, nrepeticion," _
    & " cresultado, resultadoalfanumerico, responsable," _
    & " cunidad, fecha, hora)" _
    & " values (?,?,?,?,?,?,?,?,?,?)"
    Qry.Close
    Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
    With Actuacion
      Qry(0) = .Muestra.lngHistoria
      Qry(1) = .strCaso
      Qry(2) = .strSecuencia
      Qry(3) = .intRepeticion
      Qry(4) = codResultado
      Qry(5) = val
      Qry(6) = strResponsable
      Qry(7) = intCodUnidad
      Qry(8) = Format(FechaHora, "dd/mm/yy")
      Qry(9) = Format(FechaHora, "hh:mm:ss")
    End With
    Qry.Execute
  Else
    Sql = "UPDATE ResultadoAsistencia SET resultadoalfanumerico = ?" _
    & " ,fecha = ? ,hora = ?, responsable = ?, cunidad = ?" _
    & " WHERE (historia = ? AND caso = ? AND secuencia = ?" _
    & " AND nrepeticion = ? AND cresultado = ?)"
    Qry.Close
    Set Qry = objAutoan.rdoConnect.CreateQuery("", Sql)
    With Actuacion
      Qry(0) = val
      Qry(1) = Format(FechaHora, "dd/mm/yy")
      Qry(2) = Format(FechaHora, "hh:mm:ss")
      Qry(3) = strResponsable
      Qry(4) = intCodUnidad
      Qry(5) = .Muestra.lngHistoria
      Qry(6) = .strCaso
      Qry(7) = .strSecuencia
      Qry(8) = .intRepeticion
      Qry(9) = codResultado
    End With
    Qry.Execute
  End If
  
  If IsNumeric(val) Then
    If CDbl(val) < MiDblRefMin Or _
    CDbl(val) > MiDblRefMax Then
      strEstado = cteFUERADERANGO
    Else
      strEstado = cteREALIZADA
    End If
  Else
    If objAutoan.blnRepeticionAutomatica Then
      strEstado = cteAREPETIR
    Else
      strEstado = cteREALIZADA
    End If
  End If
  
  MiStrResultado = val
  Call EscribirEnColumna(cteResultadoVALRESULTADO, val)
  Exit Property

Autoanerror:
    Dim i%, StrQry$
    For i = 0 To Qry.rdoParameters.Count - 1
      StrQry = StrQry & "; qry(" & i & ")= " & Qry(i).Value
    Next i
    Call objError.InternalError(Me, "Public Property Let strResultado", "Val =" & val & "SQL=" & Sql & StrQry)
End Property

Public Property Let DblRefMin(val As Double)
  If val <> MiDblRefMin Then
    MiDblRefMin = val
    Call EscribirEnColumna(cteResultadoREFMIN, CStr(val))
  End If
End Property

Public Property Let DblRefMax(val As Double)
  If val <> MiDblRefMax Then
    MiDblRefMax = val
    Call EscribirEnColumna(cteResultadoREFMAX, CStr(val))
  End If
End Property

Public Property Let StrRefMinMax(val As String)
  If val <> MiStrRefMinMax Then
    MiStrRefMinMax = val
    Call EscribirEnColumna(cteResultadoREFMINMAX, val)
  End If
End Property

Public Property Let DesResultado(val As String)   'resultado numerico/valor
  MiDesResultado = val
  Call EscribirEnColumna(cteResultadoDESCRESULTADO, val)
End Property

Public Property Get DesResultado() As String
    DesResultado = MiDesResultado
End Property

Public Property Let strEstado(val As constEstado)   'resultado numerico/valor
Dim DescEstado As String
  If val <> MiStrEstado Then
    DescEstado = strDescEstado(val)
    MiStrEstado = val
    Call EscribirEnColumna(cteResultadoESTADO, DescEstado)
    objAutoan.EscribirLog "EstadoResultado (" & Actuacion.Muestra.strCodMuestra & ", " & Actuacion.strCodAct & "," & codResultado & ") = " & DescEstado
    With Actuacion
      Select Case val
        Case cteFUERADERANGO
          If .lngNumLista <> 0 Then
            objAutoan.ColListasDeTrabajo(CStr(.lngNumLista)).Estado = cteLISTAPRESULTANAL
          End If
          .strEstado = IIf(.intRepeticion = 1 And objAutoan.blnRepeticionAutomatica, cteAREPETIR, cteFUERADERANGO)
        Case cteREALIZADA
          If .lngNumLista <> 0 Then
            objAutoan.ColListasDeTrabajo(CStr(.lngNumLista)).Estado = cteLISTAPRESULTANAL
          End If
          .strEstado = IIf(.strEstado <> cteFUERADERANGO And .strEstado <> cteAREPETIR, cteREALIZADA, .strEstado)
        Case cteAREPETIR
          .strEstado = cteAREPETIR
          If .lngNumLista <> 0 Then
            objAutoan.ColListasDeTrabajo(CStr(.lngNumLista)).Estado = cteLISTAPRESULTANAL
          End If
        Case Else
          
      End Select
    End With
  End If
End Property

Public Property Get strCodAct() As String
    strCodAct = MiStrCodAct
End Property

Public Property Get strEstado() As constEstado
    strEstado = MiStrEstado
End Property

Public Property Get strProperty1() As String
  strProperty1 = MiStrProperty1
End Property

Public Property Get strProperty2() As String
  strProperty2 = MiStrProperty2
End Property

Public Property Get strProperty3() As String
  strProperty3 = MiStrProperty3
End Property

Public Property Let strProperty1(val As String)   '9
  If val <> MiStrProperty1 Then
    MiStrProperty1 = val
    Call EscribirEnColumna(cteResultadoPROPERTY1, val)
  End If
End Property

Public Property Let strProperty2(val As String)   '9
  If val <> MiStrProperty2 Then
    MiStrProperty2 = val
    Call EscribirEnColumna(cteResultadoPROPERTY2, val)
  End If
End Property

Public Property Let strProperty3(val As String)   '9
  If val <> MiStrProperty3 Then
    MiStrProperty3 = val
    Call EscribirEnColumna(cteResultadoPROPERTY3, val)
  End If
End Property

Public Property Get strResultado() As String
  strResultado = MiStrResultado
End Property

Public Property Get DblRefMin() As Double
  DblRefMin = MiDblRefMin
End Property

Public Property Get StrRefMinMax() As String
  StrRefMinMax = MiStrRefMinMax
End Property

Public Property Get DblRefMax() As Double
  DblRefMax = MiDblRefMax
End Property

