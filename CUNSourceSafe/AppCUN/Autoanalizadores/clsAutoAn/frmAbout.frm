VERSION 5.00
Begin VB.Form frmAbout 
   BackColor       =   &H00C0E0FF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Acerca de..."
   ClientHeight    =   4110
   ClientLeft      =   1920
   ClientTop       =   3630
   ClientWidth     =   7770
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmAbout.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4110
   ScaleWidth      =   7770
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraFrame 
      BackColor       =   &H00C0E0FF&
      Height          =   465
      Left            =   135
      TabIndex        =   5
      Top             =   2385
      Width           =   7530
      Begin VB.Label lblUser 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ra�l Roig"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   6
         Top             =   180
         Width           =   855
      End
   End
   Begin VB.CommandButton cmdAccept 
      BackColor       =   &H0080C0FF&
      Cancel          =   -1  'True
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   6120
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   3360
      Width           =   1545
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "DataBase"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   285
      Left            =   1800
      TabIndex        =   11
      Top             =   1410
      Width           =   1110
   End
   Begin VB.Label LblBD 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Enlace a la Base de datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   225
      Left            =   3000
      TabIndex        =   10
      Top             =   1440
      Width           =   2160
   End
   Begin VB.Label lblAutoanDll 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre, Version y revisi�n del Producto"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   285
      Left            =   1800
      TabIndex        =   9
      Top             =   690
      Width           =   4620
   End
   Begin VB.Label lblDllPath 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Autoanalizadores Library v X.X"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   225
      Left            =   1830
      TabIndex        =   8
      Top             =   990
      Width           =   5805
   End
   Begin VB.Label lblExePath 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Autoanalizadores Library v X.X"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   225
      Left            =   1800
      TabIndex        =   7
      Top             =   420
      Width           =   5925
   End
   Begin VB.Image imgImage 
      Appearance      =   0  'Flat
      Height          =   1875
      Left            =   135
      Picture         =   "frmAbout.frx":030A
      Top             =   135
      Width           =   1500
   End
   Begin VB.Label lblLabel1 
      BackStyle       =   0  'Transparent
      Caption         =   $"frmAbout.frx":2530
      Height          =   1020
      Left            =   135
      TabIndex        =   4
      Top             =   3015
      Width           =   5835
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblAutoriza 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Se autoriza el uso de este producto a:"
      Height          =   195
      Left            =   135
      TabIndex        =   3
      Top             =   2205
      Width           =   2685
   End
   Begin VB.Label lblCopyright 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "� Copyright del Producto"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   1830
      TabIndex        =   2
      Top             =   1950
      Width           =   2400
   End
   Begin VB.Label lblProductName 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre, Version y revisi�n del Producto"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   285
      Left            =   1830
      TabIndex        =   1
      Top             =   120
      Width           =   4620
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWAbout
' Coded by SYSECA Bilbao
' **********************************************************************************

Private Sub Form_Load()
On Error Resume Next
  LblBD = objAutoan.strDataSource
  lblExePath = GetClientPath()
  lblDllPath = GetDllPath()
  lblProductName = GetClientVersion() & " (" & FileDateTime(lblExePath) & ")"
  lblAutoanDll = GetDllVersion() & " (" & FileDateTime(lblDllPath) & ")"
  With objAutoan.objUserApp
    lblCopyright = .LegalCopyright
    lblUser = .CompanyName
  End With
End Sub

Private Sub cmdAccept_Click()
  Call Hide
  Exit Sub
End Sub

Public Function GetClientVersion() As String
  'devuelve la version del programa en ejecucion
  
  On Error GoTo cwIntError
  With objAutoan.objUserApp
    GetClientVersion = .Title & " v" & _
      Format(CStr(.Major), "###0") & "." & _
      Format(CStr(.Minor), "###0") & "." & _
      Format(CStr(.Revision), "###0")
  End With
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "GetClienteVersion")
End Function

Public Function GetClientPath() As String
  'devuelve el path del progama en ejecucion
  
  On Error GoTo cwIntError
  With objAutoan.objUserApp
    GetClientPath = .Path & "\" & .EXEName & ".exe"
  End With
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "GetClientPath")
End Function

Public Function GetDllPath() As String
  'devuelve el path de la dll utilizada por el programa en ejecuacion
  
  On Error GoTo cwIntError
  With App
    GetDllPath = .Path & "\" & .EXEName & ".dll"
  End With
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "GetDllPath")
End Function

Public Function GetDllVersion() As String
  'devuelve la version de la dll utilizada por el programa en ejecucion
  
  On Error GoTo cwIntError
  With App
    GetDllVersion = .Title & " v" & _
    Format(CStr(.Major), "###0") & "." & _
    Format(CStr(.Minor), "###0") & "." & _
    Format(CStr(.Revision), "###0")
  End With
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "GetDllVersion")
End Function
