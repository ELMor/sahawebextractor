VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsSeguridad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Public strKeyFile   As String


Public Function GetDataBaseUser() As String
  GetDataBaseUser = GetInfo("DataBaseUser")
End Function
  

Public Function GetDataBasePassword() As String
  Dim objEncript As New clsEncript
  
'  GetDataBasePassword = objEncript.Encript(GetInfo("DatabasePassword"))
  GetDataBasePassword = GetInfo("DatabasePassword")
'  Set objEncript = Nothing
End Function


'Public Function GetDataBasePasswordOld() As String
'  Dim objEncript As New clsCWMRES
'
'  GetDataBasePasswordOld = objEncript.EncriptOld(GetInfo("DataBasePassword"))
'  Set objEncript = Nothing
'End Function


Private Function GetInfo(ByVal strKey As String) As String
  Dim strApp         As String
  Dim strBuffer      As String
  Dim strFile        As String
  Dim lngSize        As Long
  Dim objFile        As New clsFileOpen
  Dim objConfig      As New clsConfig
  
  strApp = "CWSpy"
  strBuffer = Space(256)
  
  Call objConfig.LoadKeyFile
  strFile = strKeyFile
  
  Do
    lngSize = GetPrivateProfileString(strApp, strKey, " ", strBuffer, Len(strBuffer), strFile)
    If lngSize > 0 Then
      strKeyFile = strFile
      Call objConfig.SaveKeyFile
      GetInfo = Left(strBuffer, lngSize)
    Else
      Call objFile.AddFilter("Archivos INI", "*.ini")
      If objFile.GetFile(strFile, "Archivo de claves") Then
        strFile = objFile.strFile
      Else
        Exit Do
      End If
      Set objFile = Nothing
    End If
  Loop Until lngSize > 0
  
  Set objConfig = Nothing
End Function
