VERSION 5.00
Begin VB.Form frmCWUser 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Identificación"
   ClientHeight    =   2985
   ClientLeft      =   2010
   ClientTop       =   3480
   ClientWidth     =   4650
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   4650
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraEstandar 
      Height          =   1680
      Left            =   90
      TabIndex        =   7
      Top             =   100
      Width           =   2605
      Begin VB.TextBox txtPassword 
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   180
         MaxLength       =   30
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   1170
         Width           =   2155
      End
      Begin VB.TextBox txtUser 
         Height          =   330
         Left            =   180
         MaxLength       =   30
         TabIndex        =   0
         Top             =   450
         Width           =   2155
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Clave de Acceso"
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   9
         Top             =   945
         Width           =   1215
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre de Usuario"
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   8
         Top             =   225
         Width           =   1365
      End
   End
   Begin VB.Frame fraAvanzado 
      Height          =   900
      Left            =   120
      TabIndex        =   10
      Top             =   2000
      Width           =   4300
      Begin VB.ComboBox cboSources 
         Height          =   315
         Left            =   180
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   470
         Width           =   2225
      End
      Begin VB.CommandButton cmdODBC 
         Caption         =   "&ODBC Manager"
         Height          =   375
         Left            =   2545
         TabIndex        =   6
         Top             =   400
         Width           =   1545
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Conexión ODBC"
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   11
         Top             =   200
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdAccept 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   2815
      TabIndex        =   2
      Top             =   300
      Width           =   1545
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2815
      TabIndex        =   3
      Top             =   775
      Width           =   1545
   End
   Begin VB.CommandButton cmdAdvanced 
      Caption         =   "A&vanzado..."
      Height          =   375
      Left            =   2815
      TabIndex        =   4
      Top             =   1295
      Width           =   1545
   End
End
Attribute VB_Name = "frmCWUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Const intConstSmallWinSize As Integer = 2200
Const intConstBigWinSize   As Integer = 3600
Public blnConnection As Boolean

Private Sub Form_Activate()
  If IsStrEmpty(txtUser) Then
    Call txtUser.SetFocus
  ElseIf IsStrEmpty(txtPassword) Then
    Call txtPassword.SetFocus
  ElseIf cboSources.ListIndex = -1 Then
    Call cboSources.SetFocus
  Else
    Call cmdAccept.SetFocus
  End If
End Sub
Private Sub SearchSources(ByVal strDataSource As String)
  Dim objEnv    As rdoEnvironment
  Dim strBuffer As String
  Dim strDesc   As String
  Dim intBuffer As Integer
  Dim intDesc   As Integer
  Dim lngCode   As Long
  Dim intPos    As Integer
  
  Set objEnv = rdoEnvironments(0)
  
  cboSources.Clear
  strBuffer = Space(512)
  strDesc = Space(512)
  lngCode = SQLDataSources(objEnv.henv, SQL_FETCH_NEXT, _
                           strBuffer, Len(strBuffer), intBuffer, _
                           strDesc, Len(strDesc), intDesc)
  
  Do While lngCode <> SQL_NO_DATA_FOUND
    strBuffer = Left(strBuffer, intBuffer)
    strDesc = Left(strDesc, intDesc)
    Call cboSources.AddItem(strBuffer)
    If UCase(strDataSource) = UCase(strBuffer) Then
      cboSources.ListIndex = intPos
    Else
      intPos = intPos + 1
    End If
    strBuffer = Space(512)
    strDesc = Space(512)
    lngCode = SQLDataSources(objEnv.henv, SQL_FETCH_NEXT, _
                             strBuffer, Len(strBuffer), intBuffer, _
                             strDesc, Len(strDesc), intDesc)
  Loop
End Sub

Private Sub Form_Load()
 
  txtUser = objAutoan.strUserName
  txtPassword = objAutoan.strPassword
  Call SearchSources(objAutoan.strDataSource)

  Me.Height = intConstSmallWinSize

  If cboSources.ListIndex = -1 Then
    cmdAdvanced.Value = True
  End If
End Sub

Private Sub cmdAccept_Click()
  If Not objAutoan.FConnect(cboSources.Text, txtUser, txtPassword) Then
    On Error GoTo Autoanerror
    Call txtUser.SetFocus
    txtUser.SelStart = 0
    txtUser.SelLength = Len(txtUser)
  Else
    With objAutoan
      .strUserName = txtUser
      .strPassword = txtPassword
      .strDataSource = cboSources.Text
    End With
    blnConnection = True
    Call Hide
  End If
  Exit Sub
  
Autoanerror:
  Call objError.InternalError(Me, "Fconnect", "Conectando BD")
End Sub

Private Sub cmdAdvanced_Click()
  Me.Height = IIf(Me.Height = intConstBigWinSize, _
                  intConstSmallWinSize, _
                  intConstBigWinSize)
End Sub

Private Sub cmdCancel_Click()
  blnConnection = False
  Call Hide
End Sub

Private Sub cmdODBC_Click()
  Call LaunchApplet("odbccp32.cpl")
End Sub

Private Sub cboSources_GotFocus()
  Call MakeBigWindow
End Sub

Private Sub cmdODBC_GotFocus()
  Call MakeBigWindow
End Sub

Private Sub MakeBigWindow()
  If Me.Height = intConstSmallWinSize Then
    Me.Height = intConstBigWinSize
  End If
End Sub
Private Sub LaunchApplet(ByVal strApplet As String)
  On Error Resume Next
  Call Shell("Rundll32.exe shell32.dll,Control_RunDLL " & strApplet)
End Sub

