# Microsoft Developer Studio Project File - Name="datAccess" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=datAccess - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "datAccess.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "datAccess.mak" CFG="datAccess - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "datAccess - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "datAccess - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""$/AppCUN/Servidor/dataAccess", XBFAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe

!IF  "$(CFG)" == "datAccess - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\datAccess.lib"

!ELSEIF  "$(CFG)" == "datAccess - Win32 Debug"

# PROP BASE Use_MFC 1
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "datAcces"
# PROP BASE Intermediate_Dir "datAcces"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /Zi /Od /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /YX /FD /c
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"..\datAccess.lib"
# ADD LIB32 /nologo /out:"..\datAccess.lib"

!ENDIF 

# Begin Target

# Name "datAccess - Win32 Release"
# Name "datAccess - Win32 Debug"
# Begin Source File

SOURCE=..\include\BindVar.h
# End Source File
# Begin Source File

SOURCE=..\src\dataacss.cpp
# End Source File
# Begin Source File

SOURCE=..\include\dataacss.h
# End Source File
# Begin Source File

SOURCE=..\src\dserv.cpp
# End Source File
# Begin Source File

SOURCE=..\include\Dserv.h
# End Source File
# Begin Source File

SOURCE=..\src\Dserv.pc

!IF  "$(CFG)" == "datAccess - Win32 Release"

# Begin Custom Build
InputPath=..\src\Dserv.pc
InputName=Dserv

"..\src\$(InputName).cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	f:\orant\bin\proc21 $(InputName).pre

# End Custom Build

!ELSEIF  "$(CFG)" == "datAccess - Win32 Debug"

# Begin Custom Build
InputPath=..\src\Dserv.pc
InputName=Dserv

"..\src\$(InputName).cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	f:\orant\bin\proc21 $(InputName).pre

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\include\Errores.h
# End Source File
# End Target
# End Project
