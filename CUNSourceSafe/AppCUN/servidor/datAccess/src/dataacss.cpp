#include <dataacss.h>
#include <Clinic.h>
#include <calendar.h>

#include "Dserv.h"
#include "Errores.h"


DataBaseAccess::DataBaseAccess(char* usuario, char* passwd, char* server,
                                                    unsigned long& v_result)
{
	v_result = gen_connect_ora( usuario, passwd, server );

}

DataBaseAccess::~DataBaseAccess()
{
	gen_disconnect_ora();

}

void DataBaseAccess::initializeForm()
{
	gen_read_period( _form, v_period_fechad, v_period_fechah );

  readProperties();

}



long DataBaseAccess::readCalendars(MedicalServiceSingle* mss)
{
  long  v_result = GENE0000_NORMAL;
	char	v_fecha_d[C_GEN_LEN_FECHA + 1] = "";
	char	v_fecha_h[C_GEN_LEN_FECHA + 1] = "";

  TimeStep tStep = _form->getCalendar()->getTimeStep();
  IlcInt timeStepPack = _form->getCalendar()->getTimeStepPack();

	strcpy( v_fecha_d, v_period_fechad );
	strcpy( v_fecha_h, v_period_fechah );
	v_result = gen_read_calendar( mss, tStep, timeStepPack, 
                                v_fecha_d, v_fecha_h );

  if ( v_result != GENE0000_NORMAL )
    return (v_result);

  mss->setUpCalendar();

  return (v_result);
}



void DataBaseAccess::readMedicalFactTypes()
{
  ClinicApplicationScheduler* appScheduler = _form->getClinicApplicationScheduler();
	IlcInt				numMedicalFactTypes;
	unsigned int	v_num_codact = 0;

	///////////////////////////////////////////////////////////////////////
	IlcAnyArray states;
	IlcAnySet cjtoStates;
	///////////////////////////////////////////////////////////////////////

	gen_count_registros( "PR0200",
												"",
												v_num_codact );

	numMedicalFactTypes = v_num_codact;

	states = IlcAnyArray(appScheduler->getManager(), numMedicalFactTypes);

	gen_read_act_dep( states );

	cjtoStates = IlcAnySet(appScheduler->getManager(), states);

}



void DataBaseAccess::readMedicalFactGroups()
{

  const IlcInt NUM_MEDICAL_FACTS = 1;


	unsigned int	cod_act_planif = 56;


  MedicalFactGroup* mfg = new MedicalFactGroup(_form, NUM_MEDICAL_FACTS);

	Date*	fechap = new Date( 30,12,1997,10,30);
	gen_read_fases_recursos( _form, mfg, 0, cod_act_planif);

	_form->addMedicalFactGroup(mfg);

}



void DataBaseAccess::readClinicClient(
                                      const IlcInt codPeticion,
                                      const IlcInt codClient
                                      )
{
	// Declaraci�n de variables.
	char	        v_fecha_d[C_GEN_LEN_FECHA + 1] = "";
	char	        v_fecha_h[C_GEN_LEN_FECHA + 1] = "";
	char	        v_where[C_GEN_LEN_WHERE + 1] = "";
	char*         name = new char[C_GEN_LEN_NOMBRE_CLIENTE + 1];
  unsigned int  v_count_properties = 0;
  ClinicClient* client;

  strcpy( v_fecha_d, v_period_fechad );
  strcpy( v_fecha_h, v_period_fechah );

  client = _form->getClinicApplication()->findClinicClient(codClient);

  if ( client == 0 )
  {
	  sprintf( v_where, "CI21CODPERSONA = %d", codClient );
	  gen_obtener_dato( "CI2200",
									    v_where,
									    "CI22NOMBRE",
									    name );

    client= new ClinicClient(_form, name, codClient);

    gen_read_citas_paciente(client, codClient, v_fecha_d, v_fecha_h);

  } /* fin del if */

  sprintf( v_where, "PR09NUMPETICION = %d", codPeticion );
  gen_count_registros( "PR4700",
                       v_where,
                       v_count_properties );

  if ( v_count_properties > 0 )
  { /* hay restricciones para petici�n y paciente en curso */

    gen_read_property_client( client, codPeticion, v_fecha_h );

  } /* fin del if */

}



long DataBaseAccess::readMedicalServiceType(const IlcInt codTypeRec,
                                            const IlcInt codMedicalFact,
                                            const IlcInt codDpto, 
                                            const IlcInt numPhase)
{
	// Declaraci�n de variables.
  long                v_result = GENE0000_NORMAL;
	unsigned int				v_count = 0;
	char								v_where[C_GEN_LEN_WHERE + 1] = "";
	char*								name = new char[C_GEN_LEN_DESC_RECURSO + 1];
  ClinicApplication*	app = _form->getClinicApplication();
	MedicalServiceAlternative*	msa;
  char                v_cod_dpto[C_GEN_LEN_COD_DEPARTAMENTO + 1] = "";
  char                v_cod_act[C_GEN_LEN_COD_ACTUACION + 1] = "";
  char                v_num_phase[C_GEN_LEN_DESFASE + 1] = "";

	msa = (MedicalServiceAlternative*)app->findMedicalService( codTypeRec, 
        codMedicalFact, codDpto, numPhase, ALTERNATIVE );

	if ( msa == 0 )
	{ /* no existe */

		sprintf( v_where, "AG14CODTIPRECU = %d", codTypeRec );
		gen_obtener_dato( "AG1400",
												v_where,
												"AG14DESTIPRECU",
												name );
    sprintf( v_cod_act, "%d", codMedicalFact );
    strcat( name, v_cod_act );
    sprintf( v_cod_dpto, "%d", codDpto );
    strcat( name, v_cod_dpto );
    sprintf( v_num_phase, "%d", numPhase );
    strcat( name, v_num_phase );

	if ( codDpto != 0 )
		sprintf( v_where, "PR01CODACTUACION= %d AND PR1100.AD02CODDPTO = %d \
						   AND PR05NUMFASE = %d AND PR1100.AD02CODDPTO = \
						   AG1100.AD02CODDPTO \
						   AND PR1100.AG11CODRECURSO=AG1100.AG11CODRECURSO AND \
						   AG11INDPLANIFI = -1 AND AG14CODTIPRECU=%d AND \
						   ( AG11FECFINVREC IS NULL OR AG11FECFINVREC > SYSDATE)",
				 codMedicalFact, codDpto, numPhase, codTypeRec );
	else
		sprintf( v_where, "PR01CODACTUACION= %d AND PR05NUMFASE = %d AND \
						   PR1100.AG11CODRECURSO=AG1100.AG11CODRECURSO AND \
						   AG11INDPLANIFI = -1 AND AG14CODTIPRECU=%d AND \
						   ( AG11FECFINVREC IS NULL OR AG11FECFINVREC > SYSDATE)",
				 codMedicalFact, numPhase, codTypeRec );

	gen_count_registros( "PR1100,AG1100",
						 v_where,
						 v_count );

	if ( v_count == 0 )
	{
		if ( codDpto != 0 )
		  sprintf( v_where,
			  "AG14CODTIPRECU =%d AND AG11INDPLANIFI=-1 AND \
               AD02CODDPTO = %d AND ( AG11FECFINVREC IS NULL OR \
               AG11FECFINVREC > SYSDATE)",
               codTypeRec, codDpto );
		else
			sprintf( v_where,
				"AG14CODTIPRECU =%d AND AG11INDPLANIFI=-1 AND \
               ( AG11FECFINVREC IS NULL OR \
               AG11FECFINVREC > SYSDATE)",
               codTypeRec);

		gen_count_registros( "AG1100",
													v_where,
													v_count );						
	}

    if ( v_count > 0 )
    { //alg�n recurso simple planificable y vigente.

		  IlcInt numServices = v_count;
		  msa = new MedicalServiceAlternative(app, name, numServices, codTypeRec,
                  codMedicalFact, codDpto, numPhase );

	    v_result = gen_read_recursos_simples( msa, codTypeRec, codMedicalFact, codDpto,
                                            numPhase );

      if ( v_result != GENE0000_NORMAL )
      {
        return (v_result);

      }

    } /* fin del if */
    else
      v_result = GENE0005_ERROR_NO_RECURSOS;

	} /* fin del if */

  return (v_result);

}



MedicalServiceSingle*	DataBaseAccess::readMedicalService(const IlcInt codRecurso,
                                                         long&        v_result)
{
  // Declaraci�n de variables.
  long  v_err = GENE0000_NORMAL;
	char	v_fecha_d[C_GEN_LEN_FECHA + 1] = "";
	char	v_fecha_h[C_GEN_LEN_FECHA + 1] = "";
	char	v_where[C_GEN_LEN_WHERE + 1] = "";
  char	v_sentencia[C_GEN_LEN_SENTENCIA + 1] = "";
	char* name = new char[C_GEN_LEN_DESC_RECURSO + 1];
	char	unidades[C_GEN_LEN_UNIDADES_RECURSO + 1] = "";
	int		num_unidades = 0;
	char	modoasig[C_GEN_LEN_MODO_ASIG + 1]  ="";
  unsigned long v_existe = GENE0037_REGISTRO_EXISTE;


	ReserveType reserveType;

  ClinicApplication* app = _form->getClinicApplication();
  ClinicApplicationScheduler* appScheduler = _form->getClinicApplicationScheduler();

  MedicalServiceSingle*		mss;
  PropertyConditionTree*	tree;

	mss = (MedicalServiceSingle*)app->findMedicalService( codRecurso );

	if ( mss == 0 )
	{ /* no existe */


    sprintf( v_sentencia, "SELECT 1 FROM AG1100 WHERE AG11CODRECURSO=%d \
             AND AG11INDPLANIFI=-1 AND ( AG11FECFINVREC IS NULL OR \
             TO_DATE( AG11FECFINVREC ,'DD-MM-YYYY') > \
             TO_DATE(SYSDATE,'DD-MM-YYYY'))", codRecurso );
    v_existe = gen_existe_registro( v_sentencia );

    if ( v_existe == GENE0037_REGISTRO_EXISTE )
    { //recurso simple planificable y vigente

		  sprintf( v_where, "AG11CODRECURSO = %d", codRecurso );
      gen_obtener_dato( "AG1100",
												  v_where,
												  "AG11DESRECURSO",
												  name );

		  gen_obtener_dato( "AG1100",
												  v_where,
												  "AG11NUMUNIDREC",
												  unidades );		
		  num_unidades = atoi(unidades);

		  gen_obtener_dato( "AG1100",
												  v_where,
												  "AG11MODASIGCITA",
												  modoasig );
		  if ( !strcmp( modoasig, "1" ))
		  { /* Por cantidad */

			  reserveType = QUOTA;

		  } /* fin del if */
		  else if ( !strcmp( modoasig, "2" ))
		  { /* Secuencial */

			  reserveType = SEQUENTIAL_INTERVAL;

		  } /* fin del else if */
		  else if ( !strcmp( modoasig, "3" ))
		  { /* Intervalos predeterminados */

			  reserveType = FIX_INTERVAL;

		  } /* fin del else if */

      mss = new MedicalServiceSingle(app, name, reserveType, codRecurso);

		  v_err = readCalendars(mss);

      if ( v_err == GENE0000_NORMAL )
      { //existe alg�n calendario para el recurso actual

		    tree = gen_read_tree( app, codRecurso );

		    mss->setPropertyConditionTree(tree);

		    gen_acotar_period_rec( codRecurso, v_period_fechad, v_period_fechah,
				v_fecha_d, v_fecha_h );

 		    gen_read_profiles_bands( mss, app, appScheduler, codRecurso,
														v_fecha_d, v_fecha_h );
        mss->setUpIntervalStates();

		    mss->setUpProfiles();

		    gen_read_citas_recurso(mss, codRecurso, v_fecha_d, v_fecha_h);

		    mss->setUpBusy();

      } /* fin del if */
      else
      { // calendario incorrecto

      } /* fin del else */

    } /* fin del if */
    else
    { //recurso no planificable � no vigente en periodo de estudio

      v_err = GENE0005_ERROR_NO_RECURSOS;

    } /* fin del else */

	} /* fin del if */

  v_result = v_err;

	return (mss);

}



void DataBaseAccess::readProperties()
{
  ClinicApplication* app= _form->getClinicApplication();
 
	gen_read_properties( app );

}