#ifndef __DATABASEACCESS_H_
#define __DATABASEACCESS_H_

#include <clinic.h>
#include <Dserv.h>

#include <fstream.h>
#include <ostream.h>

#include "stdafx.h"
#include "servidor.h"
#include "servidorDoc.h"

class DataBaseAccess
{
	char	v_period_fechad[C_GEN_LEN_FECHA + 1];
	char	v_period_fechah[C_GEN_LEN_FECHA + 1];

  ClinicForm* _form;
	CServidorDoc*	_server;


  long readCalendars(MedicalServiceSingle* mss);
  void readProperties();
  void readMedicalFactTypes();
  void readMedicalFactGroups();

public:
	DataBaseAccess(char*	usuario, char* passwd, char* servidor,
                 unsigned long& v_result);

	~DataBaseAccess();

  void initializeForm();
	void setForm(ClinicForm* form) { _form = form;}
	void setServidorDoc(CServidorDoc* server) { _server = server; }

	void readClinicClient( const IlcInt codPeticion, const IlcInt codClient);
  MedicalServiceSingle*	readMedicalService(const IlcInt codRecurso,
                                           long&  v_result);
	long readMedicalServiceType(const IlcInt codTypeRec, const IlcInt codMedicalFact,
                              const IlcInt codDpto, const IlcInt numPhase);

	ClinicForm* getClinicForm() { return _form; }
	CServidorDoc*	getServidorDoc() { return _server; }

};

#endif //__DATABASEACCESS_H_

