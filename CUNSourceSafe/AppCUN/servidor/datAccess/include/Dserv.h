#ifndef _DSERV_H
#define _DSERV_H

#include <Clinic.h>
#include <dataacss.h>

/*
 *******************************************************************************
 *
 *  Declaraci�n de constantes.
 *
 *******************************************************************************
 */
//Constante que contiene la longitud del string del n�mero de d�a.
const unsigned long C_GEN_LEN_DIA = 2;

//Constante que contiene la longitud del string del n�mero de mes.
const unsigned long C_GEN_LEN_MES = 2;

//Constante que contiene la longitud del string del a�o.
const unsigned long C_GEN_LEN_ANIO = 4;

//Constante que contiene la longitud del string de la fecha.
const unsigned long C_GEN_LEN_FECHA = 10;

//Constante que contiene la longitud del string del c�digo de calendario.
const unsigned long C_GEN_LEN_CODCAL = 5;

//Constante que contiene la longitud del string de la where.
const unsigned long C_GEN_LEN_WHERE = 1000;

//Constante que contiene la longitud del string de dato a recuperar de B.D.
const unsigned long C_GEN_LEN_DATO = 300;

//Constante que contiene la longitud del string del c�digo de perfil.
const unsigned long C_GEN_LEN_CODPERFIL = 3;

//Constante que contiene la longitud del string del valor desde Tipo rest.
const unsigned long C_GEN_LEN_VALOR_DESDE_HASTA_TIPEREST = 20;

//Constante que contiene la longitud del string de sentencia SQL.
const unsigned long C_GEN_LEN_SENTENCIA = 500;

//Constante que contiene la longitud del string de c�digo Actuaci�n.
const unsigned long C_GEN_LEN_COD_ACT = 9;

//Constante que contiene la longitud del string del periodo de citaci�n en min.
const unsigned long C_GEN_LEN_INTERVALO_CITA = 3;

//Constante que contiene la longitud del string del nombre del recurso.
const unsigned long C_GEN_LEN_DESC_RECURSO = 30;

//Constante que contiene la longitud del string del nombre del paciente
const unsigned long C_GEN_LEN_NOMBRE_CLIENTE = 25;

//Constante que contiene la longitud del string de la fecha y hora.
const unsigned long C_GEN_LEN_FEHOR = 16;

//Constante que contiene la longitud del string de c�digo usuario conectado.
const unsigned long C_GEN_LEN_COD_USUARIO = 6;

//Constante que contiene la longitud del string del n�mero de unidades rec. simple.
const unsigned long C_GEN_LEN_UNIDADES_RECURSO = 3;

//Constante que contiene la longitud del string del c�dgo actuaci�n.
const unsigned long C_GEN_LEN_COD_ACTUACION = 9;

//Constante que contiene la longitud del string del c�digo paciente
const unsigned long C_GEN_LEN_COD_PACIENTE = 7;

//Constante que contiene la longitud del string del c�digo departamento.
const unsigned long C_GEN_LEN_COD_DEPARTAMENTO = 3;

//Constante que contiene la longitud del string del grano de tiempo.
const unsigned long C_GEN_LEN_GRANO_TIEMPO = 2;

//Constante que contiene la longitud del string del modo asignaci�n del recurso.
const unsigned long C_GEN_LEN_MODO_ASIG = 1;

//Constante que contiene los minutos contenidos en un d�a.
const unsigned long C_GEN_MINUTOS_EN_DIA = 1440;

//Constante que contiene los minutos contenidos en una hora.
const unsigned long C_GEN_MINUTOS_EN_HORA = 60;

//Constante que contiene la longitud del string del c�digo de tipo recurso.
const unsigned long C_GEN_LEN_COD_RECURSO = 3;

//Constante que contiene la longitud del string del desfase del recurso.
const unsigned long C_GEN_LEN_DESFASE = 5;

/*
 *******************************************************************************
 *
 *  Declaraci�n de estructuras.
 *
 *******************************************************************************
 */
	typedef struct t_datos_no
		{
		int		v_level_ant;
		int		v_cod_tiprest_ant;
		char	v_desde_tiprest_ant[C_GEN_LEN_VALOR_DESDE_HASTA_TIPEREST + 1];
		char	v_hasta_tiprest_ant[C_GEN_LEN_VALOR_DESDE_HASTA_TIPEREST + 1];
		int		v_ind_inex_ant;
		} t_datos_nodo;

/*
 *******************************************************************************
 *
 *  Prototipado de los diferentes servicios.
 *
 *******************************************************************************
 */

unsigned long gen_connect_ora(
										          char*	  usuario,
										          char*	  passwd,
                              char*   servidor
										         );

void gen_disconnect_ora();

void gen_error_ora( 
                    unsigned long		ae_condi,
										unsigned long&	as_error
			   					);

unsigned long gen_obtener_dato( 
                                char*		ae_tabla,
						                    char*   ae_where,
                        				char*   ae_campo,
                        				char*   as_dato
															);

unsigned long
gen_count_registros ( 
 											char* 				ae_tabla,
                      char* 				ae_where,
                      unsigned int&	as_count
										);

unsigned long
gen_existe_registro ( 
 											char* 			ae_sentencia_sql
										);

unsigned long
gen_ejecutar_sentencia ( 
 											  char* 			ae_sentencia_sql
										   );
										
unsigned long
gen_fecha_numero ( 
                    char*	 ae_fecha 
									);

void gen_read_period( 
											ClinicForm*	_form, 
											char*				as_fecha_d,
											char*				as_fecha_h
										);

long gen_read_calendar( 
											  MedicalServiceSingle* mss,
												TimeStep							tStep,
												IlcInt								timeStepPack,
												char*									ae_fecha_d,
												char*									ae_fecha_h
											);

void gen_read_profiles_bands( 
															MedicalServiceSingle*				mss,
															ClinicApplication*					app,
															ClinicApplicationScheduler*	appScheduler,
															const IlcInt								codRecurso,
															char*												ae_fecha_d,
															char*												ae_fecha_h
														);

void gen_read_properties(
													ClinicApplication* app
												);

PropertyConditionTree*
gen_read_tree(
								ClinicApplication*			app,								
								const IlcInt						codRecurso
							);

PropertyConditionTree*
gen_read_tree_band(
										ClinicApplication*	app,								
										const IlcInt				codRecurso,
										const int						codPerfil,
										const int						codFranja
							    );

void gen_read_bands( 
											ClinicApplication*							app,
											ClinicApplicationScheduler*			appScheduler,
											MedicalServiceCalendarProfile*	profile,
											const IlcInt										codRecurso,
											const int												codPerfil,
											char*														periodd,
											char*														periodh
										);

void
gen_desglosar_fechad( 
											char*					ae_fecha,
											unsigned int&	as_dia,
											unsigned int&	as_mes,
											unsigned int&	as_anio
										);

void
gen_desglosar_fechah( 
											char*					ae_fecha,
											unsigned int&	as_dia,
											unsigned int&	as_mes,
											unsigned int&	as_anio
										);

void
gen_acotar_period_rec( 
												const IlcInt	codRecurso,
												char*					ae_period_fechad,
												char*					ae_period_fechah,
												char*					as_fecha_d,
												char*					as_fecha_h
											);

void
gen_read_act_dep( 
									IlcAnyArray	states
								);

void
gen_read_citas_paciente(
													ClinicClient*		client,
													const IlcInt		codClient,
													char*						ae_fecha_d,
													char*						ae_fecha_h
												);

void
gen_read_citas_recurso(
												MedicalServiceSingle*	mss,
												const IlcInt					codRecurso,
												char*									ae_fecha_d,
												char*									ae_fecha_h
											);

void
gen_desglosar_fecha_hora( 
													char*					ae_fehor,
													unsigned int&	as_hora,
													unsigned int&	as_min
												);

long
gen_read_recursos_simples( 
														MedicalServiceAlternative*	msa,
														const IlcInt								codTypeRec,
                            const IlcInt                codMedicalFact,
                            const IlcInt                codDpto, 
                            const IlcInt                numPhase
													);

long
gen_read_fases_recursos( 
													ClinicForm*					_form,
													MedicalFactGroup*		mfg,
													unsigned int				cod_act_planif,
                          long                recursoPref,
													Date*								fechaPref = NULL
												);

void
gen_obtener_duracion( 
                      unsigned long		minutos,
											unsigned long&	dias,
											unsigned long&	horas,
											unsigned long&	min 
                    );

void
gen_read_iterations(
										  unsigned int		cod_act_planif
										);

void
gen_read_property_client( 
                          ClinicClient* client,
                          const IlcInt  codPeticion, 
                          char*         v_fecha_h
                         );

#endif