#ifndef _ERRORES_H
#define _ERRORES_H

const unsigned long GENE0000_NORMAL = 0;

const unsigned long GENE0001_SOLUCION = 1;

const long GENE0001_ERROR_NO_FASES = -1;

const long GENE0001_ERROR_NO_ACTUACION = -1;

const long GENE0002_ERROR_NO_RECURSOS = -2;

const long GENE0004_ERROR_NO_CALENDARIO = -4;

const long GENE0005_ERROR_NO_RECURSOS = -5;

const unsigned long GEN_ERROR_ORA_AE_CONDI_ERROR = 100;

const unsigned long GENE0014_ORAERRORNOTIP = 14;

const unsigned long GENE0033_REGISTRO_NO_EXISTE = 33;

const unsigned long GENE0037_REGISTRO_EXISTE = 37;

const unsigned long GENE0043_TABLA_NO_EXISTE = 43;

const unsigned long GENE0046_NO_CONNECT = 46;

const unsigned long GENE0048_SENTEN_ERROR = 48;

const unsigned long GENE0057_REGISTRO_BLOQUEADO = 57;

const unsigned long GEN_ERROR_ORA_AE_CONDI_WARNING = 200;

const unsigned long GENE0015_ORAWARNINGNOTIP = 215;

const unsigned long GEN_ERROR_ORA_AE_CONDI_NOT_FOUND = 300;

const unsigned long GENE0013_REGNOTFOUND = 313;

#endif