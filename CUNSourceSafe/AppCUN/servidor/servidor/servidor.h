// servidor.h : main header file for the SERVIDOR application
//

#if !defined(AFX_SERVIDOR_H__BA1BADEA_7220_11D1_8276_00C04FB77FAE__INCLUDED_)
#define AFX_SERVIDOR_H__BA1BADEA_7220_11D1_8276_00C04FB77FAE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CServidorApp:
// See servidor.cpp for the implementation of this class
//

class CServidorApp : public CWinApp
{
public:
	CServidorApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServidorApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	COleTemplateServer m_server;
		// Server object for document creation

	//{{AFX_MSG(CServidorApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVIDOR_H__BA1BADEA_7220_11D1_8276_00C04FB77FAE__INCLUDED_)
