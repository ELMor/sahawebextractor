// servidorDoc.h : interface of the CServidorDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVIDORDOC_H__BA1BADF1_7220_11D1_8276_00C04FB77FAE__INCLUDED_)
#define AFX_SERVIDORDOC_H__BA1BADF1_7220_11D1_8276_00C04FB77FAE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


class CServidorDoc : public CDocument
{
protected: // create from serialization only
	CServidorDoc();
	DECLARE_DYNCREATE(CServidorDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServidorDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	char* getServerBd();
	void getDate(int dia,int mes, int anio,char* fecha);
	long Recurso[30][30][30];
	long NumRecursos[30][30];
	long getIndicePhase(long codAct, long codPhase);
	long getIndiceAct(long codAct);
	long getCodActConfirm(long cont);
	long cont_rec;
	long cont_phase;
	long cont_act;
	char* getPasswdBd();
	char* getUsuarioBd();
	long getRecursoPref(long cont);
	long contador;
	long getMinPrefAct(long cont);
	long getHoraPrefAct(long cont);
	long getAnioPrefAct(long cont);
	long getMesPrefAct(long cont);
	long getDiaPrefAct(long cont);
	long getCodAct(long cont);
	long getNumAct();
	long getMinHasta();
	long getHoraHasta();
	long getMinDesde();
	long getAnioDesde();
	long getHoraDesde();
	long getAnioHasta();
	long getMesHasta();
	long getDiaHasta();
	long getMesDesde();
	long getDiaDesde();
	virtual ~CServidorDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CServidorDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CServidorDoc)
	afx_msg void SetDiaDesde(long diad);
	afx_msg void SetMesDesde(long mesd);
	afx_msg void SetAnioDesde(long aniod);
	afx_msg void SetDiaHasta(long diah);
	afx_msg void SetMesHasta(long mesh);
	afx_msg void SetAnioHasta(long anioh);
	afx_msg void SetHoraDesde(long horad);
	afx_msg void SetMinDesde(long mind);
	afx_msg void SetHoraHasta(long horah);
	afx_msg void SetMinHasta(long minh);
	afx_msg void SetDiaSemana(long indice);
	afx_msg void SetNumAct(long num);
	afx_msg void SetCodAct(long codigo);
	afx_msg long GetNumPhases(long ind_act);
	afx_msg long GetDurationPhaseDias(long ind_act, long ind_phase);
	afx_msg long GetDurationPhaseHoras(long ind_act, long ind_phase);
	afx_msg long GetDurationPhaseMin(long ind_act, long ind_phase);
	afx_msg long GetNumRecursos(long ind_act, long ind_phase);
	afx_msg long GetDurationRecursoDias(long ind_act, long ind_phase, long ind_rec);
	afx_msg long GetDurationRecursoHoras(long ind_act, long ind_phase, long ind_rec);
	afx_msg long GetDurationRecursoMin(long ind_act, long ind_phase, long ind_rec);
	afx_msg void SetUsuarioBd(LPCTSTR usuario);
	afx_msg void SetPasswdBd(LPCTSTR passwd);
	afx_msg long GetNumPhase(long ind_act, long ind_phase);
	afx_msg BSTR GetDescPhase(long ind_act, long ind_phase);
	afx_msg long Confirm();
	afx_msg void SetStartRecursoDia(long act, long phase, long recurso, long dia);
	afx_msg void SetStartRecursoMes(long act, long phase, long recurso, long mes);
	afx_msg void SetStartRecursoAnio(long act, long phase, long recurso, long anio);
	afx_msg void SetStartRecursoHora(long act, long phase, long recurso, long hora);
	afx_msg void SetStartRecursoMin(long act, long phase, long recurso, long min);
	afx_msg void SetStartPhaseDia(long act, long phase, long dia);
	afx_msg void SetStartPhaseMes(long act, long phase, long mes);
	afx_msg void SetStartPhaseAnio(long act, long phase, long anio);
	afx_msg void SetStartPhaseHora(long act, long phase, long hora);
	afx_msg void SetStartPhaseMin(long act, long phase, long min);
	afx_msg long GetStartPhaseDia(long ind_solution, long ind_act, long ind_phase);
	afx_msg long GetStartPhaseMes(long ind_solution, long ind_act, long ind_phase);
	afx_msg long GetStartPhaseAnio(long ind_solution, long ind_act, long ind_phase);
	afx_msg long GetStartPhaseHora(long ind_solution, long ind_act, long ind_phase);
	afx_msg long GetStartPhaseMin(long ind_solution, long ind_act, long ind_phase);
	afx_msg long GetStartRecursoDia(long ind_solution, long ind_act, long ind_phase, long ind_rec);
	afx_msg long GetStartRecursoMes(long ind_solution, long ind_act, long ind_phase, long ind_rec);
	afx_msg long GetStartRecursoAnio(long ind_solution, long ind_act, long ind_phase, long ind_rec);
	afx_msg long GetStartRecursoHora(long ind_solution, long ind_act, long ind_phase, long ind_rec);
	afx_msg long GetStartRecursoMin(long ind_solution, long ind_act, long ind_phase, long ind_rec);
	afx_msg long Run(long num_solutions);
	afx_msg void SetDiaPrefAct(long ind_act, long diap);
	afx_msg void SetMesPrefAct(long ind_act, long mesp);
	afx_msg void SetAnioPrefAct(long ind_act, long aniop);
	afx_msg void SetHoraPrefAct(long ind_act, long horap);
	afx_msg void SetMinPrefAct(long ind_act, long minp);
	afx_msg void SetRecursoPreferente(long ind_act, long recurso_pref);
	afx_msg long GetCodRecurso(long ind_solution, long ind_act, long ind_phase, long ind_rec);
	afx_msg long GetRecursoPreferente(long ind_solution, long ind_act);
	afx_msg long GetNumPhaseRecPref(long ind_act);
	afx_msg long MoreSolutions(long num_solutions);
	afx_msg void Initialize();
	afx_msg long GetNumRecCapacity();
	afx_msg long GetCodRecCapacity(long ind_rec);
	afx_msg long GetLongDay();
	afx_msg long GetCapacity(long ind_rec, long dia, long mes, long anio, long ind);
	afx_msg void SetServerBd(LPCTSTR server);
	afx_msg void Close();
	afx_msg void GetObtenerFechaHasta(long ae_diah, long ae_mesh, long ae_anioh);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	char server_bd[30];
	long NumPhases[30];
	long cdAct;
	long cdPhase;
	long cdRec;
	long Phase[30][30];
	long Actuacion[30];
	long diaRec[30][30][30];
	long mesRec[30][30][30];
	long anioRec[30][30][30];
	long horaRec[30][30][30];
	long minRec[30][30][30];
	long diaPhase[30][30];
	long mesPhase[30][30];
	long anioPhase[30][30];
	long horaPhase[30][30];
	long minPhase[30][30];
	char passwd_bd[20];
	char usuario_bd[20];
	long recPref[30];
	long minPref[30];
	long horaPref[30];
	long anioPref[30];
	long mesPref[30];
	long diaPref[30];
	long codAct[30];
	long numAct;
	long diaSemana[7];
	long minHasta;
	long horaHasta;
	long minDesde;
	long horaDesde;
	long anioHasta;
	long mesHasta;
	long diaHasta;
	long anioDesde;
	long mesDesde;
	long diaDesde;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVIDORDOC_H__BA1BADF1_7220_11D1_8276_00C04FB77FAE__INCLUDED_)
