#ifndef __CHECKUP_H_
#define __CHECKUP_H_

#include /**/<ilsched/intact.h>
#include <service.h>
#include <calendar.h>

// Forward Declaration.
class ClinicClient;
class ClinicApplication;
class ClinicForm;

class MedicalFact;
class MedicalFactGroup;
class MedicalFactAllIterations;


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.h                                            */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase Phase.                    */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997 / 14-10-1997                               */
/*                                                                */
/******************************************************************
*/
class Phase 
{
  char _name[255];
  IntervalActivityCalendar _reserve;
  Duration _duration;

  MedicalServiceReserve** _services;
  IlcInt _numServices;
  IlcInt _currentService;
	IlcInt _numMedicalServicePref;

  MedicalFact* _medicalFact;
	IlcInt _codFase;

public:
	Phase* _phase;

	IlcInt _ind;

public:
  Phase(MedicalFact* mf, const char* name, Duration duration, 
        IlcInt numServices,IlcInt codFase=0);
  ~Phase();

  IntervalActivityCalendar getReserve() const { return _reserve; }
  MedicalServiceReserve* getMedicalServiceReserve(IlcInt i) const;
  MedicalFact* getMedicalFact() const { return _medicalFact; }
  void addMedicalServiceReserve(MedicalService* , Duration delay,
                                Duration duration, IlcInt capacity=1);
  char* getName() { return _name; }

	IlcInt getCurrentService() const { return _currentService; }
	IlcInt getCodFase() const { return _codFase; }

  void printOn(ostream& os) const;
  Duration getDuration() const { return _duration; }

	void setMedicalServicePref() { _numMedicalServicePref = _currentService;}
	IlcInt getMedicalServicePref() { return(_numMedicalServicePref);}

};


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.h                                             */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase MedicalFactIteration      */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class MedicalFactIteration
{
  IlcInt  _codMedicalFact;
  IlcInt  _dias;
  IlcInt  _horas;
  IlcInt  _minutos;
  MedicalFactAllIterations* _allIterations;

public:
  MedicalFactIteration(IlcInt dias, IlcInt horas, IlcInt minutos, 
                      IlcInt codMedicalFact,
                      MedicalFactAllIterations* allIterations):
                      _dias(dias),_horas(horas),_minutos(minutos),
                      _codMedicalFact(codMedicalFact),
                      _allIterations(allIterations){}

  IlcInt getCodigo() const { return _codMedicalFact; }
  IlcInt getDias() const { return _dias; }
  IlcInt getHoras() const { return _horas; }
  IlcInt getMinutos() const { return _minutos; }
  MedicalFactAllIterations* getAllIterations() const { return _allIterations; }
  void printOn();

};



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.h                                             */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase MedicalFactAllIterations  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class MedicalFactAllIterations
{
  MedicalFactIteration** _iterations;
  IlcInt _numIterations;
  IlcInt _currentIterations;

  MedicalFact* _medicalFact;

public:
	MedicalFactAllIterations(MedicalFact* medicalFact);

  void addIteration(MedicalFactIteration* iteration);
  MedicalFactIteration* getIteration(IlcInt Index);
  IlcInt getNumMedicalFactAllIterations() const { return _numIterations; }
  IlcInt getCurrentMedicalFactAllIterations() const { return _currentIterations; }

  MedicalFact* getMedicalFact() const { return _medicalFact; }
  void printOn();
};



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.h                                            */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase MedicalFact.              */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class MedicalFact 
{
protected:
  Phase** _phases;
  IlcInt _currentPhase;
  IlcInt _numPhases;

  IlcActivityArray _phasesReserves;
  IntervalActivityCalendar _action;

  ClinicClient* _client;

  const IlcInt _codClient;
	char _name[255];

  IlcInt _type;
	IlcInt _dpto;
  IlcInt  _codigo;

  MedicalFactGroup* _medicalFactGroup;
  Date* _fechaPref;

  MedicalFactAllIterations* _iterations;

public:
  MedicalFact(MedicalFactGroup* medicalFactGroup, 
              const char* name, IlcInt type, IlcInt numPhases, 
              const IlcInt codClient,const IlcInt dpto,
							Date* fechaPref=NULL);
  ~MedicalFact();

  IntervalActivityCalendar getAction() const { return _action; }
  const IlcInt getCodClient() const { return _codClient; }

  IlcInt getType() const { return _type; }
	IlcInt getDpto() const { return _dpto; }
	IlcInt getState();

  MedicalFactAllIterations* getMedicalFactIterations() const { return _iterations; }
  void setMedicalFactIterations(MedicalFactAllIterations* iterations) 
  { 
    _iterations = iterations; 
  }

  IlcInt getNumPhases() const { return _numPhases; }
	IlcInt getCurrentPhase() const { return _currentPhase; }
  Phase* getPhase(IlcInt i) const;
  void addPhase(Phase* phase,Phase* phaseBefore = NULL,IlcInt ind=1,
								Duration* durMin=NULL, Duration* durMax=NULL);

  ClinicClient* getClinicClient() const { return _client; }
	char* getName() { return _name; }

  MedicalFactGroup* getMedicalFactGroup() const { return _medicalFactGroup; }
  void printOn(ostream& os) const;

	IlcInt getMedicalFactRecPref();

  IlcInt getCodMedicalFact() const { return _codigo; }
  void setCodMedicalFact(IlcInt codigo) {  _codigo = codigo; }

};



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.h                                            */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase MedicalFactGroup.         */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class MedicalFactGroup 
{
	MedicalFact** _medicalFacts;
	IlcInt _numMedicalFacts;
	IlcInt _currentMedicalFacts;

	ClinicForm* _form;

public:
	MedicalFactGroup(ClinicForm* form, IlcInt numMedicalFacts);
	~MedicalFactGroup();

  MedicalFact* getActuacion(IlcInt codMedicalFact);
	MedicalFact* getMedicalFact(IlcInt i) const;
	void addMedicalFact(MedicalFact* medicalFact); 
	IlcInt getCurrentMedicalFacts() const { return _currentMedicalFacts; }
  IlcInt getCodMedicalFactClient( IlcInt codMedicalFact,IlcInt codClient);

	ClinicForm* getClinicForm() const { return _form; }
  void printOn(ostream& os) const;

  IlcInt getNumPhasesMedicalFact();
  IlcInt getCodMedicalFact( IlcInt codMedicalFact );
};

#endif // __CHECKUP_H_