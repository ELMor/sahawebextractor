#ifndef __SERVICE_H_
#define __SERVICE_H_

#include /**/<ilsched/discrete.h>
#include /**/<ilsched/intact.h>
#include /**/<ilsched/state.h>
#include /**/<ilsched/altresh.h>
#include <schedcal.h>
#include <servical.h>
#include <property.h>
#include <weekindx.h>

////////////////////////////////////////////////////////////////
// Foreward Classes Declaration
////////////////////////////////////////////////////////////////
class ClinicApplication;
class MedicalServiceReserve;
class Phase;


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.h                                            */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase MedicalServiceType        */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
enum MedicalServiceType { SINGLE,ALTERNATIVE };

class MedicalService
{
protected:
  ClinicApplication* _app;

  char _name[255];
	IlcInt _codRecurso;

  MedicalService(ClinicApplication* app, const char* name, IlcInt codRecurso = 0);

public:
  ClinicApplication* getClinicApplication() const { return _app; }
  virtual MedicalServiceReserve* createReserve(IlcInt delay, 
											                         Phase* phase,
                                               Duration duration,
																							 IlcInt capacity=1)= 0;

	virtual MedicalServiceType getTypeMedicalService() =0;

  virtual void printOn(ostream& os) const = 0;

  char* getName() { return _name; }
	const IlcInt getCodRecurso() const { return _codRecurso; }

  const IndexedCalendar* getCalendar() const;
};

/*
********************************************************************/
/*                   SYSECA BILBAO                                 */
/*                                                                 */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                      */
/*                                                                 */
/* NOMBRE:   Service.h                                             */
/*                                                                 */
/* DESCRIPCION: Implementación de la clase InternalTimeTable       */
/*                                                                 */
/* FECHA:    22-10-1997                                            */
/*                                                                 */
/*******************************************************************
*/
class InternalTimetable
{
  MedicalServiceSingle* _mss;
  IlcIntArray _capacityUsed;
  IlcAnyArray _medicalServiceCalendarBand;
public:
  InternalTimetable() {}
  InternalTimetable(MedicalServiceSingle* mss, IlcInt size);
  
  void setUsed(IlcInt index, MedicalServiceCalendarBand* band,
               IlcInt inic=1, IlcInt comienzo = -1);
  IlcBool isUsed(IlcInt index) const;
  IlcInt getSize() const { return _capacityUsed.getSize(); }
  MedicalServiceCalendarBand* getMedicalServiceCalendarBand(IlcInt i) const;
};



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.h                                            */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase MedicalServiceSingleBusy  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class MedicalServiceSingleBusy
{
public:
	Period _period;
	IlcInt _actuacion;

	Period getPeriod() const { return _period; }
	IlcInt getActuacion() const { return _actuacion; }

	MedicalServiceSingleBusy(Period period, IlcInt actuacion);
};




/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.h                                            */
/*                                                                */
/* DESCRIPCION: Implementación de la clase IntervalStates         */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class IntervalStates
{
  IlcAnySet _states;
  IlcInt _from;
  IlcInt _to;

public:

  IntervalStates(IlcAnySet states, IlcInt from, IlcInt to):_states(states),
                                                           _from(from),_to(to)
  {};

  IlcInt getFrom() const { return _from; }
  IlcInt getTo() const { return _to; }
  IlcAnySet getStates() { return _states; }

};


/*
********************************************************************/
/*                   SYSECA BILBAO                                 */
/*                                                                 */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                      */
/*                                                                 */
/* NOMBRE:   Service.h                                             */
/*                                                                 */
/* DESCRIPCION:                                                    */
/*           Implementación de la clase MedicalServiceSingle       */
/*                                                                 */
/* AUTOR:    Roberto Murga                                         */
/*                                                                 */
/* FECHA:    17-9-1997                                             */
/*                                                                 */
/*******************************************************************
*/
class MedicalServiceSingle: public MedicalService
{
protected:
	WeekIndexedCalendar* _defaultCalendar;
  WeekIndexedCalendar** _calendars;
  IlcInt _numCalendars;
  IlcInt _currentCalendar;

  DiscreteResourceCalendar _capacity; 

  IlcStateResource _possibleMedicalFacts;

  InternalTimetable _internalTimetable;
  
	IlcInt _capacidad;

  MedicalServiceCalendarProfile** _profiles;
  IlcInt _numProfiles;
  IlcInt _currentProfile;

  Date _fechaDispo;

  IlcInt _minTotales;

  ReserveType _reserveType;
										 
  PropertyConditionTree* _conditionsTree;

  void internalRegister(Period period, 
                        MedicalServiceCalendarBand* band= 0);

	MedicalServiceSingleBusy** _medicalServiceSingleBusySet;
	IlcInt _numMedicalServiceSingleBusy;
	IlcInt _currentMedicalServiceSingleBusy;

  IntervalStates** _medicalServiceSingleIntervalStates;
	IlcInt _numIntervalStates;
	IlcInt _currentIntervalStates;

public:
  MedicalServiceSingle(ClinicApplication* app, const char* name, 
                       ReserveType reserveType, 
											 IlcInt codRecurso,
											 IlcInt capacidad=200,
											 IlcInt maxProfiles= 32,
											 IlcInt maxCalendars= 32,
											 IlcInt maxBusy=1000);
  ~MedicalServiceSingle();

  DiscreteResourceCalendar getCapacity() const { return _capacity; }	
	IlcInt getCapacidad() const { return _capacidad; }

  IlcStateResource getPossibleMedicalFacts() const { return _possibleMedicalFacts; }	
  virtual MedicalServiceReserve* createReserve(IlcInt delay, Phase* phase,
                                               Duration duration,
																							 IlcInt capacity=1);
  
  void addProfile(MedicalServiceCalendarProfile* profile);

  void setFechaDispo();
  Date getFechaDispo() { return _fechaDispo; }

  void setMinTotales();
  IlcInt getMinTotales() { return _minTotales; }

  ReserveType getReserveType() const { return _reserveType; }

  void registerBand(Period validity, DayType dt, DayPeriod dayPeriod,
                    IlcInt timeStepPack, IlcInt capacity,
                    MedicalServiceCalendarBand* band,
										Date* dateBand = NULL);

  void registerMedicalFactTypes(Period period, IlcAnySet bandStates);
  
  void setUpReserve(MedicalServiceReserve* reserve);
  void setDurationReserve(MedicalServiceReserve* reserve);
  void requiresCapacity(MedicalServiceReserve* reserve);
  void requiresMedicalFact(MedicalServiceReserve* reserve);  
  void testClientProperties(MedicalServiceReserve* reserve);

  MedicalServiceCalendarBand* getMedicalServiceCalendarBand(IlcInt i) const
  { return _internalTimetable.getMedicalServiceCalendarBand(i); }
  
  void setUpProfiles();
  void printOn(ostream& os) const;
 
	const PropertyConditionTree* getConditionsTree() const { return _conditionsTree; }
  void setPropertyConditionTree(PropertyConditionTree* tree)
  { _conditionsTree= tree;  }

	void addBusy(MedicalServiceSingleBusy* _medicalServiceSingleBusy);
	IlcInt getCurrentMedicalServiceSingleBusy() const { return _currentMedicalServiceSingleBusy; }
	IlcInt getNumMedicalServiceSingleBusy() const { return _numMedicalServiceSingleBusy; }
	MedicalServiceSingleBusy* getBusy(IlcInt Index);
	void setUpBusy();

	void addCalendar(WeekIndexedCalendar* calendar);
  void setUpCalendar();

  void addIntervalState(IlcAnySet states, IlcInt from, IlcInt to);
  IlcInt getNumIntervalStates() const { return _numIntervalStates;}
  IlcInt getCurrentIntervalStates() const { return _currentIntervalStates;}
  IntervalStates* getIntervalStates(IlcInt i);
  void setUpIntervalStates();

	MedicalServiceType getTypeMedicalService() { return SINGLE; }
};


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.h                                            */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase MedicalServiceAlternative */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class MedicalServiceAlternative: public MedicalService
{
  MedicalServiceSingle** _services;
  IlcInt _numServices;
  IlcInt _currentService;

  IlcInt _codMedicalFact;
  IlcInt _codDpto;
  IlcInt _numPhase;

public:
  MedicalServiceAlternative(ClinicApplication* app, const char* name, 
														IlcInt numServices, IlcInt codRecurso,
                            IlcInt codMedicalFact, IlcInt codDpto,
                            IlcInt numPhase);
  ~MedicalServiceAlternative();

  void printOn(ostream&) const {}

  void addMedicalService(MedicalServiceSingle* ms);
  virtual MedicalServiceReserve* createReserve(IlcInt delay, 
											                         Phase* phase,
                                               Duration duration,
																							 IlcInt capacity=1);
  
  MedicalServiceSingle* getMedicalServiceSingle(const char* name) const;
  MedicalServiceSingle* getMedicalServiceSingle(IlcInt index) const;
  IlcInt getNumMedicalServices() const { return _currentService; }

  IlcInt getCodMedicalFact() const { return _codMedicalFact; }
  IlcInt getCodDpto() const { return _codDpto; }
  IlcInt getNumPhase() const { return _numPhase; }

	MedicalServiceType getTypeMedicalService() { return ALTERNATIVE; }

  void setUpOrder();
};


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.h                                            */
/*                                                                */
/* DESCRIPCION:                                                   */
/*           Implementación de la clase MedicalServiceReserve     */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
class MedicalServiceReserve
{
  IntervalActivityCalendar _reserve;
  MedicalService* _service;
  Phase* _phase;
  IlcInt _delay;
	IlcInt _capacity;
  Duration _duration;

public:
  MedicalServiceReserve(IlcInt delay, MedicalService* ms, Phase* phase,
												Duration duration, IlcInt capacity=1);

  IntervalActivityCalendar getReserve() const { return _reserve; }
  Phase* getPhase() const { return _phase; }
  MedicalService* getMedicalService() const { return _service; }


  IlcInt getDelay() const { return _delay; }
  Duration getDuration() const { return _duration; }

	IlcInt getCapacity() const { return _capacity; }

  void printOn(ostream& os) const;
};

#endif // __SERVICE_H_