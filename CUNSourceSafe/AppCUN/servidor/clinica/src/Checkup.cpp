#include <checkup.h>
#include <service.h>
#include <clinic.h>
#include <indexcal.h>

/////////////////////////////////////////////////////////////////////////////////
// MedicalFactIteration
////////////////////////////////////////////////////////////////////////////////
void
MedicalFactIteration::printOn()
{
}

/////////////////////////////////////////////////////////////////////////////////
// MedicalFactGroup
////////////////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Inicializa las Actuaciones Asociadas.        */
/*                                                                */
/* AUTOR:    Salvador Pe�alver (ILOG)                             */
/*                                                                */
/* FECHA:    16-10-1997                                           */
/*                                                                */
/******************************************************************
*/	
MedicalFactGroup::MedicalFactGroup(ClinicForm* form, IlcInt numMedicalFacts):
                  _numMedicalFacts(numMedicalFacts),
									_currentMedicalFacts(0),
									_form(form)
{
	_medicalFacts = new MedicalFact*[_numMedicalFacts];

}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Comprueba si actuaci�n con un c�digo         */
/*                   pertence al grupo.                           */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
IlcInt
MedicalFactGroup::getCodMedicalFact( IlcInt codMedicalFact )
{
  IlcInt numMedicalFacts = getCurrentMedicalFacts();

  for ( int i=0; i < numMedicalFacts; i++ )
  {
    if ( getMedicalFact(i)->getCodMedicalFact() == codMedicalFact )
    {
      return 1;
    }
  }

  return 0;

}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Comprueba si actuaci�n con un c�digo         */
/*                   pertence al grupo.                           */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
IlcInt
MedicalFactGroup::getCodMedicalFactClient( IlcInt codMedicalFact,
                                           IlcInt codClient)
{
  IlcInt numMedicalFacts = getCurrentMedicalFacts();

  for ( int i=0; i < numMedicalFacts; i++ )
  {
    if ( (getMedicalFact(i)->getCodMedicalFact() == codMedicalFact) &&
         (getMedicalFact(i)->getClinicClient()->getCodClient() == codClient)
         )
    {
      return 1;
    }
  }

  return 0;

}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Comprueba si actuaci�n con un c�digo         */
/*                   pertence al grupo.                           */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
MedicalFact*
MedicalFactGroup::getActuacion( IlcInt codMedicalFact )
{
  for ( IlcInt i=0; i < getCurrentMedicalFacts(); i++ )
  {
    if ( getMedicalFact(i)->getCodMedicalFact() == codMedicalFact )
    {
      return(getMedicalFact(i));
    }
  }

  return 0;

}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Destructor.                                  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
MedicalFactGroup::~MedicalFactGroup()
{    
	delete [] _medicalFacts;
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: A�ade una nueva actuaci�n al Grupo.          */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
void 
MedicalFactGroup::addMedicalFact(MedicalFact* medicalFact) 
{
	assert(_currentMedicalFacts < _numMedicalFacts);

	//A�ade una nueva actuaci�n al grupo.
  _medicalFacts[_currentMedicalFacts++]= medicalFact;
}

/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Obtener la existencia de actuaciones         */
/*                    sin fases.                                  */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
IlcInt
MedicalFactGroup::getNumPhasesMedicalFact()
{
  for (IlcInt i=0; i < _currentMedicalFacts; i++)
  {
    if ( getMedicalFact(i)->getCurrentPhase() == 0 )
      return -1;
  }

  return 1;
}


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Clinic.cpp                                           */
/*                                                                */
/* DESCRIP. FUNCION: Obtiene la (i) Actuaci�n del Grupo Asociado. */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
MedicalFact* 
MedicalFactGroup::getMedicalFact(IlcInt i) const 
{ 
	assert( i < _currentMedicalFacts );

	return( _medicalFacts[i] ); 
}




void MedicalFactGroup::printOn(ostream& os) const
{
	for (IlcInt i=0; i < _currentMedicalFacts; i++)
    _medicalFacts[i]->printOn(os);
}


////////////////////////////////////////////////////////////////////
// class MedicalFactAllIterations
////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Inicializa las Actuaciones Asociadas.        */
/*                                                                */
/* AUTOR:    Salvador Pe�alver (ILOG)                             */
/*                                                                */
/* FECHA:    16-10-1997                                           */
/*                                                                */
/******************************************************************
*/	
MedicalFactAllIterations::MedicalFactAllIterations(MedicalFact* medicalFact):
                  _numIterations(50),
									_currentIterations(0),
                  _medicalFact(medicalFact)
{
	_iterations = new MedicalFactIteration*[_numIterations];
}


/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: A�ade una nueva interacci�n.                 */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
void 
MedicalFactAllIterations::addIteration(MedicalFactIteration* iteration) 
{
	assert(_currentIterations < _numIterations);

  _iterations[_currentIterations++] = iteration;
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: A�ade una nueva interacci�n.                 */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
void 
MedicalFactAllIterations::printOn() 
{

  for( IlcInt i=0; i < getCurrentMedicalFactAllIterations(); i++ )
  {
      getIteration(i)->printOn();
  }

}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Checkup.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: A�ade una nueva interacci�n.                 */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
MedicalFactIteration* 
MedicalFactAllIterations::getIteration(IlcInt index) 
{
	assert( index < _currentIterations );

  return(_iterations[index]);
}


////////////////////////////////////////////////////////////////////
// class MedicalFact
////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   CheckUp.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Inicializa la Actuaci�n(Constructor)         */
/*                                                                */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
MedicalFact::MedicalFact(MedicalFactGroup* medicalFactGroup, 
                         const char* name,
						             IlcInt type,
						             IlcInt numPhases, 
												 const IlcInt codClient,
												 const IlcInt dpto,
												 Date* fechaPref):
						 _medicalFactGroup(medicalFactGroup),
						 _numPhases(numPhases),
						 _currentPhase(0),
             _iterations(NULL),
						 _type(type),_codClient(codClient),_dpto(dpto),
						 _fechaPref(fechaPref)
{
  ClinicApplicationScheduler* appScheduler= _medicalFactGroup->
                                            getClinicForm()->
                                            getClinicApplicationScheduler();
  
  _action= appScheduler->createActivity();
  _action.setName(name);
  strcpy(_name,name);

  appScheduler->whenValue(_action, this);

  _phasesReserves = appScheduler->createActivityArray(_numPhases);
  _phases = new Phase*[numPhases];

  _client= medicalFactGroup->getClinicForm()->getClinicApplication()->
                                              findClinicClient(codClient);

  _numReserves = 0;

}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   CheckUp.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Destructor.                                  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
MedicalFact::~MedicalFact()
{
	delete [] _phases;
}



void MedicalFact::printOn(ostream& os) const
{
	for (IlcInt i=0; i < _currentPhase; i++)
    _phases[i]->printOn(os);
}



IlcInt 
MedicalFact::getState()
{
	return( (getType()*1000)+getDpto());
}


/*
***********************************************************************/
/*                   SYSECA BILBAO                                    */
/*                                                                    */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                         */
/*                                                                    */
/* NOMBRE:   CheckUp.cpp                                              */
/*                                                                    */
/* DESCRIP. FUNCION: A�ade una nueva fase a la actuaci�n. Coloca      */
/*                   una restricci�n temporal si es necesario y       */
/*                   recubre las fases si ya est�n todas introducidas */
/*                                                                    */
/* AUTOR:    Roberto Murga                                            */
/*                                                                    */
/* FECHA:    17-9-1997                                                */
/*                                                                    */
/**********************************************************************
*/
void 
MedicalFact::addPhase(Phase* phase, Phase* phaseBefore, IlcInt ind,
											Duration* durMin, Duration* durMax)
{
	IlcInt desfaseMin = 0;
	IlcInt desfaseMax = 0;

	if( durMin != NULL )
		{
			desfaseMin = getMedicalFactGroup()->getClinicForm()->getCalendar()->getTimeUnit(*durMin);
		};

	if( durMax != NULL )
		{
			desfaseMax = getMedicalFactGroup()->getClinicForm()->getCalendar()->getTimeUnit(*durMax);
		};

  if ( phaseBefore != NULL )
  {
		phase->_phase= phaseBefore;
		phase->_ind= ind;

    IntervalActivityCalendar act1 = phaseBefore->getReserve();
    IntervalActivityCalendar act2 = phase->getReserve();

		if( (durMax != NULL) &&  (durMin != NULL) )
		{
			IntervalActivityCalendar actAux = getMedicalFactGroup()->getClinicForm()->getClinicApplicationScheduler()->
																				createActivity();
			actAux.setDurationMin(desfaseMin);
			actAux.setDurationMax(desfaseMax);

			getMedicalFactGroup()->getClinicForm()->getClinicApplicationScheduler()->
					                                    precedence(actAux, act2);

			if(ind)
			{
				getMedicalFactGroup()->getClinicForm()->getClinicApplicationScheduler()->
					                                      precedence(act1, actAux);
			}
			else
			{
				getMedicalFactGroup()->getClinicForm()->getClinicApplicationScheduler()->
																								conditionalPrecedence(act1, actAux,0);
			}
		}
		else
		{
      if(durMin != NULL)
      {
        if(ind)
        {
          getMedicalFactGroup()->getClinicForm()->getClinicApplicationScheduler()->
                                                  startsAfterEnd(act1,act2,desfaseMin);
        }
        else
        {
          getMedicalFactGroup()->getClinicForm()->getClinicApplicationScheduler()->
                                                  precedenceConditional(act1,act2,desfaseMin);
        };
      }
      else
      {
        if(ind)
        {
				  getMedicalFactGroup()->getClinicForm()->getClinicApplicationScheduler()->
                                                  startsAfterEnd(act1,act2,0);
        }
        else
        {
          getMedicalFactGroup()->getClinicForm()->getClinicApplicationScheduler()->
                                                  precedenceConditional(act1,act2,0);
        };
      }
		}
	}
	else
		{
			if( _fechaPref != NULL )
			{
				getMedicalFactGroup()->getClinicForm()->getClinicApplicationScheduler()->
															addCst(phase->getReserve().startsAfter(*_fechaPref));
			};
		}

  if( _currentPhase < _numPhases  )
  {
	  _phasesReserves[_currentPhase] = phase->getReserve();
	  _phases[_currentPhase++]= phase;
  }

  if (_currentPhase == _numPhases)
  {
    getMedicalFactGroup()->getClinicForm()->getClinicApplicationScheduler()->
                                            covers(_action, _phasesReserves);
  }

}



/*
***********************************************************************/
/*                   SYSECA BILBAO                                    */
/*                                                                    */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                         */
/*                                                                    */
/* NOMBRE:   CheckUp.cpp                                              */
/*                                                                    */
/* DESCRIP. FUNCION: Obtiene la fase (i) que compone la Actuaci�n.    */
/*                                                                    */
/* AUTOR:    Roberto Murga                                            */
/*                                                                    */
/* FECHA:    17-9-1997                                                */
/*                                                                    */
/**********************************************************************
*/
Phase* 
MedicalFact::getPhase(IlcInt i) const
{ 
	assert( i < _currentPhase );

	return(_phases[i]); 
}


MedicalServiceReserve*
MedicalFact::getMedicalFactReserveRecPref()
{
	IlcInt PhaseRecPref = -1;
	Phase* PhasePref = NULL;
	IlcInt numReserves;
	MedicalServiceReserve* reserve;

	for(IlcInt i=0; i < _currentPhase; i++)
		{
			if(getPhase(i)->getMedicalServicePref() != 255 )
			{
				PhaseRecPref = i;
				break;
			}
		}
	
	if( PhaseRecPref == -1 )
	{
		for(i=0; i < _currentPhase; i++)
		{
			numReserves = getPhase(i)->getCurrentService();

			for (IlcInt j=0; j < numReserves; j++)
			{
				reserve = getPhase(i)->getMedicalServiceReserve(j);
				i = _currentPhase;
				break;
			}
		}
		return (reserve);
	}
	else
	{
		PhasePref = getPhase(PhaseRecPref);

		return(PhasePref->getMedicalServiceReserve(
							PhasePref->getMedicalServicePref()));

	}
}



IlcInt
MedicalFact::getMedicalFactRecPref()
{
	IlcInt PhaseRecPref = -1;
	Phase* PhasePref = NULL;
	MedicalServiceReserve* MedicalService = NULL;

	for(IlcInt i=0; i < _currentPhase; i++)
		{
			if(getPhase(i)->getMedicalServicePref() != 255 )
			{
				PhaseRecPref = i;
				break;
			}
		}

	if( PhaseRecPref == -1 )
		{
			return(PhaseRecPref);
		}
	else
		{
			PhasePref = getPhase(PhaseRecPref);

			MedicalService = PhasePref->getMedicalServiceReserve(PhasePref->getMedicalServicePref());

			if(MedicalService->getMedicalService()->getTypeMedicalService() == SINGLE )
			{
				return(MedicalService->getMedicalService()->getCodRecurso());
			}
			else
			{
				return(getMedicalFactGroup()->getClinicForm()->getClinicApplication()->
							 getMedicalService(MedicalService->getReserve().getName())->
							 getCodRecurso()
							 );
			}
		}
}


////////////////////////////////////////////////////////////////////
// class Phase
////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   CheckUp.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Inicializa la fase (Constructor)             */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
Phase::Phase(MedicalFact* mf, const char* name, Duration duration, 
             IlcInt numServices,IlcInt codFase):_codFase(codFase),
       _reserve(mf->getMedicalFactGroup()->getClinicForm()->
                    getClinicApplicationScheduler()->
                    getSchedule(), duration),
		   _numServices(numServices), 
			 _currentService(0), 
			 _ind(0),
			 _phase(NULL),
       _duration(duration),
			 _medicalFact(mf)
{
  _reserve.setName(name);
  strcpy(_name,name);

  mf->getMedicalFactGroup()->getClinicForm()->getClinicApplicationScheduler()->
      requires(_reserve, mf->getClinicClient()->getCapacity());

  _services= new MedicalServiceReserve*[numServices];

	_numMedicalServicePref = 255;

  if(_services == NULL )
  {
	exit(EXIT_FAILURE);
  }
  
}




/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   CheckUp.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Destructor.                                  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
Phase::~Phase()
{
	delete [] _services;
}


			 
/*
***********************************************************************/
/*                   SYSECA BILBAO                                    */
/*                                                                    */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                         */
/*                                                                    */
/* NOMBRE:   CheckUp.cpp                                              */
/*                                                                    */
/* DESCRIP. FUNCION:Crear una reserva para un recurso y almacena el   */
/*			        recurso en el array de recursos necesarios para   */
/*                  la fase.                                          */
/*                                                                    */
/* AUTOR:    Roberto Murga                                            */
/*                                                                    */
/* FECHA:    17-9-1997                                                */
/*                                                                    */
/**********************************************************************
*/
void 
Phase::addMedicalServiceReserve(MedicalService* ms, Duration delay, 
                                Duration duration, IlcInt capacity)
{
	IlcInt desfase = 0;

	assert(_currentService < _numServices);

	desfase = ms->getCalendar()->getTimeUnit(delay);

  if ( delay.getIndicador() == -1 )
  {
    desfase = -desfase;
  }

   _services[_currentService++]= ms->createReserve(desfase, this, duration, capacity);

}



/*
***********************************************************************/
/*                   SYSECA BILBAO                                    */
/*                                                                    */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                         */
/*                                                                    */
/* NOMBRE:   CheckUp.cpp                                              */
/*                                                                    */
/* DESCRIP. FUNCION: Obtiene la Reserva (i) de un recurso en la fase  */
/*                   en curso.                                        */
/*                                                                    */
/* AUTOR:    Roberto Murga                                            */
/*                                                                    */
/* FECHA:    17-9-1997                                                */
/*                                                                    */
/**********************************************************************
*/
MedicalServiceReserve* Phase::getMedicalServiceReserve(IlcInt i) const 
{ 
	assert( i < _currentService );

	return(_services[i]); 
}



void Phase::printOn(ostream& os) const
{
  os << _reserve << endl << "Reserves: " << endl;
  for (IlcInt i=0; i < _currentService; i++)
    _services[i]->printOn(os);
}



