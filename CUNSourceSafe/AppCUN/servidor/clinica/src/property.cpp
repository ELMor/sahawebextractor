#include <property.h>
#include <clinic.h>

////////////////////////////////////////////////////////////////////////
// class Property
////////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Inicializa una Caracter�stica General con    */
/*                   un Cjto de estados v�lidos.                  */
/*                                                                */
/* AUTOR:   Salvador Pe�alver (ILOG)                              */
/*                                                                */
/* FECHA:    21-10-1997                                           */
/*                                                                */
/******************************************************************
*/	
Property::Property(ClinicApplication* app, const IlcInt codigo, const char* name, 
                   IlcInt numPossibleValues,DataTypeProperty dataType):_codigo(codigo),
																						                           _app(app)
{
  _dataType = dataType;

  strcpy(_name,name);

  if(_dataType == NUMERICO)
  {
    _possibleValues = _app->getClinicForm()->getClinicApplicationScheduler()->
                            createIntVar(0, numPossibleValues); 
  }
}


void Property::setValues(IlcInt valueFrom, IlcInt valueTo, 
                         IlcBool inclusion)
{
  if (inclusion)
  {
    _possibleValues.setRange(valueFrom, valueTo);
  }
  else
  {
    _possibleValues.removeRange(valueFrom, valueTo);
  }

}


Property::Property(const Property& p)
{
  _app= p._app;
  strcpy(_name, p._name);
	_codigo= p._codigo;
  _dataType = p._dataType;

  if(_dataType == NUMERICO)
  {
    _possibleValues= _app->getClinicForm()->getClinicApplicationScheduler()->
                           createIntVar(0, p._possibleValues.getSize());
  }
}


////////////////////////////////////////////////////////////////////////
// class PropertySet
////////////////////////////////////////////////////////////////////////
/*
********************************************************************/
/*                   SYSECA BILBAO                                 */
/*                                                                 */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                      */
/*                                                                 */
/* NOMBRE:   Service.cpp                                           */
/*                                                                 */
/* DESCRIP. FUNCION: Inicializa una Rama con (numProperties)-Nodos */
/*                                                                 */
/* AUTOR:    Roberto Murga                                         */
/*                                                                 */
/* FECHA:    17-9-1997                                             */
/*                                                                 */
/*******************************************************************
*/	
PropertySet::PropertySet(IlcInt numProperties):
						 _numProperties(numProperties),
						 _currentProperty(0)
{
	_properties = new Property*[_numProperties];
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Destrcutor.                                  */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
PropertySet::~PropertySet() 
{ 
	delete [] _properties;
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: A�ade una nueva Caracter�stica al Cjto de    */  
/*                   Caracter�sticas Generales.                   */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
void
PropertySet::addProperty(Property* property)
{
	assert(_currentProperty < _numProperties);

	_properties[_currentProperty++] = property;
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Accede a la Caracter�stica General (name).   */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	
Property*
PropertySet::getProperty( IlcInt codigo ) const
{
  Property* ms= 0;

  for (IlcInt i=0; i < _currentProperty; i++)
  {
    if (codigo == _properties[i]->getCodigo())
    {
      ms= _properties[i];
      break;
    }
  }
  return ms;
}



////////////////////////////////////////////////////////////////////////
// class PropertyCondition
////////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: A�ade un nodo(Restricci�n) a la Rama.        */  
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/	 
PropertyCondition::PropertyCondition(Property* property,
                                     IlcInt valueFrom, IlcInt valueTo, 
                                     IncluType inclusion):
                   _valueFrom(valueFrom), _valueTo(valueTo),
					         _inclusion(inclusion)
{
  _property= new Property(*property);

  _property->setValues(_valueFrom, _valueTo, _inclusion);

  strcpy(_value,"");
}



PropertyCondition::PropertyCondition(Property* property,
                                     IncluType inclusion,
                                     char* value):_inclusion(inclusion),
                                     _valueFrom(-1), _valueTo(-1)
{
  _property= new Property(*property);

  strcpy(_value,value);
}



void
PropertyCondition::printon()
{
	printf("%d - %d",getValueFrom(),getValueTo());
}



IlcBool PropertyCondition::testClientProperties(ClinicClient* client) const
{
  ClientProperty* clientProperty= client->getClientProperty(_property->
                                                            getCodigo());
	if (clientProperty)
  {

    if(clientProperty->getDataTypeProperty() == NUMERICO)
    {
      if (_property->isIn(clientProperty->getValue()))
			{

        return IlcTrue;
			}
      else
			{

        return IlcFalse;
			}
    }
    else
    {
      if(getInclusion() == INCLUSION )
      {
        if(strcmp(getValue(),clientProperty->getValueCadena()) == 0)
          return IlcTrue;
        else 
          return IlcFalse;
      }
      else
      {
        if(strcmp(getValue(),clientProperty->getValueCadena()))
          return IlcTrue;
        else 
          return IlcFalse;
      }
    }
  }

  return IlcTrue;
}



////////////////////////////////////////////////////////////////////////
// class PropertyConditionSet
////////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Crea una Constraint que engloba todos las    */  
/*                   Constraints de los respectivos nodos de la   */
/*                   Rama.                                        */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
PropertyConditionSet::PropertyConditionSet(ClinicApplication* clinicApplication,
                                           IlcInt maxConditions):
                      _clinicApplication(clinicApplication),
                      _numConditions(maxConditions),
                      _currentCondition(0)
{
  _conditions= new PropertyCondition*[_numConditions];
}



PropertyConditionSet::~PropertyConditionSet()
{
  if (_conditions)
    delete [] _conditions;
}



IlcBool PropertyConditionSet::testClientProperties(ClinicClient* client) const
{
  IlcBool isOk= IlcTrue;
  IlcInt index= 0;

  while (index < _currentCondition && isOk)
  {
    if (!_conditions[index]->testClientProperties(client))
    {
      isOk= IlcFalse;
      break;
    }
    else 
      index++;
  }
  return isOk; 
}



void PropertyConditionSet::addPropertyCondition(PropertyCondition* pc)
{
  assert(_currentCondition < _numConditions);

  _conditions[_currentCondition++]= pc;
}


PropertyCondition*
PropertyConditionSet::getPropertyCondition(IlcInt Index) const
{
  assert(Index < _currentCondition);

  return( _conditions[Index]);
}

/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Imprime la rama de restricciones.           */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
void 
PropertyConditionSet::printon()
{
	for( IlcInt i=0; i < _currentCondition; i++)
		{
			printf("{ ");
			getPropertyCondition(i)->printon();
			printf("} ");
		}

}

////////////////////////////////////////////////////////////////////////
// class PropertyConditionTree
////////////////////////////////////////////////////////////////////////
/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Construye un Servicio M�dico Simple.         */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
void PropertyConditionTree::addPropertyConditionSet(PropertyConditionSet* pcs)
{
  assert(_currentBranch < _numBranchs);

  _tree[_currentBranch++]= pcs;
}



/*
*******************************************************************/
/*                   SYSECA BILBAO                                */
/*                                                                */
/* PROYECTO: CLINICA UNIVERSITARIA DE NAVARRA                     */
/*                                                                */
/* NOMBRE:   Service.cpp                                          */
/*                                                                */
/* DESCRIP. FUNCION: Imprime el �rbol de restricciones.           */
/*                                                                */
/* AUTOR:    Roberto Murga                                        */
/*                                                                */
/* FECHA:    17-9-1997                                            */
/*                                                                */
/******************************************************************
*/
PropertyConditionSet* 
PropertyConditionTree::getPropertyConditionSet(IlcInt Index)
{
	assert(Index < _currentBranch);

  return( _tree[Index] );
}


void 
PropertyConditionTree::printon()
{
	for( IlcInt i=0; i < _currentBranch; i++)
		{
			printf("[ ");
			getPropertyConditionSet(i)->printon();
			printf("] ");
		}

}

IlcBool PropertyConditionTree::testClientProperties(ClinicClient* client) const
{
  IlcBool isOk= IlcFalse;
  IlcInt index= 0;

	if( _currentBranch == 0)
	{
		return(IlcTrue);
	}

  while (index < _currentBranch && !isOk)
  {
    if (_tree[index]->testClientProperties(client))
    {
      isOk= IlcTrue;
      break;
    }
    else    
      index++;
  }

  return isOk;
}



PropertyConditionTree::~PropertyConditionTree()
{

  delete [] _tree;
}



