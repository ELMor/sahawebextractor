#ifndef __tipos_utiles
#define __tipos_utiles

typedef long IlcInt;				
typedef unsigned long IlcUInt;
typedef void* IlcAny;
typedef double IlcFloat; 
typedef IlcInt IlcBool;
#define IlcTrue ((IlcBool)  1)
#define IlcFalse ((IlcBool) 0)

#endif // __tipos_utiles
