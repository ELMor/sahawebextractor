#ifndef __INDEXEDCALENDAR_H_
#define __INDEXEDCALENDAR_H_

#include <calendar.h>

/////////////////////////////////////////////////////////////////////////////////////
// CLASS IndexedDay
/////////////////////////////////////////////////////////////////////////////////////
class IndexedDay: public Day
{
public:
	IndexedDay(IlcUInt number, const char* name, IlcInt numberInWeek,
             const Month* month);
	virtual void printOn(ostream& os) const;
};


/////////////////////////////////////////////////////////////////////////////////////
// CLASS IndexedMonth
/////////////////////////////////////////////////////////////////////////////////////
class IndexedMonth: public Month
{
	IlcUInt _indexMonth;

	virtual void createDays();
public:
	IndexedMonth(IlcUInt number, IlcUInt year, IlcUInt& indexMonth, IlcUInt& indexDay);
	IlcUInt getIndex() const { return _indexMonth; }
};


/////////////////////////////////////////////////////////////////////////////////////
// CLASS IndexedCalendar
/////////////////////////////////////////////////////////////////////////////////////
class IndexedCalendar: public Calendar
{
protected:
	IlcUInt _firstIndexedDay;
	IlcUInt _lastIndexedDay;
	IlcUInt _timeMin;
	IlcUInt _timeMax;
	TimeStep _timeStep;
  const IlcUInt _timeStepPack;
  
	IlcUInt _numDays;
  IlcUInt _indexDay;
	const Day** _days;
  IlcUInt _indexMonth;
  
	void initializeIndexedDays();
  IlcUInt getFirstIndexedDay() const {return _firstIndexedDay;}
  IlcUInt getGlobalIndexedDay(Date date) const;
	
	virtual void createMonths();
public:
  IndexedCalendar(Period period, TimeStep timeStep= MINUTES,
                  IlcUInt tiemStepPack= 1);
 	
	IlcUInt getTimeMin() const { return _timeMin; }
	IlcUInt getTimeMax() const { return _timeMax; }

  IlcUInt getLastIndexedDay() const { return _lastIndexedDay; }

	Date getDate(IlcUInt timeUnit) const;
	IlcUInt getTimeUnit(Date date) const;
  IlcUInt getTimeUnit(Duration duration) const;

	void printOn(ostream& os) const;  
	
  const TimeStep getTimeStep() const { return _timeStep; }
  const IlcUInt getTimeStepPack() const { return _timeStepPack; }

	IlcInt getNumDays() const { return _numDays; }
	const Day* getDayFromIndex(IlcUInt indexDay) const; // to iterate the calendar...
  IlcUInt getIndexedDay(Date d) const; // return the index of this date
  
  Duration getDuration(Period period) const;
  
  void setHoliday(Date date, IlcBool yn= IlcTrue);
};

#endif // __INDEXEDCALENDAR_H_
