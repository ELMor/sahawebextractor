VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form FrmDispEstupef 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. CONTROLAR ESTUPEFACIENTES. Dispensar Estupefacientes."
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0107.frx":0000
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdDispensar 
      Caption         =   "Dispensar"
      Height          =   375
      Left            =   4800
      TabIndex        =   4
      Top             =   7440
      Width           =   1935
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Estupefacientes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6615
      Index           =   2
      Left            =   240
      TabIndex        =   0
      Tag             =   "Actuaciones Asociadas"
      Top             =   600
      Width           =   10935
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6135
         Index           =   1
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   10650
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         stylesets.count =   1
         stylesets(0).Name=   "Bloqueada"
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "FR0107.frx":000C
         SelectTypeRow   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18785
         _ExtentY        =   10821
         _StockProps     =   79
         Caption         =   "ESTUPEFACIENTES"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmDispEstupef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDispEstupef (FR0107.FRM)                                  *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: 5 DE NOVIEMBRE DE 1998                                        *
'* DESCRIPCION: Dispensar Estupefacientes                               *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cmddispensar_Click()
Dim rstdes As rdoResultset
Dim strdes As String
Dim rstori As rdoResultset
Dim strori As String
'Dim strinsert As String
Dim strInsert As String
'Dim strinsertsalida As String
Dim intMsgBox As String
Dim str13 As String
Dim rst13 As rdoResultset
Dim str80 As String
Dim rst80 As rdoResultset
Dim intIndOri As Integer
'Dim i As Integer
Dim intDevol As Integer
Dim strCodAlmacen As String
Dim strCodAlmacenDes As String

    cmdDispensar.Enabled = False
    If grdDBGrid1(1).Rows = 0 Then
        Call MsgBox("No hay ning�n estupefaciente a dispensar", vbInformation)
    Else
        If grdDBGrid1(1).Columns(3).Value = True Then
            Call MsgBox("Ya se ha dispensado este estupefaciente", vbInformation)
            cmdDispensar.Enabled = True
            Exit Sub
        End If
        intMsgBox = MsgBox("�Est� seguro de la dispensaci�n de este estupefaciente?", vbYesNo + vbQuestion)
        If intMsgBox = vbNo Then
            cmdDispensar.Enabled = True
            Exit Sub
        End If
        'strinsert = "UPDATE FR6600 SET FR26CODESTPETIC=5 WHERE FR66CODPETICION=" & txtText1(0).Text
        'objApp.rdoConnect.Execute strinsert, 64
        'objApp.rdoConnect.Execute "Commit", 64
        
        'se obtiene el almac�n de destino
        strdes = "SELECT FR04CODALMACEN FROM AD0200 WHERE AD02CODDPTO=" & grdDBGrid1(1).Columns(13).Value
        Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
        If IsNull(rstdes(0).Value) Then
            strCodAlmacenDes = "null"
        Else
            strCodAlmacenDes = rstdes(0).Value
        End If
        
        'se obtiene el almac�n del servicio 1(Farmacia) que es origen
        strori = "SELECT FR04CODALMACEN FROM AD0200,FRH200 " & _
                " WHERE AD02CODDPTO=FRH2PARAMGEN " & _
                " AND FRH2CODPARAMGEN=3 "
        Set rstori = objApp.rdoConnect.OpenResultset(strori)
        If IsNull(rstori(0).Value) Then
            strCodAlmacen = "null"
        Else
            strCodAlmacen = rstori(0).Value
        End If
        
        'grdDBGrid1(0).MoveFirst
        'For i = 0 To grdDBGrid1(0).Rows - 1
        str13 = "SELECT FR13CODCTRLESTUPEFA_SEQUENCE.nextval FROM dual"
        Set rst13 = objApp.rdoConnect.OpenResultset(str13)
        
        str80 = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
        Set rst80 = objApp.rdoConnect.OpenResultset(str80)
            
        Select Case grdDBGrid1(1).Columns(21).Value
            Case "Peticion Farmacia":
                intIndOri = 0
            Case "Hoja Quirofano":
                intIndOri = 1
            Case "Hoja Anestesia":
                intIndOri = 2
        End Select
        If grdDBGrid1(1).Columns(4).Value = True Then
            intDevol = -1
        Else
            intDevol = 0
        End If
            'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
            'Farmacia y el destino es el almac�n al que se dispensa
        strInsert = "INSERT INTO FR1300 (FR13CODCTRLESTUPEFA,FR13CODPET," & _
                      "FR13CODLINEA,FR13INDORIG,FR73CODPRODUCTO,FR90CODTIPMOV," & _
                      "FR04CODALMACEN_DES,SG02COD_PDS,FR13FECMOVIMIENTO,SG02COD_MPR," & _
                      "FR13FECDEVENVASE,CI21CODPERSONA,FR04CODALMACEN_ORI,FR13INDDEVOLPENDI," & _
                      "FR13CANTSUM) VALUES (" & _
                      rst13(0).Value & "," & _
                      grdDBGrid1(1).Columns(19).Value & "," & _
                      grdDBGrid1(1).Columns(20).Value & "," & _
                      intIndOri & "," & _
                      grdDBGrid1(1).Columns(5).Value & "," & _
                      6 & "," & _
                      strCodAlmacenDes & ",'" & _
                      objsecurity.strUser & "'," & _
                      "SYSDATE" & ",'" & _
                      grdDBGrid1(1).Columns(15).Value & "'," & _
                      "Null" & "," & _
                      grdDBGrid1(1).Columns(9).Value & "," & _
                      strCodAlmacen & "," & _
                      intDevol & "," & _
                      grdDBGrid1(1).Columns(7).Value & ")"
        objApp.rdoConnect.Execute strInsert, 64
        'strinsert = Right(strinsert, 100)
        objApp.rdoConnect.Execute "Commit", 64
        
        'se actualiza el inventario de salida(FR8000) en el que el origen de salida es farmacia
        'y el destino de la salida el almac�n al que se dispensa
        strInsert = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                     "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
                     "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES(" & _
                      rst80.rdoColumns(0).Value & "," & _
                      rstori.rdoColumns(0).Value & "," & _
                      rstdes.rdoColumns(0).Value & "," & _
                      9 & "," & _
                      "SYSDATE" & "," & _
                      "Null" & "," & _
                      grdDBGrid1(1).Columns(5).Value & "," & _
                      grdDBGrid1(1).Columns(7).Value & "," & _
                      0 & ","
        If grdDBGrid1(1).Columns(8).Value <> "" Then
            strInsert = strInsert & grdDBGrid1(1).Columns(8).Value & ")"
        Else
            strInsert = strInsert & "1)"
        End If
        
        objApp.rdoConnect.Execute strInsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        grdDBGrid1(1).Columns(3).Value = True
        grdDBGrid1(1).Update
        rst13.Close
        Set rst13 = Nothing
        rst80.Close
        Set rst80 = Nothing
         
        'grdDBGrid1(0).MoveNext
        'Next i
        rstdes.Close
        Set rstdes = Nothing
        rstori.Close
        Set rstori = Nothing
        'Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
        'objWinInfo.DataRefresh
    End If
    cmdDispensar.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Estupefacientes"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR2801J"
        .intAllowance = cwAllowModify
        .strWhere = "(BLOQUEADA = 0 OR BLOQUEADA IS NULL)"
        
        Call .FormAddOrderField("PRODUCTO", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Estupefacientes")
        'Call .FormAddFilterWhere(strKey, "BLOQUEADA", "Bloqueada", cwBoolean)
        Call .FormAddFilterWhere(strKey, "PRODUCTO", "C�digo Producto", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n Producto", cwString)
        Call .FormAddFilterWhere(strKey, "CANTIDAD", "Cantidad", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PACIENTE", "C�digo Paciente", cwNumeric)
        Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
        Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
        Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
        Call .FormAddFilterWhere(strKey, "SERVICIO", "C�digo Servicio", cwNumeric)
        Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Descripci�n", cwString)
        Call .FormAddFilterWhere(strKey, "MEDICO", "C�digo M�dico", cwString)
        Call .FormAddFilterWhere(strKey, "SG02APE1", "Doctor", cwString)
        Call .FormAddFilterWhere(strKey, "FECHA", "Fecha", cwDate)
        Call .FormAddFilterWhere(strKey, "HORA", "Hora", cwNumeric)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        'Call .FormAddFilterOrder(strKey, "BLOQUEADA", "Bloqueada")
        Call .FormAddFilterOrder(strKey, "PRODUCTO", "C�digo Producto")
        Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTO", "Descripci�n Producto")
        Call .FormAddFilterOrder(strKey, "CANTIDAD", "Cantidad")
        Call .FormAddFilterOrder(strKey, "PACIENTE", "C�digo Paciente")
        Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
        Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
        Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
        Call .FormAddFilterOrder(strKey, "SERVICIO", "C�digo Servicio")
        Call .FormAddFilterOrder(strKey, "AD02DESDPTO", "Descripci�n")
        Call .FormAddFilterOrder(strKey, "MEDICO", "C�digo M�dico")
        Call .FormAddFilterOrder(strKey, "SG02APE1", "Doctor")
        Call .FormAddFilterOrder(strKey, "FECHA", "Fecha")
        Call .FormAddFilterOrder(strKey, "HORA", "Hora")
    
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "Dispensada", "", cwBoolean)
        Call .GridAddColumn(objMultiInfo, "Dev. Pendiente", "", cwBoolean)
        Call .GridAddColumn(objMultiInfo, "C�digo Producto", "PRODUCTO", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "Descripci�n Producto", "FR73DESPRODUCTO", cwString)
        Call .GridAddColumn(objMultiInfo, "Cantidad", "CANTIDAD", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "Unidad Medida", "UNIMEDIDA", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "C�digo Paciente", "PACIENTE", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "Nombre", "CI22NOMBRE", cwString)
        Call .GridAddColumn(objMultiInfo, "Primer Apellido", "CI22PRIAPEL", cwString)
        Call .GridAddColumn(objMultiInfo, "Segundo Apellido", "CI22SEGAPEL", cwString)
        Call .GridAddColumn(objMultiInfo, "C�digo Servicio", "SERVICIO", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "Descripci�n", "AD02DESDPTO", cwString)
        Call .GridAddColumn(objMultiInfo, "C�digo M�dico", "MEDICO", cwString)
        Call .GridAddColumn(objMultiInfo, "Doctor", "SG02APE1", cwString)
        Call .GridAddColumn(objMultiInfo, "Fecha", "FECHA", cwDate)
        Call .GridAddColumn(objMultiInfo, "Hora", "HORA", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "Codigo", "CODIGO", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "L�nea", "LINEA", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "Tipo", "TABLA", cwString)
        
        Call .FormCreateInfo(objMultiInfo)
    
        'Se indica que campos son obligatorios y cuales son clave primaria
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(12)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(13)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(14)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(15)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(16)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(18)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(18)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(19)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(20)).blnInFind = True
        
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(12)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(13)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(14)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(15)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(16)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(18)).blnReadOnly = True
        Call .WinRegister
        Call .WinStabilize
    End With
    grdDBGrid1(1).Columns(3).Width = 1000
    grdDBGrid1(1).Columns(4).Width = 1400
    grdDBGrid1(1).Columns(5).Width = 1400
    grdDBGrid1(1).Columns(6).Width = 3000
    grdDBGrid1(1).Columns(7).Width = 800
    grdDBGrid1(1).Columns(8).Width = 800
    grdDBGrid1(1).Columns(9).Width = 650
    grdDBGrid1(1).Columns(10).Width = 1200
    grdDBGrid1(1).Columns(11).Width = 1200
    grdDBGrid1(1).Columns(12).Width = 1400
    grdDBGrid1(1).Columns(13).Width = 1400
    grdDBGrid1(1).Columns(14).Width = 2400
    grdDBGrid1(1).Columns(15).Width = 1200
    grdDBGrid1(1).Columns(16).Width = 2400
    grdDBGrid1(1).Columns(17).Width = 1200
    grdDBGrid1(1).Columns(18).Width = 800
    
    grdDBGrid1(1).Columns(0).Visible = False
    grdDBGrid1(1).Columns(19).Visible = False
    grdDBGrid1(1).Columns(20).Visible = False
    
    'SSDBGrid1(1).SelBookmarks.Add (SSDBGrid1(1).Bookmark)
'Dim i As Integer
'grdDBGrid1(1).MoveFirst ' Position at the first row
'For i = 0 To 2
'    If i = 1 Then
'    grdDBGrid1(1).Columns(4).StyleSet = "Grande"
'    End If
'    grdDBGrid1(1).MoveNext
'Next i

 'grdDBGrid1(1).MoveFirst
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub





Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    Dim strDisp As String
    Dim rstDisp As rdoResultset
    Dim intIndOri As Integer
    Dim i As Integer
    
    
    'grdDBGrid1(1).MoveFirst
    'For i = 1 To grdDBGrid1(1).Rows
        Select Case grdDBGrid1(1).Columns(21).Value
            Case "Peticion Farmacia":
                intIndOri = 0
            Case "Hoja Quirofano":
                intIndOri = 1
            Case "Hoja Anestesia":
                intIndOri = 2
        End Select
        strDisp = "SELECT * FROM FR1300 WHERE FR13CODPET=" & grdDBGrid1(1).Columns(19).Value & _
                  " AND FR13CODLINEA=" & grdDBGrid1(1).Columns(20).Value & _
                  " AND FR13INDORIG=" & intIndOri
        Set rstDisp = objApp.rdoConnect.OpenResultset(strDisp)
        If rstDisp.EOF = True Then
            grdDBGrid1(1).Columns(3).Value = False
        Else
            grdDBGrid1(1).Columns(3).Value = True
            If rstDisp("FR13INDDEVOLPENDI").Value = -1 Then
                grdDBGrid1(1).Columns(4).Value = True
            Else
                grdDBGrid1(1).Columns(4).Value = False
            End If
        End If
        'grdDBGrid1(1).MoveNext
    'Next i
    'grdDBGrid1(1).MoveFirst
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Estupefacientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub
Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


