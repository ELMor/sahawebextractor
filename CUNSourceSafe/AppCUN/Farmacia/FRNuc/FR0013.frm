VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmMantTipoABCProd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MANTENIMIENTO FARMACIA. Mantenimiento ABC Producto."
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0013.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraframe1 
      Caption         =   "ABC Producto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6975
      Index           =   2
      Left            =   360
      TabIndex        =   0
      Tag             =   "Actuaciones Asociadas"
      Top             =   720
      Width           =   10935
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6375
         Index           =   1
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   10650
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   18785
         _ExtentY        =   11245
         _StockProps     =   79
         Caption         =   "ABC PRODUCTO"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMantTipoABCProd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmMantTipoABCProd (FR0013.FRM)                              *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: 20 DE AGOSTO DE 1998                                          *
'* DESCRIPCION: mantenimiento Tipo ABC Producto                         *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Tipo ABC Producto"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "FR8600"
        
        Call .FormAddOrderField("FR86CODABCPRODUCT", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Tipo ABC Producto")
        Call .FormAddFilterWhere(strKey, "FR86CODABCPRODUCT", "C�digo ABC Producto", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR86PORCENTAJEUNI", "Porcentaje en Unidades", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR86PORCENTAJEDIN", "Porcentaje en Dinero", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR86FRECUENREVI", "Frecuencia de Revisi�n", cwNumeric)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR86CODABCPRODUCT", "C�digo ABC Producto")
        Call .FormAddFilterOrder(strKey, "FR86PORCENTAJEUNI", "Porcentaje en Unidades")
        Call .FormAddFilterOrder(strKey, "FR86PORCENTAJEDIN", "Porcentaje en Dinero")
        Call .FormAddFilterOrder(strKey, "FR86FRECUENREVI", "Frecuencia de Revisi�n")
    
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "C�digo ABC Producto", "FR86CODABCPRODUCT", cwNumeric, 2)
        Call .GridAddColumn(objMultiInfo, "Porcentaje en Unidades", "FR86PORCENTAJEUNI", cwDecimal, 6)
        Call .GridAddColumn(objMultiInfo, "Porcentaje en Dinero", "FR86PORCENTAJEDIN", cwDecimal, 6)
        Call .GridAddColumn(objMultiInfo, "Frecuencia de Revisi�n", "FR86FRECUENREVI", cwNumeric, 2)
  
        Call .FormCreateInfo(objMultiInfo)
    
        'Se indica que campos son obligatorios y cuales son clave primaria
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
   
        Call .WinRegister
        Call .WinStabilize
    End With
    grdDBGrid1(1).Columns(3).Width = 2000
    grdDBGrid1(1).Columns(4).Width = 2200
    grdDBGrid1(1).Columns(5).Width = 2200
    grdDBGrid1(1).Columns(6).Width = 2200
    
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    
    If intKeyCode = 27 Then 'SALIR
      If Comprobar_ABC = -1 Then
        intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
        Exit Sub
      Else
        Exit Sub
      End If
    Else
      intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
    End If

End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)

  If btnButton.Index = 30 Then 'SALIR
    If Comprobar_ABC = -1 Then
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      Exit Sub
    Else
      Exit Sub
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
  Case 80
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
  Case 100
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


Private Function Comprobar_ABC() As Integer
Dim i As Integer
Dim SUMA
Dim intregnoeliminados As Integer
Dim A1, B1, C1 As String
Dim A2, A3, A4, B2, B3, B4, C2, C3, C4 As Single
Dim FILA As Integer
Dim cont As Integer
Dim filaactual As Integer

If grdDBGrid1(1).Rows < 3 Then
  Call MsgBox("Ha de insertar obligatoriamente 3 registros." & Chr(13) _
  & "Ej.: A 80 70 60 " & Chr(13) _
  & "     B 15 20 25 " & Chr(13) _
  & "     C  5 10 15 " _
  , vbInformation, "Aviso")
  Comprobar_ABC = 0
  Exit Function
End If

'se mira que el n� de registros no eliminados sea 3
grdDBGrid1(1).Redraw = False
grdDBGrid1(1).MoveFirst
FILA = 0
filaactual = 0
For i = 0 To grdDBGrid1(1).Rows - 1
    If grdDBGrid1(1).Columns(0).Value <> "Eliminado" Then
        intregnoeliminados = intregnoeliminados + 1
        Select Case FILA
        Case 0:
          If grdDBGrid1(1).Columns(3).Value = "" Then
            Call MsgBox("El campo C�digo ABC Producto es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 3
            Exit Function
          End If
          A1 = grdDBGrid1(1).Columns(3).Value
          If grdDBGrid1(1).Columns(4).Value = "" Then
            Call MsgBox("El campo Porcentaje en Unidades es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 4
            Exit Function
          End If
          A2 = CDec(grdDBGrid1(1).Columns(4).Value)
          If grdDBGrid1(1).Columns(5).Value = "" Then
            Call MsgBox("El campo Porcentaje en Dinero es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 5
            Exit Function
          End If
          A3 = CDec(grdDBGrid1(1).Columns(5).Value)
          If grdDBGrid1(1).Columns(6).Value = "" Then
            Call MsgBox("El campo Frecuencia de Revisi�n es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 6
            Exit Function
          End If
          A4 = CDec(grdDBGrid1(1).Columns(6).Value)
        Case 1:
          If grdDBGrid1(1).Columns(3).Value = "" Then
            Call MsgBox("El campo C�digo ABC Producto es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 3
            Exit Function
          End If
          B1 = grdDBGrid1(1).Columns(3).Value
          If grdDBGrid1(1).Columns(4).Value = "" Then
            Call MsgBox("El campo Porcentaje en Unidades es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 4
            Exit Function
          End If
          B2 = CDec(grdDBGrid1(1).Columns(4).Value)
          If grdDBGrid1(1).Columns(5).Value = "" Then
            Call MsgBox("El campo Porcentaje en Dinero es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 5
            Exit Function
          End If
          B3 = CDec(grdDBGrid1(1).Columns(5).Value)
          If grdDBGrid1(1).Columns(6).Value = "" Then
            Call MsgBox("El campo Frecuencia de Revisi�n es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 6
            Exit Function
          End If
          B4 = CDec(grdDBGrid1(1).Columns(6).Value)
        Case 2:
          If grdDBGrid1(1).Columns(3).Value = "" Then
            Call MsgBox("El campo C�digo ABC Producto es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 3
            Exit Function
          End If
          C1 = grdDBGrid1(1).Columns(3).Value
          If grdDBGrid1(1).Columns(4).Value = "" Then
            Call MsgBox("El campo Porcentaje en Unidades es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 4
            Exit Function
          End If
          C2 = CDec(grdDBGrid1(1).Columns(4).Value)
          If grdDBGrid1(1).Columns(5).Value = "" Then
            Call MsgBox("El campo Porcentaje en Dinero es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 5
            Exit Function
          End If
          C3 = CDec(grdDBGrid1(1).Columns(5).Value)
          If grdDBGrid1(1).Columns(6).Value = "" Then
            Call MsgBox("El campo Frecuencia de Revisi�n es obligatorio", vbInformation, "Aviso")
            Comprobar_ABC = 0
            grdDBGrid1(1).Redraw = True
            grdDBGrid1(1).row = filaactual
            grdDBGrid1(1).Col = 6
            Exit Function
          End If
          C4 = CDec(grdDBGrid1(1).Columns(6).Value)
        End Select
        FILA = FILA + 1
    End If
  grdDBGrid1(1).MoveNext
  filaactual = filaactual + 1
Next i
grdDBGrid1(1).Redraw = True

If intregnoeliminados <> 3 Then
  Call MsgBox("Ha de insertar obligatoriamente exactamente 3 registros." & Chr(13) _
  & "Ej.: A 80 70 60 " & Chr(13) _
  & "     B 15 20 25 " & Chr(13) _
  & "     C  5 10 15 " _
  , vbInformation, "Aviso")
  Comprobar_ABC = 0
  Exit Function
End If

If A1 = "A" Then
  If B1 = "A" Then
    Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & B1 & "'" & _
                Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
    Comprobar_ABC = 0
    Exit Function
  ElseIf B1 = "B" Then
    If C1 = "A" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "B" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "C" Then
    Else
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor no valido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B � C", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    End If
  ElseIf B1 = "C" Then
    If C1 = "A" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "C" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "B" Then
    Else
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor no valido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B � C", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    End If
  Else
    Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor no valido: '" & B1 & "'" & _
                Chr(13) & "Los valores posibles son : A,B � C", vbInformation, "Aviso")
    Comprobar_ABC = 0
    Exit Function
  End If
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
ElseIf A1 = "B" Then
  If B1 = "B" Then
    Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & B1 & "'" & _
                Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
    Comprobar_ABC = 0
    Exit Function
  ElseIf B1 = "A" Then
    If C1 = "A" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "B" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "C" Then
    Else
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor no valido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B � C", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    End If
  ElseIf B1 = "C" Then
    If C1 = "B" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "C" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "A" Then
    Else
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor no valido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B � C", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    End If
  Else
    Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor no valido: '" & B1 & "'" & _
                Chr(13) & "Los valores posibles son : A,B � C", vbInformation, "Aviso")
    Comprobar_ABC = 0
    Exit Function
  End If
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
ElseIf A1 = "C" Then
  If B1 = "C" Then
    Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & B1 & "'" & _
                Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
    Comprobar_ABC = 0
    Exit Function
  ElseIf B1 = "A" Then
    If C1 = "A" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "C" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "B" Then
    Else
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor no valido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B � C", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    End If
  ElseIf B1 = "B" Then
    If C1 = "B" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "C" Then
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor repetido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B y C. Y no han de repetirse.", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    ElseIf C1 = "A" Then
    Else
      Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor no valido: '" & C1 & "'" & _
                  Chr(13) & "Los valores posibles son : A,B � C", vbInformation, "Aviso")
      Comprobar_ABC = 0
      Exit Function
    End If
  Else
    Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor no valido: '" & B1 & "'" & _
                Chr(13) & "Los valores posibles son : A,B � C", vbInformation, "Aviso")
    Comprobar_ABC = 0
    Exit Function
  End If
''''''''''''''''''''''''''''''''''''''''''''''''
Else
  Call MsgBox("La columna 'C�digo ABC Producto' contiene un valor no valido: '" & A1 & "'" & _
              Chr(13) & "Los valores posibles son : A,B � C", vbInformation, "Aviso")
  Comprobar_ABC = 0
  Exit Function
End If

If A2 + B2 + C2 <> 100 Then
  Call MsgBox("La suma de los valores de la columna 'Porcentaje en Unidades' ha de ser 100. NO :" & A2 + B2 + C2, vbInformation, "Aviso")
  Comprobar_ABC = 0
  grdDBGrid1(1).row = 0
  grdDBGrid1(1).Col = 4
  Exit Function
End If

If A3 + B3 + C3 <> 100 Then
  Call MsgBox("La suma de los valores de la columna 'Porcentaje en Dinero' ha de ser 100. NO :" & A3 + B3 + C3, vbInformation, "Aviso")
  Comprobar_ABC = 0
  grdDBGrid1(1).row = 0
  grdDBGrid1(1).Col = 5
  Exit Function
End If

'If A4 + B4 + C4 <> 100 Then
'  Call MsgBox("La suma de los valores de la columna 'Frecuencia de Revisi�n' ha de ser 100. NO :" & A4 + B4 + C4, vbInformation, "Aviso")
'  Comprobar_ABC = 0
'  grdDBGrid1(1).row = 0
'  grdDBGrid1(1).Col = 6
'  Exit Function
'End If


Comprobar_ABC = -1

End Function

