VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDispUrgencias 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Dispensar Stock de Planta. Dispensar Urgencias"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdservir 
      Caption         =   "Servir PRN"
      Height          =   375
      Left            =   3120
      TabIndex        =   36
      Top             =   7560
      Width           =   2295
   End
   Begin VB.CommandButton cmddispensar 
      Caption         =   "Dispensar sin PRN"
      Height          =   375
      Left            =   6960
      TabIndex        =   35
      Top             =   7560
      Width           =   2415
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "PRN Validados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4815
      Index           =   1
      Left            =   120
      TabIndex        =   9
      Top             =   480
      Width           =   11625
      Begin TabDlg.SSTab tabTab1 
         Height          =   4335
         Index           =   0
         Left            =   120
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   7646
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0145.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(14)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(28)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(3)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(4)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(5)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(6)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(7)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(8)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(9)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(10)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "dtcDateCombo1(2)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(0)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(1)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(2)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(3)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(4)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(5)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(6)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(7)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(8)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(9)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(10)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(11)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(12)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(13)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(14)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).ControlCount=   29
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0145.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            DataField       =   "FR91CODURGENCIA"
            Height          =   330
            Index           =   14
            Left            =   5160
            TabIndex        =   3
            Tag             =   "Urgencia"
            Top             =   3000
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   13
            Left            =   7080
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Desc.Urgencia"
            Top             =   3000
            Width           =   3360
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   12
            Left            =   5040
            TabIndex        =   33
            TabStop         =   0   'False
            Tag             =   "Dr."
            Top             =   2160
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            DataField       =   "SG02COD_MED"
            Height          =   330
            Index           =   11
            Left            =   3120
            TabIndex        =   31
            Tag             =   "C�d.M�dico Firma"
            Top             =   2160
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   10
            Left            =   4080
            MultiLine       =   -1  'True
            TabIndex        =   28
            Tag             =   "Paciente"
            Top             =   2760
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   9
            Left            =   3960
            MultiLine       =   -1  'True
            TabIndex        =   27
            Tag             =   "Paciente"
            Top             =   3480
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   8
            Left            =   3960
            MultiLine       =   -1  'True
            TabIndex        =   26
            Tag             =   "Paciente"
            Top             =   3120
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   7
            Left            =   7200
            TabIndex        =   24
            TabStop         =   0   'False
            Tag             =   "Apellido 2�"
            Top             =   1800
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   6
            Left            =   6720
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Apellido 1�"
            Top             =   1080
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   5
            Left            =   4800
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Nombre"
            Top             =   1080
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   4
            Left            =   6960
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "Historia"
            Top             =   360
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   3
            Left            =   4800
            MultiLine       =   -1  'True
            TabIndex        =   16
            Tag             =   "Paciente"
            Top             =   360
            Width           =   1920
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   2
            Left            =   2280
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Servicio"
            Top             =   1080
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   1
            Left            =   240
            MultiLine       =   -1  'True
            TabIndex        =   1
            Tag             =   "C�d.Servicio"
            Top             =   1080
            Width           =   1920
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR66CODPETICION"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   0
            Tag             =   "Petici�n"
            Top             =   360
            Width           =   1800
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4305
            Index           =   2
            Left            =   -75000
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   0
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   7594
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR66FECFIRMMEDI"
            Height          =   330
            Index           =   2
            Left            =   360
            TabIndex        =   29
            Tag             =   "Fecha Firma"
            Top             =   2160
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Urgencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   5160
            TabIndex        =   38
            Top             =   2760
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Desc Urgencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   7080
            TabIndex        =   37
            Top             =   2760
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "M�dico"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   5040
            TabIndex        =   34
            Top             =   1920
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "M�dico Firma"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   3120
            TabIndex        =   32
            Top             =   1920
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Firma"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   360
            TabIndex        =   30
            Top             =   1920
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 2�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   7200
            TabIndex        =   25
            Top             =   1560
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 1�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   6720
            TabIndex        =   23
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   4800
            TabIndex        =   21
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   6960
            TabIndex        =   19
            Top             =   120
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   4800
            TabIndex        =   17
            Top             =   120
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Desc.Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   2280
            TabIndex        =   15
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   240
            TabIndex        =   14
            Top             =   120
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   240
            TabIndex        =   13
            Top             =   840
            Width           =   975
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1920
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   5520
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1425
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   11280
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   19897
         _ExtentY        =   2514
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   5
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDispUrgencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDispUrgencias (FR0145.FRM)                                *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Dispensar Stock de Planta. Asignar Reposiciones          *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1



Private Sub cmddispensar_Click()
cmddispensar.Enabled = False
Call objsecurity.LaunchProcess("FR0146")
cmddispensar.Enabled = True
End Sub

Private Sub cmdservir_Click()
Dim rstdes As rdoResultset
Dim strdes As String
Dim rstori As rdoResultset
Dim strori As String
Dim strInsert As String
Dim strinsertentrada As String
Dim strinsertsalida As String
Dim str35 As String
Dim rst35 As rdoResultset
Dim str80 As String
Dim rst80 As rdoResultset
Dim i As Integer


cmdservir.Enabled = False
If txtText1(0).Text <> "" Then
    'se obtiene el almac�n de destino
    strdes = "SELECT FR04CODALMACEN FROM AD0200 WHERE AD02CODDPTO=" & txtText1(1).Text
    Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
    'se obtiene el almac�n del servicio 1(Farmacia) que es origen
    strori = "SELECT FR04CODALMACEN FROM AD0200,FRH200 " & _
            "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
            "AND FRH2CODPARAMGEN=3"
    Set rstori = objApp.rdoConnect.OpenResultset(strori)
    
 If (Not rstdes.EOF And Not rstori.EOF) And Not IsNull(rstori.rdoColumns(0).Value) And _
     Not IsNull(rstdes.rdoColumns(0).Value) Then
   
    strInsert = "UPDATE FR6600 SET FR26CODESTPETIC=5 WHERE FR66CODPETICION=" & txtText1(0).Text
    objApp.rdoConnect.Execute strInsert, 64
    objApp.rdoConnect.Execute "Commit", 64
   
    grdDBGrid1(0).MoveFirst
    For i = 0 To grdDBGrid1(0).Rows - 1
        str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
        Set rst35 = objApp.rdoConnect.OpenResultset(str35)
        
        str80 = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
        Set rst80 = objApp.rdoConnect.OpenResultset(str80)
        
        'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
        'Farmacia y el destino es el almac�n al que se dispensa
        strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                      "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) VALUES (" & _
                      rst35.rdoColumns(0).Value & "," & _
                      rstori.rdoColumns(0).Value & "," & _
                      rstdes.rdoColumns(0).Value & "," & _
                      "8" & "," & _
                      "SYSDATE" & "," & _
                      "NULL" & "," & _
                      grdDBGrid1(0).Columns(4).Value & "," & _
                      grdDBGrid1(0).Columns(6).Value & "," & _
                      "0" & "," & _
                      grdDBGrid1(0).Columns(7).Value & ")"
        objApp.rdoConnect.Execute strinsertentrada, 64
        objApp.rdoConnect.Execute "Commit", 64
        
        'se actualiza el inventario de salida(FR8000) en el que el origen de salida es farmacia
        'y el destino de la salida el almac�n al que se dispensa
        strinsertsalida = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                     "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
                     "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES(" & _
                      rst80.rdoColumns(0).Value & "," & _
                      rstori.rdoColumns(0).Value & "," & _
                      rstdes.rdoColumns(0).Value & "," & _
                      "7" & "," & _
                      "SYSDATE" & "," & _
                      "NULL" & "," & _
                      grdDBGrid1(0).Columns(4).Value & "," & _
                      grdDBGrid1(0).Columns(6).Value & "," & _
                      "0" & ","
        If grdDBGrid1(0).Columns(7).Value <> "" Then
            strinsertsalida = strinsertsalida & grdDBGrid1(0).Columns(7).Value & ")"
        Else
            strinsertsalida = strinsertsalida & "1)"
        End If


        objApp.rdoConnect.Execute strinsertsalida, 64
        objApp.rdoConnect.Execute "Commit", 64
        
        rst35.Close
        Set rst35 = Nothing
        rst80.Close
        Set rst80 = Nothing
        
   grdDBGrid1(0).MoveNext
   Next i
 End If
rstdes.Close
Set rstdes = Nothing
rstori.Close
Set rstori = Nothing
End If
Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
objWinInfo.DataRefresh
cmdservir.Enabled = True
End Sub

Private Sub Form_Activate()
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.DataRefresh
    tabTab1(0).Tab = 1
    grdDBGrid1(2).Width = 11295
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Peticiones"
      
    .strTable = "FR6600"
    'Estado de la petici�n=4 (petici�n validada) y s�lo peticiones que sean PRN
    .strWhere = "FR26CODESTPETIC=4 AND (FR66INDOM=0 OR FR66INDOM IS NULL)"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR66FECFIRMMEDI", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones")
    Call .FormAddFilterWhere(strKey, "FR66CODPETICION", "C�digo Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Persona", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_MED", "C�digo M�dico", cwString)
    Call .FormAddFilterWhere(strKey, "FR66FECFIRMMEDI", "Fecha Firma", cwDate)
    Call .FormAddFilterWhere(strKey, "FR91CODURGENCIA", "C�d.Urgencia", cwNumeric)

    Call .FormAddFilterOrder(strKey, "FR66CODPETICION", "C�digo Petici�n")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Servicio")
    Call .FormAddFilterOrder(strKey, "CI21CODPERSONA", "C�digo Persona")
    Call .FormAddFilterOrder(strKey, "FR66FECFIRMMEDI", "Fecha Firma")


  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "PRN"
    .strTable = "FR3200"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
    Call .FormAddRelation("FR66CODPETICION", txtText1(0))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "PRN")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR28DOSIS", "Dosis", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�d.Unidad Medida", cwNumeric)

    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "Petici�n", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�d.Producto", "FR73CODPRODUCTO", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Producto", "", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR28DOSIS", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "C�d.Unidad Medida", "FR93CODUNIMEDIDA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Unidad Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Observaciones", "FR32OBSERVFARM", cwString, 2000)

    
    
    Call .FormCreateInfo(objMasterInfo)
    
    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnInFind = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(4)), grdDBGrid1(0).Columns(5), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(7)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(7)), grdDBGrid1(0).Columns(8), "FR93DESUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(2), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(4), "CI22NUMHISTORIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(5), "CI22NOMBRE")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(9)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(9)), txtText1(6), "CI22PRIAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(10)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(7), "CI22SEGAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(11)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(11)), txtText1(12), "SG02APE1")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "FR91CODURGENCIA", "SELECT * FROM FR9100 WHERE FR91CODURGENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(13), "FR91DESURGENCIA")
    
    
    grdDBGrid1(0).Columns(3).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  grdDBGrid1(2).Columns(8).Width = 0
  grdDBGrid1(2).Columns(10).Width = 0
  grdDBGrid1(2).Columns(12).Width = 0
  
  grdDBGrid1(2).Columns(1).Width = 1000 'peticion
  grdDBGrid1(2).Columns(2).Width = 1000 'cod.servicio
  grdDBGrid1(2).Columns(3).Width = 1000 'servicio
  grdDBGrid1(2).Columns(4).Width = 600 'c�d.urgencia
  grdDBGrid1(2).Columns(5).Width = 1000 'urgencia
  grdDBGrid1(2).Columns(6).Width = 1000 'c�d.paciente
  grdDBGrid1(2).Columns(7).Width = 1000 'historia
  grdDBGrid1(2).Columns(9).Width = 1000 'nombre
  grdDBGrid1(2).Columns(11).Width = 1000 'apellido 1�
  grdDBGrid1(2).Columns(13).Width = 1300 'apellido 2�
  grdDBGrid1(2).Columns(13).Width = 1000 'fecha
  grdDBGrid1(2).Columns(14).Width = 1000 'c�d.m�dico
  grdDBGrid1(2).Columns(15).Width = 1000 'Dr.
  


Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub





Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
 '  Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


