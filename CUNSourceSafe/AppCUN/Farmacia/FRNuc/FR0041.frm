VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDefProcFab 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DEFINIR FARMACIA.Definir Producto.Definir Proceso Fabricaci�n"
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8295
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Fabricaci�n Producto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7200
      Index           =   1
      Left            =   120
      TabIndex        =   27
      Top             =   600
      Width           =   4695
      Begin ComctlLib.TreeView tvwItems 
         Height          =   6555
         Index           =   0
         Left            =   360
         TabIndex        =   28
         Top             =   480
         Width           =   4080
         _ExtentX        =   7197
         _ExtentY        =   11562
         _Version        =   327682
         HideSelection   =   0   'False
         Indentation     =   353
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   25
      Top             =   8010
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab tabGeneral1 
      Height          =   7095
      Index           =   1
      Left            =   4920
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   720
      Width           =   6735
      _ExtentX        =   11880
      _ExtentY        =   12515
      _Version        =   327681
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Proceso"
      TabPicture(0)   =   "FR0041.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Operaci�n"
      TabPicture(1)   =   "FR0041.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(2)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Producto"
      TabPicture(2)   =   "FR0041.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraFrame1(3)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6495
         Index           =   3
         Left            =   -74760
         TabIndex        =   44
         Top             =   450
         Width           =   6180
         Begin TabDlg.SSTab tabTab1 
            Height          =   6135
            Index           =   1
            Left            =   120
            TabIndex        =   45
            TabStop         =   0   'False
            Top             =   240
            Width           =   6015
            _ExtentX        =   10610
            _ExtentY        =   10821
            _Version        =   327681
            TabOrientation  =   3
            Tabs            =   2
            TabsPerRow      =   1
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Tab 0"
            TabPicture(0)   =   "FR0041.frx":0054
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(7)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(8)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(9)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(11)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(12)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(13)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(19)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(20)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(8)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(9)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(10)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(11)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(12)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(13)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(19)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(20)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).ControlCount=   16
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0041.frx":0070
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(1)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR71TPPERDIDAS"
               Height          =   330
               Index           =   20
               Left            =   240
               TabIndex        =   24
               Tag             =   "Porcentaje P�rdidas|Porcentaje P�rdidas"
               Top             =   3600
               Width           =   990
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR71CANTNECESARIA"
               Height          =   330
               Index           =   19
               Left            =   240
               TabIndex        =   23
               Tag             =   "Cantidad Necesaria|Cantidad Necesaria"
               Top             =   2880
               Width           =   1350
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   13
               Left            =   240
               Locked          =   -1  'True
               TabIndex        =   17
               Tag             =   "C�digo Producto|C�digo Producto"
               Top             =   600
               Width           =   510
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   12
               Left            =   1920
               Locked          =   -1  'True
               TabIndex        =   18
               TabStop         =   0   'False
               Tag             =   "Descripci�n Producto"
               Top             =   600
               Width           =   3615
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR70CODOPERACION"
               Height          =   330
               Index           =   11
               Left            =   240
               Locked          =   -1  'True
               TabIndex        =   19
               Tag             =   "C�digo Operaci�n|C�digo Operaci�n"
               Top             =   1320
               Width           =   510
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   10
               Left            =   1920
               Locked          =   -1  'True
               TabIndex        =   20
               TabStop         =   0   'False
               Tag             =   "Descripci�n Operaci�n"
               Top             =   1320
               Width           =   3615
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   9
               Left            =   1920
               Locked          =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   22
               TabStop         =   0   'False
               Tag             =   "Descripci�n Carro|Descripci�n Carro"
               Top             =   2040
               Width           =   3600
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR69CODPROCFABRIC"
               Height          =   330
               Index           =   8
               Left            =   240
               Locked          =   -1  'True
               TabIndex        =   21
               Tag             =   "C�digo Proceso Fabricacion|C�digo Proceso Fabricacion"
               Top             =   2040
               Width           =   510
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5715
               Index           =   1
               Left            =   -74760
               TabIndex        =   50
               TabStop         =   0   'False
               Top             =   120
               Width           =   5175
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   9128
               _ExtentY        =   10081
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Porcentaje P�rdidas"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   240
               TabIndex        =   59
               Top             =   3360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Cantidad Necesaria"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   19
               Left            =   240
               TabIndex        =   58
               Top             =   2640
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   270
               TabIndex        =   52
               Top             =   360
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   1950
               TabIndex        =   51
               Top             =   360
               Width           =   2175
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Operaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   270
               TabIndex        =   49
               Top             =   1080
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Operaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   1950
               TabIndex        =   48
               Top             =   1080
               Width           =   2175
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Proceso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   1950
               TabIndex        =   47
               Top             =   1800
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�digo Proc. Fab."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   270
               TabIndex        =   46
               Top             =   1800
               Width           =   1575
            End
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6615
         Index           =   2
         Left            =   -74760
         TabIndex        =   37
         Top             =   360
         Width           =   6180
         Begin TabDlg.SSTab tabTab1 
            Height          =   6255
            Index           =   2
            Left            =   120
            TabIndex        =   38
            TabStop         =   0   'False
            Top             =   240
            Width           =   6015
            _ExtentX        =   10610
            _ExtentY        =   11033
            _Version        =   327681
            TabOrientation  =   3
            Tabs            =   2
            TabsPerRow      =   1
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Tab 0"
            TabPicture(0)   =   "FR0041.frx":008C
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(2)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(3)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(4)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(5)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(14)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(15)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(16)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(17)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(18)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(4)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(5)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(6)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtText1(7)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(14)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(15)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(16)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(17)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(18)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).ControlCount=   18
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0041.frx":00A8
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(0)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   18
               Left            =   1800
               Locked          =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   15
               TabStop         =   0   'False
               Tag             =   "Descripci�n Centro Coste"
               Top             =   3960
               Width           =   3720
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR70PATHDESOPE"
               Height          =   690
               Index           =   17
               Left            =   240
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   16
               Tag             =   "Path del Archivo Descripci�n Operaci�n|Path del Archivo Descripci�n Operaci�n"
               Top             =   4680
               Width           =   5310
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR08CODCENTRCOSTE"
               Height          =   330
               Index           =   16
               Left            =   240
               TabIndex        =   14
               Tag             =   "C�digo Centro de Coste|C�digo Centro de Coste"
               Top             =   3960
               Width           =   990
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR70TIEMPEJECUCIO"
               Height          =   330
               Index           =   15
               Left            =   240
               TabIndex        =   13
               Tag             =   "Tiempo de Ejecuci�n|Tiempo de Ejecuci�n"
               Top             =   3240
               Width           =   990
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR70TIEMPOPREPACI"
               Height          =   330
               Index           =   14
               Left            =   240
               TabIndex        =   12
               Tag             =   "Tiempo de Preparaci�n|Tiempo de Preparaci�n"
               Top             =   2520
               Width           =   990
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR69CODPROCFABRIC"
               Height          =   330
               Index           =   7
               Left            =   240
               Locked          =   -1  'True
               TabIndex        =   10
               Tag             =   "C�digo Proceso Fabricacion|C�digo Proceso Fabricacion"
               Top             =   1680
               Width           =   510
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   6
               Left            =   1350
               Locked          =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   11
               TabStop         =   0   'False
               Tag             =   "Descripci�n Carro|Descripci�n Carro"
               Top             =   1680
               Width           =   4200
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR70DESOPERACION"
               Height          =   810
               Index           =   5
               Left            =   1320
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   9
               Tag             =   "Descripci�n Operaci�n"
               Top             =   480
               Width           =   4215
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR70CODOPERACION"
               Height          =   330
               Index           =   4
               Left            =   240
               Locked          =   -1  'True
               TabIndex        =   8
               Tag             =   "C�digo Operaci�n|C�digo Operaci�n"
               Top             =   480
               Width           =   510
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5835
               Index           =   0
               Left            =   -74880
               TabIndex        =   43
               TabStop         =   0   'False
               Top             =   120
               Width           =   5175
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   9128
               _ExtentY        =   10292
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Centro Coste"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   1800
               TabIndex        =   57
               Top             =   3720
               Width           =   2175
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Archivo Descripci�n Operaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   240
               TabIndex        =   56
               Top             =   4440
               Width           =   2775
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Centro de Coste"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   240
               TabIndex        =   55
               Top             =   3720
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tiempo de Ejecuci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   240
               TabIndex        =   54
               Top             =   3000
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tiempo de Preparaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   240
               TabIndex        =   53
               Top             =   2280
               Width           =   2415
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Proc. Fab."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   240
               TabIndex        =   42
               Top             =   1440
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Proceso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   4
               Left            =   1350
               TabIndex        =   41
               Top             =   1440
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Operaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   1350
               TabIndex        =   40
               Top             =   240
               Width           =   2175
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Operaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   270
               TabIndex        =   39
               Top             =   240
               Width           =   975
            End
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6615
         Index           =   0
         Left            =   300
         TabIndex        =   30
         Top             =   330
         Width           =   6180
         Begin TabDlg.SSTab tabTab1 
            Height          =   6255
            Index           =   0
            Left            =   120
            TabIndex        =   31
            TabStop         =   0   'False
            Top             =   240
            Width           =   6015
            _ExtentX        =   10610
            _ExtentY        =   11033
            _Version        =   327681
            TabOrientation  =   3
            Tabs            =   2
            TabsPerRow      =   1
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Tab 0"
            TabPicture(0)   =   "FR0041.frx":00C4
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(0)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(1)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(10)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(6)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "txtText1(0)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(1)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtText1(3)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(2)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "chkCheck1(0)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "chkCheck1(1)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "chkCheck1(2)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "chkCheck1(3)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).ControlCount=   12
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0041.frx":00E0
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(2)"
            Tab(1).ControlCount=   1
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Indicador Mezclas Intravenosas"
               DataField       =   "FR69INDMEZCINTRAV"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   240
               TabIndex        =   7
               Tag             =   "Mezclas Intravenosas?"
               Top             =   3960
               Width           =   3255
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Indicador Nutrici�n Artificial"
               DataField       =   "FR69INDNUTRIARTIF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   240
               TabIndex        =   6
               Tag             =   "Nutrici�n Artificial?"
               Top             =   3480
               Width           =   3015
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Indicador F�rmula Magistral"
               DataField       =   "FR69INDFORMMAGIST"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   240
               TabIndex        =   5
               Tag             =   "F�rmula Magistral?"
               Top             =   3000
               Width           =   2775
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Indicador Citost�ticos"
               DataField       =   "FR69INDCITOSTATIC"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   240
               TabIndex        =   4
               Tag             =   "Citost�ticos?"
               Top             =   2520
               Width           =   2415
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00808080&
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   2
               Left            =   240
               Locked          =   -1  'True
               TabIndex        =   2
               TabStop         =   0   'False
               Tag             =   "C�digo Producto|C�digo Producto"
               Top             =   1920
               Width           =   510
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Enabled         =   0   'False
               Height          =   330
               Index           =   3
               Left            =   1230
               Locked          =   -1  'True
               TabIndex        =   3
               TabStop         =   0   'False
               Tag             =   "Descripci�n Producto"
               Top             =   1920
               Width           =   4215
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR69DESPROCESO"
               Height          =   810
               Index           =   1
               Left            =   1230
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   1
               Tag             =   "Descripci�n Proceso|Descripci�n Proceso"
               Top             =   600
               Width           =   4200
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "FR69CODPROCFABRIC"
               Height          =   330
               Index           =   0
               Left            =   270
               Locked          =   -1  'True
               TabIndex        =   0
               Tag             =   "C�digo Proceso Fabricacion|C�digo Proceso Fabricacion"
               Top             =   600
               Width           =   510
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5745
               Index           =   2
               Left            =   -74850
               TabIndex        =   32
               TabStop         =   0   'False
               Top             =   120
               Width           =   5295
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   9340
               _ExtentY        =   10134
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   240
               TabIndex        =   36
               Top             =   1680
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   1230
               TabIndex        =   35
               Top             =   1680
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Proceso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   1230
               TabIndex        =   34
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Proc. Fab."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   240
               TabIndex        =   33
               Top             =   360
               Width           =   975
            End
         End
      End
   End
   Begin ComctlLib.ImageList imlImagenes 
      Left            =   0
      Top             =   7440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   19
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":00FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":015A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":01B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":0216
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":0274
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":02D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":0330
            Key             =   "rol"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":038E
            Key             =   "function"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":03EC
            Key             =   "window"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":044A
            Key             =   "report"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":04A8
            Key             =   "externalreport"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":0506
            Key             =   "process"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":0564
            Key             =   "application"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":05C2
            Key             =   "big"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":0620
            Key             =   "small"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":067E
            Key             =   "list"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":06DC
            Key             =   "details"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":073A
            Key             =   "exit"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0041.frx":0798
            Key             =   "refresh"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefProcFab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDefProcFab (FR0041.FRM)                                   *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: Definir Proceso de Fabricaci�n de un producto           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim mblnmoving As Boolean
' Declara variables globales.
Dim Enarbol As Boolean
Dim Enabrir As Boolean
Dim EnPostWrite As Boolean

Private Sub rellenar_TreeView()
  Dim strRelproc As String
  Dim rstRelproc As rdoResultset
  Dim strReloper As String
  Dim rstReloper As rdoResultset
  Dim strRelprod As String
  Dim rstRelprod As rdoResultset
  Dim strReldesprod As String
  Dim rstReldesprod As rdoResultset
  Dim producto, desproducto As Variant
  Dim proceso, desproceso As Variant
  Dim operacion, desoperacion As Variant
  Dim prodhijo, desprodhijo As Variant
  
    producto = frmDefProducto.txtText1(0).Text
    desproducto = frmDefProducto.txtText1(1).Text
    
    strRelproc = "SELECT * FROM FR6900 WHERE FR73CODPRODUCTO=" & producto & " ORDER BY FR69CODPROCFABRIC"
    Set rstRelproc = objApp.rdoConnect.OpenResultset(strRelproc)
  
    Call tvwItems(0).Nodes.Add(, , "product" & producto, producto & ".-" & desproducto)
  
    While rstRelproc.EOF = False
      proceso = rstRelproc("FR69CODPROCFABRIC").Value
      desproceso = rstRelproc("FR69DESPROCESO").Value
      Call tvwItems(0).Nodes.Add("product" & producto, tvwChild, "proceso" & producto & "/" & proceso, proceso & ".-" & desproceso)
      strReloper = "SELECT * FROM FR7000 WHERE FR69CODPROCFABRIC=" & proceso & " ORDER BY FR70CODOPERACION"
      Set rstReloper = objApp.rdoConnect.OpenResultset(strReloper)
      While rstReloper.EOF = False
        operacion = rstReloper("FR70CODOPERACION").Value
        desoperacion = rstReloper("FR70DESOPERACION").Value
        Call tvwItems(0).Nodes.Add("proceso" & producto & "/" & proceso, tvwChild, "operaci" & producto & "/" & proceso & "/" & operacion, operacion & ".-" & desoperacion)
        strRelprod = "SELECT * FROM FR7100 WHERE FR69CODPROCFABRIC=" & proceso & _
                     " AND FR70CODOPERACION=" & operacion & " ORDER BY FR73CODPRODUCTO"
        Set rstRelprod = objApp.rdoConnect.OpenResultset(strRelprod)
        While rstRelprod.EOF = False
            prodhijo = rstRelprod("FR73CODPRODUCTO").Value
            strReldesprod = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=" & prodhijo
            Set rstReldesprod = objApp.rdoConnect.OpenResultset(strReldesprod)
            desprodhijo = rstReldesprod("FR73DESPRODUCTO").Value
            Call tvwItems(0).Nodes.Add("operaci" & producto & "/" & proceso & "/" & operacion, tvwChild, "prodhij" & producto & "/" & proceso & "/" & operacion & "/" & prodhijo, prodhijo & ".-" & desprodhijo)
            rstReldesprod.Close
            Set rstReldesprod = Nothing
            rstRelprod.MoveNext
        Wend
        rstRelprod.Close
        Set rstRelprod = Nothing
        rstReloper.MoveNext
      Wend
      rstReloper.Close
      Set rstReloper = Nothing
      rstRelproc.MoveNext
    Wend
    rstRelproc.Close
    Set rstRelproc = Nothing

    tvwItems(0).Nodes("product" & producto).Selected = True
    tvwItems(0).Nodes("product" & producto).Expanded = True
    
    tabTab1(0).Enabled = True
    tabTab1(2).Enabled = False
    tabTab1(1).Enabled = False

End Sub

Private Sub borrar_procesos()
  Dim strRelproc As String
  Dim rstRelproc As rdoResultset
  Dim strReloper As String
  Dim rstReloper As rdoResultset
  Dim producto As Variant
  Dim proceso As Variant
  Dim operacion As Variant
  Dim Fin As Boolean
  Dim hijosP As Integer
  Dim ProcesosIncompletos
  Dim aux As String
  Dim sqlstr As String
  
  Fin = False
  
  producto = frmDefProducto.txtText1(0).Text
  
  strRelproc = "SELECT * FROM FR6900 WHERE FR73CODPRODUCTO=" & producto & " ORDER BY FR69CODPROCFABRIC"
  Set rstRelproc = objApp.rdoConnect.OpenResultset(strRelproc)

  While rstRelproc.EOF = False
    proceso = rstRelproc("FR69CODPROCFABRIC").Value
    hijosP = 0
    strReloper = "SELECT * FROM FR7000 WHERE FR69CODPROCFABRIC=" & proceso & " ORDER BY FR70CODOPERACION"
    Set rstReloper = objApp.rdoConnect.OpenResultset(strReloper)
    If rstReloper.EOF = False Then
      operacion = rstReloper("FR70CODOPERACION").Value
      hijosP = hijosP + 1
      rstReloper.MoveNext
    End If
    rstReloper.Close
    Set rstReloper = Nothing
    If hijosP = 0 Then
      ProcesosIncompletos = ProcesosIncompletos & proceso & "  "
      Fin = True
    End If
    rstRelproc.MoveNext
  Wend
  rstRelproc.Close
  Set rstRelproc = Nothing

  If Fin = True Then
      'borrar procesos
      aux = ""
      aux = ProcesosIncompletos
      aux = Left(ProcesosIncompletos, _
            InStr(ProcesosIncompletos, " ") - 1)
      proceso = aux
      
      'borrar elemento
      sqlstr = "DELETE FROM FR6900 WHERE " & _
               "FR69CODPROCFABRIC=" & proceso
      objApp.rdoConnect.Execute sqlstr, 64
      objApp.rdoConnect.Execute "commit", 64
      
      ProcesosIncompletos = Right(ProcesosIncompletos, Len(ProcesosIncompletos) - InStr(ProcesosIncompletos, " ") - 1)
      While ProcesosIncompletos <> ""
        aux = Left(ProcesosIncompletos, _
              InStr(ProcesosIncompletos, " ") - 1)
        proceso = aux
        
        'borrar elemento
        sqlstr = "DELETE FROM FR6900 WHERE " & _
                 "FR69CODPROCFABRIC=" & proceso
        objApp.rdoConnect.Execute sqlstr, 64
        objApp.rdoConnect.Execute "commit", 64
        
        ProcesosIncompletos = Right(ProcesosIncompletos, Len(ProcesosIncompletos) - InStr(ProcesosIncompletos, " ") - 1)
      Wend
  End If

End Sub


Private Function Comprobar_hijos() As Boolean
  Dim strRelproc As String
  Dim rstRelproc As rdoResultset
  Dim strReloper As String
  Dim rstReloper As rdoResultset
  Dim strRelprod As String
  Dim rstRelprod As rdoResultset
  Dim producto As Variant
  Dim proceso As Variant
  Dim operacion As Variant
  Dim Fin As Boolean
  Dim hijosP As Integer
  Dim hijosO As Integer
  Dim ProcesosIncompletos
  Dim OperacionesIncompletas
  Dim aux As String
  Dim sqlstr As String
  
  Fin = False
  
  producto = frmDefProducto.txtText1(0).Text
  
  strRelproc = "SELECT * FROM FR6900 WHERE FR73CODPRODUCTO=" & producto & " ORDER BY FR69CODPROCFABRIC"
  Set rstRelproc = objApp.rdoConnect.OpenResultset(strRelproc)

  While rstRelproc.EOF = False
    proceso = rstRelproc("FR69CODPROCFABRIC").Value
    hijosP = 0
    strReloper = "SELECT * FROM FR7000 WHERE FR69CODPROCFABRIC=" & proceso & " ORDER BY FR70CODOPERACION"
    Set rstReloper = objApp.rdoConnect.OpenResultset(strReloper)
    While rstReloper.EOF = False
      operacion = rstReloper("FR70CODOPERACION").Value
      hijosP = hijosP + 1
      hijosO = 0
      strRelprod = "SELECT * FROM FR7100 WHERE FR69CODPROCFABRIC=" & proceso & _
                   " AND FR70CODOPERACION=" & operacion & " ORDER BY FR73CODPRODUCTO"
      Set rstRelprod = objApp.rdoConnect.OpenResultset(strRelprod)
      If rstRelprod.EOF = False Then
          hijosO = hijosO + 1
      End If
      rstRelprod.Close
      Set rstRelprod = Nothing
      If hijosO = 0 Then
        OperacionesIncompletas = OperacionesIncompletas & proceso & "-" & operacion & "  "
        Fin = True
      End If
      rstReloper.MoveNext
    Wend
    rstReloper.Close
    Set rstReloper = Nothing
    If hijosP = 0 Then
      ProcesosIncompletos = ProcesosIncompletos & proceso & "  "
      Fin = True
    End If
    rstRelproc.MoveNext
  Wend
  rstRelproc.Close
  Set rstRelproc = Nothing

  If Fin = True Then
    Comprobar_hijos = False
    If MsgBox("Los Siguientes Procesos y Operaciones" & _
            " no est�n completamente definidos," & _
            " si sale seran eliminados. � Desea borrar dichos Procesos y Operaciones?" & _
            " Si pulsa SI saldr� pero borrar� todos ellos, si pula NO" & _
            " deber� definirlos completamente." & Chr(13) & Chr(13) & _
            "PROCESOS: " & Chr(13) & ProcesosIncompletos & _
            Chr(13) & Chr(13) & "OPERACIONES:" & Chr(13) & OperacionesIncompletas, vbExclamation + vbYesNo) = vbYes Then
      'borrar todos
      'borrar operaciones
      aux = ""
      aux = OperacionesIncompletas
      If aux = "" Then
        'solo hay que borrar procesos
        Call borrar_procesos
        Comprobar_hijos = True
        Exit Function
      End If
      
      aux = Left(OperacionesIncompletas, _
            InStr(OperacionesIncompletas, " ") - 1)
      proceso = Left(aux, InStr(aux, "-") - 1)
      operacion = Right(aux, Len(aux) - InStr(aux, "-"))
      
      'borrar elemento
      sqlstr = "DELETE FROM FR7000 WHERE " & _
               "FR69CODPROCFABRIC=" & proceso & _
               " AND FR70CODOPERACION=" & operacion
      objApp.rdoConnect.Execute sqlstr, 64
      objApp.rdoConnect.Execute "commit", 64
      
      OperacionesIncompletas = Right(OperacionesIncompletas, Len(OperacionesIncompletas) - InStr(OperacionesIncompletas, " ") - 1)
      While OperacionesIncompletas <> ""
        aux = Left(OperacionesIncompletas, _
              InStr(OperacionesIncompletas, " ") - 1)
        proceso = Left(aux, InStr(aux, "-") - 1)
        operacion = Right(aux, Len(aux) - InStr(aux, "-"))
        
        'borrar elemento
        sqlstr = "DELETE FROM FR7000 WHERE " & _
                 "FR69CODPROCFABRIC=" & proceso & _
                 " AND FR70CODOPERACION=" & operacion
        objApp.rdoConnect.Execute sqlstr, 64
        objApp.rdoConnect.Execute "commit", 64
        
        OperacionesIncompletas = Right(OperacionesIncompletas, Len(OperacionesIncompletas) - InStr(OperacionesIncompletas, " ") - 1)
      Wend
      
      'borrar procesos
      Call borrar_procesos
      
      Comprobar_hijos = True
    Else
      Comprobar_hijos = False
    End If
  Else
    Comprobar_hijos = True
  End If

End Function


Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)

  If intNewStatus = cwModeSingleEdit And intOldStatus = cwModeSingleOpen Then
    'ir al nodo correspondiente al abrir
    If Enabrir Then
      Select Case tabGeneral1(1).Tab
      Case 0
        tvwItems(0).Nodes("proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(0).Text).Selected = True
      Case 1
        tvwItems(0).Nodes("operaci" & frmDefProducto.txtText1(0).Text & "/" & txtText1(7).Text & "/" & txtText1(4).Text).Selected = True
      Case 2
        tvwItems(0).Nodes("prodhij" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text & "/" & txtText1(13).Text).Selected = True
      End Select
    End If
    Enabrir = False
  End If

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)

Dim i As Integer
Dim aux, aux2 As Variant
  
  If blnError = False Then
    'ver si esta el nodo en el arbol
    Select Case strFormName
    'EN PROCESO
    Case "fraFrame1(0)"
      If tvwItems(0).Nodes("product" & frmDefProducto.txtText1(0).Text).Children > 0 Then
        i = tvwItems(0).Nodes("product" & frmDefProducto.txtText1(0).Text).Child.FirstSibling.Index
        aux = CInt(Right(tvwItems(0).Nodes(i).Key, Len(tvwItems(0).Nodes(i).Key) - InStr(tvwItems(0).Nodes(i).Key, "/")))
        aux2 = CInt(txtText1(0).Text)
        If aux > aux2 Then
          Call tvwItems(0).Nodes.Add(tvwItems(0).Nodes(i).Key, _
               tvwPrevious, "proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(0).Text _
               , txtText1(0).Text & ".-" & txtText1(1).Text)
          tvwItems(0).Nodes("proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(0).Text).Selected = True
          Exit Sub
        ElseIf aux = aux2 Then
          Exit Sub
        End If
        While i <> tvwItems(0).Nodes("product" & frmDefProducto.txtText1(0).Text).Child.LastSibling.Index And aux < aux2
          If tvwItems(0).Nodes(i).Key = "proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(0).Text Then
            Exit Sub
          End If
          i = tvwItems(0).Nodes(i).Next.Index
          aux = CInt(Right(tvwItems(0).Nodes(i).Key, Len(tvwItems(0).Nodes(i).Key) - InStr(tvwItems(0).Nodes(i).Key, "/")))
        Wend
        'no existe
        If tvwItems(0).Nodes(i).Key = "proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(0).Text Then
          Exit Sub
        End If
        EnPostWrite = True
        If aux < aux2 Then
          Call tvwItems(0).Nodes.Add(tvwItems(0).Nodes(i).Key, _
               tvwNext, "proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(0).Text _
               , txtText1(0).Text & ".-" & txtText1(1).Text)
        Else
          Call tvwItems(0).Nodes.Add(tvwItems(0).Nodes(i).Key, _
               tvwPrevious, "proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(0).Text _
               , txtText1(0).Text & ".-" & txtText1(1).Text)
        End If
        tvwItems(0).Nodes("proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(0).Text).Selected = True
      Else 'no existe
        EnPostWrite = True
        Call tvwItems(0).Nodes.Add("product" & frmDefProducto.txtText1(0).Text, _
             tvwChild, "proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(0).Text _
             , txtText1(0).Text & ".-" & txtText1(1).Text)
        tvwItems(0).Nodes("proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(0).Text).Selected = True
      End If
    
    'EN OPERACION
    Case "fraFrame1(2)"
      If tvwItems(0).Nodes("proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(7).Text).Children > 0 Then
        i = tvwItems(0).Nodes("proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(7).Text).Child.FirstSibling.Index
        aux = Right(tvwItems(0).Nodes(i).Key, Len(tvwItems(0).Nodes(i).Key) - InStr(tvwItems(0).Nodes(i).Key, "/"))
        aux = CInt(Right(aux, Len(aux) - InStr(aux, "/")))
        aux2 = txtText1(4).Text
        If aux > aux2 Then
          Call tvwItems(0).Nodes.Add(tvwItems(0).Nodes(i).Key, _
               tvwPrevious, "operaci" & frmDefProducto.txtText1(0).Text & "/" _
               & txtText1(7).Text & "/" & txtText1(4).Text, _
               txtText1(4).Text & ".-" & txtText1(5).Text)
          tvwItems(0).Nodes("operaci" & frmDefProducto.txtText1(0).Text & "/" & txtText1(7).Text & "/" & txtText1(4).Text).Selected = True
          Exit Sub
        ElseIf aux = aux2 Then
          Exit Sub
        End If
        
        While i <> tvwItems(0).Nodes("proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(7).Text).Child.LastSibling.Index And aux < aux2
          If tvwItems(0).Nodes(i).Key = "operaci" & frmDefProducto.txtText1(0).Text & "/" & txtText1(7).Text & "/" & txtText1(4).Text Then
            Exit Sub
          End If
          i = tvwItems(0).Nodes(i).Next.Index
          aux = Right(tvwItems(0).Nodes(i).Key, Len(tvwItems(0).Nodes(i).Key) - InStr(tvwItems(0).Nodes(i).Key, "/"))
          aux = CInt(Right(aux, Len(aux) - InStr(aux, "/")))
        Wend
        'no existe
        If tvwItems(0).Nodes(i).Key = "operaci" & frmDefProducto.txtText1(0).Text & "/" & txtText1(7).Text & "/" & txtText1(4).Text Then
          Exit Sub
        End If
        EnPostWrite = True
        If aux < aux2 Then
          Call tvwItems(0).Nodes.Add(tvwItems(0).Nodes(i).Key, _
               tvwNext, "operaci" & frmDefProducto.txtText1(0).Text & "/" _
               & txtText1(7).Text & "/" & txtText1(4).Text, _
               txtText1(4).Text & ".-" & txtText1(5).Text)
        Else
          Call tvwItems(0).Nodes.Add(tvwItems(0).Nodes(i).Key, _
               tvwPrevious, "operaci" & frmDefProducto.txtText1(0).Text & "/" _
               & txtText1(7).Text & "/" & txtText1(4).Text, _
               txtText1(4).Text & ".-" & txtText1(5).Text)
        End If
        tvwItems(0).Nodes("operaci" & frmDefProducto.txtText1(0).Text & "/" & txtText1(7).Text & "/" & txtText1(4).Text).Selected = True
      Else 'no existe
        EnPostWrite = True
        Call tvwItems(0).Nodes.Add("proceso" & frmDefProducto.txtText1(0).Text & "/" & txtText1(7).Text, _
             tvwChild, "operaci" & frmDefProducto.txtText1(0).Text & "/" _
             & txtText1(7).Text & "/" & txtText1(4).Text, _
             txtText1(4).Text & ".-" & txtText1(5).Text)
        tvwItems(0).Nodes("operaci" & frmDefProducto.txtText1(0).Text & "/" & txtText1(7).Text & "/" & txtText1(4).Text).Selected = True
      End If
    
    'EN PRODUCTO
    Case "fraFrame1(3)"
      If tvwItems(0).Nodes("operaci" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text).Children > 0 Then
        i = tvwItems(0).Nodes("operaci" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text).Child.FirstSibling.Index
        aux = Right(tvwItems(0).Nodes(i).Key, Len(tvwItems(0).Nodes(i).Key) - InStr(tvwItems(0).Nodes(i).Key, "/"))
        aux = Right(aux, Len(aux) - InStr(aux, "/"))
        aux = CInt(Right(aux, Len(aux) - InStr(aux, "/")))
        aux2 = CInt(txtText1(13).Text)
        If aux > aux2 Then
          Call tvwItems(0).Nodes.Add(tvwItems(0).Nodes(i).Key, _
               tvwPrevious, "prodhij" & frmDefProducto.txtText1(0).Text & "/" & _
               txtText1(8).Text & "/" & txtText1(11).Text & "/" & _
               txtText1(13).Text, txtText1(13).Text & ".-" & txtText1(12).Text)
          tvwItems(0).Nodes("prodhij" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text & "/" & txtText1(13).Text).Selected = True
          Exit Sub
        ElseIf aux = aux2 Then
          Exit Sub
        End If
        
        While i <> tvwItems(0).Nodes("operaci" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text).Child.LastSibling.Index And aux < aux2
          If tvwItems(0).Nodes(i).Key = "prodhij" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text & "/" & txtText1(13).Text Then
          'If tvwItems(0).Nodes("operaci" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text).Child.Next.Key = "prodhij" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text & "/" & txtText1(13).Text Then
            Exit Sub
          End If
          i = tvwItems(0).Nodes(i).Next.Index
          aux = Right(tvwItems(0).Nodes(i).Key, Len(tvwItems(0).Nodes(i).Key) - InStr(tvwItems(0).Nodes(i).Key, "/"))
          aux = Right(aux, Len(aux) - InStr(aux, "/"))
          aux = CInt(Right(aux, Len(aux) - InStr(aux, "/")))
        Wend
        'no existe
        If tvwItems(0).Nodes(i).Key = "prodhij" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text & "/" & txtText1(13).Text Then
          Exit Sub
        End If
        EnPostWrite = True
        If aux < aux2 Then
          Call tvwItems(0).Nodes.Add(tvwItems(0).Nodes(i).Key, _
               tvwNext, "prodhij" & frmDefProducto.txtText1(0).Text & "/" & _
               txtText1(8).Text & "/" & txtText1(11).Text & "/" & _
               txtText1(13).Text, txtText1(13).Text & ".-" & txtText1(12).Text)
        Else
          Call tvwItems(0).Nodes.Add(tvwItems(0).Nodes(i).Key, _
               tvwPrevious, "prodhij" & frmDefProducto.txtText1(0).Text & "/" & _
               txtText1(8).Text & "/" & txtText1(11).Text & "/" & _
               txtText1(13).Text, txtText1(13).Text & ".-" & txtText1(12).Text)
        End If
        tvwItems(0).Nodes("prodhij" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text & "/" & txtText1(13).Text).Selected = True
        'tabGeneral1(1).Tab = 2
      Else 'no existe
        EnPostWrite = True
        Call tvwItems(0).Nodes.Add("operaci" & frmDefProducto.txtText1(0).Text & "/" & _
             txtText1(8).Text & "/" & txtText1(11).Text, _
             tvwChild, "prodhij" & frmDefProducto.txtText1(0).Text & "/" & _
             txtText1(8).Text & "/" & txtText1(11).Text & "/" & _
             txtText1(13).Text, txtText1(13).Text & ".-" & txtText1(12).Text)
        tvwItems(0).Nodes("prodhij" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text & "/" & txtText1(13).Text).Selected = True
        'Call tvwItems_Expand(0, tvwItems(0).Nodes("operaci" & frmDefProducto.txtText1(0).Text & "/" & txtText1(8).Text & "/" & txtText1(11).Text).Child.LastSibling)
        'tabGeneral1(1).Tab = 2
      End If
    End Select
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMasterInfo2 As New clsCWForm
  Dim objMasterInfo3 As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
                                
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strTable = "FR6900"
    
    Call .FormAddOrderField("FR69CODPROCFABRIC", cwAscending)
    
    .strWhere = "FR73CODPRODUCTO=" & frmDefProducto.txtText1(0).Text
   
    strKey = .strDataBase & .strTable
    'Call .FormCreateFilterWhere(strKey, "Proceso")
    'Call .FormAddFilterWhere(strKey, "FR69CODPROCFABRIC", "C�digo Proceso Fabricaci�n", cwNumeric)
    'Call .FormAddFilterOrder(strKey, "FR69CODPROCFABRIC", "C�digo Proceso Fabricaci�n")

  End With
  
  With objMasterInfo2
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(2)
    Set .grdGrid = grdDBGrid1(0)
    
    .strTable = "FR7000"
    
    Call .FormAddOrderField("FR70CODOPERACION", cwAscending)
    Call .FormAddRelation("FR69CODPROCFABRIC", txtText1(0))
    
    strKey = .strDataBase & .strTable
    'Call .FormCreateFilterWhere(strKey, "Operacion")
    'Call .FormAddFilterWhere(strKey, "FR70CODOPERACION", "C�digo Operacion", cwNumeric)
    'Call .FormAddFilterOrder(strKey, "FR70CODOPERACION", "C�digo Operacion")

  End With
  
  With objMasterInfo3
    Set .objFormContainer = fraFrame1(3)
    Set .objFatherContainer = fraFrame1(2)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    
    .strTable = "FR7100"
    
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
    Call .FormAddRelation("FR69CODPROCFABRIC", txtText1(7))
    Call .FormAddRelation("FR70CODOPERACION", txtText1(4))
    
    strKey = .strDataBase & .strTable
    'Call .FormCreateFilterWhere(strKey, "Prodhijo")
    'Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    'Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")

  End With
  
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMasterInfo2, cwFormDetail)
    Call .FormAddInfo(objMasterInfo3, cwFormDetail)
  
    Call .FormCreateInfo(objMasterInfo)
    
    
    
'    .CtrlGetInfo(txtText1(0)).blnInFind = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "FR73DESPRODUCTO")
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "FR69CODPROCFABRIC", "SELECT FR69DESPROCESO FROM FR6900 WHERE FR69CODPROCFABRIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(6), "FR69DESPROCESO")
    .CtrlGetInfo(txtText1(7)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(13)), "FR73CODPRODUCTO", "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(13)), txtText1(12), "FR73DESPRODUCTO")
    .CtrlGetInfo(txtText1(13)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR69CODPROCFABRIC", "SELECT FR69DESPROCESO FROM FR6900 WHERE FR69CODPROCFABRIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FR69DESPROCESO")
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(11)), "FR70CODOPERACION", "SELECT FR70DESOPERACION FROM FR7000 WHERE FR70CODOPERACION = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(11)), txtText1(10), "FR70DESOPERACION")
    .CtrlGetInfo(txtText1(11)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(16)), "FR08CODCENTRCOSTE", "SELECT FR08DESCENTRCOSTE FROM FR0800 WHERE FR08CODCENTRCOSTE = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(16)), txtText1(18), "FR08DESCENTRCOSTE")
    .CtrlGetInfo(txtText1(16)).blnForeign = True
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
  
  tabTab1(0).TabVisible(1) = False
  tabTab1(2).TabVisible(1) = False
  tabTab1(1).TabVisible(1) = False
  
  'tvwItems(0).ImageList = imlImagenes
  tvwItems(0).Visible = True
  Call rellenar_TreeView
  
  tabTab1(0).Caption = ""
  tabTab1(2).Caption = ""
  tabTab1(1).Caption = ""

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim dblTPPerdida As Double 'Para guardar el % de perdidas tecleado por el usuario
  Dim vntA As Variant
  
  'Se controla que % de p�rdidas no sea superior al 100%
  If txtText1(20).Text = "" Then
    dblTPPerdida = 0
  Else
    dblTPPerdida = txtText1(20).Text
  End If
  If (dblTPPerdida > 100) Or (dblTPPerdida < 0) Then
    Call objError.SetError(cwCodeMsg, "El % de p�rdidas no puede ser mayor que el 100% ni iferior al 0%", vntA)
    vntA = objError.Raise
    blnCancel = True
    Exit Sub
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabGeneral1_Click(Index As Integer, PreviousTab As Integer)

If Not Enarbol Then
  'raiz del arbol
  If InStr(tvwItems(0).SelectedItem.Key, "/") = 0 Then
    If (PreviousTab - tabGeneral1(1).Tab) = -1 Then
      If txtText1(0).Text = "" Then
        MsgBox "Debe crear antes un Proceso nuevo"
        tabGeneral1(1).Tab = 0
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        tabTab1(0).Enabled = True
        tabTab1(2).Enabled = False
        tabTab1(1).Enabled = False
        Exit Sub
      ElseIf txtText1(4).Text = "" Then
        If tabGeneral1(1).Tab = 2 Then
            MsgBox "Debe crear antes una Operacion nueva"
        End If
        Call tvwItems_Expand(0, tvwItems(0).SelectedItem.Child)
        tabTab1(0).Enabled = False
        tabTab1(2).Enabled = True
        tabTab1(1).Enabled = False
        tabGeneral1(1).Tab = 1
        Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
        Exit Sub
      Else
        tabTab1(0).Enabled = False
        tabTab1(2).Enabled = True
        tabTab1(1).Enabled = False
        Call tvwItems_Expand(0, tvwItems(0).SelectedItem.Child.Child)
      End If
    ElseIf (PreviousTab - tabGeneral1(1).Tab) = -2 Then
        If txtText1(0).Text = "" Then
          MsgBox "Debe crear antes un Proceso nuevo"
          tabTab1(0).Enabled = True
          tabTab1(2).Enabled = False
          tabTab1(1).Enabled = False
          tabGeneral1(1).Tab = 0
          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
          Exit Sub
        ElseIf txtText1(4).Text = "" Then
          MsgBox "Debe crear antes una Operacion nueva"
          Call tvwItems_Expand(0, tvwItems(0).SelectedItem.Child)
          tabTab1(0).Enabled = False
          tabTab1(2).Enabled = True
          tabTab1(1).Enabled = False
          tabGeneral1(1).Tab = 1
          Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
          Exit Sub
        ElseIf txtText1(13).Text = "" Then
            Call tvwItems_Expand(0, tvwItems(0).SelectedItem.Child.Child)
            tabTab1(0).Enabled = False
            tabTab1(2).Enabled = False
            tabTab1(1).Enabled = True
            tabGeneral1(1).Tab = 2
            Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
            Exit Sub
        Else
          tabTab1(0).Enabled = False
          tabTab1(2).Enabled = False
          tabTab1(1).Enabled = True
          Call tvwItems_Expand(0, tvwItems(0).SelectedItem.Child.Child.Child)
        End If
    End If
  'resto del arbol
  Else
    Select Case PreviousTab - tabGeneral1(1).Tab
    Case -2
      If txtText1(0).Text = "" Then
        MsgBox "Debe crear antes un Proceso nuevo"
        tabTab1(0).Enabled = True
        tabTab1(2).Enabled = False
        tabTab1(1).Enabled = False
        tabGeneral1(1).Tab = 0
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        Exit Sub
      ElseIf txtText1(4).Text = "" Then
        If tabGeneral1(1).Tab = 2 Then
            MsgBox "Debe crear antes una Operacion nueva"
        End If
        tabTab1(0).Enabled = False
        tabTab1(2).Enabled = True
        tabTab1(1).Enabled = False
        tabGeneral1(1).Tab = 1
        Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
        Exit Sub
      ElseIf txtText1(13).Text = "" Then
        tabTab1(0).Enabled = False
        tabTab1(2).Enabled = False
        tabTab1(1).Enabled = True
        tabGeneral1(1).Tab = 2
        Call tvwItems_Expand(0, tvwItems(0).SelectedItem.Child)
        Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
        Exit Sub
      Else
        tabTab1(0).Enabled = False
        tabTab1(2).Enabled = False
        tabTab1(1).Enabled = True
        Call tvwItems_Expand(0, tvwItems(0).SelectedItem.Child.Child)
      End If
    Case -1
      If PreviousTab = 0 Then
        If txtText1(0).Text = "" Then
          MsgBox "Debe crear antes un Proceso nuevo"
          tabTab1(0).Enabled = True
          tabTab1(2).Enabled = False
          tabTab1(1).Enabled = False
          tabGeneral1(1).Tab = 0
          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
          Exit Sub
        ElseIf txtText1(4).Text = "" Then
          tabTab1(0).Enabled = False
          tabTab1(2).Enabled = True
          tabTab1(1).Enabled = False
          tabGeneral1(1).Tab = 1
          Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
          Exit Sub
        Else
          tabTab1(0).Enabled = False
          tabTab1(2).Enabled = True
          tabTab1(1).Enabled = False
          Call tvwItems_Expand(0, tvwItems(0).SelectedItem.Child)
        End If
      Else
        If txtText1(4).Text = "" Then
          MsgBox "Debe crear antes una Operacion nueva"
          tabTab1(0).Enabled = False
          tabTab1(2).Enabled = True
          tabTab1(1).Enabled = False
          tabGeneral1(1).Tab = 1
          Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
          Exit Sub
        ElseIf txtText1(13).Text = "" Then
          tabTab1(0).Enabled = False
          tabTab1(2).Enabled = False
          tabTab1(1).Enabled = True
          tabGeneral1(1).Tab = 2
          Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
          Exit Sub
        Else
          tabTab1(0).Enabled = False
          tabTab1(2).Enabled = False
          tabTab1(1).Enabled = True
          Call tvwItems_Expand(0, tvwItems(0).SelectedItem.Child)
        End If
      End If
    Case 1
        If PreviousTab = 2 Then
          If txtText1(13).Text <> "" Then
            tabTab1(0).Enabled = False
            tabTab1(2).Enabled = True
            tabTab1(1).Enabled = False
            Call tvwItems_Collapse(0, tvwItems(0).SelectedItem.Parent)
          End If
        ElseIf PreviousTab = 1 Then
          If txtText1(4).Text <> "" Then
            tabTab1(0).Enabled = True
            tabTab1(2).Enabled = False
            tabTab1(1).Enabled = False
            Call tvwItems_Collapse(0, tvwItems(0).SelectedItem.Parent)
          End If
        End If
    Case 2
        If txtText1(13).Text = "" Then
          tabTab1(0).Enabled = False
          tabTab1(2).Enabled = True
          tabTab1(1).Enabled = False
          Call tvwItems_Collapse(0, tvwItems(0).SelectedItem.Parent)
        Else
          tabTab1(0).Enabled = True
          tabTab1(2).Enabled = False
          tabTab1(1).Enabled = False
          Call tvwItems_Collapse(0, tvwItems(0).SelectedItem.Parent.Parent)
        End If
    End Select
  End If

    Select Case tabGeneral1(1).Tab
    Case 0
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    Case 1
        Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
    Case 2
        Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
    End Select
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
      
Dim rsta As rdoResultset
Dim sqlstr As String
  
  Select Case btnButton.Index
  Case 2 'Nuevo
    Select Case objWinInfo.objWinActiveForm.strName
    Case "fraFrame1(0)"
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        sqlstr = "SELECT FR69CODPROCFABRIC_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
        'SendKeys "{TAB}", True
        rsta.Close
        Set rsta = Nothing
        Call objWinInfo.CtrlSet(txtText1(2), frmDefProducto.txtText1(0).Text)
        txtText1(0).SetFocus
        Call objWinInfo.CtrlGotFocus
        Call objWinInfo.CtrlLostFocus
        Exit Sub
    Case "fraFrame1(2)"
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        sqlstr = "SELECT FR70CODOPERACION_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        Call objWinInfo.CtrlSet(txtText1(4), rsta.rdoColumns(0).Value)
        'SendKeys "{TAB}", True
        rsta.Close
        Set rsta = Nothing
        txtText1(4).SetFocus
        Call objWinInfo.CtrlGotFocus
        Call objWinInfo.CtrlLostFocus
        Exit Sub
    Case "fraFrame1(3)"
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        'SendKeys "{TAB}", True
        txtText1(13).SetFocus
        Call objWinInfo.CtrlGotFocus
        Exit Sub
    End Select
  Case 3 'Abrir
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Enabrir = True
    Select Case objWinInfo.objWinActiveForm.strName
    Case "fraFrame1(0)"
      Call objWinInfo.CtrlSet(txtText1(2), frmDefProducto.txtText1(0).Text)
    Case "fraFrame1(2)"
      Call objWinInfo.CtrlSet(txtText1(7), txtText1(0).Text)
    Case "fraFrame1(3)"
      Call objWinInfo.CtrlSet(txtText1(8), txtText1(0).Text)
      Call objWinInfo.CtrlSet(txtText1(11), txtText1(4).Text)
    End Select
    Exit Sub
  Case 8 'Borrar
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Select Case objWinInfo.objWinActiveForm.strName
    Case "fraFrame1(0)"
      If txtText1(0).Text = "" Then
        tvwItems(0).Nodes.Remove (tvwItems(0).SelectedItem.Index)
      End If
    Case "fraFrame1(2)"
      If txtText1(4).Text = "" Then
        tvwItems(0).Nodes.Remove (tvwItems(0).SelectedItem.Index)
      End If
    Case "fraFrame1(3)"
      If txtText1(13).Text = "" Then
        tvwItems(0).Nodes.Remove (tvwItems(0).SelectedItem.Index)
      End If
    End Select
    Exit Sub
  Case 30 'salir
    'comprobar hijos
    If Comprobar_hijos() = False Then
      Exit Sub
    End If
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  Case 21 'Primero
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    If Left(tvwItems(0).SelectedItem.Key, 7) = "product" Then
      If tvwItems(0).SelectedItem.Children > 0 Then
        tvwItems(0).SelectedItem.Child.FirstSibling.Selected = True
      End If
    Else
      tvwItems(0).SelectedItem.FirstSibling.Selected = True
    End If
    Exit Sub
  Case 22 'Anterior
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    If Left(tvwItems(0).SelectedItem.Key, 7) = "product" Then
      If tvwItems(0).SelectedItem.Children > 0 Then
        tvwItems(0).SelectedItem.Child.FirstSibling.Selected = True
      End If
    Else
      If tvwItems(0).SelectedItem.FirstSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Previous.Selected = True
      End If
    End If
    Exit Sub
  Case 23 'Siguiente
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    If Left(tvwItems(0).SelectedItem.Key, 7) = "product" Then
      Select Case tvwItems(0).SelectedItem.Children
      Case 0
      Case 1
        tvwItems(0).SelectedItem.Child.FirstSibling.Selected = True
      Case Else
        tvwItems(0).SelectedItem.Child.FirstSibling.Selected = True
        tvwItems(0).SelectedItem.Next.Selected = True
      End Select
    Else
      If tvwItems(0).SelectedItem.LastSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Next.Selected = True
      End If
    End If
    Exit Sub
  Case 24 'Ultimo
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    If Left(tvwItems(0).SelectedItem.Key, 7) = "product" Then
      If tvwItems(0).SelectedItem.Children > 0 Then
        tvwItems(0).SelectedItem.Child.LastSibling.Selected = True
      End If
    Else
      tvwItems(0).SelectedItem.LastSibling.Selected = True
    End If
    Exit Sub
  Case Else 'Otro boton
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  End Select
  
'objWinInfo.cllWinForms("fraFrame1(0)").objFilter.strWhere = ""
'objWinInfo.cllWinForms("fraFrame1(2)").objFilter.strWhere = ""
'objWinInfo.cllWinForms("fraFrame1(3)").objFilter.strWhere = ""

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub tvwItems_Collapse(Index As Integer, ByVal Node As ComctlLib.Node)

        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)

End Sub

Private Sub tvwItems_Expand(Index As Integer, ByVal Node As ComctlLib.Node)

  If EnPostWrite = True Then
    EnPostWrite = False
    Exit Sub
  End If

  Node.EnsureVisible
  Node.Selected = True
  Call tvwItems_NodeClick(0, Node)

End Sub

'Private Sub tvwItems_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'  Set nodX = tvwItems(0).SelectedItem ' Establece el elemento arrastrado.
'End Sub

Private Sub tvwItems_NodeClick(Index As Integer, ByVal Node As ComctlLib.Node)
    
Dim aux As Variant
Dim aux2 As Variant

Enarbol = True

aux2 = Me.MousePointer
Me.MousePointer = vbHourglass
    Select Case Left(Node.Key, 7)
        Case "product":
          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
          Call objWinInfo.DataMoveFirst
          tabTab1(0).Enabled = True
          tabTab1(2).Enabled = False
          tabTab1(1).Enabled = False
          tabGeneral1(1).Tab = 0
        Case "proceso":
          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
          aux = CInt(Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/")))
          Call objWinInfo.DataRefreshGoFirst
          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR69CODPROCFABRIC").Value <> aux
            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
          Wend
          tabTab1(0).Enabled = True
          tabTab1(2).Enabled = False
          tabTab1(1).Enabled = False
          tabGeneral1(1).Tab = 0
        Case "operaci":
          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
          aux = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
          aux = CInt(Left(aux, InStr(aux, "/") - 1))
          Call objWinInfo.DataRefreshGoFirst
          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR69CODPROCFABRIC").Value <> aux
            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
          Wend
          Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
          aux = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
          aux = CInt(Right(aux, Len(aux) - InStr(aux, "/")))
          Call objWinInfo.DataRefreshGoFirst
          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR70CODOPERACION").Value <> aux
            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
          Wend
          tabTab1(0).Enabled = False
          tabTab1(2).Enabled = True
          tabTab1(1).Enabled = False
          tabGeneral1(1).Tab = 1
        Case "prodhij":
          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
          aux = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
          aux = CInt(Left(aux, InStr(aux, "/") - 1))
          Call objWinInfo.DataRefreshGoFirst
          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR69CODPROCFABRIC").Value <> aux
            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
          Wend
          
          Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
          aux = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
          aux = Right(aux, Len(aux) - InStr(aux, "/"))
          aux = CInt(Left(aux, InStr(aux, "/") - 1))
          Call objWinInfo.DataRefreshGoFirst
          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR70CODOPERACION").Value <> aux
            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
          Wend
          
          Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
          aux = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
          aux = Right(aux, Len(aux) - InStr(aux, "/"))
          aux = CInt(Right(aux, Len(aux) - InStr(aux, "/")))
          Call objWinInfo.DataRefreshGoFirst
          Call objWinInfo.objWinActiveForm.rdoCursor.MoveFirst
          While objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FR73CODPRODUCTO").Value <> aux
            Call objWinInfo.objWinActiveForm.rdoCursor.MoveNext
          Wend
          
          tabTab1(0).Enabled = False
          tabTab1(2).Enabled = False
          tabTab1(1).Enabled = True
          tabGeneral1(1).Tab = 2
    End Select
Me.MousePointer = aux2

Enarbol = False

Call objWinInfo.DataRefresh

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)

  Dim objField As clsCWFieldSearch
  Dim objSearch As clsCWSearch
  
  If strCtrl = "txtText1(2)" Or strCtrl = "txtText1(13)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
         
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"
         
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n Producto"
         
     If .Search Then
        If strCtrl = "txtText1(2)" Then
            Call objWinInfo.CtrlSet(txtText1(2), .cllValues("FR73CODPRODUCTO"))
        Else
            Call objWinInfo.CtrlSet(txtText1(13), .cllValues("FR73CODPRODUCTO"))
        End If
     End If
   End With
   Set objSearch = Nothing
 End If

  If strCtrl = "txtText1(7)" Or strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR6900"
     .strOrder = "ORDER BY FR69CODPROCFABRIC ASC"
         
     Set objField = .AddField("FR69CODPROCFABRIC")
     objField.strSmallDesc = "C�digo Proceso fabricaci�n"
         
     Set objField = .AddField("FR69DESPROCESO")
     objField.strSmallDesc = "Descripci�n Proceso"
         
     If .Search Then
        If strCtrl = "txtText1(7)" Then
            Call objWinInfo.CtrlSet(txtText1(7), .cllValues("FR69CODPROCFABRIC"))
        Else
            Call objWinInfo.CtrlSet(txtText1(8), .cllValues("FR69CODPROCFABRIC"))
        End If
     End If
   End With
   Set objSearch = Nothing
 End If

  If strCtrl = "txtText1(11)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7000"
     .strOrder = "ORDER BY FR70CODOPERACION ASC"
         
     Set objField = .AddField("FR70CODOPERACION")
     objField.strSmallDesc = "C�digo Operaci�n"
         
     Set objField = .AddField("FR70DESOPERACION")
     objField.strSmallDesc = "Descripci�n Operaci�n"
         
     If .Search Then
        Call objWinInfo.CtrlSet(txtText1(11), .cllValues("FR70CODOPERACION"))
     End If
   End With
   Set objSearch = Nothing
 End If

 If strCtrl = "txtText1(16)" Then
   Set objSearch = New clsCWSearch
   With objSearch
    .strTable = "FR0800"
    .strOrder = "ORDER BY FR08CODCENTRCOSTE ASC"
        
    Set objField = .AddField("FR08CODCENTRCOSTE")
    objField.strSmallDesc = "C�digo Centro Coste"
        
    Set objField = .AddField("FR08DESCENTRCOSTE")
    objField.strSmallDesc = "Descripci�n Centro Coste"
        
    If .Search Then
       Call objWinInfo.CtrlSet(txtText1(16), .cllValues("FR08CODCENTRCOSTE"))
    End If
  End With
  Set objSearch = Nothing
 End If

End Sub

