VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMantObservFarma 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MANTENIMIENTO FARMACIA. Observaciones Farmacia."
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   17
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Observaciones Farmacia."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7455
      Index           =   0
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   11445
      Begin TabDlg.SSTab tabTab1 
         Height          =   6900
         Index           =   0
         Left            =   240
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   480
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   12171
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0027.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(5)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(7)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(8)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(9)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(10)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(11)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(12)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(13)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(14)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(15)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(16)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblLabel1(17)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblLabel1(18)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "lblLabel1(19)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "lblLabel1(20)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "lblLabel1(21)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "lblLabel1(22)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "lblLabel1(23)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "lblLabel1(24)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "dtcDateCombo1(1)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "dtcDateCombo1(0)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(0)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(1)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(4)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(7)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtText1(2)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(3)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtText1(5)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "txtText1(6)"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "chkCheck1(0)"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "chkCheck1(1)"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "chkCheck1(2)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "chkCheck1(3)"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).Control(39)=   "txtText1(9)"
         Tab(0).Control(39).Enabled=   0   'False
         Tab(0).Control(40)=   "txtText1(8)"
         Tab(0).Control(40).Enabled=   0   'False
         Tab(0).Control(41)=   "chkCheck1(4)"
         Tab(0).Control(41).Enabled=   0   'False
         Tab(0).Control(42)=   "txtText1(10)"
         Tab(0).Control(42).Enabled=   0   'False
         Tab(0).Control(43)=   "txtText1(11)"
         Tab(0).Control(43).Enabled=   0   'False
         Tab(0).Control(44)=   "txtText1(12)"
         Tab(0).Control(44).Enabled=   0   'False
         Tab(0).Control(45)=   "txtText1(13)"
         Tab(0).Control(45).Enabled=   0   'False
         Tab(0).Control(46)=   "txtText1(15)"
         Tab(0).Control(46).Enabled=   0   'False
         Tab(0).Control(47)=   "txtText1(16)"
         Tab(0).Control(47).Enabled=   0   'False
         Tab(0).Control(48)=   "txtText1(17)"
         Tab(0).Control(48).Enabled=   0   'False
         Tab(0).Control(49)=   "txtText1(18)"
         Tab(0).Control(49).Enabled=   0   'False
         Tab(0).Control(50)=   "txtText1(19)"
         Tab(0).Control(50).Enabled=   0   'False
         Tab(0).Control(51)=   "txtText1(14)"
         Tab(0).Control(51).Enabled=   0   'False
         Tab(0).ControlCount=   52
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0027.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   14
            Left            =   7680
            TabIndex        =   56
            TabStop         =   0   'False
            Top             =   3120
            Width           =   2800
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   19
            Left            =   8040
            ScrollBars      =   2  'Vertical
            TabIndex        =   54
            Tag             =   "Apellido 2�"
            Top             =   3840
            Width           =   2355
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   18
            Left            =   5520
            ScrollBars      =   2  'Vertical
            TabIndex        =   52
            Tag             =   "apellido 1�"
            Top             =   3840
            Width           =   2355
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   17
            Left            =   3000
            ScrollBars      =   2  'Vertical
            TabIndex        =   50
            Tag             =   "Nombre"
            Top             =   3840
            Width           =   2355
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   16
            Left            =   1680
            ScrollBars      =   2  'Vertical
            TabIndex        =   48
            Tag             =   "Historia"
            Top             =   3840
            Width           =   900
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   15
            Left            =   360
            ScrollBars      =   2  'Vertical
            TabIndex        =   46
            Tag             =   "Persona"
            Top             =   3840
            Width           =   900
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   13
            Left            =   1680
            TabIndex        =   45
            TabStop         =   0   'False
            Top             =   3120
            Width           =   2800
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRG4CODFRECUENCIA"
            Height          =   330
            Index           =   12
            Left            =   4800
            ScrollBars      =   2  'Vertical
            TabIndex        =   43
            Tag             =   "Frecuencia"
            Top             =   3120
            Width           =   2700
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR34CODVIA"
            Height          =   330
            Index           =   11
            Left            =   1200
            ScrollBars      =   2  'Vertical
            TabIndex        =   41
            Tag             =   "V�a"
            Top             =   3120
            Width           =   350
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR57DOSIS"
            Height          =   330
            Index           =   10
            Left            =   360
            ScrollBars      =   2  'Vertical
            TabIndex        =   39
            Tag             =   "Dosis"
            Top             =   3120
            Width           =   612
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "FR57INDABIERTO"
            Height          =   255
            Index           =   4
            Left            =   6000
            TabIndex        =   37
            Tag             =   "Indicador Abierto"
            Top             =   5160
            Width           =   255
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   8
            Left            =   3000
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "Nombre Persona"
            Top             =   5760
            Width           =   3495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   9
            Left            =   3000
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Descripci�n Tipo Observaci�n"
            Top             =   6360
            Width           =   3495
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "FR57INDOMEC"
            Height          =   255
            Index           =   3
            Left            =   3360
            TabIndex        =   11
            Tag             =   "Indicador O.M.E.C."
            Top             =   5160
            Width           =   255
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "FR57INDNECESQUIR"
            Height          =   255
            Index           =   2
            Left            =   6000
            TabIndex        =   10
            Tag             =   "Indicador Necesidad Quir�fano"
            Top             =   4560
            Width           =   255
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "FR57INDPEDFARM"
            Height          =   255
            Index           =   1
            Left            =   3360
            TabIndex        =   9
            Tag             =   "Indicador Pedido Farmacia"
            Top             =   4560
            Width           =   255
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "FR57INDOMPRN"
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   8
            Tag             =   "Indicador O.M./PRN"
            Top             =   5160
            Width           =   255
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR27CODTIPOBSER"
            Height          =   330
            Index           =   6
            Left            =   360
            TabIndex        =   15
            Tag             =   "C�digo Tipo Observaci�n|C�digo Tipo Observaci�n"
            Top             =   6360
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "SG02COD_VAL"
            Height          =   330
            Index           =   5
            Left            =   360
            TabIndex        =   13
            Tag             =   "C�digo Persona Valida|C�digo Persona Valida"
            Top             =   5760
            Width           =   630
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR57NUMLINEA"
            Height          =   330
            Index           =   3
            Left            =   360
            ScrollBars      =   2  'Vertical
            TabIndex        =   5
            Tag             =   "N�mero L�nea OF|N�mero L�nea OF"
            Top             =   2400
            Width           =   600
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR57OBSERVACION"
            Height          =   885
            Index           =   2
            Left            =   360
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   4
            Tag             =   "Observaci�n|Observaci�n"
            Top             =   1200
            Width           =   8760
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   7
            Left            =   4560
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Descripci�n Producto"
            Top             =   480
            Width           =   3495
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR57CODPETOBSER"
            Height          =   330
            Index           =   4
            Left            =   360
            TabIndex        =   12
            Tag             =   "C�digo Petici�n Observada|C�digo Petici�n Observada"
            Top             =   4560
            Width           =   990
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            Index           =   1
            Left            =   2880
            ScrollBars      =   2  'Vertical
            TabIndex        =   2
            Tag             =   "C�digo Producto|C�digo Producto"
            Top             =   480
            Width           =   960
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR57CODOBSFARM"
            Height          =   330
            Index           =   0
            Left            =   360
            TabIndex        =   1
            Tag             =   "C�digo Observaci�n|C�digo Observaci�n"
            Top             =   480
            Width           =   990
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6345
            Index           =   0
            Left            =   -74760
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   240
            Width           =   10215
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18018
            _ExtentY        =   11192
            _StockProps     =   79
            Caption         =   "OBSERVACIONES FARMACIA"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR57FECFINVIG"
            Height          =   330
            Index           =   0
            Left            =   2040
            TabIndex        =   6
            Tag             =   "Fecha Fin Vigencia OF"
            Top             =   2400
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR57FECINIVIG"
            Height          =   330
            Index           =   1
            Left            =   4440
            TabIndex        =   7
            Tag             =   "Fecha Fin Vigencia OF"
            Top             =   2400
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 2�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   24
            Left            =   8040
            TabIndex        =   55
            Top             =   3600
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 1�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   23
            Left            =   5520
            TabIndex        =   53
            Top             =   3600
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   22
            Left            =   3000
            TabIndex        =   51
            Top             =   3600
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   21
            Left            =   1680
            TabIndex        =   49
            Top             =   3600
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   20
            Left            =   360
            TabIndex        =   47
            Top             =   3600
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Frecuencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   19
            Left            =   4800
            TabIndex        =   44
            Top             =   2880
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "V�a"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   18
            Left            =   1200
            TabIndex        =   42
            Top             =   2880
            Width           =   615
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   17
            Left            =   360
            TabIndex        =   40
            Top             =   2880
            Width           =   615
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Indicador Abierto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   6000
            TabIndex        =   38
            Top             =   4920
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Tipo Observaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   3000
            TabIndex        =   36
            Top             =   6120
            Width           =   2775
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   3000
            TabIndex        =   35
            Top             =   5520
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   4560
            TabIndex        =   34
            Top             =   240
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Tipo Observaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   360
            TabIndex        =   33
            Top             =   6120
            Width           =   2295
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Persona Valida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   360
            TabIndex        =   32
            Top             =   5520
            Width           =   1935
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Petici�n Observada"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   360
            TabIndex        =   31
            Top             =   4320
            Width           =   2415
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Indicador O.M.E.C."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   3360
            TabIndex        =   30
            Top             =   4920
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Indicador Necesidad Quir�fano"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   6000
            TabIndex        =   29
            Top             =   4320
            Width           =   2655
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Indicador Pedido Farmacia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   3360
            TabIndex        =   28
            Top             =   4320
            Width           =   2295
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Indicador O.M./PRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   360
            TabIndex        =   27
            Top             =   4920
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Inicio Vigencia OF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   4440
            TabIndex        =   26
            Top             =   2160
            Width           =   2295
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Fin Vigencia OF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   2040
            TabIndex        =   25
            Top             =   2160
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N�mero L�nea OF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   360
            TabIndex        =   24
            Top             =   2160
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Observaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   360
            TabIndex        =   23
            Top             =   960
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   2880
            TabIndex        =   21
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Observaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   20
            Top             =   240
            Width           =   1815
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   22
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMantObservFarma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmMantObservFarma (FR0027.FRM)                              *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: 19 DE AGOSTO DE 1998                                          *
'* DESCRIPCION: mantenimiento Observaciones Farmacia                    *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim gintIndice As Integer



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
Dim mensaje As String
  If intIndex = 0 Then
    If dtcDateCombo1(1).Text <> "" And dtcDateCombo1(0).Text <> "" Then
      If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) > 0 Then
          mensaje = MsgBox("La fecha de Fin de Vigencia es menor que la de Inicio", vbInformation, "Aviso")
          dtcDateCombo1(0).Text = ""
      End If
    End If
  End If
  If intIndex = 1 Then
    If dtcDateCombo1(1).Text <> "" And dtcDateCombo1(0).Text <> "" Then
      If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) > 0 Then
          mensaje = MsgBox("La fecha de Inicio de Vigencia es mayor que la de Fin", vbInformation, "Aviso")
          dtcDateCombo1(1).Text = ""
      End If
    End If
  End If
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
Dim mensaje As String
  If intIndex = 0 Then
    If dtcDateCombo1(1).Text <> "" And dtcDateCombo1(0).Text <> "" Then
      If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) > 0 Then
          mensaje = MsgBox("La fecha de Fin de Vigencia es menor que la de Inicio", vbInformation, "Aviso")
          dtcDateCombo1(0).Text = ""
      End If
    End If
  End If
  If intIndex = 1 Then
    If dtcDateCombo1(1).Text <> "" And dtcDateCombo1(0).Text <> "" Then
      If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) > 0 Then
          mensaje = MsgBox("La fecha de Inicio de Vigencia es mayor que la de Fin", vbInformation, "Aviso")
          dtcDateCombo1(1).Text = ""
      End If
    End If
  End If
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Observaciones Farmacia"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "FR5700"
    .blnAskPrimary = False
    
    Call .FormAddOrderField("FR57CODOBSFARM", cwAscending)
    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Observaciones Farmacia")
    Call .FormAddFilterWhere(strKey, "FR57CODOBSFARM", "C�digo Observaci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR57OBSERVACION", "Observaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FR57NUMLINEA", "N�mero L�nea OF", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR57FECFINVIG", "Fecha Fin Vigencia OF", cwDate)
    Call .FormAddFilterWhere(strKey, "FR57FECINIVIG", "Fecha Inicio Vigencia OF", cwDate)
    Call .FormAddFilterWhere(strKey, "FR57INDOMPRN", "Indicador O.M./PRN", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR57INDPEDFARM", "Indicador Pedido Farmacia", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR57INDNECESQUIR", "Indicador Necesidad Quir�fano", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR57INDOMEC", "Indicador O.M.E.C.", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR57CODPETOBSER", "C�digo Petici�n Observada", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_VAL", "C�digo Persona Valida", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR27CODTIPOBSER", "C�digo Tipo Observaci�n", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "FR57CODOBSFARM", "C�digo Observaci�n")
    Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
    Call .FormAddFilterOrder(strKey, "FR57OBSERVACION", "Observaci�n")
    Call .FormAddFilterOrder(strKey, "FR57NUMLINEA", "N�mero L�nea OF")
    Call .FormAddFilterOrder(strKey, "FR57FECFINVIG", "Fecha Fin Vigencia OF")
    Call .FormAddFilterOrder(strKey, "FR57FECINIVIG", "Fecha Inicio Vigencia OF")
    Call .FormAddFilterOrder(strKey, "FR57INDOMPRN", "Indicador O.M./PRN")
    Call .FormAddFilterOrder(strKey, "FR57INDPEDFARM", "Indicador Pedido Farmacia")
    Call .FormAddFilterOrder(strKey, "FR57INDNECESQUIR", "Indicador Necesidad Quir�fano")
    Call .FormAddFilterOrder(strKey, "FR57INDOMEC", "Indicador O.M.E.C.")
    Call .FormAddFilterOrder(strKey, "FR57CODPETOBSER", "C�digo Petici�n Observada")
    Call .FormAddFilterOrder(strKey, "SG02COD_VAL", "C�digo Persona Valida")
    Call .FormAddFilterOrder(strKey, "FR27CODTIPOBSER", "C�digo Tipo Observaci�n")
 
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
    
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    .CtrlGetInfo(txtText1(2)).blnMandatory = True

    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    .CtrlGetInfo(txtText1(5)).blnForeign = True
    .CtrlGetInfo(txtText1(6)).blnForeign = True
    .CtrlGetInfo(txtText1(15)).blnForeign = True
    .CtrlGetInfo(txtText1(12)).blnForeign = True
    .CtrlGetInfo(txtText1(11)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR73CODPRODUCTO", "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(7), "FR73DESPRODUCTO")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "SG02COD_VAL", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(8), "SG02APE1")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "FR27CODTIPOBSER", "SELECT FR27DESTIPOBSER FROM FR2700 WHERE FR27CODTIPOBSER = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(9), "FR27DESTIPOBSER")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(15)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(15)), txtText1(16), "CI22NUMHISTORIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(15)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(15)), txtText1(17), "CI22NOMBRE")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(15)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(15)), txtText1(18), "CI22PRIAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(15)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(15)), txtText1(19), "CI22SEGAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(12)), "FRG4CODFRECUENCIA", "SELECT * FROM FRG400 WHERE FRG4CODFRECUENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(12)), txtText1(14), "FRG4DESFRECUENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(11)), "FR34CODVIA", "SELECT * FROM FR3400 WHERE FR34CODVIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(11)), txtText1(13), "FR34DESVIA")

    Call .WinRegister
    Call .WinStabilize
  End With

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)

  If gintIndice = 6 Then
    Call objsecurity.LaunchProcess("FR0008")
  ElseIf gintIndice = 11 Then
    Call objsecurity.LaunchProcess("FR0009")
  ElseIf gintIndice = 12 Then
    Call objsecurity.LaunchProcess("FR0057")
  ElseIf gintIndice = 1 Then
    Call objsecurity.LaunchProcess("FR0035")
  End If

End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)

  Dim objField As clsCWFieldSearch
  Dim objSearch As clsCWSearch
  
     If strCtrl = "txtText1(1)" Then
       Set objSearch = New clsCWSearch
       With objSearch
        .strTable = "FR7300"
        .strOrder = "ORDER BY FR73CODPRODUCTO ASC"
            
        Set objField = .AddField("FR73CODPRODUCTO")
        objField.strSmallDesc = "C�digo Producto"
            
        Set objField = .AddField("FR73DESPRODUCTO")
        objField.strSmallDesc = "Descripci�n Producto"
            
        If .Search Then
         Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR73CODPRODUCTO"))
        End If
      End With
      Set objSearch = Nothing
    End If
  
   If strCtrl = "txtText1(5)" Then
     Set objSearch = New clsCWSearch
     With objSearch
      .strTable = "SG0200"
      .strOrder = "ORDER BY SG02COD ASC"
          
      Set objField = .AddField("SG02COD")
      objField.strSmallDesc = "C�digo Persona Valida"
          
      Set objField = .AddField("SG02APE1")
      objField.strSmallDesc = "Nombre Persona"
          
      If .Search Then
       Call objWinInfo.CtrlSet(txtText1(5), .cllValues("SG02COD"))
      End If
    End With
    Set objSearch = Nothing
  End If
  
  If strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR2700"
     .strOrder = "ORDER BY FR27CODTIPOBSER ASC"
         
     Set objField = .AddField("FR27CODTIPOBSER")
     objField.strSmallDesc = "C�digo Tipo Observaci�n"
         
     Set objField = .AddField("FR27DESTIPOBSER")
     objField.strSmallDesc = "Descripci�n Tipo Observaci�n"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(6), .cllValues("FR27CODTIPOBSER"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strCtrl = "txtText1(11)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR3400"
     .strOrder = "ORDER BY FR34CODVIA ASC"
         
     Set objField = .AddField("FR34CODVIA")
     objField.strSmallDesc = "C�digo V�a"
         
     Set objField = .AddField("FR34DESVIA")
     objField.strSmallDesc = "Descripci�n V�a"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(11), .cllValues("FR34CODVIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strCtrl = "txtText1(12)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRG400"
     .strOrder = "ORDER BY FRG4CODFRECUENCIA ASC"
         
     Set objField = .AddField("FRG4CODFRECUENCIA")
     objField.strSmallDesc = "C�digo Frecuencia"
         
     Set objField = .AddField("FRG4DESFRECUENCIA")
     objField.strSmallDesc = "Descripci�n Frecuencia"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(12), .cllValues("FRG4CODFRECUENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strCtrl = "txtText1(15)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "CI2200"
     .strOrder = "ORDER BY CI21CODPERSONA ASC"
         
     Set objField = .AddField("CI21CODPERSONA")
     objField.strSmallDesc = "C�digo Persona"
         
     Set objField = .AddField("CI22NUMHISTORIA")
     objField.strSmallDesc = "Historia"
     
     Set objField = .AddField("CI22NOMBRE")
     objField.strSmallDesc = "Nombre"
     
     Set objField = .AddField("CI22PRIAPEL")
     objField.strSmallDesc = "Apellido 1�"
     
     Set objField = .AddField("CI22SEGAPEL")
     objField.strSmallDesc = "Apellido 2�"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(15), .cllValues("CI21CODPERSONA"))
     End If
   End With
   Set objSearch = Nothing
 End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String

  If btnButton.Index = 2 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR57CODOBSFARM_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
'    SendKeys ("{TAB}")
    rsta.Close
    Set rsta = Nothing
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String

  If intIndex = 10 Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    sqlstr = "SELECT FR57CODOBSFARM_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
'    SendKeys ("{TAB}")
    rsta.Close
    Set rsta = Nothing
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  gintIndice = intIndex
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

