VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMantInventario 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MANTENIMIENTO FARMACIA. Inventario"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   21
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Inventario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7575
      Index           =   0
      Left            =   120
      TabIndex        =   20
      Top             =   480
      Width           =   11565
      Begin TabDlg.SSTab tabTab1 
         Height          =   6945
         Index           =   0
         Left            =   240
         TabIndex        =   22
         TabStop         =   0   'False
         Tag             =   "FR04CODALMACEN"
         Top             =   480
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   12250
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0064.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(4)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(8)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(9)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(11)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(12)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(14)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(15)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(16)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(17)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(3)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(7)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(8)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(9)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(10)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(11)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(13)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(14)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(15)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(16)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(17)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(18)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "Frame1"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "Frame2"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).ControlCount=   27
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0064.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.Frame Frame2 
            Height          =   975
            Left            =   360
            TabIndex        =   45
            Top             =   2880
            Width           =   10335
            Begin VB.TextBox txtexistactuales 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Left            =   6960
               Locked          =   -1  'True
               TabIndex        =   53
               Tag             =   "Existencias Actuales"
               Top             =   480
               Width           =   1572
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR47ACUENTRA"
               Height          =   330
               Index           =   2
               Left            =   2520
               TabIndex        =   49
               Tag             =   "Acumulado Entradas"
               Top             =   480
               Width           =   1572
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR47EXISTINI"
               Height          =   330
               Index           =   4
               Left            =   240
               TabIndex        =   48
               Tag             =   "Existencias Iniciales"
               Top             =   480
               Width           =   1572
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR47ACUSALIDA"
               Height          =   330
               Index           =   6
               Left            =   4680
               TabIndex        =   47
               Tag             =   "Acumulado Salidas"
               Top             =   480
               Width           =   1572
            End
            Begin VB.CommandButton cmdrellenar 
               Caption         =   "Actualizar "
               Height          =   330
               Left            =   9000
               TabIndex        =   46
               Top             =   480
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Existencias Actuales"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   6960
               TabIndex        =   54
               Top             =   240
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Acumulado Entradas"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   2520
               TabIndex        =   52
               Tag             =   "Acumulado Entradas"
               Top             =   240
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Existencias Iniciales"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   240
               TabIndex        =   51
               Top             =   240
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Acumulado Salidas"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   4680
               TabIndex        =   50
               Tag             =   "Acumulado Salidas"
               Top             =   240
               Width           =   1695
            End
         End
         Begin VB.Frame Frame1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1935
            Left            =   360
            TabIndex        =   37
            Top             =   840
            Width           =   10335
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   23
               Left            =   8280
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Unidad de Medida"
               Top             =   1320
               Width           =   900
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   22
               Left            =   6840
               TabIndex        =   7
               TabStop         =   0   'False
               Tag             =   "Dosis"
               Top             =   1320
               Width           =   1000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   21
               Left            =   4800
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Forma Farmace�tica"
               Top             =   1320
               Width           =   540
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   20
               Left            =   1680
               TabIndex        =   5
               TabStop         =   0   'False
               Tag             =   "Referencia"
               Top             =   1320
               Width           =   2700
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   19
               Left            =   240
               TabIndex        =   4
               TabStop         =   0   'False
               Tag             =   "C�digo Nacional"
               Top             =   1320
               Width           =   1080
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   1
               Left            =   240
               ScrollBars      =   2  'Vertical
               TabIndex        =   2
               Tag             =   "C�digo Almac�n"
               Top             =   600
               Width           =   1092
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   1680
               TabIndex        =   3
               TabStop         =   0   'False
               Tag             =   "Descripci�n Producto"
               Top             =   600
               Width           =   7200
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Unidad de Medida"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   8280
               TabIndex        =   44
               Top             =   1080
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   21
               Left            =   6840
               TabIndex        =   43
               Top             =   1080
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Forma Farmace�tica"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   20
               Left            =   4800
               TabIndex        =   42
               Top             =   1080
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Referencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   19
               Left            =   1680
               TabIndex        =   41
               Top             =   1080
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.Nacional"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   240
               TabIndex        =   40
               Top             =   1080
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   13
               Left            =   1680
               TabIndex        =   39
               Top             =   360
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   240
               TabIndex        =   38
               Top             =   360
               Width           =   1215
            End
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR47TPMAXERROR"
            Height          =   330
            Index           =   18
            Left            =   6000
            TabIndex        =   16
            Tag             =   "% Error M�ximo"
            Top             =   6360
            Width           =   1572
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR47STOCKPREFEREN"
            Height          =   330
            Index           =   17
            Left            =   8760
            TabIndex        =   17
            Tag             =   "Stock Preferente"
            Top             =   4920
            Visible         =   0   'False
            Width           =   1572
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR47UBICACION"
            Height          =   330
            Index           =   16
            Left            =   360
            TabIndex        =   15
            Tag             =   "Ubicaci�n"
            Top             =   6360
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR47ESPACIODISPON"
            Height          =   330
            Index           =   15
            Left            =   8760
            TabIndex        =   18
            Tag             =   "Espacio Disponible"
            Top             =   5520
            Visible         =   0   'False
            Width           =   1572
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR47ESPACIOALMTOTAL"
            Height          =   330
            Index           =   14
            Left            =   8760
            TabIndex        =   19
            Tag             =   "_Espacio total de almacenamiento"
            Top             =   6120
            Visible         =   0   'False
            Width           =   1452
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR47CANTPEDPROV"
            Height          =   330
            Index           =   13
            Left            =   360
            TabIndex        =   14
            Tag             =   "Pedido a Proveedores"
            Top             =   4920
            Width           =   1572
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR47STOCKMAX"
            Height          =   330
            Index           =   11
            Left            =   2880
            TabIndex        =   10
            Tag             =   "Stock M�ximo"
            Top             =   4200
            Width           =   1572
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR47STOCKMIN"
            Height          =   330
            Index           =   10
            Left            =   360
            TabIndex        =   9
            Tag             =   "Stock M�nimo"
            Top             =   4200
            Width           =   1572
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR47PRECSTD"
            Height          =   330
            Index           =   9
            Left            =   5520
            TabIndex        =   13
            Tag             =   "Precio Est�ndar"
            Top             =   5640
            Width           =   1452
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR47PRECULTENTR"
            Height          =   330
            Index           =   8
            Left            =   2880
            TabIndex        =   12
            Tag             =   "Precio �ltima Entrada"
            Top             =   5640
            Width           =   1452
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR47PRECMEDCOMP"
            Height          =   330
            Index           =   7
            Left            =   360
            TabIndex        =   11
            Tag             =   "Precio Medio de Compra"
            Top             =   5640
            Width           =   1452
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   3
            Left            =   840
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "Descripci�n Almac�n"
            Top             =   360
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR04CODALMACEN"
            Height          =   330
            Index           =   0
            Left            =   360
            TabIndex        =   0
            Tag             =   "C�digo Almac�n"
            Top             =   360
            Width           =   372
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5985
            Index           =   0
            Left            =   -74880
            TabIndex        =   23
            TabStop         =   0   'False
            Top             =   240
            Width           =   9735
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17171
            _ExtentY        =   10557
            _StockProps     =   79
            Caption         =   "INVENTARIO"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "% Error M�ximo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   17
            Left            =   6000
            TabIndex        =   36
            Top             =   6120
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Stock Preferente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   8760
            TabIndex        =   35
            Top             =   4680
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Ubicaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   360
            TabIndex        =   34
            Top             =   6120
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Espacio Disponible"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   8760
            TabIndex        =   33
            Top             =   5280
            Visible         =   0   'False
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Espacio total de almacenamiento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   8760
            TabIndex        =   32
            Top             =   5880
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Pedido a Proveedores"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   360
            TabIndex        =   31
            Top             =   4680
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Stock M�ximo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   2880
            TabIndex        =   30
            Top             =   3960
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Stock M�nimo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   360
            TabIndex        =   29
            Top             =   3960
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Precio Est�ndar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   5520
            TabIndex        =   28
            Top             =   5400
            Width           =   2415
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Precio �ltima Entrada"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   2880
            TabIndex        =   27
            Top             =   5400
            Width           =   2415
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Precio Medio de Compra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   360
            TabIndex        =   26
            Tag             =   "Acumulado Salidas"
            Top             =   5400
            Width           =   2415
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Almac�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   24
            Top             =   120
            Width           =   1815
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   25
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMantInventario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmMantInventario (FR0064.FRM)                               *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: ENERO DE 1999                                                 *
'* DESCRIPCION: mantenimiento de Inventario                             *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim intmantenimiento As Integer


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub cmdrellenar_Click()

Dim existencias_actuales
Dim cantidades_movidas
Dim existencias_iniciales
Dim rst47 As rdoResultset
Dim str47 As String
Dim rst35max As rdoResultset
Dim str35max As String
Dim rst35 As rdoResultset
Dim str35 As String
Dim rst80max As rdoResultset
Dim str80max As String
Dim rst80 As rdoResultset
Dim str80 As String
Dim entrada
Dim salida

cmdrellenar.Enabled = False


If txtText1(0).Text <> "" And txtText1(1).Text <> "" Then

'existencias iniciales=existencias_actuales-cantidades_movidas
str47 = "SELECT FR47ACUENTRA,FR47ACUSALIDA FROM FR4700 WHERE " & _
      "FR04CODALMACEN=" & txtText1(0).Text & _
      " AND FR73CODPRODUCTO=" & txtText1(1).Text
Set rst47 = objApp.rdoConnect.OpenResultset(str47)
If Not rst47.EOF Then
    If Not IsNull(rst47.rdoColumns("FR47ACUENTRA").Value) And _
      Not IsNull(rst47.rdoColumns("FR47ACUSALIDA").Value) Then
        existencias_actuales = rst47.rdoColumns("FR47ACUENTRA").Value - rst47.rdoColumns("FR47ACUSALIDA").Value
    End If
    If IsNull(rst47.rdoColumns("FR47ACUENTRA").Value) And _
      Not IsNull(rst47.rdoColumns("FR47ACUSALIDA").Value) Then
        existencias_actuales = 0 - rst47.rdoColumns("FR47ACUSALIDA").Value
    End If
    If Not IsNull(rst47.rdoColumns("FR47ACUENTRA").Value) And _
      IsNull(rst47.rdoColumns("FR47ACUSALIDA").Value) Then
        existencias_actuales = rst47.rdoColumns("FR47ACUENTRA").Value
    End If
    If IsNull(rst47.rdoColumns("FR47ACUENTRA").Value) And _
       IsNull(rst47.rdoColumns("FR47ACUSALIDA").Value) Then
        existencias_actuales = 0
    End If
Else
    existencias_actuales = 0
End If
rst47.Close
Set rst47 = Nothing



str35max = "SELECT count(*) FROM FR3500 WHERE " & _
            "FR04CODALMACEN_DES=" & txtText1(0).Text & _
            " AND FR73CODPRODUCTO=" & txtText1(1).Text & _
            " AND FR90CODTIPMOV=11"
Set rst35max = objApp.rdoConnect.OpenResultset(str35max)
If rst35max.rdoColumns(0).Value > 0 Then
    str35max = "SELECT TO_CHAR(MAX(FR35FECMOVIMIENTO)) FROM FR3500 WHERE " & _
                "FR04CODALMACEN_DES=" & txtText1(0).Text & _
                " AND FR73CODPRODUCTO=" & txtText1(1).Text & _
                " AND FR90CODTIPMOV=11"
    Set rst35max = objApp.rdoConnect.OpenResultset(str35max)
    If Not rst35max.EOF Then
        str35 = "SELECT FR35UNIENT FROM FR3500 WHERE " & _
            "FR04CODALMACEN_DES=" & txtText1(0).Text & _
            " AND FR73CODPRODUCTO=" & txtText1(1).Text & _
            " AND FR90CODTIPMOV=11" & _
            " AND FR35FECMOVIMIENTO=" & "'" & rst35max.rdoColumns(0).Value & "'"
        Set rst35 = objApp.rdoConnect.OpenResultset(str35)
        If Not rst35.EOF Then
            entrada = rst35.rdoColumns(0).Value
        Else
            entrada = 0
        End If
        rst35.Close
        Set rst35 = Nothing
    End If
Else
    entrada = 0
End If
rst35max.Close
Set rst35max = Nothing


str80max = "SELECT count(*) FROM FR8000 WHERE " & _
            "FR04CODALMACEN_ORI=" & txtText1(0).Text & _
            " AND FR73CODPRODUCTO=" & txtText1(1).Text & _
            " AND FR90CODTIPMOV=11"
Set rst80max = objApp.rdoConnect.OpenResultset(str80max)
If Not rst80max.EOF Then
    str80max = "SELECT TO_CHAR(MAX(FR80FECMOVIMIENTO)) FROM FR8000 WHERE " & _
                "FR04CODALMACEN_ORI=" & txtText1(0).Text & _
                " AND FR73CODPRODUCTO=" & txtText1(1).Text & _
                " AND FR90CODTIPMOV=11"
    Set rst80max = objApp.rdoConnect.OpenResultset(str80max)
    If Not rst80max.EOF Then
        str80 = "SELECT FR80UNISALEN FROM FR8000 WHERE " & _
            "FR04CODALMACEN_DES=" & txtText1(0).Text & _
            " AND FR73CODPRODUCTO=" & txtText1(1).Text & _
            " AND FR90CODTIPMOV=11" & _
            " AND FR80FECMOVIMIENTO=" & "'" & rst80max.rdoColumns(0).Value & "'"
        Set rst80 = objApp.rdoConnect.OpenResultset(str80)
        If Not rst80.EOF Then
            salida = rst80.rdoColumns(0).Value
        Else
            salida = 0
        End If
        rst80.Close
        Set rst80 = Nothing
    End If
Else
    salida = 0
End If
rst80max.Close
Set rst80max = Nothing

cantidades_movidas = entrada - salida
Call objWinInfo.CtrlSet(txtText1(4), existencias_actuales - cantidades_movidas)
'acumulado de entradas
Call objWinInfo.CtrlSet(txtText1(2), entrada)
'acumulado de salidas
Call objWinInfo.CtrlSet(txtText1(6), salida)
'existencias actuales
Call objWinInfo.CtrlSet(txtexistactuales, txtText1(2).Text - txtText1(6).Text)
objWinInfo.objWinActiveForm.blnChanged = True
tlbToolbar1.Buttons(4).Enabled = True

End If
cmdrellenar.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String

  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Inventario"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "FR4700"
    
    .blnHasMaint = True
    
    Call .FormAddOrderField("FR04CODALMACEN", cwAscending)
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Inventario")
    Call .FormAddFilterWhere(strKey, "FR04CODALMACEN", "C�digo Almac�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47EXISTINI", "Existencia Inicial", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47ACUENTRA", "Acumulado Entradas", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47ACUSALIDA", "Acumulado Salidas", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47PRECMEDCOMP", "Precio Medio de Compra", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47PRECULTENTR", "Precio �ltima Entrada", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47PRECSTD", "Precio Est�ndar", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47STOCKMIN", "Stock M�nimo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47STOCKMAX", "Stock M�ximo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47PTOPED", "Punto de Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47CANTPEDPROV", "Pedido a Proveedores", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR47ESPACIODISPON", "Espacio Disponible", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR47ESPACIOALMTOTAL", "Espacio Total Almacenamiento", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47UBICACION", "Ubicaci�n", cwString)
    'Call .FormAddFilterWhere(strKey, "FR47STOCKPREFEREN", "Stock Preferente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR47TPMAXERROR", "% Error M�ximo", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "FR47PRECSTD", "Precio Est�ndar")
    Call .FormAddFilterOrder(strKey, "FR47PRECMEDCOMP", "Precio Medio de Compra")
    Call .FormAddFilterOrder(strKey, "FR47PRECULTENTR", "Precio �ltima Entrada")
    Call .FormAddFilterOrder(strKey, "FR47STOCKMIN", "Stock M�nimo")
    Call .FormAddFilterOrder(strKey, "FR47STOCKMAX", "Stock M�ximo")
    Call .FormAddFilterOrder(strKey, "FR47EXISTINI", "Existencia Inicial")
 
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
    
    'Se indica que campos son obligatorios y cuales son clave primaria
    '.CtrlGetInfo(txtText1(0)).intKeyNo = 1
    '.CtrlGetInfo(txtText1(1)).blnMandatory = True

    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(18)).blnInFind = True
    
    .CtrlGetInfo(txtText1(0)).blnForeign = True
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    
    .CtrlGetInfo(txtText1(7)).blnReadOnly = True
    .CtrlGetInfo(txtText1(8)).blnReadOnly = True
    .CtrlGetInfo(txtText1(9)).blnReadOnly = True
    .CtrlGetInfo(txtText1(4)).blnReadOnly = True
    .CtrlGetInfo(txtText1(2)).blnReadOnly = True
    .CtrlGetInfo(txtText1(6)).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(14)).blnInGrid = False
    .CtrlGetInfo(txtText1(15)).blnInGrid = False
    .CtrlGetInfo(txtText1(17)).blnInGrid = False
    
    .CtrlGetInfo(txtexistactuales).blnNegotiated = False
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR04CODALMACEN", "SELECT * FROM FR0400 WHERE FR04CODALMACEN = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(3), "FR04DESALMACEN")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(5), "FR73DESPRODUCTO")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(19), "FR73CODNAC")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(20), "FR73REFERENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(21), "FRH7CODFORMFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(22), "FR73DOSIS")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(23), "FR93CODUNIMEDIDA")

    Call .WinRegister
    Call .WinStabilize
  End With

    txtexistactuales.Locked = True
    txtexistactuales.BackColor = -2147483633
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)

  Dim objField As clsCWFieldSearch
  Dim objSearch As clsCWSearch
  
  If strCtrl = "txtText1(0)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR0400"
     .strOrder = "ORDER BY FR04DESALMACEN ASC"
    
     Set objField = .AddField("FR04CODALMACEN")
     objField.strSmallDesc = "C�digo Almac�n"
         
     Set objField = .AddField("FR04DESALMACEN")
     objField.strSmallDesc = "Descripci�n Almac�n"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(0), .cllValues("FR04CODALMACEN"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strOrder = "ORDER BY FR73DESPRODUCTO ASC"
    
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"
         
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n Producto"
     
     Set objField = .AddField("FR73CODNAC")
     objField.strSmallDesc = "C�digo Nacional"
     
     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"
     
     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"
     
     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"
     
     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Unidad de Medida"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If

End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)

If intmantenimiento = 1 Then
    Call objsecurity.LaunchProcess("FR0035")
ElseIf intmantenimiento = 0 Then
    Call objsecurity.LaunchProcess("FR0004")
End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    'existencias_actuales=acumulado_entradas - acumulado_salidas
    If txtText1(2).Text <> "" And txtText1(6).Text <> "" Then
        Call objWinInfo.CtrlSet(txtexistactuales, txtText1(2).Text - txtText1(6).Text)
    End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim sqlstr As String

  If txtText1(0).Text <> "" Then
    sqlstr = "SELECT * FROM FR0400 WHERE FR04CODALMACEN=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If rsta.EOF = True Then
      MsgBox "El C�d.Almac�n es incorrecto.", vbExclamation
      blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
  End If
  If txtText1(1).Text <> "" Then
    sqlstr = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(1).Text
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If rsta.EOF = True Then
      MsgBox "El C�d.Producto es incorrecto.", vbExclamation
      blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
   If btnButton.Index = 2 Or btnButton.Index = 3 Then
      Me.txtText1(1).SetFocus
   End If
   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
   If btnButton.Index = 2 Or btnButton.Index = 3 Then
      Me.txtText1(0).SetFocus
   End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  intmantenimiento = intIndex
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
Dim rsta As rdoResultset
Dim stra As String
Dim rstserv As rdoResultset
Dim strserv As String
Dim rstalmacen As rdoResultset
Dim stralmacen As String
Dim almacendeFarmacia As Boolean

Call objWinInfo.CtrlLostFocus
almacendeFarmacia = False
'cuando el producto pierde el foco
If intIndex = 1 Then
    If txtText1(1).Text <> "" And txtText1(0).Text <> "" Then
        'se mira si el almac�n pertenece al servicio de Farmacia
        strserv = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=3"
        Set rstserv = objApp.rdoConnect.OpenResultset(strserv)
        stralmacen = "SELECT COUNT(*) FROM FR0400 " & _
                   "WHERE FR04CODALMACEN=" & txtText1(0).Text & _
                   " AND AD02CODDPTO=" & rstserv.rdoColumns(0).Value
        Set rstalmacen = objApp.rdoConnect.OpenResultset(stralmacen)
        If rstalmacen.rdoColumns(0).Value > 0 Then 'el almac�n pertenece a Farmacia
            almacendeFarmacia = True
        End If
        rstalmacen.Close
        Set rstalmacen = Nothing
        rstserv.Close
        Set rstserv = Nothing
        
        'se rellena el precio medio,precio �ltima entrada y precio est�ndar
        stra = "SELECT FR73PRECMED,FR73PREULTENT,FR73PRECESTD," & _
               "FRH9UBICACION,FR73STOCKMAX,FR73STOCKMIN,FR73PTOPED," & _
               "FR73CANTPEND,FR73PTEBONIF " & _
               "FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(1).Text
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        If Not rsta.EOF Then
            'precio medio de compra
            If Not IsNull(rsta.rdoColumns("FR73PRECMED").Value) Then
                Call objWinInfo.CtrlSet(txtText1(7), rsta.rdoColumns("FR73PRECMED").Value)
            Else
                Call objWinInfo.CtrlSet(txtText1(7), 0)
            End If
            'precio �ltima entrada
            If Not IsNull(rsta.rdoColumns("FR73PREULTENT").Value) Then
                Call objWinInfo.CtrlSet(txtText1(8), rsta.rdoColumns("FR73PREULTENT").Value)
            Else
                Call objWinInfo.CtrlSet(txtText1(8), 0)
            End If
            'precio est�ndar
            If Not IsNull(rsta.rdoColumns("FR73PRECESTD").Value) Then
                Call objWinInfo.CtrlSet(txtText1(9), rsta.rdoColumns("FR73PRECESTD").Value)
            Else
                Call objWinInfo.CtrlSet(txtText1(9), 0)
            End If
            Call cmdrellenar_Click
            
            'si el almac�n pertenece al servicio de Farmacia se rellena el resto de campos
            If almacendeFarmacia = True Then
                'Ubicaci�n
                If Not IsNull(rsta.rdoColumns("FRH9UBICACION").Value) Then
                    Call objWinInfo.CtrlSet(txtText1(16), rsta.rdoColumns("FRH9UBICACION").Value)
                Else
                   Call objWinInfo.CtrlSet(txtText1(16), "")
                End If
                objWinInfo.CtrlGetInfo(txtText1(16)).blnReadOnly = True
                txtText1(16).Locked = True
                txtText1(16).BackColor = &HC0C0C0
                'Stock M�ximo
                If Not IsNull(rsta.rdoColumns("FR73STOCKMAX").Value) Then
                    Call objWinInfo.CtrlSet(txtText1(11), rsta.rdoColumns("FR73STOCKMAX").Value)
                Else
                   Call objWinInfo.CtrlSet(txtText1(11), 0)
                End If
                objWinInfo.CtrlGetInfo(txtText1(11)).blnReadOnly = True
                txtText1(11).Locked = True
                txtText1(11).BackColor = &HC0C0C0
                'Stock M�nimo
                If Not IsNull(rsta.rdoColumns("FR73STOCKMIN").Value) Then
                    Call objWinInfo.CtrlSet(txtText1(10), rsta.rdoColumns("FR73STOCKMIN").Value)
                Else
                   Call objWinInfo.CtrlSet(txtText1(10), 0)
                End If
                objWinInfo.CtrlGetInfo(txtText1(10)).blnReadOnly = True
                txtText1(10).Locked = True
                txtText1(10).BackColor = &HC0C0C0
                'Pedido a Proveedores
                If Not IsNull(rsta.rdoColumns("FR73CANTPEND").Value) And _
                   Not IsNull(rsta.rdoColumns("FR73PTEBONIF").Value) Then
                    Call objWinInfo.CtrlSet(txtText1(13), rsta.rdoColumns("FR73CANTPEND").Value + rsta.rdoColumns("FR73PTEBONIF").Value)
                End If
                If Not IsNull(rsta.rdoColumns("FR73CANTPEND").Value) And _
                   IsNull(rsta.rdoColumns("FR73PTEBONIF").Value) Then
                    Call objWinInfo.CtrlSet(txtText1(13), rsta.rdoColumns("FR73CANTPEND").Value)
                End If
                If IsNull(rsta.rdoColumns("FR73CANTPEND").Value) And _
                   Not IsNull(rsta.rdoColumns("FR73PTEBONIF").Value) Then
                    Call objWinInfo.CtrlSet(txtText1(13), rsta.rdoColumns("FR73PTEBONIF").Value)
                End If
                If IsNull(rsta.rdoColumns("FR73CANTPEND").Value) And _
                   IsNull(rsta.rdoColumns("FR73PTEBONIF").Value) Then
                    Call objWinInfo.CtrlSet(txtText1(13), "")
                End If
                objWinInfo.CtrlGetInfo(txtText1(13)).blnReadOnly = True
                txtText1(13).Locked = True
                txtText1(13).BackColor = &HC0C0C0
            End If
        End If
        rsta.Close
        Set rsta = Nothing
    End If
End If
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

