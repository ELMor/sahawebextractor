VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmBloqueoLinea 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Bloqueo de L�neas."
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdDesbloqueo 
      Caption         =   "Desbloquear"
      Height          =   375
      Left            =   4920
      TabIndex        =   29
      Top             =   7560
      Width           =   1695
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "L�neas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6735
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   11565
      Begin TabDlg.SSTab tabTab1 
         Height          =   6060
         Index           =   0
         Left            =   240
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   480
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   10689
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0105.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(78)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(79)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(3)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(4)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(6)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtcDateCombo1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "dtcDateCombo1(1)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "chkCheck1(3)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(3)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(2)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "chkCheck1(2)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "chkCheck1(1)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "chkCheck1(0)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(0)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(1)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(4)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(5)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(6)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(7)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(8)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).ControlCount=   24
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0105.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            DataField       =   "SG02COD_VAL"
            Height          =   330
            Index           =   8
            Left            =   3120
            TabIndex        =   28
            Tag             =   "C�digo Petici�n"
            Top             =   600
            Visible         =   0   'False
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR57CODOBSFARM"
            Height          =   330
            Index           =   7
            Left            =   3120
            TabIndex        =   27
            Tag             =   "C�digo Petici�n"
            Top             =   120
            Visible         =   0   'False
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR57OBSERVACION"
            Height          =   1890
            Index           =   6
            Left            =   360
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   25
            Tag             =   "Observaciones"
            Top             =   3960
            Width           =   9840
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   5
            Left            =   2640
            TabIndex        =   23
            Tag             =   "Descripci�n Tipo Obsevaci�n"
            Top             =   3000
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR27CODTIPOBSER"
            Height          =   330
            Index           =   4
            Left            =   360
            TabIndex        =   21
            Tag             =   "C�digo Tipo Observaci�n"
            Top             =   3000
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR57NUMLINEA"
            Height          =   330
            Index           =   1
            Left            =   1920
            TabIndex        =   12
            Tag             =   "N�mero L�nea"
            Top             =   720
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR57CODPETOBSER"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   11
            Tag             =   "C�digo Petici�n"
            Top             =   720
            Width           =   1100
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "OM/PRN"
            DataField       =   "FR57INDOMPRN"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   4680
            TabIndex        =   10
            Tag             =   "OM/PRN?"
            Top             =   360
            Width           =   2415
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "O.M.E.C."
            DataField       =   "FR57INDOMEC"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   4680
            TabIndex        =   9
            Tag             =   "O.M.E.C.?"
            Top             =   600
            Width           =   2055
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Pedido Farmacia"
            DataField       =   "FR57INDPEDFARM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   7320
            TabIndex        =   8
            Tag             =   "Pedido Farmacia?"
            Top             =   360
            Width           =   2175
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            Index           =   2
            Left            =   240
            TabIndex        =   7
            Tag             =   "C�digo Producto"
            Top             =   1440
            Width           =   1100
         End
         Begin VB.TextBox txtText1 
            Height          =   810
            Index           =   3
            Left            =   1920
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   6
            Tag             =   "Descripci�n Producto"
            Top             =   1440
            Width           =   6015
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Necesidad Quir�fano"
            DataField       =   "FR57INDNECESQUIR"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   7320
            TabIndex        =   5
            Tag             =   "Necesidad Quir�fano?"
            Top             =   600
            Width           =   2415
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5505
            Index           =   0
            Left            =   -74760
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   240
            Width           =   10335
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18230
            _ExtentY        =   9710
            _StockProps     =   79
            Caption         =   "BLOQUEADA"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR57FECINIVIG"
            Height          =   330
            Index           =   1
            Left            =   8400
            TabIndex        =   13
            Tag             =   "Fecha Inicio Vigencia OF"
            Top             =   1320
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR57FECFINVIG"
            Height          =   330
            Index           =   0
            Left            =   8400
            TabIndex        =   14
            Tag             =   "Fecha Inicio Vigencia OF"
            Top             =   1920
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   360
            TabIndex        =   26
            Top             =   3720
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Desc. Tipo Observaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   2640
            TabIndex        =   24
            Top             =   2760
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Tipo Observaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   360
            TabIndex        =   22
            Top             =   2760
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N�m. L�nea"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   79
            Left            =   1920
            TabIndex        =   20
            Top             =   480
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cod. Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   78
            Left            =   240
            TabIndex        =   19
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cod. Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   18
            Top             =   1200
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   1920
            TabIndex        =   17
            Top             =   1200
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Inicio Vigencia OF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   8400
            TabIndex        =   16
            Top             =   1080
            Width           =   2295
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Fin Vigencia OF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   8400
            TabIndex        =   15
            Top             =   1680
            Width           =   2055
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmBloqueoLinea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmBloqueoLinea (FR0105.FRM)                                 *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Bloqueo de L�neas                                       *
'* ARGUMENTOS:  vntdata(1)=fr57codpetobserv                             *
'*              vntdata(2)=fr57numlinea                                 *
'*              vntdata(3)=fr57indomprn                                 *
'*              vntdata(4)=fr57indomec                                  *
'*              vntdata(5)=fr57indnecesquir                             *
'*              vntdata(6)=fr57fecinivig                                *
'*              vntdata(7)=fr57indpedfarm                               *
'*              vntdata(8)=fr73codproducto                              *
'*              vntdata(9)=fr57fecfinvig                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************


Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdDesbloqueo_Click()
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub Form_Activate()
        txtText1(2).SetFocus
        SendKeys "{TAB}"
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim strupdate As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Bloqueadas"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "FR5700"
    .strWhere = "FR57CODPETOBSER=" & gvntdatafr0105(1) & " AND FR57NUMLINEA=" & gvntdatafr0105(2)
    
    Call .FormAddOrderField("FR57CODOBSFARM", cwAscending)
    
    '.blnHasMaint = True
  
    strKey = .strDataBase & .strTable
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
    
    ''Se indica que campos son obligatorios y cuales son clave primaria
    '    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    '    .CtrlGetInfo(txtText1(1)).blnMandatory = True

    .CtrlGetInfo(txtText1(0)).blnReadOnly = True
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True
    .CtrlGetInfo(txtText1(2)).blnReadOnly = True
    .CtrlGetInfo(chkCheck1(0)).blnReadOnly = True
    .CtrlGetInfo(chkCheck1(1)).blnReadOnly = True
    .CtrlGetInfo(chkCheck1(2)).blnReadOnly = True
    .CtrlGetInfo(chkCheck1(3)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True

    .CtrlGetInfo(txtText1(4)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "FR27CODTIPOBSER", "SELECT * FROM FR2700 WHERE FR27CODTIPOBSER = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(5), "FR27DESTIPOBSER")
    
    Call .WinRegister
    Call .WinStabilize
  End With

    If txtText1(7).Text = "" Then
        strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=-1 where FR66CODPETICION=" & gvntdatafr0105(1) & _
                    " AND FR28NUMLINEA=" & gvntdatafr0105(2)
        objApp.rdoConnect.Execute strupdate, 64
        sqlstr = "SELECT FR57CODOBSFARM_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        objWinInfo.DataNew
        txtText1(7).Text = rsta(0).Value
        txtText1(0).Text = gvntdatafr0105(1)
        txtText1(1).Text = gvntdatafr0105(2)
        txtText1(2).Text = gvntdatafr0105(8)
        chkCheck1(0).Value = gvntdatafr0105(3)
        chkCheck1(1).Value = gvntdatafr0105(4)
        chkCheck1(2).Value = gvntdatafr0105(5)
        chkCheck1(3).Value = gvntdatafr0105(7)
        txtText1(8).Text = objsecurity.strUser
        If gvntdatafr0105(9) <> "null" Then
            dtcDateCombo1(0).Date = gvntdatafr0105(9)
        End If
        If gvntdatafr0105(6) <> "null" Then
            dtcDateCombo1(1).Date = gvntdatafr0105(6)
        End If
        objWinInfo.CtrlGotFocus
        objWinInfo.CtrlLostFocus
        rsta.Close
        Set rsta = Nothing
    End If
  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Tipos de Interacci�n" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch

  
  If strCtrl = "txtText1(4)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR2700"
     
     Set objField = .AddField("FR27CODTIPOBSER")
     objField.strSmallDesc = "Tipo Observaci�n"
         
     Set objField = .AddField("FR27DESTIPOBSER")
     objField.strSmallDesc = "Descripci�n Tipo Observaci�n"
     
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(4), .cllValues("FR27CODTIPOBSER"))
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Dim intpreg As Integer
    Dim strupdate As String
    Dim rsta As rdoResultset
    Dim sqlstr As String
    
    Select Case btnButton.Index
        Case 8:
            intpreg = MsgBox("�Est� seguro de que desea Desbloquear esta l�nea?", vbQuestion + vbYesNo)
            If intpreg = vbNo Then
                Exit Sub
            End If
            strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=0 where FR66CODPETICION=" & gvntdatafr0105(1) & _
                    " AND FR28NUMLINEA=" & gvntdatafr0105(2)
            objApp.rdoConnect.Execute strupdate, 64
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
        Case 30:
            If txtText1(7).Text <> "" Then
                sqlstr = "SELECT * from FR5700 WHERE FR57CODOBSFARM=" & txtText1(7).Text
                Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
                If rsta.EOF = True Then
                    strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=0 where FR66CODPETICION=" & gvntdatafr0105(1) & _
                    " AND FR28NUMLINEA=" & gvntdatafr0105(2)
                    objApp.rdoConnect.Execute strupdate, 64
                End If
                rsta.Close
                Set rsta = Nothing
            End If
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        Case Else:
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End Select
    
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Dim intpreg As Integer
    Dim strupdate As String
    Dim rsta As rdoResultset
    Dim sqlstr As String
    
    Select Case intIndex
        Case 60:
            intpreg = MsgBox("�Est� seguro de que desea Desbloquear esta l�nea?", vbQuestion + vbYesNo)
            If intpreg = vbNo Then
                Exit Sub
            End If
            strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=0 where FR66CODPETICION=" & gvntdatafr0105(1) & _
                    " AND FR28NUMLINEA=" & gvntdatafr0105(2)
            objApp.rdoConnect.Execute strupdate, 64
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
        Case 100:
            If txtText1(7).Text <> "" Then
                sqlstr = "SELECT * from FR5700 WHERE FR57CODOBSFARM=" & txtText1(7).Text
                Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
                If rsta.EOF = True Then
                    strupdate = "UPDATE FR2800 SET FR28INDBLOQUEADA=0 where FR66CODPETICION=" & gvntdatafr0105(1) & _
                    " AND FR28NUMLINEA=" & gvntdatafr0105(2)
                    objApp.rdoConnect.Execute strupdate, 64
                End If
                rsta.Close
                Set rsta = Nothing
            End If
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        Case Else:
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End Select
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

