VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmAnalizDiferenc 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Dispensar Dosis Unitaria. Analizar Diferencias"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdanalizar 
      Caption         =   "Analizar Diferencias"
      Height          =   255
      Left            =   5040
      TabIndex        =   21
      Top             =   4200
      Width           =   2175
   End
   Begin VB.CommandButton cmdtransdatos 
      Caption         =   "Transmitir Datos al Lector"
      Height          =   255
      Left            =   5040
      TabIndex        =   20
      Top             =   7800
      Width           =   2175
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Carros"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3615
      Index           =   1
      Left            =   120
      TabIndex        =   12
      Top             =   480
      Width           =   11580
      Begin TabDlg.SSTab tabTab1 
         Height          =   3135
         Index           =   0
         Left            =   240
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   360
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   5530
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0139.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(7)"
         Tab(0).Control(1)=   "txtText1(6)"
         Tab(0).Control(2)=   "txtText1(5)"
         Tab(0).Control(3)=   "txtText1(2)"
         Tab(0).Control(4)=   "txtText1(1)"
         Tab(0).Control(5)=   "txtText1(0)"
         Tab(0).Control(6)=   "txtText1(4)"
         Tab(0).Control(7)=   "txtText1(3)"
         Tab(0).Control(8)=   "lblLabel1(6)"
         Tab(0).Control(9)=   "lblLabel1(1)"
         Tab(0).Control(10)=   "lblLabel1(3)"
         Tab(0).Control(11)=   "lblLabel1(0)"
         Tab(0).Control(12)=   "lblLabel1(4)"
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0139.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74HORASALIDA"
            Height          =   330
            Index           =   7
            Left            =   -73440
            TabIndex        =   7
            Tag             =   "Hora"
            Top             =   2640
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR74DIA"
            Height          =   330
            Index           =   6
            Left            =   -74640
            TabIndex        =   6
            Tag             =   "D�a"
            Top             =   2640
            Width           =   500
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR87DESESTCARRO"
            Height          =   330
            Index           =   5
            Left            =   -74040
            TabIndex        =   5
            Tag             =   "Estado Carro"
            Top             =   1920
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR87CODESTCARRO"
            Height          =   330
            Index           =   2
            Left            =   -74640
            TabIndex        =   4
            Tag             =   "C�s.Estado Carro"
            Top             =   1920
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD02DESDPTO"
            Height          =   330
            Index           =   1
            Left            =   -74040
            TabIndex        =   1
            Tag             =   "Servicio"
            Top             =   480
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   0
            Left            =   -74640
            TabIndex        =   0
            Tag             =   "C�d.Servicio"
            Top             =   480
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR07DESCARRO"
            Height          =   330
            Index           =   4
            Left            =   -74040
            TabIndex        =   3
            Tag             =   "Carro"
            Top             =   1200
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR07CODCARRO"
            Height          =   330
            Index           =   3
            Left            =   -74640
            TabIndex        =   2
            Tag             =   "C�d.Carro"
            Top             =   1200
            Width           =   400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3105
            Index           =   2
            Left            =   0
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   0
            Width           =   11055
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19500
            _ExtentY        =   5477
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Hora Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   -73440
            TabIndex        =   19
            Top             =   2400
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "D�a Salida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -74640
            TabIndex        =   18
            Top             =   2400
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   -74640
            TabIndex        =   17
            Top             =   1680
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -74640
            TabIndex        =   16
            Top             =   240
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   -74640
            TabIndex        =   15
            Top             =   960
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Diferencias"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3120
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   4560
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2625
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   11385
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   20082
         _ExtentY        =   4630
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmAnalizDiferenc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmAnalizDiferenc (FR0139.FRM)                               *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: analizar diferencias                                    *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim Insertar_en_el_Carro As Boolean

Private Sub Insertar_en_Carro(codfrecuencia As String)

Dim strfrec As String
Dim rstfrec As rdoResultset
Dim strfecha As String
Dim rstfecha As rdoResultset
Dim diasemana As Integer
Dim strcarro As String
Dim rstcarro As rdoResultset
Dim strhora As String
Dim rsthora As rdoResultset
Dim existe_otro_carro As Boolean
Dim listacarros() As String
Dim i As Integer
Dim lista As String
Dim numsalidas As Integer
Dim meter_en_carro As Boolean
Dim numfrecuencias As Integer
Dim arrayfrec() As Boolean
Dim j As Integer
Dim cont As Integer

existe_otro_carro = False
meter_en_carro = False

'se sacan las horas a las que debe darse el producto
strfrec = "SELECT count(*) FROM FRG500 WHERE FRG4CODFRECUENCIA=" & _
            "'" & codfrecuencia & "'"
Set rstfrec = objApp.rdoConnect.OpenResultset(strfrec)
numfrecuencias = rstfrec.rdoColumns(0).Value
ReDim arrayfrec(numfrecuencias)
rstfrec.Close
Set rstfrec = Nothing

'se sacan las horas a las que debe darse el producto
strfrec = "SELECT FRG5HORA FROM FRG500 WHERE FRG4CODFRECUENCIA=" & _
            "'" & codfrecuencia & "'"
Set rstfrec = objApp.rdoConnect.OpenResultset(strfrec)

'se saca a qu� hora hay salida hoy de carros
strcarro = "SELECT FR07CODCARRO FROM FR0700 WHERE AD02CODDPTO=" & _
            txtText1(0).Text
Set rstcarro = objApp.rdoConnect.OpenResultset(strcarro)
'se llena una lista con los c�digos de carro
lista = "("
If Not rstcarro.EOF Then
    lista = lista & rstcarro.rdoColumns("FR07CODCARRO").Value
    rstcarro.MoveNext
End If
While Not rstcarro.EOF
    lista = lista & "," & rstcarro.rdoColumns("FR07CODCARRO").Value
rstcarro.MoveNext
Wend
lista = lista & ")"
rstcarro.Close
Set rstcarro = Nothing
strhora = "SELECT count(*) FROM FR7400 WHERE " & _
            "FR07CODCARRO IN " & lista & _
            " AND " & _
            "FR74DIA=" & "'" & txtText1(6).Text & "'" & _
            " AND " & _
            "FR87CODESTCARRO=1"
Set rsthora = objApp.rdoConnect.OpenResultset(strhora)
numsalidas = rsthora.rdoColumns(0).Value
ReDim listacarros(rsthora.rdoColumns(0).Value)
strhora = "SELECT * FROM FR7400 WHERE " & _
            "FR07CODCARRO IN " & lista & _
            " AND " & _
            "FR74DIA=" & "'" & txtText1(6).Text & "'" & _
            " AND " & _
            "FR87CODESTCARRO=1"
Set rsthora = objApp.rdoConnect.OpenResultset(strhora)
i = 0
While Not rsthora.EOF
    'listacarros(i) tiene todas las horas de salidas de carro para un d�a y un servicio
    listacarros(i) = rsthora.rdoColumns("FR74HORASALIDA").Value
    i = i + 1
rsthora.MoveNext
Wend
rsthora.Close
Set rsthora = Nothing

'se mira frecuencia por frecuencia
 While Not rstfrec.EOF
    If rstfrec.rdoColumns("FRG5HORA").Value = txtText1(7).Text Then
        'se inserta en el carro
        meter_en_carro = True
        existe_otro_carro = False
    ElseIf CDec(rstfrec.rdoColumns("FRG5HORA").Value) < CDec(txtText1(7).Text) Then
        'no se hace nada ya que la hora en que sale el carro de pantalla es mayor
        'que la hora de toma del medicamento.No se inserta nada
        meter_en_carro = False
        existe_otro_carro = True
    ElseIf CDec(rstfrec.rdoColumns("FRG5HORA").Value) > CDec(txtText1(7).Text) Then
         meter_en_carro = True
         For cont = 0 To i - 1
            If listacarros(cont) <> txtText1(7).Text Then
                If CDec(listacarros(cont)) >= CDec(txtText1(7).Text) And _
                   CDec(listacarros(cont)) <= CDec(rstfrec.rdoColumns("FRG5HORA").Value) Then
                        existe_otro_carro = True
                        Exit For
                   
                Else
                    'no se hace nada
                End If
            End If
          Next cont
    End If
'-------------------------
If meter_en_carro = True And existe_otro_carro = False Then
   arrayfrec(j) = True
End If
existe_otro_carro = False
meter_en_carro = False
j = j + 1
'-------------------------
rstfrec.MoveNext
Wend

For j = 0 To numfrecuencias
    If arrayfrec(j) = True Then
       Insertar_en_el_Carro = True
       Exit For
    End If
Next j
    
rstfrec.Close
Set rstfrec = Nothing

End Sub
Private Sub cmdanalizar_Click()
Dim cont As Integer
Dim rstcaj As rdoResultset
Dim strcaj As String
Dim strpac As String
Dim rstpac As rdoResultset
Dim strper As String
Dim rstper As rdoResultset
Dim rstasis As rdoResultset
Dim strasis As String
Dim strcama As String
Dim rstcama As rdoResultset
Dim rstpet As rdoResultset
Dim strpet As String
Dim strupdate66 As String
Dim strinsert33 As String
Dim rstprod As rdoResultset
Dim strprod As String
Dim hora As String
Dim rstcarro As rdoResultset
Dim strcarro As String
Dim inta�adir As Integer
Dim strasigcarro As String
Dim rstasigcarro As rdoResultset
Dim lista As String
Dim peticion
Dim listaprod() As String
Dim strlistaprod As String
Dim rstlistaprod As rdoResultset
Dim i As Long
Dim j As Long
Dim k As Long
Dim no_esta As Integer
Dim lista_carro() As String
Dim listacarro As String
Dim m As Long
Dim n As Long
Dim p As Long
Dim no_esta_nueva As Integer
Dim str33 As String
Dim rst33 As rdoResultset

cmdanalizar.Enabled = False
inta�adir = 0
If grdDBGrid1(0).Rows = 0 Then

'transforma la coma de separaci�n de los decimales por un punto
hora = txtText1(7).Text
hora = objGen.ReplaceStr(hora, ",", ".", 1)

'se saca el n� de cajetines m�ximo de un carro
strcaj = "SELECT FR07NUMMAXCAJ FROM FR0700 WHERE FR07CODCARRO=" & _
          txtText1(3).Text
Set rstcaj = objApp.rdoConnect.OpenResultset(strcaj)

'se sacan las personas que forman parte del carro pues s�lo de �sas se mirar�n
'las nuevas OM
strper = "SELECT DISTINCT CI21CODPERSONA FROM FR0600 WHERE " & _
                  " FR07CODCARRO=" & txtText1(3).Text & _
                  " AND FR06FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & _
                  " AND FR06HORACARGA=" & hora
Set rstper = objApp.rdoConnect.OpenResultset(strper)



For cont = 0 To rstcaj.rdoColumns(0).Value - 1
    'se cogen los pacientes de las peticiones servidas y del servicio del carro
    'ya que cada cajet�n del carro es una cama
    If Not rstper.EOF Then
        'se obtiene la asistencia abierta del paciente
         strasis = "SELECT AD01CODASISTENCI FROM AD0100 WHERE " & _
                 "CI21CODPERSONA=" & rstper.rdoColumns(0).Value & " AND " & _
                 "AD01FECFIN IS NULL"
         Set rstasis = objApp.rdoConnect.OpenResultset(strasis)
         'con esa a sistencia se obtiene la cama
         strcama = "SELECT AD15CODCAMA FROM AD1500 WHERE " & _
                 "AD01CODASISTENCI=" & rstasis.rdoColumns(0).Value & "AND " & _
                 "AD14CODESTCAMA=2"
         Set rstcama = objApp.rdoConnect.OpenResultset(strcama)
         rstasis.Close
         Set rstasis = Nothing
         
         'se cogen las peticiones OM que tiene cada paciente con estado 4(Validada)
         strpet = "SELECT FR66CODPETICION FROM FR6600 WHERE CI21CODPERSONA=" & _
                rstper.rdoColumns(0).Value & " AND " & _
                "AD02CODDPTO=" & txtText1(0).Text & " AND " & _
                "FR26CODESTPETIC=4 AND FR66INDOM=-1"
         Set rstpet = objApp.rdoConnect.OpenResultset(strpet)

         
         'se cogen las peticiones OM que tiene cada paciente con estado 8(Anulada)
         strasigcarro = "SELECT FR6600.FR66CODPETICION FROM FR6600,FR0600 " & _
                "WHERE FR6600.CI21CODPERSONA=" & rstper.rdoColumns(0).Value & " AND " & _
                "FR6600.AD02CODDPTO=" & txtText1(0).Text & " AND " & _
                "FR6600.FR26CODESTPETIC=8 AND FR6600.FR66INDOM=-1" & " AND " & _
                "FR0600.FR07CODCARRO=" & txtText1(3).Text & " AND " & _
                "FR0600.FR06FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & " AND " & _
                "FR0600.FR06HORACARGA=" & hora & " AND " & _
                "FR0600.FR66CODPETICION=FR6600.FR66CODPETICION"

         Set rstasigcarro = objApp.rdoConnect.OpenResultset(strasigcarro)
         'se carga lista con los c�digos de las peticiones Asignadas a Carro
         lista = ""
         lista = "("
         If Not rstasigcarro.EOF Then
            lista = lista & rstasigcarro.rdoColumns("FR66CODPETICION").Value
            rstasigcarro.MoveNext
         End If
         While Not rstasigcarro.EOF
               lista = lista & "," & rstasigcarro.rdoColumns("FR66CODPETICION").Value
         rstasigcarro.MoveNext
         Wend
         lista = lista & ")"
         rstasigcarro.Close
         Set rstasigcarro = Nothing
         
         If lista <> "()" Then   '***//////////////////////////////////////////
         
         'se coge de FR0600 todos los productos que est�n en el carro
         strprod = "SELECT FR73CODPRODUCTO,FR06CANTIDAD FROM FR0600 WHERE " & _
                  "FR66CODPETICION IN " & lista & _
                  " AND FR07CODCARRO=" & txtText1(3).Text & _
                  " AND FR06FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & _
                  " AND FR06HORACARGA=" & hora
         Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
         
         'se coge de FR0600 todos los productos que est�n en el carro y se meten en listaprod
         strlistaprod = "SELECT count(FR73CODPRODUCTO) FROM FR0600 WHERE " & _
                         "FR66CODPETICION IN " & lista & _
                         " AND FR07CODCARRO=" & txtText1(3).Text & _
                         " AND FR06FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & _
                         " AND FR06HORACARGA=" & hora
         Set rstlistaprod = objApp.rdoConnect.OpenResultset(strlistaprod)
         'se carga listaprod con los c�digos de los productos Asignadas a Carro
         ReDim listaprod(rstlistaprod.rdoColumns(0).Value)
         'j guarda el n� de productos del carro
         j = rstlistaprod.rdoColumns(0).Value
         strlistaprod = "SELECT FR73CODPRODUCTO FROM FR0600 WHERE " & _
                         "FR66CODPETICION IN " & lista & _
                         " AND FR07CODCARRO=" & txtText1(3).Text & _
                         " AND FR06FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & _
                         " AND FR06HORACARGA=" & hora
         Set rstlistaprod = objApp.rdoConnect.OpenResultset(strlistaprod)
         i = 0
         While Not rstlistaprod.EOF
               listaprod(i) = rstlistaprod.rdoColumns("FR73CODPRODUCTO").Value
               i = i + 1
               
         rstlistaprod.MoveNext
         Wend
         rstlistaprod.Close
         Set rstlistaprod = Nothing
         End If '****//////////////////////////////////////////////////////////
         'mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
         'se cogen las peticiones OM que tiene cada paciente con estado 4(Validada)
         strasigcarro = "SELECT FR66CODPETICION FROM FR6600 WHERE CI21CODPERSONA=" & _
                rstper.rdoColumns(0).Value & " AND " & _
                "AD02CODDPTO=" & txtText1(0).Text & " AND " & _
                "FR26CODESTPETIC=4 AND FR66INDOM=-1"
         Set rstasigcarro = objApp.rdoConnect.OpenResultset(strasigcarro)
         'se carga listacarro con los c�digos de las peticiones nuevas
         listacarro = ""
         listacarro = "("
         If Not rstasigcarro.EOF Then
            listacarro = listacarro & rstasigcarro.rdoColumns("FR66CODPETICION").Value
            rstasigcarro.MoveNext
         End If
         While Not rstasigcarro.EOF
               listacarro = listacarro & "," & rstasigcarro.rdoColumns("FR66CODPETICION").Value
         rstasigcarro.MoveNext
         Wend
         listacarro = listacarro & ")"
         rstasigcarro.Close
         Set rstasigcarro = Nothing
         
         If listacarro <> "()" Then '******************�������������������������
         
         'se pone la petici�n en estado 7 <Asignada a Carro>
         'strupdate66 = "UPDATE FR6600 SET FR26CODESTPETIC=7 WHERE FR66CODPETICION IN " & listacarro
         'objApp.rdoConnect.Execute strupdate66, 64
         'objApp.rdoConnect.Execute "Commit", 64
         
         
         'se coge de FR3200 todos los productos nuevos y se meten en lista_carro
         strlistaprod = "SELECT count(FR73CODPRODUCTO) FROM FR3200 WHERE " & _
                         "FR66CODPETICION IN " & listacarro & _
                         " AND FR32NUMMODIFIC=1"
         Set rstlistaprod = objApp.rdoConnect.OpenResultset(strlistaprod)
         'se carga lista_carro con los c�digos de los productos nuevos
         ReDim lista_carro(rstlistaprod.rdoColumns(0).Value)
         'm guarda el n� de productos nuevos
         m = rstlistaprod.rdoColumns(0).Value
         strlistaprod = "SELECT FR73CODPRODUCTO FROM FR3200 WHERE " & _
                         "FR66CODPETICION IN " & listacarro & _
                         " AND FR32NUMMODIFIC=1"
         Set rstlistaprod = objApp.rdoConnect.OpenResultset(strlistaprod)
         n = 0
         While Not rstlistaprod.EOF
               lista_carro(n) = rstlistaprod.rdoColumns("FR73CODPRODUCTO").Value
               n = n + 1
               
         rstlistaprod.MoveNext
         Wend
         rstlistaprod.Close
         Set rstlistaprod = Nothing
         
         
         
         'mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
         
         'para cada petici�n del paciente
         While Not rstpet.EOF
                'se miran los productos de cada nueva OM
                strcarro = "SELECT FR73CODPRODUCTO,FR28DOSIS,FR28NUMLINEA,FRG4CODFRECUENCIA " & _
                       "FROM FR3200 " & _
                       "WHERE FR66CODPETICION=" & rstpet.rdoColumns(0).Value & _
                       "AND FR32NUMMODIFIC=1"
                Set rstcarro = objApp.rdoConnect.OpenResultset(strcarro)
                'para cada producto nuevo se mira si est� en la lista de los productos
                'que est�n en el carro
                While Not rstcarro.EOF
                  For k = 0 To j
                      If rstcarro.rdoColumns("FR73CODPRODUCTO").Value = listaprod(k) Then
                       no_esta = 0
                       Exit For
                      Else
                       no_esta = 1
                      End If
                  Next k
                  'si el producto nuevo no est� en el carro se inserta
                  If no_esta = 1 Then
                    'se pone como A�adido el nuevo producto
                    Call Insertar_en_Carro(rstcarro.rdoColumns("FRG4CODFRECUENCIA").Value)
                    If Insertar_en_el_Carro = True Then
                        Insertar_en_el_Carro = False
                        'se pone la petici�n en estado 7 <Asignada a Carro>
                        strupdate66 = "UPDATE FR6600 SET FR26CODESTPETIC=7 " & _
                                      "WHERE FR66CODPETICION=" & rstpet.rdoColumns(0).Value
                        objApp.rdoConnect.Execute strupdate66, 64
                        objApp.rdoConnect.Execute "Commit", 64
                        strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                                 "FR73CODPRODUCTO,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                                 "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR33MOTDIFERENCIA," & _
                                 "FR33FECCARGA,FR33HORACARGA)" & _
                                 " VALUES (" & _
                                 txtText1(3).Text & "," & _
                                 rstcama.rdoColumns(0).Value & "," & _
                                 rstper.rdoColumns(0).Value & "," & _
                                 rstcarro.rdoColumns("FR73CODPRODUCTO").Value & "," & _
                                 rstcarro.rdoColumns("FR28DOSIS").Value & "," & _
                                 "NULL,NULL,NULL,NULL" & "," & _
                                 rstpet.rdoColumns(0).Value & "," & _
                                 "'A�adido'" & "," & _
                                 "TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & "," & _
                                 hora & ")"
                        objApp.rdoConnect.Execute strinsert33, 64
                        objApp.rdoConnect.Execute "Commit", 64
                    End If
                  End If
                  
                  'se cogen las peticiones OM que tiene cada paciente con estado 8(Anulada)
                  'strasigcarro = "SELECT FR66CODPETICION FROM FR6600 WHERE CI21CODPERSONA=" & _
                     rstper.rdoColumns(0).Value & " AND " & _
                     "AD02CODDPTO=" & txtText1(0).Text & " AND " & _
                     "FR26CODESTPETIC=8 AND FR66INDOM=-1"
                  strasigcarro = "SELECT FR6600.FR66CODPETICION FROM FR6600,FR0600 " & _
                    "WHERE FR6600.CI21CODPERSONA=" & rstper.rdoColumns(0).Value & " AND " & _
                    "FR6600.AD02CODDPTO=" & txtText1(0).Text & " AND " & _
                    "FR6600.FR26CODESTPETIC=8 AND FR6600.FR66INDOM=-1" & " AND " & _
                    "FR0600.FR07CODCARRO=" & txtText1(3).Text & " AND " & _
                    "FR0600.FR06FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & " AND " & _
                    "FR0600.FR06HORACARGA=" & hora & " AND " & _
                    "FR0600.FR66CODPETICION=FR6600.FR66CODPETICION"
                  Set rstasigcarro = objApp.rdoConnect.OpenResultset(strasigcarro)
                  While Not rstasigcarro.EOF
                    'se coge de FR0600 todos los productos de la petici�n ya asignada
                    strprod = "SELECT FR73CODPRODUCTO,FR06CANTIDAD FROM FR0600 WHERE " & _
                         "FR66CODPETICION=" & rstasigcarro.rdoColumns(0).Value & _
                         " AND FR07CODCARRO=" & txtText1(3).Text & _
                         " AND FR06FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & _
                         " AND FR06HORACARGA=" & hora
                    Set rstprod = objApp.rdoConnect.OpenResultset(strprod)
                    Do While Not rstprod.EOF
                    'ggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg
                    'para cada producto del carro hay que ver si est� en lista_carro que
                    'tiene los productos nuevos y si no est� se quita
                     For p = 0 To m
                      If rstprod.rdoColumns("FR73CODPRODUCTO").Value = lista_carro(p) Then
                       no_esta_nueva = 0
                       Exit For
                      Else
                       no_esta_nueva = 1
                      End If
                     Next p
                     If no_esta_nueva = 1 Then
                        str33 = "SELECT COUNT(*) FROM FR3300 WHERE " & _
                                    "FR07CODCARRO=" & txtText1(3).Text & " AND " & _
                                    "AD15CODCAMA=" & rstcama.rdoColumns(0).Value & " AND " & _
                                    "CI21CODPERSONA=" & rstper.rdoColumns(0).Value & " AND " & _
                                    "FR73CODPRODUCTO=" & rstprod.rdoColumns("FR73CODPRODUCTO").Value & " AND " & _
                                    "FR33CANTIDAD=" & rstprod.rdoColumns("FR06CANTIDAD").Value & " AND " & _
                                    "FR66CODPETICION=" & rstasigcarro.rdoColumns(0).Value & " AND " & _
                                    "FR33MOTDIFERENCIA='Eliminado'" & " AND " & _
                                    "FR33FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & " AND " & _
                                    "FR33HORACARGA=" & hora
                        Set rst33 = objApp.rdoConnect.OpenResultset(str33)
                        If rst33.rdoColumns(0).Value = 0 Then
                                    strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                                         "FR73CODPRODUCTO,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                                         "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR33MOTDIFERENCIA," & _
                                         "FR33FECCARGA,FR33HORACARGA)" & _
                                         " VALUES (" & _
                                         txtText1(3).Text & "," & _
                                         rstcama.rdoColumns(0).Value & "," & _
                                         rstper.rdoColumns(0).Value & "," & _
                                         rstprod.rdoColumns("FR73CODPRODUCTO").Value & "," & _
                                         rstprod.rdoColumns("FR06CANTIDAD").Value & "," & _
                                         "NULL,NULL,NULL,NULL" & "," & _
                                         rstasigcarro.rdoColumns(0).Value & "," & _
                                         "'Eliminado'" & "," & _
                                         "TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & "," & _
                                         hora & ")"
                                    objApp.rdoConnect.Execute strinsert33, 64
                                    objApp.rdoConnect.Execute "Commit", 64
                        End If
                        rst33.Close
                        Set rst33 = Nothing
                     End If
                    'ggggggggggggggggggggggggggggggggggggggggggggggggggggggg
                     If (rstcarro.rdoColumns("FR73CODPRODUCTO").Value = _
                        rstprod.rdoColumns("FR73CODPRODUCTO").Value) Then
                      'se mira si la cantidad es la misma
                      If (rstcarro.rdoColumns("FR28DOSIS").Value <> _
                          rstprod.rdoColumns("FR06CANTIDAD").Value) Then
                             'se pone como A�adido lo que hay en la petici�n
                             'si el producto est� en dos peticiones distintas de FR0600
                             'entonces no habr� que a�adir de nuevo el producto de la
                             'nueva petici�n en FR3300
                             '----------------
                             str33 = "SELECT COUNT(*) FROM FR3300 WHERE " & _
                                    "FR07CODCARRO=" & txtText1(3).Text & " AND " & _
                                    "AD15CODCAMA=" & rstcama.rdoColumns(0).Value & " AND " & _
                                    "CI21CODPERSONA=" & rstper.rdoColumns(0).Value & " AND " & _
                                    "FR73CODPRODUCTO=" & rstcarro.rdoColumns("FR73CODPRODUCTO").Value & " AND " & _
                                    "FR33CANTIDAD=" & rstcarro.rdoColumns("FR28DOSIS").Value & " AND " & _
                                    "FR66CODPETICION=" & rstpet.rdoColumns(0).Value & " AND " & _
                                    "FR33MOTDIFERENCIA='A�adido'" & " AND " & _
                                    "FR33FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & " AND " & _
                                    "FR33HORACARGA=" & hora
                             Set rst33 = objApp.rdoConnect.OpenResultset(str33)
                             If rst33.rdoColumns(0).Value = 0 Then
                                Call Insertar_en_Carro(rstcarro.rdoColumns("FRG4CODFRECUENCIA").Value)
                                If Insertar_en_el_Carro = True Then
                                    'se pone la petici�n en estado 7 <Asignada a Carro>
                                    strupdate66 = "UPDATE FR6600 SET FR26CODESTPETIC=7 " & _
                                      "WHERE FR66CODPETICION=" & rstpet.rdoColumns(0).Value
                                    objApp.rdoConnect.Execute strupdate66, 64
                                    objApp.rdoConnect.Execute "Commit", 64
                                    Insertar_en_el_Carro = False
                                    strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                                    "FR73CODPRODUCTO,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                                    "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR33MOTDIFERENCIA," & _
                                    "FR33FECCARGA,FR33HORACARGA)" & _
                                    " VALUES (" & _
                                    txtText1(3).Text & "," & _
                                    rstcama.rdoColumns(0).Value & "," & _
                                    rstper.rdoColumns(0).Value & "," & _
                                    rstcarro.rdoColumns("FR73CODPRODUCTO").Value & "," & _
                                    rstcarro.rdoColumns("FR28DOSIS").Value & "," & _
                                    "NULL,NULL,NULL,NULL" & "," & _
                                    rstpet.rdoColumns(0).Value & "," & _
                                    "'A�adido'" & "," & _
                                    "TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & "," & _
                                    hora & ")"
                                    objApp.rdoConnect.Execute strinsert33, 64
                                    objApp.rdoConnect.Execute "Commit", 64
                                End If
                             End If
                             rst33.Close
                             Set rst33 = Nothing
                             '����������������������������������������������������
                             str33 = "SELECT COUNT(*) FROM FR3300 WHERE " & _
                                    "FR07CODCARRO=" & txtText1(3).Text & " AND " & _
                                    "AD15CODCAMA=" & rstcama.rdoColumns(0).Value & " AND " & _
                                    "CI21CODPERSONA=" & rstper.rdoColumns(0).Value & " AND " & _
                                    "FR73CODPRODUCTO=" & rstprod.rdoColumns("FR73CODPRODUCTO").Value & " AND " & _
                                    "FR33CANTIDAD=" & rstprod.rdoColumns("FR06CANTIDAD").Value & " AND " & _
                                    "FR66CODPETICION=" & rstasigcarro.rdoColumns(0).Value & " AND " & _
                                    "FR33MOTDIFERENCIA='Eliminado'" & " AND " & _
                                    "FR33FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & " AND " & _
                                    "FR33HORACARGA=" & hora
                             Set rst33 = objApp.rdoConnect.OpenResultset(str33)
                             If rst33.rdoColumns(0).Value = 0 Then
                             '������������������������������������������������������
                                'se pone como Eliminado lo que hab�a en el carro
                                strinsert33 = "INSERT INTO FR3300 (FR07CODCARRO,AD15CODCAMA,CI21CODPERSONA," & _
                                "FR73CODPRODUCTO,FR33CANTIDAD,FR33INDCARGAREA,FR33INDINCCAMA," & _
                                "FR33INDINCPAC,FR33INDINCPROD,FR66CODPETICION,FR33MOTDIFERENCIA," & _
                                "FR33FECCARGA,FR33HORACARGA)" & _
                                " VALUES (" & _
                                txtText1(3).Text & "," & _
                                rstcama.rdoColumns(0).Value & "," & _
                                rstper.rdoColumns(0).Value & "," & _
                                rstprod.rdoColumns("FR73CODPRODUCTO").Value & "," & _
                                rstprod.rdoColumns("FR06CANTIDAD").Value & "," & _
                                "NULL,NULL,NULL,NULL" & "," & _
                                rstasigcarro.rdoColumns(0).Value & "," & _
                                "'Eliminado'" & "," & _
                                "TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')" & "," & _
                                hora & ")"
                                objApp.rdoConnect.Execute strinsert33, 64
                                objApp.rdoConnect.Execute "Commit", 64
                                End If
                                rst33.Close
                                Set rst33 = Nothing
                      End If
                    End If
                    '�������������������������������������������
                    rstprod.MoveNext
                    Loop
                    rstprod.Close
                    Set rstprod = Nothing
                  rstasigcarro.MoveNext
                  Wend
                  rstasigcarro.Close
                  Set rstasigcarro = Nothing
                  
                  
               rstcarro.MoveNext
               Wend
               rstcarro.Close
               Set rstcarro = Nothing
        '------------------------------------------------------------------------
         rstpet.MoveNext
         Wend
         rstpet.Close
         Set rstpet = Nothing

         rstcama.Close
         Set rstcama = Nothing
         
End If  '*******************************************�����������������������������
rstper.MoveNext
End If
Next cont
rstper.Close
Set rstper = Nothing
'----------

rstcaj.Close
Set rstcaj = Nothing



Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
objWinInfo.DataRefresh

End If
cmdanalizar.Enabled = True
End Sub

Private Sub cmdtransdatos_Click()
cmdtransdatos.Enabled = False
Dim rstdes As rdoResultset
Dim strdes As String
Dim rstori As rdoResultset
Dim strori As String
Dim strinsertentrada As String
Dim strinsertsalida As String
Dim str35 As String
Dim rst35 As rdoResultset
Dim str80 As String
Dim rst80 As rdoResultset
Dim i As Integer
Dim struni As String
Dim rstuni As rdoResultset
Dim mintisel As Integer

cmdtransdatos.Enabled = False

'se obtiene el almac�n que pide el producto
strdes = "SELECT FR04CODALMACEN FROM AD0200 WHERE AD02CODDPTO=" & txtText1(0).Text
Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
'se obtiene el almac�n del servicio 1(Farmacia) que es el que dispensa el producto
strori = "SELECT FR04CODALMACEN FROM AD0200,FRH200 " & _
         "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
         "AND FRH2CODPARAMGEN=3"
Set rstori = objApp.rdoConnect.OpenResultset(strori)

If (Not rstdes.EOF And Not rstori.EOF) And Not IsNull(rstori.rdoColumns(0).Value) _
  And Not IsNull(rstdes.rdoColumns(0).Value) Then
  
'se hace un bucle con todos los productos que van en el carro para llenar las
'tablas FR3500 y FR8000
grdDBGrid1(0).MoveFirst
For mintisel = 0 To grdDBGrid1(0).Rows - 1
    'Guardamos el n�mero de fila que est� seleccionada
    
    str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
    Set rst35 = objApp.rdoConnect.OpenResultset(str35)
            
    str80 = "SELECT FR80NUMMOV_SEQUENCE.nextval FROM dual"
    Set rst80 = objApp.rdoConnect.OpenResultset(str80)
            
    struni = "SELECT FR93CODUNIMEDIDA FROM FR2800 WHERE FR73CODPRODUCTO=" & _
             grdDBGrid1(0).Columns(10).Value & " AND FR66CODPETICION=" & _
             grdDBGrid1(0).Columns(14).Value
    Set rstuni = objApp.rdoConnect.OpenResultset(struni)
          
    If grdDBGrid1(0).Columns(13).Value = "A�adido" Then
        'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
        'Farmacia y el destino es el almac�n al que se dispensa
        strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                        "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                        "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) VALUES (" & _
                        rst35.rdoColumns(0).Value & "," & _
                        rstori.rdoColumns(0).Value & "," & _
                        rstdes.rdoColumns(0).Value & "," & _
                        "8" & "," & _
                        "SYSDATE" & "," & _
                        "NULL" & "," & _
                        grdDBGrid1(0).Columns(10).Value & "," & _
                        grdDBGrid1(0).Columns(12).Value & "," & _
                        "0" & ","
                        
        If IsNull(rstuni.rdoColumns(0).Value) = False Then
            strinsertentrada = strinsertentrada & rstuni.rdoColumns(0).Value & ")"
        Else
            strinsertentrada = strinsertentrada & "1)"
        End If
        objApp.rdoConnect.Execute strinsertentrada, 64
        objApp.rdoConnect.Execute "Commit", 64
                
        'se actualiza el inventario de salida(FR8000) en el que el origen de salida es farmacia
        'y el destino de la salida el almac�n al que se dispensa
        strinsertsalida = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                        "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
                        "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES(" & _
                        rst80.rdoColumns(0).Value & "," & _
                        rstori.rdoColumns(0).Value & "," & _
                        rstdes.rdoColumns(0).Value & "," & _
                        "7" & "," & _
                        "SYSDATE" & "," & _
                        "NULL" & "," & _
                        grdDBGrid1(0).Columns(10).Value & "," & _
                        grdDBGrid1(0).Columns(12).Value & "," & _
                        "0" & ","
        
        If IsNull(rstuni.rdoColumns(0).Value) = False Then
            strinsertsalida = strinsertsalida & rstuni.rdoColumns(0).Value & ")"
        Else
            strinsertsalida = strinsertsalida & "1)"
        End If
        
        objApp.rdoConnect.Execute strinsertsalida, 64
        objApp.rdoConnect.Execute "Commit", 64
    End If
    If grdDBGrid1(0).Columns(14).Value = "Eliminado" Then
        'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
        'Farmacia y el destino es el almac�n al que se dispensa
        strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                        "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                        "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) VALUES (" & _
                        rst35.rdoColumns(0).Value & "," & _
                        rstdes.rdoColumns(0).Value & "," & _
                        rstori.rdoColumns(0).Value & "," & _
                        "8" & "," & _
                        "SYSDATE" & "," & _
                        "NULL" & "," & _
                        grdDBGrid1(0).Columns(10).Value & "," & _
                        grdDBGrid1(0).Columns(12).Value & "," & _
                        "0" & ","
        If IsNull(rstuni.rdoColumns(0).Value) = False Then
            strinsertentrada = strinsertentrada & rstuni.rdoColumns(0).Value & ")"
        Else
            strinsertentrada = strinsertentrada & "1)"
        End If
        objApp.rdoConnect.Execute strinsertentrada, 64
        objApp.rdoConnect.Execute "Commit", 64
                
        'se actualiza el inventario de salida(FR8000) en el que el origen de salida es farmacia
        'y el destino de la salida el almac�n al que se dispensa
        strinsertsalida = "INSERT INTO FR8000 (FR80NUMMOV,FR04CODALMACEN_ORI,FR04CODALMACEN_DES," & _
                        "FR90CODTIPMOV,FR80FECMOVIMIENTO,FR80OBSERVMOV,FR73CODPRODUCTO," & _
                        "FR80CANTPROD,FR80PRECUNI,FR93CODUNIMEDIDA) VALUES(" & _
                        rst80.rdoColumns(0).Value & "," & _
                        rstdes.rdoColumns(0).Value & "," & _
                        rstori.rdoColumns(0).Value & "," & _
                        "7" & "," & _
                        "SYSDATE" & "," & _
                        "NULL" & "," & _
                        grdDBGrid1(0).Columns(10).Value & "," & _
                        grdDBGrid1(0).Columns(12).Value & "," & _
                        "0" & ","
                        
        If IsNull(rstuni.rdoColumns(0).Value) = False Then
            strinsertsalida = strinsertsalida & rstuni.rdoColumns(0).Value & ")"
        Else
            strinsertsalida = strinsertsalida & "1)"
        End If
        objApp.rdoConnect.Execute strinsertsalida, 64
        objApp.rdoConnect.Execute "Commit", 64
    End If
            
    rst35.Close
    Set rst35 = Nothing
    rst80.Close
    Set rst80 = Nothing
    
    rstuni.Close
    Set rstuni = Nothing
    
 grdDBGrid1(0).MoveNext
 Next mintisel
End If

rstdes.Close
Set rstdes = Nothing
rstori.Close
Set rstori = Nothing

'---------------------------------------------------------------------
'se llama a frmCrtlIncidencias
frmCrtlIncidenciasDif.txtcodservicio.Text = txtText1(0).Text
frmCrtlIncidenciasDif.txtdescservicio.Text = txtText1(1).Text
frmCrtlIncidenciasDif.txtcodcarro.Text = txtText1(3).Text
frmCrtlIncidenciasDif.txtdesccarro.Text = txtText1(4).Text
frmCrtlIncidenciasDif.txtcodestado.Text = txtText1(2).Text
frmCrtlIncidenciasDif.txtdescestado.Text = txtText1(5).Text
frmCrtlIncidenciasDif.txtdia.Text = txtText1(6).Text
frmCrtlIncidenciasDif.txthora.Text = txtText1(7).Text

Call objsecurity.LaunchProcess("FR0151")
cmdtransdatos.Enabled = True
End Sub

Private Sub Form_Activate()
If txtText1(3).Text <> "" Then
    cmdanalizar.Enabled = True
    cmdtransdatos.Enabled = True
Else
    cmdanalizar.Enabled = False
    cmdtransdatos.Enabled = False
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Carros"
      
    .strTable = "FR0701J"
    .intAllowance = cwAllowReadOnly
    'estado=2 <Se ha comenzado la carga>
    .strWhere = "FR87CODESTCARRO=2"
    
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    Call .FormAddOrderField("FR74DIA", cwAscending)
    Call .FormAddOrderField("FR74HORASALIDA", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Carros")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "C�digo Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR07DESCARRO", "Descripci�n Carro", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Descripci�n Servicio", cwString)
    Call .FormAddFilterWhere(strKey, "FR87CODESTCARRO", "C�d.Estado Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR87DESESTCARRO", "Desc.Estado Carro", cwString)
    Call .FormAddFilterWhere(strKey, "FR74DIA", "D�a Salida", cwString)
    Call .FormAddFilterWhere(strKey, "FR74HORASALIDA", "Hora Salida", cwNumeric)
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Peticiones del Carro"
    .intAllowance = cwAllowReadOnly
    
    .strTable = "FR3300"
    
    .strWhere = "FR33FECCARGA=TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY')"
    
    Call .FormAddOrderField("FR07CODCARRO", cwAscending)
    Call .FormAddRelation("FR07CODCARRO", txtText1(3))
    Call .FormAddRelation("FR33HORACARGA", txtText1(7))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones del Carro")
    Call .FormAddFilterWhere(strKey, "FR07CODCARRO", "Carro", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD15CODCAMA", "Cama", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "Persona", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d.Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR33CANTIDAD", "Cantidad", cwNumeric)
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "Carro", "FR07CODCARRO", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "Cama", "AD15CODCAMA", cwString, 7)
    Call .GridAddColumn(objMultiInfo, "Persona", "CI21CODPERSONA", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Historia", "", cwNumeric, 7) 'historia
    Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString, 25) 'nombre
    Call .GridAddColumn(objMultiInfo, "Apellido 1�", "", cwString, 25) 'apellido 1�
    Call .GridAddColumn(objMultiInfo, "Apellido 2�", "", cwString, 25) 'apellido 2�
    Call .GridAddColumn(objMultiInfo, "C�d.Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "FR33CANTIDAD", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Estado", "FR33MOTDIFERENCIA", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Petici�n", "FR66CODPETICION", cwNumeric, 9)
    
    
    Call .FormCreateInfo(objMasterInfo)
    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(10)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnInFind = True
  
    '.CtrlGetInfo(txtText1(3)).blnInGrid = False 'c�d.carro
    .CtrlGetInfo(txtText1(2)).blnInGrid = False 'c�d.estado carro

    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "CI22NUMHISTORIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(7), "CI22NOMBRE")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(8), "CI22PRIAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(9), "CI22SEGAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(10)), grdDBGrid1(0).Columns(11), "FR73DESPRODUCTO")
     
    grdDBGrid1(0).Columns(3).Visible = False
    grdDBGrid1(0).Columns(14).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
 grdDBGrid1(2).Columns(1).Width = 1100 'c�d.servicio
 grdDBGrid1(2).Columns(2).Width = 2500 'servicio
 grdDBGrid1(2).Columns(3).Width = 900 'c�d.carro
 grdDBGrid1(2).Columns(4).Width = 2500 'carro
 grdDBGrid1(2).Columns(5).Width = 2500 'estado carro
 grdDBGrid1(2).Columns(6).Width = 600 'd�a
 grdDBGrid1(2).Columns(7).Width = 600 'hora
  
 grdDBGrid1(0).Columns(4).Width = 1000 'cama
 grdDBGrid1(0).Columns(5).Width = 1000 'c�d.persona
 grdDBGrid1(0).Columns(6).Width = 1000 'historia
 grdDBGrid1(0).Columns(7).Width = 1100 'nombre
 grdDBGrid1(0).Columns(8).Width = 1200 'apellido 1�
 grdDBGrid1(0).Columns(9).Width = 1200 'apellido 2�
 grdDBGrid1(0).Columns(10).Width = 1000 'c�d.prod
 grdDBGrid1(0).Columns(11).Width = 1900 'producto
 grdDBGrid1(0).Columns(12).Width = 800 'cantidad
 grdDBGrid1(0).Columns(13).Width = 800 'estado
 
 Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
If intIndex = 3 Then
    If txtText1(3).Text <> "" Then
        cmdanalizar.Enabled = True
        cmdtransdatos.Enabled = True
    Else
        cmdanalizar.Enabled = False
        cmdtransdatos.Enabled = False
    End If
End If
  Call objWinInfo.CtrlDataChange
End Sub


