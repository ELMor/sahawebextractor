VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmVerPedPtaNoVal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. PEDIR SERVICIO FARMACIA. Ver Pedido Planta no Validado"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0136.frx":0000
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdProducto 
      Caption         =   "Sustituir Producto"
      Height          =   375
      Left            =   10080
      TabIndex        =   25
      Top             =   4680
      Width           =   1815
   End
   Begin VB.CommandButton cmdBloquear 
      Caption         =   "Bloquear/Desbloquear"
      Height          =   375
      Left            =   10080
      TabIndex        =   24
      Top             =   5400
      Width           =   1815
   End
   Begin VB.CommandButton cmdValidar 
      Caption         =   "Validar"
      Height          =   375
      Left            =   3960
      TabIndex        =   23
      Top             =   7560
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   11340
      Begin TabDlg.SSTab tabTab1 
         Height          =   2295
         Index           =   0
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   360
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0136.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(16)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(19)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(23)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(10)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(6)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(28)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "dtcDateCombo1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txttext1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txttext1(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txttext1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txttext1(6)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txttext1(3)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txttext1(5)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txttext1(4)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0136.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   4
            Left            =   2040
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Persona Peticionaria"
            Top             =   1800
            Width           =   3015
         End
         Begin VB.TextBox txttext1 
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   5
            Left            =   480
            TabIndex        =   21
            Tag             =   "C�digo Servicio"
            Top             =   1080
            Width           =   1100
         End
         Begin VB.TextBox txttext1 
            DataField       =   "SG02COD_PDS"
            Height          =   330
            Index           =   3
            Left            =   480
            TabIndex        =   18
            Tag             =   "Persona Peticionaria"
            Top             =   1800
            Width           =   1095
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   6
            Left            =   2040
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "Descripci�n Servicio"
            Top             =   1080
            Width           =   5175
         End
         Begin VB.TextBox txttext1 
            DataField       =   "FR55CODNECESUNID"
            Height          =   330
            Index           =   0
            Left            =   480
            TabIndex        =   8
            Tag             =   "C�digo Necesidad"
            Top             =   360
            Width           =   1100
         End
         Begin VB.TextBox txttext1 
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   1
            Left            =   2880
            TabIndex        =   7
            Tag             =   "Estado Petici�n"
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   2
            Left            =   3360
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Estado"
            Top             =   360
            Width           =   1605
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   0
            Left            =   -74880
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1905
            Index           =   2
            Left            =   -74880
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   240
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   3360
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR55FECPETICION"
            Height          =   330
            Index           =   0
            Left            =   7320
            TabIndex        =   13
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cod.Peticionario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   480
            TabIndex        =   20
            Top             =   1560
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Persona Peticionaria"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   2040
            TabIndex        =   19
            Top             =   1560
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   480
            TabIndex        =   17
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   2040
            TabIndex        =   16
            Top             =   840
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   23
            Left            =   7320
            TabIndex        =   14
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d. Necesidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   19
            Left            =   480
            TabIndex        =   12
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   2880
            TabIndex        =   11
            Top             =   120
            Width           =   735
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Necesidades de la Unidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Index           =   2
      Left            =   120
      TabIndex        =   0
      Top             =   3360
      Width           =   9855
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3495
         Index           =   1
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   9570
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         stylesets.count =   1
         stylesets(0).Name=   "Bloqueada"
         stylesets(0).ForeColor=   16777215
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "FR0136.frx":0044
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   16880
         _ExtentY        =   6165
         _StockProps     =   79
         Caption         =   "NECESIDADES DE LA UNIDAD"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmVerPedPtaNoVal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmVerPedPtaNoVal (FR0136.FRM)                               *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Ver Pedido Planta no Validado                           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1





Private Sub cmdbloquear_Click()
    Dim strupdate As String
    Dim rsta As rdoResultset
    Dim sqlstr As String
    
    cmdBloquear.Enabled = False
    If grdDBGrid1(1).Columns(3).Value <> "" And grdDBGrid1(1).Columns(4).Value <> "" Then
        sqlstr = "SELECT count(*) from fr2000 where fr55codnecesunid=" & grdDBGrid1(1).Columns(3).Value & _
                 " and fr20numlinea=" & grdDBGrid1(1).Columns(4).Value & _
                 " and fr20indbloqueo=-1"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        If rsta(0).Value = 0 Then
            strupdate = "UPDATE FR2000 SET FR20INDBLOQUEO=-1 WHERE FR55CODNECESUNID=" & grdDBGrid1(1).Columns(3).Value & _
                        " AND FR20NUMLINEA=" & grdDBGrid1(1).Columns(4).Value
            objApp.rdoConnect.Execute strupdate, 64
        Else
            strupdate = "UPDATE FR2000 SET FR20INDBLOQUEO=0 WHERE FR55CODNECESUNID=" & grdDBGrid1(1).Columns(3).Value & _
                        " AND FR20NUMLINEA=" & grdDBGrid1(1).Columns(4).Value
            objApp.rdoConnect.Execute strupdate, 64
        End If
    End If
    objWinInfo.DataRefresh
    rsta.Close
    Set rsta = Nothing
    cmdBloquear.Enabled = True
End Sub

Private Sub cmdProducto_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim fila As Integer

  If grdDBGrid1(1).SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar una linea."
    Exit Sub
  End If
  fila = grdDBGrid1(1).row
  
  If tlbToolbar1.Buttons(4).Enabled = True Then
    Call objWinInfo.DataSave
  End If
  
  cmdProducto.Enabled = False
  
'  If grdDBGrid1(0).SelBookmarks.Count = 0 Then
'    MsgBox "Debe seleccionar una linea."
'  Else
    
    noinsertar = True
    Call objsecurity.LaunchProcess("FR0119")
    If gintprodtotal > 0 Then
      If gintprodtotal > 1 Then
        MsgBox "Debe traer s�lo 1 producto."
      Else
          'For v = 0 To gintprodtotal - 1
          'se mira que no se inserte 2 veces el mismo producto
          'If grdDBGrid1(0).Rows > 0 Then
          '  grdDBGrid1(0).MoveFirst
          '  For i = 0 To grdDBGrid1(0).Rows - 1
          '    If grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) _
          '       And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
          '      'no se inserta el producto pues ya est� insertado en el grid
          '      noinsertar = False
          '      Exit For
          '    Else
          '      noinsertar = True
          '    End If
          '    grdDBGrid1(0).MoveNext
          '  Next i
          'End If
          'If noinsertar = True Then
            grdDBGrid1(1).SelBookmarks.RemoveAll
            grdDBGrid1(1).row = fila
            grdDBGrid1(1).Col = 5
            grdDBGrid1(1).Columns(5).Value = ""
            SendKeys gintprodbuscado(v, 0)
            'Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(5), gintprodbuscado(v, 0))
            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(6), gintprodbuscado(v, 1))
            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(7), gintprodbuscado(v, 2))
            'objWinInfo.objWinActiveForm.blnChanged = True
          'Else
          '  mensaje = MsgBox("El producto: " & gintprodbuscado(v, 2) & " ya est� guardado." & _
          '  Chr(13), vbInformation)
          'End If
          'noinsertar = True
        'Next v
      End If
    End If
    gintprodtotal = 0
'  End If
  cmdProducto.Enabled = True
End Sub

Private Sub cmdvalidar_Click()
    Dim intMsg As Integer
    Dim strupdate As String
    
    cmdValidar.Enabled = False
    If txttext1(0).Text <> "" Then
        intMsg = MsgBox("�Est� seguro que desea validar la Petici�n?", vbQuestion + vbYesNo)
        If intMsg = vbYes Then
            strupdate = "UPDATE FR5500 SET FR26CODESTPETIC=4 WHERE FR55CODNECESUNID=" & txttext1(0).Text
            objApp.rdoConnect.Execute strupdate, 64
            objWinInfo.DataRefresh
        End If
    End If
    cmdValidar.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String

  
    Set objWinInfo = New clsCWWin
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
      With objMasterInfo
        Set .objFormContainer = fraFrame1(1)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(0)
        Set .grdGrid = grdDBGrid1(2)
        
        .strName = "Necesidad Unidad"
          
        .strTable = "FR5500"
        .strWhere = "FR55CODNECESUNID=" & frmValPedPtaFarma.grdDBGrid1(1).Columns(3).Value
        .intAllowance = cwAllowReadOnly
        Call .FormAddOrderField("FR55CODNECESUNID", cwAscending)
       
        strKey = .strDataBase & .strTable
        'Call .FormCreateFilterWhere(strKey, "Necesidad Unidad")
        'Call .FormAddFilterWhere(strKey, "FR55CODNECESUNID", "C�d. Necesidad Unidad", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�d. Peticionario", cwString)
        'Call .FormAddFilterWhere(strKey, "FR55FECPETICION", "Fecha Petici�n", cwDate)
        'Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "Estado Petici�n", cwNumeric)
        'Call .FormAddFilterOrder(strKey, "FR55CODNECESUNID", "C�digo Necesidad Unidad")
    
      End With
    With objMultiInfo
        .strName = "Detalle Necesidad"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = fraFrame1(1)
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR1900"
        .intAllowance = cwAllowModify
        
    
        '.intAllowance = cwAllowReadOnly
                        
        Call .FormAddOrderField("FR19CODNECESVAL", cwAscending)
        Call .FormAddOrderField("FR20NUMLINEA", cwAscending)
        
        strKey = .strDataBase & .strTable
        
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Necesidades")
        Call .FormAddFilterWhere(strKey, "FR55CODNECESVAL", "C�d Necesidad Validado", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR20NUMLINEA", "N�mero de L�nea", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d. Producto", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "", "Producto", cwString)
        Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "", "Unidad Medida", cwString)
        Call .FormAddFilterWhere(strKey, "FR20CANTNECESQUIR", "Cantidad", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR19FECVALIDACION", "Fecha Validaci�n", cwDate)
        Call .FormAddFilterWhere(strKey, "SG02COD_VAL", "Persona Valida", cwNumeric)
        Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA_SUM", "C�d Unidad Medida Sum", cwDate)
        
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR55CODNECESUNID", "C�d Necesidad Unidad")
        Call .FormAddFilterOrder(strKey, "FR20NUMLINEA", "N�mero de L�nea")
        Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�d. Producto")
        'Call .FormAddFilterOrder(strKey, "", "Producto")
        Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA", "C�d Unidad Medida")
        'Call .FormAddFilterOrder(strKey, "", "Unidad Medida")
        Call .FormAddFilterOrder(strKey, "FR20CANTNECESQUIR", "Cantidad")
        Call .FormAddFilterOrder(strKey, "FR19FECVALIDACION", "Fecha Validaci�n")
        Call .FormAddFilterOrder(strKey, "SG02COD_VAL", "Persona Valida")
        Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA_SUM", "C�d Unidad Medida Sum")
        
        Call .FormAddRelation("FR55CODNECESUNID", txttext1(0))
    End With

    With objWinInfo
        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
        Call .GridAddColumn(objMultiInfo, "C�d. Necesidad", "FR55CODNECESUNID", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "L�nea", "FR20NUMLINEA", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "C�d. Prod", "FR73CODPRODUCTO", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Producto", "", cwNumeric, 7)
        Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
        Call .GridAddColumn(objMultiInfo, "C�d.", "FR93CODUNIMEDIDA", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo, "Unid.Medida", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Cantidad", "FR19CANTNECESQUIR", cwNumeric, 11)
        Call .GridAddColumn(objMultiInfo, "Fecha Validaci�n", "FR19FECVALIDACION", cwDate)
        Call .GridAddColumn(objMultiInfo, "C�d.Val", "SG02COD_VAL", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Valida", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d.Sum", "FR93CODUNIMEDIDA", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo, "Unid.Medida Sum", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Cant Sum", "FR19CANTSUMIFARM", cwNumeric, 11)
        
        Call .FormCreateInfo(objMasterInfo)
    
        ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        '.CtrlGetInfo(grdDBGrid1(1).Columns(4)).intKeyNo = 1
        'Se indica que campos son obligatorios y cuales son clave primaria
        .CtrlGetInfo(txttext1(0)).intKeyNo = 1

        Call .FormChangeColor(objMultiInfo)
    
        '.CtrlGetInfo(txttext1(0)).blnInFind = True
        '.CtrlGetInfo(txttext1(1)).blnInFind = True
        '.CtrlGetInfo(txttext1(2)).blnInFind = True
        '.CtrlGetInfo(txttext1(3)).blnInFind = True
        '.CtrlGetInfo(txttext1(4)).blnInFind = True
        '.CtrlGetInfo(txttext1(5)).blnInFind = True
        '.CtrlGetInfo(txttext1(6)).blnInFind = True
        '.CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True

        Call .CtrlCreateLinked(.CtrlGetInfo(txttext1(3)), "SG02COD_PDS", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txttext1(3)), txttext1(4), "SG02APE1")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txttext1(5)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txttext1(5)), txttext1(6), "AD02DESDPTO")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txttext1(1)), "FR26CODESTPETIC", "SELECT * FROM FR2600 WHERE FR26CODESTPETIC = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txttext1(1)), txttext1(2), "FR26DESESTADOPET")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "FR73CODINTFAR")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(7), "FR73DESPRODUCTO")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(9), "FR93DESUNIMEDIDA")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(12)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(12)), grdDBGrid1(1).Columns(13), "SG02APE1")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(14)), "FR93CODUNIMEDIDA_SUM", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(14)), grdDBGrid1(1).Columns(15), "FR93DESUNIMEDIDA")
        
    '.CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnForeign = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnReadOnly = True
        '.CtrlGetInfo(txttext1(3)).blnForeign = True
        '.CtrlGetInfo(txttext1(5)).blnForeign = True
        
        
        Call .WinRegister
        Call .WinStabilize
    End With
    grdDBGrid1(1).Columns(4).Width = 800
    grdDBGrid1(1).Columns(5).Width = 1000
    grdDBGrid1(1).Columns(6).Width = 1000
    grdDBGrid1(1).Columns(7).Width = 3400
    grdDBGrid1(1).Columns(8).Width = 800
    grdDBGrid1(1).Columns(9).Width = 2500
    grdDBGrid1(1).Columns(10).Width = 1000
    grdDBGrid1(1).Columns(3).Visible = False
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    Dim i As Integer
    Dim sqlstr As String
    Dim rsta As rdoResultset
    
    If Index = 1 Then
        If grdDBGrid1(1).Columns(3).Value <> "" And grdDBGrid1(1).Columns(4).Value <> "" Then
            sqlstr = "SELECT count(*) from fr2000 where fr55codnecesunid=" & grdDBGrid1(1).Columns(3).Value & _
                     " and fr20numlinea=" & grdDBGrid1(1).Columns(4).Value & _
                     " and fr20indbloqueo=-1"
            Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
            If rsta(0).Value > 0 Then
                For i = 3 To 10
                    grdDBGrid1(1).Columns(i).CellStyleSet "Bloqueada"
                Next i
            End If
          End If
    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
 ' Dim intReport As Integer
 ' Dim objPrinter As clsCWPrinter
 ' Dim blnHasFilter As Boolean
 '
 ' If strFormName = "Estupefacientes" Then
 '   Call objWinInfo.FormPrinterDialog(True, "")
 '   Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
 '   intReport = objPrinter.Selected
 '   If intReport > 0 Then
 '     blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
 '     Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
 '                                objWinInfo.DataGetOrder(blnHasFilter, True))
 '   End If
 '   Set objPrinter = Nothing
 ' End If
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    'Dim intMsg As Integer
   '
   ' If intIndex = 4 Then
   '     intMsg = MsgBox("�Est� seguro de la devoluci�n de este envase?", vbQuestion + vbYesNo)
   '     If intMsg = vbNo Then
   '         Exit Sub
   '     End If
   '     If grdDBGrid1(1).Columns(3).Value = False Then
   '         If grdDBGrid1(1).Columns(4).Value = "" Then
   '             Call MsgBox("Debe indicar la Cantidad Devuelta", vbInformation)
   '             Exit Sub
   '         End If
   '     Else
   '        Call MsgBox("Debe indicar que se ha devuelto el envase", vbInformation)
   '        Exit Sub
   '     End If
   ' End If
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Dim vntDatos(1 To 2) As Variant
    
    Call objWinInfo.GridDblClick
    If grdDBGrid1(1).Rows > 0 Then
        vntDatos(1) = "FrmRedPedStPlanta"
        vntDatos(2) = grdDBGrid1(1).Columns(5).Value
        Call objsecurity.LaunchProcess("FR0003", vntDatos)
    End If
End Sub
Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
Dim stra As String
Dim rsta As rdoResultset
    
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
    If intIndex = 1 And grdDBGrid1(1).Columns(5).Value <> "" And grdDBGrid1(1).Columns(8).Value = "" Then
      stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(1).Columns(5).Value
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(8), rsta(0).Value)
      rsta.Close
      Set rsta = Nothing
    End If
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


