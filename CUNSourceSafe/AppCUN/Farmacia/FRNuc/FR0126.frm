VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmRedHojQuirof 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Redactar Hoja de Quirofano"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   57
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   56
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab tabGeneral1 
      Height          =   7455
      Index           =   1
      Left            =   240
      TabIndex        =   58
      TabStop         =   0   'False
      Top             =   480
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   13150
      _Version        =   327681
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DATOS GENERALES"
      TabPicture(0)   =   "FR0126.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "INTERVENCIONES"
      TabPicture(1)   =   "FR0126.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(1)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "MATERIAL Y MEDICACION"
      TabPicture(2)   =   "FR0126.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraFrame1(2)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "OTROS CONSUMOS"
      TabPicture(3)   =   "FR0126.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraFrame1(3)"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "cmdbuscargruprod"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "cmdbuscarprod"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).Control(3)=   "cmdinter"
      Tab(3).Control(3).Enabled=   0   'False
      Tab(3).ControlCount=   4
      Begin VB.CommandButton cmdinter 
         Caption         =   "Interacciones"
         Height          =   495
         Left            =   -64800
         TabIndex        =   101
         Top             =   4080
         Width           =   1215
      End
      Begin VB.CommandButton cmdbuscarprod 
         Caption         =   "Productos"
         Height          =   495
         Left            =   -64800
         TabIndex        =   99
         Top             =   3240
         Width           =   1215
      End
      Begin VB.CommandButton cmdbuscargruprod 
         Caption         =   "Grupo Productos"
         Height          =   495
         Left            =   -64800
         TabIndex        =   98
         Top             =   2400
         Width           =   1215
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6840
         Index           =   3
         Left            =   -74880
         TabIndex        =   93
         Top             =   360
         Width           =   9975
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6315
            Index           =   3
            Left            =   120
            TabIndex        =   94
            Top             =   360
            Width           =   9705
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   17119
            _ExtentY        =   11139
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6840
         Index           =   1
         Left            =   -74760
         TabIndex        =   88
         Top             =   360
         Width           =   11055
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6315
            Index           =   1
            Left            =   120
            TabIndex        =   89
            Top             =   360
            Width           =   10785
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19024
            _ExtentY        =   11139
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6840
         Index           =   2
         Left            =   -74760
         TabIndex        =   70
         Top             =   360
         Width           =   11055
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6315
            Index           =   2
            Left            =   120
            TabIndex        =   71
            Top             =   360
            Width           =   10785
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19024
            _ExtentY        =   11139
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6975
         Index           =   0
         Left            =   120
         TabIndex        =   59
         Top             =   360
         Width           =   11220
         Begin TabDlg.SSTab tabTab1 
            Height          =   6495
            Index           =   0
            Left            =   120
            TabIndex        =   60
            TabStop         =   0   'False
            Top             =   360
            Width           =   10935
            _ExtentX        =   19288
            _ExtentY        =   11456
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "FR0126.frx":0070
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(16)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(15)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(14)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(28)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(12)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(18)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(19)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(22)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(23)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(29)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(30)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "dtcDateCombo1(0)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "tab1"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(2)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "chkCheck1(1)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "chkCheck1(0)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "chkCheck1(2)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(3)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(0)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(7)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(5)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(6)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(8)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(4)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txtText1(9)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "txtText1(1)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).Control(26)=   "txtText1(43)"
            Tab(0).Control(26).Enabled=   0   'False
            Tab(0).Control(27)=   "txtText1(44)"
            Tab(0).Control(27).Enabled=   0   'False
            Tab(0).Control(28)=   "cmdFirmar"
            Tab(0).Control(28).Enabled=   0   'False
            Tab(0).ControlCount=   29
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "FR0126.frx":008C
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(0)"
            Tab(1).ControlCount=   1
            Begin VB.CommandButton cmdFirmar 
               Caption         =   "Firmar"
               Height          =   495
               Left            =   6600
               TabIndex        =   102
               Top             =   1920
               Width           =   1215
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   44
               Left            =   2880
               TabIndex        =   2
               TabStop         =   0   'False
               Tag             =   "Descripci�n Quir�fano"
               Top             =   360
               Width           =   4125
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   43
               Left            =   7200
               TabIndex        =   15
               TabStop         =   0   'False
               Tag             =   "Apellido 1�"
               Top             =   1440
               Width           =   2925
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR97CODQUIROFANO"
               Height          =   330
               Index           =   1
               Left            =   1680
               TabIndex        =   1
               Tag             =   "C�d. Quir�fano"
               Top             =   360
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               DataField       =   "SG02COD"
               Height          =   330
               Index           =   9
               Left            =   6000
               TabIndex        =   14
               Tag             =   "Firma Digital"
               Top             =   1440
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR44HIND"
               Height          =   330
               Index           =   4
               Left            =   120
               TabIndex        =   9
               Tag             =   "H. Ind."
               Top             =   2280
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR44DURACINTERV"
               Height          =   330
               Index           =   8
               Left            =   3480
               TabIndex        =   13
               Tag             =   "Duraci�n Intervenci�n en Minutos"
               Top             =   2880
               Width           =   1095
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR44HINC"
               Height          =   330
               Index           =   6
               Left            =   120
               TabIndex        =   11
               Tag             =   "H. Inc."
               Top             =   2880
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR44HORAFINANEST"
               Height          =   330
               Index           =   5
               Left            =   1560
               TabIndex        =   10
               Tag             =   "Hora Fin Anestesia"
               Top             =   2280
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR44HORAFININTERV"
               Height          =   330
               Index           =   7
               Left            =   1560
               TabIndex        =   12
               Tag             =   "Hora Fin Intervenci�n"
               Top             =   2880
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR44CODHOJAQUIRO"
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   0
               Tag             =   "C�d. Hoja Quir�fano"
               Top             =   360
               Width           =   1100
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR44NUMORDENDIA"
               Height          =   330
               Index           =   3
               Left            =   120
               TabIndex        =   5
               Tag             =   "N�mero Orden en el D�a"
               Top             =   1680
               Width           =   615
            End
            Begin VB.CheckBox chkCheck1 
               Alignment       =   1  'Right Justify
               Caption         =   "Urg."
               DataField       =   "FR44INDURGENTE"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   3960
               TabIndex        =   8
               Tag             =   "Intervenci�n Urgente?"
               Top             =   1680
               Width           =   735
            End
            Begin VB.CheckBox chkCheck1 
               Alignment       =   1  'Right Justify
               Caption         =   "Programada"
               DataField       =   "FR44INDPROGRAMADA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   1245
               TabIndex        =   6
               Tag             =   "Intervenci�n Programada?"
               Top             =   1680
               Width           =   1335
            End
            Begin VB.CheckBox chkCheck1 
               Alignment       =   1  'Right Justify
               Caption         =   "Imprev."
               DataField       =   "FR44INDIMPREVISTA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   2760
               TabIndex        =   7
               Tag             =   "Intervenci�n Prevista?"
               Top             =   1680
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR44HORAINTERV"
               Height          =   330
               Index           =   2
               Left            =   2160
               TabIndex        =   4
               Tag             =   "Hora Intervenci�n"
               Top             =   1080
               Width           =   612
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   5865
               Index           =   0
               Left            =   -74760
               TabIndex        =   61
               TabStop         =   0   'False
               Top             =   240
               Width           =   10215
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   18018
               _ExtentY        =   10345
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin TabDlg.SSTab tab1 
               Height          =   3015
               Left            =   120
               TabIndex        =   62
               TabStop         =   0   'False
               Top             =   3360
               Width           =   10215
               _ExtentX        =   18018
               _ExtentY        =   5318
               _Version        =   327681
               Style           =   1
               Tabs            =   5
               TabsPerRow      =   5
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Paciente"
               TabPicture(0)   =   "FR0126.frx":00A8
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(11)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(4)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "lblLabel1(7)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "lblLabel1(8)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "lblLabel1(9)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "lblLabel1(31)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "cboDBCombo1(1)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).Control(7)=   "txtText1(11)"
               Tab(0).Control(7).Enabled=   0   'False
               Tab(0).Control(8)=   "txtText1(12)"
               Tab(0).Control(8).Enabled=   0   'False
               Tab(0).Control(9)=   "txtText1(10)"
               Tab(0).Control(9).Enabled=   0   'False
               Tab(0).Control(10)=   "txtText1(13)"
               Tab(0).Control(10).Enabled=   0   'False
               Tab(0).Control(11)=   "txtText1(14)"
               Tab(0).Control(11).Enabled=   0   'False
               Tab(0).ControlCount=   12
               TabCaption(1)   =   "Equipo Quirurgico"
               TabPicture(1)   =   "FR0126.frx":00C4
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "lblLabel1(27)"
               Tab(1).Control(0).Enabled=   0   'False
               Tab(1).Control(1)=   "lblLabel1(20)"
               Tab(1).Control(1).Enabled=   0   'False
               Tab(1).Control(2)=   "lblLabel1(0)"
               Tab(1).Control(2).Enabled=   0   'False
               Tab(1).Control(3)=   "lblLabel1(2)"
               Tab(1).Control(3).Enabled=   0   'False
               Tab(1).Control(4)=   "lblLabel1(3)"
               Tab(1).Control(4).Enabled=   0   'False
               Tab(1).Control(5)=   "lblLabel1(5)"
               Tab(1).Control(5).Enabled=   0   'False
               Tab(1).Control(6)=   "lblLabel1(6)"
               Tab(1).Control(6).Enabled=   0   'False
               Tab(1).Control(7)=   "lblLabel1(10)"
               Tab(1).Control(7).Enabled=   0   'False
               Tab(1).Control(8)=   "txtText1(21)"
               Tab(1).Control(8).Enabled=   0   'False
               Tab(1).Control(9)=   "txtText1(15)"
               Tab(1).Control(9).Enabled=   0   'False
               Tab(1).Control(10)=   "txtText1(16)"
               Tab(1).Control(10).Enabled=   0   'False
               Tab(1).Control(11)=   "txtText1(19)"
               Tab(1).Control(11).Enabled=   0   'False
               Tab(1).Control(12)=   "txtText1(20)"
               Tab(1).Control(12).Enabled=   0   'False
               Tab(1).Control(13)=   "txtText1(23)"
               Tab(1).Control(13).Enabled=   0   'False
               Tab(1).Control(14)=   "txtText1(24)"
               Tab(1).Control(14).Enabled=   0   'False
               Tab(1).Control(15)=   "txtText1(22)"
               Tab(1).Control(15).Enabled=   0   'False
               Tab(1).Control(16)=   "txtText1(25)"
               Tab(1).Control(16).Enabled=   0   'False
               Tab(1).Control(17)=   "txtText1(26)"
               Tab(1).Control(17).Enabled=   0   'False
               Tab(1).Control(18)=   "txtText1(28)"
               Tab(1).Control(18).Enabled=   0   'False
               Tab(1).Control(19)=   "txtText1(29)"
               Tab(1).Control(19).Enabled=   0   'False
               Tab(1).Control(20)=   "txtText1(27)"
               Tab(1).Control(20).Enabled=   0   'False
               Tab(1).Control(21)=   "txtText1(17)"
               Tab(1).Control(21).Enabled=   0   'False
               Tab(1).Control(22)=   "txtText1(18)"
               Tab(1).Control(22).Enabled=   0   'False
               Tab(1).ControlCount=   23
               TabCaption(2)   =   "Equipo Colaborador"
               TabPicture(2)   =   "FR0126.frx":00E0
               Tab(2).ControlEnabled=   0   'False
               Tab(2).Control(0)=   "txtText1(36)"
               Tab(2).Control(1)=   "txtText1(37)"
               Tab(2).Control(2)=   "txtText1(33)"
               Tab(2).Control(3)=   "txtText1(32)"
               Tab(2).Control(4)=   "txtText1(35)"
               Tab(2).Control(5)=   "txtText1(34)"
               Tab(2).Control(6)=   "txtText1(31)"
               Tab(2).Control(7)=   "txtText1(30)"
               Tab(2).Control(8)=   "lblLabel1(26)"
               Tab(2).Control(9)=   "lblLabel1(25)"
               Tab(2).Control(10)=   "lblLabel1(24)"
               Tab(2).Control(11)=   "lblLabel1(17)"
               Tab(2).Control(12)=   "lblLabel1(13)"
               Tab(2).ControlCount=   13
               TabCaption(3)   =   "Servicio"
               TabPicture(3)   =   "FR0126.frx":00FC
               Tab(3).ControlEnabled=   0   'False
               Tab(3).Control(0)=   "lblLabel1(1)"
               Tab(3).Control(0).Enabled=   0   'False
               Tab(3).Control(1)=   "txtText1(38)"
               Tab(3).Control(1).Enabled=   0   'False
               Tab(3).Control(2)=   "txtText1(39)"
               Tab(3).Control(2).Enabled=   0   'False
               Tab(3).ControlCount=   3
               TabCaption(4)   =   "Indicativos"
               TabPicture(4)   =   "FR0126.frx":0118
               Tab(4).ControlEnabled=   0   'False
               Tab(4).Control(0)=   "txtText1(42)"
               Tab(4).Control(1)=   "chkCheck1(11)"
               Tab(4).Control(2)=   "chkCheck1(3)"
               Tab(4).Control(3)=   "chkCheck1(4)"
               Tab(4).Control(4)=   "chkCheck1(5)"
               Tab(4).Control(5)=   "chkCheck1(6)"
               Tab(4).Control(6)=   "chkCheck1(7)"
               Tab(4).Control(7)=   "chkCheck1(8)"
               Tab(4).Control(8)=   "chkCheck1(9)"
               Tab(4).Control(9)=   "chkCheck1(10)"
               Tab(4).ControlCount=   10
               Begin VB.TextBox txtText1 
                  DataField       =   "FR44HORAINICOL"
                  Height          =   330
                  Index           =   36
                  Left            =   -74040
                  TabIndex        =   42
                  Tag             =   "Hora Inicio Colaboraci�n"
                  Top             =   2160
                  Width           =   612
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR44HORAFINCOL"
                  Height          =   330
                  Index           =   37
                  Left            =   -72240
                  TabIndex        =   43
                  Tag             =   "Hora Fin Colaboraci�n"
                  Top             =   2160
                  Width           =   612
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   33
                  Left            =   -69120
                  TabIndex        =   39
                  Tag             =   "Descripci�n Departamento"
                  Top             =   720
                  Width           =   3285
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "AD02CODDPTO_CCL"
                  Height          =   330
                  Index           =   32
                  Left            =   -70320
                  TabIndex        =   38
                  Tag             =   "Departamento Cirujano Colaborador"
                  Top             =   720
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   35
                  Left            =   -73680
                  TabIndex        =   41
                  Tag             =   "Apellido Ayudante Colaborador"
                  Top             =   1320
                  Width           =   3285
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_ACL"
                  Height          =   330
                  Index           =   34
                  Left            =   -74880
                  TabIndex        =   40
                  Tag             =   "C�digo Ayudante Colaborador"
                  Top             =   1320
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   31
                  Left            =   -73680
                  TabIndex        =   37
                  Tag             =   "Apellido Cirujano Colaborador"
                  Top             =   720
                  Width           =   3285
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_CCL"
                  Height          =   330
                  Index           =   30
                  Left            =   -74880
                  TabIndex        =   36
                  Tag             =   "C�digo Cirujano Colaborador"
                  Top             =   720
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   18
                  Left            =   -69120
                  TabIndex        =   24
                  Tag             =   "Descripci�n Departamento"
                  Top             =   600
                  Width           =   3285
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "AD02CODDPTO_CIR"
                  Height          =   330
                  Index           =   17
                  Left            =   -70320
                  TabIndex        =   23
                  Tag             =   "C�digo Departamento Cirujano"
                  Top             =   600
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR43CODHOJAANEST"
                  Height          =   330
                  Index           =   27
                  Left            =   -74880
                  TabIndex        =   33
                  Tag             =   "C�digo Hoja Anestesia"
                  Top             =   2400
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   29
                  Left            =   -72240
                  TabIndex        =   35
                  Tag             =   "Apellido Anestesista"
                  Top             =   2400
                  Width           =   3285
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   28
                  Left            =   -73440
                  TabIndex        =   34
                  Tag             =   "C�digo Anestesista"
                  Top             =   2400
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   26
                  Left            =   -69120
                  TabIndex        =   32
                  Tag             =   "Apellido Enfermera"
                  Top             =   1800
                  Width           =   3285
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_ENF"
                  Height          =   330
                  Index           =   25
                  Left            =   -70320
                  TabIndex        =   31
                  Tag             =   "C�digo Enfermera"
                  Top             =   1800
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   22
                  Left            =   -69120
                  TabIndex        =   28
                  Tag             =   "Apellido Instrumentista"
                  Top             =   1200
                  Width           =   3285
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   24
                  Left            =   -73680
                  TabIndex        =   30
                  Tag             =   "Apellido Segundo Ayudante"
                  Top             =   1800
                  Width           =   3285
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_SAY"
                  Height          =   330
                  Index           =   23
                  Left            =   -74880
                  TabIndex        =   29
                  Tag             =   "C�digo Segundo Ayudante"
                  Top             =   1800
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   20
                  Left            =   -73680
                  TabIndex        =   26
                  Tag             =   "Apellido Primer Ayudante"
                  Top             =   1200
                  Width           =   3285
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_PAY"
                  Height          =   330
                  Index           =   19
                  Left            =   -74880
                  TabIndex        =   25
                  Tag             =   "C�digo Primer Ayudante"
                  Top             =   1200
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   14
                  Left            =   5880
                  TabIndex        =   20
                  TabStop         =   0   'False
                  Tag             =   "Apellido 2� Paciente"
                  Top             =   1680
                  Width           =   2925
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   13
                  Left            =   2880
                  TabIndex        =   19
                  TabStop         =   0   'False
                  Tag             =   "Apellido 1� Paciente"
                  Top             =   1680
                  Width           =   2925
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "CI21CODPERSONA"
                  Height          =   330
                  Index           =   10
                  Left            =   240
                  TabIndex        =   16
                  Tag             =   "C�digo Paciente"
                  Top             =   960
                  Width           =   1500
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   12
                  Left            =   240
                  TabIndex        =   18
                  TabStop         =   0   'False
                  Tag             =   "Nombre Paciente"
                  Top             =   1680
                  Width           =   2565
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   11
                  Left            =   2280
                  TabIndex        =   17
                  TabStop         =   0   'False
                  Tag             =   "C�d. Historia Paciente"
                  Top             =   960
                  Width           =   1500
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR44DESOTROS"
                  Height          =   690
                  Index           =   42
                  Left            =   -69960
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   55
                  Tag             =   "Descripci�n otros Indicativos"
                  Top             =   1080
                  Width           =   3495
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Otros"
                  DataField       =   "FR44INDOTROS"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   -70200
                  TabIndex        =   54
                  Tag             =   "Otros Indicativos?"
                  Top             =   720
                  Width           =   855
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Anat. patol�gica"
                  DataField       =   "FR44INDANATPATO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   -74640
                  TabIndex        =   46
                  Tag             =   "Intervenci�n de Anatom�a Patol�gica?"
                  Top             =   720
                  Width           =   1815
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Radioterapia"
                  DataField       =   "FR44INDRADIOTERAP"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   -74640
                  TabIndex        =   47
                  Tag             =   "Intervenci�n de Radioterapia?"
                  Top             =   1080
                  Width           =   1815
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Endoscopia"
                  DataField       =   "FR44INDENDOSCOPIA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   5
                  Left            =   -74640
                  TabIndex        =   48
                  Tag             =   "Intervenci�n de Endoscopia?"
                  Top             =   1440
                  Width           =   1815
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Microsc."
                  DataField       =   "FR44INDMICROSCOPIA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   6
                  Left            =   -74640
                  TabIndex        =   49
                  Tag             =   "Intervenci�n de Microscop�a?"
                  Top             =   1800
                  Width           =   1815
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Rx"
                  DataField       =   "FR44INDRX"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   7
                  Left            =   -72240
                  TabIndex        =   50
                  Tag             =   "Rayos X?"
                  Top             =   720
                  Width           =   1335
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Laser"
                  DataField       =   "FR44INDLASER"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   -72240
                  TabIndex        =   51
                  Tag             =   "Laser?"
                  Top             =   1080
                  Width           =   1335
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "C.E.C."
                  DataField       =   "FR44INDCEC"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   9
                  Left            =   -72240
                  TabIndex        =   52
                  Tag             =   "Intervencion C.E.C.?"
                  Top             =   1440
                  Width           =   1335
               End
               Begin VB.CheckBox chkCheck1 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Cta. gasas"
                  DataField       =   "FR44INDCUANTAGASAS"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   10
                  Left            =   -72240
                  TabIndex        =   53
                  Tag             =   "Cuenta de Gasas?"
                  Top             =   1800
                  Width           =   1335
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   16
                  Left            =   -73680
                  TabIndex        =   22
                  Tag             =   "Apellido Cirujano"
                  Top             =   600
                  Width           =   3285
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_CIR"
                  Height          =   330
                  Index           =   15
                  Left            =   -74880
                  TabIndex        =   21
                  Tag             =   "C�digo Cirujano"
                  Top             =   600
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "SG02COD_INS"
                  Height          =   330
                  Index           =   21
                  Left            =   -70320
                  TabIndex        =   27
                  Tag             =   "C�digo Instrumentista"
                  Top             =   1200
                  Width           =   1155
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   39
                  Left            =   -72360
                  TabIndex        =   45
                  TabStop         =   0   'False
                  Tag             =   "Desc.Servicio"
                  Top             =   1200
                  Width           =   5445
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "AD02CODDPTO"
                  Height          =   330
                  Index           =   38
                  Left            =   -74520
                  TabIndex        =   44
                  Tag             =   "C�d.Servicio"
                  Top             =   1200
                  Width           =   2000
               End
               Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
                  DataField       =   "FRG6CODTIPPERS"
                  Height          =   330
                  Index           =   1
                  Left            =   4830
                  TabIndex        =   103
                  Tag             =   "C�digo Tipo Paciente"
                  Top             =   960
                  Width           =   1005
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets.count =   2
                  stylesets(0).Name=   "Activo"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).Picture=   "FR0126.frx":0134
                  stylesets(1).Name=   "Inactivo"
                  stylesets(1).BackColor=   255
                  stylesets(1).Picture=   "FR0126.frx":0150
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Nombre"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   1773
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 0"
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tipo Paciente"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   31
                  Left            =   4800
                  TabIndex        =   104
                  Top             =   720
                  Width           =   1200
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hora Inicio"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   26
                  Left            =   -74040
                  TabIndex        =   96
                  Top             =   1920
                  Width           =   1575
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hora Fin"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   25
                  Left            =   -72240
                  TabIndex        =   95
                  Top             =   1920
                  Width           =   1575
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "DEPARTAMENTO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   24
                  Left            =   -70320
                  TabIndex        =   85
                  Top             =   480
                  Width           =   1575
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Ayudante"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   17
                  Left            =   -74880
                  TabIndex        =   84
                  Top             =   1080
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Cirujano"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   13
                  Left            =   -74880
                  TabIndex        =   83
                  Top             =   480
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "DEPARTAMENTO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   10
                  Left            =   -70320
                  TabIndex        =   82
                  Top             =   360
                  Width           =   1575
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Hoja Anestesia"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   6
                  Left            =   -74880
                  TabIndex        =   81
                  Top             =   2160
                  Width           =   1335
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Anestesista"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   5
                  Left            =   -73440
                  TabIndex        =   80
                  Top             =   2160
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Enfermera"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   -70320
                  TabIndex        =   79
                  Top             =   1560
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Segundo Ayudante"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   2
                  Left            =   -74880
                  TabIndex        =   78
                  Top             =   1560
                  Width           =   1695
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Primer Ayudante"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   0
                  Left            =   -74880
                  TabIndex        =   77
                  Top             =   960
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Apellido 2�"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   9
                  Left            =   5880
                  TabIndex        =   76
                  Top             =   1440
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Apellido 1�"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   2880
                  TabIndex        =   75
                  Top             =   1440
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   7
                  Left            =   240
                  TabIndex        =   74
                  Top             =   1440
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Persona"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   240
                  TabIndex        =   73
                  Top             =   720
                  Width           =   1455
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Historia "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   2280
                  TabIndex        =   72
                  Top             =   720
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Cirujano"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   20
                  Left            =   -74880
                  TabIndex        =   65
                  Top             =   360
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Instrumentista"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   27
                  Left            =   -70320
                  TabIndex        =   64
                  Top             =   960
                  Width           =   1335
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Servicio"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   1
                  Left            =   -74520
                  TabIndex        =   63
                  Top             =   960
                  Width           =   2055
               End
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR44FECINTERVEN"
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   3
               Tag             =   "Fecha Intervenci�n"
               Top             =   1080
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d. Quir�fano"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   30
               Left            =   1680
               TabIndex        =   100
               Top             =   120
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Firma Digital"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   29
               Left            =   6000
               TabIndex        =   97
               Top             =   1200
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Duraci�n intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   23
               Left            =   3480
               TabIndex        =   92
               Top             =   2640
               Width           =   1935
            End
            Begin VB.Label lblLabel1 
               Caption         =   "H. Ind."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   22
               Left            =   120
               TabIndex        =   91
               Top             =   2040
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               Caption         =   "H. Inc."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   19
               Left            =   120
               TabIndex        =   90
               Top             =   2640
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Final anestesia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   1560
               TabIndex        =   87
               Top             =   2040
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Final intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   1560
               TabIndex        =   86
               Top             =   2640
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hoja Quir�fano"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   28
               Left            =   120
               TabIndex        =   69
               Top             =   120
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N�. ord./d�a"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   120
               TabIndex        =   68
               Top             =   1440
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fecha Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   120
               TabIndex        =   67
               Top             =   840
               Width           =   1815
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Hora Intervenci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   2160
               TabIndex        =   66
               Top             =   840
               Width           =   1575
            End
         End
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRedHojQuirof"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRedHojQuirof (FR0126.FRM)                                 *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: Redactar Hoja Quir�fano                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdbuscargruprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdbuscargruprod.Enabled = False

  If txtText1(0).Text <> "" Then
    noinsertar = True
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    Call objsecurity.LaunchProcess("FR0120")
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    If gintprodtotal > 0 Then
      For v = 0 To gintprodtotal - 1
        'se mira que no se inserte 2 veces el mismo producto
        If grdDBGrid1(3).Rows > 0 Then
          grdDBGrid1(3).MoveFirst
          For i = 0 To grdDBGrid1(3).Rows - 1
            If grdDBGrid1(3).Columns(5).Value = gintprodbuscado(v, 0) _
               And grdDBGrid1(3).Columns(0).Value <> "Eliminado" Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(3).MoveNext
          Next i
        End If
        If grdDBGrid1(2).Rows > 0 And noinsertar = True Then
          grdDBGrid1(2).MoveFirst
          For i = 0 To grdDBGrid1(2).Rows - 1
            If grdDBGrid1(2).Columns(5).Value = gintprodbuscado(v, 0) Then
              'no se inserta el producto pues ya est� insertado en el grid
              noinsertar = False
              Exit For
            Else
              noinsertar = True
            End If
            grdDBGrid1(2).MoveNext
          Next i
        End If
        If noinsertar = True Then
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          grdDBGrid1(3).Columns(5).Value = gintprodbuscado(v, 0)
          grdDBGrid1(3).Columns(6).Value = gintprodbuscado(v, 1)
          grdDBGrid1(3).Columns(7).Value = gintprodbuscado(v, 2)
          stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(3).Columns(5).Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          Call objWinInfo.CtrlSet(grdDBGrid1(3).Columns(9), rsta(0).Value)
          rsta.Close
          Set rsta = Nothing
        Else
          mensaje = MsgBox("El producto: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                             Chr(13), vbInformation)
        End If
        noinsertar = True
      Next v
    End If
    gintprodtotal = 0
  End If

cmdbuscargruprod.Enabled = True

End Sub

Private Sub cmdbuscarprod_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim stra As String
Dim rsta As rdoResultset

cmdbuscarprod.Enabled = False

If txtText1(0).Text <> "" Then
  noinsertar = True
  Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
  Call objsecurity.LaunchProcess("FR0119")
  Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
  If gintprodtotal > 0 Then
    For v = 0 To gintprodtotal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(3).Rows > 0 Then
        grdDBGrid1(3).MoveFirst
        For i = 0 To grdDBGrid1(3).Rows - 1
          If grdDBGrid1(3).Columns(5).Value = gintprodbuscado(v, 0) _
             And grdDBGrid1(3).Columns(0).Value <> "Eliminado" Then
            'no se inserta el producto pues ya est� insertado en el grid
            noinsertar = False
            Exit For
          Else
            noinsertar = True
          End If
          grdDBGrid1(3).MoveNext
        Next i
      End If
      If grdDBGrid1(2).Rows > 0 And noinsertar = True Then
        grdDBGrid1(2).MoveFirst
        For i = 0 To grdDBGrid1(2).Rows - 1
          If grdDBGrid1(2).Columns(5).Value = gintprodbuscado(v, 0) Then
            'no se inserta el producto pues ya est� insertado en el grid
            noinsertar = False
            Exit For
          Else
            noinsertar = True
          End If
          grdDBGrid1(2).MoveNext
        Next i
      End If
      If noinsertar = True Then
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
        grdDBGrid1(3).Columns(5).Value = gintprodbuscado(v, 0)  'cod.producto
        grdDBGrid1(3).Columns(6).Value = gintprodbuscado(v, 1) 'cod.interno
        grdDBGrid1(3).Columns(7).Value = gintprodbuscado(v, 2) 'des.producto
        stra = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO = " & grdDBGrid1(3).Columns(5).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        Call objWinInfo.CtrlSet(grdDBGrid1(3).Columns(9), rsta(0).Value)
        rsta.Close
        Set rsta = Nothing
      Else
        mensaje = MsgBox("El producto: " & gintprodbuscado(v, 0) & " ya est� guardado." & _
                  Chr(13), vbInformation)
      End If
      noinsertar = True
    Next v
  End If
  gintprodtotal = 0
End If

cmdbuscarprod.Enabled = True

End Sub

Private Sub cmdfirmar_Click()
Dim rstcantidad As rdoResultset
Dim strcantidad As String
Dim strInsert As String
Dim rstSelect As rdoResultset
Dim strSelect As String
Dim persona, producto, fecha, hora, servicio, cantreceta, indquirofano

  If txtText1(9).Text = "" Then
    
    strcantidad = "SELECT MAX(FR18CANTCONSUMIDA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
               txtText1(0).Text
    Set rstcantidad = objApp.rdoConnect.OpenResultset(strcantidad)
    If IsNull(rstcantidad(0).Value) Then
      MsgBox "No se puede firmar, ya que no ha rellenado la lista de productos."
    Else
      If rstcantidad(0).Value = 0 Then
        MsgBox "No se puede firmar, ya que no ha rellenado la lista de productos."
      Else
        strSelect = "SELECT FR73CODPRODUCTO,FR18CANTCONSUMIDA FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
               txtText1(0).Text & " AND FR18CANTCONSUMIDA>0 "
        Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
        While Not rstSelect.EOF
          persona = txtText1(10).Text
          producto = rstSelect("FR73CODPRODUCTO").Value
          fecha = "TO_DATE('" & dtcDateCombo1(0) & "','DD/MM/YYYY')"
          If InStr(txtText1(2).Text, ",") > 0 Then
            hora = Left(txtText1(2).Text, InStr(txtText1(2).Text, ",") - 1) & "." & Right(txtText1(2).Text, Len(txtText1(2).Text) - InStr(txtText1(2).Text, ","))
          Else
            hora = txtText1(2).Text
          End If
          servicio = txtText1(38).Text
          cantreceta = rstSelect("FR18CANTCONSUMIDA").Value
          indquirofano = -1
          strInsert = "INSERT INTO FR6500 (CI21CODPERSONA,FR73CODPRODUCTO,FR65FECHA,FR65HORA,AD02CODDPTO,FR65CANTRECETA,FR65INDQUIROFANO)" & _
                      " VALUES (" & persona & "," & producto & "," & fecha & "," & hora & "," & servicio & "," & cantreceta & "," & indquirofano & ")"
          objApp.rdoConnect.Execute strInsert, 64
          objApp.rdoConnect.Execute "Commit", 64
          rstSelect.MoveNext
        Wend
        rstSelect.Close
        Set rstSelect = Nothing
        txtText1(9).SetFocus
        Call objWinInfo.CtrlSet(txtText1(9), objsecurity.strUser)
        Call objWinInfo.DataSave
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        Call objWinInfo.DataRefresh
      End If
    End If
    rstcantidad.Close
    Set rstcantidad = Nothing
  Else
    MsgBox "La Hoja de Quir�fano ya esta firmada."
  End If

End Sub

Private Sub cmdinter_Click()
Dim rsta As rdoResultset
Dim stra As String
Dim rstprin As rdoResultset
Dim strprin As String
Dim i As Integer

cmdinter.Enabled = False
        
  gstrLista = ""
  'se cogen todos los principios activos de los productos que est�n en el grid y se
  'meten en una lista
  grdDBGrid1(3).MoveFirst
  For i = 0 To grdDBGrid1(3).Rows - 1
    stra = "SELECT count(*) FROM FR6400 WHERE FR73CODPRODUCTO=" & _
    grdDBGrid1(3).Columns(5).Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value > 0 Then
      strprin = "SELECT FR68CODPRINCACTIV FROM FR6400 WHERE FR73CODPRODUCTO=" & _
                grdDBGrid1(3).Columns(5).Value
      Set rstprin = objApp.rdoConnect.OpenResultset(strprin)
      If gstrLista = "" Then
        gstrLista = gstrLista & rstprin.rdoColumns(0).Value
      Else
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
      End If
      rstprin.MoveNext
      While Not rstprin.EOF
        gstrLista = gstrLista & "," & rstprin.rdoColumns(0).Value
        rstprin.MoveNext
      Wend
      rstprin.Close
      Set rstprin = Nothing
    End If
    rsta.Close
    Set rsta = Nothing
    grdDBGrid1(3).MoveNext
  Next i
  
  If gstrLista <> "" Then
    gstrLista = "(" & gstrLista & ")"
    Call objsecurity.LaunchProcess("FR0116")
  End If

cmdinter.Enabled = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim objMultiInfo2 As New clsCWForm
  Dim objMultiInfo3 As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Hoja Quirofano"
    
    .strTable = "FR4400"
    
    Call .FormAddOrderField("FR44CODHOJAQUIRO", cwAscending)
   
'vntdatos(1) = "frmFirmarHojQuirof" 'gstrllamador
'vntdatos(2) = frmFirmarHojQuirof.grdDBGrid1(0).Columns(3).Value 'gintcodhojaquiro
  
    If gstrllamador = "frmFirmarHojQuirof" Then
      .strWhere = "FR44CODHOJAQUIRO=" & gintcodhojaquiro & " AND SG02COD IS NULL " 'y NO FIRMADA
      .intAllowance = cwAllowModify
    Else
      .strWhere = "SG02COD IS NULL "
    End If
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Hoja Quirofano")
    Call .FormAddFilterWhere(strKey, "FR44CODHOJAQUIRO", "C�digo Hoja Quir�fano", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR97CODQUIROFANO", "C�digo Quir�fano", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR44FECINTERVEN", "Fecha Intervenci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR44HORAINTERV", "Hora Intervenci�n", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR44NUMORDENDIA", "N�mero Orden en el D�a", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR44INDPROGRAMADA", "Indicador Programada", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR44INDIMPREVISTA", "Indicador Imprevista", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR44INDURGENTE", "Indicador Urgente", cwBoolean)
    Call .FormAddFilterWhere(strKey, "SG02COD", "Firma Digital", cwString)
    Call .FormAddFilterWhere(strKey, "FR44HIND", "H.Ind.", cwString)
    Call .FormAddFilterWhere(strKey, "FR44HORAFINANEST", "H.Final Anestesia", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR44HINC", "H.Inc.", cwString)
    Call .FormAddFilterWhere(strKey, "FR44HORAFININTERV", "H.Final Intervenci�n", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR44DURACINTERV", "Duraci�n Intervenci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_CIR", "Cirujano", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_PAY", "Primer Ayudante", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_SAY", "Segundo Ayudante", cwString)
    Call .FormAddFilterWhere(strKey, "FR43CODHOJAANEST", "C�digo Hoja Anestesia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO_CIR", "Departamento Cirujano", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_INS", "Instrumentista", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_ENF", "Enfermera", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_CCL", "Cirujano Colaborador", cwString)
    Call .FormAddFilterWhere(strKey, "SG02COD_ACL", "Ayudante Colaborador", cwString)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO_CCL", "Departamento Cirujano Colaborador", cwString)
    Call .FormAddFilterWhere(strKey, "FR44HORAINICOL", "Hora Inicio Colaboraci�n", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR44HORAFINCOL", "Hora Fin Colaboraci�n", cwDecimal)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR44INDANATPATO", "Indicador Anatom�a Patol�gica", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR44INDRADIOTERAP", "Indicador Radio Terapia", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR44INDENDOSCOPIA", "Indicador Endoscopia", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR44INDMICROSCOPIA", "Indicador Microscop�a", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR44INDRX", "Indicador RX", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR44INDLASER", "Indicador Laser", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR44INDCEC", "Indicador C.E.C.", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR44INDCUANTAGASAS", "Indicador Cuenta Gasas", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR44INDOTROS", "Indicador Otros", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR44DESOTROS", "Descripci�n Indicador Otros", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "FR44CODHOJAQUIRO", "C�digo Hoja Quir�fano")

  End With
  
  With objMultiInfo1
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
    .strName = "Intervenciones"
    
    .strTable = "FRG300"
    
    Call .FormAddOrderField("FRG3CODINTERVENCION", cwAscending)
    
    Call .FormAddRelation("FR44CODHOJAQUIRO", txtText1(0))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Intervenciones")
    Call .FormAddFilterWhere(strKey, "FRG3CODINTERVENCION", "C�digo Intervenci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR44CODHOJAQUIRO", "C�digo Hoja Quir�fano", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRG3DESINTERVENCION", "Descripci�n Intervenci�n", cwString)
    
    Call .FormAddFilterOrder(strKey, "FRG3CODINTERVENCION", "C�digo Intervenci�n")
        
  End With
  
  With objMultiInfo2
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
    .strName = "Material y Medicacion"
    
    .strTable = "FR1800"
    
    .intAllowance = cwAllowModify
    
    Call .FormAddOrderField("FR18NUMLINEA", cwAscending)
    
    Call .FormAddRelation("FR44CODHOJAQUIRO", txtText1(0))
    
    .strWhere = "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO " & _
                "FROM FR7300 WHERE FR73INDMATMEDQUIRO<>0)"
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Material y Medicacion")
    Call .FormAddFilterWhere(strKey, "FR18NUMLINEA", "N�mero de Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR44CODHOJAQUIRO", "Hoja Quir�fano", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�digo Unidad Medida", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR18CANTCONSUMIDA", "Cantidad Consumida", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR18INDLEIDO", "Leida?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR18INDBLOQUEO", "Bloqueada?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR18INDESTHQ", "Estado Linea?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "FR18NUMLINEA", "N�mero de Linea")
        
  End With
  
  With objMultiInfo3
    Set .objFormContainer = fraFrame1(3)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(3)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys

    .strName = "Otros Productos"

    .strTable = "FR1800"

    .intAllowance = cwAllowModify + cwAllowDelete

    Call .FormAddOrderField("FR18NUMLINEA", cwAscending)

    Call .FormAddRelation("FR44CODHOJAQUIRO", txtText1(0))

    .strWhere = "FR73CODPRODUCTO IN (SELECT FR73CODPRODUCTO " & _
                "FROM FR7300 WHERE FR73INDMATMEDQUIRO=0)"

    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Otros Productos")
    Call .FormAddFilterWhere(strKey, "FR18NUMLINEA", "N�mero de Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR44CODHOJAQUIRO", "Hoja Quir�fano", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "C�digo Unidad Medida", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR18CANTCONSUMIDA", "Cantidad Consumida", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR18INDLEIDO", "Leida?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR18INDBLOQUEO", "Bloqueada?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR18INDESTHQ", "Estado Linea?", cwBoolean)
    
    Call .FormAddFilterOrder(strKey, "FR18NUMLINEA", "N�mero de Linea")

  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo3, cwFormMultiLine)

    Call .GridAddColumn(objMultiInfo1, "C�digo Intervenci�n", "FRG3CODINTERVENCION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo1, "C�digo Hoja Quir�fano", "FR44CODHOJAQUIRO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo1, "Descripci�n Intervenci�n", "FRG3DESINTERVENCION", cwString, 255)
    
    Call .GridAddColumn(objMultiInfo2, "L�nea", "FR18NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo2, "Hoja Quir�fano", "FR44CODHOJAQUIRO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo2, "C�d.Prod.", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo2, "Prod.", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo2, "Descripci�n Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo2, "Cant.Consumida", "FR18CANTCONSUMIDA", cwDecimal)
    Call .GridAddColumn(objMultiInfo2, "C�d.Uni.Medida", "FR93CODUNIMEDIDA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo2, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo2, "Leida?", "FR18INDLEIDO", cwBoolean)
    Call .GridAddColumn(objMultiInfo2, "Bloqueada?", "FR18INDBLOQUEO", cwBoolean)
    Call .GridAddColumn(objMultiInfo2, "Estado Linea?", "FR18INDESTHQ", cwBoolean)

    Call .GridAddColumn(objMultiInfo3, "L�nea", "FR18NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo3, "Hoja Quir�fano", "FR44CODHOJAQUIRO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo3, "C�d.Prod.", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo3, "Prod.", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo3, "Descripci�n Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo3, "Cant.Consumida", "FR18CANTCONSUMIDA", cwDecimal)
    Call .GridAddColumn(objMultiInfo3, "C�d.Uni.Medida", "FR93CODUNIMEDIDA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo3, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo3, "Leida?", "FR18INDLEIDO", cwBoolean)
    Call .GridAddColumn(objMultiInfo3, "Bloqueada?", "FR18INDBLOQUEO", cwBoolean)
    Call .GridAddColumn(objMultiInfo3, "Estado Linea?", "FR18INDESTHQ", cwBoolean)
   
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(2).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(2).Columns(4)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(3).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(3).Columns(4)).intKeyNo = 1
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1

    Call .FormChangeColor(objMultiInfo1)
    Call .FormChangeColor(objMultiInfo2)
    Call .FormChangeColor(objMultiInfo3)
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(10)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(11), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(12), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(13), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(14), "CI22SEGAPEL")
    .CtrlGetInfo(txtText1(10)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(15)), "SG02COD_CIR", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(15)), txtText1(16), "SG02APE1")
    .CtrlGetInfo(txtText1(15)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(17)), "AD02CODDPTO_CIR", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(17)), txtText1(18), "AD02DESDPTO")
    .CtrlGetInfo(txtText1(17)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(19)), "SG02COD_PAY", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(20), "SG02APE1")
    .CtrlGetInfo(txtText1(19)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(21)), "SG02COD_INS", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(21)), txtText1(22), "SG02APE1")
    .CtrlGetInfo(txtText1(21)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(23)), "SG02COD_SAY", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(23)), txtText1(24), "SG02APE1")
    .CtrlGetInfo(txtText1(23)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "SG02COD_ENF", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(26), "SG02APE1")
    .CtrlGetInfo(txtText1(25)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(27)), "FR43CODHOJAANEST", "SELECT * FROM FR4300 WHERE FR43CODHOJAANEST = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(27)), txtText1(28), "SG02COD_ANE")
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(28)), "SG02COD_ANE", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(28)), txtText1(29), "SG02APE1")
    .CtrlGetInfo(txtText1(27)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(30)), "SG02COD_CCL", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(30)), txtText1(31), "SG02APE1")
    .CtrlGetInfo(txtText1(30)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(32)), "AD02CODDPTO_CCL", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(32)), txtText1(33), "AD02DESDPTO")
    .CtrlGetInfo(txtText1(32)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(34)), "SG02COD_ACL", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(34)), txtText1(35), "SG02APE1")
    .CtrlGetInfo(txtText1(34)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(38)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(38)), txtText1(39), "AD02DESDPTO")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(38)), txtText1(40), "AD02CODDPTO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(40)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(40)), txtText1(41), "AD02DESDPTO")
    .CtrlGetInfo(txtText1(38)).blnForeign = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(9)), "SG02COD", "SELECT SG02APE1 FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(9)), txtText1(43), "SG02APE1")
    .CtrlGetInfo(txtText1(9)).blnForeign = True
    .CtrlGetInfo(txtText1(9)).blnReadOnly = True

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR97CODQUIROFANO", "SELECT FR97DESQUIROFANO FROM FR9700 WHERE FR97CODQUIROFANO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(44), "FR97DESQUIROFANO")
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), grdDBGrid1(2).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), grdDBGrid1(2).Columns(7), "FR73DESPRODUCTO")
    .CtrlGetInfo(grdDBGrid1(2).Columns(5)).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(9)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(9)), grdDBGrid1(2).Columns(10), "FR93DESUNIMEDIDA")
    .CtrlGetInfo(grdDBGrid1(2).Columns(9)).blnReadOnly = True
     
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), grdDBGrid1(3).Columns(6), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(5)), grdDBGrid1(3).Columns(7), "FR73DESPRODUCTO")
    .CtrlGetInfo(grdDBGrid1(3).Columns(5)).blnReadOnly = True
     
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(9)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(9)), grdDBGrid1(3).Columns(10), "FR93DESUNIMEDIDA")
    .CtrlGetInfo(grdDBGrid1(3).Columns(9)).blnReadOnly = True
     
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
    .CtrlGetInfo(txtText1(17)).blnInFind = True
    .CtrlGetInfo(txtText1(19)).blnInFind = True
    .CtrlGetInfo(txtText1(21)).blnInFind = True
    .CtrlGetInfo(txtText1(23)).blnInFind = True
    .CtrlGetInfo(txtText1(25)).blnInFind = True
    .CtrlGetInfo(txtText1(27)).blnInFind = True
    .CtrlGetInfo(txtText1(30)).blnInFind = True
    .CtrlGetInfo(txtText1(32)).blnInFind = True
    .CtrlGetInfo(txtText1(34)).blnInFind = True
    .CtrlGetInfo(txtText1(36)).blnInFind = True
    .CtrlGetInfo(txtText1(37)).blnInFind = True
    .CtrlGetInfo(txtText1(38)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    .CtrlGetInfo(chkCheck1(4)).blnInFind = True
    .CtrlGetInfo(chkCheck1(5)).blnInFind = True
    .CtrlGetInfo(chkCheck1(6)).blnInFind = True
    .CtrlGetInfo(chkCheck1(7)).blnInFind = True
    .CtrlGetInfo(chkCheck1(8)).blnInFind = True
    .CtrlGetInfo(chkCheck1(9)).blnInFind = True
    .CtrlGetInfo(chkCheck1(10)).blnInFind = True
    .CtrlGetInfo(chkCheck1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(42)).blnInFind = True
    
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT FRG6CODTIPPERS,FRG6DESTIPPERS FROM FRG600 ORDER BY FRG6CODTIPPERS"
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

grdDBGrid1(1).Columns(3).Width = 9 * 180
grdDBGrid1(1).Columns(4).Width = 10 * 180
grdDBGrid1(1).Columns(5).Width = 50 * 220

grdDBGrid1(2).Columns(7).Width = 4545
grdDBGrid1(2).Columns(8).Width = 1335
grdDBGrid1(2).Columns(9).Width = 1335
grdDBGrid1(2).Columns(10).Width = 2685

grdDBGrid1(3).Columns(7).Width = 4545
grdDBGrid1(3).Columns(8).Width = 1335
grdDBGrid1(3).Columns(9).Width = 1335
grdDBGrid1(3).Columns(10).Width = 2685

grdDBGrid1(1).Columns(4).Visible = False
grdDBGrid1(2).Columns(4).Visible = False
grdDBGrid1(3).Columns(4).Visible = False

grdDBGrid1(2).Columns(11).Visible = False
grdDBGrid1(2).Columns(12).Visible = False
grdDBGrid1(2).Columns(13).Visible = False
grdDBGrid1(3).Columns(11).Visible = False
grdDBGrid1(3).Columns(12).Visible = False
grdDBGrid1(3).Columns(13).Visible = False

grdDBGrid1(1).RemoveAll
grdDBGrid1(1).Refresh
grdDBGrid1(2).RemoveAll
grdDBGrid1(2).Refresh
grdDBGrid1(3).RemoveAll
grdDBGrid1(3).Refresh

    If gstrllamador = "frmFirmarHojQuirof" Then
      Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
    End If

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strCtrl = "txtText1(10)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "CI2200"
      
      Set objField = .AddField("CI21CODPERSONA")
      objField.strSmallDesc = "C�digo Paciente"
      
      Set objField = .AddField("CI22NUMHISTORIA")
      objField.strSmallDesc = "Historia"
      
      Set objField = .AddField("CI22NOMBRE")
      objField.strSmallDesc = "Nombre"
      
      Set objField = .AddField("CI22PRIAPEL")
      objField.strSmallDesc = "Apellido 1�"
      
      Set objField = .AddField("CI22SEGAPEL")
      objField.strSmallDesc = "Apellido 2�"
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(10), .cllValues("CI21CODPERSONA"))
      End If
    End With
    Set objSearch = Nothing
  End If
  
  If strCtrl = "txtText1(15)" Or strCtrl = "txtText1(19)" Or _
     strCtrl = "txtText1(21)" Or strCtrl = "txtText1(23)" Or _
     strCtrl = "txtText1(25)" Or strCtrl = "txtText1(30)" Or _
     strCtrl = "txtText1(34)" Or strCtrl = "txtText1(9)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "SG0200"
      
      Set objField = .AddField("SG02COD")
      objField.strSmallDesc = "C�digo Usuario"
      
      Set objField = .AddField("SG02APE1")
      objField.strSmallDesc = "Primer Apellido"
      
      If .Search Then
        Select Case strCtrl
          Case "txtText1(15)"
            Call objWinInfo.CtrlSet(txtText1(15), .cllValues("SG02COD"))
          Case "txtText1(19)"
            Call objWinInfo.CtrlSet(txtText1(19), .cllValues("SG02COD"))
          Case "txtText1(21)"
            Call objWinInfo.CtrlSet(txtText1(21), .cllValues("SG02COD"))
          Case "txtText1(23)"
            Call objWinInfo.CtrlSet(txtText1(23), .cllValues("SG02COD"))
          Case "txtText1(25)"
            Call objWinInfo.CtrlSet(txtText1(25), .cllValues("SG02COD"))
          Case "txtText1(30)"
            Call objWinInfo.CtrlSet(txtText1(30), .cllValues("SG02COD"))
          Case "txtText1(34)"
            Call objWinInfo.CtrlSet(txtText1(34), .cllValues("SG02COD"))
          Case "txtText1(9)"
            Call objWinInfo.CtrlSet(txtText1(9), .cllValues("SG02COD"))
        End Select
      End If
    End With
    Set objSearch = Nothing
  End If
  
  If strCtrl = "txtText1(17)" Or strCtrl = "txtText1(32)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "AD0200"

      Set objField = .AddField("AD02CODDPTO")
      objField.strSmallDesc = "C�digo Departamento"
      
      Set objField = .AddField("AD02DESDPTO")
      objField.strSmallDesc = "Descripci�n Departamento"
      
      If .Search Then
        Select Case strCtrl
          Case "txtText1(17)"
            Call objWinInfo.CtrlSet(txtText1(17), .cllValues("AD02CODDPTO"))
          Case "txtText1(32)"
            Call objWinInfo.CtrlSet(txtText1(32), .cllValues("AD02CODDPTO"))
        End Select
      End If
    End With
    Set objSearch = Nothing
  End If

  If strCtrl = "txtText1(38)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "AD0200"
     .strWhere = " WHERE AD32CODTIPODPTO = 3" & _
                 " AND AD02FECINICIO < (SELECT SYSDATE FROM DUAL)" & _
                 " AND ((AD02FECFIN IS NULL) OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL)))"
      
      Set objField = .AddField("AD02CODDPTO")
      objField.strSmallDesc = "C�digo Servicio"
      
      Set objField = .AddField("AD02DESDPTO")
      objField.strSmallDesc = "Descripci�n Servicio"
      
      'Set objField = .AddField("AD02CODDPTO")
      'objField.strSmallDesc = "C�digo Departamento"
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(38), .cllValues("AD02CODDPTO"))
      End If
    End With
    Set objSearch = Nothing
  End If
  
  If strCtrl = "txtText1(27)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "FR4300"
      
      Set objField = .AddField("FR43CODHOJAANEST")
      objField.strSmallDesc = "C�digo Hoja Anestesia"
      
      Set objField = .AddField("SG02COD_ANE")
      objField.strSmallDesc = "Anestesista"
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(27), .cllValues("FR43CODHOJAANEST"))
      End If
    End With
    Set objSearch = Nothing
  End If
  
  If strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "FR9700"
      
      Set objField = .AddField("FR97CODQUIROFANO")
      objField.strSmallDesc = "C�digo Quir�fano"
      
      Set objField = .AddField("FR97DESQUIROFANO")
      objField.strSmallDesc = "Descripci�n Quir�fano"
      
      If .Search Then
        Call objWinInfo.CtrlSet(txtText1(1), .cllValues("FR97CODQUIROFANO"))
      End If
    End With
    Set objSearch = Nothing
  End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)

  If txtText1(9).Text <> "" Then
        Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
        objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
        cmdbuscargruprod.Enabled = False
        cmdbuscarprod.Enabled = False
        cmdinter.Enabled = False
        
        Select Case strFormName
        Case "Hoja Quirofano"
          Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
        Case "Intervenciones"
          Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
        Case "Material y Medicacion"
          Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
        Case "Otros Productos"
          Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
        End Select
  Else
    Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    If gstrllamador = "frmFirmarHojQuirof" Then
      objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
    Else
      objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
    End If
    Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
    objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
    Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
    objWinInfo.objWinActiveForm.intAllowance = cwAllowModify
    Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
    objWinInfo.objWinActiveForm.intAllowance = cwAllowModify + cwAllowDelete
    cmdbuscargruprod.Enabled = True
    cmdbuscarprod.Enabled = True
    cmdinter.Enabled = True
    
    Select Case strFormName
    Case "Hoja Quirofano"
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    Case "Intervenciones"
      Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
    Case "Material y Medicacion"
      Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
    Case "Otros Productos"
      Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
    End Select
  End If

End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim rsta As rdoResultset
Dim sqlstr, strInsert As String
Dim cuenta, numlinea As Integer

  If blnError = False And strFormName = "Hoja Quirofano" Then
    sqlstr = "SELECT COUNT(FR44CODHOJAQUIRO) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    cuenta = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
    If cuenta = 0 Then
      'A�adir Material y Medicamentos de quir�fano
      numlinea = 1
      sqlstr = "SELECT FR73CODPRODUCTO,FR93CODUNIMEDIDA FROM FR7300 WHERE FR73INDMATMEDQUIRO<>0"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      While Not rsta.EOF
        
        'insertar linea
        strInsert = "INSERT INTO FR1800 (FR44CODHOJAQUIRO,FR18NUMLINEA,FR73CODPRODUCTO,FR93CODUNIMEDIDA,FR18CANTCONSUMIDA)" & _
                    " VALUES (" & txtText1(0).Text & "," & numlinea & "," & rsta.rdoColumns(0).Value & "," & rsta.rdoColumns(1).Value & ",0)"
        objApp.rdoConnect.Execute strInsert, 64
        objApp.rdoConnect.Execute "Commit", 64
        
        numlinea = numlinea + 1
        rsta.MoveNext
      Wend
      rsta.Close
      Set rsta = Nothing
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
      Call objWinInfo.DataRefresh
      Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
      Call objWinInfo.DataRefresh
      Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
      Call objWinInfo.DataRefresh
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    End If
  End If

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
'
'
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabGeneral1_Click(Index As Integer, PreviousTab As Integer)
    
  Select Case tabGeneral1(1).Tab
    Case 0
      Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
    Case 1
      Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
    Case 2
      Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
    Case 3
      Call objWinInfo.FormChangeActive(fraFrame1(3), True, True)
      If txtText1(0).Text <> "" Then
        Call objWinInfo.DataRefresh
      End If
      Call objWinInfo.CtrlGotFocus
  End Select

End Sub

Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
   tab1.Tab = 0
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim rstlinea As rdoResultset
Dim strlinea As String
Dim linea As Integer

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Hoja Quirofano" Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR44CODHOJAQUIRO_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns(0).Value)
    txtText1(0).SetFocus
    Call objWinInfo.CtrlGotFocus
    Call objWinInfo.CtrlLostFocus
    rsta.Close
    Set rsta = Nothing
  Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Intervenciones" Then
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      sqlstr = "SELECT FRG3CODINTERVENCION_SEQUENCE.nextval FROM dual"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      grdDBGrid1(1).Columns(3).Value = rsta.rdoColumns(0).Value
      SendKeys ("{TAB}")
      rsta.Close
      Set rsta = Nothing
    ElseIf btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Otros Productos" Then
      If txtText1(0).Text <> "" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        strlinea = "SELECT MAX(FR18NUMLINEA) FROM FR1800 WHERE FR44CODHOJAQUIRO=" & _
                  txtText1(0).Text
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea(0).Value) Then
           linea = grdDBGrid1(3).Rows
        Else
           linea = rstlinea(0).Value + grdDBGrid1(3).Rows
        End If
        grdDBGrid1(3).Columns(3).Value = linea
        SendKeys ("{TAB}")
        rstlinea.Close
        Set rstlinea = Nothing
      Else
        MsgBox "No hay ninguna hoja de quir�fano", vbInformation
        Exit Sub
      End If
    Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        
  If intIndex = 10 Then 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   tab1.Tab = 1
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


