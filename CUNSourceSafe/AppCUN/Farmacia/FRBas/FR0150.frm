VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmModLinea 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Modificar productos de la petici�n"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Productos"
      Height          =   375
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   855
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4560
      Index           =   0
      Left            =   240
      TabIndex        =   1
      Top             =   840
      Width           =   10215
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4035
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   9945
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   17542
         _ExtentY        =   7117
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmModLinea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmModLinea(FR0150.FRM)                                      *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: SEPTIEMBRE DE 1998                                            *
'* DESCRIPCION: modificar l�nea                                         *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1



Private Sub Form_Activate()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim linea As Integer
Dim stra As String
Dim rsta As rdoResultset
Dim rstope As rdoResultset
Dim strope As String

grdDBGrid1(0).Columns(23).Style = ssStyleComboBox
grdDBGrid1(0).Columns(23).AddItem ("+")
grdDBGrid1(0).Columns(23).AddItem ("/")
grdDBGrid1(0).Columns(23).AddItem ("=")


If gintbusprod = 1 Then
  gintbusprod = 0
  noinsertar = True
  If gintprodtotal > 0 Then
    strlinea = "SELECT MAX(FR28NUMLINEA) FROM FR2800 WHERE FR66CODPETICION=" & _
                        frmEstPetFarm.txtText1(0).Text
    Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
    If IsNull(rstlinea.rdoColumns(0).Value) Then
                linea = 1 + frmEstPetFarm.grdDBGrid1(0).Row
    Else
                linea = rstlinea.rdoColumns(0).Value + 1 + frmEstPetFarm.grdDBGrid1(0).Row
    End If
    rstlinea.Close
    Set rstlinea = Nothing
    
    For v = 0 To gintprodtotal - 1
           'se mira que no se inserte 2 veces el mismo producto
           stra = "SELECT FR73CODPRODUCTO FROM FR2800 WHERE FR66CODPETICION=" & _
                 frmEstPetFarm.txtText1(0).Text
           Set rsta = objApp.rdoConnect.OpenResultset(stra)
           If Not rsta.EOF Then
             If Not IsNull(rsta.rdoColumns(0).Value) Then
              Do While Not rsta.EOF
                  If "" & rsta.rdoColumns(0).Value & "" = gintprodbuscado(v, 0) Then
                      noinsertar = False
                      Exit Do
                  Else
                      noinsertar = True
                  End If
              rsta.MoveNext
              Loop
            End If
          End If
          rsta.Close
          Set rsta = Nothing
          If noinsertar = True Then
            'hay que crear una nueva l�nea id�ntica a la seleccionada
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
            grdDBGrid1(0).Columns(3).Value = frmEstPetFarm.txtText1(0).Text 'petici�n
            grdDBGrid1(0).Columns(4).Value = linea          'numl�nea
            grdDBGrid1(0).Columns(5).Value = gintprodbuscado(v, 0) 'c�d.prod
            grdDBGrid1(0).Columns(6).Value = gintprodbuscado(v, 1) 'c�d.interno
            grdDBGrid1(0).Columns(7).Value = gintprodbuscado(v, 2) 'descripci�n
            linea = linea + 1
          Else
            mensaje = MsgBox("El producto " & gintprodbuscado(v, 2) & " ya pertenece a la petici�n." & _
                           Chr(13), vbInformation)
          End If
          noinsertar = True
    Next v
 End If
 gintprodtotal = 0
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Detalle OM"
    
    .strTable = "FR2800"
    'se va a esta pantalla desde modificar l�nea (frmModLinea fr0150)
    If gintbusprod = 0 Then
    .strWhere = "FR66CODPETICION=" & frmEstPetFarm.grdDBGrid1(0).Columns(4).Value & _
           " AND FR28NUMLINEA=" & gintlinea
    'se va a esta pantalla desde buscar productos (frmbusproductos fr0119)
    Else
    .strWhere = "FR66CODPETICION is null"
    End If
    .intAllowance = cwAllowModify
    '+++++++++++++++++++++++++++++++
    Call .FormAddOrderField("FR28NUMLINEA", cwAscending)
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�digo", "FR66CODPETICION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR28NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Prod", "", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "V�a", "FR34CODVIA", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Desc.V�a", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Volumen", "FR28VOLUMEN", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Tiempo Infusi�n", "FR28TIEMINFMIN", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "C�d.Frecuencia", "FRG4CODFRECUENCIA", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Frecuencia", "", cwString, 60)
    Call .GridAddColumn(objMultiInfo, "PRN", "FR28INDDISPPRN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "C.I", "FR28INDCOMIENINMED", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "S.N", "FR28INDSN", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "FR28CANTIDAD", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Medida", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Desc.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR28DOSIS", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Periodicidad", "FRH5CODPERIODICIDAD", cwString, 10)
    Call .GridAddColumn(objMultiInfo, "Forma farmace�tica", "FRH7CODFORMFAR", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Operaci�n", "FR28OPERACION", cwString, 1)

   
    
    Call .FormCreateInfo(objMultiInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).intKeyNo = 1

    Call .FormChangeColor(objMultiInfo)
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "FR73CODINTFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(7), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), "FR34CODVIA", "SELECT * FROM FR3400 WHERE FR34CODVIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), grdDBGrid1(0).Columns(9), "FR34DESVIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(18)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(18)), grdDBGrid1(0).Columns(19), "FR93DESUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), "FRG4CODFRECUENCIA", "SELECT * FROM FRG400 WHERE FRG4CODFRECUENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), grdDBGrid1(0).Columns(13), "FRG4DESFRECUENCIA")
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(18)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(21)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(22)).blnForeign = True
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
grdDBGrid1(0).Columns(3).Visible = False
grdDBGrid1(0).Columns(4).Visible = False

grdDBGrid1(0).Columns(5).Width = 1000   'c�d.producto
grdDBGrid1(0).Columns(6).Width = 800   'c�d interno
grdDBGrid1(0).Columns(7).Width = 1500   'desc.producto
grdDBGrid1(0).Columns(8).Width = 400   'c�d.v�a
grdDBGrid1(0).Columns(9).Width = 1500   'desc.v�a
grdDBGrid1(0).Columns(10).Width = 1100  'volumen
grdDBGrid1(0).Columns(11).Width = 1100  'tiempo infusi�n
grdDBGrid1(0).Columns(12).Width = 1500  'frecuencia
grdDBGrid1(0).Columns(13).Width = 1500  'frecuencia
grdDBGrid1(0).Columns(14).Width = 500  'prn
grdDBGrid1(0).Columns(15).Width = 500  'comienzo inmediato
grdDBGrid1(0).Columns(16).Width = 500  'seg�n niveles
grdDBGrid1(0).Columns(17).Width = 1000  'cantidad compuesto
grdDBGrid1(0).Columns(18).Width = 700  'c�d.medida
grdDBGrid1(0).Columns(19).Width = 1000  'desc. medida
grdDBGrid1(0).Columns(20).Width = 700  'dosis
grdDBGrid1(0).Columns(20).Width = 1200  'periodicidad
grdDBGrid1(0).Columns(20).Width = 600  'forma farmace�tica


End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).C�d.Prod" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"

     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"

     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FR73DESPRODUCTO")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(5), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).V�a" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR3400"

     Set objField = .AddField("FR34CODVIA")
     objField.strSmallDesc = "C�digo V�a"
     
     Set objField = .AddField("FR34DESVIA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(8), .cllValues("FR34CODVIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).Medida" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad de Medida"
     
     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(18), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).C�d.Frecuencia" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRG400"

     Set objField = .AddField("FRG4CODFRECUENCIA")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FRG4DESFRECUENCIA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(12), .cllValues("FRG4CODFRECUENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).Periodicidad" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH500"

     Set objField = .AddField("FRH5CODPERIODICIDAD")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FRH5DESPERIODICIDAD")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(21), .cllValues("FRH5CODPERIODICIDAD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Detalle OM" And strCtrl = "grdDBGrid1(0).Forma farmace�tica" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"

     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(22), .cllValues("FRH7CODFORMFAR"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
End Sub



Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
     Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
     Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
     Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


