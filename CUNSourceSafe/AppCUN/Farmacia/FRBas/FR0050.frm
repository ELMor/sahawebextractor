VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{02B5E320-7292-11CF-93D5-0020AF99504A}#1.0#0"; "MSCHART.OCX"
Begin VB.Form frmContrPresuFarm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Controlar Presupuestos."
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8295
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame3 
      Height          =   495
      Left            =   7680
      TabIndex        =   12
      Top             =   480
      Width           =   3975
      Begin VB.OptionButton optPtas 
         Caption         =   "Euros"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Index           =   1
         Left            =   2040
         TabIndex        =   14
         Top             =   200
         Width           =   1575
      End
      Begin VB.OptionButton optPtas 
         Caption         =   "Ptas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Index           =   0
         Left            =   840
         TabIndex        =   13
         Top             =   200
         Width           =   1335
      End
   End
   Begin VB.Frame Frame2 
      Height          =   495
      Left            =   5280
      TabIndex        =   9
      Top             =   480
      Width           =   2175
      Begin VB.OptionButton opt2d 
         Caption         =   "2d"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   11
         Top             =   200
         Width           =   735
      End
      Begin VB.OptionButton opt2d 
         Caption         =   "3d"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Index           =   1
         Left            =   1080
         TabIndex        =   10
         Top             =   200
         Width           =   975
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Ejercicios"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6720
      Index           =   1
      Left            =   120
      TabIndex        =   6
      Top             =   1080
      Width           =   4695
      Begin ComctlLib.TreeView tvwItems 
         Height          =   6075
         Index           =   0
         Left            =   360
         TabIndex        =   7
         Top             =   480
         Width           =   4080
         _ExtentX        =   7197
         _ExtentY        =   10716
         _Version        =   327682
         HideSelection   =   0   'False
         Indentation     =   353
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
      End
   End
   Begin VB.Frame Frame1 
      Height          =   495
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   4695
      Begin VB.OptionButton optGrupos 
         Caption         =   "Servicios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Index           =   2
         Left            =   2280
         TabIndex        =   5
         Top             =   200
         Width           =   1815
      End
      Begin VB.OptionButton optGrupos 
         Caption         =   "Grupos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Index           =   1
         Left            =   840
         TabIndex        =   4
         Top             =   200
         Width           =   1215
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Gr�fica"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6480
      Index           =   0
      Left            =   4920
      TabIndex        =   2
      Top             =   1080
      Width           =   6780
      Begin MSChartLib.MSChart MSChart1 
         Height          =   6255
         Left            =   240
         OleObjectBlob   =   "FR0050.frx":0000
         TabIndex        =   8
         Top             =   360
         Width           =   6375
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8010
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin ComctlLib.ImageList imlImagenes 
      Left            =   0
      Top             =   7560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   19
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":249E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":24FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":255A
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":25B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2616
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2674
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":26D2
            Key             =   "rol"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2730
            Key             =   "function"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":278E
            Key             =   "window"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":27EC
            Key             =   "report"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":284A
            Key             =   "externalreport"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":28A8
            Key             =   "process"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2906
            Key             =   "application"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2964
            Key             =   "big"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":29C2
            Key             =   "small"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2A20
            Key             =   "list"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2A7E
            Key             =   "details"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2ADC
            Key             =   "exit"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0050.frx":2B3A
            Key             =   "refresh"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmContrPresuFarm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmContrPresuFarm (FR0050.FRM)                               *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Controlar Presupuestos                                  *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim mblnmoving As Boolean
Dim gblnabrir As Boolean
Dim gintMoneda As Integer
Dim gintMarca As Integer
Dim gGrafico1(1 To 2, 1 To 12)
Dim gGrafico2(1 To 2, 1 To 12)
Dim gPrimeraVez As Boolean

Private Sub rellenar_TreeView_GrupServ()
    Dim a�o As Variant
    Dim strejercicio As String
    Dim rstejercicio As rdoResultset
    'Dim strMes As String
    'Dim rstMes As rdoResultset
    'Dim mes As Variant
    Dim strgrupo As String
    Dim rstgrupo As rdoResultset
    Dim strdesGrupo As String
    Dim rstdesGrupo As rdoResultset
    Dim grupo As Variant
    Dim desGrupo As Variant
    Dim strServicio As String
    Dim rstServicio As rdoResultset
    Dim desServicio As Variant
    Dim strdesServicio As String
    Dim rstdesServicio As rdoResultset
    Dim servicio As Variant
    Dim gNode As Node
    Dim Gvacio As Boolean
     
    tvwItems(0).Nodes.Clear
    Gvacio = True
    
    strejercicio = "select distinct frg9ejercicio from frh100 order by frg9ejercicio asc"
    Set rstejercicio = objApp.rdoConnect.OpenResultset(strejercicio)
    
    
    While rstejercicio.EOF = False
      a�o = rstejercicio("frg9ejercicio").Value
      Call tvwItems(0).Nodes.Add(, , "ejerc" & a�o, a�o)
      'strMes = "SELECT DISTINCT FRH1MES FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & " ORDER BY FRH1MES"
      'Set rstMes = objApp.rdoConnect.OpenResultset(strMes)
      'While rstMes.EOF = False
        'mes = rstMes("FRH1MES").Value
        'Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "mes" & a�o & "/" & mes, mes)
        strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
                     " ORDER BY FR41CODGRUPPROD"
        Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
        While rstgrupo.EOF = False
            grupo = rstgrupo("FR41CODGRUPPROD").Value
            strServicio = "SELECT DISTINCT AD02CODDPTO FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
                          " AND FR41CODGRUPPROD=" & grupo & "ORDER BY AD02CODDPTO"
            Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
            strdesGrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD=" & rstgrupo("FR41CODGRUPPROD").Value
            Set rstdesGrupo = objApp.rdoConnect.OpenResultset(strdesGrupo)
            desGrupo = rstdesGrupo("FR41DESGRUPPROD").Value
            Set gNode = tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "grupo" & a�o & "/" & grupo, grupo & ".-" & desGrupo)
            Gvacio = False
            While rstServicio.EOF = False
                servicio = rstServicio("AD02CODDPTO").Value
                strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
                Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
                desServicio = rstdesServicio("AD02DESDPTO").Value
                Call tvwItems(0).Nodes.Add("grupo" & a�o & "/" & grupo, tvwChild, "servicio" & a�o & "/" & grupo & "/" & servicio, servicio & ".-" & desServicio)
                rstdesServicio.Close
                Set rstdesServicio = Nothing
                rstServicio.MoveNext
            Wend
            rstServicio.Close
            Set rstServicio = Nothing
            rstdesGrupo.Close
            Set rstdesGrupo = Nothing
            rstgrupo.MoveNext
        Wend
        rstgrupo.Close
        Set rstgrupo = Nothing
        'rstMes.MoveNext
      'Wend
      'rstMes.Close
      'Set rstMes = Nothing
      rstejercicio.MoveNext
    Wend
    rstejercicio.Close
    Set rstejercicio = Nothing
    If Gvacio = False Then
        Call tvwItems_NodeClick(0, gNode)
    End If

End Sub

Private Sub rellenar_TreeView_ServGrup()
    Dim a�o As Variant
    Dim strejercicio As String
    Dim rstejercicio As rdoResultset
    'Dim strMes As String
    'Dim rstMes As rdoResultset
    'Dim mes As Variant
    Dim strgrupo As String
    Dim rstgrupo As rdoResultset
    Dim strdesGrupo As String
    Dim rstdesGrupo As rdoResultset
    Dim grupo As Variant
    Dim desGrupo As Variant
    Dim strServicio As String
    Dim rstServicio As rdoResultset
    Dim desServicio As Variant
    Dim strdesServicio As String
    Dim rstdesServicio As rdoResultset
    Dim servicio As Variant
    
    tvwItems(0).Nodes.Clear
    
    strejercicio = "select distinct frg9ejercicio from frh100 order by frg9ejercicio asc"
    Set rstejercicio = objApp.rdoConnect.OpenResultset(strejercicio)
    
    
    While rstejercicio.EOF = False
      a�o = rstejercicio("frg9ejercicio").Value
      Call tvwItems(0).Nodes.Add(, , "ejerc" & a�o, a�o)
      'strMes = "SELECT DISTINCT FRH1MES FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & " ORDER BY FRH1MES"
      'Set rstMes = objApp.rdoConnect.OpenResultset(strMes)
      'While rstMes.EOF = False
        'mes = rstMes("FRH1MES").Value
        'Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "mes" & a�o & "/" & mes, mes)
        strServicio = "SELECT DISTINCT AD02CODDPTO FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
                     " ORDER BY AD02CODDPTO"
        Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
        While rstServicio.EOF = False
            servicio = rstServicio("AD02CODDPTO").Value
            strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
                          " AND AD02CODDPTO=" & servicio & "ORDER BY FR41CODGRUPPROD"
            Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
            strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
            Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
            desServicio = rstdesServicio("AD02DESDPTO").Value
            Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "servicio" & a�o & "/" & servicio, servicio & ".-" & desServicio)
            While rstgrupo.EOF = False
                grupo = rstgrupo("FR41CODGRUPPROD").Value
                strdesGrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD=" & rstgrupo("FR41CODGRUPPROD").Value
                Set rstdesGrupo = objApp.rdoConnect.OpenResultset(strdesGrupo)
                desGrupo = rstdesGrupo("FR41DESGRUPPROD").Value
                Call tvwItems(0).Nodes.Add("servicio" & a�o & "/" & servicio, tvwChild, "grupo" & a�o & "/" & servicio & "/" & grupo, grupo & ".-" & desGrupo)
                rstdesGrupo.Close
                Set rstdesGrupo = Nothing
                rstgrupo.MoveNext
            Wend
            rstgrupo.Close
            Set rstgrupo = Nothing
            rstdesServicio.Close
            Set rstdesServicio = Nothing
            rstServicio.MoveNext
        Wend
        rstServicio.Close
        Set rstServicio = Nothing
        'rstMes.MoveNext
      'Wend
      'rstMes.Close
      'Set rstMes = Nothing
      rstejercicio.MoveNext
    Wend
    rstejercicio.Close
    Set rstejercicio = Nothing

    

End Sub




Private Sub Form_Activate()
    
    Dim rowLabelCount As Integer
    Dim columnLabelCount As Integer
    Dim rowCount As Integer
    Dim columnCount As Integer
    Dim datagrid As datagrid
    Dim labelindex As Integer
    Dim column As Integer
    Dim row As Integer
    Dim i As Integer
    
    gintMarca = 1
    
    Set datagrid = MSChart1.datagrid
    MSChart1.ShowLegend = True
    With MSChart1.datagrid
    ' Establece los par�metros del gr�fico con
    ' los m�todos.
    
        rowLabelCount = 12
        columnLabelCount = 2
        rowCount = 12
        columnCount = 2
        .SetSize rowLabelCount, columnLabelCount, rowCount, columnCount
        labelindex = 1
        labelindex = 1
        row = 1
        .RowLabel(row, labelindex) = "Enero"
        row = 2
        .RowLabel(row, labelindex) = "Febrero"
        row = 3
        .RowLabel(row, labelindex) = "Marzo"
        row = 4
        .RowLabel(row, labelindex) = "Abril"
        row = 5
        .RowLabel(row, labelindex) = "Mayo"
        row = 6
        .RowLabel(row, labelindex) = "Junio"
        row = 7
        .RowLabel(row, labelindex) = "Julio"
        row = 8
        .RowLabel(row, labelindex) = "Agosto"
        row = 9
        .RowLabel(row, labelindex) = "Septiembre"
        row = 10
        .RowLabel(row, labelindex) = "Octubre"
        row = 11
        .RowLabel(row, labelindex) = "Noviembre"
        row = 12
        .RowLabel(row, labelindex) = "Diciembre"
    End With
    
    gPrimeraVez = False
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
                                
  
  tvwItems(0).Visible = True
  gPrimeraVez = True
  optGrupos(1).Value = True
  optPtas(0).Value = True
  opt2d(0).Value = True
  
End Sub

Private Sub MSChart1_SeriesSelected(Series As Integer, MouseFlags As Integer, Cancel As Integer)
    gintMarca = Series
End Sub

Private Sub opt2d_Click(Index As Integer)
    If Index = 0 Then
        MSChart1.chartType = VtChChartType2dLine
    Else
        MSChart1.chartType = VtChChartType3dLine
    End If
End Sub

Private Sub optGrupos_Click(Index As Integer)
    If Index = 1 Then
        Call rellenar_TreeView_GrupServ
    Else
        Call rellenar_TreeView_ServGrup
    End If
End Sub

Private Sub optPtas_Click(Index As Integer)
Dim i As Integer

    If Index = 0 Then
        gintMoneda = 0
    Else
        gintMoneda = 1
    End If
    If gPrimeraVez = False Then
        For i = 1 To 12
            Call MSChart1.datagrid.SetData(i, 1, gGrafico1(gintMoneda + 1, i), 0)
            Call MSChart1.datagrid.SetData(i, 2, gGrafico2(gintMoneda + 1, i), 0)
        Next i
    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
      
Dim rsta As rdoResultset
Dim sqlstr As String
Dim strSelect As String
Dim rstSelect As rdoResultset
Dim vntclave As Variant
Dim strupdate As String
Dim vntPtas As Variant
Dim vntEuro As Variant
  
  Select Case btnButton.Index
  Case 11
      MSChart1.EditCopy
  Case 30 'salir
    Unload Me
  Case 21 'Primero
      tvwItems(0).SelectedItem.FirstSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    Exit Sub
  Case 22 'Anterior
      If tvwItems(0).SelectedItem.FirstSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Previous.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    Exit Sub
  Case 23 'Siguiente
      If tvwItems(0).SelectedItem.LastSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Next.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    Exit Sub
  Case 24 'Ultimo
      tvwItems(0).SelectedItem.LastSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    Exit Sub
  Case Else 'Otro boton
    Exit Sub
  End Select
  

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    'Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub


Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    'Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub tvwItems_Collapse(Index As Integer, ByVal Node As ComctlLib.Node)

        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)

End Sub

Private Sub tvwItems_Expand(Index As Integer, ByVal Node As ComctlLib.Node)


  Node.EnsureVisible
  Node.Selected = True
  Call tvwItems_NodeClick(0, Node)

End Sub

Private Sub tvwItems_NodeClick(Index As Integer, ByVal Node As ComctlLib.Node)
    
Dim ejercicio As Variant
Dim mes As Variant
Dim grupo As Variant
Dim servicio As Variant
Dim primeraparte As Variant
Dim segundaparte As Variant
Dim resto As Variant
Dim strWhere As String
Dim rstwhere As rdoResultset
Dim intmes As Integer
Dim i As Integer
    
    Select Case optGrupos(1).Value
        Case True:
        
            Select Case Left(Node.Key, 3)
                Case "eje":
                  ejercicio = Right(Node.Key, Len(Node.Key) - 5)
                  strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) " & _
                            "from frh100 where frg9ejercicio=" & ejercicio & _
                            " group by frh1mes"
                  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  intmes = 0
                  MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  While rstwhere.EOF = False
                    If gintMarca = 1 Then
                        gGrafico1(1, intmes + 1) = rstwhere(0).Value
                        gGrafico1(2, intmes + 1) = rstwhere(1).Value
                    Else
                        gGrafico2(1, intmes + 1) = rstwhere(0).Value
                        gGrafico2(2, intmes + 1) = rstwhere(1).Value
                    End If
                    intmes = intmes + 1
                    rstwhere.MoveNext
                  Wend
                Case "gru":
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 6)
                  grupo = resto
                  strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
                             "frg9ejercicio=" & ejercicio & _
                             " and fr41codgrupprod=" & grupo & _
                             " group by frh1mes"
                  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  intmes = 0
                  MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  While rstwhere.EOF = False
                    If gintMarca = 1 Then
                        gGrafico1(1, intmes + 1) = rstwhere(0).Value
                        gGrafico1(2, intmes + 1) = rstwhere(1).Value
                    Else
                        gGrafico2(1, intmes + 1) = rstwhere(0).Value
                        gGrafico2(2, intmes + 1) = rstwhere(1).Value
                    End If
                    intmes = intmes + 1
                    rstwhere.MoveNext
                  Wend
                Case "ser":
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  segundaparte = Left(resto, InStr(resto, "/"))
                  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 9)
                  grupo = Left(segundaparte, Len(segundaparte) - 1)
                  servicio = resto
                  strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
                             "frg9ejercicio=" & ejercicio & _
                             " and fr41codgrupprod=" & grupo & _
                             " and ad02coddpto=" & servicio & _
                             " group by frh1mes"
                  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  intmes = 0
                  MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  While rstwhere.EOF = False
                    If gintMarca = 1 Then
                        gGrafico1(1, intmes + 1) = rstwhere(0).Value
                        gGrafico1(2, intmes + 1) = rstwhere(1).Value
                    Else
                        gGrafico2(1, intmes + 1) = rstwhere(0).Value
                        gGrafico2(2, intmes + 1) = rstwhere(1).Value
                    End If
                    intmes = intmes + 1
                    rstwhere.MoveNext
                  Wend
            End Select
        Case False:
            Select Case Left(Node.Key, 3)
                Case "eje":
                  ejercicio = Right(Node.Key, Len(Node.Key) - 5)
                  strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
                             "frg9ejercicio=" & ejercicio & _
                             " group by frh1mes"
                  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  intmes = 0
                  MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  While rstwhere.EOF = False
                    If gintMarca = 1 Then
                        gGrafico1(1, intmes + 1) = rstwhere(0).Value
                        gGrafico1(2, intmes + 1) = rstwhere(1).Value
                    Else
                        gGrafico2(1, intmes + 1) = rstwhere(0).Value
                        gGrafico2(2, intmes + 1) = rstwhere(1).Value
                    End If
                    intmes = intmes + 1
                    rstwhere.MoveNext
                  Wend
                Case "ser":
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 9)
                  servicio = resto
                  strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
                             "frg9ejercicio=" & ejercicio & _
                             " and ad02coddpto=" & servicio & _
                             " group by frh1mes"
                  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  intmes = 0
                  MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  While rstwhere.EOF = False
                    If gintMarca = 1 Then
                        gGrafico1(1, intmes + 1) = rstwhere(0).Value
                        gGrafico1(2, intmes + 1) = rstwhere(1).Value
                    Else
                        gGrafico2(1, intmes + 1) = rstwhere(0).Value
                        gGrafico2(2, intmes + 1) = rstwhere(1).Value
                    End If
                    intmes = intmes + 1
                    rstwhere.MoveNext
                  Wend
                Case "gru":
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  segundaparte = Left(resto, InStr(resto, "/"))
                  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 6)
                  servicio = Left(segundaparte, Len(segundaparte) - 1)
                  grupo = resto
                  strWhere = "select sum(frh1cantidadptas),sum(frh1cantidadeuro) from frh100 where " & _
                             "frg9ejercicio=" & ejercicio & _
                             " and fr41codgrupprod=" & grupo & _
                             " and ad02coddpto=" & servicio & _
                             " group by frh1mes"
                  Set rstwhere = objApp.rdoConnect.OpenResultset(strWhere)
                  intmes = 0
                  MSChart1.datagrid.ColumnLabel(gintMarca, 1) = Node.Text
                  While rstwhere.EOF = False
                    If gintMarca = 1 Then
                        gGrafico1(1, intmes + 1) = rstwhere(0).Value
                        gGrafico1(2, intmes + 1) = rstwhere(1).Value
                    Else
                        gGrafico2(1, intmes + 1) = rstwhere(0).Value
                        gGrafico2(2, intmes + 1) = rstwhere(1).Value
                    End If
                    intmes = intmes + 1
                    rstwhere.MoveNext
                  Wend
                End Select
        End Select
    For i = 1 To 12
        Call MSChart1.datagrid.SetData(i, 1, gGrafico1(gintMoneda + 1, i), 0)
        Call MSChart1.datagrid.SetData(i, 2, gGrafico2(gintMoneda + 1, i), 0)
    Next i
End Sub





