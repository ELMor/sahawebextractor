VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMantProtocolos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Definir Protocolos"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdbuscar 
      Caption         =   "Seleccionar Productos"
      Height          =   375
      Left            =   9960
      TabIndex        =   16
      Top             =   5040
      Width           =   1815
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Protocolo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Index           =   1
      Left            =   240
      TabIndex        =   8
      Top             =   480
      Width           =   11460
      Begin TabDlg.SSTab tabTab1 
         Height          =   1695
         Index           =   0
         Left            =   120
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   480
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   2990
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0028.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(14)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(28)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "dtcDateCombo1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).ControlCount=   8
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0028.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR75DESPROTOCOLO"
            Height          =   330
            Index           =   1
            Left            =   1440
            MultiLine       =   -1  'True
            TabIndex        =   1
            Tag             =   "Descripci�n Protocolo"
            Top             =   360
            Width           =   9000
         End
         Begin VB.TextBox txtText1 
            DataField       =   "FR75CODPROTOCOLO"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Protocolo"
            Top             =   360
            Width           =   1020
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1305
            Index           =   2
            Left            =   -74880
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   120
            Width           =   9855
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17383
            _ExtentY        =   2302
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR75FECINIVIG"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   2
            Tag             =   "Fecha Inicio Vigencia"
            Top             =   1200
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR75FECFINVIG"
            Height          =   330
            Index           =   1
            Left            =   2760
            TabIndex        =   3
            Tag             =   "Fecha Fin Vigencia"
            Top             =   1200
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Inicio Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   15
            Top             =   960
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Fin Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   2760
            TabIndex        =   14
            Top             =   960
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Protocolo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   120
            TabIndex        =   13
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Protocolo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   1440
            TabIndex        =   12
            Top             =   120
            Width           =   2175
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos del Protocolo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5040
      Index           =   0
      Left            =   240
      TabIndex        =   5
      Top             =   2880
      Width           =   9615
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4545
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   9360
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   16510
         _ExtentY        =   8017
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMantProtocolos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmMantProtocolos (FR0028.FRM)                               *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: AGOSTO DE 1998                                                *
'* DESCRIPCION: mantenimiento Protocolos                                *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


Private Sub cmdbuscar_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String

cmdbuscar.Enabled = False

noinsertar = True
Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
Call objsecurity.LaunchProcess("FR0029")
Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
For v = 0 To gintprodtotal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(0).Rows > 0 Then
        grdDBGrid1(0).MoveFirst
        For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(3).Value = gintprodbuscado(v, 0) _
                And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
               'no se inserta el protocolo pues ya est� insertado en el grid
                noinsertar = False
                Exit For
            Else
                noinsertar = True
            End If
        grdDBGrid1(0).MoveNext
        Next i
      End If
      If noinsertar = True Then
        Call objWinInfo.WinProcess(cwProcessToolBar, 2, 0)
        grdDBGrid1(0).Columns(3).Value = gintprodbuscado(v, 0)
        grdDBGrid1(0).Columns(4).Value = gintprodbuscado(v, 1)
        grdDBGrid1(0).Columns(5).Value = txtText1(0).Text
      Else
        mensaje = MsgBox("El producto: " & gintprodbuscado(v, 1) & " ya est� guardado." & _
                       Chr(13), vbInformation)
      End If
      noinsertar = True
Next v

cmdbuscar.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Protocolos"
      
    .strTable = "FR7500"
    
    Call .FormAddOrderField("FR75CODPROTOCOLO", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Protocolo")
    Call .FormAddFilterWhere(strKey, "FR75CODPROTOCOLO", "C�digo Protocolo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR75DESPROTOCOLO", "Descripci�n Protocolo", cwString)
    Call .FormAddFilterWhere(strKey, "FR75FECINIVIG", "Fecha Inicio Vigencia", cwDate)
    Call .FormAddFilterWhere(strKey, "FR75FECFINVIG", "Fecha Fin Vigencia", cwDate)

    Call .FormAddFilterOrder(strKey, "FR75CODPROTOCOLO", "C�digo Protocolo")
    Call .FormAddFilterOrder(strKey, "FR75DESPROTOCOLO", "Descripci�n Protocolo")
    Call .FormAddFilterOrder(strKey, "FR75FECINIVIG", "Fecha Inicio Vigencia")
    Call .FormAddFilterOrder(strKey, "FR75FECFINVIG", "Fecha Fin Vigencia")

  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Productos"
    
    .strTable = "FR1100"
    
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
    Call .FormAddRelation("FR75CODPROTOCOLO", txtText1(0))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Productos del Protocolo")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�d. Producto", cwNumeric)
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�d. Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Descripci�n Producto", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "C�d. Protocolo", "FR75CODPROTOCOLO", cwNumeric, 5)
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    .CtrlGetInfo(txtText1(1)).blnMandatory = True

    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True

    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(4), "FR73DESPRODUCTO")
    
    grdDBGrid1(0).Columns(5).Visible = False 'C�d.Protocolo
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String

If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Productos" Then
    MsgBox "S�lo puede a�adir productos mediante el buscador", vbExclamation
   Exit Sub
End If
   

If btnButton.Index = 2 Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        sqlstr = "SELECT FR75CODPROTOCOLO_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0).Text = rsta.rdoColumns(0).Value
        SendKeys ("{TAB}")
        rsta.Close
        Set rsta = Nothing
Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String

If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "Productos" Then
    MsgBox "S�lo puede a�adir productos mediante el buscador", vbExclamation
   Exit Sub
End If

If intIndex = 2 Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        sqlstr = "SELECT FR75CODPROTOCOLO_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0).Text = rsta.rdoColumns(0).Value
        SendKeys ("{TAB}")
        rsta.Close
        Set rsta = Nothing
Else
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End If
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
Dim mensaje As String
If intIndex = 1 Then
  If dtcDateCombo1(0).Text <> "" And dtcDateCombo1(1).Text <> "" Then
    If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
        mensaje = MsgBox("La fecha de Fin de Vigencia es menor que la de Inicio", vbInformation, "Aviso")
        dtcDateCombo1(1).Text = ""
    End If
  End If
End If
If intIndex = 0 Then
  If dtcDateCombo1(0).Text <> "" And dtcDateCombo1(1).Text <> "" Then
    If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
        mensaje = MsgBox("La fecha de Inicio de Vigencia es mayor que la de Fin", vbInformation, "Aviso")
        dtcDateCombo1(0).Text = ""
    End If
  End If
End If
Call objWinInfo.CtrlDataChange
End Sub


Private Sub dtcDateCombo1_Change(intIndex As Integer)
Dim mensaje As String
If intIndex = 1 Then
  If dtcDateCombo1(0).Text <> "" And dtcDateCombo1(1).Text <> "" Then
    If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
        mensaje = MsgBox("La fecha de Fin de Vigencia es menor que la de Inicio", vbInformation, "Aviso")
        dtcDateCombo1(1).Text = ""
    End If
  End If
End If
If intIndex = 0 Then
  If dtcDateCombo1(0).Text <> "" And dtcDateCombo1(1).Text <> "" Then
    If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
        mensaje = MsgBox("La fecha de Inicio de Vigencia es mayor que la de Fin", vbInformation, "Aviso")
        dtcDateCombo1(0).Text = ""
    End If
  End If
End If
Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


