VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEstPresuMes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Establecer Presupuestos Mensuales."
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8295
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Partida Presupuestaria"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6135
      Index           =   0
      Left            =   4920
      TabIndex        =   12
      Top             =   1080
      Width           =   6780
      Begin TabDlg.SSTab tabTab1 
         Height          =   5535
         Index           =   0
         Left            =   120
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   480
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   9763
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0049.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(11)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(10)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(9)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(7)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(6)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cbocombo1(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "cmdPtas"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cmdEuros"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(4)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(5)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(2)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(6)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(3)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(1)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).ControlCount=   18
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0049.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR41CODGRUPPROD"
            Height          =   330
            Index           =   1
            Left            =   600
            TabIndex        =   22
            Tag             =   "Grupo"
            Top             =   1440
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   3
            Left            =   600
            TabIndex        =   21
            Tag             =   "Servicio"
            Top             =   2520
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRG9EJERCICIO"
            Height          =   330
            Index           =   0
            Left            =   600
            TabIndex        =   20
            Tag             =   "Ejercicio"
            Top             =   735
            Width           =   492
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRH1CANTIDADEURO"
            Height          =   330
            Index           =   6
            Left            =   600
            TabIndex        =   19
            Tag             =   "Cantidad Euros"
            Top             =   4440
            Width           =   1212
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   690
            Index           =   2
            Left            =   2160
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   18
            Tag             =   "Descripci�n de Grupo"
            Top             =   1440
            Width           =   3645
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRH1CANTIDADPTAS"
            Height          =   330
            Index           =   5
            Left            =   600
            TabIndex        =   17
            Tag             =   "Cantidad Ptas"
            Top             =   3720
            Width           =   1212
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   690
            Index           =   4
            Left            =   2160
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   16
            Tag             =   "Descripci�n Servicio"
            Top             =   2520
            Width           =   3645
         End
         Begin VB.CommandButton cmdEuros 
            Caption         =   "Calcular Euros"
            Height          =   375
            Left            =   3000
            TabIndex        =   15
            Top             =   3720
            Width           =   1575
         End
         Begin VB.CommandButton cmdPtas 
            Caption         =   "Calcular Ptas"
            Height          =   375
            Left            =   3000
            TabIndex        =   14
            Top             =   4440
            Width           =   1575
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5265
            Index           =   2
            Left            =   -74880
            TabIndex        =   23
            TabStop         =   0   'False
            Top             =   120
            Width           =   5895
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   10398
            _ExtentY        =   9287
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cbocombo1 
            DataField       =   "FRH1MES"
            Height          =   315
            Index           =   0
            Left            =   2160
            TabIndex        =   24
            TabStop         =   0   'False
            Tag             =   "Mes"
            Top             =   720
            Width           =   2640
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            RowHeight       =   423
            Columns(0).Width=   4921
            Columns(0).Caption=   "Mes"
            Columns(0).Name =   "Mes"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   4657
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   600
            TabIndex        =   32
            Top             =   1200
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Ejercicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   600
            TabIndex        =   31
            Top             =   480
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Mes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   2160
            TabIndex        =   30
            Top             =   480
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   600
            TabIndex        =   29
            Top             =   2280
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cantidad Ptas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   600
            TabIndex        =   28
            Top             =   3480
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cantidad Euros"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   600
            TabIndex        =   27
            Top             =   4200
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   2160
            TabIndex        =   26
            Top             =   1200
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   2160
            TabIndex        =   25
            Top             =   2280
            Width           =   1455
         End
      End
   End
   Begin VB.OptionButton optGenerar 
      Caption         =   "Decremento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   240
      Index           =   1
      Left            =   10320
      TabIndex        =   11
      Top             =   7560
      Width           =   1575
   End
   Begin VB.OptionButton optGenerar 
      Caption         =   "Incremento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Index           =   0
      Left            =   8880
      TabIndex        =   10
      Top             =   7560
      Width           =   1455
   End
   Begin VB.TextBox txtTP 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   7680
      TabIndex        =   8
      Tag             =   "Porcentaje"
      Top             =   7440
      Width           =   492
   End
   Begin VB.CommandButton cmdGenerar 
      Caption         =   "Generar Presupuestos"
      Height          =   375
      Left            =   5520
      TabIndex        =   7
      Top             =   7440
      Width           =   1815
   End
   Begin VB.Frame Frame1 
      Height          =   495
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   4695
      Begin VB.OptionButton optGrupos 
         Caption         =   "Grupos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Index           =   1
         Left            =   840
         TabIndex        =   6
         Top             =   200
         Width           =   1215
      End
      Begin VB.OptionButton optGrupos 
         Caption         =   "Servicios"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Index           =   2
         Left            =   2280
         TabIndex        =   5
         Top             =   200
         Width           =   1695
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Ejercicios"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6720
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   4695
      Begin ComctlLib.TreeView tvwItems 
         Height          =   6075
         Index           =   0
         Left            =   360
         TabIndex        =   3
         Top             =   480
         Width           =   4080
         _ExtentX        =   7197
         _ExtentY        =   10716
         _Version        =   327682
         HideSelection   =   0   'False
         Indentation     =   353
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8010
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      Caption         =   "%"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   16
      Left            =   8280
      TabIndex        =   9
      Top             =   7500
      Width           =   255
   End
   Begin ComctlLib.ImageList imlImagenes 
      Left            =   0
      Top             =   7440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   19
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":0038
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":0096
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":00F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":0152
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":01B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":020E
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":026C
            Key             =   "rol"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":02CA
            Key             =   "function"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":0328
            Key             =   "window"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":0386
            Key             =   "report"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":03E4
            Key             =   "externalreport"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":0442
            Key             =   "process"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":04A0
            Key             =   "application"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":04FE
            Key             =   "big"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":055C
            Key             =   "small"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":05BA
            Key             =   "list"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":0618
            Key             =   "details"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":0676
            Key             =   "exit"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FR0049.frx":06D4
            Key             =   "refresh"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmEstPresuMes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmEstPresuMes (FR0049.FRM)                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Presupuestos por Mes                                    *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim mblnmoving As Boolean
Dim gblnabrir As Boolean
Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim rsta As rdoResultset
Dim sqlstr As String

  If txtText1(3).Text <> "" Then
    sqlstr = "SELECT * FROM AD0200 WHERE AD32CODTIPODPTO=3 AND AD02CODDPTO=" & txtText1(3).Text
    sqlstr = sqlstr & " AND AD02FECINICIO<=(SELECT SYSDATE FROM DUAL) AND ((AD02FECFIN IS NULL) OR (AD02FECFIN>=(SELECT SYSDATE FROM DUAL)))"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If rsta.EOF = True Then
      MsgBox "El C�d.Servicio es incorrecto.", vbExclamation
      blnCancel = True
    End If
    rsta.Close
    Set rsta = Nothing
  End If

End Sub
Private Sub rellenar_TreeView_GrupServ()
    Dim a�o As Variant
    Dim strejercicio As String
    Dim rstejercicio As rdoResultset
    Dim strMes As String
    Dim rstMes As rdoResultset
    Dim mes As Variant
    Dim strgrupo As String
    Dim rstgrupo As rdoResultset
    Dim strdesGrupo As String
    Dim rstdesGrupo As rdoResultset
    Dim grupo As Variant
    Dim desGrupo As Variant
    Dim strServicio As String
    Dim rstServicio As rdoResultset
    Dim desServicio As Variant
    Dim strdesServicio As String
    Dim rstdesServicio As rdoResultset
    Dim servicio As Variant
    
    
    tvwItems(0).Nodes.Clear
    
    strejercicio = "select distinct frg9ejercicio from frh100 order by frg9ejercicio asc"
    Set rstejercicio = objApp.rdoConnect.OpenResultset(strejercicio)
    
    
    While rstejercicio.EOF = False
      a�o = rstejercicio("frg9ejercicio").Value
      Call tvwItems(0).Nodes.Add(, , "ejerc" & a�o, a�o)
      strMes = "SELECT DISTINCT FRH1MES FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & " ORDER BY FRH1MES"
      Set rstMes = objApp.rdoConnect.OpenResultset(strMes)
      While rstMes.EOF = False
        mes = rstMes("FRH1MES").Value
        Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "mes" & a�o & "/" & mes, mes)
        strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
                     " AND FRH1MES='" & mes & "' ORDER BY FR41CODGRUPPROD"
        Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
        While rstgrupo.EOF = False
            grupo = rstgrupo("FR41CODGRUPPROD").Value
            strServicio = "SELECT DISTINCT AD02CODDPTO FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
                          " AND FRH1MES='" & mes & _
                          "' AND FR41CODGRUPPROD=" & grupo & "ORDER BY AD02CODDPTO"
            Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
            strdesGrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD=" & rstgrupo("FR41CODGRUPPROD").Value
            Set rstdesGrupo = objApp.rdoConnect.OpenResultset(strdesGrupo)
            desGrupo = rstdesGrupo("FR41DESGRUPPROD").Value
            Call tvwItems(0).Nodes.Add("mes" & a�o & "/" & mes, tvwChild, "grupo" & a�o & "/" & mes & "/" & grupo, grupo & ".-" & desGrupo)
            While rstServicio.EOF = False
                servicio = rstServicio("AD02CODDPTO").Value
                strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
                Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
                desServicio = rstdesServicio("AD02DESDPTO").Value
                Call tvwItems(0).Nodes.Add("grupo" & a�o & "/" & mes & "/" & grupo, tvwChild, "servicio" & a�o & "/" & mes & "/" & grupo & "/" & servicio, servicio & ".-" & desServicio)
                rstdesServicio.Close
                Set rstdesServicio = Nothing
                rstServicio.MoveNext
            Wend
            rstServicio.Close
            Set rstServicio = Nothing
            rstdesGrupo.Close
            Set rstdesGrupo = Nothing
            rstgrupo.MoveNext
        Wend
        rstgrupo.Close
        Set rstgrupo = Nothing
        rstMes.MoveNext
      Wend
      rstMes.Close
      Set rstMes = Nothing
      rstejercicio.MoveNext
    Wend
    rstejercicio.Close
    Set rstejercicio = Nothing


End Sub

Private Sub rellenar_TreeView_ServGrup()
    Dim a�o As Variant
    Dim strejercicio As String
    Dim rstejercicio As rdoResultset
    Dim strMes As String
    Dim rstMes As rdoResultset
    Dim mes As Variant
    Dim strgrupo As String
    Dim rstgrupo As rdoResultset
    Dim strdesGrupo As String
    Dim rstdesGrupo As rdoResultset
    Dim grupo As Variant
    Dim desGrupo As Variant
    Dim strServicio As String
    Dim rstServicio As rdoResultset
    Dim desServicio As Variant
    Dim strdesServicio As String
    Dim rstdesServicio As rdoResultset
    Dim servicio As Variant
    
    tvwItems(0).Nodes.Clear
    
    strejercicio = "select distinct frg9ejercicio from frh100 order by frg9ejercicio asc"
    Set rstejercicio = objApp.rdoConnect.OpenResultset(strejercicio)
    
    
    While rstejercicio.EOF = False
      a�o = rstejercicio("frg9ejercicio").Value
      Call tvwItems(0).Nodes.Add(, , "ejerc" & a�o, a�o)
      strMes = "SELECT DISTINCT FRH1MES FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & " ORDER BY FRH1MES"
      Set rstMes = objApp.rdoConnect.OpenResultset(strMes)
      While rstMes.EOF = False
        mes = rstMes("FRH1MES").Value
        Call tvwItems(0).Nodes.Add("ejerc" & a�o, tvwChild, "mes" & a�o & "/" & mes, mes)
        strServicio = "SELECT DISTINCT AD02CODDPTO FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
                     " AND FRH1MES='" & mes & "' ORDER BY AD02CODDPTO"
        Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
        While rstServicio.EOF = False
            servicio = rstServicio("AD02CODDPTO").Value
            strgrupo = "SELECT DISTINCT FR41CODGRUPPROD FROM FRH100 WHERE FRG9EJERCICIO=" & a�o & _
                          " AND FRH1MES='" & mes & _
                          "' AND AD02CODDPTO=" & servicio & "ORDER BY FR41CODGRUPPROD"
            Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
            strdesServicio = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & rstServicio("AD02CODDPTO").Value
            Set rstdesServicio = objApp.rdoConnect.OpenResultset(strdesServicio)
            desServicio = rstdesServicio("AD02DESDPTO").Value
            Call tvwItems(0).Nodes.Add("mes" & a�o & "/" & mes, tvwChild, "servicio" & a�o & "/" & mes & "/" & servicio, servicio & ".-" & desServicio)
            While rstgrupo.EOF = False
                grupo = rstgrupo("FR41CODGRUPPROD").Value
                strdesGrupo = "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD=" & rstgrupo("FR41CODGRUPPROD").Value
                Set rstdesGrupo = objApp.rdoConnect.OpenResultset(strdesGrupo)
                desGrupo = rstdesGrupo("FR41DESGRUPPROD").Value
                Call tvwItems(0).Nodes.Add("servicio" & a�o & "/" & mes & "/" & servicio, tvwChild, "grupo" & a�o & "/" & mes & "/" & servicio & "/" & grupo, grupo & ".-" & desGrupo)
                rstdesGrupo.Close
                Set rstdesGrupo = Nothing
                rstgrupo.MoveNext
            Wend
            rstgrupo.Close
            Set rstgrupo = Nothing
            rstdesServicio.Close
            Set rstdesServicio = Nothing
            rstServicio.MoveNext
        Wend
        rstServicio.Close
        Set rstServicio = Nothing
        rstMes.MoveNext
      Wend
      rstMes.Close
      Set rstMes = Nothing
      rstejercicio.MoveNext
    Wend
    rstejercicio.Close
    Set rstejercicio = Nothing

    

End Sub


Private Sub cmdEuros_Click()
    Dim strEuros As String
    Dim rstEuros As rdoResultset
    Dim vntEuros As Variant
    
    cmdEuros.Enabled = False
    Select Case SeparadorDec
        Case ".":
            Call objWinInfo.CtrlSet(txtText1(5), objGen.ReplaceStr(txtText1(5).Text, ",", ".", 1))
            Call objWinInfo.CtrlSet(txtText1(6), objGen.ReplaceStr(txtText1(6).Text, ",", ".", 1))
        Case ",":
            Call objWinInfo.CtrlSet(txtText1(5), objGen.ReplaceStr(txtText1(5).Text, ".", ",", 1))
            Call objWinInfo.CtrlSet(txtText1(6), objGen.ReplaceStr(txtText1(6).Text, ".", ",", 1))
    End Select
    strEuros = "select FRH2PARAMGEN from frh200 where FRH2CODPARAMGEN=1"
    Set rstEuros = objApp.rdoConnect.OpenResultset(strEuros)
    vntEuros = Format(CDec(Val(txtText1(5).Text) / Val(rstEuros(0).Value)), "##########0.00")
    Call objWinInfo.CtrlSet(txtText1(6), vntEuros) '/ rstEuros(0).Value
    'tlbToolbar1.Buttons(4).Enabled = True
    objWinInfo.objWinActiveForm.rdoCursor.Edit
    objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FRH1CANTIDADEURO").Value = txtText1(6).Text   'objWinInfo.objWinActiveForm.blnChanged = True
    objWinInfo.objWinActiveForm.rdoCursor.Update
    'MiCadena = Format(334.9, "###0.00") ' Devuelve "334,90".
    
    'hora = objGen.ReplaceStr(hora, ",", ".", 1)
    rstEuros.Close
    Set rstEuros = Nothing
    
    cmdEuros.Enabled = True
End Sub

Private Sub cmdGenerar_Click()
    Dim stra�o As String
    Dim rsta�o As rdoResultset
    Dim stra�o2 As String
    Dim rsta�o2 As rdoResultset
    Dim strInsert As String
    Dim strSigno As String
    Dim strPorc As String
    Dim intMensaje As Integer
    Dim strdelete As String
    cmdGenerar.Enabled = False
    
    If txtText1(0).Text <> "" Then
        stra�o = "select * from frh100 where frg9ejercicio=" & Val(txtText1(0).Text) + 1
        Set rsta�o = objApp.rdoConnect.OpenResultset(stra�o)
        If rsta�o.EOF = True Then
            If txtTP.Text <> "" Then
                If optGenerar(0).Value = True Then
                    strSigno = "+"
                Else
                    strSigno = "-"
                End If
                strPorc = Val(txtTP.Text) / 100
                strPorc = objGen.ReplaceStr(strPorc, ",", ".", 1)
                stra�o2 = "SELECT * FROM FRG900 WHERE FRG9EJERCICIO=" & Val(txtText1(0).Text) + 1
                Set rsta�o2 = objApp.rdoConnect.OpenResultset(stra�o2)
                If rsta�o2.EOF = True Then
                    strInsert = "INSERT INTO FRG900 VALUES (" & Val(txtText1(0).Text) + 1 & ")"
                    objApp.rdoConnect.Execute strInsert, 64
                    objApp.rdoConnect.Execute "COMMIT"
                End If
                
                strInsert = "INSERT INTO FRH100 " & _
                            "SELECT  FRG9EJERCICIO + 1," & _
                            "FR41CODGRUPPROD," & _
                            "AD02CODDPTO," & _
                            "FRH1MES," & _
                            "FRH1CANTIDADPTAS " & strSigno & " (FRH1CANTIDADPTAS * (" & strPorc & "))," & _
                            "FRH1CANTIDADEURO " & strSigno & " (FRH1CANTIDADEURO * (" & strPorc & "))" & _
                            " From FRH100" & _
                            " Where FRG9EJERCICIO=" & txtText1(0).Text
                objApp.rdoConnect.Execute strInsert, 64
                objApp.rdoConnect.Execute "COMMIT"
                If optGrupos(1).Value = True Then
                    Call rellenar_TreeView_GrupServ
                Else
                    Call rellenar_TreeView_ServGrup
                End If
            Else
                Call MsgBox("Debe poner un Porcentaje", vbInformation)
            End If
        Else
            intMensaje = MsgBox("El Presupuesto ya ha sido introducido. �Desea Continuar?", vbInformation + vbYesNo)
            If intMensaje = vbYes Then
                strdelete = "delete frh100 where frg9ejercicio=" & Val(txtText1(0).Text) + 1
                objApp.rdoConnect.Execute strdelete, 64
                objApp.rdoConnect.Execute "COMMIT"
                Call cmdGenerar_Click
            End If
        End If
    Else
        Call MsgBox("Debe tener selecionado un a�o", vbInformation)
    End If
    
    cmdGenerar.Enabled = True
End Sub

Private Sub cmdPtas_Click()
    Dim strPtas As String
    Dim rstPtas As rdoResultset
    Dim vntPtas As Variant
    
    cmdPtas.Enabled = False
    Select Case SeparadorDec
        Case ".":
            Call objWinInfo.CtrlSet(txtText1(5), objGen.ReplaceStr(txtText1(5).Text, ",", ".", 1))
            Call objWinInfo.CtrlSet(txtText1(6), objGen.ReplaceStr(txtText1(6).Text, ",", ".", 1))
        Case ",":
            Call objWinInfo.CtrlSet(txtText1(5), objGen.ReplaceStr(txtText1(5).Text, ".", ",", 1))
            Call objWinInfo.CtrlSet(txtText1(6), objGen.ReplaceStr(txtText1(6).Text, ".", ",", 1))
    End Select
    strPtas = "select FRH2PARAMGEN from frh200 where FRH2CODPARAMGEN=1"
    Set rstPtas = objApp.rdoConnect.OpenResultset(strPtas)
    vntPtas = Format(CDec(Val(txtText1(6).Text) * Val(rstPtas(0).Value)), "##########0.00")
    Call objWinInfo.CtrlSet(txtText1(5), vntPtas) '/ rstEuros(0).Value
    'objWinInfo.objWinActiveForm.blnChanged = True
    objWinInfo.objWinActiveForm.rdoCursor.Edit
    objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("FRH1CANTIDADPTAS").Value = txtText1(5).Text   'objWinInfo.objWinActiveForm.blnChanged = True
    objWinInfo.objWinActiveForm.rdoCursor.Update
    rstPtas.Close
    Set rstPtas = Nothing
    
    cmdPtas.Enabled = True
End Sub

Private Sub Form_Activate()

  cbocombo1(0).RemoveAll
  Call cbocombo1(0).AddItem("")
  Call cbocombo1(0).AddItem("01.ENERO")
  Call cbocombo1(0).AddItem("02.FEBRERO")
  Call cbocombo1(0).AddItem("03.MARZO")
  Call cbocombo1(0).AddItem("04.ABRIL")
  Call cbocombo1(0).AddItem("05.MAYO")
  Call cbocombo1(0).AddItem("06.JUNIO")
  Call cbocombo1(0).AddItem("07.JULIO")
  Call cbocombo1(0).AddItem("08.AGOSTO")
  Call cbocombo1(0).AddItem("09.SEPTIEMBRE")
  Call cbocombo1(0).AddItem("10.OCTUBRE")
  Call cbocombo1(0).AddItem("11.NOVIEMBRE")
  Call cbocombo1(0).AddItem("12.DICIEMBRE")
  
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
    Dim vntclave As Variant
  On Error GoTo GERROR
  If gblnabrir = True And intNewStatus = cwModeSingleEdit And intOldStatus = cwModeSingleOpen Then
    gblnabrir = False
    vntclave = "servicio" & txtText1(0).Text & "/" & cbocombo1(0).Value & "/" & txtText1(1).Text & "/" & txtText1(3).Text
    Call tvwItems_NodeClick(0, tvwItems(0).Nodes(vntclave))
    Set tvwItems(0).SelectedItem = tvwItems(0).Nodes(vntclave).FirstSibling
  End If
GERROR:
    Exit Sub
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMasterInfo2 As New clsCWForm
  Dim objMasterInfo3 As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
                                
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strTable = "FRH100"
    
    Call .FormAddOrderField("FRG9EJERCICIO", cwAscending)
    Call .FormAddOrderField("FRH1MES", cwAscending)
    Call .FormAddOrderField("FR41CODGRUPPROD", cwAscending)
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    
    '.strWhere = "FR73CODPRODUCTO=" & frmDefProducto.txtText1(0).Text
   
    .intAllowance = cwAllowModify
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Ejercicio")
    Call .FormAddFilterWhere(strKey, "FRG9EJERCICIO", "Ejercicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRH1MES", "Mes", cwString)
    Call .FormAddFilterWhere(strKey, "FR41CODGRUPPROD", "Cod Grupo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "Cod Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRH1CANTIDADPTAS", "Cantidad Ptas", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRH1CANTIDADEURO", "Cantidad Euros", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "FRG9EJERCICIO", "Ejercicio")
    Call .FormAddFilterOrder(strKey, "FRH1MES", "Mes")
    Call .FormAddFilterOrder(strKey, "FR41CODGRUPPROD", "Cod Grupo")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "Cod Servicio")
  End With
  
  
  
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
  
    Call .FormCreateInfo(objMasterInfo)
    
    

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR41CODGRUPPROD", "SELECT FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(2), "FR41DESGRUPPROD")
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(3)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(3)), txtText1(4), "AD02CODDPTO")
    .CtrlGetInfo(txtText1(3)).blnForeign = True
    
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  
  
  tvwItems(0).Visible = True
  
  'Call rellenar_TreeView_GrupServ
  optGrupos(1).Value = True
  optGenerar(0).Value = True

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    Select Case SeparadorDec
        Case ".":
            Call objWinInfo.CtrlSet(txtText1(5), objGen.ReplaceStr(txtText1(5).Text, ",", ".", 1))
            Call objWinInfo.CtrlSet(txtText1(6), objGen.ReplaceStr(txtText1(6).Text, ",", ".", 1))
        Case ",":
            Call objWinInfo.CtrlSet(txtText1(5), objGen.ReplaceStr(txtText1(5).Text, ".", ",", 1))
            Call objWinInfo.CtrlSet(txtText1(6), objGen.ReplaceStr(txtText1(6).Text, ".", ",", 1))
    End Select

End Sub


Private Sub optGrupos_Click(Index As Integer)
    If Index = 1 Then
        Call rellenar_TreeView_GrupServ
    Else
        Call rellenar_TreeView_ServGrup
    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
      
Dim rsta As rdoResultset
Dim sqlstr As String
Dim vntclave As Variant
  
  Select Case btnButton.Index
  Case 3 'Abrir
    objWinInfo.objWinActiveForm.strWhere = ""
    objWinInfo.DataRefresh
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    gblnabrir = True
    Exit Sub
  Case 30 'salir
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  Case 21 'Primero
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      tvwItems(0).SelectedItem.FirstSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    Exit Sub
  Case 22 'Anterior
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      If tvwItems(0).SelectedItem.FirstSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Previous.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    Exit Sub
  Case 23 'Siguiente
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      If tvwItems(0).SelectedItem.LastSibling <> tvwItems(0).SelectedItem Then
        tvwItems(0).SelectedItem.Next.Selected = True
        Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
      End If
    Exit Sub
  Case 24 'Ultimo
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      tvwItems(0).SelectedItem.LastSibling.Selected = True
      Call tvwItems_NodeClick(0, tvwItems(0).SelectedItem)
    Exit Sub
  Case Else 'Otro boton
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  End Select
  

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 40 'Primero
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(21))
  Case 50 'Anterior
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(22))
  Case 60 'Siguiente
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(23))
  Case 70 'Ultimo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24))
  Case Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub tvwItems_Collapse(Index As Integer, ByVal Node As ComctlLib.Node)

        Node.EnsureVisible
        Node.Selected = True
        Call tvwItems_NodeClick(0, Node)

End Sub

Private Sub tvwItems_Expand(Index As Integer, ByVal Node As ComctlLib.Node)

  'If EnPostWrite = True Then
  '  EnPostWrite = False
  '  Exit Sub
  'End If

  Node.EnsureVisible
  Node.Selected = True
  Call tvwItems_NodeClick(0, Node)

End Sub

'Private Sub tvwItems_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'  Set nodX = tvwItems(0).SelectedItem ' Establece el elemento arrastrado.
'End Sub

Private Sub tvwItems_NodeClick(Index As Integer, ByVal Node As ComctlLib.Node)
    
Dim ejercicio As Variant
Dim mes As Variant
Dim grupo As Variant
Dim servicio As Variant
Dim primeraparte As Variant
Dim segundaparte As Variant
Dim terceraparte As Variant
Dim resto As Variant
    
    Select Case optGrupos(1).Value
        Case True:
        
            Select Case Left(Node.Key, 3)
                Case "eje":
                  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                  Call objWinInfo.DataOpen
                  ejercicio = Right(Node.Key, Len(Node.Key) - 5)
                  objWinInfo.objWinActiveForm.strWhere = "frg9ejercicio=" & ejercicio
                  objWinInfo.DataRefresh
                Case "mes":
                  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  mes = resto
                  ejercicio = Right(Left(Node.Key, InStr(Node.Key, "/") - 1), Len(Left(Node.Key, InStr(Node.Key, "/") - 1)) - 3)
                  objWinInfo.objWinActiveForm.strWhere = "frg9ejercicio=" & ejercicio & " and frh1mes='" & mes & "'"
                  objWinInfo.DataRefresh
                Case "gru":
                  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  segundaparte = Left(resto, InStr(resto, "/"))
                  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 6)
                  mes = Left(segundaparte, Len(segundaparte) - 1)
                  grupo = resto
                  objWinInfo.objWinActiveForm.strWhere = "frg9ejercicio=" & ejercicio & _
                                                        " and frh1mes='" & mes & _
                                                        "' and fr41codgrupprod=" & grupo
                  objWinInfo.DataRefresh
                Case "ser":
                  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  segundaparte = Left(resto, InStr(resto, "/"))
                  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  terceraparte = Left(resto, InStr(resto, "/"))
                  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 9)
                  mes = Left(segundaparte, Len(segundaparte) - 1)
                  grupo = Left(terceraparte, Len(terceraparte) - 1)
                  servicio = resto
                  objWinInfo.objWinActiveForm.strWhere = "frg9ejercicio=" & ejercicio & _
                                                        " and frh1mes='" & mes & _
                                                        "' and fr41codgrupprod=" & grupo & _
                                                        " and ad02coddpto=" & servicio
                  objWinInfo.DataRefresh
            End Select
        Case False:
            Select Case Left(Node.Key, 3)
                Case "eje":
                  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                  Call objWinInfo.DataOpen
                  ejercicio = Right(Node.Key, Len(Node.Key) - 5)
                  objWinInfo.objWinActiveForm.strWhere = "frg9ejercicio=" & ejercicio
                  objWinInfo.DataRefresh
                Case "mes":
                  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  mes = resto
                  ejercicio = Right(Left(Node.Key, InStr(Node.Key, "/") - 1), Len(Left(Node.Key, InStr(Node.Key, "/") - 1)) - 3)
                  objWinInfo.objWinActiveForm.strWhere = "frg9ejercicio=" & ejercicio & " and frh1mes='" & mes & "'"
                  objWinInfo.DataRefresh
                Case "ser":
                  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  segundaparte = Left(resto, InStr(resto, "/"))
                  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 9)
                  mes = Left(segundaparte, Len(segundaparte) - 1)
                  servicio = resto
                  objWinInfo.objWinActiveForm.strWhere = "frg9ejercicio=" & ejercicio & _
                                                        " and frh1mes='" & mes & _
                                                        "' and ad02coddpto=" & servicio
                  objWinInfo.DataRefresh
                Case "gru":
                  Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
                  primeraparte = Left(Node.Key, InStr(Node.Key, "/"))
                  resto = Right(Node.Key, Len(Node.Key) - InStr(Node.Key, "/"))
                  segundaparte = Left(resto, InStr(resto, "/"))
                  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  terceraparte = Left(resto, InStr(resto, "/"))
                  resto = Right(resto, Len(resto) - InStr(resto, "/"))
                  ejercicio = Right(Left(primeraparte, Len(primeraparte) - 1), Len(primeraparte) - 6)
                  mes = Left(segundaparte, Len(segundaparte) - 1)
                  servicio = Left(terceraparte, Len(terceraparte) - 1)
                  grupo = resto
                  objWinInfo.objWinActiveForm.strWhere = "frg9ejercicio=" & ejercicio & _
                                                        " and frh1mes='" & mes & _
                                                        "' and fr41codgrupprod=" & grupo & _
                                                        " and ad02coddpto=" & servicio
                  objWinInfo.DataRefresh
                End Select
        End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)

  Dim objField As clsCWFieldSearch
  Dim objSearch As clsCWSearch
  
  If strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR4100"
     .strOrder = "ORDER BY FR41CODGRUPPROD ASC"
         
     Set objField = .AddField("FR41CODGRUPPROD")
     objField.strSmallDesc = "C�digo Grupo Producto"
         
     Set objField = .AddField("FR41DESGRUPPROD")
     objField.strSmallDesc = "Descripci�n Grupo Producto"
         
     If .Search Then
        Call objWinInfo.CtrlSet(txtText1(2), .cllValues("FR41DESGRUPPROD"))
     End If
   End With
   Set objSearch = Nothing
 End If

  If strCtrl = "txtText1(3)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strOrder = "ORDER BY AD02CODDPTO ASC"
     .strWhere = " WHERE AD32CODTIPODPTO = 3" & _
                 " AND AD02FECINICIO < (SELECT SYSDATE FROM DUAL)" & _
                 " AND ((AD02FECFIN IS NULL) OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL)))"
         
     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo Servicio"
         
     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n Servicio"
         
     If .Search Then
        If strCtrl = "txtText1(3)" Then
            Call objWinInfo.CtrlSet(txtText1(4), .cllValues("AD02CODDPTO"))
        End If
     End If
   End With
   Set objSearch = Nothing
 End If


End Sub

