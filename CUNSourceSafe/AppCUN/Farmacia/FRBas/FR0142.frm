VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDispSerEsp 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Dispensar Servicios Especiales. Seleccionar Unidad"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame 
      Caption         =   "Servicio"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2640
      Index           =   2
      Left            =   120
      TabIndex        =   20
      Top             =   480
      Width           =   3375
      Begin VB.TextBox txtServicio 
         BackColor       =   &H00808080&
         Height          =   330
         Index           =   1
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Tag             =   "Descripci�n Servicio"
         Top             =   1440
         Width           =   3135
      End
      Begin SSDataWidgets_B.SSDBCombo cboservicio 
         Height          =   315
         Left            =   120
         TabIndex        =   22
         Tag             =   "C�digo Servicio"
         Top             =   720
         Width           =   1095
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2223
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1931
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�digo Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   24
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Descripci�n Servicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   23
         Top             =   1200
         Width           =   1815
      End
   End
   Begin VB.CommandButton cmdtransdatos 
      Caption         =   "Preparar Reposici�n"
      Height          =   1035
      Left            =   10800
      Picture         =   "FR0142.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   5040
      Width           =   975
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Necesidad Unidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Index           =   1
      Left            =   3600
      TabIndex        =   11
      Top             =   480
      Width           =   8100
      Begin TabDlg.SSTab tabTab1 
         Height          =   2175
         Index           =   0
         Left            =   120
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   360
         Width           =   7815
         _ExtentX        =   13785
         _ExtentY        =   3836
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0142.frx":0842
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txtText1(7)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(3)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(4)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).ControlCount=   12
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0142.frx":085E
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   1
            Left            =   -73560
            TabIndex        =   5
            Tag             =   "Peticionario"
            Top             =   1680
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR55FECPETICION"
            Height          =   330
            Index           =   7
            Left            =   -71160
            TabIndex        =   6
            Tag             =   "Fecha Petici�n"
            Top             =   480
            Width           =   1815
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "SG02COD_PDS"
            Height          =   330
            Index           =   6
            Left            =   -74880
            TabIndex        =   4
            Tag             =   "Cod.Peticionario"
            Top             =   1680
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   5
            Left            =   -74280
            TabIndex        =   3
            Tag             =   "Desc.Estado Petici�n"
            Top             =   1080
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR26CODESTPETIC"
            Height          =   330
            Index           =   2
            Left            =   -74880
            TabIndex        =   2
            Tag             =   "Estado Petici�n"
            Top             =   1080
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR55CODNECESUNID"
            Height          =   330
            Index           =   0
            Left            =   -74880
            TabIndex        =   0
            Tag             =   "C�d.Nec.Unidad"
            Top             =   480
            Width           =   885
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   3
            Left            =   -72720
            TabIndex        =   1
            Tag             =   "C�d.Servicio"
            Top             =   480
            Width           =   525
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2145
            Index           =   2
            Left            =   0
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   0
            Width           =   7815
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   13785
            _ExtentY        =   3784
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   -71160
            TabIndex        =   19
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Peticionario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -74880
            TabIndex        =   17
            Top             =   1440
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Estado Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   -74880
            TabIndex        =   16
            Top             =   840
            Width           =   1995
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cod. Necesidad Unidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   -74880
            TabIndex        =   15
            Top             =   240
            Width           =   2025
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Codigo Servicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   -72720
            TabIndex        =   14
            Top             =   240
            Width           =   1455
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Necesidades del Servicio"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4800
      Index           =   0
      Left            =   120
      TabIndex        =   8
      Top             =   3120
      Width           =   10575
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4305
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   10305
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   18177
         _ExtentY        =   7594
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDispSerEsp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDispSerEsp (FR0142.FRM)                                   *
'* AUTOR: JUAN RODRIGUEZ CORRAL                                         *
'* FECHA: NOVIEMBRE DE 1998                                             *
'* DESCRIPCION: Dispensar Servicios Especiales                          *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cboservicio_Change()
  
  Call cboservicio_CloseUp

End Sub

Private Sub cboservicio_CloseUp()

Dim strauxwhere As String

  strauxwhere = ""

  If cboservicio <> "" Then
    strauxwhere = "AD02CODDPTO=" & cboservicio
    txtServicio(1).Text = cboservicio.Columns(1).Value
  Else
    txtServicio(1).Text = ""
    strauxwhere = "AD02CODDPTO IS NULL "
  End If
  
  Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
  objWinInfo.cllWinForms(1).strWhere = strauxwhere & " AND FR26CODESTPETIC=4 "
  objWinInfo.DataRefresh

End Sub

Private Sub cboservicio_KeyUp(KeyCode As Integer, Shift As Integer)

  Call cboservicio_CloseUp

End Sub

Private Sub cmdtransdatos_Click()
  
  cmdtransdatos.Enabled = False
  'se llama a frmPrepReposicion
  If grdDBGrid1(2).Rows = 0 Then
    MsgBox "No hay ninguna Necesidad de Unidad para reponer.", vbInformation
  Else
    Call objsecurity.LaunchProcess("FR0143")
  End If
  cmdtransdatos.Enabled = True

End Sub

Private Sub Form_Activate()
Dim stra As String
Dim rsta As rdoResultset

  cboservicio.RemoveAll
  Call cboservicio.AddItem("" & ";" & "")
  stra = "select AD02CODDPTO,AD02DESDPTO from AD0200" & _
          "WHERE AD32CODTIPODPTO=3 AND " & _
           "AD02FECINICIO<(SELECT SYSDATE FROM DUAL) AND " & _
           "((AD02FECFIN IS NULL) OR (AD02FECFIN>(SELECT SYSDATE FROM DUAL)))" & _
           " order by AD02CODDPTO "
  
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  While (Not rsta.EOF)
      Call cboservicio.AddItem(rsta.rdoColumns("AD02CODDPTO").Value & ";" & rsta.rdoColumns("AD02DESDPTO").Value)
      rsta.MoveNext
  Wend
  rsta.Close
  Set rsta = Nothing
  
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.DataRefresh

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Necesidad Unidad"
      
    .strTable = "FR5500"
    .intAllowance = cwAllowReadOnly
    .strWhere = "AD02CODDPTO IS NULL AND FR26CODESTPETIC=4" 'VALIDADA
    
    Call .FormAddOrderField("FR55FECPETICION", cwAscending)
    Call .FormAddOrderField("FR55CODNECESUNID", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Necesidad Unidad")
    Call .FormAddFilterWhere(strKey, "FR55CODNECESUNID", "C�digo Necesidad Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR26CODESTPETIC", "C�digo Estado Petici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD_PDS", "C�digo Persona Dispensa", cwString)
    Call .FormAddFilterWhere(strKey, "FR55FECPETICION", "Fecha Petici�n", cwDate)
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Detalle Necesidad"
    .intAllowance = cwAllowReadOnly
    
    .strTable = "FR1900"
    
    Call .FormAddOrderField("FR19FECVALIDACION", cwAscending)
    Call .FormAddRelation("FR55CODNECESUNID", txtText1(0))
        
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "N�m.L�nea", "FR20NUMLINEA", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo, "C�d.Nec.Unidad", "FR55CODNECESUNID", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�d.Nec.Validado", "FR19CODNECESVAL", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�d.Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Prod.", "", cwString, 7)
    Call .GridAddColumn(objMultiInfo, "Desc.Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "C�d.Unidad Medida", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Desc.Uni.Medida", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Cant.Necesaria", "FR19CANTNECESQUIR", cwDecimal, 11)
    Call .GridAddColumn(objMultiInfo, "C�d.Pers.Vali.", "SG02COD_VAL", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Pers.Vali.", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Fecha Vali.", "FR19FECVALIDACION", cwDate)
    Call .GridAddColumn(objMultiInfo, "C�d.Uni.Med.Sumi.", "FR93CODUNIMEDIDA_SUM", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Cant.Sumi.Farma.", "FR19CANTSUMIFARM", cwDecimal, 11)
    
    Call .FormCreateInfo(objMasterInfo)
    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
  
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR26CODESTPETIC", "SELECT FR26DESESTADOPET FROM FR2600 WHERE FR26CODESTPETIC = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(5), "FR26DESESTADOPET")
  
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "SG02COD_PDS", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 PETICIONARIO FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(1), "PETICIONARIO")
  
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(7), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(8), "FR73DESPRODUCTO")
     
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(9)), "FR93CODUNIMEDIDA", "SELECT * FROM FR9300 WHERE FR93CODUNIMEDIDA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(9)), grdDBGrid1(0).Columns(10), "FR93DESUNIMEDIDA")
     
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), "SG02COD_VAL", "SELECT SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 VALIDADOR FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(12)), grdDBGrid1(0).Columns(13), "VALIDADOR")
    
    'grdDBGrid1(0).Columns(3).Visible = False
    'grdDBGrid1(0).Columns(5).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
 
 grdDBGrid1(2).Columns("C�d.Nec.Unidad").Width = 1400
 grdDBGrid1(2).Columns("C�d.Servicio").Width = 1100
 grdDBGrid1(2).Columns("Estado Petici�n").Width = 1300
 grdDBGrid1(2).Columns("Desc.Estado Petici�n").Width = 1300
 grdDBGrid1(2).Columns("Cod.Peticionario").Width = 1100
 grdDBGrid1(2).Columns("Peticionario").Width = 2000
 grdDBGrid1(2).Columns("Fecha Petici�n").Width = 1200
 grdDBGrid1(2).Columns("C�d.Servicio").Visible = False
 
 grdDBGrid1(0).Columns(3).Width = 1000
 grdDBGrid1(0).Columns(4).Width = 1350
 grdDBGrid1(0).Columns(5).Width = 1450
 grdDBGrid1(0).Columns(6).Width = 1200
 grdDBGrid1(0).Columns(8).Width = 3000
 grdDBGrid1(0).Columns(9).Width = 1550
 grdDBGrid1(0).Columns(10).Width = 2000
 grdDBGrid1(0).Columns(11).Width = 1300
 grdDBGrid1(0).Columns(12).Width = 1200
 grdDBGrid1(0).Columns(14).Width = 1000
 grdDBGrid1(0).Columns(15).Width = 1500
 grdDBGrid1(0).Columns(16).Width = 1400
 grdDBGrid1(0).Columns(3).Visible = False
 grdDBGrid1(0).Columns(4).Visible = False
 grdDBGrid1(0).Columns(5).Visible = False
 grdDBGrid1(0).Columns(15).Visible = False
 grdDBGrid1(0).Columns(16).Visible = False
 
 Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
'
'
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


