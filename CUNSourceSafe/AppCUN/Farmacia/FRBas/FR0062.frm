VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMantCGCF 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MANTENIMIENTO  FARMACIA.Mantener Tablas del CGCF"
   ClientHeight    =   8340
   ClientLeft      =   2550
   ClientTop       =   3090
   ClientWidth     =   11670
   ClipControls    =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11670
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   11670
      _ExtentX        =   20585
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Campos de la Tabla"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4875
      Index           =   1
      Left            =   90
      TabIndex        =   8
      Top             =   3015
      Width           =   11385
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4380
         Index           =   1
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   11115
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   19606
         _ExtentY        =   7726
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Tablas del CGCF"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2490
      Index           =   0
      Left            =   90
      TabIndex        =   1
      Top             =   450
      Width           =   11385
      Begin TabDlg.SSTab tabTab1 
         Height          =   1950
         HelpContextID   =   90001
         Index           =   0
         Left            =   135
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   405
         Width           =   11010
         _ExtentX        =   19420
         _ExtentY        =   3440
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(6)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(5)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRG7TABCGCF"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   450
            TabIndex        =   3
            Tag             =   "Tabla FoxPro|Nombre de la tabla en FoxPro"
            Top             =   540
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRG7TABORAC"
            Height          =   330
            HelpContextID   =   40102
            Index           =   5
            Left            =   450
            TabIndex        =   4
            Tag             =   "Tabla ORACLE|Nombre de la tabla en ORACLE"
            Top             =   1215
            Width           =   5400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1770
            Index           =   0
            Left            =   -74910
            TabIndex        =   5
            Top             =   90
            Width           =   10365
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18283
            _ExtentY        =   3122
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tabla FoxPro"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   450
            TabIndex        =   7
            Top             =   315
            Width           =   1140
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tabla ORACLE"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   450
            TabIndex        =   6
            Top             =   990
            Width           =   1290
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11670
      _ExtentX        =   20585
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMantCGCF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'*******************************************************************************************
'***** Nombre l�gico: FrmMantCGCF
'***** Nombre f�sico: Frm0062.frm
'***** Autor: Jes�s M� Rodilla Lara
'***** Fecha creaci�n:04/01/98
'***** Fecha �ltima modificaci�n:
'***** Descripci�n:
'***** Es un mantenimiento de las tablas FRG700 (tablas de la base de datos del CGCF) y de
'***** la tabla FRG800 (campos de las tablas del CGCF). En el proceso que se encarga de
'***** actualizar la base de datos del CGCF es necesario hacer una select para cada tabla de
'***** FoxPro, obtener sus datos e insertarlos en las tablas de ORACLE. En la tabla FRG700
'***** est�n el nombre de la tabla en FoxPro y el nombre de las tablas en ORACLE.
'***** En la tabla FRG800 est� el nombre de la tabla en Foxpro, el nombre del campos y su
'***** tipo en FoxPro, el nombre del campo correspondiente y su tipo en ORACLE.
'***** Se presentar� como un maestro/detalle. En la cabecera (detalle/tabla) aparecer�n los registros del tabla FRG700 y en el detalle aparecer�n los resgistros (en modo multil�nea) de la tabla FRG800.
'***** Los campos que aparecer�n en el maestro ser�n:
'***** � Nombre Tabla FoxPro: es un campo obligatorio que deber� teclear el usuario.
'*****   Se podr� Filtar, Buscar y Ordenar por dicho campo. Es el nombre de la tabla en FoxPro.
'***** � Nombre Tabla ORACLE: es un campo obligatorio que deber� teclear el usuario.
'*****   Se podr� Filtar, Buscar y Ordenar por dicho campo. Es el nombre de la tabla en ORACLE.
'***** Los campos que aparecer�n en el detalle ser�:
'***** � Nombre Campo FoxPro: es un campo obligatorio que deber� teclear el usuario.
'*****   Se podr� Filtar, Buscar y Ordenar por dicho campo. Es el nombre del campo en FoxPro.
'***** � Tipo Campo FoxPro: es un campo obligatorio que deber� teclear el usuario.
'*****   Se podr� Filtar, Buscar y Ordenar por dicho campo. Es el tipo del campo en FoxPro.
'***** � Nombre  Campo ORACLE: es un campo obligatorio que deber� teclear el usuario.
'*****   Se podr� Filtar, Buscar y Ordenar por dicho campo. Es el nombre del campo en ORACLE.
'***** � Tipo Campo ORACLE: es un campo obligatorio que deber� teclear el usuario.
'*****   Se podr� Filtar, Buscar y Ordenar por dicho campo. Es el tipo del campo en ORACLE.
'***** Estos datos se introducir�n cada vez que cambie la estructura de los archivos dbf de
'***** FoxPro. Junto con la aplicaci�n se entregar� un juego de ensayo con los valores de las
'***** todas estas tuplas, de manera que el usuario final no tenga nada que hacer. Si se hace
'***** este mantenimiento en para facilitar futuras modificaciones que haya que hacer al
'***** proceso que actualiza la base de datos del CGCF si se diese el caso de que el CGCF
'***** cambia la estructura de estos archivos.
'*****
'***** Tablas: FRG700 , FRG800
'***** Secuencias:
'*******************************************************************************************


Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    .strName = "Tablas"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "FRG700"
    Call .FormAddOrderField("FRG7TABCGCF", cwAscending)
    strKey = .strDataBase & .strTable
    
    'Se montan los filtros
    Call .FormCreateFilterWhere(strKey, "Tablas")
    Call .FormAddFilterWhere(strKey, "FRG7TABCGCF", "Tabla FoxPro", cwString)
    Call .FormAddFilterWhere(strKey, "FRG7TABORAC", "Tabla ORACLE", cwString)
    
    'Se establecen los campos por los que se puede ordenar
    Call .FormAddFilterOrder(strKey, "FRG7TABCGCF", "Tabla FoxPro")
    Call .FormAddFilterOrder(strKey, "FRG7TABORAC", "Tabla ORACLE")
    
  End With
  
  With objMultiInfo
    .strName = "Campos"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "FRG800"

    Call .FormAddOrderField("FRG8COLCGCF", cwAscending)
    Call .FormAddRelation("FRG7TABCGCF", txtText1(6))
  
    strKey = .strDataBase & .strTable
    
    'Se establecen los campos por los que se puede filtrar
    Call .FormCreateFilterWhere(strKey, "Campos de la tabla")
    Call .FormAddFilterWhere(strKey, "FRG8COLCGCF", "Campo FoxPro", cwString)
    Call .FormAddFilterWhere(strKey, "FRG8TYPCGCF", "Tipo FoxPro", cwString)
    Call .FormAddFilterWhere(strKey, "FRG8COLORAC", "Campo ORACLE", cwString)
    Call .FormAddFilterWhere(strKey, "FRG8TYPORAC", "Tipo ORACLE", cwString)
    
    'Se montan los campos por los que se puede ordenar
    Call .FormAddFilterOrder(strKey, "FRG8COLCGCF", "Campo FoxPro")
    Call .FormAddFilterOrder(strKey, "FRG8TYPCGCF", "Tipo FoxPro")
    Call .FormAddFilterOrder(strKey, "FRG8COLORAC", "Campo ORACLE")
    Call .FormAddFilterOrder(strKey, "FRG8TYPORAC", "Tipo ORACLE")
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    'Se indican las columnas que aparecer�n en el grid
    Call .GridAddColumn(objMultiInfo, "Campo FoxPro", "FRG8COLCGCF", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Tipo FoxPro", "FRG8TYPCGCF", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Campo ORACLE", "FRG8COLORAC", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Tipo ORACLE", "FRG8TYPORAC", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Tabla FoxPro", "FRG7TABCGCF", cwString, 30)
    

    Call .FormCreateInfo(objMasterInfo)
    Call .FormChangeColor(objMultiInfo)
  
    'Campos por los que se puede buscar en las tabla
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    
    'Campos por los que se puede buscar en los campos de la tabla
    .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
       
       
    Call .WinRegister
    Call .WinStabilize
  End With
    grdDBGrid1(1).Columns(3).Width = 2480
    grdDBGrid1(1).Columns(4).Width = 2480
    grdDBGrid1(1).Columns(5).Width = 2480
    grdDBGrid1(1).Columns(6).Width = 2480
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


