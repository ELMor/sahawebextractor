VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form FrmRegDevEnv 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. CONTROLAR ESTUPEFACIENTES. Registrar Devoluci�n Envases. "
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0108.frx":0000
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdDevuelto 
      Caption         =   "Devuelto"
      Height          =   375
      Left            =   4800
      TabIndex        =   4
      Top             =   7440
      Width           =   1935
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Devoluci�n de Envases"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6615
      Index           =   2
      Left            =   240
      TabIndex        =   0
      Tag             =   "Actuaciones Asociadas"
      Top             =   600
      Width           =   10935
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6135
         Index           =   1
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   10650
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         stylesets.count =   1
         stylesets(0).Name=   "Bloqueada"
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "FR0108.frx":000C
         SelectTypeRow   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18785
         _ExtentY        =   10821
         _StockProps     =   79
         Caption         =   "DEVOLUCI�N DE ENVASES"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmRegDevEnv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmRegDevEnv (FR0108.FRM)                                    *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: 5 DE NOVIEMBRE DE 1998                                        *
'* DESCRIPCION: Registrar Devoluci�n de Envases                         *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


Private Sub cmdDevuelto_Click()
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Estupefacientes"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR1300"
        .intAllowance = cwAllowModify
        .strWhere = "FR13INDDEVOLPENDI=-1"
        
        Call .FormAddOrderField("FR13CODCTRLESTUPEFA", cwAscending)
    
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Estupefacientes")
        Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n Producto", cwString)
        Call .FormAddFilterWhere(strKey, "FR13CANTSUM", "Cantidad", cwNumeric)
        Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
        'Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
        'Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
        Call .FormAddFilterWhere(strKey, "FR13FECMOVIMIENTO", "Fecha Movimiento", cwDate)
        Call .FormAddFilterWhere(strKey, "SG02COD_MRP", "Cod. M�dico Prescriptor", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "SG02APE1", "Doctor", cwString)
        Call .FormAddFilterWhere(strKey, "FR04CODALMACEN_DES", "Cod. Almacen Destino", cwNumeric)
        'Call .FormAddFilterWhere(strKey, "FR04DESALMACEN", "Almacen Destino", cwString)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
        'Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTO", "Descripci�n Producto")
        Call .FormAddFilterOrder(strKey, "FR13CANTSUM", "Cantidad")
        Call .FormAddFilterOrder(strKey, "CI21CODPERSONA", "C�digo Paciente")
        'Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
        'Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
        'Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
        Call .FormAddFilterOrder(strKey, "FR13FECMOVIMIENTO", "Fecha Movimiento")
        Call .FormAddFilterOrder(strKey, "SG02COD_MRP", "Cod. M�dico Prescriptor")
        'Call .FormAddFilterOrder(strKey, "SG02APE1", "Doctor")
        Call .FormAddFilterOrder(strKey, "FR04CODALMACEN_DES", "Cod. Almacen Destino")
        'Call .FormAddFilterOrder(strKey, "FR04DESALMACEN", "Almacen Destino")
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones asociadas
        Call .GridAddColumn(objMultiInfo, "Dev. Pendiente", "FR13INDDEVOLPENDI", cwBoolean)
        Call .GridAddColumn(objMultiInfo, "Cantidad Devuelta", "FR13CANTDEV", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "Cod. Producto", "FR73CODPRODUCTO", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "Descripci�n", "", cwString)
        Call .GridAddColumn(objMultiInfo, "Cantidad", "FR13CANTSUM", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "C�digo Paciente", "CI21CODPERSONA", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString)
        Call .GridAddColumn(objMultiInfo, "Primer Apellido", "", cwString)
        Call .GridAddColumn(objMultiInfo, "Segundo Apellido", "", cwString)
        Call .GridAddColumn(objMultiInfo, "C�digo M�dico", "SG02COD_MPR", cwString)
        Call .GridAddColumn(objMultiInfo, "Doctor", "", cwString)
        Call .GridAddColumn(objMultiInfo, "Fecha Movimiento", "FR13FECMOVIMIENTO", cwDate)
        Call .GridAddColumn(objMultiInfo, "C�d. Almac�n Destino", "FR04CODALMACEN_DES", cwNumeric)
        Call .GridAddColumn(objMultiInfo, "Almac�n", "", cwString)
        Call .GridAddColumn(objMultiInfo, "Fecha Devoluci�n", "FR13FECDEVENVASE", cwDate)
        Call .GridAddColumn(objMultiInfo, "C�digo", "FR13CODCTRLESTUPEFA", cwNumeric)
        
        Call .FormCreateInfo(objMultiInfo)
    
        'Se indica que campos son obligatorios y cuales son clave primaria
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        '.CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(12)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(13)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(14)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(15)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(16)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnInFind = True
        
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(12)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(13)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(14)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(15)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(16)).blnReadOnly = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnReadOnly = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE  FR73CODPRODUCTO= ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "FR73DESPRODUCTO")
                
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(9), "CI22NOMBRE")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(10), "CI22PRIAPEL")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(11), "CI22SEGAPEL")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(12)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(12)), grdDBGrid1(1).Columns(13), "SG02APE1")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(15)), "FR04CODALMACEN", "SELECT * FROM FR0400 WHERE FR04CODALMACEN = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(15)), grdDBGrid1(1).Columns(16), "FR04DESALMACEN")
        
        Call .WinRegister
        Call .WinStabilize
    End With
    grdDBGrid1(1).Columns(3).Width = 1300
    grdDBGrid1(1).Columns(4).Width = 1600
    grdDBGrid1(1).Columns(5).Width = 1200
    grdDBGrid1(1).Columns(6).Width = 3000
    grdDBGrid1(1).Columns(7).Width = 800
    grdDBGrid1(1).Columns(8).Width = 1400
    grdDBGrid1(1).Columns(9).Width = 1500
    grdDBGrid1(1).Columns(10).Width = 1500
    grdDBGrid1(1).Columns(11).Width = 1500
    grdDBGrid1(1).Columns(12).Width = 1200
    grdDBGrid1(1).Columns(13).Width = 1500
    grdDBGrid1(1).Columns(14).Width = 2000
    grdDBGrid1(1).Columns(15).Width = 1200
    grdDBGrid1(1).Columns(16).Width = 2500
    grdDBGrid1(1).Columns(17).Width = 2000
    grdDBGrid1(1).Columns(18).Visible = False
    
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    Dim strupdate As String
    
    strupdate = "UPDATE FR1300 SET FR13FECDEVENVASE=SYSDATE" & _
               " WHERE FR13CODCTRLESTUPEFA=" & grdDBGrid1(1).Columns(18).Value
    objApp.rdoConnect.Execute strupdate, 64
    
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Estupefacientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Dim intMsg As Integer
    
    If btnButton.Index = 4 Then
        intMsg = MsgBox("�Est� seguro de la devoluci�n de este envase?", vbQuestion + vbYesNo)
        If intMsg = vbNo Then
            Exit Sub
        End If
        If grdDBGrid1(1).Columns(3).Value = False Then
            If grdDBGrid1(1).Columns(4).Value = "" Then
                Call MsgBox("Debe indicar la Cantidad Devuelta", vbInformation)
                Exit Sub
            End If
        Else
           Call MsgBox("Debe indicar que se ha devuelto el envase", vbInformation)
           Exit Sub
        End If
    End If
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Dim intMsg As Integer
    
    If intIndex = 4 Then
        intMsg = MsgBox("�Est� seguro de la devoluci�n de este envase?", vbQuestion + vbYesNo)
        If intMsg = vbNo Then
            Exit Sub
        End If
        If grdDBGrid1(1).Columns(3).Value = False Then
            If grdDBGrid1(1).Columns(4).Value = "" Then
                Call MsgBox("Debe indicar la Cantidad Devuelta", vbInformation)
                Exit Sub
            End If
        Else
           Call MsgBox("Debe indicar que se ha devuelto el envase", vbInformation)
           Exit Sub
        End If
    End If
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub
Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


