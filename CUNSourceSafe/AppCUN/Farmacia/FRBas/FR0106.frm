VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmVerPerfilFTP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMINISTRAR MEDICAMENTOS. Consultar Perfil Farmacoterap�utico."
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   10680
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0106.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Index           =   1
      Left            =   240
      TabIndex        =   8
      Top             =   480
      Width           =   11340
      Begin TabDlg.SSTab tabTab1 
         Height          =   1575
         HelpContextID   =   90001
         Index           =   2
         Left            =   240
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   360
         Width           =   10770
         _ExtentX        =   18997
         _ExtentY        =   2778
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0106.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lbllabel1(24)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lbllabel1(21)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lbllabel1(20)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lbllabel1(19)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lbllabel1(17)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(22)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(21)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(20)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(19)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(18)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0106.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            DataField       =   "CI22NOMBRE"
            Height          =   330
            Index           =   18
            Left            =   1800
            TabIndex        =   2
            Tag             =   "Nombre"
            Top             =   360
            Width           =   2700
         End
         Begin VB.TextBox txtText1 
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            Index           =   19
            Left            =   5400
            TabIndex        =   4
            Tag             =   "Segundo Apellido"
            Top             =   960
            Width           =   2700
         End
         Begin VB.TextBox txtText1 
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            Index           =   20
            Left            =   1800
            TabIndex        =   3
            Tag             =   "Primer Apellido"
            Top             =   960
            Width           =   2700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            Index           =   21
            Left            =   240
            TabIndex        =   1
            Tag             =   "Historia"
            Top             =   960
            Width           =   852
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   22
            Left            =   240
            TabIndex        =   0
            Tag             =   "C�digo Persona"
            Top             =   360
            Width           =   852
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1365
            Index           =   0
            Left            =   -74880
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   120
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   2408
            _StockProps     =   79
            Caption         =   "PACIENTES"
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   1800
            TabIndex        =   16
            Top             =   120
            Width           =   660
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Apellido 1�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   1800
            TabIndex        =   15
            Top             =   720
            Width           =   930
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Apellido 2�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   5400
            TabIndex        =   14
            Top             =   720
            Width           =   930
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Historia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   240
            TabIndex        =   13
            Top             =   720
            Width           =   660
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   24
            Left            =   240
            TabIndex        =   12
            Top             =   120
            Width           =   705
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Perfil Farmacoterap�utico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5295
      Index           =   0
      Left            =   240
      TabIndex        =   5
      Top             =   2640
      Width           =   11385
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4815
         Index           =   1
         Left            =   240
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   360
         Width           =   10890
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   19209
         _ExtentY        =   8493
         _StockProps     =   79
         Caption         =   "PERFIL FARMACOTERAP�UTICO"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmVerPerfilFTP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: FR0106.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 30 DE SEPTIEMBRE DE 1998                                      *
'* DESCRIPCI�N: Consulta Perfil FTP                                     *
'* ARGUMENTOS:                                                          *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
    
    If glngPaciente > 0 Then
        Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
    Else
        Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
    End If
  With objMasterInfo
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(2)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Pacientes"
      
    .strTable = "CI2200"
    .intAllowance = cwAllowReadOnly
    If glngPaciente > 0 Then
        .strWhere = "CI21CODPERSONA=" & glngPaciente
    End If
    Call .FormAddOrderField("CI21CODPERSONA", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Pacientes")
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Paciente", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI22HISTORIA", "Historia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
    Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
        
    Call .FormAddFilterOrder(strKey, "CI21CODPERSONA", "C�digo Paciente")
    Call .FormAddFilterOrder(strKey, "CI22HISTORIA", "Historia")
    Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
    Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
    Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")

    
  End With
  
    With objMultiInfo
        .strName = "Perfil FTP"
        Set .objFormContainer = fraFrame1(0)
        Set .objFatherContainer = fraFrame1(1)
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR6500"
        .intAllowance = cwAllowReadOnly
        
        Call .FormAddOrderField("FR65FECHA", cwAscending)
        Call .FormAddOrderField("FR65HORA", cwAscending)
    
        strKey = .strDataBase & .strTable
    Call .FormAddRelation("CI21CODPERSONA", txtText1(22))
        'Se establecen los campos por los que se puede filtrar
    Call .FormCreateFilterWhere(strKey, "Registrar Administraci�n")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Medicamento", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR65FECHA", "Fecha", cwDate)
    Call .FormAddFilterWhere(strKey, "FR65HORA", "Hora", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR65MLPORHORA", "Ml por Hora", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR65GOTASPORMINUTO", "Gotas por Minuto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR65INDCONSUMIDO", "�Consumido?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR65INDRECETA", "�Receta?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR65CANTRECETA", "Cantidad Receta", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Servicio", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR34CODVIA", "C�digo V�a", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR65DOSIS", "D�sis", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Medicamento")
    Call .FormAddFilterOrder(strKey, "FR65FECHA", "Fecha")
    Call .FormAddFilterOrder(strKey, "FR65HORA", "Hora")
    
    End With

    With objWinInfo
        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        Call .GridAddColumn(objMultiInfo, "Fecha", "FR65FECHA", cwDate)
        Call .GridAddColumn(objMultiInfo, "Hora", "FR65HORA", cwNumeric, 4)
        Call .GridAddColumn(objMultiInfo, "C�digo Medicamento", "FR73CODPRODUCTO", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Descripci�n Medicamento", "", cwString, 50)
        Call .GridAddColumn(objMultiInfo, "D�sis", "FR65DOSIS", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo, "Cod. Frecuencia", "FRG4CODFRECUENCIA", cwString, 15)
        Call .GridAddColumn(objMultiInfo, "Frecuencia", "", cwString, 15)
        Call .GridAddColumn(objMultiInfo, "Ml por Hora", "FR65MLPORHORA", cwNumeric, 6)
        Call .GridAddColumn(objMultiInfo, "Gotas por Minuto", "FR65GOTASPORMINUTO", cwNumeric, 6)
        Call .GridAddColumn(objMultiInfo, "�Consumido?", "FR65INDCONSUMIDO", cwBoolean)
        Call .GridAddColumn(objMultiInfo, "Cod Enfermera", "SG02COD_ENF", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Enfermera", "", cwString, 25)
        Call .GridAddColumn(objMultiInfo, "�Receta?", "FR65INDRECETA", cwDate)
        Call .GridAddColumn(objMultiInfo, "Cantidad Receta", "FR65CANTRECETA", cwNumeric, 4)
        Call .GridAddColumn(objMultiInfo, "C�digo Servicio", "AD02CODDPTO", cwNumeric, 3)
        Call .GridAddColumn(objMultiInfo, "Servicio", "", cwString, 50)
        Call .GridAddColumn(objMultiInfo, "C�digo V�a", "FR34CODVIA", cwNumeric, 2)
        Call .GridAddColumn(objMultiInfo, "V�a", "", cwString, 50)
        Call .GridAddColumn(objMultiInfo, "Aditivos", "FR65ADITIVOS", cwString, 255)
        Call .GridAddColumn(objMultiInfo, "Observaciones", "FR65OBSERV", cwString, 2000)
        
        
        Call .FormCreateInfo(objMasterInfo)
    
        'Se indica que campos son obligatorios y cuales son clave primaria
        
        Call .FormChangeColor(objMultiInfo)
        
        .CtrlGetInfo(txtText1(22)).blnInFind = True
        .CtrlGetInfo(txtText1(21)).blnInFind = True
        .CtrlGetInfo(txtText1(18)).blnInFind = True
        .CtrlGetInfo(txtText1(20)).blnInFind = True
        .CtrlGetInfo(txtText1(19)).blnInFind = True
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(12)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(13)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(14)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(18)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(19)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(20)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(21)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(22)).blnInFind = True
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "FR73DESPRODUCTO")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(19)), "FR34CODVIA", "SELECT * FROM FR3400 WHERE FR34CODVIA=?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(19)), grdDBGrid1(1).Columns(20), "FR34DESVIA")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(17)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO=?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(17)), grdDBGrid1(1).Columns(18), "AD02DESDPTO")
          
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(13)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD=?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(13)), grdDBGrid1(1).Columns(14), "SG02APE1")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), "FRG4CODFRECUENCIA", "SELECT * FROM FRG400 WHERE FRG4CODFRECUENCIA=?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(8)), grdDBGrid1(1).Columns(9), "FRD4DESFRECUENCIA")
        
        Call .WinRegister
        Call .WinStabilize
    End With

grdDBGrid1(1).Columns(5).Visible = False 'c�d.producto
grdDBGrid1(1).Columns(17).Visible = False 'c�d.servicio
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  'Dim intReport As Integer
  'Dim objPrinter As clsCWPrinter
  'Dim blnHasFilter As Boolean
  '
  'If strFormName = "Actuaciones" Then
  '  Call objWinInfo.FormPrinterDialog(True, "")
  '  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  '  intReport = objPrinter.Selected
  '  If intReport > 0 Then
  '    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
  '    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
  '                               objWinInfo.DataGetOrder(blnHasFilter, True))
  '  End If
  '  Set objPrinter = Nothing
  'End If
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub




