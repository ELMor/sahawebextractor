VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************

'REDACTAR ORDEN MEDICA
Const PRWinOM                         As String = "FR0111"
Const PRWinsituacioncarro             As String = "FR0114"
Const PRWinBuscarGruProt              As String = "FR0117"
Const PRWinBuscarProt                 As String = "FR0118"
Const PRWinBuscarGruProd              As String = "FR0120"
Const PRWinBuscarProd                 As String = "FR0119"
Const PRWinConsOMAnte                 As String = "FR0115"
Const PRWinVerIntMedica               As String = "FR0116"
Const PRWinFirmarOMPRN                As String = "FR0121"
Const PRWinVerOMSinEnv                As String = "FR0122"
Const PRWinEstOMSinEnv                As String = "FR0123"
Const PRWinVisOMPRN                   As String = "FR0124"
Const PRWinEstPetFarm                 As String = "FR0125"
Const PRWinModLinea                   As String = "FR0150"
Const PRWinSelPaciente                As String = "FR0152"
Const PRWinBuscarGrpTerap             As String = "FR0153"
Const PRWinAlergiasPaciente           As String = "FR0155"

'ADMINISTRAR MEDICAMENTOS
Const PRWinRegAdministr               As String = "FR0137"
Const PRWinVerPerfilFTP               As String = "FR0106"

'CONTROLAR INTERACCIONES MEDICAMENTOSAS
Const PRWinDefIntMedica               As String = "FR0110"

'CONTROLAR ESTUPEFACIENTES
Const PRWinMonPetEstup                As String = "FR0103"
Const PRWinVerOriPeticion             As String = "FR0104"
Const PRWinBloquearLineas             As String = "FR0105"
Const PRWinDispEstupef                As String = "FR0107"
Const PRWinRegDevEnv                  As String = "FR0108"
Const PRWinMonLibEstupef              As String = "FR0109"

'CONTROLAR MEDICAMENTOS RUM
Const PRWinDefMedBajRUM               As String = "FR0112"

'PEDIR QUIR�FANO FARMACIA
Const PRWinRedHojQuirof               As String = "FR0126"
Const PRWinFirmarHojQuirof            As String = "FR0128"
Const PRWinRedHojAnest                As String = "FR0127"
Const PRWinFirmarHojAnest             As String = "FR0129"
Const PRWinConfNecQuirof              As String = "FR0147"
Const PRWinValNecQuirof               As String = "FR0130"

'DISPENSAR STOCK DE PLANTA
Const PRWinDispUrgencias              As String = "FR0145"
Const PRWinDispSinPRN                 As String = "FR0146"

'DISPENSAR DOSIS UNITARIA
Const PRWinPrepCargaCarro             As String = "FR0138"
Const PRWinCrtlIncidencias            As String = "FR0140"
Const PRWinAnalizDiferenc             As String = "FR0139"
Const PRWinRecogerCarro               As String = "FR0141"
Const PRWinCrtlIncidenciasDif         As String = "FR0151"
Const PRWinSitCarro                   As String = "FR0157"

'CONTROLAR ANTIINFECCIOSOS
Const PRWinDefPedBajCtrl              As String = "FR0101"

'DISPENSAR SERVICIOS ESPECIALES
Const PRWinDispSerEsp                 As String = "FR0142"
Const PRWinPrepReposicion             As String = "FR0143"

'PEDIR SERVICIO FARMACIA
Const PRWinRedPedStPlanta             As String = "FR0131"
Const PRWinFirmarPedStPta             As String = "FR0132"

'LOTES
Const PRWinMantLotes                  As String = "FR0025"

Const PRWinVerPedPtaNoEnv             As String = "FR0133"
Const PRWinEstPedPtaNoEnv             As String = "FR0134"
Const PRWinValPedPtaFarma             As String = "FR0135"
Const PRWinVerPedPtaNoVal             As String = "FR0136"

'PROTOCOLOS DE QUIROFANO
Const PRWinDefProtQuirofano           As String = "FR0154"



Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess

    'REDACTAR ORDEN M�DICA
    Case PRWinOM
      Load frmRedactarOMPRN
      'Call objsecurity.AddHelpContext(528)
      Call frmRedactarOMPRN.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRedactarOMPRN
      Set frmRedactarOMPRN = Nothing
    Case PRWinsituacioncarro
      Load frmVerSitCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmVerSitCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerSitCarro
      Set frmVerSitCarro = Nothing
    Case PRWinBuscarGruProt
      Load frmBusGrpProt
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpProt.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusGrpProt
      Set frmBusGrpProt = Nothing
    Case PRWinBuscarProt
      Load frmBusProtocolos
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProtocolos.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusProtocolos
      Set frmBusProtocolos = Nothing
    Case PRWinBuscarGruProd
      Load frmBusGrpProd
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpProd.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBusGrpProd
      Set frmBusGrpProd = Nothing
    Case PRWinBuscarProd
      Load frmBusProductos
      'Call objsecurity.AddHelpContext(528)
      Call frmBusProductos.Show(vbModal)
     Call objsecurity.RemoveHelpContext
      Unload frmBusProductos
      Set frmBusProductos = Nothing
    Case PRWinConsOMAnte
      Load frmConsOMAnte
      'Call objsecurity.AddHelpContext(528)
      Call frmConsOMAnte.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmConsOMAnte
      Set frmConsOMAnte = Nothing
    Case PRWinVerIntMedica
      Load frmVerIntMedica
      'Call objsecurity.AddHelpContext(528)
      Call frmVerIntMedica.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerIntMedica
      Set frmVerIntMedica = Nothing
    Case PRWinFirmarOMPRN
      Load frmFirmarOMPRN
      'Call objsecurity.AddHelpContext(528)
      Call frmFirmarOMPRN.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmFirmarOMPRN
      Set frmFirmarOMPRN = Nothing
    Case PRWinVerOMSinEnv
      Load frmVerOMSinEnv
      'Call objsecurity.AddHelpContext(528)
      Call frmVerOMSinEnv.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerOMSinEnv
      Set frmVerOMSinEnv = Nothing
    Case PRWinEstOMSinEnv
      Load frmEstOMSinEnv
      'Call objsecurity.AddHelpContext(528)
      Call frmEstOMSinEnv.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmEstOMSinEnv
      Set frmEstOMSinEnv = Nothing
    Case PRWinVisOMPRN
      Load frmVisOMPRN
      'Call objsecurity.AddHelpContext(528)
      Call frmVisOMPRN.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVisOMPRN
      Set frmVisOMPRN = Nothing
    Case PRWinEstPetFarm
      Load frmEstPetFarm
      'Call objsecurity.AddHelpContext(528)
      Call frmEstPetFarm.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmEstPetFarm
      Set frmEstPetFarm = Nothing
    Case PRWinModLinea
      Load frmModLinea
      'Call objsecurity.AddHelpContext(528)
      Call frmModLinea.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmModLinea
      Set frmModLinea = Nothing
    Case PRWinSelPaciente
      Load frmSelPaciente
      'Call objsecurity.AddHelpContext(528)
      Call frmSelPaciente.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmSelPaciente
      Set frmSelPaciente = Nothing
    
    Case PRWinAlergiasPaciente
      Load frmAlergiasPaciente
      'Call objsecurity.AddHelpContext(528)
      Call frmAlergiasPaciente.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmAlergiasPaciente
      Set frmAlergiasPaciente = Nothing
      
      
      
    'ADMINISTRAR MEDICAMENTOS
    Case PRWinRegAdministr
      Load FrmRegAdministr
      'Call objsecurity.AddHelpContext(528)
      Call FrmRegAdministr.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload FrmRegAdministr
      Set FrmRegAdministr = Nothing
    Case PRWinVerPerfilFTP
      Load frmVerPerfilFTP
      'Call objsecurity.AddHelpContext(528)
      Call frmVerPerfilFTP.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerPerfilFTP
      Set frmVerPerfilFTP = Nothing
      glngPaciente = 0
    
    'CONTROLAR INTERACCIONES MEDICAMENTOSAS
     Case PRWinDefIntMedica
      Load frmDefIntMedica
      'Call objsecurity.AddHelpContext(528)
      Call frmDefIntMedica.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefIntMedica
      Set frmDefIntMedica = Nothing
    
    'CONTROLAR ESTUPEFACIENTES
     Case PRWinMonPetEstup
      Load frmMonPetEstup
      'Call objsecurity.AddHelpContext(528)
      Call frmMonPetEstup.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmMonPetEstup
      Set frmMonPetEstup = Nothing
     Case PRWinVerOriPeticion
      Load frmVerOriPeticion
      'Call objsecurity.AddHelpContext(528)
      Call frmVerOriPeticion.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmVerOriPeticion
      Set frmVerOriPeticion = Nothing
     Case PRWinDispEstupef
      Load FrmDispEstupef
      'Call objsecurity.AddHelpContext(528)
      Call FrmDispEstupef.Show(vbModal)
      'Call FrmDispEstupef.RemoveHelpContext
      Unload FrmDispEstupef
      Set FrmDispEstupef = Nothing
     Case PRWinBloquearLineas
      gvntdatafr0105(1) = vntData(1)
      gvntdatafr0105(2) = vntData(2)
      gvntdatafr0105(3) = vntData(3)
      gvntdatafr0105(4) = vntData(4)
      gvntdatafr0105(5) = vntData(5)
      gvntdatafr0105(6) = vntData(6)
      gvntdatafr0105(7) = vntData(7)
      gvntdatafr0105(8) = vntData(8)
      gvntdatafr0105(9) = vntData(9)
      
      Load frmBloqueoLinea
      'Call objsecurity.AddHelpContext(528)
      Call frmBloqueoLinea.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmBloqueoLinea
      Set frmBloqueoLinea = Nothing
     Case PRWinRegDevEnv
      Load FrmRegDevEnv
      'Call objsecurity.AddHelpContext(528)
      Call FrmRegDevEnv.Show(vbModal)
      'Call FrmDispEstupef.RemoveHelpContext
      Unload FrmRegDevEnv
      Set FrmRegDevEnv = Nothing
     Case PRWinMonLibEstupef
      Load FrmMonLibEstupef
      'Call objsecurity.AddHelpContext(528)
      Call FrmMonLibEstupef.Show(vbModal)
      'Call FrmDispEstupef.RemoveHelpContext
      Unload FrmMonLibEstupef
      Set FrmMonLibEstupef = Nothing

     Case PRWinBuscarGrpTerap
      Load frmBusGrpTera
      'Call objsecurity.AddHelpContext(528)
      Call frmBusGrpTera.Show(vbModal)
      'Call FrmDispEstupef.RemoveHelpContext
      Unload frmBusGrpTera
      Set frmBusGrpTera = Nothing
'CONTROLAR MEDICAMENTOS RUM
    Case PRWinDefMedBajRUM
      Load frmDefMedBajRUM
      'Call objsecurity.AddHelpContext(528)
      Call frmDefMedBajRUM.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefMedBajRUM
      Set frmDefMedBajRUM = Nothing
    
    'PEDIR QUIR�FANO FARMACIA
    Case PRWinRedHojQuirof
    
      gstrLlamador = vntData(1)
      gintcodhojaquiro = vntData(2)
    
      Load frmRedHojQuirof
      'Call objsecurity.AddHelpContext(528)
      Call frmRedHojQuirof.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRedHojQuirof
      Set frmRedHojQuirof = Nothing
    
      gstrLlamador = ""
      gintcodhojaquiro = 0
    
    Case PRWinFirmarHojQuirof
      Load frmFirmarHojQuirof
      'Call objsecurity.AddHelpContext(528)
      Call frmFirmarHojQuirof.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmFirmarHojQuirof
      Set frmFirmarHojQuirof = Nothing
    
    Case PRWinRedHojAnest
    
      gstrLlamador = vntData(1)
      gintcodhojaanest = vntData(2)
    
      Load frmRedHojAnest
      'Call objsecurity.AddHelpContext(528)
      Call frmRedHojAnest.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRedHojAnest
      Set frmRedHojAnest = Nothing
    
      gstrLlamador = ""
      gintcodhojaquiro = 0
    
    Case PRWinFirmarHojAnest
      Load frmFirmarHojAnest
      'Call objsecurity.AddHelpContext(528)
      Call frmFirmarHojAnest.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmFirmarHojAnest
      Set frmFirmarHojAnest = Nothing
      
    Case PRWinConfNecQuirof
      Load frmConfNecQuirof
      'Call objsecurity.AddHelpContext(528)
      Call frmConfNecQuirof.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmConfNecQuirof
      Set frmConfNecQuirof = Nothing
      
    Case PRWinValNecQuirof
      Load frmValNecQuirof
      'Call objsecurity.AddHelpContext(528)
      Call frmValNecQuirof.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmValNecQuirof
      Set frmValNecQuirof = Nothing
    
    'DISPENSAR STOCK DE PLANTA
    Case PRWinDispUrgencias
      Load frmDispUrgencias
      'Call objsecurity.AddHelpContext(528)
      Call frmDispUrgencias.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDispUrgencias
      Set frmDispUrgencias = Nothing

     Case PRWinDispSinPRN
      Load frmDispSinPRN
      'Call objsecurity.AddHelpContext(528)
      Call frmDispSinPRN.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDispSinPRN
      Set frmDispSinPRN = Nothing
      
    'DISPENSAR DOSIS UNITARIA
    Case PRWinPrepCargaCarro
      Load frmPrepCargaCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmPrepCargaCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmPrepCargaCarro
      Set frmPrepCargaCarro = Nothing
      
    Case PRWinCrtlIncidencias
      Load frmCrtlIncidencias
      'Call objsecurity.AddHelpContext(528)
      Call frmCrtlIncidencias.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmCrtlIncidencias
      Set frmCrtlIncidencias = Nothing
      
    Case PRWinCrtlIncidenciasDif
      Load frmCrtlIncidenciasDif
      'Call objsecurity.AddHelpContext(528)
      Call frmCrtlIncidenciasDif.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmCrtlIncidenciasDif
      Set frmCrtlIncidenciasDif = Nothing
      
    Case PRWinAnalizDiferenc
      Load frmAnalizDiferenc
      'Call objsecurity.AddHelpContext(528)
      Call frmAnalizDiferenc.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmAnalizDiferenc
      Set frmAnalizDiferenc = Nothing
      
    Case PRWinRecogerCarro
      Load frmRecogerCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmRecogerCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmRecogerCarro
      Set frmRecogerCarro = Nothing
    Case PRWinSitCarro
      Load frmSitCarro
      'Call objsecurity.AddHelpContext(528)
      Call frmSitCarro.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmSitCarro
      Set frmSitCarro = Nothing
      
    'CONTROLAR ANTIINFECCIOSOS
    Case PRWinDefPedBajCtrl
      Load frmDefPedBajCtrl
      'Call objsecurity.AddHelpContext(528)
      Call frmDefPedBajCtrl.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDefPedBajCtrl
      Set frmDefPedBajCtrl = Nothing
    
    'DISPENSAR SERVICIOS ESPECIALES
    Case PRWinDispSerEsp
      Load frmDispSerEsp
      'Call objsecurity.AddHelpContext(528)
      Call frmDispSerEsp.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmDispSerEsp
      Set frmDispSerEsp = Nothing
    Case PRWinPrepReposicion
      Load frmPrepReposicion
      'Call objsecurity.AddHelpContext(528)
      Call frmPrepReposicion.Show(vbModal)
      Call objsecurity.RemoveHelpContext
      Unload frmPrepReposicion
      Set frmPrepReposicion = Nothing
      
    'PEDIR SERVICIO FARMACIA
    Case PRWinRedPedStPlanta
      Load FrmRedPedStPlanta
      'Call objsecurity.AddHelpContext(528)
      Call FrmRedPedStPlanta.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmRedPedStPlanta
      Set FrmRedPedStPlanta = Nothing
    Case PRWinFirmarPedStPta
      Load frmFirmarPedStPta
      'Call objsecurity.AddHelpContext(528)
      Call frmFirmarPedStPta.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmFirmarPedStPta
      Set frmFirmarPedStPta = Nothing
      
    'LOTES
    Case PRWinMantLotes
      Load frmMantLotes
      'Call objsecurity.AddHelpContext(528)
      Call frmMantLotes.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmMantLotes
      Set frmMantLotes = Nothing
      
      
    Case PRWinVerPedPtaNoEnv
      Load frmVerPedPtaNoEnv
      'Call objsecurity.AddHelpContext(528)
      Call frmVerPedPtaNoEnv.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmVerPedPtaNoEnv
      Set frmVerPedPtaNoEnv = Nothing
    Case PRWinEstPedPtaNoEnv
      Load FrmEstPedPtaNoEnv
      'Call objsecurity.AddHelpContext(528)
      Call FrmEstPedPtaNoEnv.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmEstPedPtaNoEnv
      Set FrmEstPedPtaNoEnv = Nothing
    Case PRWinValPedPtaFarma
      Load frmValPedPtaFarma
      'Call objsecurity.AddHelpContext(528)
      Call frmValPedPtaFarma.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmValPedPtaFarma
      Set frmValPedPtaFarma = Nothing
    Case PRWinVerPedPtaNoVal
      Load FrmVerPedPtaNoVal
      'Call objsecurity.AddHelpContext(528)
      Call FrmVerPedPtaNoVal.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload FrmVerPedPtaNoVal
      Set FrmVerPedPtaNoVal = Nothing
      
    Case PRWinDefProtQuirofano
      Load frmDefProtQuirofano
      'Call objsecurity.AddHelpContext(528)
      Call frmDefProtQuirofano.Show(vbModal)
      'Call objsecurity.RemoveHelpContext
      Unload frmDefProtQuirofano
      Set frmDefProtQuirofano = Nothing
      
'    'Case Else
'    '  ' el c�digo de proceso no es v�lido y se devuelve falso
'    '  LaunchProcess = False
  End Select
  Call ERR.Clear

End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 52, 1 To 4) As Variant
       
  
  'REDACTAR ORDEN M�DICA
  
  aProcess(1, 1) = PRWinOM
  aProcess(1, 2) = "Redactar Orden M�dica"
  aProcess(1, 3) = False
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinsituacioncarro
  aProcess(2, 2) = "Situaci�n de los Carros"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PRWinBuscarGruProt
  aProcess(3, 2) = "Buscar Grupo de Protocolos"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = PRWinBuscarProt
  aProcess(4, 2) = "Buscar Protocolos"
  aProcess(4, 3) = False
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = PRWinBuscarGruProd
  aProcess(5, 2) = "Buscar Grupo de Productos"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow
  
  aProcess(6, 1) = PRWinBuscarProd
  aProcess(6, 2) = "Buscar Productos"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = PRWinConsOMAnte
  aProcess(7, 2) = "OM anteriores"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
  
  aProcess(8, 1) = PRWinVerIntMedica
  aProcess(8, 2) = "Ver Interacciones"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = PRWinFirmarOMPRN
  aProcess(9, 2) = "Firmar OM/PRN"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
  
  aProcess(10, 1) = PRWinVerOMSinEnv
  aProcess(10, 2) = "Enviar OM/PRN"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow
  
  aProcess(11, 1) = PRWinEstOMSinEnv
  aProcess(11, 2) = "Estudiar OM/PRN sin enviar"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
  
  aProcess(12, 1) = PRWinVisOMPRN
  aProcess(12, 2) = "Visualizar OM/PRN"
  aProcess(12, 3) = True
  aProcess(12, 4) = cwTypeWindow
  
  aProcess(13, 1) = PRWinEstPetFarm
  aProcess(13, 2) = "Estudiar Petici�n Farmacia"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeWindow
  
  aProcess(14, 1) = PRWinModLinea
  aProcess(14, 2) = "Modificar L�nea"
  aProcess(14, 3) = False
  aProcess(14, 4) = cwTypeWindow
 
  
  'ADMINISTRAR MEDICAMENTOS
  aProcess(15, 1) = PRWinRegAdministr
  aProcess(15, 2) = "Registrar Administraci�n"
  aProcess(15, 3) = True
  aProcess(15, 4) = cwTypeWindow
  
  aProcess(16, 1) = PRWinVerPerfilFTP
  aProcess(16, 2) = "Consultar Perfil FTP"
  aProcess(16, 3) = True
  aProcess(16, 4) = cwTypeWindow
  
  'CONTROLAR INTERACCIONES MEDICAMENTOSAS
  aProcess(17, 1) = PRWinDefIntMedica
  aProcess(17, 2) = "Definir Interacciones Medicamentosas"
  aProcess(17, 3) = True
  aProcess(17, 4) = cwTypeWindow
  
  'CONTROLAR ESTUPEFACIENTES
  aProcess(18, 1) = PRWinMonPetEstup
  aProcess(18, 2) = "Monitorizar Peticiones Estupefacientes"
  aProcess(18, 3) = True
  aProcess(18, 4) = cwTypeWindow
  
  aProcess(19, 1) = PRWinVerOriPeticion
  aProcess(19, 2) = "Ver ON/PRN"
  aProcess(19, 3) = False
  aProcess(19, 4) = cwTypeWindow
  
  aProcess(20, 1) = PRWinBloquearLineas
  aProcess(20, 2) = "Bloquear L�neas"
  aProcess(20, 3) = False
  aProcess(20, 4) = cwTypeWindow
  
  aProcess(21, 1) = PRWinDispEstupef
  aProcess(21, 2) = "Dispensar Estupefacientes"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeWindow
  
  aProcess(22, 1) = PRWinRegDevEnv
  aProcess(22, 2) = "Registrar Devoluci�n Envases"
  aProcess(22, 3) = True
  aProcess(22, 4) = cwTypeWindow
  
  aProcess(23, 1) = PRWinMonLibEstupef
  aProcess(23, 2) = "Monitorizar Libro Estupefacientes"
  aProcess(23, 3) = True
  aProcess(23, 4) = cwTypeWindow
  
  'CONTROLAR MEDICAMENTOS RUM
  aProcess(24, 1) = PRWinDefMedBajRUM
  aProcess(24, 2) = "Definir Medicamento Bajo RUM"
  aProcess(24, 3) = True
  aProcess(24, 4) = cwTypeWindow
  
  'PEDIR QUIR�FANO FARMACIA
  aProcess(25, 1) = PRWinRedHojQuirof
  aProcess(25, 2) = "Redactar Hoja de Quir�fano"
  aProcess(25, 3) = True
  aProcess(25, 4) = cwTypeWindow
  
  aProcess(26, 1) = PRWinFirmarHojQuirof
  aProcess(26, 2) = "Firmar Hoja de Quir�fano"
  aProcess(26, 3) = True
  aProcess(26, 4) = cwTypeWindow
  
  aProcess(27, 1) = PRWinRedHojAnest
  aProcess(27, 2) = "Redactar Hoja de Anestesia"
  aProcess(27, 3) = True
  aProcess(27, 4) = cwTypeWindow
  
  aProcess(28, 1) = PRWinFirmarHojAnest
  aProcess(28, 2) = "Firmar Hoja de Anestesia"
  aProcess(28, 3) = True
  aProcess(28, 4) = cwTypeWindow
  
  aProcess(29, 1) = PRWinConfNecQuirof
  aProcess(29, 2) = "Confeccionar Necesidades Quir�fano"
  aProcess(29, 3) = True
  aProcess(29, 4) = cwTypeWindow
  
  aProcess(30, 1) = PRWinValNecQuirof
  aProcess(30, 2) = "Validar Necesidades Quir�fano"
  aProcess(30, 3) = True
  aProcess(30, 4) = cwTypeWindow

  
  'DISPENSAR STOCK DE PLANTA
  aProcess(31, 1) = PRWinDispUrgencias
  aProcess(31, 2) = "Dispensar Urgencias"
  aProcess(31, 3) = True
  aProcess(31, 4) = cwTypeWindow
  
  aProcess(32, 1) = PRWinDispSinPRN
  aProcess(32, 2) = "Dispensar Sin PRN"
  aProcess(32, 3) = False
  aProcess(32, 4) = cwTypeWindow
  
  'DISPENSAR DOSIS UNITARIA
  aProcess(33, 1) = PRWinPrepCargaCarro
  aProcess(33, 2) = "Preparar Carga Carro"
  aProcess(33, 3) = True
  aProcess(33, 4) = cwTypeWindow
  
  aProcess(34, 1) = PRWinCrtlIncidencias
  aProcess(34, 2) = "Controlar Incidencias"
  aProcess(34, 3) = False
  aProcess(34, 4) = cwTypeWindow
  
  aProcess(35, 1) = PRWinCrtlIncidenciasDif
  aProcess(35, 2) = "Controlar Incidencias Diferencias"
  aProcess(35, 3) = False
  aProcess(35, 4) = cwTypeWindow
  
  aProcess(36, 1) = PRWinAnalizDiferenc
  aProcess(36, 2) = "Analizar Diferencias"
  aProcess(36, 3) = True
  aProcess(36, 4) = cwTypeWindow
  
  aProcess(37, 1) = PRWinRecogerCarro
  aProcess(37, 2) = "Recoger Carro"
  aProcess(37, 3) = True
  aProcess(37, 4) = cwTypeWindow
  
  'CONTROLAR ANTIINFECCIOSOS
  aProcess(38, 1) = PRWinDefPedBajCtrl
  aProcess(38, 2) = "Definir Medicamento Bajo Control"
  aProcess(38, 3) = True
  aProcess(38, 4) = cwTypeWindow
  
  'DISPENSAR SERVICIOS ESPECIALES
  aProcess(39, 1) = PRWinDispSerEsp
  aProcess(39, 2) = "Dispensar Servicios Especiales"
  aProcess(39, 3) = True
  aProcess(39, 4) = cwTypeWindow
  
  aProcess(40, 1) = PRWinPrepReposicion
  aProcess(40, 2) = "Preparar Reposicion"
  aProcess(40, 3) = False
  aProcess(40, 4) = cwTypeWindow
  
  'PEDIR SERVICIO FARMACIA
  aProcess(41, 1) = PRWinRedPedStPlanta
  aProcess(41, 2) = "Redactar Pedido Farmacia"
  aProcess(41, 3) = True
  aProcess(41, 4) = cwTypeWindow
  
  aProcess(42, 1) = PRWinFirmarPedStPta
  aProcess(42, 2) = "Firmar Pedido Farmacia"
  aProcess(42, 3) = True
  aProcess(42, 4) = cwTypeWindow
  
  aProcess(43, 1) = PRWinMantLotes
  aProcess(43, 2) = "Entrada de Lotes"
  aProcess(43, 3) = True
  aProcess(43, 4) = cwTypeWindow
        
  aProcess(44, 1) = PRWinVerPedPtaNoEnv
  aProcess(44, 2) = "Ver Pedidos de Planta sin Enviar"
  aProcess(44, 3) = True
  aProcess(44, 4) = cwTypeWindow
        
  aProcess(45, 1) = PRWinEstPedPtaNoEnv
  aProcess(45, 2) = "Estudiar Pedido de Planta sin Enviar"
  aProcess(45, 3) = False
  aProcess(45, 4) = cwTypeWindow
        
  aProcess(46, 1) = PRWinValPedPtaFarma
  aProcess(46, 2) = "Validar Pedido Farmacia"
  aProcess(46, 3) = True
  aProcess(46, 4) = cwTypeWindow
        
  aProcess(47, 1) = PRWinVerPedPtaNoVal
  aProcess(47, 2) = "Ver Pedido Planta No Validado"
  aProcess(47, 3) = True
  aProcess(47, 4) = cwTypeWindow
  
  aProcess(48, 1) = PRWinSelPaciente
  aProcess(48, 2) = "Seleccionar Paciente"
  aProcess(48, 3) = True
  aProcess(48, 4) = cwTypeWindow
  
  aProcess(49, 1) = PRWinBuscarGrpTerap
  aProcess(49, 2) = "Buscar Grupos Terap�uticos"
  aProcess(49, 3) = False
  aProcess(49, 4) = cwTypeWindow
  
  aProcess(50, 1) = PRWinDefProtQuirofano
  aProcess(50, 2) = "Protocolos del Quir�fano"
  aProcess(50, 3) = True
  aProcess(50, 4) = cwTypeWindow
  
  aProcess(51, 1) = PRWinAlergiasPaciente
  aProcess(51, 2) = "Alergias del Paciente"
  aProcess(51, 3) = False
  aProcess(51, 4) = cwTypeWindow

  aProcess(52, 1) = PRWinSitCarro
  aProcess(52, 2) = "Situaci�n de Carros"
  aProcess(52, 3) = True
  aProcess(52, 4) = cwTypeWindow

End Sub
