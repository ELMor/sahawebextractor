VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frminfProcesoAsistenciaPendienteVolante 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de procesos-asistencia pendientes de volante"
   ClientHeight    =   3390
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6915
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3390
   ScaleWidth      =   6915
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   4200
      TabIndex        =   8
      Top             =   2400
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Listado"
      Default         =   -1  'True
      Height          =   375
      Left            =   720
      TabIndex        =   7
      Top             =   2400
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Fecha de Inicio del proceso-asistencia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   6495
      Begin SSCalendarWidgets_A.SSDateCombo dtcFechaInicioInferior 
         Height          =   255
         Left            =   1200
         TabIndex        =   2
         Top             =   600
         Width           =   1695
         _Version        =   65543
         _ExtentX        =   2990
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcFechaInicioSuperior 
         Height          =   255
         Left            =   3480
         TabIndex        =   4
         Top             =   600
         Width           =   1695
         _Version        =   65543
         _ExtentX        =   2990
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "y"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3120
         TabIndex        =   3
         Top             =   600
         Width           =   105
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Entre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   600
         TabIndex        =   1
         Top             =   600
         Width           =   465
      End
   End
   Begin SSDataWidgets_B.SSDBCombo cboTipoAsist 
      Height          =   315
      Left            =   1680
      TabIndex        =   6
      Top             =   1800
      Width           =   1635
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196615
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   2884
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Tipo Asistencia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   1800
      Width           =   1320
   End
End
Attribute VB_Name = "frminfProcesoAsistenciaPendienteVolante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
  Dim strsql As String
  Dim Formulas(3, 2) As Variant
  
    
  If IsNull(dtcFechaInicioInferior.Text) Or IsNull(dtcFechaInicioSuperior.Text) Then
    MsgBox "Debe rellenar todos los campos imprescindibles (con fondo azul) de este formulario.", vbOKOnly + vbInformation, "Faltan datos"
  Else
    
    strsql = "AD11FECINICIO >= TO_DATE(" & "'" & Format(dtcFechaInicioInferior.Text, "DD/MM/YYYY") & "','DD/MM/YYYY')"
    strsql = strsql & " AND AD11FECINICIO <= TO_DATE(" & "'" & Format(dtcFechaInicioSuperior.Text, "DD/MM/YYYY") & "','DD/MM/YYYY')"
    If Not IsNull(cboTipoAsist) Then
      strsql = strsql & " AND AD12CODTIPOASIST =" & cboTipoAsist.Columns(0).Text
    End If
    
    infVolantestxtFechaInicio = Format(dtcFechaInicioInferior.Text, "DD/MM/YYYY")
    infVolantestxtFechaFin = Format(dtcFechaInicioSuperior.Text, "DD/MM/YYYY")
    If Not IsNull(cboTipoAsist.Text) Then
        infVolantestxtTipoAsistencia = cboTipoAsist.Columns(1).Text
        
    Else
        infVolantestxtTipoAsistencia = "Todas"
    End If
    Formulas(1, 1) = "FInicio"
    Formulas(1, 2) = "'" & infVolantestxtFechaInicio & "'"
    Formulas(2, 1) = "FFin"
    Formulas(2, 2) = "'" & infVolantestxtFechaFin & "'"
    Formulas(3, 1) = "TAsist"
    Formulas(3, 2) = "'" & infVolantestxtTipoAsistencia & "'"
    Call Imprimir_API(strsql, "infProcesoAsistenciaPendienteVolante.rpt", Formulas)
   
    
    
    
    
  End If
End Sub


Private Sub cmdCancelar_Click()
Unload Me
Set frminfProcesoAsistenciaPendienteVolante = Nothing


End Sub

Private Sub Form_Load()

dtcFechaInicioSuperior.Text = DateAdd("d", 1, dtcFechaInicioInferior.Text)


Call pCargarTiposAsist

End Sub
Private Sub pCargarTiposAsist()
    Dim sql As String
    
    Dim resPuesta As rdoResultset
    
    sql = "SELECT AD12CODTIPOASIST, AD12DESTIPOASIST"
    sql = sql & " FROM AD1200"
    sql = sql & " WHERE AD12FECFIN IS NULL"
    sql = sql & " ORDER BY AD12DESTIPOASIST"
    Set resPuesta = objApp.rdoConnect.OpenResultset(sql)
    
    cboTipoAsist.Text = resPuesta!AD12DESTIPOASIST
    Do While Not resPuesta.EOF
        cboTipoAsist.AddItem resPuesta!AD12CODTIPOASIST & Chr$(9) & resPuesta!AD12DESTIPOASIST
        resPuesta.MoveNext
    Loop
    resPuesta.Close
    
End Sub


