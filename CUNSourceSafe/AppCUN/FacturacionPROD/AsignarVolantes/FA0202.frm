VERSION 5.00
Begin VB.Form frmCargaDisquetes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Carga de Disquetes de Osasunbidea"
   ClientHeight    =   1365
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4635
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1365
   ScaleWidth      =   4635
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCargar 
      Height          =   735
      Left            =   2520
      Picture         =   "FA0202.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   360
      Width           =   735
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Carga Disquetes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   1410
   End
End
Attribute VB_Name = "frmCargaDisquetes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Sub cargaDisquete()
  
  Dim lngFicheroEntrada As Long
  Dim lngFicheroLog As Long
  Dim strLineaDisquete As String
  Dim rst As rdoResultset
  
  Dim rst1 As rdoResultset
  
  
  Dim strSqlComun As String
  Dim strsql As String
  Dim strSqlQdf As String
  Dim blnHayIncidencias As Boolean

 
  Dim strExpInf As String
  Dim strIdCliente As String
  Dim strNumSegSoc As String
  Dim strCodCtl As String
  Dim strDNI As String
  Dim strApel1 As String
  Dim strApel2 As String
  Dim strNomb As String
  Dim strFechNac As String
  Dim strSexo As String
  Dim strCP As String
  Dim strServ As String
  Dim strAsist As String
  Dim strNumVol As String
  Dim strOperac As String
  
  Dim strMsg As String
  Dim strTitulo As String
  Dim sql As String
  
  On Error GoTo ErrorEnLaCarga
  Screen.MousePointer = vbHourglass
  blnHayIncidencias = False
  ' Se abre el archivo cun.txt (volantes) para lectura.
  lngFicheroEntrada = FreeFile
  Open "c:\Osasunbi\CargaVolantes\CUN.TXT" For Input Lock Write As lngFicheroEntrada
  
  
  
  Do While Not EOF(lngFicheroEntrada)
    Input #lngFicheroEntrada, strLineaDisquete
    
    strExpInf = Trim(Mid(strLineaDisquete, 1, 14))
    strIdCliente = Trim(Mid(strLineaDisquete, 15, 23))
    strNumSegSoc = Trim(Mid(strLineaDisquete, 38, 10))
    strCodCtl = Trim(Mid(strLineaDisquete, 48, 2))
    strDNI = Trim(Mid(strLineaDisquete, 50, 8))
    strApel1 = Trim(Mid(strLineaDisquete, 58, 18))
    strApel2 = Trim(Mid(strLineaDisquete, 76, 18))
    strNomb = Trim(Mid(strLineaDisquete, 94, 14))
    strFechNac = Trim(Mid(strLineaDisquete, 108, 10))
    strSexo = Trim(Mid(strLineaDisquete, 118, 1))
    strCP = Trim(Mid(strLineaDisquete, 119, 5))
    strServ = Trim(Mid(strLineaDisquete, 124, 3))
    strAsist = Trim(Mid(strLineaDisquete, 127, 7))
    strNumVol = Trim(Mid(strLineaDisquete, 134, 2))
    strOperac = Trim(Mid(strLineaDisquete, 136, 1))
    
    'Comprobaciones previas a la inserci�n del volante
    '1.- Seg�n el valor de strOperac
    If (strOperac = "B") Then
    'Si es un borrado, se comprueba que existe el registro, que no ha sido asignado, que no ha sido facturado y que es v�lido.
    'Si existe se borra. Si no se avisa en el log
        strSqlComun = " WHERE FA25EXPINF = """ & strExpInf
        strSqlComun = strSqlComun & """ AND FA25IDCLIENTE = """ & strIdCliente
        strSqlComun = strSqlComun & """ AND FA25NUMSEGSOC = """ & strNumSegSoc
        strSqlComun = strSqlComun & """ AND FA25CODCTL = """ & strCodCtl
        strSqlComun = strSqlComun & """ AND FA25DNI = """ & strDNI
        strSqlComun = strSqlComun & """ AND FA25APEL1 = """ & strApel1
        strSqlComun = strSqlComun & """ AND FA25APEL2 = """ & strApel2
        strSqlComun = strSqlComun & """ AND FA25NOMB  = """ & strNomb
        strSqlComun = strSqlComun & """ AND FA25FECHNAC = " & FechaInglesa(strFechNac)
        strSqlComun = strSqlComun & " AND FA25SEXO = " & strSexo
        strSqlComun = strSqlComun & " AND FA25CODPOSTAL = """ & strCP
        strSqlComun = strSqlComun & """ AND AD02CODDPTO = " & strServ
        strSqlComun = strSqlComun & " AND FA33CODTRATAM = """ & strAsist
        strSqlComun = strSqlComun & """ AND FA25NUMVOL = """ & strNumVol
        strSqlComun = strSqlComun & """ AND AD01CODASISTENCI IS NULL AND AD07CODPROCESO IS NULL"
        strSqlComun = strSqlComun & " AND FA25FACTUR = 0 AND FA25VALID = -1"
        
        strsql = "SELECT FA25CODREG FROM FA2500" & strSqlComun
        Set rst = objApp.rdoConnect.OpenResultset(strsql)
        If Not rst.EOF Then
            sql = "UPDATE FA2500 SET FA25VALID = 0, FA25FECUPD = """ & FechaHora() & """" & strSqlComun
            Set rst1 = objApp.rdoConnect.OpenResultset(sql)
            rst1.Close
        Else
            If Not (blnHayIncidencias) Then
                blnHayIncidencias = True
                lngFicheroLog = FreeFile
                Open "c:\Osasunbi\CargaVolantes\CARGA.LOG" For Output Lock Write As lngFicheroLog
            End If
            Print #lngFicheroLog, "No se ha podido borrar el volante:"
            Print #lngFicheroLog, strLineaDisquete
        End If
        rst.Close
        
        
    ElseIf (strOperac = "M") Then
    'Si es una modificaci�n, se comprueba que existe el registro, que no ha sido facturado y que es v�lido.
    'Si existe se modifica. Si no se avisa en el log
        strSqlComun = " WHERE FA25EXPINF = """ & strExpInf
        strSqlComun = strSqlComun & """ AND FA25FACTUR = 0 AND FA25VALID = -1"
        
        strsql = "SELECT FA25CODREG FROM FA2500" & strSqlComun
        
        Set rst = objApp.rdoConnect.OpenResultset(strsql)
        If Not rst.EOF Then
            strSqlQdf = "UPDATE FA2500 SET FA25IDCLIENTE = """ & strIdCliente
            strSqlQdf = strSqlQdf & """, FA25NUMSEGSOC = """ & strNumSegSoc
            strSqlQdf = strSqlQdf & """, FA25CODCTL = """ & strCodCtl
            strSqlQdf = strSqlQdf & """, FA25DNI = """ & strDNI
            strSqlQdf = strSqlQdf & """, FA25APEL1 = """ & strApel1
            strSqlQdf = strSqlQdf & """, FA25APEL2 = """ & strApel2
            strSqlQdf = strSqlQdf & """, FA25NOMB  = """ & strNomb
            strSqlQdf = strSqlQdf & """, FA25FECHNAC = " & ModGeneral.FechaInglesa(strFechNac)
            strSqlQdf = strSqlQdf & ", FA25SEXO = " & strSexo
            strSqlQdf = strSqlQdf & ", FA25CODPOSTAL = """ & strCP
            strSqlQdf = strSqlQdf & """, AD02CODDPTO = " & strServ
            strSqlQdf = strSqlQdf & ", FA33CODTRATAM = """ & strAsist
            strSqlQdf = strSqlQdf & """, FA25NUMVOL = """ & strNumVol
            strSqlQdf = strSqlQdf & """, FA25FECUPD = """ & FechaHora() & """"
            strSqlQdf = strSqlQdf & ", FA25FECRECEP = """ & FileDateTime("c:\Osasunbi\CargaVolantes\CUN.TXT") & """"
            
            sql = strSqlQdf & strSqlComun
            Set rst1 = objApp.rdoConnect.OpenResultset(sql)
        ElseIf rst.EOF Then
        'Caso de que no exista el registro facturado se inserta el volante
            rst.Close
            strsql = "SELECT FA25CODREG FROM FA2500 WHERE FA25EXPINF = """ & strExpInf
            strsql = strsql & """ AND FA25VALID = -1"
            Set rst = objApp.rdoConnect.OpenResultset(strsql)
            If rst.EOF Then
            'Se inserta el volante
                strSqlQdf = "INSERT INTO FA2500 ( FA25EXPINF, FA25IDCLIENTE, FA25NUMSEGSOC, FA25CODCTL,"
                strSqlQdf = strSqlQdf & "FA25DNI, FA25APEL1, FA25APEL2, FA25NOMB, FA25FECHNAC, FA25SEXO,"
                strSqlQdf = strSqlQdf & "FA25CODPOSTAL, AD02CODDPTO, FA33CODTRATAM, FA25NUMVOL, FA25FECRECEP, FA25FACTUR, FA25VALID)"
                strSqlQdf = strSqlQdf & " VALUES (""" & strExpInf & """, """ & strIdCliente & """, """
                strSqlQdf = strSqlQdf & strNumSegSoc & """, """ & strCodCtl & """, """ & strDNI & """, """
                strSqlQdf = strSqlQdf & strApel1 & """, """ & strApel2 & """, """ & strNomb & """, """
                strSqlQdf = strSqlQdf & strFechNac & """, " & strSexo & ", """ & strCP & """, "
                strSqlQdf = strSqlQdf & strServ & ", """ & strAsist & """, """ & strNumVol & """, """
                strSqlQdf = strSqlQdf & FileDateTime("c:\Osasunbi\CargaVolantes\CUN.TXT") & """, 0, -1)"
                
                sql = strSqlQdf
                Set rst1 = objApp.rdoConnect.OpenResultset(sql)
                rst1.Close
                
            Else
            'Si existe el volante facturado se avisa en el log
                If Not (blnHayIncidencias) Then
                    blnHayIncidencias = True
                    lngFicheroLog = FreeFile
                    Open "c:\Osasunbi\CargaVolantes\CARGA.LOG" For Output Lock Write As lngFicheroLog
                End If
                Print #lngFicheroLog, "No se ha podido actualizar el volante porque ya est� facturado:"
                Print #lngFicheroLog, strLineaDisquete
            End If
        Else
            If Not (blnHayIncidencias) Then
                blnHayIncidencias = True
                lngFicheroLog = FreeFile
                Open "c:\Osasunbi\CargaVolantes\CARGA.LOG" For Output Lock Write As lngFicheroLog
            End If
            Print #lngFicheroLog, "No se ha podido actualizar el volante porque hay m�s de uno v�lido:"
            Print #lngFicheroLog, strLineaDisquete
        End If
        rst.Close
    ElseIf (strOperac = "") Then
    'Si es una inserci�n se comprueba que no exista el registro en estado v�lido. Si no existe se inserta. Si existe se avisa en el log.
        strSqlComun = " WHERE FA25EXPINF = """ & strExpInf
        strSqlComun = strSqlComun & """ AND FA25IDCLIENTE = """ & strIdCliente
        strSqlComun = strSqlComun & """ AND FA25NUMSEGSOC = """ & strNumSegSoc
        strSqlComun = strSqlComun & """ AND FA25CODCTL = """ & strCodCtl
        strSqlComun = strSqlComun & """ AND FA25DNI = """ & strDNI
        strSqlComun = strSqlComun & """ AND FA25APEL1 = """ & strApel1
        strSqlComun = strSqlComun & """ AND FA25APEL2 = """ & strApel2
        strSqlComun = strSqlComun & """ AND FA25NOMB  = """ & strNomb
        strSqlComun = strSqlComun & """ AND FA25FECHNAC = " & ModGeneral.FechaInglesa(strFechNac)
        strSqlComun = strSqlComun & " AND FA25SEXO = " & strSexo
        strSqlComun = strSqlComun & " AND FA25CODPOSTAL = """ & strCP
        strSqlComun = strSqlComun & """ AND AD02CODDPTO = " & strServ
        strSqlComun = strSqlComun & " AND FA33CODTRATAM = """ & strAsist
        strSqlComun = strSqlComun & """ AND FA25NUMVOL = """ & strNumVol
        strSqlComun = strSqlComun & """ AND FA25VALID = -1"
        
        strsql = "SELECT FA25CODREG FROM FA2500" & strSqlComun
        
        Set rst = objApp.rdoConnect.OpenResultset(strsql)
        If rst.EOF Then
            strSqlQdf = "INSERT INTO FA2500 ( FA25EXPINF, FA25IDCLIENTE, FA25NUMSEGSOC, FA25CODCTL,"
            strSqlQdf = strSqlQdf & "FA25DNI, FA25APEL1, FA25APEL2, FA25NOMB, FA25FECHNAC, FA25SEXO,"
            strSqlQdf = strSqlQdf & "FA25CODPOSTAL, AD02CODDPTO, FA33CODTRATAM, FA25NUMVOL, FA25FECRECEP, FA25FACTUR, FA25VALID)"
            strSqlQdf = strSqlQdf & " VALUES (""" & strExpInf & """, """ & strIdCliente & """, """
            strSqlQdf = strSqlQdf & strNumSegSoc & """, """ & strCodCtl & """, """ & strDNI & """, """
            strSqlQdf = strSqlQdf & strApel1 & """, """ & strApel2 & """, """ & strNomb & """, """
            strSqlQdf = strSqlQdf & strFechNac & """, " & strSexo & ", """ & strCP & """, "
            strSqlQdf = strSqlQdf & strServ & ", """ & strAsist & """, """ & strNumVol & """, """
            strSqlQdf = strSqlQdf & FileDateTime("c:\Osasunbi\CargaVolantes\CUN.TXT") & """, 0, -1)"
            Set rst1 = objApp.rdoConnect.OpenResultset(strSqlQdf)
             
            
        Else
            If Not (blnHayIncidencias) Then
                blnHayIncidencias = True
                lngFicheroLog = FreeFile
                Open "c:\Osasunbi\CargaVolantes\CARGA.LOG" For Output Lock Write As lngFicheroLog
            End If
            Print #lngFicheroLog, "No se ha podido insertar el volante:"
            Print #lngFicheroLog, strLineaDisquete
        End If
        rst.Close
    Else
    'Se avisa en el log que el fichero es defectuoso
        If Not (blnHayIncidencias) Then
            blnHayIncidencias = True
            lngFicheroLog = FreeFile
            Open "c:\Osasunbi\CargaVolantes\CARGA.LOG" For Output Lock Write As lngFicheroLog
        End If
        Print #lngFicheroLog, "No se ha podido interpretar el volante:"
        Print #lngFicheroLog, strLineaDisquete
    End If
  Loop

  Close lngFicheroEntrada
  If blnHayIncidencias Then
    Close lngFicheroLog
    strMsg = "Se han producido incidencias en la carga del disquete." & Chr$(13)
    strMsg = strMsg & "Revise el archivo de log o p�ngase en contacto con su administrador."
    strTitulo = "Incidencias al realizar la carga del disquete"
    MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
  Else
    strMsg = "La carga del disquete se ha realizado sin incidencias." & Chr$(13)
    strTitulo = "Fin de carga del disquete"
    MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
  End If
  Screen.MousePointer = vbDefault
  
  Exit Sub

ErrorEnLaCarga:

  strMsg = "Imposible realizar la carga del disquete." & Chr$(13)
  strMsg = strMsg & "P�ngase en contacto con su administrador."
  strTitulo = "Error al realizar la carga del disquete"
  MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
  Close lngFicheroEntrada
  If blnHayIncidencias Then
    Close lngFicheroLog
    strMsg = "Se han producido incidencias en la carga del disquete." & Chr$(13)
    strMsg = strMsg & "Revise el archivo de log o p�ngase en contacto con su administrador."
    strTitulo = "Incidencias al realizar la carga del disquete"
    MsgBox strMsg, vbOKOnly + vbInformation, strTitulo
  End If
  Screen.MousePointer = vbDefault
  
End Sub

Private Sub cmdCargar_Click()
  Dim lngRespuesta As Long
  Dim strMsg As String
  Dim strTitulo As String
  
    strMsg = "Para cargar volantes debe copiar el archivo de los volantes en C:\Osasunbi\CargaVolantes con el nombre CUN.TXT �Desea cargar los volantes ahora?"
    strTitulo = "Carga del disquete"
    lngRespuesta = MsgBox(strMsg, vbYesNo + vbQuestion, strTitulo)
    If (vbYes = lngRespuesta) Then
        Call cargaDisquete
    End If
End Sub

