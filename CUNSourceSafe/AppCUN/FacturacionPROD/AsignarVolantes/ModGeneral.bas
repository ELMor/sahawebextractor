Attribute VB_Name = "ModGeneral"
Option Explicit
Public fechaini As String
Public fechafin As String
Public Aceptar As Boolean



Public Function FechaHora() As Date
  Dim blnExito As Boolean
  Dim strsql As String
  Dim rstFechaHora As rdoResultset
  

  strsql = "SELECT SYSDATE FROM DUAL"
  If TypeName(rstFechaHora) = "Nothing" Then
    Set rstFechaHora = objApp.rdoConnect.OpenResultset(strsql, rdOpenForwardOnly)
  Else
    rstFechaHora.Requery
  End If
  FechaHora = rstFechaHora(0).Value

  rstFechaHora.Close

End Function
Public Function LPAD(str1 As String, longitud As Integer, str2 As String) As String
    Dim strLocal As String
    strLocal = str1
    Do Until (Len(strLocal) >= longitud)
        strLocal = strLocal & str2
    Loop
    LPAD = Left(strLocal, longitud)
End Function
Public Function RPAD(str1 As String, longitud As Integer, str2 As String) As String
    Dim strLocal As String
    strLocal = str1
    Do Until (Len(strLocal) >= longitud)
        strLocal = str2 & strLocal
    Loop
    RPAD = Left(strLocal, longitud)
End Function

Public Function TextoAlfa(strTexto As Variant) As String
  Dim rst As rdoResultset
  
  Dim strsql As String
  
  strsql = "SELECT GCFN05('" & strTexto & "') RESP FROM DUAL"
  Set rst = objApp.rdoConnect.OpenResultset(strsql, rdOpenForwardOnly)
   If rst!RESP <> "" Then
    TextoAlfa = rst!RESP
   Else
    TextoAlfa = ""
   End If
   
End Function


Public Function FechaInglesa(strFecha As Variant) As String
  'On Error GoTo Error_Fecha:
  
  
  If IsDate(strFecha) Then
    FechaInglesa = "#" & Format(strFecha, "MM-DD-YYYY") & "#"
  Else
    MsgBox "El parámetro no es una fecha.", vbOKOnly + vbInformation, "Error"
    FechaInglesa = "#01-01-1990#"
  End If
  
'Salir:
  'Exit Function
  
'Error_Fecha:
  'MsgBox "Error en la funcion de FechaInglesa.", vbOKOnly + vbInformation, "Error"
  'FechaInglesa = "#01-01-1990#"
  'GoTo Salir:

End Function
