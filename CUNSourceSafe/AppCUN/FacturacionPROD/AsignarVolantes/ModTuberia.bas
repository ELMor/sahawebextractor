Attribute VB_Name = "ModTuberia"
Option Explicit

Public infVolantestxtFechaInicio As String
Public infVolantestxtFechaFin As String
Public infVolantestxtTipoAsistencia As String
Public infVolantestxtNumeroFactura As String

Public Function infVolantesFechaInicio() As String
    infVolantesFechaInicio = infVolantestxtFechaInicio
End Function

Public Function infVolantesFechaFin() As String
    infVolantesFechaFin = infVolantestxtFechaFin
End Function

Public Function infVolantesTipoAsistencia() As String
    infVolantesTipoAsistencia = infVolantestxtTipoAsistencia
End Function

Public Function infVolantesNumeroFactura() As String
    infVolantesNumeroFactura = infVolantestxtNumeroFactura
End Function

