VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSeleccionProcesoAsistencia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n de proceso-asistencia"
   ClientHeight    =   7800
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   ForeColor       =   &H00000000&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7800
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDes 
      Caption         =   "&Deseleccionar Todo"
      Height          =   375
      Left            =   2520
      TabIndex        =   29
      Top             =   7320
      Width           =   2295
   End
   Begin VB.CommandButton cmdSel 
      Caption         =   "&Seleccionar Todo"
      Height          =   375
      Left            =   360
      TabIndex        =   28
      Top             =   7320
      Width           =   1815
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   6960
      TabIndex        =   17
      Top             =   7320
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   5280
      TabIndex        =   16
      Top             =   7320
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Campos de b�squeda"
      ForeColor       =   &H00FF0000&
      Height          =   3135
      Left            =   120
      TabIndex        =   10
      Top             =   240
      Width           =   9255
      Begin VB.CommandButton cmdLimpiar 
         Caption         =   "&Limpiar"
         Height          =   375
         Left            =   8040
         TabIndex        =   30
         Top             =   840
         Width           =   855
      End
      Begin VB.Frame Frame4 
         Caption         =   "Por n� de Factura"
         ForeColor       =   &H00FF0000&
         Height          =   1455
         Left            =   4680
         TabIndex        =   26
         Top             =   1560
         Width           =   3255
         Begin VB.TextBox txtNumeroFactura 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   1080
            TabIndex        =   9
            Top             =   480
            Width           =   1455
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "Factura"
            Height          =   195
            Left            =   360
            TabIndex        =   27
            Top             =   480
            Width           =   540
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Por Paciente"
         ForeColor       =   &H00FF0000&
         Height          =   2775
         Left            =   240
         TabIndex        =   18
         Top             =   240
         Width           =   4335
         Begin VB.TextBox txtBuscaNombre 
            Height          =   285
            Left            =   1920
            TabIndex        =   0
            Top             =   240
            Width           =   2175
         End
         Begin VB.TextBox txtBuscaApellido1 
            Height          =   285
            Left            =   1920
            TabIndex        =   1
            Top             =   600
            Width           =   2175
         End
         Begin VB.TextBox txtBuscaApellido2 
            Height          =   285
            Left            =   1920
            TabIndex        =   2
            Top             =   960
            Width           =   2175
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcBuscaFechaNacimiento 
            Height          =   255
            Left            =   1920
            TabIndex        =   3
            Top             =   1320
            Width           =   1815
            _Version        =   65543
            _ExtentX        =   3201
            _ExtentY        =   450
            _StockProps     =   93
            ForeColor       =   -2147483630
            BackColor       =   -2147483634
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboBuscaSexo 
            Height          =   255
            Left            =   1920
            TabIndex        =   6
            Top             =   2400
            Width           =   1035
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            _Version        =   196615
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Sexo"
            Columns(1).Name =   "Sexo"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   450
            _StockProps     =   93
            ForeColor       =   -2147483630
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboBuscaTipoAsistencia 
            Height          =   255
            Left            =   1920
            TabIndex        =   4
            Top             =   1680
            Width           =   2235
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            _Version        =   196615
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Tipo"
            Columns(1).Name =   "Tipo"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3942
            _ExtentY        =   450
            _StockProps     =   93
            ForeColor       =   -2147483630
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboBuscaDpto 
            Height          =   255
            Left            =   1920
            TabIndex        =   5
            Top             =   2040
            Width           =   2235
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            _Version        =   196615
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3942
            _ExtentY        =   450
            _StockProps     =   93
            ForeColor       =   -2147483630
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre"
            Height          =   195
            Left            =   360
            TabIndex        =   25
            Top             =   240
            Width           =   555
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Primer apellido"
            Height          =   195
            Left            =   360
            TabIndex        =   24
            Top             =   600
            Width           =   1020
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Segundo Apellido"
            Height          =   195
            Left            =   360
            TabIndex        =   23
            Top             =   960
            Width           =   1245
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "F. de nacimiento"
            Height          =   195
            Left            =   360
            TabIndex        =   22
            Top             =   1320
            Width           =   1170
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Sexo"
            Height          =   195
            Left            =   360
            TabIndex        =   21
            Top             =   2400
            Width           =   360
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Tipo asistencia"
            Height          =   195
            Left            =   360
            TabIndex        =   20
            Top             =   1680
            Width           =   1065
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Departamento"
            Height          =   195
            Left            =   360
            TabIndex        =   19
            Top             =   2040
            Width           =   1005
         End
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   8040
         TabIndex        =   14
         Top             =   360
         Width           =   855
      End
      Begin VB.Frame Frame2 
         Caption         =   "Por fecha de inicio de la asistencia"
         ForeColor       =   &H00FF0000&
         Height          =   1095
         Left            =   4680
         TabIndex        =   11
         Top             =   240
         Width           =   3255
         Begin SSCalendarWidgets_A.SSDateCombo dtcFechaInicioInferior 
            Height          =   255
            Left            =   840
            TabIndex        =   7
            Top             =   240
            Width           =   1575
            _Version        =   65543
            _ExtentX        =   2778
            _ExtentY        =   450
            _StockProps     =   93
            ForeColor       =   -2147483630
            BackColor       =   -2147483634
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcFechaInicioSuperior 
            Height          =   255
            Left            =   840
            TabIndex        =   8
            Top             =   600
            Width           =   1575
            _Version        =   65543
            _ExtentX        =   2778
            _ExtentY        =   450
            _StockProps     =   93
            ForeColor       =   -2147483630
            BackColor       =   -2147483634
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Y"
            Height          =   195
            Left            =   360
            TabIndex        =   13
            Top             =   600
            Width           =   105
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Entre"
            Height          =   195
            Left            =   360
            TabIndex        =   12
            Top             =   240
            Width           =   375
         End
      End
   End
   Begin SSDataWidgets_B.SSDBGrid grdResultadoBus 
      Height          =   3615
      Left            =   120
      TabIndex        =   15
      Top             =   3600
      Width           =   11640
      _Version        =   196615
      DataMode        =   2
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      SelectTypeCol   =   0
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      MaxSelectedRows =   400
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20532
      _ExtentY        =   6376
      _StockProps     =   79
      Caption         =   "Resultado de la B�squeda"
      ForeColor       =   16711680
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSeleccionProcesoAsistencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim select1 As String

Private Sub cboBuscaDpto_InitColumnProps()
dtcFechaInicioInferior.Text = ""
dtcFechaInicioSuperior.Text = ""
txtNumeroFactura.Text = ""

End Sub

Private Sub cboBuscaSexo_InitColumnProps()
dtcFechaInicioInferior.Text = ""
dtcFechaInicioSuperior.Text = ""
txtNumeroFactura.Text = ""

End Sub

Private Sub cboBuscaTipoAsistencia_InitColumnProps()
dtcFechaInicioInferior.Text = ""
dtcFechaInicioSuperior.Text = ""
txtNumeroFactura.Text = ""

End Sub

Private Sub cmdAceptar_Click()
  Dim strsql1 As String
 Dim nombre As String
 
  If grdResultadoBus.SelBookmarks.Count > 1 Then
    Call objPipe.PipeSet("Select", select1)
  ElseIf grdResultadoBus.Columns("Asistencia").Text <> "" And grdResultadoBus.SelBookmarks.Count = 1 Then
        strsql1 = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,"
        strsql1 = strsql1 & "CI22DNI,CI22FECNACIM,CI22NUMSEGSOC,AD02DESDPTO,"
        strsql1 = strsql1 & "AD01CODASISTENCI,AD07CODPROCESO,AD11FECINICIO,AD12DESTIPOASIST"
        strsql1 = strsql1 & " FROM AD1121J"
        strsql1 = strsql1 & " WHERE AD01CODASISTENCI = " & grdResultadoBus.Columns("Asistencia").Text
        strsql1 = strsql1 & " AND AD07CODPROCESO = " & grdResultadoBus.Columns("Proceso").Text
       Call objPipe.PipeSet("Select", strsql1)
 End If

    
 Unload Me
 Set frmSeleccionProcesoAsistencia = Nothing
 
 Load frmProcesoAsistencia
 frmProcesoAsistencia.Show 1
End Sub

Private Sub cmdBuscar_Click()
  Dim strCondicion As String
  Dim strsql As String
  Dim rst1 As rdoResultset
  Dim formu As String
  
  Screen.MousePointer = vbHourglass
  
  'Si no se ha rellenado ningun campo de b�squeda
  If txtBuscaApellido1.Text = "" And _
    txtBuscaApellido2.Text = "" And _
    dtcBuscaFechaNacimiento.Text = "" And _
    dtcFechaInicioInferior.Text = "" And _
    dtcFechaInicioSuperior.Text = "" And txtNumeroFactura.Text = "" Then
    MsgBox "Debe rellenar al menos un campo de b�squeda.", vbOKOnly + vbInformation, "B�squeda nula"
    Screen.MousePointer = vbDefault
    
    Exit Sub
    
    
  Else
    strCondicion = "AD01CODASISTENCI<>0 "
    'Si se ha rellenado el campo Apellido1 se buscara apellidos1 que contengan
    'una transformacion (mediante la funcion TextoAlfa) del texto introducido en este campo.
    If txtBuscaApellido1.Text <> "" Then
      strCondicion = strCondicion & " AND CI22PRIAPEL LIKE '"
      strCondicion = strCondicion & TextoAlfa(txtBuscaApellido1.Text) & "%'"
      Call objPipe.PipeSet("Busqueda", 1)
    End If
    If txtBuscaApellido2.Text <> "" Then
      strCondicion = strCondicion & " AND CI22SEGAPEL LIKE '"
      strCondicion = strCondicion & TextoAlfa(txtBuscaApellido2.Text) & "%'"
      Call objPipe.PipeSet("Busqueda", 1)
    End If
    If txtBuscaNombre.Text <> "" Then
      strCondicion = strCondicion & " AND CI22NOMBRE LIKE '"
      strCondicion = strCondicion & TextoAlfa(txtBuscaNombre.Text) & "%'"
      Call objPipe.PipeSet("Busqueda", 1)
    End If
    If dtcBuscaFechaNacimiento.Text <> "" Then
      strCondicion = strCondicion & " AND CI22FECNACIM = TO_DATE('"
      strCondicion = strCondicion & Format(dtcBuscaFechaNacimiento.Text, "DD/MM/YYYY") & "','DD/MM/YYYY') "
      Call objPipe.PipeSet("Busqueda", 1)
    End If
    If cboBuscaSexo.Text <> "" Then
      strCondicion = strCondicion & " AND CI30CODSEXO ="
      strCondicion = strCondicion & cboBuscaSexo.Columns(0).Text
      Call objPipe.PipeSet("Busqueda", 1)
    End If
    If cboBuscaTipoAsistencia.Text <> "" Then
      strCondicion = strCondicion & " AND AD12CODTIPOASIST = '"
      strCondicion = strCondicion & cboBuscaTipoAsistencia.Columns(0).Text & "'"
      Call objPipe.PipeSet("Busqueda", 1)
    End If
    If cboBuscaDpto.Text <> "" Then
      strCondicion = strCondicion & " AND AD02CODDPTO ="
      strCondicion = strCondicion & cboBuscaDpto.Columns(0).Text
      Call objPipe.PipeSet("Busqueda", 1)
    End If
    If dtcFechaInicioInferior.Text <> "" Then
      strCondicion = strCondicion & " AND AD11FECINICIO >= TO_DATE('"
      strCondicion = strCondicion & Format(dtcFechaInicioInferior.Text, "DD/MM/YYYY") & "','DD/MM/YYYY')"
      Call objPipe.PipeSet("fecini", dtcFechaInicioInferior.Text)
      
    End If
    If dtcFechaInicioSuperior.Text <> "" Then
      strCondicion = strCondicion & " AND AD11FECINICIO <= TO_DATE('"
      strCondicion = strCondicion & Format(dtcFechaInicioSuperior, "DD/MM/YYYY") & "','DD/MM/YYYY') "
      Call objPipe.PipeSet("fecfin", dtcFechaInicioSuperior.Text)
    End If
    If txtNumeroFactura.Text <> "" Then
     strCondicion = "AD1122J.AD07CODPROCESO = FA0400.AD07CODPROCESO AND AD1122J.AD01CODASISTENCI = FA0400.AD01CODASISTENCI AND FA0400.FA04CANTFACT > 0 "
     strCondicion = strCondicion & "AND FA0400.FA04NUMFACREAL IS NULL AND FA0400.FA04NUMFACT = '" & txtNumeroFactura & "'"
     Call objPipe.PipeSet("Nfactura", txtNumeroFactura.Text)
    End If
    If objPipe.PipeExist("Busqueda") Or (objPipe.PipeExist("fecini") And objPipe.PipeExist("fecfin")) Then
    
      strsql = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL"
      strsql = strsql & ",CI22FECNACIM,AD12DESTIPOASIST,AD02DESDPTO"
      strsql = strsql & ",AD11FECINICIO,AD01CODASISTENCI,AD07CODPROCESO,CI22DNI,CI22NUMSEGSOC"
      strsql = strsql & " FROM AD1121J WHERE " & strCondicion
    ElseIf objPipe.PipeExist("Nfactura") Then
      strsql = "SELECT AD1122J.CI22NOMBRE,AD1122J.CI22PRIAPEL,AD1122J.CI22SEGAPEL,"
      strsql = strsql & "AD1122J.CI22DNI,AD1122J.CI22FECNACIM,AD1122J.CI22NUMSEGSOC,AD1122J.AD02DESDPTO,"
      strsql = strsql & "AD1122J.AD01CODASISTENCI,AD1122J.AD07CODPROCESO,AD1122J.AD11FECINICIO,AD1122J.AD12DESTIPOASIST,"
      strsql = strsql & "FA0400.FA04CANTFACT, FA0400.FA04NUMFACREAL, FA0400.FA04NUMFACT "
      strsql = strsql & "FROM AD1122J,FA0400 WHERE " & strCondicion
   End If
   End If
   
      Set rst1 = objApp.rdoConnect.OpenResultset(strsql)
      Do While Not rst1.EOF
         grdResultadoBus.AddItem rst1!CI22NOMBRE & " " & rst1!CI22PRIAPEL & " " & rst1!CI22SEGAPEL & Chr$(9) _
         & rst1!CI22FECNACIM & Chr$(9) _
         & rst1!AD12DESTIPOASIST & Chr$(9) _
         & rst1!AD02DESDPTO & Chr$(9) _
         & rst1!AD11FECINICIO & Chr$(9) _
         & rst1(7) & Chr$(9) _
         & rst1(8)
         rst1.MoveNext
      Loop
    rst1.Close
    

 
  Screen.MousePointer = vbDefault
select1 = strsql

End Sub

Private Sub cmdCancelar_Click()
Unload Me
Set frmSeleccionProcesoAsistencia = Nothing

End Sub

Private Sub cmdDes_Click()
grdResultadoBus.SelBookmarks.RemoveAll

End Sub



Private Sub cmdSel_Click()

Dim i As Long

For i = 0 To grdResultadoBus.Rows - 1
  grdResultadoBus.SelBookmarks.Add (i)
Next i



End Sub

Private Sub cmdLimpiar_Click()
txtBuscaNombre = ""
txtBuscaApellido1 = ""
txtBuscaApellido2 = ""
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
cboBuscaTipoAsistencia.Text = ""
cboBuscaDpto.Text = ""
dtcFechaInicioInferior.Text = ""
dtcFechaInicioSuperior.Text = ""
txtNumeroFactura.Text = ""
txtBuscaNombre.SetFocus
grdResultadoBus.RemoveAll

End Sub

Private Sub dtcBuscaFechaNacimiento_Change()
dtcFechaInicioInferior.Text = ""
dtcFechaInicioSuperior.Text = ""
txtNumeroFactura.Text = ""

End Sub

Private Sub dtcBuscaFechaNacimiento_Click()

dtcFechaInicioInferior.Text = ""
dtcFechaInicioSuperior.Text = ""
txtNumeroFactura.Text = ""
End Sub

Private Sub dtcFechaInicioInferior_Change()
txtBuscaNombre.Text = ""
txtBuscaApellido1.Text = ""
txtBuscaApellido2.Text = ""
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
cboBuscaDpto.Text = ""
cboBuscaTipoAsistencia.Text = ""
txtNumeroFactura.Text = ""

End Sub

Private Sub dtcFechaInicioInferior_Click()
txtBuscaNombre.Text = ""
txtBuscaApellido1.Text = ""
txtBuscaApellido2.Text = ""
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
cboBuscaDpto.Text = ""
cboBuscaTipoAsistencia.Text = ""
txtNumeroFactura.Text = ""
End Sub

Private Sub dtcFechaInicioSuperior_Change()
txtBuscaNombre.Text = ""
txtBuscaApellido1.Text = ""
txtBuscaApellido2.Text = ""
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
cboBuscaDpto.Text = ""
cboBuscaTipoAsistencia.Text = ""
txtNumeroFactura.Text = ""
End Sub

Private Sub dtcFechaInicioSuperior_Click()
txtBuscaNombre.Text = ""
txtBuscaApellido1.Text = ""
txtBuscaApellido2.Text = ""
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
cboBuscaDpto.Text = ""
cboBuscaTipoAsistencia.Text = ""
txtNumeroFactura.Text = ""

End Sub

Private Sub Form_Load()
 With grdResultadoBus
    .Columns(0).Caption = "Nombre y apellidos"
    .Columns(0).Width = 3000
    .Columns(1).Caption = "F. nacimiento"
    .Columns(1).Width = 1400
    .Columns(2).Caption = "Tipo asis"
    .Columns(2).Width = 1300
    .Columns(3).Caption = "Departamento"
    .Columns(3).Width = 1700
    .Columns(4).Caption = "F. inicio"
    .Columns(4).Width = 1400
    .Columns(5).Caption = "Asistencia"
    .Columns(5).Visible = False
    .Columns(6).Caption = "Proceso"
    .Columns(6).Visible = False
 End With
Call Cargar_Combos
dtcBuscaFechaNacimiento.Text = ""
dtcFechaInicioInferior.Text = ""
dtcFechaInicioSuperior.Text = ""
cboBuscaSexo.Text = ""
cboBuscaTipoAsistencia.Text = ""
cboBuscaDpto.Text = ""
End Sub
Private Sub Cargar_Combos()
Dim sql As String
Dim rst As rdoResultset
sql = "SELECT AD12CODTIPOASIST,AD12DESTIPOASIST FROM AD1200"
Set rst = objApp.rdoConnect.OpenResultset(sql)
Do While Not rst.EOF
   cboBuscaTipoAsistencia.AddItem rst(0) & Chr$(9) & rst(1)
   rst.MoveNext
Loop
rst.Close
sql = "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200"
Set rst = objApp.rdoConnect.OpenResultset(sql)
Do While Not rst.EOF
  cboBuscaDpto.AddItem rst(0) & Chr$(9) & rst(1)
  rst.MoveNext
Loop
rst.Close
  cboBuscaSexo.AddItem "1" & Chr$(9) & "Hombre"
  cboBuscaSexo.AddItem "2" & Chr$(9) & "Mujer"
  
End Sub

Private Sub grdResultadoBus_Click()
grdResultadoBus.SelBookmarks.RemoveAll
grdResultadoBus.SelBookmarks.Add (grdResultadoBus.RowBookmark(grdResultadoBus.Row))
End Sub

Private Sub grdResultadoBus_DblClick()
Dim strsql1 As String
grdResultadoBus.SelBookmarks.Add (grdResultadoBus.RowBookmark(grdResultadoBus.Row))

        strsql1 = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL,"
        strsql1 = strsql1 & "CI22DNI,CI22FECNACIM,CI22NUMSEGSOC,AD02DESDPTO,"
        strsql1 = strsql1 & "AD01CODASISTENCI,AD07CODPROCESO,AD11FECINICIO,AD12DESTIPOASIST"
        strsql1 = strsql1 & " FROM AD1121J"
        strsql1 = strsql1 & " WHERE AD01CODASISTENCI = " & grdResultadoBus.Columns("Asistencia").Text
        strsql1 = strsql1 & " AND AD07CODPROCESO = " & grdResultadoBus.Columns("Proceso").Text
       Call objPipe.PipeSet("Select", strsql1)
 
 Unload Me
 Set frmSeleccionProcesoAsistencia = Nothing
 
 Load frmProcesoAsistencia
 frmProcesoAsistencia.Show 1
End Sub

Private Sub txtBuscaApellido1_Change()
dtcFechaInicioInferior.Text = ""
dtcFechaInicioSuperior.Text = ""
txtNumeroFactura.Text = ""

End Sub

Private Sub txtBuscaApellido2_Change()
dtcFechaInicioInferior.Text = ""
dtcFechaInicioSuperior.Text = ""
txtNumeroFactura.Text = ""

End Sub

Private Sub txtBuscaNombre_Change()
dtcFechaInicioInferior.Text = ""
dtcFechaInicioSuperior.Text = ""
txtNumeroFactura.Text = ""

End Sub

Private Sub txtNumeroFactura_Change()
txtBuscaNombre.Text = ""
txtBuscaApellido1.Text = ""
txtBuscaApellido2.Text = ""
dtcBuscaFechaNacimiento.Text = ""
cboBuscaSexo.Text = ""
cboBuscaDpto.Text = ""
cboBuscaTipoAsistencia.Text = ""
dtcFechaInicioInferior.Text = ""
dtcFechaInicioSuperior.Text = ""

End Sub
