VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmProcesoAsistenciaExento 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asignaci�n de volantes de Osasunbidea"
   ClientHeight    =   4005
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4005
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSiRequiereVolante 
      Caption         =   "&S� requiere volante"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   3480
      Width           =   1935
   End
   Begin SSDataWidgets_B.SSDBGrid grdProcAsist 
      Height          =   2535
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   11640
      _Version        =   196615
      DataMode        =   2
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      SelectTypeCol   =   0
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20532
      _ExtentY        =   4471
      _StockProps     =   79
      ForeColor       =   8388608
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Proceso-asistencia exento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   360
      TabIndex        =   1
      Top             =   120
      Width           =   3180
   End
End
Attribute VB_Name = "frmProcesoAsistenciaExento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSiRequiereVolante_Click()
 Dim strsql As String
 Dim lngRespuesta As Long
 Dim rst1 As rdoResultset
 
 

    lngRespuesta = MsgBox("�Est� seguro de que quiere indicar que el proceso-asistencia si requiere volante?", vbYesNo + vbQuestion, "No requiere volante")
    If vbYes = lngRespuesta Then
        strsql = "DELETE FROM FA3200 WHERE AD01CODASISTENCI = " & grdProcAsist.Columns("Asistencia").Text
        strsql = strsql & " AND AD07CODPROCESO = " & grdProcAsist.Columns("Proceso").Text
        Set rst1 = objApp.rdoConnect.OpenResultset(strsql)
        Call Cargar_Grid
        
        
        
        
        
    End If
End Sub

Private Sub Form_Load()
Call Formatear_Grid
Call Cargar_Grid


End Sub
Private Sub Formatear_Grid()
With grdProcAsist
   .Columns(0).Caption = "Nombre y apellidos"
   .Columns(0).Width = 2300
   .Columns(1).Caption = "DNI"
   .Columns(1).Width = 1500
   .Columns(2).Caption = "Fecha nac."
   .Columns(2).Width = 1800
   .Columns(3).Caption = "Num. seg. soc."
   .Columns(3).Width = 2000
   .Columns(4).Caption = "Sexo"
   .Columns(4).Width = 1000
   .Columns(5).Caption = "Servicio"
   .Columns(5).Width = 2500
   .Columns(6).Caption = "Fecha inicio"
   .Columns(6).Width = 1800
   .Columns(7).Caption = "Tipo asistencia"
   .Columns(7).Width = 2300
   .Columns(8).Caption = "Asistencia"
   .Columns(8).Visible = False
   .Columns(9).Caption = "Proceso"
   .Columns(9).Visible = False
   
  End With
End Sub

Private Sub Cargar_Grid()
Dim sql As String
Dim rst As rdoResultset
Dim sexo As String
grdProcAsist.RemoveAll
sql = "SELECT CI2200.CI22NOMBRE,CI2200.CI22PRIAPEL,CI2200.CI22SEGAPEL"
sql = sql & ",CI2200.CI22DNI,CI2200.CI22FECNACIM,CI2200.CI22NUMSEGSOC"
sql = sql & ",CI2200.CI30CODSEXO,CI2200.CI22NOMBREALF,CI2200.CI22PRIAPELALF,CI2200.CI22SEGAPELALF"
sql = sql & ",FA3200.AD01CODASISTENCI,FA3200.AD07CODPROCESO,AD1100.AD11FECINICIO,AD0200.AD02CODDPTO"
sql = sql & ",AD0200.AD02DESDPTO,AD1200.AD12CODTIPOASIST,AD1200.AD12DESTIPOASIST"
sql = sql & " FROM FA3200,AD0700,AD1100,CI2200,AD0500,AD0200,AD2500,AD1200"
sql = sql & " WHERE FA3200.AD07CODPROCESO=AD0700.AD07CODPROCESO AND FA3200.AD01CODASISTENCI=AD1100.AD01CODASISTENCI"
sql = sql & " AND FA3200.AD07CODPROCESO=AD1100.AD07CODPROCESO AND AD0700.CI21CODPERSONA=CI2200.CI21CODPERSONA"
sql = sql & " AND FA3200.AD01CODASISTENCI=AD0500.AD01CODASISTENCI AND FA3200.AD07CODPROCESO=AD0500.AD07CODPROCESO"
sql = sql & " AND AD0500.AD02CODDPTO=AD0200.AD02CODDPTO AND FA3200.AD01CODASISTENCI=AD2500.AD01CODASISTENCI AND AD2500.AD12CODTIPOASIST=AD1200.AD12CODTIPOASIST"
Set rst = objApp.rdoConnect.OpenResultset(sql)
Do While Not rst.EOF
  If rst(6) = 1 Then
    sexo = "Hombre"
  Else
   sexo = "Mujer"
  End If
  
  grdProcAsist.AddItem rst(0) & " " & rst(1) & " " & rst(2) & Chr$(9) _
  & rst(3) & Chr$(9) _
  & rst(4) & Chr$(9) _
  & rst(5) & Chr$(9) _
  & sexo & Chr$(9) _
  & rst(14) & Chr$(9) _
  & rst(12) & Chr$(9) _
  & rst(16) & Chr$(9) _
  & rst(10) & Chr$(9) _
  & rst(11)
  
  rst.MoveNext
  sexo = ""
Loop

End Sub

Private Sub grdProcAsist_Click()
grdProcAsist.SelBookmarks.RemoveAll
   grdProcAsist.SelBookmarks.Add (grdProcAsist.RowBookmark(grdProcAsist.Row))
End Sub

