VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frminfAsignacionVolanteProcesoAsistencia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de asignación de volantes"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7155
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   7155
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   4200
      TabIndex        =   6
      Top             =   1920
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Listado"
      Default         =   -1  'True
      Height          =   375
      Left            =   720
      TabIndex        =   5
      Top             =   1920
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Fecha de asignación de volante y proc-asist"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   6495
      Begin SSCalendarWidgets_A.SSDateCombo dtcFechaInicioInferior 
         Height          =   255
         Left            =   1200
         TabIndex        =   1
         Top             =   600
         Width           =   1695
         _Version        =   65543
         _ExtentX        =   2990
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcFechaInicioSuperior 
         Height          =   255
         Left            =   3480
         TabIndex        =   2
         Top             =   600
         Width           =   1695
         _Version        =   65543
         _ExtentX        =   2990
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "y"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3120
         TabIndex        =   4
         Top             =   600
         Width           =   105
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Entre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   600
         TabIndex        =   3
         Top             =   600
         Width           =   465
      End
   End
End
Attribute VB_Name = "frminfAsignacionVolanteProcesoAsistencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
  Dim strsql As String
  Dim Formulas(2, 2) As Variant
  If IsNull(dtcFechaInicioInferior) Or IsNull(dtcFechaInicioSuperior) Then
    MsgBox "Debe rellenar todos los campos imprescindibles (con fondo azul) de este formulario.", vbOKOnly + vbInformation, "Faltan datos"
  Else
    
    strsql = "FA25FECASIGN >= TO_DATE('" & Format(dtcFechaInicioInferior, "DD/MM/YYYY") & "','DD/MM/YYYY')"
    strsql = strsql & " AND FA25FECASIGN <= TO_DATE('" & Format(dtcFechaInicioSuperior, "DD/MM/YYYY") & "','DD/MM/YYYY')"
    
    
    infVolantestxtFechaInicio = dtcFechaInicioInferior
    infVolantestxtFechaFin = dtcFechaInicioSuperior
    Formulas(1, 1) = "FIni"
    Formulas(1, 2) = "'" & infVolantestxtFechaInicio & "'"
    Formulas(2, 1) = "FFin"
    Formulas(2, 2) = "'" & infVolantestxtFechaFin & "'"
    Call Imprimir_API(strsql, "infAsignacionVolanteProcesoAsistencia.rpt", Formulas)
    
   
    
  End If
End Sub

Private Sub cmdCancelar_Click()
Unload Me
Set frminfAsignacionVolanteProcesoAsistencia = Nothing

End Sub

Private Sub Form_Load()
dtcFechaInicioSuperior = DateAdd("d", 1, dtcFechaInicioInferior.Text)

End Sub
