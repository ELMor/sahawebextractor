VERSION 5.00
Begin VB.Form frmRestriccionesProcesoAsistenciaFactura 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asignaci�n de volantes de Osasunbidea"
   ClientHeight    =   2040
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5880
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2040
   ScaleWidth      =   5880
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2880
      TabIndex        =   3
      Top             =   1440
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   600
      TabIndex        =   2
      Top             =   1440
      Width           =   1335
   End
   Begin VB.TextBox txtNumeroFactura 
      BackColor       =   &H00FFFF00&
      Height          =   285
      Left            =   2400
      TabIndex        =   1
      Top             =   360
      Width           =   1455
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "N�mero de Factura"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   480
      TabIndex        =   0
      Top             =   360
      Width           =   1635
   End
End
Attribute VB_Name = "frmRestriccionesProcesoAsistenciaFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub cmdAceptar_Click()
  Dim strsql As String
  
    
  If txtNumeroFactura = "" Then
    MsgBox "Debe rellenar todos los campos de este formulario.", vbOKOnly + vbInformation, "Faltan datos"
  Else
    infVolantestxtNumeroFactura = txtNumeroFactura.Text
    Call objPipe.PipeSet("NF", infVolantestxtNumeroFactura)
    Load frmProcesoAsistenciaFactura
    frmProcesoAsistenciaFactura.Show 1
   
   Unload Me
   
  End If
End Sub

Private Sub cmdCancelar_Click()
Unload Me
Set frmRestriccionesProcesoAsistenciaFactura = Nothing

End Sub
