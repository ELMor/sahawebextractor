VERSION 5.00
Begin VB.Form frm_MovAntOnline 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recuperar movimientos antiguos"
   ClientHeight    =   3645
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4515
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3645
   ScaleWidth      =   4515
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtMensaje 
      Height          =   1680
      Left            =   225
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1080
      Width           =   4065
   End
   Begin VB.CommandButton cmdComprobar 
      Caption         =   "&Comprobar"
      Default         =   -1  'True
      Height          =   510
      Left            =   225
      TabIndex        =   2
      Top             =   2925
      Width           =   1185
   End
   Begin VB.CommandButton cmdRecuperar 
      Caption         =   "&Recuperar"
      Enabled         =   0   'False
      Height          =   510
      Left            =   1665
      TabIndex        =   3
      Top             =   2925
      Width           =   1185
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   510
      Left            =   3105
      TabIndex        =   4
      Top             =   2925
      Width           =   1185
   End
   Begin VB.TextBox txtNumFact 
      Height          =   330
      Left            =   2520
      MaxLength       =   10
      TabIndex        =   0
      Top             =   315
      Width           =   1770
   End
   Begin VB.Label Label1 
      Caption         =   "N�mero de Factura Antigua"
      Height          =   510
      Left            =   225
      TabIndex        =   5
      Top             =   405
      Width           =   2040
   End
End
Attribute VB_Name = "frm_MovAntOnline"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private qryFact As New rdoQuery
Private qryRecuperar As New rdoQuery
Private strNumCaso As String


Private Sub Form_Load()
   Dim SQL As String
   
   SQL = " select fa0400.ad07codproceso,fa0400.ad01codasistenci,ad08numcaso,r8mov.* from ad0800, fa0400, r8mov "
   SQL = SQL & " where fa04numfact = ? and "
   SQL = SQL & " fa0400.ad07codproceso = ad0800.ad07codproceso and "
   SQL = SQL & " fa0400.ad01codasistenci= ad0800.ad01codasistenci and "
   SQL = SQL & " ad08numcaso = numcaso(+)"
   
   qryFact.SQL = SQL
   
   ' preparar la query en el servidor
   Set qryFact.ActiveConnection = objApp.rdoConnect
   qryFact.Prepared = True
   
   SQL = " BEGIN MOVANTONLINE(?); END;"
   
   qryRecuperar.SQL = SQL
   
   ' preparar la query en el servidor
   Set qryRecuperar.ActiveConnection = objApp.rdoConnect
   qryRecuperar.Prepared = True
   
   
End Sub

Private Sub cmdComprobar_Click()
   Dim RS As rdoResultset
   
   On Error GoTo error
   
   txtMensaje.Text = ""
   cmdRecuperar.Enabled = False
   
   If txtNumFact.Text = "" Then Exit Sub
   
   qryFact.rdoParameters(0) = txtNumFact.Text
   Set RS = qryFact.OpenResultset
   If RS.EOF Then
      txtMensaje.Text = "La factura no existe."
   Else
      
      ' comprobar que tenga movimientos esa factura
      If IsNull(RS!NumCaso) Then
         ' Guardar el numero del caso
         strNumCaso = RS!AD08NumCaso
         
         txtMensaje.Text = "La factura no tiene movimientos."
         cmdRecuperar.Enabled = True
      Else
         'Mostrar los movimientos
         txtMensaje.Text = "Movimientos :  "
         
         txtMensaje.Text = txtMensaje.Text & RS!TRABA8
         RS.MoveNext
         Do While Not RS.EOF
            txtMensaje.Text = txtMensaje.Text & ", " & RS!TRABA8
            RS.MoveNext
         Loop
      End If
   End If
Exit Sub

error:
   MsgBox "Se ha producido un error " & Err.Description, vbInformation + vbOKOnly, "Error"
End Sub

Private Sub cmdRecuperar_Click()
   On Error GoTo error
   If strNumCaso <> "" Then
      ' recuperar los movimientos
'      qryRecuperar.rdoParameters(0) = strNumCaso
      objApp.rdoConnect.Execute "BEGIN MOVANTONLINE('" & strNumCaso & "'); END;", 64
   End If
Exit Sub

error:
   MsgBox "Se ha producido un error " & Err.Description, vbInformation + vbOKOnly, "Error"
End Sub

Private Sub cmdSalir_Click()
   Unload Me
End Sub

Private Sub txtNumFact_Change()
   cmdRecuperar.Enabled = False
   txtMensaje.Text = ""
End Sub

Private Sub txtNumFact_GotFocus()
   txtNumFact.SelStart = 0
   txtNumFact.SelLength = Len(txtNumFact)
End Sub
