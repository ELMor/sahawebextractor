VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frm_InsercionFactMasiva 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Inserci�n Facturaci�n Masiva"
   ClientHeight    =   3345
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6405
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3345
   ScaleWidth      =   6405
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdFacturar 
      Caption         =   "Facturar"
      Height          =   375
      Left            =   2235
      TabIndex        =   4
      Top             =   2370
      Width           =   1575
   End
   Begin VB.ComboBox cboCodEntidad 
      Height          =   315
      Left            =   2595
      TabIndex        =   3
      Top             =   960
      Width           =   3615
   End
   Begin VB.TextBox txtCodEntidad 
      Height          =   315
      Left            =   1995
      TabIndex        =   2
      Top             =   960
      Width           =   495
   End
   Begin VB.ComboBox cboCodTipEcon 
      Height          =   315
      Left            =   2595
      TabIndex        =   1
      Top             =   480
      Width           =   3615
   End
   Begin VB.TextBox txtCodTipEcon 
      Height          =   315
      Left            =   1995
      TabIndex        =   0
      Top             =   480
      Width           =   495
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFecha 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   1995
      TabIndex        =   8
      Tag             =   "Fecha Inicio"
      Top             =   1440
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Entidad:"
      Height          =   255
      Left            =   480
      TabIndex        =   7
      Top             =   1080
      Width           =   1440
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo Econ�mico:"
      Height          =   255
      Left            =   480
      TabIndex        =   6
      Top             =   600
      Width           =   1440
   End
   Begin VB.Label lblFecInicio 
      Alignment       =   1  'Right Justify
      Caption         =   "Mes a facturar:"
      Height          =   255
      Left            =   480
      TabIndex        =   5
      Top             =   1485
      Width           =   1440
   End
End
Attribute VB_Name = "frm_InsercionFactMasiva"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim QrySelec(1 To 15) As New rdoQuery

Private Type typeRNF
  Asistencia As Long
  CodCateg As String
  CodRNF As String
  Cantidad As Long
  Fecha As String
End Type
Dim MatrizRNF() As typeRNF
Dim CadenaConceptos As String

Private varFact As factura
Private Paciente As Paciente
Private Asistencia As Asistencia
Private NuevoRNF As rnf
Private PropuestaFact As PropuestaFactura
Private NodoFACT As Nodo
Private Nodos As Nodo
Private ConciertoAplicable As Concierto
Private Entidad As Nodo
Private rnf As rnf

Dim NumHisto As Long
Dim PrimeraVez As Boolean
Dim NumFactura As String
Dim TotalPacientes As Long

Private Sub cboCodEntidad_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13CODENTIDAD From CI1300 Where CI13DESENTIDAD = '" & Me.cboCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodEntidad.Text = RS("CI13CODENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If

End Sub

Private Sub cboCodTipEcon_Click()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodTipEcon.Text) <> "" Then
    SQL = "Select CI32CODTIPECON From CI3200 Where CI32DESTIPECON = '" & Me.cboCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodTipEcon.Text = RS("CI32CODTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.txtCodTipEcon.Text = ""
      Me.cboCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If
End Sub

Private Sub cboCodTipEcon_DropDown()
Dim SQL As String
Dim RS As rdoResultset

  cboCodTipEcon.Clear
  
  SQL = "Select CI32DESTIPECON From CI3200 Where CI32INDRESPECO = -1"
  Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
  If Not RS.EOF Then
    RS.MoveLast
    RS.MoveFirst
    While Not RS.EOF
      cboCodTipEcon.AddItem RS("CI32DESTIPECON") & ""
      RS.MoveNext
    Wend
  End If
End Sub

Private Sub cboCodTipEcon_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodTipEcon.Text) <> "" Then
    SQL = "Select CI32CODTIPECON From CI3200 Where CI32DESTIPECON = '" & Me.cboCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodTipEcon.Text = RS("CI32CODTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.txtCodTipEcon.Text = ""
      Me.cboCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If

End Sub
Private Function ComprobarCampos() As Boolean
    
    'Comprobamos que haya un tipo econ�mico
    If Trim(Me.txtCodTipEcon.Text) = "" Then
      MsgBox "Se debe seleccionar un Tipo Econ�mico.", vbOKOnly + vbInformation, "Aviso"
      If Me.txtCodTipEcon.Enabled = True Then
        Me.txtCodTipEcon.SetFocus
      End If
      ComprobarCampos = False
      Exit Function
    End If
    
    'Comprobamos que haya una entidad
    If Trim(Me.txtCodEntidad.Text) = "" Then
      MsgBox "Se debe seleccionar una entidad.", vbOKOnly + vbInformation, "Aviso"
      If Me.txtCodEntidad.Enabled = True Then
        Me.txtCodEntidad.SetFocus
      End If
      ComprobarCampos = False
      Exit Function
    End If
    
    'Comprobamos que la fecha  sea correcta
    If Not IsNull(Me.SDCFecha.Date) Then
      If Not IsDate(Me.SDCFecha.Date) Then
        MsgBox "La Fecha de Inicio no es correcta.", vbOKOnly + vbInformation, "Aviso"
        If Me.SDCFecha.Enabled = True Then
          Me.SDCFecha.SetFocus
        End If
        ComprobarCampos = False
        Exit Function
      End If
    End If
    ComprobarCampos = True
End Function


Private Sub cmdFacturar_Click()
Dim str$
Dim qy As rdoQuery
Dim UltimoDia$
Dim PrimerDia$
Dim PrimerDiaMesSiguiente$
Dim seguir As Boolean
Me.MousePointer = vbHourglass
seguir = ComprobarCampos()
If Not seguir Then Exit Sub
PrimerDia = Format("01/" & Format(SDCFecha.Date, "MM/YYYY"), "DD/MM/YYYY")
PrimerDiaMesSiguiente = DateAdd("m", 1, PrimerDia)
UltimoDia = DateAdd("d", -1, PrimerDiaMesSiguiente)
str = " INSERT INTO FA3700 (FA37CODPAC, AD01CODASISTENCI, AD07CODPROCESO, " & _
 "CI32CODTIPECON, CI13CODENTIDAD, AD12CODTIPOASIST, FA37INDFACT, FA37MES)" & _
 "SELECT  FA37CODPAC_SEQUENCE.NEXTVAL, AD01CODASISTENCI, AD07CODPROCESO, ?, " & _
 "?, 0, 0, TO_DATE(?,'DD/MM/YYYY') " & _
 "From AD0800 " & _
 "Where " & _
 "(AD01CODASISTENCI,AD07CODPROCESO) IN " & _
 "(SELECT /*+ RULE +*/ DISTINCT AD1100.AD01CODASISTENCI, AD1100.AD07CODPROCESO " & _
 "FROM  FA0400, AD0800, AD1100 " & _
 "WHERE  AD11FECINICIO < TO_DATE(?,'DD/MM/YYYY') " & _
 "AND  NVL(AD11FECFIN, AD08FECFIN) >=  TO_DATE(?,'DD/MM/YYYY') " & _
 "AND  AD1100.CI32CODTIPECON = ? " & _
 "AND  AD1100.CI13CODENTIDAD = ? " & _
 "AND  AD0800.AD01CODASISTENCI = AD1100.AD01CODASISTENCI " & _
 "AND  AD0800.AD07CODPROCESO = AD1100.AD07CODPROCESO " & _
 "AND  AD08FECFIN < TO_DATE(?,'DD/MM/YYYY') " & _
 "AND  FA0400.AD01CODASISTENCI (+) = AD1100.AD01CODASISTENCI " & _
 "AND  FA0400.AD07CODPROCESO (+)= AD1100.AD07CODPROCESO " & _
 "AND  FA0400.CI32CODTIPECON (+)= AD1100.CI32CODTIPECON " & _
 "AND  FA0400.CI13CODENTIDAD (+)= AD1100.CI13CODENTIDAD " & _
 "AND  FA04NUMFACT IS NULL)"
 Set qy = objApp.rdoConnect.CreateQuery("", str)
  qy(0) = txtCodTipEcon.Text
  qy(1) = txtCodEntidad.Text
  qy(2) = UltimoDia
  qy(3) = PrimerDiaMesSiguiente
  qy(4) = PrimerDia
  qy(5) = txtCodTipEcon.Text
  qy(6) = txtCodEntidad.Text
  qy(7) = PrimerDiaMesSiguiente
 qy.Execute
 qy.Close
 Me.MousePointer = vbDefault
 If Err = 0 Then
  MsgBox "Proceso Finalizado", vbOKOnly + vbInformation, "Inserci�n Facturaci�n Masiva"
 End If
End Sub


Private Sub Form_Load()
'  Call modFacturacion.IniciarQRYFact
'  Call modFacturacion.InicQrySelec
'  Call modFacturacion.IniciarQRYMasiva
  objCW.blnAutoDisconnect = False
End Sub


Private Sub txtCodEntidad_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13DESENTIDAD From CI1300 Where CI13CODENTIDAD = '" & Me.txtCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.cboCodEntidad.Text = RS("CI13DESENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If


End Sub


Private Sub txtCodTipEcon_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI32DESTIPECON From CI3200 Where CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.cboCodTipEcon.Text = RS("CI32DESTIPECON") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "El Tipo Econ�mico introducido no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodTipEcon.Text = ""
      Me.txtCodTipEcon.SetFocus
    End If
  Else
    Me.txtCodTipEcon.Text = ""
  End If


End Sub
Private Sub cboCodEntidad_Click()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.cboCodEntidad.Text) <> "" And Trim(Me.txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13CODENTIDAD From CI1300 Where CI13DESENTIDAD = '" & Me.cboCodEntidad.Text & "' " & _
              "And CI32CODTIPECON = '" & Me.txtCodTipEcon.Text & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodEntidad.Text = RS("CI13CODENTIDAD") & ""
        RS.MoveNext
      Wend
    Else
      MsgBox "La Entidad  introducida no es correcto.", vbCritical + vbOKOnly, "Atenci�n"
      Me.cboCodEntidad.Text = ""
      Me.txtCodEntidad.SetFocus
    End If
  Else
    Me.txtCodEntidad.Text = ""
  End If
End Sub

Private Sub cboCodEntidad_DropDown()
Dim SQL As String
Dim RS As rdoResultset

  cboCodEntidad.Clear
  
  If Trim(txtCodTipEcon.Text) <> "" Then
    SQL = "Select CI13DESENTIDAD From CI1300 Where CI32CODTIPECON = '" & Me.txtCodTipEcon & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        cboCodEntidad.AddItem RS("CI13DESENTIDAD") & ""
        RS.MoveNext
      Wend
    End If
  Else
    MsgBox "Debe seleccionar un tipo econ�mico", vbInformation + vbOKOnly, "Aviso"
  End If
End Sub

