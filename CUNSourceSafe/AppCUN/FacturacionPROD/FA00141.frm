VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_ListAltas 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Listado de Altas"
   ClientHeight    =   2220
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3585
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2220
   ScaleWidth      =   3585
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   915
      Left            =   120
      TabIndex        =   6
      Top             =   720
      Width           =   3375
      Begin VB.OptionButton optPendientes 
         Caption         =   "Pendientes fra."
         Height          =   195
         Left            =   1680
         TabIndex        =   9
         Top             =   240
         Width           =   1515
      End
      Begin VB.OptionButton optAlfabetico 
         Caption         =   "Por Orden Alfab�tico"
         Height          =   195
         Left            =   240
         TabIndex        =   8
         Top             =   600
         Width           =   1935
      End
      Begin VB.OptionButton optHistoria 
         Caption         =   "Por Historia"
         Height          =   195
         Left            =   240
         TabIndex        =   7
         Top             =   240
         Value           =   -1  'True
         Width           =   1215
      End
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Iniciar Impresi�n"
      Height          =   330
      Left            =   1020
      TabIndex        =   1
      Top             =   1800
      Width           =   1725
   End
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   240
      Left            =   270
      TabIndex        =   0
      Top             =   855
      Visible         =   0   'False
      Width           =   375
      _Version        =   196608
      _ExtentX        =   661
      _ExtentY        =   423
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      Header          =   "LISTADO DE ALTAS"
      MousePointer    =   7
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   1755
      TabIndex        =   2
      Tag             =   "Fecha Inicio"
      Top             =   45
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFin 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   1755
      TabIndex        =   3
      Tag             =   "Fecha Inicio"
      Top             =   360
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label lblFecFin 
      Caption         =   "Fecha de Final:"
      Enabled         =   0   'False
      Height          =   255
      Left            =   180
      TabIndex        =   5
      Top             =   405
      Width           =   1650
   End
   Begin VB.Label lblFecInicio 
      Caption         =   "Fecha de Inicio:"
      Enabled         =   0   'False
      Height          =   255
      Left            =   180
      TabIndex        =   4
      Top             =   90
      Width           =   1650
   End
End
Attribute VB_Name = "frm_ListAltas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ArrayListado() As String

Private Sub cmdImprimir_Click()
Dim Seguir As Boolean
Dim Nombre As String
Dim Historia As String
Dim Tipo As String
Dim Dias As Integer
Dim MiSql As String
Dim MiRs As rdoResultset
Dim SqLpAc As String
Dim RsPaC As rdoResultset
Dim SqLtIpO As String
Dim RsTiPo As rdoResultset
Dim SqLdPtO As String
Dim RsDpTo As rdoResultset
Dim SqLcAmA As String
Dim RsCaMa As rdoResultset
Dim Departamento As String
Dim Cont As Integer
Dim Listar As Boolean
Dim Cama As String

  'Comprobamos que la fecha de inicio no sea mayor que la fecha final y que estas sean correctas
  If Not IsDate(Me.SDCFechaInicio.Date) Then
    MsgBox "La fecha de inicio no es correcta", vbOKOnly + vbCritical, "Aviso"
    Exit Sub
  End If
  If Not IsDate(Me.SDCFechaFin.Date) Then
    MsgBox "La fecha final no es correcta", vbOKOnly + vbCritical, "Aviso"
    Exit Sub
  End If
  If CDate(Me.SDCFechaFin.Date) < CDate(Me.SDCFechaInicio.Date) Then
    MsgBox "La fecha final no puede ser menor que la fecha de inicio", vbCritical + vbOKOnly, "Atenci�n"
    Exit Sub
  End If
  
  'Una vez comprobados las fechas seleccionamos de PR0400 todas los procesos y asistencias que tienen como departamento
  'urgencias y se encuentran entre dos fechas concretas.
  MiSql = "Select /*+RULE*/ AD0800.AD01CODASISTENCI, AD0800.AD07CODPROCESO, AD08FECINICIO, AD08FECFIN, " & _
              "AD0100.CI21CODPERSONA, AD0500.AD02CODDPTO, CI2200.CI22PRIAPEL, CI2200.CI22NUMHISTORIA " & _
              "From AD0500,AD0100,AD0800,FA0400,AD2500,CI2200 " & _
              "Where AD08FECFIN >= TO_DATE('" & Me.SDCFechaInicio & " 00:00:00','DD/MM/YYYY HH24:MI:SS') " & _
              "AND AD08FECFIN <= TO_DATE('" & Me.SDCFechaFin & " 23:59:59','DD/MM/YYYY HH24:MI:SS') " & _
              "AND AD0800.AD01CODASISTENCI = AD0100.AD01CODASISTENCI " & _
              "AND AD0800.AD01CODASISTENCI = AD0500.AD01CODASISTENCI " & _
              "AND AD0800.AD07CODPROCESO = AD0500.AD07CODPROCESO " & _
              "AND FA0400.AD01CODASISTENCI (+)= AD0800.AD01CODASISTENCI " & _
              "AND FA0400.AD07CODPROCESO (+)= AD0800.AD07CODPROCESO " & _
              "AND AD0800.AD01CODASISTENCI = AD2500.AD01CODASISTENCI " & _
              "AND AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
              "AND AD2500.AD12CODTIPOASIST = 1 "

  If Me.optAlfabetico.Value = True Then
    MiSql = MiSql & "ORDER BY CI22PRIAPEL ASC"
  ElseIf Me.optHistoria.Value = True Then
    MiSql = MiSql & "ORDER BY CI2200.CI22NUMHISTORIA ASC "
  ElseIf Me.optPendientes.Value = True Then
    MiSql = MiSql & "AND FA0400.FA04CODFACT IS NULL"
  End If
  
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    Cont = 1
    While Not MiRs.EOF
      If Not IsNull(MiRs("CI21CODPERSONA")) Then
        SqLpAc = "Select CI22NUMHISTORIA,CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL " & _
                      "From CI2200 WHERE CI21CODPERSONA = " & MiRs("CI21CODPERSONA")
        Set RsPaC = objApp.rdoConnect.OpenResultset(SqLpAc, 3)
        If Not RsPaC.EOF Then
          Nombre = ""
          If Not IsNull(RsPaC("CI22NOMBRE")) Then
            Nombre = Nombre & RsPaC("CI22NOMBRE") & " "
          End If
          If Not IsNull(RsPaC("CI22PRIAPEL")) Then
            Nombre = Nombre & RsPaC("CI22PRIAPEL") & " "
          End If
          If Not IsNull(RsPaC("CI22SEGAPEL")) Then
            Nombre = Nombre & RsPaC("CI22SEGAPEL")
          End If
          If Not IsNull(RsPaC("CI22NUMHISTORIA")) Then
            Historia = CStr(RsPaC("CI22NUMHISTORIA"))
          Else
            Historia = ""
          End If
        Else
          Nombre = ""
          Historia = ""
        End If
      End If
      'Buscamos el tipo econ�mico de la persona
      If Not IsNull(MiRs("AD07CODPROCESO")) And Not IsNull(MiRs("AD01CODASISTENCI")) Then
        SqLtIpO = "SELECT CI13CODENTIDAD, CI32CODTIPECON FROM AD1100 " & _
                  "WHERE AD07CODPROCESO = " & MiRs("AD07CODPROCESO") & " " & _
                  "AND AD01CODASISTENCI = " & MiRs("AD01CODASISTENCI") & " " & _
                  "AND AD11FECFIN IS NULL"
        Set RsTiPo = objApp.rdoConnect.OpenResultset(SqLtIpO, 3)
        If Not RsTiPo.EOF Then
          If Not IsNull(RsTiPo("CI13CODENTIDAD")) And Not IsNull(RsTiPo("CI32CODTIPECON")) Then
            Tipo = RsTiPo("CI32CODTIPECON") & "/" & Right(Space(2) & RsTiPo("CI13CODENTIDAD"), 2)
          End If
        End If
      Else
        Tipo = ""
      End If
      'Buscamos el departamento responsable de la asistencia
      If Not IsNull(MiRs("AD02CODDPTO")) Then
        SqLdPtO = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = " & MiRs("AD02CODDPTO")
        Set RsDpTo = objApp.rdoConnect.OpenResultset(SqLdPtO, 3)
        If Not RsDpTo.EOF Then
          Departamento = RsDpTo("AD02DESDPTO")
        End If
      End If
      'Buscamos la �ltima cama en la que ha estado esa persona.
      If Not IsNull(MiRs("AD07CODPROCESO")) And Not IsNull(MiRs("AD01CODASISTENCI")) Then
        SqLcAmA = "Select GCFN06(AD15CODCAMA) As CAMA From AD1600 " & _
                  "Where AD01CODASISTENCI = " & MiRs("AD01CODASISTENCI") & _
                  " And AD07CODPROCESO = " & MiRs("AD07CODPROCESO") & " ORDER BY AD16FECFIN DESC"
        Set RsCaMa = objApp.rdoConnect.OpenResultset(SqLcAmA, 3)
        If Not RsCaMa.EOF Then
          If Not IsNull(RsCaMa("CAMA")) Then
            Cama = RsCaMa("CAMA")
          Else
            Cama = ""
          End If
        Else
          Cama = ""
        End If
      Else
        Cama = ""
      End If
      ReDim Preserve ArrayListado(1 To 8, 1 To Cont)
      ArrayListado(1, Cont) = Historia
      ArrayListado(2, Cont) = Nombre
      If Not IsNull(MiRs("AD08FECINICIO")) Then
        ArrayListado(3, Cont) = CStr(MiRs("AD08FECINICIO"))
      Else
        ArrayListado(3, Cont) = ""
      End If
      If Not IsNull(MiRs("AD08FECFIN")) Then
        ArrayListado(4, Cont) = CStr(MiRs("AD08FECFIN"))
      Else
        ArrayListado(4, Cont) = ""
      End If
      If Not IsNull(MiRs("AD08FECINICIO")) And Not IsNull(MiRs("AD08FECFIN")) Then
        Dias = DateDiff("D", CDate(MiRs("AD08FECINICIO")), CDate(MiRs("AD08FECFIN")))
        ArrayListado(5, Cont) = Dias
      End If
      ArrayListado(6, Cont) = Departamento
      ArrayListado(7, Cont) = Tipo
      ArrayListado(8, Cont) = Cama
      Cont = Cont + 1
      MiRs.MoveNext
    Wend
    Seguir = True
  Else
    MsgBox "No hay entradas en urgencias en las fechas seleccionadas", vbInformation + vbOKOnly, "Aviso"
    Seguir = False
  End If
  If Seguir Then
    Call Me.ImprimirListado
  End If
End Sub


Sub ImprimirListado()
Dim Cont As Integer
Dim Pagina As Integer
Dim Texto As String
Dim NoAbonos As Integer
Dim Tipo As String

  'Calculamos el n�mero de pagos que vamos a listar
  NoAbonos = UBound(ArrayListado, 2)
  Pagina = 1
  vsPrinter.Orientation = orLandscape
  vsPrinter.Preview = False
  vsPrinter.Action = 3
  vsPrinter.FontSize = 9
  vsPrinter.FontName = "Courier New"
  vsPrinter.Paragraph = ""
  Texto = "N� HISTORIA NOMBRE                              CAMA    FECHA DE INGRESO FECHA DE ALTA    DIAS DEPARTAMENTO         TIPO"
  vsPrinter.Paragraph = Texto
  Texto = "----------- ----------------------------------- ------- ---------------- ---------------- ---- -------------------- ----"
  vsPrinter.Paragraph = Texto
  For Cont = 1 To NoAbonos
    Texto = Right(Space(11) & Trim(ArrayListado(1, Cont)), 11) & " " 'N� de Historia
    Texto = Texto & Left(Trim(ArrayListado(2, Cont)) & Space(35), 35) & " "  'Paciente
    Texto = Texto & Left(Trim(ArrayListado(8, Cont)) & Space(7), 7) & " " 'C�digo de Cama
    Texto = Texto & Format(ArrayListado(3, Cont), "DD/MM/YYYY HH:MM") & " "  'Fecha de Inicio
    Texto = Texto & Format(ArrayListado(4, Cont), "DD/MM/YYYY HH:MM") & " "  'Fecha de Final
    Texto = Texto & Right(Space(4) & Trim(ArrayListado(5, Cont)), 4) & " " 'D�as
    Texto = Texto & Left(Trim(ArrayListado(6, Cont)) & Space(20), 20) & " " 'Departamento
    Texto = Texto & Right(Space(4) & Trim(ArrayListado(7, Cont)), 4) 'Tipo
    vsPrinter.Paragraph = Texto
    If vsPrinter.CurrentY > 260 * 54.6101086 Then
      'Cuando llegamos al final de la p�gina pasamos a la siguiente.
      'Y Volvemos a imprimir la cabecera.
      vsPrinter.NewPage
      vsPrinter.CurrentY = 10 * 54.6101086
      Pagina = Pagina + 1
      vsPrinter.Paragraph = ""
      Texto = "N� HISTORIA NOMBRE                              CAMA    FECHA DE INGRESO FECHA DE ALTA    DIAS DEPARTAMENTO         TIPO"
      vsPrinter.Paragraph = Texto
      Texto = "----------- ----------------------------------- ------- ---------------- ---------------- ---- -------------------- ----"
      vsPrinter.Paragraph = Texto
    End If
  Next
  vsPrinter.Action = 6
End Sub




