VERSION 5.00
Begin VB.Form frm_Propiedades 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Propiedades"
   ClientHeight    =   4740
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6240
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4740
   ScaleWidth      =   6240
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   420
      Left            =   5220
      TabIndex        =   14
      Top             =   4230
      Width           =   960
   End
   Begin VB.Frame Frame2 
      Caption         =   "Propiedades"
      Height          =   3300
      Left            =   0
      TabIndex        =   15
      Top             =   810
      Width           =   6180
      Begin VB.CheckBox chkDescontable 
         Alignment       =   1  'Right Justify
         Caption         =   "Descontable"
         Height          =   375
         Left            =   135
         TabIndex        =   8
         Top             =   1800
         Width           =   1650
      End
      Begin VB.CheckBox ChkSuficiente 
         Alignment       =   1  'Right Justify
         Caption         =   "Suficiente"
         Height          =   195
         Left            =   3015
         TabIndex        =   10
         Top             =   1395
         Width           =   1650
      End
      Begin VB.CheckBox chkFactuOblig 
         Alignment       =   1  'Right Justify
         Caption         =   "Factura Obligatoria"
         Height          =   195
         Left            =   3015
         TabIndex        =   11
         Top             =   2160
         Width           =   1650
      End
      Begin VB.CheckBox chkLinOblig 
         Alignment       =   1  'Right Justify
         Caption         =   "L�nea Obligatoria"
         Height          =   195
         Left            =   135
         TabIndex        =   9
         Top             =   2160
         Width           =   1650
      End
      Begin VB.CheckBox chkExcluido 
         Alignment       =   1  'Right Justify
         Caption         =   "Excluido"
         Height          =   330
         Left            =   135
         TabIndex        =   7
         Top             =   1575
         Width           =   1650
      End
      Begin VB.CheckBox chkNecesario 
         Alignment       =   1  'Right Justify
         Caption         =   "Necesario"
         Height          =   195
         Left            =   135
         TabIndex        =   6
         Top             =   1395
         Width           =   1650
      End
      Begin VB.TextBox txtPrecioDia 
         BackColor       =   &H80000003&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1620
         TabIndex        =   13
         Top             =   2880
         Width           =   1140
      End
      Begin VB.TextBox txtDescuento 
         BackColor       =   &H80000003&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1620
         TabIndex        =   12
         Top             =   2520
         Width           =   1140
      End
      Begin VB.TextBox txtGarantia 
         BackColor       =   &H80000003&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1620
         TabIndex        =   5
         Top             =   990
         Width           =   1140
      End
      Begin VB.TextBox txtFranqSuma 
         BackColor       =   &H80000003&
         Enabled         =   0   'False
         Height          =   285
         Left            =   4500
         TabIndex        =   4
         Top             =   630
         Width           =   1140
      End
      Begin VB.TextBox txtFranqUnit 
         BackColor       =   &H80000003&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1620
         TabIndex        =   3
         Top             =   630
         Width           =   1140
      End
      Begin VB.TextBox txtPrecioRef 
         BackColor       =   &H80000003&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1620
         TabIndex        =   2
         Top             =   270
         Width           =   1140
      End
      Begin VB.Label Label1 
         Caption         =   "Precio referencia"
         Height          =   285
         Index           =   6
         Left            =   135
         TabIndex        =   22
         Top             =   315
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Franquicia Unitaria"
         Height          =   285
         Index           =   5
         Left            =   135
         TabIndex        =   21
         Top             =   675
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Franquicia de Suma"
         Height          =   285
         Index           =   4
         Left            =   2970
         TabIndex        =   20
         Top             =   675
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Periodo Garant�a"
         Height          =   285
         Index           =   3
         Left            =   135
         TabIndex        =   19
         Top             =   1035
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Descuento"
         Height          =   240
         Index           =   2
         Left            =   135
         TabIndex        =   18
         Top             =   2565
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Precio d�a"
         Height          =   285
         Index           =   1
         Left            =   135
         TabIndex        =   17
         Top             =   2925
         Width           =   1455
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Categor�a"
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   45
      Width           =   6180
      Begin VB.TextBox txtDesignacion 
         BackColor       =   &H80000004&
         Enabled         =   0   'False
         Height          =   285
         Left            =   1575
         TabIndex        =   1
         Text            =   " "
         Top             =   270
         Width           =   2940
      End
      Begin VB.Label Label1 
         Caption         =   "Designaci�n:"
         Height          =   285
         Index           =   0
         Left            =   135
         TabIndex        =   16
         Top             =   315
         Width           =   1050
      End
   End
End
Attribute VB_Name = "frm_Propiedades"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

