Attribute VB_Name = "ModFunciones"
Option Explicit

Public qryConc As New rdoQuery
Public qryCateg As New rdoQuery
Public qryTarifa As New rdoQuery
Public qryNodoFact As New rdoQuery

Public Sub IniciarQRY()
   
   qryConc.Sql = "SELECT FA09CODNODOCONC, FA09DESIG From fa0900 WHERE FA09INDCONCIERTO = -1"
   
   Set qryConc.ActiveConnection = objApp.rdoConnect
   qryConc.Prepared = True


   qryCateg.Sql = "Select FA05CODCATEG, FA05DESIG From FA0500 " & _
                  " where UPPER(FA05DESIG) Like upper(?) " & _
                  "   And TO_DATE(?,'DD/MM/YYYY') between FA05FECINICIO AND FA05FECFIN "
              
   Set qryCateg.ActiveConnection = objApp.rdoConnect
   qryCateg.Prepared = True


   qryTarifa.Sql = "SELECT * FROM fa1500 " & _
                  " Where fa15codconc = ? " & _
                  "   AND fa15codfin = ? " & _
                  "   AND FA15FECINICIO <= TO_DATE(?,'DD/MM/YYYY') " & _
                  "   AND TO_DATE(?,'DD/MM/YYYY') < FA15FECFIN"

   Set qryTarifa.ActiveConnection = objApp.rdoConnect
   qryTarifa.Prepared = True


   qryNodoFact.Sql = "SELECT FA09DESIG From fa0900 WHERE FA09CODNODOCONC = ?"
   
   Set qryNodoFact.ActiveConnection = objApp.rdoConnect
   qryNodoFact.Prepared = True

End Sub

