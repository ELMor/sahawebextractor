VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "Comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPrecios 
   Caption         =   "Consulta de Tarifas"
   ClientHeight    =   8415
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10365
   Icon            =   "FA00166.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8415
   ScaleWidth      =   10365
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "Buscar"
      Default         =   -1  'True
      Height          =   330
      Left            =   9270
      TabIndex        =   13
      Top             =   45
      Width           =   1050
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   330
      Left            =   9270
      TabIndex        =   8
      Top             =   765
      Width           =   1050
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "Consultar"
      Height          =   330
      Left            =   9270
      TabIndex        =   7
      Top             =   405
      Width           =   1050
   End
   Begin VB.Frame Frame3 
      Caption         =   "Categoria"
      Height          =   645
      Left            =   45
      TabIndex        =   3
      Top             =   90
      Width           =   8610
      Begin VB.TextBox txtDesig 
         Height          =   330
         Left            =   1215
         TabIndex        =   1
         Top             =   225
         Width           =   4605
      End
      Begin SSCalendarWidgets_A.SSDateCombo sdcFecha 
         Height          =   330
         Left            =   6660
         TabIndex        =   4
         Top             =   225
         Width           =   1770
         _Version        =   65537
         _ExtentX        =   3122
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483634
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.TextBox txtCodCateg 
         Height          =   285
         Left            =   5580
         TabIndex        =   9
         Top             =   270
         Visible         =   0   'False
         Width           =   330
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Descripción :"
         Height          =   195
         Left            =   165
         TabIndex        =   6
         Top             =   315
         Width           =   930
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Fecha :"
         Height          =   195
         Left            =   5940
         TabIndex        =   5
         Top             =   315
         Width           =   540
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Tarifas"
      Height          =   7305
      Left            =   5355
      TabIndex        =   0
      Top             =   1080
      Width           =   4965
      Begin ComctlLib.TreeView tvTarifas 
         Height          =   6945
         Left            =   90
         TabIndex        =   2
         Top             =   225
         Width           =   4785
         _ExtentX        =   8440
         _ExtentY        =   12250
         _Version        =   327682
         Indentation     =   529
         LabelEdit       =   1
         Sorted          =   -1  'True
         Style           =   7
         ImageList       =   "ImageList"
         Appearance      =   1
      End
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog1 
      Left            =   1170
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7530
      Left            =   45
      TabIndex        =   10
      Top             =   855
      Width           =   5190
      _ExtentX        =   9155
      _ExtentY        =   13282
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "Categorias"
      TabPicture(0)   =   "FA00166.frx":0742
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "lvCat"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Conciertos"
      TabPicture(1)   =   "FA00166.frx":075E
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "lvConciertos"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin ComctlLib.ListView lvConciertos 
         Height          =   7035
         Left            =   90
         TabIndex        =   11
         Top             =   400
         Width           =   5010
         _ExtentX        =   8837
         _ExtentY        =   12409
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         Icons           =   "ImageList"
         SmallIcons      =   "ImageList"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Conciertos"
            Object.Width           =   7056
         EndProperty
      End
      Begin ComctlLib.ListView lvCat 
         Height          =   7035
         Left            =   -74910
         TabIndex        =   12
         Top             =   400
         Width           =   5010
         _ExtentX        =   8837
         _ExtentY        =   12409
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         Icons           =   "ImageList"
         SmallIcons      =   "ImageList"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Conciertos"
            Object.Width           =   7056
         EndProperty
      End
   End
   Begin ComctlLib.ImageList ImageList 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   16777215
      UseMaskColor    =   0   'False
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   9
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00166.frx":077A
            Key             =   "NodoCerrado"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00166.frx":0A94
            Key             =   "NodoAbierto"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00166.frx":0DAE
            Key             =   "Hoja"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00166.frx":10C8
            Key             =   "Editando"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00166.frx":13E2
            Key             =   "Borrado"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00166.frx":16FC
            Key             =   "Concierto"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00166.frx":1A16
            Key             =   "Facturable"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00166.frx":1D30
            Key             =   "CatCerrado"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00166.frx":204A
            Key             =   "CatAbierto"
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ImageList imlImageList1 
      Left            =   585
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
End
Attribute VB_Name = "frmPrecios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdBuscar_Click()
   If txtDesig.Text <> "" Then
      CargarCategorias
   End If
End Sub

Private Sub cmdSalir_Click()
   Unload Me
End Sub

Private Sub Form_Load()
   Dim strEntorno1 As String
   Dim strEntorno2 As String

  'QUITAR ESTAS LINEAS CUANDO SE FUNCIONE CON SEGURIDAD
  If App.PrevInstance Then End
  Screen.MousePointer = vbHourglass

  Call InitApp

  With objApp

    Call .Register(App, Screen)

    Set .frmFormMain = Me
    Set .imlImageList = imlImageList1
    Set .crCrystal = Nothing
    Set .dlgCommon = dlgCommonDialog1
    .strUserName = "CUN"
    .strPassword = "qrmpyh"
    .strDataSource = "oracle73"
'    .strPassword = "TUY"
'    .strDataSource = "EST_PROD"
    .intUserEnv = 1
    .blnAskPassword = False
    .objUserColor.lngReadOnly = &HC0C0C0
    .objUserColor.lngMandatory = &HFFFF00
    .blnUseRegistry = True
  End With

  strEntorno1 = "Entorno de Desarrollo"
  strEntorno2 = "Entorno de Explotación"

  With objEnv
    Call .AddEnv(strEntorno1)
    Call .AddValue(strEntorno1, "Main", "FA.")

    Call .AddEnv(strEntorno2)
    Call .AddValue(strEntorno2, "Main", "")

  End With

  If Not objApp.CreateInfo Then
    Screen.MousePointer = vbDefault
    Call ExitApp
  End If


  'entrada = True

  objSecurity.strUser = "PRFAC"
  objSecurity.RegSession

   Call IniciarQRY
   Call CargarConciertos
   
Screen.MousePointer = vbDefault


End Sub

Public Sub CargarConciertos()
   Dim Conc As New ListItem

   Dim rs As rdoResultset
   
   Screen.MousePointer = vbHourglass
   
   lvConciertos.ListItems.Clear
   
   Set rs = qryConc.OpenResultset
   
   While Not rs.EOF
      ' No crear nodos con el nombre nulo (vacio)
      If "" & rs("FA09DESIG") <> "" Then
         Set Conc = lvConciertos.ListItems.Add(, , rs("FA09DESIG"), "Concierto", "Concierto")
         Conc.Tag = rs("FA09CODNODOCONC")
         
      End If
     
      rs.MoveNext
   Wend
   
   rs.Close
   Set rs = Nothing
   
   Screen.MousePointer = vbDefault

End Sub

Public Sub CargarCategorias()
   Dim Cat As New ListItem

   Dim rs As rdoResultset
   
   Screen.MousePointer = vbHourglass
   
   lvCat.ListItems.Clear
   
   qryCateg.rdoParameters(0) = "%" & txtDesig.Text & "%"
   qryCateg.rdoParameters(1) = sdcFecha.FocusDate
   
   
   Set rs = qryCateg.OpenResultset
   
   While Not rs.EOF
      ' No crear nodos con el nombre nulo (vacio)
      If "" & rs("FA05DESIG") <> "" Then
         Set Cat = lvCat.ListItems.Add(, , rs("FA05DESIG"), "CatCerrado", "CatCerrado")
         Cat.Tag = rs("fa05codcateg")
      End If
     
      rs.MoveNext
   Wend
   
   If lvCat.ListItems.Count > 0 Then
      lvCat.ListItems.Item(1).Selected = True
      cmdConsultar.Enabled = True
   Else
      cmdConsultar.Enabled = False
   End If
   
   rs.Close
   Set rs = Nothing
   Screen.MousePointer = vbDefault

End Sub

Private Sub cmdConsultar_Click()
   
   CargarTarifas
End Sub

Private Sub CargarTarifas()
   Dim rs As rdoResultset
   Dim rs2 As rdoResultset
   Dim Conc As ListItem
   Dim raiz As Node
   Dim nodo As Node
   Dim pvp As Node
   Dim nodoFact As String
   Dim ruta As String
   Dim punto As Integer
   
   Screen.MousePointer = vbHourglass
   
   tvTarifas.Nodes.Clear
   
   Set raiz = tvTarifas.Nodes.Add(, , , lvCat.SelectedItem.Text)
   
   
   For Each Conc In lvConciertos.ListItems
      If Conc.Selected Then
         Set nodo = tvTarifas.Nodes.Add(raiz, tvwChild, "." & Conc.Tag & "./", Conc.Text, "Concierto")
         nodo.EnsureVisible
         
         qryTarifa.rdoParameters(0) = Conc.Tag
         qryTarifa.rdoParameters(1) = lvCat.SelectedItem.Tag
         qryTarifa.rdoParameters(2) = sdcFecha.FocusDate
         qryTarifa.rdoParameters(3) = sdcFecha.FocusDate
         
         
         Set rs = qryTarifa.OpenResultset
         Do While Not rs.EOF
            ' buscar la descripcion del nodo facturable
            ruta = rs("FA15RUTA")
            punto = InStr(2, ruta, ".")
            ruta = Right(ruta, Len(ruta) - punto)
            
            punto = InStr(1, ruta, ".")
            nodoFact = Left(ruta, punto - 1)
           
            qryNodoFact.rdoParameters(0) = nodoFact
                        
            Set rs2 = qryNodoFact.OpenResultset
            If Not rs2.EOF Then
               nodoFact = rs2("FA09DESIG")
            End If
            
            ' crear el nodo en el treeview
            Set pvp = tvTarifas.Nodes.Add(nodo, tvwChild, rs("FA15RUTA"), nodoFact & "  (" & rs("FA15PRECIOREF") & "Pts) ", "CatCerrado")
            pvp.EnsureVisible
            rs.MoveNext
         Loop
         
         
      End If
   Next
   Screen.MousePointer = vbDefault
   
End Sub
