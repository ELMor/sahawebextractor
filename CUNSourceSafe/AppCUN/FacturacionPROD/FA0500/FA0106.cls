VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "RNF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public CodCateg As Long
Public CodRNF As String
Public Cantidad As Double
Public Fecha As String
Public Asignado As Boolean
Public Ruta As String
Public Key As String
Public Precio As Double
Public Descripcion As String
Public Desglose As Boolean
Public CodAtrib As Long
Public Name As String
Public Facturado As Boolean
Public CodProceso As String
Public CodAsistenci As String
Public Permanente As Boolean
Public KeyREco As String
Public PeriodoGara As Integer


Private auxCantidad As Double
Private auxPrecio As Double
Private auxDescripcion As String
Private auxName As String
Private auxPermanente As Boolean
Private auxKeyREco As String
Private auxPeriodoGara As Integer

'************************************************************
' IMPORTANTE :
'
' Cuando se a�adan nuevas propiedades hay que tener en cuenta
' que hay que incluirlas en la rutina de copiar los RNF.
'
'      Public Sub Copiar(RNFOrigen As rnf)
'
'************************************************************

' Indice del nodo anterior en la coleccion (Desglose de factura)
Public NodoAnterior As Long
Public NodoFACT As Long

Public Sub Copiar(RNFOrigen As RNF)
    With RNFOrigen
        CodCateg = .CodCateg
        CodRNF = .CodRNF
        Cantidad = .Cantidad
        Fecha = .Fecha
        Asignado = True
        Ruta = ""
        Key = .Key
        Precio = .Precio
        Descripcion = .Descripcion
        Desglose = .Desglose
        CodAtrib = 0
        Name = .Name
        Facturado = .Facturado
        CodProceso = .CodProceso
        CodAsistenci = .CodAsistenci
        Permanente = .Permanente
        KeyREco = .KeyREco
        PeriodoGara = .PeriodoGara
    End With
End Sub

Public Sub Restaurar()
   ' se restauran los atributos originales
   
   Cantidad = auxCantidad
   Precio = auxPrecio
   Descripcion = auxDescripcion
   Name = auxName
   Permanente = auxPermanente
   KeyREco = auxKeyREco
   PeriodoGara = auxPeriodoGara
End Sub

Public Sub Guardar()
   ' se hace una copia de los atributos originales
   
   auxCantidad = Cantidad
   auxPrecio = Precio
   auxDescripcion = Descripcion
   auxName = Name
   auxPermanente = Permanente
   auxKeyREco = KeyREco
   auxPeriodoGara = PeriodoGara
End Sub
