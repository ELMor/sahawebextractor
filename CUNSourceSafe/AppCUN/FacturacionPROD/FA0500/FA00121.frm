VERSION 5.00
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_Impresion 
   Caption         =   "Presentaci�n Preliminar"
   ClientHeight    =   7935
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9135
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7935
   ScaleWidth      =   9135
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.ComboBox cboDocumento 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "FA00121.frx":0000
      Left            =   90
      List            =   "FA00121.frx":0002
      TabIndex        =   16
      Text            =   "cboDocumento"
      Top             =   2280
      Width           =   2565
   End
   Begin VB.PictureBox Picture1 
      Height          =   1770
      Left            =   630
      Picture         =   "FA00121.frx":0004
      ScaleHeight     =   1710
      ScaleWidth      =   1755
      TabIndex        =   15
      Top             =   5085
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton cmdModificar 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Modificar Observ."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1620
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   3510
      Width           =   1095
   End
   Begin VB.CommandButton cmdPrint 
      BackColor       =   &H00C0C0C0&
      Caption         =   "&Imprimir"
      Default         =   -1  'True
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   135
      TabIndex        =   14
      Top             =   2835
      Width           =   1095
   End
   Begin VB.CommandButton cmdPagAtras 
      BackColor       =   &H00C0C0C0&
      Caption         =   "P�gina &anterior"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   135
      TabIndex        =   13
      Top             =   4230
      Width           =   1095
   End
   Begin VB.CommandButton cmdGrabar 
      BackColor       =   &H00C0C0C0&
      Caption         =   "&Grabar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1620
      TabIndex        =   12
      Top             =   2835
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdGestCobros 
      BackColor       =   &H00C0C0C0&
      Caption         =   " Gesti�n  de cobros"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1575
      TabIndex        =   11
      Top             =   7020
      Width           =   1095
   End
   Begin VB.CommandButton cmdSalir 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Sa&lir"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   90
      TabIndex        =   10
      Top             =   7020
      Width           =   1095
   End
   Begin VB.CommandButton cmdPagAdelante 
      BackColor       =   &H00C0C0C0&
      Caption         =   "P�gina &siguiente"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1620
      TabIndex        =   9
      Top             =   4230
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   2250
      TabIndex        =   7
      Top             =   45
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.ComboBox cmbZoom 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   75
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   1650
      Width           =   1080
   End
   Begin VB.ComboBox cmbOrientation 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   90
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1020
      Width           =   1275
   End
   Begin VB.ComboBox cmb_printers 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   90
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   345
      Width           =   2745
   End
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   7800
      Left            =   2880
      TabIndex        =   0
      Top             =   90
      Visible         =   0   'False
      Width           =   6225
      _Version        =   196608
      _ExtentX        =   10980
      _ExtentY        =   13758
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      PreviewMode     =   1
   End
   Begin VB.TextBox txtCodDocum 
      Height          =   285
      Left            =   90
      TabIndex        =   18
      Text            =   "Text1"
      Top             =   2295
      Width           =   375
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Tipo de documento:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   3
      Left            =   75
      TabIndex        =   17
      Top             =   2025
      Width           =   1710
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Zoom:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   1
      Left            =   105
      TabIndex        =   6
      Top             =   1395
      Width           =   555
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Orientaci�n de la p�gina:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   2
      Left            =   135
      TabIndex        =   5
      Top             =   765
      Width           =   2160
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Impresora a utilizar:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Index           =   0
      Left            =   135
      TabIndex        =   2
      Top             =   90
      Width           =   1935
   End
End
Attribute VB_Name = "frm_Impresion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim MyPage%         'Keep the output view to be printed
Dim OldOrientation  'Don't mess with my printer settings



Const ConvX = 54.0809
Const ConvY = 54.6101086

Private Type LineaImpre
  Linea As String
  Cantidad As Double
End Type

'Variables que utilizaremos para gestionar pagina adelante y atr�s
Dim NumPaginas As Integer
Dim PagActual As Integer
'Dim DescConcierto As String
Private NumPag As Integer
Dim Pagina As Integer

Dim qry As New rdoQuery

Public ImpresionMasiva As Boolean
Public Impresora As String

Private Type HASS
  Tipo As String
  Historia As String
  Nombre As String
  SegSocial As String
  Afiliado As String
  Ingreso As String
  Departamento As String
  Direccion As String
  Poblacion As String
  Alta As String
  Hora As String
  Cama As String
  Fecha As String
  HistAsist As String
  Prov As String
End Type
Dim Datos() As HASS

Private Type LinFactura
  Cantidad As Double
  Descripcion As String
  ImpUnit As Double
  ImpTotal As Double
End Type

Dim Lineas() As LinFactura

Dim qryJASS(1 To 12) As New rdoQuery

Function CalcularUltimoDia(Mes As Double, ANYO As Integer) As Date
Dim Fecha As Date

  Fecha = CVDate("31/01/" & ANYO)
  Fecha = DateAdd("m", Mes - 1, Fecha)
  CalcularUltimoDia = Fecha
  
End Function

Function CalcularPrimerDia(Mes As Double, ANYO As Integer) As Date
Dim Fecha As Date

  Fecha = CVDate("01/" & Mes & "/" & ANYO)
  CalcularPrimerDia = Fecha
  
End Function


Sub ImprimirHASSFactura(NumProc As Long, NumAsist As Long, Mes As Double, ANYO As Integer)
Dim Fecha As Date
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim CodFactura As Long

  Fecha = CVDate("01/" & Mes & "/" & ANYO)
  'Seleccionamos la factura que debemos imprimir
  MiSqL = "Select FA04CODFACT From FA0400 Where AD01CODASISTENCI = " & NumAsist & " " & _
          "And AD07CODPROCESO = " & NumProc & " AND FA04FECFACTURA = TO_DATE('" & Fecha & "','DD/MM/YYYY')"
  
  MiSqL = "Select fa0400.*, ad0700.ci22numhistoria " & _
          "From FA0400, ad0700 " & _
          "Where AD01CODASISTENCI = " & NumAsist & " " & _
          "  AND FA0400.AD07CODPROCESO = " & NumProc & _
          "  AND FA04FECFACTURA = TO_DATE('" & Fecha & "','DD/MM/YYYY')" & _
          "  AND FA0400.AD07CODPROCESO = AD0700.AD07CODPROCESO"
  
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    If Not IsNull(MiRs("FA04CODFACT")) Then
      CodFactura = MiRs("FA04CODFACT")
    Else
      CodFactura = 0
    End If
  End If
  Call Me.IniciarQRYJASS
  If CodFactura <> 0 Then
    vsPrinter.Preview = False
    vsPrinter.Action = 3
    Call Me.IniciarQRYJASS
    Call ObtenerDatosJASS(NumProc, NumAsist, CodFactura, MiRs("FA04NUMFACT"), MiRs("CI22NUMHISTORIA"))
    Call ImprimirImpresoJASS
    Call ImprimirDatosJASS(Mes, ANYO)
    Call ImprimirLineasJASS
    vsPrinter.Action = paEndDoc
  End If
  
End Sub


Sub CalcularNumeroPaginas(factura As Integer)
    Dim s$, fmt$
    Dim x, Y
    Dim LineaFact As String
    Dim ImporteFact As Double
    Dim ArrayFactura()  As LineaImpre
    Dim ContLineas
    Dim PosicionX As Integer
    Dim PosicionY As Integer
    Dim PosBlanco As Long
    Dim PosComa As Long
    Dim Cadena As String
    Dim Texto As String
    Dim MaxLin As Integer
    Dim MaxRNF As Integer
    Dim Lineas As Integer
    Dim Limite As Integer
    Dim PosObserv As Integer
    
    
    'Inicializo el contador de las p�ginas que componen la factura
    Pagina = 1
        
    With vsPrinter
        .CurrentY = 5434
        .MarginRight = 12 * ConvX
        .FontName = "Arial"
        .FontSize = 10
        .MarginLeft = 2000
        
        ' esto es para recoger el numero de lineas de factura contenidas
        ' en ArrayLineas, pero controlando el error que da cuando no tiene
        ' ninguna
        MaxLin = 0
        On Error Resume Next
        MaxLin = UBound(ArrayLineas)
        On Error GoTo 0
        
        For x = 1 To MaxLin
            If ArrayLineas(x).mostrar Then
                If ArrayLineas(x).NFactura = factura Then
                    .CurrentX = 2000
                    .TextAlign = taLeftBaseline
                    Cadena = UCase(ArrayLineas(x).Descripcion) & ": "
                    PosicionX = .CurrentX
                    While Cadena <> ""
                        PosBlanco = InStr(1, Cadena, " ")
                        If PosBlanco <> 0 Then
                            Texto = Left(Cadena, PosBlanco)
                            Cadena = Right(Cadena, Len(Cadena) - PosBlanco)
                            If PosicionX + .TextWidth(Texto) <= 9085 Then
                                PosicionX = PosicionX + .TextWidth(Texto)
                            Else
                                .CurrentX = 2000
                                .CurrentY = .CurrentY + 200
                                If vsPrinter.CurrentY > 220 * ConvY Then
                                    Pagina = Pagina + 1
                                    .CurrentY = 5434
                                End If
                                PosicionX = PosicionX + .TextWidth(Texto)
                            End If
                        End If
                    Wend
             
                    ' esto es para recoger el numero de RNFs contenidos
                    ' en arrayRNFs, pero controlando el error que da cuando no tiene
                    ' ninguna
                    MaxRNF = 0
                    On Error Resume Next
                    MaxRNF = UBound(arrayRNFs)
                    On Error GoTo 0
                
                    'PosicionX = .CurrentX
                    For Y = 1 To MaxRNF
                        If arrayRNFs(Y).NFactura = factura And arrayRNFs(Y).NLinea = ArrayLineas(x).NLinea And _
                            arrayRNFs(Y).Desglose = True And arrayRNFs(Y).Cantidad <> 0 Then
                            'A�adimos la descripci�n de los distintos rnfs que corresponden a la l�nea
                            'Eliminaremos de la cadena la parte que se queda a la derecha de la coma en el caso de que esta exista
                            Cadena = arrayRNFs(Y).Descripcion
                            PosComa = InStr(1, Cadena, ",")
                            If PosComa <> 0 Then
                                Cadena = Left(Cadena, PosComa - 1)
                            End If
                            If arrayRNFs(Y).Cantidad <> 1 And UCase(ArrayLineas(x).Descripcion) <> "MEDICACI�N" Then
                                Cadena = Cadena & " (" & arrayRNFs(Y).Cantidad & ")"
                            End If
                            Cadena = Cadena & ","
                            Cadena = Cadena & " "
                            While Cadena <> ""
                                PosBlanco = InStr(1, Cadena, " ")
                                If PosBlanco <> 0 Then
                                    Texto = Left(Cadena, PosBlanco)
                                    Cadena = Right(Cadena, Len(Cadena) - PosBlanco)
                                    If PosicionX + .TextWidth(Texto) <= 9085 Then
                                        PosicionX = PosicionX + .TextWidth(Texto)
                                    Else
                                        .CurrentX = 2000
                                        PosicionX = .CurrentX
                                        .CurrentY = .CurrentY + 200
                                        If vsPrinter.CurrentY > 220 * ConvY Then
                                            Pagina = Pagina + 1
                                            .CurrentY = 5434
                                        End If
                                        PosicionX = PosicionX + .TextWidth(Texto)
                                    End If
                                End If
                            Wend
                            'a�adimos los rnfs
                        Else
                            ' El RNF no se imprime
                        End If
                    Next
          
                    PosicionY = .CurrentY
                    '.CurrentX = 217 * ConvX
                    .CurrentY = .CurrentY - 200
                    .TextAlign = taRightTop
                    .Text = FormatearMoneda(ArrayLineas(x).Importe, tiImporteLinea)
                    .TextAlign = taLeftBaseline
                    .CurrentY = .CurrentY + 200
                    .CurrentX = PosicionX
                    While .CurrentX <= 9085
                        .Text = "."
                    Wend
                    .CurrentX = 2000
                    .CurrentY = .CurrentY + 400
                    If vsPrinter.CurrentY > 220 * ConvY Then
                        Pagina = Pagina + 1
                        Call NuevaPagina(factura)
                        .MarginRight = 12 * ConvX
                        .FontSize = 10
                    End If
                End If
            Else
                ' la linea no es imprimible por que sus RNFs tienen cantidad 0
            End If
        Next
                
 
        '.Paragraph = ""
        .SpaceAfter = 100
        .TextAlign = 0
   
        '-------------------------------------------------------
        ' Impresi�n desde la base de datos.
        '-------------------------------------------------------
        '-------------------------------------------------------
        ' Imprimimos el total.
        '-------------------------------------------------------
        x = .CurrentY
        x = x - 150
        'Imprimimos la l�nea que simboliza el final de la suma
        .DrawLine 170 * ConvX, x, 200 * ConvX, x
        .MarginRight = 12 * ConvX
        .CurrentX = 42 * ConvX
        .Text = " EXENTO DE  I.V.A."
        .CurrentX = 130 * ConvX
        .Text = "TOTAL PESETAS"
'        .TextAlign = taRightTop
'        .CurrentX = 217 * ConvX
'        '.CurrentY = .CurrentY + 400
'        .CurrentY = .CurrentY - 200
'        .MarginLeft = 1500
'        .Paragraph = Format(ArrayCabecera(factura).TotFactura, "###,###,##0")
        PosicionY = .CurrentY
        .TextAlign = taRightTop
        .Text = FormatearMoneda(ArrayCabecera(factura).TotFactura, tiTotalFact)
        .CurrentY = .CurrentY + 500
        x = .CurrentY
        x = x - 200
        'Imprimimos la doble l�nea que separa el total ptas de total euros
        .DrawLine 130 * ConvX, x, 200 * ConvX, x
        .DrawLine 130 * ConvX, x + 80, 200 * ConvX, x + 80
        .CurrentX = 203 * ConvX
        .TextAlign = taLeftBaseline
        .CurrentY = .CurrentY + 200
        .CurrentX = 130 * ConvX
        .Text = "TOTAL EUROS"
        .TextAlign = taRightBaseline
        .Text = Format(ArrayCabecera(factura).TotFactura / 166.386, "###,##0.00")
        .TextAlign = taLeftBaseline
        .FontBold = False
        .CurrentX = 16 * ConvX
        .CurrentY = PosObserv * ConvY
        .MarginLeft = 16 * ConvX
        .Paragraph = UCase(ArrayCabecera(factura).Observac)
    End With


End Sub

Sub EncabezadoPagina(factura As Integer)

Dim MiSqL As String
Dim rsBuscar As rdoResultset
Dim Texto As String
Dim NoPagina As Integer
Dim PosicionY As Long

'    MiSql = "Select FA09OBSFAC From FA0900 Where FA09CODNODOCONC = " & ArrayCabecera(factura).codConcierto
'    Set rsBuscar = objApp.rdoConnect.OpenResultset(MiSql, 3)
'    If Not rsBuscar.EOF Then
'      DescConcierto = rsBuscar("FA09OBSFAC") & ""
'    Else
'      DescConcierto = ""
'    End If
    With vsPrinter
        '.TextColor = QBColor(0)
        '------------------------------------------------------------------------
        ' REALIZAMOS LA IMPRESI�N DE LA CABECERA Y EL LATERAL
        '------------------------------------------------------------------------
        'Imprimimos el escudo de la cl�nica.
        .X1 = 35 * ConvX
        .Y1 = 5 * ConvY
        .X2 = 59 * ConvX
        .Y2 = 35 * ConvY
        .Picture = Me.Picture1.Picture
                
        'Imprimimos el texto de debajo del escudo CLINICA...
        .FontName = "Arial"
        .CurrentX = 0 * ConvX
        .CurrentY = 35 * ConvY
        .MarginLeft = 0
        'Le asignamos un gran margen derecho y le decimos que lo centre
        .MarginRight = 120 * ConvX
        .FontSize = 14
        .TextAlign = taCenterBaseline
        .FontName = "Times New Roman"
        .Paragraph = "CLINICA UNIVERSITARIA"
        .SpaceAfter = 2 * ConvX
        .FontSize = 8
        .FontBold = False
        .CurrentY = 40 * ConvY
        .Paragraph = "FACULTAD DE MEDICINA"
        .CurrentY = 43 * ConvY
        .Paragraph = "UNIVERSIDAD DE NAVARRA"
        .DrawLine 39 * ConvX, 45 * ConvY, 49 * ConvX, 45 * ConvY
        .CurrentY = 49 * ConvY
        .FontName = "Arial"
        .FontBold = True
        .Paragraph = "ADMINISTRACION"
        .FontBold = False
        .TextAlign = taLeftBaseline
        .MarginRight = 0
        
        'Imprimimos la direcci�n y los tel�fonos (Parte derecha del encabezado)
        .CurrentX = 140 * ConvX
        .FontSize = 8
        .CurrentY = 20 * ConvY
        .Paragraph = "Avda. P�o XII, 36"
        .CurrentX = 140 * ConvX
        .CurrentY = 23 * ConvY
        .Paragraph = "Apartado, 4209"
        .CurrentX = 140 * ConvX
        .CurrentY = 29 * ConvY
        .Paragraph = "Tel�fonos:"
        .CurrentX = 140 * ConvX
        .CurrentY = 32 * ConvY
        .Paragraph = "Centralita 948.25.54.00"
        .CurrentX = 140 * ConvX
        .CurrentY = 35 * ConvY
        .Paragraph = "Administraci�n 948.29.63.94"
        .CurrentX = 140 * ConvX
        .CurrentY = 41 * ConvY
        .Paragraph = "Fax: 948.29.65.00"
        .CurrentX = 140 * ConvX
        .CurrentY = 44 * ConvY
        .Paragraph = "C.I.F.: Q.3168001 J"
        .CurrentX = 140 * ConvX
        .CurrentY = 47 * ConvY
        .Paragraph = "31080 PAMPLONA"
        .CurrentX = 140 * ConvX
        .CurrentY = 50 * ConvY
        .Paragraph = "(ESPA�A)"
        
        'Imprimimos el lateral de la p�gina
        .FontName = "Times New Roman"
        .TextAngle = 900 'Escribimos en vertical.
        .FontSize = 10
        .CurrentY = 180 * ConvY
        .CurrentX = 5 * ConvX
        .Paragraph = "Al abonar este importe rogamos haga referencia al n�mero de factura."
        .CurrentY = 180 * ConvY
        .CurrentX = 8 * ConvX + 30
        .Text = "Cuentas corrientes:"
        .FontBold = True
        .CurrentY = 152 * ConvY
        .CurrentX = 8 * ConvX + 30
        .Text = "BSCH N� 0049.2729.54.2214255346"
        .CurrentY = 152 * ConvY
        .CurrentX = 11 * ConvX + 60
        .Text = "BBVA N� 0182.5912.71.0010288974"
        .CurrentY = 167 * ConvY
        .CurrentX = 14.3 * ConvX + 60
        .Text = "Banco Popular Espa�ol N� 0075.0018.78.0601380478"
        .BrushStyle = 1
        .DrawRectangle 2 * ConvX, 57 * 70.6101086, 17 * ConvX, 187 * ConvY 'Lo recuadramos.
        '.DrawRectangle 2 * ConvX, 57 * 70.6101086, 13 * ConvX, 187 * ConvY 'Lo recuadramos.
        .FontBold = False
        .TextAngle = 0 'Anulamos la escritura vertical.
        
        'Imprimimos los datos del paciente y de la atenci�n.
        .CurrentY = 60 * ConvY
        .CurrentX = 2000
        .MarginLeft = 2000
        .MarginRight = 10 * ConvX
        .FontSize = 10
        '.FontBold = True
        .FontName = "Arial"
        ' Datos del Responsable Economico
        .Paragraph = UCase(IIf(ArrayCabecera(factura).reco.Tratamiento <> "", ArrayCabecera(factura).reco.Tratamiento & " ", "") & ArrayCabecera(factura).reco.Name)
        .CurrentY = .CurrentY - 1 * ConvY
        .Paragraph = "NIF: " & UCase(ArrayCabecera(factura).reco.DNI)
        .CurrentY = .CurrentY - 1 * ConvY
        ' datos del paciente
        If ArrayCabecera(factura).reco.Codigo <> ArrayCabecera(factura).Paciente.Codigo Then
            .Paragraph = UCase(ArrayCabecera(factura).Paciente.Tratamiento & " " & ArrayCabecera(factura).Paciente.Name)
        Else
            .Paragraph = ""
        End If
        .CurrentY = .CurrentY - 1 * ConvY
        If ArrayCabecera(factura).reco.Codigo = "800563" _
            Or ArrayCabecera(factura).reco.Codigo = "800564" _
            Or ArrayCabecera(factura).reco.Codigo = "800565" Then
          .Paragraph = "N SOE: " & PartirNumSegSoc(ArrayCabecera(factura).Paciente.NumSegSoc)
        End If
       
       .Paragraph = ""
       .Paragraph = ""
       ' .FontBold = False
         
        Texto = ArrayCabecera(factura).Descrip
        .Paragraph = Texto
        .Paragraph = ""
        PosicionY = .CurrentY
        .CurrentY = 60 * ConvY
        .TextAlign = taRightBaseline
'        .Text = objSecurity.strUser & " " & 1
        NumPag = NumPag + 1
        .Text = ArrayCabecera(factura).SG02COD_ADD & "  " & NumPag
        .TextAlign = taLeftBaseline
        .CurrentY = PosicionY
      End With
      
End Sub


Sub NuevaPagina(factura As Integer)
  Call PiePagina(factura)
  vsPrinter.NewPage
  Call EncabezadoPagina(factura)
  vsPrinter.MarginRight = 12 * ConvX
  vsPrinter.FontName = "Arial"
End Sub

Private Function PartirNumSegSoc(Numero As String) As String
Dim Cadena As String

  If Len(Numero) = 10 Then
    Cadena = Left(Numero, 2) & "/" & Mid(Numero, 3, 8)
  ElseIf Len(Numero) = 12 Then
    Cadena = Left(Numero, 2) & "/" & Mid(Numero, 3, 8) & "/" & Right(Cadena, 2)
  End If
  PartirNumSegSoc = Cadena
  
End Function

Sub PiePagina(factura As Integer)
    
    With vsPrinter
        '.TextColor = QBColor(0)
        '--------------------------------------------------------
        ' IMPRIMIMOS LOS DATOS REFERENTES AL PIE
        '--------------------------------------------------------
        'Primera rejilla
        
        .FontSize = 10
        .CurrentX = 19 * ConvX
        .CurrentY = 250 * ConvY
        .Text = ArrayCabecera(factura).Paciente.NumHistoria & "-" & ArrayCabecera(factura).NumCaso & "      " & ArrayCabecera(factura).reco.Codigo
        'N�mero de factura
        .CurrentX = 36 * ConvX
        .CurrentY = 263 * ConvY
        .Text = IIf(ArrayCabecera(factura).NFactura = ArrayCabecera(factura).NumFactProvisional, "Provisional", ArrayCabecera(factura).NFactura)
        'Habitaci�n
        .CurrentX = 78 * ConvX
        .CurrentY = 270 * ConvY
        .Text = ArrayCabecera(factura).Habitacion
        'Fecha
        .CurrentX = 36 * ConvX
        .CurrentY = 275 * ConvY
        .Text = Format(ArrayCabecera(factura).Fecha, "dd/mm/yyyy")
        
        'Datos del Paciente.
        .MarginLeft = 100 * ConvX
        .CurrentX = 100 * ConvX
        .CurrentY = 247 * ConvY
        .Paragraph = UCase(ArrayCabecera(factura).reco.Tratamiento)
        '.CurrentX = 100 * ConvX
        .Paragraph = UCase(ArrayCabecera(factura).reco.Name)
        '.CurrentX = 100 * ConvX
        .Paragraph = UCase(ArrayCabecera(factura).reco.Direccion)
        '.CurrentX = .CurrentX + 100
        .FontUnderline = True
        '.CurrentX = 100 * ConvX
        .Paragraph = UCase(ArrayCabecera(factura).reco.CPPoblac)
        
        .FontUnderline = False
        '.CurrentX = 100 * ConvX
        .Paragraph = UCase(ArrayCabecera(factura).reco.Pais)
        
        
        .FontBold = False
        .FontUnderline = False
        'Imprimimos la rejilla que contiene los datos
        .BrushStyle = 1
        .FontSize = 8
        .CurrentX = 17 * ConvX
        .CurrentY = 256 * ConvY
        .Text = "N� FACTURA"
        .CurrentX = 17 * ConvX
        .CurrentY = 268 * ConvY
        .Text = "FECHA"
        'Definimos las l�neas
        .DrawLine 92 * ConvX, 241 * ConvY, 92 * ConvX, 277 * ConvY
        .DrawLine 16 * ConvX, 253 * ConvY, 92 * ConvX, 253 * ConvY
        .DrawLine 16 * ConvX, 265 * ConvY, 92 * ConvX, 265 * ConvY
        .DrawRectangle 16 * ConvX, 241 * ConvY, 196 * ConvX, 277 * ConvY, 200, 200
        .CurrentX = 16 * ConvX
        .CurrentY = 291 * ConvY
        .MarginLeft = 16 * ConvX
'        .Paragraph = UCase(DescConcierto)
         ' Ahora la descripcion del concierto se recoge de la ci1300
        .Paragraph = UCase(ArrayCabecera(factura).DescConcierto)
    End With
End Sub

Function RecogerFechasQuimio(factura As Long, Linea As Long, Contador As Integer) As String
Dim ArrayFechas() As String
Dim rs03 As rdoResultset
Dim cont As Integer
Dim Encontrado As Boolean
Dim x As Integer
Dim Cadena As String

  ReDim ArrayFechas(1 To 1)
  'Recogemos todos los rnfs de esa linea
  qryJASS(7).rdoParameters(0) = factura
  qryJASS(7).rdoParameters(1) = Linea
  Set rs03 = qryJASS(7).OpenResultset
  If Not rs03.EOF Then
    cont = 0
    While Not rs03.EOF
      Encontrado = False
      For x = 1 To UBound(ArrayFechas)
        If Format(rs03("FA03FECHA"), "DD") = ArrayFechas(x) Then
          Encontrado = True
        End If
      Next
      If Encontrado = False Then
        cont = cont + 1
        ReDim Preserve ArrayFechas(1 To cont)
        ArrayFechas(cont) = Format(rs03("FA03FECHA"), "DD")
      End If
      rs03.MoveNext
    Wend
  End If
  If cont > 0 Then
    For x = 1 To cont
      Cadena = Cadena & ArrayFechas(x) & ","
    Next
    Cadena = Left(Cadena, Len(Cadena) - 1)
  Else
    Cadena = ""
  End If
  Lineas(Contador).Cantidad = cont
  RecogerFechasQuimio = Cadena
End Function

Private Sub cboDocumento_Click()
Dim SQL As String
Dim RS As rdoResultset
Dim x As Integer
Dim Impreso As Integer

  If Trim(Me.cboDocumento.Text) <> "" Then
    SQL = "Select FA57NUMIMPRESO From FA5700 Where FA57DESCORTA = '" & Me.cboDocumento & "'"
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      RS.MoveLast
      RS.MoveFirst
      While Not RS.EOF
        Me.txtCodDocum.Text = RS("FA57NUMIMPRESO") & ""
        RS.MoveNext
      Wend
    End If
  End If
  vsPrinter.KillDoc
  vsPrinter.Action = paStartDoc
  'MousePointer = 12
  
  'On Error GoTo error
  For x = 1 To UBound(ArrayCabecera)
    ArrayCabecera(x).numPagInicial = vsPrinter.PageCount
    NumPag = 0
    'Call CalcularNumeroPaginas(x)
    Impreso = Me.txtCodDocum.Text
    If Impreso = 1 Then
      Call EncabezadoPagina(x)
      Call LineasFactura(x)
      Call PiePagina(x)
    ElseIf Impreso = 2 Then
      Call Me.IniciarQRYJASS
      Call ObtenerDatosJASS(ArrayCabecera(x).Proceso, ArrayCabecera(x).NumCaso, ArrayCabecera(x).CodFact, ArrayCabecera(x).NFactura, ArrayCabecera(x).Paciente.NumHistoria)
      Call ImprimirImpresoJASS
      Call ImprimirDatosJASS(1, 2000)
      Call ImprimirLineasJASS
    ElseIf Impreso = 3 Then
      Call EncabezadoPagina(x)
      Call LineasFacturaMutuas(x)
      Call PiePagina(x)
    End If
    If x <> UBound(ArrayCabecera) Then
      vsPrinter.NewPage
    End If
    
  Next
   
  vsPrinter.Action = 6 'se visualizan todas las factura
  MousePointer = 0
  
  NumPaginas = vsPrinter.PageCount
  If NumPaginas = 1 Then
    Me.cmdPagAdelante.Enabled = False
    Me.cmdPagAtras.Enabled = False
  ElseIf NumPaginas > 1 Then
    Me.cmdPagAdelante.Enabled = True
    Me.cmdPagAtras.Enabled = False
  Else
    Me.cmdPagAdelante.Enabled = False
    Me.cmdPagAtras.Enabled = False
  End If
  PagActual = 1
  
  
End Sub


Private Sub cboDocumento_DropDown()
Dim SQL As String
Dim RS As rdoResultset

  Me.cboDocumento.Clear
  
  SQL = "Select FA57DESCORTA From FA5700"
  Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
  If Not RS.EOF Then
    RS.MoveLast
    RS.MoveFirst
    While Not RS.EOF
      Me.cboDocumento.AddItem RS("FA57DESCORTA") & ""
      RS.MoveNext
    Wend
  End If

End Sub


Private Sub cboDocumento_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub cmb_printers_Click()
        
        Dim s$
        
        vsPrinter.Device = cmb_printers.List(cmb_printers.ListIndex)
        
        '------------------------------------------------------
        ' Show selected printer attributes
        '------------------------------------------------------
        s = "DPI: " & Str$(vsPrinter.DPI) & Chr(13)
        s = s & "Port: :" & vsPrinter.Port & Chr(13)
        s = s & "Driver: :" & vsPrinter.Driver

       ' lStatus = s

End Sub


Private Sub cmbOrientation_Click()
  
  MousePointer = 11
  
  vsPrinter.Orientation = cmbOrientation.ListIndex
  cmbZoom_click
  If MyPage >= 0 Then Command1_Click

  MousePointer = 0

End Sub

Private Sub cmbZoom_click()
  
  vsPrinter.Visible = False
  MousePointer = 11
    
  'Change the screen size to zoom value
  vsPrinter.Zoom = Val(cmbZoom)
  
  MousePointer = 0
  vsPrinter.Visible = True

End Sub

Private Sub cmdGestCobros_Click()
Dim x As Long

  'Unload Me
  
   
  Call objSecurity.LaunchProcess("FA0121", ArrayCabecera(FacturaVisible(vsPrinter.PreviewPage)).reco.Codigo)
  
'  For x = 1 To UBound(ArrayCabecera)
'    frm_Cobros.pGestion (ArrayCabecera(x).reco.Codigo)
'    Set frm_Cobros = Nothing
'  Next
    
End Sub

Private Sub cmdGrabar_Click()
    ArrayCabecera(1).Grabar = True
    Unload Me
End Sub

Private Sub cmdModificar_Click()
   Dim nFactVisible As Integer
   Dim obs As String
   Dim nPagina As Integer
   
   nPagina = vsPrinter.PreviewPage
   nFactVisible = FacturaVisible(nPagina)
   
   obs = ArrayCabecera(nFactVisible).Observac
   Call frm_Observaciones.pObservaciones(ArrayCabecera(nFactVisible).NFactura, obs)
   
   If obs <> "-1" Then  ' se ha modificado
      ArrayCabecera(FacturaVisible(nPagina)).Observac = obs
      Command1_Click
      vsPrinter.PreviewPage = nPagina
      PagActual = nPagina - 1
      cmdPagAdelante_Click
   End If

End Sub

Private Sub cmdPagAdelante_Click()
  PagActual = PagActual + 1
  vsPrinter.PreviewPage = PagActual
  If PagActual = NumPaginas Then
    cmdPagAdelante.Enabled = False
  Else
    cmdPagAdelante.Enabled = True
  End If
  If PagActual = 1 Then
    cmdPagAtras.Enabled = False
  Else
    cmdPagAtras.Enabled = True
  End If
End Sub

Private Sub cmdPagAtras_Click()
  PagActual = PagActual - 1
  vsPrinter.PreviewPage = PagActual
  If PagActual = NumPaginas Then
    cmdPagAdelante.Enabled = False
  Else
    cmdPagAdelante.Enabled = True
  End If
  If PagActual = 1 Then
    cmdPagAtras.Enabled = False
  Else
    cmdPagAtras.Enabled = True
  End If
End Sub


Private Sub cmdPrint_Click()
Dim x As Integer
Dim Impreso As Integer

  ' get ready to print
  cmdPrint.Enabled = False
  MousePointer = 11
  
  ' print to the printer
  vsPrinter.Preview = False
  vsPrinter.Action = 3 ' StartDoc
  If vsPrinter.error Then Beep: Exit Sub
  
  MousePointer = 12
  
  On Error GoTo error
    
  For x = 1 To UBound(ArrayCabecera)
   ' asignar el numero de factura inicial
    ArrayCabecera(x).numPagInicial = vsPrinter.PageCount
    NumPag = 0
    'Call CalcularNumeroPaginas(x)

    Impreso = Me.txtCodDocum.Text
    If Impreso = 1 Then
      Call EncabezadoPagina(x)
      Call LineasFactura(x)
      Call PiePagina(x)
    ElseIf Impreso = 2 Then
      Call Me.IniciarQRYJASS
      Call ObtenerDatosJASS(ArrayCabecera(x).Proceso, ArrayCabecera(x).NumCaso, ArrayCabecera(x).CodFact, ArrayCabecera(x).NFactura, ArrayCabecera(x).Paciente.NumHistoria)
      Call ImprimirImpresoJASS
      Call ImprimirDatosJASS(Format(ArrayCabecera(x).Fecha, "MM"), Format(ArrayCabecera(x).Fecha, "YYYY"))
      Call ImprimirLineasJASS
    ElseIf Impreso = 3 Then
      Call EncabezadoPagina(x)
      Call LineasFacturaMutuas(x)
      Call PiePagina(x)
    End If
    If x <> UBound(ArrayCabecera) Then
      vsPrinter.NewPage
    End If
        
  Next
  vsPrinter.Action = 6 'se visualizan todas las factura
  'vsPrinter.SaveDoc "c:\factura\" & ArrayCabecera(1).NumCaso & ".txt"
  MousePointer = 0
  
  NumPaginas = vsPrinter.PageCount
  If NumPaginas = 1 Then
    Me.cmdPagAdelante.Enabled = False
    Me.cmdPagAtras.Enabled = False
  ElseIf NumPaginas > 1 Then
    Me.cmdPagAdelante.Enabled = True
    Me.cmdPagAtras.Enabled = False
  Else
    Me.cmdPagAdelante.Enabled = False
    Me.cmdPagAtras.Enabled = False
  End If
  PagActual = 1
  
  vsPrinter.Preview = True

  ' all done
  cmdPrint.Enabled = True
  MousePointer = 0

Exit Sub
error:
    
End Sub

Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub Command1_Click()
Dim x As Integer
Dim Impreso As Integer
Dim MiSqL
Dim MiRs As rdoResultset
Dim SqlAsist As String
Dim RsAsis As rdoResultset
Dim TipoAsist As Integer

  ' remember page for use with Print command
  ' MyPage = Index%
  ' print pictures, stretch and align anywhere on the page
  ' we have a print job, so let's enable these guys
  cmbZoom.Enabled = True
  cmdPrint.Enabled = True
  cmbOrientation.Enabled = True

  ' start the print preview job
   If Not ImpresionMasiva Then
      vsPrinter.Action = 3 ' StartDoc
      If vsPrinter.error Then Beep: Exit Sub
   Else
      Call IniciarQRY
      If Impresora <> "" Then
         vsPrinter.Device = Impresora
      End If
   End If
  MousePointer = 12
  
    On Error GoTo error
    
  For x = 1 To UBound(ArrayCabecera)
      If ImpresionMasiva Then
         vsPrinter.Action = 3 ' StartDoc
         If vsPrinter.error Then Beep: Exit Sub
         ' habria que marcar la factura como impresa
         qry.rdoParameters(0) = 1
         qry.rdoParameters(1) = ArrayCabecera(x).NFactura
         qry.Execute

      End If
      ' asignar el numero de factura inicial
      ArrayCabecera(x).numPagInicial = vsPrinter.PageCount
    NumPag = 0
    'Call CalcularNumeroPaginas(x)
      
    'Seleccionamos el tipo de asistencia
    SqlAsist = "SELECT AD12CODTIPOASIST From AD1100, AD2500 " & _
              "Where AD1100.AD01CODASISTENCI =  " & ArrayCabecera(x).NumCaso & _
              " AND AD1100.AD01CODASISTENCI = AD2500.AD01CODASISTENCI " & _
              "AND AD2500.AD25FECFIN IS NULL"
    Set RsAsis = objApp.rdoConnect.OpenResultset(SqlAsist, 3)
    If Not RsAsis.EOF Then
      If Not IsNull(RsAsis("AD12CODTIPOASIST")) Then
        TipoAsist = RsAsis("AD12CODTIPOASIST")
      Else
        TipoAsist = 1
      End If
    Else
      TipoAsist = 1
    End If
    'Comprobamos cual es el impreso que debemos utilizar para la impresi�n.
    MiSqL = "SELECT FA57NUMIMPRESO From FA5601J " & _
            "Where CI13CODENTIDAD =  '" & ArrayCabecera(x).CodEnt & "' " & _
            "AND CI32CODTIPECON = '" & ArrayCabecera(x).TipEcon & "' " & _
            "AND AD12CODTIPOASIST = " & TipoAsist
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      Impreso = MiRs("FA57NUMIMPRESO")
      Me.txtCodDocum.Text = MiRs("FA57NUMIMPRESO")
    Else
      Impreso = 1
      Me.txtCodDocum.Text = 1
    End If
    If Me.txtCodDocum.Text <> "" Then
      Call txtCodDocum_LostFocus
    End If
    If Impreso = 1 Then
      Call EncabezadoPagina(x)
      Call LineasFactura(x)
      Call PiePagina(x)
    ElseIf Impreso = 2 Then
      Call Me.IniciarQRYJASS
      Call ObtenerDatosJASS(ArrayCabecera(x).Proceso, ArrayCabecera(x).NumCaso, ArrayCabecera(x).CodFact, ArrayCabecera(x).NFactura, ArrayCabecera(x).Paciente.NumHistoria)
      Call ImprimirImpresoJASS
      Call ImprimirDatosJASS(CDbl(Format(ArrayCabecera(x).Fecha, "mm")), CInt(Format(ArrayCabecera(x).Fecha, "yyyy")))
      Call ImprimirLineasJASS
    ElseIf Impreso = 3 Then
      Call EncabezadoPagina(x)
      Call LineasFacturaMutuas(x)
      Call PiePagina(x)
    End If
    If ImpresionMasiva Then
        vsPrinter.Action = 6 'se imprime en cada factura
        
        ' habria que marcar la factura como impresa
         qry.rdoParameters(0) = 2
         qry.rdoParameters(1) = ArrayCabecera(x).NFactura
         qry.Execute

    Else
      If x <> UBound(ArrayCabecera) Then
        vsPrinter.NewPage
      End If
        
    End If
    
  Next
   If ImpresionMasiva Then
'      Unload Me
   Else
       vsPrinter.Action = 6 'se visualizan todas las factura
   End If
  'vsPrinter.SaveDoc "c:\factura\" & ArrayCabecera(1).NumCaso & ".txt"
  MousePointer = 0
  
  NumPaginas = vsPrinter.PageCount
  If NumPaginas = 1 Then
    Me.cmdPagAdelante.Enabled = False
    Me.cmdPagAtras.Enabled = False
  ElseIf NumPaginas > 1 Then
    Me.cmdPagAdelante.Enabled = True
    Me.cmdPagAtras.Enabled = False
  Else
    Me.cmdPagAdelante.Enabled = False
    Me.cmdPagAtras.Enabled = False
  End If
  PagActual = 1
  
  
Exit Sub
error:
    
End Sub

Private Sub LineasFactura(factura As Integer)
    Dim s$, fmt$
    Dim x, Y
    Dim LineaFact As String
    Dim ImporteFact As Double
    Dim ArrayFactura()  As LineaImpre
    Dim ContLineas
    Dim PosicionX As Integer
    Dim PosicionY As Integer
    Dim PosBlanco As Long
    Dim PosComa As Long
    Dim Cadena As String
    Dim Texto As String
    Dim MaxLin As Integer
    Dim MaxRNF As Integer
    Dim Lineas As Integer
    Dim Limite As Integer
    Dim PosObserv As Integer
    
    
    'Inicializo el array de las l�neas de factura y su contador
    ContLineas = 1
    'Calcularemos el n�mero de l�neas de las observaciones para marcar el inicio de estas en la
    '�ltima p�gina de la impresi�n.
    Cadena = ArrayCabecera(factura).Observac & " "
    PosicionX = 2000
    Lineas = 1
    While Cadena <> ""
      PosBlanco = InStr(1, Cadena, " ")
      If PosBlanco <> 0 Then
        Texto = Left(Cadena, PosBlanco)
        Cadena = Right(Cadena, Len(Cadena) - PosBlanco)
        If PosicionX + vsPrinter.TextWidth(Texto) <= 9085 Then
          PosicionX = PosicionX + vsPrinter.TextWidth(Texto)
        Else
          PosicionX = 2000
          PosicionX = PosicionX + vsPrinter.TextWidth(Texto)
          Lineas = Lineas + 1
        End If
      End If
    Wend
    
    'Si el las l�neas son m�s de 2 que es lo calculado en un principio, restamos 3 mm al espacio
    'destinado a las l�neas por cada una nueva que se escribe.
    
    If Lineas <= 2 Then
      Limite = 220
      PosObserv = 235
    Else
      Limite = 220 - ((Lineas - 2) * 3)
      PosObserv = 235 - ((Lineas - 2) * 3)
    End If
    
    With vsPrinter
'        MsgBox .CurrentY
        .MarginRight = 12 * ConvX
        '--------------------------------------------------------
        ' create table format
        '--------------------------------------------------------
        fmt = "=7085|>_1730;"                   '^Center > Right
        
        '--------------------------------------------------------
        ' create table string
        '--------------------------------------------------------
        .FontName = "Arial"
        .FontSize = 10
        .MarginLeft = 2000
        
        ' esto es para recoger el numero de lineas de factura contenidas
        ' en ArrayLineas, pero controlando el error que da cuando no tiene
        ' ninguna
        MaxLin = 0
        On Error Resume Next
        MaxLin = UBound(ArrayLineas)
        On Error GoTo 0
        
        For x = 1 To MaxLin
            If ArrayLineas(x).mostrar Then
                If ArrayLineas(x).NFactura = factura Then
                    .CurrentX = 2000
                    .TextAlign = taLeftBaseline
                    Cadena = UCase(ArrayLineas(x).Descripcion) & ": "
                    PosicionX = .CurrentX
                    While Cadena <> ""
                        PosBlanco = InStr(1, Cadena, " ")
                        If PosBlanco <> 0 Then
                            Texto = Left(Cadena, PosBlanco)
                            Cadena = Right(Cadena, Len(Cadena) - PosBlanco)
                            If PosicionX + .TextWidth(Texto) <= 9085 Then
                                .Text = Texto
                                PosicionX = .CurrentX
                            Else
                                .CurrentX = 2000
                                .CurrentY = .CurrentY + 200
                                If vsPrinter.CurrentY > 220 * ConvY Then
                                    Call NuevaPagina(factura)
                                    .MarginRight = 12 * ConvX
                                    .FontSize = 10
                                End If
                                .Text = Texto
                                PosicionX = .CurrentX
                            End If
                        End If
                    Wend
             
                    ' esto es para recoger el numero de RNFs contenidos
                    ' en arrayRNFs, pero controlando el error que da cuando no tiene
                    ' ninguna
                    MaxRNF = 0
                    On Error Resume Next
                    MaxRNF = UBound(arrayRNFs)
                    On Error GoTo 0
                
                    PosicionX = .CurrentX
                    For Y = 1 To MaxRNF
                        If arrayRNFs(Y).NFactura = factura And arrayRNFs(Y).NLinea = ArrayLineas(x).NLinea And _
                            arrayRNFs(Y).Desglose = True And arrayRNFs(Y).Cantidad <> 0 Then
                            'A�adimos la descripci�n de los distintos rnfs que corresponden a la l�nea
                            'Eliminaremos de la cadena la parte que se queda a la derecha de la coma en el caso de que esta exista
                            Cadena = UCase(arrayRNFs(Y).Descripcion)
                            PosComa = InStr(1, Cadena, ",")
                            If PosComa <> 0 Then
                                Cadena = Left(Cadena, PosComa - 1)
                            End If
                            If arrayRNFs(Y).Cantidad <> 1 And arrayRNFs(Y).Cantidad <> -1 And Trim(UCase(ArrayLineas(x).Descripcion)) <> "MEDICACI�N" Then
                                Cadena = Cadena & " (" & arrayRNFs(Y).Cantidad & ")"
                            End If
                            Cadena = Cadena & ","
                            Cadena = Cadena & " "
                            While Cadena <> ""
                                PosBlanco = InStr(1, Cadena, " ")
                                If PosBlanco <> 0 Then
                                    Texto = Left(Cadena, PosBlanco)
                                    Cadena = Right(Cadena, Len(Cadena) - PosBlanco)
                                    '.TextColor = QBColor(15)
                                    '.Text = Texto
                                    'If .CurrentX <= 9085 Then
                                    If PosicionX + .TextWidth(Texto) <= 9085 Then
                                        '.CurrentX = PosicionX
                                        '.TextColor = QBColor(0)
                                        .Text = Texto
                                        PosicionX = .CurrentX
                                    Else
                                        .CurrentX = 2000
                                        .CurrentY = .CurrentY + 200
                                        If vsPrinter.CurrentY > 220 * ConvY Then
                                            Call NuevaPagina(factura)
                                            .MarginRight = 12 * ConvX
                                            .CurrentX = 2000
                                            .FontSize = 10
                                        End If
                                        '.TextColor = QBColor(0)
                                        .Text = Texto
                                        PosicionX = .CurrentX
                                    End If
                                End If
                            Wend
                            'a�adimos los rnfs
                        Else
                            ' El RNF no se imprime
                        End If
                    Next
          
                    PosicionY = .CurrentY
                    '.CurrentX = 217 * ConvX
                    '.CurrentY = .CurrentY - 200
                    .TextAlign = taRightBaseline
                    .Text = FormatearMoneda(ArrayLineas(x).Importe, tiImporteLinea)
                    .TextAlign = taLeftBaseline
                    .CurrentX = PosicionX
                    While .CurrentX <= 9085
                        .Text = "."
                    Wend
                    .CurrentX = 2000
                    .CurrentY = .CurrentY + 400
                    If vsPrinter.CurrentY > 200 * ConvY Then
                        Call NuevaPagina(factura)
                        .MarginRight = 12 * ConvX
                        .FontSize = 10
                    End If
                End If
            Else
                ' la linea no es imprimible por que sus RNFs tienen cantidad 0
            End If
        Next
                
 
        '.Paragraph = ""
        .SpaceAfter = 100
        .TextAlign = 0
   
        '-------------------------------------------------------
        ' Impresi�n desde la base de datos.
        '-------------------------------------------------------
        '-------------------------------------------------------
        ' Imprimimos el total.
        '-------------------------------------------------------
        x = .CurrentY
        If x + 700 > Limite * ConvY Then
          Call NuevaPagina(factura)
          .MarginRight = 12 * ConvX
          .FontSize = 10
        End If
        If x + 700 > PosObserv * ConvY Then
          Call NuevaPagina(factura)
          .MarginRight = 12 * ConvX
          .FontSize = 10
        End If
        x = x - 150
        'Imprimimos la l�nea que simboliza el final de la suma
        .DrawLine 170 * ConvX, x, 200 * ConvX, x
        .MarginRight = 12 * ConvX
        .CurrentX = 42 * ConvX
        .Text = " EXENTO DE  I.V.A."
        .CurrentX = 130 * ConvX
        .Text = "TOTAL PESETAS"
        PosicionY = .CurrentY
        .TextAlign = taRightTop
        .Text = FormatearMoneda(ArrayCabecera(factura).TotFactura, tiTotalFact)
        .CurrentY = .CurrentY + 500
        x = .CurrentY
        x = x - 200
        'Imprimimos la doble l�nea que separa el total ptas de total euros
        .DrawLine 130 * ConvX, x, 200 * ConvX, x
        .DrawLine 130 * ConvX, x + 80, 200 * ConvX, x + 80
        .CurrentX = 203 * ConvX
        .TextAlign = taLeftBaseline
        .CurrentY = .CurrentY + 200
        .CurrentX = 130 * ConvX
        .Text = "TOTAL EUROS"
        .TextAlign = taRightBaseline
        .Text = Format(ArrayCabecera(factura).TotFactura / 166.386, "###,##0.00")
        .TextAlign = taLeftBaseline
        .FontBold = False
        .CurrentX = 16 * ConvX
        .CurrentY = PosObserv * ConvY
        .MarginLeft = 16 * ConvX
        .Paragraph = UCase(ArrayCabecera(factura).Observac)
    End With

End Sub



Private Sub LineasFacturaMutuas(factura As Integer)
    Dim s$, fmt$
    Dim x, Y
    Dim LineaFact As String
    Dim ImporteFact As Double
    Dim ArrayFactura()  As LineaImpre
    Dim ContLineas
    Dim PosicionX As Integer
    Dim PosicionY As Integer
    Dim PosBlanco As Long
    Dim PosComa As Long
    Dim Cadena As String
    Dim Texto As String
    Dim MaxLin As Integer
    Dim MaxRNF As Integer
    Dim Lineas As Integer
    Dim Limite As Integer
    Dim PosObserv As Integer
    Dim Cantidad As Double
    Dim Precio As String
    Dim SinRNFs As Boolean
    Dim TextoRNF As String
    
    'Inicializo el array de las l�neas de factura y su contador
    ContLineas = 1
    'Calcularemos el n�mero de l�neas de las observaciones para marcar el inicio de estas en la
    '�ltima p�gina de la impresi�n.
    Cadena = ArrayCabecera(factura).Observac & " "
    PosicionX = 2000
    Lineas = 1
    While Cadena <> ""
      PosBlanco = InStr(1, Cadena, " ")
      If PosBlanco <> 0 Then
        Texto = Left(Cadena, PosBlanco)
        Cadena = Right(Cadena, Len(Cadena) - PosBlanco)
        If PosicionX + vsPrinter.TextWidth(Texto) <= 9085 Then
          PosicionX = PosicionX + vsPrinter.TextWidth(Texto)
        Else
          PosicionX = 2000
          PosicionX = PosicionX + vsPrinter.TextWidth(Texto)
          Lineas = Lineas + 1
        End If
      End If
    Wend
    
    'Si el las l�neas son m�s de 2 que es lo calculado en un principio, restamos 3 mm al espacio
    'destinado a las l�neas por cada una nueva que se escribe.
    
    If Lineas <= 2 Then
      Limite = 220
      PosObserv = 235
    Else
      Limite = 220 - ((Lineas - 2) * 3)
      PosObserv = 235 - ((Lineas - 2) * 3)
    End If
    
    With vsPrinter
'        MsgBox .CurrentY
        .MarginRight = 12 * ConvX
        '--------------------------------------------------------
        ' create table format
        '--------------------------------------------------------
        fmt = "=7085|>_1730;"                   '^Center > Right
        
        '--------------------------------------------------------
        ' create table string
        '--------------------------------------------------------
        .FontName = "Arial"
        .FontSize = 10
        .MarginLeft = 2000
        
        ' esto es para recoger el numero de lineas de factura contenidas
        ' en ArrayLineas, pero controlando el error que da cuando no tiene
        ' ninguna
        MaxLin = 0
        On Error Resume Next
        MaxLin = UBound(ArrayLineas)
        On Error GoTo 0
        'Dado que lo podemos necesitar para la l�nea del forfait, obtenemos el n�mero de rnfs aqu�.
        MaxRNF = 0
        On Error Resume Next
        MaxRNF = UBound(arrayRNFs)
'        On Error GoTo 0
    
        For x = 1 To MaxLin
            If ArrayLineas(x).mostrar Then
              If ArrayLineas(x).Importe <> 0 Then
                If ArrayLineas(x).NFactura = factura Then
                    .CurrentX = 2000
                    .TextAlign = taLeftBaseline
                    
                    ' ///////////////////////////////////////////////
                    ' ***********************************************
                    ' MODIFICACION DE PATXI
                    If UCase(ArrayLineas(x).Descripcion) <> "*HONORARIOS*" Then
                        Cadena = UCase(ArrayLineas(x).Descripcion) & ": "
                        PosicionX = .CurrentX
                        PosComa = InStr(1, Cadena, ",")
                        If PosComa <> 0 Then
                          Cadena = Left(Cadena, PosComa - 1)
                        End If
                        'Si se trata de la l�nea de forfait, a�adimos la descripci�n del mismo
                        If ArrayLineas(x).Descripcion = "FORFAIT DE " Then
                          For Y = 1 To MaxRNF
                            If arrayRNFs(Y).NFactura = factura And arrayRNFs(Y).NLinea = ArrayLineas(x).NLinea And _
                              arrayRNFs(Y).Desglose = True And arrayRNFs(Y).Cantidad <> 0 Then
                              Cadena = Cadena & arrayRNFs(Y).Descripcion & " "
                            End If
                          Next
                        End If
                    Else
                        'Cadena = UCase(ArrayLineas(x).Descripcion) & ": "
                        PosicionX = .CurrentX
                        
                        'Si se trata de la l�nea de Honorarios, a�adimos la descripci�n del mismo
                        For Y = 1 To MaxRNF
                          If arrayRNFs(Y).NFactura = factura And arrayRNFs(Y).NLinea = ArrayLineas(x).NLinea And _
                            arrayRNFs(Y).Desglose = True And arrayRNFs(Y).Cantidad <> 0 Then
                            Cadena = Cadena & arrayRNFs(Y).Descripcion & " "
                          End If
                        Next
                    End If
                    ' *************************************************
                    ' \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                    
                    Cadena = Cadena & " "
                    ArrayLineas(x).Descripcion = UCase(ArrayLineas(x).Descripcion)
                    
                    If Left(ArrayLineas(x).Descripcion, 12) = "ESTANCIAS EN" Then
                    
                     If ArrayLineas(x).Precio <> 0 Then
                        Cantidad = ArrayLineas(x).Importe / ArrayLineas(x).Precio
                     
                     Else
                        ' hay que calcular la cantidad
                        Cantidad = 0
                        For Y = 1 To MaxRNF
                           If arrayRNFs(Y).NFactura = factura And arrayRNFs(Y).NLinea = ArrayLineas(x).NLinea And _
                              arrayRNFs(Y).Cantidad <> 0 Then
                             Cantidad = Cantidad + arrayRNFs(Y).Cantidad
                           Else
                             ' El RNF no se tiene en cuenta
                           End If
                        Next
                        If Cantidad <> 0 Then
                           ArrayLineas(x).Precio = ArrayLineas(x).Importe / Cantidad
                        End If

                     End If
                     
                      .CurrentX = 2500 - .TextWidth(Format(Cantidad, "###,##0.0"))
                      .Text = Format(Cantidad, "###,##0.0")
                      .CurrentX = 2600
                      
                      .Text = ArrayLineas(x).Descripcion
                      Precio = "A " & Right(Space(12) & Format(ArrayLineas(x).Precio, "###,###,##0.##"), 12)
                      .CurrentX = 9000 - .TextWidth(Precio)
                      .Text = Precio
                      .TextAlign = taRightBaseline
                      '.CurrentY = .CurrentY - 400
                      .Text = FormatearMoneda(ArrayLineas(x).Importe, tiImporteLinea)
                      .CurrentY = .CurrentY + 200
                      .TextAlign = taLeftBaseline
                      Cadena = ""
                    End If
                    
                    While Cadena <> ""
                        PosBlanco = InStr(1, Cadena, " ")
                        If PosBlanco <> 0 Then
                            Texto = Left(Cadena, PosBlanco)
                            Cadena = Right(Cadena, Len(Cadena) - PosBlanco)
                            If PosicionX + .TextWidth(Texto) <= 9085 Then
                                .Text = Texto
                                PosicionX = .CurrentX
                            Else
                                .CurrentX = 2000
                                .CurrentY = .CurrentY + 200
                                If vsPrinter.CurrentY > 220 * ConvY Then
                                    Call NuevaPagina(factura)
                                    .MarginRight = 12 * ConvX
                                    .FontSize = 10
                                End If
                                .Text = Texto
                                PosicionX = .CurrentX
                            End If
                        End If
                    Wend
                    
                    ' ///////////////////////////////////////////////
                    ' ***********************************************
                    ' MODIFICACION DE PATXI
                    If ArrayLineas(x).Descripcion = "FORFAIT DE " Or ArrayLineas(x).Descripcion = "*HONORARIOS*" Then
                    ' ***********************************************
                    ' \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                    
                      '.CurrentY = .CurrentY - 200
                      .TextAlign = taRightBaseline
                      .Text = FormatearMoneda(ArrayLineas(x).Importe, tiImporteLinea)
                      .TextAlign = taLeftBaseline
                    ElseIf ArrayLineas(x).Descripcion = "HONORARIOS ANESTESIA" Then
                      .TextAlign = taRightBaseline
                      .Text = FormatearMoneda(ArrayLineas(x).Importe, tiImporteLinea)
                      .TextAlign = taLeftBaseline
                    ElseIf ArrayLineas(x).Descripcion = "DERECHOS QUIR�FANO" Then
                      .TextAlign = taRightBaseline
                      .Text = FormatearMoneda(ArrayLineas(x).Importe, tiImporteLinea)
                      .TextAlign = taLeftBaseline
                    End If
                    .CurrentY = .CurrentY + 200
                    ' esto es para recoger el numero de RNFs contenidos
                    ' en arrayRNFs, pero controlando el error que da cuando no tiene
                    ' ninguna
                    
                    PosicionX = .CurrentX
                    SinRNFs = True
                    For Y = 1 To MaxRNF
                        ' ///////////////////////////////////////////////
                        ' ***********************************************
                        ' MODIFICACION DE PATXI
                        If ArrayLineas(x).Descripcion = "FORFAIT DE " Or ArrayLineas(x).Descripcion = "*HONORARIOS*" Then
                        ' ***********************************************
                        ' \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                          SinRNFs = False
                        ElseIf Left(ArrayLineas(x).Descripcion, 12) = "ESTANCIAS EN" Then
                          SinRNFs = False
                        Else
                          If arrayRNFs(Y).NFactura = factura And arrayRNFs(Y).NLinea = ArrayLineas(x).NLinea And _
                             arrayRNFs(Y).Desglose = True And arrayRNFs(Y).Cantidad <> 0 Then
                             SinRNFs = False
                            'Comprobamos que no se repitan las descripciones de la linea y del rnf
                            If arrayRNFs(Y).Descripcion = ArrayLineas(x).Descripcion Then
                              TextoRNF = ""
                              .TextAlign = taRightBaseline
                              .CurrentY = .CurrentY - 200
                              .Text = FormatearMoneda(arrayRNFs(Y).Cantidad * arrayRNFs(Y).Precio, tiImporteLinea)
                              .TextAlign = taLeftBaseline
                            Else
                              TextoRNF = arrayRNFs(Y).Descripcion
                              'Imprimimos la l�nea con las pruebas que se corresponden con esa factura
                              .CurrentX = 2500 - .TextWidth(arrayRNFs(Y).Cantidad)
                              .Text = arrayRNFs(Y).Cantidad
                              .CurrentX = 2600
                              PosComa = InStr(1, TextoRNF, ",")
                              If PosComa <> 0 Then
                                TextoRNF = Left(TextoRNF, PosComa - 1)
                              End If
                              .Text = TextoRNF
                              .TextAlign = taRightBaseline
                              .Text = FormatearMoneda(arrayRNFs(Y).Cantidad * arrayRNFs(Y).Precio, tiImporteLinea)
                              .CurrentY = .CurrentY + 200
                              .TextAlign = taLeftBaseline
                            End If
                          ElseIf arrayRNFs(Y).Desglose = False And arrayRNFs(Y).NLinea = ArrayLineas(x).NLinea Then
                            If ArrayLineas(x).Precio = 0 Then
                              ArrayLineas(x).Precio = arrayRNFs(Y).Precio
                            End If
                          Else
                            ' El RNF no se imprime
                          End If
                          If vsPrinter.CurrentY > 220 * ConvY Then
                            Call NuevaPagina(factura)
                            .MarginRight = 12 * ConvX
                            .FontSize = 10
                          End If
                        End If
                    Next
                    If SinRNFs = True Then
                      If ArrayLineas(x).Precio <> 0 Then
                        .CurrentX = 2500 - .TextWidth(ArrayLineas(x).Importe / ArrayLineas(x).Precio)
                        .Text = ArrayLineas(x).Importe / ArrayLineas(x).Precio
                        .CurrentX = 2600
                        TextoRNF = ArrayLineas(x).Descripcion
                        PosComa = InStr(1, TextoRNF, ",")
                        If PosComa <> 0 Then
                          TextoRNF = Left(TextoRNF, PosComa - 1)
                        End If
                        .Text = TextoRNF
                        .TextAlign = taRightBaseline
                        .Text = FormatearMoneda(ArrayLineas(x).Importe, tiImporteLinea)
                        .CurrentY = .CurrentY + 200
                        .TextAlign = taLeftBaseline
                      End If
                    End If
                    .CurrentX = 2000
                    .CurrentY = .CurrentY + 400
                    If vsPrinter.CurrentY > 200 * ConvY Then
                        Call NuevaPagina(factura)
                        .MarginRight = 12 * ConvX
                        .FontSize = 10
                    End If
                End If
              End If
            Else
                ' la linea no es imprimible por que sus RNFs tienen cantidad 0
            End If
        Next
                
 
        '.Paragraph = ""
        .SpaceAfter = 100
        .TextAlign = 0
   
        '-------------------------------------------------------
        ' Impresi�n desde la base de datos.
        '-------------------------------------------------------
        '-------------------------------------------------------
        ' Imprimimos el total.
        '-------------------------------------------------------
        x = .CurrentY
        If x + 700 > Limite * ConvY Then
          Call NuevaPagina(factura)
          .MarginRight = 12 * ConvX
          .FontSize = 10
        End If
        If x + 700 > PosObserv * ConvY Then
          Call NuevaPagina(factura)
          .MarginRight = 12 * ConvX
          .FontSize = 10
        End If
        x = x - 150
        'Imprimimos la l�nea que simboliza el final de la suma
        .DrawLine 170 * ConvX, x, 200 * ConvX, x
        .MarginRight = 12 * ConvX
        .CurrentX = 42 * ConvX
        .Text = " EXENTO DE  I.V.A."
        .CurrentX = 130 * ConvX
        .Text = "TOTAL PESETAS"
        PosicionY = .CurrentY
        .TextAlign = taRightTop
        .Text = FormatearMoneda(ArrayCabecera(factura).TotFactura, tiTotalFact)
        .CurrentY = .CurrentY + 500
        x = .CurrentY
        x = x - 200
        'Imprimimos la doble l�nea que separa el total ptas de total euros
        .DrawLine 130 * ConvX, x, 200 * ConvX, x
        .DrawLine 130 * ConvX, x + 80, 200 * ConvX, x + 80
        .CurrentX = 203 * ConvX
        .TextAlign = taLeftBaseline
        .CurrentY = .CurrentY + 200
        .CurrentX = 130 * ConvX
        .Text = "TOTAL EUROS"
        .TextAlign = taRightBaseline
        .Text = Format(ArrayCabecera(factura).TotFactura / 166.386, "###,##0.00")
        .TextAlign = taLeftBaseline
        If ArrayCabecera(factura).reco.Codigo = "800563" _
            Or ArrayCabecera(factura).reco.Codigo = "800564" _
            Or ArrayCabecera(factura).reco.Codigo = "800565" Then
          If .CurrentY + 400 < (PosObserv * ConvY) - 200 Then
            .CurrentY = .CurrentY + 400
          ElseIf .CurrentY + 200 > PosObserv * ConvY Then
            Call NuevaPagina(factura)
          Else
            .CurrentY = .CurrentY + 200
          End If
          .CurrentX = 2000
          .Paragraph = "POR EL CENTRO CONCERTADO"
        End If
        
        .FontBold = False
        .CurrentX = 16 * ConvX
        .CurrentY = PosObserv * ConvY
        .MarginLeft = 16 * ConvX
        .Paragraph = UCase(ArrayCabecera(factura).Observac)
    End With

End Sub




Private Sub Form_Load()

    Dim i%, s$
        
    '------------------------------------------------------
    ' save orientation to clean up later
    '------------------------------------------------------
    OldOrientation = vsPrinter.Orientation
    MyPage = -1                         ' no current page
  
    '------------------------------------------------------
    ' preset zoom levels (you can choose your own)
    '------------------------------------------------------
    With cmbZoom
        .AddItem "30"
        .AddItem "50"
        .AddItem "75"
        .AddItem "100"
        .AddItem "120"
        .AddItem "200"
        .AddItem "300"
        .AddItem "400"
        
        .ListIndex = 2
    End With
    
    '------------------------------------------------------
    ' orientation (you cannot choose your own)
    '------------------------------------------------------
    With cmbOrientation
        .AddItem "Portrait"
        .AddItem "Landscape"
        .ListIndex = 0
    End With

    '------------------------------------------------------
    ' ready, set default page to 0
    '------------------------------------------------------
    MyPage = 0
  
    With vsPrinter
        
         If ImpresionMasiva Then
            .Preview = False
         Else
            .Preview = True         ' Show preview to screen
         End If
         
        .PreviewPage = 1        ' default preview page to first page
        .MarginBottom = 0
        '------------------------------------------------------
        ' show available devices
        ' and honor Windows default selection
        '------------------------------------------------------
        Dim curdev%
        For i = 0 To .NDevices - 1
          If .Devices(i) = "\\CPU00605\ADMINISTRACION" Then
            cmb_printers.AddItem .Devices(i)
          ElseIf .Devices(i) = "ADMINISTRACION" Then
            cmb_printers.AddItem .Devices(i)
          ElseIf .Devices(i) = "\\CPU00613\HPFACT" Then
            cmb_printers.AddItem .Devices(i)
          ElseIf .Devices(i) = "HPFACT" Then
            cmb_printers.AddItem .Devices(i)
          ElseIf .Devices(i) = "\\CPU00615\HPFACT" Then
            cmb_printers.AddItem .Devices(i)
          ElseIf .Devices(i) = "\\CPDCUNSERVER\HP4SI-DUP" Then
            cmb_printers.AddItem .Devices(i)
          ElseIf .Devices(i) = "\\CPU00609\HP DeskJet 600" And (objSecurity.strUser = "CII" Or objSecurity.strUser = "OTL" Or objSecurity.strUser = "I�AKI") Then
            cmb_printers.AddItem .Devices(i)
          ElseIf .Devices(i) = "HP DeskJet 600" And (objSecurity.strUser = "CII" Or objSecurity.strUser = "OTL" Or objSecurity.strUser = "I�AKI") Then
            cmb_printers.AddItem .Devices(i)
          End If
          If .Devices(i) = .Device Then curdev = i
        Next
        For i = 0 To .NDevices - 1
          If .Devices(i) = "Acrobat PDFWriter" Then
            cmb_printers.AddItem .Devices(i)
          End If
        Next
        cmb_printers.ListIndex = 0
        
    End With
    Command1_Click
End Sub

Public Function pImpresion(objFactura As factura, Prop As String) As factura
Dim Posicion As Integer
    
    'Set varFact = objFactura
    'Buscamos el / para separar el n�mero de asistencia y la propuesta seleccionada
'    Posicion = InStr(1, Prop, "/")
'    If Posicion <> 0 Then
      'Propuesta seleccionada
'      Propuesta = Right(Prop, Len(Prop) - Posicion)
      'N�mero de asistencia.
'      NumAsist = Left(Prop, Posicion - 1)
'    End If
'    Load frm_Impresion
'      frm_Impresion.Show (vbModal)
            
    'Set objFactura = varFact
      Unload frm_Impresion


End Function

Sub ImprimirDatosJASS(Mes As Double, ANYO As Integer)
Dim Texto As String
Dim UltimoDia As Date
Dim PrimerDia As Date

  With vsPrinter
    .MarginLeft = 0
    .MarginTop = 0
    .Preview = True
    .PenStyle = psSolid
    
    .DrawLine 26 * ConvX, 19 * ConvY, 26 * ConvX, 24 * ConvY
    .FontName = "Courier New"
    .FontSize = 10
    .TextAlign = taLeftBaseline
    .CurrentY = 24 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Datos(1).Tipo & "      " & Datos(1).HistAsist
    .CurrentY = 28 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Datos(1).Nombre
    .CurrentY = 32 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Datos(1).SegSocial & " " & Left(Datos(1).Afiliado & Space(13), 13) & " " & Datos(1).Prov
    .CurrentY = 36 * ConvY
    .CurrentX = 28 * ConvX
    PrimerDia = CalcularPrimerDia(Mes, ANYO)
    'If CDate(Datos(1).Ingreso) < CDate(PrimerDia) Then
    '  .Text = Right(Space(12) & Format(PrimerDia, "dd/mm/yyyy"), 12)
    'Else
      .Text = Right(Space(12) & Format(Datos(1).Ingreso, "dd/mm/yyyy"), 12)
    'End If
    .CurrentY = 40 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Datos(1).Departamento
    .CurrentY = 44 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Datos(1).Direccion
    .CurrentY = 48 * ConvY
    .CurrentX = 28 * ConvX
    .Text = Datos(1).Poblacion
    .CurrentY = 33 * ConvY
    .CurrentX = 112 * ConvX
    .Text = Right(Space(12) & Datos(1).Cama, 12)
    .CurrentY = 45 * ConvY
    .CurrentX = 112 * ConvX
    UltimoDia = CalcularUltimoDia(Mes, ANYO)
    If Trim(Datos(1).Alta) <> "" Then
      If Datos(1).Alta > UltimoDia Then
        .Text = Right(Space(12) & Format(UltimoDia, "dd/mm/yyyy"), 12)
        .CurrentY = 49 * ConvY
        .CurrentX = 112 * ConvX
        .Text = Right(Space(12) & "24:00", 12)
        .FontSize = 9
        .CurrentY = 155 * ConvY
        .CurrentX = 57 * ConvX
        .Text = "Pamplona, " & Format(UltimoDia, "d ""de ""MMMM"" de ""yyyy")
      Else
        .Text = Right(Space(12) & Datos(1).Alta, 12)
        .CurrentY = 49 * ConvY
        .CurrentX = 112 * ConvX
        .Text = Right(Space(12) & Datos(1).Hora, 12)
        .FontSize = 9
        .CurrentY = 155 * ConvY
        .CurrentX = 57 * ConvX
        .Text = "Pamplona, " & Format(Datos(1).Alta, "d ""de ""MMMM"" de ""yyyy")
      End If
    Else
      .Text = Right(Space(12) & Format(UltimoDia, "dd/mm/yyyy"), 12)
      .CurrentY = 49 * ConvY
      .CurrentX = 112 * ConvX
      .Text = Right(Space(12) & "24:00", 12)
      .FontSize = 9
      .CurrentY = 155 * ConvY
      .CurrentX = 57 * ConvX
      .Text = "Pamplona, " & Format(UltimoDia, "d ""de ""MMMM"" de ""yyyy")
    End If
    'dibujamos todo el rayado
    .DrawRectangle 7 * ConvX, 94 * ConvY, 141 * ConvX, 146 * ConvY
    .FontSize = 10
    
    .CurrentY = 149 * ConvY
    .CurrentX = 5 * ConvX
    '.Text = "2611991133"
  End With

End Sub

Sub ImprimirImpresoJASS()
Dim Texto As String
  With vsPrinter
    .MarginLeft = 0
    .MarginTop = 0
    .Preview = False
    .PenStyle = psSolid
    .DrawLine 26 * ConvX, 19 * ConvY, 26 * ConvX, 24 * ConvY
    .DrawLine 111 * ConvX, 19 * ConvY, 111 * ConvX, 24 * ConvY
    .DrawLine 26 * ConvX, 19 * ConvY, 31 * ConvX, 19 * ConvY
    .DrawLine 106 * ConvX, 19 * ConvY, 111 * ConvX, 19 * ConvY
    .DrawLine 26 * ConvX, 47 * ConvY, 26 * ConvX, 52 * ConvY
    .DrawLine 111 * ConvX, 47 * ConvY, 111 * ConvX, 52 * ConvY
    .DrawLine 26 * ConvX, 52 * ConvY, 31 * ConvX, 52 * ConvY
    .DrawLine 106 * ConvX, 52 * ConvY, 111 * ConvX, 52 * ConvY
    .TextAlign = taCenterBaseline
    .FontSize = 13
    .FontName = "times New Roman"
    .CurrentY = 12 * ConvY
    .Text = "CLINICA UNIVERSITARIA"
    .Paragraph = ""
    .FontBold = False
    .FontSize = 7
    .TextAlign = taLeftBaseline
    .CurrentY = 26 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Concertado"
    .CurrentY = 30 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Asistencia a D."
    .CurrentY = 34 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "N.� Afiliado"
    .CurrentY = 38 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "INGRESO"
    .CurrentY = 42 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Dr. encargado"
    .CurrentY = 46 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Domicilio"
    .CurrentY = 37 * ConvY
    .CurrentX = 126 * ConvX
    .Text = "SALIDA"
    .CurrentY = 42 * ConvY
    .CurrentX = 125 * ConvX
    .Text = "D�a y hora"
    .CurrentY = 42 * ConvY
    .CurrentX = 124 * ConvX
    .FontSize = 8
    .CurrentY = 61 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Datos de la persona que firma la conformidad a esta factura:"
    .CurrentY = 65 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Nombre:"
    .CurrentY = 73 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Domicilio:"
    .CurrentY = 82 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "Vinculaci�n con el enfermo:"
    .FontBold = True
    .FontSize = 10
    .CurrentY = 91 * ConvY
    .CurrentX = 8 * ConvX
    .Text = "SERVICIOS PRESTADOS:"
    'dibujamos todo el rayado
    .BrushStyle = bsTransparent
    .PenStyle = psSolid
    .DrawRectangle 7 * ConvX, 94 * ConvY, 141 * ConvX, 146 * ConvY
    .DrawLine 7 * ConvX, 102 * ConvY, 141 * ConvX, 102 * ConvY
    .PenStyle = psDot
    .DrawLine 7 * ConvX, 108 * ConvY, 37 * ConvX, 108 * ConvY
    .DrawLine 47 * ConvX, 108 * ConvY, 141 * ConvX, 108 * ConvY
    .DrawLine 7 * ConvX, 112 * ConvY, 141 * ConvX, 112 * ConvY
    .DrawLine 7 * ConvX, 116 * ConvY, 141 * ConvX, 116 * ConvY
    .DrawLine 7 * ConvX, 120 * ConvY, 141 * ConvX, 120 * ConvY
    .DrawLine 7 * ConvX, 124 * ConvY, 141 * ConvX, 124 * ConvY
    .DrawLine 7 * ConvX, 128 * ConvY, 141 * ConvX, 128 * ConvY
    .DrawLine 7 * ConvX, 132 * ConvY, 141 * ConvX, 132 * ConvY
    .DrawLine 7 * ConvX, 137 * ConvY, 141 * ConvX, 137 * ConvY
    .PenStyle = psSolid
    .DrawLine 83 * ConvX, 138 * ConvY, 141 * ConvX, 138 * ConvY
    .DrawLine 83 * ConvX, 94 * ConvY, 83 * ConvX, 146 * ConvY
    .DrawLine 109 * ConvX, 94 * ConvY, 109 * ConvX, 146 * ConvY
    .FontBold = False
    .FontSize = 8
    .CurrentY = 99 * ConvY
    .CurrentX = 37 * ConvX
    .Text = "DETALLE"
    .CurrentY = 99 * ConvY
    .CurrentX = 88 * ConvX
    .Text = "Precio unitario"
    .CurrentY = 99 * ConvY
    .CurrentX = 120 * ConvX
    .Text = "IMPORTE"
    .Paragraph = ""
    .FontSize = 6
    .CurrentY = 108.5 * ConvY
    .CurrentX = 35 * ConvX
    .Text = "d�as estancia"
    .Paragraph = ""
    .Paragraph = ""
    .FontSize = 8
    .CurrentY = 161 * ConvY
    .CurrentX = 8 * ConvX
    .FontSize = 7
    .Text = "POR EL CENTRO CONCERTADO"
    .Paragraph = ""
    .CurrentY = 161 * ConvY
    .CurrentX = 99 * ConvX
    .Text = "CONFORME"
    .Paragraph = ""
    .FontSize = 6
    .CurrentY = 175 * ConvY
    .CurrentX = 88 * ConvX
    .Text = "(Firma del paciente o acompa�ante)"
    .PenStyle = psDot
    .DrawLine 67 * ConvX, 172 * ConvY, 143 * ConvX, 172 * ConvY
  End With
End Sub

Sub ImprimirLineasJASS()

Dim Texto As String
Dim x As Integer
Dim cont As Integer
Dim PosY
Dim NumLineas As Integer

  On Error Resume Next
  NumLineas = UBound(Lineas)
  If Err <> 0 Then
    NumLineas = 0
  End If
  On Error GoTo 0
  
  cont = 1
  PosY = 107
  For x = 1 To NumLineas
    If Lineas(x).ImpTotal <> 0 Then
      With vsPrinter
        .FontSize = 10
        .TextAlign = taLeftBaseline
        .CurrentY = PosY * ConvY
        If Lineas(x).Cantidad = 0 Then
          Lineas(x).Cantidad = 1
        End If
        .CurrentX = 15 * ConvX - .TextWidth(Lineas(x).Cantidad & " ")
        .Text = Lineas(x).Cantidad & " " & Lineas(x).Descripcion
        .CurrentY = PosY * ConvY
        .CurrentX = 83 * ConvX
        If Lineas(x).ImpUnit <> 0 And Lineas(x).Descripcion = "Estancias" Then
          .Text = Right(Space(12) & Format(Lineas(x).ImpUnit, "#,###,##0,##"), 12) & " "
        Else
          .Text = Space(12)
        End If
        .CurrentY = PosY * ConvY
        .CurrentX = 110 * ConvX
        .Text = Right(Space(14) & Format(Lineas(x).ImpTotal, "###,###,##0,##"), 14) & " "
        cont = cont + 1
        PosY = PosY + 4
        If cont = 9 Then
          .FontSize = 12
          .CurrentY = 142 * ConvY
          .CurrentX = 59 * ConvX
          .Text = "SUMA Y SIGUE"
          .NewPage
          .Action = paNewPage
          Call Me.ImprimirImpresoJASS
          Call Me.ImprimirDatosJASS(1, 2000)
          cont = 1
        End If
      End With
    End If
  Next
  vsPrinter.FontSize = 12
  vsPrinter.CurrentY = 142 * ConvY
  vsPrinter.CurrentX = 59 * ConvX
  vsPrinter.Text = "TOTAL. . . . ."
End Sub


Sub IniciarQRYJASS()
Dim MiSqL As String
Dim i As Integer

  MiSqL = "Select  AD01FECINICIO, AD01FECFIN, CI22NUMHISTORIA From AD0100 Where " & _
          "AD01CODASISTENCI = ?"
  qryJASS(1).SQL = MiSqL
  
  MiSqL = "Select FA04FECDESDE,FA04FECHASTA From FA0400 Where FA04CODFACT = ?"
  qryJASS(11).SQL = MiSqL
  
  MiSqL = "Select CI22PRIAPEL||', '||CI22SEGAPEL ||', '|| CI22NOMBRE As Nombre, CI22NUMSEGSOC,CI21CODPERSONA " & _
          "From CI2200 Where CI22NUMHISTORIA = ?"
  qryJASS(2).SQL = MiSqL
  
  MiSqL = "Select GCFN06(AD15CODCAMA) as Cama From AD1600 Where AD01CODASISTENCI = ? And AD07CODPROCESO = ? ORDER BY AD16FECFIN DESC"
  qryJASS(3).SQL = MiSqL

  MiSqL = "Select AD02DESDPTO From AD0200, AD0500 Where " & _
          "AD0500.AD02CODDPTO = AD0200.AD02CODDPTO " & _
          "And AD0500.AD01CODASISTENCI = ? " & _
          "And AD0500.AD07CODPROCESO = ?"
  qryJASS(4).SQL = MiSqL
  
  MiSqL = "Select DECODE(TIPOASIST,1,'HOSPITALIZADO',2,'AMBULATORIO') AS TipoAsist From FA0419J " & _
          "Where FA04CODFACT = ?"
  qryJASS(5).SQL = MiSqL
  
  MiSqL = "SELECT FA1600.FA04CODFACT, FA1600.FA16NUMLINEA, t2.*, fa1600.* " & _
          "FROM fa0400 t2, fa1600, ad0700 " & _
          "Where t2.ad07codproceso = ad0700.ad07codproceso " & _
          "  AND ad0700.ci22numhistoria = ? " & _
          "  AND FA04NUMFACREAL IS NULL " & _
          "  AND t2.FA04CODFACT = FA1600.FA04CODFACT " & _
          "  AND FA16IMPORTE <> 0 " & _
          "  AND fa61codtipofact = 3 " & _
          "  AND EXISTS " & _
          "               ( " & _
          "               SELECT CI32CODTIPECON, CI13CODENTIDAD, FA09CODNODOCONC, FA09CODNODOCONC_FACT, FA04FECDESDE, FA04FECHASTA " & _
          "               FROM fa0400 t1 " & _
          "               WHERE t1.fa04numfact = ? " & _
          "                 AND FA04NUMFACREAL IS NULL " & _
          "                 AND t1.ad07codproceso = ? " & _
          "                 AND t1.ad01codasistenci = ? " & _
          "                 AND fa61codtipofact = 3 " & _
          "                 AND t1.CI32CODTIPECON = t2.CI32CODTIPECON " & _
          "                 AND t1.CI13CODENTIDAD = t2.CI13CODENTIDAD " & _
          "                 AND t1.FA09CODNODOCONC = t2.FA09CODNODOCONC " & _
          "                 AND t1.FA09CODNODOCONC_FACT = t2.FA09CODNODOCONC_FACT " & _
          "                 AND t1.FA04FECDESDE = t2.FA04FECDESDE " & _
          "                 AND t1.FA04FECHASTA = t2.FA04FECHASTA " & _
          "               ) "
   
  MiSqL = MiSqL & "UNION ALL "
  
  MiSqL = MiSqL & "SELECT FA1600.FA04CODFACT, FA1600.FA16NUMLINEA, t2.*, fa1600.* " & _
          "FROM fa0400 t2, fa1600, ad0700 " & _
          "Where ad0700.ci22numhistoria = ? " & _
          "  AND t2.fa04numfact = ? " & _
          "  AND FA04NUMFACREAL IS NULL " & _
          "  AND t2.ad07codproceso = ad0700.ad07codproceso " & _
          "  AND t2.FA04CODFACT = FA1600.FA04CODFACT " & _
          "  AND FA16IMPORTE <> 0 " & _
          "  AND (fa61codtipofact <> 3 OR fa61codtipofact IS NULL) " & _
          "ORDER BY 1, 2"
          
  qryJASS(6).SQL = MiSqL
  

  MiSqL = "Select * From FA0300 Where FA04NUMFACT = ? AND FA16NUMLINEA = ?" ' AND FA03INDFACT <> 0"
  qryJASS(7).SQL = MiSqL
  
  'Cogemos la descripci�n del forfait aplicado
  MiSqL = "Select * From FA4600 Where FA46CODDESEXTERNO = ?"
  qryJASS(8).SQL = MiSqL
  
  'Comprobamos si es un forfait.
  MiSqL = "Select * From FA1500 Where FA15CODATRIB = ?"
  qryJASS(9).SQL = MiSqL
  'Buscamos la entidad por la que se ha facturado
  MiSqL = "Select CI13CODENTIDAD From FA0400 Where FA04CODFACT = ?"
  qryJASS(10).SQL = MiSqL
  
  For i = 1 To 11
    Set qryJASS(i).ActiveConnection = objApp.rdoConnect
    qryJASS(i).Prepared = True
  Next
                         
                
End Sub


Sub ObtenerDatosJASS(Proceso As Long, Asistencia As Long, factura As Long, NumFactura As String, Historia As String)

Dim rs01 As rdoResultset
Dim rs22 As rdoResultset
Dim rs10 As rdoResultset
Dim rs02 As rdoResultset
Dim rs12 As rdoResultset
Dim rs16 As rdoResultset
Dim rsA16 As rdoResultset
Dim rs46 As rdoResultset
Dim rs15 As rdoResultset
Dim rs03 As rdoResultset
Dim rs04 As rdoResultset
Dim cont As Integer
Dim Persona As New Persona
Dim CodPersona As Long
  
  'Creamos el array que va a contener los datos de la cabecera del JASS
  ReDim Datos(1 To 1)
  Erase Lineas()
  
  'Seleccionamos la fecha de inicio, fecha fin y n�mero de historia de una asistencia
  qryJASS(1).rdoParameters(0) = Asistencia
  Set rs01 = qryJASS(1).OpenResultset
  If Not rs01.EOF Then
'    If Not IsNull(rs01("AD01FECINICIO")) Then
'      Datos(1).Ingreso = Format(rs01("AD01FECINICIO"), "DD/MM/YYYY     HH:MM")
'    Else
'      Datos(1).Ingreso = ""
'    End If
    If Not IsNull(rs01("AD01FECFIN")) Then
      Datos(1).Alta = Format(rs01("AD01FECFIN"), "DD/MM/YYYY")
      Datos(1).Hora = Format(rs01("AD01FECFIN"), "HH:MM")
    Else
      Datos(1).Alta = ""
      Datos(1).Hora = ""
    End If
    If Not IsNull(rs01("CI22NUMHISTORIA")) Then
      Datos(1).Historia = rs01("CI22NUMHISTORIA")
      Datos(1).HistAsist = rs01("CI22NUMHISTORIA") & "/" & Asistencia
    End If
  End If

  'Seleccionamos la fecha de inicio de la factura
  qryJASS(11).rdoParameters(0) = factura
  Set rs04 = qryJASS(11).OpenResultset
  If Not rs04.EOF Then
    If Not IsNull(rs04("FA04FECDESDE")) Then
      Datos(1).Ingreso = Format(rs04("FA04FECDESDE"), "DD/MM/YYYY")
    End If
  End If
  
  'Selecconamos el nombre y el n� de la seg social de una persona por su historia
  qryJASS(2).rdoParameters(0) = CLng(Datos(1).Historia)
  Set rs22 = qryJASS(2).OpenResultset
  If Not rs22.EOF Then
    If Not IsNull(rs22("Nombre")) Then
      Datos(1).Nombre = rs22("Nombre")
    Else
      Datos(1).Nombre = ""
    End If
    If Not IsNull(rs22("CI22NUMSEGSOC")) Then
      Datos(1).SegSocial = rs22("CI22NUMSEGSOC")
    End If
    If Not IsNull(rs22("CI21CODPERSONA")) Then
      CodPersona = rs22("CI21CODPERSONA")
      Persona.Codigo = CodPersona
      Datos(1).Direccion = Persona.Direccion
      Datos(1).Poblacion = Persona.Poblacion & "(" & Persona.Provincia & ")"
      'Datos(1).Prov = Persona.ProvCorta
    End If
  End If
  'Buscamos la provincia a la que hay que facturar
  qryJASS(10).rdoParameters(0) = factura
  Set rs04 = qryJASS(10).OpenResultset
  If Not rs04.EOF Then
    Datos(1).Prov = IIf(Not IsNull(rs04("CI13CODENTIDAD")), rs04("CI13CODENTIDAD"), "")
  Else
    Datos(1).Prov = ""
  End If
  'Cogemos la �ltima cama en la que ha estado el paciente
  qryJASS(3).rdoParameters(0) = Asistencia
  qryJASS(3).rdoParameters(1) = Proceso
  Set rsA16 = qryJASS(3).OpenResultset
  If Not rsA16.EOF Then
    If Not IsNull(rsA16("Cama")) Then
      Datos(1).Cama = rsA16("cama")
    Else
      Datos(1).Cama = ""
    End If
  Else
    Datos(1).Cama = ""
  End If
  
  'Seleccionamos el departamento responsable de un proceso'asistencia
  qryJASS(4).rdoParameters(0) = Asistencia
  qryJASS(4).rdoParameters(1) = Proceso
  Set rs02 = qryJASS(4).OpenResultset
  If Not rs02.EOF Then
    If Not IsNull(rs02("AD02DESDPTO")) Then
      Datos(1).Departamento = "DPTO. " & UCase(rs02("AD02DESDPTO"))
    Else
      Datos(1).Departamento = ""
    End If
  End If
  
  'Seleccionamos si el proceso asistencia es Ambulatorio u Hospitalizado
  qryJASS(5).rdoParameters(0) = factura
  Set rs12 = qryJASS(5).OpenResultset
  If Not rs12.EOF Then
    If Not IsNull(rs12("TipoAsist")) Then
      Datos(1).Tipo = rs12("TipoAsist")
    Else
      Datos(1).Tipo = ""
    End If
  End If
  cont = 1
  
  'Seleccionamos las l�neas de la factura para cada proceso-asistencia
  qryJASS(6).rdoParameters(0) = Historia
  qryJASS(6).rdoParameters(1) = NumFactura
  qryJASS(6).rdoParameters(2) = Proceso
  qryJASS(6).rdoParameters(3) = Asistencia
  qryJASS(6).rdoParameters(4) = Historia
  qryJASS(6).rdoParameters(5) = NumFactura
  
  Set rs16 = qryJASS(6).OpenResultset
  
  If Not rs16.EOF Then
    'rs16.MoveLast
    'rs16.MoveFirst
    ReDim Preserve Lineas(1 To cont)
    cont = 1
    While Not rs16.EOF
      ReDim Preserve Lineas(1 To cont)
      'Seleccionamos la cantidad y el precio unitario de cada una de las l�neas
      qryJASS(7).rdoParameters(0) = rs16("FA04CODFACT")
      qryJASS(7).rdoParameters(1) = rs16("FA16NUMLINEA")
      Set rs03 = qryJASS(7).OpenResultset
      If Not rs03.EOF Then
        Lineas(cont).ImpUnit = 0 & rs03("FA03PRECIOFACT")
        Lineas(cont).Cantidad = 0
        While Not rs03.EOF
          qryJASS(9).rdoParameters(0) = rs03("FA15CODATRIB")
          Set rs15 = qryJASS(9).OpenResultset
          If Not rs15.EOF Then
            If rs15("FA15INDSUFICIENTE") = 1 And rs15("FA15INDFACTOBLIG") = 1 Then
              If rs15("FA46CODDESEXTERNO") <> "" Then
                qryJASS(8).rdoParameters(0) = rs15("FA46CODDESEXTERNO")
                Set rs46 = qryJASS(8).OpenResultset
                If Not rs46.EOF Then
                  Lineas(cont).Descripcion = rs46("FA46DESPROC")
                End If
              End If
            End If
          End If
            
          Lineas(cont).Cantidad = Lineas(cont).Cantidad + IIf(Not IsNull(rs03("FA03CANTFACT")), rs03("FA03CANTFACT"), 0)
          rs03.MoveNext
        Wend
      Else
        Lineas(cont).Cantidad = 0
        Lineas(cont).ImpUnit = 0
      End If
      'If Not IsNull(rs16("FA16CANTIDAD")) Then
      '  Lineas(Cont).Descripcion = rs16("FA16CANTIDAD")
      'Else
      '  Lineas(Cont).Descripcion = ""
      'End If
      If Not IsNull(rs16("FA16DESCRIP")) Then
        If rs16("FA16DESCRIP") = "SESION QUIMIOTERAPIA" Then
          Lineas(cont).Descripcion = rs16("FA16DESCRIP") & " (" & RecogerFechasQuimio(rs16("FA04CODFACT"), rs16("FA16NUMLINEA"), cont) & ")"
        Else
          Lineas(cont).Descripcion = rs16("FA16DESCRIP")
        End If
      Else
        Lineas(cont).Descripcion = ""
      End If
      If Not IsNull(rs16("FA16IMPORTE")) Then
        Lineas(cont).ImpTotal = rs16("FA16IMPORTE")
      Else
        Lineas(cont).ImpTotal = 0
      End If
      rs16.MoveNext
      cont = cont + 1
    Wend
  End If
End Sub


Private Sub Form_Resize()
   On Error Resume Next
   If Me.Width < 6000 Then Me.Width = 6000
   If Me.Height < 3000 Then Me.Height = 3000
   
   vsPrinter.Width = Me.Width - 150 - vsPrinter.Left
   vsPrinter.Height = Me.Height - 450 - vsPrinter.Top
End Sub

Private Sub Form_Unload(Cancel As Integer)
  
  '--------------------------------------------------------
  ' restore printer orientation
  '--------------------------------------------------------
  vsPrinter.Orientation = OldOrientation

End Sub

' esta funcion devuelve el numero de la factura que se corresponde con la
' pagina pasada como parametro
Public Function FacturaVisible(numPaginaVisible As Integer) As Integer
   Dim i As Integer
   ' por defecto es la 0, (es un error si no la encuentra)
   FacturaVisible = 0
   For i = UBound(ArrayCabecera) To 1 Step -1
      If ArrayCabecera(i).numPagInicial <= numPaginaVisible Then
         FacturaVisible = i
         Exit For
      End If
   Next i
End Function

'Private Sub EvitarModif()
'   If Not blnPermitirModifFact Then
'      cmdPrint.Visible = False
'      cmdGrabar.Caption = "Grabar e Imprimir"
'   End If
'End Sub


Sub IniciarQRY()
   Dim MiSqL As String

    'Asignar el numero de factura REAL 0 (marca de anulaci�n) a la factura ANULADA
  MiSqL = "Update FA5000 SET FA50ESTADO = ? WHERE FA04NUMFACT = ?"
  qry.SQL = MiSqL
  
  'Activamos las querys.
    Set qry.ActiveConnection = objApp.rdoConnect
    qry.Prepared = True
End Sub

Private Sub txtCodDocum_LostFocus()
Dim SQL As String
Dim RS As rdoResultset

  If Trim(Me.txtCodDocum.Text) <> "" Then
    SQL = "Select FA57DESCORTA From FA5700 Where FA57NUMIMPRESO = " & Me.txtCodDocum.Text
    Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
    If Not RS.EOF Then
      cboDocumento.Text = RS("FA57DESCORTA")
    Else
      Me.cboDocumento.Text = ""
    End If
  End If

End Sub


