VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_AcuseRecibos 
   AutoRedraw      =   -1  'True
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Impresi�n de Acuses de Recibo"
   ClientHeight    =   1230
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1230
   ScaleWidth      =   6150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture1 
      Height          =   90
      Left            =   45
      Picture         =   "FA00133.frx":0000
      ScaleHeight     =   30
      ScaleWidth      =   30
      TabIndex        =   8
      Top             =   1080
      Visible         =   0   'False
      Width           =   90
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Iniciar Impresi�n"
      Height          =   330
      Left            =   2115
      TabIndex        =   7
      Top             =   810
      Width           =   1725
   End
   Begin ComctlLib.ProgressBar PrgBar1 
      Height          =   240
      Left            =   540
      TabIndex        =   2
      Top             =   810
      Visible         =   0   'False
      Width           =   4830
      _ExtentX        =   8520
      _ExtentY        =   423
      _Version        =   327682
      Appearance      =   1
      Min             =   1
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   1890
      TabIndex        =   5
      Tag             =   "Fecha Inicio"
      Top             =   45
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFin 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   1890
      TabIndex        =   6
      Tag             =   "Fecha Inicio"
      Top             =   360
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   690
      Left            =   270
      TabIndex        =   9
      Top             =   1305
      Width           =   600
      _Version        =   196608
      _ExtentX        =   1058
      _ExtentY        =   1217
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      PreviewMode     =   1
   End
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   240
      Left            =   3870
      TabIndex        =   10
      Top             =   1350
      Width           =   240
      _Version        =   196608
      _ExtentX        =   423
      _ExtentY        =   423
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      TableBorder     =   0
      PreviewMode     =   1
      AutoRTF         =   0   'False
   End
   Begin VB.Label lblFecFin 
      Caption         =   "Fecha de Final:"
      Height          =   255
      Left            =   315
      TabIndex        =   4
      Top             =   405
      Width           =   1650
   End
   Begin VB.Label lblFecInicio 
      Caption         =   "Fecha de Inicio:"
      Height          =   255
      Left            =   315
      TabIndex        =   3
      Top             =   90
      Width           =   1650
   End
   Begin VB.Label lbl2alinea 
      Alignment       =   2  'Center
      Caption         =   "ESPERE POR FAVOR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   270
      TabIndex        =   1
      Top             =   360
      Visible         =   0   'False
      Width           =   5370
   End
   Begin VB.Label lbl1alinea 
      Alignment       =   2  'Center
      Caption         =   "IMPRIMIENDO LOS ACUSES DE RECIBO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   270
      TabIndex        =   0
      Top             =   45
      Visible         =   0   'False
      Width           =   5370
   End
End
Attribute VB_Name = "frm_AcuseRecibos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const ConvX = 54.0809
Const ConvY = 54.6101086

Dim qry(1 To 10) As New rdoQuery

Private Type Resumen
  Historia As Long
  Persona As String
  Pago As Long
End Type

Dim ResumenAcuses() As Resumen

Dim Cont As Integer
Dim UnAcuse As Boolean
Dim Fecha As Date

Sub CuerpoAcuseRecibo(FormaPago As Integer, Cantidad As Double)
Dim Texto As String
Dim MiSql As String
Dim MiRs As rdoResultset
Dim Cadena As String
  
  'Le damos a la fecha el formato xx de xxxxxxxxxxxx de xxxx
  vsPrinter.CurrentX = 50 * ConvX
  vsPrinter.CurrentY = 90 * ConvY
  Cadena = Format(Fecha, "DD")
  Cadena = Cadena & " de "
  Cadena = Cadena & UCase(Format(Fecha, "MMMM"))
  Cadena = Cadena & " de "
  Cadena = Cadena & Format(Fecha, "YYYY")
  vsPrinter.Paragraph = "PAMPLONA " & Cadena
  
  vsPrinter.CurrentX = 50 * ConvX
  vsPrinter.CurrentY = 105 * ConvY
  vsPrinter.Paragraph = "Muy se�or nuestro: "

  'Imprimiremos el texto del acuse de recibo.
  vsPrinter.MarginLeft = 50 * ConvX
  vsPrinter.MarginRight = 35 * ConvX
  vsPrinter.TextAlign = taJustBaseline
  vsPrinter.SpaceBefore = 2 * ConvY
  Texto = Space(7)
  Texto = Texto & "Nos es grato acusar recibo a su env�o de "
  MiSql = "Select FA20DESC From FA2000 Where FA20CODTIPPAGO = " & FormaPago
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    Texto = Texto & LCase(MiRs("FA20DESC")) & " "
  End If
  Texto = Texto & "a nuestro favor de PESETAS, " & Format(Cantidad, "###,###,##0.##")
  Texto = Texto & " correspondiente a la factura relacionada al pie de este escrito, "
  Texto = Texto & "cuyo importe recibimos de conformidad y abonamos en su apreciable cuenta."
   vsPrinter.Paragraph = Texto
   vsPrinter.SpaceBefore = 2 * ConvY
   Texto = Space(7)
   Texto = Texto & "D�ndole las m�s expresivas gracias por su atenci�n, le saludamos atentamente."
   vsPrinter.Paragraph = Texto
   vsPrinter.CurrentX = 100 * ConvX
   vsPrinter.CurrentY = 190 * ConvY
   Texto = "Por la administraci�n."
   vsPrinter.Paragraph = Texto
   
End Sub

Sub DesgloseFacturasRecibo(NoPago As Long, Persona As Long)
Dim MiSql As String
Dim MiRs As rdoResultset
Dim sqlFact As String
Dim rsFact As rdoResultset
Dim Texto As String
Dim Posicion As Integer

  'Seleccionamos las facturas que se han compensado con ese pago.
  MiSql = "Select * From FA1800 Where FA17CODPAGO = " & NoPago
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    Texto = "DETALLE QUE SE CITA"
    vsPrinter.CurrentX = 50 * ConvX
    vsPrinter.CurrentY = 200 * ConvY
    vsPrinter.FontUnderline = True
    vsPrinter.Paragraph = Texto
    vsPrinter.TextAlign = taLeftBaseline
    vsPrinter.CurrentX = 60 * ConvX
    vsPrinter.CurrentY = 210 * ConvY
    vsPrinter.Text = "FECHA"
    vsPrinter.CurrentX = 100 * ConvX
    vsPrinter.CurrentY = 210 * ConvY
    vsPrinter.Text = "N� FACTURA"
    vsPrinter.CurrentX = 170 * ConvX - vsPrinter.TextWidth("IMPORTE")
    vsPrinter.CurrentY = 210 * ConvY
    vsPrinter.Text = "IMPORTE"
    vsPrinter.FontUnderline = False
    MiRs.MoveLast
    MiRs.MoveFirst
    Posicion = 220
    While Not MiRs.EOF
      'vsPrinter.TextAlign = taLeftBaseline
      ''''I�aki 16/02/00, cambiamos la fecha y c�digo de compensaci�n por la de factura.
      If Not IsNull(MiRs("FA04CODFACT")) Then
        sqlFact = "Select FA04NUMFACT, FA04FECFACTURA From FA0400 Where FA04CODFACT = " & MiRs("FA04CODFACT")
        Set rsFact = objApp.rdoConnect.OpenResultset(sqlFact, 3)
        If Not rsFact.EOF Then
          vsPrinter.CurrentX = 60 * ConvX
          vsPrinter.CurrentY = Posicion * ConvY
          vsPrinter.Text = Format(rsFact("FA04FECFACTURA"), "DD/MM/YYYY")
          vsPrinter.CurrentX = 100 * ConvX
          vsPrinter.CurrentY = Posicion * ConvY
          vsPrinter.Text = rsFact("FA04NUMFACT")
          'vsPrinter.TextAlign = taRightBaseline
          vsPrinter.CurrentX = 170 * ConvX - vsPrinter.TextWidth(Format(MiRs("FA18IMPCOMP"), "###,###,##0"))
          vsPrinter.CurrentY = Posicion * ConvY
          'vsPrinter.CurrentX = 200 * ConvX
          vsPrinter.Text = Format(MiRs("FA18IMPCOMP"), "###,###,##0")
          Posicion = Posicion + 6
          If Posicion > 270 Then
            Call NuevaPagina
            Posicion = 100
            Call Me.EncabezadoPagina(Persona)
          End If
        End If
      End If
'      'Fecha de Compensacion.
'      vsPrinter.Text = Format(MiRs("FA18FECCOMP"), "DD/MM/YYYY")
'      vsPrinter.CurrentX = 100 * ConvX
'      vsPrinter.CurrentY = Posicion * ConvY
'      'C�digo de factura
'      vsPrinter.Text = "13/" & MiRs("FA04CODFACT")
'      vsPrinter.TextAlign = taRightBaseline
'      vsPrinter.CurrentX = 140 * ConvX
'      vsPrinter.CurrentY = Posicion * ConvY
      'Importe compensado.
      MiRs.MoveNext
    Wend
  End If
End Sub

Sub ImprimirAcuseRecibo(NoPago As Long, Responsable As Long, FormaPago As Integer, Cantidad As Double, Fecha As Date)
  vsPrinter.Preview = False
  vsPrinter.Action = 3 ' StartDoc
  Call EncabezadoPagina(Responsable)
  Call CuerpoAcuseRecibo(FormaPago, Cantidad)
  Call DesgloseFacturasRecibo(NoPago, Responsable)
  vsPrinter.Action = 6
End Sub




Sub EncabezadoPagina(Persona As Long)
Dim MiSql As String
Dim rsBuscar As rdoResultset
Dim Nombre As String
Dim Direccion As String
Dim Poblacion As String
Dim Provincia As String
Dim RSFECHA As rdoResultset
Dim Paciente As New Persona
Dim Seguir As Boolean

    'Buscamos los datos relativos a la persona.
    'qry(1).rdoParameters(0) = Persona
    Paciente.Codigo = Persona
    'Set rsBuscar = qry(1).OpenResultset
    'If Not rsBuscar.EOF Then
    '    If Not IsNull(rsBuscar("CI34DESTRATAMI")) Then
    '        Nombre = "" & rsBuscar("CI34DESTRATAMI")
    '    Else
    '        ' si no tiene tratamiento ponerlo por defecto seg�n el sexo
    '        If rsBuscar("CI30CODSEXO") = 1 Then
    '            Nombre = "Sr. D."
    '        Else
    '            Nombre = "Sra. D�a."
    '        End If
    '    End If
    '
    '  Nombre = Nombre & " " & rsBuscar("CI22NOMBRE") & " " & rsBuscar("CI22PRIAPEL") & " " & rsBuscar("CI22SEGAPEL")
    '  Direccion = rsBuscar("CI10CALLE") & " " & rsBuscar("CI10PORTAL") & " " & rsBuscar("CI10RESTODIREC")
    '  Poblacion = rsBuscar("CI07CODPOSTAL") & " " & rsBuscar("CI10DESLOCALID")
    '  Provincia = "" & rsBuscar("CI26DESPROVI")
    If Paciente.Tratamiento <> "" Then
      Nombre = Paciente.Tratamiento & " " & Paciente.Name
    Else
      Nombre = Paciente.Name
    End If
    Direccion = Paciente.Direccion
    Poblacion = Paciente.CPPoblac '& " " & Paciente.Poblacion
    Provincia = "" 'Paciente.Provincia
    'Else
    '  Nombre = ""
    '  Direccion = ""
    '  Poblacion = ""
    '  Provincia = ""
    'End If
    
    'Asignamos al array de resumen el n� de historia y el nombre de la persona
    'No lo haremos si s�lo vamos a emitir un acuse a un determinado pago
    If UnAcuse = False Then
      ResumenAcuses(Cont).Historia = 0 & Paciente.NumHistoria
      ResumenAcuses(Cont).Persona = Paciente.Name
    End If
    
    vsPrinter.Preview = False
    With vsPrinter
        .TextColor = QBColor(0)
        '-------------------------------------------------------
        ' REALIZAMOS LA IMPRESI�N DE LA CABECERA
        '-------------------------------------------------------
        'Imprimimos el escudo de la cl�nica.
        .X1 = 35 * ConvX
        .Y1 = 5 * ConvY
        .X2 = 59 * ConvX
        .Y2 = 35 * ConvY
        .Picture = Me.Picture1.Picture
                
        'Imprimimos el texto de debajo del escudo CLINICA...
        .FontName = "Arial"
        .CurrentX = 0 * ConvX
        .CurrentY = 35 * ConvY
        .MarginLeft = 0
        'Le asignamos un gran margen derecho y le decimos que lo centre
        .MarginRight = 120 * ConvX
        .FontSize = 14
        .TextAlign = taCenterBaseline
        .FontName = "Times New Roman"
        .Paragraph = "CLINICA UNIVERSITARIA"
        .SpaceAfter = 2 * ConvX
        .FontSize = 8
        .FontBold = False
        .CurrentY = 40 * ConvY
        .Paragraph = "FACULTAD DE MEDICINA"
        .CurrentY = 43 * ConvY
        .Paragraph = "UNIVERSIDAD DE NAVARRA"
        .DrawLine 39 * ConvX, 45 * ConvY, 49 * ConvX, 45 * ConvY
        .CurrentY = 49 * ConvY
        .FontName = "Arial"
        .FontBold = True
        .Paragraph = "ADMINISTRACION"
        .FontBold = False
        .TextAlign = taLeftBaseline
        .MarginRight = 0
        
        'Imprimimos la direcci�n y los tel�fonos (Parte derecha del encabezado)
        .CurrentX = 140 * ConvX
        .FontSize = 8
        .CurrentY = 16 * ConvY
        .Paragraph = "Avda. P�o XII, 36"
        .CurrentX = 140 * ConvX
        .CurrentY = 19 * ConvY
        .Paragraph = "Apartado, 4209"
        .CurrentX = 140 * ConvX
        .CurrentY = 25 * ConvY
        .Paragraph = "Tel�fonos:"
        .CurrentX = 140 * ConvX
        .CurrentY = 28 * ConvY
        .Paragraph = "Centralita 948.25.54.00"
        .CurrentX = 140 * ConvX
        .CurrentY = 31 * ConvY
        .Paragraph = "Administraci�n 948.29.63.94"
        .CurrentX = 140 * ConvX
        .CurrentY = 37 * ConvY
        .Paragraph = "Fax: 948.29.65.00"
        .CurrentX = 140 * ConvX
        .CurrentY = 40 * ConvY
        .Paragraph = "C.I.F.: Q.3168001 J"
        .CurrentX = 140 * ConvX
        .CurrentY = 43 * ConvY
        .Paragraph = "31080 PAMPLONA (ESPA�A)"
        
        .FontName = "Courier New"
        .FontSize = 11
        .CurrentX = 100 * ConvX
        .CurrentY = 59 * ConvY
        Seguir = False
        While Seguir = False
          If .CurrentX + .TextWidth(Nombre) > 10500 Then
            Nombre = Left(Nombre, Len(Nombre) - 1)
          Else
            Seguir = True
          End If
        Wend
        .Paragraph = Nombre
        .CurrentX = 100 * ConvX
        .CurrentY = 63 * ConvY
        Seguir = False
        While Seguir = False
          If .CurrentX + .TextWidth(Direccion) > 10500 Then
            Direccion = Left(Direccion, Len(Direccion) - 1)
          Else
            Seguir = True
          End If
        Wend
        .Paragraph = Direccion
        .CurrentX = 100 * ConvX
        .CurrentY = 67 * ConvY
        Seguir = False
        While Seguir = False
          If .CurrentX + .TextWidth(Poblacion) > 10500 Then
            Poblacion = Left(Poblacion, Len(Poblacion) - 1)
          Else
            Seguir = True
          End If
        Wend
        .Paragraph = Poblacion
        .CurrentX = 100 * ConvX
        .CurrentY = 71 * ConvY
        .Paragraph = Paciente.Pais
        
      End With
End Sub
Sub ImprimirResumen()
Dim Texto As String
Dim x As Integer
Dim ContPags
  
  ContPags = 1
  With vsPrinter1
    .FontName = "Courier New"
    .FontSize = 11
    .MarginLeft = 1500
    .Preview = False
    .Action = 3 ' StartDoc
    .FontUnderline = True
    Texto = "RESUMEN ACUSES DE RECIBO DE FECHA " & Format(Me.SDCFechaInicio, "DD/MM/YYYY")
    Texto = Texto & Space(10)
    Texto = Texto & "P�g.: " & ContPags
    .Paragraph = Texto
    .FontUnderline = False
    .Paragraph = ""
    Texto = " N.PAGO  HISTORIA           NOMBRE O RAZ�N SOCIAL         "
    .Paragraph = Texto
    Texto = "-------- -------- ----------------------------------------"
    .Paragraph = Texto
    For x = 1 To UBound(ResumenAcuses)
      Texto = Right(Space(7) & ResumenAcuses(x).Pago, 7) & "  "
      Texto = Texto & Right(Space(7) & ResumenAcuses(x).Historia, 7) & "  "
      Texto = Texto & Left(ResumenAcuses(x).Persona & Space(40), 40)
      .Paragraph = Texto
      If .CurrentY > 260 * 54.6101086 Then
        ContPags = ContPags + 1
        .NewPage
        .FontUnderline = True
        Texto = "RESUMEN ACUSES DE RECIBO DE FECHA " & Format(Me.SDCFechaInicio, "DD/MM/YYYY")
        Texto = Texto & Space(10)
        Texto = Texto & "P�g.: " & ContPags
        .Paragraph = Texto
        .FontUnderline = False
        .Paragraph = ""
        Texto = " N.PAGO  HISTORIA           NOMBRE O RAZ�N SOCIAL         "
        .Paragraph = Texto
        Texto = "-------- -------- ----------------------------------------"
        .Paragraph = Texto
      End If
    Next
    .Paragraph = ""
    Texto = "TOTAL DE ACUSES IMPRESOS: " & UBound(ResumenAcuses)
    .Paragraph = Texto
    .Action = 6
  End With
End Sub


Sub ImprimirUno(Pago As Long, Persona As Long, TipoPago As Integer, Cantidad As Double, Fecha As Date)

  UnAcuse = True
  Call Me.ImprimirAcuseRecibo(Pago, Persona, TipoPago, Cantidad, Fecha)

End Sub

Sub IniciarQRY()
Dim MiSql As String
Dim x As Integer
  'Seleccionamos los datos personales de una persona.
  MiSql = " Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI22NUMDIRPRINC, " & _
              " CI10CALLE, CI10PORTAL, CI10RESTODIREC,  CI07CODPOSTAL, CI10DESLOCALID, " & _
              " CI26DESPROVI, CI34DESTRATAMI, CI2200.CI22NUMHISTORIA, CI22DNI, CI30CODSEXO, " & _
              " CI1000.CI19CODPAIS, DECODE(CI1000.CI19CODPAIS,45,'',CI19DESPAIS) DESPAIS " & _
              " from CI2200, CI1000,CI3400,CI2600, CI1900 " & _
              " where CI2200.CI21CODPERSONA = ?" & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA(+)  " & _
              " AND CI1000.CI10INDDIRPRINC = -1)" & _
              " AND (CI2200.CI34CODTRATAMI = CI3400.CI34CODTRATAMI(+))" & _
              " AND (CI2200.CI26CODPROVI = CI2600.CI26CODPROVI(+))" & _
              " AND (CI1000.CI19CODPAIS = CI1900.CI19CODPAIS (+))"
  qry(1).SQL = MiSql

  'Seleccionamos los datos de una Entidad que sea responsable econ�mico
  MiSql = " Select CI21CODPERSONA From CI0900 Where CI13CODENTIDAD = ?"
  qry(2).SQL = MiSql
  
  MiSql = " Select CI23RAZONSOCIAL, CI23NUMDIRPRINC, CI10CALLE, CI10PORTAL," & _
              " CI10RESTODIREC,CI07CODPOSTAL,CI10DESLOCALID,CI26DESPROVI" & _
              " CI1000.CI19CODPAIS, DECODE(CI1000.CI19CODPAIS,45,'',CI19DESPAIS) DESPAIS " & _
              " From CI2300, CI1000, CI0900,CI2600, CI1900 " & _
              " Where CI2300.CI21CODPERSONA = ?" & _
              " And (CI2300.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
              " And CI1000.CI10INDDIRPRINC = -1) " & _
              " AND (CI2200.CI26CODPROVI = CI2600.CI26CODPROVI(+))" & _
              " AND (CI1000.CI19CODPAIS = CI1900.CI19CODPAIS (+))"
  qry(3).SQL = MiSql
  
  
  For x = 1 To 3
    Set qry(x).ActiveConnection = objApp.rdoConnect
    qry(x).Prepared = True
  Next

End Sub


Sub NuevaPagina()

  vsPrinter.Action = paEndDoc
  vsPrinter.Action = paStartDoc
  
End Sub

Private Sub cmdImprimir_Click()
Dim MiSql As String
Dim MiRs As rdoResultset
Dim NoAcuses As Long
Dim MiSqlACU As String
Dim x As Integer

  'Ocultamos los objetos de selecci�n y mostramos las etiquetas y el progress bar
  Me.lblFecInicio.Visible = False
  Me.lblFecFin.Visible = False
  Me.SDCFechaInicio.Visible = False
  Me.SDCFechaFin.Visible = False
  Me.lbl1alinea.Visible = True
  Me.lbl2alinea.Visible = True
  Me.cmdImprimir.Visible = False
  Me.PrgBar1.Visible = True
  UnAcuse = False
  DoEvents
  'Seleccionamos los pagos de los que debemos lanzar acuse de recibo
  MiSql = "Select * From FA1700 Where FA17FECCIERRE >= TO_DATE('" & Me.SDCFechaInicio.Date & "','DD/MM/YYYY')" & _
              " AND FA17FECCIERRE <= TO_DATE('" & Me.SDCFechaFin.Date & "','DD/MM/YYYY')" & _
              " AND FA17INDACUSE = 1 AND FA17INDACUENVI = 0 "
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    NoAcuses = MiRs.RowCount
    PrgBar1.Min = 0
    PrgBar1.Max = NoAcuses
    Cont = 0
    While Not MiRs.EOF
      Cont = Cont + 1
      ReDim Preserve ResumenAcuses(1 To Cont)
      'Asignamos al array de resumen de acuses el n� del pago
      ResumenAcuses(Cont).Pago = MiRs("FA17CODPAGO")
      'Imprimimos el acuse de recibo.
      Call ImprimirAcuseRecibo(MiRs("FA17CODPAGO"), MiRs("CI21CODPERSONA"), MiRs("FA20CODTIPPAGO"), MiRs("FA17CANTIDAD"), MiRs("FA17FECPAGO"))
      'Le indicamos que ya se ha enviado el acuse de recibo.
      MiSqlACU = "UPDATE FA1700 SET FA17INDACUENVI = 1 WHERE FA17CODPAGO = " & MiRs("FA17CODPAGO")
      'objApp.rdoConnect.Execute MiSqlACU
      MiRs.MoveNext
      PrgBar1.Value = Cont
    Wend
  End If
  Call ImprimirResumen
  For x = 1 To UBound(ResumenAcuses)
    Debug.Print ResumenAcuses(x).Pago & " " & ResumenAcuses(x).Historia & ResumenAcuses(x).Persona
  Next
  MsgBox "Proceso de emisi�n finalizado", vbOKOnly + vbInformation, "Emisi�n de Acuses de Recibo"
  'Ocultamos los objetos de selecci�n y mostramos las etiquetas y el progress bar
  Me.lblFecInicio.Visible = True
  Me.lblFecFin.Visible = True
  Me.SDCFechaInicio.Visible = True
  Me.SDCFechaFin.Visible = True
  Me.lbl1alinea.Visible = False
  Me.lbl2alinea.Visible = False
  Me.cmdImprimir.Visible = True
  Me.PrgBar1.Visible = False
  DoEvents
  
End Sub


Private Sub Form_Load()
Dim RSFECHA As rdoResultset

  Set RSFECHA = objApp.rdoConnect.OpenResultset("Select SYSDATE From Dual")
  If Not RSFECHA.EOF Then
    Fecha = RSFECHA(0)
  End If

  Call IniciarQRY
End Sub

