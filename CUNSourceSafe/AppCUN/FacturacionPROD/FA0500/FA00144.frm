VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_CtaCte 
   Caption         =   "Cuenta corriente"
   ClientHeight    =   6135
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8925
   LinkTopic       =   "Form1"
   ScaleHeight     =   6135
   ScaleWidth      =   8925
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture1 
      Height          =   120
      Left            =   0
      Picture         =   "FA00144.frx":0000
      ScaleHeight     =   120
      ScaleWidth      =   30
      TabIndex        =   10
      Top             =   0
      Visible         =   0   'False
      Width           =   30
   End
   Begin VB.Frame Frame3 
      Height          =   1095
      Left            =   6480
      TabIndex        =   8
      Top             =   60
      Width           =   2130
      Begin VB.CheckBox chkTodos 
         Caption         =   "Mostrar Totales"
         Height          =   195
         Left            =   240
         TabIndex        =   13
         Top             =   780
         Width           =   1455
      End
      Begin VB.OptionButton optPdtes 
         Caption         =   "Pendientes"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   480
         Width           =   1755
      End
      Begin VB.OptionButton optTodos 
         Caption         =   "Todos"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   180
         Value           =   -1  'True
         Width           =   1755
      End
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Height          =   495
      Left            =   4185
      TabIndex        =   7
      Top             =   450
      Width           =   1935
   End
   Begin VB.Frame Frame2 
      Caption         =   " Cuenta Corriente "
      Height          =   4725
      Left            =   135
      TabIndex        =   1
      Top             =   1290
      Width           =   8730
      Begin SSDataWidgets_B.SSDBGrid grdCuentaCte 
         Height          =   4410
         Left            =   135
         TabIndex        =   2
         Top             =   225
         Width           =   8475
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   5
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   2117
         Columns(0).Caption=   "Fecha"
         Columns(0).Name =   "Fecha"
         Columns(0).Alignment=   1
         Columns(0).CaptionAlignment=   2
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6244
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Desc"
         Columns(1).CaptionAlignment=   2
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1773
         Columns(2).Caption=   "Debe"
         Columns(2).Name =   "Debe"
         Columns(2).Alignment=   1
         Columns(2).CaptionAlignment=   2
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1773
         Columns(3).Caption=   "Haber"
         Columns(3).Name =   "Haber"
         Columns(3).Alignment=   1
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   2117
         Columns(4).Caption=   "Saldo"
         Columns(4).Name =   "Saldo"
         Columns(4).Alignment=   1
         Columns(4).CaptionAlignment=   2
         Columns(4).AllowSizing=   0   'False
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   14949
         _ExtentY        =   7779
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   " Selecci�n de Fechas "
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3615
      Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
         DataField       =   "FA02FECINICIO"
         Height          =   285
         Left            =   1740
         TabIndex        =   3
         Tag             =   "Fecha Inicio"
         Top             =   240
         Width           =   1725
         _Version        =   65537
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFin 
         DataField       =   "FA02FECINICIO"
         Height          =   285
         Left            =   1740
         TabIndex        =   4
         Tag             =   "Fecha Inicio"
         Top             =   555
         Width           =   1725
         _Version        =   65537
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.Label lblFecInicio 
         Caption         =   "Fecha de Inicio:"
         Height          =   255
         Left            =   165
         TabIndex        =   6
         Top             =   285
         Width           =   1650
      End
      Begin VB.Label lblFecFin 
         Caption         =   "Fecha de Final:"
         Height          =   255
         Left            =   165
         TabIndex        =   5
         Top             =   600
         Width           =   1650
      End
   End
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   1530
      Left            =   2100
      TabIndex        =   9
      Top             =   4320
      Width           =   3450
      _Version        =   196608
      _ExtentX        =   6085
      _ExtentY        =   2699
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
   End
   Begin VB.CommandButton cmdTexto 
      Caption         =   "&Fichero de Texto"
      Height          =   495
      Left            =   4185
      TabIndex        =   14
      Top             =   675
      Visible         =   0   'False
      Width           =   1935
   End
End
Attribute VB_Name = "frm_CtaCte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Type Lineas
  fecha As Date
  Descrip As String
  Debe As Double
  Haber As Double
  Saldo As Double
End Type

Private Type LineasIMQ
  fecha As Date
  Descrip As String
  Importe As Double
  Nombre As String
End Type

Dim ArrayLineas() As Lineas
Dim ArrayIMQ() As LineasIMQ

Dim SaldoIntermedio As Double
Dim SaldoFacturas As Double
Dim SaldoPagos As Double
Dim SaldoPeriodo As Double
Dim Persona As Long
Dim Nombre As String
Dim Saldo As Double
Dim Lineas As Integer
Dim SaldoDebe As Double
Dim SaldoHaber As Double
Dim SaldoAnt As Double

Const ConvX = 54.0809
Const ConvY = 54.6101086

Dim DatosPersona As New Persona
Dim qry(1 To 10) As New rdoQuery

Function ComprobarFechas() As Boolean

    'Comprobamos que la fecha de inicio sea correcta
    If Not IsNull(Me.SDCFechaInicio.Date) Then
      If Not IsDate(Me.SDCFechaInicio.Date) Then
        MsgBox "La Fecha de Inicio no es correcta.", vbOKOnly + vbInformation, "Aviso"
        If Me.SDCFechaInicio.Enabled = True Then
          Me.SDCFechaInicio.SetFocus
        End If
        ComprobarFechas = False
        Exit Function
      End If
    End If
    
    'Comprobamos que la fecha de finalizaci�n sea correcta
    If Not IsNull(Me.SDCFechaFin.Date) Then
      If Not IsDate(Me.SDCFechaFin.Date) Then
        MsgBox "La Fecha Fin no es correcta.", vbOKOnly + vbInformation, "Aviso"
        If Me.SDCFechaFin.Enabled = True Then
          Me.SDCFechaFin.SetFocus
        End If
        ComprobarFechas = False
        Exit Function
      End If
    End If
    
    'Comprobamos que la fecha de inicio no sea mayor que la de finalizaci�n.
    If CDate(Me.SDCFechaFin.Date) < CDate(Me.SDCFechaInicio.Date) Then
      MsgBox "La fecha de inicio no puede ser superior a la de finalizaci�n.", vbOKOnly + vbInformation, "Aviso"
      If Me.SDCFechaFin.Enabled = True Then
        Me.SDCFechaFin.SetFocus
      End If
      ComprobarFechas = False
      Exit Function
    End If
    
    ComprobarFechas = True
    
End Function


Sub ConsultarCtaCte(CodPersona As Long, NombrePersona As String, SaldoPersona As Double)
  
  Persona = CodPersona
  Nombre = NombrePersona
  SaldoPersona = Saldo
  DatosPersona.Codigo = CodPersona
  Load frm_CtaCte
  'Set frmSeleccion.varFact = objFactura
  frm_CtaCte.Show (vbModal)
  Unload frm_CtaCte

End Sub

Sub EscribirCuentaTXT()

Dim Cont As Integer
Dim Pagina As Integer
Dim Texto As String
Dim NoAbonos As Integer
Dim Tipo As String
Dim ImpreLinea

  SaldoDebe = 0
  SaldoHaber = 0
  NoAbonos = UBound(ArrayLineas, 1)
  Pagina = 1
  If DatosPersona.NumHistoria <> "" Then
    MsgBox "El nombre del fichero de texto es C:\" & DatosPersona.NumHistoria & ".TXT"
    Open "C:\" & DatosPersona.NumHistoria & ".TXT" For Append As #1
  Else
    MsgBox "El nombre del fichero de texto es C:\" & DatosPersona.Codigo & ".TXT"
    Open "C:\" & DatosPersona.Codigo & ".TXT" For Append As #1
  End If
  Texto = Space(5) & "   FECHA          DESCRIPCION            DEBE        HABER        SALDO"
  Write #1, Texto
  Texto = Space(5) & "---------- ------------------------- ------------ ------------ ------------"
  Write #1, Texto
  If Me.optPdtes.Value = False Then
    If SaldoAnt <> 0 Then
      'Texto = Space(37) & "Saldo anterior........... " & Right(Space(12) & Format(SaldoAnt, "###,###,##0.##"), 12)
      If SaldoAnt > 0 Then
        SaldoDebe = SaldoDebe + SaldoAnt
        Texto = Space(5) & Space(10) & " "
        Texto = Texto & Left("Saldo a fecha " & Format(Me.SDCFechaInicio.Date, "DD/MM/YYYY") & "." & Space(25), 25) & " "
        Texto = Texto & Right(Space(12) & Format(SaldoAnt, "###,###,##0.##"), 12) & " "
        Texto = Texto & Right(Space(12), 12) & " "
        Texto = Texto & Right(Space(12) & Format(SaldoAnt, "###,###,##0.##"), 12)
        Write #1, Texto
      ElseIf SaldoAnt < 0 Then
        SaldoHaber = SaldoHaber + SaldoAnt
        Texto = Space(5) & Space(10) & " "
        Texto = Texto & Left("Saldo a fecha " & Format(Me.SDCFechaInicio.Date, "DD/MM/YYYY") & "." & Space(25), 25) & " "
        Texto = Texto & Right(Space(12) & Format(SaldoAnt, "###,###,##0.##"), 12) & " "
        Texto = Texto & Right(Space(12), 12) & " "
        Texto = Texto & Right(Space(12) & Format(SaldoAnt, "###,###,##0.##"), 12)
        Write #1, Texto
      End If
    End If
  End If
  For Cont = 1 To NoAbonos
    Texto = Space(5) & Right(Space(10) & Format(ArrayLineas(Cont).fecha, "DD/MM/YYYY"), 10) & " " 'Fecha
    Texto = Texto & Left(Trim(ArrayLineas(Cont).Descrip) & Space(25), 25) & " "  'Descripci�n
    If ArrayLineas(Cont).Debe <> 0 Then
      SaldoDebe = SaldoDebe + ArrayLineas(Cont).Debe
      Texto = Texto & Right(Space(12) & Format(ArrayLineas(Cont).Debe, "###,###,##0.##"), 12) & " " 'Debe
    Else
      Texto = Texto & Space(13)
    End If
    If ArrayLineas(Cont).Haber <> 0 Then
      SaldoHaber = SaldoHaber + ArrayLineas(Cont).Haber
      Texto = Texto & Right(Space(12) & Format(ArrayLineas(Cont).Haber, "###,###,##0.##"), 12) & " " 'Haber
    Else
      Texto = Texto & Space(13)
    End If
    'If Me.optTodos.Value = True Then
      Texto = Texto & Right(Space(12) & Format(ArrayLineas(Cont).Saldo, "###,###,##0.##"), 12) & " " 'Saldo
    'End If
    If ArrayLineas(Cont).Debe = 0 And ArrayLineas(Cont).Haber = 0 Then
      'No imprimimos la l�nea
    Else
      Write #1, Texto
    End If
  Next
  If chkTodos.Value = 1 Then
    If Me.optTodos.Value = True Then
      Texto = Space(5) & "                                     ------------ ------------ ------------"
      Write #1, Texto
      Texto = Space(5) & Space(37)
      Texto = Texto & Right(Space(12) & Format(SaldoDebe, "###,###,##0.##"), 12) & " "
      Texto = Texto & Right(Space(12) & Format(SaldoHaber, "###,###,##0.##"), 12) & " "
      Texto = Texto & Right(Space(12) & Format((ArrayLineas(Cont - 1).Saldo), "###,###,##0.##"), 12)
      Write #1, Texto
    End If
    Texto = Space(5) & "Saldo a fecha " & Format(Me.SDCFechaFin.Date, "DD/MM/YYYY") & "....: " & Right(Space(12) & Format(ArrayLineas(Cont - 1).Saldo, "###,###,##0.##"), 12)
    'Texto = "Saldo Periodo..........:" & Right(Space(12) & Format(SaldoPeriodo, "###,###,##0.##"), 12)
    Write #1, Texto
  End If
  Close #1
End Sub

Sub GenerarArraysCuentas4()
Dim Sql04 As String
Dim rs04 As rdoResultset
Dim sql17 As String
Dim rs17 As rdoResultset
Dim sql18 As String
Dim rs18 As rdoResultset
Dim Cont As Integer

  'Inicializamos el contador de tama�o del array a 0
  Cont = 0
  SaldoFacturas = 0
  SaldoPagos = 0
  'Seleccionamos todos las facturas de esa persona
  Sql04 = "Select CI21CODPERSONA, FECHA,  from FA1701J Where CI21CODPERSONA = " & Persona & _
          " AND FECHA >= TO_DATE('" & Me.SDCFechaInicio.Date & "','DD/MM/YYYY')" & _
          " AND FECHA <= TO_DATE('" & Me.SDCFechaFin.Date & "','DD/MM/YYYY') ORDER BY ORDEN"
          
  Set rs04 = objApp.rdoConnect.OpenResultset(Sql04, 3)
  If Not rs04.EOF Then
    rs04.MoveLast
    rs04.MoveFirst
    While Not rs04.EOF
      Cont = Cont + 1
      ReDim Preserve ArrayLineas(1 To Cont)
      ArrayLineas(Cont).fecha = CDate(rs04("FA04FECFACTURA"))
      ArrayLineas(Cont).Descrip = "Factura n� " & rs04("FA04NUMFACT")

      If rs04("FA04INDCOMPENSA") = 1 Then
        ArrayLineas(Cont).Descrip = ArrayLineas(Cont).Descrip & "*"
      End If
      'Si la factura es positiva la ponemos en el debe y si es negativa en el haber
      If rs04("FA04CANTFACT") >= 0 Then
        'Calcularemos la suma de las compensaciones de esa factura.
        sql18 = "Select Sum(FA18IMPCOMP) as Suma From FA1800 Where FA04CODFACT = " & rs04("FA04CODFACT")
        Set rs18 = objApp.rdoConnect.OpenResultset(sql18, 3)
        If Not rs18.EOF Then
          ArrayLineas(Cont).Debe = Abs(CLng(rs04("FA04CANTFACT"))) '- CLng("0" & rs18("SUMA"))
          SaldoFacturas = SaldoFacturas + CLng(rs04("FA04CANTFACT")) '- CLng("0" & rs18("SUMA"))
          SaldoDebe = SaldoDebe + CLng(rs04("FA04CANTFACT")) '- CLng("0" & rs18("SUMA"))
        Else
          ArrayLineas(Cont).Debe = Abs(CLng(rs04("FA04CANTFACT")))
          SaldoFacturas = SaldoFacturas + CLng(rs04("FA04CANTFACT"))
          SaldoDebe = SaldoDebe + CLng(rs04("FA04CANTFACT"))
        End If
        ArrayLineas(Cont).Haber = 0
      Else
        ArrayLineas(Cont).Haber = Abs(CLng(rs04("FA04CANTFACT")))
        SaldoFacturas = SaldoFacturas + CLng(rs04("FA04CANTFACT"))
        SaldoHaber = SaldoHaber + CLng(rs04("FA04CANTFACT"))
        ArrayLineas(Cont).Debe = 0
      End If
      rs04.MoveNext
    Wend
  End If
  'Seleccionamos todos los pagos realizados por esa persona
  sql17 = "Select * From FA1700 Where CI21CODPERSONA = " & Persona & _
          " AND FA17FECPAGO >= TO_DATE('" & Me.SDCFechaInicio.Date & "','DD/MM/YYYY')" & _
          " AND FA17FECPAGO <= TO_DATE('" & Me.SDCFechaFin.Date & "','DD/MM/YYYY')"
  Set rs17 = objApp.rdoConnect.OpenResultset(sql17, 3)
  If Not rs17.EOF Then
    rs17.MoveLast
    rs17.MoveFirst
    While Not rs17.EOF
      Cont = Cont + 1
      ReDim Preserve ArrayLineas(1 To Cont)
      ArrayLineas(Cont).fecha = CDate(rs17("FA17FECPAGO"))
      ArrayLineas(Cont).Descrip = "Abono n� " & rs17("FA17CODPAGO")
      'Si el abono es positivo lo ponemos en el haber y si es negativo en el debe
      If CLng(rs17("FA17CANTIDAD")) >= 0 Then
        ArrayLineas(Cont).Haber = Abs(CLng(rs17("FA17CANTIDAD")))
        SaldoPagos = SaldoPagos + CLng(rs17("FA17CANTIDAD"))
        SaldoHaber = SaldoHaber + Abs(CLng(rs17("FA17CANTIDAD")))
        ArrayLineas(Cont).Debe = 0
      Else
        ArrayLineas(Cont).Debe = Abs(CLng(rs17("FA17CANTIDAD")))
        SaldoPagos = SaldoPagos + CLng(rs17("FA17CANTIDAD"))
        SaldoDebe = SaldoDebe + Abs(CLng(rs17("FA17CANTIDAD")))
        ArrayLineas(Cont).Haber = 0
      End If
    rs17.MoveNext
    Wend
  End If
  Lineas = Cont
End Sub
Sub GenerarArraysCuentas()
Dim MiSql As String
Dim rs04 As rdoResultset
Dim sql17 As String
Dim rs17 As rdoResultset
Dim sql18 As String
Dim rs18 As rdoResultset
Dim Cont As Integer

  'Inicializamos el contador de tama�o del array a 0
  Cont = 0
  SaldoFacturas = 0
  SaldoPagos = 0
  'Seleccionamos todos las facturas de esa persona
  MiSql = "Select CI21CODPERSONA, DECODE(FAC_ABO,1,'Factura n� '||COD,2,'Abono n� '||COD) AS DESCR, " & _
          "FECHA, DEBE, HABER, SALDO " & _
          "From FA1701J " & _
          "Where CI21CODPERSONA = " & Persona & " " & _
          "And FECHA >= TO_DATE('" & Me.SDCFechaInicio.Date & "','DD/MM/YYYY') " & _
          "And FECHA <= TO_DATE('" & Me.SDCFechaFin.Date & "','DD/MM/YYYY') " & _
          "ORDER BY ORDEN"
  Set rs04 = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not rs04.EOF Then
    rs04.MoveLast
    rs04.MoveFirst
    While Not rs04.EOF
      Cont = Cont + 1
      ReDim Preserve ArrayLineas(1 To Cont)
      ArrayLineas(Cont).fecha = CDate(rs04("FECHA"))
      ArrayLineas(Cont).Descrip = rs04("DESCR")
      ArrayLineas(Cont).Debe = CLng(rs04("DEBE"))
      ArrayLineas(Cont).Haber = CLng(rs04("HABER"))
      ArrayLineas(Cont).Saldo = CLng(rs04("SALDO"))
      If Left(ArrayLineas(Cont).Descrip, 1) = "F" Then
        SaldoFacturas = SaldoFacturas + CLng(rs04("DEBE")) - CLng(rs04("HABER"))
      ElseIf Left(ArrayLineas(Cont).Descrip, 1) = "A" Then
        SaldoPagos = SaldoPagos - CLng(rs04("DEBE")) + CLng(rs04("HABER"))
      End If
      rs04.MoveNext
    Wend
  End If
  Lineas = Cont
End Sub

Sub GenerarArraysIMQ()
Dim Sql01 As String
Dim rs01 As rdoResultset
Dim Sql04 As String
Dim rs04 As rdoResultset
Dim sql17 As String
Dim rs17 As rdoResultset
Dim sql18 As String
Dim rs18 As rdoResultset
Dim Cont As Integer
Dim Pdte As Double
Dim Proc As Long

  'Inicializamos el contador de tama�o del array a 0
  Cont = 0
  SaldoFacturas = 0
  SaldoPagos = 0
  'Seleccionamos todos las facturas de esa persona
  Sql04 = "Select * from FA0400 Where CI21CODPERSONA = 800022 And FA04NUMFACREAL IS NULL" & _
          " AND FA04FECFACTURA >= TO_DATE('" & Me.SDCFechaInicio.Date & "','DD/MM/YYYY')" & _
          " AND FA04FECFACTURA <= TO_DATE('" & Me.SDCFechaFin.Date & "','DD/MM/YYYY')" & _
          " AND FA04INDCOMPENSA <> 2 AND FA04CANTFACT <> 0"
  Set rs04 = objApp.rdoConnect.OpenResultset(Sql04, 3)
  If Not rs04.EOF Then
    rs04.MoveLast
    rs04.MoveFirst
    While Not rs04.EOF
      Cont = Cont + 1
      ReDim Preserve ArrayIMQ(1 To Cont)
      ArrayIMQ(Cont).fecha = CDate(rs04("FA04FECFACTURA"))
      ArrayIMQ(Cont).Descrip = "F " & rs04("FA04NUMFACT")
      'Si la factura es positiva la ponemos en el debe y si es negativa en el haber
      ArrayIMQ(Cont).Importe = CLng(rs04("FA04CANTFACT"))
      SaldoFacturas = SaldoFacturas + CLng(rs04("FA04CANTFACT"))
      SaldoDebe = SaldoDebe + Abs(CLng(rs04("FA04CANTFACT")))
      'Seleccionamos los datos de esa persona
      Proc = 0 & rs04("AD07CODPROCESO")
      If Proc <> 0 Then
        Sql01 = "Select CI22PRIAPEL|| ' ' ||CI22SEGAPEL||' '|| CI22NOMBRE As NOMBRE "
        Sql01 = Sql01 & " From CI2200, AD0700, FA0400 "
        Sql01 = Sql01 & " Where FA0400.AD07CODPROCESO = " & Proc
        Sql01 = Sql01 & " And AD0700.AD07CODPROCESO = FA0400.AD07CODPROCESO"
        Sql01 = Sql01 & " And CI2200.CI21CODPERSONA = AD0700.CI21CODPERSONA"
        Set rs01 = objApp.rdoConnect.OpenResultset(Sql01, 3)
        If Not rs01.EOF Then
          If Not IsNull(rs01("nombre")) Then
          ArrayIMQ(Cont).Nombre = rs01("nombre") & ""
          Debug.Print ArrayIMQ(Cont).Nombre
          End If
        Else
          ArrayIMQ(Cont).Nombre = ""
        End If
      Else
        ArrayIMQ(Cont).Nombre = "MAEZTU ZUDAIRE FULGENCIO"
      End If
      rs04.MoveNext
    Wend
  End If
  Lineas = Cont
  chkTodos.Value = 0
End Sub

Sub GenerarArraysPdtes()
Dim MiSql As String
Dim rs04 As rdoResultset
Dim Saldo As Double

  'Inicializamos el tama�o del array a 0
  Cont = 0
  SaldoFacturas = 0
  SaldoPagos = 0
  Saldo = 0
  'Seleccionamos todas las facturas de esa persona
  MiSql = "Select CI21CODPERSONA, DECODE(TR,1,'Factura n� '||CO,2,'Abono n� '||CO) AS DESCR, " & _
          "FECHA, DEBE, HABER From FA1702J " & _
          "Where CI21CODPERSONA = " & Persona & _
          "And FECHA >= TO_DATE('" & Me.SDCFechaInicio.Date & "','DD/MM/YYYY') " & _
          "And FECHA <= TO_DATE('" & Me.SDCFechaFin.Date & "','DD/MM/YYYY') " & _
          "Order by FECHA"
  Set rs04 = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not rs04.EOF Then
    ReDim ArrayLineas(1 To 1)
    While Not rs04.EOF
      Cont = Cont + 1
      ReDim Preserve ArrayLineas(1 To Cont)
      ArrayLineas(Cont).fecha = CDate(rs04("FECHA"))
      ArrayLineas(Cont).Descrip = rs04("DESCR")
      ArrayLineas(Cont).Debe = rs04("DEBE")
      ArrayLineas(Cont).Haber = rs04("HABER")
      Saldo = Saldo + ArrayLineas(Cont).Debe - ArrayLineas(Cont).Haber
      ArrayLineas(Cont).Saldo = Saldo
      If Left(ArrayLineas(Cont).Descrip, 1) = "A" Then
        SaldoPagos = SaldoPagos + ArrayLineas(Cont).Debe - ArrayLineas(Cont).Haber
      ElseIf Left(ArrayLineas(Cont).Descrip, 1) = "F" Then
        SaldoFacturas = SaldoFacturas + ArrayLineas(Cont).Debe - ArrayLineas(Cont).Haber
      End If
      rs04.MoveNext
    Wend
  End If
  Lineas = Cont
  chkTodos.Value = 0
      
End Sub

Sub ImprimirCabecera()
Dim Trat As String

  '------------------------------------------------------------------------
  ' REALIZAMOS LA IMPRESI�N DE LA CABECERA Y EL LATERAL
  '------------------------------------------------------------------------
  
  'Imprimimos el escudo de la cl�nica.
  vsPrinter1.X1 = 35 * ConvX
  vsPrinter1.Y1 = 5 * ConvY
  vsPrinter1.X2 = 59 * ConvX
  vsPrinter1.Y2 = 35 * ConvY
  vsPrinter1.Picture = Me.Picture1.Picture
          
  'Imprimimos el texto de debajo del escudo CLINICA...
  vsPrinter1.FontName = "Arial"
  vsPrinter1.CurrentX = 0 * ConvX
  vsPrinter1.CurrentY = 35 * ConvY
  vsPrinter1.MarginLeft = 0
  'Le asignamos un gran margen derecho y le decimos que lo centre
  vsPrinter1.MarginRight = 120 * ConvX
  vsPrinter1.FontSize = 14
  vsPrinter1.TextAlign = taCenterBaseline
  vsPrinter1.FontName = "Times New Roman"
  vsPrinter1.Paragraph = "CLINICA UNIVERSITARIA"
  vsPrinter1.SpaceAfter = 2 * ConvX
  vsPrinter1.FontSize = 8
  vsPrinter1.FontBold = False
  vsPrinter1.CurrentY = 40 * ConvY
  vsPrinter1.Paragraph = "FACULTAD DE MEDICINA"
  vsPrinter1.CurrentY = 43 * ConvY
  vsPrinter1.Paragraph = "UNIVERSIDAD DE NAVARRA"
  vsPrinter1.DrawLine 39 * ConvX, 45 * ConvY, 49 * ConvX, 45 * ConvY
  vsPrinter1.CurrentY = 49 * ConvY
  vsPrinter1.FontName = "Arial"
  vsPrinter1.FontBold = True
  vsPrinter1.Paragraph = "ADMINISTRACION"
  vsPrinter1.FontBold = False
  vsPrinter1.TextAlign = taLeftBaseline
  vsPrinter1.MarginRight = 0
  
  'Imprimimos la direcci�n y los tel�fonos (Parte derecha del encabezado)
  vsPrinter1.CurrentX = 140 * ConvX
  vsPrinter1.FontSize = 8
  vsPrinter1.CurrentY = 20 * ConvY
  vsPrinter1.Paragraph = "Avda. P�o XII, 36"
  vsPrinter1.CurrentX = 140 * ConvX
  vsPrinter1.CurrentY = 23 * ConvY
  vsPrinter1.Paragraph = "Apartado, 4209"
  vsPrinter1.CurrentX = 140 * ConvX
  vsPrinter1.CurrentY = 29 * ConvY
  vsPrinter1.Paragraph = "Tel�fonos:"
  vsPrinter1.CurrentX = 140 * ConvX
  vsPrinter1.CurrentY = 32 * ConvY
  vsPrinter1.Paragraph = "Centralita 948.25.54.00"
  vsPrinter1.CurrentX = 140 * ConvX
  vsPrinter1.CurrentY = 35 * ConvY
  vsPrinter1.Paragraph = "Administraci�n 948.29.63.94"
  vsPrinter1.CurrentX = 140 * ConvX
  vsPrinter1.CurrentY = 41 * ConvY
  vsPrinter1.Paragraph = "Fax: 948.29.65.00"
  vsPrinter1.CurrentX = 140 * ConvX
  vsPrinter1.CurrentY = 44 * ConvY
  vsPrinter1.Paragraph = "C.I.F.: Q.3168001 J"
  vsPrinter1.CurrentX = 140 * ConvX
  vsPrinter1.CurrentY = 47 * ConvY
  vsPrinter1.Paragraph = "31080 PAMPLONA"
  vsPrinter1.CurrentX = 140 * ConvX
  vsPrinter1.CurrentY = 50 * ConvY
  vsPrinter1.Paragraph = "(ESPA�A)"
  
  vsPrinter1.CurrentY = 60 * ConvY
  vsPrinter1.FontSize = 11
  vsPrinter1.MarginRight = 0
  vsPrinter1.FontName = "Courier New"
  vsPrinter1.Paragraph = ""
  'Recortamos el tratamiento de Sr. D. a D. y de Sra. D�a. a D�a.
  If DatosPersona.Tratamiento = "Sr. D." Then
    Trat = "D."
  ElseIf DatosPersona.Tratamiento = "Sra. D�a." Then
    Trat = "D�a."
  End If
  If DatosPersona.Tratamiento <> "" Then
    vsPrinter1.Paragraph = Space(5) & "Cuenta Corriente de " & Trat & " " & DatosPersona.Name & " (" & DatosPersona.NumHistoria & ")"
  Else
    vsPrinter1.Paragraph = Space(5) & "Cuenta Corriente de " & DatosPersona.Name & " (" & DatosPersona.Codigo & ")"
  End If
  
  vsPrinter1.Paragraph = Space(5) & "Fecha: " & Format(Me.SDCFechaFin.Date, "dd/mm/yyyy")
  
End Sub

Sub ImprimirCuenta()

Dim Cont As Integer
Dim Pagina As Integer
Dim Texto As String
Dim NoAbonos As Integer
Dim Tipo As String
Dim ImpreLinea

  SaldoDebe = 0
  SaldoHaber = 0
  NoAbonos = UBound(ArrayLineas, 1)
  Pagina = 1
  vsPrinter1.MarginLeft = 5000
  vsPrinter1.MarginRight = 0
  vsPrinter1.Preview = False
  vsPrinter1.Action = 3
  Call ImprimirCabecera
  vsPrinter1.Paragraph = ""
  vsPrinter1.Paragraph = ""
  Texto = Space(5) & "   FECHA          DESCRIPCION            DEBE        HABER        SALDO"
  vsPrinter1.Paragraph = Texto
  Texto = Space(5) & "---------- ------------------------- ------------ ------------ ------------"
  vsPrinter1.Paragraph = Texto
  If Me.optPdtes.Value = False Then
    If SaldoAnt <> 0 Then
      'Texto = Space(37) & "Saldo anterior........... " & Right(Space(12) & Format(SaldoAnt, "###,###,##0.##"), 12)
      If SaldoAnt > 0 Then
        SaldoDebe = SaldoDebe + SaldoAnt
        Texto = Space(5) & Space(10) & " "
        Texto = Texto & Left("Saldo a fecha " & Format(Me.SDCFechaInicio.Date, "DD/MM/YYYY") & "." & Space(25), 25) & " "
        Texto = Texto & Right(Space(12) & Format(SaldoAnt, "###,###,##0.##"), 12) & " "
        Texto = Texto & Right(Space(12), 12) & " "
        Texto = Texto & Right(Space(12) & Format(SaldoAnt, "###,###,##0.##"), 12)
        vsPrinter1.Paragraph = Texto
      ElseIf SaldoAnt < 0 Then
        SaldoHaber = SaldoHaber + SaldoAnt
        Texto = Space(5) & Space(10) & " "
        Texto = Texto & Left("Saldo a fecha " & Format(Me.SDCFechaInicio.Date, "DD/MM/YYYY") & "." & Space(25), 25) & " "
        Texto = Texto & Right(Space(12) & Format(SaldoAnt, "###,###,##0.##"), 12) & " "
        Texto = Texto & Right(Space(12), 12) & " "
        Texto = Texto & Right(Space(12) & Format(SaldoAnt, "###,###,##0.##"), 12)
        vsPrinter1.Paragraph = Texto
      End If
    End If
  End If
  For Cont = 1 To NoAbonos
    Texto = Space(5) & Right(Space(10) & Format(ArrayLineas(Cont).fecha, "DD/MM/YYYY"), 10) & " " 'Fecha
    Texto = Texto & Left(Trim(ArrayLineas(Cont).Descrip) & Space(25), 25) & " "  'Descripci�n
    If ArrayLineas(Cont).Debe <> 0 Then
      SaldoDebe = SaldoDebe + ArrayLineas(Cont).Debe
      Texto = Texto & Right(Space(12) & Format(ArrayLineas(Cont).Debe, "###,###,##0.##"), 12) & " " 'Debe
    Else
      Texto = Texto & Space(13)
    End If
    If ArrayLineas(Cont).Haber <> 0 Then
      SaldoHaber = SaldoHaber + ArrayLineas(Cont).Haber
      Texto = Texto & Right(Space(12) & Format(ArrayLineas(Cont).Haber, "###,###,##0.##"), 12) & " " 'Haber
    Else
      Texto = Texto & Space(13)
    End If
    'If Me.optTodos.Value = True Then
      Texto = Texto & Right(Space(12) & Format(ArrayLineas(Cont).Saldo, "###,###,##0.##"), 12) & " " 'Saldo
    'End If
    If ArrayLineas(Cont).Debe = 0 And ArrayLineas(Cont).Haber = 0 Then
      'No imprimimos la l�nea
    Else
      vsPrinter1.Paragraph = Texto
    End If
    If vsPrinter1.CurrentY > 260 * 54.6101086 Then
      'Cuando llegamos al final de la p�gina pasamos a la siguiente.
      'Y Volvemos a imprimir la cabecera.
      Texto = Space(5) & Space(11) & "Sigue..................."
      vsPrinter1.Paragraph = Texto
      vsPrinter1.NewPage
      Call ImprimirCabecera
      vsPrinter1.Paragraph = ""
      vsPrinter1.Paragraph = ""
      Pagina = Pagina + 1
      Texto = Space(5) & "   FECHA          DESCRIPCION            DEBE        HABER        SALDO"
      vsPrinter1.Paragraph = Texto
      Texto = Space(5) & "---------- ------------------------- ------------ ------------ ------------"
      vsPrinter1.Paragraph = Texto
      'If ArrayLineas(Cont).Saldo <> 0 Then
        Texto = Space(5) & Space(10) & " "
        Texto = Texto & Left("Saldo anterior..........." & Space(25), 25) & " "
        Texto = Texto & Right(Space(12), 12) & " "
        Texto = Texto & Right(Space(12), 12) & " "
        Texto = Texto & Right(Space(12) & Format(ArrayLineas(Cont).Saldo, "###,###,##0.##"), 12)
      'End If
      vsPrinter1.Paragraph = Texto
    End If
  Next
  If chkTodos.Value = 1 Then
    If Me.optTodos.Value = True Then
      Texto = Space(5) & "                                     ------------ ------------ ------------"
      vsPrinter1.Paragraph = Texto
      Texto = Space(5) & Space(37)
      Texto = Texto & Right(Space(12) & Format(SaldoDebe, "###,###,##0.##"), 12) & " "
      Texto = Texto & Right(Space(12) & Format(SaldoHaber, "###,###,##0.##"), 12) & " "
      Texto = Texto & Right(Space(12) & Format((SaldoDebe - SaldoHaber), "###,###,##0.##"), 12)
      vsPrinter1.Paragraph = Texto
    End If
    vsPrinter1.Paragraph = ""
    'SaldoPeriodo = SaldoFacturas - SaldoPagos
    'Texto = "Total Facturas.........:" & Right(Space(12) & Format(SaldoFacturas, "###,###,##0.##"), 12)
    'vsPrinter1.Paragraph = Texto
    'Texto = "Total Pagos............:" & Right(Space(12) & Format(SaldoPagos, "###,###,##0.##"), 12)
    'vsPrinter1.Paragraph = Texto
    Texto = Space(5) & "Saldo a fecha " & Format(Me.SDCFechaFin.Date, "DD/MM/YYYY") & "....: " & Right(Space(12) & Format(ArrayLineas(Cont - 1).Saldo, "###,###,##0.##"), 12)
    'Texto = "Saldo Periodo..........:" & Right(Space(12) & Format(SaldoPeriodo, "###,###,##0.##"), 12)
    vsPrinter1.Paragraph = Texto
  End If
  vsPrinter1.Action = 6
End Sub
Sub ImprimirCuentaIMQ()

Dim Cont As Integer
Dim Pagina As Integer
Dim Texto As String
Dim NoAbonos As Integer
Dim Tipo As String

  NoAbonos = UBound(ArrayIMQ, 1)
  Pagina = 1
  vsPrinter1.MarginLeft = 30 * ConvX
  vsPrinter1.MarginRight = 0
  vsPrinter1.Preview = False
  vsPrinter1.Action = 3
  Call ImprimirCabecera
  vsPrinter1.Paragraph = ""
  vsPrinter1.Paragraph = ""
  Texto = "   FECHA     DESCRIPCION       DEBE      PACIENTE"
  vsPrinter1.Paragraph = Texto
  Texto = "---------- ---------------- ------------ ----------------------------------"
  vsPrinter1.Paragraph = Texto
  'If SaldoAnt <> 0 Then
  '  Texto = Space(37) & "Saldo anterior........... " & Right(Space(12) & Format(SaldoAnt, "###,###,##0.##"), 12)
  '  vsPrinter1.Paragraph = Texto
  'End If
  For Cont = 1 To NoAbonos
    Texto = Right(Space(10) & Format(ArrayIMQ(Cont).fecha, "DD/MM/YYYY"), 10) & " " 'Fecha
    Texto = Texto & Left(Trim(ArrayIMQ(Cont).Descrip) & Space(16), 16) & " "  'Descripci�n
    If ArrayIMQ(Cont).Importe <> 0 Then
      Texto = Texto & Right(Space(12) & Format(ArrayIMQ(Cont).Importe, "###,###,##0.##"), 12) & " " 'Debe
    Else
      Texto = Texto & Space(13)
    End If
    Texto = Texto & Left(ArrayIMQ(Cont).Nombre & Space(34), 34) & " "  'Saldo
    vsPrinter1.Paragraph = Texto
    If vsPrinter1.CurrentY > 260 * 54.6101086 Then
      'Cuando llegamos al final de la p�gina pasamos a la siguiente.
      'Y Volvemos a imprimir la cabecera.
      vsPrinter1.NewPage
      Call ImprimirCabecera
      vsPrinter1.Paragraph = ""
      vsPrinter1.Paragraph = ""
      Pagina = Pagina + 1
      Texto = "   FECHA     DESCRIPCION       DEBE      PACIENTE"
      vsPrinter1.Paragraph = Texto
      Texto = "---------- ---------------- ------------ ----------------------------------"
      vsPrinter1.Paragraph = Texto
    End If
  Next
  Texto = "                                     ------------ ------------             "
  'vsPrinter1.Paragraph = Texto
  Texto = Space(37)
  Texto = Texto & Right(Space(12) & Format(SaldoDebe, "###,###,##0.##"), 12) & " "
  'Texto = Texto & Right(Space(12) & Format(SaldoHaber, "###,###,##0.##"), 12) & " "
  vsPrinter1.Paragraph = Texto
  'vsPrinter1.Paragraph = ""
  SaldoPeriodo = SaldoFacturas - SaldoPagos
  Texto = "Total Facturas.........:" & Right(Space(12) & Format(SaldoFacturas, "###,###,##0.##"), 12)
  vsPrinter1.Paragraph = Texto
  'Texto = "Total Pagos............:" & Right(Space(12) & Format(SaldoPagos, "###,###,##0.##"), 12)
  'vsPrinter1.Paragraph = Texto
  'Texto = "Saldo Periodo..........:" & Right(Space(12) & Format(SaldoPeriodo, "###,###,##0.##"), 12)
  'vsPrinter1.Paragraph = Texto
  vsPrinter1.Action = 6
End Sub


Sub IniciarQRY()
Dim MiSql As String
Dim x As Integer
  
  'Facturas positivas y negativas con abonos
  MiSql = "SELECT NVL(Sum(FA18IMPCOMP),0) AS SUMA FROM FA1800 WHERE FA04CODFACT = ?"
  qry(1).SQL = MiSql
  
  'Facturas positivas con facturas negativas Buscar FA04CODFACT_POS en FA5800
  MiSql = "SELECT NVL(SUM(FA58IMPCOMP),0) AS SUMA FROM FA5800 WHERE FA04CODFACT_POS = ?"
  qry(2).SQL = MiSql
  
  'Facturas negativas con facturas positivas Buscar FA04CODFACT_NEG en FA5800
  MiSql = "SELECT NVL(SUM(FA58IMPCOMP),0) AS SUMA FROM FA5800 WHERE FA04CODFACT_NEG = ?"
  qry(3).SQL = MiSql
  
  
  'Abonos Positivos  y negativos con compensaciones
  MiSql = "SELECT NVL(SUM(FA18IMPCOMP),0) AS SUMA FROM FA1800 WHERE FA17CODPAGO = ?"
  qry(4).SQL = MiSql
  
  'Abonos positivos con abonos negativos
  MiSql = "SELECT NVL(SUM(FA51IMPCOMP),0) AS SUMA FROM FA5100 WHERE FA17CODPAGO_POS = ?"
  qry(5).SQL = MiSql
  
  'Abonos negativos con abonos positivos
  MiSql = "SELECT NVL(SUM(FA51IMPCOMP),0) AS SUMA FROM FA5100 WHERE FA17CODPAGO_NEG = ?"
  qry(6).SQL = MiSql
  
  For x = 1 To 6
    Set qry(x).ActiveConnection = objApp.rdoConnect
    qry(x).Prepared = True
  Next

End Sub

Sub OrdenarPorFechas()
Dim Cont1 As Integer
Dim Cont2 As Integer
Dim TmpFecha As Date
Dim TmpDesc As String
Dim TmpDebe As Double
Dim TmpHaber As Double

  For Cont1 = UBound(ArrayLineas, 1) To 1 Step -1
    For Cont2 = Cont1 To UBound(ArrayLineas, 1) - 1
      If ArrayLineas(Cont2).fecha > ArrayLineas(Cont2 + 1).fecha Then
        'Copiar a dos variables los valores del siguiente
        TmpFecha = ArrayLineas(Cont2 + 1).fecha
        TmpDesc = ArrayLineas(Cont2 + 1).Descrip
        TmpDebe = ArrayLineas(Cont2 + 1).Debe
        TmpHaber = ArrayLineas(Cont2 + 1).Haber
        'Copiar los valores al siguiente
        ArrayLineas(Cont2 + 1).fecha = ArrayLineas(Cont2).fecha
        ArrayLineas(Cont2 + 1).Descrip = ArrayLineas(Cont2).Descrip
        ArrayLineas(Cont2 + 1).Debe = ArrayLineas(Cont2).Debe
        ArrayLineas(Cont2 + 1).Haber = ArrayLineas(Cont2).Haber
        'Copiar los valores al anterior
        ArrayLineas(Cont2).fecha = TmpFecha
        ArrayLineas(Cont2).Descrip = TmpDesc
        ArrayLineas(Cont2).Debe = TmpDebe
        ArrayLineas(Cont2).Haber = TmpHaber
      ElseIf ArrayLineas(Cont2).fecha < ArrayLineas(Cont2 + 1).fecha Then
        Exit For
      End If
    Next
  Next
  
End Sub

Sub OrdenarPorNombre()
Dim Cont1 As Integer
Dim Cont2 As Integer
Dim TmpFecha As Date
Dim TmpDesc As String
Dim TmpDebe As Double
Dim TmpNombre As String

  For Cont1 = UBound(ArrayIMQ, 1) To 1 Step -1
    For Cont2 = Cont1 To UBound(ArrayIMQ, 1) - 1
      If ArrayIMQ(Cont2).Nombre > ArrayIMQ(Cont2 + 1).Nombre Then
        'Copiar a dos variables los valores del siguiente
        TmpFecha = ArrayIMQ(Cont2 + 1).fecha
        TmpDesc = ArrayIMQ(Cont2 + 1).Descrip
        TmpDebe = ArrayIMQ(Cont2 + 1).Importe
        TmpNombre = ArrayIMQ(Cont2 + 1).Nombre
        'Copiar los valores al siguiente
        ArrayIMQ(Cont2 + 1).fecha = ArrayIMQ(Cont2).fecha
        ArrayIMQ(Cont2 + 1).Descrip = ArrayIMQ(Cont2).Descrip
        ArrayIMQ(Cont2 + 1).Importe = ArrayIMQ(Cont2).Importe
        ArrayIMQ(Cont2 + 1).Nombre = ArrayIMQ(Cont2).Nombre
        'Copiar los valores al anterior
        ArrayIMQ(Cont2).fecha = TmpFecha
        ArrayIMQ(Cont2).Descrip = TmpDesc
        ArrayIMQ(Cont2).Importe = TmpDebe
        ArrayIMQ(Cont2).Nombre = TmpNombre
      ElseIf ArrayIMQ(Cont2).Nombre < ArrayIMQ(Cont2 + 1).Nombre Then
        Exit For
      End If
    Next
  Next
  
End Sub


Sub RellenarGrid(Pdtes As Boolean)
Dim Cont As Integer
Dim Texto As String

  If Me.optPdtes.Value = False Then
    SaldoAnt = Me.CalcularSaldoAnterior(Persona)
  Else
    SaldoAnt = 0
  End If
  SaldoIntermedio = SaldoAnt
  Me.grdCuentaCte.RemoveAll
  If SaldoAnt <> 0 Then
    Texto = "" & Chr(9) & "Saldo Anterior " & Chr(9) & "" & Chr(9) & "" & Chr(9) & Format(saldoanterior, "###,###,##0.##")
    Me.grdCuentaCte.AddItem Texto
  End If
  For Cont = 1 To UBound(ArrayLineas, 1)
    SaldoIntermedio = SaldoIntermedio + ArrayLineas(Cont).Debe
    SaldoIntermedio = SaldoIntermedio - ArrayLineas(Cont).Haber
    ArrayLineas(Cont).Saldo = SaldoIntermedio
    Texto = ArrayLineas(Cont).fecha & Chr(9)
    Texto = Texto & ArrayLineas(Cont).Descrip & Chr(9)
    Texto = Texto & ArrayLineas(Cont).Debe & Chr(9)
    Texto = Texto & ArrayLineas(Cont).Haber & Chr(9)
    'If Pdtes = False Then
    Texto = Texto & ArrayLineas(Cont).Saldo
    'Texto = Texto & Me.CalcularSaldoAFecha(Persona, CDate(ArrayLineas(Cont).Fecha))
    'Else
    '  Texto = Texto & ""
    'End If
    grdCuentaCte.AddItem Texto
  Next
End Sub


Private Sub cmdConsultar_Click()
Dim Seguir As Boolean
Dim Respuesta As Integer

  If cmdConsultar.Caption = "&Consultar" Then
    Seguir = ComprobarFechas()
    If Seguir = True Then
      If Persona = 800022 Then
        Respuesta = MsgBox("�Quiere el Listado con los datos de los pacientes?", vbYesNo, "IMQ de Navarra")
        If Respuesta = vbYes Then
          Call GenerarArraysIMQ
          Call OrdenarPorNombre
          If Lineas > 0 Then
            Call Me.ImprimirCuentaIMQ
          End If
        Else
          GoTo Normal
        End If
      Else
Normal:
        If Me.optTodos.Value = True Then
          Call GenerarArraysCuentas
        ElseIf Me.optPdtes.Value = True Then
          Call GenerarArraysPdtes
        End If
        
        If Lineas > 0 Then
          'Call OrdenarPorFechas
          'Le pasamos el parametro para indicarle si estamos imprimiendo los pendientes
          If Me.optTodos.Value = True Then
            Call RellenarGrid(False)
          Else
            Call RellenarGrid(True)
          End If
          cmdConsultar.Caption = "&Imprimir"
          If objSecurity.strUser = "JMV" Or objSecurity.strUser = "I�AKI" Then
            Me.cmdConsultar.Top = 90
            Me.cmdTexto.Visible = True
          End If
        Else
          MsgBox "No hay movimientos en la cuenta contable entre las fechas seleccionadas", vbInformation + vbOKOnly, "Atencion"
        End If
      End If
    End If
  ElseIf cmdConsultar.Caption = "&Imprimir" Then
    Call ImprimirCuenta
  End If
  
End Sub

Function CalcularSaldoAnterior(Persona As Long) As Double
Dim SqlFacturas As String
Dim rsFacturas As rdoResultset
Dim SqlPagos As String
Dim rsPagos As rdoResultset
Dim Saldo As Double
Dim Facturas As Double
Dim Pagos As Double

  SqlFacturas = "Select Sum(FA04CANTFACT) as Facturas  From FA0400 Where  CI21CODPERSONA = " & Persona & _
                      " and FA04NUMFACREAL Is  Null " & _
                      " and FA04FECFACTURA < TO_DATE('" & Format(Me.SDCFechaInicio.Date, "DD/MM/YYYY") & "','DD/MM/YYYY')"
  Set rsFacturas = objApp.rdoConnect.OpenResultset(SqlFacturas, 3)
  If Not rsFacturas.EOF Then
    If Not IsNull(rsFacturas(0)) Then
      Facturas = rsFacturas(0)
    Else
      Facturas = 0
    End If
  Else
    Facturas = 0
  End If
  SqlPagos = "Select Sum(FA17CANTIDAD) as Pagos From FA1700 Where CI21CODPERSONA = " & Persona & _
             " and FA17FECPAGO < TO_DATE('" & Format(Me.SDCFechaInicio.Date, "DD/MM/YYYY") & "','DD/MM/YYYY')"
  Set rsPagos = objApp.rdoConnect.OpenResultset(SqlPagos, 3)
  If Not rsPagos.EOF Then
    If Not IsNull(rsPagos(0)) Then

      Pagos = rsPagos(0)
    Else
      Pagos = 0
    End If
  Else
    Pagos = 0
  End If
  Saldo = Facturas - Pagos
  CalcularSaldoAnterior = Saldo
  
End Function


Function CalcularSaldoAFecha(Persona As Long, fecha As Date) As Variant
'Dim SqlFacturas As String
'Dim rsFacturas As rdoResultset
'Dim SqlPagos As String
'Dim rsPagos As rdoResultset
'Dim Saldo As Double
'Dim Facturas As Double
'Dim Pagos As Double
'
'  SqlFacturas = "Select Sum(FA04CANTFACT) as Facturas  From FA0400 Where  CI21CODPERSONA = " & Persona & _
'                      " and FA04NUMFACREAL Is  Null " & _
'                      " and FA04FECFACTURA <= TO_DATE('" & Format(Fecha, "DD/MM/YYYY") & "','DD/MM/YYYY')"
'  Set rsFacturas = objApp.rdoConnect.OpenResultset(SqlFacturas, 3)
'  If Not rsFacturas.EOF Then
'    If Not IsNull(rsFacturas(0)) Then
'      Facturas = rsFacturas(0)
'    Else
'      Facturas = 0
'    End If
'  Else
'    Facturas = 0
'  End If
'  SqlPagos = "Select Sum(FA17CANTIDAD) as Pagos From FA1700 Where CI21CODPERSONA = " & Persona & _
'             " and FA17FECPAGO < TO_DATE('" & Format(Me.SDCFechaInicio.Date, "DD/MM/YYYY") & "','DD/MM/YYYY')"
'  Set rsPagos = objApp.rdoConnect.OpenResultset(SqlPagos, 3)
'  If Not rsPagos.EOF Then
'    If Not IsNull(rsPagos(0)) Then
'
'      Pagos = rsPagos(0)
'    Else
'      Pagos = 0
'    End If
'  Else
'    Pagos = 0
'  End If
'  Saldo = Facturas - Pagos
'  'CalcularSaldoAnterior = CVar(Saldo)
'
End Function

Private Sub Command1_Click()

End Sub


Private Sub cmdTexto_Click()

  Call EscribirCuentaTXT
End Sub


Private Sub Form_Load()
  Call Me.IniciarQRY
End Sub

