Attribute VB_Name = "modAbonos"
Option Explicit
Dim qryRNFs As New rdoQuery
Dim qryCabecera As New rdoQuery
Dim qryInsert As New rdoQuery
Dim qryFecha As New rdoQuery

Private Sub IniciarQRY(strFact As String)
   On Error Resume Next
   qryRNFs.SQL = "" & _
      "  select AD02CODDPTO_R, AD02DESDPTO, " & _
      "     sum(decode(fa15inddescont,1,(FA03CANTFACT * fa03preciofact) * (100 - fa16dcto) / 100,0)) Descontable," & _
      "     sum(decode(fa15inddescont,0,(FA03CANTFACT * fa03preciofact),0)) NoDescontable," & _
      "     sum(FA03CANTFACT * fa03preciofact) total" & _
      "  From fa0300, fa1600, fa1500, AD0200" & _
      "  where fa04numfact in (" & _
      "         select fa04codfact from fa0400" & _
      "         Where fa04numfact in (" & strFact & ")" & _
      "               and fa04numfacreal is null)" & _
      "     and fa0300.fa04numfact = fa1600.fa04codfact" & _
      "     and fa0300.fa16numlinea = fa1600.fa16numlinea" & _
      "     and fa0300.fa15codatrib = fa1500.fa15codatrib" & _
      "     and fa0300.AD02CODDPTO_R = AD0200.AD02CODDPTO" & _
      "  group by AD02CODDPTO_R, AD02DESDPTO" & _
      "  order by descontable desc, nodescontable desc"
      
   Set qryRNFs.ActiveConnection = objApp.rdoConnect
   qryRNFs.Prepared = True
   
   qryCabecera.SQL = "" & _
      "  select * from fa0400" & _
      "  Where fa04numfact in (" & strFact & ")" & _
      "        and fa04numfacreal is null " & _
      "  order by AD07CODPROCESO, AD01CODASISTENCI"
   
   Set qryCabecera.ActiveConnection = objApp.rdoConnect
   qryCabecera.Prepared = True
   
   qryInsert.SQL = "INSERT INTO FA0300 " & _
          "(FA03NUMRNF,FA05CODCATEG, FA13CODESTRNF,AD01CODASISTENCI, " & _
          " FA03FECHA,CI22NUMHISTORIA,AD07CODPROCESO, FA03INDREALIZADO, " & _
          " AD02CODDPTO_R) " & _
          " values " & _
          "(?, ?, 1, ?, ?, ?, ?, ?, ?)"
   Set qryInsert.ActiveConnection = objApp.rdoConnect
   qryInsert.Prepared = True

End Sub

'**********************************************************************************************
'
' este proceso se utiliza para crear abonos de facturas automaticamente.
'
' se le pasa como parametro un string con una serie de numeros de factura entrecomillas y separados
' por comas.
' se calcula cual es el m�ximo importe descontable y se solicita cuanto se quiere aplicar,
' despues se genera una factura con una sola linea y con un rnf por cada departamento al que
' se le haya descontado algun importe
'
'**********************************************************************************************
Public Sub Abonar(strFact As String)
   Dim MaxDto As Double
   Dim MaxNoDto As Double
   Dim Importe As Double
'   Dim strImporte As String
   Dim RS As rdoResultset
   Dim MiRs As rdoResultset
   Dim FechaActual As String
   
   Screen.MousePointer = vbHourglass
   
   ' preparar las qrys
   IniciarQRY strFact

   ' recoger la fecha del sistema para generar despues la factura
   Set RS = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
   FechaActual = Format(RS("Sysdate"), "DD/MM/YYYY")
   
   ' recoger los rnfs de las facturas originales para calcular las diferencias a aplicar
   Set RS = qryRNFs.OpenResultset(rdOpenKeyset)
   
   ' calcular la cantidad maxima descontable
   MaxDto = 0
   Do While Not RS.EOF
'      If RS("Descontable") > 0 Then
         MaxDto = MaxDto + RS("Descontable")
         MaxNoDto = MaxNoDto + RS("NoDescontable")
'      Else
'         Exit Do
'      End If
      RS.MoveNext
   Loop
   
   Screen.MousePointer = vbDefault
   
   
   ' pedir el importe del abono
   Importe = PedirImporte(MaxDto, MaxNoDto)
   
   If Importe = -1 Then
      ' operacion cancelada por el usuario
      Exit Sub
   End If
   
   RS.MoveFirst
   Screen.MousePointer = vbHourglass
   
   
   
   Dim Asistencia As New Asistencia
   Dim reco As New Persona
   Dim Propuesta As New PropuestaFactura
   Dim AsistAnterior As String
   Dim NuevoProcAsist As ProcAsist
   Dim ProcAnterior As String
   Dim Concierto As New Nodo
   
   Asistencia.PropuestasFactura.Add Propuesta, "1"
   Asistencia.Facturado = False
   Set MiRs = qryCabecera.OpenResultset(rdOpenKeyset)
   Do While Not MiRs.EOF
      ' a�adir el proceso-asistencia (solo los codigos) a la asistencia para tener una relacion de
      ' los diferentes proc-asist que se van a facturar juntos.
      If MiRs("AD01CODASISTENCI") <> AsistAnterior Or MiRs("AD07CODPROCESO") <> ProcAnterior Then
         Set NuevoProcAsist = New ProcAsist
         NuevoProcAsist.Asistencia = MiRs("AD01CODASISTENCI")
         NuevoProcAsist.proceso = MiRs("AD07CODPROCESO")
         Asistencia.ProcAsist.Add NuevoProcAsist
         
         AsistAnterior = MiRs("AD01CODASISTENCI")
         ProcAnterior = MiRs("AD07CODPROCESO")
      End If
            
      If reco.Codigo = "" Then
         ' crear el reco
         reco.Codigo = "" & MiRs("CI21CODPERSONA")
   
         reco.Concierto.Concierto = MiRs("FA09CODNODOCONC")
         Concierto.codConcierto = MiRs("FA09CODNODOCONC")
         If Not IsNull(MiRs("FA09CODNODOCONC_FACT")) Then
            reco.Concierto.NodoAFacturar = MiRs("FA09CODNODOCONC_FACT")
            Concierto.Codigo = MiRs("FA09CODNODOCONC_FACT")
         End If
         Propuesta.NodosAplicados.Add Concierto
         
         reco.CodTipEcon = "" & MiRs("CI32CODTIPECON")
         reco.EntidadResponsable = "" & MiRs("CI13CODENTIDAD")
         
         reco.proceso = MiRs("AD07CODPROCESO")
         reco.Asistencia = MiRs("AD01CODASISTENCI")
         ' a�adirlo
         Asistencia.AddREco reco, "" & MiRs("FA04FECDESDE"), "" & MiRs("FA04FECHASTA")
      Else
         ' REVISAR QUE SOLO HAYA UN RECO DIFERENTE
         If reco.Codigo <> MiRs("CI21CODPERSONA") Then
            MsgBox "Ha seleccionado facturas de varios responsables. todas deben corresponder al mismo", vbCritical + vbOKOnly, "Abonos de facturas"
            Exit Sub
         End If

      End If

      MiRs.MoveNext
   Loop
   
   '****************************************************************************************
   ' SE CREA UNA NUEVA LINEA QUE CONTENGA EL TOTAL DEL IMPORTE DEL ABONO Y SE LE A�ADE
   ' UN RNF POR CADA DEPARTAMENTO CON LA PARTE DEL DESCUENTO QUE LE CORRESPONDA.
   '****************************************************************************************
   Dim NuevaLinea As Nodo
   Dim objRNF As rnf
   Dim impLinea As Double
   Dim Facts As String
   Dim rnf As rnf
   
   Set NuevaLinea = New Nodo
   NuevaLinea.FactOblig = True  'Importe # 0, lo facturaremos.
   
   NuevaLinea.Name = "Nota de abono correspondiente a las facturas n� " & strFact
   ' marcar el nodo como NO descontable
   NuevaLinea.Descont = False
   ' cantidad
   NuevaLinea.LineaFact.Cantidad = 1
   ' precio
   NuevaLinea.LineaFact.PrecioNoDescontable = -Importe
   'el descuento es 0.
   NuevaLinea.LineaFact.Dto = 0
   ' importe
   NuevaLinea.LineaFact.ImporteNoDescontable = -Importe
   NuevaLinea.EntidadResponsable = 1
   NuevaLinea.LimiteFranquicia = -1
   NuevaLinea.OrdImp = 0
   NuevaLinea.KeyREco = 1
   NuevaLinea.Desglose = True
   'Finalmente a�adimos la nueva l�nea de factura (nodo) y su rnf.
   Propuesta.NuevasLineas.Add NuevaLinea, "R" & NuevaLinea.KeyREco & "L001"
   NuevaLinea.Key = "R" & NuevaLinea.KeyREco & "L001"
   
   RS.MoveFirst
   Do While Not RS.EOF
      If RS("Descontable") > 0 Or (RS("NoDescontable") > 0 And Importe > MaxDto) Then
      
         Set objRNF = New rnf
         With objRNF
            .CodCateg = 42331

            .CodRNF = fNextClave("FA03NUMRNF", "FA0300")
            .fecha = FechaActual
            .Cantidad = 1
            If Importe < MaxDto Then
               ' se aplica el descuento proporcionalmente entre los descontables
               impLinea = (Importe / MaxDto) * RS("Descontable")
               
            ElseIf Importe = MaxDto Then
               ' se aplica todo lo descontable
               impLinea = RS("Descontable")
               
            Else
               ' se aplica el descuento a la totalidad de los descontables y
               ' el resto proporcionalmente entre los no descontables
               impLinea = RS("Descontable") + ((Importe - MaxDto) / MaxNoDto * RS("NoDescontable"))
            
            End If
            
            .Precio = -NumToSql(RedondearMoneda(impLinea, tiImporteLinea))
            .Asignado = True
            
            .Name = RS("AD02DESDPTO") & ""
            .Descripcion = RS("AD02DESDPTO") & ""
                   
            ' Pendiente de facturar
            .Facturado = False
            .Permanente = False
            .Desglose = True
            .CodProceso = reco.proceso
            .CodAsistenci = reco.Asistencia
            
            ' grabar los rnfs creados
            qryInsert.rdoParameters(0) = .CodRNF
            qryInsert.rdoParameters(1) = .CodCateg
            qryInsert.rdoParameters(2) = .CodAsistenci
            qryInsert.rdoParameters(3) = .fecha
            qryInsert.rdoParameters(4) = ""
            qryInsert.rdoParameters(5) = .CodProceso
            qryInsert.rdoParameters(6) = 1
            qryInsert.rdoParameters(7) = RS("AD02CODDPTO_R")
            qryInsert.Execute
         End With
               
         
                  
         ' A�adir el nuevo RNF a la asistencia
         NuevaLinea.RNFs.Add objRNF, objRNF.CodRNF
         objRNF.Key = objRNF.CodRNF
      End If
      
      RS.MoveNext
   Loop
   
   
   
   ' buscar si ha cuadrado
   ' calcular la cantidad maxima aplicada
   MaxDto = 0
   For Each rnf In NuevaLinea.RNFs
         MaxDto = MaxDto + rnf.Precio
   Next
   If MaxDto <> -Importe Then
      Propuesta.NuevasLineas(1).RNFs(1).Precio = Propuesta.NuevasLineas(1).RNFs(1).Precio + _
                                                      (-MaxDto - Importe)
   End If
   
   ' grabar la factura provisional e imprimirla para que validen si la graban definitivamente o no
   Asistencia.GrabarFactura "1", FechaActual, "P"
   Dim aFacts(1) As String
   aFacts(1) = Asistencia.REcos(1).NFactura
   Screen.MousePointer = vbDefault
   ImprimirFact aFacts, True
End Sub

' esta funcion pide un numero que sera el importe a abonar y lo devuelve
' si se cancela se devuelve -1
Private Function PedirImporte(MaxDto As Double, MaxNoDto As Double) As Double
   Dim strImporte As String
   Dim blnSalir As Boolean
   Dim respuesta As Integer
   
   blnSalir = False
   
   Do While Not blnSalir
      ' se pide un importe
      strImporte = InputBox("Introduzca el importe a abonar " & Chr(10) & _
                            "    M�ximo Dto : " & MaxDto & ")", _
                            "Abonos de facturas", 0)
                           
      ' hay que controlar los posibles errores
      If strImporte = "" Then
         ' ha pulsado cancelar
         PedirImporte = -1
         blnSalir = True
         
      ElseIf Not IsNumeric(strImporte) Then
         ' debe teclear un n�mero
         MsgBox "el importe debe ser num�rico", vbOKOnly + vbCritical, "Abonos de facturas"
         
      ElseIf Val(strImporte) <= 0 Or Val(strImporte) > MaxDto + MaxNoDto Then
         ' debe ser positivo
         MsgBox "el importe debe ser mayor que 0 y menor que " & MaxDto + MaxNoDto, vbOKOnly + vbCritical, "Abonos de facturas"
      
      ElseIf CDbl(strImporte) <= MaxDto Then
         ' se va a abonar un importe que solo afecta a los descontables, correcto
         PedirImporte = CDbl(strImporte)
         blnSalir = True
         
      Else
         ' la cantidad a abonar afectar� a los movimientos no descontables, pedir conformidad
         respuesta = MsgBox("El abono supera la cantidad descontable y " & Chr(10) & _
                            "afectar� a movimientos no descontables" & Chr(10) & Chr(10) & _
                            "� Desea generar el abono correspondiente ?", vbCritical + vbYesNoCancel, "Abonos de facturas")
                            
         Select Case respuesta
            Case vbYes
               PedirImporte = CDbl(strImporte)
               blnSalir = True
               
            Case vbCancel
               ' ha pulsado cancelar
               PedirImporte = -1
               blnSalir = True
         End Select
         
            
      End If
   Loop
   
End Function
