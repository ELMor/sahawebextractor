VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_SaldosPdtes 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Selecci�n de un registro determinado"
   ClientHeight    =   6765
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   10995
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6765
   ScaleWidth      =   10995
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin SSDataWidgets_B.SSDBGrid grdPacientes 
      Height          =   4515
      Left            =   90
      TabIndex        =   28
      Top             =   2205
      Width           =   10860
      ScrollBars      =   2
      _Version        =   131078
      DataMode        =   2
      GroupHeaders    =   0   'False
      HeadLines       =   0
      Col.Count       =   5
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   1402
      Columns(0).Caption=   "Selec"
      Columns(0).Name =   "Historia"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   6350
      Columns(1).Caption=   "Paciente"
      Columns(1).Name =   "Paciente"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1958
      Columns(2).Caption=   "Proceso"
      Columns(2).Name =   "RECO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   6350
      Columns(3).Caption=   "Asistencia"
      Columns(3).Name =   "NombreRECO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   2117
      Columns(4).Caption=   "Historia"
      Columns(4).Name =   "Saldo"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      _ExtentX        =   19156
      _ExtentY        =   7964
      _StockProps     =   79
      Caption         =   "Pacientes a facturar"
   End
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   420
      Left            =   0
      TabIndex        =   30
      Top             =   6300
      Visible         =   0   'False
      Width           =   330
      _Version        =   196608
      _ExtentX        =   582
      _ExtentY        =   741
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      TableBorder     =   0
      Header          =   "CL�NICA UNIVERSITARIA                            S A L D O S   P E N D I E N T E S"
      MarginRight     =   0
      Preview         =   0   'False
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Height          =   375
      Left            =   9630
      TabIndex        =   29
      Top             =   315
      Width           =   1140
   End
   Begin VB.Frame Frame1 
      Caption         =   " Criterios de Selecci�n"
      Height          =   2085
      Left            =   0
      TabIndex        =   0
      Top             =   45
      Width           =   10905
      Begin VB.OptionButton optFactura 
         Caption         =   "Factura"
         Height          =   285
         Left            =   9900
         TabIndex        =   32
         Top             =   1710
         Width           =   870
      End
      Begin VB.OptionButton optSaldo 
         Caption         =   "Saldo"
         Height          =   285
         Left            =   8865
         TabIndex        =   31
         Top             =   1710
         Value           =   -1  'True
         Width           =   825
      End
      Begin VB.ComboBox cboProvincia 
         Height          =   315
         Left            =   8055
         TabIndex        =   27
         Top             =   1305
         Width           =   2670
      End
      Begin VB.TextBox txtCantidadPdte 
         Height          =   285
         Left            =   7020
         MaxLength       =   12
         TabIndex        =   22
         Top             =   1710
         Width           =   1680
      End
      Begin VB.TextBox txtNumFact 
         Height          =   285
         Left            =   1395
         MaxLength       =   7
         TabIndex        =   21
         Top             =   1710
         Width           =   1635
      End
      Begin VB.TextBox txtProvincia 
         Height          =   285
         Left            =   7605
         TabIndex        =   20
         Top             =   1305
         Width           =   375
      End
      Begin VB.TextBox txtPoblacion 
         Height          =   285
         Left            =   1395
         TabIndex        =   19
         Top             =   1305
         Width           =   4560
      End
      Begin VB.TextBox txtApel2RECO 
         Height          =   285
         Left            =   7335
         TabIndex        =   18
         Top             =   2385
         Width           =   3030
      End
      Begin VB.TextBox txtApel1RECO 
         Height          =   285
         Left            =   4230
         TabIndex        =   17
         Top             =   2385
         Width           =   3030
      End
      Begin VB.TextBox txtNombreRECO 
         Height          =   285
         Left            =   1125
         TabIndex        =   16
         Top             =   2385
         Width           =   3030
      End
      Begin VB.TextBox txtApel2 
         Height          =   285
         Left            =   7605
         TabIndex        =   15
         Top             =   900
         Width           =   3030
      End
      Begin VB.TextBox txtApel1 
         Height          =   285
         Left            =   4500
         TabIndex        =   14
         Top             =   900
         Width           =   3030
      End
      Begin VB.TextBox txtNombre 
         Height          =   285
         Left            =   1395
         TabIndex        =   13
         Top             =   900
         Width           =   3030
      End
      Begin VB.TextBox txtHistoria 
         Height          =   285
         Left            =   1440
         MaxLength       =   8
         TabIndex        =   12
         Top             =   315
         Width           =   1140
      End
      Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFact 
         Height          =   285
         Left            =   3960
         TabIndex        =   24
         Tag             =   "Fecha Inicio"
         Top             =   1710
         Width           =   1725
         _Version        =   65537
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483634
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.Label Label1 
         Caption         =   "Paciente...........:"
         Height          =   195
         Index           =   13
         Left            =   180
         TabIndex        =   26
         Top             =   945
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "RECO...............:"
         Height          =   195
         Index           =   12
         Left            =   -90
         TabIndex        =   25
         Top             =   2430
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre o Raz�n Social"
         Height          =   195
         Index           =   1
         Left            =   1440
         TabIndex        =   23
         Top             =   630
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "Cantidad Pdte..:"
         Height          =   195
         Index           =   11
         Left            =   5805
         TabIndex        =   11
         Top             =   1755
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "Fecha.......:"
         Height          =   195
         Index           =   10
         Left            =   3105
         TabIndex        =   10
         Top             =   1755
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "N� de Factura...:"
         Height          =   195
         Index           =   9
         Left            =   180
         TabIndex        =   9
         Top             =   1755
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "Provincia:"
         Height          =   240
         Index           =   8
         Left            =   6300
         TabIndex        =   8
         Top             =   1350
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "Poblaci�n.........:"
         Height          =   240
         Index           =   7
         Left            =   180
         TabIndex        =   7
         Top             =   1395
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "Segunado Apellido:"
         Height          =   195
         Index           =   6
         Left            =   7380
         TabIndex        =   6
         Top             =   2160
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "Primer Apellido:"
         Height          =   195
         Index           =   5
         Left            =   4275
         TabIndex        =   5
         Top             =   2160
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre o Raz�n Social"
         Height          =   195
         Index           =   4
         Left            =   1125
         TabIndex        =   4
         Top             =   2160
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "Segundo Apellido:"
         Height          =   195
         Index           =   3
         Left            =   7650
         TabIndex        =   3
         Top             =   630
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "Primer Apellido:"
         Height          =   195
         Index           =   2
         Left            =   4545
         TabIndex        =   2
         Top             =   630
         Width           =   1905
      End
      Begin VB.Label Label1 
         Caption         =   "N� de Historia....:"
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   1
         Top             =   360
         Width           =   1905
      End
   End
End
Attribute VB_Name = "frm_SaldosPdtes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim rsResult As rdoResultset

Sub Buscar()
Dim Seguir As Boolean
Dim MiSql As String
Dim Texto As String
Dim Anterior As String
Dim MiWhere As String
Dim MiGroup As String
Dim SqlRespon As String
Dim rsRespon As rdoResultset
Dim SqLpAc As String
Dim RsPaC As rdoResultset

  'Comprobaremos que los datos introducidos en la pantalla son correctos.
  Seguir = ComprobarDatos()
  If Seguir = False Then
    Exit Sub
  End If
  Me.grdPacientes.RemoveAll
  
  MiSql = "Select /*+ RULE */ CI22NUMHISTORIA, CI22NOMBRE||' '||CI22PRIAPEL||' '||CI22SEGAPEL as Nombre,"
  MiSql = MiSql & " CI10DESLOCALID,CI2200.CI21CODPERSONA, CI21CODPERSONA_REC, CI21SALDO "
  MiSql = MiSql & " from CI2200,CI2600,FA0400,CI2100,CI1000 "
  'Crearemos la sql de selecci�n en base a los datos que se nos han introducido en pantalla.
  MiWhere = "Where CI2200.CI21CODPERSONA = FA0400.CI21CODPERSONA "
  MiWhere = MiWhere & " And CI1000.CI26CODPROVI = CI2600.CI26CODPROVI(+)"
  MiWhere = MiWhere & " And CI2100.CI21CODPERSONA = CI2200.CI21CODPERSONA "
  MiWhere = MiWhere & " And CI1000.CI21CODPERSONA = CI2200.CI21CODPERSONA "
  MiWhere = MiWhere & " And CI1000.CI10INDDIRPRINC = -1"
  MiWhere = MiWhere & " And CI2100.CI21SALDO > 0 "
  MiWhere = MiWhere & " And FA0400.FA04INDCOMPENSA <> 2 "
  If Trim(txtHistoria.Text) <> "" Then
    MiWhere = MiWhere & " And CI22NUMHISTORIA = " & Me.txtHistoria
  End If
  If Trim(txtNombre.Text) <> "" Then
    MiWhere = MiWhere & " And CI22NOMBRE LIKE '%" & Me.txtNombre & "%'"
  End If
  If Trim(txtApel1.Text) <> "" Then
    MiWhere = MiWhere & " And CI22PRIAPEL LIKE '%" & Me.txtApel1 & "%'"
  End If
  If Trim(txtApel2.Text) <> "" Then
    MiWhere = MiWhere & " And CI22SEGAPEL LIKE '%" & Me.txtApel2 & "%'"
  End If
  If Trim(txtNombreRECO.Text) <> "" Then
    MiWhere = MiWhere & " And CI22NOMBRE LIKE '%" & Me.txtNombreRECO & "%'"
  End If
  If Trim(txtApel1RECO.Text) <> "" Then
    MiWhere = MiWhere & " And CI22PRIAPEL LIKE '%" & Me.txtApel1RECO & "%'"
  End If
  If Trim(txtApel2RECO.Text) <> "" Then
    MiWhere = MiWhere & " And CI22SEGAPEL LIKE '%" & Me.txtApel2RECO & "%'"
  End If
  If Trim(txtPoblacion.Text) <> "" Then
    MiWhere = MiWhere & " And CI10DESLOCALID LIKE '%" & Me.txtPoblacion & "%'"
  End If
  If Trim(txtProvincia.Text) <> "" Then
    MiWhere = MiWhere & " And CI2600.CI26DESCORTA = '" & Me.txtProvincia & "'"
  End If
  If Trim(txtNumFact.Text) <> "" Then
    MiWhere = MiWhere & " And FA04NUMFACT = '13/" & Me.txtNumFact & "'"
  End If
  If Trim(SDCFechaFact.Date) <> "" Then
    MiWhere = MiWhere & " And FA04FECFACTURA = TO_DATE ('" & Format(Me.SDCFechaFact.Date, "DD/MM/YYYY") & "','DD/MM/YYYY')"
  End If
  If Trim(txtCantidadPdte.Text) <> "" Then
    If Me.optSaldo.Value = True Then
      MiWhere = MiWhere & " And CI21SALDO = " & CDbl(Me.txtCantidadPdte.Text)
    ElseIf Me.optFactura = True Then
      MiWhere = MiWhere & " And FA04CANTFACT = " & CDbl(Me.txtCantidadPdte.Text)
    End If
  End If
  MiGroup = "GROUP BY CI22NUMHISTORIA, CI22NOMBRE||' '||CI22PRIAPEL||' '||CI22SEGAPEL," & _
            " CI10DESLOCALID,CI2200.CI21CODPERSONA, CI21CODPERSONA_REC, CI21SALDO "
  MiSql = MiSql & " " & MiWhere & " " & MiGroup
  Set rsResult = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not rsResult.EOF Then
    rsResult.MoveLast
    rsResult.MoveFirst
    Anterior = ""
    While Not rsResult.EOF
      'Buscaremos que esa persona  sea responsable de otra
      SqlRespon = "Select CI22NUMHISTORIA,CI22NOMBRE||' '||CI22PRIAPEL||' '||CI22SEGAPEL As Nombre From CI2200 Where CI21CODPERSONA_REC = " & rsResult("CI21CODPERSONA")
      Set rsRespon = objApp.rdoConnect.OpenResultset(SqlRespon, 3)
      If Not rsRespon.EOF Then
        Texto = rsRespon("CI22NUMHISTORIA") & Chr(9) & rsRespon("Nombre") & Chr(9) & _
                rsResult("CI21CODPERSONA") & Chr(9) & rsResult("NOMBRE") & Chr(9) & rsResult("CI21SALDO")
      ElseIf Not IsNull(rsResult("CI21CODPERSONA_REC")) Then
        'Buscaremos en el caso de que no sea responsable si el responsable es otro
        SqLpAc = "Select CI21CODPERSONA,CI22NOMBRE||' '||CI22PRIAPEL||' '||CI22SEGAPEL As Nombre From CI2200 Where CI21CODPERSONA = " & rsResult("CI21CODPERSONA_REC")
        Set RsPaC = objApp.rdoConnect.OpenResultset(SqLpAc, 3)
        If Not RsPaC.EOF Then
          Texto = rsResult("CI22NUMHISTORIA") & Chr(9) & rsResult("Nombre") & Chr(9) & _
                  RsPaC("CI21CODPERSONA") & Chr(9) & RsPaC("NOMBRE") & Chr(9) & rsResult("CI21SALDO")
        End If
      Else
        Texto = rsResult("CI22NUMHISTORIA") & Chr(9) & rsResult("NOMBRE") & Chr(9) & rsResult("CI21CODPERSONA") & Chr(9) & rsResult("NOMBRE") & Chr(9) & rsResult("CI21SALDO")
      End If
      Me.grdPacientes.AddItem Texto
      rsResult.MoveNext
    Wend
  End If
  

End Sub

Function ComprobarDatos() As Boolean
Dim sqlProv As String
Dim rsProv As rdoResultset

  'Comprobamos que la cantidad pendiente es un dato v�lido.
  If Trim(txtCantidadPdte.Text) <> "" Then
    If Not IsNumeric(txtCantidadPdte.Text) Then
      MsgBox "El campo cantidad pendiente es un campo de tipo num�rico", vbInformation + vbOKOnly, "Aviso"
      If txtCantidadPdte.Enabled = True Then
        txtCantidadPdte.Text = ""
        txtCantidadPdte.SetFocus
      End If
      ComprobarDatos = False
      Exit Function
    End If
  End If
  
  'Comprobamos que el n�mero de historia es v�lido
  If Trim(txtHistoria) <> "" Then
    If Not IsNumeric(txtHistoria) Then
      MsgBox "El campo historia es un campo de tipo num�rico", vbInformation + vbOKOnly, "Aviso"
      If txtHistoria.Enabled = True Then
        txtHistoria.Text = ""
        txtHistoria.SetFocus
      End If
      ComprobarDatos = False
      Exit Function
    End If
  End If
  
  'Comprobamos que el n�mero de factura es v�lido
  If Trim(txtNumFact) <> "" Then
    If Not IsNumeric(txtNumFact) Then
      MsgBox "El campo n�mero de factura no es v�lido", vbInformation + vbOKOnly, "Aviso"
      If txtNumFact.Enabled = True Then
        txtNumFact.Text = ""
        txtNumFact.SetFocus
      End If
      ComprobarDatos = False
      Exit Function
    End If
  End If
  
  'Comprobaremos que el dato relativo a la provincia es v�lido.
  If Trim(txtProvincia.Text) <> "" Then
    sqlProv = "Select CI26DESPROVI From CI2600 Where CI26DESCORTA = '" & UCase(Me.txtProvincia.Text) & "'"
    Set rsProv = objApp.rdoConnect.OpenResultset(sqlProv, 3)
    If Not rsProv.EOF Then
      Me.cboProvincia.Text = rsProv(0)
    Else
      MsgBox "La provincia introducida no es v�lida", vbInformation + vbOKOnly, "Aviso"
      Me.txtProvincia.Text = ""
      Me.cboProvincia.Text = ""
      If Me.txtProvincia.Enabled = True Then
        txtProvincia.SetFocus
      End If
      ComprobarDatos = False
      Exit Function
    End If
  Else
    Me.cboProvincia.Text = ""
  End If
  
  ComprobarDatos = True
End Function

Sub DatosAMostrar()

  'NumHistoria  -> CI2200.CI22NUMHISTORIA
  'Nombre       -> CI2200.CI22PRIAPEL & " " & CI2200.CI22SEGAPEL & " " & CI2200.CI22NOMBRE

  'Nombre RECO  -> Dependiendo de si es entidad CI2300.CI23RAZONSOCIAL o de si es f�sica CI2200.
  'Direcci�n    -> CI1000.CI10CALLE & " " & CI1000.CI10PORTAL & " " & CI1000.CI10RESTODIREC
  'Poblaci�n    -> CI1000.CI10DESLOCALID
  'Provincia(corta) -> CI2600.CI26DESCORTA
  'SaldoTRotal -> SUMA DE LAS FACTURAS
  '  N�mero   -> FA0400.FA04NUMFACT
  '  Fecha    -> FA0400.FA04FECFACTURA
  '  Importe  -> FA0400.FA04CANTFACT
  
  'Seleccionaremos de la base de datos aquellas personas cuyo saldo > 0
  
  'Para cada uno de ellos crearemos una l�nea general y una l�nea por cada una de las facturas.
  
  'Dejaremos creado el fichero para que hagan la selecci�n en base a como ellos quieran.
  
End Sub

Sub Imprimir()
Dim Texto As String
Dim x As Integer
Dim i As Integer

  vsPrinter1.Orientation = orLandscape
  vsPrinter1.Preview = False
  vsPrinter1.Action = paStartDoc
  vsPrinter1.FontName = "Courier New"
  vsPrinter1.FontSize = 9
  vsPrinter1.MarginRight = 0
  'Construcci�n de la cabecera de la p�gina
  Texto = "Historia Nombre del Paciente                 Direcci�n                      Poblaci�n                      PR    Saldo  "
  vsPrinter1.Paragraph = Texto
  Texto = "-------- ----------------------------------- ------------------------------ ------------------------------ -- ----------"
  vsPrinter1.Paragraph = Texto
  'Construcci�n de la cadena de impresi�n.
  Texto = Right(Space(8) & "Historia", 8) & " "
  Texto = Texto & Left("Nombre" & Space(35), 35) & " "
  Texto = Texto & Left("Direccion" & Space(30), 30) & " "
  Texto = Texto & Left("Poblacion" & Space(30), 30) & " "
  Texto = Texto & Left("Provincia" & Space(2), 2) & " "
  vsPrinter1.Paragraph = Texto
  'Texto = Texto & Right(Space(10) & Format(Saldo, "##,###,##0"), 10)
  'A�adimos el nombre del responsable econ�mico
  Texto = Space(10) & "Responsable"
  vsPrinter1.Paragraph = Texto
  'Para cada una de las personas a�adimos las l�neas con las facturas pendientes
  'A�adiremos las facturas de una en una en orden ascendente en base al tiempo y hasta un m�ximo de
  '4 facturas por l�nea de XX/XXXXXXX XX/XX/XXXX XX,XXX,XXX
  For x = 1 To 3
    Texto = Space(3)
    For i = 1 To 4
      Texto = Texto & "XX/XXXXXXX XX/XX/XXXX XX,XXX,XXX" & " "
    Next
    vsPrinter1.Paragraph = Texto
  Next
  vsPrinter1.Action = 6
  
End Sub

Private Sub cboProvincia_Click()
Dim sqlProv As String
Dim rsProv As rdoResultset

  If Trim(Me.cboProvincia.Text) <> "" Then
    sqlProv = "Select CI26DESCORTA From CI2600 Where CI26DESPROVI = '" & Me.cboProvincia.Text & "'"
    Set rsProv = objApp.rdoConnect.OpenResultset(sqlProv, 3)
    If Not rsProv.EOF Then
      Me.txtProvincia.Text = rsProv(0)
    Else
      MsgBox "La provincia introducida no es v�lida", vbInformation + vbOKOnly, "Aviso"
      Me.txtProvincia.Text = ""
      Me.cboProvincia.Text = ""
      If Me.cboProvincia.Enabled = True Then
        cboProvincia.SetFocus
      End If
    End If
  Else
    Me.txtProvincia.Text = ""
  End If


End Sub

Private Sub cboProvincia_DropDown()
Dim sqlProv As String
Dim rsProv As rdoResultset
  
  sqlProv = "Select CI26DESPROVI From CI2600 ORDER BY CI26CODPROVI"
  Set rsProv = objApp.rdoConnect.OpenResultset(sqlProv, 3)
  If Not rsProv.EOF Then
    While Not rsProv.EOF
      Me.cboProvincia.AddItem rsProv(0)
      rsProv.MoveNext
    Wend
  End If

End Sub

Private Sub cboProvincia_LostFocus()
Dim sqlProv As String
Dim rsProv As rdoResultset

  If Trim(Me.cboProvincia.Text) <> "" Then
    sqlProv = "Select CI26DESCORTA From CI2600 Where CI26DESPROVI = '" & Me.cboProvincia.Text & "'"
    Set rsProv = objApp.rdoConnect.OpenResultset(sqlProv, 3)
    If Not rsProv.EOF Then
      Me.txtProvincia.Text = rsProv(0)
    Else
      MsgBox "La provincia introducida no es v�lida", vbInformation + vbOKOnly, "Aviso"
      Me.txtProvincia.Text = ""
      Me.cboProvincia.Text = ""
      If Me.cboProvincia.Enabled = True Then
        cboProvincia.SetFocus
      End If
    End If
  Else
    Me.txtProvincia.Text = ""
  End If

End Sub


Private Sub cmdBuscar_Click()
  

  If cmdBuscar.Caption = "&Buscar" Then
    Call Buscar
  ElseIf cmdBuscar.Caption = "Imprimir" Then
    Call Imprimir
  End If
  
End Sub


Private Sub grdPacientes_DblClick()
  
  Call frm_Cobros.pGestion(grdPacientes.Columns(2).Value)
  Set frm_Cobros = Nothing
End Sub

Private Sub txtCantidadPdte_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtCantidadPdte.Text)
End Sub


Private Sub txtHistoria_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarEnteros(KeyAscii)
End Sub


Private Sub txtNumFact_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarEnteros(KeyAscii)
End Sub



Private Sub txtProvincia_LostFocus()
Dim sqlProv As String
Dim rsProv As rdoResultset

  If Trim(txtProvincia.Text) <> "" Then
    sqlProv = "Select CI26DESPROVI From CI2600 Where CI26DESCORTA = '" & UCase(Me.txtProvincia.Text) & "'"
    Set rsProv = objApp.rdoConnect.OpenResultset(sqlProv, 3)
    If Not rsProv.EOF Then
      Me.cboProvincia.Text = rsProv(0)
    Else
      MsgBox "La provincia introducida no es v�lida", vbInformation + vbOKOnly, "Aviso"
      Me.txtProvincia.Text = ""
      Me.cboProvincia.Text = ""
      If Me.txtProvincia.Enabled = True Then
        txtProvincia.SetFocus
      End If
    End If
  Else
    Me.cboProvincia.Text = ""
  End If
End Sub


