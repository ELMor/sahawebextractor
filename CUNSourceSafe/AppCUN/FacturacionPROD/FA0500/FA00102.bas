Attribute VB_Name = "modFacturacion"
'*************************************************************
'*
'*  Modulo que contiene las funciones necesarias
'*  para la facturaci�n de RNF's
'*
'*************************************************************


' tipo de datos para almacenar los datos generados en la pantalla de
' modificaci�n de atributos y que indican :
'   - nombre del parametro
'   - si es actualizable o no
'   - el nuevo valor
Public Type CamposAtrib
    campo As String
    Actualizar As Boolean
    Valor As String
End Type
' constantes que se corresponden con el orden de los parametros de atributos
' en la sql
Public Const FA15PRECIOREF = 1
Public Const FA15PRECIODIA = 2
Public Const FA15DESCUENTO = 3
Public Const FA15PERIODOGARA = 4
Public Const FA15INDNECESARIO = 5
Public Const FA15INDSUFICIENTE = 6
Public Const FA15INDEXCLUIDO = 7
Public Const FA15INDLINOBLIG = 8
Public Const FA15INDFACTOBLIG = 9
Public Const FA15INDDESCONT = 10
Public Const CI13CODENTIDAD = 11
Public Const FA15INDSUPLEMENTO = 12
Public Const FA15INDDESGLOSE = 13
Public Const FA15OrdImp = 14

' estas dos siempre deben ser las ultimas
Public Const FA15Variaci�nPorcentual = 15
Public Const FA15Redondeo = 16

'Variable que utilizaremos para comprobar si se ha pulsado el boton salir (X)
Global BotonSalir As Boolean
'Variable que utilizaremos para facturar otra propuesta de factura de una historia
Global ContinuarFactura As Boolean

' variable que usaremos para determinar el orden en la presentaci�n del desglose
' puede ser :
'   - '0' Fecha
'   - '1' Descripcion del RNF
Global OrdenDesglose As Integer

Public Enum faTipoAsignacion
    TieneRnfSuficiente = 0
    NoTieneRnfNecesario = 1
    RnfOpcional = 2
    NoTieneRNFs = 3
    TieneRNFs = 4
End Enum

' tipo de dato enumerado para definir los diferentes precios
' que puede tener un objeto de tipo nodo.
Public Enum TipoPrecio
    tpSinPrecio = 0
    tpPrecioRef = 1
    tpPrecioDia = 2
    tpPrecioCantidad = 3
    tpNodoRelacionado = 4
    tpPrecioCantidadDias = 5
End Enum

' se utiliza para determinar el tipo de importe, para la hora de los redondeos
' poder aplicar diferentes decimales seg�n de que se trate
Public Enum TipoImporte
   tiPrecioUnit = 0     ' Precio unitario
   tiImporteLinea = 1   ' importe total de la linea
   tiTotalFact = 2      ' total de la factura
End Enum

'Querys que utilizaremos para la anulaci�n de las facturas
Dim qryAnula(1 To 13) As New rdoQuery
Dim qry(1 To 30) As New rdoQuery
Global QrySelec(1 To 30) As New rdoQuery
Global qryFact(1 To 30) As New rdoQuery
'N� de historia que utilizaremos para rehacer una factura. Lo recogeremos a la hora del borrado de
'la tabla FA0300 en base a la factura que estamos borrando.
Dim Historia As Long

Private varFact As factura
Private Paciente As Paciente
Private Asistencia As Asistencia
Private NuevoRNF As rnf

Global blnPermitirModifFact As Boolean
Global blnFacturacionMasiva As Boolean

Sub BorradoFactura(factura As String)
Dim Cont As Integer
Dim MiRs As rdoResultset
Dim rsCodigo As rdoResultset
Dim rsHistoria As rdoResultset

  On Error GoTo ErrorEnAnulacion
  objApp.rdoConnect.BeginTrans
    'Comprobaremos que la factura no se ha contabilizado.
    qryAnula(2).rdoParameters(0) = factura
    Set MiRs = qryAnula(2).OpenResultset
    If Not IsNull(MiRs(0)) Then
      'La factura est� contabilizada habr� que estudiarlo.
      'Asignaremos el 0 al N�mero de factura real para indicarle que ha sido anulada.
      qryAnula(3).rdoParameters(0) = 0
      qryAnula(3).rdoParameters(1) = factura 'N� de la factura
      qryAnula(3).Execute
      'Le indicaremos que la factura est� compensada para que no apareza en los recordatorios.
      qryAnula(9).rdoParameters(0) = factura 'n� de la factura
      qryAnula(9).Execute
      'Seleccionaremos todos los c�digos de factura que tienen ese n� de factura para borrar los rnfs y las compensaciones
      qryAnula(10).rdoParameters(0) = factura 'N� de la factura
      Set rsCodigo = qryAnula(10).OpenResultset
      If Not rsCodigo.EOF Then
        While Not rsCodigo.EOF
          'Seleccionamos el n� de la historia en base al c�digo y la FA0300
          qryAnula(11).rdoParameters(0) = rsCodigo(0)
          Set rsHistoria = qryAnula(11).OpenResultset
          If Not rsHistoria.EOF Then
            Historia = rsHistoria(0)
          End If
          'Borraremos de la FA0300 los rnfs relacionados con esa factura
          qryAnula(5).rdoParameters(0) = rsCodigo(0) 'C�digo de la factura
          qryAnula(5).Execute
          ' Marcar los permanentes como no facturados
          qryAnula(5).rdoParameters(0) = rsCodigo(0) 'C�digo de la factura
          qryAnula(5).Execute
          'Borraremos de la FA1800 las posibles compensaciones que se refieran a esa factura
          qryAnula(8).rdoParameters(0) = rsCodigo(0) 'Codigo de la factura
          qryAnula(8).Execute
          rsCodigo.MoveNext
        Wend
      End If
    Else
      'La factura est� sin contabilizar la anulamos sin m�s.
      'Asignaremos el 0 al N�mero de factura real para indicarle que ha sido anulada.
      qryAnula(3).rdoParameters(0) = 0
      qryAnula(3).rdoParameters(1) = factura 'N� de la factura
      qryAnula(3).Execute
      'Le indicaremos que la factura est� compensada para que no apareza en los recordatorios.
      qryAnula(9).rdoParameters(0) = factura 'n� de la factura
      qryAnula(9).Execute
      'Seleccionaremos todos los c�digos de factura que tienen ese n� de factura para borrar los rnfs y las compensaciones
      qryAnula(10).rdoParameters(0) = factura 'N� de la factura
      Set rsCodigo = qryAnula(10).OpenResultset
      If Not rsCodigo.EOF Then
        While Not rsCodigo.EOF
          'Seleccionamos el n� de la historia en base al c�digo y la FA0300
          qryAnula(11).rdoParameters(0) = rsCodigo(0)
          Set rsHistoria = qryAnula(11).OpenResultset
          If Not rsHistoria.EOF Then
            Historia = rsHistoria(0)
          End If
          'Borraremos de la FA0300 los rnfs relacionados con esa factura
          qryAnula(5).rdoParameters(0) = rsCodigo(0) 'C�digo de la factura
          qryAnula(5).Execute
          'Borraremos de la FA1800 las posibles compensaciones que se refieran a esa factura
          qryAnula(8).rdoParameters(0) = rsCodigo(0) 'Codigo de la factura
          qryAnula(8).Execute
          rsCodigo.MoveNext
        Wend
      End If
    End If
  objApp.rdoConnect.CommitTrans
  
Exit Sub

ErrorEnAnulacion:
  objApp.rdoConnect.RollbackTrans
  MsgBox "Se ha producido un error en la anulaci�n de las facturas, vuelva a realizar el proceso", vbOKOnly + vbCritical, "Atenci�n"

End Sub

' Mediante la funci�n comprobar borrado recuperaremos los datos de la factura y los mostraremos
' para que la persona que est� utilizando el programa nos indique si realmente quiere realizar el
' borrado de la factura y generaci�n de una factura con el mismo proceso-asistencia.
Function ConfirmarBorrado(factura As String) As Boolean
Dim respuesta As Integer
Dim SqlResponsable As String
Dim rsResponsable As rdoResultset
Dim sqlDatos As String
Dim rsdatos As rdoResultset
Dim Responsable As String
Dim Texto As String

  'Comprobamos que se nos ha pasado una factura
  If Trim(factura) <> "" Then
    'Seleccionamos los datos que vamos a mostrar de la factura
    sqlDatos = "SELECT CI32CODTIPECON, CI13CODENTIDAD, CI21CODPERSONA, " & _
                    " FA04DESCRIP,FA04CANTFACT,FA04FECFACTURA " & _
                    " FROM FA0400 " & _
                    " wHERE FA04NUMFACT = '" & factura & "'"
    Set rsdatos = objApp.rdoConnect.OpenResultset(sqlDatos, 3)
    If Not rsdatos.EOF Then
      rsdatos.MoveLast
      rsdatos.MoveFirst
      If Not IsNull(rsdatos("CI21CODPERSONA")) Then
        'Buscaremos los datos personales del responsable econ�mico de la factura
        SqlResponsable = " SELECT CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL " & _
                                  " FROM CI2200 " & _
                                  " WHERE CI21CODPERSONA = " & rsdatos("CI21CODPERSONA")
        Set rsResponsable = objApp.rdoConnect.OpenResultset(SqlResponsable, 3)
        If Not rsResponsable.EOF Then
          Responsable = rsResponsable("CI22NOMBRE") & " " & rsResponsable("CI22PRIAPEL") & " " & rsResponsable("CI22SEGAPEL")
        Else
          MsgBox "No se ha encontrado el responsable econ�mico. No seguir� el borrado", vbInformation + vbOKOnly, "Aviso"
          ConfirmarBorrado = False
          Exit Function
        End If
      ElseIf Not IsNull(rsdatos("CI32CODTIPECON")) And Not IsNull(rsdatos("CI13CODENTIDAD")) Then
        'Buscaremos el nombre de la entidad responsable economica de la factura
        SqlResponsable = "SELECT CI13DESENTIDAD " & _
                                  " FROM CI1300" & _
                                  " WHERE CI32CODTIPECON = '" & rsdatos("CI32CODTIPECON") & "'" & _
                                  " AND CI13CODENTIDAD = '" & rsdatos("CI13CODENTIDAD") & "'"
        Set rsResponsable = objApp.rdoConnect.OpenResultset(SqlResponsable, 3)
        If Not rsResponsable.EOF Then
          Responsable = rsResponsable("CI13DESENTIDAD")
        Else
          MsgBox "No se ha encontrado la entidad responsable. No seguir� el borrado", vbInformation + vbOKOnly, "Aviso"
          ConfirmarBorrado = False
          Exit Function
        End If
      End If
      Texto = "Se va a procecer a la reelaboraci�n de la factura " & factura & Chr(10) & Chr(13)
      Texto = Texto & "A nombre de: " & Responsable & Chr(10) & Chr(13)
      If Not IsNull(rsdatos("FA04FECFACTURA")) Then
        Texto = Texto & "De fecha: " & Format(rsdatos("FA04FECFACTURA"), "dd/mm/yyyy") & Chr(10) & Chr(13)
      End If
      If Not IsNull(rsdatos("FA04CANTFACT")) Then
        Texto = Texto & "Por un total de: " & Format(rsdatos("FA04CANTFACT"), "###,###,##0.##") & Chr(10) & Chr(13)
      End If
      Texto = Texto & "�Es esto correcto?"
      respuesta = MsgBox(Texto, vbQuestion + vbYesNo, "Atenci�n")
      If respuesta = vbYes Then
        ConfirmarBorrado = True
        Exit Function
      Else
        ConfirmarBorrado = False
        Exit Function
      End If
    Else
      MsgBox "No se ha encontrado ninguna factura con el n�mero seleccionado", vbInformation + vbOKOnly, "Aviso"
      ConfirmarBdorrado = False
      Exit Function
    End If
  End If
End Function


Sub FacturaMasiva(Historia As Long, proceso As Long, Asistencia As Long, FecInicio As String, FecFin As String, FechaFact As String)

Dim IntPosGuion As Integer
Dim strProcAsist As String
Dim IntCont As Integer
Dim lngFilas As Long
Dim ProcAsist As String
Dim blnFacturaConjunta As Boolean
Dim x As Integer

  Set varFact = New factura
  ' activar la facturacion masiva para que no muestre mensajes que puedan
  ' interrumpir el proceso autom�tico de facturas
  blnFacturacionMasiva = True
  
  ProcAsist = "("
  strProcAsist = ""

  ' generar la factura de los nodos seleccionados
  frm_FactMasiva.MousePointer = vbHourglass
  
  ProcAsist = ProcAsist & " (AD07CODPROCESO = " & proceso & " AND AD01CODASISTENCI = " & Asistencia & ") OR"
  strProcAsist = strProcAsist & proceso & "," & Asistencia & ","

  frm_FactMasiva.MousePointer = vbNormal

  
  If ProcAsist <> "(" Then
    ProcAsist = Left(ProcAsist, Len(ProcAsist) - 3) & ")"
    strProcAsist = Left(strProcAsist, Len(strProcAsist) - 1)
  
    If Len(ProcAsist) > 40 Then
        blnFacturaConjunta = True
    End If
    
    InicQrySelec2 (ProcAsist)
  
    Call GenerarSqlRNF(strProcAsist, FecInicio, FecFin)
  
    'Call BuscarPruebas
    Call LlenarTypeRNF(ProcAsist, Historia, FecInicio, FecFin)
    
    varFact.AsignarRNFs
    
    Call GuardarFacturaMasiva(Historia, FechaFact)
  End If

  varFact.Destruir
  Set varFact = Nothing
  
End Sub

Sub GuardarFacturaMasiva(Historia As Long, Optional FechaFact As String = "")
    Dim CodFactura As Long
    Dim NumFactura As String
    Dim Dto As Double
    Dim IntContLineas As Integer
    Dim Facturada As Boolean
    Dim SubImporte As Double
    Dim IntCont As Integer
    Dim rsPaciente As rdoResultset
    Dim intPos As Integer
    Dim Coletilla As String
    Dim ProcesoAsistencia As ProcAsist
    Dim rsEntidad As rdoResultset
    Dim objREco As Persona
    Dim Linea As Nodo
    Dim lngProceso As Long
    Dim lngAsistencia As Long
    Dim NoFactura As String
    Dim Propuesta As PropuestaFactura
    Dim intProp As Integer
    Dim Cont As Integer
    Dim PrioriAnt As Integer
    On Error GoTo ErrorenGrabacion
    Dim Cod36 As Long
    Dim TipoFact As String
    
    'Abrimos una transacci�n para tratar toda la inserci�n de las facturas.
    objApp.rdoConnect.BeginTrans
      
    'Nos posicionamos en la propuesta seleccionada dentro del objeto myFactura
    'CodPaciente = CStr(Me.grdSSPac.Columns(0).Text)
    Set Paciente = varFact.Pacientes(CStr(Historia))
    Set Asistencia = Paciente.Asistencias(1)
    
    CodFactura = fNextClave("FA04CODFACT", "FA0400")
    Asistencia.Facturado = True
    intPos = InStr(1, Asistencia.Codigo, "-")
    lngProceso = CLng(Left(Asistencia.Codigo, intPos - 1))
    lngAsistencia = CLng(Right(Asistencia.Codigo, Len(Asistencia.Codigo) - intPos))
    Cont = 1
    PrioriAnt = 32100
    For Each Propuesta In Asistencia.PropuestasFactura
'      Cod36 = fNextClave("FA36NUMPROP", "FA3600")
'      qry(7).rdoParameters(0) = CodFactura
'      qry(7).rdoParameters(2) = Propuesta.Name
'      qry(7).rdoParameters(3) = CLng(Propuesta.Importe)
'      qry(7).rdoParameters(1) = Propuesta.Prioridad
'      qry(7).rdoParameters(4) = Cod36
'      qry(7).rdoParameters(5) = 0
'      qry(7).Execute
      If Propuesta.Prioridad < PrioriAnt Then
        intProp = Cont
        PrioriAnt = Propuesta.Prioridad
      End If
      Cont = Cont + 1
    Next
    If intProp = 0 Then intProp = 1
    Set PropuestaFact = Asistencia.PropuestasFactura(intProp)
'    qry(8).rdoParameters(0) = CodFactura
'    qry(8).rdoParameters(1) = PropuestaFact.Name
'    qry(8).Execute
    
'  End If
   
    TipoFact = frm_FactMasiva.txtCodTipEcon & frm_FactMasiva.txtCodEntidad
    TipoFact = TipoFact & IIf(frm_FactMasiva.optHopit.Value = True, "1", "2")

    Asistencia.GrabarFactura PropuestaFact.Key, FechaFact, TipoFact, False
    Exit Sub

'**************************************************************
' CONTROL DE ERRORES
'**************************************************************
ErrorenGrabacion:
    'Se ha producido un error en la grabaci�n, anulamos la transacci�n
    objApp.rdoConnect.RollbackTrans
    Debug.Print "Se ha producido un error en la grabaci�n:" & Historia
    Write #1, "Se ha producido un error en la grabaci�n:" & Historia
    'Resume Next
End Sub

Sub Main()

End Sub

Private Function ProxNumFact() As String
  If frm_FactMasiva.optHopit.Value = True Then
    ProxNumFact = frm_FactMasiva.txtCodTipEcon & frm_FactMasiva.txtCodEntidad & "1/" & fNextClave("FA04CODFACT", "FA0400")
  Else
    ProxNumFact = frm_FactMasiva.txtCodTipEcon & frm_FactMasiva.txtCodEntidad & "2/" & fNextClave("FA04CODFACT", "FA0400")
  End If
'    ProxNumFact = objSecurity.strMachine
End Function


'Sub GenerarSqlRNF(Proceso As Long, Asistencia As Long, FechaInicio As String, FechaFin As String)
Sub GenerarSqlRNF(ProcesoAsistencia As String, FechaInicio As String, FechaFin As String)

Dim MiSql As String
Dim MiRs As rdoResultset
Dim Cadena As String
Dim aProcAsist() As String
Dim Proc As String
Dim Asist As String
Dim x As Integer
Dim i As Integer
  
    
    ' primero se BORRAN los RNFs existentes en la FA0300 del mismo
    ' proceso-asistencia y NO FACTURADOS
    QrySelec(11).Execute
    x = 0
    Do While ProcesoAsistencia <> ""
        x = x + 1
        If Left(ProcesoAsistencia, 1) = "," Then
            ProcesoAsistencia = Right(ProcesoAsistencia, Len(ProcesoAsistencia) - 1)
        End If
        Proc = Left(ProcesoAsistencia, 10)
        ProcesoAsistencia = Right(ProcesoAsistencia, Len(ProcesoAsistencia) - 11)
        Asist = Left(ProcesoAsistencia, 10)
        ProcesoAsistencia = Right(ProcesoAsistencia, Len(ProcesoAsistencia) - 10)
         
        ReDim Preserve aProcAsist(1 To 2, 1 To x)
        aProcAsist(1, x) = Proc
        aProcAsist(2, x) = Asist
    Loop

    On Error GoTo errorRNFs
    
    Cadena = ""
    'Seleccionamos todas las vistas de los grupos que se encuentren activos
    Set MiRs = QrySelec(6).OpenResultset(rdOpenKeyset)
    If Not MiRs.EOF Then
        MiRs.MoveLast
        MiRs.MoveFirst
        While Not MiRs.EOF
            For i = 1 To x
               ' En las vistas de farmacia discriminar el control de los RNF ya facturados en funcion
               ' de la fecha :
               '  en los anteriores al 01/04/2000 no se compara la cantidad (para evitar el problema de cambio de dosis)
               '  en las posteriores si que se compara
               If MiRs(0) = "FAIBM0004J" Or MiRs(0) = "FAIBM0005J" Then
               
                  'Creamos la SQL de selecci�n de los RNF, una para cada Proceso-Asistencia
                  MiSql = "INSERT INTO FA0300 " & _
                            "(FA03NUMRNF,FA05CODCATEG, FA03PRECIO, FA03PRECIOFACT, FA03CANTIDAD, FA03CANTFACT, FA13CODESTRNF,AD01CODASISTENCI, " & _
                            " FA03FECHA,CI22NUMHISTORIA,FA03OBSERV,AD07CODPROCESO, FA03INDREALIZADO, " & _
                            " AD02CODDPTO_R, SG02COD_R, AD02CODDPTO_S, SG02COD_S) " & _
                            "  SELECT " & _
                            "    FA03NUMRNF_SEQUENCE.NEXTVAL, FA05CODCATEG, PRECIO, PRECIO, CANTIDAD,  CANTIDAD,  1, " & _
                            "  AD01CODASISTENCI,  FECHA,  CI22NUMHISTORIA, " & _
                            "    OBS , AD07CODPROCESO, ESTADO, DPTOREALIZA, DRREALIZA, DPTOSOLICITA, DRSOLICITA " & _
                            "  From " & MiRs(0) & " T1 " & _
                            "  Where " & _
                            "        T1.AD07CODPROCESO = " & aProcAsist(1, i) & _
                            "    AND T1.AD01CODASISTENCI = " & aProcAsist(2, i) & _
                            "    AND T1.IC = 0 " & _
                    IIf(FechaInicio <> "", " AND T1.FECHA >= TO_DATE('" & FechaInicio & " 00:00:00' , 'DD/MM/YYYY HH24:MI:SS') ", "") & _
                    IIf(FechaFin <> "", " AND T1.FECHA <= TO_DATE('" & FechaFin & " 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') ", "")
                    
                  MiSql = MiSql & _
                            "    AND " & _
                            "       ( (T1.FECHA <= TO_DATE('31/03/2000 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') AND NOT EXISTS " & _
                            "          (SELECT AD07CODPROCESO , AD01CODASISTENCI, FA05CODCATEG, FA03FECHA " & _
                            "           FROM FA0300 T2 " & _
                            "           WHERE " & _
                            "             T2.AD07CODPROCESO = T1.AD07CODPROCESO " & _
                            "             AND T2.AD01CODASISTENCI=T1.AD01CODASISTENCI " & _
                            "             AND T2.FA05CODCATEG=T1.FA05CODCATEG " & _
                            "             AND T2.FA03FECHA=T1.FECHA " & _
                            "             AND T2.FA03FECHA <= TO_DATE('31/03/2000 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') " & _
                            "             AND T2.FA03INDFACT <> 3))" & _
                            "        OR " & _
                            "         (T1.FECHA > TO_DATE('31/03/2000 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') AND NOT EXISTS " & _
                            "          (SELECT AD07CODPROCESO , AD01CODASISTENCI, FA05CODCATEG, FA03FECHA, FA03CANTIDAD " & _
                            "           FROM FA0300 T2 " & _
                            "           WHERE " & _
                            "             T2.AD07CODPROCESO = T1.AD07CODPROCESO " & _
                            "             AND T2.AD01CODASISTENCI=T1.AD01CODASISTENCI " & _
                            "             AND T2.FA05CODCATEG=T1.FA05CODCATEG " & _
                            "             AND T2.FA03CANTIDAD=T1.CANTIDAD AND T2.FA03FECHA=T1.FECHA " & _
                            "             AND T2.FA03FECHA > TO_DATE('31/03/2000 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') " & _
                            "             AND T2.FA03INDFACT <> 3))" & _
                            "        )"
                            
               ' Resto de vistas que no son farmacia
               Else
                  'Creamos la SQL de selecci�n de los RNF, una para cada Proceso-Asistencia
                  MiSql = "INSERT INTO FA0300 " & _
                            "(FA03NUMRNF,FA05CODCATEG, FA03PRECIO, FA03PRECIOFACT, FA03CANTIDAD, FA03CANTFACT, FA13CODESTRNF,AD01CODASISTENCI, " & _
                            " FA03FECHA,CI22NUMHISTORIA,FA03OBSERV,AD07CODPROCESO, FA03INDREALIZADO, " & _
                            " AD02CODDPTO_R, SG02COD_R, AD02CODDPTO_S, SG02COD_S) " & _
                            "  SELECT " & _
                            "    FA03NUMRNF_SEQUENCE.NEXTVAL, FA05CODCATEG, PRECIO, PRECIO, CANTIDAD,  CANTIDAD,  1, " & _
                            "  AD01CODASISTENCI,  FECHA,  CI22NUMHISTORIA, " & _
                            "    OBS , AD07CODPROCESO, ESTADO, DPTOREALIZA, DRREALIZA, DPTOSOLICITA, DRSOLICITA " & _
                            "  From " & MiRs(0) & " T1 " & _
                            "  Where " & _
                            "        T1.AD07CODPROCESO = " & aProcAsist(1, i) & _
                            "    AND T1.AD01CODASISTENCI = " & aProcAsist(2, i) & _
                            "    AND T1.IC = 0 " & _
                    IIf(FechaInicio <> "", " AND T1.FECHA >= TO_DATE('" & FechaInicio & " 00:00:00' , 'DD/MM/YYYY HH24:MI:SS') ", "") & _
                    IIf(vbChecked And FechaFin <> "", " AND T1.FECHA <= TO_DATE('" & FechaFin & " 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') ", "")

                  MiSql = MiSql & _
                            "    AND NOT EXISTS " & _
                            "     (SELECT " & _
                            "  AD07CODPROCESO , AD01CODASISTENCI, FA05CODCATEG, FA03FECHA, FA03CANTIDAD " & _
                            "      FROM FA0300 T2 " & _
                            "      Where " & _
                            "        T2.AD07CODPROCESO = T1.AD07CODPROCESO " & _
                            "     AND T2.AD01CODASISTENCI=T1.AD01CODASISTENCI " & _
                            "     AND T2.FA05CODCATEG=T1.FA05CODCATEG " & _
                            "     AND T2.FA03CANTIDAD=T1.CANTIDAD AND T2.FA03FECHA=T1.FECHA " & _
                            "     AND T2.FA03INDFACT <> 3" & _
                            "     )"
                            
               End If
               
               ' ejecutar la vista correspondiente
               objApp.rdoConnect.Execute MiSql
            
            Next
            MiRs.MoveNext
          
        Wend
    End If
    


Exit Sub
errorRNFs:
    MsgBox "Error en el proceso de recogida de RNFs" & Chr(13) & "La factura no ser� correcta" & Chr(13) & Chr(13) & _
           Err.Number & " " & Err.Description, vbCritical + vbOKOnly, "ERROR"
    Resume Next
End Sub


Sub LlenarTypeRNF(ProcesoAsist As String, Historia As Long, FecInicio As String, FecFin As String)
   Dim MiSql As String
   Dim MiRs As rdoResultset
   Dim sqlFecha As String
   Dim rsfecha As rdoResultset
   Dim rsNombre As rdoResultset
   Dim MiSqLConc As String
   Dim MiRsConc As rdoResultset
   Dim Cont As Long
   Dim NumRNF As Long
   Dim ContRNF As Long
   Dim Fila As Integer
   Dim RNFAnterior As String
   Dim AsistAnterior As String
   Dim ProcAnterior As String
   Dim PacienteAnterior As String
   Dim fecha As String
   Dim NuevoProcAsist As ProcAsist
   Dim blnPacienteYaREco As Boolean
   Dim objREco As Persona
   Dim objPaciente As Persona
   Dim reco As Persona
   Dim blnMensajeYaMostrado As Boolean
   Dim objRNF As rnf
   Dim blnFueraDeFecha As Boolean

    
  On Error GoTo ErrorenRNF
  
  RNFAnterior = ""
  AsistAnterior = ""
  PacienteAnterior = ""
  ProcAnterior = ""
  ContRNF = 0
  ' vaciar el objeto factura
  'Set varFact = New Factura
    

    
   'Si la cadena de selecci�n de RNF no es nula.
   '*7
'    QrySelec(7).rdoParameters(0) = ProcesoAsist
   Set MiRs = QrySelec(7).OpenResultset(rdOpenKeyset)
   If Not MiRs.EOF Then
      Cont = 1
      ContRNF = 0
      Do While Not MiRs.EOF
      
         blnFueraDeFecha = False
         
'         ' ************************************************************************************************
'         ' revisar si hay que avisar de la existencia de movimientos posteriores a la fecha de fin
'         ' de la factura
'         ' ************************************************************************************************
'         If chkAplicarFiltro(0).Value = vbChecked And _
'            SDCFechaInicio.Date <> "" And _
'            CVDate(MiRs("FA03FECHA")) < CVDate(SDCFechaInicio.Date & " 00:00:00") Then
'            blnFueraDeFecha = True
'            ' sobrepasa el limite inferior de fechas
'         End If
'
'         If chkAplicarFiltro(1).Value = vbChecked And _
'            SDCFechaFin.Date <> "" And _
'            CVDate(MiRs("FA03FECHA")) > CVDate(SDCFechaFin.Date & " 23:59:59") Then
'            blnFueraDeFecha = True
'            ' sobrepasa el limite superior de fechas
'         End If
'
'
'         If chkAplicarFiltro(1).Value = vbUnchecked And SDCFechaFin.Date <> "" And Not blnMensajeYaMostrado Then
'            If CVDate(MiRs("FA03FECHA")) > CVDate(SDCFechaFin.Date & " 23:59:59") Then
'               blnMensajeYaMostrado = True
'               MsgBox "Hay movimientos posteriores a la fecha de fin de factura, que ser�n incluidos en la propuesta.", vbInformation + vbOKOnly, "Facturaci�n"
'            End If
'         End If
         
         
         If Not blnFueraDeFecha Then
            ' ************************************************************************************************
            ' si se detecta un NUEVO PACIENTE (numero de historia) crearlo y a�adirlo a la coleccion
            ' ************************************************************************************************
            If MiRs("CI22NUMHISTORIA") <> PacienteAnterior Then
               ' crear el objeto paciente
               Set Paciente = New Paciente
               Paciente.Codigo = Historia
               
               ' buscar el CI21CODPERSONA correspondiente al numero de historia
               QrySelec(8).rdoParameters(0) = Historia
               Set rsNombre = QrySelec(8).OpenResultset(rdOpenStatic)
               
               If Not rsNombre.EOF Then
                  ' utilizar un objeto persona para standarizar y simplificar la busqueda
                  ' del resto de sus datos
                  Set objPaciente = New Persona
                  objPaciente.Codigo = rsNombre("CI21CODPERSONA") & ""
                  
                  With Paciente
                        .CodPersona = objPaciente.Codigo
                        .Name = objPaciente.Name
                        .Direccion = objPaciente.Direccion
                        .FecNacimiento = objPaciente.FecNacimiento
                     
                  End With
               End If
                
               ' a�adirlo a la coleccion
               varFact.Pacientes.Add Paciente, MiRs("CI22NUMHISTORIA") & ""
               
               ' guardar su numero de historia para compararlo con el siguiente
               PacienteAnterior = MiRs("CI22NUMHISTORIA")
            End If
            
            
            
            ' ************************************************************************************************
            ' Ver si el PROCESO-ASISTENCIA es el primero o ha cambiado para procesarlo de diferente manera:
            '     - si es la primera : se crea el objeto asistencia para contener los RNFs y las propuestas
            '
            '     - si es distinta de la anterior : no se crea el objeto
            '
            '     - en ambos casos se buscan sus REcos para a�adirlos a la asistencia
            ' ************************************************************************************************
            If MiRs("AD01CODASISTENCI") <> AsistAnterior Or MiRs("AD07CODPROCESO") <> ProcAnterior Then
               If AsistAnterior = "" Or ProcAnterior = "" Then
                  ' esta es la primera asistencia entonces la creamos
                  Set Asistencia = New Asistencia
                  
                  Asistencia.Codigo = MiRs("AD07CODPROCESO") & "-" & MiRs("AD01CODASISTENCI")
      
                  Asistencia.FactAntigua = FactContabil
                  Asistencia.NumFactAnt = NoFactura
                  Asistencia.FechaAnterior = FecAnte
                  
                  Asistencia.Facturado = False
                  Asistencia.strProcAsist = ProcesoAsist
                  
                  
                  ' Hay que recoger las fechas del proceso-asistencia
                  sqlFecha = "Select AD08FECINICIO, AD08FECFIN From AD0800" & _
                                  " Where AD01CODASISTENCI = " & MiRs("AD01CODASISTENCI") & " AND " & _
                                  " AD07CODPROCESO = " & MiRs("AD07CODPROCESO")
                  Set rsfecha = objApp.rdoConnect.OpenResultset(sqlFecha, 3)
                  
                  If Not rsfecha.EOF Then
                      ' cargar las fechas del filtro para la coletilla de la factura
'                      If FechaInicio <> "" Then
'                          Asistencia.FecInicioFact = SDCFechaInicio.Date
'                      Else
                          If Not IsNull(rsfecha("AD08FECINICIO")) Then
                            Asistencia.FecInicioFact = Format(rsfecha("AD08FECINICIO"), "DD/MM/YYYY")
                          End If
'                      End If
                      
'                      If SDCFechaFin.Date <> "" Then
'                          Asistencia.FecFinFact = SDCFechaFin.Date
'                      Else
                          If Not IsNull(rsfecha("AD08FECFIN")) Then
                            Asistencia.FecFinFact = Format(rsfecha("AD08FECFIN"), "DD/MM/YYYY")
                          End If
'                      End If
                      
                      ' fechas reales de la asistencia (puede que haya RNF anteriores o posteriores a la asistencia,
                      ' con lo cual se prolongarian estas fechas para contenerlos a todos.
                      If Not IsNull(rsfecha("AD08FECINICIO")) Then
                          Asistencia.FecInicio = Format(rsfecha("AD08FECINICIO"), "DD/MM/YYYY HH:MM:SS")
                      End If
                      If Not IsNull(rsfecha("AD08FECFIN")) Then
                          Asistencia.FecFin = Format(DateAdd("D", 1, CDate(rsfecha("AD08FECFIN"))), "DD/MM/YYYY HH:MM:SS")
                      End If
                  End If
                  
                  
                  ' Hay que recoger el ultimo dpto responsable del proceso-asistencia
                  sqlFecha = "select AD0500.AD02CODDPTO, AD02DESDPTO from ad0500, ad0200 " & _
                             "Where AD01CODASISTENCI = " & MiRs("AD01CODASISTENCI") & _
                                  " AND AD07CODPROCESO = " & MiRs("AD07CODPROCESO") & _
                                  " AND ad0500.AD02codDPTO = ad0200.AD02codDPTO " & _
                                  " AND AD05FECFINRESPON is null"
                                  
                  Set rsfecha = objApp.rdoConnect.OpenResultset(sqlFecha, 3)
                  
                  If Not rsfecha.EOF Then
                     Asistencia.CodDptoResp = "" & rsfecha("AD02codDPTO").Value
                     Asistencia.DptoResp = "" & rsfecha("AD02DESDPTO").Value
                  End If
                  
                  
                  ' A�adir la asistencia al paciente
                  Paciente.Asistencias.Add Asistencia, CStr(Asistencia.Codigo)
               End If
               
               ' a�adir el proceso-asistencia (solo los codigos) a la asistencia para tener una relacion de
               ' los diferentes proc-asist que se van a facturar juntos.
               Set NuevoProcAsist = New ProcAsist
               NuevoProcAsist.Asistencia = MiRs("AD01CODASISTENCI")
               NuevoProcAsist.proceso = MiRs("AD07CODPROCESO")
               Asistencia.ProcAsist.Add NuevoProcAsist
               
               AsistAnterior = MiRs("AD01CODASISTENCI")
               ProcAnterior = MiRs("AD07CODPROCESO")
                  
                  
               
               ' ************************************************************************************************
               ' hay que buscar los recos de la asistencia y a�adirlos a la misma para despues localizar a que
               ' Reco corresponde cada RNF
               ' ************************************************************************************************
               QrySelec(9).rdoParameters(0) = MiRs("AD01CODASISTENCI")
               QrySelec(9).rdoParameters(1) = MiRs("AD07CODPROCESO")
               Set MiRsConc = QrySelec(9).OpenResultset(rdOpenKeyset)
            
               Do While Not MiRsConc.EOF
                  If Trim(MiRsConc("FA09CODNODOCONC")) <> "" Then
                     If IsNumeric(MiRsConc("FA09CODNODOCONC")) Then
                        ' a�adir el nodo de concierto a la asistencia
                        If Not IsNull(MiRsConc("FA09CODNODOFACT")) Then
                           Asistencia.AddConcierto MiRsConc("FA09CODNODOCONC"), MiRsConc("FA09CODNODOFACT")
                        Else
                           Asistencia.AddConcierto MiRsConc("FA09CODNODOCONC")
                        End If
                        ' crear el REco
                        Set reco = New Persona
                        If "" & MiRsConc("CI21CODPERSONA") <> "" Then
                           reco.Codigo = MiRsConc("CI21CODPERSONA")
                        Else
                           reco.Codigo = Paciente.CodPersona
                        End If
                        
                        reco.Concierto.Concierto = MiRsConc("FA09CODNODOCONC")
                        If Not IsNull(MiRsConc("FA09CODNODOFACT")) Then
                           reco.Concierto.NodoAFacturar = MiRsConc("FA09CODNODOFACT")
                        End If
   '                     reco.Concierto.Name = "" & MiRsConc("FA09DESIG")
                        reco.CodTipEcon = "" & MiRsConc("CI32CODTIPECON")
                        reco.EntidadResponsable = "" & MiRsConc("CI13CODENTIDAD")
                        reco.DesEntidad = "" & MiRsConc("CI13DESENTIDAD")
                        reco.proceso = MiRs("AD07CODPROCESO")
                        reco.Asistencia = MiRs("AD01CODASISTENCI")
                        ' a�adirlo
                        
                        Asistencia.AddREco reco, "" & MiRsConc("AD11FECINICIO"), "" & MiRsConc("AD11FECFIN")
                     End If
                  End If
                  MiRsConc.MoveNext
               Loop
      
            End If
            
            
            
            ' ************************************************************************************************
            ' crear y a�adir el rnf a la asistencia
            ' ************************************************************************************************
   
            With Asistencia
               ' si es un proceso-asistencia distinto a�adirlo a la coleccion de proc-asist.
               If MiRs("AD01CODASISTENCI") <> AsistAnterior Or MiRs("AD07CODPROCESO") <> ProcAnterior Then
                   Set NuevoProcAsist = New ProcAsist
                   NuevoProcAsist.Asistencia = MiRs("AD01CODASISTENCI")
                   NuevoProcAsist.proceso = MiRs("AD07CODPROCESO")
                   .ProcAsist.Add NuevoProcAsist
               End If
   
               If MiRs("FA05CODCATEG") <> RNFAnterior Then
                 Cont = 0
                 RNFAnterior = MiRs("FA05CODCATEG")
               Else
                 Cont = Cont + 1
               End If
               Set NuevoRNF = New rnf
               With NuevoRNF
                   .CodCateg = MiRs("FA05CODCATEG")
                   .CodRNF = MiRs("FA03NUMRNF")
                   .fecha = MiRs("FA03FECHA") & ""
                   If IsNumeric(MiRs("FA03CANTFACT")) Then
                       .Cantidad = MiRs("FA03CANTFACT")
                   End If
                   If IsNumeric(MiRs("FA03PRECIOFACT")) Then
                       .Precio = MiRs("FA03PRECIOFACT")
                   Else
                     .Precio = -1
                   End If
                   .Asignado = False
                   
                   .Name = MiRs("FA03OBSIMP") & ""
                   .Descripcion = MiRs("FA03OBSERV") & ""
                   
                   If Not IsNull(MiRs("FA03INDFACT")) Then
                       Select Case MiRs("FA03INDFACT")
                           Case 0 ' Pendiente de facturar
                               .Facturado = False
'                               .Permanente = False
                           Case 1 ' Introducci�n o modificaci�n manual
                               .Facturado = False
'                               .Permanente = True
                           Case 2 ' Facturado
                               .Facturado = True
'                               .Permanente = False
                           Case 3 ' Se ha compensado (Anulado) con otra factura
                               .Facturado = True
'                               .Permanente = False
                           Case 4 ' compensa otra factura
                               .Facturado = True
'                               .Permanente = False
                       End Select
                   Else
                       .Facturado = False
'                       .Permanente = False
                   End If
                   
                  If IsNull(MiRs("FA03INDPERMANENTE")) Then
                     .Permanente = False
                  Else
                     .Permanente = True
                  End If
                   
                   If Not IsNull(MiRs("FA03PERIODOGARA")) Then
                     .PeriodoGara = MiRs("FA03PERIODOGARA")
                     Asistencia.FechaFinGarantia = Format(DateAdd("d", MiRs("FA03PERIODOGARA"), MiRs("FA03FECHA")), "YYYYMMDD")
                   End If
                   
                   .CodProceso = MiRs("AD07CODPROCESO")
                   .CodAsistenci = MiRs("AD01CODASISTENCI")
                   
   '                .KeyREco = Asistencia.BuscarREco(.Fecha, .CodProceso, .CodAsistenci)
               End With
               
               ' Guardar las propiedades del rnf susceptibles de modificacion para poder recuperarlas
               ' despues
               NuevoRNF.Guardar
               ' A�adir el nuevo RNF a la asistencia
              .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & Format(Cont, "000")
               NuevoRNF.Key = MiRs("FA05CODCATEG") & "-" & Format(Cont, "000")
               
               MiRs.MoveNext
               
               If CVDate(NuevoRNF.fecha) < CVDate(Asistencia.FecInicio) Then
'                 Asistencia.FecInicio = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
                 Asistencia.FecInicio = Format(NuevoRNF.fecha, "DD/MM/YYYY 00:00:00")
               End If
               
               If CVDate(NuevoRNF.fecha) > CVDate(IIf(Asistencia.FecFin <> "", Asistencia.FecFin, "01/01/1900")) Then
'                 Asistencia.FecFin = Format(DateAdd("D", 1, CDate(NuevoRNF.Fecha)), "DD/MM/YYYY HH:MM:SS")
                 Asistencia.FecFin = Format(DateAdd("D", 1, CDate(NuevoRNF.fecha)), "DD/MM/YYYY 00:00:00")
               End If
               
             End With
         Else
            MiRs.MoveNext

             
         End If
      Loop
      
      
      
      For Each Paciente In varFact.Pacientes
         ' ************************************************************************************************
         ' A�adir el paciente como REco en la Asistencia, si no estaba ya
         ' ponerlo por defecto
         ' ************************************************************************************************
         blnPacienteYaREco = False
         For Each reco In Asistencia.REcos
             If reco.Codigo = Paciente.CodPersona Then
                 blnPacienteYaREco = True
                 Exit For
             End If
         Next
         If Not blnPacienteYaREco Then
             Asistencia.AddConcierto 97, 328
                         
             Set reco = New Persona
             reco.Codigo = Paciente.CodPersona
                     reco.Concierto.Concierto = 97
                     reco.Concierto.NodoAFacturar = 328
'                     reco.Concierto.Name = "" & MiRsConc("FA09DESIG")
                     reco.CodTipEcon = "P"
                     reco.EntidadResponsable = "A"
                     reco.DesEntidad = "Privado A"
'                     reco.proceso = MiRs("AD07CODPROCESO")
'                     reco.Asistencia = MiRs("AD01CODASISTENCI")

             Asistencia.AddREco reco, Asistencia.FecInicio, Asistencia.FecFin
         End If
      
         Asistencia.DefaultREco = reco.Key
         
         ' ************************************************************************************************
         ' realizar la asignacion de REcos a los RNFs
         ' ************************************************************************************************
         For Each objRNF In Asistencia.RNFs
            objRNF.KeyREco = Asistencia.BuscarREco(objRNF.fecha, objRNF.CodProceso, objRNF.CodAsistenci)
         Next objRNF
      Next Paciente
      
      ' generar las propuestas de factura
      'varFact.AsignarRNFs
      
      
      ' lanzar el formulario de seleccion de propuestas de factura
      'frm_Propuestas.pPropuestas varFact
      'MsgEstado ""
   Else
      Debug.Print "No hay ning�n registro que responda a las Condiciones del filtro " & Historia
      Write #1, "No hay ning�n registro que responda a las Condiciones del filtro " & Historia
      BotonSalir = True
   End If

Exit Sub
ErrorenRNF:
    Select Case Err.Number
    Case 13
        ' se ha producido un error en las fechas de los RNFs
        Resume Next
    Case 457
        Cont = Cont + 1
        Resume
    Case Else
        MsgBox "Se ha producido un error en la inserci�n de los RNF", vbCritical + vbOKOnly, "Aviso"
        objApp.rdoConnect.RollbackTrans
        Resume Next
    End Select
End Sub



Private Function ProxNumFactMasiva() As String
  If frm_FactMasiva.optHopit.Value = True Then
    ProxNumFactMasiva = frm_FactMasiva.txtCodTipEcon & frm_FactMasiva.txtCodEntidad & "1/" & fNextClave("FA04CODFACT", "FA0400")
  Else
    ProxNumFactMasiva = frm_FactMasiva.txtCodTipEcon & frm_FactMasiva.txtCodEntidad & "2/" & fNextClave("FA04CODFACT", "FA0400")
  End If
'    ProxNumFact = objSecurity.strMachine
End Function

Sub InicQrySelec2(ProcesoAsistencia As String)
    Dim MiSql As String
    Dim x As Integer
  
    '+1
    'Proceso - Asistencia
'    MiSql = "SELECT * FROM FA0300 " & _
              "WHERE (AD07CODPROCESO, AD01CODASISTENCI ) IN " & ProcesoAsistencia & _
              " AND FA03INDREALIZADO = 0"
    MiSql = "SELECT * FROM FA0300 " & _
              "WHERE " & ProcesoAsistencia & _
              " AND FA03INDREALIZADO = 0"
    QrySelec(1).SQL = MiSql
    'Activamos la query.
    Set QrySelec(1).ActiveConnection = objApp.rdoConnect
    QrySelec(1).Prepared = True

    '+7
    'Proceso-Asistencia
'    MiSql = "SELECT * FROM FA0300 WHERE (AD07CODPROCESO, AD01CODASISTENCI ) IN " & ProcesoAsistencia & " AND FA03INDREALIZADO <> 0" & _
                " AND FA04NUMFACT IS NULL " & _
               " order by CI22NUMHISTORIA, AD07CODPROCESO, AD01CODASISTENCI, FA05CODCATEG, FA03FECHA"
    MiSql = "SELECT * FROM FA0300 WHERE " & ProcesoAsistencia & " AND FA03INDREALIZADO > 0" & _
                " AND FA04NUMFACT IS NULL " & _
                " order by CI22NUMHISTORIA, AD07CODPROCESO, AD01CODASISTENCI, FA05CODCATEG, FA03FECHA"
    QrySelec(7).SQL = MiSql
    'Activamos la query.
    Set QrySelec(7).ActiveConnection = objApp.rdoConnect
    QrySelec(7).Prepared = True
  
    ' borrar los registros de RNFs no facturados de la fa0300 para ese proceso, asistencia
    MiSql = "DELETE from FA0300 " & _
            "Where " & ProcesoAsistencia & " AND FA04NUMFACT is NULL AND FA03INDPERMANENTE is NULL"
    QrySelec(11).SQL = MiSql
    'Activamos la query.
    Set QrySelec(11).ActiveConnection = objApp.rdoConnect
    QrySelec(11).Prepared = True


End Sub

Sub InicQrySelec()
Dim MiSql As String
Dim x As Integer
  
  '+1
'  'Proceso - Asistencia
'  MiSql = "SELECT * FROM FA0300 " & _
'            "WHERE (AD07CODPROCESO, AD01CODASISTENCI ) IN ? " & _
'            " AND FA03INDREALIZADO = 0"
'  QrySelec(1).SQL = MiSql
  '+2
  'Codigo de la persona
  MiSql = "Select AD07CODPROCESO, AD07DESNOMBPROCE FROM AD0700 WHERE CI21CODPERSONA = ? " & _
                " AND AD07FECHORAINICI >= TO_DATE('01/01/1997','DD/MM/YYYY') ORDER BY AD07FECHORAINICI DESC"
  QrySelec(2).SQL = MiSql
  '+3
  'Proceso
  MiSql = "Select AD0800.AD01CODASISTENCI, AD0800.AD07CODPROCESO,AD0100.AD01FECINICIO," & _
               " AD0100.AD01FECFIN,AD0200.AD02DESDPTO,SG0200.SG02NOM,SG0200.SG02APE1,SG0200.SG02APE2," & _
               " AD1100.CI32CODTIPECON,AD1100.CI13CODENTIDAD" & _
               " FROM AD0800, AD0100,AD0200,SG0200,AD1100,AD0500" & _
               " WHERE AD0800.AD07CODPROCESO = ? " & _
               " AND (AD0100.AD01CODASISTENCI = AD0800.AD01CODASISTENCI) " & _
               " AND (AD0800.AD01CODASISTENCI = AD0500.AD01CODASISTENCI AND " & _
                     "AD0800.AD07CODPROCESO = AD0500.AD07CODPROCESO) " & _
               " AND (AD0500.AD02CODDPTO = AD0200.AD02CODDPTO) AND " & _
                     "(AD0500.SG02COD = SG0200. SG02COD) " & _
               " AND AD0500.AD05FECFINRESPON IS NULL " & _
               " AND (AD0800.AD01CODASISTENCI = AD1100.AD01CODASISTENCI AND " & _
                     "AD0800.AD07CODPROCESO = AD1100.AD07CODPROCESO)" & _
               " AND AD1100.AD11FECFIN IS NULL " & _
               " ORDER BY AD0100.AD01FECINICIO DESC"
  
  'MiSqL = "Select AD01CODASISTENCI FROM  AD0800 WHERE AD07CODPROCESO = ? ORDER BY AD01CODASISTENCI DESC"
  QrySelec(3).SQL = MiSql
  '+4
  'Asistencia
  MiSql = "Select AD01FECINICIO, AD01FECFIN, AD1200.AD12CODTIPOASIST, AD12DESTIPOASIST From AD0100, AD2500, AD1200 " & _
          " Where AD0100.AD01CODASISTENCI = ? AND AD0100.AD01CODASISTENCI = AD2500.AD01CODASISTENCI(+) AND " & _
          " AD2500.AD12CODTIPOASIST = AD1200.AD12CODTIPOASIST (+) "
  QrySelec(4).SQL = MiSql
  '+5
  'Asistencia-Proceso
  MiSql = "Select FA04NUMFACT, FA04NUMFACREAL From FA0400 Where AD01CODASISTENCI = ? " & _
             " AND AD07CODPROCESO = ?"
  QrySelec(5).SQL = MiSql
  '+6
  MiSql = "SELECT FA08DESVISTA FROM FA0800 WHERE FA11CODESTGRUPO = 1"
  QrySelec(6).SQL = MiSql
  
  
  'N� de historia
'  MiSql = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, " & _
              "CI22FECNACIM, CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, CI10DESLOCALID" & _
              " from CI2200, CI1000" & _
              " where CI22NUMHISTORIA = ? " & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
              " AND CI1000.CI10INDDIRPRINC = -1)"
  MiSql = "Select CI21CODPERSONA from CI2200 where CI22NUMHISTORIA = ? "
              
  QrySelec(8).SQL = MiSql
  
  '+9
  MiSql = " SELECT FA09CODNODOCONC, FA09CODNODOFACT, CI21CODPERSONA,AD1100.CI32CODTIPECON, AD1100.CI13CODENTIDAD, " & _
            " CI13DESENTIDAD, AD11FECINICIO, AD11FECFIN " & _
            " FROM AD1100,CI1300 " & _
            " WHERE AD1100.CI32CODTIPECON=CI1300.CI32CODTIPECON " & _
            " AND AD1100.CI13CODENTIDAD=CI1300.CI13CODENTIDAD" & _
            " AND AD01CODASISTENCI = ? AND AD07CODPROCESO = ? " & _
            " ORDER BY AD11FECINICIO DESC"
  QrySelec(9).SQL = MiSql
  
  '+10
  MiSql = "Select FA05DESIG From FA0500 Where FA05CODCATEG = ?"
  QrySelec(10).SQL = MiSql
  
'    ' borrar los registros de RNFs no facturados de la fa0300 para ese proceso, asistencia
'    MiSql = "DELETE from FA0300 " & _
'            "Where (AD07CODPROCESO, AD01CODASISTENCI) IN ? AND FA04NUMFACT is NULL"
'    QrySelec(11).SQL = MiSql

  'Activamos las querys.
  For x = 1 To 11
    If x <> 1 Or x <> 7 Or x <> 11 Then
        Set QrySelec(x).ActiveConnection = objApp.rdoConnect
        QrySelec(x).Prepared = True
    End If
  Next


End Sub



Sub IniciarQRY()
Dim MiSql As String
Dim x As Integer
  
  'Seleccionar los datos de una factura en base al n� de factura
  MiSql = "Select * from FA0400 Where FA04NUMFACT = ?"
  qryAnula(1).SQL = MiSql
  
  'Comprobaremos que la factura no se ha contabilizado.
  MiSql = "Select FA04FECCONTABIL From FA0400 Where FA04NUMFACT = ?"
  qryAnula(2).SQL = MiSql
  
  'Poner en FA0400 el campo de factura real a 0 si es anulaci�n o el N� si es reconstrucci�n
  MiSql = "Update FA0400 Set FA04NUMFACREAL = ? Where FA04NUMFACT = ?"
  qryAnula(3).SQL = MiSql
  
  'Seleccionamos los datos personales de una persona.
  MiSql = " Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, " & _
              " CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, " & _
              " CI07CODPOSTAL, CI10DESLOCALID, CI34DESTRATAMI, CI22NUMHISTORIA, CI22DNI, CI30CODSEXO" & _
              " from CI2200, CI1000,CI3400 " & _
              " where CI2200.CI21CODPERSONA = ?" & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA(+)  " & _
              " AND CI1000.CI10INDDIRPRINC = -1)" & _
              " AND (CI2200.CI34CODTRATAMI = CI3400.CI34CODTRATAMI(+))"
  qryAnula(4).SQL = MiSql

  'Borrar en FA0300 los rnfs asociados con esa factura
  MiSql = "Delete From FA0300 Where FA04NUMFACT = ?  AND FA03INDPERMANENTE IS NULL"
  qryAnula(5).SQL = MiSql
  
  'Borrar en FA0300 los rnfs asociados con esa factura
  MiSql = "update FA0300 set fa04numfact = null, fa03indfact = 0 Where FA04NUMFACT = ?  AND FA03INDPERMANENTE IS NOT NULL"
  qryAnula(13).SQL = MiSql
  
  
  'Seleccionamos los datos de una Entidad que sea responsable econ�mico
  MiSql = " Select CI21CODPERSONA From CI0900 Where CI13CODENTIDAD = ?"
  qryAnula(6).SQL = MiSql
  MiSql = " Select CI23RAZONSOCIAL, CI23NUMDIRPRINC, CI10CALLE, CI10PORTAL," & _
              " CI10RESTODIREC,CI07CODPOSTAL,CI10DESLOCALID,CI26DESPROVI" & _
              " From CI2300, CI1000, CI0900,CI2600 " & _
              " Where CI2300.CI21CODPERSONA = ?" & _
              " And (CI2300.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
              " And CI1000.CI10INDDIRPRINC = -1) " & _
              " AND (CI2200.CI26CODPROVI = CI2600.CI26CODPROVI(+))"
  qryAnula(7).SQL = MiSql
        
  'Borrar en FA1800 las compensaciones que se corresponden a la factura
  MiSql = "Delete From FA1800 Where FA04CODFACT = ?"
  qryAnula(8).SQL = MiSql
  
  'En la tabla FA0400 indicaremos que la factura est� compensada.
  MiSql = "Update FA0400 Set FA04INDCOMPENSA = 2 Where FA04NUMFACT = ?"
  qryAnula(9).SQL = MiSql
  
  'Buscaremos los c�digos de factura que se corresponden con un determinado n� de factura
  MiSql = "Select FA04CODFACT From FA0400 Where FA04NUMFACT = ?"
  qryAnula(10).SQL = MiSql
  
  'Seleccionaremos el n� de historia que se ha aplicado para hacer una determinada factura de cara
  'a rehacerla posteriormente.
  MiSql = "Select CI22NUMHISTORIA From FA0300 Where FA04NUMFACT = ?"
  qryAnula(11).SQL = MiSql
  
  'Seleccionamos el proceso-asistencia correspondiente a la factura que vamos a reelaborar.
  MiSql = "Select AD07CODPROCESO, AD01CODASISTENCI From FA0400 Where FA04NUMFACT = ?"
  qryAnula(12).SQL = MiSql
  
  'Activamos las querys.
  For x = 1 To 13
    Set qryAnula(x).ActiveConnection = objApp.rdoConnect
    qryAnula(x).Prepared = True
  Next
End Sub
Sub IniciarQRYMasiva()
Dim MiSql As String
Dim x As Integer
  
'  ''Queries relacionadas con la obtenci�n de una entidad a partir del n�mero de asistencia.
'  'Seleccionar el c�digo de entidad a partir del n�mero de asistencia.
'  MiSql = "SELECT CI32CODTIPECON, CI13CODENTIDAD,CI21CODPERSONA FROM AD1100 WHERE AD01CODASISTENCI = ? " & _
'               "AND AD07CODPROCESO = ?"
'  qry(1).SQL = MiSql
'  'Seleccionar la descripci�n de la entidad
'  MiSql = "SELECT CI13DESENTIDAD, CI13CODENTIDAD FROM CI1300 WHERE CI13CODENTIDAD = ? AND CI32CODTIPECON = ?"
'  qry(2).SQL = MiSql
  
  'Seleccionar la descripci�n de una categor�a.
  MiSql = "Select FA05DESIG from FA0500 Where FA05CODCATEG = ?"
  qry(3).SQL = MiSql
  
  'Insertar una cabecera de factura en FA0400
  MiSql = "Insert Into FA0400 (FA04CODFACT,FA04NUMFACT,CI21CODPERSONA,CI13CODENTIDAD," & _
              "FA04DESCRIP,FA04CANTFACT,FA04DESCUENTO,AD01CODASISTENCI,FA04OBSERV," & _
              "FA09CODNODOCONC,AD07CODPROCESO,CI32CODTIPECON,FA04FECFACTURA, FA04INDNMGC, FA04FECDESDE, FA04FECHASTA) " & _
              "Values (?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'))"
  qry(4).SQL = MiSql
  
  'Insertar una l�nea de factura en FA1600
  MiSql = "Insert into FA1600 (FA04CODFACT,FA16NUMLINEA,FA14CODNODOCONC,FA16DESCRIP," & _
          "FA16PRCTGRNF,FA16PRECIO,FA16DCTO,FA16IMPORTE,FA16ORDFACT,FA16CANTSINFRAN,FA16FRANQUICIA" & _
          ") Values (?,?,?,?,?,?,?,?,?,?,?)"
  qry(5).SQL = MiSql
  
  'Asignar a FA0300 el codigo de la factura y el n�mero de l�nea
  MiSql = "Update FA0300 " & _
                " set FA04NUMFACT = ?, FA16NUMLINEA = ?, FA03INDDESGLOSE = ?, FA15CODATRIB = ?, " & _
                " FA03PRECIOFACT = ?, FA03CANTFACT = ?, FA03OBSIMP = ? " & _
                " Where FA03NUMRNF = ?"
  qry(6).SQL = MiSql
  
  '
  MiSql = "Insert Into FA3600 (FA04CODFACT, FA36NUMORDEN, FA36DESIG, FA36IMPORTE, FA36NUMPROP, FA36INDFACT) Values (?,?,?,?,?,?)"
  qry(7).SQL = MiSql
'
  MiSql = "Update FA3600 Set FA36INDFACT = 1 Where FA04CODFACT = ? AND FA36DESIG = ?"
  qry(8).SQL = MiSql
'
  'Asignamos al FA04NUMFACTREAL de la antigua el n�mero de factura creado
'  MiSql = "Update FA0400 Set FA04NUMFACREAL = ? Where FA04NUMFACT = ?"
'  qry(9).SQL = MiSql
  
  'Activamos las querys.
  For x = 1 To 8
    Set qry(x).ActiveConnection = objApp.rdoConnect
    qry(x).Prepared = True
  Next
End Sub



Sub IniciarQRYFact()
Dim MiSql As String
Dim x As Integer
  
  'Seleccionar la descripci�n de una categor�a.
  MiSql = "Select FA05DESIG from FA0500 Where FA05CODCATEG = ?"
  qryFact(3).SQL = MiSql
  
  'Insertar una cabecera de factura en FA0400
  MiSql = "Insert Into FA0400 (FA04CODFACT,FA04NUMFACT,CI21CODPERSONA,CI13CODENTIDAD," & _
              "FA04DESCRIP,FA04CANTFACT,FA04DESCUENTO,AD01CODASISTENCI,FA04OBSERV," & _
              "FA09CODNODOCONC,AD07CODPROCESO,CI32CODTIPECON,FA04FECFACTURA, FA04INDNMGC, FA04FECDESDE, FA04FECHASTA) " & _
              "Values (?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'))"
  qryFact(4).SQL = MiSql
  
  'Insertar una l�nea de factura en FA1600
  MiSql = "Insert into FA1600 (FA04CODFACT,FA16NUMLINEA,FA14CODNODOCONC,FA16DESCRIP," & _
          "FA16PRCTGRNF,FA16PRECIO,FA16DCTO,FA16IMPORTE,FA16ORDFACT,FA16CANTSINFRAN,FA16FRANQUICIA" & _
          ") Values (?,?,?,?,?,?,?,?,?,?,?)"
  qryFact(5).SQL = MiSql
  
  'Asignar a FA0300 el codigo de la factura y el n�mero de l�nea
  MiSql = "Update FA0300 " & _
                " set FA04NUMFACT = ?, FA16NUMLINEA = ?, FA03INDDESGLOSE = ?, FA15CODATRIB = ?, " & _
                " FA03PRECIOFACT = ?, FA03CANTFACT = ?, FA03OBSIMP = ? " & _
                " Where FA03NUMRNF = ?"
  qryFact(6).SQL = MiSql
  
  '
  MiSql = "Insert Into FA3600 (FA04CODFACT, FA36NUMORDEN, FA36DESIG, FA36IMPORTE, FA36NUMPROP, FA36INDFACT) Values (?,?,?,?,?,?)"
  qryFact(7).SQL = MiSql
'
  MiSql = "Update FA3600 Set FA36INDFACT = 1 Where FA04CODFACT = ? AND FA36NUMORDEN = ?"
  qryFact(8).SQL = MiSql
'
  'Asignamos al FA04NUMFACTREAL de la antigua el n�mero de factura creado
  MiSql = "Update FA0400 Set FA04NUMFACREAL = ? Where FA04NUMFACT = ?"
  qryFact(9).SQL = MiSql
  
  'Activamos las querys.
  For x = 1 To 9
    Set qryFact(x).ActiveConnection = objApp.rdoConnect
    qryFact(x).Prepared = True
  Next
End Sub

Sub pFacturacion(Optional blnPermitirModif As Boolean = True, Optional CI21CODPERSONA As Variant = "")
   blnPermitirModifFact = blnPermitirModif
   
'   Load frm_Seleccion
'   frm_Seleccion.IdPersona2.Text = CI21CODPERSONA
   If Not IsMissing(CI21CODPERSONA) Then
      Call objPipe.PipeSet("CI21CODPERSONA", CI21CODPERSONA)
   End If
   
   Call frm_Seleccion.Show(vbModal)
   Set frm_Seleccion = Nothing
   
'   Dim view As New frm_Seleccion
'   'Load view
'   'view.IdPersona1.Text = CI21CODPERSONA
'   view.Show vbModal
'   Set view = Nothing
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' Function VentanaAtributos(aAtributos() As CamposAtrib) As Boolean
'
' Esta funci�n llama a la ventana de petici�n de nuevos atributos
' y nos devolvera un array con los campos y valores a modificar
' el formato del array es una linea por cada atributo que es de un tipo definido
' por el usuario con los siguientes propiedades :
'       .Campo : nombre del campo de la BD a modificar (p.e. FA15FECINICIO)
'       .Actualizar : si se ha seleccionado este campo como modificable
'       .Valor : es el valor a insertar en la BD
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function VentanaAtributos(aAtributos() As CamposAtrib, Nombre As String) As Boolean
    Dim i As Integer
    With frm_Atributos
        ' mostrar la  pantalla de modificacion de atributos
        .Caption = .Caption & Nombre
        .Show vbModal
        
        
        VentanaAtributos = .blnAceptar
        
        ' recoger los valores de los atributos
        If .blnAceptar Then
            '**************************************************
            aAtributos(FA15PRECIOREF).campo = "FA15PRECIOREF"
            aAtributos(FA15PRECIOREF).Actualizar = .cat(0).Value
            aAtributos(FA15PRECIOREF).Valor = .at(0).Text
            '**************************************************
            aAtributos(FA15PRECIODIA).campo = "FA15PRECIODIA"
            aAtributos(FA15PRECIODIA).Actualizar = .cat(1).Value
            aAtributos(FA15PRECIODIA).Valor = .at(1).Text
            '**************************************************
            aAtributos(FA15DESCUENTO).campo = "FA15DESCUENTO"
            aAtributos(FA15DESCUENTO).Actualizar = .cat(2).Value
            aAtributos(FA15DESCUENTO).Valor = .at(2).Text
            '**************************************************
            aAtributos(FA15PERIODOGARA).campo = "FA15PERIODOGARA"
            aAtributos(FA15PERIODOGARA).Actualizar = .cat(3).Value
            aAtributos(FA15PERIODOGARA).Valor = .at(3).Text
            '**************************************************
            aAtributos(FA15INDNECESARIO).campo = "FA15INDNECESARIO"
            aAtributos(FA15INDNECESARIO).Actualizar = .cac(0).Value
            aAtributos(FA15INDNECESARIO).Valor = .ac(0).Value
            '**************************************************
            aAtributos(FA15INDSUFICIENTE).campo = "FA15INDSUFICIENTE"
            aAtributos(FA15INDSUFICIENTE).Actualizar = .cac(1).Value
            aAtributos(FA15INDSUFICIENTE).Valor = .ac(1).Value
            '**************************************************
            aAtributos(FA15INDEXCLUIDO).campo = "FA15INDEXCLUIDO"
            aAtributos(FA15INDEXCLUIDO).Actualizar = .cac(2).Value
            aAtributos(FA15INDEXCLUIDO).Valor = .ac(2).Value
            '**************************************************
            aAtributos(FA15INDLINOBLIG).campo = "FA15INDLINOBLIG"
            aAtributos(FA15INDLINOBLIG).Actualizar = .cac(4).Value
            aAtributos(FA15INDLINOBLIG).Valor = .ac(4).Value
            '**************************************************
            aAtributos(FA15INDFACTOBLIG).campo = "FA15INDFACTOBLIG"
            aAtributos(FA15INDFACTOBLIG).Actualizar = .cac(5).Value
            aAtributos(FA15INDFACTOBLIG).Valor = .ac(5).Value
            '**************************************************
            aAtributos(FA15INDDESCONT).campo = "FA15INDDESCONT"
            aAtributos(FA15INDDESCONT).Actualizar = .cac(3).Value
            aAtributos(FA15INDDESCONT).Valor = .ac(3).Value
            '**************************************************
            aAtributos(CI13CODENTIDAD).campo = "CI13CODENTIDAD"
            aAtributos(CI13CODENTIDAD).Actualizar = .ccbossEntidad.Value
            If .cbossEntidad.Value = "" Then
                aAtributos(CI13CODENTIDAD).Valor = "''"
            Else
                aAtributos(CI13CODENTIDAD).Valor = "'" & .cbossEntidad.Columns(0).Text & "'"
            End If
            '**************************************************
            aAtributos(FA15Variaci�nPorcentual).campo = "Variaci�nPorcentual"
            aAtributos(FA15Variaci�nPorcentual).Actualizar = .cat(4).Value
            aAtributos(FA15Variaci�nPorcentual).Valor = IIf(.at(4).Text = "", 0, .at(4).Text)
            '**************************************************
            aAtributos(FA15Redondeo).campo = "Redondeo"
            aAtributos(FA15Redondeo).Actualizar = .cat(5).Value
            ' si el campo de redondeo esta vacio se pone por defecto que lo redondee a
            ' la unidad
            aAtributos(FA15Redondeo).Valor = IIf(.at(5).Text = "", 1, .at(5).Text)
            '**************************************************
            aAtributos(FA15INDSUPLEMENTO).campo = "FA15INDSUPLEMENTO"
            aAtributos(FA15INDSUPLEMENTO).Actualizar = .cac(6).Value
            aAtributos(FA15INDSUPLEMENTO).Valor = .ac(6).Value
            '**************************************************
            aAtributos(FA15INDDESGLOSE).campo = "FA15INDDESGLOSE"
            aAtributos(FA15INDDESGLOSE).Actualizar = .cac(7).Value
            aAtributos(FA15INDDESGLOSE).Valor = .ac(7).Value
            '**************************************************
            aAtributos(FA15OrdImp).campo = "FA15OrdImp"
            aAtributos(FA15OrdImp).Actualizar = .cat(6).Value
            aAtributos(FA15OrdImp).Valor = .at(6).Text
            '**************************************************
        End If
    End With
    
    ' descargar la pantalla de atributos
    Unload frm_Atributos
End Function

Public Sub VisionGlobal(CodPersona As String, Optional proceso As String, Optional Asistencia As String)
   Screen.MousePointer = vbHourglass
    
   ReDim arData(1 To 2) As Variant
   arData(1) = CodPersona
   arData(2) = 1
   Call objSecurity.LaunchProcess("HC02", arData())
   
   
   
'    With frm_VisionGlobal
'        Set .Actuaciones1.Connect = objApp.rdoConnect
'
'        .Procesos1.lngHistoria = CLng(Historia)
'        .Procesos1.blnVerSinActuaciones = True
'
''        If proceso <> "" Then
''            .Procesos1.strProceso = proceso
''        End If
''        If Asistencia <> "" Then
''            .Procesos1.strAsistencia = Asistencia
''        End If
'        .Procesos1.Refresh
'
''        Call .Procesos1_Selected(proceso, Asistencia, "", 0, 0, 0)
'
'        Screen.MousePointer = vbDefault
'
'        .Show vbModal
'    End With
    
End Sub

