VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_ListadoCobros 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Listado de Cobros de Caja"
   ClientHeight    =   1530
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3720
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1530
   ScaleWidth      =   3720
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   690
      Left            =   90
      TabIndex        =   3
      Top             =   630
      Visible         =   0   'False
      Width           =   420
      _Version        =   196608
      _ExtentX        =   741
      _ExtentY        =   1217
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   555
      Left            =   810
      TabIndex        =   2
      Top             =   720
      Width           =   2175
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   1755
      TabIndex        =   0
      Tag             =   "Fecha Inicio"
      Top             =   135
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label lblFecInicio 
      Caption         =   "Fecha de Inicio:"
      Height          =   255
      Left            =   180
      TabIndex        =   1
      Top             =   180
      Width           =   1650
   End
End
Attribute VB_Name = "frm_ListadoCobros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type CobrosRealizados
  CodCobro As Long
  FecCobro As Date
  CodPerso As String
  Persona  As String
End Type

Dim Cobros() As CobrosRealizados
Dim Horas() As Integer
Dim Personas() As String

Const ConvX = 54.0809
Const ConvY = 54.6101086

Sub ImprimirListadoCobros()
Dim Cont As Integer
Dim Pagina As Integer
Dim Texto As String
Dim NoCobros As Integer
Dim Tipo As String
Dim Columna As Integer
Dim PersonaAnterior As String
Dim Primera As Boolean
  'Calculamos el n�mero de pagos que vamos a listar
  NoCobros = UBound(Cobros)
  Pagina = 1
  vsPrinter1.Orientation = orLandscape
  vsPrinter1.Preview = False
  vsPrinter1.Action = 3
  vsPrinter1.MarginTop = 20 * ConvX
  vsPrinter1.MarginBottom = 10 * ConvX
  vsPrinter1.FontSize = 8
  vsPrinter1.FontName = "Courier New"
  'vsPrinter1.Paragraph = ""
  vsPrinter1.MarginLeft = 10 * ConvY
  vsPrinter1.CurrentY = 20 * ConvX
  vsPrinter1.FontSize = 9
  vsPrinter1.FontBold = True
  vsPrinter1.Paragraph = Trim(Cobros(1).Persona)
  vsPrinter1.FontSize = 8
  vsPrinter1.FontBold = False
  PersonaAnterior = Cobros(1).Persona
  Columna = 1
  For Cont = 1 To NoCobros
    If PersonaAnterior <> Cobros(Cont).Persona Then
      vsPrinter1.Paragraph = ""
      vsPrinter1.FontSize = 9
      vsPrinter1.FontBold = True
      vsPrinter1.Paragraph = Trim(Cobros(Cont).Persona)
      vsPrinter1.FontSize = 8
      vsPrinter1.FontBold = False
      PersonaAnterior = Cobros(Cont).Persona
    End If
    Texto = Cobros(Cont).CodPerso & " "
    Texto = Texto & Right(Space(8) & Trim(Cobros(Cont).CodCobro), 8) & " "
    Texto = Texto & Right(Space(15) & Format(Cobros(Cont).FecCobro, "DD/MM/YYYY HH:MM"), 15)
    vsPrinter1.Paragraph = Texto
    If vsPrinter1.CurrentY > 170 * 54.6101086 Then
      'Cuando llegamos al final de la p�gina pasamos a la siguiente.
      'Y Volvemos a imprimir la cabecera.
      'vsPrinter1.NewPage
      If Columna = 1 Then
        vsPrinter1.MarginLeft = 150 * ConvY
        vsPrinter1.CurrentY = 20 * ConvX
        vsPrinter1.Paragraph = ""
        vsPrinter1.CurrentY = 20 * ConvX
        Columna = 2
      ElseIf Columna = 2 Then
        vsPrinter1.NewPage
        vsPrinter1.MarginLeft = 10 * ConvY
        vsPrinter1.CurrentY = 20 * ConvX
        vsPrinter1.Paragraph = ""
        vsPrinter1.CurrentY = 20 * ConvX
        Columna = 1
      End If
    End If
  Next
  vsPrinter1.Action = 6

End Sub
Sub ImprimirResumenCobros()
Dim Cont As Integer
Dim Texto As String
Dim NoPersonas As Integer
Dim Tipo As String
Dim Columna As Integer
Dim PersonaAnterior As String
Dim Primera As Boolean
  'Calculamos el n�mero de pagos que vamos a listar
  
  NoPersonas = UBound(Personas, 2)
  
  vsPrinter1.Orientation = orPortrait
  vsPrinter1.Preview = False
  vsPrinter1.Action = 3
  vsPrinter1.MarginTop = 20 * ConvY
  vsPrinter1.MarginBottom = 10 * ConvY
  vsPrinter1.FontName = "Courier New"
  vsPrinter1.FontSize = 12
  vsPrinter1.FontBold = True
  vsPrinter1.FontUnderline = True
  Texto = "LISTADO RESUMIDO DE COBROS DE FECHA: " & Format(Me.SDCFechaInicio, "DD/MM/YYYY")
  vsPrinter1.Paragraph = Texto
  vsPrinter1.FontSize = 10
  vsPrinter1.FontBold = False
  vsPrinter1.FontUnderline = False
  'vsPrinter1.Paragraph = ""
  vsPrinter1.MarginLeft = 20 * ConvY
  'vsPrinter1.CurrentY = 20 * ConvX
  vsPrinter1.Paragraph = ""
  Texto = "+----------------------------------------+-----+-----+-----+-----+-----+-----+ "
  vsPrinter1.Paragraph = Texto
  Texto = "|                                        |08:30|11:00|13:30|16:00|19:00|     |"
  vsPrinter1.Paragraph = Texto
  Texto = "|EMPLEADO                                |11:00|13:30|16:00|19:00|21:00|TOTAL|"
  vsPrinter1.Paragraph = Texto
  Texto = "+----------------------------------------+-----+-----+-----+-----+-----+-----+"
  vsPrinter1.Paragraph = Texto
  vsPrinter1.Paragraph = "| I FASE                                                                     |"
  Texto = "+----------------------------------------+-----+-----+-----+-----+-----+-----+"
  vsPrinter1.Paragraph = Texto
  For Cont = 1 To NoPersonas
    If Personas(2, Cont) = 1 Then
      Texto = "|" & Left(Personas(1, Cont) & Space(40), 40) & "|"
      Texto = Texto & Right(Space(5) & Horas(1, Cont), 5) & "|"
      Texto = Texto & Right(Space(5) & Horas(2, Cont), 5) & "|"
      Texto = Texto & Right(Space(5) & Horas(3, Cont), 5) & "|"
      Texto = Texto & Right(Space(5) & Horas(4, Cont), 5) & "|"
      Texto = Texto & Right(Space(5) & Horas(5, Cont), 5) & "|"
      Texto = Texto & Right(Space(5) & Horas(6, Cont), 5) & "|"
      vsPrinter1.Paragraph = Texto
      Texto = "+----------------------------------------------+-----+-----+-----+-----+-----+ "
      vsPrinter1.Paragraph = Texto
    End If
  Next
  vsPrinter1.Paragraph = "|                                                                             |"
  vsPrinter1.Paragraph = "| IV FASE                                                                   |"
  Texto = "+----------------------------------------------+-----+-----+-----+-----+-----+ "
  vsPrinter1.Paragraph = Texto
  For Cont = 1 To NoPersonas
    If Personas(2, Cont) = 2 Then
      Texto = "|" & Left(Personas(1, Cont) & Space(40), 40) & "|"
      Texto = Texto & Right(Space(5) & Horas(1, Cont), 5) & "|"
      Texto = Texto & Right(Space(5) & Horas(2, Cont), 5) & "|"
      Texto = Texto & Right(Space(5) & Horas(3, Cont), 5) & "|"
      Texto = Texto & Right(Space(5) & Horas(4, Cont), 5) & "|"
      Texto = Texto & Right(Space(5) & Horas(5, Cont), 5) & "|"
      Texto = Texto & Right(Space(5) & Horas(6, Cont), 5) & "|"
      vsPrinter1.Paragraph = Texto
      Texto = "+----------------------------------------------+-----+-----+-----+-----+-----+ "
      vsPrinter1.Paragraph = Texto
    End If
  Next
  vsPrinter1.Action = 6
End Sub


Private Sub cmdImprimir_Click()
Dim MiSql As String
Dim MiRs As rdoResultset
Dim sqlPersona As String
Dim rsPersona As rdoResultset
Dim PersonaAnt As String
Dim Persona As String
Dim Cont As Integer
Dim ContPer As Integer
Dim Hora As String

  PersonaAnt = ""
  Cont = 0
  ContPer = 0
  MiSql = "Select FA17CODPAGO, FA17FECADD, SG02COD_ADD FROM FA1700 " & _
         "WHERE  FA17FECADD > TO_DATE('" & Format(Me.SDCFechaInicio.Date, "DD/MM/YYYY") & " 00:00:00','DD/MM/YYYY HH24:MI:SS') " & _
          "AND FA17FECADD < TO_DATE('" & Format(Me.SDCFechaInicio.Date, "DD/MM/YYYY") & " 23:59:59','DD/MM/YYYY HH24:MI:SS') " & _
          "AND SG02COD_ADD NOT IN ('JDG','OTL') " & _
          "ORDER BY SG02COD_ADD, FA17FECADD"

  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    While Not MiRs.EOF
      Cont = Cont + 1
      If MiRs("SG02COD_ADD") <> PersonaAnt Then
        ContPer = ContPer + 1
        ReDim Preserve Horas(1 To 6, 1 To ContPer)
        'Inicializamos el contador
        Horas(1, ContPer) = 0
        Horas(2, ContPer) = 0
        Horas(3, ContPer) = 0
        Horas(4, ContPer) = 0
        Horas(5, ContPer) = 0
        Horas(6, ContPer) = 0
        ReDim Preserve Personas(1 To 2, 1 To ContPer)
        sqlPersona = "Select SG02NOM, SG02APE1, SG02APE2 FROM SG0200 " & _
                     "Where SG02COD = '" & MiRs("SG02COD_ADD") & "'"
        Set rsPersona = objApp.rdoConnect.OpenResultset(sqlPersona, 3)
        If Not rsPersona.EOF Then
          Persona = UCase(rsPersona("SG02NOM")) & " "
          Persona = Persona & UCase(rsPersona("SG02APE1")) & " "
          Persona = Persona & UCase(rsPersona("SG02APE2")) & " "
          Personas(1, ContPer) = Persona
        Else
          Persona = ""
          Personas(1, ContPer) = ""
        End If
        If MiRs("SG02COD_ADD") = "CMO" Or MiRs("SG02COD_ADD") = "COL" Or MiRs("SG02COD_ADD") = "MSA" Then
          Personas(2, ContPer) = 2
        Else
          Personas(2, ContPer) = 1
        End If
        PersonaAnt = MiRs("SG02COD_ADD")
      End If
      ReDim Preserve Cobros(1 To Cont)
      If Not IsNull(MiRs("FA17CODPAGO")) Then
        Cobros(Cont).CodCobro = MiRs("FA17CODPAGO")
      End If
      If Not IsNull(MiRs("FA17FECADD")) Then
        Cobros(Cont).FecCobro = MiRs("FA17FECADD")
        Hora = CStr(Format(Cobros(Cont).FecCobro, "HH:NN"))
      End If
      If Not IsNull(MiRs("SG02COD_ADD")) Then
        Cobros(Cont).CodPerso = MiRs("SG02COD_ADD")
      End If
      Cobros(Cont).Persona = Persona
      If (Hora >= "08:30" And Hora < "11:00") Then
        Horas(1, ContPer) = Horas(1, ContPer) + 1
      ElseIf (Hora >= "11:00" And Hora < "13:30") Then
        Horas(2, ContPer) = Horas(2, ContPer) + 1
      ElseIf (Hora >= "13:30" And Hora < "16:00") Then
        Horas(3, ContPer) = Horas(3, ContPer) + 1
      ElseIf (Hora >= "16:00" And Hora < "19:00") Then
        Horas(4, ContPer) = Horas(4, ContPer) + 1
      ElseIf (Hora >= "19:00" And Hora <= "21:00") Then
        Horas(5, ContPer) = Horas(5, ContPer) + 1
      End If
      Horas(6, ContPer) = Horas(6, ContPer) + 1
      MiRs.MoveNext
    Wend
  End If
  'Call ImprimirListadoCobros
  Call Me.ImprimirResumenCobros
End Sub
Private Sub Form_Load()
Dim MiRs As rdoResultset

  Set MiRs = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not MiRs.EOF Then
    Me.SDCFechaInicio.Date = Format(DateAdd("d", -1, MiRs(0)), "dd/mm/yyyy")
  End If
End Sub


