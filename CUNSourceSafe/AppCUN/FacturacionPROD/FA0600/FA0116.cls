VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Persona"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private strCodigo As String ' Es el CI21CODPERSONA


' El tipo de persona depende de la CI2100 (si es persona fisica o juridica)
Public TipoPersona As Integer

Public Key As String
' Codigo del tipo economico del REco (depende de cada asistencia)
Public CodTipEcon As String
Public EntidadResponsable As String
Public DesEntidad As String
Public Coletilla As String
Public Dto As Single
Public Observaciones As String


' Codigo del concierto a aplicar (depende de cada asistencia)
Public Concierto As New CodigosConcierto
'Public Conciertos As New Collection

' el reco puede se valido en diferentes tramos de fechas
Public TramosFechas As New Collection

' proc-asist al que corresponde el REco
Public proceso As String
Public Asistencia As String

' Importe de la parte de la propuesta asignada a este REco
Public ImporteFact As Double
Public NFactura As String
Public CodFactura As Long

' Datos de la persona o entidad
Public NumHistoria As String
Public Name As String
Public Direccion As String
Public CP As String
Public Poblacion As String
Public Provincia As String
Public ProvCorta As String
Public Tratamiento As String
Public DNI As String
Public Pais As String
Public FecNacimiento As String
Public Atencion As String
Public NumSegSoc As String


' Se utiliza solo para contener los nodos de concierto
' aplicables que se encuentren despues de hacer una primera
' asignacion de RNFs. a partir de esta colecci�n se realizan
' las diferentes combinaciones de conciertos que se puedan
' aplicar simultaneamente.
Public NodosAplicables As New Collection


' CODIGO ( CI21CODPERSONA )
Public Property Let Codigo(ByVal CI21CODPERSONA As String)
    strCodigo = CI21CODPERSONA
    
    If CI21CODPERSONA <> "" Then
      ' recoger los datos del REco
      RecogerDatos
    End If
End Property
Public Property Get Codigo() As String
    Codigo = strCodigo
End Property

Public Property Get CPPoblac() As String
    CPPoblac = CP & " " & Poblacion & "     " & Provincia
End Property

'*****************************************************************************************
'  rutina que a partir del CI21CODPERSONA busca el resto de sus datos
'*****************************************************************************************
Private Sub RecogerDatos()
   Dim SQL As String
   Dim RS As rdoResultset
   
   On Error GoTo error
   
   ' valores por defecto
   TipoPersona = 1
   Name = ""
   
   ' Recoger el tipo de persona
   SQL = "select CI33CODTIPPERS from ci2100 where ci21codpersona = " & strCodigo
   Set RS = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly)
   If Not RS.EOF Then
       TipoPersona = RS("CI33CODTIPPERS")
   End If
   
   ' Recoger su nombre o descripci�n seg�n su tipo
   If TipoPersona = 1 Then
      '*************************************************
      '           PERSONA FISICA
      '*************************************************
      SQL = " Select CI22NOMBRE||' '||CI22PRIAPEL||' '||CI22SEGAPEL As NOMBRE, CI22NUMDIRPRINC,  " & _
            " CI10CALLE, CI10PORTAL, CI10RESTODIREC, CI07CODPOSTAL, CI10DESLOCALID,  " & _
            " CI26DESPROVI, CI26DESCORTA, CI34DESTRATAMI, CI2200.CI22NUMHISTORIA, CI22DNI, CI30CODSEXO, " & _
            " CI1000.CI19CODPAIS, DECODE(CI1000.CI19CODPAIS,45,'',CI19DESPAIS) DESPAIS, CI22FECNACIM, CI22NUMSEGSOC " & _
            " from CI2200, CI1000, CI3400, CI2600, CI1900 " & _
            " where CI2200.CI21CODPERSONA = " & strCodigo & _
            " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA(+)  " & _
            " AND (CI1000.CI10INDDIRPRINC = -1 OR CI1000.CI10INDDIRPRINC is null))" & _
            " AND (CI2200.CI34CODTRATAMI = CI3400.CI34CODTRATAMI(+))" & _
            " AND (CI1000.CI26CODPROVI = CI2600.CI26CODPROVI(+))" & _
            " AND (CI1000.CI19CODPAIS = CI1900.CI19CODPAIS (+))"
      ' Abrir el Resulset
      Set RS = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly)
      
      ' Cargar sus datos
      If Not RS.EOF Then
         Name = "" & RS("Nombre")
         
         CP = "" & RS("CI07CODPOSTAL")
         Poblacion = "" & RS("CI10DESLOCALID")
         Provincia = "" & RS("CI26DESPROVI")
         ProvCorta = "" & RS("CI26DESCORTA")
         
         Direccion = "" & RS("CI10CALLE") & " " & RS("CI10PORTAL") & " " & RS("CI10RESTODIREC")
         Pais = "" & RS("DesPais")
         
         DNI = "" & RS("CI22DNI")
         
         FecNacimiento = "" & RS("CI22FECNACIM")
         
         If Not IsNull(RS("CI34DESTRATAMI")) Then
             Tratamiento = "" & RS("CI34DESTRATAMI")
         Else
            ' si no tiene tratamiento ponerlo por defecto seg�n el sexo
            If RS("CI30CODSEXO") = 1 Then
               Tratamiento = "Sr. D."
            Else
               Tratamiento = "Sra. D�a."
            End If
         End If
         
         NumHistoria = "" & RS("CI22NUMHISTORIA")
         NumSegSoc = "" & RS("CI22NUMSEGSOC")
          
      End If
      
      Atencion = ""
   
   Else
      '*************************************************
      '           PERSONA JURIDICA
      '*************************************************
      SQL = " Select CI23RAZONSOCIAL, CI23NUMDIRPRINC, CI10CALLE, CI10PORTAL," & _
            " CI10RESTODIREC, CI07CODPOSTAL, CI10DESLOCALID, CI26DESPROVI, CI26DESCORTA,  " & _
            " CI1000.CI19CODPAIS, DECODE(CI1000.CI19CODPAIS,45,'',CI19DESPAIS) DESPAIS, CI23CIF, CI23ATENCION " & _
            " From CI2300, CI1000, CI0900, CI2600, CI1900 " & _
            " Where CI2300.CI21CODPERSONA = " & strCodigo & _
            " And (CI2300.CI21CODPERSONA = CI1000.CI21CODPERSONA (+) " & _
            " And (CI1000.CI10INDDIRPRINC = -1 OR CI1000.CI10INDDIRPRINC is null)) " & _
            " AND (CI1000.CI26CODPROVI = CI2600.CI26CODPROVI(+))" & _
            " AND (CI1000.CI19CODPAIS = CI1900.CI19CODPAIS (+))"
   
      Set RS = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly)
      If Not RS.EOF Then
         Name = "" & RS("CI23RAZONSOCIAL")
         
         CP = "" & RS("CI07CODPOSTAL")
         Poblacion = "" & RS("CI10DESLOCALID")
         Provincia = "" & RS("CI26DESPROVI")
         ProvCorta = "" & RS("CI26DESCORTA")
   
         Direccion = "" & RS("CI10CALLE") & " " & RS("CI10PORTAL") & " " & RS("CI10RESTODIREC")
         Pais = "" & RS("DesPais")
   
         DNI = "" & RS("CI23CIF")
         
         FecNacimiento = ""
         
         Tratamiento = ""
   
         NumHistoria = ""
         NumSegSoc = ""
         
         Atencion = "" & RS("CI23ATENCION")
      End If
       
   End If
   Exit Sub
   
error:
End Sub

