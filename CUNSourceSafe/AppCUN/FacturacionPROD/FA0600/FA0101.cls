VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' **********************************************************************************

' Las ventanas: numeraci�n a partir del DTM100
Const FAWINAsignarVolantesOsasunbidea As String = "FA0169"
Const FAWINFacturaInsalud             As String = "FA0302"
Const FAWINFacturasOsasunbidea        As String = "FA0303"


' Los listados: numeraci�n a partir del DT0101
Const FARepFA0201          As String = "FA0201"


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objCW = mobjCW
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub

' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

'  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
 
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case FAWINAsignarVolantesOsasunbidea
        frm_DatosOsasunbidea.Show vbModal
        Set frm_DatosOsasunbidea = Nothing
    Case FAWINFacturaInsalud
        frmFacturaINSALUD.Show vbModal
        Set frmFacturaINSALUD = Nothing
    Case FAWINFacturasOsasunbidea
        frmFacturaOsasunbidea.Show vbModal
        Set frmFacturaOsasunbidea = Nothing
    Case Else
        ' el c�digo de proceso no es v�lido y se devuelve falso
        LaunchProcess = False
  End Select
  
'  Call Err.Clear
End Function

Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz al n� de procesos que se definan
  ReDim aProcess(1 To 1, 1 To 4) As Variant
 
  'aProcess(1, 1) = DTWinMantenimientos
  'aProcess(1, 2) = "Mantenimientos del Servicio de Dietas"
  'aProcess(1, 3) = True
  'aProcess(1, 4) = cwTypeWindow
  
  aProcess(1, 1) = FARepFA0201
  aProcess(1, 2) = "Listado de Conceptos Agrupantes"
  aProcess(1, 3) = False
  aProcess(1, 4) = cwTypeReport
  
  
End Sub

