Attribute VB_Name = "ModAnulacionFact"
Option Explicit

Dim qry(1 To 20) As New rdoQuery
'Qrys para la factura negativa.
Dim QryNeg(1 To 10) As New rdoQuery
'El c�digo de la nueva factura lo utilizamos como variable global para cabecera, l�neas y RNFs.
Dim CodFact As Long

Sub AnularCompensaciones(CodFactura As Long)

  'Eliminamos las compensaciones que ten�a esa factura
  QryNeg(9).rdoParameters(0) = CodFactura
  QryNeg(9).Execute
  
End Sub

'************************************************************************************
'   AnularFact :
'
'   Parametros :
'        - un array de numeros de factura, p.e. ("13/1600001","13/1600002",...)
'        - un CI22NUMHISTORIA (opcional) que servir� para que cuando se da el caso
'          de una factura que se corresponde con varios pacientes (p.e. Osasunbidea, Insalud, etc)
'          se pueda anular la parte relacionada con ese CI22NUMHISTORIA y no la totalidad, de cara a poder
'          modificar los jass, etc.
'   Esta funci�n recoge los numeros de factura y se encarga de anularlas
'************************************************************************************
Public Sub AnularFact(nFact() As String, Optional CI22NUMHISTORIA As String = "")

    On Error GoTo errorFact
    If UBound(nFact) > 0 Then
        ' pedir conformidad para la anulaci�n de facturas
        If MsgBox("� Est� seguro de anular " & IIf(UBound(nFact) > 1, "estas facturas", "esta factura") & " ?", vbQuestion + vbYesNo + vbDefaultButton2, "Confirmar la anulaci�n") = vbYes Then
            ' inicializar las queries
            Call IniciarQRY
            ' Una vez comprobado lo anterior llenamos los tipos definidos por el usario correspondientes a las facturas
            Call AnularFacturas(nFact, CI22NUMHISTORIA)
        End If
    End If
Exit Sub
errorFact:
    MsgBox "Se ha producido un error " & Err.Number & " en la anulaci�n de facturas", vbCritical + vbOKOnly, "Facturaci�n"
'    Resume
End Sub


Sub AnularFacturas(nFact() As String, Optional CI22NUMHISTORIA As String = "")

    Dim Cont As Integer
    Dim MiRs As rdoResultset
    Dim rsCodigo As rdoResultset
    Dim NumFact As String
    

    On Error GoTo ErrorEnAnulacion
    
    For Cont = 1 To UBound(nFact)
        NumFact = nFact(Cont)
        ' realizar el borrado de cada factura en una transacion
        objApp.rdoConnect.BeginTrans
        
        If NumFact <> "" Then
            '2 -> numfact
            'Comprobaremos que la factura no se ha contabilizado.
            qry(2).rdoParameters(0) = NumFact
            Set MiRs = qry(2).OpenResultset
            If Not IsNull(MiRs(0)) Then
               'La factura est� contabilizada hay que generar una negativa
               ' comprobar si la factura contiene varios pacientes
               If FactMultiple(NumFact) Then
                  If MsgBox("La factura : " & NumFact & " corresponde a varios pacientes," & Chr(10) & _
                            "y est� contabilizada." & Chr(10) & Chr(10) & _
                            "� Desea Generar una factura negativa por la totalidad ?", vbYesNo + vbCritical + vbDefaultButton2, _
                            "Anulaci�n de Facturas") = vbYes Then
                     Call GenerarFactNegativa(NumFact)
                  End If
               Else
                  If MsgBox("La factura : " & NumFact & " est� contabilizada." & Chr(10) & Chr(10) & _
                            "� Desea Generar una factura negativa ?", vbYesNo + vbCritical + vbDefaultButton2, _
                            "Anulaci�n de Facturas") = vbYes Then
                     Call GenerarFactNegativa(NumFact)
                  End If
'                  Call GenerarFactNegativa(NumFact)
               End If

            Else
               'La factura est� sin contabilizar la anulamos sin m�s.

               ' comprobar si la factura contiene varios pacientes
               If FactMultiple(NumFact) Then
                  If MsgBox("La factura : " & NumFact & " corresponde a varios pacientes," & Chr(10) & Chr(10) & _
                            "� Desea borrarla en su totalidad ?", vbYesNo + vbCritical + vbDefaultButton2, _
                            "Anulaci�n de Facturas") = vbYes Then
                     Call BorrarFact(NumFact)
                  Else
                     If CI22NUMHISTORIA <> "" Then
                        If MsgBox("La factura : " & NumFact & " corresponde a varios pacientes," & Chr(10) & Chr(10) & _
                                  "� Desea borrar la parte correspondiente a la Historia " & CI22NUMHISTORIA & " ?", _
                                  vbYesNo + vbCritical + vbDefaultButton2, _
                                  "Anulaci�n de Facturas") = vbYes Then
                           Call BorrarFact(NumFact, CI22NUMHISTORIA)
                        End If
                     End If
                  End If
               Else
                  Call BorrarFact(NumFact)
               End If
            End If
        End If
        objApp.rdoConnect.CommitTrans
ProximaFact:
    Next
  
Exit Sub

ErrorEnAnulacion:
    objApp.rdoConnect.RollbackTrans
    MsgBox "Se ha producido un error en la anulaci�n de la factura : " & NumFact & ", Revisela cuando acabe el proceso", vbOKOnly + vbCritical, "Atenci�n"
    Resume Next
    Resume ProximaFact
End Sub

Function FactMultiple(NumFact As String) As Boolean
   '********************************************************************
   ' esta funcion devuelve :
   '     True  : si la factura corresponde a varios pacientes (historias)
   '     False : si solo contiene un paciente
   '********************************************************************
   Dim MiRs As rdoResultset
   
   qry(13).rdoParameters(0) = NumFact
   Set MiRs = qry(13).OpenResultset

   If MiRs(0) > 1 Then
      FactMultiple = True
   Else
      FactMultiple = False
   End If
End Function

Sub BorrarFact(NumFact As String, Optional CI22NUMHISTORIA As String = "")
   Dim rsCodigo As rdoResultset
   
   If CI22NUMHISTORIA = "" Then
   ' no se ha pasado ningun numero de historia, entonces se borra (logicamente) la factura completa
   
      ' Seleccionaremos todos los c�digos de factura que tienen ese n� de factura
      ' para borrar las lineas, los rnfs y las compensaciones
      qry(10).rdoParameters(0) = NumFact 'N� de la factura
      Set rsCodigo = qry(10).OpenResultset
      
   Else
   ' Se borra solo la parte de la factura correspondiente a la historia pasada como parametro
   
      ' Seleccionaremos todos los c�digos de factura que tienen ese n� de factura y ese NUMERO DE HISTORIA
      ' para borrar las lineas, los rnfs y las compensaciones
      qry(14).rdoParameters(0) = NumFact           'N� de la factura
      qry(14).rdoParameters(1) = CI22NUMHISTORIA   'N� de historia
      Set rsCodigo = qry(14).OpenResultset
   End If
   
   If Not rsCodigo.EOF Then
      While Not rsCodigo.EOF
         'Asignaremos el 0 al N�mero de factura real para indicarle que ha sido anulada.
         qry(3).rdoParameters(0) = 0
         qry(3).rdoParameters(1) = rsCodigo("FA04CODFACT") 'C�digo de la factura
         qry(3).Execute
         
         'Le indicaremos que la factura est� compensada para que no apareza en los recordatorios.
         qry(9).rdoParameters(0) = rsCodigo("FA04CODFACT") 'C�digo de la factura
         qry(9).Execute
         
         'Borraremos de la FA0300 los rnfs relacionados con esa factura
         qry(5).rdoParameters(0) = rsCodigo("FA04CODFACT") 'C�digo de la factura
         qry(5).Execute
         
         'marcaremos los permanentes como no facturados
         qry(6).rdoParameters(0) = rsCodigo("FA04CODFACT") 'C�digo de la factura
         qry(6).Execute
         
         'Borraremos de la FA1800 las posibles compensaciones que se refieran a esa factura
         qry(8).rdoParameters(0) = rsCodigo("FA04CODFACT") 'Codigo de la factura
         qry(8).Execute
         
         'Borraremos las posibles compensaciones que pueda tener como factura positiva.
         qry(15).rdoParameters(0) = rsCodigo("FA04CODFACT")
         qry(15).Execute
         
         'Borraremos las posibles compensaciones que pueda tener como factura negativa.
         qry(16).rdoParameters(0) = rsCodigo("FA04CODFACT")
         qry(16).Execute
         
         'Borraremos de la FA1600 las lineas relacionadas con esa factura
         qry(12).rdoParameters(0) = rsCodigo("FA04CODFACT")
         qry(12).Execute
         rsCodigo.MoveNext
       Wend
   End If
   
End Sub

Public Sub GenerarFactNegativa(nFact As String)
 Dim rsFactura As rdoResultset
Dim rsLineas As rdoResultset
Dim rsRNFs As rdoResultset
Dim CodFact As Long
Dim Importe As Double
Dim NumFactura As String

  Call IniciarQRYNeg
  'Ccogemos el n�mero de factura que va a llevar la negativa
  NumFactura = "13/" & fNextClave("FA04NUMFACT", "FA0400")
  QryNeg(1).rdoParameters(0) = nFact
  Set rsFactura = QryNeg(1).OpenResultset(rdOpenStatic)
  If Not rsFactura.EOF Then
    rsFactura.MoveLast
    rsFactura.MoveFirst
    While Not rsFactura.EOF
      If Not IsNull(rsFactura("FA04CODFACT")) Then
        CodFact = rsFactura("FA04CODFACT")
      Else
        CodFact = 0
      End If
      If CodFact <> 0 Then
        Call GenerarCabeceraNegativa(CodFact, rsFactura, NumFactura)
        Call GenerarLineasNegativas(CodFact)
        Call GenerarRNFsNegativos(CodFact)
        Call AnularCompensaciones(CodFact)
      End If
      rsFactura.MoveNext
    Wend
  End If
  
End Sub


Sub GenerarCabeceraNegativa(CodFactura As Long, Datos As rdoResultset, NumeroFact As String)
Dim MiRsFecha As rdoResultset
  
    'C�digo de la Factura (Nuevo)
    CodFact = fNextClave("FA04CODFACT", "FA0400")
    QryNeg(4).rdoParameters(0) = CodFact
    'N�mero de Factura (Nuevo)
    QryNeg(4).rdoParameters(1) = NumeroFact
    'Codigo de la persona Responsable
    If Not IsNull(Datos("CI21CODPERSONA")) Then
      QryNeg(4).rdoParameters(2) = Datos("CI21CODPERSONA")
    Else
      QryNeg(4).rdoParameters(2) = Null
    End If
    'C�digo de la entidad Responsable
    If Not IsNull(Datos("CI13CODENTIDAD")) Then
      QryNeg(4).rdoParameters(3) = Datos("CI13CODENTIDAD")
    Else
      QryNeg(4).rdoParameters(3) = Null
    End If
    'Descripci�n de la factura
    If Not IsNull(Datos("FA04DESCRIP")) Then
      QryNeg(4).rdoParameters(4) = Datos("FA04DESCRIP")
    Else
      QryNeg(4).rdoParameters(4) = Null
    End If
    'Cantidad Facturada (Debe ser negativa � 0 si hay varias asistencias)
    If Not IsNull(Datos("FA04CANTFACT")) Then
      QryNeg(4).rdoParameters(5) = 0 - CDbl(Datos("FA04CANTFACT"))
    Else
      QryNeg(4).rdoParameters(5) = 0
    End If
    'Descuento efectuado en factura
    If Not IsNull(Datos("FA04DESCUENTO")) Then
      QryNeg(4).rdoParameters(6) = Datos("FA04DESCUENTO")
    Else
      QryNeg(4).rdoParameters(6) = Null
    End If
    'Fecha de la factura (cogerla del sistema)
    Set MiRsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
    If Not MiRsFecha.EOF Then
      QryNeg(4).rdoParameters(7) = Format(MiRsFecha(0), "dd/mm/yyyy")
    Else
      QryNeg(4).rdoParameters(7) = Null
     End If
    'C�digo de la asistencia.
    If Not IsNull(Datos("AD01CODASISTENCI")) Then
      QryNeg(4).rdoParameters(8) = Datos("AD01CODASISTENCI")
    Else
      QryNeg(4).rdoParameters(8) = Null
    End If
    'Observaciones
    If Not IsNull(Datos("FA04OBSERV")) Then
      QryNeg(4).rdoParameters(9) = Datos("FA04OBSERV")
    Else
      QryNeg(4).rdoParameters(9) = Null
    End If
    'C�digo del nodo de concierto.
    If Not IsNull(Datos("FA09CODNODOCONC")) Then
      QryNeg(4).rdoParameters(10) = Datos("FA09CODNODOCONC")
    Else
      QryNeg(4).rdoParameters(10) = Null
    End If
    'Indicador de si la factura est�c compensada o no.
'    If Not IsNull(Datos("FA04INDCOMPENSA")) Then
      QryNeg(4).rdoParameters(11) = 2
'    Else
'      QryNeg(4).rdoParameters(11) = Null
'    End If
    'Indicador de recordatorio.
    If Not IsNull(Datos("FA04INDRECORDAR")) Then
      QryNeg(4).rdoParameters(12) = Datos("FA04INDRECORDAR")
    Else
      QryNeg(4).rdoParameters(12) = Null
    End If
    'C�digo del Proceso.
    If Not IsNull(Datos("AD07CODPROCESO")) Then
      QryNeg(4).rdoParameters(13) = Datos("AD07CODPROCESO")
    Else
      QryNeg(4).rdoParameters(13) = Null
    End If
    'N�mero de factura real (Nulo).
    QryNeg(4).rdoParameters(14) = Null
    'Fecha de contabilizaci�n (Nulo).
    QryNeg(4).rdoParameters(15) = Null
    'C�digo de tipo econ�mico de la entidad responsable (si la hubiere)
    If Not IsNull(Datos("CI32CODTIPECON")) Then
      QryNeg(4).rdoParameters(16) = Datos("CI32CODTIPECON")
    Else
      QryNeg(4).rdoParameters(16) = Null
    End If
    'Indicador de no mostrar en Gesti�n de cobros.
    If Not IsNull(Datos("FA04INDNMGC")) Then
      QryNeg(4).rdoParameters(17) = Datos("FA04INDNMGC")
    Else
      QryNeg(4).rdoParameters(17) = Null
    End If
    'Fecha desde
    If Not IsNull(Datos("FA04FECDESDE")) Then
      QryNeg(4).rdoParameters(18) = Format(Datos("FA04FECDESDE"), "dd/mm/yyyy")
    Else
      QryNeg(4).rdoParameters(18) = ""
    End If
    'FechaHasta
    If Not IsNull(Datos("FA04FECHASTA")) Then
      QryNeg(4).rdoParameters(19) = Format(Datos("FA04FECHASTA"), "dd/mm/yyyy")
    Else
      QryNeg(4).rdoParameters(19) = ""
    End If
    QryNeg(4).Execute
    
    ' marcar la factura como compensada
    QryNeg(8).rdoParameters(0) = CodFactura
    QryNeg(8).Execute
    
    'Generar la compensaci�n de la factura anterior con la negativa.
    QryNeg(10).rdoParameters(0) = CodFactura
    QryNeg(10).rdoParameters(1) = CodFact
    QryNeg(10).rdoParameters(2) = Format(MiRsFecha(0), "dd/mm/yyyy")
    QryNeg(10).rdoParameters(3) = CDbl(Datos("FA04CANTFACT"))
    QryNeg(10).Execute
    
End Sub

Sub GenerarLineasNegativas(CodFactura As Long)
Dim rsLineas As rdoResultset

  QryNeg(2).rdoParameters(0) = CodFactura
  Set rsLineas = QryNeg(2).OpenResultset(rdOpenStatic)
  If Not rsLineas.EOF Then
    rsLineas.MoveLast
    rsLineas.MoveFirst
    While Not rsLineas.EOF
      'C�digo de factura
      QryNeg(5).rdoParameters(0) = CodFact
      'N�mero de l�nea
      If Not IsNull(rsLineas("FA16NUMLINEA")) Then
        QryNeg(5).rdoParameters(1) = rsLineas("FA16NUMLINEA")
      Else
        QryNeg(5).rdoParameters(1) = Null
      End If
      'C�digo de nodo de concierto
      If Not IsNull(rsLineas("FA14CODNODOCONC")) Then
        QryNeg(5).rdoParameters(2) = rsLineas("FA14CODNODOCONC")
      Else
        QryNeg(5).rdoParameters(2) = Null
      End If
      'Descripci�n de la l�nea
      If Not IsNull(rsLineas("FA16DESCRIP")) Then
        QryNeg(5).rdoParameters(3) = rsLineas("FA16DESCRIP")
      Else
        QryNeg(5).rdoParameters(3) = Null
      End If
      '????????????
      If Not IsNull(rsLineas("FA16PRCTGRNF")) Then
        QryNeg(5).rdoParameters(4) = rsLineas("FA16PRCTGRNF")
      Else
        QryNeg(5).rdoParameters(4) = Null
      End If
      'Precio de la linea (debe ser negativo � 0)
      If Not IsNull(rsLineas("FA16PRECIO")) Then
        QryNeg(5).rdoParameters(5) = 0 - CDbl(rsLineas("FA16PRECIO"))
      Else
        QryNeg(5).rdoParameters(5) = Null
      End If
      'Descuento de la l�nea
      If Not IsNull(rsLineas("FA16DCTO")) Then
        QryNeg(5).rdoParameters(6) = CDbl(rsLineas("FA16DCTO"))
      Else
        QryNeg(5).rdoParameters(6) = Null
      End If
      'Importe de la l�nea(debe ser negativo � 0)
      If Not IsNull(rsLineas("FA16IMPORTE")) Then
        QryNeg(5).rdoParameters(7) = NumToSql(0 - CDbl(rsLineas("FA16IMPORTE")))
      Else
        QryNeg(5).rdoParameters(7) = Null
      End If
      'Orden en la factura
      If Not IsNull(rsLineas("FA16ORDFACT")) Then
        QryNeg(5).rdoParameters(8) = rsLineas("FA16ORDFACT")
      Else
        QryNeg(5).rdoParameters(8) = Null
      End If
      QryNeg(5).Execute
      rsLineas.MoveNext
    Wend
  End If
End Sub

Sub GenerarRNFsNegativos(CodFactura As Long)
Dim rsRNFs As rdoResultset
Dim CodRNF As Long
  
  QryNeg(3).rdoParameters(0) = CodFactura
  Set rsRNFs = QryNeg(3).OpenResultset(rdOpenKeyset)
   
'  If Not rsRNFs.EOF Then
'    rsRNFs.MoveLast
'    rsRNFs.MoveFirst
    While Not rsRNFs.EOF
    
      'N�mero del rnf(secuencia)
      QryNeg(6).rdoParameters(0) = fNextClave("FA03NUMRNF", "FA0300")
      'C�digo de categor�a
      If Not IsNull(rsRNFs("FA05CODCATEG")) Then
        QryNeg(6).rdoParameters(1) = rsRNFs("FA05CODCATEG")
      Else
        QryNeg(6).rdoParameters(1) = Null
      End If
      'Estado del RNF
      If Not IsNull(rsRNFs("FA13CODESTRNF")) Then
        QryNeg(6).rdoParameters(2) = rsRNFs("FA13CODESTRNF")
      Else
        QryNeg(6).rdoParameters(2) = Null
      End If
      'C�digo de la factura
'      If Not IsNull(rsRNFs("FA04NUMFACT")) Then
        QryNeg(6).rdoParameters(3) = CodFact
'      Else
'        QryNeg(6).rdoParameters(3) = Null
'      End If
      'C�digo de Asistencia
      If Not IsNull(rsRNFs("AD01CODASISTENCI")) Then
        QryNeg(6).rdoParameters(4) = rsRNFs("AD01CODASISTENCI")
      Else
        QryNeg(6).rdoParameters(4) = Null
      End If
      'Cantidad (debe ser negativa � 0)
      If Not IsNull(rsRNFs("FA03CANTIDAD")) Then
        QryNeg(6).rdoParameters(5) = NumToSql(0 - CDbl(rsRNFs("FA03CANTIDAD")))
      Else
        QryNeg(6).rdoParameters(5) = Null
      End If
      '??????????
      If Not IsNull(rsRNFs("FA03NUMRNF_P")) Then
        QryNeg(6).rdoParameters(6) = rsRNFs("FA03NUMRNF_P")
      Else
        QryNeg(6).rdoParameters(6) = Null
      End If
      'Precio del RNF.
      If Not IsNull(rsRNFs("FA03PRECIO")) Then
        QryNeg(6).rdoParameters(7) = NumToSql(rsRNFs("FA03PRECIO"))
      Else
        QryNeg(6).rdoParameters(7) = Null
      End If
      'Indicador de si est� facturado
'      If Not IsNull(rsRNFs("FA03INDFACT")) Then
'        QryNeg(6).rdoParameters(8) = rsRNFs("FA03INDFACT")
'      Else
'        QryNeg(6).rdoParameters(8) = Null
'      End If
'      rsRNFs("FA03INDFACT") = 3
      QryNeg(6).rdoParameters(8) = 4
      
      'Precio en factura
      If Not IsNull(rsRNFs("FA03PRECIOFACT")) Then
        QryNeg(6).rdoParameters(9) = NumToSql(rsRNFs("FA03PRECIOFACT"))
      Else
        QryNeg(6).rdoParameters(9) = Null
      End If
      '0bservaciones
      If Not IsNull(rsRNFs("FA03OBSERV")) Then
        QryNeg(6).rdoParameters(10) = rsRNFs("FA03OBSERV")
      Else
        QryNeg(6).rdoParameters(10) = Null
      End If
      'Franquicia unitaria
      If Not IsNull(rsRNFs("FA03FRANQUNI")) Then
        QryNeg(6).rdoParameters(11) = NumToSql(rsRNFs("FA03FRANQUNI"))
      Else
        QryNeg(6).rdoParameters(11) = Null
      End If
      'Franquicia de suma
      If Not IsNull(rsRNFs("FA03FRANQSUMA")) Then
        QryNeg(6).rdoParameters(12) = NumToSql(rsRNFs("FA03FRANQSUMA"))
      Else
        QryNeg(6).rdoParameters(12) = Null
      End If
      'Descuento
      If Not IsNull(rsRNFs("FA03DESCUENTO")) Then
        QryNeg(6).rdoParameters(13) = NumToSql(rsRNFs("FA03DESCUENTO"))
      Else
        QryNeg(6).rdoParameters(13) = Null
      End If
      'Periodo de Garant�a
      If Not IsNull(rsRNFs("FA03PERIODOGARA")) Then
        QryNeg(6).rdoParameters(14) = NumToSql(rsRNFs("FA03PERIODOGARA"))
      Else
        QryNeg(6).rdoParameters(14) = Null
      End If
      'Precio diario
      If Not IsNull(rsRNFs("FA03PRECIODIA")) Then
        QryNeg(6).rdoParameters(15) = NumToSql(rsRNFs("FA03PRECIODIA"))
      Else
        QryNeg(6).rdoParameters(15) = Null
      End If
      'Fecha del RNF
      If Not IsNull(rsRNFs("FA03FECHA")) Then
        QryNeg(6).rdoParameters(16) = Format(rsRNFs("FA03FECHA"), "dd/mm/yyyy hh:nn:ss")
      Else
        QryNeg(6).rdoParameters(16) = Null
      End If
      'Departamento responsable
      If Not IsNull(rsRNFs("AD02CODDPTO_R")) Then
        QryNeg(6).rdoParameters(17) = rsRNFs("AD02CODDPTO_R")
      Else
        QryNeg(6).rdoParameters(17) = Null
      End If
      'M�dico responsable
      If Not IsNull(rsRNFs("SG02COD_R")) Then
        QryNeg(6).rdoParameters(18) = rsRNFs("SG02COD_R")
      Else
        QryNeg(6).rdoParameters(18) = Null
      End If
      'Departamento solicitante
      If Not IsNull(rsRNFs("AD02CODDPTO_S")) Then
        QryNeg(6).rdoParameters(19) = rsRNFs("AD02CODDPTO_S")
      Else
        QryNeg(6).rdoParameters(19) = Null
      End If
      'Doctor Solicitante
      If Not IsNull(rsRNFs("SG02COD_S")) Then
        QryNeg(6).rdoParameters(20) = rsRNFs("SG02COD_S")
      Else
        QryNeg(6).rdoParameters(20) = Null
      End If
      'N�mero de historia
      If Not IsNull(rsRNFs("CI22NUMHISTORIA")) Then
        QryNeg(6).rdoParameters(21) = rsRNFs("CI22NUMHISTORIA")
      Else
        QryNeg(6).rdoParameters(21) = Null
      End If
      'N�mero de l�nea en el que se encuentra dentro de la factura
      If Not IsNull(rsRNFs("FA16NUMLINEA")) Then
        QryNeg(6).rdoParameters(22) = rsRNFs("FA16NUMLINEA")
      Else
        QryNeg(6).rdoParameters(22) = Null
      End If
      'Indicador de desglose en factura
      If Not IsNull(rsRNFs("FA03INDDESGLOSE")) Then
        QryNeg(6).rdoParameters(23) = rsRNFs("FA03INDDESGLOSE")
      Else
        QryNeg(6).rdoParameters(23) = Null
      End If
      'C�digo de atributo
      If Not IsNull(rsRNFs("FA15CODATRIB")) Then
        QryNeg(6).rdoParameters(24) = rsRNFs("FA15CODATRIB")
      Else
        QryNeg(6).rdoParameters(24) = Null
      End If
      'C�digo de proceso
      If Not IsNull(rsRNFs("AD07CODPROCESO")) Then
        QryNeg(6).rdoParameters(25) = rsRNFs("AD07CODPROCESO")
      Else
        QryNeg(6).rdoParameters(25) = Null
      End If
      'Indicador de si est� realizada la prueba
      If Not IsNull(rsRNFs("FA03INDREALIZADO")) Then
        QryNeg(6).rdoParameters(26) = rsRNFs("FA03INDREALIZADO")
      Else
        QryNeg(6).rdoParameters(26) = Null
      End If
      'Cantidad Facturada
      If Not IsNull(rsRNFs("FA03CANTFACT")) Then
        QryNeg(6).rdoParameters(27) = NumToSql(rsRNFs("FA03CANTFACT"))
      Else
        QryNeg(6).rdoParameters(27) = Null
      End If
      'Observaciones en impresi�n?
      If Not IsNull(rsRNFs("FA03OBSIMP")) Then
        QryNeg(6).rdoParameters(28) = rsRNFs("FA03OBSIMP")
      Else
        QryNeg(6).rdoParameters(28) = Null
      End If
      
      ' modificar el indicativo de facturado en los rnfs originales
'      rsRNFs.Update
      QryNeg(7).rdoParameters(0) = CodFactura
      QryNeg(7).Execute
      
      ' grabar el rnf negativo
      QryNeg(6).Execute
      rsRNFs.MoveNext
    Wend
'  End If
End Sub

Sub IniciarQRY()
    Dim MiSql As String
    Dim X As Integer
  
    'Seleccionar los datos de una factura en base al n� de factura
    MiSql = "Select * from FA0400 Where FA04NUMFACT = ? AND FA04NUMFACREAL IS NULL"
    qry(1).SQL = MiSql
    
    'Comprobaremos que la factura no se ha contabilizado.
    MiSql = "Select FA04FECCONTABIL From FA0400 Where FA04NUMFACT = ?"
    qry(2).SQL = MiSql
    
    'Poner en FA0400 el campo de factura real a 0 si es anulaci�n o el N� si es reconstrucci�n
    MiSql = "Update FA0400 Set FA04NUMFACREAL = ? Where FA04CODFACT = ?"
    qry(3).SQL = MiSql
    
    'Seleccionamos los datos personales de una persona.
'    MiSql = " Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, " & _
'                " CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, " & _
'                " CI07CODPOSTAL, CI10DESLOCALID, CI34DESTRATAMI, CI22NUMHISTORIA, CI22DNI, CI30CODSEXO" & _
'                " from CI2200, CI1000,CI3400 " & _
'                " where CI2200.CI21CODPERSONA = ?" & _
'                " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA(+)  " & _
'                " AND CI1000.CI10INDDIRPRINC = -1)" & _
'                " AND (CI2200.CI34CODTRATAMI = CI3400.CI34CODTRATAMI(+))"
'    qry(4).SQL = MiSql
    
    'Borrar en FA0300 los rnfs asociados con esa factura
    MiSql = "Delete From FA0300 Where FA04NUMFACT = ? AND FA03INDPERMANENTE IS NULL"
    ' AND (FA03INDFACT = 0 OR FA03INDFACT is NULL)"
    qry(5).SQL = MiSql
  
   'Borrar en FA0300 los rnfs asociados con esa factura
   MiSql = "update FA0300 " & _
           "set fa04numfact = null, fa03indfact = 0 " & _
           "Where FA04NUMFACT = ?  AND FA03INDPERMANENTE IS NOT NULL"
           
'   MiSql = "update FA0300 " & _
           "set fa04numfact = null, fa03indfact = 0 , FA03PRECIOFACT = FA03PRECIO, FA03CANTFACT = FA03CANTIDAD " & _
           "Where FA04NUMFACT = ?  AND FA03INDPERMANENTE IS NOT NULL"
   qry(6).SQL = MiSql
  
    
    'Seleccionamos los datos de una Entidad que sea responsable econ�mico
'    MiSql = " Select CI21CODPERSONA From CI0900 Where CI13CODENTIDAD = ?"
'    qry(6).SQL = MiSql
'    MiSql = " Select CI23RAZONSOCIAL, CI23NUMDIRPRINC, CI10CALLE, CI10PORTAL," & _
'                " CI10RESTODIREC,CI07CODPOSTAL,CI10DESLOCALID" & _
'                " From CI2300, CI1000, CI0900 " & _
'                " Where CI2300.CI21CODPERSONA = ?" & _
'                " And (CI2300.CI21CODPERSONA = CI1000.CI21CODERSONA " & _
'                " And CI1000.CI10INDDIRPRINC = -1) " & _
'    qry(7).SQL = MiSql
          
    'Borrar en FA1800 las compensaciones que se corresponden a la factura
    MiSql = "Delete From FA1800 Where FA04CODFACT = ?"
    qry(8).SQL = MiSql
    
    'En la tabla FA0400 indicaremos que la factura est� compensada.
    MiSql = "Update FA0400 Set FA04INDCOMPENSA = 2 Where FA04CODFACT = ?"
    qry(9).SQL = MiSql
    
    'Buscaremos los c�digos de factura que se corresponden con un determinado n� de factura
    MiSql = "Select FA04CODFACT From FA0400 Where FA04NUMFACT = ?"
    qry(10).SQL = MiSql
    
    'Como es del mes y no est� contabilizada borramos los registros de la FA0400 y FA16OO
'    'correspondientes a esa factura.
'    MiSql = "Delete From FA0400 Where FA04NUMFACT = ?"
'    qry(11).SQL = MiSql
    
    MiSql = "Delete From FA1600 Where FA04CODFACT = ?"
    qry(12).SQL = MiSql
    
    MiSql = "SELECT  count(distinct  CI22NUMHISTORIA) " & _
            "From FA0400, AD0100 " & _
            "Where FA04NUMFACREAL Is Null " & _
            "  AND FA04NUMFACT = ? " & _
            "  AND FA0400.AD01CODASISTENCI= AD0100.AD01CODASISTENCI"
            
    qry(13).SQL = MiSql
    
    'Buscaremos los c�digos de factura que se corresponden con un determinado n� de factura y historia
    MiSql = "Select fa04codfact from FA0400, ad0100 " & _
            "Where FA04NUMFACT = ? " & _
            "     AND FA04NUMFACREAL IS NULL " & _
            "     and fa0400.AD01CODASISTENCI = ad0100.AD01CODASISTENCI " & _
            "     and ci22numhistoria = ?"
            
    qry(14).SQL = MiSql
    
    'Borraremos la compensaci�n que pueda tener con otras facturas.
    'La factura es positiva
    MiSql = "Delete from FA5800 Where FA04CODFACT_POS = ?"
    qry(15).SQL = MiSql
    'La factura en negativa
    MiSql = "Delete from FA5800 Where FA04CODFACT_NEG = ?"
    qry(16).SQL = MiSql
    
    'Activamos las querys.
    For X = 1 To 16
      Set qry(X).ActiveConnection = objApp.rdoConnect
      qry(X).Prepared = True
    Next
End Sub



Sub IniciarQRYNeg()
Dim MiSql As String
Dim X
    'Seleccionamos la factura que vamos a generar como negativa
    MiSql = "Select * From FA0400 Where FA04NUMFACT = ? AND FA04NUMFACREAL IS NULL"
    QryNeg(1).SQL = MiSql
    'Seleccionamos las l�neas de dicha factura
    MiSql = "Select * From FA1600 Where FA04CODFACT = ?"
    QryNeg(2).SQL = MiSql
    'Seleccionamos los RNFs de dicha factura
    MiSql = "Select * From FA0300 Where FA04NUMFACT = ?"
    QryNeg(3).SQL = MiSql
    'Insertamos en FA0400 una l�nea nueva que contenga los datos de la factura anterior,
    'pero con cantidad negativa.
    MiSql = "Insert Into FA0400 (FA04CODFACT, FA04NUMFACT, CI21CODPERSONA, CI13CODENTIDAD, " & _
            "FA04DESCRIP, FA04CANTFACT, FA04DESCUENTO, FA04FECFACTURA, AD01CODASISTENCI, FA04OBSERV, " & _
            "FA09CODNODOCONC, FA04INDCOMPENSA, FA04INDRECORDAR, AD07CODPROCESO, FA04NUMFACREAL, " & _
            "FA04FECCONTABIL, CI32CODTIPECON, FA04INDNMGC, FA04FECDESDE, FA04FECHASTA) Values " & _
            "(?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'))"
    QryNeg(4).SQL = MiSql
    'Insertamos en FA1600 las l�neas de la factura con cantidad negativa, asign�ndolos a la nueva fra.
    MiSql = "Insert Into FA1600 (FA04CODFACT, FA16NUMLINEA, FA14CODNODOCONC, FA16DESCRIP, " & _
            "FA16PRCTGRNF, FA16PRECIO, FA16DCTO, FA16IMPORTE, FA16ORDFACT) Values(?,?,?,?,?,?,?,?,?)"
    QryNeg(5).SQL = MiSql
    'Insertamos en FA0300 los RNFs con cantidad negativa y precio positivo, asign�ndolos a la nueva fra.
    MiSql = "Insert Into FA0300(FA03NUMRNF,FA05CODCATEG,FA13CODESTRNF,FA04NUMFACT,AD01CODASISTENCI," & _
            "FA03CANTIDAD,FA03NUMRNF_P,FA03PRECIO,FA03INDFACT,FA03PRECIOFACT,FA03OBSERV,FA03FRANQUNI," & _
            "FA03FRANQSUMA,FA03DESCUENTO,FA03PERIODOGARA,FA03PRECIODIA,FA03FECHA,AD02CODDPTO_R," & _
            "SG02COD_R,AD02CODDPTO_S,SG02COD_S,CI22NUMHISTORIA,FA16NUMLINEA,FA03INDDESGLOSE," & _
            "FA15CODATRIB,AD07CODPROCESO,FA03INDREALIZADO,FA03CANTFACT,FA03OBSIMP) Values " & _
            "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?,?,?,?,?,?,?)"
    QryNeg(6).SQL = MiSql
    
    ' actualizar los rnf de la factura original como compensados
    MiSql = "update FA0300 set fa03indfact = 3 Where FA04NUMFACT = ?"
    QryNeg(7).SQL = MiSql
    
    ' marcar la factura original como compensada
    MiSql = "update FA0400 set fa04indcompensa = 2 Where FA04CODFACT = ?"
    QryNeg(8).SQL = MiSql

    'Borrar en FA1800 las compensaciones que se corresponden a la factura
    MiSql = "Delete From FA1800 Where FA04CODFACT = ?"
    QryNeg(9).SQL = MiSql
  
    'Insertar en FA5800 la compensaci�n de la factura positiva con la negativa.
    MiSql = "Insert Into FA5800(FA04CODFACT_POS, FA04CODFACT_NEG, FA58FECCOMP, FA58IMPCOMP) " & _
            "VALUES (?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?)"
    QryNeg(10).SQL = MiSql
    
    'Activamos las querys.
    For X = 1 To 10
      Set QryNeg(X).ActiveConnection = objApp.rdoConnect
      QryNeg(X).Prepared = True
    Next
End Sub


