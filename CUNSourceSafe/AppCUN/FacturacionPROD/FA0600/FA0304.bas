Attribute VB_Name = "modAsignacionNumero"
Option Explicit





Private Function fAsignarNumFactura_TipoAsist(strFecha$, strTipoAsist$, strNumFact$, strErrorMsg$, txtMsg As TextBox) As Boolean
    ' Asigna el n� de factura a las facturas del tipo asistencia elegido
    
    Dim SQL As String
    Dim qryDatos As rdoQuery
    Dim intResp As Integer
    Dim intNumFact As Integer
    Dim strTexto As String
    
    On Error Resume Next
    
    strNumFact = Trim(InputBox("Introduzca el n� de factura para pacientes " & strTipoAsist & ": 13/", _
                        "Asignaci�n de n�meros de factura"))

    If Not IsNumeric(strNumFact) Or Len(strNumFact) <> 7 Then
        strTexto = "N�mero de factura incorrecto." & Chr$(13) & Chr$(10)
        If Not IsNumeric(strNumFact) Then
            strTexto = strTexto & "Debe introducir un valor num�rico."
        ElseIf Len(strNumFact) <> 6 Then
            strTexto = strTexto & "Debe tener 6 d�gitos."
        End If
        MsgBox strTexto, vbExclamation, "Asignaci�n de N�mero de Factura"
        strNumFact = ""
        Exit Function
    End If
    
    strNumFact = "13/" & strNumFact
    
    Call pMensaje("Asig. Num. Fact. " & strTipoAsist & ": " & strNumFact, txtMsg)
    
    SQL = "UPDATE   FA0400 " _
        & " SET     FA04NUMFACT = ? " _
        & " WHERE   FA04NUMFACT IN "
    If strTipoAsist = "HO" Then
        SQL = SQL & "(SELECT FA04NUMFACT "
        SQL = SQL & " FROM   FA0414J "
        SQL = SQL & " WHERE " _
                & "             FA04FECFACTURA = TO_DATE(?,'DD/MM/YYYY') " _
                & "         AND CI32CODTIPECON = 'S'" _
                & "         AND CI13CODENTIDAD = 'NA' " _
                & " )"
        SQL = SQL & "   AND EXISTS "
    Else
        SQL = SQL & "(SELECT FA04NUMFACT "
        SQL = SQL & " FROM  FA0422J "
        SQL = SQL & " WHERE " _
                & "             FA04FECFACTURA = TO_DATE(?,'DD/MM/YYYY') " _
                & "         AND CI32CODTIPECON = 'S'" _
                & "         AND CI13CODENTIDAD = 'NA' " _
                & " )"
        SQL = SQL & "   AND NOT EXISTS "
    End If
    SQL = SQL & "   (SELECT FA03NUMRNF " _
              & "    FROM   FA0300, " _
              & "           FA0500 " _
              & "    WHERE  FA0500.FA05CODCATEG = FA0300.FA05CODCATEG " _
              & "       AND FA0500.FA08CODGRUPO = 5 " _
              & "       AND FA0300.FA04NUMFACT = FA0400.FA04CODFACT " _
              & "   )"
    SQL = SQL & "   AND FA04NUMFACREAL IS NULL "


    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = strNumFact
    qryDatos(1) = Format(strFecha, "dd/mm/yyyy")
    qryDatos.Execute
    If Err <> 0 Then
        Call pMensaje("Asig. Num. Fact. " & strTipoAsist & ": ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Asig. Num. Fact. " & strTipoAsist & ": " & qryDatos.RowsAffected & " regs.", txtMsg)
    
    fAsignarNumFactura_TipoAsist = True
End Function

Public Function fAsignarNumFactura(strFecha$, _
                                blnHospi As Boolean, _
                                blnAmbu As Boolean, _
                                strErrorMsg$, _
                                txtMsg As TextBox) As Boolean
    ' Asigna los n�meros de factura
    
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim strNumFactHO$, strNumFactAM$
    Dim intRes As Integer
    
    If blnHospi Or blnAmbu Then
        Screen.MousePointer = vbHourglass
        objApp.rdoConnect.BeginTrans
        If blnHospi Then
            If Not fAsignarNumFactura_TipoAsist(strFecha, "HO", strNumFactHO, strErrorMsg, txtMsg) Then
                objApp.rdoConnect.RollBackTrans
                Screen.MousePointer = vbDefault
                Exit Function
            End If
        End If
        If blnAmbu Then
            If Not fAsignarNumFactura_TipoAsist(strFecha, "AM", strNumFactAM, strErrorMsg, txtMsg) Then
                objApp.rdoConnect.RollBackTrans
                Screen.MousePointer = vbDefault
                Exit Function
            End If
        End If
        
        intRes = MsgBox("Asignaci�n realizada con �xito. " _
                & Chr$(13) & Chr$(10) _
                & " Hospitalizados: " & strNumFactHO _
                & Chr$(13) & Chr$(10) _
                & " Ambulatorios: " & strNumFactAM _
                & Chr$(13) & Chr$(10) _
                & " �Desea guardar los cambios?" _
                , vbInformation + vbYesNo, "Asignaci�n de N�meros de Factura")
        If intRes = vbYes Then
            objApp.rdoConnect.CommitTrans
            fAsignarNumFactura = True
        Else
            objApp.rdoConnect.RollBackTrans
        End If

        Screen.MousePointer = vbDefault
    End If
End Function


Public Sub pDatosFacturas(strFecha$, _
                        strNumFacHospi$, strImporteFacHospi$, _
                        strNumFacAmbu$, strImporteFacAmbu$, _
                        strNumFacTot$, strImporteFacTot$)
                        
    ' Se obtienen los datos de las facturas de esa fecha para facturas de Osasunbidea
    
    
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim lngNumFacTot As Long, lngImporteFacTot As Long
    
'    SQL = "SELECT DECODE(AD12CODTIPOASIST, " _
'            & "             1,  'HO', " _
'            & "                 'AM') AS TIPOASISTENCIA, " _
'            & " COUNT(DISTINCT FA04CODFACT) AS NUMFACT, " _
'            & " SUM(FA04CANTFACT) AS IMPORTEFACT "
'    SQL = SQL & " FROM  FA0400, " _
'            & "         AD2500  "
'    SQL = SQL & " WHERE AD2500.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
'            & "         AND FA0400.FA04FECFACTURA BETWEEN   AD2500.AD25FECINICIO " _
'            & "                             AND     NVL(AD2500.AD25FECFIN, SYSDATE) " _
'            & "         AND FA0400.CI32CODTIPECON = 'S'" _
'            & "         AND FA0400.CI13CODENTIDAD = 'NA' " _
'            & "         AND FA0400.FA04FECFACTURA = TO_DATE(?,'DD/MM/YYYY') " _
'            & "         AND FA0400.FA04NUMFACREAL IS NULL "
'    SQL = SQL & " GROUP BY DECODE(AD12CODTIPOASIST, " _
'            & "             1,  'HO', " _
'            & "                 'AM')"
    
    
    SQL = "SELECT 'HO' AS TIPOASISTENCIA, " _
            & " COUNT(DISTINCT FA04CODFACT) AS NUMFACT, " _
            & " SUM(FA04CANTFACT) AS IMPORTEFACT "
    SQL = SQL & " FROM  FA0400 "
    SQL = SQL & " WHERE FA04CODFACT IN "
    SQL = SQL & "(SELECT FA04CODFACT "
    SQL = SQL & " FROM  FA0414J "
    SQL = SQL & " WHERE " _
            & "         FA04FECFACTURA = TO_DATE(?,'DD/MM/YYYY') " _
            & "         AND CI32CODTIPECON = 'S'" _
            & "         AND CI13CODENTIDAD = 'NA' " _
            & " )"
    SQL = SQL & " AND EXISTS "
    SQL = SQL & "   (SELECT FA03NUMRNF " _
              & "    FROM   FA0300, " _
              & "           FA0500 " _
              & "    WHERE  FA0500.FA05CODCATEG = FA0300.FA05CODCATEG " _
              & "       AND FA0500.FA08CODGRUPO = 5 " _
              & "       AND FA0300.FA04NUMFACT = FA0400.FA04CODFACT " _
              & "   )"
    SQL = SQL & " UNION "
    SQL = SQL & "SELECT 'AM' AS TIPOASISTENCIA, " _
            & " COUNT(DISTINCT FA04CODFACT) AS NUMFACT, " _
            & " SUM(FA04CANTFACT) AS IMPORTEFACT "
    SQL = SQL & " FROM  FA0400 "
    SQL = SQL & " WHERE FA04CODFACT IN "
    SQL = SQL & "(SELECT FA04CODFACT "
    SQL = SQL & " FROM  FA0422J "
    SQL = SQL & " WHERE " _
            & "         FA04FECFACTURA = TO_DATE(?,'DD/MM/YYYY') " _
            & "         AND CI32CODTIPECON = 'S'" _
            & "         AND CI13CODENTIDAD = 'NA' " _
            & " )"
    SQL = SQL & " AND NOT EXISTS "
    SQL = SQL & "   (SELECT FA03NUMRNF " _
              & "    FROM   FA0300, " _
              & "           FA0500 " _
              & "    WHERE  FA0500.FA05CODCATEG = FA0300.FA05CODCATEG " _
              & "       AND FA0500.FA08CODGRUPO = 5 " _
              & "       AND FA0300.FA04NUMFACT = FA0400.FA04CODFACT " _
              & "   )"
    
    
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFecha, "dd/mm/yyyy")
    qryDatos(1) = Format$(strFecha, "dd/mm/yyyy")
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsDatos.EOF
        Select Case rsDatos!tipoAsistencia
            Case "HO"
                strNumFacHospi = Format$(rsDatos!numFact, "##,###")
                strImporteFacHospi = Format$(rsDatos!importeFact, "##,### Pta")
            Case "AM"
                strNumFacAmbu = Format$(rsDatos!numFact, "##,###")
                strImporteFacAmbu = Format$(rsDatos!importeFact, "##,### Pta")
        End Select
        lngNumFacTot = lngNumFacTot + rsDatos!numFact
        lngImporteFacTot = lngImporteFacTot + IIf(IsNull(rsDatos!importeFact), 0, rsDatos!importeFact)
        rsDatos.MoveNext
    Loop
    rsDatos.Close
    qryDatos.Close
    
    strNumFacTot = Format$(lngNumFacTot, "##,###")
    strImporteFacTot = Format$(lngImporteFacTot, "##,### Pta")

End Sub



