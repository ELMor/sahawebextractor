Attribute VB_Name = "modFunciones"
Option Explicit

'Public Enum FADestMsg
'   dmPantalla = 0    ' los mensajes se envian a pantalla
'   dmFichero = 1     ' los mensajes se envian a fichero
'End Enum

Public Function fNextClave(campo$, tabla$) As String
'Devuelve el siguiente valor de la clave principal obtenido del sequence
    Dim SQL$, rsMaxClave As rdoResultset
    
    SQL = "SELECT " & campo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(SQL)
    fNextClave = rsMaxClave(0)
    rsMaxClave.Close
    
End Function
Public Function fValidarDecimales(Tecla As Integer, Texto As String) As Integer
Dim strComa As String
Dim intPos As Integer

  Select Case Tecla
    Case 48 To 57
      fValidarDecimales = Tecla
      Exit Function
    Case 8
      fValidarDecimales = Tecla
      Exit Function
    Case 44
      strComa = Trim(Texto)
      intPos = InStr(strComa, ",")
      If intPos <> 0 Then
        fValidarDecimales = 0
        Exit Function
      Else
        fValidarDecimales = Tecla
        Exit Function
      End If
    Case 46
      strComa = Trim(Texto)
      intPos = InStr(strComa, ",")
      If intPos <> 0 Then
        fValidarDecimales = 0
        Exit Function
      Else
        fValidarDecimales = 44
        Exit Function
      End If
    Case 45
      If Trim(Texto) = "" Then
        fValidarDecimales = Tecla
        Exit Function
      Else
        fValidarDecimales = 0
        Exit Function
      End If
    Case Else
      fValidarDecimales = 0
  End Select
End Function
Public Function fValidarNumFactura(Tecla As Integer, Texto As String) As Integer
Dim strComa As String
Dim intPos As Integer

  Select Case Tecla
    Case 65 To 90
      fValidarNumFactura = Tecla
      Exit Function
    Case 48 To 57
      fValidarNumFactura = Tecla
      Exit Function
    Case 8
      fValidarNumFactura = Tecla
      Exit Function
    Case 47
      strComa = Trim(Texto)
      intPos = InStr(strComa, "/")
      If intPos <> 0 Then
        fValidarNumFactura = 0
        Exit Function
      Else
        fValidarNumFactura = Tecla
        Exit Function
      End If
    Case Else
      fValidarNumFactura = 0
  End Select
End Function

Public Function fValidarEnteros(Tecla As Integer) As Integer
Dim strComa As String
Dim intPos As Integer

  Select Case Tecla
    Case 48 To 57
      fValidarEnteros = Tecla
      Exit Function
    Case 8
      fValidarEnteros = Tecla
      Exit Function
    Case Else
      fValidarEnteros = 0
  End Select
End Function

Public Function NumToSql(Numero As Double) As String
    On Error GoTo error
    NumToSql = Format(Numero, "########0.####")
    Exit Function
error:
    NumToSql = 0
End Function

Public Function RedondearMoneda(Importe As Double, Tipo As TipoImporte, Optional nMoneda As Integer = 0) As Double
   Dim strMascara As String
   
   On Error GoTo error
   
   ' determinar que mascara hay que aplicar dependiendo del tipo de importe y la moneda
   Select Case Tipo
      Case tiPrecioUnit
         strMascara = "##########0"
      Case tiImporteLinea
         strMascara = "##########0"
      Case tiTotalFact
         strMascara = "##########0"
   End Select
         
   ' aplicar la mascara seleccionada
   RedondearMoneda = CDbl(Format(Importe, strMascara))
   
   Exit Function
error:
   MsgBox "Error en el redondeo de importes", vbCritical + vbOKOnly
   RedondearMoneda = 0
End Function

Public Function FormatearMoneda(Importe As Double, Tipo As TipoImporte, Optional nMoneda As Integer = 0) As String
   Dim strMascara As String
   
   On Error GoTo error
   
   ' determinar que mascara hay que aplicar dependiendo del tipo de importe y la moneda
   Select Case Tipo
      Case tiPrecioUnit
         strMascara = "##,###,###,##0"
      Case tiImporteLinea
         strMascara = "##,###,###,##0"
      Case tiTotalFact
         strMascara = "##,###,###,##0"
   End Select
         
   ' aplicar la mascara seleccionada
   FormatearMoneda = Format(Importe, strMascara)
   
   Exit Function
error:
   MsgBox "Error en el formateo de importes", vbCritical + vbOKOnly
   FormatearMoneda = 0
End Function


Public Function MsgBoxFact(Prompt As String, Optional Buttons As VbMsgBoxStyle = vbOKOnly, Optional Title As String = "Facturación") As VbMsgBoxResult
   Dim Resultado As VbMsgBoxResult
   
   If blnFacturacionMasiva Then
      ' los mensajes se envian a un fichero
      Write #1, Prompt
      Resultado = vbOK
   Else
      ' los mensajes se envian a pantalla
      Resultado = MsgBox(Prompt, Buttons, Title)
   End If
   
   MsgBoxFact = Resultado
End Function
