Attribute VB_Name = "modImprimeFact"
Option Explicit

Const ConvX = 54.0809
Const ConvY = 54.6101086
Dim vsFact As vsPrinter
Sub ImprimirDesgloseAmbulatorio(arEntidad() As typeEntidad, Entidad As Integer, Factura As Integer, strFechaFact$)
Dim Paciente, j, Linea, Cont As Integer
Dim LinAmbul As Integer
Dim Texto As String
Dim Diagnos As String
Dim Especia As String
Dim DepAnt As String
Dim nDesg As Integer
Dim TotPacientes, totLineas As Long
Dim blnPrincipal As Boolean

  DepAnt = ""
  Linea = 0
  Cont = 2
  
  With arEntidad(Entidad).arFacturasEntidad(Factura)
    'Imprimimos la cabecera de la pagina
    Call CabeceraAmbulatorio(ConvertirMeses(Format(IIf(.strFecha = "", strFechaFact, .strFecha), "MM")), CInt(Format(IIf(.strFecha = "", strFechaFact, .strFecha), "YYYY")), _
                              arEntidad(Entidad).strDesig, .strNumFact, _
                             Cont, strFechaFact, arEntidad(Entidad).strEntidad, .strDescrip)
                           
    On Error Resume Next
      LinAmbul = UBound(.arPacientes)
      If Err <> 0 Then
        LinAmbul = 0
      End If
    On Error GoTo 0
    For Paciente = 1 To LinAmbul
      'Miramos si el departamento es distinto para ponerlo
      blnPrincipal = False
      If DepAnt <> .arPacientes(Paciente).strDpto Then
        Texto = "***********"
        vsFact.CurrentX = 25 * ConvY
        vsFact.Text = Texto
        Texto = UCase(.arPacientes(Paciente).strDpto)
        vsFact.CurrentX = 55 * ConvY
        vsFact.Text = Texto
        vsFact.Paragraph = ""
        vsFact.Paragraph = ""
        DepAnt = .arPacientes(Paciente).strDpto
      End If
      'LINEAS DEL FICHERO
      'Linea = Linea + 1
      'Texto = Linea 'Contador
      Texto = Paciente
      vsFact.CurrentX = 30 * ConvY - vsFact.TextWidth(Texto)
      vsFact.Text = Texto
      Texto = .arPacientes(Paciente).strNumSS 'N�mero de la seguridad social del asegurado
      vsFact.CurrentX = 56 * ConvY - vsFact.TextWidth(Texto)
      vsFact.Text = Texto
      vsFact.CurrentX = 60 * ConvY
      Texto = "SUBD"
      vsFact.Text = Texto
      vsFact.CurrentX = 73 * ConvY
      Texto = .arPacientes(Paciente).strPaciente 'Nombre del Paciente
      While vsFact.TextWidth(Texto) > 50 * ConvY
        Texto = Left(Texto, Len(Texto) - 1)
      Wend
      vsFact.Text = Texto
      'Imprimimos el servicio y el diagn�stico.
      Texto = UCase(.arPacientes(Paciente).strDpto)
      While vsFact.TextWidth(Texto) > 21 * ConvY
        Texto = Left(Texto, Len(Texto) - 1)
      Wend
      vsFact.CurrentX = 232 * ConvY
      vsFact.Text = Texto
      Texto = UCase(.arPacientes(Paciente).strDiagnostico)
      'vsFact.CurrentX = 277 * ConvY - vsFact.TextWidth(Texto)
      vsFact.CurrentX = 255 * ConvY
      vsFact.Text = Left(Texto, 15)
      'Cogemos cada una de las l�neas y las imprimimos.
      'En el caso de que sea la primera, comprobamos si es la principal para poner las revisiones, 1�s , etc.
      On Error Resume Next
        totLineas = UBound(.arPacientes(Paciente).arFacturasEntidad_Lineas)
        If Err <> 0 Then
          totLineas = 0
        End If
      On Error GoTo 0
      'N� de servicios
      For Linea = 1 To totLineas
        If .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).blnMostrar Then
            If .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).blnPrincipal Then
              
                If blnPrincipal Then
                    ' Si ya se ha introducido anteriormente una l�nea principal -> la mueva principal
                    ' se escribe con la designaci�n etc.
                    Texto = .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).dblCantidad
                    vsFact.CurrentX = 69 * ConvY - vsFact.TextWidth(Texto)
                    vsFact.Text = Texto
                    vsFact.CurrentX = 73 * ConvY
                    Texto = .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).strDescrip
                    vsFact.Text = Texto
                End If
              
              Texto = .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).dblCantidad
              vsFact.CurrentX = 139 * ConvY - vsFact.TextWidth(Texto)
              vsFact.Text = Texto
              'D�as
              Texto = .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).strFechas
              vsFact.CurrentX = 141 * ConvY
              vsFact.Text = Texto
              'N� de visitas (primera y sucesiva)
              Texto = .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).dblCantidadPrimera
              If Texto <> 0 Then
                vsFact.CurrentX = 163 * ConvY - vsFact.TextWidth(Texto)
                vsFact.Text = Texto
              End If
              Texto = .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).dblCantidadSucesiva
              If Texto <> 0 Then
                vsFact.CurrentX = 170 * ConvY - vsFact.TextWidth(Texto)
                vsFact.Text = Texto
              End If
              'Precio (primera y sucesiva)
              Texto = .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).dblPrecioPrimera
              Texto = Format(Texto, "###,###,##0")
              If Texto <> 0 Then
                vsFact.CurrentX = 192 * ConvY - vsFact.TextWidth(Texto)
                vsFact.Text = Texto
              End If
              Texto = .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).dblPrecioSucesiva
              Texto = Format(Texto, "###,###,##0")
              If Texto <> 0 Then
                vsFact.CurrentX = 211 * ConvY - vsFact.TextWidth(Texto)
                vsFact.Text = Texto
              End If
              'Costo total
              Texto = Format(.arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).dblImporte, "###,###,##0")
              vsFact.CurrentX = 228 * ConvY - vsFact.TextWidth(Texto)
              vsFact.Text = Texto
              vsFact.Paragraph = ""
                
              blnPrincipal = True
            Else
              If Not blnPrincipal Then
                vsFact.Paragraph = ""
                blnPrincipal = True
              End If
              Texto = .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).dblCantidad
              vsFact.CurrentX = 69 * ConvY - vsFact.TextWidth(Texto)
              vsFact.Text = Texto
              vsFact.CurrentX = 73 * ConvY
              Texto = .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).strDescrip
              vsFact.Text = Texto
              'D�as
              Texto = .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).strFechas
              vsFact.CurrentX = 141 * ConvY
              vsFact.Text = Texto
              Texto = Format(.arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).dblPrecio, "###,###,##0")
              vsFact.CurrentX = 211 * ConvY - vsFact.TextWidth(Texto)
              vsFact.Text = Texto
              Texto = Format(.arPacientes(Paciente).arFacturasEntidad_Lineas(Linea).dblImporte, "###,###,##0")
              vsFact.CurrentX = 228 * ConvY - vsFact.TextWidth(Texto)
              vsFact.Text = Texto
              Texto = UCase(.arPacientes(Paciente).strDpto)
              While vsFact.TextWidth(Texto) > 21 * ConvY
                Texto = Left(Texto, Len(Texto) - 1)
              Wend
              vsFact.CurrentX = 232 * ConvY
              vsFact.Text = Texto
              Texto = UCase(.arPacientes(Paciente).strDiagnostico)
              'vsFact.CurrentX = 277 * ConvY - vsFact.TextWidth(Texto)
              vsFact.CurrentX = 255 * ConvY
              vsFact.Text = Left(Texto, 15)
              vsFact.Paragraph = ""
            End If
            If vsFact.CurrentY > 180 * ConvX Then
              vsFact.NewPage
              Cont = Cont + 1
              Call CabeceraAmbulatorio(ConvertirMeses(Format(.strFecha, "MM")), CInt(Format(.strFecha, "YYYY")), _
                                       arEntidad(Entidad).strDesig, _
                                       .strNumFact, Cont, strFechaFact, arEntidad(Entidad).strEntidad, _
                                       .strDescrip)
            End If
        End If
      Next
      vsFact.Paragraph = ""
      If vsFact.CurrentY > 180 * ConvX Then
        vsFact.NewPage
        Cont = Cont + 1
        Call CabeceraAmbulatorio(ConvertirMeses(Format(.strFecha, "MM")), CInt(Format(.strFecha, "YYYY")), _
                                    arEntidad(Entidad).strDesig, _
                                    .strNumFact, Cont, strFechaFact, arEntidad(Entidad).strEntidad, _
                                    .strDescrip)
      End If
    Next
    vsFact.Action = paNewPage
  End With
End Sub

Sub ImprimirDesgloseEstancias(arEntidad() As typeEntidad, Entidad As Integer, Factura As Integer, strFechaFact$)
Dim x, j As Integer
Dim Texto As String
Dim Diagnos As String
Dim Especia As String
Dim LinEstan, LinFact As Long
Dim Paciente, Linea  As Integer
Dim Cont As Integer

  With arEntidad(Entidad).arFacturasEntidad(Factura)
  
    On Error Resume Next
      LinEstan = UBound(.arPacientes)
      If Err <> 0 Then
        LinEstan = 0
      End If
    On Error GoTo 0
  Cont = 2
  Call CabeceraEstancias(ConvertirMeses(Format(IIf(.strFecha = "", strFechaFact, .strFecha), "MM")), _
                             CInt(Format(IIf(.strFecha = "", strFechaFact, .strFecha), "YYYY")), _
                             arEntidad(Entidad).strDesig, .strNumFact, _
                             Cont, strFechaFact, arEntidad(Entidad).strEntidad)
    For Paciente = 1 To LinEstan
      vsFact.Paragraph = ""
      vsFact.Paragraph = ""
     'LINEAS DEL FICHERO
      Texto = Paciente 'Contador
      vsFact.CurrentX = 24 * ConvY - vsFact.TextWidth(Texto)
      vsFact.Text = Texto
      'N�mero de la seguridad social
      Texto = .arPacientes(Paciente).strNumSS
      vsFact.CurrentX = 50 * ConvY - vsFact.TextWidth(Texto)
      vsFact.Text = Texto
      'Texto Fijo "SUBD"
      vsFact.CurrentX = 55 * ConvY
      vsFact.Text = "SUBD"
      'Nombre del asegurado
      vsFact.CurrentX = 68 * ConvY
      Texto = .arPacientes(Paciente).strPaciente
      While vsFact.TextWidth(Texto) > 50 * ConvY
        Texto = Left(Texto, Len(Texto) - 1)
      Wend
      vsFact.Text = Texto
      'Fecha de entrada
      vsFact.CurrentX = 125 * ConvY
      'Texto = .arPacientes(Paciente).strFechaIngreso
      'Texto = Format(.arPacientes(Paciente).strFechaIngreso, "MM-DD-HH")
        If Format(arEntidad(Entidad).arFacturasEntidad(Factura).strFecha, "MM") = Format(.arPacientes(Paciente).strFechaIngreso, "MM") Then
          Texto = "00-" & Format(.arPacientes(Paciente).strFechaIngreso, "DD-HH")
        Else
          Texto = Format(.arPacientes(Paciente).strFechaIngreso, "MM-DD-HH")
        End If
      vsFact.Text = Texto
      'Fecha de la intervenci�n
      vsFact.CurrentX = 146 * ConvY
      Texto = Format(.arPacientes(Paciente).strFechaIntervencion, "MM-DD-HH")
      vsFact.Text = Texto
      'Fecha de alta
      vsFact.CurrentX = 165 * ConvY
      'Texto = .arPacientes(Paciente).strFechaAlta
      'Texto = Format(.arPacientes(Paciente).strFechaAlta, "MM-DD")
        If Format(arEntidad(Entidad).arFacturasEntidad(Factura).strFecha, "MM") = Format(.arPacientes(Paciente).strFechaAlta, "MM") Then
          Texto = "00-" & Format(.arPacientes(Paciente).strFechaAlta, "DD-HH")
        Else
          Texto = Format(.arPacientes(Paciente).strFechaAlta, "MM-DD-HH")
        End If
      vsFact.Text = Texto
      'Diagn�stico de salida
      Diagnos = UCase(Left(.arPacientes(Paciente).strDiagnostico, 10)) 'Diagn�stico de salida
      vsFact.CurrentX = 253 * ConvY - vsFact.TextWidth(Diagnos)
      vsFact.Text = Diagnos
      'Especialidad
      Especia = UCase(Left(.arPacientes(Paciente).strDpto, 10)) 'Especialidad
      vsFact.CurrentX = 275 * ConvY - vsFact.TextWidth(Especia)
      vsFact.Text = Especia
      On Error Resume Next
        LinFact = UBound(.arPacientes(Paciente).arFacturasEntidad_Lineas)
        If Err <> 0 Then
          LinFact = 0
        End If
      On Error GoTo 0
      For Linea = 1 To LinFact
        With .arPacientes(Paciente).arFacturasEntidad_Lineas(Linea)
          If .blnPrincipal = True Then
            'N�mero de estancias
            Texto = .dblCantidad
            vsFact.CurrentX = 185 * ConvY - vsFact.TextWidth(Texto)
            vsFact.Text = Texto
            'Coste de las estancias
            Texto = .dblImporte
            Texto = Format(Texto, "###,###,##0")
            vsFact.CurrentX = 208 * ConvY - vsFact.TextWidth(Texto)
            vsFact.Text = Texto
            'Coste de los suplementos
            Texto = .dblImporteSuplementos
            Texto = Format(Texto, "###,###,##0")
            vsFact.CurrentX = 228 * ConvY - vsFact.TextWidth(Texto)
            vsFact.Text = Texto
          Else
            vsFact.Paragraph = ""
            If .blnMostrar = True Then
              'Cantidad
              Texto = .dblCantidad
              vsFact.CurrentX = 66 * ConvY - vsFact.TextWidth(Texto)
              vsFact.Text = Texto
              'Descripci�n
              vsFact.CurrentX = 68 * ConvY
              vsFact.Text = .strDescrip
              'Precio
              Texto = Format(CLng(.dblPrecio), "###,###,##0.##")
              vsFact.CurrentX = 208 * ConvY - vsFact.TextWidth(Texto)
              vsFact.Text = Texto
              'Importe
              Texto = Format(.dblImporte, "###,###,##0.##")
              vsFact.CurrentX = 228 * ConvY - vsFact.TextWidth(Texto)
              vsFact.Text = Texto
              'Diagnostico
              vsFact.CurrentX = 253 * ConvY - vsFact.TextWidth(Diagnos)
              vsFact.Text = Diagnos
              'Especialidad
              vsFact.CurrentX = 275 * ConvY - vsFact.TextWidth(Especia)
              vsFact.Text = Especia
              If vsFact.CurrentY > 180 * ConvX Then
                vsFact.NewPage
                Cont = Cont + 1
                Call CabeceraEstancias(ConvertirMeses(Format(arEntidad(Entidad).arFacturasEntidad(Factura).strFecha, "MM")), _
                             CInt(Format(arEntidad(Entidad).arFacturasEntidad(Factura).strFecha, "YYYY")), _
                             arEntidad(Entidad).strDesig, arEntidad(Entidad).arFacturasEntidad(Factura).strNumFact, _
                             Cont, strFechaFact, arEntidad(Entidad).strEntidad)
              End If
            End If
          End If
        End With
'        vsFact.Paragraph = ""
'        vsFact.Paragraph = ""
        If vsFact.CurrentY > 180 * ConvX Then
          vsFact.NewPage
          'Call CabeceraEstancias
          Cont = Cont + 1
          Call CabeceraEstancias(ConvertirMeses(Format(.strFecha, "MM")), _
                             CInt(Format(.strFecha, "YYYY")), _
                             arEntidad(Entidad).strDesig, .strNumFact, _
                             Cont, strFechaFact, arEntidad(Entidad).strEntidad)
          
        End If
      Next
    Next
    vsFact.Action = paNewPage
  End With


End Sub
Sub ImprimirDesgloseForfaits(arEntidad() As typeEntidad, Entidad As Integer, Factura As Integer, strFechaFact$)
Dim Cont, LineaFact As Integer
Dim Texto As String
Dim Linea, Paciente As Long
Dim LinForfait As Long
Dim Pacientes As Long
Dim PosicionPrecio As Double
Dim PosicionLinea As Double
Dim ImporteForf As Double
Dim ImporteSupl As Double

  With arEntidad(Entidad).arFacturasEntidad(Factura)
    Linea = 0
    On Error Resume Next
    Pacientes = UBound(.arPacientes)
    If Err <> 0 Then
      Pacientes = 0
    End If
    On Error GoTo 0
    'Imprimimos la cabecera
    Cont = 2
    Call CabeceraForfaits(ConvertirMeses(Format(.strFecha, "MM")), CInt(Format(.strFecha, "YYYY")), _
                          arEntidad(Entidad).strDesig, .strNumFact, CInt(Cont), strFechaFact, arEntidad(Entidad).strEntidad)
    PosicionLinea = vsFact.CurrentY
    For Paciente = 1 To Pacientes
      vsFact.Paragraph = ""
      With .arPacientes(Paciente)
        'Contador
        Linea = Linea + 1
        'Texto = Linea
        Texto = Paciente
        vsFact.CurrentX = 28 * ConvY - vsFact.TextWidth(Texto)
        vsFact.Text = Texto
        'Asegurado
        Texto = .strNumSS
        vsFact.CurrentX = 52 * ConvY - vsFact.TextWidth(Texto)
        vsFact.Text = Texto
        vsFact.CurrentX = 55 * ConvY
        vsFact.Text = "FORF"
        'Nombre del asegurado
        vsFact.CurrentX = 68 * ConvY
        Texto = .strPaciente
        While vsFact.TextWidth(Texto) > 50 * ConvY
          Texto = Left(Texto, Len(Texto) - 1)
        Wend
        vsFact.Text = Texto
        'Fecha de ingreso (MM-DD-HH)
        vsFact.CurrentX = 125 * ConvY
        If Format(arEntidad(Entidad).arFacturasEntidad(Factura).strFecha, "MM") = Format(.strFechaIngreso, "MM") Then
          Texto = "00-" & Format(.strFechaIngreso, "DD-HH")
        Else
          Texto = Format(.strFechaIngreso, "MM-DD-HH")
        End If
        vsFact.Text = Texto
        'Fecha de la intervenci�n (MM-DD)
        vsFact.CurrentX = 146 * ConvY
        If Format(arEntidad(Entidad).arFacturasEntidad(Factura).strFecha, "MM") = Format(.strFechaIntervencion, "MM") Then
          Texto = "00-" & Format(.strFechaIntervencion, "DD")
        Else
          Texto = Format(.strFechaIntervencion, "MM-DD")
        End If
        vsFact.Text = Texto
        'Fecha de Alta (DD-HH)
        vsFact.CurrentX = 165 * ConvY
        Texto = Format(.strFechaAlta, "DD-HH")
        vsFact.Text = Texto
        'Diagn�stico
        Texto = UCase(Left(.strDiagnostico, 10))
        vsFact.CurrentX = 253 * ConvY - vsFact.TextWidth(Texto)
        vsFact.Text = Texto
        'Especialidad
        Texto = UCase(Left(.strDpto, 10))
        'vsFact.CurrentX = 275 * ConvY - vsFact.TextWidth(Texto)
        vsFact.CurrentX = 255 * ConvY
        vsFact.Text = Left(Texto, 15)
        'Guardamos las posici�n del precio para poder luego imprimir los precios
        PosicionPrecio = vsFact.CurrentY
        'vsFact.Paragraph = ""
        On Error Resume Next
          LinForfait = UBound(.arFacturasEntidad_Lineas)
          If Err <> 0 Then
            LinForfait = 0
          End If
        On Error GoTo 0
        'Inicializamos las variables de los importes
        ImporteForf = 0
        ImporteSupl = 0
        For LineaFact = 1 To LinForfait
          With .arFacturasEntidad_Lineas(LineaFact)
            If .blnMostrar And .blnPrincipal Then
              ImporteForf = ImporteForf + .dblImporte
              ImporteSupl = ImporteSupl + .dblImporteSuplementos
              vsFact.Paragraph = ""
              vsFact.CurrentX = 60 * ConvY
              vsFact.Text = .strDescrip
            End If
          End With
        Next
        vsFact.Paragraph = ""
        PosicionLinea = vsFact.CurrentY
        If ImporteForf <> 0 Then
          vsFact.CurrentY = PosicionPrecio
          Texto = Format(ImporteForf, "###,###,##0.##")
          vsFact.CurrentX = 208 * ConvY - vsFact.TextWidth(Texto)
          vsFact.Text = Texto
        End If
        If ImporteSupl <> 0 Then
          vsFact.CurrentY = PosicionPrecio
          Texto = Format(ImporteSupl, "###,###,##0.##")
          vsFact.CurrentX = 228 * ConvY - vsFact.TextWidth(Texto)
          vsFact.Text = Texto
        End If
        vsFact.CurrentY = PosicionLinea
        'Comprobaremos que no tenemos que realizar un salto de p�gina para mantener el paciente
        'y los forfaits unidos.
        If vsFact.CurrentY > 190 * ConvX Then
          vsFact.NewPage
          Cont = Cont + 1
          Call CabeceraForfaits(ConvertirMeses(Format(arEntidad(Entidad).arFacturasEntidad(Factura).strFecha, "MM")), _
                                CInt(Format(arEntidad(Entidad).arFacturasEntidad(Factura).strFecha, "YYYY")), _
                                arEntidad(Entidad).strDesig, arEntidad(Entidad).arFacturasEntidad(Factura).strNumFact, CInt(Cont), _
                                strFechaFact, arEntidad(Entidad).strEntidad)
        End If
      End With
    Next
    vsFact.Action = paNewPage
  End With
End Sub
Sub ImprimirFacturaInsalud(arEntidad() As typeEntidad, strFechaFact$)
Dim Entidades As Integer
Dim Facturas As Integer
Dim intCurrEnti As Integer
Dim intCurrFact As Integer

  Set vsFact = frmFacturaINSALUD.vsPrinter
  Entidades = UBound(arEntidad)
  
  vsFact.Preview = False
  vsFact.PreviewMode = pmPrinter
  vsFact.Action = paStartDoc
  
  ' Problemas con el vsView
  'vsFact.Action = paStartDoc
  'vsFact.Action = paEndDoc
  ' *******************************
  
  'Cogemos cada una de las lineas de arEntidad.
        ' Se facturan por grupos
        ' 1� Hospitalizados (Estancias y Forfaits)
        ' 2� El resto
        
  ' Hospitalizados
  For intCurrEnti = 1 To Entidades
    'Cogemos cada una de las facturas de la entidad
    On Error Resume Next
    Facturas = 0
    Facturas = UBound(arEntidad(intCurrEnti).arFacturasEntidad)
    For intCurrFact = 1 To Facturas
      'Imprimiremos la cabecera en base al campo intTipo
      With arEntidad(intCurrEnti).arFacturasEntidad(intCurrFact)
        If .intTipo = intFactEstancias Or .intTipo = intFactForfait Or .intTipo = intFactRadiocirugia Then
            Select Case .intTipo
              Case intFactEstancias 'Factura de Estancias
                  Call ImprimirPrimeraEstancias(arEntidad(), intCurrEnti, intCurrFact, strFechaFact)
                  Call ImprimirDesgloseEstancias(arEntidad(), intCurrEnti, intCurrFact, strFechaFact)
              Case intFactForfait, intFactRadiocirugia 'Factura Formato Forfait
                  Call ImprimirPrimeraForfaits(arEntidad(), intCurrEnti, intCurrFact, strFechaFact)
                  Call ImprimirDesgloseForfaits(arEntidad(), intCurrEnti, intCurrFact, strFechaFact)
            End Select
        End If
      End With
    Next intCurrFact
  Next intCurrEnti

  ' Ambulatorios
  For intCurrEnti = 1 To Entidades
    'Cogemos cada una de las facturas de la entidad.
    Facturas = UBound(arEntidad(intCurrEnti).arFacturasEntidad)
    For intCurrFact = 1 To Facturas
      'Imprimiremos la cabecera en base al campo intTipo
      With arEntidad(intCurrEnti).arFacturasEntidad(intCurrFact)
        If .intTipo <> intFactEstancias And .intTipo <> intFactForfait And .intTipo <> intFactRadiocirugia Then
            Call ImprimirPrimeraAmbulatorio(arEntidad(), intCurrEnti, intCurrFact, strFechaFact)
            Call ImprimirDesgloseAmbulatorio(arEntidad(), intCurrEnti, intCurrFact, strFechaFact)
        End If
      End With
    Next intCurrFact
  Next intCurrEnti
  vsFact.Action = paEndDoc
End Sub


Sub ImprimirResumenInsalud(arResumen() As typeResumen, _
                            strFechaFact$)

Dim intCurrTipoAsist As intTipoAsist
Dim intNumTipoAsist As Integer

  Set vsFact = frmFacturaINSALUD.vsPrinter
  intNumTipoAsist = UBound(arResumen)
  
  vsFact.Preview = False
  vsFact.PreviewMode = pmPrinter
  vsFact.Action = paStartDoc
  
  
  'Cogemos cada una de las lineas de arResumen.
        ' Se facturan por grupos
        ' 1� Hospitalizados
        ' 2� Ambulatorios
        

  For intCurrTipoAsist = 1 To intNumTipoAsist
    Call pImprimirResumenDatos(arResumen(), intCurrTipoAsist, strFechaFact)
    If intCurrTipoAsist < intNumTipoAsist Then
        vsFact.Action = paNewPage
    End If
  Next intCurrTipoAsist
    
  vsFact.Action = paEndDoc
  
  
  

End Sub

Sub CabeceraAmbulatorio(Mes As String, Anyo As Integer, Insalud As String, Factura As String, Contador As Integer, strFechaFact$, strEntidad$, strDesig$)
Dim Texto As String

  With vsFact
'    .Orientation = orLandscape
'    .FontName = "Arial"
'    .FontSize = 9
'    .Action = paStartDoc
'    .MarginTop = 15 * ConvX
'    .MarginLeft = 25 * ConvY
'    .MarginRight = 20 * ConvY
    
    'CABECERA DE LA FACTURA
'    .CurrentX = 25 * ConvY
'    Texto = "CENTRO SANITARIO CL�NICA UNIVERSITARIA"
'    .Text = Texto
'    .CurrentX = 210 * ConvY
'    Texto = "NUMERO 005"
'    .Text = Texto
'    .CurrentX = 235 * ConvY
'    Texto = "HOJA NUM."
'    .Text = Texto
'    Texto = Contador
'    .CurrentX = 265 * ConvY - .TextWidth(Texto)
'    .Text = Texto
'    .Paragraph = ""
'    .CurrentX = 85 * ConvY
'    Texto = "FACTURACION DEL MES DE " & ConvertirMeses(Format(strFechaFact, "mm"))
'    .Text = Texto
'    .CurrentX = 185 * ConvY
'    Texto = "ENTIDAD " & strEntidad
'    .Text = Texto
'    .Paragraph = ""
'    .TextAlign = taJustBaseline
'    .CurrentX = 25 * ConvY
'    Texto = "NOTA A CARGO DEL " & UCase(Insalud) & " , POR CONSULTAS EXTERNAS EN REGIMEN AMBULATORIO, " & _
'            "EN EL MES DE " & Mes & "/" & Anyo & ", DE LOS PACIENTES QUE SE DETALLAN, ACOMPA��NDOSE DE LOS " & _
'            "CORRESPONDIENTES PARTES DE ASISTENCIA DEBIDAMENTE AUTORIZADOS"
'    .Paragraph = Texto
'    .TextAlign = taLeftBaseline
'    .CurrentY = .CurrentY - 200
'    .CurrentX = 225 * ConvY
'    Texto = "REFERENCIA N. " & Factura
'    .Text = Texto

    .CurrentX = 20 * ConvY
    Texto = "CENTRO SANITARIO CLINICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "HOJA NUM"
    .Text = Texto
    .CurrentX = 245 * ConvY
    Texto = Contador
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 85 * ConvY
    Texto = "FACTURACI�N DEL MES DE " & ConvertirMeses(Format(strFechaFact, "mm"))
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "ENTIDAD " & strEntidad
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    Texto = "NOTA A CARGO DEL " & UCase(Insalud) & " , POR"
    
    If strDesig = strDescripProtesis Or strDesig = strDescripMedicacion Then
        Texto = Texto & " CONCEPTO DE " & strDesig
    Else
        Texto = Texto & " CONSULTAS EXTERNAS EN REGIMEN AMBULATORIO"
    End If
    Texto = Texto & ", EN EL MES DE " & Mes & "/" & Anyo & ", DE LOS PACIENTES QUE SE DETALLAN, ACOMPA��NDOSE DE LOS " & _
            "CORRESPONDIENTES PARTES DE ASISTENCIA DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .CurrentY = .CurrentY - 200
    .TextAlign = taLeftBaseline
    .CurrentX = 225 * ConvY
    Texto = "NUM.FRA " & Factura
    .Text = Texto
    .Paragraph = ""
    '.Paragraph = ""

    ' CABECERA DE LINEAS
    .Paragraph = ""
    .Paragraph = ""
    .CurrentX = 25 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 130 * ConvY
    Texto = "N.SERVICIOS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "N.VISITAS"
    .Text = Texto
    .CurrentX = 187 * ConvY
    Texto = "P R E C I O"
    .Text = Texto
    .CurrentX = 216 * ConvY
    Texto = "COSTO"
    .Text = Texto
    .CurrentX = 235 * ConvY
    Texto = "SERVICIO"
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 25 * ConvY
    Texto = "OR"
    .Text = Texto
    .CurrentX = 35 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 60 * ConvY
    Texto = "INSPE"
    .Text = Texto
    .CurrentX = 73 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 132 * ConvY
    Texto = "NUM  DIAS"
    .Text = Texto
    .CurrentX = 155 * ConvY
    Texto = "PRI    SUC"
    .Text = Texto
    .CurrentX = 181 * ConvY
    Texto = "PRIMER SUCESIVA"
    .Text = Texto
    .CurrentX = 217 * ConvY
    Texto = "TOTAL"
    .Text = Texto
    .CurrentX = 232 * ConvY
    Texto = "CL DENOMIN"
    .Text = Texto
    Texto = "DIAGNOSTICO"
    .CurrentX = 277 * ConvY - .TextWidth(Texto)
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 19 * ConvY
    While .CurrentX < 277 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .Paragraph = ""
        
  End With
End Sub
Sub CabeceraEstancias(Mes As String, Anyo As Integer, Insalud As String, Factura As String, Contador As Integer, strFechaFact$, strEntidad$)

Dim Texto As String
  With vsFact
    'CABECERA DE LA FACTURA
    .CurrentX = 20 * ConvY
    Texto = "CENTRO SANITARIO CLINICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "HOJA NUM"
    .Text = Texto
    .CurrentX = 245 * ConvY
    Texto = Contador
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 85 * ConvY
    Texto = "FACTURACI�N DEL MES DE " & ConvertirMeses(Format(strFechaFact, "mm"))
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "ENTIDAD " & strEntidad
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    Texto = "NOTA A CARGO DEL INSALUD DE " & UCase(Insalud) & " POR LA HOSPITALIZACI�N DEL MES DE " & Mes & " / " & Anyo & _
            ", DE LOS PACIENTES QUE SE DETALLAN, ACOMPA��NDOSE DE LOS CORRESPONDIENTES PARTES DE ASISTENCIA " & _
            "DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .CurrentY = .CurrentY - 200
    .TextAlign = taLeftBaseline
    .CurrentX = 225 * ConvY
    Texto = "NUM.FRA " & Factura
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    
    'CABECERA DE LAS LINEAS
    .CurrentX = 20 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "N."
    .Text = Texto
    .FontUnderline = True
    .CurrentX = 125 * ConvY
    Texto = "F.ENTRA"
    .Text = Texto
    .CurrentX = 145 * ConvY
    Texto = "F.INT"
    .Text = Texto
    .CurrentX = 160 * ConvY
    Texto = "F.SALIDA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "N"
    .Text = Texto
    .CurrentX = 200 * ConvY
    Texto = "C O S T O S"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "ESPECIALI"
    .Text = Texto
    .Paragraph = ""
    .FontUnderline = False
    .CurrentX = 20 * ConvY
    Texto = "ORD"
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 55 * ConvY
    Texto = "INSP."
    .Text = Texto
    .CurrentX = 68 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 125 * ConvY
    Texto = "MM.DD.H"
    .Text = Texto
    .CurrentX = 144 * ConvY
    Texto = "MM-DD"
    .Text = Texto
    .CurrentX = 162 * ConvY
    Texto = "DD-HH"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "EST"
    .Text = Texto
    .CurrentX = 192 * ConvY
    Texto = "ESTANCIAS"
    .Text = Texto
    .CurrentX = 212 * ConvY
    Texto = "SUPLEM."
    .Text = Texto
    .CurrentX = 230 * ConvY
    Texto = "DIAGNOSTICO"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "CL.DENOM"
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 19 * ConvY
    While .CurrentX < 275 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .Paragraph = ""
  End With
  
End Sub

Sub CabeceraForfaits(Mes As String, Anyo As Integer, Insalud As String, Factura As String, Contador As Integer, strFechaFact$, strEntidad$)

Dim Texto As String
  With vsFact
    'CABECERA DE LA FACTURA
    .CurrentX = 20 * ConvY
    Texto = "CENTRO SANITARIO CLINICA UNIVERSITARIA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "NUMERO 005"
    .Text = Texto
    .CurrentX = 210 * ConvY
    Texto = "HOJA NUM"
    .Text = Texto
    .CurrentX = 245 * ConvY
    Texto = Contador
    .Text = Texto
    .Paragraph = ""
    .CurrentX = 85 * ConvY
    Texto = "FACTURACI�N DEL MES DE " & ConvertirMeses(Format(strFechaFact, "mm"))
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "ENTIDAD " & strEntidad
    .Text = Texto
    .Paragraph = ""
    .TextAlign = taJustBaseline
    Texto = "NOTA A CARGO DEL " & UCase(Insalud) & " POR LOS PROCESOS QUIR�RGICOS EN EL MES DE " & _
            Mes & " / " & Anyo & _
            ", DE LOS PACIENTES QUE SE DETALLAN, ACOMPA��NDOSE DE LOS CORRESPONDIENTES PARTES DE ASISTENCIA " & _
            "DEBIDAMENTE AUTORIZADOS"
    .Paragraph = Texto
    .CurrentY = .CurrentY - 200
    .TextAlign = taLeftBaseline
    .CurrentX = 225 * ConvY
    Texto = "NUM.FRA " & UCase(Factura)
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    
    'CABECERA DE LAS LINEAS
    .CurrentX = 20 * ConvY
    Texto = "N."
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "N."
    .Text = Texto
    .FontUnderline = True
    .CurrentX = 125 * ConvY
    Texto = "F.ENTRA"
    .Text = Texto
    .CurrentX = 145 * ConvY
    Texto = "F.INT"
    .Text = Texto
    .CurrentX = 160 * ConvY
    Texto = "F.SALIDA"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "N"
    .Text = Texto
    .CurrentX = 200 * ConvY
    Texto = "C O S T O S"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "ESPECIALI"
    .Text = Texto
    .Paragraph = ""
    .FontUnderline = False
    .CurrentX = 20 * ConvY
    Texto = "ORD"
    .Text = Texto
    .CurrentX = 30 * ConvY
    Texto = "ASEGURADO"
    .Text = Texto
    .CurrentX = 55 * ConvY
    Texto = "INSP."
    .Text = Texto
    .CurrentX = 68 * ConvY
    Texto = "NOMBRE"
    .Text = Texto
    .CurrentX = 125 * ConvY
    Texto = "MM.DD.H"
    .Text = Texto
    .CurrentX = 144 * ConvY
    Texto = "MM-DD"
    .Text = Texto
    .CurrentX = 162 * ConvY
    Texto = "DD-HH"
    .Text = Texto
    .CurrentX = 180 * ConvY
    Texto = "EST"
    .Text = Texto
    .CurrentX = 192 * ConvY
    Texto = "ESTANCIAS"
    .Text = Texto
    .CurrentX = 212 * ConvY
    Texto = "SUPLEM."
    .Text = Texto
    .CurrentX = 230 * ConvY
    Texto = "DIAGNOSTICO"
    .Text = Texto
    .CurrentX = 255 * ConvY
    Texto = "CL.DENOM"
    .Text = Texto
    
    .Paragraph = ""
    .CurrentX = 19 * ConvY
    
    While .CurrentX <= 275 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
  End With
  
End Sub
Sub ImprimirPrimeraAmbulatorio(arEntidad() As typeEntidad, Entidad As Integer, Factura As Integer, strFechaFact$)
Dim Texto As String
Dim Insalud As New Persona
Dim x As Integer
Dim Cont As Integer
Dim Total As Double
Dim CabAmbulat As Long
Dim Fecha

  'Inicializamos el total de la factura a 0
  Total = 0
  'Obtenemos los datos del Insalud que se est� facturando.
  Insalud.Codigo = arEntidad(Entidad).lngCodReco
  Fecha = arEntidad(Entidad).arFacturasEntidad(Factura).strFecha
  With vsFact
    'Como es la cabecera de la factura de hospitalizados incluimos el escudo de la cl�nica.
    .Preview = False
    .FontName = "Arial"
    .FontSize = 9
    .Orientation = orLandscape
    .X1 = 20 * ConvY
    .Y1 = 30 * ConvX
    .X2 = 50 * ConvY
    .Y2 = 70 * ConvX
    '.Picture = Me.Picture1.Picture
    
    .MarginTop = 30 * ConvX
    .MarginLeft = 50 * ConvY
    .FontName = "Arial"
    .FontSize = 9
    .CurrentX = 50 * ConvY
    Texto = "CLINICA UNIVERSITARIA DE NAVARRA"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.Name
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "AVDA. PIO XII, 36"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = "C.I.F.: " & Insalud.DNI
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "31008 - PAMPLONA"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.Direccion
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "C.I.F.: Q-3168001-J"
    .Text = Texto
    .CurrentX = 195 * ConvY
    Texto = Insalud.CP & " " & Insalud.Poblacion
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 50 * ConvY
    Texto = "N� INSC. S.S.: 31/32.361/24"
    .Paragraph = Texto
    Texto = "COD. BCO: 0049 COD. SUC: 0022 N�CTA:5822-0"
    .Paragraph = Texto
    Texto = "REFERENCIA: " & arEntidad(Entidad).arFacturasEntidad(Factura).strNumFact
    .Paragraph = Texto
    Texto = Format(strFechaFact, "DD") & " DE " & ConvertirMeses(Format(strFechaFact, "MM")) & " DE " & Format(strFechaFact, "YYYY")
    .Paragraph = Texto
    .Paragraph = ""
    .MarginLeft = 25 * ConvY
    .TextAlign = taCenterBaseline
    .FontUnderline = True
    Texto = Insalud.Name
    .Paragraph = Texto
    .TextAlign = taLeftBaseline
    .FontUnderline = False
    .Paragraph = ""
    .Paragraph = ""
    Texto = "NOTA A CARGO DEL " & UCase(Insalud.Name) & " , POR "
    
    If arEntidad(Entidad).arFacturasEntidad(Factura).strDescrip = strDescripProtesis _
        Or arEntidad(Entidad).arFacturasEntidad(Factura).strDescrip = strDescripMedicacion Then
        Texto = Texto & " CONCEPTO DE " & arEntidad(Entidad).arFacturasEntidad(Factura).strDescrip
    Else
        Texto = Texto & " CONSULTAS EXTERNAS EN REGIMEN AMBULATORIO"
    End If
    
    Texto = Texto & " EN ESTE CENTRO EN EL MES DE " & _
            ConvertirMeses(Format(Fecha, "MM")) & "/ " & _
            Format(Fecha, "YYYY") & _
            ", DE LOS PACIENTES QUE A CONTINUACION SE DETALLAN ACOMPA��NDOSE DE LOS CORRESPONDIENTES " & _
            "PARTES DE ASISTENCIA DEBIDAMENTES AUTORIZADOS"
    .Paragraph = Texto
    .Paragraph = ""
    
    'Cogemos el resumen de los forfaits y lo imprimimos.
    On Error Resume Next
      CabAmbulat = UBound(arEntidad(Entidad).arFacturasEntidad(Factura).arResumen)
      If Err <> 0 Then
        CabAmbulat = 0
      End If
    On Error GoTo 0
    For x = 1 To CabAmbulat
      .CurrentX = 33 * ConvY
      Texto = ""
      If arEntidad(Entidad).arFacturasEntidad(Factura).arResumen(x).dblCantidad <> 0 Then
        Texto = arEntidad(Entidad).arFacturasEntidad(Factura).arResumen(x).dblCantidad
      End If
      .Text = Texto
      .CurrentX = 50 * ConvY
      Texto = arEntidad(Entidad).arFacturasEntidad(Factura).arResumen(x).strTexto
      While .TextWidth(Texto) > 85 * ConvY
        Texto = Left(Texto, Len(Texto) - 1)
      Wend
      .Text = Texto
      If arEntidad(Entidad).arFacturasEntidad(Factura).arResumen(x).dblPrecio <> 0 Then
        .CurrentX = 210 * ConvY
        Texto = "A"
        .Text = Texto
        Texto = Format(arEntidad(Entidad).arFacturasEntidad(Factura).arResumen(x).dblPrecio, "##,###,##0")
        .CurrentX = 238 * ConvY - .TextWidth(Texto)
        .Text = Texto
        .CurrentX = 240 * ConvY
        Texto = "PESETAS"
        .Text = Texto
      Else
        Texto = ""
      End If
      Texto = Format(arEntidad(Entidad).arFacturasEntidad(Factura).arResumen(x).dblImporte, "###,###,##0.##")
      Total = Total + CDbl(Texto)
      .CurrentX = 280 * ConvY - .TextWidth(Texto)
      .Text = Texto
      .CurrentY = .CurrentY + 300
      'vsFact.Paragraph = ""
    Next
    .CurrentX = 255 * ConvY
    While .CurrentX < 280 * ConvY
      .Text = "-"
    Wend
    .Paragraph = ""
    .CurrentX = 240 * ConvY
    .Text = "T O T A L"
    Texto = Format(Total, "###,###,##0.##")
    .CurrentX = (280 * ConvY) - .TextWidth(Texto)
    .Text = Texto
    .Paragraph = ""
    .Paragraph = ""
    .Paragraph = "EXENTO DE I.V.A. ART. 13-1,2"
    .Paragraph = "POR EL CENTRO CONCERTADO"
    If arEntidad(Entidad).strEntidad = "M" Then
        .Paragraph = "IGNACIO ORDOQUI"
        .Paragraph = "JEFE S. ADMINISTRACION"
    End If
    .Action = paNewPage
  End With


End Sub
Function ConvertirMeses(MesEntrada As String) As String
  Select Case MesEntrada
  Case "01"
    ConvertirMeses = "ENERO"
  Case "02"
    ConvertirMeses = "FEBRERO"
  Case "03"
    ConvertirMeses = "MARZO"
  Case "04"
    ConvertirMeses = "ABRIL"
  Case "05"
    ConvertirMeses = "MAYO"
  Case "06"
    ConvertirMeses = "JUNIO"
  Case "07"
    ConvertirMeses = "JULIO"
  Case "08"
    ConvertirMeses = "AGOSTO"
  Case "09"
    ConvertirMeses = "SEPTIEMBRE"
  Case "10"
    ConvertirMeses = "OCTUBRE"
  Case "11"
    ConvertirMeses = "NOVIEMBRE"
  Case "12"
    ConvertirMeses = "DICIEMBRE"
  End Select
End Function

Sub ImprimirPrimeraEstancias(arEntidad() As typeEntidad, Entidad As Integer, Factura As Integer, strFechaFact$)

Dim Texto As String
Dim Insalud As New Persona
Dim Cabecera As Double
Dim Total As Double
Dim totLineas As Long
Dim Linea As Integer
Dim dblTotalFact As Double
Dim Cont As Integer

  With vsFact
    .X1 = 20 * ConvY
    .Y1 = 28 * ConvX
    .X2 = 50 * ConvY
    .Y2 = 68 * ConvX
    '.Picture = Me.Picture1.Picture
    .MarginTop = 30 * ConvX
    .MarginLeft = 50 * ConvY
    .Orientation = orLandscape
    .FontName = "Arial"
    .FontSize = 9
    .MarginRight = 20 * ConvY
    Cabecera = 30 * ConvX
    
    'CABECERA DE LA FACTURA
    .CurrentY = Cabecera
    .CurrentX = 55 * ConvY
    Texto = "CLINICA UNIVERSITARIA DE NAVARRA"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "AVDA. PIO XII, 36"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "31008 - PAMPLONA"
    .Text = Texto
    .CurrentX = 55 * ConvY
    .CurrentY = .CurrentY + 200
    Texto = "C.I.F. 31-3168001-J"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "N�INSC. S.S.:31/32.361/24"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "COD.BCO.: 0049 COD.SUC.:022 N�CTA:5822-0"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "REFERENCIA N.: " & arEntidad(Entidad).arFacturasEntidad(Factura).strNumFact
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = Format(strFechaFact, "DD") & " DE " & ConvertirMeses(Format(strFechaFact, "MM")) & " DE " & Format(strFechaFact, "YYYY")
    .Text = Texto
    Insalud.Codigo = arEntidad(Entidad).lngCodReco
    .MarginLeft = 180 * ConvY
    .CurrentY = Cabecera
    .CurrentX = 190 * ConvY
    Texto = UCase(Insalud.Name)
    .Text = Texto
    .CurrentX = 190 * ConvY
    .CurrentY = .CurrentY + 200
    Texto = "C.I.F.: - " & Insalud.DNI
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 190 * ConvY
    Texto = UCase(Insalud.Direccion)
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 190 * ConvY
    Texto = UCase(Insalud.CPPoblac)
    .Text = Texto
    .CurrentY = .CurrentY + 1000
    
    vsFact.MarginLeft = 25 * ConvY
    vsFact.TextAlign = taCenterBaseline
    vsFact.FontUnderline = True
    Texto = UCase(Insalud.Name)
    vsFact.Paragraph = Texto
    vsFact.TextAlign = taJustBaseline
    vsFact.FontUnderline = False
    vsFact.Paragraph = ""
    Texto = "NOTA A CARGO DEL " & UCase(Insalud.Name) & " , POR LA HOSPITALIZACI�N EN " & _
            "ESTE CENTRO EN EL MES DE " & ConvertirMeses(Format(arEntidad(Entidad).arFacturasEntidad(Factura).strFecha, "MM")) & _
            " DE " & Format(arEntidad(Entidad).arFacturasEntidad(Factura).strFecha, "YYYY") & _
            ", DE LOS PACIENTES QUE A CONTINUACION SE " & _
            "DETALLAN, ACOMPA��NDOSE LOS CORRESPONDIENTES PARTES DEBIDAMENTE AUTORIZADOS"
    vsFact.Paragraph = Texto
    vsFact.Paragraph = ""
    
    'Recogemos las l�neas de la factura
    With arEntidad(Entidad).arFacturasEntidad(Factura)
      On Error Resume Next
      totLineas = UBound(.arResumen)
      For Linea = 1 To totLineas
        If .arResumen(Linea).dblCantidad <> 0 Then
          Texto = Format(.arResumen(Linea).dblCantidad, "###,##0.0")
          vsFact.CurrentX = 35 * ConvY - vsFact.TextWidth(Texto)
          vsFact.Text = Texto
        End If
        vsFact.CurrentX = 40 * ConvY
        vsFact.Text = .arResumen(Linea).strTexto
        Texto = Texto
        If .arResumen(Linea).dblPrecio <> 0 Then
          vsFact.CurrentX = 130 * ConvY
          vsFact.Text = "A"
          Texto = Format(.arResumen(Linea).dblPrecio, "###,###,##0.##")
          vsFact.CurrentX = 170 * ConvY - vsFact.TextWidth(Texto)
          vsFact.Text = Texto
        End If
        vsFact.CurrentX = 180 * ConvY '- vsFact.TextWidth(Texto)
        vsFact.Text = "PESETAS"
        Texto = Format(.arResumen(Linea).dblImporte, "###,###,##0.##")
        dblTotalFact = dblTotalFact + .arResumen(Linea).dblImporte
        vsFact.CurrentX = 280 * ConvY - vsFact.TextWidth(Texto)
        vsFact.Text = Texto
        vsFact.Paragraph = ""
      Next
    End With
'    vsFact.CurrentX = 220 * ConvY
'    While vsFact.CurrentX < 265 * ConvY
'      vsFact.Text = "-"
'    Wend
    .CurrentX = 255 * ConvY
    While .CurrentX < 280 * ConvY
      .Text = "-"
    Wend
    vsFact.Paragraph = ""
    vsFact.CurrentX = 180 * ConvY
    Texto = "T O T A L "
    vsFact.Text = Texto
    Texto = Format(dblTotalFact, "###,###,##0.##")
    'vsFact.CurrentX = 265 * ConvY - vsFact.TextWidth(Texto)
    vsFact.CurrentX = 280 * ConvY - vsFact.TextWidth(Texto)
    vsFact.Text = Texto
    vsFact.Paragraph = ""
    vsFact.Paragraph = ""
    vsFact.Paragraph = "EXENTO DE I.V.A. ART. 13-1,2"
    vsFact.Paragraph = "POR EL CENTRO CONCERTADO"
    If arEntidad(Entidad).strEntidad = "M" Then
        .Paragraph = "IGNACIO ORDOQUI"
        .Paragraph = "JEFE S. ADMINISTRACION"
    End If
    vsFact.Action = paNewPage
  End With

End Sub


Sub ImprimirPrimeraForfaits(arEntidad() As typeEntidad, Entidad As Integer, Factura As Integer, strFechaFact$)
Dim Texto As String
Dim Insalud As New Persona
Dim x As Integer
Dim Cont As Integer
Dim Total As Double
Dim linForf As Long
Dim Linea As Integer

  'Inicializamos el total de la factura a 0
  Total = 0
  'Obtenemos los datos del Insalud que se est� facturando.
  Insalud.Codigo = arEntidad(Entidad).lngCodReco
  
  With arEntidad(Entidad).arFacturasEntidad(Factura)
    'Como es la cabecera de la factura de hospitalizados incluimos el escudo de la cl�nica.
'    vsFact.Preview = False
    vsFact.FontName = "Arial"
    vsFact.FontSize = 9
    vsFact.Orientation = orLandscape
'    vsFact.Action = paStartDoc
    vsFact.X1 = 20 * ConvY
    vsFact.Y1 = 30 * ConvX
    vsFact.X2 = 50 * ConvY
    vsFact.Y2 = 70 * ConvX
'    vsFact.Picture = Me.Picture1.Picture
    
    vsFact.MarginTop = 30 * ConvX
    vsFact.MarginLeft = 50 * ConvY
    vsFact.FontName = "Arial"
    vsFact.FontSize = 9
    vsFact.CurrentX = 50 * ConvY
    Texto = "CLINICA UNIVERSITARIA DE NAVARRA"
    vsFact.Text = Texto
    vsFact.CurrentX = 195 * ConvY
    Texto = Insalud.Name
    vsFact.Text = Texto
    vsFact.CurrentY = vsFact.CurrentY + 200
    vsFact.CurrentX = 50 * ConvY
    Texto = "AVDA. PIO XII, 36"
    vsFact.Text = Texto
    vsFact.CurrentX = 195 * ConvY
    Texto = "C.I.F.: " & Insalud.DNI
    vsFact.Text = Texto
    vsFact.CurrentY = vsFact.CurrentY + 200
    vsFact.CurrentX = 50 * ConvY
    Texto = "31008 - PAMPLONA"
    vsFact.Text = Texto
    vsFact.CurrentX = 195 * ConvY
    Texto = Insalud.Direccion
    vsFact.Text = Texto
    vsFact.CurrentY = vsFact.CurrentY + 200
    vsFact.CurrentX = 50 * ConvY
    Texto = "C.I.F.: Q-3168001-J"
    vsFact.Text = Texto
    vsFact.CurrentX = 195 * ConvY
    Texto = Insalud.CP & " " & Insalud.Poblacion
    vsFact.Text = Texto
    vsFact.CurrentY = vsFact.CurrentY + 200
    vsFact.CurrentX = 50 * ConvY
    Texto = "N� INSC. S.S.: 31/32.361/24"
    vsFact.Paragraph = Texto
    Texto = "COD. BCO: 0049 COD. SUC: 0022 N�CTA:5822-0"
    vsFact.Paragraph = Texto
    Texto = "REFERENCIA: " & .strNumFact
    vsFact.Paragraph = Texto
    Texto = Format(strFechaFact, "DD") & " DE " & ConvertirMeses(Format(strFechaFact, "MM")) & " DE " & Format(strFechaFact, "YYYY")
    vsFact.Paragraph = Texto
    vsFact.Paragraph = ""
    vsFact.MarginLeft = 25 * ConvY
    vsFact.TextAlign = taCenterBaseline
    vsFact.FontUnderline = True
    Texto = Insalud.Name
    vsFact.Paragraph = Texto
    vsFact.TextAlign = taLeftBaseline
    vsFact.FontUnderline = False
    vsFact.Paragraph = ""
    vsFact.Paragraph = ""
    Texto = "NOTA A CARGO DEL " & UCase(Insalud.Name) & " , POR LOS PROCESOS QUIRURGICOS EN EL MES DE " & _
            ConvertirMeses(Format(.strFecha, "MM")) & "/ " & Format(.strFecha, "YYYY") & _
            ", DE LOS PACIENTES QUE A CONTINUACION SE DETALLAN ACOMPA��NDOSE DE LOS CORRESPONDIENTES " & _
            "PARTES DE ASISTENCIA DEBIDAMENTES AUTORIZADOS"
  vsFact.Paragraph = Texto
  vsFact.Paragraph = ""
  
  'Cogemos el resumen de los forfaits y lo imprimimos.
  On Error Resume Next
    linForf = UBound(.arResumen)
    If Err <> 0 Then
      linForf = 0
    End If
  On Error GoTo 0
  
  For Linea = 1 To linForf
    With .arResumen(Linea)
      'Cantidad
      If .dblCantidad <> 0 Then
        Texto = .dblCantidad
        vsFact.CurrentX = 36 * ConvY - vsFact.TextWidth(Texto)
        vsFact.Text = Texto
      End If
      'Descripcion
      Texto = IIf(Len(.strTexto) > 55, Left(.strTexto, 55), .strTexto)
      vsFact.CurrentX = 38 * ConvY
      vsFact.Text = Texto
      
      Texto = IIf(Len(.strTexto2) > 55, Left(.strTexto2, 55), .strTexto2)
      vsFact.CurrentX = 120 * ConvY
      vsFact.Text = Texto
      
      'Precio
      If .dblPrecio <> 0 Then
        vsFact.CurrentX = 210 * ConvY
        vsFact.Text = "A"
        Texto = Format(.dblPrecio, "###,###,##0.##")
        vsFact.CurrentX = 238 * ConvY - vsFact.TextWidth(Texto)
        vsFact.Text = Texto
      End If
      vsFact.CurrentX = 240 * ConvY
      vsFact.Text = "PESETAS"
      'Importe
      Texto = Format(.dblImporte, "###,###,##0.##")
      vsFact.CurrentX = 280 * ConvY - vsFact.TextWidth(Texto)
      vsFact.Text = Texto
      'Sumamos el importe al total
      Total = Total + .dblImporte
      vsFact.Paragraph = ""
    End With
  Next
  vsFact.CurrentX = 255 * ConvY
  While vsFact.CurrentX < 279 * ConvY
    vsFact.Text = "-"
  Wend
  vsFact.Paragraph = ""
  vsFact.CurrentX = 240 * ConvY
  vsFact.Text = "T O T A L"
  Texto = Format(Total, "###,###,##0.##")
  vsFact.CurrentX = 280 * ConvY - vsFact.TextWidth(Texto)
  vsFact.Text = Texto
  vsFact.Paragraph = ""
  vsFact.Paragraph = ""
  vsFact.Paragraph = "EXENTO DE I.V.A. ART. 13-1,2"
  vsFact.Paragraph = "POR EL CENTRO CONCERTADO"
    If arEntidad(Entidad).strEntidad = "M" Then
        vsFact.Paragraph = "IGNACIO ORDOQUI"
        vsFact.Paragraph = "JEFE S. ADMINISTRACION"
    End If

  vsFact.Action = paNewPage
  End With
End Sub

Private Sub pImprimirResumenDatos(arResumen() As typeResumen, _
                                    intCurrTipoAsist As intTipoAsist, _
                                    strFecha$)
Dim Texto As String
Dim Insalud As New Persona
Dim Cabecera As Double
Dim Total As Double
Dim totLineas As Long
Dim Linea As Integer
Dim dblTotalFact As Double
Dim Cont As Integer

  With vsFact
    .X1 = 20 * ConvY
    .Y1 = 28 * ConvX
    .X2 = 50 * ConvY
    .Y2 = 68 * ConvX
    '.Picture = Me.Picture1.Picture
    .MarginTop = 30 * ConvX
    .MarginLeft = 50 * ConvY
    .Orientation = orLandscape
    .FontName = "Arial"
    .FontSize = 9
    .MarginRight = 20 * ConvY
    Cabecera = 30 * ConvX
    
    'CABECERA DE LA FACTURA
    .CurrentY = Cabecera
    .CurrentX = 55 * ConvY
    Texto = "CLINICA UNIVERSITARIA DE NAVARRA"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "AVDA. PIO XII, 36"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "31008 - PAMPLONA"
    .Text = Texto
    .CurrentX = 55 * ConvY
    .CurrentY = .CurrentY + 200
    Texto = "C.I.F. 31-3168001-J"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "N�INSC. S.S.:31/32.361/24"
    .Text = Texto
    .CurrentY = .CurrentY + 200
    .CurrentX = 55 * ConvY
    Texto = "COD.BCO.: 0049 COD.SUC.:022 N�CTA:5822-0"
    .Text = Texto
    .MarginLeft = 180 * ConvY
    .CurrentY = Cabecera
    .CurrentX = 190 * ConvY
    Texto = Format(strFecha, "DD") & " DE " & ConvertirMeses(Format(strFecha, "MM")) & " DE " & Format(strFecha, "YYYY")
    .Text = Texto
    .CurrentY = .CurrentY + 1600
    
    vsFact.MarginLeft = 25 * ConvY
    vsFact.TextAlign = taJustBaseline
    vsFact.FontUnderline = False
    vsFact.Paragraph = ""
    Texto = "NOTA A CARGO DEL INSALUD TODAS LAS PROVINCIAS "
    Select Case intCurrTipoAsist
        Case intTipoAmbulatorio
            Texto = Texto & " POR LA ASISTENCIAEN REGIMEN AMBULATORIO "
        Case intTipoHospitalizado
            Texto = Texto & " POR LA HOSPITALIZACION EN ESTE CENTRO "
    End Select
    Texto = Texto & "DURANTE EL MES DE " & ConvertirMeses(Format(strFecha, "MM")) & _
            " DE " & Format(strFecha, "YYYY")
    
    vsFact.Paragraph = Texto
    vsFact.Paragraph = ""
    
    'Recogemos las l�neas datos
    With arResumen(intCurrTipoAsist)
      On Error Resume Next
      totLineas = UBound(.arDatos)
      For Linea = 1 To totLineas
        With .arDatos(Linea)
            If .dblCantidad <> 0 Then
              Texto = Format(.dblCantidad, "###,##0.0")
              vsFact.CurrentX = 35 * ConvY - vsFact.TextWidth(Texto)
              vsFact.Text = Texto
            End If
            
            vsFact.CurrentX = 40 * ConvY
            vsFact.Text = .strDescrip1
            Texto = Texto
            
            Texto = IIf(Len(.strDescrip2) > 55, Left(.strDescrip2, 55), .strDescrip2)
            vsFact.CurrentX = 120 * ConvY
            vsFact.Text = Texto
            
            If .dblPrecio <> 0 Then
              vsFact.CurrentX = 220 * ConvY
              vsFact.Text = "A"
              Texto = Format(.dblPrecio, "###,###,##0.##")
              vsFact.CurrentX = 240 * ConvY - vsFact.TextWidth(Texto)
              vsFact.Text = Texto
            End If
            vsFact.CurrentX = 245 * ConvY '- vsFact.TextWidth(Texto)
            vsFact.Text = "PESETAS"
            Texto = Format(.dblImporte, "###,###,##0.##")
            dblTotalFact = dblTotalFact + .dblImporte
            vsFact.CurrentX = 280 * ConvY - vsFact.TextWidth(Texto)
            vsFact.Text = Texto
            vsFact.Paragraph = ""
        End With
      Next
    End With
'    vsFact.CurrentX = 220 * ConvY
'    While vsFact.CurrentX < 265 * ConvY
'      vsFact.Text = "-"
'    Wend
    .CurrentX = 255 * ConvY
    While .CurrentX < 280 * ConvY
      .Text = "-"
    Wend
    vsFact.Paragraph = ""
    vsFact.CurrentX = 180 * ConvY
    Texto = "T O T A L "
    vsFact.Text = Texto
    Texto = Format(dblTotalFact, "###,###,##0.##")
    'vsFact.CurrentX = 265 * ConvY - vsFact.TextWidth(Texto)
    vsFact.CurrentX = 280 * ConvY - vsFact.TextWidth(Texto)
    vsFact.Text = Texto
    vsFact.Paragraph = ""
    vsFact.Paragraph = ""
    vsFact.Paragraph = "EXENTO DE I.V.A. ART. 13-1,2"
    vsFact.Paragraph = "POR EL CENTRO CONCERTADO"
  End With

End Sub


