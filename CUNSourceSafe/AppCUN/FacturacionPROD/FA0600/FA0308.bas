Attribute VB_Name = "modInsertReg"
Option Explicit

Public Function fInsertarReg(strFecha$, _
                                  blnHospi As Boolean, _
                                  blnAmbu As Boolean, _
                                  blnRehabil As Boolean, _
                                  strErrorMsg As String, _
                                  txtMsg As TextBox _
                                ) As Boolean

    ' Inserta registros en la tabla FA3700
    
    Dim SQL As String
    Dim qryDatos As rdoQuery
    
    On Error Resume Next
    If blnAmbu Or blnHospi Then
        SQL = "INSERT INTO /*+ RULE */ FA3700 " _
            & " (FA37CODPAC, " _
            & " AD01CODASISTENCI, " _
            & " AD07CODPROCESO, " _
            & " CI32CODTIPECON, " _
            & " CI13CODENTIDAD, " _
            & " AD12CODTIPOASIST, " _
            & " FA37INDFACT, " _
            & " FA37MES " _
            & " )"
        SQL = SQL & " SELECT    /*+ RULE */ FA37CODPAC_SEQUENCE.NEXTVAL, " _
            & "                 FA3701J.AD01CODASISTENCI, " _
            & "                 FA3701J.AD07CODPROCESO, " _
            & "                 FA3701J.CI32CODTIPECON, " _
            & "                 FA3701J.CI13CODENTIDAD, " _
            & "                 DECODE (FA3701J.TASI, 'H',1,2), " _
            & "                 0, " _
            & "                 LAST_DAY(TO_DATE(?,'DD/MM/YYYY')) " _
            & "      FROM       FA3701J, " _
            & "                 AD0100 " _
            & "      WHERE      FA3701J.FECHA BETWEEN    TO_DATE(?,'DD/MM/YYYY')" _
            & "                                 AND      LAST_DAY(TO_DATE(?,'DD/MM/YYYY')) " _
            & "             AND AD0100.AD01CODASISTENCI = FA3701J.AD01CODASISTENCI " _
            & "             AND FA3701J.CI32CODTIPECON = 'S' " _
            & "             AND FA3701J.CI13CODENTIDAD = 'NA' " _
            & "             AND NOT EXISTS "
'            & "                 (SELECT FA37CODPAC " _
'            & "                  FROM   FA3700 " _
'            & "                  WHERE  FA3700.FA37MES = FA3701J.FECHA " _
'            & "                     AND FA3700.CI32CODTIPECON = FA3701J.CI32CODTIPECON " _
'            & "                     AND FA3700.CI13CODENTIDAD = FA3701J.CI13CODENTIDAD " _
'            & "                     AND FA3700.AD01CODASISTENCI = FA3701J.AD01CODASISTENCI " _
'            & "                     AND FA3700.AD07CODPROCESO = FA3701J.AD07CODPROCESO " _
'            & "                 )"
        SQL = SQL & "           (SELECT /*+ RULE */ FA04NUMFACT " _
            & "                  FROM   FA0400 " _
            & "                  WHERE  FA0400.CI32CODTIPECON = FA3701J.CI32CODTIPECON " _
            & "                     AND FA0400.CI13CODENTIDAD = FA3701J.CI13CODENTIDAD " _
            & "                     AND FA0400.AD01CODASISTENCI = FA3701J.AD01CODASISTENCI " _
            & "                     AND FA0400.AD07CODPROCESO = FA3701J.AD07CODPROCESO " _
            & "                     AND FA0400.FA04NUMFACREAL IS NULL" _
            & "                 )"
        SQL = SQL & "      AND EXISTS " _
            & "                  (SELECT    /*+ RULE */ AD1100.AD01CODASISTENCI " _
            & "                   FROM      AD1100, AD1800 " _
            & "                   WHERE     AD1100.AD01CODASISTENCI = FA3701J.AD01CODASISTENCI " _
            & "                         AND AD1100.AD07CODPROCESO = FA3701J.AD07CODPROCESO " _
            & "                         AND (AD1100.AD11INDVOLANTE = 0 OR AD1100.AD11INDVOLANTE IS NULL)" _
            & "                         AND AD1800.AD01CODASISTENCI = AD1100.AD01CODASISTENCI " _
            & "                         AND AD1800.AD07CODPROCESO = AD1100.AD07CODPROCESO " _
            & "                         AND AD1800.AD19CODTIPODOCUM =1 " _
            & "                         AND AD1800.CI21CODPERSONA = AD0100.CI21CODPERSONA " _
            & "                 )"
        If Not blnAmbu Or Not blnHospi Then
            SQL = SQL & "   AND FA3701J.TASI = ?"
        End If
        
        Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
        qryDatos(0) = strFecha
        qryDatos(1) = "1/" & Format$(DateAdd("m", -6, CVDate(strFecha)), "mm/yyyy")
        qryDatos(2) = strFecha
        If Not blnAmbu Or Not blnHospi Then
            If blnAmbu Then qryDatos(3) = "A"
            If blnHospi Then qryDatos(3) = "H"
        End If
        
        qryDatos.Execute
        
        If Err <> 0 Then
            Call pMensaje("Inserción Registros: ERROR", txtMsg)
            strErrorMsg = error
            Exit Function
        Else
            Call pMensaje("Inserción Registros: " & qryDatos.RowsAffected & " regs.", txtMsg)
        End If
    End If
        
    If blnRehabil Then
        ' Hay que insertar los paciente de Rehabilitación y hemodiálisis
        
        ' Rehabilitación
        SQL = "INSERT INTO FA3700" _
            & " (FA37CODPAC, " _
            & " AD01CODASISTENCI, " _
            & " AD07CODPROCESO , " _
            & " CI32CODTIPECON, " _
            & " CI13CODENTIDAD, " _
            & " AD12CODTIPOASIST," _
            & " FA37INDFACT, " _
            & " FA37FECINICIO, " _
            & " FA37FECFIN, " _
            & " FA37MES  " _
            & " ) "
        SQL = SQL & " select  " _
            & " FA37CODPAC_SEQUENCE.nextval, " _
            & " ad01codasistenci, " _
            & " ad07codproceso, " _
            & " 'S', " _
            & " 'NA', " _
            & " 2, " _
            & " 0, " _
            & " TO_DATE(?,'DD/MM/YYYY'), " _
            & " last_day(TO_DATE(?,'DD/MM/YYYY')), " _
            & " last_day(TO_DATE(?,'DD/MM/YYYY')) "
'        SQL = SQL & " From FA0306J " _
'            & " where ci32codtipecon ='S' " _
'            & " AND CI13CODENTIDAD ='NA' " _
'            & " AND FA0306J.DPTOREA=211 " _
'            & " AND FA0306J.DPTORESP=211 " _
'            & " AND TRUNC(FA0306J.PR04FECFINACT) BETWEEN TO_DATE(?,'DD/MM/YYYY') " _
'            & "                         AND LAST_DAY(TO_DATE(?,'DD/MM/YYYY')) "
        SQL = SQL & " FROM AD0800 " _
            & " WHERE   (AD01CODASISTENCI, AD07CODPROCESO ) IN "
        SQL = SQL & " ( SELECT AD01CODASISTENCI, AD07CODPROCESO "
        SQL = SQL & "   FROM FA0306J " _
            & "         WHERE ci32codtipecon ='S' " _
            & "             AND CI13CODENTIDAD ='NA' " _
            & "             AND FA0306J.DPTOREA=211 " _
            & "             AND FA0306J.DPTORESP=211 " _
            & "             AND TRUNC(FA0306J.PR04FECFINACT) BETWEEN TO_DATE(?,'DD/MM/YYYY') " _
            & "                         AND LAST_DAY(TO_DATE(?,'DD/MM/YYYY')) " _
            & "     )"
        
        Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
        qryDatos(0) = "1/" & Format$(strFecha, "mm/yyyy")
        qryDatos(1) = strFecha
        qryDatos(2) = strFecha
        qryDatos(3) = "1/" & Format$(strFecha, "mm/yyyy")
        qryDatos(4) = strFecha
        
        qryDatos.Execute
        
        If Err <> 0 Then
            Call pMensaje("Inserción Registros Rehabilitación: ERROR", txtMsg)
            strErrorMsg = error
            Exit Function
        Else
            Call pMensaje("Inserción Registros Rehabilitación: " & qryDatos.RowsAffected & " regs.", txtMsg)
        End If

        ' Hemodiálisis
        SQL = "INSERT INTO FA3700" _
            & " (FA37CODPAC, " _
            & " AD01CODASISTENCI, " _
            & " AD07CODPROCESO , " _
            & " CI32CODTIPECON, " _
            & " CI13CODENTIDAD, " _
            & " AD12CODTIPOASIST," _
            & " FA37INDFACT, " _
            & " FA37FECINICIO, " _
            & " FA37FECFIN, " _
            & " FA37MES " _
            & " ) "
        SQL = SQL & " SELECT " _
            & " FA37CODPAC_SEQUENCE.nextval, " _
            & " ad01codasistenci, " _
            & " ad07codproceso, " _
            & " 'S', " _
            & " 'NA', " _
            & " 2, " _
            & " 0, " _
            & " TO_DATE(?,'DD/MM/YYYY'), " _
            & " last_day(TO_DATE(?,'DD/MM/YYYY')), " _
            & " last_day(TO_DATE(?,'DD/MM/YYYY')) " _
            & " FROM AD0800 " _
            & " WHERE   (AD01CODASISTENCI, AD07CODPROCESO ) IN "
        SQL = SQL & " ( SELECT AD01CODASISTENCI, AD07CODPROCESO "
        SQL = SQL & "   FROM FA0306J " _
            & "         WHERE ci32codtipecon ='S' " _
            & "             AND CI13CODENTIDAD ='NA' " _
            & "             AND FA0306J.PR01CODACTUACION =3346 " _
            & "             AND FA0306J.DPTOREA=212 " _
            & "             AND FA0306J.DPTORESP=212 " _
            & "             AND TRUNC(FA0306J.PR04FECFINACT) BETWEEN TO_DATE(?,'DD/MM/YYYY') " _
            & "                         AND LAST_DAY(TO_DATE(?,'DD/MM/YYYY')) " _
            & "     )"
        
        Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
        qryDatos(0) = "1/" & Format$(strFecha, "mm/yyyy")
        qryDatos(1) = strFecha
        qryDatos(2) = strFecha
        qryDatos(3) = "1/" & Format$(strFecha, "mm/yyyy")
        qryDatos(4) = strFecha
        
        qryDatos.Execute
        
        If Err <> 0 Then
            Call pMensaje("Inserción Registros Rehabilitación: ERROR", txtMsg)
            strErrorMsg = error
            Exit Function
        Else
            Call pMensaje("Inserción Registros Rehabilitación: " & qryDatos.RowsAffected & " regs.", txtMsg)
        End If

    End If
    
    fInsertarReg = True
End Function
