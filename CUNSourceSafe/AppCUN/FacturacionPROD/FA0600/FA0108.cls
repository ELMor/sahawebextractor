VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Atributos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public Codigo As Long
Public Suficiente As Boolean
Public Necesario As Boolean
Public Name As String
Public EsNodoFacturable As Boolean
Public FecInicio As String
Public FecFin As String
Public Opcional As Boolean
Public PrecioRef As Double
Public PrecioDia As Double
Public FranqUni As Boolean
Public FranqSuma As Boolean
Public PeriodoGara As Integer
Public Excluido As Boolean
Public LinOblig As Boolean
Public Descont As Boolean
Public FactOblig As Boolean
Public Descuento As Double
Public Ruta As String
Public EsCategoria As Boolean
Public CodTipEcon As String
Public EntidadResponsable As String
Public EsFinal As Boolean
Public Desglose As Boolean
Public Suplemento As Boolean
Public Descripcion As String
Public RutaRel As String
Public RelFijo As Double
Public RelPor As Double
Public OrdImp As Integer
Public CodAtrib As Long
Public TramAcum As Boolean
Public TramDias As Boolean
Public PlazoInter As Integer
Public NecRnfOrigen As Boolean
Public NecRnfDestino As Boolean
Public RecogerCant As Boolean
Public RecogerValor As Boolean
Public ContForf As Boolean
'************************************************************
' IMPORTANTE :
'
' Cuando se a�adan nuevas propiedades hay que tener en cuenta
' que hay que incluirlas en la rutina de copiar los Nodos
' en la clase nodo
'
'      Public Sub Copiar(NodoOrigen As nodo)
'
'************************************************************


'************************************************************
' Todas estas variables que empiezan por Aux son para guardar los datos
' que por defecto tiene el concierto por si en alg�n momento es necesario
' recuperarlos por haberlos modificado en la edicion de facturas
'************************************************************
Private AuxPrecioRef As Double
Private AuxPrecioDia As Double
Private AuxExcluido As Boolean
Private AuxLinOblig As Boolean
Private AuxDescont As Boolean
Private AuxFactOblig As Boolean
Private AuxDescuento As Double
Private AuxCodTipEcon As String
Private AuxEntidadResponsable As String
Private AuxDesglose As Boolean
Private AuxSuplemento As Boolean
Private AuxDescripcion As String
Private AuxOrdImp As Integer


Public Sub Restaurar()
    ' se restauran los atributos originales
    PrecioRef = AuxPrecioRef
    PrecioDia = AuxPrecioDia
    Excluido = AuxExcluido
    LinOblig = AuxLinOblig
    Descont = AuxDescont
    FactOblig = AuxFactOblig
    Descuento = AuxDescuento
    CodTipEcon = AuxCodTipEcon
    EntidadResponsable = AuxEntidadResponsable
    Desglose = AuxDesglose
    Suplemento = AuxSuplemento
    Descripcion = AuxDescripcion
    OrdImp = AuxOrdImp
End Sub

Public Sub Guardar()
    ' se realiza una copia de seguridad de los atributos modificables
    AuxPrecioRef = PrecioRef
    AuxPrecioDia = PrecioDia
    AuxExcluido = Excluido
    AuxLinOblig = LinOblig
    AuxDescont = Descont
    AuxFactOblig = FactOblig
    AuxDescuento = Descuento
    AuxCodTipEcon = CodTipEcon
    AuxEntidadResponsable = EntidadResponsable
    AuxDesglose = Desglose
    AuxSuplemento = Suplemento
    AuxDescripcion = Descripcion
    AuxOrdImp = OrdImp
End Sub

