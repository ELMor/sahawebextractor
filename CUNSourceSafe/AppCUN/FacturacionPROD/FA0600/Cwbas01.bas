Attribute VB_Name = "PublicConstants"
Option Explicit


' constantes para tipo de mantenimiento
Public Const cwFormDetail As Byte = 0
Public Const cwFormMultiLine As Byte = 1
Public Const cwFormMaster As Byte = 2


' constantes para posibles estados del mantenimiento
Public Const cwModeSingleEmpty As Byte = 0
Public Const cwModeSingleEdit As Byte = 1
Public Const cwModeSingleAddKey As Byte = 2
Public Const cwModeSingleAddRest As Byte = 3
Public Const cwModeSingleOpen As Byte = 4
Public Const cwModeMultiLineEdit As Byte = 0
Public Const cwModeMultiLineSave As Byte = 1
Public Const cwModeMultiLineReload As Byte = 2


' posibles constantes para el modelo de ventana
Public Const cwWithToolBar As Byte = 1
Public Const cwWithStatusBar As Byte = 2
Public Const cwWithMenu As Byte = 4
Public Const cwWithoutToolBar As Byte = 0
Public Const cwWithoutStatusBar As Byte = 0
Public Const cwWithoutMenu As Byte = 0


' posibles constantes para el modelo de formulario
Public Const cwWithGrid As Byte = 1
Public Const cwWithTab As Byte = 2
Public Const cwWithKeys As Byte = 4
Public Const cwWithoutGrid As Byte = 0
Public Const cwWithoutTab As Byte = 0
Public Const cwWithoutKeys As Byte = 0


' constantes para los menues
Public Const cwProcessData As Byte = 0
Public Const cwProcessEdit As Byte = 1
Public Const cwProcessFilter As Byte = 2
Public Const cwProcessRegister As Byte = 3
Public Const cwProcessOptions As Byte = 4
Public Const cwProcessHelp As Byte = 5
Public Const cwProcessToolBar As Byte = 6
Public Const cwProcessStatusBar As Byte = 7
Public Const cwProcessKeys As Byte = 8


' posibles modos de crear un cursor
Public Const cwGotoFirst As Byte = 1
Public Const cwGotoPrevious As Byte = 2
Public Const cwGotoNext As Byte = 3
Public Const cwGotoLast As Byte = 4
Public Const cwGotoActual As Byte = 5
Public Const cwGotoOnlyPrimary As Byte = 6


' Valores para los tabs del formulario
Public Const cwTabDetail As Byte = 0
Public Const cwTabGrid As Byte = 1


' tipos de mensajes de CodeWizard y Usuario
Public Const cwMessageCode As Byte = 0
Public Const cwMessageUser As Byte = 1


' constantes de mensajes de usuario
Public Const cwUserMessage As Byte = 0
Public Const cwUserQuery As Byte = 1


' mascaras para los controles de datos
Public Const cwMaskString As Byte = 0
Public Const cwMaskUpper As Byte = 1
Public Const cwMaskLower As Byte = 2
Public Const cwMaskInteger As Byte = 3
Public Const cwMaskReal As Byte = 4


' constantes para permisos del formulario
Public Const cwAllowAdd As Byte = 1
Public Const cwAllowModify As Byte = 2
Public Const cwAllowDelete As Byte = 4
Public Const cwAllowAll As Byte = 7
Public Const cwAllowReadOnly As Byte = 0


' constantes para las funciones SQL soportadas
Public Const cwSQLFuncNone As Byte = 0
Public Const cwSQLFuncIsNull As Byte = 1


' contantes para eventos
Public Const cwEventPreRead As Byte = 1
Public Const cwEventPostRead As Byte = 2
Public Const cwEventPreWrite As Byte = 3
Public Const cwEventPostWrite As Byte = 4
Public Const cwEventPreDelete As Byte = 5
Public Const cwEventPostDelete As Byte = 6
Public Const cwEventPreRestore As Byte = 7
Public Const cwEventPostRestore As Byte = 8
Public Const cwEventPreValidate As Byte = 9
Public Const cwEventPostValidate As Byte = 10
Public Const cwEventPreDefault As Byte = 11
Public Const cwEventPostDefault As Byte = 12
Public Const cwEventPreChangeStatus As Byte = 13
Public Const cwEventPostChangeStatus As Byte = 14
Public Const cwEventPreChanged As Byte = 15
Public Const cwEventPostChanged As Byte = 16
Public Const cwEventPrint As Byte = 17
Public Const cwEventForeign As Byte = 18
Public Const cwEventMaint As Byte = 19


' constantes para la ToolBar del formulario
Public Const cwToolBarButtonNew As String = "b0"
Public Const cwToolBarButtonOpen As String = "b1"
Public Const cwToolBarButtonSave As String = "b2"
Public Const cwToolBarButtonPrint As String = "b3"
Public Const cwToolBarButtonDelete As String = "b4"
Public Const cwToolBarButtonCut As String = "b5"
Public Const cwToolBarButtonCopy As String = "b6"
Public Const cwToolBarButtonPaste As String = "b7"
Public Const cwToolBarButtonUndo As String = "b8"
Public Const cwToolBarButtonFind As String = "b9"
Public Const cwToolBarButtonFilterOn As String = "b10"
Public Const cwToolBarButtonFilterOff As String = "b11"
Public Const cwToolBarButtonFirst As String = "b12"
Public Const cwToolBarButtonPrevious As String = "b13"
Public Const cwToolBarButtonNext As String = "b14"
Public Const cwToolBarButtonLast As String = "b15"
Public Const cwToolBarButtonRefresh As String = "b16"
Public Const cwToolBarButtonMaint As String = "b17"
Public Const cwToolBarButtonExit As String = "b18"


Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long
