VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DatosFact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Precio As Double
Public PrecioNoDescontable As Double

Public Cantidad As Double
Public Dto As Double

Public Importe As Double
Public ImporteNoDescontable As Double

Public Valor As Double
Public ValorNoDescontable As Double

Public YaEstaCalculado As Boolean

Public DescuentoManual As Double


'Public EsLineaFactura As Boolean
'Public LimiteFranquicia As Double

Public Function Total() As Double
    ' Tiene importe total
'    Total = RedondearMoneda(Importe * (100 - (Dto + DescuentoManual)) / 100, tiImporteLinea)
    Total = Importe * (100 - (Dto + DescuentoManual)) / 100
End Function

Public Function TotalNoDescontable() As Double
    ' Tiene importe total
'    TotalNoDescontable = RedondearMoneda(ImporteNoDescontable, tiImporteLinea)
    TotalNoDescontable = ImporteNoDescontable
End Function
