Attribute VB_Name = "modExportarFactura"
Option Explicit

Private Function fExport_CompruebaExistencia(strFecha$, strFich$, strErrorMsg$, txtMsg As TextBox) As Boolean
    ' Comprueba la existencia del fichero
    
    Dim strFileName As String
    Dim res As VbFileAttribute
    Dim intResp As Integer
    Dim strTexto As String
    
    strFileName = strFilePath & "\" & strFich & Format$(strFecha, "yyyymm") & ".cun"
    
    On Error Resume Next
    res = GetAttr(strFileName)
    If Err Then
        ' No existe el fichero
        ' Se contin�a con la operaci�n normal
        fExport_CompruebaExistencia = True
    Else
        ' Existe el fichero
        ' Se pregunta si se quiere borrar
        strTexto = "Ya existe el archivo " & strFileName & ". " _
                & Chr$(10) & Chr$(13) & "�Desea sobreescribirlo?"
        intResp = MsgBox(strTexto, vbQuestion + vbYesNo, "Ficheros de Osasunbidea")
        If intResp = vbYes Then
            Kill strFileName
            If Err <> 0 Then
                strErrorMsg = Error
                Call pMensaje("Eliminaci�n Fichero " & strFich & ": ERROR!!!!!", txtMsg)
                fExport_CompruebaExistencia = False
            Else
                Call pMensaje("Eliminaci�n Fichero " & strFich & ": Realizado", txtMsg)
                fExport_CompruebaExistencia = True
            End If
        Else
            fExport_CompruebaExistencia = False
        End If
    End If
        
End Function

Public Function fExportFichOsasun(strFecha$, _
                                    blnExportPacHospi As Boolean, _
                                    blnExportPacAmbu As Boolean, _
                                    blnExportConcept As Boolean, _
                                    blnExportDiag As Boolean, _
                                    strErrorMsg$, _
                                    txtMsg As TextBox) As Boolean
    
    Dim intRespHosp As Integer
    Dim intRespAmbu As Integer
        
    ' Se comprueba la existencia del fichero de Hospitalizados
    If blnExportPacHospi Then
        If Not fExport_CompruebaExistencia(strFecha, "HO", strErrorMsg, txtMsg) Then Exit Function
    End If
    
    ' Se comprueba la existencia del fichero de Ambulatorios
    If blnExportPacAmbu Then
        If Not fExport_CompruebaExistencia(strFecha, "AM", strErrorMsg, txtMsg) Then Exit Function
    End If
    
    ' Se comprueba la existencia del fichero de Conceptos
    If blnExportConcept Then
        If Not fExport_CompruebaExistencia(strFecha, "CO", strErrorMsg, txtMsg) Then Exit Function
    End If
    
    ' Se comprueba la existencia del fichero de Diagn�sticos
    If blnExportDiag Then
        If Not fExport_CompruebaExistencia(strFecha, "DG", strErrorMsg, txtMsg) Then Exit Function
    End If
    
    
    
    ' Crea el fichero de pacientes hospitalizados
    If blnExportPacHospi Then
        If Not fFich_CreaFichHospi(strFecha, strErrorMsg, txtMsg) Then
            Exit Function
        End If
    End If
    
    ' Crea el fichero de pacientes ambulatorios
    If blnExportPacAmbu Then
        If Not fFich_CreaFichAmbu(strFecha, strErrorMsg, txtMsg) Then
            Exit Function
        End If
    End If

    ' Crea el fichero de conceptos
    If blnExportConcept Then
        If Not fFich_CreaFichConcept(strFecha, strErrorMsg, txtMsg) Then
            Exit Function
        End If
    End If

    ' Crea el fichero de diagn�sticos
    If blnExportDiag Then
        If Not fFich_CreaFichDiag(strFecha, strErrorMsg, txtMsg) Then
            Exit Function
        End If
    End If

    fExportFichOsasun = True
End Function

Private Function fFich_CreaFichConcept(strFecha$, strErrorMsg$, txtMsg As TextBox) As Boolean
Dim SQL$
Dim qryDatos As rdoQuery
Dim rsDatos As rdoResultset
Dim strFileName As String
Dim intNumArch As Integer
    
    strFileName = strFilePath & "\CO" & Format$(strFecha, "yyyymm") & ".cun"
    
    intNumArch = FreeFile
    Open strFileName For Output As #intNumArch
    
  SQL = "SELECT " _
        & " RPAD(FA5200.FA52NUMSEGSOC,10)||" _
        & " RPAD(FA5200.FA52FECINICIO,8)||" _
        & " RPAD(FA5300.FA53CODIGO,6)||" _
        & " RPAD(FA5300.FA53CONCEPTO,2)||" _
        & " RPAD(FA5300.FA53DESCRIP,36)||" _
        & " LPAD(FA5300.FA53CANTIDAD,4)||" _
        & " LPAD(FA5300.FA53IMPORTE,10)||" _
        & " LPAD(FA5300.FA53IMPORTEGRUPO,10)||" _
        & " FA5200.FA52NUMREG||" _
        & " RPAD(FA5200.FA52FECPROCESO,6)||" _
        & " RPAD(FA5300.FA53FECHACONCEPTO,8) " _
        & "     AS TEXTO "
  SQL = SQL & " FROM FA5300," _
        & "         FA5200 "
  SQL = SQL & " WHERE   FA5200.FA52FECPROCESO=FA5300.FA52FECPROCESO " _
        & "         AND FA5200.FA52NUMREG=FA5300.FA52NUMREG " _
        & "         AND FA5200.FA52FECPROCESO=?"
  SQL = SQL & " ORDER BY FA5200.FA52NUMREG"
  Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
  qryDatos(0) = Format$(strFecha, "yyyymm")
  Set rsDatos = qryDatos.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
  Do While Not rsDatos.EOF
    Print #intNumArch, rsDatos!TEXTO
    rsDatos.MoveNext
  Loop
  Close #intNumArch
  rsDatos.Close
  qryDatos.Close
  fFich_CreaFichConcept = True
End Function

Public Function fFich_CreaFichDiag(strFecha$, strErrorMsg$, txtMsg As TextBox) As Boolean
Dim SQL$
Dim qryDatos As rdoQuery
Dim rsDatos As rdoResultset
Dim strFileName As String
Dim intNumArch As Integer
    
    strFileName = strFilePath & "\DG" & Format$(strFecha, "yyyymm") & ".cun"
    
    intNumArch = FreeFile
    Open strFileName For Output As #intNumArch
  SQL = "SELECT" _
        & "     RPAD(FA5400.FA54NUMSEGSOC,10)||" _
        & "     RPAD(FA5400.FA54FECINICIO,8)||" _
        & "     RPAD(FA5400.FA54CODIGO,6)||" _
        & "     RPAD(FA5400.FA54CONCEPTO,2)||" _
        & "     RPAD(FA5400.FA54DESCRIP,36)||" _
        & "     LPAD(FA5400.FA54TIPO,4)||" _
        & "     RPAD(FA5400.FA54LA24,24)||" _
        & "     RPAD(FA5400.FA54FECPROCESO,6) as TEXTO"
 SQL = SQL & " FROM FA5400 "
 SQL = SQL & " WHERE " _
        & "         FA5400.FA54FECPROCESO=? "
 SQL = SQL & " ORDER BY FA5400.FA54NUMDIAG"
  Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
  qryDatos(0) = Format$(strFecha, "yyyymm")
  Set rsDatos = qryDatos.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
  Do While Not rsDatos.EOF
    Print #intNumArch, rsDatos!TEXTO
    rsDatos.MoveNext
  Loop
  Close #intNumArch
  rsDatos.Close
  qryDatos.Close
  fFich_CreaFichDiag = True
End Function


Private Function fFich_CreaFichHospi(strFecha$, strErrorMsg$, txtMsg As TextBox) As Boolean
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim strFileName As String
    Dim intNumArch As Integer
    
    strFileName = strFilePath & "\HO" & Format$(strFecha, "yyyymm") & ".cun"
    
    intNumArch = FreeFile
    Open strFileName For Output As #intNumArch
    
    SQL = "SELECT " _
        & "         RPAD(FA52NUMSEGSOC,10)||" _
        & "         RPAD(FA52FECINICIO,8)|| " _
        & "         FA52TIPOASISTENCIA||" _
        & "         RPAD(FA52APEL1,18)||" _
        & "         RPAD(FA52APEL2,18)||" _
        & "         RPAD(FA52NOMB,14)||" _
        & "         RPAD(FA52CITE_CIP,23)||" _
        & "         RPAD(FA52FECHNAC,7)||" _
        & "         RPAD(FA52SEXO,1)||" _
        & "         RPAD(FA52CODPOSTAL,5)||" _
        & "         RPAD(FA52FECINGRESO,9)||" _
        & "         RPAD(FA52CIRCINGRESO,1)||" _
        & "         RPAD(FA52FECFIN,9)||" _
        & "         RPAD(FA52CIRCALTA,1)||" _
        & "         RPAD(FA52CODDPTO,3)||" _
        & "         LPAD(FA52CANTFACT,10)||" _
        & "         FA52NUMREG||" _
        & "         FA52FECPROCESO||" _
        & "         RPAD(FA52EXPINF,14) AS TEXTO "
    SQL = SQL & " FROM  FA5200 "
    SQL = SQL & " WHERE     FA52FECPROCESO = ? " _
        & "             AND FA52TIPOASISTENCIA = 'HO' "
    SQL = SQL & " ORDER BY  FA52NUMREG "
                
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFecha, "yyyymm")
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsDatos.EOF
        Print #intNumArch, rsDatos!TEXTO
        rsDatos.MoveNext
    Loop
    rsDatos.Close
    qryDatos.Close
    Close #intNumArch
    fFich_CreaFichHospi = True
End Function


Private Function fFich_CreaFichAmbu(strFecha$, strErrorMsg$, txtMsg As TextBox) As Boolean
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim strFileName As String
    Dim intNumArch As Integer
    
    strFileName = strFilePath & "\AM" & Format$(strFecha, "yyyymm") & ".cun"
    
    intNumArch = FreeFile
    Open strFileName For Output As #intNumArch
    
    SQL = "SELECT " _
        & "         RPAD(FA52NUMSEGSOC,10)||" _
        & "         RPAD(FA52FECINICIO,8)|| " _
        & "         FA52TIPOASISTENCIA||" _
        & "         RPAD(FA52APEL1,18)||" _
        & "         RPAD(FA52APEL2,18)||" _
        & "         RPAD(FA52NOMB,14)||" _
        & "         RPAD(FA52CITE_CIP,23)||" _
        & "         RPAD(FA52FECHNAC,7)||" _
        & "         RPAD(FA52SEXO,1)||" _
        & "         RPAD(FA52CODPOSTAL,5)||" _
        & "         RPAD(FA52FECCONSULTA,7)||" _
        & "         RPAD(FA52TIPOCONSULTA,1)||" _
        & "         RPAD(FA52CODDPTO,3)||" _
        & "         LPAD(FA52CANTFACT,10)||" _
        & "         FA52NUMREG||" _
        & "         FA52FECPROCESO||" _
        & "         RPAD(FA52EXPINF,14) AS TEXTO "
    SQL = SQL & " FROM  FA5200 "
    SQL = SQL & " WHERE     FA52FECPROCESO = ? " _
        & "             AND FA52TIPOASISTENCIA = 'AM' "
    SQL = SQL & " ORDER BY  FA52NUMREG "
                
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFecha, "yyyymm")
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsDatos.EOF
        Print #intNumArch, rsDatos!TEXTO
        rsDatos.MoveNext
    Loop
    rsDatos.Close
    qryDatos.Close
    Close #intNumArch
    
    fFich_CreaFichAmbu = True
End Function




