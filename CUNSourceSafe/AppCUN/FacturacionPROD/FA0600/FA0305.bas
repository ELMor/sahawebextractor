Attribute VB_Name = "modAsignacionFecha"
Option Explicit

                                

Public Sub pDatosFacturasMes(strFecha$, _
                        strNumFacHospi$, strImporteFacHospi$, _
                        strNumFacAmbu$, strImporteFacAmbu$, _
                        strNumFacTot$, strImporteFacTot$)
                        
    ' Se obtienen los datos de las facturas de esa fecha para facturas de Osasunbidea
    
    
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim lngNumFacTot As Long, lngImporteFacTot As Long
    
    SQL = "SELECT DECODE(AD12CODTIPOASIST, " _
            & "             1,  'HO', " _
            & "                 'AM') AS TIPOASISTENCIA, " _
            & " COUNT(DISTINCT FA04CODFACT) AS NUMFACT, " _
            & " SUM(FA04CANTFACT) AS IMPORTEFACT "
    SQL = SQL & " FROM  FA0400, " _
            & "         AD0100, " _
            & "         AD2500  "
    SQL = SQL & " WHERE AD2500.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
            & "         AND FA0400.AD01CODASISTENCI = AD0100.AD01CODASISTENCI" _
            & "         AND FA0400.FA04FECFACTURA BETWEEN   AD2500.AD25FECINICIO " _
            & "                             AND     NVL(AD2500.AD25FECFIN, SYSDATE) " _
            & "         AND FA0400.CI32CODTIPECON = 'S'" _
            & "         AND FA0400.CI13CODENTIDAD = 'NA' " _
            & "         AND AD0100.AD01FECFIN BETWEEN " _
            & "                                 TO_DATE(?,'DD/MM/YYYY')" _
            & "                             AND LAST_DAY(TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'))" _
            & "         AND FA0400.FA04NUMFACREAL IS NULL "
    SQL = SQL & " GROUP BY DECODE(AD12CODTIPOASIST, " _
            & "             1,  'HO', " _
            & "                 'AM')"
    
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = "1/" & Format$(strFecha, "mm/yyyy")
    qryDatos(1) = Format$(strFecha & " 23:59:59", "dd/mm/yyyy hh:nn:ss")
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsDatos.EOF
        Select Case rsDatos!tipoAsistencia
            Case "HO"
                strNumFacHospi = Format$(rsDatos!numFact, "##,###")
                strImporteFacHospi = Format$(rsDatos!importeFact, "##,### Pta")
            Case "AM"
                strNumFacAmbu = Format$(rsDatos!numFact, "##,###")
                strImporteFacAmbu = Format$(rsDatos!importeFact, "##,### Pta")
        End Select
        lngNumFacTot = lngNumFacTot + rsDatos!numFact
        lngImporteFacTot = lngImporteFacTot + rsDatos!importeFact
        rsDatos.MoveNext
    Loop
    rsDatos.Close
    qryDatos.Close
    
    strNumFacTot = Format$(lngNumFacTot, "##,###")
    strImporteFacTot = Format$(lngImporteFacTot, "##,### Pta")

End Sub

Public Function fAsignarFechaFactura(strFecha$, _
                                blnHospi As Boolean, _
                                blnAmbu As Boolean, _
                                strErrorMsg$, _
                                txtMsg As TextBox) As Boolean
   ' Asigna las fechas de factura
    
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim intRes As Integer
    
    If blnHospi Or blnAmbu Then
        Screen.MousePointer = vbHourglass
        objApp.rdoConnect.BeginTrans
        
        SQL = "UPDATE    FA0400 " _
            & " SET     FA04FECFACTURA = TO_DATE(?,'DD/MM/YYYY') " _
            & " WHERE   FA04CODFACT IN "
        SQL = SQL & " (SELECT   FA04CODFACT " _
            & "         FROM    FA0400, " _
            & "                 AD0100  "
        If Not blnHospi Or Not blnAmbu Then
            SQL = SQL & " , AD2500"
        End If
        SQL = SQL & "   WHERE   " _
            & "                 FA0400.AD01CODASISTENCI = AD0100.AD01CODASISTENCI " _
            & "             AND FA0400.CI32CODTIPECON = 'S' " _
            & "             AND FA0400.CI13CODENTIDAD = 'NA' " _
            & "             AND FA0400.FA04NUMFACREAL IS NULL " _
            & "             AND AD0100.AD01FECFIN BETWEEN " _
            & "                                         TO_DATE(?,'DD/MM/YYYY')" _
            & "                                     AND LAST_DAY(TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'))"
        If Not blnHospi Or Not blnAmbu Then
            SQL = SQL & "   AND FA0400.FA04FECFACTURA BETWEEN   AD2500.AD25FECINICIO" _
                      & "                            AND     NVL(AD2500.AD25FECFIN, SYSDATE)" _
                      & "         AND DECODE(AD12CODTIPOASIST, " _
                      & "             1,  'HO', " _
                      & "                 'AM') =  ?"
        End If
            SQL = SQL & ")"
        Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
        qryDatos(0) = Format(strFecha, "dd/mm/yyyy")
        qryDatos(1) = "1/" & Format(strFecha, "mm/yyyy")
        qryDatos(2) = Format(strFecha & " 23:59:59", "dd/mm/yyyy hh:nn:ss")
        If Not blnHospi Or Not blnAmbu Then
            If blnHospi Then
                qryDatos(3) = "HO"
            ElseIf blnAmbu Then
                qryDatos(3) = "AM"
            End If
        End If
        qryDatos.Execute
        If Err <> 0 Then
            Call pMensaje("Asig. Fecha " & strFecha & ": ERROR", txtMsg)
            strErrorMsg = Error
            qryDatos.Close
            objApp.rdoConnect.RollBackTrans
            Exit Function
        End If
        
        Call pMensaje("Asig. Fecha Fact. " & strFecha & ": " & qryDatos.RowsAffected & " regs.", txtMsg)
        
        intRes = MsgBox("Asignaci�n realizada con �xito. " _
                & Chr$(13) & Chr$(10) _
                & " Fecha: " & Format(strFecha, "dd/mm/yyyy") _
                & Chr$(13) & Chr$(10) _
                & " �Desea guardar los cambios?" _
                , vbInformation + vbYesNo, "Asignaci�n de N�meros de Factura")
        If intRes = vbYes Then
            objApp.rdoConnect.CommitTrans
            fAsignarFechaFactura = True
        Else
            objApp.rdoConnect.RollBackTrans
        End If

        Screen.MousePointer = vbDefault
    End If


End Function



