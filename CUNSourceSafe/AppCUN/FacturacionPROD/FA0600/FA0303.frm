VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "crystl32.ocx"
Begin VB.Form frmFacturaINSALUD 
   Caption         =   "Facturas INSALUD"
   ClientHeight    =   4500
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7980
   LinkTopic       =   "Form1"
   ScaleHeight     =   4500
   ScaleWidth      =   7980
   StartUpPosition =   3  'Windows Default
   Begin Crystal.CrystalReport crReport1 
      Left            =   4455
      Top             =   270
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   795
      Left            =   2880
      TabIndex        =   22
      Top             =   180
      Visible         =   0   'False
      Width           =   1515
      _Version        =   196608
      _ExtentX        =   2672
      _ExtentY        =   1402
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      Preview         =   0   'False
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   495
      Left            =   6720
      TabIndex        =   17
      Top             =   720
      Width           =   1215
   End
   Begin VB.Frame Frame2 
      Caption         =   "Fecha de las facturas"
      Height          =   1215
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   2415
      Begin SSCalendarWidgets_A.SSDateCombo ssdcFechaFactura 
         Height          =   375
         Left            =   120
         TabIndex        =   16
         Top             =   480
         Width           =   1815
         _Version        =   65537
         _ExtentX        =   3201
         _ExtentY        =   661
         _StockProps     =   93
         AllowEdit       =   0   'False
      End
   End
   Begin TabDlg.SSTab sstabINSALUD 
      Height          =   3195
      Left            =   0
      TabIndex        =   0
      Top             =   1260
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   5636
      _Version        =   327681
      Style           =   1
      TabHeight       =   520
      TabCaption(0)   =   "Informes"
      TabPicture(0)   =   "FA0303.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Impresos de Factura"
      TabPicture(1)   =   "FA0303.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdCrearINSALUDFacturas"
      Tab(1).Control(1)=   "fraProgreso"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Inserci�n de Registros"
      TabPicture(2)   =   "FA0303.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmdCrearRegsInsalud"
      Tab(2).ControlCount=   1
      Begin VB.CommandButton cmdCrearRegsInsalud 
         Caption         =   "Crear"
         Height          =   495
         Left            =   -68340
         TabIndex        =   23
         Top             =   2340
         Width           =   1215
      End
      Begin VB.Frame fraProgreso 
         Height          =   855
         Left            =   -74880
         TabIndex        =   20
         Top             =   1140
         Width           =   7635
         Begin ComctlLib.ProgressBar proProgreso 
            Height          =   435
            Left            =   60
            TabIndex        =   21
            Top             =   240
            Width           =   7455
            _ExtentX        =   13150
            _ExtentY        =   767
            _Version        =   327682
            Appearance      =   1
         End
      End
      Begin VB.CommandButton cmdCrearINSALUDFacturas 
         Caption         =   "&Crear"
         Height          =   495
         Left            =   -68340
         TabIndex        =   19
         Top             =   2340
         Width           =   1215
      End
      Begin VB.Frame Frame1 
         Caption         =   "Compropbaci�n"
         Height          =   2715
         Left            =   60
         TabIndex        =   1
         Top             =   420
         Width           =   6075
         Begin VB.CommandButton cmdFinMes 
            Caption         =   "Hospitalizados Fin de Mes"
            Height          =   495
            Left            =   120
            TabIndex        =   18
            Top             =   2100
            Width           =   1215
         End
         Begin VB.CommandButton cmdRehabilitacion 
            Caption         =   "Rehabilitaci�n"
            Height          =   495
            Left            =   4740
            TabIndex        =   14
            Top             =   2100
            Width           =   1215
         End
         Begin VB.CommandButton cmdHemodi�lisis 
            Caption         =   "Hemodi�lisis"
            Height          =   495
            Left            =   4740
            TabIndex        =   13
            Top             =   1500
            Width           =   1215
         End
         Begin VB.CommandButton cmdRadioterapia 
            Caption         =   "Radioterapia"
            Height          =   495
            Left            =   4740
            TabIndex        =   12
            Top             =   900
            Width           =   1215
         End
         Begin VB.CommandButton cmdProtesis 
            Caption         =   "Pr�tesis"
            Height          =   495
            Left            =   4740
            TabIndex        =   11
            Top             =   300
            Width           =   1215
         End
         Begin VB.CommandButton cmdActoMedico 
            Caption         =   "Acto M�dico"
            Height          =   495
            Left            =   2880
            TabIndex        =   10
            Top             =   1500
            Width           =   1215
         End
         Begin VB.CommandButton cmdForfAmbu 
            Caption         =   "Forfait Ambulatorio"
            Height          =   495
            Left            =   2880
            TabIndex        =   9
            Top             =   900
            Width           =   1215
         End
         Begin VB.CommandButton cmdForfPruebasAmbu 
            Caption         =   "Forfait Pruebas Ambulatorios"
            Height          =   495
            Left            =   2880
            TabIndex        =   8
            Top             =   300
            Width           =   1215
         End
         Begin VB.CommandButton cmdForfPruebasHospi 
            Caption         =   "Forfait Pruebas Hospitalizados"
            Height          =   495
            Left            =   1500
            TabIndex        =   7
            Top             =   2100
            Width           =   1215
         End
         Begin VB.CommandButton cmdForfInterv 
            Caption         =   "Forfait Intervenciones"
            Height          =   495
            Left            =   1500
            TabIndex        =   6
            Top             =   1500
            Width           =   1215
         End
         Begin VB.CommandButton cmdForfCadera 
            Caption         =   "Forfait Cadera, Catarat., Rodill."
            Height          =   495
            Left            =   1500
            TabIndex        =   5
            Top             =   900
            Width           =   1215
         End
         Begin VB.CommandButton cmdEstancias 
            Caption         =   "Estancias"
            Height          =   495
            Left            =   1500
            TabIndex        =   4
            Top             =   300
            Width           =   1215
         End
         Begin VB.CommandButton cmdGaranHospi 
            Caption         =   "Garant�as Hospitalizados"
            Height          =   495
            Left            =   120
            TabIndex        =   3
            Top             =   900
            Width           =   1215
         End
         Begin VB.CommandButton cmdGaranAmbu 
            Caption         =   "Garant�as Ambulatorios"
            Height          =   495
            Left            =   120
            TabIndex        =   2
            Top             =   300
            Width           =   1215
         End
      End
   End
End
Attribute VB_Name = "frmFacturaINSALUD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim crReport As CrystalReport

Private Sub pLimpiarReportFormulas()
    Dim i As Integer
    
    On Error Resume Next
    For i = 0 To 5
        crReport.Formulas(i) = ""
        If Err <> 0 Then Exit For
    Next i

End Sub

Private Sub cmdActoMedico_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0122.rpt"
    crReport.WindowTitle = "Facturas por Acto M�dico"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0423J.FA04FECFACTURA}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,01") & ")" _
                                 & " AND {FA0423J.FA04FECFACTURA}<DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")"
    'crReport.Formulas(0) = "TITULO = 'FACTURAS HOSPITALIZADAS DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"

    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdCrearINSALUDFacturas_Click()
    Dim arEntidad() As typeEntidad
    Dim arResumen(1 To 2) As typeResumen
    
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim SQL As String
    Dim i As Integer
    Dim strFecha As String
    
    Screen.MousePointer = vbHourglass
    
    arResumen(1).intTipoResumen = intTipoHospitalizado
    arResumen(2).intTipoResumen = intTipoAmbulatorio
    
    strFecha = "1/" & Format$(ssdcFechaFactura.Text, "mm/yyyy")
    
    
    proProgreso.Max = 100
    proProgreso.Value = 0
    fraProgreso.Caption = "Localizando Provincias a Facturar"
    fraProgreso.Parent.Refresh
    
'    SQL = "SELECT   /*+ rule */ CI1300.CI32CODTIPECON, " _
'        & "         CI1300.CI13CODENTIDAD, " _
'        & "         CI1300.CI13DESENTIDAD, " _
'        & "         CI2900.CI21CODPERSONA_REC "
'    SQL = SQL & " FROM  CI1300, CI2900 "
'    SQL = SQL & " WHERE CI2900.CI32CODTIPECON = 'S'" _
'              & "   AND CI2900.CI13CODENTIDAD <>'NA' " _
'              & "   AND CI2900.CI21CODPERSONA = 800638 " _
'              & "   AND CI2900.CI32CODTIPECON = CI1300.CI32CODTIPECON " _
'              & "   AND CI2900.CI13CODENTIDAD = CI1300.CI13CODENTIDAD " _
'              & "   AND CI1300.CI13FECFIVGENT IS NULL" _
'              & "   AND EXISTS " _
'              & "       (SELECT /*+ rule */ FA04NUMFACT " _
'              & "        FROM   FA0400 " _
'              & "        WHERE  FA04NUMFACREAL IS NULL " _
'              & "           AND FA0400.CI32CODTIPECON = CI1300.CI32CODTIPECON " _
'              & "           AND FA0400.CI13CODENTIDAD = CI1300.CI13CODENTIDAD " _
'              & "       AND FA0400.FA04FECFACTURA "
'              & "                   BETWEEN TO_DATE(?,'DD/MM/YYYY') " _
'              & "                   AND     LAST_DAY(TO_DATE(?,'DD/MM/YYYY'))" _
'              & "       )"
'    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
'    qryDatos(0) = strFecha
'    qryDatos(1) = strFecha
'    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    SQL = "SELECT   DISTINCT /*+ rule */ FA0400.CI32CODTIPECON, " _
        & "         FA0400.CI13CODENTIDAD, " _
        & "         CI1300.CI13DESENTIDAD, " _
        & "         FA0400.CI21CODPERSONA "
    SQL = SQL & " FROM  CI1300, FA0400 "
    SQL = SQL & " WHERE " _
              & "       CI1300.CI32CODTIPECON = FA0400.CI32CODTIPECON " _
              & "   AND CI1300.CI13CODENTIDAD = FA0400.CI13CODENTIDAD " _
              & "   AND FA04NUMFACREAL IS NULL " _
              & "   AND FA0400.CI32CODTIPECON = 'S' " _
              & "   AND FA0400.CI13CODENTIDAD <> 'NA' " _
              & "       AND FA0400.FA04FECFACTURA "
    SQL = SQL & "                   BETWEEN TO_DATE('" & strFecha & "','DD/MM/YYYY') " _
              & "                   AND     LAST_DAY(TO_DATE('" & strFecha & "','DD/MM/YYYY'))"
    SQL = SQL & " ORDER BY CI13DESENTIDAD"
    Set rsDatos = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsDatos.EOF
        i = i + 1
        ReDim Preserve arEntidad(1 To i)
        With arEntidad(i)
            .strTipoEcon = rsDatos!CI32CODTIPECON
            .strEntidad = rsDatos!CI13CODENTIDAD
            .strDesig = rsDatos!ci13desentidad
            .lngCodReco = rsDatos!CI21CODPERSONA
        End With
        rsDatos.MoveNext
    Loop
    rsDatos.Close
    'qryDatos.Close
    
    ' Se crean las facturas de RMN y PET
    proProgreso.Max = 100
    proProgreso.Value = 0
    fraProgreso.Caption = "Localizando Facturas RMN y PET"
    fraProgreso.Parent.Refresh
    
    If fPrepareFacturasINSALUD(strFecha, arEntidad()) Then
        
        'proProgreso.Value = 100
        fraProgreso.Caption = "Localizando Facturas"
        fraProgreso.Parent.Refresh
        
        If fCargaFacturasINSALUD(strFecha, arEntidad(), proProgreso, fraProgreso, arResumen()) Then
            
            
            'Call pActualizarNumerosDeFactura(arEntidad(), strFecha)
            
            'proProgreso.Value = 100
            fraProgreso.Caption = "Imprimiendo Facturas"
            fraProgreso.Parent.Refresh
            
            SQL = "select TRUNC(LAST_DAY(TO_DATE(?,'DD/MM/YYYY'))) as fecha FROM DUAL"
            Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
            qryDatos(0) = strFecha
            Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            strFecha = rsDatos!Fecha
            rsDatos.Close
            qryDatos.Close
            Call ImprimirFacturaInsalud(arEntidad(), strFecha)
            Call ImprimirResumenInsalud(arResumen(), strFecha)
        Else
            MsgBox "Mal"
        End If
    Else
        MsgBox "Se ha producido un error en la preparaci�n de facturas", vbCritical, "Error"
    End If
    
    proProgreso.Value = 0
    fraProgreso.Caption = ""
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdCrearRegsInsalud_Click()
'insert into fa3700
'(
'fa37codpac,
'ad01codasistenci,
'ad07codproceso,
'ad12codtipoasist,
'ci32codtipecon,
'ci13codentidad,
'fa37indfact,
'fa37fecinicio,
'fa37fecfin,
'fa37mes
')
'select  /*+ rule */
' fa37codpac_sequence.nextval,
' ad1100.ad01codasistenci,
' ad1100.ad07codproceso,
' 2,
' ad1100.ci32codtipecon,
' ad1100.ci13codentidad,
' 0,
' greatest(ad0100.ad01fecinicio,to_date('1/8/2000','dd/mm/yyyy')),
' least(ad0100.ad01fecfin,to_date('31/8/2000','dd/mm/yyyy')),
' to_date('31/8/2000','dd/mm/yyyy')
'From
' ad1100,
' ad0100
'Where
'  ad1100.ci32codtipecon ='S'
' and ad0100.ad01codasistenci = ad1100.ad01codasistenci
' and ad1100.ci13codentidad<>'NA'
' and ad0100.ad01fecinicio < to_date('1/9/2000','dd/mm/yyyy')
' and ad0100.ad01fecfin >= nvl(to_date('1/8/2000','dd/mm/yyyy'),sysdate)
' and ad1100.ad11fecfin is null
' and exists
'  (select pr04numactplan
'  From pr0400
'  Where pr0400.ad01codasistenci = ad1100.ad01codasistenci
'   and pr0400.ad07codproceso = ad1100.ad07codproceso
'   and pr0400.pr37codestado <6
'  )
End Sub

Private Sub cmdEstancias_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0117.rpt"
    crReport.WindowTitle = "Facturas por Estancias"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0423J.FA04FECFACTURA}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,01") & ")" _
                                 & " AND {FA0423J.FA04FECFACTURA}<DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")"
    'crReport.Formulas(0) = "TITULO = 'FACTURAS HOSPITALIZADAS DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"

    crReport.Action = 1
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdFinMes_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0125.rpt"
    crReport.WindowTitle = "Hospitalizados a Fin de Mes"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{AD1605J.AD16FECCAMBIO} < DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")" _
                                & " AND ({AD1605J.AD16FECFIN} >=DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")" _
                                & "       OR  ISNULL({AD1605J.AD16FECFIN}) " _
                                & "      )"
    crReport.Formulas(0) = "TITULO = 'PACIENTES HOSPITALIZADAS A FIN DEL MES DE " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"


    crReport.Action = 1
    Screen.MousePointer = vbDefault


End Sub

Private Sub cmdForfAmbu_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0120.rpt"
    crReport.WindowTitle = "Facturas por Forfait Ambulatorio"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0423J.FA04FECFACTURA}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,01") & ")" _
                                 & " AND {FA0423J.FA04FECFACTURA}<DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")"
    'crReport.Formulas(0) = "TITULO = 'FACTURAS HOSPITALIZADAS DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"

    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdForfCadera_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0118.rpt"
    crReport.WindowTitle = "Facturas por Forfait de Cataratas, Cadera y Rodilla"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0423J.FA04FECFACTURA}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,01") & ")" _
                                 & " AND {FA0423J.FA04FECFACTURA}<DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")"
    'crReport.Formulas(0) = "TITULO = 'FACTURAS HOSPITALIZADAS DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"

    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub


Private Sub cmdForfInterv_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0119.rpt"
    crReport.WindowTitle = "Facturas de Intervenciones"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0423J.FA04FECFACTURA}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,01") & ")" _
                                 & " AND {FA0423J.FA04FECFACTURA}<DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")"
    'crReport.Formulas(0) = "TITULO = 'FACTURAS HOSPITALIZADAS DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"

    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub


Private Sub cmdForfPruebasAmbu_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0121_2.rpt"
    crReport.WindowTitle = "Facturas por Forfait Pruebas Ambulatorios"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0423J.FA04FECFACTURA}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,01") & ")" _
                                 & " AND {FA0423J.FA04FECFACTURA}<DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")"
    'crReport.Formulas(0) = "TITULO = 'FACTURAS HOSPITALIZADAS DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"

    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub


Private Sub cmdForfPruebasHospi_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0121_1.rpt"
    crReport.WindowTitle = "Facturas por Forfait Pruebas Hospitalizados"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0423J.FA04FECFACTURA}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,01") & ")" _
                                 & " AND {FA0423J.FA04FECFACTURA}<DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")"
    'crReport.Formulas(0) = "TITULO = 'FACTURAS HOSPITALIZADAS DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"

    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub


Private Sub cmdGaranAmbu_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0123.rpt"
    crReport.WindowTitle = "Garant�as Ambulatorios"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0425J.FA04FECFACTURA}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,01") & ")" _
                                 & " AND {FA0425J.FA04FECFACTURA}<DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")"
    'crReport.Formulas(0) = "TITULO = 'FACTURAS HOSPITALIZADAS DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"


    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdGaranHospi_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0124.rpt"
    crReport.WindowTitle = "Garant�as Hospitalizados"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0425J.FA04FECFACTURA}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,01") & ")" _
                                 & " AND {FA0425J.FA04FECFACTURA}<DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")"
    'crReport.Formulas(0) = "TITULO = 'FACTURAS HOSPITALIZADAS DE OSASUNBIDEA CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"


    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub


Private Sub cmdProtesis_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0116.rpt"
    crReport.WindowTitle = "Pr�tesis"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = " {FA1503J.CI32CODTIPECON} = 'S' " _
                                & " AND {FA1503J.CI13CODENTIDAD}<>'NA' " _
                                & " AND {FA1503J.FR65FECHA}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,1") & ") " _
                                & " AND {FA1503J.FR65FECHA}< DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,1") & ")"
    
    
    crReport.Formulas(0) = "TITULO = 'PR�TESIS INSALUD " & Format(ssdcFechaFactura.Text, "mm/yyyy") & "'"
    crReport.Action = 1
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdRadioterapia_Click()
    Screen.MousePointer = vbHourglass
    
    Call pLimpiarReportFormulas
    
    crReport.ReportFileName = rptPath & "\FA0113.rpt"
    crReport.WindowTitle = "Radioterapia INSALUD"
    crReport.Destination = crptToWindow
    crReport.DiscardSavedData = True
    crReport.SelectionFormula = "{FA0306J.PR04FECFINACT}>=DATE(" & Format(ssdcFechaFactura.Text, "yyyy,mm,01") & ")" _
                                 & " AND {FA0306J.PR04FECFINACT}<DATE(" & Format(DateAdd("m", 1, CVDate(ssdcFechaFactura.Text)), "yyyy,mm,01") & ")"
    crReport.Formulas(0) = "TITULO = 'RADIOTERAPIA DE INSALUD CORRESPONDIENTES A " & UCase(Format(ssdcFechaFactura.Text, "mmmm - yyyy")) & "'"

    crReport.Action = 1
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub



Private Sub Form_Load()
     rptPath = App.Path
    Set crReport = frmFacturaINSALUD.crReport1
    crReport.WindowBorderStyle = crptFixedDouble
    crReport.WindowState = crptMaximized
    crReport.Connect = objApp.rdoConnect.Connect
End Sub


