Attribute VB_Name = "modFacturaOsasunbidea"
Option Explicit

Public Function fEjecutarFichOsasun(strFecha$, _
                                    blnCrearPacHospi As Boolean, _
                                    blnCrearPacAmbu As Boolean, _
                                    blnCrearConcept As Boolean, _
                                    blnCrearA�adirDiag As Boolean, _
                                    blnCrearCrearDiag As Boolean, _
                                    strErrorMsg$, _
                                    txtMsg As TextBox) As Boolean
    
    Dim intRespHosp As Integer
    Dim intRespAmbu As Integer
        
    ' Se eliminan los registros de Hospitalizados
    If blnCrearPacHospi Then
        intRespHosp = fFich_BorraPac(strFecha, "HO", strErrorMsg, txtMsg)
        If intRespHosp = vbCancel Then Exit Function
    End If
    
    ' Se eliminan los registros de Ambulatorios
    If blnCrearPacAmbu Then
        intRespAmbu = fFich_BorraPac(strFecha, "AM", strErrorMsg, txtMsg)
        If intRespAmbu = vbCancel Then Exit Function
    End If
    
    ' Crea el fichero de pacientes hospitalizados
    If intRespHosp = vbYes Then
        If Not fFich_CreaPac(strFecha, "HO", strErrorMsg, txtMsg) Then
            Exit Function
        End If
    End If
    
    ' Crea el fichero de pacientes ambulatorios
    If intRespAmbu = vbYes Then
        If Not fFich_CreaPac(strFecha, "AM", strErrorMsg, txtMsg) Then
            Exit Function
        End If
    End If
    
    ' Crea el fichero de conceptos
    If blnCrearConcept Then
        If intRespHosp = vbYes Then
            ' Se crean los conceptos de los pacientes hospitalizados
            If Not fFich_CreaConcep(strFecha, "HO", strErrorMsg, txtMsg) Then Exit Function
            
            If Not fFich_ConceptInsertarLineasCero(strFecha, "HO", strErrorMsg, txtMsg) Then Exit Function
            
            If Not fFich_ConcepActualizarConcepto("86", strFecha, "Estancias Primer Tramo", "ES", "000001", strErrorMsg, txtMsg) Then Exit Function
            If Not fFich_ConcepActualizarConcepto("87", strFecha, "Estancias Segundo Tramo", "ES", "000002", strErrorMsg, txtMsg) Then Exit Function
            If Not fFich_ConcepActualizarConcepto("88", strFecha, "Estancias Resto", "ES", "000009", strErrorMsg, txtMsg) Then Exit Function
            If Not fFich_ConcepActualizarConcepto("92", strFecha, "Estancias en UCI", "ES", "000021", strErrorMsg, txtMsg) Then Exit Function
            If Not fFich_ConcepActualizarConcepto("94", strFecha, "Estancias Unidad Coronaria", "ES", "000041", strErrorMsg, txtMsg) Then Exit Function
            If Not fFich_ConcepActualizarConcepto("89", strFecha, "Estancias Primer tramo Quir�rgico", "ES", "000011", strErrorMsg, txtMsg) Then Exit Function
            If Not fFich_ConcepActualizarConcepto("90", strFecha, "Estancias Segundo tramo Quir�rgico", "ES", "000012", strErrorMsg, txtMsg) Then Exit Function
            If Not fFich_ConcepActualizarConcepto("91", strFecha, "Estancias Resto Quir�rgico", "ES", "000019", strErrorMsg, txtMsg) Then Exit Function
            
            If Not fFich_ConcepActualizaCantidad(strFecha, "HO", strErrorMsg, txtMsg) Then Exit Function
            If Not fFich_ConcepActualizaImporte(strFecha, "HO", strErrorMsg, txtMsg) Then Exit Function
                        
            If Not fFich_ConcepActualizaFecha(strFecha, "HO", strErrorMsg, txtMsg) Then Exit Function
            
            If Not fFich_ConcepInsertQuirur(strFecha, strErrorMsg, txtMsg) Then Exit Function

        End If
        If intRespAmbu = vbYes Then
            ' Se crean los conceptos de los pacientes ambulatorios
            If Not fFich_CreaConcep(strFecha, "AM", strErrorMsg, txtMsg) Then Exit Function
            If Not fFich_ConceptInsertarLineasCero(strFecha, "AM", strErrorMsg, txtMsg) Then Exit Function
            If Not fFich_ConcepActualizaCantidad(strFecha, "AM", strErrorMsg, txtMsg) Then Exit Function
            If Not fFich_ConcepActualizaFecha(strFecha, "AM", strErrorMsg, txtMsg) Then Exit Function
        End If
    End If
    
    ' A�ade los diagn�sticos al fichero de conceptos
    If blnCrearA�adirDiag Then
        If intRespHosp = vbYes Then
            If Not fFich_ConcepActualizarInsertDiag(strFecha, "HO", strErrorMsg, txtMsg) Then Exit Function
        End If
        If intRespAmbu = vbYes Then
            If Not fFich_ConcepActualizarInsertDiag(strFecha, "AM", strErrorMsg, txtMsg) Then Exit Function
        End If
        
        
    End If
    
    ' Crea el fichero de diagn�sticos
    If blnCrearCrearDiag Then
        If intRespHosp = vbYes Then
            If Not fFich_DiagInsertDiag(strFecha, "HO", strErrorMsg, txtMsg) Then Exit Function
        End If
        If intRespAmbu = vbYes Then
            If Not fFich_DiagInsertDiag(strFecha, "AM", strErrorMsg, txtMsg) Then Exit Function
        End If
    End If

    fEjecutarFichOsasun = True
End Function
Public Function fFich_BorraPac(strFechaFact$, strTipoAsist$, strErrorMsg$, txtMsg As TextBox) As Integer
    ' Se crea el fichero de hospitalizados del mes
    
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim intResp As Integer
    Dim blnBorrado As Boolean
    
    On Error Resume Next
    
    ' Primero se mira si existen registros
    SQL = "SELECT count(*) as cuenta" _
        & " FROM    FA5200 " _
        & " WHERE   FA52FECPROCESO = ? " _
        & "     AND FA52TIPOASISTENCIA = ? "
        
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "yyyymm")
    qryDatos(1) = strTipoAsist
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        If rsDatos!cuenta > 0 Then
            intResp = MsgBox("Existen " & rsDatos!cuenta _
                    & " registros de pacientes tipo " & strTipoAsist & " del periodo " _
                    & Format$(strFechaFact, "mm/yyyy") _
                    & ". Si contin�a adelante se borrar�n esos datos para volver a generarse. " _
                    & " �Desea seguir adelante con la operaci�n?" _
                    , vbQuestion + vbYesNoCancel, "Creaci�n del fichero de Pacientes")
                    blnBorrado = True
        Else
            intResp = vbYes
        End If
    Else
        intResp = vbYes
    End If

    rsDatos.Close
    qryDatos.Close
    
    Select Case intResp
        Case vbYes
            If blnBorrado Then
                ' Primero se borran los registros de conceptos de la FA5300
                SQL = "DELETE FROM FA5300" _
                    & " WHERE   (FA52FECPROCESO, FA52NUMREG) IN" _
                    & "         (SELECT     FA52FECPROCESO, FA52NUMREG  " _
                    & "          FROM       FA5200 " _
                    & "          WHERE      FA52FECPROCESO = ? " _
                    & "                     AND FA52TIPOASISTENCIA = ? " _
                    & "         )"
                Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
                qryDatos(0) = Format$(strFechaFact, "yyyymm")
                qryDatos(1) = strTipoAsist
                qryDatos.Execute
                If Err <> 0 Then
                    Call pMensaje("Borrado Conceptos " & strTipoAsist & ": ERROR", txtMsg)
                    strErrorMsg = Error
                    Exit Function
                Else
                    Call pMensaje("Borrado Conceptos " & strTipoAsist & ": " & qryDatos.RowsAffected & " regs.", txtMsg)
                End If
                qryDatos.Close
                ' Se borran los registros
                SQL = "DELETE FROM FA5200" _
                    & " WHERE   FA52FECPROCESO = ? " _
                    & "     AND FA52TIPOASISTENCIA = ? "
                Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
                qryDatos(0) = Format$(strFechaFact, "yyyymm")
                qryDatos(1) = strTipoAsist
                qryDatos.Execute
                If Err <> 0 Then
                    Call pMensaje("Borrado " & strTipoAsist & ": ERROR", txtMsg)
                    strErrorMsg = Error
                    Exit Function
                Else
                    Call pMensaje("Pac. " & strTipoAsist & " Borrado: " & qryDatos.RowsAffected & " regs.", txtMsg)
                End If
            End If
        
'        Case vbNo
'            ' Se informa de que se anule la ejecuci�n de esta secci�n, pero se contin�e con la ejecuci�n global
'            fFich_BorraPac = True
'        Case vbCancel
'            ' Se informa de que se anule la ejecuci�n global
'            fFich_CreaPac = False
    End Select
    fFich_BorraPac = intResp
End Function
Private Function fFich_ConcepActualizarConcepto(strFA46Cod$, strFecha$, strTexto$, strConcepto$, strCodigo$, strErrorMsg$, txtMsg As TextBox) As Boolean
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    
    
    On Error Resume Next
    
    SQL = "UPDATE FA5300" _
        & " SET FA46CODDESEXTERNO = ?, " _
        & "     FA53CONCEPTO = RPAD(?,2), " _
        & "     FA53CODIGO = RPAD(?,6) " _
        & " WHERE   ltrim(rtrim(UPPER(FA53DESCRIP))) = ? " _
        & "     AND FA52FECPROCESO = ? " _

        
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = strFA46Cod
    qryDatos(1) = strConcepto
    qryDatos(2) = strCodigo
    qryDatos(3) = UCase(strTexto)
    qryDatos(4) = Format$(strFecha, "yyyymm")
    qryDatos.Execute
        
    If Err <> 0 Then
        Call pMensaje("Actualizar Concepto '" & strTexto & "': ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Actualizar Concepto '" & strTexto & "': " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close
        
    fFich_ConcepActualizarConcepto = True
    

End Function

Private Function fFich_ConcepActualizarInsertDiag(strFechaFact$, strTipoAsist$, strErrorMsg$, txtMsg As TextBox) As Boolean
    ' Se insertan los diagn�sticos en la tabla de conceptos
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    

    On Error Resume Next
    SQL = " INSERT INTO FA5300 " _
        & " (" _
        & " FA53NUMCONCEPTO,        " _
        & " FA52FECPROCESO,         " _
        & " FA52NUMREG ,            " _
        & " FA53CODIGO ,            " _
        & " FA53CONCEPTO,           " _
        & " FA53DESCRIP,            " _
        & " FA53CANTIDAD,           " _
        & " FA04CODFACT,            " _
        & " FA16NUMLINEA,           " _
        & " FA46CODDESEXTERNO,      " _
        & " FA53IMPORTE ,           " _
        & " FA53IMPORTEGRUPO        " _
        & " )"
    SQL = SQL & " SELECT FA53NUMCONCEPTO_SEQUENCE.nextval," _
        & "             FA5200.FA52FECPROCESO, " _
        & "             FA5200.FA52NUMREG, " _
        & "             RPAD(NVL(AD4000.AD39CODIGO,' '),6), " _
        & "             'DG', " _
        & "             RPAD(NVL(AD3900.AD39DESCODIGO,' '),36), " _
        & "             LPAD(DECODE(AD4000.AD40TIPODIAGPROC, " _
        & "                         'P',1, " _
        & "                         'S',0, " _
        & "                         0),4)," _
        & "             FA5200.FA04CODFACT, " _
        & "             0, " _
        & "             456, " _
        & "             LPAD(' ',10), " _
        & "             LPAD(' ',10) "
    SQL = SQL & " FROM  AD3900, " _
        & "             AD4000, " _
        & "             FA0400, " _
        & "             FA5200  "
    SQL = SQL & " WHERE     FA0400.FA04CODFACT = FA5200.FA04CODFACT " _
        & "             AND AD4000.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
        & "             AND AD4000.AD07CODPROCESO = FA0400.AD07CODPROCESO " _
        & "             AND AD3900.AD39CODIGO = AD4000.AD39CODIGO " _
        & "             AND AD3900.AD39TIPOCODIGO ='D' " _
        & "             AND FA5200.FA52FECPROCESO = ? " _
        & "             AND FA5200.FA52TIPOASISTENCIA = ? "
        
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "yyyymm")
    qryDatos(1) = strTipoAsist
    qryDatos.Execute
        
    If Err <> 0 Then
        Call pMensaje("Concepto Diag. " & strTipoAsist & ": ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Concepto Diag. " & strTipoAsist & ": " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close
        
    fFich_ConcepActualizarInsertDiag = True

End Function

Private Function fFich_ConcepInsertQuirur(strFecha$, strErrorMsg$, txtMsg As TextBox) As Boolean
    ' Se insertan las intervenciones de estancias quir�rgicas con precio 0
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    
    
    On Error Resume Next
    SQL = " INSERT INTO FA5300 " _
        & " (" _
        & " FA53NUMCONCEPTO,        " _
        & " FA52FECPROCESO,         " _
        & " FA52NUMREG ,            " _
        & " FA53CODIGO ,            " _
        & " FA53CONCEPTO,           " _
        & " FA53DESCRIP,            " _
        & " FA53CANTIDAD,           " _
        & " FA53FECHACONCEPTO,      " _
        & " FA53IMPORTE,            " _
        & " FA53IMPORTEGRUPO,       " _
        & " FA04CODFACT,            " _
        & " FA16NUMLINEA,           " _
        & " FA46CODDESEXTERNO       " _
        & " )"
    SQL = SQL & " SELECT FA53NUMCONCEPTO_SEQUENCE.nextval," _
        & "             FA5200.FA52FECPROCESO, " _
        & "             FA5200.FA52NUMREG, " _
        & "             RPAD(NVL(TO_CHAR(FA0300.FA05CODCATEG),' '),6), " _
        & "             'IQ'," _
        & "             RPAD(NVL(FA0500.FA05DESIG, ' '),36), " _
        & "             LPAD(NVL(FA0300.FA03CANTFACT,1),4), " _
        & "             TO_CHAR(FA0300.FA03FECHA,'YYYYMMDD'), " _
        & "             lpad('0',10), " _
        & "             lpad('0',10), " _
        & "             FA5200.FA04CODFACT, " _
        & "             FA0300.FA16NUMLINEA, " _
        & "             153 "
    SQL = SQL & "   FROM    FA0300, FA0500, FA5200 "
    SQL = SQL & "   WHERE   FA0500.FA05CODCATEG = FA0300.FA05CODCATEG " _
        & "             AND FA0300.FA04NUMFACT = FA5200.FA04CODFACT " _
        & "             AND FA0500.FA08CODGRUPO = 6" _
        & "             AND FA5200.FA04CODFACT IN " _
        & "             (SELECT FA04CODFACT " _
        & "              FROM   FA5300 " _
        & "              WHERE  FA52FECPROCESO = ? " _
        & "                 AND FA53CONCEPTO ='ES' " _
        & "             ) "
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFecha, "yyyymm")
    qryDatos.Execute
        
    If Err <> 0 Then
        Call pMensaje("Insert Quir�rg.: ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Insert Quir�rg.: " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close


    fFich_ConcepInsertQuirur = True
End Function

Private Function fFich_ConceptInsertarLineasCero(strFechaFact$, strTipoAsist$, strErrorMsg$, txtMsg As TextBox) As Boolean
    ' Se toman los pacientes de la tabla FA5200 y se mira si tienen registros en la tabla
    ' FA5300. Si no lo tienen, se insertan primero lo sque tengan alguna revisi�n o
    ' exploraci�n.
    ' En el resto de casos se introducir�n todas las l�neas de factura
    
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    
    
    On Error Resume Next
    
    ' 1� Se insertan las revisiones
    SQL = " INSERT INTO FA5300 " _
        & " (" _
        & " FA53NUMCONCEPTO,        " _
        & " FA52FECPROCESO,         " _
        & " FA52NUMREG ,            " _
        & " FA53CODIGO ,            " _
        & " FA53CONCEPTO,           " _
        & " FA53DESCRIP,            " _
        & " FA53CANTIDAD,           " _
        & " FA53IMPORTE,            " _
        & " FA53IMPORTEGRUPO,       " _
        & " FA04CODFACT,            " _
        & " FA16NUMLINEA,           " _
        & " FA46CODDESEXTERNO       " _
        & " )"
    SQL = SQL & " SELECT FA53NUMCONCEPTO_SEQUENCE.nextval," _
        & "             FA5200.FA52FECPROCESO, " _
        & "             FA5200.FA52NUMREG, " _
        & "             RPAD(NVL(NVL(FA4600.FA46CODDIAG, TO_CHAR(FA0500.FA05CODCATEG)),' '),6), " _
        & "             NVL(FA4600.FA46CONCEPTO,'++')," _
        & "             RPAD(NVL(NVL(FA4600.FA46DESCRIP,FA0500.FA05DESIG), ' '),36), " _
        & "             LPAD(NVL('1',1),4), " _
        & "             lpad('0',10), " _
        & "             lpad('0',10), " _
        & "             FA5200.FA04CODFACT, " _
        & "             FA0300.FA16NUMLINEA, " _
        & "             FA4600.FA46CODDESEXTERNO "
    SQL = SQL & "   FROM    FA4600, FA1500, FA0300, FA0500, FA5200 "
    SQL = SQL & "   WHERE   FA0500.FA05CODCATEG = FA0300.FA05CODCATEG " _
        & "             AND FA0300.FA04NUMFACT = FA5200.FA04CODFACT " _
        & "             AND FA1500.FA15CODATRIB (+)= FA0300.FA15CODATRIB " _
        & "             AND FA4600.FA46CODDESEXTERNO (+)= FA1500.FA46CODDESEXTERNO " _
        & "             AND FA0500.FA08CODGRUPO = 7" _
        & "             AND FA0500.FA05CODORIGC2 = 0 " _
        & "             AND FA0500.FA05CODORIGC3 IN(1,2,4,7) " _
        & "             AND NOT EXISTS " _
        & "             (SELECT FA04CODFACT " _
        & "              FROM   FA5300 " _
        & "              WHERE  FA5300.FA52FECPROCESO = FA5200.FA52FECPROCESO " _
        & "                 AND FA5300.FA52NUMREG = FA5200.FA52NUMREG" _
        & "             ) " _
        & "             AND FA5200.FA52FECPROCESO = ? " _
        & "             AND FA52TIPOASISTENCIA = ? "
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "yyyymm")
    qryDatos(1) = strTipoAsist
    qryDatos.Execute
        
    If Err <> 0 Then
        Call pMensaje("Concept Lineas 0 Rev. " & strTipoAsist & ": ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Concept Lineas 0 Rev. " & strTipoAsist & ": " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close

    ' 2� Para el resto se insertan todas las l�neas de factura
    SQL = " INSERT INTO FA5300 " _
        & " (" _
        & " FA53NUMCONCEPTO,        " _
        & " FA52FECPROCESO,         " _
        & " FA52NUMREG ,            " _
        & " FA53CODIGO ,            " _
        & " FA53CONCEPTO,           " _
        & " FA53DESCRIP,            " _
        & " FA53CANTIDAD,           " _
        & " FA53IMPORTE,            " _
        & " FA53IMPORTEGRUPO,       " _
        & " FA04CODFACT,            " _
        & " FA16NUMLINEA,           " _
        & " FA46CODDESEXTERNO       " _
        & " )"
    SQL = SQL & " SELECT FA53NUMCONCEPTO_SEQUENCE.nextval," _
        & "             FA5200.FA52FECPROCESO, " _
        & "             FA5200.FA52NUMREG, " _
        & "             RPAD(NVL(NVL(FA4600.FA46CODDIAG,FA1600.FA14CODNODOCONC),' '),6), " _
        & "             NVL(FA4600.FA46CONCEPTO,'++')," _
        & "             RPAD(NVL(NVL(FA4600.FA46DESCRIP,FA1600.FA16DESCRIP), ' '),36), " _
        & "             LPAD('1',4), " _
        & "             lpad('0',10), " _
        & "             lpad('0',10), " _
        & "             FA5200.FA04CODFACT, " _
        & "             FA1600.FA16NUMLINEA, " _
        & "             FA4600.FA46CODDESEXTERNO "
    SQL = SQL & "   FROM    FA4600, FA1500, FA1600, FA5200 "
    SQL = SQL & "   WHERE   FA1600.FA04CODFACT = FA5200.FA04CODFACT " _
        & "             AND FA1500.FA15CODATRIB (+)= FA1600.FA15CODATRIB " _
        & "             AND FA4600.FA46CODDESEXTERNO (+)= FA1500.FA46CODDESEXTERNO " _
        & "             AND NOT EXISTS " _
        & "             (SELECT FA04CODFACT " _
        & "              FROM   FA5300 " _
        & "              WHERE  FA5300.FA52FECPROCESO = FA5200.FA52FECPROCESO " _
        & "                 AND FA5300.FA52NUMREG = FA5200.FA52NUMREG" _
        & "             ) " _
        & "             AND FA5200.FA52FECPROCESO = ? " _
        & "             AND FA52TIPOASISTENCIA = ? "
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "yyyymm")
    qryDatos(1) = strTipoAsist
    qryDatos.Execute
        
    If Err <> 0 Then
        Call pMensaje("Concept Lineas 0 Varios " & strTipoAsist & ": ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Concept Lineas 0 Varios " & strTipoAsist & ": " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close
    
    
    ' 3� Las facturas sin l�nea de factura se envi�n como revisiones
    SQL = " INSERT INTO FA5300 " _
        & " (" _
        & " FA53NUMCONCEPTO,        " _
        & " FA52FECPROCESO,         " _
        & " FA52NUMREG ,            " _
        & " FA53CODIGO ,            " _
        & " FA53CONCEPTO,           " _
        & " FA53DESCRIP,            " _
        & " FA53CANTIDAD,           " _
        & " FA53IMPORTE,            " _
        & " FA53IMPORTEGRUPO,       " _
        & " FA04CODFACT,            " _
        & " FA16NUMLINEA,           " _
        & " FA46CODDESEXTERNO       " _
        & " )"
    SQL = SQL & " SELECT FA53NUMCONCEPTO_SEQUENCE.nextval," _
        & "             FA5200.FA52FECPROCESO, " _
        & "             FA5200.FA52NUMREG, " _
        & "             '000002', " _
        & "             'CO'," _
        & "             RPAD('REVISION',36), " _
        & "             LPAD('1',4), " _
        & "             lpad('0',10), " _
        & "             lpad('0',10), " _
        & "             FA5200.FA04CODFACT, " _
        & "             0, " _
        & "             103 "
    SQL = SQL & "   FROM    FA5200 "
    SQL = SQL & "   WHERE   NOT EXISTS " _
        & "             (SELECT FA04CODFACT " _
        & "              FROM   FA5300 " _
        & "              WHERE  FA5300.FA52FECPROCESO = FA5200.FA52FECPROCESO " _
        & "                 AND FA5300.FA52NUMREG = FA5200.FA52NUMREG" _
        & "             ) " _
        & "             AND FA5200.FA52FECPROCESO = ? " _
        & "             AND FA52TIPOASISTENCIA = ? "
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "yyyymm")
    qryDatos(1) = strTipoAsist
    qryDatos.Execute
        
    If Err <> 0 Then
        Call pMensaje("Concept Lineas 0 Resto " & strTipoAsist & ": ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Concept Lineas 0 Resto " & strTipoAsist & ": " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close

    fFich_ConceptInsertarLineasCero = True
    
    

End Function

Private Function fFich_CreaPac(strFechaFact$, strTipoAsist$, strErrorMsg$, txtMsg As TextBox) As Boolean
     'Se generan los registros
    Select Case strTipoAsist
        Case "HO"
             'Hospitalizados
            fFich_CreaPac = fFich_CreaPac_Hospi(strFechaFact, strErrorMsg, txtMsg)
        Case "AM"
             'Ambulatorios
            fFich_CreaPac = fFich_CreaPac_Ambu(strFechaFact, strErrorMsg, txtMsg)
    End Select

End Function


Private Function fFich_CreaConcep(strFechaFact$, strTipoAsist$, strErrorMsg$, txtMsg As TextBox) As Boolean
     'Se generan los registros de Conceptos

    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    
    On Error Resume Next
    
    SQL = " INSERT INTO FA5300 " _
        & " (" _
        & " FA53NUMCONCEPTO,        " _
        & " FA52FECPROCESO,         " _
        & " FA52NUMREG ,            " _
        & " FA46CODDESEXTERNO ,     " _
        & " FA53CODIGO ,            " _
        & " FA53CONCEPTO,           " _
        & " FA53DESCRIP,            " _
        & " FA53CANTIDAD,           " _
        & " FA53IMPORTE ,           " _
        & " FA53IMPORTEGRUPO ,      " _
        & " FA04CODFACT,            " _
        & " FA16NUMLINEA           " _
        & " )"
    SQL = SQL & " SELECT FA53NUMCONCEPTO_SEQUENCE.nextval," _
        & " FA5200.FA52FECPROCESO, " _
        & " FA5200.FA52NUMREG, " _
        & " NVL(FA1500.FA46CODDESEXTERNO,0), " _
        & " RPAD(NVL(FA4600.FA46CODDIAG,NVL(FA1500.FA15CODFIN,'000000')),6)  , " _
        & " RPAD(NVL(FA4600.FA46CONCEPTO,'++'),2), " _
        & " RPAD(NVL(NVL(FA4600.FA46DESCRIP,FA1600.FA16DESCRIP),' '),36), " _
        & " LPAD(1,4), " _
        & " LPAD(FA1600.FA16IMPORTE,10), " _
        & " LPAD(FA1600.FA16IMPORTE,10), " _
        & " FA1600.FA04CODFACT, " _
        & " FA1600.FA16NUMLINEA "
    SQL = SQL & " FROM " _
        & "         FA1500, " _
        & "         FA4600, " _
        & "         FA1600, " _
        & "         FA5200 "
    SQL = SQL & " WHERE " _
        & "             FA1600.FA04CODFACT = FA5200.FA04CODFACT " _
        & "         AND FA1500.FA15CODATRIB (+) = FA1600.FA15CODATRIB " _
        & "         AND FA4600.FA46CODDESEXTERNO (+) = FA1500.FA46CODDESEXTERNO " _
        & "         AND (FA1600.FA16IMPORTE >0 OR FA1600.FA16FRANQUICIA >0)" _
        & "         AND FA5200.FA52FECPROCESO = ? " _
        & "         AND FA5200.FA52TIPOASISTENCIA = ? " _
        & "         AND TO_NUMBER(FA5200.FA52CANTFACT) >0 "
                    
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "yyyymm")
    qryDatos(1) = strTipoAsist
    qryDatos.Execute
    If Err <> 0 Then
        Call pMensaje("Conceptos " & strTipoAsist & ": ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Conceptos " & strTipoAsist & ": " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close
    
        
    ' Se introducen los conceptos de las facturas con valor 0
    
    
    fFich_CreaConcep = True
End Function

Private Function fFich_CreaPac_Ambu(strFechaFact$, strErrorMsg$, txtMsg As TextBox) As Boolean
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim SQL As String
    Dim intNumReg As Integer
    
    On Error Resume Next
    
    ' Primero se obtiene la siguiente posici�n en la tabla FA5200. Es un contador por Fecha de Proceso
    SQL = "SELECT NVL(MAX(TO_NUMBER(FA52NUMREG)),0)  AS FA52NUMREG    " _
        & " FROM    FA5200 " _
        & " WHERE   FA52FECPROCESO = ?"
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "YYYYMM")
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        intNumReg = rsDatos!FA52NUMREG
    End If
    rsDatos.Close
    qryDatos.Close
       
    Call pMensaje("AM Siguiente Reg: " & intNumReg + 1, txtMsg)
    

    
    ' Pacientes ambulatorios con consultas

    SQL = "INSERT INTO FA5200" _
         & "( " _
            & " FA52NUMSEGSOC, " _
            & " FA52FECINICIO, " _
            & " FA52TIPOASISTENCIA, " _
            & " FA52APEL1, " _
            & " FA52APEL2, " _
            & " FA52NOMB, " _
            & " FA52CITE_CIP, " _
            & " FA52FECHNAC, " _
            & " FA52SEXO, " _
            & " FA52CODPOSTAL, " _
            & " FA52FECCONSULTA, " _
            & " FA52TIPOCONSULTA, " _
            & " FA52CODDPTO, " _
            & " FA52CANTFACT, " _
            & " FA52NUMREG, " _
            & " FA52FECPROCESO, " _
            & " FA52EXPINF, " _
            & " FA04CODFACT " _
        & ")"
    
    SQL = SQL & "SELECT /*+ RULE */" _
 & "    RPAD(NVL(NVL(FA2500.FA25NUMSEGSOC,CI2200.CI22NUMSEGSOC),' '),10) AS FA25NUMSEGSOC," _
 & "    NVL(TO_CHAR(AD0100.AD01FECINICIO,'YYYYMMDD'),'        ') AS AD01FECINICIO," _
 & "    'AM' AS TIPOASISTENCIA," _
 & "    RPAD(NVL(NVL(FA2500.FA25APEL1,CI2200.CI22PRIAPEL),' '),18) AS FA25APEL1," _
 & "    RPAD(NVL(NVL(FA2500.FA25APEL2,CI2200.CI22SEGAPEL),' '),18) AS FA25APEL2," _
 & "    RPAD(NVL(NVL(FA2500.FA25NOMB,CI2200.CI22NOMBRE),' '),14) AS FA25NOMB," _
 & "    RPAD(NVL(FA2500.FA25IDCLIENTE,' '),23) AS FA25IDCLIENTE," _
 & "    RPAD(NVL(TO_CHAR(NVL(FA2500.FA25FECHNAC,CI2200.CI22FECNACIM),'DDMMYYY'),' '),7) AS FA25FECHNAC," _
 & "    RPAD(NVL(TO_CHAR(NVL(FA2500.FA25SEXO,CI2200.CI30CODSEXO)),' '),1) AS FA25SEXO," _
 & "    RPAD(NVL(TO_CHAR(FA2500.FA25CODPOSTAL),' '),5) AS FA25CODPOSTAL," _
 & "    RPAD(NVL(TO_CHAR(FA0305.FA03FECHA,'DDMMYYY'),' '),7) AS FECHACONSULTA," _
 & "    NVL(FA0305.TIPOCONSULTA,1) AS TIPOCONSULTA," _
 & "    RPAD(NVL(TO_CHAR(AD0500.AD02CODDPTO),' '),3) AS AD02CODDPTO," _
 & "    LPAD(FA0400.FA04CANTFACT,10) AS FA04CANTFACT," _
 & "    LPAD(ROWNUM + " & intNumReg & ",4,'0') AS NUMREG," _
 & "    ? AS FECPROCESO," _
 & "    RPAD(NVL(FA2500.FA25EXPINF,' '),14) AS FA25EXPINF, " _
 & "    FA0400.FA04CODFACT "
SQL = SQL & " FROM " _
 & "    AD1100, " _
 & "    FA0305," _
 & "    AD0100," _
 & "    AD0500," _
 & "    FA2501 FA2500," _
 & "    CI2200," _
 & "    FA0400"
 SQL = SQL & " WHERE" _
 & "        AD0100.AD01CODASISTENCI = FA0400.AD01CODASISTENCI" _
 & "    AND AD0500.AD01CODASISTENCI = FA0400.AD01CODASISTENCI" _
 & "    AND AD0500.AD07CODPROCESO = FA0400.AD07CODPROCESO" _
 & "    AND AD0500.AD05FECFINRESPON IS NULL" _
 & "    AND FA2500.AD01CODASISTENCI (+)= FA0400.AD01CODASISTENCI" _
 & "    AND FA2500.AD07CODPROCESO (+)= FA0400.AD07CODPROCESO" _
 & "    AND FA0305.FA04CODFACT  = FA0400.FA04CODFACT" _
 & "    AND CI2200.CI22NUMHISTORIA = AD0100.CI22NUMHISTORIA" _
 & "    AND FA0400.FA04NUMFACREAL IS NULL" _
 & "    AND AD1100.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
 & "    AND AD1100.AD07CODPROCESO = FA0400.AD07CODPROCESO " _
 & "    AND AD1100.AD11FECFIN IS NULL " _
 & "    AND (AD1100.AD11INDVOLANTE = 0 OR AD1100.AD11INDVOLANTE IS NULL) " _

 SQL = SQL & "    AND FA0400.FA04CODFACT IN" _
 & "        (SELECT /*+ RULE */ FA04CODFACT" _
 & "        FROM    " _
 & "            FA0419J " _
 & "        WHERE" _
 & "            TIPOASIST = 2" _
 & "            AND CI32CODTIPECON = 'S'" _
 & "            AND CI13CODENTIDAD ='NA'" _
 & "            AND FA04FECFACTURA = TO_DATE(?,'DD/MM/YYYY') " _
 & "        )"
 SQL = SQL & "   AND EXISTS " _
 & "        (SELECT /*+ RULE */ AD01CODASISTENCI " _
 & "         FROM   AD1800 " _
 & "         WHERE      AD1800.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
 & "            AND     AD1800.AD07CODPROCESO = FA0400.AD07CODPROCESO " _
 & "            AND     AD1800.AD19CODTIPODOCUM = 1 " _
 & "        )"
 SQL = SQL & "   AND EXISTS " _
 & "        (SELECT /*+ RULE */ FA04CODFACT " _
 & "         FROM   FA1600 " _
 & "         WHERE      FA1600.FA04CODFACT = FA0400.FA04CODFACT " _
 & "        )"
 

'SQL = SQL & "GROUP BY " _
 & "    TO_CHAR(AD0100.AD01FECINICIO,'YYYYMMDD')," _
 & "    RPAD(NVL(FA2500.FA25APEL1,CI2200.CI22PRIAPEL),18)," _
 & "    RPAD(NVL(FA2500.FA25APEL2,CI2200.CI22SEGAPEL),18) ," _
 & "    RPAD(NVL(FA2500.FA25NOMB,CI2200.CI22NOMBRE),14) ," _
 & "    RPAD(TO_CHAR(NVL(FA2500.FA25FECHNAC,CI2200.CI22FECNACIM),'DDMMYYY'),7) ," _
 & "    RPAD(NVL(FA2500.FA25SEXO,CI2200.CI30CODSEXO),1) ," _
 & "    RPAD(FA2500.FA25CODPOSTAL,5) ," _
 & "    RPAD(TO_CHAR(FA0305.FA03FECHA,'DDMMYYY'),7) ," _
 & "    NVL(FA0305.TIPOCONSULTA,1) ," _
 & "    RPAD(AD0500.AD02CODDPTO,3) ," _
 & "    LPAD(FA0400.FA04CANTFACT,10) ," _
 & "    FA0400.FA04CODFACT, " _
 & "    LPAD(ROWNUM + " & intNumReg & ",4,'0') "

                    
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "YYYYMM")
    qryDatos(1) = Format$(strFechaFact, "DD/MM/YYYY")
    qryDatos.Execute
    
    If Err <> 0 Then
        Call pMensaje("Pac. AM Consultas: ERROR", txtMsg)
        strErrorMsg = Error
        Exit Function
    End If
    
    Call pMensaje("Pac. AM Consultas: " & qryDatos.RowsAffected & " regs.", txtMsg)
    'intNumReg = intNumReg + qryDatos.RowsAffected
    ' Primero se obtiene la siguiente posici�n en la tabla FA5200. Es un contador por Fecha de Proceso
    SQL = "SELECT NVL(MAX(TO_NUMBER(FA52NUMREG)),0)  AS FA52NUMREG    " _
        & " FROM    FA5200 " _
        & " WHERE   FA52FECPROCESO = ?"
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "YYYYMM")
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        intNumReg = rsDatos!FA52NUMREG
    End If
    rsDatos.Close
    qryDatos.Close
       
    Call pMensaje("AM Siguiente Reg: " & intNumReg + 1, txtMsg)

' Pacientes ambulatorios sin consultas
    SQL = "INSERT INTO FA5200" _
         & "( " _
            & " FA52NUMSEGSOC, " _
            & " FA52FECINICIO, " _
            & " FA52TIPOASISTENCIA, " _
            & " FA52APEL1, " _
            & " FA52APEL2, " _
            & " FA52NOMB, " _
            & " FA52CITE_CIP, " _
            & " FA52FECHNAC, " _
            & " FA52SEXO, " _
            & " FA52CODPOSTAL, " _
            & " FA52FECCONSULTA, " _
            & " FA52TIPOCONSULTA, " _
            & " FA52CODDPTO, " _
            & " FA52CANTFACT, " _
            & " FA52NUMREG, " _
            & " FA52FECPROCESO, " _
            & " FA52EXPINF, " _
            & " FA04CODFACT " _
        & ")"
SQL = SQL & "SELECT /*+ RULE */" _
        & " RPAD(NVL(FA2500.FA25NUMSEGSOC,' '),10) AS FA25NUMSEGSOC," _
        & " NVL(TO_CHAR(AD0100.AD01FECINICIO,'YYYYMMDD'),'        ') AS AD01FECINICIO," _
        & " 'AM' AS TIPOASISTENCIA," _
        & " RPAD(NVL(NVL(FA2500.FA25APEL1,CI2200.CI22PRIAPEL),' '),18) AS FA25APEL1," _
        & " RPAD(NVL(NVL(FA2500.FA25APEL2,CI22SEGAPEL),' '),18) AS FA25APEL2," _
        & " RPAD(NVL(NVL(FA2500.FA25NOMB,CI2200.CI22NOMBRE),' '),14) AS FA25NOMB," _
        & " RPAD(NVL(FA2500.FA25IDCLIENTE,' '),23) AS FA25IDCLIENTE," _
        & " RPAD(NVL(TO_CHAR(NVL(FA2500.FA25FECHNAC,CI2200.CI22FECNACIM),'DDMMYYY'),' '),7) AS FA25FECHNAC," _
        & " RPAD(NVL(TO_CHAR(NVL(FA2500.FA25SEXO,CI2200.CI30CODSEXO)),' '),1) AS FA25SEXO," _
        & " RPAD(NVL(TO_CHAR(FA2500.FA25CODPOSTAL),' '),5) AS FA25CODPOSTAL," _
        & " RPAD(NVL(TO_CHAR(AD0100.AD01FECINICIO,'DDMMYYY'),' '),7) AS FECHACONSULTA," _
        & " 1 AS TIPOCONSULTA," _
        & " RPAD(NVL(TO_CHAR(AD0500.AD02CODDPTO),' '),3) AS AD02CODDPTO," _
        & " LPAD(FA0400.FA04CANTFACT,10) AS FA04CANTFACT," _
        & " LPAD(ROWNUM + " & intNumReg & ",4,'0') AS NUMREG," _
        & " ? AS FECPROCESO," _
        & " RPAD(NVL(FA2500.FA25EXPINF, ' '),14) AS FA25EXPINF, " _
        & " FA0400.FA04CODFACT "
 SQL = SQL & " FROM " _
        & " AD1100, " _
        & " AD0100," _
        & " AD0500," _
        & " FA2501 FA2500," _
        & " CI2200," _
        & " FA0400"
 SQL = SQL & " WHERE " _
        & "     AD0100.AD01CODASISTENCI = FA0400.AD01CODASISTENCI" _
        & " AND AD0500.AD01CODASISTENCI = FA0400.AD01CODASISTENCI" _
        & " AND AD0500.AD07CODPROCESO = FA0400.AD07CODPROCESO" _
        & " AND AD0500.AD05FECFINRESPON IS NULL" _
        & " AND FA2500.AD01CODASISTENCI (+)= FA0400.AD01CODASISTENCI" _
        & " AND FA2500.AD07CODPROCESO (+)= FA0400.AD07CODPROCESO" _
        & " AND CI2200.CI22NUMHISTORIA = AD0100.CI22NUMHISTORIA" _
        & " AND FA0400.FA04NUMFACREAL IS NULL" _
        & " AND AD1100.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
        & " AND AD1100.AD07CODPROCESO = FA0400.AD07CODPROCESO " _
        & " AND AD1100.AD11FECFIN IS NULL " _
        & " AND (AD1100.AD11INDVOLANTE = 0 OR AD1100.AD11INDVOLANTE IS NULL) " _
        & " AND NOT EXISTS"
 SQL = SQL & "      (SELECT /*+ RULE */" _
        & "          FA03NUMRNF" _
        & "      FROM   " _
        & "         FA0500," _
        & "         FA0300" _
        & "      WHERE      FA0300.FA04NUMFACT = FA0400.FA04CODFACT" _
        & "         AND FA0500.FA05CODCATEG = FA0300.FA05CODCATEG" _
        & "         AND FA0500.FA08CODGRUPO =7" _
        & "         AND FA0500.FA05CODORIGC2 =0" _
        & "     )   "
 SQL = SQL & "    AND FA0400.FA04CODFACT IN" _
        & "        (SELECT /*+ RULE */ FA04CODFACT" _
        & "        FROM    " _
        & "            FA0419J " _
        & "        WHERE" _
        & "            TIPOASIST = 2" _
        & "            AND CI32CODTIPECON = 'S'" _
        & "            AND CI13CODENTIDAD ='NA'" _
        & "            AND FA04FECFACTURA = TO_DATE(?,'DD/MM/YYYY') " _
        & "        )"
 SQL = SQL & "   AND EXISTS " _
        & "        (SELECT /*+ RULE */ AD01CODASISTENCI " _
        & "         FROM   AD1800 " _
        & "         WHERE      AD1800.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
        & "            AND     AD1800.AD07CODPROCESO = FA0400.AD07CODPROCESO " _
        & "            AND     AD1800.AD19CODTIPODOCUM = 1 " _
        & "        )"
 SQL = SQL & "   AND EXISTS " _
        & "        (SELECT /*+ RULE */ FA04CODFACT " _
        & "         FROM   FA1600 " _
        & "         WHERE      FA1600.FA04CODFACT = FA0400.FA04CODFACT " _
        & "        )"
'SQL = SQL & "GROUP BY " _
 & "    TO_CHAR(AD0100.AD01FECINICIO,'YYYYMMDD')," _
 & "    RPAD(NVL(FA2500.FA25APEL1,CI2200.CI22PRIAPEL),18)," _
 & "    RPAD(NVL(FA2500.FA25APEL2,CI2200.CI22SEGAPEL),18) ," _
 & "    RPAD(NVL(FA2500.FA25NOMB,CI2200.CI22NOMBRE),14) ," _
 & "    RPAD(TO_CHAR(NVL(FA2500.FA25FECHNAC,CI2200.CI22FECNACIM),'DDMMYYY'),7) ," _
 & "    RPAD(NVL(FA2500.FA25SEXO,CI2200.CI30CODSEXO),1) ," _
 & "    RPAD(FA2500.FA25CODPOSTAL,5) ," _
 & "    RPAD(TO_CHAR(AD0100.AD01FECINICIO,'DDMMYYY'),7) ," _
 & "    RPAD(AD0500.AD02CODDPTO,3) ," _
 & "    LPAD(FA0400.FA04CANTFACT,10) ," _
 & "    FA0400.FA04CODFACT, " _
 & "    LPAD(ROWNUM + " & intNumReg & ",4,'0') "


    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "YYYYMM")
    qryDatos(1) = Format$(strFechaFact, "DD/MM/YYYY")
    qryDatos.Execute
        
'    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'    Do While Not rsDatos.EOF
'        Debug.Print rsDatos!numreg
'        rsDatos.MoveNext
'    Loop

    If Err <> 0 Then
        Call pMensaje("Pac. AM Sin Consultas: ERROR", txtMsg)
        strErrorMsg = Error
        Exit Function
    End If
    
    Call pMensaje("Pac. AM Sin Consultas: " & qryDatos.RowsAffected & " regs.", txtMsg)
    
    fFich_CreaPac_Ambu = True
    
    qryDatos.Close
End Function


Private Function fFich_CreaPac_Hospi(strFechaFact$, strErrorMsg$, txtMsg As TextBox) As Boolean
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    Dim SQL As String
    Dim intNumReg As Integer
    
    On Error Resume Next
    
    ' Primero se obtiene la siguiente posici�n en la tabla FA5200. Es un contador por Fecha de Proceso
    SQL = "SELECT NVL(MAX(TO_NUMBER(FA52NUMREG)),0)  AS FA52NUMREG    " _
        & " FROM    FA5200 " _
        & " WHERE   FA52FECPROCESO = ?"
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "YYYYMM")
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        intNumReg = rsDatos!FA52NUMREG
    End If
    rsDatos.Close
    qryDatos.Close
    Call pMensaje("HO Siguiente Reg: " & intNumReg + 1, txtMsg)

    
    SQL = "INSERT INTO FA5200" _
         & "( " _
            & " FA52NUMSEGSOC, " _
            & " FA52FECINICIO, " _
            & " FA52TIPOASISTENCIA, " _
            & " FA52APEL1, " _
            & " FA52APEL2, " _
            & " FA52NOMB, " _
            & " FA52CITE_CIP, " _
            & " FA52FECHNAC, " _
            & " FA52SEXO, " _
            & " FA52CODPOSTAL, " _
            & " FA52FECINGRESO, " _
            & " FA52CIRCINGRESO, " _
            & " FA52FECFIN, " _
            & " FA52CIRCALTA, " _
            & " FA52CODDPTO, " _
            & " FA52CANTFACT, " _
            & " FA52NUMREG, " _
            & " FA52FECPROCESO, " _
            & " FA52EXPINF, " _
            & " FA04CODFACT " _
        & ")"
    SQL = SQL & "SELECT /*+ RULE */ " _
                    & " RPAD(NVL(NVL(FA2500.FA25NUMSEGSOC,CI2200.CI22NUMSEGSOC),' '),10) AS FA25NUMSEGSOC, " _
                    & " NVL(TO_CHAR(AD0100.AD01FECINICIO,'YYYYMMDD'),'        ') AS AD01FECINICIO, " _
                    & " 'HO' AS TIPOASISTENCIA, " _
                    & " RPAD(NVL(NVL(FA2500.FA25APEL1,CI2200.CI22PRIAPEL),' '),18) AS FA25APEL1, " _
                    & " RPAD(NVL(NVL(FA2500.FA25APEL2,CI2200.CI22SEGAPEL),' '),18) AS FA25APEL2, " _
                    & " RPAD(NVL(NVL(FA2500.FA25NOMB, CI2200.CI22NOMBRE),' '),14) AS FA25NOMB, " _
                    & " RPAD(NVL(FA2500.FA25IDCLIENTE,' '),23) AS FA25IDCLIENTE, " _
                    & " RPAD(NVL(TO_CHAR(NVL(FA2500.FA25FECHNAC,CI2200.CI22FECNACIM),'DDMMYYY'),' '),7) AS FA25FECHNAC, " _
                    & " RPAD(NVL(TO_CHAR(NVL(FA2500.FA25SEXO, CI2200.CI30CODSEXO)),' '),1) AS FA25SEXO, " _
                    & " RPAD(NVL(TO_CHAR(FA2500.FA25CODPOSTAL),' '),5) AS FA25CODPOSTAL, " _
                    & " NVL(TO_CHAR(nvl(FA0400_1.FA04FECDESDE,AD0100.AD01FECINICIO),'HH24DDMMYYY'),'         ') AS AD01FECINGRESO, " _
                    & " DECODE(AD0100.AD01INDURGENTE,-1,1,2) AS CIRCINGRESO, " _
                    & " NVL(TO_CHAR(NVL(FA0400_1.FA04FECHASTA,AD0100.AD01FECFIN),'HH24DDMMYYY'),'         ') AS AD01FECFIN, " _
                    & " DECODE(AD0100.AD27CODALTAASIST, " _
                                                & " 1,1, " _
                                                & " 2,1, " _
                                                & " 3,1, " _
                                                & " 4,3, " _
                                                & " 5,2, " _
                                                & " 6,4, " _
                                                & " 7,1, " _
                                                & " 1) AS CIRCALTA, " _
                    & " RPAD(NVL(TO_CHAR(AD0500.AD02CODDPTO),' '),3) AS AD02CODDPTO, "
        SQL = SQL & " LPAD(FA0400_1.FA04CANTFACT,10) AS FA04CANTFACT, " _
                    & " LPAD(ROWNUM + " & intNumReg & ",4,'0') AS NUMREG, " _
                    & " ? , " _
                    & " RPAD(NVL(FA2500.FA25EXPINF,' '),14) AS FA25EXPINF, " _
                    & " FA0400_1.FA04CODFACT "
        SQL = SQL & " FROM " _
                    & " AD1100, " _
                    & " CI2200, " _
                    & " AD0100, " _
                    & " AD0500, " _
                    & " FA2501 FA2500, " _
                    & " FA0400 FA0400_1 "
        SQL = SQL & " WHERE " _
                    & " AD0100.AD01CODASISTENCI = FA0400_1.AD01CODASISTENCI " _
                    & " AND AD0500.AD01CODASISTENCI = FA0400_1.AD01CODASISTENCI " _
                    & " AND AD0500.AD07CODPROCESO = FA0400_1.AD07CODPROCESO " _
                    & " AND AD0500.AD05FECFINRESPON IS NULL " _
                    & " AND FA2500.AD01CODASISTENCI (+)= FA0400_1.AD01CODASISTENCI " _
                    & " AND FA2500.AD07CODPROCESO (+)= FA0400_1.AD07CODPROCESO " _
                    & " AND CI2200.CI22NUMHISTORIA = AD0100.CI22NUMHISTORIA" _
                    & " AND AD1100.AD01CODASISTENCI = FA0400_1.AD01CODASISTENCI " _
                    & " AND AD1100.AD07CODPROCESO = FA0400_1.AD07CODPROCESO " _
                    & " AND AD1100.AD11FECFIN IS NULL " _
                    & " AND (AD1100.AD11INDVOLANTE = 0 OR AD1100.AD11INDVOLANTE IS NULL) " _
                    & " AND FA0400_1.FA04NUMFACREAL IS NULL"
         SQL = SQL & "    AND FA0400_1.FA04CODFACT IN" _
                    & "        (SELECT /*+ RULE */ FA04CODFACT" _
                    & "        FROM    " _
                    & "            FA0419J " _
                    & "        WHERE" _
                    & "            TIPOASIST = 1" _
                    & "            AND CI32CODTIPECON = 'S'" _
                    & "            AND CI13CODENTIDAD ='NA'" _
                    & "            AND FA04FECFACTURA = TO_DATE(?,'DD/MM/YYYY') " _
                    & "        )"

    '    SQL = SQL & "   AND EXISTS " _
                    & "        (SELECT /*+ RULE */ AD01CODASISTENCI " _
                    & "         FROM   AD1800 " _
                    & "         WHERE      AD1800.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
                    & "            AND     AD1800.AD07CODPROCESO = FA0400.AD07CODPROCESO " _
                    & "            AND     AD1800.AD19CODTIPODOCUM = 1 " _
                    & "        )"
                    
   ' SQL = SQL & "GROUP BY " _
                    & " TO_CHAR(AD0100.AD01FECINICIO,'YYYYMMDD') , " _
                    & " RPAD(NVL(FA2500.FA25APEL1,CI2200.CI22PRIAPEL),18) , " _
                    & " RPAD(NVL(FA2500.FA25APEL2,CI2200.CI22SEGAPEL),18) , " _
                    & " RPAD(NVL(FA2500.FA25NOMB, CI2200.CI22NOMBRE),14) , " _
                    & " RPAD(TO_CHAR(NVL(FA2500.FA25FECHNAC,CI2200.CI22FECNACIM),'DDMMYYY'),7) , " _
                    & " RPAD(NVL(FA2500.FA25SEXO, CI2200.CI30CODSEXO),1) , " _
                    & " RPAD(FA2500.FA25CODPOSTAL,5) , " _
                    & " TO_CHAR(AD0100.AD01FECINICIO,'HH24DDMMYYY') , " _
                    & " DECODE(AD0100.AD01INDURGENTE,-1,1,2) , " _
                    & " TO_CHAR(AD0100.AD01FECFIN,'HH24DDMMYYY') , " _
                    & " DECODE(AD0100.AD27CODALTAASIST, " _
                                                & " 1,1, " _
                                                & " 2,1, " _
                                                & " 3,1, " _
                                                & " 4,3, " _
                                                & " 5,2, " _
                                                & " 6,4, " _
                                                & " 7,1, " _
                                                & " 1)," _
                    & " RPAD(AD0500.AD02CODDPTO,3) , " _
                    & " LPAD(FA0400_1.FA04CANTFACT,10) , " _
                    & " FA0400_1.FA04CODFACT "
                    
                    
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "YYYYMM")
    qryDatos(1) = Format$(strFechaFact, "DD/MM/YYYY")
    qryDatos.Execute
    If Err <> 0 Then
        Call pMensaje("Pac. HO: ERROR", txtMsg)
        strErrorMsg = Error
        Exit Function
    End If
    
    Call pMensaje("Pac. HO: " & qryDatos.RowsAffected & " regs.", txtMsg)
    
    fFich_CreaPac_Hospi = True
    qryDatos.Close
End Function




Private Function fFich_ConcepActualizaFecha(strFechaFact$, strTipoAsist$, strErrorMsg$, txtMsg As TextBox) As Boolean
    ' Se actualizan las fechas de cada uno de las l�neas generadas
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    
    SQL = "UPDATE    FA5300 FA5300_1" _
        & " SET     FA53FECHACONCEPTO = " _
        & "         (SELECT " _
        & "                 TO_CHAR(MIN(FA0300.FA03FECHA),'YYYYMMDD') " _
        & "          FROM   FA0300" _
        & "          WHERE      FA0300.FA04NUMFACT = FA5300_1.FA04CODFACT " _
        & "                 AND FA0300.FA16NUMLINEA = FA5300_1.FA16NUMLINEA " _
        & "         )" _
        & "     WHERE (FA5300_1.FA52FECPROCESO, FA5300_1.FA52NUMREG) IN " _
        & "         (SELECT FA52FECPROCESO, FA52NUMREG " _
        & "          FROM   FA5200 " _
        & "          WHERE  FA5200.FA52FECPROCESO = ? " _
        & "                 AND FA5200.FA52TIPOASISTENCIA = ? " _
        & "         )"

    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "yyyymm")
    qryDatos(1) = strTipoAsist
    qryDatos.Execute
    If Err <> 0 Then
        Call pMensaje("Conceptos " & strTipoAsist & " (Actualizar Fecha): ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Conceptos " & strTipoAsist & "(Actualizar Fecha): " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close
      
    
    fFich_ConcepActualizaFecha = True
End Function

Private Function fFich_ConcepActualizaImporte(strFechaFact$, strTipoAsist$, strErrorMsg$, txtMsg As TextBox) As Boolean
    ' Se actualizan las cantidades de cada uno de las l�neas generadas
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    
    SQL = "UPDATE    FA5300 FA5300_1" _
        & " SET     FA53IMPORTEGRUPO = " _
        & "         (SELECT " _
        & "                 LPAD(TO_CHAR(SUM (TO_NUMBER(FA53IMPORTE))),10) " _
        & "          FROM   FA5300" _
        & "          WHERE      FA5300.FA52FECPROCESO = FA5300_1.FA52FECPROCESO " _
        & "                  AND FA5300.FA52NUMREG = FA5300_1.FA52NUMREG " _
        & "                  AND FA5300.FA53CONCEPTO = 'ES'   " _
        & "          GROUP BY " _
        & "                   FA5300.FA52FECPROCESO,   " _
        & "                   FA5300.FA52NUMREG " _
        & "         )" _
        & " WHERE   FA53CONCEPTO='ES' " _
        & "     AND FA52FECPROCESO = ? "

    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "yyyymm")
    qryDatos.Execute
    If Err <> 0 Then
        Call pMensaje("Conceptos " & strTipoAsist & " (Actualizar Importe Grupo): ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Conceptos " & strTipoAsist & "(Actualizar Importe Grupo): " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close
      
    
    fFich_ConcepActualizaImporte = True
End Function

Private Function fFich_ConcepActualizaCantidad(strFechaFact$, strTipoAsist$, strErrorMsg$, txtMsg As TextBox) As Boolean
    ' Se actualizan las cantidades de cada uno de las l�neas generadas
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    
    SQL = "UPDATE    FA5300 FA5300_1" _
        & " SET     FA53CANTIDAD = " _
        & "         (SELECT " _
        & "                 LPAD(DECODE(FLOOR(SUM(FA0300.FA03CANTFACT)/9999), " _
        & "                     0,FLOOR(SUM(FA0300.FA03CANTFACT)),  " _
        & "                     9999),4) " _
        & "          FROM   FA0300" _
        & "          WHERE      FA0300.FA04NUMFACT = FA5300_1.FA04CODFACT " _
        & "                 AND FA0300.FA16NUMLINEA = FA5300_1.FA16NUMLINEA " _
        & "         )" _
        & " WHERE FA5300_1.FA53CONCEPTO NOT IN ('DI','RE','DG') " _
        & "     AND (FA5300_1.FA52FECPROCESO, FA5300_1.FA52NUMREG) IN " _
        & "         (SELECT FA52FECPROCESO, FA52NUMREG " _
        & "          FROM   FA5200 " _
        & "          WHERE  FA5200.FA52FECPROCESO = ? " _
        & "                 AND FA5200.FA52TIPOASISTENCIA = ? " _
        & "         )"

    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "yyyymm")
    qryDatos(1) = strTipoAsist
    qryDatos.Execute
    If Err <> 0 Then
        Call pMensaje("Conceptos " & strTipoAsist & " (Actualizar Cantidad): ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Conceptos " & strTipoAsist & "(Actualizar Cantidad): " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close
      
    
    SQL = "UPDATE    FA5300 FA5300_1" _
        & " SET     FA53CANTIDAD = " _
        & "         (SELECT " _
        & "                 LPAD(COUNT(DISTINCT TO_CHAR(FA0300.FA03FECHA,'DD/MM/YYYY')),4)" _
        & "          FROM   FA0300" _
        & "          WHERE      FA0300.FA04NUMFACT = FA5300_1.FA04CODFACT " _
        & "                 AND FA0300.FA16NUMLINEA = FA5300_1.FA16NUMLINEA " _
        & "         )" _
        & " WHERE FA5300_1.FA53CONCEPTO IN ('DI','RE') " _
        & "     AND (FA5300_1.FA52FECPROCESO, FA5300_1.FA52NUMREG) IN " _
        & "         (SELECT FA52FECPROCESO, FA52NUMREG " _
        & "          FROM   FA5200 " _
        & "          WHERE FA5200.FA52FECPROCESO = ? " _
        & "                 AND FA5200.FA52TIPOASISTENCIA = ? " _
        & "         )"
        
        

    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "yyyymm")
    qryDatos(1) = strTipoAsist
    qryDatos.Execute
    If Err <> 0 Then
        Call pMensaje("Conceptos " & strTipoAsist & " (Actualizar Cuenta): ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Conceptos " & strTipoAsist & "(Actualizar Cuenta): " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close


    ' Todos aquellos que no tengan movimientos asociados -> se actualizan a 1
    SQL = "UPDATE   FA5300 " _
        & " SET     FA53CANTIDAD = LPAD('1',4)" _
        & " WHERE    (FA5300.FA52FECPROCESO, FA5300.FA52NUMREG) IN " _
        & "         (SELECT FA52FECPROCESO, FA52NUMREG " _
        & "          FROM   FA5200 " _
        & "          WHERE FA5200.FA52FECPROCESO = ? " _
        & "                 AND FA5200.FA52TIPOASISTENCIA = ? " _
        & "         )" _
        & "     AND NOT EXISTS " _
        & "         (SELECT FA03NUMRNF " _
        & "          FROM   FA0300 " _
        & "          WHERE      FA0300.FA04NUMFACT = FA5300.FA04CODFACT " _
        & "                 AND FA0300.FA16NUMLINEA = FA5300.FA16NUMLINEA " _
        & "         )"
        
        

    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFechaFact, "yyyymm")
    qryDatos(1) = strTipoAsist
    qryDatos.Execute
    If Err <> 0 Then
        Call pMensaje("Conceptos " & strTipoAsist & " (Actualizar Cuenta Sin RNF): ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Conceptos " & strTipoAsist & "(Actualizar Cuenta Sin RNF): " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close
    
    fFich_ConcepActualizaCantidad = True
End Function



Private Function fFich_DiagInsertDiag(strFecha$, strTipoAsist$, strErrorMsg$, txtMsg As TextBox) As Boolean
    ' Se insertan los diagn�sticos en la tabla de conceptos
    Dim SQL As String
    Dim qryDatos As rdoQuery, rsDatos As rdoResultset
    
    
    On Error Resume Next
    SQL = " INSERT INTO FA5400 " _
        & " (" _
        & " FA54NUMDIAG,        " _
        & " FA54NUMSEGSOC,         " _
        & " FA54FECINICIO ,            " _
        & " FA54CODIGO ,            " _
        & " FA54CONCEPTO,           " _
        & " FA54DESCRIP,            " _
        & " FA54TIPO,           " _
        & " FA04CODFACT,            " _
        & " FA54LA24,           " _
        & " FA54FECPROCESO       " _
        & " )"
    SQL = SQL & " SELECT FA54NUMDIAG_SEQUENCE.NEXTVAL," _
        & "             FA5200.FA52NUMSEGSOC, " _
        & "             FA5200.FA52FECINICIO, " _
        & "             RPAD(NVL(AD4000.AD39CODIGO,' '),6), " _
        & "             'DG', " _
        & "             RPAD(NVL(AD3900.AD39DESCODIGO,' '),36), " _
        & "             LPAD(DECODE(AD4000.AD40TIPODIAGPROC, " _
        & "                         'P',1, " _
        & "                         'S',0, " _
        & "                         0),4)," _
        & "             FA5200.FA04CODFACT, " _
        & "             RPAD(' ',24), " _
        & "             FA5200.FA52FECPROCESO "
    SQL = SQL & " FROM  AD3900, " _
        & "             AD4000, " _
        & "             FA0400, " _
        & "             FA5200  "
    SQL = SQL & " WHERE     FA0400.FA04CODFACT = FA5200.FA04CODFACT " _
        & "             AND AD4000.AD01CODASISTENCI = FA0400.AD01CODASISTENCI " _
        & "             AND AD4000.AD07CODPROCESO = FA0400.AD07CODPROCESO " _
        & "             AND AD3900.AD39CODIGO = AD4000.AD39CODIGO " _
        & "             AND AD3900.AD39TIPOCODIGO ='D' " _
        & "             AND FA5200.FA52FECPROCESO = ? " _
        & "             AND FA5200.FA52TIPOASISTENCIA = ? "
        
    Set qryDatos = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatos(0) = Format$(strFecha, "yyyymm")
    qryDatos(1) = strTipoAsist
    qryDatos.Execute
        
    If Err <> 0 Then
        Call pMensaje("Insert Diag. " & strTipoAsist & ": ERROR", txtMsg)
        strErrorMsg = Error
        qryDatos.Close
        Exit Function
    End If
    
    Call pMensaje("Insert Diag. " & strTipoAsist & ": " & qryDatos.RowsAffected & " regs.", txtMsg)
    qryDatos.Close
        
    fFich_DiagInsertDiag = True

End Function

Public Sub pMensaje(strText$, txtMsg As TextBox)
    txtMsg.Text = txtMsg.Text & Chr$(13) & Chr$(10) & strText
    txtMsg.SelStart = Len(txtMsg.Text)
    txtMsg.Refresh
    
End Sub


