VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_CompensaFact 
   Caption         =   "Compensaci�n de una factura positiva con una negativa"
   ClientHeight    =   5580
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8610
   LinkTopic       =   "Form1"
   ScaleHeight     =   5580
   ScaleWidth      =   8610
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtImporte 
      Height          =   285
      Left            =   6390
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   810
      Width           =   1275
   End
   Begin VB.TextBox txtNumFactura 
      Height          =   285
      Left            =   2250
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   90
      Width           =   1275
   End
   Begin VB.TextBox txtResponsable 
      Height          =   285
      Left            =   2250
      Locked          =   -1  'True
      TabIndex        =   7
      Tag             =   "txtResponsable"
      Top             =   450
      Width           =   5235
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   420
      Left            =   1935
      TabIndex        =   6
      Top             =   5040
      Width           =   1275
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   420
      Left            =   5220
      TabIndex        =   5
      Top             =   5040
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   " Facturas Pendientes de Compensar "
      Height          =   2490
      Left            =   135
      TabIndex        =   3
      Top             =   2430
      Width           =   8340
      Begin SSDataWidgets_B.SSDBGrid grdFacturas 
         Height          =   2175
         Left            =   135
         TabIndex        =   4
         Top             =   225
         Width           =   8115
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         Col.Count       =   7
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Name =   "Compensar"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   1879
         Columns(1).Caption=   "N� Factura"
         Columns(1).Name =   "NoFactura"
         Columns(1).Alignment=   1
         Columns(1).CaptionAlignment=   2
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   7435
         Columns(2).Caption=   "Descripcion"
         Columns(2).Name =   "Descripcion"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1931
         Columns(3).Caption=   "Pdte. Comp."
         Columns(3).Name =   "PdteCompensar"
         Columns(3).Alignment=   1
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   2117
         Columns(4).Caption=   "Cantidad"
         Columns(4).Name =   "Cantidad"
         Columns(4).Alignment=   1
         Columns(4).CaptionAlignment=   2
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   556
         Columns(5).Name =   "Entera"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   11
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "Codigo"
         Columns(6).Name =   "Codigo"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   11
         Columns(6).FieldLen=   256
         _ExtentX        =   14314
         _ExtentY        =   3836
         _StockProps     =   79
      End
   End
   Begin VB.TextBox txtObservaciones 
      Height          =   780
      Left            =   2205
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   1170
      Width           =   6225
   End
   Begin VB.TextBox txtCodPago 
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   285
   End
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   420
      Left            =   180
      TabIndex        =   1
      Top             =   5850
      Visible         =   0   'False
      Width           =   465
      _Version        =   196608
      _ExtentX        =   820
      _ExtentY        =   741
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      TableBorder     =   0
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFecha 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   5625
      TabIndex        =   8
      Tag             =   "Fecha Inicio"
      Top             =   135
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      Enabled         =   0   'False
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFact 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   2250
      TabIndex        =   19
      Tag             =   "Fecha Inicio"
      Top             =   810
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      Enabled         =   0   'False
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label lblCodFactura 
      Caption         =   "Factura"
      Height          =   240
      Left            =   3735
      TabIndex        =   18
      Top             =   135
      Width           =   1230
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Importe de Factura:"
      Height          =   240
      Index           =   1
      Left            =   4500
      TabIndex        =   17
      Top             =   855
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "N� de Factura:"
      Height          =   240
      Index           =   3
      Left            =   360
      TabIndex        =   15
      Top             =   135
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Responsable Econ�mico:"
      Height          =   240
      Index           =   0
      Left            =   360
      TabIndex        =   13
      Top             =   495
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Factura:"
      Height          =   240
      Index           =   2
      Left            =   360
      TabIndex        =   12
      Top             =   855
      Width           =   1815
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      Caption         =   "Cantidad Pendiente de Asignar:"
      Height          =   240
      Left            =   360
      TabIndex        =   11
      Top             =   2025
      Width           =   2355
   End
   Begin VB.Label lblPendiente 
      Caption         =   "Pendiente"
      Height          =   240
      Left            =   2790
      TabIndex        =   10
      Top             =   2025
      Width           =   1230
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Observaciones"
      Height          =   240
      Index           =   6
      Left            =   315
      TabIndex        =   9
      Top             =   1215
      Width           =   1815
   End
End
Attribute VB_Name = "frm_CompensaFact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Nombre As String
Dim FecPago As String
Dim CodPersona As Long
Dim Conciertos As String
Dim PagoNuevo As Boolean
Dim ImporteFactura As Double
Dim CodFact As String
Dim Pagado As Double
Dim Apuntes As Integer

Private Type FactPos
  Codigo As Long
  Cantidad As Double
  Compens As Boolean
End Type
Dim arPosit() As FactPos
Sub CargarFacturas(Persona As Long, Conciertos As String)
Dim MiSql As String
Dim MiRs As rdoResultset
Dim MisqlSuma As String
Dim MiRsSuma As rdoResultset
Dim MiInsertar As String
Dim IntPospyc As Integer
Dim Conc As Integer
Dim Importe As Long

  Me.grdFacturas.RemoveAll
  While Conciertos <> ""
    IntPospyc = InStr(1, Conciertos, ";")
    If IntPospyc <> 0 Then
      Conc = Left(Conciertos, IntPospyc - 1)
      Conciertos = Right(Conciertos, Len(Conciertos) - IntPospyc)
    ElseIf Conciertos <> "" Then
      Conc = Conciertos
      Conciertos = ""
    End If
    MiSql = "Select FA04NUMFACT, FA0400.FA04CODFACT,FA04CANTFACT,FA04DESCRIP, FA04INDCOMPENSA, " & _
            "NVL(Sum(FA18IMPCOMP),0) as Compensa, NVL(Sum(FA58IMPCOMP),0) As Compensa2 " & _
            "From FA0400,FA1800,FA5800 " & _
            "Where FA0400.FA04CODFACT = FA1800.FA04CODFACT(+) " & _
            "And FA0400.FA04CODFACT = FA5800.FA04CODFACT_NEG(+) " & _
            "And FA09CODNODOCONC IN (" & Conc & ") " & _
            "And CI21CODPERSONA = " & Persona & " " & _
            "And FA04INDCOMPENSA <> 2 " & _
            "And FA04CANTFACT < 0 " & _
            "Group By FA04NUMFACT, FA0400.FA04CODFACT, FA04CANTFACT,FA04DESCRIP,FA04INDCOMPENSA " & _
            "ORDER BY FA04CODFACT"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      While Not MiRs.EOF
        MiInsertar = False & Chr(9) & MiRs("FA04NUMFACT") & Chr(9) & MiRs("FA04DESCRIP") & Chr(9) & _
                           FormatearMoneda(CDbl(MiRs("FA04CANTFACT")) - (CDbl(MiRs("Compensa")) + CDbl(MiRs("CompEnsa2"))), tiTotalFact) & _
                           Chr(9) & 0 & Chr(9) & False & Chr(9) & MiRs("FA04CODFACT")
        grdFacturas.AddItem MiInsertar
        MiRs.MoveNext
      Wend
    End If
  Wend
 
End Sub
Sub CompFactura(idpersona As Long, Persona As String, fecha As String, Concierto As String, Importe As Double, factura As String)
  CodPersona = idpersona
  Nombre = UCase(Persona)
  FecPago = fecha
  Conciertos = Concierto
  ImporteFactura = Importe
  CodFact = factura
  frm_CompensaFact.Show vbModal
  Set frm_CompensaFact = Nothing
End Sub




Private Sub cmdAceptar_Click()

Dim NoPago As String
Dim MiSql As String
Dim SqlComp As String
Dim MiRs As rdoResultset
Dim x, ContArr As Integer
Dim Resultado As Integer
Dim CompensaPagos As Double
Dim CompensaFacturas As Double
Dim Compensado As Double
Dim PdteCompensar As Double

  
  'Cargamos en un array todas las facturas que componen la positiva a compensar.
  MiSql = "Select FA0400.FA04CODFACT, FA04CANTFACT, NVL(Sum(FA18IMPCOMP),0) AS Compensa, " & _
          "NVL(Sum(FA58IMPCOMP),0) As Compensa2 " & _
          "From FA0400,FA1800,FA5800 " & _
          "Where FA0400.FA04CODFACT = FA1800.FA04CODFACT(+) " & _
          "And FA0400.FA04CODFACT = FA5800.FA04CODFACT_POS(+) " & _
          "And FA04INDCOMPENSA IN (1,0) " & _
          "And FA04CANTFACT > 0 " & _
          "And FA04NUMFACT = '" & Me.lblCodFactura & "' Group By FA0400.FA04CODFACT,FA04CANTFACT"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    x = 0
    While Not MiRs.EOF
      x = x + 1
      If MiRs("FA04CANTFACT") - (MiRs("Compensa") + MiRs("Compensa2")) > 0 Then
        ReDim Preserve arPosit(1 To x)
        arPosit(x).Codigo = MiRs("FA04CODFACT")
        arPosit(x).Cantidad = MiRs("FA04CANTFACT") - (MiRs("Compensa") + MiRs("Compensa2"))
        arPosit(x).Compens = False
      End If
      MiRs.MoveNext
    Wend
  End If
  On Error GoTo ErrorenGrabacion
  objApp.rdoConnect.BeginTrans
    Pagado = 0
    Apuntes = 0
    grdFacturas.MoveFirst
    For x = 1 To grdFacturas.Rows
      If grdFacturas.Columns(5).Value = True Or grdFacturas.Columns(4).Value <> 0 Then
        PdteCompensar = Abs(grdFacturas.Columns(4).Value)
        'Tendremos que compensar la factura con las facturas que se encuentran en el array
        For ContArr = 1 To UBound(arPosit)
          If arPosit(ContArr).Compens = False And PdteCompensar > 0 Then
            MiSql = "INSERT INTO FA5800 (FA04CODFACT_POS,FA04CODFACT_NEG,FA58FECCOMP,FA58IMPCOMP) " & _
                    "VALUES (" & arPosit(ContArr).Codigo & "," & grdFacturas.Columns(6).Text & _
                         ", TO_DATE('" & Me.SDCFecha.Date & "','DD/MM/YYYY'),"
            If PdteCompensar >= arPosit(ContArr).Cantidad Then
              MiSql = MiSql & arPosit(ContArr).Cantidad & ")"
              PdteCompensar = PdteCompensar - arPosit(ContArr).Cantidad
              arPosit(ContArr).Compens = True
              objApp.rdoConnect.Execute "Update FA0400 Set FA04INDCOMPENSA = 2 Where FA04CODFACT = " & arPosit(ContArr).Codigo
            Else
              MiSql = MiSql & PdteCompensar & ")"
              arPosit(ContArr).Cantidad = arPosit(ContArr).Cantidad - PdteCompensar
              PdteCompensar = 0
              objApp.rdoConnect.Execute "Update FA0400 Set FA04INDCOMPENSA = 1 Where FA04CODFACT = " & arPosit(ContArr).Codigo
            End If
            objApp.rdoConnect.Execute MiSql
          End If
          If PdteCompensar = 0 Then
            Exit For
          End If
        Next
        'Comprobaremos si la factura negativa est� totalmente compensada con la positiva.
        If grdFacturas.Columns(3).Value = grdFacturas.Columns(4).Value Then
          'La factura negativa est� totalmente comenpensada
          SqlComp = "Update FA0400 Set FA04INDCOMPENSA = 2 Where FA04CODFACT = " & grdFacturas.Columns(6).Text
          objApp.rdoConnect.Execute SqlComp
        Else
          SqlComp = "Update FA0400 Set FA04INDCOMPENSA = 1 Where FA04CODFACT = " & grdFacturas.Columns(6).Text
          objApp.rdoConnect.Execute SqlComp
        End If
      End If
      grdFacturas.MoveNext
    Next
  objApp.rdoConnect.CommitTrans
  Unload Me
Exit Sub

ErrorenGrabacion:
  MsgBox "Se ha producido un error en la grabaci�n de los pagos", vbCritical + vbOKOnly, "Atenci�n"
  objApp.rdoConnect.RollbackTrans

End Sub




Private Sub cmdCancelar_Click()
  Unload Me
End Sub


Function CalcularPendiente(Pago As String) As Double
Dim MiSql As String
Dim MiRs As rdoResultset
Dim AcumuladoPagos As Double
Dim AcumuladoFacturas As Double

  'Calculamos las compensaciones que tienen todos los c�digos de factura con abonos y fact. negativas
  MiSql = "Select FA04NUMFACT, NVL(Sum(FA18IMPCOMP),0) AS Compensa, NVL(Sum(FA58IMPCOMP),0) As Compensa2 " & _
          "From FA0400,FA1800,FA5800 " & _
          "Where FA0400.FA04CODFACT = FA1800.FA04CODFACT " & _
          "And FA0400.FA04CODFACT = FA5800.FA04CODFACT_POS " & _
          "And FA04NUMFACT = '" & Pago & "' " & _
          "Group By FA04NUMFACT"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    CalcularPendiente = (MiRs("Compensa") + MiRs("Compensa2"))
  Else
    CalcularPendiente = 0
  End If
  
End Function

Private Sub Form_Load()
Dim MiSql As String
Dim MiRs As rdoResultset

  Me.txtResponsable.Text = Nombre
  Me.SDCFecha.Date = FecPago
  lblCodFactura = CodFact
  Me.Caption = "Compensaci�n de facturas positivas con facturas negativas"
  'Me.txtObservaciones = MiRs("FA17OBSERV") & ""
  'Seleccionamos la descripci�n y el n� de la factura
  MiSql = "Select FA04NUMFACT, FA04DESCRIP,FA04FECFACTURA From FA0400 Where FA04NUMFACT = '" & CodFact & "'"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql)
  If Not MiRs.EOF Then
    Me.txtNumFactura = MiRs("FA04NUMFACT")
    Me.txtObservaciones = MiRs("FA04DESCRIP") & ""
    If Not IsNull(MiRs("FA04FECFACTURA")) Then
      Me.SDCFechaFact = MiRs("FA04FECFACTURA")
    End If
  Else
  End If
  Me.txtImporte = ImporteFactura
  lblPendiente = Me.txtImporte - CalcularPendiente(MiRs("FA04NUMFACT"))
  Call Me.CargarFacturas(CodPersona, Conciertos)
End Sub

Private Sub grdFacturas_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim x As Integer
Dim Pendiente As Double
  If ColIndex = 4 Then
    If grdFacturas.Columns(4).Text = "" Then
      grdFacturas.Columns(4).Text = 0
    End If
    'La primera comprobaci�n ser� la de que la cantidad a compensar no es mayor que la cantidad facturada
    If CDbl(Abs(grdFacturas.Columns(4).Text)) > CDbl(Abs(grdFacturas.Columns(3).Text)) Then
      MsgBox "La cantidad a compensar no puede ser mayor que la cantidad pendiente", vbOKOnly + vbCritical, "Atenci�n"
      grdFacturas.Columns(4).Text = OldValue
    Else
      ' Comprobaremos que la cantidad que sumamos no supera a la que queda pendiente
      Pendiente = CDbl(lblPendiente) - OldValue
      If Pendiente - grdFacturas.Columns(4).Text < 0 Then
        MsgBox "No se puede asignar esa cantidad ya que es mayor que la cantidad pendiente de asignaci�n", vbCritical + vbOKOnly, "Atenci�n"
        lblPendiente = Pendiente - OldValue
        grdFacturas.Columns(4).Text = OldValue
      Else
        lblPendiente = Pendiente - grdFacturas.Columns(4).Text
      End If
    End If
  ElseIf ColIndex = 5 Then
    
  End If
  
  
End Sub

Private Sub grdFacturas_Click()
Dim Pendiente As Double

  If grdFacturas.Col = 5 Then
    If grdFacturas.Columns(5).Value = True Then
      Pendiente = grdFacturas.Columns(4).Text
      grdFacturas.Columns(4).Text = 0
      lblPendiente = Pendiente
    ElseIf grdFacturas.Columns(5).Value = False Then
      Pendiente = CDbl(lblPendiente) + grdFacturas.Columns(4).Text
      If Pendiente >= grdFacturas.Columns(3).Text Then
        grdFacturas.Columns(4).Text = CDbl(Pendiente)
        lblPendiente = 0
      Else
        MsgBox "No es posible compensar totalmente la factura, dado que la cantidad pendiente no es suficiente", vbCritical + vbOKOnly, "Atencion"
        grdFacturas.Columns(5).Value = False
      End If
    End If
  End If
End Sub



