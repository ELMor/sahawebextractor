CREATE OR REPLACE procedure Repartos(steps number, Entid boolean, Rep boolean) is
   estado number;
   fecha  date;
   asient number;
   operac number;
begin

   if steps=1 then
      fecha := to_date('31121999','ddmmyyyy');
      asient:= 1111;
      operac:= 1;
      if Entid then
         estado:=	 FAPK02.FacturarIH(fecha);
      end if;
      if Rep then
         update   fa0400
              set fa04feccontabil=fecha
            where fa04codfact in
                  (select fa04codfact from fa0424j where fa03fecha=fecha);
         commit;
         fapk01.FAPR02(asient);
         fapk01.FAPR01(fecha,asient);
      end if;
   end if;


   if steps=2 then
      fecha := to_date('31012000','ddmmyyyy');
      asient:= 1111;
      operac:= 2;
      if Entid then
         estado:=	 FAPK02.FacturarIH(fecha);
      end if;
      if Rep then
         update   fa0400
              set fa04feccontabil=fecha
            where fa04codfact in
                  (select fa04codfact from fa0424j where fa03fecha=fecha);
         commit;
         fapk01.FAPR01(fecha,asient);
      end if;
   end if;

   if steps=3 then
      fecha := to_date('29022000','ddmmyyyy');
      asient:= 1111;
      operac:= 3;
      if Entid then
         estado:=	 FAPK02.FacturarIH(fecha);
      end if;
      if Rep then
         update   fa0400
              set fa04feccontabil=fecha
            where fa04codfact in
                  (select fa04codfact from fa0424j where fa03fecha=fecha);
         commit;
         fapk01.FAPR01(fecha,asient);
      end if;
   end if;


   if steps=4 then
      fecha := to_date('31032000','ddmmyyyy');
      asient:= 1111;
      operac:= 4;
      if Entid then
         estado:=	 FAPK02.FacturarIH(fecha);
      end if;
      if Rep then
         update   fa0400
              set fa04feccontabil=fecha
            where fa04codfact in
                  (select fa04codfact from fa0424j where fa03fecha=fecha);
         commit;
         fapk01.FAPR01(fecha,asient);
      end if;
   end if;

   if steps=5 then
      fecha := to_date('30042000','ddmmyyyy');
      asient:= 1111;
      operac:= 5;
      if Entid then
         estado:=	 FAPK02.FacturarIH(fecha);
      end if;
      if Rep then
         update   fa0400
              set fa04feccontabil=fecha
            where fa04codfact in
                  (select fa04codfact from fa0424j where fa03fecha=fecha);
         commit;
         fapk01.FAPR01(fecha,asient);
      end if;
   end if;

   if steps=6 then
      fecha := to_date('31052000','ddmmyyyy');
      asient:= 1111;
      operac:= 6;
      if Entid then
         estado:=	 FAPK02.FacturarIH(fecha);
      end if;
      if Rep then
         update   fa0400
              set fa04feccontabil=fecha
            where fa04codfact in
                  (select fa04codfact from fa0424j where fa03fecha=fecha);
         commit;
         fapk01.FAPR01(fecha,asient);
      end if;
   end if;

   if steps=7 then
      fecha := to_date('30062000','ddmmyyyy');
      asient:= 1111;
      operac:= 7;
      if Entid then
         estado:=	 FAPK02.FacturarIH(fecha);
      end if;
      if Rep then
         update   fa0400
              set fa04feccontabil=fecha
            where fa04codfact in
                  (select fa04codfact from fa0424j where fa03fecha=fecha);
         commit;
         fapk01.FAPR01(fecha,asient);
      end if;
   end if;
end;
/

