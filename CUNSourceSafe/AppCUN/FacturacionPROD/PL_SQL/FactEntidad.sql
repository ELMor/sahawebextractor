CREATE OR REPLACE PACKAGE FACTENTIDAD IS
		FUNCTION FacturarEntidad(FechaFact DATE) RETURN BOOLEAN;
   
		CodTipEcon CONSTANT VARCHAR(1) := 'I';
		CodConc CONSTANT VARCHAR(6) := '126';
   CodNodo CONSTANT VARCHAR(6) := '454';
   
END FACTENTIDAD;
/
PACKAGE BODY FACTENTIDAD AS
FUNCTION BorrarRNFs(nProceso ad0700.ad07codproceso%TYPE, nAsistenci ad0100.ad01codasistenci%TYPE) RETURN BOOLEAN IS
/**********************************************************************************************************************
  FUNCTION      : BorrarRNFs()
  SCOPE         : PRIVATE

  DESCRIPTION   : elimina de la tabla de RNFs todos los registros correspondientes al proc-asist pasado como
                  parametro. Si por cualquier motivo falla la operaci�n fallase, hace ROLLBACK y devuelve FALSE
  PARAMETERS IN : nProceso, nAsistenci
  RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (error)
 *********************************************************************************************************************/
BEGIN
   DELETE FROM FA0300 WHERE FA0300.AD07CODPROCESO = nProceso
	   		 	   		 AND FA0300.AD01CODASISTENCI = nAsistenci
	   			   		 AND FA04NUMFACT IS NULL AND FA03INDPERMANENTE IS NULL;
   COMMIT;

   RETURN TRUE;

   EXCEPTION
   		 WHEN OTHERS THEN
   		 	  ROLLBACK;
   		 	  RETURN FALSE;
END BorrarRNFs;

FUNCTION ValorarRNFs(nProceso ad0700.ad07codproceso%TYPE, nAsistenci ad0100.ad01codasistenci%TYPE) RETURN BOOLEAN IS
/**********************************************************************************************************************
  FUNCION QUE VALORA LOS RNFS DE UN PROCESO-ASISTENCIA
**********************************************************************************************************************/

   CURSOR curRNFs(nP ad0700.ad07codproceso%TYPE, nA ad0100.ad01codasistenci%TYPE) IS
		SELECT FA1500.FA15CODATRIB CodAtrib, FA1500.FA15PRECIOREF, FA1500.FA15PRECIODIA, FA15FECINICIO, FA0300.*
		FROM FA0300, FA1500
		WHERE FA0300.AD07CODPROCESO = nP
			AND FA0300.AD01CODASISTENCI = nA
			AND FA15ESCAT = 1
			AND FA0300.FA05CODCATEG = FA1500.FA15CODFIN
			AND FA15CODCONC = CodConc
			AND FA15RUTA LIKE '.'||CodConc||'.'||CodNodo||'.%'
			AND FA03FECHA BETWEEN FA15FECINICIO AND FA15FECFIN
      AND FA04NUMFACT IS NULL;

BEGIN
-- HAY QUE TENER EN CUENTA QUE EL PRECIO PUEDE SER :
--			DETERMINADO POR EL QUE YA TENGA EL PROPIO RNF
--			POR CANTIDAD
--			POR DIA
--			POR TRAMOS (p.e. Estancias, Quirofanos, etc.)
--			CONDICIONADO A OTRO NODO EN UN PORCENTAJE (p.e. Honorarios de Anestesia, etc.)
FOR RNF IN curRNFs(nProceso, nAsistenci) LOOP
	IF RNF.FA03PRECIOFACT IS NOT  NULL THEN
		  -- YA tiene precio no hay que modificarlo
     NULL;
	ELSIF rnf.fa15precioref IS NOT NULL THEN
	   -- Tiene precio de referencia
	   UPDATE FA0300
	   SET FA03PRECIOFACT = RNF.FA15PRECIOREF,
         FA0300.FA15CODATRIB = RNF.CODATRIB
     WHERE FA03NUMRNF = RNF.FA03NUMRNF;

	ELSIF RNF.FA15PRECIODIA IS NOT NULL THEN
	   -- Tiene precio por dia
	   UPDATE FA0300
	   SET FA03PRECIOFACT = RNF.FA15PRECIODIA,
         FA0300.FA15CODATRIB = RNF.CODATRIB
     WHERE FA03NUMRNF = RNF.FA03NUMRNF;

	ELSIF RNF.FA15PRECIODIA IS NOT NULL THEN
  	  -- Hay que buscar si tiene precios por cantidad o condicionado
     
     
	   -- NO tiene precio -> se le pone precio 0
	   UPDATE FA0300
	   SET FA03PRECIOFACT = 0,
         FA0300.FA15CODATRIB = RNF.CODATRIB
     WHERE FA03NUMRNF = RNF.FA03NUMRNF;
	END IF;

END LOOP;


RETURN TRUE;

EXCEPTION
	 WHEN OTHERS THEN
	 	  ROLLBACK;
	 	  RETURN FALSE;

END ValorarRNFs;


FUNCTION CargarRNFs(nProceso ad0700.ad07codproceso%TYPE, nAsistenci ad0100.ad01codasistenci%TYPE) RETURN BOOLEAN IS
/**********************************************************************************************************************
  FUNCION QUE CARGA LOS RNFS DE UN PROCESO-ASISTENCIA, ANTES SE BORRAN LOS QUE ESTUVIERAN PENDIENTES
**********************************************************************************************************************/

BEGIN
   -- cargar los nuevos rnfs en la fa0300
   INSERT INTO FA0300
	(FA03NUMRNF,FA05CODCATEG, FA03PRECIO, FA03PRECIOFACT, FA03CANTIDAD, FA03CANTFACT, FA13CODESTRNF,AD01CODASISTENCI,
	FA03FECHA,CI22NUMHISTORIA,FA03OBSERV,AD07CODPROCESO, FA03INDREALIZADO,
	AD02CODDPTO_R, SG02COD_R, AD02CODDPTO_S, SG02COD_S)
   		  SELECT
		  		  FA03NUMRNF_SEQUENCE.NEXTVAL, FA05CODCATEG, PRECIO, PRECIO, CANTIDAD,  CANTIDAD,  1,
				  AD01CODASISTENCI,  FECHA,  CI22NUMHISTORIA,
	  	  		  OBS , AD07CODPROCESO, ESTADO, DPTOREALIZA, DRREALIZA, DPTOSOLICITA, DRSOLICITA
   		  FROM FATMPJ T1
		  WHERE
	      		T1.AD07CODPROCESO = nProceso
	      		AND T1.AD01CODASISTENCI = nAsistenci
	      		AND NOT EXISTS
						(
				        SELECT AD07CODPROCESO , AD01CODASISTENCI, FA05CODCATEG, FA03FECHA, FA03CANTIDAD
				        FROM FA0300 T2
				        WHERE T2.AD07CODPROCESO = T1.AD07CODPROCESO
							AND T2.AD01CODASISTENCI=T1.AD01CODASISTENCI
							AND T2.FA05CODCATEG=T1.FA05CODCATEG
							AND T2.FA03CANTIDAD=T1.CANTIDAD AND T2.FA03FECHA=T1.FECHA
							AND T2.FA03INDFACT <> 3
				        );

RETURN TRUE;

EXCEPTION
	 WHEN OTHERS THEN
	 	  ROLLBACK;
	 	  RETURN FALSE;

END CargarRNFs;

FUNCTION GenerarLineas(FechaFact DATE) RETURN BOOLEAN IS
/**********************************************************************************************************************
  FUNCION QUE GENERA UNA LINEA DE FACTURA POR CADA DEPARTAMENTO
**********************************************************************************************************************/

   CURSOR curDPTO(Fecha DATE) IS
				SELECT ad02coddpto_r Codigo,
         		 MAX(AD0200.AD02DESDPTO) Desig,
         		 ROUND(SUM(fa03cantfact * fa03preciofact)) Importe
         FROM FA3700, FA0300, AD0200
				WHERE ci32codtipecon = CodTipEcon
					AND FA37MES = Fecha
					AND fa37indfact = 2
					AND FA3700.AD07CODPROCESO = FA0300.AD07CODPROCESO
					AND FA3700.AD01CODASISTENCI=FA0300.AD01CODASISTENCI
				  	AND fa04numfact IS NULL
				  	AND FA0300.AD02CODDPTO_R = AD0200.AD02CODDPTO
				GROUP BY ad02coddpto_r;
	NumFact NUMBER;
	CodFact NUMBER;
  nLinea NUMBER;
  TotalFact NUMBER;

BEGIN
		-- recoger el nuevo codigo de factura para las lineas
		SELECT fa04codfact_sequence.NEXTVAL INTO NumFact FROM dual;
		SELECT fa04codfact_sequence.NEXTVAL INTO CodFact FROM dual;

   nLinea := 0;
   TotalFact := 0;
	 --************************************************
		--   GRABAR LAS LINEAS ( UNA POR DEPARTAMENTO )
	 --************************************************
		FOR Dpto IN curDPTO(FechaFact) LOOP
			-- aumentar el contador de lineas
			nLinea := nLinea + 1;
      -- acumular el importe para el total de la factura
      TotalFact:=TotalFact+Dpto.Importe;
		  -- insertar la nueva linea
			INSERT INTO FA1600 (FA04CODFACT,FA16NUMLINEA,FA16DESCRIP,FA16IMPORTE,FA16ORDFACT)
			       VALUES (CodFact,nLinea,Dpto.Desig, Dpto.Importe, Dpto.Codigo);
		END LOOP;

	 --***************************************************
		--	 ACTUALIZAR LOS RNFS CON EL CODIGO DE LA FACTURA
	 --***************************************************
   
/*
	 --************************************************************
		--  GRABAR LA CABECERA ( una por cada proc-asist facturado )
	 --************************************************************
  	INSERT INTO FA0400 (FA04CODFACT,FA04NUMFACT,CI21CODPERSONA,CI13CODENTIDAD,FA04DESCRIP,FA04CANTFACT,FA04DESCUENTO,AD01CODASISTENCI,FA04OBSERV,
               FA09CODNODOCONC,AD07CODPROCESO,CI32CODTIPECON,FA04FECFACTURA, FA04INDNMGC, FA04FECDESDE, FA04FECHASTA, FA04INDCOMPENSA,
               FA09CODNODOCONC_FACT, FA04CODFACT_GARAN, FA04FECFINGARANTIA, FA04NUMACUMULGARAN )
               VALUES (CodFact,NumFact,?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'),?,?,?,TO_DATE(?,'DD/MM/YYYY'),?)
*/

	 --*******************************************************
		--  ACTUALIZAR EN LA FA3700 LOS PACIENTES YA FACTURADOS
	 --*******************************************************
   
   COMMIT;

RETURN TRUE;

EXCEPTION
	 WHEN OTHERS THEN
	 	  ROLLBACK;
	 	  RETURN FALSE;

END GenerarLineas;

FUNCTION FacturarEntidad(FechaFact DATE) RETURN BOOLEAN IS
/**********************************************************************************************************************
  FUNCTION      : FacturarEntidad()
  SCOPE         : PUBLIC

  DESCRIPTION   : Crea las facturas de una entidad ( de momento es una constante ) y aplicando
  						un concierto determinado ( de momento tambien es constante ), recogiendo todos
                  los pacientes pendientes de un determinado mes (parametro) y valorando todos sus rnfs
                  
  PARAMETERS IN : Mes ( se corresponde con el FA37MES ) determina que pacientes hay que facturar
  RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (error)
**********************************************************************************************************************/
   Estado BOOLEAN;
   -- cursor que contiene los pacientes de la fa3700 pendientes de facturar
   CURSOR curPacientes(Mes DATE) IS
   		  	SELECT * FROM FA3700
		  			WHERE ci32codtipecon = CodTipEcon
							AND fa37mes = Mes
							AND fa37indfact = 0;

BEGIN

		FOR Paciente IN curPacientes(FechaFact) LOOP

				-- borrar los rnfs del proc-asist que ya est�n en la FA0300
				Estado := BorrarRNFs(Paciente.AD07CODPROCESO, Paciente.AD01CODASISTENCI);

				-- Cargar los rnfs del proc-asist en la FA0300
				Estado := CargarRNFs(Paciente.AD07CODPROCESO, Paciente.AD01CODASISTENCI);

				-- valorar los rnfs del proc-asist
				Estado := ValorarRNFs(Paciente.AD07CODPROCESO, Paciente.AD01CODASISTENCI);

         -- marcar el paciente como facturado, de momento le ponemos un '2' para saber
         --	cuales est�n involucrados en esta factura, cuando se genere la factura completa
         -- se marcar�n definitivamente con '1'
		      UPDATE FA3700
		      SET FA37INDFACT = 2
		      WHERE FA37CODPAC = Paciente.FA37CODPAC;

      COMMIT;
		END LOOP;
   
		Estado := GenerarLineas(FechaFact);

   COMMIT;
RETURN TRUE;

EXCEPTION
	 WHEN OTHERS THEN
	 	  ROLLBACK;
	 	  RETURN FALSE;

END FacturarEntidad;


END FACTENTIDAD;
