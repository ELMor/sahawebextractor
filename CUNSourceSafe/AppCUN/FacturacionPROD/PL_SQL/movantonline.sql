   PROCEDURE movantonline (nc VARCHAR2) IS
      CURSOR c1 (v1 VARCHAR2, v2 VARCHAR2) IS
         SELECT *
           FROM r1mov_f
          WHERE nhist1 = v1
            AND ncaso1 = v2;

      CURSOR c2 (v1 VARCHAR2, v2 VARCHAR2) IS
         SELECT *
           FROM r2mov_f
          WHERE nhist2 = v1
            AND ncaso2 = v2;

      CURSOR c3 (v1 VARCHAR2, v2 VARCHAR2) IS
         SELECT *
           FROM r3mov_f
          WHERE nhist3 = v1
            AND ncaso3 = v2;

      CURSOR c5 (v1 VARCHAR2, v2 VARCHAR2) IS
         SELECT *
           FROM r5mov_f
          WHERE nhist5 = v1
            AND ncaso5 = v2;

      CURSOR c8 (v1 VARCHAR2, v2 VARCHAR2) IS
         SELECT *
           FROM r8mov_f
          WHERE nhist8 = v1
            AND ncaso8 = v2;

      v   NUMBER;
   BEGIN
      /*Traer movimientos historicos al OnLine*/
      SELECT COUNT (*)
        INTO v
        FROM r8mov
       WHERE numcaso = nc;

      IF v = 0 THEN                                       /* No estaba ya a�adido*/
         FOR r1 IN c1 (SUBSTR (nc, 1, 6), '0' || SUBSTR (nc, 7, 4)) LOOP
            INSERT INTO r1mov
                 VALUES (
                    r1.flag1,
                    r1.nhist1,
                    r1.ncaso1,
                    r1.nfact1,
                    r1.fsoli1,
                    r1.hsoli1,
                    r1.docto1,
                    r1.ssoli1,
                    r1.medic1,
                    r1.canti1,
                    r1.impor1,
                    r1.impss1,
                    r1.nhist1 || SUBSTR (r1.ncaso1, 2, 4)
                 );
         END LOOP;

         FOR r2 IN c2 (SUBSTR (nc, 1, 6), '0' || SUBSTR (nc, 7, 4)) LOOP
            INSERT INTO r2mov
                 VALUES (
                    r2.flag2,
                    r2.nhist2,
                    r2.ncaso2,
                    r2.nfact2,
                    r2.finic2,
                    r2.hinic2,
                    r2.nquir2,
                    r2.durac2,
                    r2.inter2,
                    r2.dintr2,
                    r2.impdq2,
                    r2.servc2,
                    r2.ciruj2,
                    r2.impci2,
                    r2.sanes2,
                    r2.anest2,
                    r2.impan2,
                    r2.serco2,
                    r2.circo2,
                    r2.impco2,
                    r2.nhist2 || SUBSTR (r2.ncaso2, 2, 4)
                 );
         END LOOP;

         FOR r3 IN c3 (SUBSTR (nc, 1, 6), '0' || SUBSTR (nc, 7, 4)) LOOP
            INSERT INTO r3mov
                 VALUES (
                    r3.flag3,
                    r3.nhist3,
                    r3.ncaso3,
                    r3.nfact3,
                    r3.fsoli3,
                    r3.hsoli3,
                    r3.docto3,
                    r3.ssoli3,
                    r3.medic3,
                    r3.canti3,
                    r3.impor3,
                    r3.inter3,
                    r3.demed3,
                    r3.impss3,
                    r3.nhist3 || SUBSTR (r3.ncaso3, 2, 4)
                 );
         END LOOP;

         FOR r5 IN c5 (SUBSTR (nc, 1, 6), '0' || SUBSTR (nc, 7, 4)) LOOP
            INSERT INTO r5mov
                 VALUES (
                    r5.flag5,
                    r5.nhist5,
                    r5.ncaso5,
                    r5.nfact5,
                    r5.fines5,
                    r5.hines5,
                    r5.cama5,
                    r5.servi5,
                    r5.canti5,
                    r5.impor5,
                    r5.ffine5,
                    r5.hfine5,
                    r5.fingr5,
                    r5.hingr5,
                    r5.nhist5 || SUBSTR (r5.ncaso5, 2, 4)
                 );
         END LOOP;

         FOR r8 IN c8 (SUBSTR (nc, 1, 6), '0' || SUBSTR (nc, 7, 4)) LOOP
            INSERT INTO r8mov
                 VALUES (
                    r8.flag8,
                    r8.nhist8,
                    r8.ncaso8,
                    r8.nfact8,
                    r8.fsoli8,
                    r8.hsoli8,
                    r8.docto8,
                    r8.ssoli8,
                    r8.freal8,
                    r8.hreal8,
                    r8.traba8,
                    r8.canti8,
                    r8.impor8,
                    r8.grsup8,
                    r8.impss8,
                    r8.nhist8 || SUBSTR (r8.ncaso8, 2, 4),
                    0
                 );
         END LOOP;

         COMMIT;
      END IF;
   END;
