   FUNCTION fafn04 (str VARCHAR2)
      RETURN VARCHAR2 IS
      str_rep   VARCHAR2 (100);
   BEGIN
      str_rep                    :=
        TRANSLATE (UPPER (fafn03 (str)),  '��������������������',  'AAAAEEEEIIIIOOOOUUUU');
      RETURN (str_rep);
   END fafn04;
