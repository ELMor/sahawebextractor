VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmFactAuto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Facturaci�n Autom�tica"
   ClientHeight    =   6795
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7515
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6795
   ScaleWidth      =   7515
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame3 
      Caption         =   "Mes a Facturar"
      Height          =   975
      Left            =   4440
      TabIndex        =   6
      Top             =   2040
      Width           =   3015
      Begin SSCalendarWidgets_A.SSDateCombo cbossMes 
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   2775
         _Version        =   65537
         _ExtentX        =   4895
         _ExtentY        =   661
         _StockProps     =   93
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Tipo Asistencia"
      Height          =   1935
      Left            =   4440
      TabIndex        =   4
      Top             =   0
      Width           =   3015
      Begin ComctlLib.ListView lstTipAsist 
         Height          =   1575
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   2778
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Desig"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tipo Econ�mico / Entidad"
      Height          =   6015
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   4335
      Begin ComctlLib.ListView lstTipEcon 
         Height          =   5655
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   4095
         _ExtentX        =   7223
         _ExtentY        =   9975
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   3
         BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Desig"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   1
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Tipo Econ�mico"
            Object.Width           =   353
         EndProperty
         BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
            SubItemIndex    =   2
            Key             =   ""
            Object.Tag             =   ""
            Text            =   "Entidad"
            Object.Width           =   353
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   495
      Left            =   6240
      TabIndex        =   1
      Top             =   6240
      Width           =   1215
   End
   Begin VB.CommandButton cmdFacturar 
      Caption         =   "&Facturar"
      Height          =   495
      Left            =   4800
      TabIndex        =   0
      Top             =   6240
      Width           =   1215
   End
End
Attribute VB_Name = "frmFactAuto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Function fSelectAsist() As String
    ' Se recogen los c�digos de tipo econ�mico/ entidad seleccionados
    
    Dim lstItem As ListItem
    Dim strTexto As String
    
    For Each lstItem In lstTipAsist.ListItems
        If lstItem.Selected Then
            strTexto = strTexto & Right(lstItem.Key, Len(lstItem.Key) - 1) & ", "
        End If
    Next
    
    If Len(strTexto) > 0 Then
        strTexto = Left(strTexto, Len(strTexto) - 2)
        fSelectAsist = "(" & strTexto & ")"
    End If

End Function

Private Function fSelectEnti() As String
    ' Se recogen los c�digos de tipo econ�mico/ entidad seleccionados
    
    Dim lstItem As ListItem
    Dim strTexto As String
    
    For Each lstItem In lstTipEcon.ListItems
        If lstItem.Selected Then
            strTexto = strTexto & "('" & lstItem.SubItems(1) _
                                & "','" & lstItem.SubItems(2) & "'), "
        End If
    Next
    
    If Len(strTexto) > 0 Then
        strTexto = Left(strTexto, Len(strTexto) - 2)
        fSelectEnti = "(" & strTexto & ")"
    End If
End Function

Private Sub pCargarListView()
    ' Se carga el litview con los diferentes tipos econ�micos existentes sin facturar
    ' en la tabla FA3700
    
    Dim SQL As String
    Dim rsTip As rdoResultset
    Dim lstItem As ListItem
    
    ' Tipo Econ�micos
    SQL = "SELECT    CI32CODTIPECON, " _
        & "         CI13CODENTIDAD, " _
        & "         CI13DESENTIDAD " _
        & " FROM    CI1300 " _
        & " WHERE   (CI32CODTIPECON, CI13CODENTIDAD) IN " _
        & "         (SELECT CI32CODTIPECON, CI13CODENTIDAD " _
        & "         FROM    FA3700 " _
        & "         WHERE   FA37INDFACT = 0 " _
        & "         )" _
        & " ORDER BY CI32CODTIPECON, " _
        & "         CI13CODENTIDAD "
        
    Set rsTip = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsTip.EOF
        Set lstItem = lstTipEcon.ListItems.Add(, , rsTip!CI13DESENTIDAD)
        lstItem.SubItems(1) = rsTip!CI32CODTIPECON
        lstItem.SubItems(2) = rsTip!CI13CODENTIDAD
        rsTip.MoveNext
    Loop
    rsTip.Close
    
    ' Tipos Asistencias
    SQL = "SELECT   AD12CODTIPOASIST, " _
        & "         AD12DESTIPOASIST " _
        & " FROM    AD1200 " _
        & " WHERE   (AD12CODTIPOASIST) IN " _
        & "         (SELECT AD12CODTIPOASIST " _
        & "         FROM    FA3700 " _
        & "         WHERE   FA37INDFACT = 0 " _
        & "         )" _
        & " ORDER BY AD12DESTIPOASIST "
        
    Set rsTip = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsTip.EOF
        Set lstItem = lstTipAsist.ListItems.Add(, "k" & rsTip!AD12CODTIPOASIST, rsTip!AD12DESTIPOASIST)
        rsTip.MoveNext
    Loop
    rsTip.Close
    
    With cbossMes
        .AllowNullDate = False
        .Date = Now
        .MaxDate = Now
        .MinDate = "1/12/1999"
    End With
    
End Sub

Private Sub pCrearRNFs(lngCodAsist As Long, lngCodProc As Long, strFechaInicio$, strFechaFin$)
    Dim SQL As String
    Dim qryRNF As rdoQuery
    Dim rsGrupos As rdoResultset
    Dim qryInsertRNF As rdoQuery
    
    SQL = "DELETE from FA0300 " & _
            " WHERE     AD07CODPROCESO = ? " _
            & "     AND AD01CODASISTENCI = ? " _
            & "     AND FA04NUMFACT is NULL " _
            & "     AND (FA03INDFACT = 0 OR FA03INDFACT IS NULL)"

    Set qryRNF = objApp.rdoConnect.CreateQuery("", SQL)
    qryRNF(0) = lngCodProc
    qryRNF(1) = lngCodAsist
    qryRNF.Execute
    
    
    SQL = "SELECT FA08DESVISTA " _
        & " FROM FA0800 " _
        & " WHERE FA11CODESTGRUPO = 1"
    Set rsGrupos = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsGrupos.EOF
        SQL = "INSERT INTO FA0300 " & _
                  "(FA03NUMRNF,FA05CODCATEG, FA03PRECIO, FA03PRECIOFACT, FA03CANTIDAD, FA03CANTFACT, FA13CODESTRNF,AD01CODASISTENCI, " & _
                  " FA03FECHA,CI22NUMHISTORIA,FA03OBSERV,AD07CODPROCESO, FA03INDREALIZADO, " & _
                  " AD02CODDPTO_R, SG02COD_R, AD02CODDPTO_S, SG02COD_S) " & _
                  "  SELECT " & _
                  "    FA03NUMRNF_SEQUENCE.NEXTVAL, FA05CODCATEG, PRECIO, PRECIO, CANTIDAD,  CANTIDAD,  1, " & _
                  "  AD01CODASISTENCI,  FECHA,  CI22NUMHISTORIA, " & _
                  "    OBS , AD07CODPROCESO, ESTADO, DPTOSOLICITA, DRSOLICITA, DPTOREALIZA, DRREALIZA " & _
                  "  From " & rsGrupos!FA08DESVISTA & " T1 " & _
                  "  Where " & _
                  "        T1.AD07CODPROCESO = ?" & _
                  "    AND T1.AD01CODASISTENCI = ?" & _
          IIf(strFechaInicio <> "", " AND T1.FECHA >= TO_DATE(? , 'DD/MM/YYYY') ", "") & _
          IIf(strFechaFin <> "", " AND T1.FECHA < TO_DATE(? , 'DD/MM/YYYY') ", "") & _
                  "    AND NOT EXISTS " & _
                  "     (SELECT " & _
                  "  AD07CODPROCESO , AD01CODASISTENCI, FA05CODCATEG, FA03FECHA, FA03CANTIDAD " & _
                  "      FROM FA0300 T2 " & _
                  "      Where " & _
                  "        T2.AD07CODPROCESO = T1.AD07CODPROCESO " & _
                  "     AND T2.AD01CODASISTENCI=T1.AD01CODASISTENCI " & _
                  "     AND T2.FA05CODCATEG=T1.FA05CODCATEG " & _
                  "     AND T2.FA03CANTIDAD=T1.CANTIDAD " & _
                  "     AND T2.FA03FECHA=T1.FECHA " & _
                  "     )"
        
        Set qryInsertRNF = objApp.rdoConnect.CreateQuery("", SQL)
        qryInsertRNF(0) = lngCodProc
        qryInsertRNF(1) = lngCodAsist
        If strFechaInicio <> "" Then
            qryInsertRNF(2) = Format$(strFechaInicio, "dd/mm/yyyy")
        End If
        If strFechaFin <> "" Then
            qryInsertRNF(2) = Format$(strFechaFin, "dd/mm/yyyy")
        End If
        qryInsertRNF.Execute
        qryInsertRNF.Close
        
        rsGrupos.MoveNext
    Loop
    rsGrupos.Close
End Sub

Private Sub pFacturarProcAsist(lngCodProc As Long, lngCodAsist As Long, _
                                strFechaInicio$, strFechaFin$, _
                                rsFact As rdoResultset)
    Dim varFact As New factura
    Dim objPaciente As New Paciente
    Dim objAsist As New Asistencia
    Dim objREco As Persona
    Dim objProcAsist As New ProcAsist
    Dim blnPacYaReco As Boolean
    Dim SQL As String
    Dim qryFact As rdoQuery
                                    
    ' Primer nivel
    '       DATOS DEL PACIENTE
    objPaciente.Codigo = rsFact!CI22NUMHISTORIA
    objPaciente.CodPersona = rsFact!CI21CODPERSONA
    
    varFact.Pacientes.Add objPaciente, CStr(objPaciente.Codigo)
    
    ' Segundo Nivel
    '       DATOS DEL PROCESO-ASISTENCIA
    objAsist.Codigo = lngCodProc & "-" & lngCodAsist
    If Not IsNull(rsFact!AD01FECINICIO) Then
        objAsist.FecInicio = Format$(rsFact!AD01FECINICIO, "dd/mm/yyyy hh:nn:ss")
    End If
    If Not IsNull(rsFact!AD01FECFIN) Then
        objAsist.FecFin = Format$(rsFact!AD01FECFIN, "dd/mm/yyyy hh:nn:ss")
    End If
    
    '       Fechas para la coletilla de la factura
    If Not IsNull(rsFact!FA37FECINICIO) Then
        objAsist.FecInicioFact = Format$(rsFact!FA37FECINICIO, "dd/mm/yyyy")
    ElseIf Not IsNull(rsFact!AD01FECINICIO) Then
        objAsist.FecInicioFact = Format$(rsFact!AD01FECINICIO, "dd/mm/yyyy")
    Else
        objAsist.FecInicioFact = ""
    End If
    If Not IsNull(rsFact!FA37FECFIN) Then
        objAsist.FecFinFact = Format$(rsFact!FA37FECFIN, "dd/mm/yyyy")
    ElseIf Not IsNull(rsFact!AD01FECFIN) Then
        objAsist.FecFinFact = Format$(rsFact!AD01FECFIN, "dd/mm/yyyy")
    Else
        objAsist.FecFinFact = ""
    End If
    '       Where de Selecci�n
    objAsist.strProcAsist = "(AD07CODPROCESO = " & lngCodProc & " AND AD01CODASISTENCI = " & lngCodAsist & ")"
    objAsist.Facturado = False
    
    
    '       3er nivel
    '       DATOS DE LOS CONCIERTOS
    '       DATOS DE LOS RECOS DE LA ASISTENCIA
    Do While lngCodProc = rsFact!AD07CODPROCESO And lngCodAsist = rsFact!AD01CODASISTENCI
        If Not IsNull(rsFact!FA09CODNODOCONC) Then
            ' Concierto
            objAsist.AddConcierto rsFact!FA09CODNODOCONC
                
            ' Reco
            Set objREco = New Persona
            If IsNull(rsFact!CI21CODPERSONA_RECO) Then
                ' Se pone el responsable el paciente si no tiene Reco
                objREco.Codigo = rsFact!CI21CODPERSONA
            Else
                objREco.Codigo = rsFact!CI21CODPERSONA_RECO
            End If
            objREco.Concierto.Concierto = rsFact!FA09CODNODOCONC
            objREco.CodTipEcon = rsFact!CI32CODTIPECON
            objREco.EntidadResponsable = rsFact!CI13CODENTIDAD
            objREco.DesEntidad = rsFact!CI13DESENTIDAD
            objREco.FecInicio = rsFact!AD11FECINICIO
            objREco.FecFin = IIf(IsNull(rsFact!AD11FECFIN), "", rsFact!AD11FECFIN)
            
            objAsist.AddREco objREco
        End If
        rsFact.MoveNext
        If rsFact.EOF Then Exit Do
    Loop
    
    '       Se mira si el paciente est� como Reco. Si no lo est� -> se a�ade
    blnPacYaReco = False
    For Each objREco In objAsist.REcos
        If objREco.Codigo = objPaciente.CodPersona Then
            blnPacYaReco = True
            Exit For
        End If
    Next
    If blnPacYaReco = False Then
        Set objREco = New Persona
        objREco.Codigo = objPaciente.CodPersona
        objAsist.AddREco objREco
    End If
    
    
    objPaciente.Asistencias.Add objAsist, CStr(objAsist.Codigo)
    
    
    '       4� nivel
    '       DATOS DE LOS PROCESOS-ASISTENCIAS
    objProcAsist.Asistencia = lngCodAsist
    objProcAsist.Proceso = lngCodProc
    objAsist.ProcAsist.Add objProcAsist
    
    '       5� nivel
    '       DATOS DE LOS RNFS

    If fFacturarProcAsist_CargarRNFs(lngCodProc, lngCodAsist, _
                                    strFechaInicio, strFechaFin, _
                                    rsFact, objAsist) Then
        ' Se generan las propuestas de factura
        varFact.AsignarRNFs
    
        ' Se guardan las diferentes propuestas en la base de datos
        Call pFacturarProcAsist_GuardarPropuestas(rsFact, varFact)
    End If
    
    varFact.Destruir
    
    Set varFact = Nothing
    ' Se se�ala que se ha facturado
    SQL = "UPDATE   FA3700" _
        & " SET     FA37INDFACT = 1 " _
        & " WHERE   FA37CODPAC = ? "
    Set qryFact = objApp.rdoConnect.CreateQuery("", SQL)
    qryFact(0) = rsFact!FA37CODPAC
    qryFact.Execute
    qryFact.Close
    ' ******************************
End Sub

Private Function fFacturarProcAsist_CargarRNFs(lngCodProc As Long, lngCodAsist As Long, _
                                    strFechaInicio$, strFechaFin$, _
                                    rsFact As rdoResultset, objAsist As Asistencia) As Boolean
                                    
    ' Se cargan los RNFs en la estructura de la asistencia
    
    Dim SQL As String
    Dim qryRNF As rdoQuery, rsRNF As rdoResultset
    Dim lngCodCategAnt As Long
    Dim lngNumCateg As Long
    Dim objRNF As rnf
    Dim objREco As Persona
    
    SQL = "SELECT   FA05CODCATEG, " _
                & " FA03NUMRNF, " _
                & " FA03FECHA, " _
                & " FA03CANTFACT, " _
                & " FA03PRECIOFACT, " _
                & " FA03OBSIMP, " _
                & " FA03OBSERV, " _
                & " FA03INDFACT "
    SQL = SQL & " FROM FA0300 " _
        & " WHERE   AD01CODASISTENCI = ?" _
        & "     AND AD07CODPROCESO = ?" _
        & "     AND FA03INDREALIZADO > 0" _
        & "     AND FA04NUMFACT IS NULL " _
        & " ORDER BY FA05CODCATEG, FA03FECHA"
    
    Set qryRNF = objApp.rdoConnect.CreateQuery("", SQL)
    qryRNF(0) = lngCodAsist
    qryRNF(1) = lngCodProc
    Set rsRNF = qryRNF.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsRNF.EOF
        fFacturarProcAsist_CargarRNFs = True
        If lngCodCategAnt <> rsRNF!fa05codcateg Then
            lngNumCateg = 0
            lngCodCategAnt = rsRNF!fa05codcateg
        Else
            lngNumCateg = lngNumCateg + 1
        End If
        
        Set objRNF = New rnf
        With objRNF
            .CodCateg = rsRNF!fa05codcateg
            .CodRNF = rsRNF!FA03NUMRNF
            .Fecha = rsRNF!FA03FECHA
            If IsNumeric(rsRNF!FA03CANTFACT) Then .Cantidad = rsRNF!FA03CANTFACT
            If IsNumeric(rsRNF!FA03PRECIOFACT) Then .Precio = rsRNF!FA03PRECIOFACT
            .Asignado = False
            .Name = rsRNF!FA03OBSIMP & ""
            .Descripcion = rsRNF!FA03OBSERV & ""
            If Not IsNull(rsRNF!FA03INDFACT) Then
                Select Case rsRNF!FA03INDFACT
                    Case constRNFPendiente  ' Pendiente de facturar
                        .Facturado = False
                        .Permanente = False
                    Case constRNFManual     ' Introducci�n o modificaci�n manual
                        .Facturado = False
                        .Permanente = True
'                    Case constRNFFacturado  ' Facturado
'                        .Facturado = True
'                        .Permanente = False
'                    Case constRNFAnulado    ' Se ha compensado (Anulado) con otra factura
'                        .Facturado = True
'                        .Permanente = False
'                    Case constRNFCompensa   ' Compensa otra factura
'                        .Facturado = True
'                        .Permanente = False
                    Case Else
                        .Facturado = True
                        .Permanente = False
                End Select
            End If
            .CodAsistenci = lngCodAsist
            .CodProceso = lngCodProc
            
            ' Se especifica el reco de cada RNF
            For Each objREco In objAsist.REcos
                If objREco.Concierto.Concierto <> 0 Then
                    .KeyREco = objREco.Key
                    If CVDate(objREco.FecInicio) < CVDate(.Fecha) Then Exit For
                End If
            Next
        End With
        ' Se guardan las propiedades del rnf susceptibles de modificaci�n para poder recuperarlas despu�s
        objRNF.Guardar
        ' A�adir RNF a la asistencia
        objAsist.RNFs.Add objRNF, objRNF.CodCateg & "-" & Format(lngNumCateg, "000")
        objRNF.Key = rsRNF!fa05codcateg & "-" & Format(lngNumCateg, "000")
        rsRNF.MoveNext
    Loop
    rsRNF.Close
    qryRNF.Close
    
End Function

Private Sub pFacturarProcAsist_GuardarPropuestas(rsFact As rdoResultset, varFact As factura)
    
    ' Se guardan en la base de datos las propuestas
    
    Dim objProp As PropuestaFactura
    Dim arProp() As Double
    Dim i As Integer
    Dim intIndexFact As Integer, lngPrioridadFact As Long
    Dim dblImporteFact As Double, strKeyProp As String
    Dim objREco As Persona
    Dim strCodFact As String
    Dim SQL As String
    Dim qryProp As rdoQuery
    
    Erase arProp
    
    lngPrioridadFact = 999999  ' Se inicializa as� para que cualquiera tenga prioridad mayor
    
    ' Se mira cual es la que tiene el orden menor , y se factura
    For Each objProp In varFact.Pacientes(1).Asistencias(1).PropuestasFactura
        i = i + 1
        ReDim Preserve arProp(1 To 3, 1 To i)
        arProp(1, i) = objProp.Prioridad
        arProp(2, i) = objProp.Importe
        arProp(3, i) = objProp.Name
        
        If arProp(1, i) < lngPrioridadFact _
            Or (arProp(1, i) = lngPrioridadFact And arProp(2, i) > dblImporteFact) Then
            ' Si la nueva propuesta es de mayor prioridad
            ' O si la prioridad es igual, pero el importe es mayor
            intIndexFact = i
            lngPrioridadFact = arProp(1, i)
            dblImporteFact = arProp(2, i)
        End If
    Next
    
    If intIndexFact > 0 Then
        ' Se indica la propuesta a facturar
        ' Como Key de la propuesta se utiliza el nombre
        Call varFact.Pacientes(1).Asistencias(1).GrabarFactura _
                    (arProp(3, intIndexFact), _
                    IIf(IsNull(rsFact!FA37MES), Format$(Now, "dd/mm/yyyy"), Format$(rsFact!FA37MES, "dd/mm/yyyy")), _
                    rsFact!CI32CODTIPECON & rsFact!CI13CODENTIDAD & rsFact!AD12CODTIPOASIST)
        
        ' Se busca el reco que se est� facturando
        For Each objREco In varFact.Pacientes(1).Asistencias(1).REcos
            If objREco.Codigo = rsFact!CI21CODPERSONA_RECO Then
                strCodFact = objREco.CodFactura
                Exit For
            End If
        Next
        
        SQL = "INSERT INTO FA3600" _
            & "(FA36NUMPROP,FA04CODFACT, FA36NUMORDEN, FA36DESIG, FA36IMPORTE, FA36INDFACT) " _
            & " VALUES " _
            & "(FA36NUMPROP_SEQUENCE.NEXTVAL,?,?,?,?,?)"
        Set qryProp = objApp.rdoConnect.CreateQuery("", SQL)
            
        ' Se guardan los datos en la tabla de propuestas
        For i = 1 To UBound(arProp, 2)
            qryProp(0) = strCodFact
            qryProp(1) = arProp(1, i)
            qryProp(2) = arProp(3, i)
            qryProp(3) = arProp(2, i)
            If i = intIndexFact Then
                qryProp(4) = 1
            Else
                qryProp(4) = 0
            End If
            qryProp.Execute
        Next i
    End If
End Sub

Private Sub cmdFacturar_Click()
    Dim SQL As String
    Dim qryFact As rdoQuery, rsFact As rdoResultset
    Dim lngProcAnt As Long, lngAsistAnt As Long
    Dim strProcAsist As String, ProcAsist As String
    
    Dim strEnti As String, strTipAsist As String
    Dim strFechaInicio$, strFechaFin$
    ' Se localizan todos los datos que cumplan las condiciones
    ' y no est�n facturados
    
    strEnti = fSelectEnti()
    strTipAsist = fSelectAsist()
    
    If strEnti <> "" And strTipAsist <> "" Then
        Screen.MousePointer = vbHourglass
        SQL = "SELECT   /*+ RULE */ FA3700.FA37CODPAC, " _
            & "         FA3700.AD01CODASISTENCI, " _
            & "         FA3700.AD07CODPROCESO, " _
            & "         FA3700.CI32CODTIPECON, " _
            & "         FA3700.CI13CODENTIDAD, " _
            & "         FA3700.FA37FECINICIO, " _
            & "         FA3700.FA37FECFIN, " _
            & "         FA3700.FA37MES, " _
            & "         AD0100.CI22NUMHISTORIA, " _
            & "         AD0100.CI21CODPERSONA AS CI21CODPERSONA, " _
            & "         AD0100.AD01FECINICIO, " _
            & "         AD0100.AD01FECFIN, " _
            & "         AD2500.AD12CODTIPOASIST, " _
            & "         CI1300.FA09CODNODOCONC, " _
            & "         AD1100.CI32CODTIPECON, " _
            & "         AD1100.CI13CODENTIDAD, " _
            & "         AD1100.CI21CODPERSONA AS CI21CODPERSONA_RECO, " _
            & "         CI1300.CI13DESENTIDAD, " _
            & "         AD1100.AD11FECINICIO, " _
            & "         AD1100.AD11FECFIN "
        SQL = SQL & " FROM  FA3700, " _
            & "             AD0100, " _
            & "             AD1100, " _
            & "             CI1300, " _
            & "             AD2500 "
        SQL = SQL & " WHERE     AD0100.AD01CODASISTENCI = FA3700.AD01CODASISTENCI " _
                    & "     AND AD1100.AD01CODASISTENCI = FA3700.AD01CODASISTENCI " _
                    & "     AND AD1100.AD07CODPROCESO = FA3700.AD07CODPROCESO " _
                    & "     AND CI1300.CI32CODTIPECON = AD1100.CI32CODTIPECON " _
                    & "     AND CI1300.CI13CODENTIDAD = AD1100.CI13CODENTIDAD " _
                    & "     AND AD2500.AD01CODASISTENCI = AD0100.AD01CODASISTENCI " _
                    & "     AND AD2500.AD25FECFIN IS NULL " _
                    & "     AND FA37INDFACT=0" _
                    & "     AND (FA3700.CI32CODTIPECON , FA3700.CI13CODENTIDAD) IN " & strEnti _
                    & "     AND FA3700.AD12CODTIPOASIST IN " & strTipAsist _
                    & "     AND FA3700.FA37MES = TO_DATE('1/" & Format$(cbossMes.Date, "mm/yyyy") & "','DD/MM/YYYY') "
        '************************************************
        ' AQU� HAY QUE PONER EL RESTO DE CONDICIONES
        '           1.- Tipo Econ�mico
        '           2.- Entidad
        '           3.- Fechas
        '           4.- Hospitalizado-Ambulatorio
        '***********************************************
        SQL = SQL & " ORDER BY  AD0100.CI21CODPERSONA, " _
                    & "         FA3700.AD07CODPROCESO, " _
                    & "         FA3700.AD01CODASISTENCI, " _
                    & "         AD1100.AD11FECINICIO "
        
        Set qryFact = objApp.rdoConnect.CreateQuery("", SQL)
        
        Set rsFact = qryFact.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        Do While Not rsFact.EOF
            If lngProcAnt <> rsFact!AD07CODPROCESO Or lngAsistAnt <> rsFact!AD01CODASISTENCI Then
                ' Cambio de Proceso Asistencia
                lngProcAnt = rsFact!AD07CODPROCESO
                lngAsistAnt = rsFact!AD01CODASISTENCI
                
                If Not IsNull(rsFact!FA37FECINICIO) Then
                    strFechaInicio = rsFact!FA37FECINICIO
                Else
                    strFechaInicio = ""
                End If
                If Not IsNull(rsFact!FA37FECFIN) Then
                    strFechaFin = rsFact!FA37FECFIN
                Else
                    strFechaFin = ""
                End If
                
                
                ' Se crean los RNFs del ProcesoAsistencia
                Call pCrearRNFs(lngProcAnt, lngAsistAnt, strFechaInicio, strFechaFin)
                
                Call pFacturarProcAsist(lngProcAnt, lngAsistAnt, strFechaInicio, strFechaFin, rsFact)
            End If
        Loop
        rsFact.Close
        qryFact.Close
    ElseIf strTipAsist = "" Then
        MsgBox "Seleccione los tipos asistencias a facturar", vbInformation, "Facturaci�n"
    Else
        MsgBox "Seleccione los tipos econ�micos / entidades a facturar", vbInformation, "Facturaci�n"
    End If
    Screen.MousePointer = vbDefault
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub


Private Sub Form_Load()
    Call pCargarListView
End Sub

