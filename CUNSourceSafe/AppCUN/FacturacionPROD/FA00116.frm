VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frm_Propuestas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Seleccione una propuesta de factura"
   ClientHeight    =   3060
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11010
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3060
   ScaleWidth      =   11010
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   600
      Left            =   9810
      TabIndex        =   0
      Top             =   90
      Width           =   1140
   End
   Begin SSDataWidgets_B.SSDBGrid grdPropuestas 
      Height          =   2895
      Left            =   90
      TabIndex        =   1
      Top             =   45
      Width           =   9570
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      Col.Count       =   4
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Paciente"
      Columns(0).Name =   "Paciente"
      Columns(0).AllowSizing=   0   'False
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   13229
      Columns(1).Caption=   "Propuesta"
      Columns(1).Name =   "Propuesta"
      Columns(1).AllowSizing=   0   'False
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   2646
      Columns(2).Caption=   "Importe"
      Columns(2).Name =   "Importe"
      Columns(2).Alignment=   1
      Columns(2).AllowSizing=   0   'False
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "Key"
      Columns(3).Name =   "Key"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   16880
      _ExtentY        =   5106
      _StockProps     =   79
      Caption         =   "Propuestas de Factura"
   End
End
Attribute VB_Name = "frm_Propuestas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private varFact As factura
Private Paciente As Paciente
Private Asistencia As Asistencia
Private NuevoRNF As rnf
Private ConciertoAplicable As Concierto
Private Entidad As Nodo
Private PropuestaFactura As PropuestaFactura

Public Function pPropuestas(objFactura As factura) As String
   Set varFact = objFactura
   Load frm_Propuestas
   frm_Propuestas.Show (vbModal)
           
   
   varFact.Destruir
   Set varFact = Nothing

End Function

Private Sub cmdAceptar_Click()
   Dim Posicion As Integer
   Dim Texto As String
   Dim Propuesta As String
   
   MsgBox "Ha seleccionado la propuesta " & grdPropuestas.Columns("Propuesta").Text
   
   If Me.grdPropuestas.Columns("Propuesta").Text <> "" Then
      Texto = grdPropuestas.Columns("Paciente").Text
   
'      Propuesta = Texto & "/" & Me.grdPropuestas.Columns("Propuesta").Text
      Propuesta = Me.grdPropuestas.Columns("Paciente").Text & "/" & Me.grdPropuestas.Columns("Key").Text
      frmEditFactura.pFactura varFact, Propuesta
   
      If ContinuarFactura Then
         Call Form_Load
      Else
         Unload Me
      End If
   Else
      Unload Me
   End If
End Sub

Private Sub Form_Load()
    Dim txtNombre As String
    Dim txtHistoria As String
    Dim txtAsistencia As String
    Dim aTextos() As String
    Dim aImportes() As Double
    Dim aPrioridad() As Integer
    Dim i As Integer
    Dim ii As Integer
    Dim iMax As Integer
    Dim Importe As Double
    Dim Prioridad As Integer
    
     Me.grdPropuestas.RemoveAll
   ' Primero se meten las descripciones de las propuestas y sus importes
    ' en arrays para despues mostrarlas ordenadas por importe.
    For Each Paciente In varFact.Pacientes
        txtHistoria = Paciente.Codigo
        For Each Asistencia In Paciente.Asistencias
            txtAsistencia = txtHistoria & "-" & Asistencia.Codigo
            If Asistencia.PropuestasFactura.Count > 0 Then
                ReDim aTextos(1 To Asistencia.PropuestasFactura.Count) As String
                ReDim aImportes(1 To Asistencia.PropuestasFactura.Count) As Double
                ReDim aPrioridad(1 To Asistencia.PropuestasFactura.Count) As Integer
                i = 1
                For Each PropuestaFactura In Asistencia.PropuestasFactura
                    If Asistencia.Facturado = False Then
                        aImportes(i) = PropuestaFactura.Importe
                        aPrioridad(i) = PropuestaFactura.Prioridad
                        
                        txtNombre = txtAsistencia & Chr(9) & PropuestaFactura.Name & Chr(9) & Format(aImportes(i), "###,###,###,##0") & Chr(9) & PropuestaFactura.Key
                        
                        aTextos(i) = txtNombre
                        
                        i = i + 1
                    End If
                Next
                
                '''''''''
                ' ahora se muestran ordenadas por la prioridad
'                For i = 1 To UBound(aTextos)
'                    Importe = aImportes(1)
'                    iMax = 1
'                    For ii = 2 To UBound(aTextos)
'                        If aImportes(ii) > Importe Then
'                            Importe = aImportes(ii)
'                            iMax = ii
'                        End If
'                    Next
'
'                    Me.grdPropuestas.AddItem aTextos(iMax)
'
'                    aImportes(iMax) = -1
'                Next

                For i = 1 To UBound(aTextos)
                    Prioridad = aPrioridad(1)
                    Importe = aImportes(1)
                    iMax = 1
                    For ii = 2 To UBound(aTextos)
                        If (aPrioridad(ii) < Prioridad) Or _
                           (aPrioridad(ii) = Prioridad And aImportes(ii) > Importe) Then
                            Prioridad = aPrioridad(ii)
                            Importe = aImportes(ii)
                            iMax = ii
                        End If
                    Next
                    
                    Me.grdPropuestas.AddItem aTextos(iMax)
                    
                    aImportes(iMax) = -1
                    aPrioridad(iMax) = 32600
                Next
                '''''''''
            Else
                ReDim aTextos(0) As String
                ReDim aImportes(0) As Double
                ReDim aPrioridad(0) As Integer
            End If
            
            
        Next
    Next

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode = 0 Then
    BotonSalir = True
  End If
End Sub

Private Sub grdPropuestas_DblClick()
    Call cmdAceptar_Click
End Sub

