VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_Conceptos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Conceptos Agrupantes"
   ClientHeight    =   3840
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3840
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Conceptos Agrupantes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Index           =   0
      Left            =   90
      TabIndex        =   5
      Top             =   450
      Width           =   9420
      Begin TabDlg.SSTab tabTab1 
         Height          =   2460
         Index           =   0
         Left            =   135
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   360
         Width           =   9150
         _ExtentX        =   16140
         _ExtentY        =   4339
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FA00102.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(5)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "SDCConcep(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "SDCConcep(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtConceptos(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtConceptos(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtConceptos(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtConceptos(3)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "cboActivo(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FA00102.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBCombo cboActivo 
            DataField       =   "FA12CODESTCAGRUP"
            Height          =   255
            Index           =   0
            Left            =   1755
            TabIndex        =   14
            Tag             =   "Activo|Activo/Inactivo"
            Top             =   1335
            Width           =   2055
            DataFieldList   =   "column 0"
            _Version        =   131078
            DataMode        =   2
            Cols            =   2
            Columns(0).Width=   3200
            _ExtentX        =   3625
            _ExtentY        =   450
            _StockProps     =   93
            ForeColor       =   -2147483640
            BackColor       =   16777215
            DataFieldToDisplay=   "column 1"
         End
         Begin VB.TextBox txtConceptos 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FA02DESPROCALM"
            Height          =   285
            Index           =   3
            Left            =   1755
            MaxLength       =   50
            TabIndex        =   11
            Tag             =   "Proc. Alm.|Proceso Almacenado"
            Top             =   975
            Width           =   6255
         End
         Begin VB.TextBox txtConceptos 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FA02CODCAGRUP"
            Height          =   315
            Index           =   0
            Left            =   6180
            MaxLength       =   3
            TabIndex        =   0
            Tag             =   "C�digo|C�digo de Concepto Agrupante"
            Top             =   240
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.TextBox txtConceptos 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FA02DESVISTA"
            Height          =   285
            Index           =   2
            Left            =   1755
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Vista|Nombre de la vista"
            Top             =   600
            Width           =   6255
         End
         Begin VB.TextBox txtConceptos 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FA02DESIG"
            Height          =   285
            Index           =   1
            Left            =   1755
            MaxLength       =   20
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n del Concepto"
            Top             =   240
            Width           =   3375
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2145
            Index           =   0
            Left            =   -74910
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   90
            Width           =   8655
            _Version        =   131078
            DataMode        =   2
            RecordSelectors =   0   'False
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15266
            _ExtentY        =   3784
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo SDCConcep 
            DataField       =   "FA02FECINICIO"
            Height          =   285
            Index           =   0
            Left            =   1755
            TabIndex        =   15
            Tag             =   "Fecha Inicio"
            Top             =   1665
            Width           =   1725
            _Version        =   65537
            _ExtentX        =   3043
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483634
            Enabled         =   0   'False
            MinDate         =   "1900/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo SDCConcep 
            DataField       =   "FA02FECFIN"
            Height          =   285
            Index           =   1
            Left            =   1755
            TabIndex        =   16
            Tag             =   "Fecha Fin"
            Top             =   2025
            Width           =   1725
            _Version        =   65537
            _ExtentX        =   3043
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483634
            Enabled         =   0   'False
            MinDate         =   "1900/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Inicio Validez:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   315
            TabIndex        =   18
            Top             =   1665
            Width           =   1275
         End
         Begin VB.Label lblLabel1 
            Alignment       =   1  'Right Justify
            Caption         =   "Final Validez:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   90
            TabIndex        =   17
            Top             =   2025
            Width           =   1500
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Activo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   960
            TabIndex        =   13
            Top             =   1335
            Width           =   720
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Proc. Almacen.:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   0
            Left            =   240
            TabIndex        =   12
            Top             =   960
            Width           =   1440
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Vista:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   1080
            TabIndex        =   10
            Top             =   570
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   5400
            TabIndex        =   9
            Top             =   240
            Visible         =   0   'False
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   480
            TabIndex        =   8
            Top             =   225
            Width           =   1200
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   6
      Top             =   3435
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   714
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Conceptos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objDetailInfo As New clsCWForm
Dim FechaDia As Date
Private Sub Form_Load()
  Dim strKey As String
  Dim SQL As String
  Dim MiRsFecha As rdoResultset
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

  With objDetailInfo
    .strName = "Conceptos"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "FA0200"
    Call .FormAddOrderField("FA02CODCAGRUP", cwAscending)
    .blnHasMaint = False
    .blnAskPrimary = False
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "FA0200")
    'Call .FormAddFilterWhere(strKey, "FA02CODCAGRUP", "C�digo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FA02DESIG", "Designaci�n", cwString)
    'Call .FormAddFilterWhere(strKey, "FA02DESVISTA", "Vista", cwString)
    'Call .FormAddFilterWhere(strKey, "FA02DESPROCALM", "Proceso Alm.", cwString)
    Call .FormAddFilterWhere(strKey, "FA12CODESTCAGRUP", "�Activo?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FA02FECINICIO", "Fec. Inicio", cwDate)
    Call .FormAddFilterWhere(strKey, "FA02FECFIN", "Fec. Final", cwDate)
        
    Call .FormAddFilterOrder(strKey, "FA02CODCAGRUP", "C�digo")
    Call .FormAddFilterOrder(strKey, "FA02DESIG", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "FA02DESVISTA", "Vista")
    Call .FormAddFilterOrder(strKey, "FA02DESPROCALM", "Proceso Alm.")
    Call .FormAddFilterOrder(strKey, "FA12CODESTCAGRUP", "�Activo?")
    
    Call .objPrinter.Add("FA01001", "Listado de Conceptos Agrupantes")

  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
    .CtrlGetInfo(cboActivo(0)).blnInFind = True
    .CtrlGetInfo(cboActivo(0)).blnInGrid = True
    .CtrlGetInfo(txtConceptos(0)).blnValidate = False
    .CtrlGetInfo(txtConceptos(0)).blnInFind = True
    .CtrlGetInfo(txtConceptos(0)).blnInGrid = True
    .CtrlGetInfo(txtConceptos(1)).blnInFind = True
    .CtrlGetInfo(txtConceptos(1)).blnInGrid = True
    .CtrlGetInfo(txtConceptos(2)).blnInFind = True
    .CtrlGetInfo(txtConceptos(3)).blnInFind = True
      
    SQL = "SELECT FA12CODESTCAGRUP, FA12DESIG FROM FA1200"
    .CtrlGetInfo(cboActivo(0)).strSQL = SQL
    '.CtrlGetInfo(cboActivo(0)).blnForeign = True

    Call .WinRegister
    Call .WinStabilize
  End With
  grdDBGrid1(0).Columns(0).Width = 500
  grdDBGrid1(0).Columns(1).Width = 1000
  grdDBGrid1(0).Columns(2).Width = 2000
  grdDBGrid1(0).Columns(3).Width = 2000
  grdDBGrid1(0).Columns(4).Width = 2000
  grdDBGrid1(0).Columns(5).Width = 1500
  Me.Refresh
  Set MiRsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not MiRsFecha.EOF Then
    FechaDia = MiRsFecha(0)
  End If
  'SDCConcep(0).MinDate = FechaDia
  SDCConcep(1).MinDate = FechaDia
  'Comprobamos que no esta dado de baja el registro y en caso afirmativo desactivamos todo
  If SDCConcep(1).Date <> "31/12/3000" Then
    txtConceptos(0).Enabled = False
    txtConceptos(1).Enabled = False
    txtConceptos(2).Enabled = False
    txtConceptos(3).Enabled = False
    cboActivo(0).Enabled = False
    SDCConcep(0).Enabled = False
    SDCConcep(1).Enabled = False
  Else
    txtConceptos(0).Enabled = True
    txtConceptos(1).Enabled = True
    txtConceptos(2).Enabled = True
    txtConceptos(3).Enabled = True
    cboActivo(0).Enabled = True
    SDCConcep(0).Enabled = True
    SDCConcep(1).Enabled = True
  End If
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    
    If strFormName = objDetailInfo.strName Then
        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
            txtConceptos(0).Text = fNextClave("FA02CODCAGRUP", "FA0200")
            objDetailInfo.rdoCursor("FA02CODCAGRUP") = txtConceptos(0).Text
        End If
    End If
    
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)

  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  Select Case strFormName
    Case objDetailInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
  
  End Select

End Sub

Private Sub SDCConcep_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
  tlbToolbar1.Buttons(2).Enabled = True
End Sub

Private Sub SDCConcep_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
  SDCConcep(Index).MinDate = "1/1/1900"
End Sub

Private Sub SDCConcep_DropDown(Index As Integer)
  SDCConcep(Index).MinDate = FechaDia
End Sub


Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tlbToolbar1_ButtonClick(ByVal btnButton As Button)
Dim Resp As Integer
  SDCConcep(0).MinDate = "1/1/1900"
  If btnButton.Index = 8 Then
    If SDCConcep(1).Date = "31/12/3000" Then
      Resp = MsgBox("Para dar de baja un concepto agrupante deberemos modificar su fecha de fin de validez", vbYesNo + vbInformation, "Aviso")
      If Resp = vbYes Then
        SDCConcep(1).Date = FechaDia
      End If
    Else
      MsgBox "Concepto agrupante ya dado de baja", vbOKOnly + vbInformation, "Aviso"
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    If btnButton.Index = 2 Then
      SDCConcep(0).Date = FechaDia
      SDCConcep(1).Date = "31/12/3000"
      SDCConcep(1).Enabled = False
    Else
      SDCConcep(1).Enabled = True
    End If
    
    If Trim(SDCConcep(0).Date) <> "" Then
      If SDCConcep(0).Date < Format(FechaDia, "DD/MM/YYYY") Then
        SDCConcep(0).Enabled = False
      Else
        SDCConcep(0).Enabled = True
      End If
    Else
      SDCConcep(0).Enabled = True
    End If
    If SDCConcep(1).Date <> "31/12/3000" Then
      txtConceptos(0).Enabled = False
      txtConceptos(1).Enabled = False
      txtConceptos(2).Enabled = False
      txtConceptos(3).Enabled = False
      cboActivo(0).Enabled = False
      SDCConcep(0).Enabled = False
      SDCConcep(1).Enabled = False
    Else
      txtConceptos(0).Enabled = True
      txtConceptos(1).Enabled = True
      txtConceptos(2).Enabled = True
      txtConceptos(3).Enabled = True
      cboActivo(0).Enabled = True
      SDCConcep(0).Enabled = True
      SDCConcep(1).Enabled = True
    End If
  End If

End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub

Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

Private Sub cboactivo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboactivo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboactivo_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtConceptos_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtConceptos_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtConceptos_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

