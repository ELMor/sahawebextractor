
   CREATE OR REPLACE VIEW fa1502 (
      fa15codconc,
      designacion,
      fa15precioref,
      fa15descuento
   ) AS
      SELECT t1.fa15codconc,
             LPAD ('.', 2 * LEVEL) || DECODE (
                                         t1.fa15escat, 0, fa09desig, 
   fa05desig
                                      ) designacion,
             fa15precioref,
             fa15descuento
        FROM fa1501 t1,
             fa1500 t2,
             fa0900 t3,
             fa0500 t4
       WHERE t1.fa15codatrib = t2.fa15codatrib
         AND (
                   (    t1.fa15escat = 0
                    AND t1.fa15codfin = t3.fa09codnodoconc)
                OR (    t1.fa15escat = -1
                    AND t1.fa15codfin = t4.fa05codcateg)
             )
