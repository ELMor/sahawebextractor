   CREATE OR REPLACE VIEW fa0004j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic,
      grupo,
      PVL,
      codExt
   ) AS
      SELECT /*+ rule */
   /* FA0004J: Facturación Farmacia:  No facturable por dosis,producto 1 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (SIGN (fr6500.fr65cantidad) * CEIL (ABS (fr6500.fr65cantidad))),
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
             1,
             DECODE (
                fr6500.fr73codproducto, 999999999, fr6500.fr65precio,
                FRQ100.FRQ1PRECIOVENTA
             ),
             DECODE(FR65INDINTERCIENT,-1,-1,0),
	     '0'||FR7300.FR00CODGRPTERAP,
             DECODE (
                fr6500.fr73codproducto, 999999999, fr6500.fr65precio,
                FRQ100.FRQ1PRECIOVENTACON
             ),
	     FR7300.FR73CODPRODUCTO
        FROM 
             fr6600,
             fr7300,
             ci2200,
             fa0500,
	     FRQ100,
	     fr6500
       WHERE fa0500.fa08codgrupo (+)= 1
         AND fa0500.fa05codorigen (+)= fr7300.fr73codintfar
         AND fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND (   (fr7300.fr73indfactdosis IS NULL)
              OR (fr7300.fr73indfactdosis = 0))
         AND fr6500.pr62codhojaquir IS NULL
	 AND FRQ100.fr73codproducto = FR6500.fr73codproducto
	 AND fr65fecha	BETWEEN
				FRQ100.FRQ1FECINICIO
			AND
				NVL(FRQ100.FRQ1FECFIN,TO_DATE('31/12/3000','DD/MM/YYYY'))
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
                1,
                DECODE (
                  fr6500.fr73codproducto, 999999999, fr6500.fr65precio,
                  FRQ100.FRQ1PRECIOVENTA
                ),                DECODE(FR65INDINTERCIENT,-1,-1,0),
		'0'||FR7300.FR00CODGRPTERAP,
                DECODE (
                  fr6500.fr73codproducto, 999999999, fr6500.fr65precio,
                  FRQ100.FRQ1PRECIOVENTACON
                ),
		FR7300.FR73CODPRODUCTO
      UNION ALL
      SELECT /*+ rule */
   /* FA0004J: Facturación Farmacia:  No facturable por dosis, diluyente */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (CEIL (fr6500.fr65cantidaddil / fr7300.fr73volumen)),
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
             1,
             TO_NUMBER (FRQ100.FRQ1PRECIOVENTA),
             DECODE(FR65INDINTERCIENT,-1,-1,0),
	     '0'||FR7300.FR00CODGRPTERAP,
             TO_NUMBER (FRQ100.FRQ1PRECIOVENTACON),
	     FR7300.FR73CODPRODUCTO
        FROM 
             fr6600,
             fr7300,
             ci2200,
             fa0500,
	     FRQ100,
	     fr6500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_dil = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND (   (fr7300.fr73indfactdosis IS NULL)
              OR (fr7300.fr73indfactdosis = 0))
         AND fr6500.fr73codproducto_dil IS NOT NULL
         AND fa0500.fa05codorigen (+)= fr7300.fr73codintfar
         AND fa0500.fa08codgrupo (+)= 1
         AND fr7300.fr73volumen > 0
         AND fr6500.pr62codhojaquir IS NULL
	 AND FRQ100.fr73codproducto = FR6500.fr73codproducto_dil
	 AND fr65fecha	BETWEEN
				FRQ100.FRQ1FECINICIO
			AND
				NVL(FRQ100.FRQ1FECFIN,TO_DATE('31/12/3000','DD/MM/YYYY'))
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
                1 ,
		TO_NUMBER (FRQ100.FRQ1PRECIOVENTA),
                DECODE(FR65INDINTERCIENT,-1,-1,0),
		'0'||FR7300.FR00CODGRPTERAP,
		TO_NUMBER (FRQ100.FRQ1PRECIOVENTACON),
		FR7300.FR73CODPRODUCTO
      UNION ALL
      SELECT /*+ rule */
   /* FA0004J: Facturación Farmacia:  No facturable por dosis, producto 2 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (CEIL (
                     DECODE (
                        fr7300.fr73dosis, NULL, 1, 0,1, fr6500.fr65dosis_2 /
                                                      fr7300.fr73dosis
                     )
                  )) "CANTIDAD",
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
             1,
             TO_NUMBER (FRQ100.FRQ1PRECIOVENTA),
             DECODE(FR65INDINTERCIENT,-1,-1,0),
             '0'||FR7300.FR00CODGRPTERAP,
	     TO_NUMBER (FRQ100.FRQ1PRECIOVENTACON),
	     FR7300.FR73CODPRODUCTO
        FROM 
             fr6600,
             fr7300,
             ci2200,
             fa0500,
	     FRQ100,
	     fr6500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_2 = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND (   (fr7300.fr73indfactdosis IS NULL)
              OR (fr7300.fr73indfactdosis = 0))
         AND fr6500.fr73codproducto_2 IS NOT NULL
         AND fr6500.fr65dosis_2 IS NOT NULL
         AND fa0500.fa05codorigen (+)= fr7300.fr73codintfar
         AND fa0500.fa08codgrupo (+)= 1
         AND fr6500.pr62codhojaquir IS NULL
	 AND FRQ100.fr73codproducto = FR6500.fr73codproducto_2
	 AND FR6500.fr65fecha	BETWEEN
				FRQ100.FRQ1FECINICIO
			AND
				NVL(FRQ100.FRQ1FECFIN,TO_DATE('31/12/3000','DD/MM/YYYY'))
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
                1,
		TO_NUMBER (FRQ100.FRQ1PRECIOVENTA),
                DECODE(FR65INDINTERCIENT,-1,-1,0),
		'0'||FR7300.FR00CODGRPTERAP,
		TO_NUMBER (FRQ100.FRQ1PRECIOVENTACON),
		FR7300.FR73CODPRODUCTO
      UNION ALL
      SELECT /*+ rule */
   /* FA0004J: Facturación Farmacia:  facturable por dosis, producto 1 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SIGN (SUM (fr6500.fr65cantidad)) *
                CEIL (10 * ABS (SUM (fr6500.fr65cantidad))) /
                10,
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
             1,
             TO_NUMBER (FRQ100.FRQ1PRECIOVENTA),
             DECODE(FR65INDINTERCIENT,-1,-1,0),
	     '0'||FR7300.FR00CODGRPTERAP,
	     TO_NUMBER (FRQ100.FRQ1PRECIOVENTACON),
	     FR7300.FR73CODPRODUCTO
        FROM 
             fr6600,
             fr7300,
             ci2200,
             fa0500,
	     FRQ100,
	     fr6500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND fr7300.fr73indfactdosis = -1
         AND fa0500.fa05codorigen (+)= fr7300.fr73codintfar
         AND fa0500.fa08codgrupo (+)= 1
         AND fr6500.pr62codhojaquir IS NULL
	 AND FRQ100.fr73codproducto = FR6500.fr73codproducto
	 AND FR6500.fr65fecha	BETWEEN
				FRQ100.FRQ1FECINICIO
			AND
				NVL(FRQ100.FRQ1FECFIN,TO_DATE('31/12/3000','DD/MM/YYYY'))
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
                1,
		TO_NUMBER (FRQ100.FRQ1PRECIOVENTA),
                DECODE(FR65INDINTERCIENT,-1,-1,0),
		'0'||FR7300.FR00CODGRPTERAP,
		TO_NUMBER (FRQ100.FRQ1PRECIOVENTACON),
		FR7300.FR73CODPRODUCTO
      UNION ALL
      SELECT /*+ rule */
   /* FA0004J: Facturación Farmacia: facturable por dosis, diluyente */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (CEIL (fr6500.fr65cantidaddil / fr7300.fr73volumen)),
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
             1,
             TO_NUMBER (FRQ100.FRQ1PRECIOVENTA),
             DECODE(FR65INDINTERCIENT,-1,-1,0),
	     '0'||FR7300.FR00CODGRPTERAP,
	     TO_NUMBER (FRQ100.FRQ1PRECIOVENTACON),
	     FR7300.FR73CODPRODUCTO
        FROM 
             fr6600,
             fr7300,
             ci2200,
             fa0500,
	     FRQ100,
	     fr6500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_dil = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND fr7300.fr73indfactdosis = -1
         AND fr6500.fr73codproducto_dil IS NOT NULL
         AND fa0500.fa05codorigen (+)= fr7300.fr73codintfar
         AND fa0500.fa08codgrupo (+)= 1
         AND fr7300.fr73volumen > 0
         AND fr6500.pr62codhojaquir IS NULL
	 AND FRQ100.fr73codproducto = FR6500.fr73codproducto_dil
	 AND FR6500.fr65fecha	BETWEEN
				FRQ100.FRQ1FECINICIO
			AND
				NVL(FRQ100.FRQ1FECFIN,TO_DATE('31/12/3000','DD/MM/YYYY'))
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
                1,
		TO_NUMBER (FRQ100.FRQ1PRECIOVENTA),
                DECODE(FR65INDINTERCIENT,-1,-1,0),
		'0'||FR7300.FR00CODGRPTERAP,
		TO_NUMBER (FRQ100.FRQ1PRECIOVENTACON),
		FR7300.FR73CODPRODUCTO
      UNION ALL
      SELECT /*+ rule */
   /* FA0004J: Facturación Farmacia: facturable por dosis, producto 2 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             CEIL (
                10 *
                   SUM (DECODE (
                           fr7300.fr73dosis, NULL, 1, fr6500.fr65dosis_2 /
                                                         fr7300.fr73dosis
                        ))
             ) /
                10,
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
             1,
             TO_NUMBER (FRQ100.FRQ1PRECIOVENTA),
             DECODE(FR65INDINTERCIENT,-1,-1,0),
	     '0'||FR7300.FR00CODGRPTERAP,
	     TO_NUMBER (FRQ100.FRQ1PRECIOVENTACON),
	     FR7300.FR73CODPRODUCTO
        FROM 
             fr6600,
             fr7300,
             ci2200,
             fa0500,
	     FRQ100,
	     fr6500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_2 = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND fr7300.fr73indfactdosis = -1
         AND fr6500.fr73codproducto_2 IS NOT NULL
         AND fr6500.fr65dosis_2 IS NOT NULL
         AND fr6500.fr73codproducto_dil IS NOT NULL
         AND fa0500.fa08codgrupo (+)= 1
         AND fa0500.fa05codorigen (+)= fr7300.fr73codintfar
         AND fr6500.pr62codhojaquir IS NULL
	 AND FRQ100.fr73codproducto = FR6500.fr73codproducto_2
	 AND FR6500.fr65fecha	BETWEEN
				FRQ100.FRQ1FECINICIO
			AND
				NVL(FRQ100.FRQ1FECFIN,TO_DATE('31/12/3000','DD/MM/YYYY'))
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                NVL(FA0500.FA05DESIG,FR7300.FR73DESPRODUCTO),
                1,
		TO_NUMBER (FRQ100.FRQ1PRECIOVENTA),
                DECODE(FR65INDINTERCIENT,-1,-1,0),
		'0'||FR7300.FR00CODGRPTERAP,
		TO_NUMBER (FRQ100.FRQ1PRECIOVENTACON),
		FR7300.FR73CODPRODUCTO
