   CREATE OR REPLACE VIEW fa0010j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT /*Vista que genera revision si es Exploraciones*/
             ad0500.ad07codproceso,
             ad0500.ad01codasistenci,
             fa05codcateg,
             1 cantidad,
             ad01fecinicio fecha,
             ci22numhistoria,
             0 dptosolicita,
             TO_CHAR (NULL) drsolicita,
             ad02coddpto dptorealiza,
             TO_CHAR (NULL) drrealiza,
             NULL obs,
             1 estado,
             TO_NUMBER (NULL) precio,
             0 ic
        FROM ad0100,
             ad2500,
             ad0500,
             fa0500
       WHERE ad0100.ad01codasistenci = ad0500.ad01codasistenci
         AND ad2500.ad01codasistenci = ad0100.ad01codasistenci
         AND ad0500.ad05fecfinrespon IS NULL
         AND ad2500.ad12codtipoasist = 3
         AND ad2500.ad25fecfin IS NULL
         AND fa0500.fa05codorigc1 = ad0500.ad02coddpto
         AND fa0500.fa05codorigc2 = 0
         AND fa0500.fa05codorigc3 = 4
         AND fa0500.fa05codorigc4 = 1
         AND fa0500.fa08codgrupo = 7;

   CREATE OR REPLACE VIEW fa0008j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT/*+ RULE */
   /* VISTA RNF DE BANCO DE SANGRE */
   /* Productos Administrados*/
          bs0300.ad07codproceso,
          bs0300.ad01codasistenci,
          fa0500.fa05codcateg,
          COUNT (*),
          NVL (bs1800.bs18fecsalida, bs1800.bs18fecadd),
          ad0100.ci22numhistoria,
          TO_NUMBER (NULL),
          NULL,
          201,
          NULL,
          '',
          1,
          TO_NUMBER (NULL),
          0
        FROM fa0500,
             bs1800,
             bs0300,
             ad0100
       WHERE bs1800.bs03codbolsa = bs0300.bs03codbolsa
         AND ad0100.ad01codasistenci = bs0300.ad01codasistenci
         AND fa0500.fa05codorigc1 = 201
         AND fa0500.fa08codgrupo = 9
         AND fa0500.fa05codorigen = bs0300.bs01codprod
         AND bs0300.bs10codestbolsa IN (5, 6) /* Unidades Enviadas, Transfundidas*/
         AND (
                   bs1800.bs18indtransf = -1
                OR (    bs1800.bs18indtransf = 0
                    AND bs1800.bs18fecentrada IS NULL)
             )
       GROUP BY bs0300.ad07codproceso,
                bs0300.ad01codasistenci,
                fa0500.fa05codcateg,
                NVL (bs1800.bs18fecsalida, bs1800.bs18fecadd),
                ad0100.ci22numhistoria
      UNION ALL
   /* Pruebas Cruzadas */
      SELECT bs1400.ad07codproceso,
             bs1400.ad01codasistenci,
             11866,                                          /* Pruebas Cruzadas */
             COUNT (*),
             NVL (bs1400.bs14feccruce, bs1400.bs14fecadd),
             ad0100.ci22numhistoria,
             TO_NUMBER (NULL),
             NULL,
             201,
             NULL,
             '',
             1,
             TO_NUMBER (NULL),
             0
        FROM bs1400,
             bs0300,
             bs0100,
             bs3200,
             ad0100
       WHERE ad0100.ad01codasistenci = bs1400.ad01codasistenci
         AND bs0300.bs03codbolsa = bs1400.bs03codbolsa
         AND bs0100.bs01codprod = bs0300.bs01codprod
         AND bs3200.bs32codtiprod = bs0100.bs32codtiprod
         AND bs3200.bs32indprcruzadas = -1
       GROUP BY bs1400.ad07codproceso,
                bs1400.ad01codasistenci,
                NVL (bs1400.bs14feccruce, bs1400.bs14fecadd),
                ad0100.ci22numhistoria
      UNION ALL
   /* Anticuerpos Irregulares*/
      SELECT bs2000.ad07codproceso,
             bs2000.ad01codasistenci,
             11872,                                   /* Anticuerpos Irregulares */
             1,
             NVL (bs2000.bs20fecpeticion, bs2000.bs20fecadd),
             ad0100.ci22numhistoria,
             TO_NUMBER (NULL),
             NULL,
             201,
             NULL,
             '',
             1,
             TO_NUMBER (NULL),
             0
        FROM bs2000,
             bs3200,
             ad0100
       WHERE ad0100.ad01codasistenci = bs2000.ad01codasistenci
         AND bs3200.bs32codtiprod = bs2000.bs32codtiprod
         AND bs2000.bs22codestreserva < 9
         AND bs3200.bs32indprcruzadas = -1
       GROUP BY bs2000.ad07codproceso,
                bs2000.ad01codasistenci,
                NVL (bs2000.bs20fecpeticion, bs2000.bs20fecadd),
                ad0100.ci22numhistoria
      UNION ALL
   /* Unidades Caducadas*/
      SELECT/*+ rule */ bs1400.ad07codproceso,
                        bs1400.ad01codasistenci,
                        fa0500.fa05codcateg,
                        COUNT (*),
                        bs0300.bs03feccaduc,
                        ad0100.ci22numhistoria,
                        TO_NUMBER (NULL),
                        NULL,
                        201,
                        NULL,
                        '',
                        1,
                        TO_NUMBER (NULL),
                        0
        FROM bs1400,
             bs0300,
             fa0500,
             ad0100
       WHERE ad0100.ad01codasistenci = bs1400.ad01codasistenci
         AND bs0300.bs03codbolsa = bs1400.bs03codbolsa
         AND fa0500.fa05codorigc1 = 201
         AND fa0500.fa08codgrupo = 9
         AND fa0500.fa05codorigen = bs0300.bs01codprod
         AND bs0300.bs10codestbolsa = 7
         AND bs1400.bs14fecdescruce IS NULL
       GROUP BY bs1400.ad07codproceso,
                bs1400.ad01codasistenci,
                fa0500.fa05codcateg,
                bs0300.bs03feccaduc,
                ad0100.ci22numhistoria
      UNION ALL
      SELECT/*+ rule */ bs0300.ad07codproceso,
                        bs0300.ad01codasistenci,
                        fa0500.fa05codcateg,
                        COUNT (*),
                        bs0300.bs03feccaduc,
                        bs0300.ci22numhistoria,
                        TO_NUMBER (NULL),
                        NULL,
                        201,
                        NULL,
                        '',
                        1,
                        TO_NUMBER (NULL),
                        0
        FROM bs0300,
             fa0500
       WHERE fa0500.fa05codorigc1 = 201
         AND fa0500.fa08codgrupo = 9
         AND fa0500.fa05codorigen = bs0300.bs01codprod
         AND bs0300.bs10codestbolsa = 7
       GROUP BY bs0300.ad07codproceso,
                bs0300.ad01codasistenci,
                fa0500.fa05codcateg,
                bs0300.bs03feccaduc,
                bs0300.ci22numhistoria;

   CREATE OR REPLACE VIEW fa0004j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT
   /* FA0004J: Facturación Farmacia:  No facturable por dosis,producto 1 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (SIGN (fr6500.fr65cantidad) * CEIL (ABS (fr6500.fr65cantidad))),
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             '',
             1,
             DECODE (
                fr6500.fr73codproducto, 999999999, fr6500.fr65precio,
                TO_NUMBER (NULL)
             ),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             fr6600,
             fr7300,
             ci2200,
             fa0500
       WHERE fa0500.fa08codgrupo = 1
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND (   (fr7300.fr73indfactdosis IS NULL)
              OR (fr7300.fr73indfactdosis = 0))
         AND fr6500.pr62codhojaquir IS NULL
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                '',
                1,
                DECODE (
                   fr6500.fr73codproducto, 999999999, fr6500.fr65precio,
                   TO_NUMBER (NULL)
                ),
                DECODE(FR65INDINTERCIENT,-1,-1,0)
      UNION ALL
      SELECT
   /* FA0004J: Facturación Farmacia:  No facturable por dosis, diluyente */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (CEIL (fr6500.fr65cantidaddil / fr7300.fr73volumen)),
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             '',
             1,
             TO_NUMBER (NULL),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             fr6600,
             fr7300,
             ci2200,
             fa0500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_dil = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND (   (fr7300.fr73indfactdosis IS NULL)
              OR (fr7300.fr73indfactdosis = 0))
         AND fr6500.fr73codproducto_dil IS NOT NULL
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fa0500.fa08codgrupo = 1
         AND fr7300.fr73volumen > 0
         AND fr6500.pr62codhojaquir IS NULL
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                '',
                1 ,
                DECODE(FR65INDINTERCIENT,-1,-1,0)
      UNION ALL
      SELECT
   /* FA0004J: Facturación Farmacia:  No facturable por dosis, producto 2 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (CEIL (
                     DECODE (
                        fr7300.fr73dosis, NULL, 1, 0,1,fr6500.fr65dosis_2 /
                                                      fr7300.fr73dosis
                     )
                  )) "CANTIDAD",
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             '',
             1,
             TO_NUMBER (NULL),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             fr6600,
             fr7300,
             ci2200,
             fa0500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_2 = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND (   (fr7300.fr73indfactdosis IS NULL)
              OR (fr7300.fr73indfactdosis = 0))
         AND fr6500.fr73codproducto_2 IS NOT NULL
         AND fr6500.fr65dosis_2 IS NOT NULL
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fa0500.fa08codgrupo = 1
         AND fr6500.pr62codhojaquir IS NULL
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                '',
                1,
                DECODE(FR65INDINTERCIENT,-1,-1,0)
      UNION ALL
      SELECT
   /* FA0004J: Facturación Farmacia:  facturable por dosis, producto 1 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SIGN (SUM (fr6500.fr65cantidad)) *
                CEIL (10 * ABS (SUM (fr6500.fr65cantidad))) /
                10,
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             '',
             1,
             TO_NUMBER (NULL),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             fr6600,
             fr7300,
             ci2200,
             fa0500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND fr7300.fr73indfactdosis = -1
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fa0500.fa08codgrupo = 1
         AND fr6500.pr62codhojaquir IS NULL
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                '',
                1,
                DECODE(FR65INDINTERCIENT,-1,-1,0)
      UNION ALL
      SELECT
   /* FA0004J: Facturación Farmacia: facturable por dosis, diluyente */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (CEIL (fr6500.fr65cantidaddil / fr7300.fr73volumen)),
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             '',
             1,
             TO_NUMBER (NULL),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             fr6600,
             fr7300,
             ci2200,
             fa0500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_dil = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND fr7300.fr73indfactdosis = -1
         AND fr6500.fr73codproducto_dil IS NOT NULL
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fa0500.fa08codgrupo = 1
         AND fr7300.fr73volumen > 0
         AND fr6500.pr62codhojaquir IS NULL
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                '',
                1,
                DECODE(FR65INDINTERCIENT,-1,-1,0)
      UNION ALL
      SELECT
   /* FA0004J: Facturación Farmacia: facturable por dosis, producto 2 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             CEIL (
                10 *
                   SUM (DECODE (
                           fr7300.fr73dosis, NULL, 1, fr6500.fr65dosis_2 /
                                                         fr7300.fr73dosis
                        ))
             ) /
                10,
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6600.ad02coddpto,
             DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
             215,
             NULL,
             '',
             1,
             TO_NUMBER (NULL),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             fr6600,
             fr7300,
             ci2200,
             fa0500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_2 = fr7300.fr73codproducto
         AND fr6500.fr66codpeticion = fr6600.fr66codpeticion (+)
         AND fr7300.fr73indfactdosis = -1
         AND fr6500.fr73codproducto_2 IS NOT NULL
         AND fr6500.fr65dosis_2 IS NOT NULL
         AND fr6500.fr73codproducto_dil IS NOT NULL
         AND fa0500.fa08codgrupo = 1
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fr6500.pr62codhojaquir IS NULL
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6600.ad02coddpto,
                DECODE (fr6600.sg02cod_med, fr6600.sg02cod_enf, fr6600.sg02cod_med),
                215,
                NULL,
                '',
                1,
                DECODE(FR65INDINTERCIENT,-1,-1,0);
                
   CREATE OR REPLACE VIEW faibm4j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT a.ad07codproceso,
             a.ad01codasistenci,
             b.fa05codcateg,
             TO_NUMBER (c.canti1),
             TO_DATE (
                c.fsoli1 ||
                   DECODE (c.hsoli1, 'FFFF', '0000', '2400', '0000', c.hsoli1),
                'YYYYMMDDHH24MI'
             ),
             TO_NUMBER (SUBSTR (c.nhist1, 1, 6)),
             0,
             '',
             215,
             '',
             '',
             1,
             DECODE (
                impss1, 0, TO_NUMBER (impor1) / TO_NUMBER (c.canti1),
                TO_NUMBER (impss1) / TO_NUMBER (c.canti1)
             ),
             DECODE(c.flag1,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r1mov c
       WHERE c.numcaso = a.ad08numcaso
         AND b.fa05codorigen = c.medic1
         AND b.fa08codgrupo = 1;

   CREATE OR REPLACE VIEW faibm0004j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,
             ic
        FROM faibm4j
       WHERE fecha <= TO_DATE ('30121999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,
             ic           
        FROM fa0004j
       WHERE fecha > TO_DATE ('30121999 16', 'DDMMYYYY HH24');

   CREATE OR REPLACE VIEW fa0005j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT
   /* FA0005J: Facturación Quirófano: no facturable por dosis, producto 1 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (CEIL (fr6500.fr65cantidad)),
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6500.ad02coddpto,
             DECODE (
                fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
             ),
             215,
             NULL,
             '',
             1,
             to_number(null),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             pr6200,
             fr7300,
             ci2200,
             fa0500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto = fr7300.fr73codproducto
         AND fr6500.pr62codhojaquir = pr6200.pr62codhojaquir
         AND (   (fr7300.fr73indfactdosis IS NULL)
              OR (fr7300.fr73indfactdosis = 0))
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fa0500.fa08codgrupo = 1
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6500.ad02coddpto,
                DECODE (
                   fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                   NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
                ),
                215,
                NULL,
                '',
                1 ,
                DECODE(FR65INDINTERCIENT,-1,-1,0)
      UNION ALL
      SELECT
   /* FA0005J: Facturación Quirófano: no facturable por dosis, diluyente */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (CEIL (fr6500.fr65cantidaddil / fr7300.fr73volumen)),
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6500.ad02coddpto,
             DECODE (
                fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
             ),
             215,
             NULL,
             '',
             1,
             to_number(null),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             pr6200,
             fr7300,
             ci2200,
             fa0500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_dil = fr7300.fr73codproducto
         AND fr6500.pr62codhojaquir = pr6200.pr62codhojaquir
         AND (   (fr7300.fr73indfactdosis IS NULL)
              OR (fr7300.fr73indfactdosis = 0))
         AND fr6500.fr73codproducto_dil IS NOT NULL
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fr7300.fr73volumen > 0
         AND fa0500.fa08codgrupo = 1
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6500.ad02coddpto,
                DECODE (
                   fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                   NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
                ),
                215,
                NULL,
                '',
                1,
                DECODE(FR65INDINTERCIENT,-1,-1,0)
      UNION ALL
      SELECT
   /* FA0005J: Facturación Quirófano: No facturable por dosis, producto 2 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (CEIL (
                     DECODE (
                        fr7300.fr73dosis, NULL, 1, 0,1,fr6500.fr65dosis_2 /
                                                      fr7300.fr73dosis
                     )
                  )),
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6500.ad02coddpto,
             DECODE (
                fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
             ),
             215,
             NULL,
             '',
             1 ,
             to_number(null),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             pr6200,
             fr7300,
             ci2200,
             fa0500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_2 = fr7300.fr73codproducto
         AND fr6500.pr62codhojaquir = pr6200.pr62codhojaquir
         AND (   (fr7300.fr73indfactdosis IS NULL)
              OR (fr7300.fr73indfactdosis = 0))
         AND fr6500.fr73codproducto_2 IS NOT NULL
         AND fr6500.fr65dosis_2 IS NOT NULL
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fa0500.fa08codgrupo = 1
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6500.ad02coddpto,
                DECODE (
                   fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                   NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
                ),
                215,
                NULL,
                '',
                1 ,
                DECODE(FR65INDINTERCIENT,-1,-1,0)
      UNION ALL
      SELECT
   /* FA0005J: Facturación Quirófano: facturable por dosis, producto 1 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             CEIL (10 * SUM (fr6500.fr65cantidad)) / 10,
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6500.ad02coddpto,
             DECODE (
                fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
             ),
             215,
             NULL,
             '',
             1,
             to_number(null),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             pr6200,
             fr7300,
             ci2200,
             fa0500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto = fr7300.fr73codproducto
         AND fr6500.pr62codhojaquir = pr6200.pr62codhojaquir
         AND fr7300.fr73indfactdosis = -1
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fa0500.fa08codgrupo = 1
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6500.ad02coddpto,
                DECODE (
                   fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                   NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
                ),
                215,
                NULL,
                '',
                1,
                DECODE(FR65INDINTERCIENT,-1,-1,0)
      UNION ALL
      SELECT
   /* FA0005J: Facturación Quirófano: facturable por dosis, diluyente */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             SUM (CEIL (fr6500.fr65cantidaddil / fr7300.fr73volumen)),
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6500.ad02coddpto,
             DECODE (
                fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
             ),
             215,
             NULL,
             '',
             1,
             to_number(null),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             pr6200,
             fr7300,
             ci2200,
             fa0500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_dil = fr7300.fr73codproducto
         AND fr6500.pr62codhojaquir = pr6200.pr62codhojaquir
         AND fr7300.fr73indfactdosis = -1
         AND fr6500.fr73codproducto_dil IS NOT NULL
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fr7300.fr73volumen > 0
         AND fa0500.fa08codgrupo = 1
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6500.ad02coddpto,
                DECODE (
                   fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                   NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
                ),
                215,
                NULL,
                '',
                1,
                DECODE(FR65INDINTERCIENT,-1,-1,0)
      UNION ALL
      SELECT
   /* FA0005J: Facturación Quirófano: facturable por dosis, producto 2 */
             fr6500.ad07codproceso,
             fr6500.ad01codasistenci,
             fa0500.fa05codcateg,
             CEIL (
                10 *
                   SUM (DECODE (
                           fr7300.fr73dosis, NULL, 1, fr6500.fr65dosis_2 /
                                                         fr7300.fr73dosis
                        ))
             ) /
                10,
             fr65fecha,
             ci2200.ci22numhistoria,
             fr6500.ad02coddpto,
             DECODE (
                fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
             ),
             215,
             NULL,
             '',
             1,
             to_number(null),
             DECODE(FR65INDINTERCIENT,-1,-1,0)
        FROM fr6500,
             pr6200,
             fr7300,
             ci2200,
             fa0500
       WHERE fr6500.ci21codpersona = ci2200.ci21codpersona
         AND fr6500.fr73codproducto_2 = fr7300.fr73codproducto
         AND fr6500.pr62codhojaquir = pr6200.pr62codhojaquir
         AND fr7300.fr73indfactdosis = -1
         AND fr6500.fr73codproducto_2 IS NOT NULL
         AND fr6500.fr65dosis_2 IS NOT NULL
         AND fa0500.fa05codorigen = fr7300.fr73codintfar
         AND fa0500.fa08codgrupo = 1
       GROUP BY fr6500.ad07codproceso,
                fr6500.ad01codasistenci,
                fa0500.fa05codcateg,
                fr65fecha,
                ci2200.ci22numhistoria,
                fr6500.ad02coddpto,
                DECODE (
                   fr6500.fr65indimputquir, -1, pr6200.sg02cod_cir, 0,
                   NVL (pr6200.sg02cod_ane, pr6200.sg02cod_cir)
                ),
                215,
                NULL,
                '',
                1,
                DECODE(FR65INDINTERCIENT,-1,-1,0);

   CREATE OR REPLACE VIEW faibm5j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT a.ad07codproceso,
             a.ad01codasistenci,
             b.fa05codcateg,
             TO_NUMBER (c.canti3),
             TO_DATE (
                c.fsoli3 ||
                   DECODE (c.hsoli3, 'FFFF', '0000', '2400', '0000', c.hsoli3),
                'YYYYMMDDHH24MI'
             ),
             TO_NUMBER (SUBSTR (c.nhist3, 1, 6)),
             0,
             '',
             215,
             '',
             '',
             1,
             DECODE (
                impss3, 0, TO_NUMBER (impor3) / TO_NUMBER (c.canti3),
                TO_NUMBER (impss3) / TO_NUMBER (c.canti3)
             ),
             DECODE(c.flag3,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r3mov c
       WHERE c.numcaso = a.ad08numcaso
         AND b.fa05codorigen = c.medic3
         AND b.fa08codgrupo = 1;

   CREATE OR REPLACE VIEW faibm0005j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,
             ic
        FROM faibm5j
       WHERE fecha <= TO_DATE ('30121999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,
             ic
        FROM fa0005j
       WHERE fecha > TO_DATE ('30121999 16', 'DDMMYYYY HH24');

   CREATE OR REPLACE VIEW fa0000j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT DISTINCT
   /* VISTA RNF DE INTERVENCIONES */
   /* Derechos de Quirófano */
                      t4.ad07codproceso,
                      t4.ad01codasistenci,
                      DECODE (c1.ag11codrecurso, 585, 35352, 586, 35352, 35354),
                                    /*QUIROFANOS AMBULATORIOS-RESTO DE QUIROFANOS*/
                      DECODE (
                         pr62fecini_anes, NULL,
                         GREATEST (1, FLOOR (pr62numminint / 15 + 0.5)),
                         DECODE (
                            pr62fecfin_anes, NULL,
                            GREATEST (1, FLOOR (pr62numminint / 15 + 0.5)),
                            1 +
                               FLOOR (
                                  (pr62fecfin_anes - pr62fecini_anes) * 1440 / 15
                               )
                         )
                      ),
                      NVL (t4.pr04fecfinact, t4.pr04fecadd),
                      t7.ci22numhistoria,
                      t9.ad02coddpto,
                      t9.sg02cod,
                      213,
                      NULL,
                      '',
                      DECODE (pr37codestado, 1, 0, 1),
                      DECODE (t4.pr04indintcientif,-1,-1,0)
        FROM pr0400 t4,
             ad0700 t7,
             pr0900 t9,
             pr0300 t3,
             pr6200 t6,
             ci0100 c1
       WHERE t7.ad07codproceso = t4.ad07codproceso
         AND t3.pr03numactpedi = t4.pr03numactpedi
         AND t9.pr09numpeticion = t3.pr09numpeticion
         AND t4.pr37codestado < 6/*ESTADOS PLANIF, CITAD, REALIZANDOSE, REALIZADA*/
         AND t6.pr62codhojaquir = t4.pr62codhojaquir
         AND pr62codestado > 0
         AND c1.pr04numactplan = t4.pr04numactplan
         AND c1.ci01sitcita = '1'
      UNION
   /* Derechos de Anestesia */
      SELECT t4.ad07codproceso,
             t4.ad01codasistenci,
             35353,
             1,
             NVL (t4.pr04fecfinact, t4.pr04fecadd),
             t7.ci22numhistoria,
             t9.ad02coddpto,
             t9.sg02cod,
             223,
             NULL,
             '',
             DECODE (pr37codestado, 1, 0, 1),
             DECODE (t4.pr04indintcientif,-1,-1,0)
        FROM pr0400 t4,
             ad0700 t7,
             pr0900 t9,
             pr0300 t3,
             pr6200 t6
       WHERE t7.ad07codproceso = t4.ad07codproceso
         AND t3.pr03numactpedi = t4.pr03numactpedi
         AND t9.pr09numpeticion = t3.pr09numpeticion
         AND t4.pr37codestado < 6/*ESTADOS PLANIF, CITAD, REALIZANDOSE, REALIZADA*/
         AND t6.pr62codhojaquir = t4.pr62codhojaquir
         AND pr62codestado > 0
         AND t6.sg02cod_ane IS NOT NULL
      UNION
      SELECT
   /* Vista RNF (Registro Normalizado de Facturación) de Intervenciones*/
             t4.ad07codproceso,
             t4.ad01codasistenci,
             f5.fa05codcateg,
             COUNT (*) cantidad,
             NVL (pr04fecfinact, pr04fecadd),
             t7.ci22numhistoria,
             t9.ad02coddpto,
             t9.sg02cod,
             t4.ad02coddpto,
             '0',
             '',
             DECODE (pr37codestado, 1, 0, 2, 0, 1),
             DECODE (t4.pr04indintcientif,-1,-1,0)
        FROM pr0100 t1,
             pr0400 t4,
             ad0700 t7,
             pr0900 t9,
             pr0300 t3,
             fa0500 f5
       WHERE f5.fa08codgrupo = 6
         AND f5.fa05codorigen = t4.pr01codactuacion
         AND t1.pr01codactuacion = t4.pr01codactuacion
         AND t1.pr12codactividad = 207                           /*intervenciones*/
         AND t7.ad07codproceso = t4.ad07codproceso
         AND t3.pr03numactpedi = t4.pr03numactpedi
         AND t9.pr09numpeticion = t3.pr09numpeticion
         AND t4.pr37codestado IN
                  (
                     1,
                     2,
                     3,
                     4,
                     5
                  )              /*ESTADOS PLANIF, CITAD, REALIZANDOSE, REALIZADA*/
       GROUP BY t4.ad07codproceso,
                t4.ad01codasistenci,
                f5.fa05codcateg,
                NVL (pr04fecfinact, pr04fecadd),
                t7.ci22numhistoria,
                t9.ad02coddpto,
                t9.sg02cod,
                t4.ad02coddpto,
                '0',
                '',
                DECODE (pr37codestado, 1, 0, 2, 0, 1),
                DECODE (t4.pr04indintcientif,-1,-1,0);

   CREATE OR REPLACE VIEW faibm1j1 (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT a.ad07codproceso,
             a.ad01codasistenci ad01codasistenci,
             b.fa05codcateg fa05codcateg,
             1 cantidad,
             TO_DATE (c.finic2, 'YYYYMMDD'),
             TO_NUMBER (SUBSTR (c.nhist2, 1, 6)) ci22numhistoria,
             TO_NUMBER (c.servc2) dptosolicita,
             '0' drsolicita,
             213 dptorealiza,
             TO_CHAR (c.ciruj2) drrealiza,
             '',
             1 ,
             DECODE(c.flag2,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r2mov c,
             pr0200 d
       WHERE c.numcaso = a.ad08numcaso
         AND d.gc01fpclav_ibs = c.inter2
         AND d.gc01fctreg_ibs = 6
         AND b.fa05codorigen = d.pr01codactuacion
         AND b.fa08codgrupo = 6;

   CREATE OR REPLACE VIEW faibm1j2 (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT a.ad07codproceso,
             a.ad01codasistenci ad01codasistenci,
             35353 fa05codcateg,
             1 cantidad,
             TO_DATE (c.finic2, 'YYYYMMDD'),
             TO_NUMBER (SUBSTR (c.nhist2, 1, 6)) ci22numhistoria,
             TO_NUMBER (c.servc2) dptosolicita,
             '0' drsolicita,
             TO_NUMBER (sanes2) dptorealiza,
             TO_CHAR (anest2) drrealiza,
             '',
             1 ,
             DECODE(c.flag2,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r2mov c,
             pr0200 d
       WHERE c.numcaso = a.ad08numcaso
         AND b.fa05codorigen = d.pr01codactuacion
         AND d.gc01fpclav_ibs = c.inter2
         AND d.gc01fctreg_ibs = 6
         AND b.fa08codgrupo = 6
         AND sanes2 IS NOT NULL;

   CREATE OR REPLACE VIEW faibm1j3 (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT a.ad07codproceso,
             a.ad01codasistenci ad01codasistenci,
             35354 fa05codcateg,
             SUM (TRUNC (4 * SUBSTR (durac2, 1, 2) + SUBSTR (durac2, 3, 2) / 15)) cantidad,
             TO_DATE (c.finic2, 'YYYYMMDD'),
             TO_NUMBER (SUBSTR (c.nhist2, 1, 6)) ci22numhistoria,
             TO_NUMBER (c.servc2) dptosolicita,
             '0' drsolicita,
             213 dptorealiza,
             '0' drrealiza,
             NULL,
             1 ,
             DECODE(c.flag2,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r2mov c,
             pr0200 d
       WHERE c.numcaso = a.ad08numcaso
         AND b.fa05codorigen = d.pr01codactuacion
         AND d.gc01fpclav_ibs = c.inter2
         AND d.gc01fctreg_ibs = 6
         AND b.fa08codgrupo = 6
         AND c.flag2 <> '0A'                                  -- NO INVESTIGACION  
       GROUP BY ad07codproceso,
                ad01codasistenci,
                TO_DATE (c.finic2, 'YYYYMMDD'),
                TO_NUMBER (SUBSTR (c.nhist2, 1, 6)),
                c.servc2,
                DECODE(c.flag2,'0A',-1,0);

   CREATE OR REPLACE VIEW faibmj (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             ic               
        FROM faibm1j1
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             ic               
        FROM faibm1j2
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             ic               
        FROM faibm1j3;

   CREATE OR REPLACE VIEW faibm0000j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             TO_NUMBER (NULL),
             ic
        FROM faibmj
       WHERE fecha <= TO_DATE ('30121999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             TO_NUMBER (NULL),
             ic
        FROM fa0000j
       WHERE fecha > TO_DATE ('30121999 16', 'DDMMYYYY HH24');

   CREATE OR REPLACE VIEW ad1699j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT
             /*ESTANCIAS LOS QUE NO SON DE INSALUD*/
             a7.ad07codproceso,
             t1.ad01codasistenci,
             f5.fa05codcateg,
             DECODE (
                regnum, 0, DECODE (
                              TO_CHAR (fecha, 'HH24'), 12, 0.5, 13, 0.5, 14, 0.5,
                              15, 0.5, 16, 0.5, 17, 0.5, 18, 0.5, 19, 0.5, 20, 0.5,
                              21, 0, 22, 0, 23, 0, 1
                           ), DECODE (
                                 ultimo, -1, DECODE (
                                                TO_CHAR (fecha, 'HH24'), 18, 1, 19,
                                                1, 20, 1, 21, 1, 22, 1, 23, 1, 0.5
                                             ), 1
                              )
             ) cantidad,
             fecha,
             a7.ci22numhistoria,
             0 dptosolicita,
             TO_CHAR (0) drsolicita,
             a5.ad02coddpto dptorealiza,
             TO_CHAR (0) drrealiza,
             gcfn06 (t1.ad15codcama) obs,
             1 estado,
             0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad1500 a5
       WHERE f5.fa08codgrupo = 5
         AND t1.ad13codtipocama = f5.fa05codorigen
         AND a7.ad07codproceso = t1.ad07codproceso
         AND NOT EXISTS (SELECT ci32codtipecon
                           FROM ad1100 z1
                          WHERE z1.ad07codproceso = t1.ad07codproceso
                            AND z1.ad01codasistenci = t1.ad01codasistenci
                            AND z1.ci32codtipecon = 'S')
         AND a5.ad15codcama = t1.ad15codcama
      UNION ALL
      SELECT/*ESTANCIAS LOS QUE SI SON DE INSALUD PERO NO DE OSASUNBIDEA,QUE TIENE ESTANCIAS QUIRURGICAS*/
          a7.ad07codproceso,
          t1.ad01codasistenci,
          f5.fa05codcateg,
          1 cantidad,
          fecha,
          a7.ci22numhistoria,
          0 dptosolicita,
          TO_CHAR (0) drsolicita,
          a5.ad02coddpto dptorealiza,
          TO_CHAR (0) drrealiza,
          gcfn06 (t1.ad15codcama) obs,
          1 estado,
          0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad1500 a5
       WHERE f5.fa08codgrupo = 5
         AND t1.ad13codtipocama = f5.fa05codorigen
         AND a7.ad07codproceso = t1.ad07codproceso
         AND EXISTS (SELECT ci32codtipecon
                       FROM ad1100 z1
                      WHERE z1.ad07codproceso = t1.ad07codproceso
                        AND z1.ad01codasistenci = t1.ad01codasistenci
                        AND z1.ci32codtipecon = 'S'
                        AND z1.ci13codentidad <> 'NA')
         AND t1.regnum > 0
         AND a5.ad15codcama = t1.ad15codcama
      UNION ALL
      SELECT/*ESTANCIAS LOS QUE SI SON DE OSASUNBIDEA, ESTANCIAS NO QUIRURGICAS*/
          a7.ad07codproceso,
          t1.ad01codasistenci,
          f5.fa05codcateg,
          1 cantidad,
          fecha,
          a7.ci22numhistoria,
          0 dptosolicita,
          TO_CHAR (0) drsolicita,
          a5.ad02coddpto dptorealiza,
          TO_CHAR (0) drrealiza,
          gcfn06 (t1.ad15codcama) obs,
          1 estado,
          0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad1500 a5
       WHERE f5.fa08codgrupo = 5
         AND t1.ad13codtipocama = f5.fa05codorigen
         AND a7.ad07codproceso = t1.ad07codproceso
         AND EXISTS (SELECT ci32codtipecon
                       FROM ad1100 z1
                      WHERE z1.ad07codproceso = t1.ad07codproceso
                        AND z1.ad01codasistenci = t1.ad01codasistenci
                        AND z1.ci32codtipecon = 'S'
                        AND z1.ci13codentidad = 'NA')
         AND t1.regnum > 0
         AND a5.ad15codcama = t1.ad15codcama
         AND fecha <
              (SELECT NVL (
                         MIN (pr04feciniact), SYSDATE
                      )/* MENOR QUE LA MIN FECHA SI TIENE INTERVENCION */
                 FROM ad1697j
                WHERE ad1697j.ad07codproceso = t1.ad07codproceso
                  AND ad1697j.ad01codasistenci = t1.ad01codasistenci)
      UNION ALL
      SELECT/*ESTANCIAS LOS QUE SI SON DE OSASUNBIDEA,ESTANCIAS QUIRURGICAS*/
          a7.ad07codproceso,
          t1.ad01codasistenci,
          42287,
          1 cantidad,
          fecha,
          a7.ci22numhistoria,
          0 dptosolicita,
          TO_CHAR (0) drsolicita,
          a5.ad02coddpto dptorealiza,
          TO_CHAR (0) drrealiza,
          gcfn06 (t1.ad15codcama) obs,
          1 estado,
          0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad1500 a5
       WHERE f5.fa08codgrupo = 5
         AND t1.ad13codtipocama = f5.fa05codorigen
         AND a7.ad07codproceso = t1.ad07codproceso
         AND EXISTS (SELECT ci32codtipecon
                       FROM ad1100 z1
                      WHERE z1.ad07codproceso = t1.ad07codproceso
                        AND z1.ad01codasistenci = t1.ad01codasistenci
                        AND z1.ci32codtipecon = 'S'
                        AND z1.ci13codentidad = 'NA')
         AND t1.regnum > 0
         AND a5.ad15codcama = t1.ad15codcama
         AND fecha >=
              (SELECT NVL (
                         MIN (pr04feciniact), SYSDATE
                      )/* MAYOR QUE LA MIN FECHA SI TIENE INTERVENCION */
                 FROM ad1697j
                WHERE ad1697j.ad07codproceso = t1.ad07codproceso
                  AND ad1697j.ad01codasistenci = t1.ad01codasistenci)
      UNION ALL
      SELECT/*ASISTENCIA CLINICA EN PLANTA*/
          DISTINCT a7.ad07codproceso,
                   t1.ad01codasistenci,
                   f5.fa05codcateg,
                   1 cantidad,
                   fecha,
                   a7.ci22numhistoria,
                   0 dptosolicita,
                   TO_CHAR (0) drsolicita,
                   a5.ad02coddpto dptorealiza,
                   TO_CHAR (0) drrealiza,
                   '',
                   1 ,
                   0
        FROM ad1698j t1,
             ad0700 a7,
             fa0500 f5,
             ad0500 a5
       WHERE f5.fa08codgrupo = 7
         AND
             /*PRUEBAS:Asistencia Clinica*/
             f5.fa05codorigc1 = a5.ad02coddpto
         AND f5.fa05codorigc2 = 0
         AND f5.fa05codorigc3 = 3
         AND f5.fa05codorigc4 = 2
         AND a7.ad07codproceso = t1.ad07codproceso
         AND a5.ad07codproceso = t1.ad07codproceso
         AND a5.ad01codasistenci = t1.ad01codasistenci
         AND fecha BETWEEN a5.ad05fecinirespon AND NVL (
                                                      a5.ad05fecfinrespon, SYSDATE
                                                   )
         AND fecha <
              (SELECT NVL (
                         MIN (pr04feciniact), SYSDATE
                      )/* MENOR QUE LA MIN FECHA SI TIENE INTERVENCION */
                 FROM ad1697j
                WHERE ad1697j.ad07codproceso = t1.ad07codproceso
                  AND ad1697j.ad01codasistenci = t1.ad01codasistenci
                  AND ad1697j.ad02coddpto = a5.ad02coddpto)
      UNION ALL
      SELECT/*ASISTENCIA CLINICA EN UCI*/
          DISTINCT a7.ad07codproceso,
                   t1.ad01codasistenci,
                   41980,
                   1 cantidad,
                   fecha,
                   a7.ci22numhistoria,
                   0 dptosolicita,
                   TO_CHAR (0) drsolicita,
                   318 dptorealiza,
                   TO_CHAR (0) drrealiza,
                   '',
                   1 ,
                   0
        FROM ad1698j t1,
             ad0700 a7,
             ad1500 a15
       WHERE a7.ad07codproceso = t1.ad07codproceso
         AND a15.ad15codcama = t1.ad15codcama
         AND a15.ad02coddpto = 318                                          /*UCI*/;
         
   CREATE OR REPLACE VIEW faibm3j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT /*Estancias en Clinica*/
             ad07codproceso,
             ad01codasistenci,
             fa05codcateg,
             cantidad,
             fecha,
             ci22numhistoria,
             dptosolicita,
             TO_CHAR (drsolicita),
             dptorealiza,
             TO_CHAR (drrealiza),
             obs,
             estado,
             0
        FROM faibm3;

   CREATE OR REPLACE VIEW faibm0003j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             TO_NUMBER (NULL),
             ic
        FROM faibm3j
       WHERE fecha <= TO_DATE ('15101999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             TO_NUMBER (NULL),
             ic
        FROM ad1699j
       WHERE fecha > TO_DATE ('15101999 16', 'DDMMYYYY HH24');

   CREATE OR REPLACE VIEW fa0006j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      (SELECT                                               /*ANATOMIA PATOLOGICA*/
           pr0400.ad07codproceso,
           pr0400.ad01codasistenci,
           fa0500.fa05codcateg,
           COUNT (*) AS cantidad,
           NVL (pr0400.pr04feciniact, pr04fecadd) AS fecha,
           ci2200.ci22numhistoria,
           pr0900.ad02coddpto AS dptosolicita,
           pr0900.sg02cod AS drsolicita,
           204 AS dptorealiza,
           NULL AS drrealiza,
           '' AS obs,
           1 AS estado,
           to_number(null),
           decode(pr04indintcientif,-1,-1,0)
         FROM pr0400,
              fa9000,
              fa0500,
              ci2200,
              pr0300,
              pr0900
        WHERE fa9000.fa90cod =
               (SELECT MIN (fa9000.fa90cod)
                  FROM fa00063j,
                       fa9000
                 WHERE fa9000.fa90codtipo = fa00063j.fa90codtipo
                   AND fa00063j.pr04numactplan = pr0400.pr04numactplan)
          AND fa0500.fa05codorigen = fa9000.fa90codigo
          AND ci2200.ci21codpersona = pr0400.ci21codpersona
          AND pr0300.pr03numactpedi = pr0400.pr03numactpedi
          AND pr0900.pr09numpeticion = pr0300.pr09numpeticion
          AND pr0400.pr01codactuacion = 3809
        GROUP BY pr0400.ad07codproceso,
                 pr0400.ad01codasistenci,
                 fa0500.fa05codcateg,
                 NVL (pr0400.pr04feciniact, pr04fecadd),
                 ci2200.ci22numhistoria,
                 pr0900.ad02coddpto,
                 pr0900.sg02cod,
                 decode(pr04indintcientif,-1,-1,0));

   CREATE OR REPLACE VIEW fa0002j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT
   /* Vista RNF (Registro Normalizado de Facturación) de Pruebas (No consultas)*/
             t4.ad07codproceso,
             t4.ad01codasistenci,
             f5.fa05codcateg,
             SUM (NVL (pr04cantidad, 1)),
             NVL (pr04feciniact, pr04fecadd),
             t7.ci22numhistoria,
             t9.ad02coddpto,
             t9.sg02cod,
             t4.ad02coddpto,
             '0',
             DECODE (t4.pr01codactuacion, 3865, ad08observac, ''),
                                     /* Nombre del paciente si se venden isotopos*/
             DECODE (pr37codestado, 1, 0, 2, 0, 1),
             to_number(null),
             decode(pr04indintcientif,-1,-1,0)
        FROM pr0100 t1,
             pr0400 t4,
             ad0700 t7,
             pr0900 t9,
             pr0300 t3,
             fa0500 f5,
             ad0800 a8
       WHERE f5.fa08codgrupo = 7
         AND f5.fa05codorigen = t4.pr01codactuacion
         AND t1.pr01codactuacion = t4.pr01codactuacion
         AND t1.pr12codactividad <> 201                     /* EXCEPTO CONSULTAS */
         AND t7.ad07codproceso = t4.ad07codproceso
         AND t3.pr03numactpedi = t4.pr03numactpedi
         AND t9.pr09numpeticion = t3.pr09numpeticion
         AND t4.pr37codestado < 6/*ESTADOS PLANIF, CITAD, REALIZANDOSE, REALIZADA*/
         AND t1.pr01codactuacion >= 2000        /*NO DEVOLVER NADA DE LABORATORIO*/
   /*NO DEVOLVER TACS Y RM, SE DEVUELVEN A CONTINUACION*/
         AND f5.fa05codcateg NOT IN (SELECT fa05codcateg
                                       FROM fa2600)
   /* BIOPSIAS POR VISTA FA0006J*/
         AND (   t1.pr01codactuacion <> 3809
              OR pr37codestado < 3)
         AND a8.ad01codasistenci = t4.ad01codasistenci
         AND a8.ad07codproceso = t4.ad07codproceso
       GROUP BY t4.ad07codproceso,
                t4.ad01codasistenci,
                f5.fa05codcateg,
                NVL (pr04feciniact, pr04fecadd),
                t7.ci22numhistoria,
                t9.ad02coddpto,
                t9.sg02cod,
                t4.ad02coddpto,
                '0',
                DECODE (t4.pr01codactuacion, 3865, ad08observac, ''),
                DECODE (pr37codestado, 1, 0, 2, 0, 1),
                decode(pr04indintcientif,-1,-1,0)
      UNION ALL
      SELECT                                      /* VISTA DE TACS Y RM MULTIPLES*/
          pr0400.ad07codproceso,
          pr0400.ad01codasistenci,
          fa2800.fa05codcateg,
          1 cantidad,
          TO_DATE (
             TO_CHAR (NVL (pr0400.pr04feciniact, pr0400.pr04fecadd), 'DD/MM/YYYY'),
             'DD/MM/YYYY'
          ),
          ad0700.ci22numhistoria,
          MIN (pr0900.ad02coddpto),
          MIN (pr0900.sg02cod),
          pr0400.ad02coddpto,
          NULL,
          MIN (fafn01 (
                  pr0400.ad07codproceso, pr0400.ad01codasistenci,
                  NVL (pr0400.pr04fecfinact, pr0400.pr04fecadd),
                  fa2800.fa05codcateg
               )),
          DECODE (pr37codestado, 1, 0, 2, 0, 1),
          to_number(null),
          decode(pr04indintcientif,-1,-1,0)
        FROM pr0400,
             ad0700,
             pr0900,
             pr0300,
             fa2800,
             fa2600,
             fa0500
       WHERE fa0500.fa08codgrupo = 7
         AND fa0500.fa05codorigen = pr0400.pr01codactuacion
         AND pr0400.pr01codactuacion = pr0400.pr01codactuacion
         AND ad0700.ad07codproceso = pr0400.ad07codproceso
         AND pr0300.pr03numactpedi = pr0400.pr03numactpedi
         AND pr0900.pr09numpeticion = pr0300.pr09numpeticion
         AND pr0400.pr37codestado IN (1, 2, 3, 4, 5)
         AND fa2600.fa05codcateg = fa0500.fa05codcateg
         AND fa2800.fa27codgrupofac = fa2600.fa27codgrupofac
       GROUP BY pr0400.ad07codproceso,
                pr0400.ad01codasistenci,
                fa2800.fa05codcateg,
                TO_DATE (
                   TO_CHAR (
                      NVL (pr0400.pr04feciniact, pr0400.pr04fecadd), 'DD/MM/YYYY'
                   ), 'DD/MM/YYYY'
                ),
                FLOOR (
                   TO_NUMBER (
                      TO_CHAR (
                         NVL (pr0400.pr04feciniact, pr0400.pr04fecadd), 'HH24'
                      )
                   ) /
                      14
                ),
                ad0700.ci22numhistoria,
                pr0400.ad02coddpto,
                fa2800.fa28cantdesde,
                fa2800.fa28canthasta,
                DECODE (pr37codestado, 1, 0, 2, 0, 1),
                decode(pr04indintcientif,-1,-1,0)
      HAVING COUNT (fa0500.fa05codcateg) BETWEEN fa2800.fa28cantdesde
                 AND fa2800.fa28canthasta
      UNION ALL
      SELECT pr0400.ad07codproceso,         /* PEROXIDASAS DE ANATOMIA PATOLOGICA*/
             pr0400.ad01codasistenci,
             fa2800.fa05codcateg AS fa05codcateg,
             1 AS cantidad,
             MIN (NVL (pr0400.pr04feciniact, pr04fecadd)) AS fecha,
             ad0100.ci22numhistoria,
             pr0900.ad02coddpto AS dptosolicita,
             pr0900.sg02cod AS drsolicita,
             204 AS dptorealiza,
             NULL AS drrealiza,
             '' AS obs,
             1 AS estado,
             to_number(null),
             decode(pr04indintcientif,-1,-1,0)
        FROM ap3600,
             ap1200,
             ap2100,
             prasist pa,
             pr0400,
             ad0100,
             pr0300,
             pr0900,
             fa2800,
             fa2600
       WHERE ap1200.ap12_codtec = ap3600.ap12_codtec
         AND ap2100.ap21_codref = ap3600.ap21_codref
         AND pa.nh = ap2100.ap21_codhist
         AND pa.nc = ap2100.ap21_codcaso
         AND pa.ns = ap2100.ap21_codsec
         AND pr0400.pr04numactplan = pa.pr04numactplan
         AND ad0100.ad01codasistenci = pr0400.ad01codasistenci
         AND pr0300.pr03numactpedi = pr0400.pr03numactpedi
         AND pr0900.pr09numpeticion = pr0300.pr09numpeticion
         AND fa2600.fa05codcateg = 12673
         AND fa2800.fa27codgrupofac = fa2600.fa27codgrupofac
         AND ap1200.ap04_codtitec = 4
         AND ap3600.ap36_indfact = -1
       GROUP BY pr0400.ad07codproceso,
                pr0400.ad01codasistenci,
                fa2800.fa05codcateg,
                ad0100.ci22numhistoria,
                pr0900.ad02coddpto,
                pr0900.sg02cod,
                fa2800.fa28cantdesde,
                fa2800.fa28canthasta,
                decode(pr04indintcientif,-1,-1,0)
      HAVING COUNT (*) BETWEEN fa2800.fa28cantdesde AND fa2800.fa28canthasta
      UNION ALL
      SELECT pr0400.ad07codproceso,               /* CITOMETRIAS Categoría 12676 */
             pr0400.ad01codasistenci,
             12676,
             COUNT (*) AS cantidad,
             TO_DATE (
                TO_CHAR (NVL (pr0400.pr04feciniact, pr04fecadd), 'DD/MM/YYYY'),
                'DD/MM/YYYY'
             ) AS fecha,
             ad0100.ci22numhistoria,
             pr0900.ad02coddpto AS dptosolicita,
             pr0900.sg02cod AS drsolicita,
             204 AS dptorealiza,
             NULL AS drrealiza,
             '' AS obs,
             1 AS estado,
             to_number(null),
             decode(pr04indintcientif,-1,-1,0)
        FROM ap3600,
             ap1200,
             ap2100,
             prasist pa,
             pr0400,
             ad0100,
             pr0300,
             pr0900
       WHERE ap1200.ap12_codtec = ap3600.ap12_codtec
         AND ap2100.ap21_codref = ap3600.ap21_codref
         AND pa.nh = ap2100.ap21_codhist
         AND pa.nc = ap2100.ap21_codcaso
         AND pa.ns = ap2100.ap21_codsec
         AND pr0400.pr04numactplan = pa.pr04numactplan
         AND ad0100.ad01codasistenci = pr0400.ad01codasistenci
         AND pr0300.pr03numactpedi = pr0400.pr03numactpedi
         AND pr0900.pr09numpeticion = pr0300.pr09numpeticion
         AND ap1200.ap04_codtitec = 7
         AND ap3600.ap36_indfact = -1
       GROUP BY pr0400.ad07codproceso,
                pr0400.ad01codasistenci,
                TO_DATE (
                   TO_CHAR (NVL (pr0400.pr04feciniact, pr04fecadd), 'DD/MM/YYYY'),
                   'DD/MM/YYYY'
                ),
                ad0100.ci22numhistoria,
                pr0900.ad02coddpto,
                pr0900.sg02cod,
                decode(pr04indintcientif,-1,-1,0);

   CREATE OR REPLACE VIEW fa0001j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT        /* VISTA RNF DE CONSULTAS,DISTINGE DE JEFE/CONSULTOR Y RESTO */
          DISTINCT t4.ad07codproceso,
                   t4.ad01codasistenci,
                   fa05codcateg,
                   1,
                   DECODE (pr37codestado, 4, pr04fecfinact, pr04fecadd),
                   t7.ci22numhistoria,
                   t9.ad02coddpto,
                   t9.sg02cod,
                   t4.ad02coddpto,
                   t11.sg02cod,
                   '',
                   DECODE (pr37codestado, 1, 0, 2, 0, 1),
                   to_number(null),
                   decode(pr04indintcientif,-1,-1,0)
        FROM pr0100 t1,
             pr0400 t4,
             ad0700 t7,
             pr0900 t9,
             pr0300 t3,
             pr1000 c1,
             ag1100 t11,
             ad0300 a3,
             fa0500 t5,
             ad0800 t8,
             pr0200 t2
       WHERE t4.ad07codproceso = t8.ad07codproceso
         AND t4.ad07codproceso = t7.ad07codproceso
         AND t4.ad01codasistenci = t8.ad01codasistenci
   /*ESTADOS PLANIF, CITAD, REALIZANDOSE, REALIZADA*/
         AND t4.pr37codestado IN (1, 2, 3, 4, 5)
         AND t4.pr01codactuacion = t1.pr01codactuacion
         AND t1.pr12codactividad = 201/* CONSULTAS */
         AND t1.pr01codactuacion = t2.pr01codactuacion
         AND t5.fa08codgrupo = 7
   /*SI EN PR0200 NO TIENE CODIGO IBM, CONSTRUIRLO, SI LO TIENE, USARLO*/
         AND (
                   (
                          t2.gc01fpclav_sib IS NULL
                      AND t5.fa05codorigc1 = t4.ad02coddpto
                      AND t5.fa05codorigc2 = 0
                      AND t5.fa05codorigc3 =
                             DECODE (
                                t4.pr01codactuacion, 4293/*URGENCIAS*/, 1,
                                3630/*ARV1*/, 8, 4039/*ARV2*/, 8,
                                3070/*PSICOTERAPIA*/, 2,
                                DECODE (t8.ad10codtippacien, 2, 7, 3, 2, 1)
                             )/* N-R, R, N */
                      AND t5.fa05codorigc4 =
                             DECODE (
                                t4.pr01codactuacion, 4293/*URGENCIAS*/, 2,
                                6306/*ANESTESIA*/, 2, 6304/*ANESTESIA*/, 2,
                                3630/*ARV1*/, 1, 4039/*ARV2*/, 2,
                                3070/*PSICOTERAPIA*/,
                                DECODE (
                                   t11.sg02cod, 'SCE', 4,
                                   DECODE (a3.ad31codpuesto, 6, 5, 5, 5, 1, 6, 5)
                                ), DECODE (
                                      t11.sg02cod, 'EML', 1, 'ABM', 1, 'ERH', 1,
                                      'PRV', 1, 'MLM', 1, 'RGT', 1, 'SCE', 1, 'JMB',
                                      1, 'JCM', 1, 'SDR', 2,
                                      DECODE (
                                         a3.ad31codpuesto, 6, 2, 5, 2, 1, 3, 2
                                      )
                                   )
                             )
                   )
                OR (
                          t2.gc01fpclav_sib IS NOT NULL
                      AND t5.fa05codorigen = t2.gc01fpclav_sib
                   )
             )
         AND t4.pr03numactpedi = t3.pr03numactpedi
         AND t3.pr09numpeticion = t9.pr09numpeticion
         AND c1.pr04numactplan = t4.pr04numactplan
   /* AMPLIAR POR LOS RESIDENTES (T11.SG02COD IS NULL)*/
         AND t11.ag11codrecurso = c1.ag11codrecurso
         AND a3.sg02cod (+) = t11.sg02cod
         AND a3.ad02coddpto (+) = t11.ad02coddpto
         AND a3.ad03fecfin (+) IS NULL
         AND c1.pr07numfase = 1;

   CREATE OR REPLACE VIEW faibm2j1 (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT/*+ INDEX(C R8MOV03) INDEX(B FA0502) USE_NL(B) */
          a.ad07codproceso,
          a.ad01codasistenci,
          b.fa05codcateg,
          TO_NUMBER (c.canti8),
          TO_DATE (c.freal8, 'YYYYMMDD'),
          TO_NUMBER (SUBSTR (c.nhist8, 1, 6)),
          TO_NUMBER (c.ssoli8),
          TO_CHAR (c.docto8),
          TO_NUMBER (SUBSTR (c.traba8, 1, 3)),
          '0',
          '',
          1 ,
          to_number(NULL),
          DECODE(c.flag8,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r8mov c
       WHERE c.numcaso = a.ad08numcaso
         AND b.fa08codgrupo = 7
         AND c.traba8 = b.fa05codorigen;

   CREATE OR REPLACE VIEW faibm2j2 (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT a.ad07codproceso,
             a.ad01codasistenci,
             b.fa05codcateg,
             TO_NUMBER (c.canti8),
             TO_DATE (c.freal8, 'YYYYMMDD'),
             TO_NUMBER (SUBSTR (c.nhist8, 1, 6)),
             TO_NUMBER (c.ssoli8),
             TO_CHAR (c.docto8),
             TO_NUMBER (SUBSTR (c.traba8, 1, 3)),
             '0',
             '',
             1 ,
             to_number(null),
             DECODE(c.flag8,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r8mov c,
             pr0200 d,
             pr0100 e
       WHERE c.numcaso = a.ad08numcaso
         AND d.gc01fctreg_ibs = 7
         AND d.gc01fpclav_ibs = c.traba8
         AND d.pr01codactuacion = b.fa05codorigen
         AND b.fa08codgrupo = 7
         AND d.pr01codactuacion = e.pr01codactuacion
         AND e.pr12codactividad <> 201/* CONSULTAS SE DEVUELVEN EN LA FAIBM2J1*/
         AND (
                   (
                          b.fa08codgrupo = 7
                      AND e.pr01codactuacion >=
                                            2000/*NO DEVOLVER NADA DE LABORATORIO*/
                   )
                OR (b.fa08codgrupo = 9)                   /*NI DE BANCO DE SANGRE*/
             )
         AND NOT EXISTS (SELECT fa05codcateg
                           FROM fa2600
                          WHERE fa2600.fa05codcateg = b.fa05codcateg)
      UNION ALL
      SELECT                                               /*TACS Y RM MULTIPLES */
          a.ad07codproceso,
          a.ad01codasistenci,
          g.fa05codcateg,
          1,
          TO_DATE (c.freal8, 'YYYYMMDD'),
          TO_NUMBER (SUBSTR (c.nhist8, 1, 6)),
          MIN (TO_NUMBER (c.ssoli8)),
          MIN (TO_CHAR (c.docto8)),
          TO_NUMBER (SUBSTR (c.traba8, 1, 3)),
          '0',
          MIN (fafn01 (
                  a.ad07codproceso, a.ad01codasistenci, TO_DATE (
                                                           c.freal8 || c.hreal8,
                                                           'YYYYMMDDHH24MI'
                                                        ), g.fa05codcateg
               )),
          1 ,
          to_number(null),
          DECODE(c.flag8,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r8mov c,
             pr0200 d,
             pr0100 e,
             fa2600 f,
             fa2800 g
       WHERE c.numcaso = a.ad08numcaso
         AND d.gc01fctreg_ibs = 7
         AND d.gc01fpclav_ibs = c.traba8
         AND d.pr01codactuacion = b.fa05codorigen
         AND b.fa08codgrupo = 7
         AND d.pr01codactuacion = e.pr01codactuacion
         AND b.fa05codcateg = f.fa05codcateg
         AND f.fa27codgrupofac = g.fa27codgrupofac
       GROUP BY a.ad07codproceso,
                a.ad01codasistenci,
                g.fa05codcateg,
                TO_DATE (c.freal8, 'YYYYMMDD'),
                FLOOR (TO_NUMBER (SUBSTR (hreal8, 1, 2)) / 14),
                TO_NUMBER (SUBSTR (c.nhist8, 1, 6)),
                TO_NUMBER (SUBSTR (c.traba8, 1, 3)),
                g.fa28cantdesde,
                g.fa28canthasta,
                DECODE(c.flag8,'0A',-1,0)
      HAVING COUNT (b.fa05codcateg) BETWEEN g.fa28cantdesde AND g.fa28canthasta;

   CREATE OR REPLACE VIEW faibm2j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT /*Pruebas del IBM pendientes de facturar*/
             ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic               
        FROM faibm2j1
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic               
        FROM faibm2j2;

   CREATE OR REPLACE VIEW faibm0002j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic               
        FROM faibm2j
       WHERE fecha <= TO_DATE ('30121999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic               
        FROM fa0001j
       WHERE fecha > TO_DATE ('30121999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic               
        FROM fa0002j
       WHERE fecha > TO_DATE ('30121999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic               
        FROM fa0006j
       WHERE fecha > TO_DATE ('30121999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT                                             /*Pruebas de Laboratorio*/
          ad0800.ad07codproceso,
          ad0800.ad01codasistenci,
          fa0500.fa05codcateg,
          COUNT (distinct pa.secuencia) AS cantidad,
          MIN (NVL (ma.fechaextraccion, ma.fechacreacion)),
          pa.historia,
          pa.dptosolicitud,
          pa.drsolicitud,
          MIN (pr0200.ad02coddpto),
          '0',
          '',
          DECODE (pa.estado, 1, 0, 1),
          TO_NUMBER (NULL),
          DECODE(investigacion,0,0,-1)
        FROM pruebaasistencia pa,
             fa0500,
             pr0200,
             muestraprueba mp,
             muestraasistencia ma,
             seguimientoprueba sp,
             ad0800
       WHERE fa0500.fa05codorigen = pa.ctipoprueba
         AND fa0500.fa08codgrupo = 7
         AND mp.historia = pa.historia
         AND mp.caso = pa.caso
         AND mp.secuencia = pa.secuencia
         AND sp.historia = pa.historia
         AND sp.caso = pa.caso
         AND sp.secuencia = pa.secuencia
         AND sp.nrepeticion = 1
         AND sp.proceso = 1
         AND ma.cmuestra = mp.cmuestra
         AND pa.estado <= 9
         AND pr0200.pr01codactuacion = pa.ctipoprueba
         AND pa.historia = SUBSTR (ad0800.ad08numcaso, 1, 6)
         AND pa.caso = SUBSTR (ad0800.ad08numcaso, 7, 4)
         AND MOD (pa.secuencia, 100) = 0
       GROUP BY ad0800.ad07codproceso,
                ad0800.ad01codasistenci,
                fa0500.fa05codcateg,
                pa.historia,
                pa.caso,
                pa.secuencia,
                pa.dptosolicitud,
                pa.drsolicitud,
                DECODE (pa.estado, 1, 0, 1),
                DECODE(investigacion,0,0,-1);

   CREATE OR REPLACE VIEW fatmpj (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT *
        FROM faibm0002j
      UNION ALL
      SELECT *
        FROM faibm0003j
      UNION ALL
      SELECT *
        FROM faibm0000j
      UNION ALL
      SELECT *
        FROM faibm0004j
      UNION ALL
      SELECT *
        FROM faibm0005j
      UNION ALL
      SELECT *
        FROM fa0008j
      UNION ALL
      SELECT *
        FROM fa0010j;
