
   CREATE OR REPLACE VIEW fa0306j (
      ci22numhistoria,
      paciente,
      ad01codasistenci,
      ad01fecinicio,
      ad01fecfin,
      ad07codproceso,
      dptorea,
      pr01codactuacion,
      pr01descorta,
      pr04feciniact,
      pr04fecfinact,
      ci32codtipecon,
      ci13codentidad,
      dptoresp,
      drresp
   ) AS
      (
   /* Contiene las actuaciones realizadas en cada fecha,
   con su tipo econůmico y dpto/dr responsable */
       SELECT/*+ ORDERED */
           DISTINCT ci2200.ci22numhistoria,
                    ci22priapel || ' ' || ci22segapel || ', ' || ci22nombre paciente,
                    pr0400.ad01codasistenci,
                    ad01fecinicio,
                    ad01fecfin,
                    pr0400.ad07codproceso,
                    pr0400.ad02coddpto dptorea,
                    pr0400.pr01codactuacion,
                    pr01descorta,
                    TRUNC (pr04feciniact) pr04feciniact,
                    TRUNC (pr04fecfinact) pr04fecfinact,
                    ad1100.ci32codtipecon,
                    ad1100.ci13codentidad,
                    ad0500.ad02coddpto dptoresp,
                    ad0500.sg02cod drresp
         FROM pr0400,
              ci2200,
              pr0100,
              ad0100,
              ad1100,
              ad0500
        WHERE pr0400.pr37codestado = 4
          AND ci2200.ci21codpersona = pr0400.ci21codpersona
          AND ad0100.ad01codasistenci = pr0400.ad01codasistenci
          AND pr0100.pr01codactuacion = pr0400.pr01codactuacion
          AND ad1100.ad01codasistenci = pr0400.ad01codasistenci
          AND ad1100.ad07codproceso = pr0400.ad07codproceso
          AND pr04fecfinact BETWEEN ad11fecinicio AND NVL (
                                                         ad11fecfin,
                                                         TO_DATE (
                                                            '31/12/9999',
                                                            'DD/MM/YYYY'
                                                         )
                                                      )
          AND ad0500.ad01codasistenci = pr0400.ad01codasistenci
          AND ad0500.ad07codproceso = pr0400.ad07codproceso
          AND pr04fecfinact BETWEEN ad05fecinirespon AND NVL (
                                                            ad05fecfinrespon,
                                                            TO_DATE (
                                                               '31/12/9999',
                                                               'DD/MM/YYYY'
                                                            )
                                                         )
   /*
   AND NOT EXISTS
           (SELECT FA04NUMFACT
           FROM    FA0300, FA0500
           WHERE           FA0300.AD01CODASISTENCI = PR0400.AD01CODASISTENCI
                   AND FA0300.AD07CODPROCESO = PR0400.AD07CODPROCESO
                   AND TRUNC(FA0300.FA03FECHA) = TRUNC(PR0400.PR04FECFINACT)
                   AND FA0300.FA05CODCATEG = FA0500.FA05CODCATEG
                   AND FA0300.FA04NUMFACT IS NOT NULL
                   AND FA0500.FA08CODGRUPO = 7
                   AND FA0500.FA05CODORIGEN = PR0400.PR01CODACTUACION
           )
   */
                                                          )
