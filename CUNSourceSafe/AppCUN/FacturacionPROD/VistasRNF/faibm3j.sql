   CREATE OR REPLACE VIEW faibm3j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT /*Estancias en Clinica*/
             ad07codproceso,
             ad01codasistenci,
             fa05codcateg,
             cantidad,
             fecha,
             ci22numhistoria,
             dptosolicita,
             TO_CHAR (drsolicita),
             dptorealiza,
             TO_CHAR (drrealiza),
             obs,
             estado,
             0
        FROM faibm3
