   CREATE OR REPLACE VIEW faibm1j2 (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      ic
   ) AS
      SELECT a.ad07codproceso,
             a.ad01codasistenci ad01codasistenci,
             35353 fa05codcateg,
             1 cantidad,
             TO_DATE (c.finic2, 'YYYYMMDD'),
             TO_NUMBER (SUBSTR (c.nhist2, 1, 6)) ci22numhistoria,
             TO_NUMBER (c.servc2) dptosolicita,
             '0' drsolicita,
             TO_NUMBER (sanes2) dptorealiza,
             TO_CHAR (anest2) drrealiza,
             '',
             1 ,
             DECODE(c.flag2,'0A',-1,0)
        FROM ad0800 a,
             fa0500 b,
             r2mov c,
             pr0200 d
       WHERE c.numcaso = a.ad08numcaso
         AND b.fa05codorigen = d.pr01codactuacion
         AND d.gc01fpclav_ibs = c.inter2
         AND d.gc01fctreg_ibs = 6
         AND b.fa08codgrupo = 6
         AND sanes2 IS NOT NULL
