
   CREATE OR REPLACE VIEW fa9901j (
      codorigen,
      desorigen,
      fa08codgrupo,
      fa08desig,
      indutilizado
   ) AS
      (
   /* PRUEBAS */
       SELECT TO_CHAR (pr01codactuacion) AS codorigen,
              UPPER (NVL (pr01descompleta, pr01descorta)) AS desorigen,
              fa0800.fa08codgrupo,
              fa0800.fa08desig,
              NVL (fa06codramacat, 0) / NVL (fa06codramacat, 1) AS 
   indutilizado
         FROM pr0100,
              fa0800,
              fa0502j
        WHERE fa0502j.fa05codorigen (+) = pr0100.pr01codactuacion
          AND fa0502j.fa08codgrupo (+) = 7
          AND fa0800.fa08codgrupo = 7
          AND fa0800.fa11codestgrupo = 1
          AND pr0100.pr01fecfin IS NULL
          AND pr12codactividad IN (203, 208, 215)
       UNION ALL
   /* INTERVENCIONES */
       SELECT TO_CHAR (pr01codactuacion) AS codorigen,
              UPPER (NVL (pr01descompleta, pr01descorta)) AS desorigen,
              fa0800.fa08codgrupo,
              fa0800.fa08desig,
              NVL (fa06codramacat, 0) / NVL (fa06codramacat, 1) AS 
   indutilizado
         FROM pr0100,
              fa0800,
              fa0502j
        WHERE fa0502j.fa05codorigen (+) = pr0100.pr01codactuacion
          AND fa0502j.fa08codgrupo (+) = 6
          AND fa0800.fa08codgrupo = 6
          AND fa0800.fa11codestgrupo = 1
          AND pr0100.pr01fecfin IS NULL
          AND pr12codactividad IN (207)
       UNION ALL
   /* BANCO DE SANGRE */
       SELECT TO_CHAR (bs01codprod) AS codorigen,
              UPPER (bs01descrip) AS desorigen,
              fa0800.fa08codgrupo,
              fa0800.fa08desig,
              NVL (fa06codramacat, 0) / NVL (fa06codramacat, 1) AS 
   indutilizado
         FROM bs0100,
              fa0800,
              fa0502j
        WHERE fa0502j.fa05codorigen (+) = bs0100.bs01codprod
          AND fa0502j.fa08codgrupo (+) = 9
          AND fa0800.fa08codgrupo = 9
          AND fa0800.fa11codestgrupo = 1
          AND bs0100.bs01indactiva = -1
       UNION ALL
   /* FARMACIA */
       SELECT fr73codintfar AS codorigen,
              UPPER (fr73desproducto) AS desorigen,
              fa0800.fa08codgrupo,
              fa0800.fa08desig,
              NVL (fa06codramacat, 0) / NVL (fa06codramacat, 1) AS 
   indutilizado
         FROM fr7300,
              fa0800,
              fa0502j
        WHERE fa0502j.fa05codorigen (+) = fr7300.fr73codintfar
          AND fa0502j.fa08codgrupo (+) = 1
          AND fa0800.fa08codgrupo = 1
          AND fa0800.fa11codestgrupo = 1
          AND fr7300.fr73fecfinvig IS NULL)
