   CREATE OR REPLACE VIEW faibm0002j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic,
      grupo,
      PVL,
      codExt
   ) AS
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic,
	     NULL,
	     NULL,
	     NULL               
        FROM faibm2j
       WHERE fecha <= TO_DATE ('30121999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic,               
	     NULL,
	     NULL,
	     NULL               
        FROM fa0001j
       WHERE fecha > TO_DATE ('30121999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic,               
	     NULL,
	     NULL,
	     NULL               
        FROM fa0002j
       WHERE fecha > TO_DATE ('30121999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT ad07codproceso,  
             ad01codasistenci,
             fa05codcateg,    
             cantidad,        
             fecha,           
             ci22numhistoria, 
             dptosolicita,    
             drsolicita,      
             dptorealiza,     
             drrealiza,       
             obs,             
             estado,          
             precio,          
             ic,               
	     NULL,
	     NULL,
	     NULL               
        FROM fa0006j
       WHERE fecha > TO_DATE ('30121999 16', 'DDMMYYYY HH24')
      UNION ALL
      SELECT                                             /*Pruebas de Laboratorio*/
          ad0800.ad07codproceso,
          ad0800.ad01codasistenci,
          fa0500.fa05codcateg,
          COUNT (distinct pa.secuencia) AS cantidad,
          MIN (NVL (ma.fechaextraccion, ma.fechacreacion)),
          pa.historia,
          pa.dptosolicitud,
          pa.drsolicitud,
          MIN (pr0200.ad02coddpto),
          '0',
          '',
          DECODE (pa.estado, 1, 0, 1),
          TO_NUMBER (NULL),
          DECODE(investigacion,0,0,-1),
	  NULL,
	  NULL,
	  NULL               
        FROM pruebaasistencia pa,
             fa0500,
             pr0200,
             muestraprueba mp,
             muestraasistencia ma,
             seguimientoprueba sp,
             ad0800
       WHERE fa0500.fa05codorigen = pa.ctipoprueba
         AND fa0500.fa08codgrupo = 7
         AND mp.historia = pa.historia
         AND mp.caso = pa.caso
         AND mp.secuencia = pa.secuencia
         AND sp.historia = pa.historia
         AND sp.caso = pa.caso
         AND sp.secuencia = pa.secuencia
         AND sp.nrepeticion = 1
         AND sp.proceso = 1
         AND ma.cmuestra = mp.cmuestra
         AND pa.estado <= 9
         AND pr0200.pr01codactuacion = pa.ctipoprueba
         AND pa.historia = SUBSTR (ad0800.ad08numcaso, 1, 6)
         AND pa.caso = SUBSTR (ad0800.ad08numcaso, 7, 4)
         AND MOD (pa.secuencia, 100) = 0
       GROUP BY ad0800.ad07codproceso,
                ad0800.ad01codasistenci,
                fa0500.fa05codcateg,
                pa.historia,
                pa.caso,
                pa.secuencia,
                pa.dptosolicitud,
                pa.drsolicitud,
                DECODE (pa.estado, 1, 0, 1),
                DECODE(investigacion,0,0,-1)
