



   CREATE OR REPLACE VIEW fa0406j (fa48coddiskacunsa, fa1r00) AS
      SELECT DISTINCT fa4800.fa48coddiskacunsa,
                      LPAD (SUBSTR (fa0400.fa04numfact, 4), 7, '0') ||
                                                  -- Numero de Factura: Number(7)  
                         '00' ||                        -- Registros de tipo '00'  
                         TO_CHAR (fa04fecfactura, 'YYYYMMDD') ||
                                                  -- Fecha de Factura  'YYYYMMDD'  
                         LPAD (NVL (ci03dni, ' '), 12, ' ') ||
                                                          -- DNI/NIF Varchar2(12)  
                         LPAD (NVL (TO_CHAR (ci03numpoliza), '          '), 10, '0') ||
                                                   -- Numero de poliza Number(10)  
                         TO_CHAR (ad08fecinicio, 'YYYYMMDD') ||
                                                              -- Fecha de Ingreso  
                         RPAD (NVL (TO_CHAR (ad08fecfin, 'YYYYMMDD'), ' '), 8, ' ') ||
                                                                  -- Fecha de Fin  
                         DECODE (ad12codtipoasist, 1, 'H', 'A') ||
                                                  -- Tipo de Asistencia 'H' o 'A'  
                         SUBSTR (ad08numcaso, 1, 6) ||
                         LPAD (SUBSTR (ad08numcaso, 7, 4), 5, '0') ||
                                         -- Historia, Caso : Number(6), Number(5)  
                         RPAD (
                            ci22priapel || ',' || ci22segapel || ',' || ci22nombre,
                            48, ' '
                         ) ||        -- Apellido1 Apellido2, Nombre : Varchar(48)  
                         RPAD (ad0500.ad02coddpto, 3, '0') ||
                                                -- Servicio Responsable:Number(3)  
                         LPAD (NVL (TO_CHAR (ci03numasegura), ' '), 6, ' ') ||
                                        -- Numero de As3egurado ACUNSA: Number(6)  
                         LPAD (NVL (ci36codtippoli, ' '), 3, ' ')
                            fa1r00                     -- Tipo de poliza: Char(3)  
        FROM fa0400,
             ad0800,
             ad2500,
             ad0500,
             ci2200,
             fa4800,
             fa4900,
             ci0300
       WHERE fa0400.ad07codproceso = ad0800.ad07codproceso
         AND fa0400.ad01codasistenci = ad0800.ad01codasistenci
         AND fa0400.ad07codproceso = ad0500.ad07codproceso
         AND ad0500.ad05fecfinrespon IS NULL
         AND fa0400.ad01codasistenci = ad0500.ad01codasistenci
         AND fa0400.ad01codasistenci = ad2500.ad01codasistenci
         AND ad2500.ad25fecfin IS NULL
         AND ci0300.ci22numhistoria (+) = SUBSTR (ad0800.ad08numcaso, 1, 6)
         AND ci0300.ci03fecinipoli (+) <= ad0800.ad08fecinicio
         AND NVL (ci0300.ci03fecfinpoli (+), SYSDATE) >= ad0800.ad08fecinicio
         AND fa0400.ci32codtipecon = 'J'
         AND ci2200.ci22numhistoria = SUBSTR (ad0800.ad08numcaso, 1, 6)
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa04numfacreal IS NULL
         AND fa0400.fa04cantfact > 0
