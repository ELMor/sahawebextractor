



   CREATE OR REPLACE VIEW fa0401j (
      ci22numhistoria,
      ci22nombre,
      ci22priapel,
      ci22segapel,
      ci21codpersona,
      reco,
      ci16codlocalid,
      ci17codmunicip,
      ci26codprovi,
      ci19codpais,
      ci10calle,
      ci10portal,
      ci10restodirec,
      ci07codpostal,
      fa04fecfactura,
      fa04numfact,
      fa04cantfact
   ) AS
      SELECT t22_1.ci22numhistoria,
             t22_1.ci22nombre,
             t22_1.ci22priapel,
             t22_1.ci22segapel,
             t22_2.ci21codpersona,
             t22_2.ci22nombre || ' ' || t22_2.ci22priapel || ' ' || t22_2.ci22segapel reco,
             t10.ci16codlocalid,
             t10.ci17codmunicip,
             t10.ci26codprovi,
             t10.ci19codpais,
             t10.ci10calle,
             t10.ci10portal,
             t10.ci10restodirec,
             t10.ci07codpostal,
             t4.fa04fecfactura,
             t4.fa04numfact,
             t4.fa04cantfact
        FROM fa0400 t4,
             ci2200 t22_1,
             ci2200 t22_2,
             ad0700 t7,
             ci1000 t10
       WHERE t4.fa04indcompensa < 2
         AND t4.fa04indnmgc IS NULL
         AND t22_2.ci21codpersona = t4.ci21codpersona
         AND t10.ci21codpersona = t4.ci21codpersona
         AND t10.ci10inddirprinc = -1
         AND t7.ad07codproceso = t4.ad07codproceso
         AND t22_1.ci21codpersona = t7.ci21codpersona
      UNION ALL
      SELECT t22.ci22numhistoria,
             t22.ci22nombre,
             t22.ci22priapel,
             t22.ci22segapel,
             t23.ci21codpersona,
             t23.ci23razonsocial reco,
             t10.ci16codlocalid,
             t10.ci17codmunicip,
             t10.ci26codprovi,
             t10.ci19codpais,
             t10.ci10calle,
             t10.ci10portal,
             t10.ci10restodirec,
             t10.ci07codpostal,
             t4.fa04fecfactura,
             t4.fa04numfact,
             t4.fa04cantfact
        FROM fa0400 t4,
             ci2200 t22,
             ci2300 t23,
             ad0700 t7,
             ci1000 t10
       WHERE t4.fa04indcompensa < 2
         AND t4.fa04indnmgc IS NULL
         AND t23.ci21codpersona = t4.ci21codpersona
         AND t10.ci21codpersona = t4.ci21codpersona
         AND t10.ci10inddirprinc = -1
         AND t7.ad07codproceso = t4.ad07codproceso
         AND t22.ci21codpersona = t7.ci21codpersona
