



   CREATE OR REPLACE VIEW fa0411j (
      fa48coddiskacunsa,
      movacun,
      fa04numfact,
      importe
   ) AS
      SELECT fa4800.fa48coddiskacunsa,
             'M' ||                                                        -- 'M'  
                    SUBSTR (fa0400.fa04numfact, 4) ||        -- Numero de Factura  
                '8' ||
             -- Tipo 8: Pruebas Menos las de Taller Ortopedico y menos Honorarios  
                TO_CHAR (fa03fecha, 'YYYYMMDD') ||          -- fecha de la prueba  
                LPAD (fa0500.fa05codcateg, 6, '0') ||
                                               -- Codigo de la prueba : Number(6)  
                LPAD (fa03cantfact, 4, '0') ||   -- Cantidad Facturada: Number(4)  
                LPAD (
                   (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact, 10, '0'
                ) ||                        -- Precio total Facturado: Number(10)  
                LPAD (fa0300.ad02coddpto_r, 3, '0') ||
                                           -- Departamento que realiza: Number(3)  
                LPAD (fa0300.sg02cod_r, 4, ' '),            -- Doctor que realiza  
             fa0400.fa04numfact,
             (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact importe
        FROM fa0400,
             fa0300,
             fa0500,
             fa4800,
             fa4900,
             fa1600
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0400.fa04codfact = fa1600.fa04codfact
         AND fa0300.fa16numlinea = fa1600.fa16numlinea
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0500.fa08codgrupo IN (7, 9)
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa0300.ad02coddpto_r <> 404                      --TALLER ORTOPEDICO  
         AND fa04numfacreal IS NULL
         AND NOT (
                       (
                              NVL (fa05codorigc2, -1) = 0
                          AND NVL (fa05codorigc3, -1) IN (0, 1, 2, 3, 5, 7, 8)
                       )
                    OR fa05codorigen IN (SELECT pr01codactuacion
                                           FROM pr0100
                                          WHERE pr12codactividad IN (
                                                                       201,
                                                                       214,
                                                                       215
                                                                    ))
                 )
      UNION ALL
      SELECT fa4800.fa48coddiskacunsa,
             'M' ||                                                        -- 'M'  
                    SUBSTR (fa0400.fa04numfact, 4) ||        -- Numero de Factura  
                '6' ||                                      -- Tipo 6: Honorarios  
                TO_CHAR (fa03fecha, 'YYYYMMDD') ||          -- fecha de la prueba  
                LPAD (fa0500.fa05codcateg, 6, '0') ||
                                               -- Codigo de la prueba : Number(6)  
                LPAD (fa03cantfact, 4, '0') ||   -- Cantidad Facturada: Number(4)  
                LPAD (
                   (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact, 10, '0'
                ) ||                        -- Precio total Facturado: Number(10)  
                LPAD (fa0300.ad02coddpto_r, 3, '0') ||
                                           -- Departamento que realiza: Number(3)  
                LPAD (fa0300.sg02cod_r, 4, ' '),            -- Doctor que realiza  
             fa0400.fa04numfact,
             (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact importe
        FROM fa0400,
             fa0300,
             fa0500,
             fa4800,
             fa4900,
             fa1600
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0400.fa04codfact = fa1600.fa04codfact
         AND fa0300.fa16numlinea = fa1600.fa16numlinea
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0500.fa08codgrupo IN (7, 9)
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa0300.ad02coddpto_r <> 404                      --TALLER ORTOPEDICO  
         AND fa04numfacreal IS NULL
         AND fa0300.fa03cantfact <> 0
         AND (
                   (
                          NVL (fa05codorigc2, -1) = 0
                      AND NVL (fa05codorigc3, -1) IN (0, 1, 2, 3, 5, 7, 8)
                   )
                OR fa05codorigen IN (SELECT pr01codactuacion
                                       FROM pr0100
                                      WHERE pr12codactividad IN (201, 214, 215))
             )
      UNION ALL
      SELECT fa4800.fa48coddiskacunsa,
             'M' ||                                                        -- 'M'  
                    SUBSTR (fa0400.fa04numfact, 4) ||        -- Numero de factura  
                '1' ||                           -- Tipo 1: Farmacia sin Protesis  
                LPAD (SUM ( (100 - fa16dcto) / 100 * fa16importe), 10, '0'),
                                                 -- Suma de Farmacia sin Protesis  
             fa0400.fa04numfact,
             SUM ( (100 - fa16dcto) / 100 * fa16importe) importe
        FROM fa0400,
             fa1600,
             fa4800,
             fa4900
       WHERE fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa0400.fa04numfacreal IS NULL
         AND fa0400.fa04cantfact <> 0
         AND fa1600.fa04codfact = fa0400.fa04codfact
         AND fa1600.fa16numlinea IN (SELECT DISTINCT t2.fa16numlinea
                                       FROM fa0300 t2,
                                            fa0500 t3,
                                            fr7300 t4
                                      WHERE t2.ad02coddpto_r = 215
                                        AND t2.fa05codcateg = t3.fa05codcateg
                                        AND t2.fa04numfact = fa0400.fa04codfact
                                        AND t4.fr73codintfar = t3.fa05codorigen
                                        AND t4.fr00codgrpterap NOT LIKE
                                               'QI%')
       GROUP BY fa4800.fa48coddiskacunsa, fa0400.fa04numfact
      UNION ALL
      SELECT fa4800.fa48coddiskacunsa,
             'M' ||                                                        -- 'M'  
                    SUBSTR (fa0400.fa04numfact, 4) ||        -- Numero de factura  
                '9' ||           -- Tipo 9: Farmacia Prótesis + Taller Ortopédico  
                LPAD (SUM ( (100 - fa16dcto) / 100 * fa16importe), 10, '0'),
                                             -- Suma Prótesis + Taller Ortopédico  
             fa0400.fa04numfact,
             SUM ( (100 - fa16dcto) / 100 * fa16importe) importe
        FROM fa0400,
             fa1600,
             fa4800,
             fa4900
       WHERE fa04numfacreal IS NULL
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa1600.fa04codfact = fa0400.fa04codfact
         AND (
                   (
                      fa1600.fa16numlinea IN (SELECT DISTINCT t2.fa16numlinea
                                                FROM fa0300 t2,
                                                     fa0500 t3,
                                                     fr7300 t4
                                               WHERE t2.ad02coddpto_r = 215
                                                 AND t2.fa05codcateg =
                                                                    t3.fa05codcateg
                                                 AND t2.fa04numfact =
                                                                 fa0400.fa04codfact
                                                 AND t4.fr73codintfar =
                                                                   t3.fa05codorigen
                                                 AND t4.fr00codgrpterap LIKE
                                                        'QI%')
                   )
                OR (
                      fa1600.fa16numlinea IN (SELECT DISTINCT fa16numlinea
                                                FROM fa0300 t5
                                               WHERE t5.fa04numfact =
                                                                 fa0400.fa04codfact
                                                 AND t5.ad02coddpto_r = 404)
                   )
             )
       GROUP BY fa4800.fa48coddiskacunsa, fa0400.fa04numfact
      UNION ALL
      SELECT fa4800.fa48coddiskacunsa,
             'M' ||                                                        -- 'M'  
                    SUBSTR (fa0400.fa04numfact, 4) ||        -- Numero de Factura  
                '2' ||                                  -- Tipo 2: Intervenciones  
                TO_CHAR (fa03fecha, 'YYYYMMDD') ||    -- fecha de la Intervencion  
                LPAD (fa0500.fa05codcateg, 6, '0') ||
                                         -- Codigo de la intervencion : Number(6)  
                LPAD (fa03cantfact, 4, '0') ||   -- Cantidad Facturada: Number(4)  
                LPAD (
                   (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact, 10, '0'
                ) ||                        -- Precio total Facturado: Number(10)  
                LPAD (ad02coddpto_r, 3, '0') ||
                                           -- Departamento que realiza: Number(3)  
                LPAD (sg02cod_r, 4, ' '),                   -- Doctor que realiza  
             fa0400.fa04numfact,
             (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact importe
        FROM fa0400,
             fa0300,
             fa0500,
             fa4800,
             fa4900,
             fa1600
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0400.fa04codfact = fa1600.fa04codfact
         AND fa0300.fa16numlinea = fa1600.fa16numlinea
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0500.fa08codgrupo = 6
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa04numfacreal IS NULL
      UNION ALL
      SELECT fa4800.fa48coddiskacunsa,
             'M' ||                                                        -- 'M'  
                    SUBSTR (fa0400.fa04numfact, 4) ||        -- Numero de Factura  
                '3' ||                           -- Tipo 3: Derechos de quirofano  
                TO_CHAR (MIN (fa03fecha), 'YYYYMMDD') ||
                                                      -- fecha de la Intervencion  
                LPAD (fa05codcateg, 6, '0') ||
                                   -- Codigo de derechos de quirofano : Number(6)  
                LPAD (SUM (fa03cantfact * 15), 4, '0') ||
                                       -- Cantidad Facturada (Minutos): Number(4)  
                LPAD ( (100 - fa16dcto) / 100 * fa16importe, 10, '0'),
                                                             -- Importe facturado  
             fa0400.fa04numfact,
             (100 - fa16dcto) / 100 * fa16importe importe
        FROM fa0400,
             fa0300,
             fa4800,
             fa4900,
             fa1600
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0300.fa05codcateg IN (35354, 35352)
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa04numfacreal IS NULL
         AND fa0400.fa04codfact = fa1600.fa04codfact
         AND fa0300.fa16numlinea = fa1600.fa16numlinea
         AND fa0300.fa03cantfact <> 0
       GROUP BY fa4800.fa48coddiskacunsa,
                fa0400.fa04numfact,
                fa0300.fa05codcateg,
                fa1600.fa16importe,
                fa1600.fa16dcto
      UNION ALL
      SELECT fa4800.fa48coddiskacunsa,
             'M' ||                                                        -- 'M'  
                    SUBSTR (fa0400.fa04numfact, 4) ||        -- Numero de Factura  
                '4' ||                                     -- Tipo 4: Anestesista  
                TO_CHAR (MIN (fa03fecha), 'YYYYMMDD') ||
                                                      -- fecha de la Intervencion  
                LPAD (fa05codcateg, 6, '0') ||
                                  -- Codigo de honorarios anestesista : Number(6)  
                LPAD (SUM (fa03cantfact), 4, '0') ||
                                                 -- Cantidad Facturada: Number(4)  
                LPAD ( (100 - fa16dcto) / 100 * fa16importe, 10, '0'),
                                            -- Precio total Facturado: Number(10)  
             fa0400.fa04numfact,
             (100 - fa16dcto) / 100 * fa16importe importe
        FROM fa0400,
             fa0300,
             fa4800,
             fa4900,
             fa1600
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0300.fa05codcateg = 35353
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa04numfacreal IS NULL
         AND fa1600.fa04codfact = fa0400.fa04codfact
         AND fa0300.fa16numlinea = fa1600.fa16numlinea
       GROUP BY fa4800.fa48coddiskacunsa,
                fa0400.fa04numfact,
                fa0300.fa05codcateg,
                fa16importe,
                fa16dcto
      UNION ALL
      SELECT fa4800.fa48coddiskacunsa,
             'M' ||                                                        -- 'M'  
                    SUBSTR (fa0400.fa04numfact, 4) ||        -- Numero de Factura  
                '5' ||                                       -- Tipo 5: Estancias  
                TO_CHAR (fa03fecha, 'YYYYMMDD') ||        -- fecha de la estancia  
                LPAD (fa0500.fa05codcateg, 6, '0') ||
                                             -- Codigo de la estancia : Number(6)  
                LPAD (fa03cantfact, 4, '0') ||   -- Cantidad Facturada: Number(4)  
                LPAD (
                   (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact, 10, '0'
                ) ||                        -- Precio total Facturado: Number(10)  
                LPAD (ad02coddpto_r, 3, '0') ||
                                           -- Departamento que realiza: Number(3)  
                LPAD (sg02cod_r, 4, ' '),                   -- Doctor que realiza  
             fa0400.fa04numfact,
             (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact importe
        FROM fa0400,
             fa0300,
             fa0500,
             fa4800,
             fa4900,
             fa1600
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0400.fa04codfact = fa1600.fa04codfact
         AND fa0300.fa16numlinea = fa1600.fa16numlinea
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0500.fa08codgrupo = 5
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa04numfacreal IS NULL
