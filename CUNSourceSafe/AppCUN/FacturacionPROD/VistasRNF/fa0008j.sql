   CREATE OR REPLACE VIEW fa0008j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic,
      grupo,
      PVL,
      codExt
   ) AS
      SELECT/*+ RULE */
   /* VISTA RNF DE BANCO DE SANGRE */
   /* Productos Administrados*/
          bs0300.ad07codproceso,
          bs0300.ad01codasistenci,
          fa0500.fa05codcateg,
          COUNT (*),
          NVL(NVL (bs1800.bs18fecsalida, bs1800.bs18fecadd), AD0100.AD01FECINICIO),
          ad0100.ci22numhistoria,
          TO_NUMBER (NULL),
          NULL,
          201,
          NULL,
          '',
          1,
          TO_NUMBER (NULL),
          0,
	  NULL,
	  NULL,
	  NULL
        FROM fa0500,
             bs1800,
             bs0300,
             ad0100
       WHERE bs1800.bs03codbolsa = bs0300.bs03codbolsa
         AND ad0100.ad01codasistenci = bs0300.ad01codasistenci
         AND fa0500.fa05codorigc1 = 201
         AND fa0500.fa08codgrupo = 9
         AND fa0500.fa05codorigen = bs0300.bs01codprod
         AND bs0300.bs10codestbolsa IN (5, 6) /* Unidades Enviadas, Transfundidas*/
         AND (
                   bs1800.bs18indtransf = -1
                OR (    bs1800.bs18indtransf = 0
                    AND bs1800.bs18fecentrada IS NULL)
             )
       GROUP BY bs0300.ad07codproceso,
                bs0300.ad01codasistenci,
                fa0500.fa05codcateg,
                NVL(NVL (bs1800.bs18fecsalida, bs1800.bs18fecadd), AD0100.AD01FECINICIO),
                ad0100.ci22numhistoria
      UNION ALL
   /* Pruebas Cruzadas */
      SELECT bs1400.ad07codproceso,
             bs1400.ad01codasistenci,
             11866,                                          /* Pruebas Cruzadas */
             COUNT (*),
             NVL(NVL (bs1400.bs14feccruce, bs1400.bs14fecadd), AD0100.AD01FECINICIO),
             ad0100.ci22numhistoria,
             TO_NUMBER (NULL),
             NULL,
             201,
             NULL,
             '',
             1,
             TO_NUMBER (NULL),
             0,
	     NULL,
	     NULL,
	     NULL
        FROM bs1400,
             bs0300,
             bs0100,
             bs3200,
             ad0100
       WHERE ad0100.ad01codasistenci = bs1400.ad01codasistenci
         AND bs0300.bs03codbolsa = bs1400.bs03codbolsa
         AND bs0100.bs01codprod = bs0300.bs01codprod
         AND bs3200.bs32codtiprod = bs0100.bs32codtiprod
         AND bs3200.bs32indprcruzadas = -1
       GROUP BY bs1400.ad07codproceso,
                bs1400.ad01codasistenci,
                NVL(NVL (bs1400.bs14feccruce, bs1400.bs14fecadd), AD0100.AD01FECINICIO),
                ad0100.ci22numhistoria
      UNION ALL
   /* Anticuerpos Irregulares*/
      SELECT bs2000.ad07codproceso,
             bs2000.ad01codasistenci,
             11872,                                   /* Anticuerpos Irregulares */
             1,
             NVL(NVL (bs2000.bs20fecpeticion, bs2000.bs20fecadd), AD0100.AD01FECINICIO),
             ad0100.ci22numhistoria,
             TO_NUMBER (NULL),
             NULL,
             201,
             NULL,
             '',
             1,
             TO_NUMBER (NULL),
             0,
	     NULL,
	     NULL,
	     NULL
        FROM bs2000,
             bs3200,
             ad0100
       WHERE ad0100.ad01codasistenci = bs2000.ad01codasistenci
         AND bs3200.bs32codtiprod = bs2000.bs32codtiprod
         AND bs2000.bs22codestreserva < 9
         AND bs3200.bs32indprcruzadas = -1
       GROUP BY bs2000.ad07codproceso,
                bs2000.ad01codasistenci,
                NVL(NVL (bs2000.bs20fecpeticion, bs2000.bs20fecadd), AD0100.AD01FECINICIO),
                ad0100.ci22numhistoria
      UNION ALL
   /* Unidades Caducadas*/
      SELECT/*+ rule */ bs1400.ad07codproceso,
                        bs1400.ad01codasistenci,
                        fa0500.fa05codcateg,
                        COUNT (*),
                        bs0300.bs03feccaduc,
                        ad0100.ci22numhistoria,
                        TO_NUMBER (NULL),
                        NULL,
                        201,
                        NULL,
                        '',
                        1,
                        TO_NUMBER (NULL),
                        0,
			NULL,
			NULL,
			NULL
        FROM bs1400,
             bs0300,
             fa0500,
             ad0100
       WHERE ad0100.ad01codasistenci = bs1400.ad01codasistenci
         AND bs0300.bs03codbolsa = bs1400.bs03codbolsa
         AND fa0500.fa05codorigc1 = 201
         AND fa0500.fa08codgrupo = 9
         AND fa0500.fa05codorigen = bs0300.bs01codprod
         AND bs0300.bs10codestbolsa = 7
         AND bs1400.bs14fecdescruce IS NULL
       GROUP BY bs1400.ad07codproceso,
                bs1400.ad01codasistenci,
                fa0500.fa05codcateg,
                bs0300.bs03feccaduc,
                ad0100.ci22numhistoria
      UNION ALL
      SELECT/*+ rule */ bs0300.ad07codproceso,
                        bs0300.ad01codasistenci,
                        fa0500.fa05codcateg,
                        COUNT (*),
                        bs0300.bs03feccaduc,
                        bs0300.ci22numhistoria,
                        TO_NUMBER (NULL),
                        NULL,
                        201,
                        NULL,
                        '',
                        1,
                        TO_NUMBER (NULL),
                        0,
			NULL,
			NULL,
			NULL
        FROM bs0300,
             fa0500
       WHERE fa0500.fa05codorigc1 = 201
         AND fa0500.fa08codgrupo = 9
         AND fa0500.fa05codorigen = bs0300.bs01codprod
         AND bs0300.bs10codestbolsa = 7
       GROUP BY bs0300.ad07codproceso,
                bs0300.ad01codasistenci,
                fa0500.fa05codcateg,
                bs0300.bs03feccaduc,
                bs0300.ci22numhistoria
