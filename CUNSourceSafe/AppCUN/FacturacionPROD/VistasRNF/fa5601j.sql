



   CREATE OR REPLACE VIEW fa5601j (
      ci32codtipecon,
      ci13codentidad,
      ad12codtipoasist,
      fa57numimpreso
   ) AS
      SELECT ci32codtipecon, ci13codentidad, ad12codtipoasist, fa57numimpreso
        FROM ci1300,
             fa5600
       WHERE ci1300.fa55codtipimpfac = fa5600.fa55codtipimpfac
