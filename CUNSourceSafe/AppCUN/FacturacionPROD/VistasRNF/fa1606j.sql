
   CREATE OR REPLACE VIEW fa1606j (
      fa04codfact,
      fa16numlinea,
      fa14codnodoconc,
      fa16descrip,
      fa16prctgrnf,
      fa16precio,
      fa16dcto,
      fa16importe,
      fa16ordfact,
      sg02cod_add,
      fa16fecadd,
      sg02cod_upd,
      fa16fecupd,
      fa16franquicia,
      fa16cantsinfran,
      fa15codatrib
   ) AS
      (SELECT fa04codfact,
              fa16numlinea,
              fa14codnodoconc,
              RTRIM (RPAD (fa16descrip, 50)) AS fa16descrip,
              fa16prctgrnf,
              fa16precio,
              fa16dcto,
              fa16importe,
              fa16ordfact,
              sg02cod_add,
              fa16fecadd,
              sg02cod_upd,
              fa16fecupd,
              fa16franquicia,
              fa16cantsinfran,
              fa15codatrib
         FROM fa1600)

