   CREATE OR REPLACE VIEW fatmpj (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT *
        FROM faibm0002j
      UNION ALL
      SELECT *
        FROM faibm0003j
      UNION ALL
      SELECT *
        FROM faibm0000j
      UNION ALL
      SELECT *
        FROM faibm0004j
      UNION ALL
      SELECT *
        FROM faibm0005j
      UNION ALL
      SELECT *
        FROM fa0008j
      UNION ALL
      SELECT *
        FROM fa0010j;
