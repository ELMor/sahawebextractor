



   CREATE OR REPLACE VIEW fa0409j (fa48coddiskacunsa, fa1r99) AS
      SELECT fa4800.fa48coddiskacunsa,
             '0000000' ||                                              -- Factura  
                          '99' ||                           -- Registro tipo '99'  
                LPAD (SUM (fa04cantfact), 10, '0') fa1r99        -- Importe Total  
        FROM fa0400,
             fa4800,
             fa4900
       WHERE fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa04numfacreal IS NULL
         AND fa04cantfact <> 0
       GROUP BY fa4800.fa48coddiskacunsa
