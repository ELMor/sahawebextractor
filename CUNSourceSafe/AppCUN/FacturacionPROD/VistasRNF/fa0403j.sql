



   CREATE OR REPLACE VIEW fa0403j (
      fa04nyap,
      fa04numfact,
      fa04cantfact,
      ad08fecinicio,
      ad08fecfin,
      fa04diag_princ,
      ad01codasistenci,
      ad07codproceso,
      fa04codfact
   ) AS
      SELECT ci22priapel || ' ' || ci22segapel || ', ' || ci22nombre AS fa04nyap,
             fa0400.fa04numfact,
             fa0400.fa04cantfact,
             ad0800.ad08fecinicio,
             ad0800.ad08fecfin,
             fafn05 (fa0400.ad01codasistenci, fa0400.ad07codproceso) AS fa04diag_princ,
             fa0400.ad01codasistenci,
             fa0400.ad07codproceso,
             fa0400.fa04codfact
        FROM fa0400,
             ad0700,
             ad0800,
             ci2200
       WHERE fa0400.ad01codasistenci = ad0800.ad01codasistenci
         AND fa0400.ad07codproceso = ad0800.ad07codproceso
         AND ad0800.ad07codproceso = ad0700.ad07codproceso
         AND ad0700.ci21codpersona = ci2200.ci21codpersona
         AND fa0400.fa04cantfact > 0
         AND fa0400.fa04numfacreal IS NULL
