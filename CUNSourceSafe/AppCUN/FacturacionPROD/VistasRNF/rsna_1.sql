
   CREATE OR REPLACE VIEW rsna_1 (ad01codasistenci, ad07codproceso) AS
      (SELECT a1.ad01codasistenci, a8.ad07codproceso
         FROM ad0800 a8,
              ad0100 a1
        WHERE a1.ad01codasistenci = a8.ad01codasistenci
          AND EXISTS (SELECT ci21codpersona
                        FROM rsna_2
                       WHERE rsna_2.ci21codpersona = a1.ci21codpersona
                         AND (
                                   rsna_2.ad01codasistenci <> 
   a8.ad01codasistenci
                                OR rsna_2.ad07codproceso <> a8.ad07codproceso
                             )
                         AND rsna_2.ad01fecfin < a1.ad01fecinicio
                         AND a1.ad01fecinicio - rsna_2.ad01fecfin < 180))
