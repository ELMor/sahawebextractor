
   CREATE OR REPLACE VIEW fa0301j (
      ad07codproceso,
      ad01codasistenci,
      ci32codtipecon,
      ci13codentidad,
      ci22numhistoria,
      ad01fecinicio,
      ad01fecfin,
      minfa03,
      maxfa03
   ) AS
      (
   /* vista para la obtenci�n de estancias sin facturar */
       SELECT ad1600.ad07codproceso,
              ad1600.ad01codasistenci,
              ad1100.ci32codtipecon,
              ad1100.ci13codentidad,
              ad0100.ci22numhistoria,
              TO_DATE (TO_CHAR (ad0100.ad01fecinicio, 'DD/MM/YYYY'), 'DD/MM/YYYY') AS ad01fecinicio,
              TO_DATE (TO_CHAR (ad0100.ad01fecfin, 'DD/MM/YYYY'), 'DD/MM/YYYY') AS ad01fecfin,
              TO_DATE (TO_CHAR (MIN (fa0300.fa03fecha), 'DD/MM/YYYY'), 'DD/MM/YYYY') AS minfa03,
              TO_DATE (TO_CHAR (MAX (fa0300.fa03fecha), 'DD/MM/YYYY'), 'DD/MM/YYYY') AS maxfa03
         FROM ad1600,
              ad1100,
              ad0100,
              fa0300
        WHERE ad1100.ad07codproceso = ad1600.ad07codproceso
          AND ad1100.ad01codasistenci = ad1600.ad01codasistenci
          AND ad1100.ad11fecfin IS NULL
          AND ad0100.ad01codasistenci = ad1600.ad01codasistenci
          AND ad1600.ad16feccambio < TO_DATE ('1/2/2000', 'DD/MM/YYYY')
          AND (
                    ad1600.ad16fecfin > TO_DATE ('1/2/2000', 'DD/MM/YYYY')
                 OR ad1600.ad16fecfin IS NULL
              )
          AND ad1600.ad14codestcama = 2
          AND fa0300.ad01codasistenci (+) = ad1600.ad01codasistenci
          AND fa0300.ad07codproceso (+) = ad1600.ad07codproceso
          AND (
                    fa0300.fa03fecha BETWEEN TO_DATE ('1/1/2000', 'DD/MM/YYYY')
                        AND TO_DATE ('31/1/2000', 'DD/MM/YYYY')
                 OR fa0300.fa03fecha IS NULL
              )
          AND (
                    ad0100.ad01fecfin > TO_DATE ('1/1/2000', 'DD/MM/YYYY')
                 OR ad0100.ad01fecfin IS NULL
              )
        GROUP BY ad1600.ad07codproceso,
                 ad1600.ad01codasistenci,
                 ad1100.ci32codtipecon,
                 ad1100.ci13codentidad,
                 ad0100.ci22numhistoria,
                 ad0100.ad01fecinicio,
                 ad0100.ad01fecfin)
