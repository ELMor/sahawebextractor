



   CREATE OR REPLACE VIEW fa04xx (fecha, fa04numfact, fa04cantfact) AS
      (SELECT/*+ RULE */
           DECODE (ad2500.ad12codtipoasist, 1, MAX (fa03fecha), MIN (fa03fecha)) AS fecha,
           fa0400.fa04numfact,
           fa04cantfact
         FROM fa0400,
              ad2500,
              fa0300,
              ad0100
        WHERE fa0300.fa04numfact = fa0400.fa04codfact
          AND ad0100.ad01codasistenci = fa0400.ad01codasistenci
          AND fa0400.fa04numfacreal IS NULL
          AND ad2500.ad01codasistenci = ad0100.ad01codasistenci
          AND ad2500.ad25fecfin IS NULL
          AND fa0400.ci32codtipecon = 'J'
          AND fa0400.fa04fecfactura > TO_DATE ('1/1/2000', 'DD/MM/YYYY')
        GROUP BY fa0400.fa04numfact, fa04cantfact, ad2500.ad12codtipoasist)
