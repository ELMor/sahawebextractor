



   CREATE OR REPLACE VIEW fa0419j (
      fa04codfact,
      fa04fecfactura,
      ci32codtipecon,
      ci13codentidad,
      tipoasist
   ) AS
      (SELECT/*+ RULE */
           fa04codfact,
           fa04fecfactura,
           ci32codtipecon,
           ci13codentidad,
           1 AS tipoasist
         FROM fa0400
        WHERE EXISTS (SELECT/*+ RULE */
                          fa03numrnf
                        FROM fa0500,
                             fa0300
                       WHERE fa0300.fa04numfact = fa0400.fa04codfact
                         AND fa0500.fa05codcateg = fa0300.fa05codcateg
                         AND fa0500.fa08codgrupo = 5)
       UNION ALL
       SELECT/*+ RULE */ fa04codfact,
                         fa04fecfactura,
                         ci32codtipecon,
                         ci13codentidad,
                         2 AS tipoasist
         FROM fa0400
        WHERE NOT EXISTS (SELECT/*+ RULE */
                              fa03numrnf
                            FROM fa0500,
                                 fa0300
                           WHERE fa0300.fa04numfact = fa0400.fa04codfact
                             AND fa0500.fa05codcateg = fa0300.fa05codcateg
                             AND fa0500.fa08codgrupo = 5))
