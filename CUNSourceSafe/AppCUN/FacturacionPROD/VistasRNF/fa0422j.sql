
   CREATE OR REPLACE VIEW fa0422j (
      ad07codproceso,
      ad01codasistenci,
      ad01fecinicio,
      ad01fecfin,
      ci32codtipecon,
      ci13codentidad,
      fa04codfact,
      fa04numfact,
      fa04cantfact,
      fa04fecfactura,
      ci21codpersona,
      ci22numhistoria,
      ad02coddpto,
      ad02desdpto,
      sg02cod,
      paciente,
      fa16numlinea,
      fa16descrip,
      fa16franquicia,
      fa16cantsinfran,
      fa16importe,
      desig,
      fa15codatrib,
      FA09CODNODOCONC_FACT
   ) AS
      (
   /* VISTA PARA OBTENER PROCESOS ASISTENCIAS FACTURADOS Y SUS LINEAS*/
       SELECT/*+ RULE */
           ad0800.ad07codproceso,
           ad0800.ad01codasistenci,
           NVL (fa04fecdesde, ad0100.ad01fecinicio) AS ad01fecinicio,
           NVL (fa04fechasta, ad0100.ad01fecfin) AS ad01fecfin,
           /* DECODE ( SUBSTR(FA1500.FA15RUTA,6,3),
                376,1,
                379,1,
                2
             ) AS AD12CODTIPOASIST,
           FA0419J.TIPOASIST AS AD12CODTIPOASIST, */
           fa0400.ci32codtipecon,
           fa0400.ci13codentidad,
           fa0400.fa04codfact,
           fa0400.fa04numfact,
           fa0400.fa04cantfact,
           fa0400.fa04fecfactura,
           ad0100.ci21codpersona,
           ad0100.ci22numhistoria,
           ad0500.ad02coddpto,
           ad0200.ad02desdpto,
           ad0500.sg02cod,
           (
              ci2200.ci22priapel || ' ' || ci2200.ci22segapel || ', ' ||
                 ci2200.ci22nombre
           ) AS paciente,
           fa1600.fa16numlinea,
           fa1600.fa16descrip,
           TO_NUMBER (
              DECODE (fa1600.fa16franquicia, -1, NULL, fa1600.fa16franquicia)
           ) AS fa16franquicia,
           TO_NUMBER (
              DECODE (fa1600.fa16cantsinfran, 0, NULL, fa1600.fa16cantsinfran)
           ) AS fa16cantsinfran,
           fa1600.fa16importe,
           RTRIM (LTRIM (LPAD (fa1600.fa16descrip, 20))) AS desig,
           fa1600.fa15codatrib,
	   fa0400.FA09CODNODOCONC_FACT
         FROM ad1100,
              ad0800,
              fa0400,
              ad0500,
              ci2200,
              ad0200,
              fa1500,
              fa1600,
              ad0100
   /*  FA0419J */
        WHERE ad0800.ad01codasistenci = ad0100.ad01codasistenci
          AND fa1500.fa15codatrib (+) = fa1600.fa15codatrib
          /* AND FA0419J.FA04CODFACT = FA0400.FA04CODFACT */
          AND fa0400.ad01codasistenci = ad0800.ad01codasistenci
          AND fa0400.ad07codproceso = ad0800.ad07codproceso
          AND fa0400.fa04numfacreal IS NULL
          AND ad0500.ad01codasistenci = ad0800.ad01codasistenci
          AND ad0500.ad07codproceso = ad0800.ad07codproceso
          AND ad0500.ad05fecfinrespon IS NULL
          AND ci2200.ci21codpersona = ad0100.ci21codpersona
          AND ad0200.ad02coddpto = ad0500.ad02coddpto
          AND fa1600.fa04codfact = fa0400.fa04codfact
          AND (   fa1600.fa16importe > 0
               OR fa1600.fa16franquicia > 0)
          AND fa0400.fa04cantfact > 0
          AND EXISTS (SELECT/*+ rule */
                          ad01codasistenci
                        FROM ad1800
                       WHERE ad1800.ad07codproceso = fa0400.ad07codproceso
                         AND ad1800.ad01codasistenci = fa0400.ad01codasistenci
                         AND ad1800.ci21codpersona = ci2200.ci21codpersona
                         AND ad1800.ad19codtipodocum = 1)
          AND ad1100.ad01codasistenci = ad0800.ad01codasistenci
          AND ad1100.ad07codproceso = ad0800.ad07codproceso
          AND ad1100.ad11fecfin IS NULL
          AND (   ad1100.ad11indvolante = 0
               OR ad1100.ad11indvolante IS NULL)
	AND NOT EXISTS 
		(SELECT FA04NUMFACT
		FROM	FA0300,
			FA0500
		WHERE		FA0500.FA05CODCATEG = FA0300.FA05CODCATEG
			AND	FA0500.FA08CODGRUPO=5
			AND	FA0300.FA04NUMFACT = FA0400.FA04CODFACT
		)
       UNION ALL
       /* SE A�ADEN LAS FACTURAS DE LA FA0400 QUE SEAN 0 PERO TENGAN VOLANTE */
       SELECT/*+ RULE */ ad0800.ad07codproceso,
                         ad0800.ad01codasistenci,
                         NVL (fa04fecdesde, ad0100.ad01fecinicio) AS ad01fecinicio,
                         NVL (fa04fechasta, ad0100.ad01fecfin) AS ad01fecfin,
   /* DECODE ( SUBSTR(FA1500.FA15RUTA,6,3),
         376,1,
         379,1,
         2
      ) AS AD12CODTIPOASIST,
    FA0419J.TIPOASIST AS AD12CODTIPOASIST, */
                         fa0400.ci32codtipecon,
                         fa0400.ci13codentidad,
                         fa0400.fa04codfact,
                         fa0400.fa04numfact,
                         fa0400.fa04cantfact,
                         fa0400.fa04fecfactura,
                         ad0100.ci21codpersona,
                         ad0100.ci22numhistoria,
                         ad0500.ad02coddpto,
                         ad0200.ad02desdpto,
                         ad0500.sg02cod,
                         (
                            ci2200.ci22priapel || ' ' || ci2200.ci22segapel || ', ' ||
                               ci2200.ci22nombre
                         ) AS paciente,
                         fa1600.fa16numlinea,
                         fa1600.fa16descrip,
                         TO_NUMBER (
                            DECODE (
                               fa1600.fa16franquicia, -1, NULL,
                               fa1600.fa16franquicia
                            )
                         )
                               AS fa16franquicia,
                         TO_NUMBER (
                            DECODE (
                               fa1600.fa16cantsinfran, 0, NULL,
                               fa1600.fa16cantsinfran
                            )
                         )
                               AS fa16cantsinfran,
                         fa1600.fa16importe,
                         RTRIM (LTRIM (LPAD (fa1600.fa16descrip, 20))) AS desig,
                         fa1600.fa15codatrib,
			 fa0400.FA09CODNODOCONC_FACT
         FROM
              /* FA0419J, */
              ad1100,
              ad0800,
              fa1500,
              fa0400,
              ad0500,
              ci2200,
              ad0200,
              fa1600,
              ad0100
        WHERE ad0800.ad01codasistenci = ad0100.ad01codasistenci
          AND fa1500.fa15codatrib (+) = fa1600.fa15codatrib
   /*  AND FA0419J.FA04CODFACT = FA0400.FA04CODFACT */
          AND fa0400.ad01codasistenci = ad0800.ad01codasistenci
          AND fa0400.ad07codproceso = ad0800.ad07codproceso
          AND fa0400.fa04numfacreal IS NULL
          AND ad0500.ad01codasistenci = ad0800.ad01codasistenci
          AND ad0500.ad07codproceso = ad0800.ad07codproceso
          AND ad0500.ad05fecfinrespon IS NULL
          AND ci2200.ci21codpersona = ad0100.ci21codpersona
          AND ad0200.ad02coddpto = ad0500.ad02coddpto
          AND fa0400.fa04cantfact = 0
          AND fa1600.fa04codfact = fa0400.fa04codfact
          AND fa1600.fa16numlinea = 1
          AND EXISTS (SELECT/*+ RULE */
                          ad01codasistenci
                        FROM ad1800
                       WHERE ad1800.ad07codproceso = fa0400.ad07codproceso
                         AND ad1800.ad01codasistenci = fa0400.ad01codasistenci
                         AND ad1800.ci21codpersona = ci2200.ci21codpersona
                         AND ad1800.ad19codtipodocum = 1)
          AND ad1100.ad01codasistenci = ad0800.ad01codasistenci
          AND ad1100.ad07codproceso = ad0800.ad07codproceso
          AND ad1100.ad11fecfin IS NULL
          AND (   ad1100.ad11indvolante = 0
               OR ad1100.ad11indvolante IS NULL)
	AND NOT EXISTS 
		(SELECT FA04NUMFACT
		FROM	FA0300,
			FA0500
		WHERE		FA0500.FA05CODCATEG = FA0300.FA05CODCATEG
			AND	FA0500.FA08CODGRUPO=5
			AND	FA0300.FA04NUMFACT = FA0400.FA04CODFACT
		)
)
