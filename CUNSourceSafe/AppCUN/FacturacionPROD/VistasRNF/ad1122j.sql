

   CREATE OR REPLACE VIEW ad1122j (
      ci22nombre,
      ci22priapel,
      ci22segapel,
      ci22dni,
      ci22fecnacim,
      ci22numsegsoc,
      ci30codsexo,
      ci22nombrealf,
      ci22priapelalf,
      ci22segapelalf,
      ad01codasistenci,
      ad07codproceso,
      ad11fecinicio,
      ad02coddpto,
      ad02desdpto,
      ad12codtipoasist,
      ad12destipoasist
   ) AS
      SELECT ci22nombre,
             ci22priapel,
             ci22segapel,
             ci22dni,
             ci22fecnacim,
             ci22numsegsoc,
             ci30codsexo,
             ci22nombrealf,
             ci22priapelalf,
             ci22segapelalf,
             ad01codasistenci,
             ad07codproceso,
             ad11fecinicio,
             ad02coddpto,
             ad02desdpto,
             ad12codtipoasist,
             ad12destipoasist
        FROM ad1121j
       WHERE NOT EXISTS (SELECT ad01codasistenci
                           FROM fa2500
                          WHERE ad01codasistenci = ad1121j.ad01codasistenci
                            AND ad07codproceso = ad1121j.ad07codproceso
                            AND fa25valid = -1)
         AND NOT EXISTS (SELECT ad01codasistenci
                           FROM fa3200
                          WHERE ad01codasistenci = ad1121j.ad01codasistenci
                            AND ad07codproceso = ad1121j.ad07codproceso)
