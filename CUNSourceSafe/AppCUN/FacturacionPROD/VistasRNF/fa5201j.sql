



   CREATE OR REPLACE VIEW fa5201 (
      fa52tipoasistencia,
      fa52fecproceso,
      fa04numfact,
      importe,
      fa04fecfactura
   ) AS
      (SELECT fa52tipoasistencia,
              fa52fecproceso,
              MIN (fa0400.fa04numfact) AS fa04numfact,
              SUM (TO_NUMBER (fa52cantfact)) AS importe,
              MAX (fa0400.fa04fecfactura) AS fa04fecfactura
         FROM fa0400,
              fa5200
        WHERE fa0400.fa04codfact = fa5200.fa04codfact
          AND fa0400.fa04numfacreal IS NULL
        GROUP BY fa52tipoasistencia, fa52fecproceso)
