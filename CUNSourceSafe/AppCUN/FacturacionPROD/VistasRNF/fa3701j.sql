CREATE OR REPLACE VIEW fa3701j (
   tasi,
   fecha,
   ad01codasistenci,
   ad07codproceso,
   ci32codtipecon,
   ci13codentidad
) AS
   SELECT /*Altas Hosp/Amb en un mes y tipo economico. 
            Se basa en prueba de Hospit. (no valido Enero-Febrero 2000)*/
          'H',
          LAST_DAY(trunc(pr0400.pr04fecfinact)),
          pr0400.ad01codasistenci,
          pr0400.ad07codproceso,
          ad1100.ci32codtipecon,
          ad1100.ci13codentidad
     FROM pr0100,
          ad1100,
          pr0400
    WHERE pr0400.pr37codestado = 4
      AND pr0100.pr01codactuacion = pr0400.pr01codactuacion
      AND pr0100.pr12codactividad = 209
      AND ad1100.ad01codasistenci = pr0400.ad01codasistenci
      AND ad1100.ad07codproceso = pr0400.ad07codproceso
   UNION ALL
   SELECT 'A',
          LAST_DAY(trunc(ad0800.ad08fecfin)),
          ad0800.ad01codasistenci,
          ad0800.ad07codproceso,
          ad1100.ci32codtipecon,
          ad1100.ci13codentidad
     FROM ad1100,
          ad0800
    WHERE ad1100.ad01codasistenci = ad0800.ad01codasistenci
      AND ad1100.ad07codproceso = ad0800.ad07codproceso
      AND NOT EXISTS (SELECT pr0400.ad01codasistenci, pr0400.ad07codproceso
                        FROM pr0100,
                             pr0400
                       WHERE pr0400.pr37codestado = 4
                         AND pr0100.pr01codactuacion = pr0400.pr01codactuacion
                         AND pr0100.pr12codactividad = 209
                         AND ad0800.ad01codasistenci = pr0400.ad01codasistenci
                         AND ad0800.ad07codproceso = pr0400.ad07codproceso)
