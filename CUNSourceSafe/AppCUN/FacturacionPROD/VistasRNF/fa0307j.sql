   CREATE OR REPLACE VIEW fa0307j (
      grupo,
      ci22numhistoria,
      fa04fecfactura,
      ci13codentidad,
      fa03fecha,
      fa05codcateg,
      fa05desig,
      cantidad,
      precio,
      ad02coddpto,
      fa04numfact1,
      ci32codtipecon,
      fechaAlta
   ) AS
      SELECT          /*valoracion entidad colaboradora */               /*HONORARIOS*/
	  /*+ RULE */
          1 grupo,
          ad0700.ci22numhistoria,
          min(fa0400.fa04fecfactura),
          fa0400.ci13codentidad,
          trunc(fa0300.fa03fecha),
          fa0300.fa05codcateg,
          fa0500.fa05desig,
          sum(fa0300.fa03cantfact),
          sum(fa0300.fa03cantfact * fa03preciofact),
          fa0300.ad02coddpto_r,
          min(fa0400.fa04codfact),
          fa0400.ci32codtipecon,
	  max(ad0100.ad01fecfin)
        FROM fa0300,
             fa0400,
             fa0500,
             ad0700,
	     ad0100
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0400.fa04numfact LIKE 'I%'
         AND fa0400.ad07codproceso = ad0700.ad07codproceso
         AND TO_CHAR(fa0500.fa08codgrupo) = '7'
         and ad0100.ad01codasistenci = fa0300.ad01codasistenci
         AND NVL (fa0500.fa05codorigc2, -1) = 0
         AND NVL (fa0500.fa05codorigc3, -1) IN (0, 1, 2, 3, 5, 7, 8)
    GROUP BY ad0700.ci22numhistoria,fa0400.ci13codentidad,trunc(fa0300.fa03fecha),
             fa0300.fa05codcateg,fa0500.fa05desig,fa0300.ad02coddpto_r,
	     fa0400.ci32codtipecon
      UNION ALL
      SELECT                                                            /*PRUEBAS*/
	  /*+ RULE */
          2 grupo,
          ad0700.ci22numhistoria,
          fa0400.fa04fecfactura,
          fa0400.ci13codentidad,
          TRUNC (fa0300.fa03fecha),
          fa0300.fa05codcateg,
          fa0500.fa05desig,
          fa0300.fa03cantfact,
          fa0300.fa03cantfact * fa03preciofact,
          fa0300.ad02coddpto_r,
          fa0400.fa04codfact,
          fa0400.ci32codtipecon,
	  ad0100.ad01fecfin
        FROM fa0300,
             fa0400,
             fa0500,
             ad0700,
	     ad0100
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0400.fa04numfact LIKE 'I%'
         AND fa0400.ad07codproceso = ad0700.ad07codproceso
	 and ad0100.ad01codasistenci = fa0300.ad01codasistenci
         AND fa0500.fa08codgrupo = 7
         AND (
                   NVL (fa0500.fa05codorigc2, -1) <> 0
                OR NVL (fa0500.fa05codorigc3, -1) NOT IN (0, 1, 2, 3, 4, 5, 7, 8)
             )
      UNION ALL
      SELECT                                                     /*INTERVENCIONES*/
          /*+ RULE */
	  3 grupo,
          ad0700.ci22numhistoria,
          fa0400.fa04fecfactura,
          fa0400.ci13codentidad,
          TRUNC (fa0300.fa03fecha),
          fa0300.fa05codcateg,
          fa0500.fa05desig,
          fa0300.fa03cantfact,
          fa0300.fa03cantfact * fa03preciofact,
          fa0300.ad02coddpto_r,
          fa0400.fa04codfact,
          fa0400.ci32codtipecon,
	  ad0100.ad01fecfin
        FROM fa0300,
             fa0400,
             fa0500,
             ad0700,
	     ad0100
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0400.fa04numfact LIKE 'I%'
         AND fa0400.ad07codproceso = ad0700.ad07codproceso
	 AND AD0100.AD01CODASISTENCI = FA0300.AD01CODASISTENCI
         AND (
                   fa0500.fa08codgrupo = 6
                OR fa0500.fa05codcateg BETWEEN 35352 AND 35354
             )
      UNION ALL
      SELECT                                                          /*ESTANCIAS*/
          /*+ RULE */
	  4 grupo,
          ad0700.ci22numhistoria,
          fa0400.fa04fecfactura,
          fa0400.ci13codentidad,
          TRUNC (MAX (fa0300.fa03fecha)),
          fa0300.fa05codcateg,
          fa0500.fa05desig,
          SUM (fa0300.fa03cantfact),
          SUM (fa0300.fa03cantfact * fa03preciofact),
          fa0300.ad02coddpto_r,
          fa0400.fa04codfact,
          fa0400.ci32codtipecon,
	  max(ad0100.ad01fecfin)
        FROM fa0300,
             fa0400,
             fa0500,
             ad0700,
	     ad0100
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0400.ad07codproceso = ad0700.ad07codproceso
         AND fa0400.fa04numfact LIKE 'I%'
         AND fa0500.fa08codgrupo = 5
	 and ad0100.ad01codasistenci = fa0300.ad01codasistenci
       GROUP BY ad0700.ci22numhistoria,
                fa0400.fa04fecfactura,
                fa0400.ci13codentidad,
                fa0300.fa05codcateg,
                fa0500.fa05desig,
                fa0300.ad02coddpto_r,
                fa0400.fa04codfact,
                fa0400.ci32codtipecon
      UNION ALL
      SELECT                                                           /*FARMACIA*/
          /*+ RULE */
	  5 grupo,
          ad0700.ci22numhistoria,
          fa0400.fa04fecfactura,
          fa0400.ci13codentidad,
          TRUNC (MIN (fa0300.fa03fecha)),
          1,
          'FARMACIA',
          1,
          SUM (fa0300.fa03cantfact * fa03preciofact),
          fa0300.ad02coddpto_r,
          fa0400.fa04codfact,
          fa0400.ci32codtipecon,	
	  max(ad0100.ad01fecfin)
        FROM fa0300,
             fa0400,
             fa0500,
             ad0700,
	     ad0100
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0400.ad07codproceso = ad0700.ad07codproceso
         AND fa0400.fa04numfact LIKE 'I%'
         AND fa0500.fa08codgrupo = 1
	 and ad0100.ad01codasistenci = fa0300.ad01codasistenci
       GROUP BY ad0700.ci22numhistoria,
                fa0400.fa04fecfactura,
                fa0400.ci13codentidad,
                fa0300.ad02coddpto_r,
                fa0400.fa04codfact,
                fa0400.ci32codtipecon
     UNION ALL
      SELECT                                                            /*BANCO DE SANGRE*/
	  /*+ RULE */
          9 grupo,
          ad0700.ci22numhistoria,
          fa0400.fa04fecfactura,
          fa0400.ci13codentidad,
          TRUNC (fa0300.fa03fecha),
          fa0300.fa05codcateg,
          fa0500.fa05desig,
          fa0300.fa03cantfact,
          fa0300.fa03cantfact * fa03preciofact,
          fa0300.ad02coddpto_r,
          fa0400.fa04codfact,
          fa0400.ci32codtipecon,
	  ad0100.ad01fecfin
        FROM fa0300,
             fa0400,
             fa0500,
             ad0700,
	     ad0100
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0400.fa04numfact LIKE 'I%'
         AND fa0400.ad07codproceso = ad0700.ad07codproceso
	 and ad0100.ad01codasistenci = fa0300.ad01codasistenci
         AND fa0500.fa08codgrupo = 9
