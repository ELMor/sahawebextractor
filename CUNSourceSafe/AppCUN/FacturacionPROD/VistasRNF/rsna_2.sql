
   CREATE OR REPLACE VIEW rsna_2 (
      ci21codpersona,
      ad07codproceso,
      ad01codasistenci,
      procesoasistencia,
      ad01fecfin,
      ci32codtipecon,
      ci13codentidad,
      ad08numcaso,
      ad12codtipoasist,
      ad12destipoasist,
      fechasasistencia,
      ad02coddpto,
      ad02desdpto,
      rsnumreg,
      rsfact,
      rsfecha,
      rstddiag,
      rscant,
      rsimpor,
      rsdescr,
      rstipo
   ) AS
      (SELECT ad0100.ci21codpersona,
              ad0800.ad07codproceso,
              ad0800.ad01codasistenci,
              TO_CHAR (ad0800.ad07codproceso) || ' - ' ||
                 TO_CHAR (ad0800.ad01codasistenci) AS procesoasistencia,
              ad0100.ad01fecfin,
              ad1100.ci32codtipecon,
              ad1100.ci13codentidad,
              ad0800.ad08numcaso,
              ad2500.ad12codtipoasist,
              ad1200.ad12destipoasist,
              TO_CHAR (ad0100.ad01fecinicio, 'DD/MM/YYYY') || ' - ' ||
                 TO_CHAR (ad0100.ad01fecfin, 'DD/MM/YYYY') AS 
   fechasasistencia,
              ad0200.ad02coddpto,
              ad0200.ad02desdpto,
              rsna.rsnumreg,
              rsna.rsfact,
              rsna.rsfecha,
              rsna.rstddiag,
              rsna.rscant,
              rsna.rsimpor,
              rsna.rsdescr,
              rsna.rstipo
         FROM rsna,
              ad0100,
              ad0800,
              ad0500,
              ad0200,
              ad2500,
              ad1200,
              ad1100
        WHERE ad0800.ad01codasistenci = ad0100.ad01codasistenci
          AND rsna.rshist = TO_NUMBER (SUBSTR (ad0800.ad08numcaso, 1, 6))
          AND rsna.rscaso = TO_NUMBER (SUBSTR (ad0800.ad08numcaso, 7, 4))
          AND ad0500.ad01codasistenci = ad0800.ad01codasistenci
          AND ad0500.ad07codproceso = ad0800.ad07codproceso
          AND ad0500.ad05fecfinrespon IS NULL
          AND ad0200.ad02coddpto = ad0500.ad02coddpto
          AND ad2500.ad01codasistenci = ad0800.ad01codasistenci
          AND ad2500.ad25fecfin IS NULL
          AND ad1200.ad12codtipoasist = ad2500.ad12codtipoasist
          AND ad1100.ad01codasistenci = ad0800.ad01codasistenci
          AND ad1100.ad07codproceso = ad0800.ad07codproceso
          AND ad1100.ad11fecfin IS NULL)
