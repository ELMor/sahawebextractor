



   CREATE OR REPLACE VIEW fa0407j (fa48coddiskacunsa, fa1r10, fa04numfact, importe) AS
      SELECT fa4800.fa48coddiskacunsa,
             LPAD (SUBSTR (fa0400.fa04numfact, 4), 7, '0') ||
                                                              -- Numero de Factura: Number(7)  
                                                              '10' ||
                -- Registros de tipo '10'  
                RPAD (fa0300.ad02coddpto_r, 3, '0') ||
                -- Servicio Realizador:Number(3)  
                RPAD (gcfn05 (ad02desdpto), 12, ' ') ||
                -- Nombre del servicio anterior: Varchar  
                LPAD (
                   SUM ( (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact),
                   10, '0'
                ) fa1r10,                                  -- Reparto: Number(10)  
             fa0400.fa04numfact,
             SUM ( (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact) importe
        FROM fa0400,
             fa0300,
             ad0200,
             fa4800,
             fa4900,
             fa0500,
             fa1600
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa0400.fa04codfact = fa1600.fa04codfact
         AND fa0300.fa16numlinea = fa1600.fa16numlinea
         AND ad0200.ad02coddpto = fa0300.ad02coddpto_r
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa0400.fa04numfacreal IS NULL
         AND fa0400.fa04cantfact <> 0
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0300.fa05codcateg <> 35353                /*Honorarios Anestesista*/
         AND fa0300.ad02coddpto_r <> 213                              /*Quirofano*/
         AND (
                   fa0300.ad02coddpto_r <> 215
                OR (    fa0300.ad02coddpto_r = 215
                    AND fa0500.fa08codgrupo <> 1)
             )
       GROUP BY fa4800.fa48coddiskacunsa,
                fa0400.fa04numfact,
                ad02coddpto_r,
                ad02desdpto
      UNION ALL
      SELECT                                                          /*Quirofano*/
          DISTINCT fa4800.fa48coddiskacunsa,
                   LPAD (SUBSTR (fa0400.fa04numfact, 4), 7, '0') ||
                                                                    -- Numero de Factura: Number(7)  
                                                                    '10' ||
                      -- Registros de tipo '10'  
                      RPAD (fa0300.ad02coddpto_r, 3, '0') ||
                      -- Servicio Realizador:Number(3)  
                      RPAD (gcfn05 (ad02desdpto), 12, ' ') ||
                      -- Nombre del servicio anterior: Varchar  
                      LPAD ( ( (100 - fa16dcto) / 100 * fa16importe), 10, '0') fa1r10,
                   -- Reparto: Number(10)  
                   fa0400.fa04numfact,
                   (100 - fa16dcto) / 100 * fa16importe importe
        FROM fa0400,
             fa0300,
             fa1600,
             ad0200,
             fa4800,
             fa4900
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND ad0200.ad02coddpto = fa0300.ad02coddpto_r
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa0400.fa04numfacreal IS NULL
         AND fa0400.fa04cantfact <> 0
         AND fa0300.ad02coddpto_r = 213
         AND fa1600.fa04codfact = fa0400.fa04codfact
         AND fa0300.fa16numlinea = fa1600.fa16numlinea
      UNION ALL
      SELECT/*Honorarios Anestesia*/
          DISTINCT fa4800.fa48coddiskacunsa,
                   LPAD (SUBSTR (fa0400.fa04numfact, 4), 7, '0') ||
                                                                    -- Numero de Factura: Number(7)  
                                                                    '10' ||
                      -- Registros de tipo '10'  
                      RPAD (fa0300.ad02coddpto_r, 3, '0') ||
                      -- Servicio Realizador:Number(3)  
                      RPAD (gcfn05 (ad02desdpto), 12, ' ') ||
                      -- Nombre del servicio anterior: Varchar  
                      LPAD ( (100 - fa16dcto) / 100 * fa16importe, 10, '0') fa1r10,
                   -- Reparto: Number(10)  
                   fa0400.fa04numfact,
                   (100 - fa16dcto) / 100 * fa16importe importe
        FROM fa0400,
             fa0300,
             fa1600,
             ad0200,
             fa4800,
             fa4900
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND ad0200.ad02coddpto = fa0300.ad02coddpto_r
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa0400.fa04numfacreal IS NULL
         AND fa0400.fa04cantfact <> 0
         AND fa0300.fa05codcateg = 35353
         AND fa1600.fa04codfact = fa0400.fa04codfact
         AND fa0300.fa16numlinea = fa1600.fa16numlinea
      UNION ALL
      SELECT                                                           /*Farmacia*/
          fa4800.fa48coddiskacunsa,
          LPAD (SUBSTR (fa0400.fa04numfact, 4), 7, '0') ||
                                                           -- Numero de Factura: Number(7)  
                                                           '10' ||
             -- Registros de tipo '10'  
             RPAD (fa0300.ad02coddpto_r, 3, '0') ||
             -- Servicio Realizador:Number(3)  
             RPAD (gcfn05 (ad02desdpto), 12, ' ') ||
             -- Nombre del servicio anterior: Varchar  
             LPAD (
                ROUND (SUM ( (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact)),
                10, '0'
             ) fa1r10,                                     -- Reparto: Number(10)  
          fa0400.fa04numfact,
          ROUND (SUM ( (100 - fa16dcto) / 100 * fa03cantfact * fa03preciofact)) importe
        FROM fa0400,
             fa0300,
             ad0200,
             fa4800,
             fa4900,
             fa0500,
             fa1600
       WHERE fa0400.fa04codfact = fa0300.fa04numfact
         AND fa1600.fa04codfact = fa0400.fa04codfact
         AND fa0300.fa16numlinea = fa1600.fa16numlinea
         AND ad0200.ad02coddpto = fa0300.ad02coddpto_r
         AND fa4800.fa48coddiskacunsa = fa4900.fa48coddiskacunsa
         AND fa4900.fa04numfact = fa0400.fa04numfact
         AND fa0400.fa04numfacreal IS NULL
         AND fa0400.fa04cantfact <> 0
         AND fa0300.fa05codcateg = fa0500.fa05codcateg
         AND fa0300.ad02coddpto_r = 215                                /*Farmacia*/
         AND fa0500.fa08codgrupo = 1
       GROUP BY fa4800.fa48coddiskacunsa,
                fa0400.fa04numfact,
                ad02coddpto_r,
                ad02desdpto
