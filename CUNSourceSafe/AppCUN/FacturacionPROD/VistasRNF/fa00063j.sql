   CREATE OR REPLACE VIEW fa00063j (pr04numactplan, fa90codtipo) AS
      (
   /* Devuelve el FA90CODTIPO de la Biopsia en concreto */
       SELECT pr04numactplan, NVL (SUM (cuenta), 0) AS fa90codtipo
         FROM fa00062j
        GROUP BY pr04numactplan)
