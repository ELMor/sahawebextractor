
   CREATE OR REPLACE VIEW fa2501 (
      fa25numsegsoc,
      fa25apel1,
      fa25apel2,
      fa25nomb,
      fa25idcliente,
      fa25fechnac,
      fa25sexo,
      fa25codpostal,
      fa25expinf,
      ad01codasistenci,
      ad07codproceso
   ) AS
      (SELECT MAX (fa2500.fa25numsegsoc) AS fa25numsegsoc,
              MIN (fa2500.fa25apel1) AS fa25apel1,
              MIN (fa2500.fa25apel2) AS fa25apel2,
              MIN (fa2500.fa25nomb) AS fa25nomb,
              MIN (fa2500.fa25idcliente) AS fa25idcliente,
              MIN (fa2500.fa25fechnac) AS fa25fechnac,
              MIN (fa2500.fa25sexo) AS fa25sexo,
              MIN (fa2500.fa25codpostal) AS fa25codpostal,
              MAX (fa2500.fa25expinf) AS fa25expinf,
              ad01codasistenci,
              ad07codproceso
         FROM fa2500
        GROUP BY ad01codasistenci, ad07codproceso)
