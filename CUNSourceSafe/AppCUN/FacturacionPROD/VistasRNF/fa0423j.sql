



   CREATE OR REPLACE VIEW fa0423j (
      fa04codfact,
      fa04numfact,
      fa04cantfact,
      fa04fecfactura,
      ad01codasistenci,
      ad07codproceso,
      ci32codtipecon,
      ci13codentidad,
      fa04fecdesde,
      fa04fechasta,
      fa09codnodoconc_fact,
      ad01fecinicio,
      horainicio,
      ad01fecfin,
      horafin,
      ad39codigo,
      ad40orden,
      ad39descodigo,
      ci22numhistoria,
      paciente,
      ci22numsegsoc,
      ad02desdpto,
      fechainterv
   ) AS
      (
   /* FACTURAS INSALUD */
       SELECT fa0400.fa04codfact,
              fa0400.fa04numfact,
              fa0400.fa04cantfact,
              fa0400.fa04fecfactura,
              fa0400.ad01codasistenci,
              fa0400.ad07codproceso,
              fa0400.ci32codtipecon,
              fa0400.ci13codentidad,
              fa0400.fa04fecdesde,
              fa0400.fa04fechasta,
              fa0400.fa09codnodoconc_fact,
              ad0100.ad01fecinicio,
              TO_CHAR (ad0100.ad01fecinicio, 'HH24') AS horainicio,
              ad0100.ad01fecfin,
              TO_CHAR (ad0100.ad01fecfin, 'HH24') AS horafin,
              ad4000.ad39codigo,
              ad4000.ad40orden,
              ad3900.ad39descodigo,
              ci2200.ci22numhistoria,
              ci2200.ci22priapel || ' ' || ci2200.ci22segapel || ', ' ||
                 ci2200.ci22nombre AS paciente,
              ci2200.ci22numsegsoc,
              ad0200.ad02desdpto,
              MIN (fa0300.fa03fecha) AS fechainterv
         FROM fa0400,
              ad0100,
              fa0300,
              fa0500,
              ad3900,
              ad4000,
              ci2200,
              ad0500,
              ad0200
        WHERE fa0400.ci32codtipecon = 'S'
          AND fa0400.ci13codentidad <> 'NA'
          AND fa0400.fa04numfacreal IS NULL
          AND ad0100.ad01codasistenci = fa0400.ad01codasistenci
          AND ci2200.ci21codpersona = ad0100.ci21codpersona
          AND ad0500.ad07codproceso = fa0400.ad07codproceso
          AND ad0500.ad01codasistenci = fa0400.ad01codasistenci
          AND ad0500.ad05fecfinrespon IS NULL
          AND ad0200.ad02coddpto = ad0500.ad02coddpto
          AND ad4000.ad07codproceso (+) = fa0400.ad07codproceso
          AND ad4000.ad01codasistenci (+) = fa0400.ad01codasistenci
          AND ad3900.ad39codigo (+) = ad4000.ad39codigo
          AND ad4000.ad40tipodiagproc (+) = 'P'
          AND ad3900.ad39tipocodigo (+) = 'D'
          AND fa0300.fa04numfact = fa0400.fa04codfact
          AND fa0500.fa05codcateg = fa0300.fa05codcateg
          AND fa0500.fa08codgrupo = 6
        GROUP BY fa0400.fa04codfact,
                 fa0400.fa04numfact,
                 fa0400.fa04cantfact,
                 fa0400.fa04fecfactura,
                 fa0400.ad01codasistenci,
                 fa0400.ad07codproceso,
                 fa0400.ci32codtipecon,
                 fa0400.ci13codentidad,
                 fa0400.fa04fecdesde,
                 fa0400.fa04fechasta,
                 fa0400.fa09codnodoconc_fact,
                 ad0100.ad01fecinicio,
                 TO_CHAR (ad0100.ad01fecinicio, 'HH24'),
                 ad0100.ad01fecfin,
                 TO_CHAR (ad0100.ad01fecfin, 'HH24'),
                 ad4000.ad07codproceso,
                 ad4000.ad01codasistenci,
                 ad4000.ad39codigo,
                 ad4000.ad40orden,
                 ad3900.ad39descodigo,
                 ci2200.ci22numhistoria,
                 ci2200.ci22priapel || ' ' || ci2200.ci22segapel || ', ' ||
                    ci2200.ci22nombre,
                 ci2200.ci22numsegsoc,
                 ad0200.ad02desdpto
       UNION ALL
       SELECT fa0400.fa04codfact,
              fa0400.fa04numfact,
              fa0400.fa04cantfact,
              fa0400.fa04fecfactura,
              fa0400.ad01codasistenci,
              fa0400.ad07codproceso,
              fa0400.ci32codtipecon,
              fa0400.ci13codentidad,
              fa0400.fa04fecdesde,
              fa0400.fa04fechasta,
              fa0400.fa09codnodoconc_fact,
              ad0100.ad01fecinicio,
              TO_CHAR (ad0100.ad01fecinicio, 'HH24') AS horainicio,
              ad0100.ad01fecfin,
              TO_CHAR (ad0100.ad01fecfin, 'HH24') AS horafin,
              ad4000.ad39codigo,
              ad4000.ad40orden,
              ad3900.ad39descodigo,
              ci2200.ci22numhistoria,
              ci2200.ci22priapel || ' ' || ci2200.ci22segapel || ', ' ||
                 ci2200.ci22nombre AS paciente,
              ci2200.ci22numsegsoc,
              ad0200.ad02desdpto,
              TO_DATE ('1/1/1901', 'DD/MM/YYYY') AS fechainterv
         FROM fa0400,
              ad0100,
              ad3900,
              ad4000,
              ci2200,
              ad0500,
              ad0200
        WHERE fa0400.ci32codtipecon = 'S'
          AND fa0400.ci13codentidad <> 'NA'
          AND fa0400.fa04numfacreal IS NULL
          AND ad0100.ad01codasistenci = fa0400.ad01codasistenci
          AND ci2200.ci21codpersona = ad0100.ci21codpersona
          AND ad0500.ad07codproceso = fa0400.ad07codproceso
          AND ad0500.ad01codasistenci = fa0400.ad01codasistenci
          AND ad0500.ad05fecfinrespon IS NULL
          AND ad0200.ad02coddpto = ad0500.ad02coddpto
          AND ad4000.ad07codproceso (+) = fa0400.ad07codproceso
          AND ad4000.ad01codasistenci (+) = fa0400.ad01codasistenci
          AND ad3900.ad39codigo (+) = ad4000.ad39codigo
          AND ad4000.ad40tipodiagproc (+) = 'P'
          AND ad3900.ad39tipocodigo (+) = 'D'
          AND NOT EXISTS (SELECT fa04codfact
                            FROM fa0300,
                                 fa0500
                           WHERE fa0300.fa04numfact = fa0400.fa04codfact
                             AND fa0500.fa05codcateg = fa0300.fa05codcateg
                             AND fa0500.fa08codgrupo = 6))
