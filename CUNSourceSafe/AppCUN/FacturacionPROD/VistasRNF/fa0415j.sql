



   CREATE OR REPLACE VIEW fa0415j (
      ad07codproceso,
      ad01codasistenci,
      ad01fecinicio,
      ad01fecfin,
      ad12codtipoasist,
      ci32codtipecon,
      ci13codentidad,
      fa04codfact,
      fa04numfact,
      fa04cantfact,
      fa04fecfactura,
      ci21codpersona,
      ci22numhistoria,
      ad02coddpto,
      sg02cod,
      paciente
   ) AS
      (
   /* VISTA PARA OBTENER PROCESOS ASISTENCIAS FACTURADOS Y SU IMPORTE */
       SELECT/*+ RULE */
           ad0800.ad07codproceso,
           ad0800.ad01codasistenci,
           ad0100.ad01fecinicio,
           ad0100.ad01fecfin,
           TO_NUMBER (ad2500.ad12codtipoasist) AS ad12codtipoasist,
           UPPER (ad1100.ci32codtipecon) AS ci32codtipecon,
           UPPER (ad1100.ci13codentidad) AS ci13codentidad,
           fa0400.fa04codfact,
           fa0400.fa04numfact,
           fa0400.fa04cantfact,
           fa0400.fa04fecfactura,
           ad0100.ci21codpersona,
           ad0100.ci22numhistoria,
           ad0500.ad02coddpto,
           ad0500.sg02cod,
           (
              ci2200.ci22priapel || ' ' || ci2200.ci22segapel || ', ' ||
                 ci2200.ci22nombre
           ) AS paciente
         FROM ad0800,
              ad2500,
              ad1100,
              fa0400,
              ad0500,
              ci2200,
              ad0100
        WHERE ad0800.ad01codasistenci = ad0100.ad01codasistenci
          AND ad2500.ad01codasistenci = ad0100.ad01codasistenci
          /* AND AD2500.AD25FECFIN IS NULL */
          AND fa0400.fa04fecfactura BETWEEN ad2500.ad25fecinicio
                  AND NVL (ad2500.ad25fecfin, SYSDATE)
          AND ad1100.ad01codasistenci = ad0800.ad01codasistenci
          AND ad1100.ad07codproceso = ad0800.ad07codproceso
          AND ad1100.ad11fecfin IS NULL
          AND fa0400.ad01codasistenci (+) = ad0800.ad01codasistenci
          AND fa0400.ad07codproceso (+) = ad0800.ad07codproceso
          AND fa0400.fa04numfacreal IS NULL
          AND ad0500.ad01codasistenci = ad0800.ad01codasistenci
          AND ad0500.ad07codproceso = ad0800.ad07codproceso
          AND ad0500.ad05fecfinrespon IS NULL
          AND ci2200.ci21codpersona = ad0100.ci21codpersona
          AND fa0400.ci32codtipecon = ad1100.ci32codtipecon
          AND fa0400.ci13codentidad = ad1100.ci13codentidad)
