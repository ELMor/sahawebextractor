   CREATE OR REPLACE VIEW fa0001j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      SELECT        /* VISTA RNF DE CONSULTAS,DISTINGUE DE JEFE/CONSULTOR Y RESTO */
          DISTINCT t4.ad07codproceso,
                   t4.ad01codasistenci,
                   fa05codcateg,
                   1,
                   DECODE (pr37codestado, 4, pr04fecfinact, pr04fecadd),
                   t7.ci22numhistoria,
                   t9.ad02coddpto,
                   t9.sg02cod,
                   t4.ad02coddpto,
                   min(t11.sg02cod),
                   '',
                   DECODE (pr37codestado, 1, 0, 2, 0, 1),
                   to_number(null),
                   decode(pr04indintcientif,-1,-1,0)
        FROM pr0100 t1,
             pr0400 t4,
             ad0700 t7,
             pr0900 t9,
             pr0300 t3,
             pr1000 c1,
             ag1100 t11,
             ad0300 a3,
             fa0500 t5,
             ad0800 t8,
             pr0200 t2
       WHERE t4.ad07codproceso = t8.ad07codproceso
         AND t4.ad07codproceso = t7.ad07codproceso
         AND t4.ad01codasistenci = t8.ad01codasistenci
   /*ESTADOS PLANIF, CITAD, REALIZANDOSE, REALIZADA*/
         AND t4.pr37codestado IN (1, 2, 3, 4, 5)
         AND t4.pr01codactuacion = t1.pr01codactuacion
         AND t1.pr12codactividad = 201/* CONSULTAS */
         AND t1.pr01codactuacion = t2.pr01codactuacion
         AND t5.fa08codgrupo = 7
   /*SI EN PR0200 NO TIENE CODIGO IBM, CONSTRUIRLO, SI LO TIENE, USARLO*/
         AND (
                   (
                          t2.gc01fpclav_sib IS NULL
                      AND t5.fa05codorigc1 = DECODE(t1.pr01codactuacion, 
						   2761 /* Consulta Urgencias Pediatria */, -121, 
											  t4.ad02coddpto)
                      AND t5.fa05codorigc2 = 0
                      AND t5.fa05codorigc3 =
                             DECODE (
                                t4.pr01codactuacion, 4293/*URGENCIAS*/, 1,
				2761 /* Consulta Urgencias Pediatria */, 1,
                                3630/*ARV1*/, 8, 4039/*ARV2*/, 8,
                                3070/*PSICOTERAPIA*/, 2,
                                DECODE (t8.ad10codtippacien, 2, 7, 3, 2, 1)
                             )/* N-R, R, N */
                      AND t5.fa05codorigc4 =
                             DECODE (
                                t4.pr01codactuacion, 4293/*URGENCIAS*/, 2,
                                6306/*ANESTESIA*/, 2, 6304/*ANESTESIA*/, 2,
                                3630/*ARV1*/, 1, 4039/*ARV2*/, 2,
				3313/*ENT. COLAB.*/, 2,
				3314/*ENT. COLAB.*/, 2,
				3315/*ENT. COLAB.*/, 2,
                                3070/*PSICOTERAPIA*/,
                                DECODE (
                                   t11.sg02cod, 'SCE', 4,
                                   DECODE (a3.ad31codpuesto, 6, 5, 5, 5, 1, 6, 5)
                                ), DECODE (
                                      t11.sg02cod, 'EML', 1, 'ABM', 3, 'ERH', 1,
                                      'PRV', 1, 'MLM', 1, 'RGT', 1, 'SCE', 1, 'JMB',
                                      1, 'JCM', 1, 'SDR', 2,
                                      DECODE (
                                         a3.ad31codpuesto, 6, 2, 5, 2, 1, 3, 2
                                      )
                                   )
                             )
                   )
                OR (
                          t2.gc01fpclav_sib IS NOT NULL
                      AND t5.fa05codorigen = t2.gc01fpclav_sib
                   )
             )
         AND t4.pr03numactpedi = t3.pr03numactpedi
         AND t3.pr09numpeticion = t9.pr09numpeticion
         AND c1.pr04numactplan = t4.pr04numactplan
   /* AMPLIAR POR LOS RESIDENTES (T11.SG02COD IS NULL)*/
         AND t11.ag11codrecurso = c1.ag11codrecurso
         AND a3.sg02cod (+) = t11.sg02cod
         AND a3.ad02coddpto (+) = t11.ad02coddpto
         AND a3.ad03fecfin (+) IS NULL
         AND c1.pr07numfase = 1
GROUP BY
	t4.pr04numactplan,
	t4.ad07codproceso,
        t4.ad01codasistenci,
        fa05codcateg,
        DECODE (pr37codestado, 4, pr04fecfinact, pr04fecadd),
        t7.ci22numhistoria,
        t9.ad02coddpto,
        t9.sg02cod,
        t4.ad02coddpto,
        DECODE (pr37codestado, 1, 0, 2, 0, 1),
        decode(pr04indintcientif,-1,-1,0)