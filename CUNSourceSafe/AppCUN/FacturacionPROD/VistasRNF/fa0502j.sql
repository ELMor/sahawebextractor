



   CREATE OR REPLACE VIEW fa0502j (fa05codorigen, fa06codramacat, fa08codgrupo) AS
      SELECT "FA"."FA0500"."FA05CODORIGEN",
             "FA"."FA0600"."FA06CODRAMACAT",
             "FA"."FA0500"."FA08CODGRUPO"
        FROM "FA"."FA9900",
             "FA"."FA0500",
             "FA"."FA0600"
       WHERE fa0500.fa05codcateg = fa0600.fa05codcateg
         AND fa9900.fa99codprop = 1
         AND TO_DATE (fa99valor, 'DD/MM/YYYY HH24:MI:SS') BETWEEN fa06fecinicio
                 AND fa06fecfin - 1
