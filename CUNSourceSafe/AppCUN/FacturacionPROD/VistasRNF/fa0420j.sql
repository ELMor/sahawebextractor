   CREATE OR REPLACE VIEW fa0420j (
      fa04codfact,
      fa04numfact,
      fa04cantfact,
      importemov
   ) AS
      SELECT                  /* CANTIDAD FACTURADA FRENTE IMPORTE DE MOVIMIENTOS*/
          fa0400.fa04codfact,
          fa0400.fa04numfact,
          fa0400.fa04cantfact,
          SUM (DECODE (
                  fa0300.fa05codcateg, 35352, fa16importe, 35353, fa16importe,
                  35354, fa16importe, fa0300.fa03cantfact * fa0300.fa03preciofact *
                                         (100 - fa16dcto) /
                                         100
               )) importemov
        FROM fa0300,
             fa1600,
             fa0400
       WHERE fa0400.fa04numfacreal IS NULL
         AND fa0300.fa04numfact = fa0400.fa04codfact
         AND fa1600.fa04codfact = fa0400.fa04codfact
         AND fa0300.fa16numlinea = fa1600.fa16numlinea
       GROUP BY fa0400.fa04codfact, fa0400.fa04numfact, fa0400.fa04cantfact
