CREATE OR REPLACE VIEW fa3702j (
   tasi,
   fecha,
   ad01codasistenci,
   ad07codproceso,
   ci32codtipecon,
   ci13codentidad
) AS
   SELECT 
          /*Altas Hosp/Amb en un mes y tipo economico. 
            Se basa en Tabla de camas. Valido desde 1999*/
          'H',
          LAST_DAY(TRUNC(MAX(ad16fecfin))),
          ad1600.ad01codasistenci,
          ad1600.ad07codproceso,
          ad1100.ci32codtipecon,
          ad1100.ci13codentidad
     FROM ad1100,
          ad1600
    WHERE ad1600.ad14codestcama = 2
      AND ad1100.ad01codasistenci = ad1600.ad01codasistenci
      AND ad1100.ad07codproceso   = ad1600.ad07codproceso
 GROUP BY ad1600.ad01codasistenci,
          ad1600.ad07codproceso,
	       ad1100.ci32codtipecon,
	       ad1100.ci13codentidad
   UNION ALL
   SELECT 
          'A',
          LAST_DAY(TRUNC(ad0800.ad08fecfin)),
          ad0800.ad01codasistenci,
          ad0800.ad07codproceso,
          ad1100.ci32codtipecon,
          ad1100.ci13codentidad
     FROM ad1100,
          ad0800
    WHERE ad1100.ad01codasistenci = ad0800.ad01codasistenci
      AND ad1100.ad07codproceso = ad0800.ad07codproceso
      AND NOT EXISTS (SELECT ad14codestcama
                        FROM ad1600
                       WHERE ad1600.ad14codestcama = 2
                         AND ad1600.ad01codasistenci = ad0800.ad01codasistenci
                         AND ad1600.ad07codproceso   = ad0800.ad07codproceso)
