   CREATE OR REPLACE VIEW fa0006j (
      ad07codproceso,
      ad01codasistenci,
      fa05codcateg,
      cantidad,
      fecha,
      ci22numhistoria,
      dptosolicita,
      drsolicita,
      dptorealiza,
      drrealiza,
      obs,
      estado,
      precio,
      ic
   ) AS
      (SELECT                                               /*ANATOMIA PATOLOGICA*/
           pr0400.ad07codproceso,
           pr0400.ad01codasistenci,
           fa0500.fa05codcateg,
           COUNT (*) AS cantidad,
           NVL (pr0400.pr04feciniact, pr04fecadd) AS fecha,
           ci2200.ci22numhistoria,
           pr0900.ad02coddpto AS dptosolicita,
           pr0900.sg02cod AS drsolicita,
           204 AS dptorealiza,
           NULL AS drrealiza,
           '' AS obs,
           1 AS estado,
           to_number(null),
           decode(pr04indintcientif,-1,-1,0)
         FROM pr0400,
              fa9000,
              fa0500,
              ci2200,
              pr0300,
              pr0900
        WHERE fa9000.fa90cod =
               (SELECT MIN (fa9000.fa90cod)
                  FROM fa00063j,
                       fa9000
                 WHERE fa9000.fa90codtipo = fa00063j.fa90codtipo
                   AND fa00063j.pr04numactplan = pr0400.pr04numactplan)
          AND fa0500.fa05codorigen = fa9000.fa90codigo
          AND ci2200.ci21codpersona = pr0400.ci21codpersona
          AND pr0300.pr03numactpedi = pr0400.pr03numactpedi
          AND pr0900.pr09numpeticion = pr0300.pr09numpeticion
          AND pr0400.pr01codactuacion = 3809
        GROUP BY pr0400.ad07codproceso,
                 pr0400.ad01codasistenci,
                 fa0500.fa05codcateg,
                 NVL (pr0400.pr04feciniact, pr04fecadd),
                 ci2200.ci22numhistoria,
                 pr0900.ad02coddpto,
                 pr0900.sg02cod,
                 decode(pr04indintcientif,-1,-1,0))
