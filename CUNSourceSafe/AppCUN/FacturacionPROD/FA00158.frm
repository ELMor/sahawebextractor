VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frm_NuevosRNFs 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Introducci�n manual de movimientos"
   ClientHeight    =   6780
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   6780
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   4470
      Left            =   90
      TabIndex        =   10
      Top             =   1530
      Width           =   11750
      _ExtentX        =   20717
      _ExtentY        =   7885
      _Version        =   327681
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Categorias disponibles"
      TabPicture(0)   =   "FA00158.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "tvCat"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Nuevos movimientos"
      TabPicture(1)   =   "FA00158.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "grdssRNFs"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin ComctlLib.TreeView tvCat 
         Height          =   3930
         Left            =   -74910
         TabIndex        =   11
         Top             =   465
         Width           =   11550
         _ExtentX        =   20373
         _ExtentY        =   6932
         _Version        =   327682
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         ImageList       =   "ImageList"
         Appearance      =   1
      End
      Begin SSDataWidgets_B.SSDBGrid grdssRNFs 
         Height          =   3930
         Left            =   90
         TabIndex        =   13
         Top             =   420
         Width           =   11550
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   16
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         CaptionAlignment=   0
         Columns.Count   =   16
         Columns(0).Width=   1799
         Columns(0).Caption=   "Fecha"
         Columns(0).Name =   "FA03FECHA"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   7
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16776960
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "Cod.Categ."
         Columns(1).Name =   "FA05CODCATEG"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   3
         Columns(1).FieldLen=   256
         Columns(2).Width=   5768
         Columns(2).Caption=   "Descripci�n"
         Columns(2).Name =   "DESCRIPCION"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16776960
         Columns(3).Width=   1349
         Columns(3).Caption=   "Cantidad"
         Columns(3).Name =   "FA03CANTIDAD"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   5
         Columns(3).FieldLen=   256
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   16776960
         Columns(4).Width=   2434
         Columns(4).Caption=   "Precio"
         Columns(4).Name =   "FA03PRECIO"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   5
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Caption=   "Observaciones"
         Columns(5).Name =   "FA03OBSERV"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "Dpto. Solic."
         Columns(6).Name =   "AD02CODDPTO_S"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Style=   3
         Columns(6).Nullable=   2
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "Dtor. Solic."
         Columns(7).Name =   "SG02COD_S"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Style=   3
         Columns(7).Nullable=   2
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "Dpto.Realiz"
         Columns(8).Name =   "AD02CODDPTO_R"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Nullable=   2
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "Dtor. Realiz."
         Columns(9).Name =   "SG02COD_R"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).Style=   3
         Columns(9).Nullable=   2
         Columns(10).Width=   2461
         Columns(10).Caption=   "Dpto Realiz."
         Columns(10).Name=   "Dpto_R"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(10).HasBackColor=   -1  'True
         Columns(10).BackColor=   16776960
         Columns(10).Nullable=   2
         Columns(11).Width=   2381
         Columns(11).Caption=   "Doctor Realiz."
         Columns(11).Name=   "Doctor_R"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(11).Nullable=   2
         Columns(12).Width=   2461
         Columns(12).Caption=   "Dpto. Solic."
         Columns(12).Name=   "Dpto_s"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(12).Nullable=   2
         Columns(13).Width=   3200
         Columns(13).Caption=   "Doctor Solic."
         Columns(13).Name=   "Doctor_S"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(13).Nullable=   2
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "CantOriginal"
         Columns(14).Name=   "CantOriginal"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "PrecioOriginal"
         Columns(15).Name=   "PrecioOriginal"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         _ExtentX        =   20373
         _ExtentY        =   6932
         _StockProps     =   79
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown ssDpto_S 
      Height          =   510
      Left            =   2475
      TabIndex        =   14
      Top             =   6165
      Width           =   780
      DataFieldList   =   "Column 1"
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      DividerStyle    =   4
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo"
      Columns(0).Name =   "Codigo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Nullable=   2
      Columns(1).Width=   3200
      Columns(1).Caption=   "Designacion"
      Columns(1).Name =   "Designacion"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   1376
      _ExtentY        =   900
      _StockProps     =   77
      ForeColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.CommandButton cmdGrabar 
      Caption         =   "&Grabar"
      Default         =   -1  'True
      Height          =   510
      Left            =   8970
      TabIndex        =   12
      Top             =   6165
      Width           =   1230
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   510
      Left            =   10335
      TabIndex        =   9
      Top             =   6165
      Width           =   1230
   End
   Begin VB.Frame Frame1 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1185
      Left            =   90
      TabIndex        =   0
      Top             =   135
      Width           =   11760
      Begin VB.Label lblHistoria 
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Height          =   330
         Left            =   1350
         TabIndex        =   7
         Top             =   225
         Width           =   1590
      End
      Begin VB.Label Label5 
         Caption         =   "Historia"
         Height          =   240
         Left            =   225
         TabIndex        =   8
         Top             =   375
         Width           =   1050
      End
      Begin VB.Label lblAsistencia 
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Height          =   330
         Left            =   4770
         TabIndex        =   6
         Top             =   630
         Width           =   1590
      End
      Begin VB.Label lblProceso 
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Height          =   330
         Left            =   1350
         TabIndex        =   5
         Top             =   630
         Width           =   1590
      End
      Begin VB.Label lblPaciente 
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Height          =   330
         Left            =   4770
         TabIndex        =   4
         Top             =   225
         Width           =   6900
      End
      Begin VB.Label Label3 
         Caption         =   "Nombre"
         Height          =   285
         Left            =   3645
         TabIndex        =   3
         Top             =   360
         Width           =   645
      End
      Begin VB.Label Label2 
         Caption         =   "Asistencia"
         Height          =   195
         Left            =   3645
         TabIndex        =   2
         Top             =   750
         Width           =   1410
      End
      Begin VB.Label Label1 
         Caption         =   "Proceso"
         Height          =   240
         Left            =   225
         TabIndex        =   1
         Top             =   750
         Width           =   1995
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown ssDpto_R 
      Height          =   510
      Left            =   3465
      TabIndex        =   15
      Top             =   6165
      Width           =   780
      DataFieldList   =   "Column 1"
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      DividerType     =   0
      DividerStyle    =   4
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo"
      Columns(0).Name =   "Codigo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Nullable=   2
      Columns(1).Width=   3200
      Columns(1).Caption=   "Designacion"
      Columns(1).Name =   "Designacion"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   1376
      _ExtentY        =   900
      _StockProps     =   77
      ForeColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown ssDtor_S 
      Height          =   510
      Left            =   4455
      TabIndex        =   16
      Top             =   6165
      Width           =   780
      DataFieldList   =   "Column 1"
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      DividerStyle    =   4
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo"
      Columns(0).Name =   "Codigo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Nullable=   2
      Columns(1).Width=   3200
      Columns(1).Caption=   "Designacion"
      Columns(1).Name =   "Designacion"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Nullable=   2
      _ExtentX        =   1376
      _ExtentY        =   900
      _StockProps     =   77
      ForeColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown ssDtor_R 
      Height          =   510
      Left            =   5445
      TabIndex        =   17
      Top             =   6165
      Width           =   780
      DataFieldList   =   "Column 1"
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      DividerStyle    =   4
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Codigo"
      Columns(0).Name =   "Codigo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Nullable=   2
      Columns(1).Width=   3200
      Columns(1).Caption=   "Designacion"
      Columns(1).Name =   "Designacion"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Nullable=   2
      _ExtentX        =   1376
      _ExtentY        =   900
      _StockProps     =   77
      ForeColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin ComctlLib.ImageList ImageList 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   16777215
      UseMaskColor    =   0   'False
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   9
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00158.frx":0038
            Key             =   "NodoCerrado"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00158.frx":0352
            Key             =   "NodoAbierto"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00158.frx":066C
            Key             =   "Hoja"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00158.frx":0986
            Key             =   "Editando"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00158.frx":0CA0
            Key             =   "Borrado"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00158.frx":0FBA
            Key             =   "Concierto"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00158.frx":12D4
            Key             =   "Facturable"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00158.frx":15EE
            Key             =   "CatCerrado"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00158.frx":1908
            Key             =   "CatAbierto"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frm_NuevosRNFs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim qryInsert As New rdoQuery
Dim qryCargar As New rdoQuery
Dim qryBorrar As New rdoQuery
Dim qryDoctor As New rdoQuery
Dim blnModificado As Boolean

Private rq(1 To 30) As New rdoQuery

Private Enum TipoDeNodo
  tnRaiz = 0
  tnNodo = 1
  tnCatRaiz = 2
  tnCatFinal = 3
  tnErrorNodo = 4
End Enum
Private miObjApp As Object

Public Sub Inicializar(objApp As Variant)
   Set miObjApp = objApp
   
   ' Inicializar las queries
   IniciarQRY
   
   InitTVCat
   
   CargarRNF
   
   CargarDptos

   blnModificado = False
End Sub

Private Sub IniciarQRY()
   Dim MiSql As String
   '****************************************************************************************************
   ' preparar la SQL que carga los datos de RNFs ya existentes
   '****************************************************************************************************
   MiSql = "select fa0300.*, fa05desig, " & _
           "       dptoR.AD02DESDPTO Dpto_R, dtorR.SG02NOM || ' ' || dtorR.SG02APE1 || ' ' || dtorR.SG02APE2 Doctor_R, " & _
           "       dptoS.AD02DESDPTO Dpto_S, dtorS.SG02NOM || ' ' || dtorS.SG02APE1 || ' ' || dtorS.SG02APE2  Doctor_S " & _
           "from fa0300, fa0500, ad0200 dptoR,  sg0200 dtorR, " & _
           "     ad0200 dptoS, sg0200 dtorS " & _
           "Where ad07codproceso = ? And ad01codasistenci = ?  " & _
           "      And FA03INDPERMANENTE = 1 And fa04numfact Is Null " & _
           "      and fa0300.fa05codcateg = fa0500.fa05codcateg " & _
           "      and fa0300.ad02coddpto_r = dptoR.ad02coddpto " & _
           "      and fa0300.ad02coddpto_s = dptos.ad02coddpto(+) " & _
           "      and fa0300.sg02cod_r = dtorR.sg02cod (+) " & _
           "      and fa0300.sg02cod_s = dtors.sg02cod(+)"
   
   qryCargar.SQL = MiSql
  'Activamos la query
   Set qryCargar.ActiveConnection = objApp.rdoConnect
   qryCargar.Prepared = True
   
   '****************************************************************************************************
   ' preparar la SQL que borra los datos de RNFs ya existentes, para insertarlos con las modificaciones
   '****************************************************************************************************
   MiSql = "delete fa0300 where ad07codproceso = ? and ad01codasistenci = ?  " & _
           "               and FA03INDPERMANENTE = 1 and fa04numfact is null"
   
   qryBorrar.SQL = MiSql
  'Activamos la query
   Set qryBorrar.ActiveConnection = objApp.rdoConnect
   qryBorrar.Prepared = True
   
   '****************************************************************************************************
   ' preparar la SQL que inserta los RNFs nuevos
   '****************************************************************************************************
   MiSql = "INSERT INTO FA0300 " & _
            "(FA03NUMRNF, AD07CODPROCESO, AD01CODASISTENCI, FA05CODCATEG,  " & _
            " FA03PRECIO, FA03PRECIOFACT, FA03CANTIDAD, FA03CANTFACT, FA13CODESTRNF, " & _
            " FA03FECHA,CI22NUMHISTORIA,FA03OBSERV,FA03INDREALIZADO, " & _
            " AD02CODDPTO_R, SG02COD_R, AD02CODDPTO_S, SG02COD_S, " & _
            " FA03OBSIMP, FA03INDPERMANENTE) " & _
           "VALUES (FA03NUMRNF_SEQUENCE.NEXTVAL,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),?,?,?,?,?,?,?,?,'1')"
   
   qryInsert.SQL = MiSql
  'Activamos la query
   Set qryInsert.ActiveConnection = objApp.rdoConnect
   qryInsert.Prepared = True
   
   
   '****************************************************************************************************
   ' preparar la SQL que recoge los doctores de un departamento
   '****************************************************************************************************
   MiSql = "select sg0200.SG02COD, SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 Nombre from ad0300,sg0200  " & _
           "Where ad0300.sg02cod = sg0200.sg02cod" & _
           " and ad02coddpto = ? " & _
           " and ((SYSDATE between SG02FECACT and SG02FECDES) or sg02fecdes is null)"
   
   qryDoctor.SQL = MiSql
  'Activamos la query
   Set qryDoctor.ActiveConnection = objApp.rdoConnect
   qryDoctor.Prepared = True


   '****************************************************************************************************
   '****************************************************************************************************
   
'   Select para recoger las categorias hijas de una categoria
'   Parametros:
'            0 - Cod. Categor�a Padre
'            1 - Fecha Proceso
'            2 - Fecha Proceso
    rq(4).SQL = "SELECT FA0500.FA05CODCATEG, FA05DESIG FROM FA0500, FA0600 " & _
                "WHERE FA0500.FA05CODCATEG = FA0600.FA05CODCATEG AND FA0600.FA05CODCATEG_P = ? AND " & _
                "FA06FECINICIO <= SYSDATE AND " & _
                "SYSDATE < FA06FECFIN " & _
                "ORDER BY FA05DESIG"

   Set rq(4).ActiveConnection = miObjApp.rdoConnect
   rq(4).Prepared = True
'   Select de inicializaci�n de Categor�as (aquellas que son raices)
'   Parametros:
'            0 - Fecha Proceso
'            1 - Fecha Proceso
    rq(5).SQL = "SELECT FA0500.FA05CODCATEG, FA05DESIG FROM FA0500, FA0600 " & _
                "WHERE FA0500.FA05CODCATEG = FA0600.FA05CODCATEG AND FA0600.FA05CODCATEG_P IS NULL AND " & _
                "FA06FECINICIO <= SYSDATE AND " & _
                "SYSDATE < FA06FECFIN "
                
   Set rq(5).ActiveConnection = miObjApp.rdoConnect
   rq(5).Prepared = True


End Sub

Private Sub cmdGrabar_Click()
   If Validar = True Then
      Grabar
      blnModificado = False
      Unload Me
   End If
End Sub

Private Sub cmdSalir_Click()
   Unload Me
End Sub


Public Sub InitTVCat()
    Dim RS As rdoResultset, n As Node
    tvCat.Nodes.Clear
'    mskHoraProceso_LostFocus
    
   
    Set RS = rq(5).OpenResultset
    While Not RS.EOF
        Set n = tvCat.Nodes.Add(, , "/." & RS(0) & ".", RS(1), "CatCerrado", "Editando")
        n.Tag = RS(0)
        tvCat.Nodes.Add n, tvwChild, n.Key & "dummy", "dummy"
        RS.MoveNext
    Wend
    
    RS.Close
    Set RS = Nothing
End Sub



Private Sub Form_Load()
   Me.Width = Screen.Width
End Sub

Private Sub Form_Resize()
   SSTab1.Width = Me.Width - 250
   grdssRNFs.Width = Me.Width - 450
   tvCat.Width = Me.Width - 450
   cmdGrabar.Left = Me.Width - 2715
   cmdSalir.Left = Me.Width - 1390
End Sub

Private Sub Form_Unload(Cancel As Integer)
   Dim intSalir As Integer
   If blnModificado Then
      intSalir = MsgBox("� Desea grabar los cambios antes de salir ?", vbQuestion + vbYesNoCancel, "A�adir Movimientos")
      
      If intSalir = vbYes Then
         If Validar = True Then
            Grabar
            blnModificado = False
            Cancel = 0
         Else
            Cancel = 1
         End If
      ElseIf intSalir = vbNo Then
         Cancel = 0
      Else
         Cancel = 1
      End If
   End If
End Sub

Private Sub grdssRNFs_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
   Dim blncancel As Integer
   ' por defecto que no muestre el mensaje en ingles
   DispPromptMsg = 0
   
   blncancel = MsgBox("� Desea borrar esta linea ?", vbQuestion + vbYesNo, "Borrar Movimientos")
   
   If blncancel = vbYes Then
      Cancel = 0
      blnModificado = True
   Else
      Cancel = 1
   End If
End Sub

Private Sub grdssRNFs_Change()
   blnModificado = True
End Sub

Private Sub grdssRNFs_InitColumnProps()
   ' enlazar el grid con los dropdown de los departamentosy los doctores
   grdssRNFs.Columns("DPTO_S").DropDownHwnd = ssDpto_S.hWnd
   grdssRNFs.Columns("DPTO_R").DropDownHwnd = ssDpto_R.hWnd
   
   grdssRNFs.Columns("Doctor_S").DropDownHwnd = ssDtor_S.hWnd
   grdssRNFs.Columns("Doctor_R").DropDownHwnd = ssDtor_R.hWnd
End Sub

Private Sub grdssRNFs_KeyPress(KeyAscii As Integer)
   If grdssRNFs.Col = grdssRNFs.Columns("Descripcion").Position And grdssRNFs.Columns("FA05CODCATEG").Value = "" Then
      MsgBox "No se puede escribir directamente la descripci�n" & Chr(10) & Chr(10) & "debe elegir una categoria del arbol adjunto", vbInformation + vbOKOnly, "Aviso"
      KeyAscii = 0
   End If
End Sub

Private Sub grdssRNFs_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
   If grdssRNFs.Col = grdssRNFs.Columns("Doctor_R").Position Then
      ' hay que actualizar el dropdown con el nuevo departamento si es diferente del que contiene
      CargarDoctores grdssRNFs.Columns("AD02CODDPTO_R").Value, "R"
   End If
   
   If grdssRNFs.Col = grdssRNFs.Columns("Doctor_S").Position Then
      ' hay que actualizar el dropdown con el nuevo departamento si es diferente del que contiene
      CargarDoctores grdssRNFs.Columns("AD02CODDPTO_S").Value, "S"
   End If
   
End Sub

Private Sub ssDpto_S_Click()
   grdssRNFs.Columns("ad02coddpto_s").Value = ssDpto_S.Columns("Codigo").Value
   Call CargarDoctores(ssDpto_S.Columns("Codigo").Value, "S")
End Sub

Private Sub ssDpto_R_Click()
   grdssRNFs.Columns("ad02coddpto_R").Value = ssDpto_R.Columns("Codigo").Value
   Call CargarDoctores(ssDpto_R.Columns("Codigo").Value, "R")
End Sub

Private Sub ssDtor_S_Click()
   grdssRNFs.Columns("SG02COD_S").Value = ssDtor_S.Columns("Codigo").Value
End Sub

Private Sub ssDtor_R_Click()
   grdssRNFs.Columns("SG02COD_R").Value = ssDtor_R.Columns("Codigo").Value
End Sub

Private Sub tvCat_Collapse(ByVal Node As ComctlLib.Node)
    Call Collapse(tvCat, Node)
End Sub

Private Sub tvCat_DblClick()
   Dim Node As Node
   
   Set Node = tvCat.SelectedItem
   
   If Node.Children = 0 Then
      ' es categoria final
      Call CrearRNF(Node)
   Else
      ' revisar si tiene hijos o solo es el dummy
      If Node.Child.Text = "dummy" Then
         ' comprobar si tiene hijos
         Call Expand(tvCat, Node)
         If Node.Children = 0 Then
            Call CrearRNF(Node)
         Else
            MsgBox "Esta categoria no es final", vbCritical + vbOKOnly, "Error"
         End If

      Else
         MsgBox "Esta categoria no es final", vbCritical + vbOKOnly, "Error"
      End If
   End If

End Sub

Private Sub tvCat_Expand(ByVal Node As ComctlLib.Node)
    Call Expand(tvCat, Node)
End Sub

Private Sub Collapse(TreeView As TreeView, ByVal Node As ComctlLib.Node)
    Screen.MousePointer = vbHourglass
    
    If Node.Image = "NodoAbierto" Then
        Node.Image = "NodoCerrado"
    Else
        If Node.Image = "CatAbierto" Then
            Node.Image = "CatCerrado"
        End If
    End If
    
    Screen.MousePointer = vbDefault
End Sub

Private Sub Expand(TreeView As TreeView, ByVal Node As ComctlLib.Node)
    Dim n As Node, RS As rdoResultset, str$
    Dim PosBarra As Integer
    Dim PosPunto As Integer
    Dim CatRaiz As Long
    
    On Error Resume Next
    
    Screen.MousePointer = vbHourglass
    
    If Node.Image = "NodoCerrado" Then
        Node.Image = "NodoAbierto"
    Else
        If Node.Image = "CatCerrado" Then
            Node.Image = "CatAbierto"
        End If
    End If
    
    Err.Clear
    Set n = TreeView.Nodes.Item(Node.Key & "dummy")
    str = n.Key
    On Error GoTo 0
    If str <> "" Then
        ' es la primera vez que se abre
        
         ' empezamos a buscar en categor�as
         TreeView.Nodes.Remove n.Key
   
         rq(4).rdoParameters(0).Value = Node.Tag
         
         Set RS = rq(4).OpenResultset
         While Not RS.EOF
             Set n = TreeView.Nodes.Add(Node, tvwChild, Node.Key & RS(0) & ".", RS(1), "CatCerrado", "Editando")
             n.Tag = RS(0)
             TreeView.Nodes.Add n, tvwChild, n.Key & "dummy", "dummy"
             RS.MoveNext
         Wend
         RS.Close
        
    End If
    
    Screen.MousePointer = vbDefault
End Sub


Private Function TipoNodo(n As Node) As TipoDeNodo
    On Error GoTo ErrorNodo
    Dim Key As String
    
    Key = n.Key
    
    ' es un nodo
    If n.Key Like "*/" Then
        If n.Key Like ".*.*.*/" Then
            ' es un nodo intermedio
            TipoNodo = tnNodo
        Else
            ' es un raiz de concierto
            TipoNodo = tnRaiz
        End If
        
    ' es una categor�a
    Else
        If n.Key Like "*/*.*.*." Then
            ' es categor�a intermedia o final
            TipoNodo = tnCatFinal
        Else
            ' es categor�a raiz
            TipoNodo = tnCatRaiz
        End If
    End If
    Exit Function

ErrorNodo:
    TipoNodo = tnErrorNodo
End Function

Private Sub CrearRNF(objNode As Node)
   Dim strNuevoRNF As String
   strNuevoRNF = "" & Chr$(9) & objNode.Tag & Chr$(9) & objNode.Text
   
   grdssRNFs.AddItem strNuevoRNF
   blnModificado = True
End Sub

Private Sub CargarDptos()
   Dim RS As rdoResultset
   Dim SQL As String
   Dim Dpto As String
   
   ssDpto_S.RemoveAll
   ssDpto_R.RemoveAll
   ssDpto_S.AddItem "-1" & Chr$(9) & ""
   ssDpto_R.AddItem "-1" & Chr$(9) & ""
   
   
   SQL = "select ad02coddpto, ad02desdpto from ad0200 order by 2"
   Set RS = objApp.rdoConnect.OpenResultset(SQL, 3)
   While Not RS.EOF
      ssDpto_S.AddItem RS("ad02coddpto").Value & Chr$(9) & RS("ad02desdpto").Value
      ssDpto_R.AddItem RS("ad02coddpto").Value & Chr$(9) & RS("ad02desdpto").Value
      RS.MoveNext
   Wend
   RS.Close

End Sub

Private Sub CargarDoctores(CodDpto As String, Tipo As String)
   Dim RS As rdoResultset
   Dim SQL As String
   Dim Dpto As String
   
   
   If Tipo = "S" Then
      If ssDtor_S.Tag = CodDpto Then
         ' los datos ya est�n cargados, no hace falta nada
         Exit Sub
      Else
         ssDtor_S.RemoveAll
         ssDtor_S.Tag = CodDpto
      End If
   Else
      If ssDtor_R.Tag = CodDpto Then
         ' los datos ya est�n cargados, no hace falta nada
         Exit Sub
      Else
         ssDtor_R.RemoveAll
         ssDtor_R.Tag = CodDpto
      End If
   End If
   
   qryDoctor.rdoParameters(0) = "" & CodDpto

   Set RS = qryDoctor.OpenResultset(rdOpenKeyset)
        
   While Not RS.EOF
      If Tipo = "S" Then
         ssDtor_S.AddItem RS("SG02COD").Value & Chr$(9) & RS("Nombre").Value
      Else
         ssDtor_R.AddItem RS("SG02COD").Value & Chr$(9) & RS("Nombre").Value
      End If
      RS.MoveNext
   Wend
   
   
   RS.Close

End Sub

Private Function Validar() As Boolean
   Dim X As Integer
   
   Validar = False
   grdssRNFs.MoveFirst
   
   For X = 1 To grdssRNFs.Rows
      ' validar la fecha
      If grdssRNFs.Columns("FA03FECHA").Value = "" Then
         MsgBox "Fecha invalida", vbCritical + vbOKOnly, "Error de validaci�n"
         grdssRNFs.Col = grdssRNFs.Columns("FA03FECHA").Position
         Exit Function
      End If
      ' validar la categoria
      If grdssRNFs.Columns("FA05CODCATEG").Value = "" Then
         MsgBox "Debe elegir una categoria", vbCritical + vbOKOnly, "Error de validaci�n"
         grdssRNFs.Col = grdssRNFs.Columns("FA05CODCATEG").Position
         Exit Function
      End If
      ' validar la cantidad
      If grdssRNFs.Columns("FA03CANTIDAD").Value = "" Then
         MsgBox "La cantidad es invalida", vbCritical + vbOKOnly, "Error de validaci�n"
         grdssRNFs.Col = grdssRNFs.Columns("FA03CANTIDAD").Position
         Exit Function
      End If
      ' validar la categoria
      If grdssRNFs.Columns("AD02CODDPTO_R").Value = "" Then
         MsgBox "El Departamento realizador es obligatorio", vbCritical + vbOKOnly, "Error de validaci�n"
         grdssRNFs.Col = grdssRNFs.Columns("AD02CODDPTO_R").Position
         Exit Function
      End If
         
         
      grdssRNFs.MoveNext
   Next X
   
   Validar = True
End Function

Function Grabar() As Boolean
   ' validar los datos
   ' si son correctos, grabarlos
   Dim X As Integer
   
   ' BORRAR LOS RNFS ANTERIORES
   qryBorrar.rdoParameters(0) = "" & lblProceso.Caption
   qryBorrar.rdoParameters(1) = "" & lblAsistencia.Caption
   qryBorrar.Execute
   
   
   Grabar = False
   grdssRNFs.MoveFirst
   
   For X = 1 To grdssRNFs.Rows
      qryInsert.rdoParameters(0) = "" & lblProceso.Caption
      qryInsert.rdoParameters(1) = "" & lblAsistencia.Caption
      qryInsert.rdoParameters(2) = "" & grdssRNFs.Columns("FA05CODCATEG").Value
      If grdssRNFs.Columns("FA03PRECIO").Value = "" Then
         qryInsert.rdoParameters(3) = Null
         qryInsert.rdoParameters(4) = Null
      Else
         qryInsert.rdoParameters(3) = IIf(grdssRNFs.Columns("PRECIOORIGINAL").Value = "", Null, "" & grdssRNFs.Columns("PRECIOORIGINAL").Value)
         
         qryInsert.rdoParameters(4) = NumToSql(RedondearMoneda(grdssRNFs.Columns("FA03PRECIO").Value, tiImporteLinea))
      End If
'      qryInsert.rdoParameters(5) = NumToSql(RedondearMoneda(grdssRNFs.Columns("FA03CANTIDAD").Value, tiImporteLinea))
      qryInsert.rdoParameters(5) = IIf(grdssRNFs.Columns("CANTORIGINAL").Value = "", Null, "" & grdssRNFs.Columns("CANTORIGINAL").Value)
      
      qryInsert.rdoParameters(6) = NumToSql(grdssRNFs.Columns("FA03CANTIDAD").Value)
      qryInsert.rdoParameters(7) = "1"
      qryInsert.rdoParameters(8) = Format(grdssRNFs.Columns("FA03FECHA").Value, "dd/MM/YYYY hh:mm:SS")
      qryInsert.rdoParameters(9) = "" & lblHistoria.Caption
      qryInsert.rdoParameters(10) = "" & grdssRNFs.Columns("FA03OBSERV").Value
      qryInsert.rdoParameters(11) = "1"
      qryInsert.rdoParameters(12) = IIf(grdssRNFs.Columns("AD02CODDPTO_R").Value = "-1", Null, "" & grdssRNFs.Columns("AD02CODDPTO_R").Value)
      qryInsert.rdoParameters(13) = "" & grdssRNFs.Columns("SG02COD_R").Value
      qryInsert.rdoParameters(14) = IIf(grdssRNFs.Columns("AD02CODDPTO_S").Value = "-1", Null, "" & grdssRNFs.Columns("AD02CODDPTO_S").Value)
      qryInsert.rdoParameters(15) = "" & grdssRNFs.Columns("SG02COD_S").Value
      
      If grdssRNFs.Columns("DESCRIPCION").Value <> "" Then
         qryInsert.rdoParameters(16) = grdssRNFs.Columns("DESCRIPCION").Value
      Else
         qryInsert.rdoParameters(16) = Null
      End If
      
      qryInsert.Execute
      
      grdssRNFs.MoveNext
   Next X
   
   ' la grabaci�n ha sido correcta
   Grabar = True
   blnModificado = False

End Function

Private Sub CargarRNF()
   Dim RS As rdoResultset
   
   
   qryCargar.rdoParameters(0) = "" & lblProceso.Caption
   qryCargar.rdoParameters(1) = "" & lblAsistencia.Caption

   Set RS = qryCargar.OpenResultset(rdOpenKeyset)
      
   
   While Not RS.EOF
   
      grdssRNFs.AddItem RS("FA03FECHA").Value & Chr$(9) & _
                        RS("FA05CODCATEG").Value & Chr$(9) & _
                        IIf(IsNull(RS("FA03OBSIMP").Value), RS("FA05DESIG").Value, RS("FA03OBSIMP").Value) & Chr$(9) & _
                        RS("FA03CANTFACT").Value & Chr$(9) & _
                        RS("FA03PRECIOFACT").Value & Chr$(9) & _
                        RS("FA03OBSERV").Value & Chr$(9) & _
                        RS("AD02CODDPTO_S").Value & Chr$(9) & _
                        RS("SG02COD_S").Value & Chr$(9) & _
                        RS("AD02CODDPTO_R").Value & Chr$(9) & _
                        RS("SG02COD_R").Value & Chr$(9) & _
                        RS("Dpto_R").Value & Chr$(9) & _
                        RS("Doctor_R").Value & Chr$(9) & _
                        RS("Dpto_S").Value & Chr$(9) & _
                        RS("Doctor_S").Value & Chr$(9) & _
                        RS("FA03CANTIDAD").Value & Chr$(9) & _
                        RS("FA03PRECIO").Value & Chr$(9)

      RS.MoveNext
   Wend
   RS.Close
End Sub



