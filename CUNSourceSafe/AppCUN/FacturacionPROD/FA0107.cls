VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Paciente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public Codigo As Long ' es el numero de historia
Public Asistencias As New Collection
Public Name As String
Public Direccion As String
Public FecNacimiento As String

'Public FactAntigua As Boolean
'Public NumFactAnt As String
'Public FechaAnterior As String

Public CodPersona As String  ' es el CI21CODPERSONA

Public Sub Destruir()
   For Each obj In Asistencias
      obj.Destruir
'      Set obj = Nothing
   Next
   Set Asistencias = Nothing

End Sub

