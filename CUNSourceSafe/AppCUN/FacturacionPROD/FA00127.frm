VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmA_DtosCantidad 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Descuentos por Cantidad"
   ClientHeight    =   3105
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   8205
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   5
   Icon            =   "FA00127.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3105
   ScaleWidth      =   8205
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Descuentos por Cantidad"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2220
      Index           =   0
      Left            =   45
      TabIndex        =   1
      Top             =   495
      Width           =   8115
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1725
         Index           =   0
         Left            =   90
         TabIndex        =   2
         Top             =   405
         Width           =   7935
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         UseExactRowCount=   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   13996
         _ExtentY        =   3043
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   2820
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_DtosCantidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public objDescuentos As New clsCWForm

Public CodAtrib As String
Private qry As New rdoQuery
Dim WithEvents objWin As clsCWWin
Attribute objWin.VB_VarHelpID = -1

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey    As String
  
  On Error GoTo cwIntError
  
  Call objApp.SplashOn
  
  Set objWin = New clsCWWin
  
  Call objWin.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, _
                                tlbToolbar1, _
                                stbStatusBar1, _
                                cwWithAll)
  
'  With objWin.objDoc
'    .cwPRJ = "CWSpy"
'    .cwMOD = "M�dulo Grupos de Usuarios"
'    .cwAUT = "SYSECA Bilbao"
'    .cwDAT = "22-07-97"
'    .cwDES = "Este formulario mantiene los grupos de usuarios"
'    .cwUPD = "22-07-97 - SYSECA Bilbao - Creaci�n del m�dulo"
'    .cwEVT = ""
'  End With

  With objDescuentos
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
'    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
'    .strDataBase = objEnv.GetValue("DataBase")
    .strTable = "FA0700"
    .strWhere = "FA15CODATRIB = " & CodAtrib

    Call .FormAddOrderField("FA07ACANTFIN", cwAscending)
  
'    strd("sg0006", "Relaci�n de Usuarios por Grupo")
    
    .intCursorSize = -1
    .strName = "Descuentos"
  End With
  
  With objWin
    Call .FormAddInfo(objDescuentos, cwFormMultiLine)
    Call .GridAddColumn(objDescuentos, "CodAtrib", "FA15CODATRIB", cwNumeric)
    Call .GridAddColumn(objDescuentos, "CodDto", "FA07CODDESCUENT", cwNumeric)
'    Call .GridAddColumn(objDescuentos, "Desde Cantidad", "FA07CANTINI", cwNumeric)
    Call .GridAddColumn(objDescuentos, "Hasta Cantidad (incluida)", "FA07ACANTFIN", cwNumeric)
    Call .GridAddColumn(objDescuentos, "Precio", "FA07PRECIO", cwNumeric)
    Call .GridAddColumn(objDescuentos, "Desde Cantidad", "FA07CANTINI", cwNumeric)
  
    Call .FormCreateInfo(objDescuentos)
    
'    With .CtrlGetInfo(grdDBGrid1(0).Columns(3))
'      .intMask = cwMaskUpper
'      .blnInFind = True
'    End With
     
    
    Call .WinRegister
    Call .WinStabilize
    
    Call .DataRefresh
  End With
    
'    grdDBGrid1(0).Columns(0).Visible = False
'    grdDBGrid1(0).Columns(1).Visible = False
'    grdDBGrid1(0).Columns(2).Visible = False
    grdDBGrid1(0).Columns(3).Visible = False
    grdDBGrid1(0).Columns(4).Visible = False
    grdDBGrid1(0).Columns(7).Visible = False
    
    grdDBGrid1(0).Columns(5).Alignment = ssCaptionAlignmentRight
    grdDBGrid1(0).Columns(6).Alignment = ssCaptionAlignmentRight
    grdDBGrid1(0).Columns(6).NumberFormat = "##,###,###,###"
    

    ' Inicializar la Query que busca el siguiente codigo de descuento
    ' para un codigo de atributo determinado
    qry.SQL = "SELECT MAX(FA07CODDESCUENT) + 1 Nuevo FROM FA0700 WHERE FA15CODATRIB = ?"
    Set qry.ActiveConnection = objApp.rdoConnect
    qry.Prepared = True

cwResError:
  Call objApp.SplashOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "Load")
  Resume cwResError
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWin.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWin.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWin.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWin.WinDeRegister
  Call objWin.WinRemoveInfo
End Sub

Private Sub objWin_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
    Dim blnRepetido As Boolean
    blnRepetido = False
    Dim i As Integer
    Dim x As Integer
    Dim Cantidad As Integer
    
    ' Si la cantidad esta vacia avisar
    If grdDBGrid1(0).Columns(5).Text = "" Then
        MsgBox "La cantidad no puede estar vacia", vbOKOnly, "Error de validaci�n"
        blnCancel = True
        Exit Sub
    End If
    
    ' Si el precio esta vacio avisar
    If grdDBGrid1(0).Columns(6).Text = "" Then
        MsgBox "El precio no puede estar vacio", vbOKOnly, "Error de validaci�n"
        blnCancel = True
        Exit Sub
    End If
    
'    grdDBGrid1(0).MoveFirst
'    For i = 1 To grdDBGrid1(0).Rows - 1
'        grdDBGrid1(0).Columns(5).Value = i
'        grdDBGrid1(0).MoveNext
'    Next
    
    
    ' Si el nuevo precio no tiene codigo asignarselo
    If grdDBGrid1(0).Columns(4).Text = "" Then
        grdDBGrid1(0).Columns(3).Text = CodAtrib
        grdDBGrid1(0).Columns(4).Text = NuevoCodigoDesc
    End If

End Sub

Private Sub objWin_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'    If strFormName = "Descuentos" Then
'        If objDescuentos.rdoCursor("FA15CODATRIB") = "" Then
'            objDescuentos.rdoCursor("FA15CODATRIB") = CodAtrib
'            objDescuentos.rdoCursor("FA07CODDESCUENT") = NuevoCodigoDesc
'        End If
'    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWin_cwPrint(ByVal strFormName As String)
  Dim strWhere  As String
  Dim strOrder  As String
  
  If strFormName = "Groups" Then
    With objWin.FormPrinterDialog(True, "")
      If .Selected > 0 Then
        strWhere = objWin.DataGetWhere(False)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
          strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), "WHERE ", " AND ")
          strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = "ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWin.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWin.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWin.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWin.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWin.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWin.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWin.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWin.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Function NuevoCodigoDesc() As String
    Dim RS As rdoResultset

    qry.rdoParameters(0) = CodAtrib
    
    Set RS = qry.OpenResultset

    If Not RS.EOF Then
        NuevoCodigoDesc = IIf(IsNull(RS!Nuevo), "1", RS!Nuevo)
    Else
        NuevoCodigoDesc = "1"
    End If
    
    Set RS = Nothing
End Function
