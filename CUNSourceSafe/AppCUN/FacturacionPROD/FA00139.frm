VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "Comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frm_Seleccion 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Selecci�n de la Asistencia  a facturar"
   ClientHeight    =   7080
   ClientLeft      =   45
   ClientTop       =   645
   ClientWidth     =   11655
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7080
   ScaleWidth      =   11655
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdAbonos 
      Caption         =   "&Hacer Abono"
      Height          =   375
      Left            =   10485
      TabIndex        =   24
      Top             =   5805
      Width           =   1140
   End
   Begin VB.CheckBox chkMostrar 
      Caption         =   "V&er Procesos"
      Height          =   330
      Index           =   3
      Left            =   10485
      Style           =   1  'Graphical
      TabIndex        =   30
      Top             =   3780
      Width           =   1140
   End
   Begin VB.CommandButton cmdFinAsistencia 
      Caption         =   "F&in Asistencia"
      Height          =   465
      Left            =   10485
      TabIndex        =   16
      Top             =   495
      Width           =   1140
   End
   Begin VB.CommandButton cmdNuevosRNFs 
      Caption         =   "A&�adir Movimientos"
      Enabled         =   0   'False
      Height          =   510
      Left            =   10485
      TabIndex        =   29
      Top             =   4320
      Width           =   1140
   End
   Begin VB.CheckBox chkAplicarFiltro 
      Alignment       =   1  'Right Justify
      Caption         =   "Aplicar en el filtro"
      Height          =   195
      Index           =   1
      Left            =   5760
      TabIndex        =   13
      ToolTipText     =   $"FA00139.frx":0000
      Top             =   2700
      Value           =   1  'Checked
      Width           =   1545
   End
   Begin VB.CheckBox chkAplicarFiltro 
      Alignment       =   1  'Right Justify
      Caption         =   "Aplicar en el filtro"
      Height          =   195
      Index           =   0
      Left            =   45
      TabIndex        =   11
      ToolTipText     =   $"FA00139.frx":0105
      Top             =   2700
      Width           =   1545
   End
   Begin VB.CommandButton cmdPersFis 
      Caption         =   "&Hoja de Filiaci�n"
      Height          =   465
      Left            =   10485
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   1590
      Width           =   1140
   End
   Begin VB.CommandButton cmdCambioAsist 
      Caption         =   "Cambios A&sistencia"
      Height          =   510
      Left            =   10485
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   1020
      Width           =   1140
   End
   Begin VB.CommandButton cmdReelabora 
      Caption         =   "&Reelaborar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   10485
      TabIndex        =   23
      Top             =   5355
      Width           =   1140
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "&Anular Fact."
      Enabled         =   0   'False
      Height          =   375
      Left            =   10485
      TabIndex        =   22
      Top             =   4905
      Width           =   1140
   End
   Begin VB.CommandButton cmdVisionGlobal 
      Caption         =   "&Vision Global"
      Height          =   465
      Left            =   10485
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   2115
      Width           =   1140
   End
   Begin ComctlLib.TreeView tvwProcesos 
      Height          =   3795
      Left            =   0
      TabIndex        =   15
      Top             =   2970
      Width           =   10365
      _ExtentX        =   18283
      _ExtentY        =   6694
      _Version        =   327682
      HideSelection   =   0   'False
      Indentation     =   794
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdMarcar 
      Caption         =   "&Marcar"
      Height          =   330
      Left            =   10485
      TabIndex        =   20
      Top             =   2970
      Width           =   1140
   End
   Begin VB.CommandButton cmdFacturar 
      Caption         =   "&Facturar"
      Height          =   330
      Left            =   10485
      TabIndex        =   21
      Top             =   3375
      Width           =   1140
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   10
      Top             =   6810
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   476
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Frame fraFrame1 
      Caption         =   " Persona"
      Height          =   2115
      Index           =   0
      Left            =   45
      TabIndex        =   1
      Top             =   450
      Width           =   10350
      Begin TabDlg.SSTab tabPacientes 
         Height          =   1770
         HelpContextID   =   90001
         Left            =   135
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   270
         Width           =   10140
         _ExtentX        =   17886
         _ExtentY        =   3122
         _Version        =   327681
         Style           =   1
         Tabs            =   1
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FA00139.frx":01B9
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "txtText1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txtText1(4)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(2)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "IdPersona2"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1305
            Index           =   0
            Left            =   -74880
            TabIndex        =   9
            Top             =   450
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   18865
            _ExtentY        =   2302
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin idperson.IdPersona IdPersona2 
            Height          =   1320
            Left            =   90
            TabIndex        =   3
            Top             =   360
            Width           =   10005
            _ExtentX        =   17648
            _ExtentY        =   2328
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NOMBRE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   1
            Left            =   4575
            TabIndex        =   8
            Tag             =   "Nombre Persona|Nombre Persona"
            Top             =   405
            Visible         =   0   'False
            Width           =   1950
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   7140
            TabIndex        =   7
            Tag             =   "Primer Apellido|Primer Apellido"
            Top             =   855
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   7380
            TabIndex        =   6
            Tag             =   "Segundo Apellido|Segundo Apellido"
            Top             =   1200
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22DNI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   6840
            TabIndex        =   5
            Tag             =   "D.N.I|D.N.I "
            Top             =   405
            Visible         =   0   'False
            Width           =   1845
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   8790
            TabIndex        =   4
            Tag             =   "N�mero Historia"
            Top             =   420
            Visible         =   0   'False
            Width           =   1230
         End
      End
   End
   Begin SSDataWidgets_B.SSDBGrid grdPruebas 
      Height          =   1305
      Left            =   0
      TabIndex        =   26
      Top             =   5445
      Visible         =   0   'False
      Width           =   10230
      ScrollBars      =   2
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      Col.Count       =   8
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      RowNavigation   =   1
      MaxSelectedRows =   10
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   609
      Columns(0).Caption=   "Facturar"
      Columns(0).Name =   "Facturar"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   12991
      Columns(1).Caption=   "Prueba"
      Columns(1).Name =   "Prueba"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1773
      Columns(2).Caption=   "Fecha"
      Columns(2).Name =   "Fecha"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1773
      Columns(3).Caption=   "Cantidad"
      Columns(3).Name =   "Cantidad"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "CodRNF"
      Columns(4).Name =   "CodRNF"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   2117
      Columns(5).Caption=   "Proceso"
      Columns(5).Name =   "Proceso"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   2117
      Columns(6).Caption=   "Asistencia"
      Columns(6).Name =   "Asistencia"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "NumRNF"
      Columns(7).Name =   "NumRNF"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      _ExtentX        =   18045
      _ExtentY        =   2302
      _StockProps     =   79
      Caption         =   "Pruebas Solicitadas"
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "&Continuar"
      Height          =   465
      Left            =   10485
      TabIndex        =   25
      Top             =   6300
      Visible         =   0   'False
      Width           =   1140
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   3015
      TabIndex        =   12
      Tag             =   "Fecha Inicio"
      Top             =   2655
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaFin 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   8640
      TabIndex        =   14
      Tag             =   "Fecha Inicio"
      Top             =   2655
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin ComctlLib.ImageList ImageList 
      Left            =   0
      Top             =   2655
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   16777215
      UseMaskColor    =   0   'False
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   20
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":01D5
            Key             =   "NodoCerrado"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":04EF
            Key             =   "AsistSoloFarmacia"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":0809
            Key             =   "RespMedico"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":0B23
            Key             =   "AsistVacia"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":0E3D
            Key             =   "AsistCerrada"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":1157
            Key             =   "AsistAbierta"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":1471
            Key             =   "Asistencia"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":178B
            Key             =   "TipoAsist"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":1AA5
            Key             =   "RespEconomico"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":1DBF
            Key             =   "NodoAbierto"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":20D9
            Key             =   "Hoja"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":23F3
            Key             =   "Editando"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":270D
            Key             =   "Borrado"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":2A27
            Key             =   "Concierto"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":2D41
            Key             =   "Factura"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":305B
            Key             =   "FacturaGarantia"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":3375
            Key             =   "Facturable"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":368F
            Key             =   "Abonable"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":39A9
            Key             =   "CatCerrado"
         EndProperty
         BeginProperty ListImage20 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00139.frx":3CC3
            Key             =   "CatAbierto"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblFecFin 
      Caption         =   "Fecha de Final:"
      Height          =   255
      Left            =   7380
      TabIndex        =   28
      Top             =   2700
      Width           =   1650
   End
   Begin VB.Label lblFecInicio 
      Caption         =   "Fecha de Inicio:"
      Height          =   255
      Left            =   1755
      TabIndex        =   27
      Top             =   2700
      Width           =   1650
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
   Begin VB.Menu mnuFact 
      Caption         =   "Facturas"
      Visible         =   0   'False
      Begin VB.Menu mnuFactAnular 
         Caption         =   "&Anular"
      End
      Begin VB.Menu mnuFactReelaborar 
         Caption         =   "&Reelaborar"
      End
      Begin VB.Menu mnuFactMarcar 
         Caption         =   "&Marcar/Desmarcar para hacer abono"
      End
      Begin VB.Menu mnuFactImprimir 
         Caption         =   "&Imprimir"
      End
      Begin VB.Menu mnuFactEditar 
         Caption         =   "&Editar"
      End
      Begin VB.Menu mnuFactA�adirOtra 
         Caption         =   "A�adirle O&tra Factura"
      End
   End
End
Attribute VB_Name = "frm_Seleccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim aProcAsist() As String
Dim QrySelec(1 To 15) As New rdoQuery
Dim qryProcesos(1 To 7) As New rdoQuery
Dim qryBloqueo(1 To 3) As New rdoQuery
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Public intIndGrid As Integer
Dim objMasterInfo As New clsCWForm
Dim blnMarcado As Boolean

Private Type typeRNF
  Asistencia As Long
  CodCateg As String
  CodRNF As String
  Cantidad As Long
  Fecha As String
End Type
Dim MatrizRNF() As typeRNF
Dim CadenaConceptos As String

Private varFact As factura
Private Paciente As Paciente
Private Asistencia As Asistencia
Private reco As Persona
Private NuevoRNF As rnf
Dim NumHisto As Long
Dim PrimeraVez As Boolean
Dim ProcAsist As String
Dim blnFacturaConjunta As Boolean

Dim arrayProc() As String
'Dim NoFactura As String
Dim FactContabil As Boolean
Dim FecAnte As String

Dim blnBloqueado As Boolean
Dim blnEntrada As Boolean

Sub BuscarPruebas()
   Dim MiSqL As String
   Dim MiRs As rdoResultset
   Dim SqlCateg As String
   Dim rsCateg As rdoResultset
   Dim Categoria As String
   Dim Texto As String
    
   MsgEstado "Buscando actuaciones no realizadas"
    
  Me.MousePointer = vbHourglass
    
  Me.grdPruebas.RemoveAll
  '*1
'  QrySelec(1).rdoParameters(0) = ProcesoAsistencia
  Set MiRs = QrySelec(1).OpenResultset(rdOpenKeyset)
  If Not MiRs.EOF Then
    tvwProcesos.Height = tvwProcesos.Height - 1350
    
    Me.grdPruebas.Visible = True
    Me.cmdContinuar.Visible = True

    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      If Not IsNull(MiRs("FA05CODCATEG")) Then
        '*10
        QrySelec(10).rdoParameters(0) = MiRs("FA05CODCATEG")
        'SqlCateg = "Select FA05DESIG From FA0500 Where FA05CODCATEG = " & MiRs("FA05CODCATEG")
        'Set rsCateg = objApp.rdoConnect.OpenResultset(SqlCateg, 3)
        Set rsCateg = QrySelec(10).OpenResultset(rdOpenKeyset)
        If Not rsCateg.EOF Then
          Categoria = rsCateg("FA05DESIG")
          Texto = 1 & Chr(9) & Categoria & Chr(9) & Format(MiRs("FA03fecha"), "dd/mm/yyyy hh:mm:ss") & Chr(9) & 0 & _
                  MiRs("FA03CANTIDAD") & Chr(9) & MiRs("FA05CODCATEG") & Chr(9) & _
                  MiRs("AD07CODPROCESO") & Chr(9) & MiRs("AD01CODASISTENCI") & Chr(9) & MiRs("FA03NUMRNF")
          Me.grdPruebas.AddItem Texto
        Else
          Categoria = ""
        End If
      End If
      MiRs.MoveNext
    Wend
    Me.MousePointer = vbDefault
    
    cmdContinuar.Enabled = True

  Else
    Me.MousePointer = vbDefault
    
    MsgEstado ""
    
    Call cmdContinuar_Click
  End If
  
  
End Sub

Sub InicQrySelec()
   Dim MiSqL As String
   Dim X As Integer
  
   ' SELECCION DE PROCESOS Y SUS RESPONSABLES MEDICOS
   MiSqL = "select AD0700.AD07CODPROCESO, AD07DESNOMBPROCE, ad07fechorainici, ad07fechorafin, " & _
           "     AD0400.AD04FECINIRESPON, AD0400.AD04FECFINRESPON, " & _
           "     SG0200.SG02NOM||' '||SG0200.SG02APE1||' '||SG0200.SG02APE2 AS Doctor, " & _
           "     AD02DESDPTO " & _
           "From AD0700, AD0400, SG0200, AD0200 " & _
           "where ci21codpersona = ? AND " & _
           "       AD07FECHORAINICI >= TO_DATE('01/01/1996','DD/MM/YYYY') AND " & _
           "       AD0700.AD07CODPROCESO = AD0400.AD07CODPROCESO AND " & _
           "       AD0400.SG02COD = SG0200.SG02COD AND " & _
           "       AD0400.AD02CODDPTO = AD0200.AD02CODDPTO " & _
           "order by ad07fechorainici desc, AD04FECINIRESPON desc"
   qryProcesos(1).SQL = MiSqL
          
   
   ' Modificada la sql para que cuente cuantas actuaciones tiene la asistencia en la pr0400
   ' para determinar que asistencias est�n vacias.
   MiSqL = "select /*+ RULE */ " & _
            "SUM(DECODE(PR0400.PR37CODESTADO,6,0,NULL,0,1)) AS CUENTA, " & _
                  "AD0700.AD07CODPROCESO, AD0800.AD01CODASISTENCI, " & _
                  "AD0800.AD08FECINICIO, AD0800.AD08FECFIN, AD08OBSERVAC, " & _
                  "AD05FECINIRESPON, AD05FECFINRESPON, " & _
                  "SG0200.SG02NOM||' '||SG0200.SG02APE1||' '||SG0200.SG02APE2 AS Doctor, " & _
                  "AD02DESDPTO " & _
             "From AD0700, AD0800, AD0500, SG0200, AD0200, PR0400 " & _
             "Where AD0700.CI21CODPERSONA = ? " & _
                  "AND AD07FECHORAINICI >= TO_DATE('01/01/1996','DD/MM/YYYY') " & _
                  "AND AD0700.AD07CODPROCESO = AD0800.AD07CODPROCESO " & _
                  "AND AD0700.AD07CODPROCESO = AD0500.AD07CODPROCESO " & _
                  "AND AD0800.AD01CODASISTENCI = AD0500.AD01CODASISTENCI " & _
                  "AND AD0500.SG02COD = SG0200.SG02COD " & _
                  "AND AD0500.AD02CODDPTO = AD0200.AD02CODDPTO " & _
                  "AND PR0400.AD07CODPROCESO (+)= AD0800.AD07CODPROCESO " & _
                  "AND PR0400.AD01CODASISTENCI (+)= AD0800.AD01CODASISTENCI " & _
             "GROUP BY AD0700.AD07CODPROCESO, AD0800.AD01CODASISTENCI, " & _
                    "AD0800.AD08FECINICIO, AD0800.AD08FECFIN, AD08OBSERVAC, " & _
                    "AD05FECINIRESPON, AD05FECFINRESPON, " & _
                    "SG0200.SG02NOM||' '||SG0200.SG02APE1||' '||SG0200.SG02APE2, " & _
                    "AD02DESDPTO " & _
           " order by AD08FECINICIO desc, AD05FECINIRESPON desc"
   
   qryProcesos(2).SQL = MiSqL
   
   
   ' SELECCION DE TIPOS DE ASISTENCIA
   MiSqL = "select AD0700.AD07CODPROCESO, AD0800.AD01CODASISTENCI, " & _
           "       AD25FECINICIO , AD25FECFIN, AD12DESTIPOASIST " & _
           " From AD0700, AD0800, AD2500, AD1200 " & _
           " where ci21codpersona = ? AND " & _
           " AD07FECHORAINICI >= TO_DATE('01/01/1996','DD/MM/YYYY') AND " & _
           "      AD0700.AD07CODPROCESO = AD0800.AD07CODPROCESO AND " & _
           "      AD0800.AD01CODASISTENCI = AD2500.AD01CODASISTENCI AND " & _
           "      (" & _
           "       ( AD25FECINICIO >= AD08FECINICIO AND AD25FECINICIO <= NVL(AD08FECFIN, TO_DATE('31/12/3000','DD/MM/YYYY'))) OR " & _
           "       ( NVL(AD25FECFIN, TO_DATE('01/01/1900','DD/MM/YYYY')) > AD08FECINICIO AND NVL(AD25FECFIN, TO_DATE('31/12/3000','DD/MM/YYYY')) <= NVL(AD08FECFIN, TO_DATE('31/12/3000','DD/MM/YYYY'))) OR " & _
           "       ( AD25FECINICIO < AD08FECINICIO AND NVL(AD25FECFIN, TO_DATE('31/12/3000','DD/MM/YYYY')) >= NVL(AD08FECFIN, TO_DATE('31/12/3000','DD/MM/YYYY'))) " & _
           "      ) AND " & _
           "      AD2500.AD12CODTIPOASIST = AD1200.AD12CODTIPOASIST " & _
           " order by AD25FECINICIO desc "
   qryProcesos(3).SQL = MiSqL
   
   ' SELECCION DE RESPONSABLES ECONOMICOS DE LA ASISTENCIA
   MiSqL = "select AD0700.AD07CODPROCESO, AD1100.AD01CODASISTENCI, " & _
           "       AD11FECINICIO , AD11FECFIN, AD1100.CI32CODTIPECON, AD1100.CI13CODENTIDAD, AD1100.CI21CODPERSONA, " & _
           "       NVL(LTRIM(CI22NOMBRE||' '||CI22PRIAPEL), CI23RAZONSOCIAL) AS RECO " & _
           " From AD0700, AD1100, CI2200, CI2300 " & _
           " where AD0700.ci21codpersona = ? AND " & _
           "      AD07FECHORAINICI >= TO_DATE('01/01/1996','DD/MM/YYYY') AND " & _
           "      AD0700.AD07CODPROCESO = AD1100.AD07CODPROCESO AND " & _
           "      AD1100.CI21CODPERSONA = CI2200.CI21CODPERSONA(+) AND " & _
           "      AD1100.CI21CODPERSONA = CI2300.CI21CODPERSONA(+) " & _
           " order by AD1100.AD01CODASISTENCI, AD11FECINICIO desc "
   qryProcesos(4).SQL = MiSqL
           
   ' SELECCION DE FACTURAS DE LA ASISTENCIA
'   MiSqL = "select AD0700.AD07CODPROCESO, FA0400.AD01CODASISTENCI, " & _
           "       FA04NUMFACT, FA04FECFACTURA, FA04FECDESDE, FA04FECHASTA, " & _
           "       FA04CANTFACT, FA04DESCRIP, FA04INDNMGC, " & _
           "       FA0400.CI32CODTIPECON , FA0400.CI13CODENTIDAD, FA0400.CI21CODPERSONA, " & _
           "       NVL(LTRIM(CI22NOMBRE||' '||CI22PRIAPEL), CI23RAZONSOCIAL) AS RECO, " & _
           "       FA04CODFACT_GARAN, FA04FECFINGARANTIA " & _
           " From AD0700, FA0400 , CI2200, CI2300 " & _
           " where AD0700.ci21codpersona = ? AND " & _
           "      AD07FECHORAINICI >= TO_DATE('01/01/1996','DD/MM/YYYY') AND " & _
           "      AD0700.AD07CODPROCESO = FA0400.AD07CODPROCESO  AND " & _
           "      FA0400.CI21CODPERSONA = CI2200.CI21CODPERSONA(+) AND " & _
           "      FA0400.CI21CODPERSONA = CI2300.CI21CODPERSONA(+) AND " & _
           "      FA04NUMFACREAL IS NULL " & _
           " order by FA04FECFACTURA desc, FA04NUMFACT DESC "
           
   MiSqL = "select DISTINCT AD0700.AD07CODPROCESO, FA0400.AD01CODASISTENCI, " & _
           "       FA04NUMFACT, FA04FECFACTURA, MIN(FA04FECDESDE) as FA04FECDESDE, MAX(FA04FECHASTA) as FA04FECHASTA, " & _
           "       SUM(FA04CANTFACT) as FA04CANTFACT, MAX(FA04DESCRIP) as FA04DESCRIP, MAX(FA04INDNMGC) as FA04INDNMGC, " & _
           "       MAX(FA0400.CI32CODTIPECON) as CI32CODTIPECON, MAX(FA0400.CI13CODENTIDAD) as CI13CODENTIDAD, MAX(FA0400.CI21CODPERSONA) as CI21CODPERSONA, " & _
           "       MAX(NVL(LTRIM(CI22NOMBRE||' '||CI22PRIAPEL), CI23RAZONSOCIAL)) AS RECO, " & _
           "       MAX(FA04CODFACT_GARAN) as FA04CODFACT_GARAN, MAX(FA04FECFINGARANTIA) as FA04FECFINGARANTIA " & _
           " From AD0700, FA0400 , CI2200, CI2300 " & _
           " where AD0700.ci21codpersona = ? AND " & _
           "      AD07FECHORAINICI >= TO_DATE('01/01/1996','DD/MM/YYYY') AND " & _
           "      AD0700.AD07CODPROCESO = FA0400.AD07CODPROCESO  AND " & _
           "      FA0400.CI21CODPERSONA = CI2200.CI21CODPERSONA(+) AND " & _
           "      FA0400.CI21CODPERSONA = CI2300.CI21CODPERSONA(+) AND " & _
           "      FA04NUMFACREAL IS NULL " & _
           " GROUP BY AD0700.AD07CODPROCESO, FA0400.AD01CODASISTENCI, FA04NUMFACT , FA04FECFACTURA " & _
           " order by FA04FECFACTURA desc, FA04NUMFACT DESC "
   qryProcesos(5).SQL = MiSqL
   
   ' Recoger el importe total de una factura.
'   MiSqL = "Select FA04CANTFACT From FA0400 Where FA04NUMFACT = ? " & _
            " AND FA04NUMFACREAL IS NULL AND FA04INDNMGC IS NULL"

  MiSqL = "Select SUM(FA04CANTFACT) AS FA04CANTFACT from FA0400 " & _
          "Where FA04NUMFACT = ? " & _
          "  AND FA04NUMFACREAL IS NULL " & _
          "  AND EXISTS (SELECT * FROM ad0700 " & _
          "              Where ad0700.CI22NUMHISTORIA = ? " & _
          "                AND ad0700.ad07codproceso = FA0400.AD07CODPROCESO)"
   qryProcesos(6).SQL = MiSqL
           
   ' comprobar que la asistencia no tiene movimientos de farmacia
   
      MiSqL = "select count(*) as Cuenta from faibm0005j " & _
               "Where ad07codproceso = ? " & _
               "  and ad01codasistenci = ? " & _
               "Union " & _
               "select count(*) as Cuenta from faibm0004j " & _
               "Where ad07codproceso = ? " & _
               "  and ad01codasistenci = ? " & _
               "order by 1 desc"
   qryProcesos(7).SQL = MiSqL
   
   
   For X = 1 To 7
      Set qryProcesos(X).ActiveConnection = objApp.rdoConnect
      qryProcesos(X).Prepared = True
   Next
  
  '****************************************************************************************
  
  MiSqL = "SELECT FA08DESVISTA FROM FA0800 WHERE FA11CODESTGRUPO = 1"
  QrySelec(6).SQL = MiSqL
  'Activamos las querys.
   Set QrySelec(6).ActiveConnection = objApp.rdoConnect
   QrySelec(6).Prepared = True
  
  
  MiSqL = "Select CI21CODPERSONA from CI2200 where CI22NUMHISTORIA = ? "
      
  QrySelec(8).SQL = MiSqL
  'Activamos las querys.
   Set QrySelec(8).ActiveConnection = objApp.rdoConnect
   QrySelec(8).Prepared = True
  
  '+9
  MiSqL = " SELECT FA09CODNODOCONC, FA09CODNODOFACT, CI21CODPERSONA,AD1100.CI32CODTIPECON, AD1100.CI13CODENTIDAD, " & _
            " CI13DESENTIDAD, AD11FECINICIO, AD11FECFIN " & _
            " FROM AD1100,CI1300 " & _
            " WHERE AD1100.CI32CODTIPECON=CI1300.CI32CODTIPECON " & _
            " AND AD1100.CI13CODENTIDAD=CI1300.CI13CODENTIDAD" & _
            " AND AD01CODASISTENCI = ? AND AD07CODPROCESO = ? " & _
            " ORDER BY AD11FECINICIO DESC"
  QrySelec(9).SQL = MiSqL
  'Activamos las querys.
   Set QrySelec(9).ActiveConnection = objApp.rdoConnect
   QrySelec(9).Prepared = True
  
  '+10
  MiSqL = "Select FA05DESIG From FA0500 Where FA05CODCATEG = ?"
  QrySelec(10).SQL = MiSqL
  'Activamos las querys.
   Set QrySelec(10).ActiveConnection = objApp.rdoConnect
   QrySelec(10).Prepared = True
  
End Sub
Sub InicQrySelec2(ProcesoAsistencia As String)
    
    Dim MiSqL As String
    Dim X As Integer
  
    '+1
    'Proceso - Asistencia
    MiSqL = "SELECT * FROM FA0300 " & _
              "WHERE " & ProcesoAsistencia & _
              " AND FA03INDREALIZADO = 0"
'    QrySelec(1).SQL = MiSqL
'    'Activamos la query.
'    Set QrySelec(1).ActiveConnection = objApp.rdoConnect
'    QrySelec(1).Prepared = True

      Set QrySelec(1) = objApp.rdoConnect.CreateQuery("", MiSqL)

    '+7
    'Proceso-Asistencia
    MiSqL = "SELECT * FROM FA0300 WHERE " & ProcesoAsistencia & " AND FA03INDREALIZADO = 1" & _
                " AND FA04NUMFACT IS NULL " & _
                " order by CI22NUMHISTORIA, AD07CODPROCESO, AD01CODASISTENCI, FA05CODCATEG, FA03FECHA"
                
      Set QrySelec(7) = objApp.rdoConnect.CreateQuery("", MiSqL)
  
    ' borrar los registros de RNFs no facturados de la fa0300 para ese proceso, asistencia
    MiSqL = "DELETE from FA0300 " & _
            "Where " & ProcesoAsistencia & " AND FA04NUMFACT is NULL AND FA03INDPERMANENTE is NULL"
            
      Set QrySelec(11) = objApp.rdoConnect.CreateQuery("", MiSqL)


   ' averiguar si hay movimientos facturables posteriores a la fecha
    MiSqL = "SELECT * FROM FA0300 WHERE " & ProcesoAsistencia & " AND FA03INDREALIZADO = 1" & _
                " AND FA04NUMFACT IS NULL " & _
                " order by CI22NUMHISTORIA, AD07CODPROCESO, AD01CODASISTENCI, FA05CODCATEG, FA03FECHA"
                
      Set QrySelec(12) = objApp.rdoConnect.CreateQuery("", MiSqL)

End Sub

Sub InicQryBloqueo()
    Dim sqlBloqueo As String
    Dim X As Integer
    
    ' Activar el bloqueo para esa historia
    sqlBloqueo = "INSERT INTO FA3400 (CI22NUMHISTORIA, SG02COD, FA34CPU) values (?,?,?)"
    qryBloqueo(1).SQL = sqlBloqueo
    'Activamos la query.
    Set qryBloqueo(1).ActiveConnection = objApp.rdoConnect
    qryBloqueo(1).Prepared = True
  
  
    ' Borrar el bloqueo de esa historia para la facturacion
    sqlBloqueo = "DELETE from FA3400 Where SG02COD = ? AND FA34CPU = ?"
    qryBloqueo(2).SQL = sqlBloqueo
    'Activamos la query.
    Set qryBloqueo(2).ActiveConnection = objApp.rdoConnect
    qryBloqueo(2).Prepared = True


    ' obtener quien ha bloqueado esa historia
    sqlBloqueo = "select CI22NUMHISTORIA, SG02COD, FA34CPU from FA3400 Where CI22NUMHISTORIA = ?"
    qryBloqueo(3).SQL = sqlBloqueo
    'Activamos la query.
    Set qryBloqueo(3).ActiveConnection = objApp.rdoConnect
    qryBloqueo(3).Prepared = True


End Sub


Private Sub chkMostrar_Click(Index As Integer)
   Refrescar
End Sub

Private Sub cmdAbonos_Click()
   Dim Nodo As Node
   Dim contFact As Integer
   Dim anumfact() As String
   Dim strFact As String
   ' revisar si se permiten las modificaciones
   If Not blnPermitirModifFact Then
      MsgBox "No tiene permisos para hacer abonos de facturas", vbExclamation + vbOKOnly, "Anulaci�n de Facturas"
      Exit Sub
   End If
    
   contFact = 0
   strFact = ""
   For Each Nodo In tvwProcesos.Nodes
      If Left(Nodo.Tag, 1) = "F" And Nodo.Image = "Abonable" Then
         If InStr(strFact, Right(Nodo.Tag, Len(Nodo.Tag) - 1)) = 0 Then
            contFact = contFact + 1
            ReDim Preserve anumfact(1 To contFact) As String
            anumfact(contFact) = Right(Nodo.Tag, Len(Nodo.Tag) - 1)
            strFact = strFact & "'" & Right(Nodo.Tag, Len(Nodo.Tag) - 1) & "',"
         End If
      End If
   Next
   
   If Len(strFact) > 0 Then
      strFact = Left(strFact, Len(strFact) - 1)
      Abonar strFact
   Else
      MsgBox "Antes de hacer un abono debe marcar las facturas a las que afectar�", vbInformation + vbOKOnly, "Abonos de Facturas"
   End If
End Sub

Private Sub cmdAnular_Click()
   ' revisar si se permiten las modificaciones
   If Not blnPermitirModifFact Then
      MsgBox "No tiene permisos para anular facturas", vbExclamation + vbOKOnly, "Anulaci�n de Facturas"
      Exit Sub
   End If
   
    ' al pulsar en un nodo se carga el numero de la factura seleccionada
    ' en un array para pasarselo a la funci�n que anula facturas
    If Left(tvwProcesos.SelectedItem.Tag, 1) = "F" Then
        Dim anumfact(1) As String
        anumfact(1) = Right(tvwProcesos.SelectedItem.Tag, Len(tvwProcesos.SelectedItem.Tag) - 1)
        MsgEstado "Anulando factura : " & Right(tvwProcesos.SelectedItem.Tag, Len(tvwProcesos.SelectedItem.Tag) - 1)
        Call AnularFact(anumfact, IdPersona2.Historia)
    End If
    MsgEstado "Refrescando Informaci�n de Procesos-Asistencia"
    cmdAnular.Enabled = False
'    tvwProcesos.Nodes.Remove tvwProcesos.SelectedItem.Index
    Refrescar
    MsgEstado ""
End Sub

Private Sub cmdCambioAsist_Click()
   On Error Resume Next
   
   Dim vntData(1) As Variant
   vntData(1) = IdPersona2.Text
   
   If Left(tvwProcesos.SelectedItem.Key, 1) = "A" Then
      vntData(2) = Right(tvwProcesos.SelectedItem.Tag, 10)
   End If
   
'   Call objSecurity.LaunchProcess("AD0115", vntData)
   Call objSecurity.LaunchProcess("AD3000", vntData)
End Sub

Private Sub cmdFinAsistencia_Click()
   Dim vntData(1 To 3) As Variant
   
   'Abre la form de fin de asistencia pasando codigo persona y Asistencia
'   vntData(1) = "FA0102" 'nombre del form llamador (utilizado en el Launcher)
'   vntData(2) = IdPersona2.Text  'codigo persona
'   vntData(3) = Right(tvwProcesos.SelectedItem.Tag, 10)  'codigo asistencia
'   Call objSecurity.LaunchProcess("AD0113", vntData)
   
   Call objSecurity.LaunchProcess("AD0211", IdPersona2.Text)
End Sub

Private Sub cmdHojaFiliac_Click()
   Dim vntdatos(1) As Variant
   vntdatos(1) = IdPersona2.Text
   Call objSecurity.LaunchProcess("CI4017", vntdatos)
End Sub

Private Sub cmdNuevosRNFs_Click()
   On Error Resume Next
   
   Dim vntData(1 To 5) As Variant
   
   If Left(tvwProcesos.SelectedItem.Key, 1) = "A" Then
   
      vntData(1) = IdPersona2.Historia                      ' historia
      vntData(2) = IdPersona2.Nombre & " " & _
                   IdPersona2.Apellido1 & " " & _
                   IdPersona2.Apellido2                     ' nombre paciente
      vntData(3) = Trim(Left(tvwProcesos.SelectedItem.Tag, 11))   ' proceso
      vntData(4) = Right(tvwProcesos.SelectedItem.Tag, 10)  ' asistencia
      Set vntData(5) = objApp
      
      Call objSecurity.LaunchProcess("FA0158", vntData)
   Else
      MsgBox "Debe seleccionar la asistencia en la" & Chr(10) & _
             "que a�adir los movimientos", vbInformation + vbOKOnly, "A�adir movimientos"
   End If

End Sub

Private Sub cmdPersFis_Click()
   Dim vntdatos(1) As Variant
   vntdatos(1) = IdPersona2.Text
   
'   Call objSecurity.LaunchProcess("CI4017", vntdatos)
   Call objSecurity.LaunchProcess("CI4017", IdPersona2.Text)
End Sub

Private Sub cmdContinuar_Click()
    Dim Cont As Integer
    Dim MiSqL As String

    On Error GoTo ErrorenGrabacion
    objApp.rdoConnect.BeginTrans
      If grdPruebas.Rows <> 0 Then
        MsgEstado "Actualizando estado de pruebas no realizadas"
        grdPruebas.MoveFirst
        For Cont = 1 To grdPruebas.Rows
          If grdPruebas.Columns(0).Value = True Then
            MiSqL = "Update FA0300 Set FA03INDREALIZADO = 1 Where FA05CODCATEG =  " & grdPruebas.Columns(4).Text & _
                        " And FA03INDREALIZADO = 0 And AD07CODPROCESO = " & grdPruebas.Columns(5).Text & " AND AD01CODASISTENCI = " & grdPruebas.Columns(6).Text & _
                        " And FA03FECHA = TO_DATE('" & grdPruebas.Columns(2).Text & "','DD/MM/YYYY HH24:MI:SS')"
            objApp.rdoConnect.Execute MiSqL
          End If
          grdPruebas.MoveNext
        Next
      End If
    objApp.rdoConnect.CommitTrans
    On Error GoTo 0
    Call LlenarTypeRNF(ProcAsist)
  

    Me.grdPruebas.Visible = False
    Me.cmdContinuar.Visible = False
'    tvwProcesos.Height = 3795
    tvwProcesos.Height = Me.Height - 3900
    
    Call Desbloquear
    chkAplicarFiltro(0).Value = False
    chkAplicarFiltro(1).Value = vbChecked
    SDCFechaInicio.Date = ""
    SDCFechaFin.Date = ""
    
   ' Inicializar el numero de factura de la reelaboraci�n
   NoFactura = ""
    Screen.MousePointer = vbDefault
Exit Sub
  Unload Me
Exit Sub
ErrorenGrabacion:
    MsgBox "Se ha producido un error en la asignaci�n de pruebas solicitadas", vbCritical + vbOKOnly, "Atenci�n"
    objApp.rdoConnect.RollbackTrans
End Sub

Private Sub cmdFacturar_Click()
    Dim Nodo As Node
    Dim IntPosGuion As Integer
    Dim proceso As Long
    Dim Asistencia As Long
    Dim FechaInicio As String
    Dim FechaFin As String
    Dim strProcAsist As String
    
    If Not Bloquear(IdPersona2.Historia) Then
        Exit Sub
    End If
    
    
    cmdContinuar.Enabled = False
    
    ProcAsist = "("
    strProcAsist = ""
    ' generar la factura de los nodos seleccionados
    For Each Nodo In tvwProcesos.Nodes
        If Nodo.Image = "Facturable" Then
        
              If Left(Nodo.Key, 1) = "A" Then
                If Nodo.Tag <> "" Then
                  Me.MousePointer = vbHourglass
                  IntPosGuion = InStr(1, Nodo.Tag, "-")
                  If IntPosGuion <> 0 Then
                    proceso = Left(Nodo.Tag, IntPosGuion - 1)
                    Asistencia = Right(Nodo.Tag, Len(Nodo.Tag) - IntPosGuion)
                    
                    ProcAsist = ProcAsist & " (AD07CODPROCESO = " & proceso & " AND AD01CODASISTENCI = " & Asistencia & ") OR"
                    strProcAsist = strProcAsist & proceso & "," & Asistencia & ","
                    
                    FechaInicio = SDCFechaInicio.Date
                    FechaFin = SDCFechaFin.Date
            
                    Me.MousePointer = vbNormal
                    
                  End If
                End If
              End If
        
        End If
    Next
    
    If ProcAsist <> "(" Then
        ProcAsist = Left(ProcAsist, Len(ProcAsist) - 3) & ")"
        strProcAsist = Left(strProcAsist, Len(strProcAsist) - 1)
        
        If Len(ProcAsist) > 40 Then
            blnFacturaConjunta = True
        End If
        
        InicQrySelec2 (ProcAsist)
        
        Call GenerarSqlRNF(strProcAsist, FechaInicio, FechaFin)
        
        Call BuscarPruebas
    End If
End Sub

Private Sub cmdMarcar_Click()
   On Error GoTo error
   If Left(tvwProcesos.SelectedItem.Key, 1) = "A" And tvwProcesos.SelectedItem.Image <> "AsistVacia" Then
      If tvwProcesos.SelectedItem.Image = "Facturable" Then
         tvwProcesos.SelectedItem.Image = Icono(tvwProcesos.SelectedItem)
         cmdMarcar.Caption = "&Marcar"
         blnMarcado = False
      Else
         tvwProcesos.SelectedItem.Image = "Facturable"
         cmdMarcar.Caption = "Des&marcar"
         blnMarcado = True
      End If
   ElseIf Left(tvwProcesos.SelectedItem.Tag, 1) = "F" Then
      If tvwProcesos.SelectedItem.Image = "Abonable" Then
         tvwProcesos.SelectedItem.Image = "Factura"
         cmdMarcar.Caption = "&Marcar"
         blnMarcado = False
      Else
         tvwProcesos.SelectedItem.Image = "Abonable"
         cmdMarcar.Caption = "Des&marcar"
         blnMarcado = True
      End If
   End If
Exit Sub
error:
' MsgBox "error"
End Sub

Private Sub cmdReelabora_Click()
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim Cont As Integer
Dim Rama As Node
Dim FechaDesde As String
Dim FechaHasta As String
Dim X As Integer
   ' revisar si se permiten las modificaciones
   If Not blnPermitirModifFact Then
      MsgBox "No tiene permisos para reelaborar facturas", vbExclamation + vbOKOnly, "Reelaboraci�n de Facturas"
      Exit Sub
   End If
   
    If Not Bloquear(IdPersona2.Historia) Then
        Exit Sub
    End If
    
  'Inicializamos el valor de N� de Factura anterior y si est� contabilizada.
  NoFactura = Right(Me.tvwProcesos.SelectedItem.Tag, Len(tvwProcesos.SelectedItem.Tag) - 1)
  
  FactContabil = False
  'Seleccionamos las facturas que se corresponden con ese c�digo para obtener los procesos
  'que se han facturado dentro de ellas
  MiSqL = "Select AD07CODPROCESO,AD01CODASISTENCI,FA04FECCONTABIL,FA04FECDESDE,FA04FECHASTA,FA04FECFACTURA " & _
          "From FA0400 Where FA04NUMFACT = '" & NoFactura & "' and FA04NUMFACREAL IS NULL"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    Cont = 0
    While Not MiRs.EOF
      Cont = Cont + 1
      ReDim Preserve arrayProc(1 To 4, 1 To Cont)
      If Not IsNull(MiRs("AD07CODPROCESO")) And Not IsNull(MiRs("AD01CODASISTENCI")) Then
        arrayProc(1, Cont) = " " & MiRs("AD07CODPROCESO") & "- " & MiRs("AD01CODASISTENCI")
      Else
        arrayProc(1, Cont) = Null
      End If
      If Not IsNull(MiRs("FA04FECCONTABIL")) Then
        arrayProc(2, Cont) = MiRs("FA04FECCONTABIL")
        'Si la fecha de contabilizaci�n no es nula lo indicamos para hacer la factura negativa.
        FactContabil = True
      Else
        arrayProc(2, Cont) = ""
      End If
      If Not IsNull(MiRs("FA04FECDESDE")) Then
        arrayProc(3, Cont) = MiRs("FA04FECDESDE")
        FechaDesde = Format(MiRs("FA04FECDESDE"), "DD/MM/YYYY")
      Else
        arrayProc(3, Cont) = ""
      End If
      If Not IsNull(MiRs("FA04FECHASTA")) Then
        arrayProc(4, Cont) = MiRs("FA04FECHASTA")
        FechaHasta = Format(MiRs("FA04FECHASTA"), "DD/MM/YYYY")
      Else
        arrayProc(4, Cont) = ""
      End If
      If Not IsNull(MiRs("FA04FECFACTURA")) Then
        FecAnte = Format(MiRs("FA04FECFACTURA"), "DD/MM/YYYY")
      End If
      MiRs.MoveNext
    Wend
  End If
  
'  If FactContabil = True Then
'    'realizaremos la factura negativa
'    ModAnulacionFact.GenerarFactNegativa (NoFactura)
'  Else
    'Anularemos la factura y la reharemos con el mismo n�mero
    MsgEstado "Anulando factura : " & Right(tvwProcesos.SelectedItem.Tag, Len(tvwProcesos.SelectedItem.Tag) - 1)
    Dim aFactura(1 To 1) As String
    aFactura(1) = NoFactura
    Call AnularFact(aFactura)
    
'  End If
  
  MsgEstado "Marcando las asistencias contenidas en la factura"
  'Marcamos todos los procesos asistencia que estaban seleccionados en la anterior factura
  For Each Rama In tvwProcesos.Nodes
'    Debug.Print Rama.Tag
    For X = 1 To Cont
      If Rama.Tag = arrayProc(1, X) Then
        Rama.Image = "Facturable"
        Exit For
      End If
    Next
  Next
  'Le Ponemos las fechas de inicio y final que figuran en la factura anterior
  If Trim(FechaDesde) <> "" Then
    Me.SDCFechaInicio.Date = CDate(FechaDesde)
  End If
  If Trim(FechaHasta) <> "" Then
    Me.SDCFechaFin.Date = CDate(FechaHasta)
  End If
  
  MsgEstado ""
End Sub

Private Sub cmdVisionGlobal_Click()
    Dim strProceso As String
    Dim strAsistencia As String
    
    On Error Resume Next
    
    If Left(tvwProcesos.SelectedItem.Key, 1) = "P" Then
        strProceso = Right(tvwProcesos.SelectedItem.Key, 10)
        Call VisionGlobal(IdPersona2.Text, strProceso)
        
    ElseIf Left(tvwProcesos.SelectedItem.Key, 1) = "A" Then
        strProceso = Left(Trim(tvwProcesos.SelectedItem.Tag), 10)
        strAsistencia = Right(tvwProcesos.SelectedItem.Tag, 10)
        Call VisionGlobal(IdPersona2.Text, strProceso, strAsistencia)
        
    Else
        Call VisionGlobal(IdPersona2.Text)
    End If
        
    Screen.MousePointer = vbDefault

End Sub

Private Sub Form_Activate()

  If blnEntrada = True Then
    tvwProcesos.SetFocus
    blnEntrada = False
  End If
  
End Sub

Private Sub Form_Resize()
   tvwProcesos.Width = frm_Seleccion.Width - 1300
   tvwProcesos.Height = frm_Seleccion.Height - 3900
   
   cmdFinAsistencia.Left = frm_Seleccion.Width - 1260
   cmdCambioAsist.Left = frm_Seleccion.Width - 1260
   cmdPersFis.Left = frm_Seleccion.Width - 1260
   cmdVisionGlobal.Left = frm_Seleccion.Width - 1260
   cmdMarcar.Left = frm_Seleccion.Width - 1260
   cmdFacturar.Left = frm_Seleccion.Width - 1260
   cmdNuevosRNFs.Left = frm_Seleccion.Width - 1260
   cmdAnular.Left = frm_Seleccion.Width - 1260
   cmdReelabora.Left = frm_Seleccion.Width - 1260
   
   cmdContinuar.Left = frm_Seleccion.Width - 1260
   cmdContinuar.Top = frm_Seleccion.Height - 1400
   
   chkMostrar(3).Left = frm_Seleccion.Width - 1260
   cmdAbonos.Left = frm_Seleccion.Width - 1260
   grdPruebas.Top = frm_Seleccion.Height - 2250
   
End Sub

Private Sub Form_Unload(Cancel As Integer)
  'Call idPersona2.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
  Set frm_Seleccion = Nothing
End Sub

Private Sub mnuFactAnular_Click()
   cmdAnular_Click
End Sub

Private Sub mnuFactA�adirOtra_Click()
   ' al pulsar en un nodo se carga el numero de la factura seleccionada
   ' en un array para pasarselo a la funci�n que imprime facturas
   Dim anumfact As String
   anumfact = Right(tvwProcesos.SelectedItem.Tag, Len(tvwProcesos.SelectedItem.Tag) - 1)
   Call frm_A�adirOtrasFact.A�adirFact(anumfact, IdPersona2.Text)
End Sub

Private Sub mnuFactEditar_Click()
   MsgBox "Editar"
End Sub

Private Sub mnuFactImprimir_Click()
   ' al pulsar en un nodo se carga el numero de la factura seleccionada
   ' en un array para pasarselo a la funci�n que imprime facturas
   Dim anumfact(1) As String
   anumfact(1) = Right(tvwProcesos.SelectedItem.Tag, Len(tvwProcesos.SelectedItem.Tag) - 1)
   Call ImprimirFact(anumfact, , IdPersona2.Historia)
End Sub

Private Sub mnuFactMarcar_Click()
   cmdMarcar_Click
End Sub

Private Sub mnuFactReelaborar_Click()
   cmdReelabora_Click
End Sub

'Sin Grid 6390
'Con Grid 7770

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  If strFormName = "fraFrame1(0)" And strCtrl = "idPersona2" Then
     IdPersona2.SearchPersona
     objWinInfo.DataRefresh
  End If
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
'Poner el color en titulo del frame exterior de actuaciones
  'fraFrame1(9).ForeColor = fraFrame1(tabTab1(2).Tab + 1).ForeColor
  If strFormName = "fraFrame1(0)" Then
    IdPersona2.blnAvisos = True
    tlbToolbar1.Buttons(26).Enabled = True

  Else
    IdPersona2.blnAvisos = False
    tlbToolbar1.Buttons(26).Enabled = False
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) = 0 Then
      objWinInfo.DataRefresh
    End If
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
    End If
  End If
  
  
End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'  If strFormName = "fraFrame1(0)" Then
'    Call idPersona2.ReadPersona
'  End If
    If Trim(IdPersona2.Text) <> "" And IsNumeric(IdPersona2.Historia) Then
      If IdPersona2.Historia <> "" Then
        Refrescar
      End If
    End If

End Sub
Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Public Function pRNF(objFactura As factura, Historia As Long) As factura
Dim MiSqL
    If Historia = 0 Then
      NumHisto = 0
    Else
      NumHisto = Historia
    End If
    Set varFact = objFactura
    PrimeraVez = True
    Load frm_Seleccion
      'Set frmSeleccion.varFact = objFactura
     frm_Seleccion.Show (vbModal)
    Set objFactura = varFact
    Unload frm_Seleccion
End Function

Private Sub Form_Load()
  
  Dim intGridIndex      As Integer
  
  blnEntrada = True
  
  Call objApp.SplashOn
  
  Call objApp.AddCtrl(TypeName(IdPersona2))
  Call IdPersona2.BeginControl(objApp, objGen)
  Set objWinInfo = New clsCWWin
  Call objWinInfo.WinCreateInfo(cwModeSingleOpen, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                 cwWithAll)
  
  With objMasterInfo
    '.strName = "Paciente"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPacientes
    Set .grdGrid = grdDBGrid1(0)
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2200"
    .strInitialWhere = "CI21CODPERSONA = 1000001"
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("CI21CODPERSONA", False)
    .blnChanged = False
  End With
  
  
  With objWinInfo
    
    Call .FormAddInfo(objMasterInfo, cwFormDetail)

    Call .FormCreateInfo(objMasterInfo)
    
    IdPersona2.ToolTipText = ""
    .CtrlGetInfo(IdPersona2).blnForeign = True
    
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    
 Call .WinRegister
    Call .WinStabilize
    IdPersona2.BackColor = objApp.objUserColor.lngKey
'    blncommit = False
  End With
      
      

 Call objApp.SplashOff
 
  Call Me.InicQrySelec
  Call Me.InicQryBloqueo
      
   chkAplicarFiltro(0).Value = False
   chkAplicarFiltro(1).Value = vbChecked
  SDCFechaInicio.Date = ""
  SDCFechaFin.Date = ""
  tvwProcesos.ImageList = ImageList
  
   If objPipe.PipeExist("CI21CODPERSONA") Then
      IdPersona2.Text = objPipe.PipeGet("CI21CODPERSONA")
      Call objPipe.PipeRemove("CI21CODPERSONA")
      tvwProcesos.Refresh
   End If
  
  Call EvitarModif
  'Me.Height = 6400
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
  If intUnloadMode = 0 Then
    BotonSalir = True
  End If
  
End Sub


Private Sub idPersona2_Change()
'   On Error Resume Next
  Call objWinInfo.CtrlDataChange
  'If PrimeraVez = True Then
  '  PrimeraVez = False
  'Else
    If Trim(IdPersona2.Text) <> "" And IsNumeric(IdPersona2.Text) Then
      If CDbl(IdPersona2.Text) >= 800000 Then
         'tvwProcesos.SetFocus
         Refrescar
      End If
'      Call Procesos(idPersona2.Text)
    End If
'  tvwProcesos.SetFocus
  'End If
End Sub

Sub Procesos(Persona As Long)
   Dim n As Node
   Dim Texto As String
   Dim Clave As String
   Dim ClaveAsist As String
   Dim ClaveFact As String
   Dim TextoFact As String
   Dim RsProcResp As rdoResultset
   Dim RsImporte As rdoResultset
   Dim rsFarmacia As rdoResultset
   Dim StrCadena As String

   On Error GoTo error

   
'   Set varFact = New factura
   blnMarcado = False
    
    Screen.MousePointer = vbHourglass
    Call LockWindowUpdate(Me.hWnd)
   tvwProcesos.Nodes.Clear
    
   '*************************************************
   '         PROCESOS - RESPONSABLES MEDICOS
   '*************************************************
   MsgEstado "Cargando Procesos"
    
   qryProcesos(1).rdoParameters(0) = Persona
   Set RsProcResp = qryProcesos(1).OpenResultset(rdOpenStatic)
   If Not RsProcResp.EOF Then
   
      If chkMostrar(3).Value = 1 Then  ' hay que mostrar los procesos
         Do While Not RsProcResp.EOF
            Clave = "P" & str(RsProcResp("AD07CODPROCESO"))
            Texto = "Proceso : " & RsProcResp("AD07CODPROCESO") & _
                    " " & RsProcResp("AD07DESNOMBPROCE") & _
                    "    (" & Format(RsProcResp("ad07fechorainici"), "dd/mm/yyyy") & _
                    " - " & Format(RsProcResp("ad07fechorafin"), "dd/mm/yyyy") & ")"
                    
            'A�adimos al treeview el proceso
            Set n = Me.tvwProcesos.Nodes.Add(, , Clave, Left(Texto, 99), "NodoCerrado")
            n.Expanded = True
         
'            If chkMostrar(0).Value = 1 Then
               ' a�adir los responsables medicos del proceso
               Texto = RsProcResp("AD02DESDPTO") & _
                       " - " & RsProcResp("Doctor") & _
                       "    ( " & Format(RsProcResp("AD04FECINIRESPON"), "dd/mm/yyyy") & " )"
                       
               Set n = Me.tvwProcesos.Nodes.Add(tvwProcesos.Nodes(Clave), tvwChild, , Left(Texto, 99), "RespMedico")
'            End If
            RsProcResp.MoveNext
          
         Loop ' rsProcResp
      End If
      
'      RsProcResp.Close
      
      '*************************************************
      '       ASISTENCIAS - RESPONSABLES MEDICOS
      '*************************************************
      MsgEstado "Cargando Responsables M�dicos de las Asistencias"
       
      qryProcesos(2).rdoParameters(0) = Persona
      Set RsProcResp = qryProcesos(2).OpenResultset(rdOpenStatic)
      Do While Not RsProcResp.EOF
         Clave = "P" & str(RsProcResp("AD07CODPROCESO"))
         ClaveAsist = "A" & str(RsProcResp("AD07CODPROCESO")) & "-" & str(RsProcResp("AD01CODASISTENCI"))
         
         Texto = "Asistencia : " & RsProcResp("AD01CODASISTENCI") & _
                 "    (" & Format(RsProcResp("AD08FECINICIO"), "dd/mm/yyyy") & _
                 " - " & Format(RsProcResp("AD08FECFIN"), "dd/mm/yyyy") & ")"
                 
         'A�adimos al treeview la asistencia
         ' hay que colgar la asistencia del proceso o de la raiz seg�n se muestren o no los procesos
         If chkMostrar(3).Value = 1 Then
            Set n = Me.tvwProcesos.Nodes.Add(tvwProcesos.Nodes(Clave), tvwChild, ClaveAsist, Left(Texto, 99))
         Else
            Set n = Me.tvwProcesos.Nodes.Add(, , ClaveAsist, Left(Texto, 99))
         End If
         
         
         tvwProcesos.Nodes(ClaveAsist).Tag = str(RsProcResp("AD07CODPROCESO")) & "-" & str(RsProcResp("AD01CODASISTENCI"))
         
         ' revisar si est� vacia la asistencia
         If RsProcResp("Cuenta") <> 0 Then
            tvwProcesos.Nodes(ClaveAsist).Image = Icono(tvwProcesos.Nodes(ClaveAsist))
            tvwProcesos.Nodes(ClaveAsist).Expanded = True
         Else
            ' no tiene movimientos en la PR0400, hay que revisar si tiene farmacia
            qryProcesos(7).rdoParameters(0) = RsProcResp("AD07CODPROCESO")
            qryProcesos(7).rdoParameters(1) = RsProcResp("AD01CODASISTENCI")
            qryProcesos(7).rdoParameters(2) = RsProcResp("AD07CODPROCESO")
            qryProcesos(7).rdoParameters(3) = RsProcResp("AD01CODASISTENCI")
            
            Set rsFarmacia = qryProcesos(7).OpenResultset(rdOpenStatic)
            If rsFarmacia("Cuenta") = 0 Then
               tvwProcesos.Nodes(ClaveAsist).Image = "AsistVacia"
               tvwProcesos.Nodes(ClaveAsist).Expanded = False
            Else
               ' solo tiene farmacia, algo no est� bien
               tvwProcesos.Nodes(ClaveAsist).Image = "AsistSoloFarmacia"
               tvwProcesos.Nodes(ClaveAsist).Expanded = True
            End If
         End If
      
'         If chkMostrar(0).Value = 1 Then
'             a�adir los responsables medicos de la asistencia
            Texto = RsProcResp("AD02DESDPTO") & _
                    " - " & RsProcResp("Doctor") & _
                    "    ( " & Format(RsProcResp("AD05FECINIRESPON"), "dd/mm/yyyy") & " )"
            Set n = Me.tvwProcesos.Nodes.Add(tvwProcesos.Nodes(ClaveAsist), tvwChild, , Left(Texto, 99), "RespMedico")
'         End If
         
         RsProcResp.MoveNext
       
      Loop ' rsProcResp
      
      
'      If chkMostrar(1).Value = 1 Then
'         '*************************************************
'         '      TIPOS DE ASISTENCIA
'         '*************************************************
         MsgEstado "Cargando tipos de las Asistencias"

         qryProcesos(3).rdoParameters(0) = Persona
         Set RsProcResp = qryProcesos(3).OpenResultset(rdOpenStatic)
         Do While Not RsProcResp.EOF
            ClaveAsist = "A" & str(RsProcResp("AD07CODPROCESO")) & "-" & str(RsProcResp("AD01CODASISTENCI"))

            ' a�adir los tipos de la asistencia
            Texto = RsProcResp("AD12DESTIPOASIST") & _
                    "    (" & Format(RsProcResp("AD25FECINICIO"), "dd/mm/yyyy") & _
                    " - " & Format(RsProcResp("AD25FECFIN"), "dd/mm/yyyy") & ")"

            Set n = Me.tvwProcesos.Nodes.Add(tvwProcesos.Nodes(ClaveAsist), tvwChild, , Left(Texto, 99), "TipoAsist")

            RsProcResp.MoveNext

         Loop ' rsProcResp
'      End If
      
'      If chkMostrar(2).Value = 1 Then
      
         '*************************************************
         '   RESPONSABLES ECONOMICOS DE LAS ASISTENCIAS
         '*************************************************
         MsgEstado "Cargando Responsables Econ�micos de las Asistencias"
          
         qryProcesos(4).rdoParameters(0) = Persona
         Set RsProcResp = qryProcesos(4).OpenResultset(rdOpenStatic)
         Do While Not RsProcResp.EOF
            ClaveAsist = "A" & str(RsProcResp("AD07CODPROCESO")) & "-" & str(RsProcResp("AD01CODASISTENCI"))
                
            ' a�adir los Responsables economicos de la asistencia
            Texto = RsProcResp("CI32CODTIPECON") & RsProcResp("CI13CODENTIDAD") & _
                    "    (" & Format(RsProcResp("AD11FECINICIO"), "dd/mm/yyyy") & _
                    " - " & Format(RsProcResp("AD11FECFIN"), "dd/mm/yyyy") & ")" & _
                    "  " & RsProcResp("RECO")
                    
            Set n = Me.tvwProcesos.Nodes.Add(tvwProcesos.Nodes(ClaveAsist), tvwChild, , Left(Texto, 99), "RespEconomico")
            
            RsProcResp.MoveNext
          
         Loop ' rsProcResp
'      End If
      
      '*************************************************
      '   FACTURAS
      '*************************************************
      MsgEstado "Cargando Facturas"
       
      qryProcesos(5).rdoParameters(0) = Persona
      Set RsProcResp = qryProcesos(5).OpenResultset(rdOpenStatic)
      Do While Not RsProcResp.EOF
         ClaveAsist = "A" & str(RsProcResp("AD07CODPROCESO")) & "-" & str(RsProcResp("AD01CODASISTENCI"))
             
             
'         If IsNull(RsProcResp("FA04INDNMGC")) Then
'            ' si es la cabecera principal
'            StrCadena = Format(RsProcResp("FA04CANTFACT"), "###,###,##0") & " Pts"
'
'         Else
'            'hay que recoger el importe, porque es una agrupacion de varias asistencias
            qryProcesos(6).rdoParameters(0) = "" & RsProcResp("FA04NUMFACT")
            qryProcesos(6).rdoParameters(1) = IdPersona2.Historia
            Set RsImporte = qryProcesos(6).OpenResultset(rdOpenKeyset)
            If Not RsImporte.EOF Then
               StrCadena = Format(RsImporte("FA04CANTFACT"), "###,###,##0") & " Pts"
            Else
               StrCadena = "0 Pts"
            End If
'         End If

         ' a�adir las facturas de la asistencia
         Texto = RsProcResp("FA04NUMFACT") & _
                 "   ( " & Format(StrCadena, "###,###,##0") & " )" & _
                 "   del  " & Format(RsProcResp("FA04FECFACTURA"), "dd/mm/yyyy") & _
                 "   entre   (" & Format(RsProcResp("FA04FECDESDE"), "dd/mm/yyyy") & _
                 " - " & Format(RsProcResp("FA04FECHASTA"), "dd/mm/yyyy") & ")" & _
                 "    (" & RsProcResp("CI32CODTIPECON") & RsProcResp("CI13CODENTIDAD") & ")" & _
                 "  " & RsProcResp("RECO")
                 
         Set n = Me.tvwProcesos.Nodes.Add(tvwProcesos.Nodes(ClaveAsist), tvwChild, , Texto, "Factura")
'         Set n = Me.tvwProcesos.Nodes.Add(tvwProcesos.Nodes(ClaveAsist), tvwChild, , Left(Texto, 99), "Factura")
         n.Tag = "F" & RsProcResp("FA04NUMFACT")
         
         If RsProcResp("FA04CODFACT_GARAN") <> "" Or RsProcResp("FA04FECFINGARANTIA") <> "" Then
            n.Image = "FacturaGarantia"
         End If
         
         tvwProcesos.Nodes(ClaveAsist).Image = "Factura"
         
         RsProcResp.MoveNext
       
      Loop ' rsProcResp
      
      MsgEstado ""
      
   Else
      MsgEstado "La persona seleccionada no tiene procesos relacionados"
   End If
   Call LockWindowUpdate(0&)
   Screen.MousePointer = vbDefault
      
Exit Sub

error:
   Select Case Err.Number
      Case 35602, 35601
         Resume Next
      
      Case Else
         MsgBox Err.Number & " " & Err.Description
         Resume
   End Select
End Sub

Private Sub idPersona2_GotFocus()
'  frmCitasPaciente.MousePointer = vbHourglass
'  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call objWinInfo.CtrlGotFocus
'  frmCitasPaciente.MousePointer = vbDefault
End Sub


Private Sub idPersona2_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub




Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  objMasterInfo.blnChanged = False
  If btnButton.Index = 16 Then
    Call Desbloquear
    Call IdPersona2.Buscar
    
  ElseIf btnButton.Index = 26 Then
    Call Refrescar
    
  ElseIf btnButton.Index = 30 Then
    Call Desbloquear
    BotonSalir = True
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    
  Else
    Call Desbloquear
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
End Sub

Private Sub tvwProcesos_DblClick()
  DoEvents
  If tvwProcesos.Nodes.Count > 0 Then
     If Left(tvwProcesos.SelectedItem.Key, 1) = "A" Then
     ' si es una asistencia y est� sin marcar se marca y se factura
        If tvwProcesos.SelectedItem.Image = "Facturable" Then
        Else
           tvwProcesos.SelectedItem.Image = "Facturable"
           cmdMarcar.Caption = "Des&marcar"
           blnMarcado = True
        End If
        cmdFacturar_Click

   
    ElseIf Left(tvwProcesos.SelectedItem.Tag, 1) = "F" Then
    ' al pulsar en un nodo se carga el numero de la factura seleccionada
    ' en un array para pasarselo a la funci�n que imprime facturas
       Dim anumfact(1) As String
       anumfact(1) = Right(tvwProcesos.SelectedItem.Tag, Len(tvwProcesos.SelectedItem.Tag) - 1)
       Call ImprimirFact(anumfact, , IdPersona2.Historia)
    End If
  End If
End Sub

'Sub GenerarSqlRNF(Proceso As Long, Asistencia As Long, FechaInicio As String, FechaFin As String)
Sub GenerarSqlRNF(ProcesoAsistencia As String, FechaInicio As String, FechaFin As String)
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim Cadena As String
'Dim aProcAsist() As String
Dim Proc As String
Dim Asist As String
Dim X As Integer
Dim i As Integer
  
    MsgEstado "Recogiendo RNFs"
    
    ' primero se BORRAN los RNFs existentes en la FA0300 del mismo
    ' proceso-asistencia y NO FACTURADOS
    QrySelec(11).Execute
    X = 0
    Do While ProcesoAsistencia <> ""
        X = X + 1
        If Left(ProcesoAsistencia, 1) = "," Then
            ProcesoAsistencia = Right(ProcesoAsistencia, Len(ProcesoAsistencia) - 1)
        End If
        Proc = Left(ProcesoAsistencia, 10)
        ProcesoAsistencia = Right(ProcesoAsistencia, Len(ProcesoAsistencia) - 11)
        Asist = Left(ProcesoAsistencia, 10)
        ProcesoAsistencia = Right(ProcesoAsistencia, Len(ProcesoAsistencia) - 10)
         
        ReDim Preserve aProcAsist(1 To 2, 1 To X)
        aProcAsist(1, X) = Proc
        aProcAsist(2, X) = Asist
    Loop

    On Error GoTo errorRNFs
    
    Cadena = ""
    'Seleccionamos todas las vistas de los grupos que se encuentren activos
    Set MiRs = QrySelec(6).OpenResultset(rdOpenKeyset)
    If Not MiRs.EOF Then
        MiRs.MoveLast
        MiRs.MoveFirst
        While Not MiRs.EOF
            For i = 1 To X
               ' En las vistas de farmacia discriminar el control de los RNF ya facturados en funcion
               ' de la fecha :
               '  en los anteriores al 01/04/2000 no se compara la cantidad (para evitar el problema de cambio de dosis)
               '  en las posteriores si que se compara
               If MiRs(0) = "FAIBM0004J" Or MiRs(0) = "FAIBM0005J" Then
               
                  'Creamos la SQL de selecci�n de los RNF, una para cada Proceso-Asistencia
                  MiSqL = "INSERT INTO FA0300 " & _
                            "(FA03NUMRNF,FA05CODCATEG, FA03PRECIO, FA03PRECIOFACT, FA03CANTIDAD, FA03CANTFACT, FA13CODESTRNF,AD01CODASISTENCI, " & _
                            " FA03FECHA,CI22NUMHISTORIA,FA03OBSERV,AD07CODPROCESO, FA03INDREALIZADO, " & _
                            " AD02CODDPTO_R, SG02COD_R, AD02CODDPTO_S, SG02COD_S) " & _
                            "  SELECT /*+ RULE */ " & _
                            "    FA03NUMRNF_SEQUENCE.NEXTVAL, FA05CODCATEG, PRECIO, PRECIO, CANTIDAD,  CANTIDAD,  1, " & _
                            "  AD01CODASISTENCI,  FECHA,  CI22NUMHISTORIA, " & _
                            "    OBS , AD07CODPROCESO, ESTADO, DPTOREALIZA, DRREALIZA, DPTOSOLICITA, DRSOLICITA " & _
                            "  From " & MiRs(0) & " T1 " & _
                            "  Where " & _
                            "        T1.AD07CODPROCESO = " & aProcAsist(1, i) & _
                            "    AND T1.AD01CODASISTENCI = " & aProcAsist(2, i) & _
                            "    AND T1.IC = 0 " & _
                    IIf(chkAplicarFiltro(0).Value = vbChecked And FechaInicio <> "", " AND T1.FECHA >= TO_DATE('" & FechaInicio & " 00:00:00' , 'DD/MM/YYYY HH24:MI:SS') ", "") & _
                    IIf(chkAplicarFiltro(1).Value = vbChecked And FechaFin <> "", " AND T1.FECHA <= TO_DATE('" & FechaFin & " 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') ", "")
                    
                  MiSqL = MiSqL & _
                            "    AND " & _
                            "       ( (T1.FECHA <= TO_DATE('31/03/2000 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') AND NOT EXISTS " & _
                            "          (SELECT AD07CODPROCESO , AD01CODASISTENCI, FA05CODCATEG, FA03FECHA " & _
                            "           FROM FA0300 T2 " & _
                            "           WHERE " & _
                            "             T2.AD07CODPROCESO = T1.AD07CODPROCESO " & _
                            "             AND T2.AD01CODASISTENCI=T1.AD01CODASISTENCI " & _
                            "             AND T2.FA05CODCATEG=T1.FA05CODCATEG " & _
                            "             AND T2.FA03FECHA=T1.FECHA " & _
                            "             AND T2.FA03FECHA <= TO_DATE('31/03/2000 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') " & _
                            "             AND T2.FA03INDFACT <> 3))" & _
                            "        OR " & _
                            "         (T1.FECHA > TO_DATE('31/03/2000 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') AND NOT EXISTS " & _
                            "          (SELECT AD07CODPROCESO , AD01CODASISTENCI, FA05CODCATEG, FA03FECHA, FA03CANTIDAD " & _
                            "           FROM FA0300 T2 " & _
                            "           WHERE " & _
                            "             T2.AD07CODPROCESO = T1.AD07CODPROCESO " & _
                            "             AND T2.AD01CODASISTENCI=T1.AD01CODASISTENCI " & _
                            "             AND T2.FA05CODCATEG=T1.FA05CODCATEG " & _
                            "             AND T2.FA03CANTIDAD=T1.CANTIDAD AND T2.FA03FECHA=T1.FECHA " & _
                            "             AND T2.FA03FECHA > TO_DATE('31/03/2000 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') " & _
                            "             AND T2.FA03INDFACT <> 3))" & _
                            "        )"
                            
               ' Resto de vistas que no son farmacia
               Else
                  'Creamos la SQL de selecci�n de los RNF, una para cada Proceso-Asistencia
                  MiSqL = "INSERT INTO FA0300 " & _
                            "(FA03NUMRNF,FA05CODCATEG, FA03PRECIO, FA03PRECIOFACT, FA03CANTIDAD, FA03CANTFACT, FA13CODESTRNF,AD01CODASISTENCI, " & _
                            " FA03FECHA,CI22NUMHISTORIA,FA03OBSERV,AD07CODPROCESO, FA03INDREALIZADO, " & _
                            " AD02CODDPTO_R, SG02COD_R, AD02CODDPTO_S, SG02COD_S) " & _
                            "  SELECT  /*+ RULE */ " & _
                            "    FA03NUMRNF_SEQUENCE.NEXTVAL, FA05CODCATEG, PRECIO, PRECIO, CANTIDAD,  CANTIDAD,  1, " & _
                            "  AD01CODASISTENCI,  FECHA,  CI22NUMHISTORIA, " & _
                            "    OBS , AD07CODPROCESO, ESTADO, DPTOREALIZA, DRREALIZA, DPTOSOLICITA, DRSOLICITA " & _
                            "  From " & MiRs(0) & " T1 " & _
                            "  Where " & _
                            "        T1.AD07CODPROCESO = " & aProcAsist(1, i) & _
                            "    AND T1.AD01CODASISTENCI = " & aProcAsist(2, i) & _
                            "    AND T1.IC = 0 " & _
                    IIf(chkAplicarFiltro(0).Value = vbChecked And FechaInicio <> "", " AND T1.FECHA >= TO_DATE('" & FechaInicio & " 00:00:00' , 'DD/MM/YYYY HH24:MI:SS') ", "") & _
                    IIf(chkAplicarFiltro(1).Value = vbChecked And FechaFin <> "", " AND T1.FECHA <= TO_DATE('" & FechaFin & " 23:59:59' , 'DD/MM/YYYY HH24:MI:SS') ", "")
                  
                  MiSqL = MiSqL & _
                            "    AND NOT EXISTS " & _
                            "     (SELECT " & _
                            "  AD07CODPROCESO , AD01CODASISTENCI, FA05CODCATEG, FA03FECHA, FA03CANTIDAD " & _
                            "      FROM FA0300 T2 " & _
                            "      Where " & _
                            "        T2.AD07CODPROCESO = T1.AD07CODPROCESO " & _
                            "     AND T2.AD01CODASISTENCI=T1.AD01CODASISTENCI " & _
                            "     AND T2.FA05CODCATEG=T1.FA05CODCATEG " & _
                            "     AND T2.FA03CANTIDAD=T1.CANTIDAD AND T2.FA03FECHA=T1.FECHA " & _
                            "     AND T2.FA03INDFACT <> 3" & _
                            "     )"
                            
               End If
               
               ' ejecutar la vista correspondiente
               objApp.rdoConnect.Execute MiSqL
            
            Next
            MiRs.MoveNext
          
        Wend
    End If
    
    
    MsgEstado ""

Exit Sub
errorRNFs:
'    MsgBox "Error en el proceso de recogida de RNFs" & Chr(13) & "La factura no ser� correcta" & Chr(13) & Chr(13) & _

    MsgBox "Se ha producido un error en la inserci�n de los RNFs" & Chr(13) & "La factura no ser� correcta" & Chr(13) & Chr(13) & _
           Err.Number & " " & Err.Description, vbCritical + vbOKOnly, "ERROR"
    Resume Next
End Sub

Sub LlenarTypeRNF(ProcesoAsist As String)
   Dim MiSqL As String
   Dim MiRs As rdoResultset
   Dim sqlFecha As String
   Dim rsfecha As rdoResultset
   Dim rsNombre As rdoResultset
   Dim MiSqLConc As String
   Dim MiRsConc As rdoResultset
   Dim Cont As Long
   Dim NumRNF As Long
   Dim ContRNF As Long
   Dim i As Integer
   Dim RNFAnterior As String
   Dim AsistAnterior As String
   Dim ProcAnterior As String
   Dim PacienteAnterior As String
   Dim Fecha As String
   Dim NuevoProcAsist As ProcAsist
   Dim blnPacienteYaREco As Boolean
   Dim objREco As Persona
   Dim objPaciente As Persona
   Dim reco As Persona
   Dim blnMensajeYaMostrado As Boolean
   Dim objRNF As rnf
   Dim blnFueraDeFecha As Boolean
    
   MsgEstado "Generando las propuestas de factura"
    
   On Error GoTo ErrorenRNF
  
   RNFAnterior = ""
   AsistAnterior = ""
   PacienteAnterior = ""
   ProcAnterior = ""
   ContRNF = 0
  
   ' crear un objeto para la nueva factura
   Set varFact = New factura
  ' activar la facturacion masiva para que no muestre mensajes que puedan
  ' interrumpir el proceso autom�tico de facturas
  blnFacturacionMasiva = False
  
  MsgEstado "Recogiendo RNFs a facturar"
   
   ' ************************************************************************************************
   ' ahora se van a recoger todos los rnfs a facturas en un unico recordset, puede que correspondan
   ' a diferentes pacientes o proceso-asistencias, por lo que habr� que controlar cuando se produce
   ' un cambio de alguno de estos datos para crear los correspondientes objetos.
   ' ************************************************************************************************
   Set MiRs = QrySelec(7).OpenResultset(rdOpenKeyset)
   If Not MiRs.EOF Then
      Cont = 1
      ContRNF = 0
      Do While Not MiRs.EOF
      
         blnFueraDeFecha = False
         ' ************************************************************************************************
         ' revisar si hay que avisar de la existencia de movimientos posteriores a la fecha de fin
         ' de la factura
         ' ************************************************************************************************
         If chkAplicarFiltro(0).Value = vbChecked And _
            SDCFechaInicio.Date <> "" And _
            CVDate(MiRs("FA03FECHA")) < CVDate(SDCFechaInicio.Date & " 00:00:00") Then
            blnFueraDeFecha = True
            ' sobrepasa el limite inferior de fechas
         End If
         
         If chkAplicarFiltro(1).Value = vbChecked And _
            SDCFechaFin.Date <> "" And _
            CVDate(MiRs("FA03FECHA")) > CVDate(SDCFechaFin.Date & " 23:59:59") Then
            blnFueraDeFecha = True
            ' sobrepasa el limite superior de fechas
         End If
         
         If chkAplicarFiltro(1).Value = vbUnchecked And SDCFechaFin.Date <> "" And Not blnMensajeYaMostrado Then
            If CVDate(MiRs("FA03FECHA")) > CVDate(SDCFechaFin.Date & " 23:59:59") Then
               blnMensajeYaMostrado = True
               MsgBox "Hay movimientos posteriores a la fecha de fin de factura, que ser�n incluidos en la propuesta.", vbInformation + vbOKOnly, "Facturaci�n"
            End If
         End If
         
         
         If Not blnFueraDeFecha Then
            ' ************************************************************************************************
            ' si se detecta un NUEVO PACIENTE (numero de historia) crearlo y a�adirlo a la coleccion
            ' ************************************************************************************************
'            If MiRs("CI22NUMHISTORIA") <> PacienteAnterior Then
            If IIf(IsNull(MiRs("CI22NUMHISTORIA")), IdPersona2.Historia, MiRs("CI22NUMHISTORIA")) <> PacienteAnterior Then
               ' crear el objeto paciente
               Set Paciente = New Paciente
               Paciente.Codigo = IdPersona2.Historia
               
               ' buscar el CI21CODPERSONA correspondiente al numero de historia
               QrySelec(8).rdoParameters(0) = IdPersona2.Historia
               Set rsNombre = QrySelec(8).OpenResultset(rdOpenStatic)
               
               If Not rsNombre.EOF Then
                  ' utilizar un objeto persona para standarizar y simplificar la busqueda
                  ' del resto de sus datos
                  Set objPaciente = New Persona
                  objPaciente.Codigo = rsNombre("CI21CODPERSONA") & ""
                  
                  With Paciente
                        .CodPersona = objPaciente.Codigo
                        .Name = objPaciente.Name
                        .Direccion = objPaciente.Direccion
                        .FecNacimiento = objPaciente.FecNacimiento
                     
                  End With
               End If
                
               ' a�adirlo a la coleccion
               varFact.Pacientes.Add Paciente, IdPersona2.Historia & ""
               
               ' guardar su numero de historia para compararlo con el siguiente
               PacienteAnterior = IdPersona2.Historia
            End If
            
            
            
            ' ************************************************************************************************
            ' Ver si el PROCESO-ASISTENCIA es el primero o ha cambiado para procesarlo de diferente manera:
            '     - si es la primera : se crea el objeto asistencia para contener los RNFs y las propuestas
            '
            '     - si es distinta de la anterior : no se crea el objeto
            '
            '     - en ambos casos se buscan sus REcos para a�adirlos a la asistencia
            ' ************************************************************************************************
            If MiRs("AD01CODASISTENCI") <> AsistAnterior Or MiRs("AD07CODPROCESO") <> ProcAnterior Then
               If AsistAnterior = "" Or ProcAnterior = "" Then
                  ' esta es la primera asistencia entonces la creamos
                  Set Asistencia = New Asistencia
                  
                  Asistencia.Codigo = MiRs("AD07CODPROCESO") & "-" & MiRs("AD01CODASISTENCI")
      
                  Asistencia.FactAntigua = FactContabil
                  Asistencia.NumFactAnt = NoFactura
                  Asistencia.FechaAnterior = FecAnte
                  
                  Asistencia.Facturado = False
                  Asistencia.strProcAsist = ProcesoAsist
                  
                  
                  ' Hay que recoger las fechas del proceso-asistencia
                  sqlFecha = "Select AD08FECINICIO, AD08FECFIN From AD0800" & _
                                  " Where AD01CODASISTENCI = " & MiRs("AD01CODASISTENCI") & " AND " & _
                                  " AD07CODPROCESO = " & MiRs("AD07CODPROCESO")
                  Set rsfecha = objApp.rdoConnect.OpenResultset(sqlFecha, 3)
                  
                  If Not rsfecha.EOF Then
                      ' cargar las fechas del filtro para la coletilla de la factura
                      If SDCFechaInicio.Date <> "" Then
                          Asistencia.FecInicioFact = SDCFechaInicio.Date
                      Else
                          If Not IsNull(rsfecha("AD08FECINICIO")) Then
                            Asistencia.FecInicioFact = Format(rsfecha("AD08FECINICIO"), "DD/MM/YYYY")
                          End If
                      End If
                      
                      If SDCFechaFin.Date <> "" Then
                          Asistencia.FecFinFact = SDCFechaFin.Date
                      Else
                          If Not IsNull(rsfecha("AD08FECFIN")) Then
                            Asistencia.FecFinFact = Format(rsfecha("AD08FECFIN"), "DD/MM/YYYY")
                          End If
                      End If
                      
                      ' fechas reales de la asistencia (puede que haya RNF anteriores o posteriores a la asistencia,
                      ' con lo cual se prolongarian estas fechas para contenerlos a todos.
                      If Not IsNull(rsfecha("AD08FECINICIO")) Then
                          Asistencia.FecInicio = Format(rsfecha("AD08FECINICIO"), "DD/MM/YYYY HH:MM:SS")
                      End If
                      If Not IsNull(rsfecha("AD08FECFIN")) Then
                          Asistencia.FecFin = Format(DateAdd("D", 1, CDate(rsfecha("AD08FECFIN"))), "DD/MM/YYYY HH:MM:SS")
                      End If
                  End If
                  
                  
                  ' Hay que recoger el ultimo dpto responsable del proceso-asistencia
'                  sqlFecha = "select ad0500.AD02codDPTO, AD02DESDPTO, AD05FECINIRESPON from ad0500, ad0200 " & _
                             "Where AD01CODASISTENCI = " & MiRs("AD01CODASISTENCI") & _
                                  " AND AD07CODPROCESO = " & MiRs("AD07CODPROCESO") & _
                                  " AND ad0500.AD02codDPTO = ad0200.AD02codDPTO " & _
                                  " AND AD05FECFINRESPON is null " & _
                             "order by AD05FECINIRESPON DESC"
                  sqlFecha = "select ad0500.AD02codDPTO, AD02DESDPTO, AD05FECINIRESPON from ad0500, ad0200 " & _
                             "Where " & ProcAsist & _
                                  " AND ad0500.AD02codDPTO = ad0200.AD02codDPTO " & _
                                  " AND AD05FECFINRESPON is null " & _
                             "order by AD05FECINIRESPON DESC"
                                  
                  Set rsfecha = objApp.rdoConnect.OpenResultset(sqlFecha, 3)
                  
                  If Not rsfecha.EOF Then
                     Asistencia.CodDptoResp = "" & rsfecha("AD02codDPTO").Value
                     Asistencia.DptoResp = "" & rsfecha("AD02DESDPTO").Value
                  End If
                  
                  
                  ' A�adir la asistencia al paciente
                  Paciente.Asistencias.Add Asistencia, CStr(Asistencia.Codigo)
               End If
               
               ' a�adir el proceso-asistencia (solo los codigos) a la asistencia para tener una relacion de
               ' los diferentes proc-asist que se van a facturar juntos.
               Set NuevoProcAsist = New ProcAsist
               NuevoProcAsist.Asistencia = MiRs("AD01CODASISTENCI")
               NuevoProcAsist.proceso = MiRs("AD07CODPROCESO")
               Asistencia.ProcAsist.Add NuevoProcAsist
               
               AsistAnterior = MiRs("AD01CODASISTENCI")
               ProcAnterior = MiRs("AD07CODPROCESO")
                  
                  
               
               ' ************************************************************************************************
               ' hay que buscar los recos de la asistencia y a�adirlos a la misma para despues localizar a que
               ' Reco corresponde cada RNF
               ' ************************************************************************************************
               QrySelec(9).rdoParameters(0) = MiRs("AD01CODASISTENCI")
               QrySelec(9).rdoParameters(1) = MiRs("AD07CODPROCESO")
               Set MiRsConc = QrySelec(9).OpenResultset(rdOpenKeyset)
            
               Do While Not MiRsConc.EOF
                  If Trim(MiRsConc("FA09CODNODOCONC")) <> "" Then
                     If IsNumeric(MiRsConc("FA09CODNODOCONC")) Then
                        ' a�adir el nodo de concierto a la asistencia
                        If Not IsNull(MiRsConc("FA09CODNODOFACT")) Then
                           Asistencia.AddConcierto MiRsConc("FA09CODNODOCONC"), MiRsConc("FA09CODNODOFACT")
                        Else
                           Asistencia.AddConcierto MiRsConc("FA09CODNODOCONC")
                        End If
                        ' crear el REco
                        Set reco = New Persona
                        If "" & MiRsConc("CI21CODPERSONA") <> "" Then
                           reco.Codigo = MiRsConc("CI21CODPERSONA")
                        Else
                           reco.Codigo = Paciente.CodPersona
                        End If
                        
                        reco.Concierto.Concierto = MiRsConc("FA09CODNODOCONC")
                        If Not IsNull(MiRsConc("FA09CODNODOFACT")) Then
                           reco.Concierto.NodoAFacturar = MiRsConc("FA09CODNODOFACT")
                        End If
   '                     reco.Concierto.Name = "" & MiRsConc("FA09DESIG")
                        reco.CodTipEcon = "" & MiRsConc("CI32CODTIPECON")
                        reco.EntidadResponsable = "" & MiRsConc("CI13CODENTIDAD")
                        reco.DesEntidad = "" & MiRsConc("CI13DESENTIDAD")
                        reco.proceso = MiRs("AD07CODPROCESO")
                        reco.Asistencia = MiRs("AD01CODASISTENCI")
                        ' a�adirlo
                        
                        Asistencia.AddREco reco, "" & MiRsConc("AD11FECINICIO"), "" & MiRsConc("AD11FECFIN")
                     End If
                  End If
                  MiRsConc.MoveNext
               Loop
      
            End If
            
            
            
            ' ************************************************************************************************
            ' crear y a�adir el rnf a la asistencia
            ' ************************************************************************************************
   
            With Asistencia
               ' si es un proceso-asistencia distinto a�adirlo a la coleccion de proc-asist.
               If MiRs("AD01CODASISTENCI") <> AsistAnterior Or MiRs("AD07CODPROCESO") <> ProcAnterior Then
                   Set NuevoProcAsist = New ProcAsist
                   NuevoProcAsist.Asistencia = MiRs("AD01CODASISTENCI")
                   NuevoProcAsist.proceso = MiRs("AD07CODPROCESO")
                   .ProcAsist.Add NuevoProcAsist
               End If
   
               If MiRs("FA05CODCATEG") <> RNFAnterior Then
                 Cont = 0
                 RNFAnterior = MiRs("FA05CODCATEG")
               Else
                 Cont = Cont + 1
               End If
               Set NuevoRNF = New rnf
               With NuevoRNF
                   .CodCateg = MiRs("FA05CODCATEG")
                   .CodRNF = MiRs("FA03NUMRNF")
                   .Fecha = MiRs("FA03FECHA") & ""
                   If IsNumeric(MiRs("FA03CANTFACT")) Then
                       .Cantidad = MiRs("FA03CANTFACT")
                   End If
                   If IsNumeric(MiRs("FA03PRECIOFACT")) Then
                       .Precio = MiRs("FA03PRECIOFACT")
                   Else
                     .Precio = -1
                   End If
                   .Asignado = False
                   
                   .Name = MiRs("FA03OBSIMP") & ""
                   .Descripcion = MiRs("FA03OBSERV") & ""
                   
                   If Not IsNull(MiRs("FA03INDFACT")) Then
                       Select Case MiRs("FA03INDFACT")
                           Case 0 ' Pendiente de facturar
                               .Facturado = False
'                               .Permanente = False
                           Case 1 ' Introducci�n o modificaci�n manual
                               .Facturado = False
'                               .Permanente = True
                           Case 2 ' Facturado
                               .Facturado = True
'                               .Permanente = False
                           Case 3 ' Se ha compensado (Anulado) con otra factura
                               .Facturado = True
'                               .Permanente = False
                           Case 4 ' compensa otra factura
                               .Facturado = True
'                               .Permanente = False
                       End Select
                   Else
                       .Facturado = False
'                       .Permanente = False
                   End If
                   
                  If IsNull(MiRs("FA03INDPERMANENTE")) Then
                     .Permanente = False
                  Else
                     .Permanente = True
                  End If
                   
                   If Not IsNull(MiRs("FA03PERIODOGARA")) Then
                     .PeriodoGara = MiRs("FA03PERIODOGARA")
                     Asistencia.FechaFinGarantia = Format(DateAdd("d", MiRs("FA03PERIODOGARA"), MiRs("FA03FECHA")), "YYYYMMDD")
                   End If
                   
                   .CodProceso = MiRs("AD07CODPROCESO")
                   .CodAsistenci = MiRs("AD01CODASISTENCI")
                   
   '                .KeyREco = Asistencia.BuscarREco(.Fecha, .CodProceso, .CodAsistenci)
               End With
               
               ' Guardar las propiedades del rnf susceptibles de modificacion para poder recuperarlas
               ' despues
               NuevoRNF.Guardar
               ' A�adir el nuevo RNF a la asistencia
              .RNFs.Add NuevoRNF, NuevoRNF.CodCateg & "-" & Format(Cont, "000")
               NuevoRNF.Key = MiRs("FA05CODCATEG") & "-" & Format(Cont, "000")
               
               MiRs.MoveNext
               
               If CVDate(NuevoRNF.Fecha) < CVDate(Asistencia.FecInicio) Then
'                 Asistencia.FecInicio = Format(NuevoRNF.Fecha, "DD/MM/YYYY HH:MM:SS")
                 Asistencia.FecInicio = Format(NuevoRNF.Fecha, "DD/MM/YYYY 00:00:00")
               End If
               
               If CVDate(NuevoRNF.Fecha) >= CVDate(IIf(Asistencia.FecFin <> "", Format(Asistencia.FecFin, "DD/MM/YYYY 00:00:00"), "01/01/1900")) Then
'                 Asistencia.FecFin = Format(DateAdd("D", 1, CDate(NuevoRNF.Fecha)), "DD/MM/YYYY HH:MM:SS")
                 Asistencia.FecFin = Format(DateAdd("D", 1, CDate(NuevoRNF.Fecha)), "DD/MM/YYYY 00:00:00")
               End If
               
             End With
         Else
            MiRs.MoveNext

             
         End If
      Loop
      
      
      
      For Each Paciente In varFact.Pacientes
         ' ************************************************************************************************
         ' A�adir el paciente como REco en la Asistencia, si no estaba ya
         ' ponerlo por defecto
         ' ************************************************************************************************
         blnPacienteYaREco = False
         For Each reco In Asistencia.REcos
             If reco.Codigo = Paciente.CodPersona Then
                 blnPacienteYaREco = True
                 Exit For
             End If
         Next
         If Not blnPacienteYaREco Then
             Asistencia.AddConcierto 97, 328
                         
             Set reco = New Persona
             reco.Codigo = Paciente.CodPersona
                     reco.Concierto.Concierto = 97
                     reco.Concierto.NodoAFacturar = 328
'                     reco.Concierto.Name = "" & MiRsConc("FA09DESIG")
                     reco.CodTipEcon = "P"
                     reco.EntidadResponsable = "A"
                     reco.DesEntidad = "Privado A"
'                     reco.proceso = MiRs("AD07CODPROCESO")
'                     reco.Asistencia = MiRs("AD01CODASISTENCI")

             Asistencia.AddREco reco, Asistencia.FecInicio, Asistencia.FecFin
         End If
      
         Asistencia.DefaultREco = reco.Key
         
         ' ************************************************************************************************
         ' realizar la asignacion de REcos a los RNFs
         ' ************************************************************************************************
         For Each objRNF In Asistencia.RNFs
            objRNF.KeyREco = Asistencia.BuscarREco(objRNF.Fecha, objRNF.CodProceso, objRNF.CodAsistenci)
         Next objRNF
      Next Paciente
      
      
      MsgEstado "Calculando propuestas de factura"
      ' generar las propuestas de factura
      varFact.MostrarMensajes = True
      
      varFact.AsignarRNFs
      
      MsgEstado "Mostrando propuestas"
      
      ' lanzar el formulario de seleccion de propuestas de factura
      frm_Propuestas.pPropuestas varFact
      MsgEstado ""
      
    Else
    
      MsgBox "No hay ning�n registro que responda a las condiciones del filtro", vbOKOnly + vbInformation, "aviso"
      BotonSalir = True


   End If
   
   ' vaciar el objeto factura
   varFact.Destruir
   Set varFact = Nothing


Exit Sub
ErrorenRNF:
    Select Case Err.Number
    Case 13
        ' se ha producido un error en las fechas de los RNFs
        Resume Next
    Case 457
        Cont = Cont + 1
        Resume
    Case Else
'        MsgBox "Se ha producido un error en la inserci�n de los RNF", vbCritical + vbOKOnly, "Aviso"
        MsgBox "Error en el proceso de recogida de RNFs" & Chr(13) & "La factura no ser� correcta" & Chr(13) & Chr(13) & _
           Err.Number & " " & Err.Description, vbCritical + vbOKOnly, "ERROR"
        objApp.RollbackTrans
        Resume Next
    End Select
End Sub

Private Sub tvwProcesos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  On Error GoTo IsNothing
  
  tvwProcesos.SelectedItem = tvwProcesos.HitTest(X, Y)
  
   If Button = vbRightButton Then
'      lblXY.Left = x
'      lblXY.Top = y
      
'      Dragging = False
    
      If Left(tvwProcesos.SelectedItem.Tag, 1) = "F" Then
         mnuFactAnular.Enabled = True
         mnuFactReelaborar.Enabled = True
      
         If tvwProcesos.SelectedItem.Image = "Abonable" Then
            mnuFactMarcar.Caption = "Des&marcar"
         Else
            mnuFactMarcar.Caption = "&Marcar"
         End If
         PopupMenu mnuFact
      End If
   Else
   
   End If
   Exit Sub
  
IsNothing:
'  tvwProcesos.Drag 0

End Sub

Private Sub tvwProcesos_NodeClick(ByVal Node As ComctlLib.Node)
    
   If Left(Node.Tag, 1) = "F" Then
      cmdFinAsistencia.Enabled = False
      cmdNuevosRNFs.Enabled = False
       
      cmdAnular.Enabled = True
      cmdReelabora.Enabled = True
      
      If Node.Image = "Abonable" Then
          cmdMarcar.Caption = "Des&marcar"
      Else
          cmdMarcar.Caption = "&Marcar"
      End If
      
      
   Else
      If Left(tvwProcesos.SelectedItem.Key, 1) = "A" Then
         cmdFinAsistencia.Enabled = True
         cmdNuevosRNFs.Enabled = True
         
         cmdAnular.Enabled = False
         cmdReelabora.Enabled = False
         
         If Node.Image = "Facturable" Then
             cmdMarcar.Caption = "Des&marcar"
         Else
             cmdMarcar.Caption = "&Marcar"
         End If
      Else
         cmdFinAsistencia.Enabled = False
         cmdNuevosRNFs.Enabled = False
         cmdAnular.Enabled = False
         cmdReelabora.Enabled = False
      End If
   End If

End Sub

Private Sub Refrescar()
   Call Desbloquear
   If IdPersona2.Text <> "" Then
      Call Procesos(IdPersona2.Text)
   End If
   chkAplicarFiltro(0).Value = False
   chkAplicarFiltro(1).Value = vbChecked
   SDCFechaInicio.Date = ""
   SDCFechaFin.Date = ""
   
   ' Inicializar el numero de factura de la reelaboraci�n
   NoFactura = ""
End Sub

Private Sub MsgEstado(msg As String)
    Me.stbStatusBar1.Panels(1).Text = msg
End Sub

Private Function Bloquear(NumHistoria As String) As Boolean
'   Devuelve:
'        - true si se ha conseguido bloquear correctamente
'        - false si no se ha bloqueado
    On Error GoTo ErrorBloqueo
    
    qryBloqueo(1).rdoParameters(0) = NumHistoria
    qryBloqueo(1).rdoParameters(1) = objSecurity.strUser
    qryBloqueo(1).rdoParameters(2) = objSecurity.strMachine
    qryBloqueo(1).Execute
    
    Bloquear = True
    blnBloqueado = True
    
Exit Function
ErrorBloqueo:
    If Err.Number = 40002 Then
        ' la historia ya est� bloqueada, hay que revisar si lo ha bloqueado el mismo usuario
        Dim RS As rdoResultset
        
        qryBloqueo(3).rdoParameters(0) = NumHistoria
'
'        qryBloqueo(3).rdoParameters(1) = objSecurity.strMachine
        Set RS = qryBloqueo(3).OpenResultset
        
        If RS("SG02COD") = objSecurity.strUser And RS("FA34CPU") = objSecurity.strMachine Then
            ' lo habia bloqueado yo mismo, puedo continuar
            Bloquear = True
            blnBloqueado = True
        Else
            Bloquear = False
            blnBloqueado = False
            MsgBox "El usuario : " & RS("SG02COD") & Chr(13) & Chr(13) & _
                   "ya est� facturando este paciente" & Chr(13) & Chr(13) & _
                   "en la CPU : " & RS("FA34CPU"), vbInformation + vbOKOnly
        End If
        
    Else
        Bloquear = False
        blnBloqueado = False
        MsgBox Err.Number & " " & Err.Description, vbInformation + vbOKOnly
    End If
End Function

Private Function Desbloquear()

    On Error GoTo ErrorBloqueo
    
    If blnBloqueado Then
        qryBloqueo(2).rdoParameters(0) = objSecurity.strUser
        qryBloqueo(2).rdoParameters(1) = objSecurity.strMachine
        qryBloqueo(2).Execute
        
        blnBloqueado = False
        
    End If
    
Exit Function
ErrorBloqueo:
    
End Function

Private Function Icono(Asistencia As Node) As String
   Dim n As Integer
   On Error GoTo error
   Icono = "Asistencia"

   If Asistencia.Children > 0 Then ' There are children.
      n = Asistencia.Child.Index

      While n <= Asistencia.Child.LastSibling.Index
      
         If Left(tvwProcesos.Nodes(n).Tag, 1) = "F" Then
            Icono = "Factura"
            Exit Function
         End If
         
         ' Reset N to next sibling's index.
         n = tvwProcesos.Nodes(n).Next.Index
      Wend
   End If
Exit Function

error:
   If Err.Number = 91 Then Exit Function
End Function

Private Sub EvitarModif()
   If Not blnPermitirModifFact Then
      cmdNuevosRNFs.Visible = False
      cmdAnular.Visible = False
      cmdReelabora.Visible = False
   End If
End Sub
