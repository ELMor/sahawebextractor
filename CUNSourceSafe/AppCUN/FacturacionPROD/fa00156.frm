VERSION 5.00
Begin VB.Form frm_Observaciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Modificaciones de factura"
   ClientHeight    =   2655
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6495
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2655
   ScaleWidth      =   6495
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtObs 
      Height          =   2175
      Left            =   90
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   405
      Width           =   5055
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   420
      Left            =   5265
      TabIndex        =   2
      Top             =   630
      Width           =   1140
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   420
      Left            =   5265
      TabIndex        =   1
      Top             =   90
      Width           =   1140
   End
   Begin VB.Label Label1 
      Caption         =   "Observaciones :"
      Height          =   195
      Left            =   90
      TabIndex        =   3
      Top             =   135
      Width           =   5055
   End
End
Attribute VB_Name = "frm_Observaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim miObs As String
Dim miFact As String

Private Sub cmdAceptar_Click()
   miObs = txtObs.Text
   Grabar
   Unload Me
End Sub

Public Sub pObservaciones(nFact, obs)
   txtObs.Text = obs
   miObs = obs
   miFact = nFact
   
   Me.Show vbModal
   
   obs = miObs
End Sub

Private Sub cmdCancelar_Click()
   miObs = "-1"
   Unload Me
End Sub

Sub Grabar()
   Dim MiSql As String
   Dim qry As New rdoQuery
  
   'Asignar la nueva descripcion
   MiSql = "Update FA0400 set FA04OBSERV = ? Where FA04NUMFACT = ? AND FA04NUMFACREAL IS NULL"
  
   qry.SQL = MiSql
   Set qry.ActiveConnection = objApp.rdoConnect
   qry.Prepared = True
   
   qry.rdoParameters(0) = miObs
   qry.rdoParameters(1) = miFact
   
   qry.Execute

End Sub

