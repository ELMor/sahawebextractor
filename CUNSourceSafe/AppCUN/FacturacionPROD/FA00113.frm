VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frm_Desglose 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Desglose de la factura"
   ClientHeight    =   4575
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8055
   LinkTopic       =   "Form1"
   ScaleHeight     =   4575
   ScaleWidth      =   8055
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   330
      Left            =   6840
      TabIndex        =   2
      Top             =   4140
      Width           =   1095
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   330
      Left            =   5625
      TabIndex        =   1
      Top             =   4140
      Width           =   1095
   End
   Begin SSDataWidgets_B.SSDBGrid grdRNF 
      Height          =   3975
      Left            =   90
      TabIndex        =   0
      Top             =   45
      Width           =   7845
      ScrollBars      =   2
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   16
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   16
      Columns(0).Width=   1164
      Columns(0).Caption=   "Mostrar"
      Columns(0).Name =   "Mostrar"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   1270
      Columns(1).Caption=   "Facturar"
      Columns(1).Name =   "Facturar"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Style=   2
      Columns(2).Width=   900
      Columns(2).Caption=   "Cant."
      Columns(2).Name =   "Cantidad"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   4445
      Columns(3).Caption=   "Descripcion"
      Columns(3).Name =   "Descripcion"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   1799
      Columns(4).Caption=   "Precio"
      Columns(4).Name =   "Precio"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   1270
      Columns(5).Caption=   "Dto. %"
      Columns(5).Name =   "Descuento"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   2037
      Columns(6).Caption=   "Importe"
      Columns(6).Name =   "Importe"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "Nodo"
      Columns(7).Name =   "Nodo"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Categoria"
      Columns(8).Name =   "Categoria"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Ruta"
      Columns(9).Name =   "Ruta"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "Fecha"
      Columns(10).Name=   "Fecha"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "MostAnterior"
      Columns(11).Name=   "MostAnterior"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "FactAnterior"
      Columns(12).Name=   "FactAnterior"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "NodoAnterior"
      Columns(13).Name=   "NodoAnterior"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "SoloRNF"
      Columns(14).Name=   "SoloRNF"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "ClaveAgrup"
      Columns(15).Name=   "ClaveAgrup"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      _ExtentX        =   13838
      _ExtentY        =   7011
      _StockProps     =   79
      Caption         =   "RNF"
   End
End
Attribute VB_Name = "frm_Desglose"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private varFact As factura
Private paciente As paciente
Private Asistencia As Asistencia
Private rnf As rnf
Private PropuestaFact As PropuestaFactura
Private NodoFACT As Nodo
Private Nodos As Nodo
Private ConciertoAplicable As Concierto
Private Entidad As Nodo

Private Type typeRNF
  Clave As String
  grdRNF As String
  Mostrar As Integer
  Facturar As Integer
  Cantidad As Double
  Descripcion As String
  Precio As Long
  Descto As Double
  Importe As Long
  Nodo As Integer
  Categoria As Integer
  Ruta As String
  Fecha As String
  MostrarAnt As Integer
  FacturarAnt As Integer
  NodoAnterior As Integer
  SoloRNF As Integer
End Type

Dim ArrayRNF() As typeRNF
Dim ArrayGRID() As typeRNF

Dim PacDesg As String
Dim AsistDesg As String
Dim PropFacDesg As String
Dim NodoDesg As Integer
Dim RespEconom As String

Sub A�adirNodoFactura(Precio As Long, Descuento As Double, Cantidad As Integer, Franquicia As Boolean)
Dim NodosFACT As New Nodo
Dim NodoRNF As New Nodo
Dim RNFFact As New rnf
Dim intIndNodo As Integer
Dim intIndRNF As Integer
Dim ContRNFS As Integer
Dim intConta As Integer

    'Creamos un nuevo nodo y un rnf
    Set NodosFACT = New Nodo
    Set RNFFact = New rnf
    'Buscamos las propiedades de ese rnf y las copiamos al nuevo nodo
    Set NodoRNF = BuscarNodo(grdRNF.Columns(9).Value, grdRNF.Columns(10).Value)
    NodosFACT.Copiar NodoRNF
    'Al nodo que hemos creado le decimos que se muestre y en base al precio le indicamos si se factura o no.
    NodosFACT.LinOblig = True
    '*mod 21/12/1999
    If Precio = 0 Then
      NodosFACT.FactOblig = False 'Importe 0, no lo facturamos.
    Else
      NodosFACT.FactOblig = True  'Importe # 0, lo facturaremos.
    End If
     'Nos posiciconamos en el nodo de factura que corresponde al nodo a insertar
    Set NodoFACT = varFact.Pacientes(PacDesg).Asistencias(AsistDesg).PropuestasFactura(PropFacDesg).NodosFactura(NodoDesg)
    NodosFACT.LineaFact.Precio = Precio
    NodosFACT.LineaFact.PrecioNoDescontable = 0
    NodosFACT.LineaFact.Dto = Descuento
    NodosFACT.LineaFact.Cantidad = Cantidad
    NodosFACT.LineaFact.Importe = (Cantidad * Precio) - (((Cantidad * Precio) * Descuento) / 100)
    NodosFACT.EntidadResponsable = RespEconom
    NodosFACT.Editable = True
    'Si el precio es 0 directamente le indicamos que el descuento tambi�n es 0.
    If Precio = 0 Then
      NodosFACT.LineaFact.Dto = 0
    End If
    'Finalmente a�adimos la nueva l�nea de factura (nodo) y su rnf.
    NodoFACT.Nodos.Add NodosFACT
    'Una vez que tenemos ya el nodo creado movemos a el todos los rnfs que se corresponden con el
    'Primero calculamos el n�mero de rnfs que hay en el array de RNFs y lo recorremos para moverlos
    ContRNFS = UBound(ArrayRNF, 1)
    For intConta = 1 To ContRNFS
      'El rnf pertenece a la agrupaci�n con la que se corresponde la l�nea.
      If ArrayRNF(intConta).grdRNF = Me.grdRNF.Columns(15).Value Then
        'Nos posicioneamos en el ran  y copiamos los datos en el nuevo RNF
        intIndNodo = ArrayRNF(intConta).Nodo
        Set Nodos = varFact.Pacientes(PacDesg).Asistencias(AsistDesg).PropuestasFactura(PropFacDesg).NodosFactura(NodoDesg).Nodos(intIndNodo)
        intIndRNF = ArrayRNF(intConta).Categoria
        Set rnf = Nodos.RNFs(intIndRNF)
        RNFFact.Copiar rnf 'Copiado de los datos
        RNFFact.Ruta = ArrayRNF(intConta).Ruta 'Le indicamos la ruta, ya que no la copia por defecto
        RNFFact.NodoAnterior = ArrayRNF(intConta).Nodo 'Le indicamos el nodo donde estaba posicionado.
        'Lo a�adiremos al nuevo nodo
        NodosFACT.RNFs.Add RNFFact
      End If
    Next
    'Eliminamos el rnf.
    For intConta = ContRNFS To 1 Step -1 'Se hace empezando del final para respetar el indice de los nodos y las categor�as.
      If ArrayRNF(intConta).grdRNF = Me.grdRNF.Columns(15).Value Then
        intIndNodo = ArrayRNF(intConta).Nodo
        Set Nodos = varFact.Pacientes(PacDesg).Asistencias(AsistDesg).PropuestasFactura(PropFacDesg).NodosFactura(NodoDesg).Nodos(intIndNodo)
        intIndRNF = ArrayRNF(intConta).Categoria
        Nodos.RNFs.Remove (intIndRNF)
      End If
    Next
    'En el caso de que no haya m�s rnfs debajo le decimos que no facture el nodo.
     If Nodos.RNFs.Count = 0 Then
       Nodos.LinOblig = False
       Nodos.FactOblig = False
     End If
    
    

End Sub


Function BuscarNodo(Ruta As String, FecRnf As String) As Nodo
Dim strRestante As String
Dim intPunto As Integer
Dim intBarra As Integer
Dim blnNodos As Boolean
Dim Categoria As String
Dim Entidad As Boolean
Dim CadenaBusqueda As Variant
Dim NodoAtrib As Nodo

  If Ruta <> "" Then
    'Inicializamos la cadena de b�squeda y el string que analizaremos
    CadenaBusqueda = ""
    strRestante = Ruta
    blnNodos = True
    Entidad = True
    'Mientras quede una parte del string a analizar...
    While strRestante <> ""
      'Analizamos la parte correspondiente a los nodos...
      While blnNodos = True
        If Left(strRestante, 1) = "." Then
          strRestante = Right(strRestante, Len(strRestante) - 1)
        End If
        'Buscamos la posici�n del punto
        intPunto = InStr(1, strRestante, ".", 1)
        If intPunto <> 0 Then
          Categoria = Left(strRestante, intPunto - 1)
          If Entidad = True Then
            'Al ser la primera se trata de la entidad
            Set NodoAtrib = varFact.ConciertosAplicables(Trim(str(Categoria))).Entidad
            Entidad = False
          Else
            'Buscamos la clave correcta, indic�ndole que la busque en nodos
            Categoria = BuscarClave(NodoAtrib, Categoria, FecRnf, "N")
            Set NodoAtrib = NodoAtrib.Nodos(CStr(Categoria))
          End If
          strRestante = Right(strRestante, Len(strRestante) - intPunto)
        End If
        If Mid(strRestante, 1, 1) = "/" Then
          'Encontramos la barra que separa los nodos de las categor�as y la eliminamos. Ahora son cat.
          blnNodos = False
          strRestante = Right(strRestante, Len(strRestante) - 1)
        End If
        If strRestante = "" Then
          blnNodos = False
        End If
      Wend
      'Pasamos a analizar la parte correspondiente a las categor�as
      If Left(strRestante, 1) = "." Then
        strRestante = Right(strRestante, Len(strRestante) - 1)
      End If
      intPunto = InStr(1, strRestante, ".", 1)
      If intPunto <> 0 Then
        'Encontramos el punto
        Categoria = Left(strRestante, intPunto - 1)
        'Buscamos la clave correcta, indic�ndole que la busque en categor�as
        If Trim(Categoria) <> "" Then
          Categoria = BuscarClave(NodoAtrib, Categoria, FecRnf, "C")
          Set NodoAtrib = NodoAtrib.Categorias(CStr(Categoria))
          strRestante = Right(strRestante, Len(strRestante) - intPunto)
        End If
      End If
    Wend
  End If
  Set BuscarNodo = NodoAtrib
End Function

' Funci�n para buscar la clave correspondiente a un determinado nodo o categor�a dentro del
' concierto aplicado
' Ruta -> Ruta v�lida para el nodo o cat anterior
'         Ej: ConciertosAplicables(74).Entidad.Nodos(254-000).Nodos(241-003).Categorias(2410-000)
' C�digo -> Clave del nodo o categor�a a buscar.
' Fecha -> Fecha del RNF
' Tipo -> Nos indica si es un (N)odo o una (C)ategor�a.
Function BuscarClave(nodoAnt As Nodo, Codigo As String, Fecha As String, Tipo As String) As String
Dim Nodo As Nodo
Dim blnSeguir As Boolean
Dim IntCont As Integer
Dim Clave As String

  On Error GoTo ErrorEnBuscarClave

  'Inicializamos las variablex que utilzaremos para...
  blnSeguir = True    'Continuar la b�squeda
  IntCont = 0         'Contador de nodos mirados
  
  'Nos posicionamos en el nodo o categor�a padre.
  Set Nodo = nodoAnt
  
  'Mientras no encontremos la fecha...
  While blnSeguir
    Clave = Codigo & "-" & Format(IntCont, "000")
    If Tipo = "N" Then
      'Si es nodo buscamos dentro de la colecci�n de los nodos
      Set Nodo = Nodo.Nodos(Clave)
    ElseIf Tipo = "C" Then
      'Si es categor�a buscamos dentro de la colecci�n de categor�as
      Set Nodo = Nodo.Categorias(Clave)
    End If
    If Format(Fecha, "yyyymmdd") >= Format(Nodo.FecInicio, "yyyymmdd") _
              And Format(Fecha, "yyyymmdd") < Format(Nodo.FecFin, "yyyymmdd") Then
      'La fecha est� entre ellas.
      BuscarClave = Clave
      blnSeguir = False
    End If
    IntCont = IntCont + 1
  Wend
  
Exit Function
ErrorEnBuscarClave:
  'No se ha encontrado la key
  If Err = 5 Then
    Clave = ""
  End If
End Function
Private Sub CargarRNFs()
Dim strTexto As String
Dim lngPrecio As Long
Dim lngSuma As Long
Dim Atributos As Nodo
Dim IntContNodo As Integer
Dim intContCat As Integer
Dim intLineaOblig As Integer
Dim intFactOblig As Integer
Dim intSoloUno As Integer
Dim Importe As Long
Dim intContador As Integer
Dim CatAnterior As Long
Dim PrDiaAnterior As Long
Dim PrecioAnterior As Long
Dim FechaAnterior As String
Dim Cantidad As Integer
Dim Precio As Long

  'Iniciamos a 0 los contadores tanto de suma como de indicador de nodo y de categor�a
  lngSuma = 0
  IntContNodo = 1
  intContCat = 1
  
  'Nos posicionamos en el paciente, la asistencia y la propuesta de factura seleccionadas.
  Set paciente = varFact.Pacientes(PacDesg)
  Set Asistencia = paciente.Asistencias(AsistDesg)
  Set PropuestaFact = Asistencia.PropuestasFactura(PropFacDesg)
  'Analizamos cada uno de los nodos de factura e iniciamos el contador de rnfs a 1
  For Each NodoFACT In PropuestaFact.NodosFactura
    intContador = 1
    'Analizamos cada uno de los nodos que se encuentran dentro de los nodos de factura
    For Each Nodos In NodoFACT.Nodos
      'Comprobamos si la l�ea de factura es editable.
      If Nodos.Editable = True Then
        'Inicializamos el contador de categor�as del nodo.
        intContCat = 1
        'Analizamos los RNFs que se encuentran dentro de cada nodo
        For Each rnf In Nodos.RNFs
          'Redimensionamos el array de tipo ArrayRNF que contiene todos los RNFs sin agrupar.
          ReDim Preserve ArrayRNF(1 To intContador)
          'Si tiene una ruta, buscamos dentro de la base de datos los atributos de ese nodo.
          If rnf.Ruta <> "" Then
            Set Atributos = BuscarNodo(rnf.Ruta, rnf.Fecha)
          End If
          'Depende de si s�lo tiene un rnf o m�s miraremos la configuraci�n de facturar y mostrar
  '      If Nodos.RNFs.Count = 1 Then
          If rnf.NodoAnterior <> 0 Then
            If Nodos.LinOblig = True Then
              intLineaOblig = -1
            Else
              intLineaOblig = 0
            End If
            If Nodos.FactOblig = True Then
              intFactOblig = -1
            Else
              intFactOblig = 0
            End If
          Else
            If Atributos.LinOblig = True Then
              intLineaOblig = -1
            Else
              intLineaOblig = 0
            End If
            If Atributos.FactOblig = True Then
              intFactOblig = -1
            Else
              intFactOblig = 0
            End If
          End If
          
          'Si es un nodo que se va a facturar forzamos el que se muestre
          If intFactOblig = True Then
            If intLineaOblig = False Then
              Nodos.LinOblig = True
              intLineaOblig = -1
            End If
          End If
          
          'Calculamos el importe del rnf
          'Importe = (Atributos.LineaFact.Precio + Atributos.LineaFact.PrecioNoDescontable) * Atributos.LineaFact.Cantidad
          'Importe = Importe - ((Importe * Atributos.LineaFact.Dto) / 100)
          If Atributos.PrecioDia <> -1 Then
            Importe = (Atributos.PrecioDia) - ((Atributos.PrecioDia * Atributos.Descuento) / 100)
            Precio = Atributos.PrecioDia
          ElseIf Atributos.PrecioRef <> -1 Then
            Importe = (Atributos.PrecioRef * rnf.Cantidad)
            Importe = Importe - ((Importe * Atributos.Descuento) / 100)
            Precio = Atributos.PrecioRef
          Else
            Precio = 0
            Importe = 0
          End If
          'Introducimos el rnf dentro del tipo que contiene los rnfs sin agrupar
          ArrayRNF(intContador).Clave = rnf.CodCateg
          ArrayRNF(intContador).Mostrar = intLineaOblig
          ArrayRNF(intContador).Facturar = intFactOblig
          ArrayRNF(intContador).Cantidad = rnf.Cantidad
          ArrayRNF(intContador).Descripcion = Atributos.Name
          ArrayRNF(intContador).Precio = Precio
          ArrayRNF(intContador).Descto = Atributos.Descuento
          ArrayRNF(intContador).Importe = Importe
          ArrayRNF(intContador).Nodo = IntContNodo
          ArrayRNF(intContador).Categoria = intContCat
          ArrayRNF(intContador).Ruta = rnf.Ruta
          ArrayRNF(intContador).Fecha = rnf.Fecha
          ArrayRNF(intContador).MostrarAnt = intLineaOblig
          ArrayRNF(intContador).FacturarAnt = intFactOblig
          ArrayRNF(intContador).NodoAnterior = rnf.NodoAnterior
          ArrayRNF(intContador).SoloRNF = 0
          ArrayRNF(intContador).grdRNF = ComprobarAgrupacion(ArrayRNF(), intContador, True)
          intContCat = intContCat + 1
          intContador = intContador + 1
        Next
      End If
      IntContNodo = IntContNodo + 1
    Next
  Next
  Cantidad = UBound(ArrayGRID)
  For intContador = 1 To Cantidad
      strTexto = ArrayGRID(intContador).Mostrar _
                  & Chr$(9) & ArrayGRID(intContador).Facturar _
                  & Chr$(9) & ArrayGRID(intContador).Cantidad _
                  & Chr$(9) & ArrayGRID(intContador).Descripcion _
                  & Chr$(9) & ArrayGRID(intContador).Precio _
                  & Chr$(9) & ArrayGRID(intContador).Descto _
                  & Chr$(9) & ArrayGRID(intContador).Importe _
                  & Chr$(9) & ArrayGRID(intContador).Nodo _
                  & Chr$(9) & ArrayGRID(intContador).Categoria _
                  & Chr$(9) & ArrayGRID(intContador).Ruta _
                  & Chr$(9) & ArrayGRID(intContador).Fecha _
                  & Chr$(9) & ArrayGRID(intContador).MostrarAnt _
                  & Chr$(9) & ArrayGRID(intContador).FacturarAnt _
                  & Chr$(9) & ArrayGRID(intContador).NodoAnterior _
                  & Chr$(9) & ArrayGRID(intContador).SoloRNF _
                  & Chr$(9) & ArrayGRID(intContador).grdRNF
      Me.grdRNF.AddItem strTexto
  Next
End Sub
' Mediante la funci�n ComprobarAgrupaci�n, comprobaremos, si hemos de agrupar el rnf con otros iguales,
' o por el contrario, si es distinto a los ya existentes, y
Private Function ComprobarAgrupacion(ArrayRNF() As typeRNF, Posicion As Integer, Diario As Boolean) As String
Dim ContadorGrid As Integer
Dim ElementosGrid As Integer
Dim A�adir As Boolean
Dim Nuevo As Integer

  A�adir = True
  ElementosGrid = UBound(ArrayGRID, 1)
  If ElementosGrid <> 0 Then
    For ContadorGrid = 1 To ElementosGrid
      If ArrayGRID(ContadorGrid).Clave = ArrayRNF(Posicion).Clave Then
        If Diario = True Then
          'Tiene Precio/d�a y la fecha es la misma, con lo cual no se a�aden, ya que s�lo se factura 1.
          If ArrayGRID(ContadorGrid).Fecha = ArrayRNF(Posicion).Fecha Then
            A�adir = False
            ComprobarAgrupacion = ArrayGRID(ContadorGrid).grdRNF
            Exit For
          End If
        ElseIf Diario = False Then
          'El precio es normal, con lo cual a igual categor�a sumamos las cantidades.
          If ArrayGRID(ContadorGrid).Precio = ArrayRNF(Posicion).Precio Then
            If ArrayGRID(ContadorGrid).Descto = ArrayRNF(Posicion).Descto Then
              'A�adimos las cantidades
              ArrayGRID(ContadorGrid).Cantidad = ArrayGRID(ContadorGrid).Cantidad + ArrayRNF(Posicion).Cantidad
              ArrayGRID(ContadorGrid).Importe = ArrayGRID(ContadorGrid).Importe + ArrayRNF(Posicion).Importe
              A�adir = False
              ComprobarAgrupacion = ArrayGRID(ContadorGrid).grdRNF
              Exit For
            End If
          End If
        End If
      End If
    Next
  End If
  If A�adir Then
    Nuevo = ElementosGrid + 1
    If Nuevo = 1 Then
      ReDim ArrayGRID(1 To 1)
    Else
      ReDim Preserve ArrayGRID(1 To Nuevo)
    End If
    ArrayGRID(Nuevo).grdRNF = "grd" & str(Nuevo)
    ArrayGRID(Nuevo).Clave = ArrayRNF(Posicion).Clave
    ArrayGRID(Nuevo).Mostrar = ArrayRNF(Posicion).Mostrar
    ArrayGRID(Nuevo).Facturar = ArrayRNF(Posicion).Facturar
    ArrayGRID(Nuevo).Cantidad = ArrayRNF(Posicion).Cantidad
    ArrayGRID(Nuevo).Descripcion = ArrayRNF(Posicion).Descripcion
    ArrayGRID(Nuevo).Precio = ArrayRNF(Posicion).Precio
    ArrayGRID(Nuevo).Descto = ArrayRNF(Posicion).Descto
    ArrayGRID(Nuevo).Importe = ArrayRNF(Posicion).Importe
    ArrayGRID(Nuevo).Nodo = ArrayRNF(Posicion).Nodo
    ArrayGRID(Nuevo).Categoria = ArrayRNF(Posicion).Categoria
    ArrayGRID(Nuevo).Ruta = ArrayRNF(Posicion).Ruta
    ArrayGRID(Nuevo).Fecha = ArrayRNF(Posicion).Fecha
    ArrayGRID(Nuevo).MostrarAnt = ArrayRNF(Posicion).MostrarAnt
    ArrayGRID(Nuevo).FacturarAnt = ArrayRNF(Posicion).FacturarAnt
    ArrayGRID(Nuevo).NodoAnterior = ArrayRNF(Posicion).NodoAnterior
    ArrayGRID(Nuevo).SoloRNF = ArrayRNF(Posicion).SoloRNF
    ComprobarAgrupacion = ArrayGRID(Nuevo).grdRNF
  End If
End Function
Sub ModificarPrecioNodo(Precio As Long, Franquicia As Boolean)
Dim intIndNodo As Integer
Dim intIndRNF As Integer
    
    intIndNodo = grdRNF.Columns(7).Value
    Set Nodos = varFact.Pacientes(PacDesg).Asistencias(AsistDesg).PropuestasFactura(PropFacDesg).NodosFactura(1).Nodos(intIndNodo)
'    Nodos.LinOblig = True
'    If Precio = 0 Then
'      Nodos.FactOblig = False 'Importe 0, no lo facturamos.
'    Else
'      Nodos.FactOblig = True  'Importe # 0, lo facturaremos.
'    End If
    Nodos.LineaFact.Precio = Precio
    Nodos.LineaFact.PrecioNoDescontable = 0
    'Si el precio es 0 directamente le indicamos que el descuento tambi�n es 0.
'    If Precio = 0 Then
'      Nodos.LineaFact.Dto = 0
'    End If

End Sub
Sub QuitarNodoFactura(Franquicia As Boolean)
Dim NodosFACT As New Nodo
Dim NodoRNF As Nodo
Dim RNFFact As New rnf
Dim intIndNodo As Integer
Dim intIndRNF As Integer
Dim intIndNodoAnt As Integer
Dim ContRNFS As Integer
Dim IntCont As Integer

    ContRNFS = UBound(ArrayRNF)
    For IntCont = ContRNFS To 1 Step -1
      If ArrayRNF(IntCont).grdRNF = grdRNF.Columns(15).Value Then
        If ArrayRNF(IntCont).NodoAnterior <> 0 Then
          'Nos posicionamos en el rnf que vamos a trasladar.
          intIndNodo = ArrayRNF(IntCont).Nodo
          intIndRNF = ArrayRNF(IntCont).Categoria
          Set rnf = varFact.Pacientes(PacDesg).Asistencias(AsistDesg).PropuestasFactura(PropFacDesg).NodosFactura(NodoDesg).Nodos(intIndNodo).RNFs(intIndRNF)
          'Creamos un nuevo rnf
          Set RNFFact = New rnf
          'Copiamos los datos en el nuevo rnf
          RNFFact.Copiar rnf  'Copiado de los datos
          RNFFact.Ruta = ArrayRNF(IntCont).Ruta 'Le indicamos la ruta que no la copia por defecto
          'Eliminamos el rnf.
          Set NodoRNF = varFact.Pacientes(PacDesg).Asistencias(AsistDesg).PropuestasFactura(PropFacDesg).NodosFactura(NodoDesg).Nodos(intIndNodo)
          NodoRNF.RNFs.Remove (intIndRNF)
          If NodoRNF.RNFs.Count = 0 Then
            varFact.Pacientes(PacDesg).Asistencias(AsistDesg).PropuestasFactura(PropFacDesg).NodosFactura(NodoDesg).Nodos.Remove (intIndNodo)
          End If
          'Nos posicionamos en el nodo anterior al que pertenec�a el rnf.
          intIndNodoAnt = ArrayRNF(IntCont).NodoAnterior
          Set NodoRNF = varFact.Pacientes(PacDesg).Asistencias(AsistDesg).PropuestasFactura(PropFacDesg).NodosFactura(NodoDesg).Nodos(intIndNodoAnt)
          'A�adimos al nodo el rnf que devolvemos a su sitio.
          NodoRNF.RNFs.Add RNFFact
          'Le indicamos que vuelva a dejar el nodo como l�nea obligatoria y facturable.
          NodoRNF.LinOblig = True
          NodoRNF.FactOblig = True
        End If
      End If
    Next
End Sub
Private Sub cmdAceptar_Click()
Dim x As Integer
      
      '+-------+----------+-------------------------------------------+
      '| M Foc | MAn FAn  | RESULTADO                                 |
      '+-------+----------+-------------------------------------------+
      '|  0   1  |  11  12  |  Columna que ocupan dentro del grid.      |
      '|--------+----------+-------------------------------------------+
      '|  0   0  |   0   0  |  No hay cambios *                         |
      '|  0   0  |   0   1  |  Facturar desactivado.                    |
      '|  0   0  |   1   0  |  Mostrar desactivado.                     |
      '|  0   0  |   1   1  |  Mostrar y facturar desactivados.         |
      '|  0   1  |   0   0  |  Facturar activado                        |
      '|  0   1  |   0   1  |  No hay cambios *                         |
      '|  0   1  |   1   0  |  Facturar activado, mostar desactivado    |
      '|  0   1  |   1   1  |  Mostar desactivado                       |
      '|  1   0  |   0   0  |  Mostar activado                          |
      '|  1   0  |   0   1  |  Mostar activado, facturar desactivado    |
      '|  1   0  |   1   0  |  No hay cambios *                         |
      '|  1   0  |   1   1  |  Facturar desactivado                     |
      '|  1   1  |   0   0  |  Mostrar y facturar activados.            |
      '|  1   1  |   0   1  |  Mostrar activado                         |
      '|  1   1  |   1   0  |  Facturar activado                        |
      '|  1   1  |   1   1  |  No hay cambios *                         |
      '+---------+----------+-------------------------------------------+
  
  'Nos posicionamos en la �ltima fila para examinarlas de la �ltima a la primera.
  grdRNF.MoveLast
  
  For x = grdRNF.Rows - 1 To 0 Step -1
    Select Case grdRNF.Columns(0).Value
      Case 0  'Mostrar Desactivado
        Select Case grdRNF.Columns(1).Value
          Case 0  'Mostrar Desactivado, Facturar Desactivado.
            If grdRNF.Columns(11).Value = 0 And grdRNF.Columns(12).Value = -1 Then
              'Desactivado el checkbox de facturar.
              'Eliminar el nodo de factura y su rnf. Ponemos el rnf en el nodo anterior(1)
              Call Me.QuitarNodoFactura(False)
              'COMPROBAR FRANQUICIAS CON NUEVOS RNFS.
            ElseIf grdRNF.Columns(11).Value = -1 And grdRNF.Columns(12).Value = 0 Then
              'Desactivado el checkbox de mostrar.
              'Eliminamos el nodo de factura con importe 0 (no tiene rnfs)(2)
              Call Me.QuitarNodoFactura(False)
            ElseIf grdRNF.Columns(11).Value = -1 And grdRNF.Columns(12).Value = -1 Then
              'Desactivados los checkbox de mostrar y facturar.
              'Eliminamos el nodo de factura y su rnf. Ponemos el rnf en el nodo anterior(1)
              Call Me.QuitarNodoFactura(False)
              'COMPROBAR FRANQUICIAS CON NUEVOS RNFS.
            End If
          Case -1 'Mostrar Desactivado, Facturar Activado.
            If grdRNF.Columns(11).Value = 0 And grdRNF.Columns(12).Value = 0 Then
              'Activado el checkbox de facturar.
              'Agregamos en nodo de factura y su rnf. Eliminamos el rnf del nodo anterior(3)
              Call Me.A�adirNodoFactura(grdRNF.Columns(6).Value, grdRNF.Columns(5).Value, grdRNF.Columns(2).Value, False)
              'COMPROBAR LA FRANQUICIA SIN ESE RNF. PUEDE DESAPARECER LA L�NEA DE FRANQUICIA.
            ElseIf grdRNF.Columns(11).Value = -1 And grdRNF.Columns(12).Value = 0 Then
            'Activado el checkbox de mostrar, desactivado el de facturar
            'Eliminamos el nodo de factura y rnf a nodo anterior. A�adimos el nodo con valor 0(4)
             Call Me.ModificarPrecioNodo(0, False)
              'COMPROBAMOS FRANQUICIAS CON NUEVOS RNF. PUEDE APARECER LA L�NEA DE FRANQUICIA.
            End If
          End Select
      Case -1 'Mostrar Activado
        Select Case grdRNF.Columns(1).Value
          Case 0  'Mostrar Activado, Facturar Desactivado.
            If grdRNF.Columns(11).Value = 0 And grdRNF.Columns(12).Value = 0 Then
              'Activamos el checkbox de mostar.
              'A�adimos un nodo de factura con importe 0(5)
              Call Me.A�adirNodoFactura(0, 0, 0, False)
            ElseIf grdRNF.Columns(11).Value = 0 And grdRNF.Columns(12).Value = -1 Then
              'Deactivamos el checkbox de facturar, activamos el de mostar.
              'Eliminamos nodo de facutura, rnf a nodo anterior a�adimos nodo factura importe 0(4)
              Call Me.ModificarPrecioNodo(0, False)
              'COMPROBAR LA FRANQUICIA SIN ESE RNF. PUEDE DESAPARECER LA L�NEA DE FRANQUICIA.
            ElseIf grdRNF.Columns(11).Value = -1 And grdRNF.Columns(12).Value = -1 Then
              'Desactivamos el checkbox de facturar.
              'Eliminamos nodo de factura, rnf a nodo anterior, a�adimos nodo factura importe 0(4)
              Call Me.ModificarPrecioNodo(0, False)
              'COMPROBAR LA FRANQUICIA SIN ESE RNF. PUEDE DESAPARECER LA L�NEA DE FRANQUICIA.
            End If
          Case -1 'Mostrar Activado, Facturar Activado.
            If grdRNF.Columns(11).Value = 0 And grdRNF.Columns(12).Value = 0 Then
              'Activamos los checkbox de facturar y mostar.
              'A�adimos nodo de factura y su rnf.(3)
              Call Me.A�adirNodoFactura(grdRNF.Columns(6).Value, grdRNF.Columns(5).Value, grdRNF.Columns(2).Value, False)
            ElseIf grdRNF.Columns(11).Value = -1 And grdRNF.Columns(12).Value = 0 Then
              'Activamos el checkbox de facturar sobre una l�nea ya mostrada.
              'Eliminamos nodo factura con importe 0 y a�adimos nodo factura y su rnf.(6)
              Call Me.ModificarPrecioNodo(grdRNF.Columns(6).Value, False)
              'COMPROBAR LA FRANQUICIA SIN ESE RNF. PUEDE DESAPARECER LA L�NEA DE FRANQUICIA.
            End If
          End Select
    End Select
    'Nos posicionamos en la l�nea anterior
    grdRNF.MovePrevious
  Next
  Unload Me
  
End Sub
Private Sub cmdCancelar_Click()
  Unload Me
End Sub
Private Sub Form_Load()
  ReDim ArrayGRID(0 To 0)
  Call CargarRNFs
End Sub
Public Function pDesglose(objFactura As factura, paciente As String, Asistencia As String, PropuestaFact As String, NodoFactura As String, Responsable As String) As factura
    Set varFact = objFactura
    PacDesg = paciente
    AsistDesg = Asistencia
    PropFacDesg = PropuestaFact
    NodoDesg = CInt(NodoFactura)
    RespEconom = Responsable
    Load frm_Desglose
    'Set frmSeleccion.varFact = objFactura
      frm_Desglose.Show (vbModal)
            
    Set objFactura = varFact
    Unload frm_Desglose
End Function
Private Sub grdRNF_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim Cantidad As Long
Dim Precio As Long
Dim Dto As Integer

  If ColIndex = 5 Then
    If grdRNF.Columns(5).Value > 100 Then
      MsgBox "El % de descuento no puede ser superior a 100", vbInformation + vbOKOnly, "Aviso"
      Exit Sub
    End If
  End If
  If ColIndex = 2 Or ColIndex = 4 Or ColIndex = 5 Then
    Cantidad = grdRNF.Columns(2).Value
    Precio = grdRNF.Columns(4).Value
    Dto = grdRNF.Columns(5).Value
    If grdRNF.Columns(2).Text <> "" And grdRNF.Columns(4).Text <> "" Then
      If grdRNF.Columns(5).Text <> "" Then
        grdRNF.Columns(6).Text = (Cantidad * Precio) - ((Cantidad * Precio * Dto) / 100)
      Else
        grdRNF.Columns(6).Text = Cantidad * Precio
      End If
    End If
  End If
End Sub

Private Sub grdRNF_KeyPress(KeyAscii As Integer)
  If grdRNF.Col = 2 Or grdRNF.Col = 4 Then
    KeyAscii = fValidarEnteros(KeyAscii)
  ElseIf grdRNF.Col = 5 Then
    KeyAscii = fValidarDecimales(KeyAscii, grdRNF.Columns(5).Value)
  End If
End Sub


