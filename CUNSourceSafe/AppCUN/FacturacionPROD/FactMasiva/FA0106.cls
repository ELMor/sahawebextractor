VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "RNF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public CodCateg As Long
Public CodRNF As String
Public Cantidad As Double
Public Fecha As String
Public Asignado As Boolean
Public Ruta As String
Public Key As String
Public Precio As Double
Public Descripcion As String
Public Desglose As Boolean
Public CodAtrib As Long
Public Name As String
Public Facturado As Boolean
'************************************************************
' IMPORTANTE :
'
' Cuando se a�adan nuevas propiedades hay que tener en cuenta
' que hay que incluirlas en la rutina de copiar los RNF.
'
'      Public Sub Copiar(RNFOrigen As rnf)
'
'************************************************************

' Indice del nodo anterior en la coleccion (Desglose de factura)
Public NodoAnterior As Long
Public NodoFACT As Long

Public Sub Copiar(RNFOrigen As RNF)
    With RNFOrigen
        CodCateg = .CodCateg
        CodRNF = .CodRNF
        Cantidad = .Cantidad
        Fecha = .Fecha
        Asignado = True
        Ruta = ""
        Key = .Key
        Precio = .Precio
        Descripcion = .Descripcion
        Desglose = .Desglose
        CodAtrib = 0
        Name = .Name
        Facturado = .Facturado
    End With
End Sub

