VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Concierto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Asistencia"
Private qryNodos As New rdoQuery
Private qryCateg As New rdoQuery
Private qryPrecioCant As New rdoQuery

' Fecha de inicio y final del concierto,
' dependen del primer y �ltimo RNF's a asignar al concierto
Public FecInicio As String
Public FecFin As String

Public Entidad As New Nodo

Public Property Get Codigo() As Long
'se usa cuando se asigna un valor a una propiedad, en el lado derecho de la asignaci�n.
'Syntax: Debug.Print X.CodCateg
    Codigo = Entidad.Codigo
End Property

'Inicializacion de los rdoQuery
Private Sub InitQRY(CodAsistencia As String)

'   Parametros:
'            0 - Ruta
'            1 - Fecha Inicio
'            2 - Fecha Inicio
'            3 - Fecha Final
'            4 - Fecha Final
'            5 - Fecha Inicio
'            6 - Fecha Final
    
    
    qryNodos.SQL = "SELECT  FA15RUTA, translate(replace(fa15ruta,'/.',''),'./1234567890','a') puntos , FA15CODFIN CODIGO, " & _
                         "  FA15ESCAT EsCategoria, " & _
                         "  FA0900.FA09DESIG, '' FA05DESIG, " & _
                         "  FA15FECINICIO, FA15FECFIN, FA15PRECIOREF, " & _
                         "  FA15PRECIODIA, FA15INDFRANQSUMA, FA15INDFRANQUNI, FA15PERIODOGARA, " & _
                         "  FA15DESCUENTO, " & _
                         "  FA15INDNECESARIO, FA15INDSUFICIENTE, FA15INDEXCLUIDO, " & _
                         "  FA15INDDESCONT, FA15INDLINOBLIG, FA1500.FA15CODATRIB," & _
                         "  FA15INDFACTOBLIG, FA15INDOPCIONAL, FA1500.CI13CODENTIDAD, FA09INDFACTURABLE, " & _
                         "  FA15INDDESGLOSE, FA15INDSUPLEMENTO, FA15DESCRIPCION, FA15ORDIMP, " & _
                         "  FA15RUTAREL, FA15RELFIJO, FA15RELPOR, " & _
                         "  COUNT(FA0700.FA15CODATRIB) PrecioCantidad, FA15INDTramAcum, FA15INDTramDias, FA15PlazoInter, " & _
                         "  FA15INDNecRnfOrigen, FA15INDNecRnfDestino, FA15INDrecogerCant, FA15INDrecogerValor, FA1500.CI32CODTIPECON "
'                         "  COUNT(FA0700.FA15CODATRIB) PrecioCantidad, FA15INDTramAcum, FA15INDTramDias, FA15PlazoInter, FA15CODCONC "
                         
    qryNodos.SQL = qryNodos.SQL & _
                    "FROM FA1500, FA0900, FA0700 " & _
                    "WHERE FA0900.FA09CODNODOCONC (+) = FA15CODFIN " & _
                        "AND FA0700.FA15CODATRIB (+)= FA1500.FA15CODATRIB " & _
                        "AND FA15INDEXCLUIDO = 0 " & _
                        "AND FA15CODCONC = ? AND FA15ESCAT = 0 " & _
                        "AND (( FA15FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
                        "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN ) OR " & _
                        "( FA15FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
                        "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN )  OR " & _
                        "( TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') <= FA15FECINICIO AND " & _
                        "FA15FECFIN <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')))   "
                        
    qryNodos.SQL = qryNodos.SQL & _
                    "GROUP BY FA15RUTA, translate(replace(fa15ruta,'/.',''),'./1234567890','a') , FA15CODFIN , " & _
                    "         FA15ESCAT, " & _
                    "         FA0900.FA09DESIG, '', FA15FECINICIO, FA15FECFIN, FA15PRECIOREF, " & _
                    "        FA15PRECIODIA,FA15INDFRANQSUMA, FA15INDFRANQUNI,FA15PERIODOGARA, " & _
                    "        FA15DESCUENTO, " & _
                    "        FA15INDNECESARIO, FA15INDSUFICIENTE,FA15INDEXCLUIDO, " & _
                    "        FA15INDDESCONT,FA15INDLINOBLIG, FA1500.FA15CODATRIB, " & _
                    "        FA15INDFACTOBLIG , FA15INDOPCIONAL, FA1500.CI13CODENTIDAD, FA09INDFACTURABLE, " & _
                    "        FA15INDDESGLOSE, FA15INDSUPLEMENTO, FA15DESCRIPCION, FA15ORDIMP, " & _
                    "        FA15RUTAREL, FA15RELFIJO, FA15RELPOR, FA15INDTramAcum, FA15INDTramDias, FA15PlazoInter, " & _
                    "        FA15INDNecRnfOrigen, FA15INDNecRnfDestino, FA15INDrecogerCant, FA15INDrecogerValor, FA1500.CI32CODTIPECON " & _
                    "ORDER BY 2 ASC, FA15INDSUFICIENTE DESC, FA15INDNECESARIO DESC, FA09INDFACTURABLE ASC, FA15FECINICIO"

    qryCateg.SQL = "SELECT  FA15RUTA, translate(replace(fa15ruta,'/.',''),'./1234567890','a') puntos , FA15CODFIN CODIGO, " & _
                    "       FA15ESCAT EsCategoria, " & _
                    "       FA0500.FA05DESIG, '' FA09DESIG, " & _
                    "       FA15FECINICIO, FA15FECFIN, FA15PRECIOREF, " & _
                    "       FA15PRECIODIA,FA15INDFRANQSUMA, FA15INDFRANQUNI, FA15PERIODOGARA, " & _
                    "       FA15DESCUENTO, " & _
                    "       FA15INDNECESARIO, FA15INDSUFICIENTE, FA15INDEXCLUIDO, " & _
                    "       FA15INDDESCONT, FA15INDLINOBLIG, FA1500.FA15CODATRIB, " & _
                    "       FA15INDFACTOBLIG, FA15INDOPCIONAL, FA1500.CI13CODENTIDAD, 0 FA09INDFACTURABLE, " & _
                    "       FA15INDDESGLOSE, FA15INDSUPLEMENTO, FA15DESCRIPCION, FA15ORDIMP, " & _
                    "       FA15RUTAREL, FA15RELFIJO, FA15RELPOR, " & _
                    "       COUNT(FA0700.FA15CODATRIB) PrecioCantidad, FA15INDTramAcum, FA15INDTramDias, FA15PlazoInter, " & _
                    "       FA15INDNecRnfOrigen, FA15INDNecRnfDestino, FA15INDrecogerCant, FA15INDrecogerValor, FA1500.CI32CODTIPECON "
    
    qryCateg.SQL = qryCateg.SQL & _
                    "FROM FA1500, FA0500, FA0700 " & _
                    "WHERE FA0500.FA05CODCATEG = FA15CODFIN " & _
                    "        AND FA15INDEXCLUIDO = 0 " & _
                    "        AND FA15CODCONC = ? AND FA15ESCAT <> 0 " & _
                    "        AND (( FA15FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                    "        AND TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN " & _
                    "        ) OR ( FA15FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                    "        AND TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN) " & _
                    "        OR ( TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') <=FA15FECINICIO " & _
                    "        AND FA15FECFIN <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'))) "
                    
    qryCateg.SQL = qryCateg.SQL & _
                    "        AND FA0700.FA15CODATRIB (+)= FA1500.FA15CODATRIB" & _
                    "        AND FA0500.FA05CODCATEG IN( " & _
                    "            SELECT  distinct fa05codcateg From fa0600 " & _
                    "            Where (( FA06FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                    "            AND TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA06FECFIN " & _
                    "            ) OR ( FA06FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                    "            AND TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA06FECFIN) " & _
                    "            OR ( TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                    "            <=FA06FECINICIO AND FA06FECFIN <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'))) " & _
                    "            start with fa05codcateg in " & _
                    "             (select  fa05codcateg " & _
                    "             From fa0300 " & _
                    "             Where (AD07CodProceso,ad01codasistenci) IN ( " & CodAsistencia & "  ) " & _
                    "             ) " & _
                    "            Connect by Prior  fa05codcateg_p = fa05codcateg " & _
                    "            ) "

    qryCateg.SQL = qryCateg.SQL & _
                    "GROUP BY FA15RUTA, translate(replace(fa15ruta,'/.',''),'./1234567890','a') , FA15CODFIN , " & _
                    "         FA15ESCAT, " & _
                    "        FA0500.FA05DESIG, FA15FECINICIO, FA15FECFIN, FA15PRECIOREF, " & _
                    "        FA15PRECIODIA,FA15INDFRANQSUMA, FA15INDFRANQUNI,FA15PERIODOGARA, " & _
                    "        FA15DESCUENTO, " & _
                    "        FA15INDNECESARIO, FA15INDSUFICIENTE,FA15INDEXCLUIDO, " & _
                    "        FA15INDDESCONT,FA15INDLINOBLIG, FA1500.FA15CODATRIB, " & _
                    "        FA15INDFACTOBLIG , FA15INDOPCIONAL, FA1500.CI13CODENTIDAD, '', 0, " & _
                    "        FA15INDDESGLOSE, FA15INDSUPLEMENTO, FA15DESCRIPCION, FA15ORDIMP, " & _
                    "        FA15RUTAREL, FA15RELFIJO, FA15RELPOR, FA15INDTramAcum, FA15INDTramDias, FA15PlazoInter, " & _
                    "        FA15INDNecRnfOrigen, FA15INDNecRnfDestino, FA15INDrecogerCant, FA15INDrecogerValor, FA1500.CI32CODTIPECON " & _
                    "ORDER BY 2 ASC, FA15INDSUFICIENTE DESC, FA15INDNECESARIO Desc, FA15FECINICIO "

    qryPrecioCant.SQL = "SELECT FA07ACANTFIN, FA07PRECIO, FA07DESIG FROM FA0700 WHERE FA15CODATRIB = ? ORDER BY FA07ACANTFIN"
    
    ' preparar la query en el servidor
    Set qryNodos.ActiveConnection = ConD
    qryNodos.Prepared = True
    
    Set qryCateg.ActiveConnection = ConD
    qryCateg.Prepared = True
    
    Set qryPrecioCant.ActiveConnection = ConD
    qryPrecioCant.Prepared = True
End Sub

Public Function Inicializar(Codigo As Long, strFecInicio As String, strFecFin As String, CodAsistencia As String)
    Dim RS As rdoResultset
    Dim strRuta As String
'    Dim i As Integer
    
    Screen.MousePointer = vbHourglass

    InitQRY CodAsistencia

    FecInicio = strFecInicio
    FecFin = strFecFin
    
    
    qryNodos.rdoParameters(0).Value = Codigo
    qryNodos.rdoParameters(1).Value = FecInicio
    qryNodos.rdoParameters(2).Value = FecInicio
    qryNodos.rdoParameters(3).Value = FecFin
    qryNodos.rdoParameters(4).Value = FecFin
    qryNodos.rdoParameters(5).Value = FecInicio
    qryNodos.rdoParameters(6).Value = FecFin
    
    
    Set RS = qryNodos.OpenResultset
    
    
    
    ' Cargar los datos del primer registro que se corresponde
    ' con la entidad
    If Not RS.EOF Then
        With Entidad
            .Codigo = Codigo
            .Ruta = "." & Codigo & "."
            .Name = "" & RS!FA09DESIG
            .FecInicio = IIf(Format(RS!FA15FECINICIO, "yyyymmdd") > Format(FecInicio, "yyyymmdd"), Format(RS!FA15FECINICIO, "dd/mm/yyyy 00:00:00"), FecInicio)
            .FecFin = IIf(Format(RS!FA15FECFIN, "yyyymmdd") < Format(FecFin, "yyyymmdd"), Format(RS!FA15FECFIN, "dd/mm/yyyy 00:00:00"), FecFin)
            .EsCategoria = False
            .EsNodoFacturable = False
            
            ' asignar el responsable economico del nodo
            If IsNull(RS!CI13CODENTIDAD) Or IsNull(RS!CI32CODTIPECON) Then
                .EntidadResponsable = ""
                .CodTipEcon = ""
            Else
                .EntidadResponsable = RS!CI13CODENTIDAD
                .CodTipEcon = RS!CI32CODTIPECON
            End If
        End With
        RS.MoveNext

    End If

    Do While Not RS.EOF
        ' Quitar el �ltimo c�digo, que se corresponde con el nodo a crear
        strRuta = Left(RS!FA15RUTA, Len(RS!FA15RUTA) - Len(RS!Codigo) - 2)
        ' Quitar el primer c�digo, que se corresponde con la entidad
        strRuta = Right(strRuta, Len(strRuta) - InStr(2, strRuta, "."))
        
        Call BuscarNodo(strRuta, RS, Entidad, True)
        
        RS.MoveNext
    Loop
    RS.Close
        
    
    
    
    '*********************************************************
    ' Buscar las categorias que componen el concierto
    qryCateg.rdoParameters(0).Value = Codigo
    qryCateg.rdoParameters(1).Value = FecInicio
    qryCateg.rdoParameters(2).Value = FecInicio
    qryCateg.rdoParameters(3).Value = FecFin
    qryCateg.rdoParameters(4).Value = FecFin
    qryCateg.rdoParameters(5).Value = FecInicio
    qryCateg.rdoParameters(6).Value = FecFin
    
    qryCateg.rdoParameters(7).Value = FecInicio
    qryCateg.rdoParameters(8).Value = FecInicio
    qryCateg.rdoParameters(9).Value = FecFin
    qryCateg.rdoParameters(10).Value = FecFin
    qryCateg.rdoParameters(11).Value = FecInicio
    qryCateg.rdoParameters(12).Value = FecFin
    
'    qryCateg.rdoParameters(13).Value = CodAsistencia
    
    
    Set RS = qryCateg.OpenResultset
    
'    i = 0
    Do While Not RS.EOF
'        Debug.Print RS!FA15RUTA
        
        ' Quitar el �ltimo c�digo, que se corresponde con el nodo a crear
        strRuta = Left(RS!FA15RUTA, Len(RS!FA15RUTA) - Len(RS!Codigo) - 1)
        ' Quitar el primer c�digo, que se corresponde con la entidad
        strRuta = Right(strRuta, Len(strRuta) - InStr(2, strRuta, "."))
        
        Call BuscarNodo(strRuta, RS, Entidad, True)
'        i = i + 1
        RS.MoveNext
    Loop
    RS.Close
    

    Screen.MousePointer = vbDefault
End Function

Private Function BuscarNodo(ByVal Ruta As String, RS As rdoResultset, nodoPadre As Nodo, ByVal blnEsNodo As Boolean)
    Dim blnRamaAgotada As Boolean
    Static blnFinBusqueda As Boolean
    Dim RSPrecios As rdoResultset
    
    Dim Categoria As String
    Dim strRestante As String
    Dim intPunto As Integer
    Dim intContNodos As Integer
    Dim objTramoPrecioCantidad As New TramoPrecioCantidad
    
    On Error GoTo error
    
    intContNodos = 0
    
    blnFinBusqueda = False
    blnRamaAgotada = False
    
    If Ruta <> "" And Ruta <> "/." Then
      strRestante = Ruta

        ' Quitar el primer punto
        If Left(strRestante, 1) = "." Then
          strRestante = Right(strRestante, Len(strRestante) - 1)
        End If
        
        If InStr(1, strRestante, "/.", 1) = 1 Then
            blnEsNodo = False
            strRestante = Right(strRestante, Len(strRestante) - 2)
'        Else
'            strRestante = Right(strRestante, Len(strRestante) - intPunto)
        End If
    
        ' Buscar el Siguiente punto
        intPunto = InStr(1, strRestante, ".", 1)
        If intPunto <> 0 Then
            Categoria = Left(strRestante, intPunto - 1)
            
'            If InStr(1, strRestante, "/.", 1) = 1 Then
'                blnEsNodo = False
'                strRestante = Right(strRestante, Len(strRestante) - intPunto - 2)
'            Else
                strRestante = Right(strRestante, Len(strRestante) - intPunto)
'            End If
                          
            If blnEsNodo = True Then
            
                Do While Not blnRamaAgotada And Not blnFinBusqueda
'                    Debug.Print Categoria & "-" & Format(intContNodos, "000")
                    
                    Call BuscarNodo(strRestante, RS, nodoPadre.Nodos(Categoria & "-" & Format(intContNodos, "000")), blnEsNodo)
                    intContNodos = intContNodos + 1
                Loop
                
            Else
            
                Do While Not blnRamaAgotada And Not blnFinBusqueda
'                    Debug.Print Categoria & "-" & Format(intContNodos, "000")
                    
                    Call BuscarNodo(strRestante, RS, nodoPadre.Categorias(Categoria & "-" & Format(intContNodos, "000")), blnEsNodo)
                    intContNodos = intContNodos + 1
                Loop
                
            End If
          
        End If
        
        If Mid(strRestante, 1, 1) = "/" Then
          strRestante = Right(strRestante, Len(strRestante) - 1)
        End If
    Else
    
        
        Dim NuevoNodo As Nodo
        Set NuevoNodo = nodoPadre.Add(RS!Codigo, RS!FA15RUTA, IIf(RS!EsCategoria, RS!FA05DESIG, RS!FA09DESIG), Format(RS!FA15FECINICIO, "dd/mm/yyyy 00:00:00"), Format(RS!FA15FECFIN, "dd/mm/yyyy 00:00:00"), RS!EsCategoria, IIf(RS!FA09INDFACTURABLE = True, True, False))

        
        With NuevoNodo
            ' crear la relacion con su padre
            Set .Parent = nodoPadre
            ' A�adirle el resto de atributos
            
            .PrecioRef = IIf(IsNull(RS!FA15PRECIOREF), -1, RS!FA15PRECIOREF)
            .PrecioDia = IIf(IsNull(RS!FA15PRECIODIA), -1, RS!FA15PRECIODIA)
            .FranqSuma = "0" & RS!FA15INDFRANQSUMA
            .FranqUni = "0" & RS!FA15INDFRANQUNI
            .PeriodoGara = "0" & RS!FA15PERIODOGARA
            .Suficiente = IIf(IsNull(RS!FA15INDSUFICIENTE), 0, RS!FA15INDSUFICIENTE)
            .Necesario = IIf(IsNull(RS!FA15INDNECESARIO), 0, RS!FA15INDNECESARIO)
            .Opcional = IIf(IsNull(RS!FA15INDOPCIONAL), 0, RS!FA15INDOPCIONAL)
            .Descont = IIf(IsNull(RS!FA15INDDESCONT), 0, RS!FA15INDDESCONT)
            .LinOblig = IIf(IsNull(RS!FA15INDLINOBLIG), 0, RS!FA15INDLINOBLIG)
            .FactOblig = RS!FA15INDFACTOBLIG Or IIf(RS!FA09INDFACTURABLE = True, True, False)
            .Descuento = "0" & RS!FA15DESCUENTO
            
            .Desglose = IIf(IsNull(RS!FA15INDDESGLOSE), 0, RS!FA15INDDESGLOSE)
            .Suplemento = IIf(IsNull(RS!FA15INDSUPLEMENTO), 0, RS!FA15INDSUPLEMENTO)
            .Descripcion = "" & RS!FA15DESCRIPCION
            
            .RutaRel = "" & RS!FA15RUTAREL
            .NecRnfOrigen = IIf(IsNull(RS!FA15INDNecRnfOrigen), 0, RS!FA15INDNecRnfOrigen)
            .NecRnfDestino = IIf(IsNull(RS!FA15INDNecRnfDestino), 0, RS!FA15INDNecRnfDestino)
            .RecogerCant = IIf(IsNull(RS!FA15INDRecogerCant), 0, RS!FA15INDRecogerCant)
            .RecogerValor = IIf(IsNull(RS!FA15INDRecogerValor), 0, RS!FA15INDRecogerValor)
            .RelFijo = IIf(IsNull(RS!FA15RelFijo), -1, RS!FA15RelFijo)
            .RelPor = IIf(IsNull(RS!FA15RelPor), -1, RS!FA15RelPor)
            
            .OrdImp = "0" & RS!FA15OrdImp
            
            .CodAtrib = "0" & RS!FA15CodAtrib
            
            .TramAcum = IIf(IsNull(RS!FA15INDTramAcum), 0, RS!FA15INDTramAcum)
            .TramDias = IIf(IsNull(RS!FA15INDTramDias), 0, RS!FA15INDTramDias)
            .PlazoInter = IIf(IsNull(RS!FA15PlazoInter), 0, RS!FA15PlazoInter)
            
            ''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''
            If RS!PrecioCantidad > 0 Then
                qryPrecioCant.rdoParameters(0) = RS!FA15CodAtrib
                
                Set RSPrecios = qryPrecioCant.OpenResultset
                Do While Not RSPrecios.EOF
                
                    Set objTramoPrecioCantidad = New TramoPrecioCantidad
                    objTramoPrecioCantidad.HastaCantidad = RSPrecios!fa07acantfin
                    objTramoPrecioCantidad.Precio = RSPrecios!fa07precio
                    objTramoPrecioCantidad.Descripcion = "" & RSPrecios!fa07Desig
                    .TramosPrecioCantidad.Add objTramoPrecioCantidad
                    
                    RSPrecios.MoveNext
                Loop
            End If
                
            ' asignar el responsable economico del nodo
            ' si no tiene por defecto se le pone el del nodo padre
            If IsNull(RS!CI13CODENTIDAD) Or IsNull(RS!CI32CODTIPECON) Then
                .EntidadResponsable = .Parent.EntidadResponsable
                .CodTipEcon = .Parent.CodTipEcon
            Else
                .EntidadResponsable = RS!CI13CODENTIDAD
                .CodTipEcon = RS!CI32CODTIPECON
            End If
                
        End With
        
        
        ' Si el nodo a a�adir esta comprendido entre las fechas del nodo padre
        ' no hace falta recorrer mas ramas del arbol en donde colocarlo
        If (Format(nodoPadre.FecInicio, "yyyymmdd") <= Format(RS!FA15FECINICIO, "yyyymmdd")) And _
           (Format(RS!FA15FECFIN, "yyyymmdd") <= Format(nodoPadre.FecFin, "yyyymmdd")) Then
            blnFinBusqueda = True
        End If
    
    End If

Exit Function

error:
    Select Case Err.Number
        Case 5
            ' no se encuentra en la colecci�n
            blnRamaAgotada = True
            Resume Next
            
        Case vbObjectError + 1
            ' no est� entre las fechas
            ' hay que buscar el siguiente
             Resume Next
        Case 91
            ' El objeto no existe
            Exit Function
            
        Case Else
            MsgBox "Error n� " & Err.Number & " " & Err.Description
'            Resume
    End Select
End Function

Private Function Max(str1 As String, str2 As String) As String
    If str1 > str2 Then
        Max = str1
    Else
        Max = str2
    End If
End Function

Private Function Min(str1 As String, str2 As String) As String
    If str1 < str2 Then
        Min = str1
    Else
        Min = str2
    End If
End Function

