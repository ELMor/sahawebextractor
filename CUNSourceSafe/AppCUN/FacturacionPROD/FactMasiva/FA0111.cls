VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CodigosConcierto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Codigo del concierto a aplicar, p.e. 74 ... ('Osasunbidea')
' es obligatorio
Public Concierto As Long
' Codigo del nodo a facturar, p.e. 300 ... ('Forfait de T.H.')
' es opcional y sirve para concretar mas en donde asignar los RNF's
Public NodoAFacturar As Long

