VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "Comdlg32.ocx"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_NuevoPago 
   Caption         =   "Introducci�n de un nuevo pago"
   ClientHeight    =   5865
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8640
   LinkTopic       =   "Form2"
   ScaleHeight     =   5865
   ScaleWidth      =   8640
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAutomat 
      Caption         =   "C&ompensaci�n Autom�tica"
      Height          =   375
      Left            =   6390
      TabIndex        =   32
      Top             =   2295
      Visible         =   0   'False
      Width           =   2130
   End
   Begin VB.TextBox txtCodPago 
      Height          =   285
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Visible         =   0   'False
      Width           =   285
   End
   Begin vsViewLib.vsPrinter vsPrinter1 
      Height          =   420
      Left            =   180
      TabIndex        =   29
      Top             =   5310
      Visible         =   0   'False
      Width           =   465
      _Version        =   196608
      _ExtentX        =   820
      _ExtentY        =   741
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      PageBorder      =   0
      TableBorder     =   0
   End
   Begin VB.CommandButton cmdAntiguo 
      Caption         =   "&Compensar por Antig�edad"
      Height          =   375
      Left            =   4140
      TabIndex        =   21
      Top             =   2295
      Visible         =   0   'False
      Width           =   2130
   End
   Begin VB.CheckBox chkIndAcuse 
      Alignment       =   1  'Right Justify
      Caption         =   "�Sin Acuse?"
      Height          =   195
      Left            =   6975
      TabIndex        =   10
      Top             =   2025
      Width           =   1275
   End
   Begin VB.ComboBox cboFormaPago 
      Height          =   315
      Left            =   2745
      TabIndex        =   6
      Top             =   1215
      Width           =   5730
   End
   Begin VB.TextBox txtInicPago 
      Height          =   285
      Left            =   2250
      TabIndex        =   5
      Top             =   1215
      Width           =   420
   End
   Begin VB.TextBox txtApunte 
      Height          =   285
      Left            =   5445
      TabIndex        =   9
      Top             =   1935
      Width           =   1275
   End
   Begin VB.TextBox txtObservaciones 
      Height          =   285
      Left            =   2250
      TabIndex        =   7
      Top             =   1575
      Width           =   6225
   End
   Begin VB.TextBox txtRemesa 
      Height          =   285
      Left            =   2250
      TabIndex        =   8
      Top             =   1935
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   " Facturas Pendientes de Compensar "
      Height          =   2490
      Left            =   135
      TabIndex        =   13
      Top             =   2745
      Width           =   8340
      Begin SSDataWidgets_B.SSDBGrid grdPagos 
         Height          =   2175
         Left            =   135
         TabIndex        =   14
         Top             =   225
         Width           =   8115
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         Col.Count       =   7
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Name =   "Compensar"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   1879
         Columns(1).Caption=   "N� Factura"
         Columns(1).Name =   "NoFactura"
         Columns(1).Alignment=   1
         Columns(1).CaptionAlignment=   2
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   7435
         Columns(2).Caption=   "Descripcion"
         Columns(2).Name =   "Descripcion"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1931
         Columns(3).Caption=   "Pdte. Comp."
         Columns(3).Name =   "PdteCompensar"
         Columns(3).Alignment=   1
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   2117
         Columns(4).Caption=   "Cantidad"
         Columns(4).Name =   "Cantidad"
         Columns(4).Alignment=   1
         Columns(4).CaptionAlignment=   2
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   556
         Columns(5).Name =   "Entera"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   11
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "Codigo"
         Columns(6).Name =   "Codigo"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   11
         Columns(6).FieldLen=   256
         _ExtentX        =   14314
         _ExtentY        =   3836
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdCompensar 
      Caption         =   "C&ompensar"
      Height          =   330
      Left            =   4185
      TabIndex        =   11
      Top             =   2295
      Width           =   1275
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   420
      Left            =   5220
      TabIndex        =   18
      Top             =   5355
      Width           =   1275
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   420
      Left            =   1935
      TabIndex        =   17
      Top             =   5355
      Width           =   1275
   End
   Begin VB.TextBox txtCantidad 
      Height          =   285
      Left            =   6390
      TabIndex        =   2
      Top             =   495
      Width           =   1275
   End
   Begin VB.TextBox txtResponsable 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2250
      TabIndex        =   0
      Tag             =   "txtResponsable"
      Top             =   135
      Width           =   5235
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFecha 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   2250
      TabIndex        =   1
      Tag             =   "Fecha Inicio"
      Top             =   495
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaEfecto 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   2250
      TabIndex        =   3
      Tag             =   "Fecha Inicio"
      Top             =   840
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaCierre 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   6390
      TabIndex        =   4
      Tag             =   "Fecha Inicio"
      Top             =   855
      Width           =   1725
      _Version        =   65537
      _ExtentX        =   3043
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      Enabled         =   0   'False
      DefaultDate     =   ""
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.TextBox txtCodFormaPago 
      Height          =   285
      Left            =   2250
      TabIndex        =   28
      Top             =   1215
      Width           =   420
   End
   Begin VB.TextBox txtNotaAbono 
      Height          =   285
      Left            =   2250
      TabIndex        =   30
      Top             =   1215
      Width           =   420
   End
   Begin MSComDlg.CommonDialog cmdDlg1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Forma de Pago:"
      Height          =   240
      Index           =   8
      Left            =   360
      TabIndex        =   27
      Top             =   1260
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Apunte:"
      Height          =   240
      Index           =   7
      Left            =   3735
      TabIndex        =   26
      Top             =   1980
      Width           =   1635
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Observaciones"
      Height          =   240
      Index           =   6
      Left            =   360
      TabIndex        =   25
      Top             =   1620
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Remesa:"
      Height          =   240
      Index           =   5
      Left            =   360
      TabIndex        =   24
      Top             =   1980
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Cierre:"
      Height          =   240
      Index           =   4
      Left            =   4500
      TabIndex        =   23
      Top             =   900
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Efecto:"
      Height          =   240
      Index           =   3
      Left            =   360
      TabIndex        =   22
      Top             =   900
      Width           =   1815
   End
   Begin VB.Label lblPendiente 
      Caption         =   "Pendiente"
      Height          =   240
      Left            =   2790
      TabIndex        =   20
      Top             =   2340
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.Label lblTitulo 
      Alignment       =   1  'Right Justify
      Caption         =   "Cantidad Pendiente de Asignar:"
      Height          =   240
      Left            =   360
      TabIndex        =   19
      Top             =   2340
      Visible         =   0   'False
      Width           =   2355
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha de Pago:"
      Height          =   240
      Index           =   2
      Left            =   360
      TabIndex        =   16
      Top             =   540
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Cantidad Entregada:"
      Height          =   240
      Index           =   1
      Left            =   4500
      TabIndex        =   15
      Top             =   540
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Responsable Econ�mico:"
      Height          =   240
      Index           =   0
      Left            =   360
      TabIndex        =   12
      Top             =   180
      Width           =   1815
   End
End
Attribute VB_Name = "frm_NuevoPago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Nombre As String
Dim FecPago As String
Dim CodPersona As Long
Dim Conciertos As String
Dim PagoNuevo As Boolean
Dim ImportePago As Double
Dim CodPago As Long
Dim Pagado As Double
Dim Apuntes As Integer
Dim QryPagos(1 To 10) As New rdoQuery

Private Type FallosAcunsa
  NumFact As String
  ImpFact As Double
  ImpPago As Double
End Type

Dim arFallos() As FallosAcunsa

Sub CargarFacturas(Persona As Long, Conciertos As String)
Dim MiSql As String
Dim MiRs As rdoResultset
Dim MisqlSuma As String
Dim MiRsSuma As rdoResultset
Dim MiInsertar As String
Dim IntPospyc As Integer
Dim Conc As Integer
Dim Importe As Long

  Me.grdPagos.RemoveAll
  While Conciertos <> ""
    IntPospyc = InStr(1, Conciertos, ";")
    If IntPospyc <> 0 Then
      Conc = Left(Conciertos, IntPospyc - 1)
      Conciertos = Right(Conciertos, Len(Conciertos) - IntPospyc)
    ElseIf Conciertos <> "" Then
      Conc = Conciertos
      Conciertos = ""
    End If
    'Cargamos todas las facturas que no est�n compensadas en su totalidad
    '****QRY1
    QryPagos(1).rdoParameters(0) = CodPersona
    QryPagos(1).rdoParameters(1) = CodPersona
    QryPagos(1).rdoParameters(2) = Conc
    Set MiRs = QryPagos(1).OpenResultset
    
    If Not MiRs.EOF Then
      'MiRs.MoveLast
      'MiRs.MoveFirst
      While Not MiRs.EOF
        MiInsertar = False & Chr(9) & MiRs("FA04NUMFACT") & Chr(9) & MiRs("DESCRIP") & "" & Chr(9) & _
                           FormatearMoneda(CDbl(MiRs("Suma")) - (MiRs("cOMPENSADO") + MiRs("COMPENSADO2")), tiTotalFact) & Chr(9) & 0 & _
                           Chr(9) & False & Chr(9) & ""  'mirs("FA04CODFACT")
        grdPagos.AddItem MiInsertar
        MiRs.MoveNext
      Wend
    End If
  Wend
  
End Sub

Sub CompensarFacturasAcunsa(TotalPago As Double)
Dim TextLine
Dim factura As String
Dim Cantidad As Double
Dim Cont As Integer
Dim ContComp As Integer
Dim ContNoComp As Integer
Dim NumFact As String
Dim SqLbUsCaR As String
Dim rsBuscar As rdoResultset
Dim Pdte As Double
Dim Fichero As String
Dim MiSql As String
Dim fecha As Date
Dim Asunto As String
Dim TotalFact As Integer
Dim CantTotal As Double
Dim Resultado As Integer
Dim CantPdte As Double


  Cont = 0
  ContComp = 0
  ContNoComp = 0
  CantPdte = TotalPago
  
  cmdDlg1.ShowOpen
  Fichero = cmdDlg1.filename
  If Fichero <> "" Then
    Open Fichero For Input As #1 ' Open file
      Do While Not EOF(1) ' Loop until end of file.
        Cont = Cont + 1
        Line Input #1, TextLine ' Read line into variable.
        'Si son las 5 primeras l�neas corespondientes a la cabecera cogemos los datos de estas.
        If Cont <= 5 Then
          Select Case Cont
            Case 1
              'Fecha de emisi�n del listado
              fecha = CDate(Trim(Mid(TextLine, 21, 25)))
            Case 2
              'Asunto o periodo correspondiente
              Asunto = Trim(Mid(TextLine, 21, 25))
            Case 3
              'Total de facturas del listado
              TotalFact = CInt(Trim(Mid(TextLine, 21, 25)))
            Case 4
              'Montante del listado en pesetas
              CantTotal = CDbl(Trim(Mid(TextLine, 21, 25)))
              'En el caso de que el montante del listado sea superior a la cantidad pendiente
              'de compensar del pago le damos la posibidad de continuar.
              If CantTotal > TotalPago Then
                Resultado = MsgBox("El total de las facturas incluidas en el fichero es superior " & _
                                   "a la cantidad pendiente de asignar del pago. " & _
                                   "�Continuar?", vbYesNo + vbQuestion, "Proceso " & _
                                   "autom�tico de compensaci�n")
                If Resultado = vbNo Then
                  'Se ha decidido parar el proceso.
                  MsgBox "Se ha parado el proceso de compensaci�n autom�tica", vbOKOnly, "Proceso autom�tico de compensaci�n"
                  Close #1
                  Exit Sub
                End If
              End If
          End Select
        Else
          factura = Trim(Left(TextLine, 20))
          NumFact = "13/" & factura
          Cantidad = CDbl(Trim(Mid(TextLine, 21, 20)))
          Debug.Print factura & "-" & Cantidad
          'Buscamos la factura y comprobamos que la cantidad pendiente por compensar.
          SqLbUsCaR = "Select FA0400.FA04CODFACT, NVL(FA04CANTFACT,0) as Cantidad, NVL(SUM(FA18IMPCOMP),0) as SUMACOMP " & _
                      "From FA0400,FA1800 Where FA04NUMFACT = '" & NumFact & "' " & _
                      "And FA0400.FA04CODFACT = FA1800.FA04CODFACT(+) " & _
                      "GROUP BY FA0400.FA04CODFACT,FA04CANTFACT"
          Set rsBuscar = objApp.rdoConnect.OpenResultset(SqLbUsCaR, 3)
          If Not rsBuscar.EOF Then
            Pdte = CDbl(rsBuscar("Cantidad") - rsBuscar("SUMACOMP"))
            If Pdte > Cantidad Then
              Debug.Print "Es mayor la cantidad que la pendiente en " & NumFact
              ContNoComp = ContNoComp + 1
            Else
              'Comprobaremosq que tenemos suficiente dinero para compensar la factura.
              If Pdte > CantPdte Then
                'Compensaremos la factura por la cantidad pendiente y abandonaremos la compensaci�n de facturas
                ContComp = ContComp + 1
                MsgBox "El pago " & CodPago & " no se ha compensado en su totalidad", vbOKOnly + vbInformation, "Compensaci�n autom�tica"

                CantPdte = 0
              Else
                'Compensamos la factura
                ContComp = ContComp + 1
                MiSql = "INSERT INTO FA1800 (FA17CODPAGO,FA04CODFACT,FA18FECCOMP,FA18IMPCOMP) " & _
                         "VALUES (" & CodPago & "," & rsBuscar("fa04codfact") & _
                         ", TO_DATE('" & Me.SDCFecha.Date & "','DD/MM/YYYY')," & rsBuscar("CAntidad") & ")"
                objApp.rdoConnect.Execute MiSql
                MiSql = "Update FA0400 Set FA04INDCOMPENSA = 2 Where FA04CODFACT = " & rsBuscar("FA04CODFACT")
                objApp.rdoConnect.Execute MiSql
                CantPdte = CantPdte - CDbl(rsBuscar("Cantidad"))
              End If
            End If
          Else
            Debug.Print "No existe la factura = " & NumFact
            ContNoComp = ContNoComp + 1
          End If
          If CantPdte = 0 Then
            Exit Do
          End If
        End If
      Loop
    Close #1  ' Close file.
    Debug.Print "Total..........: " & Cont
    Debug.Print "Compensadas....: " & ContComp
    Debug.Print "No Compensadas.: " & ContNoComp
    MsgBox "Total..........: " & Cont & Chr(13) & Chr(10) & "Compensadas....: " & ContComp & Chr(13) & Chr(10) & "No Compensadas.: " & ContNoComp
  End If
  
End Sub

Sub ImprimirAbono(NoPago As Long, Responsable As String)
Dim Texto As String
Dim MiSql As String
Dim MiRs As rdoResultset
Dim sqlFact As String
Dim rsFact As rdoResultset
Dim Acumulado As Double
Dim Compensacion As String

  If Pagado = 0 Then
    Compensacion = "N NO COMPENSADO"
  ElseIf CDbl(txtCantidad) = Pagado Then
    Compensacion = "C COMPENSADO"
  ElseIf CDbl(txtCantidad) > Pagado Then
    Compensacion = "P PARCIALM. COMPENSADO"
  End If
  With vsPrinter1
    .Preview = False
    .Action = 3
    .FontSize = 11
    .FontName = "Courier New"
    .CurrentY = 660
    .FontBold = True
    '.FontUnderline = True
    .TextAlign = taCenterBaseline
    Texto = "A   B   O   N   O"
    .Paragraph = Texto
    Texto = "================="
    .Paragraph = Texto
    .Paragraph = "                                               " & objSecurity.strUser & " " & Me.SDCFecha.Date
    '.FontBold = False
    '.FontUnderline = False
    .TextAlign = taLeftBaseline
    Texto = "NUMERO DE ABONO.......: " & Space(7 - Len(str(NoPago))) & NoPago
    .Paragraph = Texto
    Texto = "RESPONSABLE ECONOMICO.: " & CodPersona & " " & Me.txtResponsable.Text
    .Paragraph = Texto
    Texto = "FORMA DE PAGO.........: " & Me.txtInicPago.Text & " " & Me.cboFormaPago.Text
    .Paragraph = Texto
    Texto = "CONCEPTO..............: " & Me.txtObservaciones
    .Paragraph = Texto
    Texto = "FECHA INTRODUCC. ABONO: " & Me.SDCFecha.Date
    .Paragraph = Texto
    Texto = "FECHA EFECTO CAJA.....: " & Me.SDCFechaEfecto.Date
    .Paragraph = Texto
    Texto = "IMPORTE...............: " & Space(12 - Len(Format(Me.txtCantidad.Text, "###,###,###"))) & Format(Me.txtCantidad.Text, "###,###,###")
    .Paragraph = Texto
    Texto = "COMPONSACION..........: " & Compensacion
    .Paragraph = Texto
    Texto = "N.APUNTES COMPENSADOS.: " & Space(12 - Len(Trim(str(Apuntes)))) & Apuntes
    .Paragraph = Texto
    .Paragraph = ""
    Texto = "TIPO FECHA          NUMERO NOMBRE                                  IMPORTE"
    .Paragraph = Texto
    Texto = "--------------------------------------------------------------------------"
    .Paragraph = Texto
    'Seleccionamos las facturas compensadas
    MiSql = "Select * From FA1800 Where FA17CODPAGO = " & CodPago
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      Acumulado = 0
      While Not MiRs.EOF
        '***QRY2
        QryPagos(2).rdoParameters(0) = MiRs("FA04CODFACT")
        Set rsFact = QryPagos(2).OpenResultset
        If Not rsFact.EOF Then
          Texto = "F   "
          Texto = Texto & " " & Format(rsFact("FA04FECFACTURA"), "DD/MM/YYYY")
          Texto = Texto & Right(Space(11) & rsFact("FA04NUMFACT"), 11) & " "
          Texto = Texto & Left(Trim(rsFact("CI22NUMHISTORIA")) & " " & Trim(Responsable) & Space(37), 37)
          Texto = Texto & Space(10 - Len(Format(MiRs("FA18IMPCOMP"), "###,###,##0"))) & Format(MiRs("FA18IMPCOMP"), "###,###,##0")
        Else
          Texto = "F   "
          Texto = Texto & " " & Format(MiRs("FA18FECCOMP"), "DD/MM/YYYY")
          Texto = Texto & Space(8) & " "
          Texto = Texto & Left(Trim(Responsable) & Space(37), 37)
          Texto = Texto & Space(10 - Len(Format(MiRs("FA18IMPCOMP"), "###,###,##0"))) & Format(MiRs("FA18IMPCOMP"), "###,###,##0")
        End If
        .Paragraph = Texto
        Acumulado = Acumulado + CDbl(MiRs("FA18IMPCOMP"))
        MiRs.MoveNext
      Wend
      Texto = "                                                              ------------"
      .Paragraph = Texto
      Texto = "                                          TOTAL COMPENSADO...."
      Texto = Texto & Space(12 - Len(Format(Acumulado, "###,###,##0"))) & Format(Acumulado, "###,###,##0")
      .Paragraph = Texto
      Texto = "                                                              ============"
      .Paragraph = Texto
    End If
    .Action = 6
  End With
End Sub

Sub IniciarQRYs()
Dim MiSql As String
Dim x As Integer

  MiSql = "Select /*+ INDEX(FA0400,FA0402)*/ FA04NUMFACT, Max(FA0400.FA04CODFACT) As Factura," & _
          "Sum(FA04CANTFACT) As Suma,Max(FA04FECFACTURA) As Fecha, Max(FA04DESCRIP) AS Descrip," & _
          "Min(FA04INDCOMPENSA) As Compensada, COUNT(FA0400.FA04CODFACT) As Cantidad, " & _
          "NVL(Sum(FA18IMPCOMP),0) AS Compensado, NVL(Sum(FA58IMPCOMP),0) AS Compensado2 " & _
          "From FA0400,FA1800,FA5800 " & _
          "Where FA04NUMFACREAL IS NULL " & _
          "And CI21CODPERSONA = ? " & _
          "And FA0400.FA04CODFACT = FA1800.FA04CODFACT(+) " & _
          "And FA0400.FA04CODFACT = FA5800.FA04CODFACT_POS(+) " & _
          "And EXISTS " & _
            "(Select FA04NUMFACT " & _
            "From FA0400 FA0400_1 " & _
            "Where FA0400_1.FA04NUMFACREAL IS NULL " & _
            "And FA0400_1.FA04NUMFACT = FA0400.FA04NUMFACT " & _
            "And FA04INDCOMPENSA IN (1,0) " & _
            "And CI21CODPERSONA = ? " & _
            "And FA09CODNODOCONC = ?) " & _
            "Group By FA04NUMFACT Order by Fecha Asc"
  QryPagos(1).SQL = MiSql
  
  MiSql = "Select FA04FECFACTURA, FA04NUMFACT,CI22NUMHISTORIA " & _
          "From FA0400,AD0100 " & _
          "Where FA04CODFACT = ? " & _
          "And FA0400.AD01CODASISTENCI = AD0100.AD01CODASISTENCI"
  QryPagos(2).SQL = MiSql
        
  MiSql = "Select FA04CODFACT, FA04CANTFACT, FA04INDCOMPENSA " & _
          "From FA0400 Where FA04NUMFACT = ? " & _
          "And FA04NUMFACREAL Is Null AND FA04INDCOMPENSA IN (1,0) " & _
          "Order By FA04FECFACTURA"
  QryPagos(3).SQL = MiSql
        
  MiSql = "Select nvl(Sum(FA18IMPCOMP),0) As Suma From FA1800 " & _
          "Where FA04CODFACT = ?"
  QryPagos(4).SQL = MiSql
  
  MiSql = "Select nvl(Sum(FA58IMPCOMP) ,0) As Suma From FA5800 " & _
          "Where FA04CODFACT_POS = ?"
  QryPagos(5).SQL = MiSql
          
  MiSql = "INSERT INTO FA1800 (FA17CODPAGO,FA04CODFACT,FA18FECCOMP,FA18IMPCOMP) " & _
          "VALUES (?,?, TO_DATE(?,'DD/MM/YYYY'),?)"
  QryPagos(6).SQL = MiSql
  
  MiSql = "Update FA0400 Set FA04INDCOMPENSA = ? Where FA04CODFACT = ?"
  QryPagos(7).SQL = MiSql
            
  For x = 1 To 7
    Set QryPagos(x).ActiveConnection = objApp.rdoConnect
    QryPagos(x).Prepared = True
  Next
  
End Sub


Sub NuevoPago(idpersona As Long, Persona As String, fecha As String, Concierto As String, Anadir As Boolean, Importe As Double, Pago As Long)
  CodPersona = idpersona
  Nombre = UCase(Persona)
  FecPago = fecha
  Conciertos = Concierto
  PagoNuevo = Anadir
  ImportePago = Importe
  CodPago = Pago
  frm_NuevoPago.Show vbModal
  Set frm_NuevoPago = Nothing
End Sub


Private Sub cboFormaPago_Click()
Dim MiSql As String
Dim MiRs As rdoResultset

  If Trim(Me.cboFormaPago.Text) <> "" Then
    MiSql = "Select * from FA2000 Where FA20DESC = '" & Me.cboFormaPago.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      Me.txtInicPago.Text = MiRs("FA20DESIG")
      Me.txtCodFormaPago.Text = MiRs("FA20CODTIPPAGO")
      Me.txtNotaAbono = MiRs("FA20INDNOTABO")
      If MiRs("FA20INDNOTABO") = 1 Then
        Me.chkIndAcuse.Value = 0
        Me.chkIndAcuse.Enabled = True
      Else
        Me.chkIndAcuse.Value = 1
        Me.chkIndAcuse.Enabled = False
      End If
    Else
      MsgBox "No existe la forma de pago", vbInformation + vbOKOnly, "Aviso"
      If cboFormaPago.Enabled = True Then
        cboFormaPago.SetFocus
      End If
    End If
  End If
End Sub

Private Sub cboFormaPago_DropDown()
Dim MiSql As String
Dim MiRs As rdoResultset

  cboFormaPago.Clear
  MiSql = "Select FA20DESC FROM FA2000"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      Me.cboFormaPago.AddItem MiRs(0)
      MiRs.MoveNext
    Wend
  End If
  
End Sub


Private Sub cboFormaPago_LostFocus()
Dim MiSql As String
Dim MiRs As rdoResultset

  If Trim(Me.cboFormaPago.Text) <> "" Then
    MiSql = "Select * from FA2000 Where FA20DESC = '" & Me.cboFormaPago.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      Me.txtInicPago.Text = MiRs("FA20DESIG")
      Me.txtCodFormaPago.Text = MiRs("FA20CODTIPPAGO")
      Me.txtNotaAbono = MiRs("FA20INDNOTABO")
      If MiRs("FA20INDNOTABO") = 1 Then
        Me.chkIndAcuse.Value = 0
        chkIndAcuse.Enabled = True
      Else
        Me.chkIndAcuse.Value = 1
        chkIndAcuse.Enabled = False
      End If
    Else
      MsgBox "No existe la forma de pago", vbInformation + vbOKOnly, "Aviso"
      If cboFormaPago.Enabled = True Then
        cboFormaPago.SetFocus
      End If
    End If
  End If
End Sub

Private Sub cmdAceptar_Click()
Dim NoPago As String
Dim MiSql As String
Dim x As Integer
Dim Resultado As Integer
Dim rsFact As rdoResultset
Dim SqlComp As String
Dim PdteComp As Double
Dim MiRs As rdoResultset
Dim Compensado As Double
Dim Cantidad As Double
Dim Indicativo As Integer

  On Error GoTo ErrorenGrabacion
  objApp.rdoConnect.BeginTrans
    If PagoNuevo = True Then
      NoPago = fNextClave("FA17CODPAGO", "FA1700")
      MiSql = "INSERT INTO FA1700 (FA17CODPAGO,CI21CODPERSONA,FA17FECPAGO," & _
                   "FA17CANTIDAD,FA17FECCIERRE,FA17FECEFECTO,FA17APUNTE,FA17REMESA," & _
                   "FA17OBSERV,FA20CODTIPPAGO," & _
                   "FA17INDACUSE, FA17INDACUENVI)VALUES("
      MiSql = MiSql & NoPago  'FA17CODPAGO
      MiSql = MiSql & "," & CodPersona 'CI21CODPERSONA
      MiSql = MiSql & ",TO_DATE('" & Me.SDCFecha & "','DD/MM/YYYY')" 'FA17FECPAGO
      MiSql = MiSql & "," & Me.txtCantidad 'FA17CANTIDAD
      If Not IsNull(Me.SDCFechaCierre.Date) Then
        MiSql = MiSql & ",TO_DATE('" & Me.SDCFechaCierre & "','DD/MM/YYYY')" 'FA17FECCIERRE
      Else
        MiSql = MiSql & ",NULL"
      End If
      If Not IsNull(Me.SDCFechaEfecto.Date) Then
        MiSql = MiSql & ",TO_DATE('" & Me.SDCFechaEfecto & "','DD/MM/YYYY')" 'FA17FECEFECTO
      Else
        MiSql = MiSql & ",NULL"
      End If
      If Trim(txtApunte) <> "" Then
        MiSql = MiSql & ",'" & Me.txtApunte & "'" 'FA17APUNTE
      Else
        MiSql = MiSql & ",NULL"
      End If
      If Trim(txtRemesa) <> "" Then
        MiSql = MiSql & ",'" & Me.txtRemesa & "'" 'FA17REMESA
      Else
        MiSql = MiSql & ",NULL"
      End If
      If Trim(txtObservaciones) <> "" Then
        MiSql = MiSql & ",'" & Me.txtObservaciones & "'" 'FA17OBSERV
      Else
        MiSql = MiSql & ",NULL"
      End If
      MiSql = MiSql & "," & Me.txtCodFormaPago 'FA20CODTIPPAGO
      If Me.txtNotaAbono.Text = 1 Then
        MiSql = MiSql & ",1" 'Como se ha emitido la nota de abono no es necesario el acuse
      Else
        MiSql = MiSql & ",0"
      End If
      If Me.chkIndAcuse.Value = 1 Then
        MiSql = MiSql & ",1"
      Else
        MiSql = MiSql & ",0"
      End If
      MiSql = MiSql & ")"
      CodPago = NoPago
      objApp.rdoConnect.Execute MiSql
    Else ' Se trata de un pago anterior que ha podido ser modificado
      Resultado = MsgBox("�Desea guardar los datos de la parte superior?", vbYesNo + vbQuestion, "Modificaci�n de un Cobro")
      If Resultado = vbYes Then
        MiSql = "UPDATE FA1700 SET FA17FECPAGO = TO_DATE('" & Me.SDCFecha & "','DD/MM/YYYY')," & _
                     "FA17CANTIDAD = " & Me.txtCantidad
        If Not IsNull(Me.SDCFechaEfecto.Date) Then
          MiSql = MiSql & ", FA17FECEFECTO = TO_DATE('" & Me.SDCFechaEfecto & "','DD/MM/YYYY')" 'FA17FECEFECTO
        Else
          MiSql = MiSql & ", FA17FECEFECTO = NULL"
        End If
        If Trim(txtApunte) <> "" Then
          MiSql = MiSql & ", FA17APUNTE = '" & Me.txtApunte & "'" 'FA17APUNTE
        Else
          MiSql = MiSql & ", FA17APUNTE = NULL"
        End If
        If Trim(txtRemesa) <> "" Then
          MiSql = MiSql & ", FA17REMESA = '" & Me.txtRemesa & "'" 'FA17REMESA
        Else
          MiSql = MiSql & ", FA17REMESA = NULL"
        End If
        If Trim(txtObservaciones) <> "" Then
          MiSql = MiSql & ", FA17OBSERV = '" & Me.txtObservaciones & "'" 'FA17OBSERV
        Else
          MiSql = MiSql & ", FA17OBSERV = NULL"
        End If
        If Me.chkIndAcuse.Value = 1 Then
          MiSql = MiSql & ", FA17INDACUENVI = 1"
        Else
          MiSql = MiSql & ", FA17INDACUENVI = 0"
        End If
        MiSql = MiSql & ", FA20CODTIPPAGO = " & Me.txtCodFormaPago 'FA20CODTIPPAGO
        MiSql = MiSql & " WHERE FA17CODPAGO = " & txtCodPago
        objApp.rdoConnect.Execute MiSql
      End If
    End If
    Pagado = 0
    Apuntes = 0
    grdPagos.MoveFirst
    For x = 1 To grdPagos.Rows

'   ///////////////////////////////////////////
'   COBROS NUEVOS, EST�N LAS FACTURAS AGRUPADAS
'   ///////////////////////////////////////////
      If grdPagos.Columns(4).Text <> 0 Then
        'Seleccionamos todas las facturas que tienen ese c�digo y no est�n completamente compensadas
        '***QRY3
        QryPagos(3).rdoParameters(0) = grdPagos.Columns(1).Text
        Set rsFact = QryPagos(3).OpenResultset
        PdteComp = CDbl(grdPagos.Columns(4).Text)
        If Not rsFact.EOF Then
          While Not rsFact.EOF And PdteComp > 0
            'Calcularemos la parte de la factura que ya est� compensada
            '***QRY4
            QryPagos(4).rdoParameters(0) = rsFact("FA04CODFACT")
            Set MiRs = QryPagos(4).OpenResultset
            Compensado = CDbl(MiRs("Suma"))
            '***QRY5
            QryPagos(5).rdoParameters(0) = rsFact("FA04CODFACT")
            Set MiRs = QryPagos(5).OpenResultset
            Compensado = Compensado + CDbl(MiRs("SUMA"))
            If PdteComp >= rsFact("FA04CANTFACT") - Compensado Then
              Cantidad = rsFact("FA04CANTFACT") - Compensado
              PdteComp = PdteComp - Cantidad
              Indicativo = 2
            Else
              Cantidad = PdteComp
              PdteComp = 0
              Indicativo = 1
            End If
            '***QRY6
            QryPagos(6).rdoParameters(0) = CodPago
            QryPagos(6).rdoParameters(1) = rsFact("FA04CODFACT")
            QryPagos(6).rdoParameters(2) = Me.SDCFecha.Date
            QryPagos(6).rdoParameters(3) = CDbl(Cantidad)
            QryPagos(6).Execute
            '***QRY7
            QryPagos(7).rdoParameters(0) = Indicativo
            QryPagos(7).rdoParameters(1) = rsFact("FA04CODFACT")
            QryPagos(7).Execute
            Pagado = Pagado + CDbl(grdPagos.Columns(4).Text)
            Apuntes = Apuntes + 1
            rsFact.MoveNext
          Wend
        End If
      End If
      grdPagos.MoveNext
    Next
  objApp.rdoConnect.CommitTrans
  If PagoNuevo Then
    If Me.txtInicPago = "TR" And Trim(Me.txtRemesa.Text) <> "" And Trim(Me.txtRemesa.Text) <> "" Then
    Else
      Call ImprimirAbono(CodPago, txtResponsable)
    End If
  End If
  Unload Me
Exit Sub

ErrorenGrabacion:
  MsgBox "Se ha producido un error en la grabaci�n de los pagos", vbCritical + vbOKOnly, "Atenci�n"
  objApp.rdoConnect.RollbackTrans
End Sub
Private Sub cmdAutomat_Click()
Dim TextLine
Dim factura As String
Dim Cantidad As Double
Dim Cont As Integer
Dim ContComp As Integer
Dim ContNoComp As Integer
Dim NumFact As String
Dim SqLbUsCaR As String
Dim rsBuscar As rdoResultset
Dim Pdte As Double
Dim Fichero As String
Dim MiSql As String
Dim qryAutom(1 To 5) As New rdoQuery
Dim Indice As Integer
Dim x As Integer

  Cont = 0
  ContComp = 0
  ContNoComp = 0
  
  'Creamos las qrys que utilizaremos en la compensaci�n autom�tica.
  MiSql = "Select FA0400.FA04CODFACT, NVL(FA04CANTFACT,0) as Cantidad, NVL(SUM(FA18IMPCOMP),0) as SUMACOMP " & _
          "From FA0400,FA1800 Where FA04NUMFACT = ? " & _
          "And FA0400.FA04CODFACT = FA1800.FA04CODFACT(+) " & _
          "And FA0400.FA04NUMFACREAL Is Null " & _
          "GROUP BY FA0400.FA04CODFACT,FA04CANTFACT"
  qryAutom(1).SQL = MiSql
  Set qryAutom(1).ActiveConnection = objApp.rdoConnect
  qryAutom(1).Prepared = True
  
  MiSql = "INSERT INTO FA1800 (FA17CODPAGO,FA04CODFACT,FA18FECCOMP,FA18IMPCOMP) " & _
          "VALUES (?,?,TO_DATE(?,'DD/MM/YYYY'),?)"
  qryAutom(2).SQL = MiSql
  Set qryAutom(2).ActiveConnection = objApp.rdoConnect
  qryAutom(2).Prepared = True
  
  MiSql = "Update FA0400 Set FA04INDCOMPENSA = ? Where FA04CODFACT = ?"
  qryAutom(3).SQL = MiSql
  Set qryAutom(3).ActiveConnection = objApp.rdoConnect
  qryAutom(3).Prepared = True
  
  On Error GoTo ErrorEnCoMpensacion
  objApp.rdoConnect.BeginTrans
  cmdDlg1.ShowOpen
  Fichero = cmdDlg1.filename
  If Fichero <> "" Then
    Open Fichero For Input As #1 ' Open file
      Do While Not EOF(1) ' Loop until end of file.
        Cont = Cont + 1
        Me.Caption = Cont
        Line Input #1, TextLine ' Read line into variable.
        If Cont > 5 Then
          If Trim(TextLine) <> "" Then
            factura = Trim(Left(TextLine, 27))
            NumFact = "13/" & factura
            Cantidad = CDbl(Right(TextLine, 15))
            Debug.Print factura & "-" & Cantidad
            
            'Buscamos la factura y comprobamos que la cantidad pendiente por compensar.
            qryAutom(1).rdoParameters(0) = NumFact
            Set rsBuscar = qryAutom(1).OpenResultset
            
            If Not rsBuscar.EOF Then
              Pdte = CDbl(rsBuscar("Cantidad") - rsBuscar("SUMACOMP"))
              If Cantidad > Pdte Then
                Debug.Print "Es mayor la cantidad que la pendiente en " & NumFact
                ContNoComp = ContNoComp + 1
                ReDim Preserve arFallos(1 To ContNoComp)
                arFallos(ContNoComp).NumFact = NumFact
                arFallos(ContNoComp).ImpFact = Pdte
                arFallos(ContNoComp).ImpPago = Cantidad
              Else
                If Cantidad < Pdte Then
                  ContNoComp = ContNoComp + 1
                  ReDim Preserve arFallos(1 To ContNoComp)
                  arFallos(ContNoComp).NumFact = NumFact
                  arFallos(ContNoComp).ImpFact = Pdte
                  arFallos(ContNoComp).ImpPago = Cantidad
                  Indice = 1
                Else
                  Indice = 2
                End If
                'Compensamos la factura
                ContComp = ContComp + 1
                qryAutom(2).rdoParameters(0) = CodPago
                qryAutom(2).rdoParameters(1) = rsBuscar("FA04CODFACT")
                qryAutom(2).rdoParameters(2) = Format(Me.SDCFecha.Date, "DD/MM/YYYY")
                qryAutom(2).rdoParameters(3) = rsBuscar("CANTIDAD")
                qryAutom(2).Execute
                'Indicamos el indice y el codigo de factura
                qryAutom(3).rdoParameters(0) = Indice
                qryAutom(3).rdoParameters(1) = rsBuscar("FA04CODFACT")
                qryAutom(3).Execute
              End If
            Else
              Debug.Print "No existe la factura = " & NumFact
              ContNoComp = ContNoComp + 1
            End If
          End If
        End If
      Loop
    Close #1  ' Close file.
    Debug.Print "Total..........: " & Cont
    Debug.Print "Compensadas....: " & ContComp
    Debug.Print "No Compensadas.: " & ContNoComp
    MsgBox "Total..........: " & Cont & Chr(13) & Chr(10) & "Compensadas....: " & ContComp & Chr(13) & Chr(10) & "No Compensadas.: " & ContNoComp
    objApp.rdoConnect.CommitTrans
  End If
  If ContNoComp > 0 Then
    Me.vsPrinter1.Preview = False
    Me.vsPrinter1.Action = paStartDoc
    vsPrinter1.FontUnderline = True
    vsPrinter1.CurrentX = 40 * 50
    vsPrinter1.Text = "N� de factura"
    vsPrinter1.CurrentX = 80 * 50
    vsPrinter1.Text = "Imp. Factura"
    vsPrinter1.CurrentX = 120 * 50
    vsPrinter1.Text = "Imp. Pago"
    vsPrinter1.Paragraph = ""
    vsPrinter1.FontUnderline = False
    For x = 1 To ContNoComp
      vsPrinter1.CurrentX = 40 * 50
      vsPrinter1.Text = arFallos(x).NumFact
      vsPrinter1.CurrentX = 80 * 50
      vsPrinter1.Text = arFallos(x).ImpFact
      vsPrinter1.CurrentX = 120 * 50
      vsPrinter1.Text = arFallos(x).ImpPago
      vsPrinter1.Paragraph = ""
      If vsPrinter1.CurrentY > 270 * 50 Then
        vsPrinter1.NewPage
        vsPrinter1.FontUnderline = True
        vsPrinter1.CurrentX = 40 * 50
        vsPrinter1.Text = "N� de factura"
        vsPrinter1.CurrentX = 80 * 50
        vsPrinter1.Text = "Imp. Factura"
        vsPrinter1.CurrentX = 120 * 50
        vsPrinter1.Text = "Imp. Pago"
        vsPrinter1.Paragraph = ""
        vsPrinter1.FontUnderline = False
      End If
    Next
    Me.vsPrinter1.Action = paEndDoc
  End If
Exit Sub
ErrorEnCoMpensacion:
  MsgBox "Se ha producido un error en la compensaci�n autom�tica de facturas", vbCritical + vbOKOnly, "Compensacion Autom�tica"
  objApp.rdoConnect.RollbackTrans
  Resume Next
  Close #1
End Sub

Private Sub cmdAntiguo_Click()
Dim ContLineas As Integer
Dim Pendiente As Double
Dim x As Integer
  'Asignamos a contlineas el n�mero de l�neas que tiene el grid
  ContLineas = grdPagos.Rows
  'Asignamos a pendiente el valor que queda pendiente por asignar
  Pendiente = CDbl(lblPendiente)
  'Recorreremos cada una de las l�neas para poner el asignado a 0 e incrementar el pendiente
  grdPagos.MoveFirst
  For x = 1 To ContLineas
    If CDbl(grdPagos.Columns(4).Text) > 0 Then
      Pendiente = Pendiente + CDbl(grdPagos.Columns(4).Text)
      grdPagos.Columns(4).Text = 0
      grdPagos.Columns(5).Value = False
    End If
    grdPagos.MoveNext
  Next
  'Recorreremos las l�neas del grid e iremos asignando a las facturas la cantidad necesaria
  'para su compensaci�n hasta el momento de quedar una parcialmente compensada y el
  'pendiente sea 0
  grdPagos.MoveFirst
  For x = 1 To ContLineas
    If CDbl(grdPagos.Columns(3).Text) <= Pendiente Then
      grdPagos.Columns(4).Text = grdPagos.Columns(3).Text
      Pendiente = Pendiente - CDbl(grdPagos.Columns(3).Text)
      grdPagos.Columns(5).Value = True
    ElseIf CDbl(grdPagos.Columns(3).Text) > Pendiente Then
      grdPagos.Columns(4).Text = Format(Pendiente, "###,###,##0.##")
      Pendiente = 0
    End If
    If Pendiente = 0 Then
      lblPendiente = 0
      Exit For
    End If
    grdPagos.MoveNext
  Next
  lblPendiente = Pendiente
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub cmdCompensar_Click()
Dim Respuesta As Integer
Dim MiSql As String
Dim MiRs As rdoResultset

  If PagoNuevo = True Then
    If Trim(txtCantidad.Text) <> "" Then
      Respuesta = MsgBox("El pago realizado es de " & Format(Me.txtCantidad.Text, "###,###,##0.###") & _
                                      Chr(10) & Chr(13) & "�Es esto correcto?", vbQuestion + vbYesNo, "Confirmaci�n del Pago")
      If Respuesta = vbYes Then
        frm_NuevoPago.Height = 6300
        cmdCompensar.Visible = False
        Me.lblTitulo.Visible = True
        Me.lblPendiente.Visible = True
        Me.lblPendiente.Caption = Me.txtCantidad.Text
        'Comprobamos que no es una persona jur�dica para activar el bot�n de compensar por antig�edad.
        MiSql = "Select CI33CODTIPPERS From CI2100 Where CI21CODPERSONA = " & CodPersona
        Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
        If Not MiRs.EOF Then
          If Not IsNull(MiRs("CI33CODTIPPERS")) Then
            If MiRs("CI33CODTIPPERS") = 2 Then
              Me.cmdAntiguo.Visible = False
            Else
              Me.cmdAntiguo.Visible = True
            End If
          End If
        End If
        Me.cmdAutomat.Visible = True
        If Conciertos <> "" Then
          Call CargarFacturas(CodPersona, Conciertos)
        End If
      End If
    Else
      MsgBox "Debe introducir una cantidad", vbOKOnly + vbInformation, "Aviso"
      Me.txtCantidad.SetFocus
    End If
  Else
      frm_NuevoPago.Height = 6300
      cmdCompensar.Visible = False
       Me.lblTitulo.Visible = True
      Me.lblPendiente.Visible = True
      Me.lblPendiente.Caption = CDbl(txtCantidad.Text) - CalcularPendiente(CodPago)
      Me.cmdAntiguo.Visible = True
      Me.cmdAutomat.Visible = True
      Call CargarFacturas(CodPersona, Conciertos)
    End If
End Sub

Function CalcularPendiente(Pago As Long) As Double
Dim MiSql As String
Dim MiRs As rdoResultset
Dim Acumulado As Double

  MiSql = "Select NVL(SUM(FA18IMPCOMP),0) As Suma From FA1800 Where FA17CODPAGO = " & Pago
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    Acumulado = (MiRs("Suma"))
  Else
    Acumulado = 0
  End If
  'Seleccionamos las compensaciones que pueda tener con otros aboonos
  MiSql = "Select NVL(Sum(FA51IMPCOMP),0) As Suma From FA5100 Where FA17CODPAGO_POS = " & Pago
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    Acumulado = Acumulado + MiRs("Suma")
  End If
  MiSql = "Select NVL(Sum(FA51IMPCOMP),0) As Suma From FA5100 Where FA17CODPAGO_NEG = " & Pago
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    Acumulado = Acumulado + MiRs("Suma")
  End If
  CalcularPendiente = Acumulado
End Function


Private Sub Form_Load()
Dim MiSql As String
Dim MiRs As rdoResultset

  Call Me.IniciarQRYs
  
  Me.txtResponsable.Text = Nombre
  Me.SDCFecha.Date = FecPago
  If PagoNuevo = True Then
    frm_NuevoPago.Height = 3100
    Me.SDCFechaEfecto.Date = FecPago
    Me.Caption = "Introducci�n de un nuevo pago"
  Else
    txtCodPago = CodPago
    Me.Caption = "Asignaci�n a facturas del remanente de un pago"
    MiSql = "Select * From FA1700 Where FA17CODPAGO = " & CodPago
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      Me.SDCFecha = Format(MiRs("FA17FECPAGO"), "dd/mm/yyyy")
      If Not IsNull(MiRs("FA17FECCIERRE")) Then
        Me.SDCFechaCierre.Date = Format(MiRs("FA17FECCIERRE"), "DD/MM/YYYY")
      End If
      If Not IsNull(MiRs("FA17FECEFECTO")) Then
        Me.SDCFechaEfecto.Date = Format(MiRs("FA17FECEFECTO"), "DD/MM/YYYY")
      End If
      Me.txtApunte = MiRs("FA17APUNTE") & ""
      Me.txtRemesa = MiRs("FA17REMESA") & ""
      Me.txtObservaciones = MiRs("FA17OBSERV") & ""
      If Not IsNull(MiRs("FA20CODTIPPAGO")) Then
        Me.txtCodFormaPago = MiRs("FA20CODTIPPAGO")
        Call txtCodFormaPago_LostFocus
      End If
      If MiRs("FA17INDACUENVI") = 1 Then
        Me.chkIndAcuse.Value = 1
      Else
        Me.chkIndAcuse.Value = 0
      End If
    End If
    Me.txtCantidad = ImportePago
    If Not IsNull(MiRs("FA17FECEFECTO")) Then
      Me.txtCantidad.Locked = True
    End If
    Call cmdCompensar_Click
  End If
End Sub

Private Sub grdPagos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim x As Integer
Dim Pendiente As Double
  If ColIndex = 4 Then
    If grdPagos.Columns(4).Text = "" Then
      grdPagos.Columns(4).Text = 0
    End If
    'La primera comprobaci�n ser� la de que la cantidad a compensar no es mayor que la cantidad facturada
    If CDbl(grdPagos.Columns(4).Text) > CDbl(grdPagos.Columns(3).Text) Then
      MsgBox "La cantidad a compensar no puede ser mayor que la cantidad pendiente", vbOKOnly + vbCritical, "Atenci�n"
      grdPagos.Columns(4).Text = OldValue
    Else
      ' Comprobaremos que la cantidad que sumamos no supera a la que queda pendiente
      Pendiente = CDbl(lblPendiente) + OldValue
      If Pendiente - grdPagos.Columns(4).Text < 0 Then
        MsgBox "No se puede asignar esa cantidad ya que es mayor que la cantidad pendiente de asignaci�n", vbCritical + vbOKOnly, "Atenci�n"
        lblPendiente = Pendiente - OldValue
        grdPagos.Columns(4).Text = OldValue
      Else
        lblPendiente = Pendiente - grdPagos.Columns(4).Text
      End If
    End If
  ElseIf ColIndex = 5 Then
    
  End If
  
  
End Sub

Private Sub grdPagos_Click()
Dim Pendiente As Double

  If grdPagos.Col = 5 Then
    If grdPagos.Columns(5).Value = True Then
      Pendiente = CDbl(lblPendiente) + grdPagos.Columns(4).Text
      grdPagos.Columns(4).Text = 0
      lblPendiente = Pendiente
    ElseIf grdPagos.Columns(5).Value = False Then
      Pendiente = CDbl(lblPendiente) + grdPagos.Columns(4).Text
      If Pendiente >= grdPagos.Columns(3).Text Then
        grdPagos.Columns(4).Text = grdPagos.Columns(3).Text
        lblPendiente = Pendiente - grdPagos.Columns(4).Text
      Else
        MsgBox "No es posible compensar totalmente la factura, dado que la cantidad pendiente no es suficiente" & _
        Chr(10) & Chr(13) & "Puede compensarla introduciendo el importe en la columna Cantidad", vbCritical + vbOKOnly, "Atencion"
        grdPagos.Columns(5).Value = False
      End If
    End If
  End If
End Sub

Private Sub txtCantidad_LostFocus()
Dim MiSql As String
Dim MiRs As rdoResultset
Dim Suma As Double
Dim Compensado As Double
Dim Cont As Integer

  'Comprobaremos que el campo cantidad no se ha dejado en blanco
  If Trim(txtCantidad.Text) <> "" Then
    If Not IsNumeric(txtCantidad.Text) Then
      MsgBox "La cantidad debe ser un dato num�rico", vbExclamation + vbOKOnly, "Atenci�n"
      If txtCantidad.Enabled = True Then
        txtCantidad.SetFocus
      End If
      Exit Sub
    End If
  Else
    Exit Sub
  End If
  'Comprobaremos la cantidad que ya est� compensada, para impedir que se introduzca una cantidad menor.
  If PagoNuevo = False Then
    MiSql = "Select SUM(FA18IMPCOMP) As SUMA from FA1800 Where FA17CODPAGO = " & Me.txtCodPago
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      If Not IsNull(MiRs("SUMA")) Then
        Suma = CDbl(MiRs("SUMA"))
      Else
        Suma = 0
      End If
    End If
    If CDbl(txtCantidad.Text) < Suma Then
      MsgBox "La cantidad introducida no puede ser menor a la de la suma de las facturas compensadas " & _
             "por ese pago.", vbInformation + vbOKOnly, "Atenci�n"
      If txtCantidad.Enabled = True Then
        txtCantidad.SetFocus
      End If
      Exit Sub
    End If
    'Comprobaremos que la cantidad compensada hasta ese momento no supera la introducida en el pago
    grdPagos.MoveFirst
    Compensado = 0
    For Cont = 1 To Me.grdPagos.Rows
      Compensado = Compensado + CDbl(grdPagos.Columns(4).Value)
      grdPagos.MoveNext
    Next
    If Compensado > CDbl(txtCantidad.Text) - Suma Then
      MsgBox "La cantidad introducida no puede ser inferior a la cantidad actualmente compensada", vbExclamation + vbOKOnly, "Atenci�n"
    End If
    'Cambiaremos la cantidad pendiente de compensar en el lblpendiente
    Me.lblPendiente.Caption = CDbl(txtCantidad.Text) - Suma
  End If
End Sub


Private Sub txtCodFormaPago_LostFocus()
Dim MiSql As String
Dim MiRs As rdoResultset

  If Trim(txtCodFormaPago.Text) <> "" Then
    MiSql = "Select * From FA2000 Where FA20CODTIPPAGO = " & Me.txtCodFormaPago.Text
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      Me.txtInicPago = MiRs("FA20DESIG")
      Me.cboFormaPago.Text = MiRs("FA20DESC")
      Me.txtNotaAbono = MiRs("FA20INDNOTABO")
      If MiRs("FA20INDNOTABO") = 1 Then
        Me.chkIndAcuse.Value = 0
        Me.chkIndAcuse.Enabled = True
      Else
        Me.chkIndAcuse.Value = 1
        Me.chkIndAcuse.Enabled = False
      End If
    Else
      MsgBox "No existe la forma de pago", vbInformation + vbOKOnly, "Aviso"
    End If
  Else
    txtCodFormaPago.Text = ""
    cboFormaPago.Text = ""
  End If

End Sub


Private Sub txtInicPago_LostFocus()
Dim MiSql As String
Dim MiRs As rdoResultset

  If Trim(txtInicPago.Text) <> "" Then
    txtInicPago = UCase(txtInicPago.Text)
    MiSql = "Select * From FA2000 Where FA20DESIG = '" & Me.txtInicPago.Text & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
    If Not MiRs.EOF Then
      Me.txtInicPago = MiRs("FA20DESIG")
      Me.cboFormaPago.Text = MiRs("FA20DESC")
      Me.txtNotaAbono = MiRs("FA20INDNOTABO")
      If cboFormaPago.Enabled = True Then
        cboFormaPago.SetFocus
      End If
    Else
      MsgBox "No existe la forma de pago", vbInformation + vbOKOnly, "Aviso"
      If txtInicPago.Enabled = True Then
        txtInicPago.SetFocus
      End If
    End If
  Else
    txtCodFormaPago.Text = ""
    cboFormaPago.Text = ""
  End If
End Sub


