VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmNuevaFranja 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Nueva Franja"
   ClientHeight    =   2160
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5685
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2160
   ScaleWidth      =   5685
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   4680
      TabIndex        =   12
      Top             =   1680
      Width           =   975
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   0
      TabIndex        =   13
      Top             =   1680
      Width           =   975
   End
   Begin VB.CheckBox chkDomingo 
      Alignment       =   1  'Right Justify
      Caption         =   "D"
      Height          =   255
      Left            =   5115
      TabIndex        =   9
      Top             =   600
      Width           =   495
   End
   Begin VB.CheckBox chkSabado 
      Alignment       =   1  'Right Justify
      Caption         =   "S"
      Height          =   255
      Left            =   4320
      TabIndex        =   8
      Top             =   600
      Width           =   495
   End
   Begin VB.CheckBox chkViernes 
      Alignment       =   1  'Right Justify
      Caption         =   "V"
      Height          =   255
      Left            =   3510
      TabIndex        =   7
      Top             =   600
      Width           =   495
   End
   Begin VB.CheckBox chkJueves 
      Alignment       =   1  'Right Justify
      Caption         =   "J"
      Height          =   255
      Left            =   2595
      TabIndex        =   6
      Top             =   600
      Width           =   615
   End
   Begin VB.CheckBox chkMiercoles 
      Alignment       =   1  'Right Justify
      Caption         =   "X"
      Height          =   255
      Left            =   1800
      TabIndex        =   5
      Top             =   600
      Width           =   495
   End
   Begin VB.CheckBox chkMartes 
      Alignment       =   1  'Right Justify
      Caption         =   "M"
      Height          =   255
      Left            =   990
      TabIndex        =   4
      Top             =   600
      Width           =   495
   End
   Begin VB.CheckBox chkLunes 
      Alignment       =   1  'Right Justify
      Caption         =   "L"
      Height          =   255
      Left            =   75
      TabIndex        =   3
      Top             =   600
      Width           =   615
   End
   Begin VB.TextBox txtValor 
      BackColor       =   &H00FFFF00&
      Height          =   285
      Left            =   4920
      TabIndex        =   11
      Top             =   1080
      Width           =   495
   End
   Begin MSMask.MaskEdBox mskHoraIni 
      Height          =   315
      Left            =   840
      TabIndex        =   0
      Top             =   120
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   556
      _Version        =   327681
      BackColor       =   16776960
      MaxLength       =   5
      Mask            =   "##:##"
      PromptChar      =   "_"
   End
   Begin MSMask.MaskEdBox mskHoraFin 
      Height          =   315
      Left            =   2280
      TabIndex        =   1
      Top             =   120
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   556
      _Version        =   327681
      BackColor       =   16776960
      MaxLength       =   5
      Mask            =   "##:##"
      PromptChar      =   "_"
   End
   Begin SSDataWidgets_B.SSDBCombo cboSSDBActividad 
      Height          =   315
      Left            =   3720
      TabIndex        =   2
      Tag             =   "Departamento"
      Top             =   120
      Width           =   1905
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AutoRestore     =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasForeColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   7620
      Columns(1).Caption=   "Descripci�n"
      Columns(1).Name =   "Descripci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasForeColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      _ExtentX        =   3360
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBCombo cboSSDBAsignacion 
      Height          =   315
      Left            =   1560
      TabIndex        =   10
      Tag             =   "Departamento"
      Top             =   1080
      Width           =   1785
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AutoRestore     =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasForeColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   7620
      Columns(1).Caption=   "Descripci�n"
      Columns(1).Name =   "Descripci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasForeColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      _ExtentX        =   3149
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16776960
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Modo de Asignacion:"
      Height          =   195
      Index           =   4
      Left            =   0
      TabIndex        =   18
      Top             =   1080
      Width           =   1500
   End
   Begin VB.Label lblLabel1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "N� Citas Admitidas:"
      Height          =   195
      Index           =   3
      Left            =   3360
      TabIndex        =   17
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Actividad:"
      Height          =   195
      Index           =   2
      Left            =   3000
      TabIndex        =   16
      Top             =   120
      Width           =   705
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Hora Fin:"
      Height          =   195
      Index           =   0
      Left            =   1560
      TabIndex        =   15
      Top             =   120
      Width           =   645
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Hora Inicio:"
      Height          =   195
      Index           =   1
      Left            =   0
      TabIndex        =   14
      Top             =   120
      Width           =   810
   End
End
Attribute VB_Name = "frmNuevaFranja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngCodRecurso As Long
Dim intCodPerfil As Integer
Dim intFranja
Dim strMeCaption As String
Dim strHoraIni As String
Dim strHoraFin As String

Private Sub Form_Load()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

'Cargar Actividad
sql = "SELECT PR12CODACTIVIDAD, PR12DESACTIVIDAD FROM PR1200"
sql = sql & " ORDER BY PR12DESACTIVIDAD"
Set rs = objApp.rdoConnect.OpenResultset(sql)
While Not rs.EOF
    cboSSDBActividad.AddItem rs!PR12CODACTIVIDAD & Chr$(9) & rs!PR12DESACTIVIDAD
    rs.MoveNext
Wend
cboSSDBActividad.AddItem "0" & Chr$(9) & "Ninguno"

'cargar modo asignacion
cboSSDBAsignacion.AddItem "1" & Chr$(9) & "POR CANTIDAD"
cboSSDBAsignacion.AddItem "2" & Chr$(9) & "SECUENCIAL"
cboSSDBAsignacion.AddItem "3" & Chr$(9) & "INTERVALOS PREDETERMINADOS"
cboSSDBAsignacion.AddItem "4" & Chr$(9) & "INTERVALOS OSCILANTES"
If objPipe.PipeExist("AG0202_AG0207_AG11CODRECURSO") Then
    lngCodRecurso = objPipe.PipeGet("AG0202_AG0207_AG11CODRECURSO")
    objPipe.PipeRemove ("AG0202_AG0207_AG11CODRECURSO")
End If
If objPipe.PipeExist("AG0202_AG0207_AG07CODPERFIL") Then
    intCodPerfil = objPipe.PipeGet("AG0202_AG0207_AG07CODPERFIL")
    objPipe.PipeRemove ("AG0202_AG0207_AG07CODPERFIL")
End If
If objPipe.PipeExist("AG0202_AG0207_AG04CODFRANJA") Then
    intFranja = objPipe.PipeGet("AG0202_AG0207_AG04CODFRANJA")
    objPipe.PipeRemove ("AG0202_AG0207_AG04CODFRANJA")
    pCargarDatos
Else
    intFranja = 0
End If
strMeCaption = Me.Caption
End Sub


''Private Sub cboSSDBAsignacion_CloseUp()
''If cboSSDBAsignacion.Value <> "" Then
''Select Case cboSSDBAsignacion.Value
''    Case 1
''        lblLabel1(3).Caption = "Citas Admitidas"
''        lblLabel1(3).Visible = True
''        txtValor.Visible = True
''    Case 2 'secuencial
''        lblLabel1(3).Visible = False
''        txtValor.Visible = False
''    Case 3, 4
''        lblLabel1(3).Caption = "Intervalo Cita"
''        lblLabel1(3).Visible = True
''        txtValor.Visible = True
''End Select
''End If
''End Sub
Private Sub cboSSDBAsignacion_KeyPress(KeyAscii As Integer)
If cboSSDBAsignacion.Value <> "" Then
Select Case cboSSDBAsignacion.Value
    Case 1
        lblLabel1(3).Caption = "Citas Admitidas"
        lblLabel1(3).Visible = True
        txtValor.Visible = True
    Case 2
        lblLabel1(3).Visible = False
        txtValor.Visible = False
    Case 3, 4
        lblLabel1(3).Caption = "Intervalo Cita"
        lblLabel1(3).Visible = True
        txtValor.Visible = True
End Select
End If
End Sub
Private Sub txtValor_KeyPress(KeyAscii As Integer)

If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
    KeyAscii = 13
End If

End Sub
Private Sub cmdAceptar_Click()
Dim strOR$
Dim sql$
Dim qry1 As rdoQuery
Dim rs1 As rdoResultset
'comprobamos los campos obligatorios
    If mskHoraIni.Text = "" Or mskHoraIni = "" Then
        MsgBox "Introduzca la hora de inicio y fin", vbExclamation, strMeCaption
        Exit Sub
    End If
    If chkLunes.Value = 0 And chkMartes.Value = 0 And chkMiercoles.Value = 0 And chkJueves.Value = 0 _
    And chkViernes.Value = 0 And chkSabado.Value = 0 And chkDomingo.Value = 0 Then
        MsgBox "No hay ningun d�a seleccionado", vbExclamation, strMeCaption
        Exit Sub
    End If
    If cboSSDBAsignacion.Text = "" Then
        MsgBox "Seleccione modo de Asignacion de las citas", vbExclamation, strMeCaption
        Exit Sub
    End If
    If Trim(txtValor) = "" Then
            MsgBox "Indique la Cantidad de citas admitidas", vbExclamation, strMeCaption
            Exit Sub
    End If
    'comprobamos que las franjas tengan fechas correctas
    '1.- la hora fin < que la hora de incio
    If Val(mskHoraIni.ClipText) > Val(mskHoraFin.ClipText) Then
        MsgBox "Hora de Incio superior a Hora de Fin", vbExclamation, strMeCaption
        Exit Sub
    End If
    '2.- que no se solape con ninguna otra franja en los mismos dias
    sql = "SELECT COUNT(*) FROM AG0400 WHERE "
    sql = sql & "AG11CODRECURSO = ? "
    sql = sql & " AND AG07CODPERFIL =  ?"
    sql = sql & " AND TO_DATE(AG04HORINFRJHH||':'||AG04HORINFRJMM,'HH24:MI') < "
    sql = sql & "TO_DATE('" & mskHoraFin.Text & "','HH24:MI')"
    sql = sql & " AND TO_DATE(AG04HORFIFRJHH||':'||AG04HORFIFRJMM,'HH24:MI') > "
    sql = sql & "TO_DATE('" & mskHoraIni.Text & "','HH24:MI') AND ("
    If chkLunes.Value = 1 Then sql = sql & "AG04INDLUNFRJA = -1 OR "
    If chkMartes.Value = 1 Then sql = sql & "AG04INDMARFRJA = -1 OR "
    If chkMiercoles.Value = 1 Then sql = sql & "AG04INDMIEFRJA = -1 OR "
    If chkJueves.Value = 1 Then sql = sql & "AG04INDJUEFRJA = -1 OR "
    If chkViernes.Value = 1 Then sql = sql & "AG04INDVIEFRJA = -1 OR "
    If chkSabado.Value = 1 Then sql = sql & "AG04INDSABFRJA = -1 OR "
    If chkDomingo.Value = 1 Then sql = sql & "AG04INDDOMFRJA = -1 OR "
    sql = Left(sql, Len(sql) - 4)
    sql = sql & ")"
    If intFranja <> 0 Then
        sql = sql & " AND AG04CODFRANJA <> ?"
    End If
    Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
        qry1(0) = lngCodRecurso
        qry1(1) = intCodPerfil
        If intFranja <> 0 Then qry1(2) = intFranja
    Set rs1 = qry1.OpenResultset()
    If rs1(0) > 0 Then
        MsgBox "Esta franja interfiere con una ya creada", vbExclamation, strMeCaption
        Exit Sub
    End If
    rs1.Close
    qry1.Close
'si estan todos vamos a crear la franja
    If intFranja = 0 Then pCrearFranja Else pModFranja
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub pCrearFranja()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim intCodFranja As Integer
Screen.MousePointer = vbHourglass

'hallamos el codigo de la nueva franja
sql = "SELECT MAX(AG04CODFRANJA) FROM AG0400 WHERE AG11CODRECURSO = ? "
sql = sql & " AND AG07CODPERFIL = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
Set rs = qry.OpenResultset
If IsNull(rs(0)) Then intCodFranja = 1 Else intCodFranja = rs(0) + 1
rs.Close
qry.Close

'Insertarmos la franja
sql = "INSERT INTO AG0400 (AG11CODRECURSO,AG07CODPERFIL,AG04CODFRANJA,"
sql = sql & "PR12CODACTIVIDAD,AG04INDLUNFRJA,AG04INDMARFRJA,"
sql = sql & "AG04INDMIEFRJA,AG04INDJUEFRJA,AG04INDVIEFRJA,"
sql = sql & "AG04INDSABFRJA,AG04INDDOMFRJA,AG04HORINFRJHH,"
sql = sql & "AG04HORINFRJMM,AG04HORFIFRJHH,AG04HORFIFRJMM,"
sql = sql & "AG04NUMCITADMI,AG04INTERVCITA,AG04MODASIGCITA) VALUES"
sql = sql & " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
    qry(2) = intCodFranja 'CODIGO DE LAFRANJA
    If cboSSDBActividad.Text = "" Then qry(3) = Null Else qry(3) = cboSSDBActividad.Value
    If chkLunes.Value = 1 Then qry(4) = -1 Else qry(4) = 0
    If chkMartes.Value = 1 Then qry(5) = -1 Else qry(5) = 0
    If chkMiercoles.Value = 1 Then qry(6) = -1 Else qry(6) = 0
    If chkJueves.Value = 1 Then qry(7) = -1 Else qry(7) = 0
    If chkViernes.Value = 1 Then qry(8) = -1 Else qry(8) = 0
    If chkSabado.Value = 1 Then qry(9) = -1 Else qry(9) = 0
    If chkDomingo.Value = 1 Then qry(10) = -1 Else qry(10) = 0
    qry(11) = Format$(mskHoraIni.Text, "HH") 'hh inicio
    qry(12) = Right(mskHoraIni.Text, 2) 'mm inicio
    qry(13) = Format$(mskHoraFin.Text, "HH")
    qry(14) = Right(mskHoraFin.Text, 2) 'mm inicio
    Select Case cboSSDBAsignacion.Value
        Case 1 'Asignacion por cantidad: grabamos las citas admitidas
            qry(15) = txtValor.Text
            qry(16) = Null
        Case 2 'secuencial tiende a desaparecer
            qry(15) = txtValor.Text
            qry(16) = Null
        Case 3, 4
            qry(15) = txtValor.Text
            qry(16) = DateDiff("n", mskHoraIni.Text, mskHoraFin.Text) / Val(txtValor.Text)
    End Select
    qry(17) = cboSSDBAsignacion.Value
qry.Execute
Screen.MousePointer = vbDefault
If Err > 0 Then
    MsgBox "La Franja no se ha podido Crear." & Chr$(13) & Error, vbExclamation, strMeCaption
Else
    MsgBox "Franja Creada", vbOKOnly, strMeCaption
    Call objPipe.PipeSet("AG0207_AG0202_AG04CODFRANJA", intCodFranja)
End If
End Sub

Private Sub pModFranja()
Dim sql As String
Dim sql1 As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim rs1 As rdoResultset
'Dim strHoraIni As String
'Dim strHoraFin As String
Dim strIniPerfil As String
Dim intR As Integer
Dim msg As String



On Error GoTo Canceltrans

 objApp.BeginTrans
Screen.MousePointer = vbHourglass

'miramos cuantos pacientes quedan afectados
'hayamos los periodos de vigencia
sql = "SELECT AG09FECINVIPER,AG09FECFIVIPER FROM AG0900 "
sql = sql & " WHERE AG11CODRECURSO = ? "
sql = sql & " AND AG07CODPERFIL = ?"
sql = sql & " AND AG09FECFIVIPER > SYSDATE "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    'miramos cuantos pacientes hay en esos periodos de vigencia
    sql = "SELECT COUNT(*) FROM CI0100, PR0400 WHERE AG11CODRECURSO = ? "
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN "
    sql = sql & " AND PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND CI01SITCITA = '1' AND ("
    While Not rs.EOF
        If CDate(Format(rs(0), "DD/MM/YYYY")) < CDate(fAhora) Then _
        strIniPerfil = CDate(Now) Else strIniPerfil = rs(0)
        sql1 = sql1 & "(CI01FECCONCERT >= TO_DATE( '" & Format$(strIniPerfil, "DD/MM/YYYY") & "','DD/MM/YYYY')"
        sql1 = sql1 & " AND CI01FECCONCERT <= TO_DATE( '" & Format$(rs(1), "DD/MM/YYYY") & "','DD/MM/YYYY')) OR "
        rs.MoveNext
    Wend
    rs.Close
    qry.Close
    sql1 = Left(sql1, Len(sql1) - 3)
    sql1 = sql1 & ") AND ("
    If chkLunes.Value = 1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%LUNES%' OR"
    If chkMartes.Value = 1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%MARTES%' OR"
    If chkMiercoles.Value = 1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%MI�RCOLES%' OR"
    If chkJueves.Value = 1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%JUEVES%' OR"
    If chkViernes.Value = 1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%viernes%' OR"
    If chkSabado.Value = 1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%S�BADO%' OR"
    If chkDomingo.Value = 1 Then sql1 = sql1 & " TO_CHAR(CI01FECCONCERT,'DAY') LIKE '%DOMINGO%' OR"
    sql1 = Left(sql1, Len(sql1) - 3)
    sql1 = sql1 & ") AND TO_CHAR(CI01FECCONCERT,'HH24:MI') >= '" & strHoraIni & "'"
    sql1 = sql1 & " AND  TO_CHAR(CI01FECCONCERT,'HH24:MI') < '" & strHoraFin & "' "
    sql = sql & sql1
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
    Set rs1 = qry.OpenResultset()
    intR = MsgBox("Quedar�n afectados " & rs1(0) & " pacientes" & Chr$(13) & _
            "Desea Continuar? ", vbYesNo + vbQuestion, Me.Caption)
    If intR = vbNo Then
        Screen.MousePointer = vbDefault
        objApp.RollBackTrans
        Exit Sub
    End If
    If rs1(0) > 0 Then 'EN CASO DE QUE HAYA PACIENTES AFECTADOS
       rs1.Close
        sql = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_PLANIFICADA
        sql = sql & " WHERE PR37CODESTADO = " & constESTACT_CITADA
        sql = sql & " AND PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM CI0100 "
        sql = sql & " WHERE CI01SITCITA = '1' AND (" & sql1
        sql = sql & " AND AG11CODRECURSO = ? )"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngCodRecurso
        qry.Execute
        qry.Close
        sql = "UPDATE CI0100 SET CI01SITCITA = '4' WHERE AG11CODRECURSO = ? "
        sql = sql & " AND CI01SITCITA = '1' AND PR04NUMACTPLAN IN (SELECT"
        sql = sql & " PR0400.PR04NUMACTPLAN FROM CI0100,PR0400 WHERE "
        sql = sql & " PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
        sql = sql & " AND PR37CODESTADO = 1 AND (" & sql1
        sql = sql & " AND AG11CODRECURSO = ? AND CI01SITCITA = '1' )"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngCodRecurso
            qry(1) = qry(0)
        qry.Execute
        qry.Close
    End If

    'miramos las reservas
    sql = "SELECT TO_CHAR(CI01FECCONCERT,'DD/MM/YYYY HH24:MI'), PR08DESOBSERV"
    sql = sql & " FROM CI0100, PR0400, PR0800 WHERE AG11CODRECURSO = ? "
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN "
    sql = sql & " AND PR0400.PR03NUMACTPEDI = PR0800.PR03NUMACTPEDI "
    sql = sql & " AND PR37CODESTADO = " & constESTACT_CITADA
    sql = sql & " AND CI01SITCITA = '5' AND ("
    sql = sql & sql1
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngCodRecurso
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
    While Not rs.EOF
            If Not IsNull(rs(1)) Then
                msg = msg & rs(0) & "-> " & rs(1) & Chr$(13)
            Else
                msg = msg & rs(0) & Chr$(13)
            End If
            rs.MoveNext
        Wend
    End If
    MsgBox "Las siguientes reservas quedaran afectadas :" & Chr$(13) & msg, vbOKOnly, "Reservas"
    rs.Close
    qry.Close
End If 'RS.EOF (PERIODOS DEL ACTIVOS)
sql = "UPDATE AG0400 SET PR12CODACTIVIDAD = ?,AG04INDLUNFRJA = ?,AG04INDMARFRJA = ?,"
sql = sql & "AG04INDMIEFRJA = ?,AG04INDJUEFRJA = ?,AG04INDVIEFRJA = ?,"
sql = sql & "AG04INDSABFRJA = ?,AG04INDDOMFRJA = ?,AG04HORINFRJHH = ?,"
sql = sql & "AG04HORINFRJMM = ?,AG04HORFIFRJHH = ?,AG04HORFIFRJMM = ?,"
sql = sql & "AG04NUMCITADMI = ?,AG04INTERVCITA = ?,AG04MODASIGCITA = ?"
sql = sql & " WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? AND AG04CODFRANJA = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    If cboSSDBActividad.Text = "" Then qry(0) = Null Else qry(0) = cboSSDBActividad.Value
    If chkLunes.Value = 1 Then qry(1) = -1 Else qry(1) = 0
    If chkMartes.Value = 1 Then qry(2) = -1 Else qry(2) = 0
    If chkMiercoles.Value = 1 Then qry(3) = -1 Else qry(3) = 0
    If chkJueves.Value = 1 Then qry(4) = -1 Else qry(4) = 0
    If chkViernes.Value = 1 Then qry(5) = -1 Else qry(5) = 0
    If chkSabado.Value = 1 Then qry(6) = -1 Else qry(6) = 0
    If chkDomingo.Value = 1 Then qry(7) = -1 Else qry(7) = 0
    qry(8) = Format$(mskHoraIni.Text, "HH") 'hh inicio
    qry(9) = Right(mskHoraIni.Text, 2) 'mm inicio
    qry(10) = Format$(mskHoraFin.Text, "HH")
    qry(11) = Right(mskHoraFin.Text, 2) 'mm fin
    Select Case cboSSDBAsignacion.Value
        Case 1 'Asignacion por cantidad: grabamos las citas admitidas
            qry(12) = txtValor.Text
            qry(13) = Null
        Case 2 'secuencial tiende a desaparecer
            qry(12) = txtValor.Text
            qry(13) = Null
        Case 3, 4
            qry(12) = txtValor.Text
            qry(13) = DateDiff("n", mskHoraIni.Text, mskHoraFin.Text) / Val(txtValor.Text)
      End Select
    qry(14) = cboSSDBAsignacion.Value
    qry(15) = lngCodRecurso
    qry(16) = intCodPerfil
    qry(17) = intFranja 'CODIGO DE LAFRANJA
qry.Execute
qry.Close
objApp.CommitTrans
Screen.MousePointer = vbDefault

Exit Sub
Canceltrans:
    Screen.MousePointer = vbDefault

    MsgBox "La Frajan NO se ha modificado" & Chr$(13) & Error, vbExclamation, strMeCaption
    objApp.RollBackTrans
End Sub

Private Sub pCargarDatos()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim i As Integer

sql = "SELECT PR12CODACTIVIDAD,AG04INDLUNFRJA,AG04INDMARFRJA,"
sql = sql & "AG04INDMIEFRJA,AG04INDJUEFRJA,AG04INDVIEFRJA,"
sql = sql & "AG04INDSABFRJA,AG04INDDOMFRJA,AG04HORINFRJHH,"
sql = sql & "AG04HORINFRJMM,AG04HORFIFRJHH,AG04HORFIFRJMM,"
sql = sql & "AG04NUMCITADMI,AG04INTERVCITA,AG04MODASIGCITA "
sql = sql & " FROM AG0400 "
sql = sql & " WHERE AG11CODRECURSO = ? AND AG07CODPERFIL = ? AND AG04CODFRANJA = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngCodRecurso
    qry(1) = intCodPerfil
    qry(2) = intFranja 'CODIGO DE LAFRANJA
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    mskHoraIni.Text = Format$(rs!AG04HORINFRJHH & ":" & rs!AG04HORINFRJMM, "hh:mm")
    mskHoraFin.Text = Format$(rs!AG04HORFIFRJHH & ":" & rs!AG04HORFIFRJMM, "hh:mm")
    strHoraIni = Format$(rs!AG04HORINFRJHH & ":" & rs!AG04HORINFRJMM, "hh:mm")
    strHoraFin = Format$(rs!AG04HORFIFRJHH & ":" & rs!AG04HORFIFRJMM, "hh:mm")
    If Not IsNull(rs!PR12CODACTIVIDAD) Then
        cboSSDBActividad.MoveFirst
        Do While Trim(cboSSDBActividad.Columns(0).Value) <> Trim(rs!PR12CODACTIVIDAD)
            cboSSDBActividad.MoveNext
            i = i + 1
            If i > 20 Then Exit Do
        Loop
        cboSSDBActividad.Text = cboSSDBActividad.Columns(1).Value
    End If
    i = 0
    If rs!AG04INDLUNFRJA = 0 Then chkLunes.Value = 0 Else chkLunes.Value = 1
    If rs!AG04INDMARFRJA = 0 Then chkMartes.Value = 0 Else chkMartes.Value = 1
    If rs!AG04INDMIEFRJA = 0 Then chkMiercoles.Value = 0 Else chkMiercoles.Value = 1
    If rs!AG04INDJUEFRJA = 0 Then chkJueves.Value = 0 Else chkJueves.Value = 1
    If rs!AG04INDVIEFRJA = 0 Then chkViernes.Value = 0 Else chkViernes.Value = 1
    If rs!AG04INDSABFRJA = 0 Then chkSabado.Value = 0 Else chkSabado.Value = 1
    If rs!AG04INDDOMFRJA = 0 Then chkDomingo.Value = 0 Else chkDomingo.Value = 1
    cboSSDBAsignacion.MoveFirst
    Do While Trim(cboSSDBAsignacion.Columns(0).Value) <> Trim(rs!AG04MODASIGCITA)
        cboSSDBAsignacion.MoveNext
        i = i + 1
        If i > 20 Then Exit Do
    Loop
    cboSSDBAsignacion.Text = cboSSDBAsignacion.Columns(1).Value
    If Not IsNull(rs!AG04NUMCITADMI) Then txtValor = rs!AG04NUMCITADMI
End If

End Sub
