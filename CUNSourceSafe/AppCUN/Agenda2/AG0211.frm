VERSION 5.00
Begin VB.Form frmSelSexo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n de datos del paciente"
   ClientHeight    =   1185
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2880
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1185
   ScaleWidth      =   2880
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   1740
      TabIndex        =   3
      Top             =   660
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Sexo"
      ForeColor       =   &H00FF0000&
      Height          =   915
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1515
      Begin VB.OptionButton optSexo 
         Caption         =   "Mujer"
         Height          =   315
         Index           =   1
         Left            =   180
         TabIndex        =   2
         Top             =   540
         Width           =   915
      End
      Begin VB.OptionButton optSexo 
         Caption         =   "Hombre"
         Height          =   315
         Index           =   0
         Left            =   180
         TabIndex        =   1
         Top             =   240
         Value           =   -1  'True
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmSelSexo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
    If optSexo(0).Value = True Then
        Call objPipe.PipeSet("SEL_SEXO", constSEXO_HOMBRE)
    Else
        Call objPipe.PipeSet("SEL_SEXO", constSEXO_MUJER)
    End If
    Unload Me
End Sub
