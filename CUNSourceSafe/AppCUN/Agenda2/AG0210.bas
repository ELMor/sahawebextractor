Attribute VB_Name = "modEstructuras"
Option Explicit

'******* ESTRUCTURAS PARA CARGAR LOS HUECOS PARA CITACI�N ******
'horas para las que se han encontrado huecos
Public Type typeCHHoras
    strHoraIni As String
    strHoraFin As String
    strNHuecos As String
    intCodFranja As Integer
    intCodPerfil As Integer
End Type

'recursos para los que se buscan huecos
Public Type typeCHRec
    blnHueco As Boolean
    arHoras() As typeCHHoras
End Type

'actuaciones para las que se buscan huecos
Public Type typeCHAct
    blnHueco As Boolean
    arRec() As typeCHRec
End Type

'd�as para los que se busca huecos: se utiliza con un array de longitud fija
Public Type typeCHDias
    blnHueco As Boolean
    arAct() As typeCHAct
End Type

'******* ESTRUCTURAS COMPLEMENTARIAS PARA CITACI�N POR HUECOS ******
'datos de las franjas de un recurso
Public Type typeFranjas
    intCodFranja As Integer
    intCodPerfil As Integer
    strFecBaja As String
    intL As Integer
    intM As Integer
    intX As Integer
    intJ As Integer
    intV As Integer
    intS As Integer
    intD As Integer
    strHoraIni As String
    strHoraFin As String
    strModo As String
    intInterv As Integer
    intNumCitaAdmi As Integer
    intNumCitaActAdmi As Integer
End Type

'datos de las citas de un recurso
Public Type typeDatosCita
    strHora As String
    lngNumMinOcu As Long
    strCodAct As String
End Type

Public Type typeCitas
    strFecha As String
    arCitaDatos() As typeDatosCita
End Type

'intervalos de una franja citable por intervalos
Public Type typeIntervalos
    strHoraIni As String
    strHoraFin As String
    blnOcupado As Boolean
End Type

'******* OTRAS ESTRUCTURAS ******
'datos para la impresi�n de actuaciones programadas del paciente
Public Type typeDatos
    strDpto As String
    strRec As String
    intCodActiv As Integer
    lngNAPlan As Long
End Type

