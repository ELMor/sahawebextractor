VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmCitaUrgencias 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Citaci�n (Consultas de Urgencias)"
   ClientHeight    =   3180
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6180
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3180
   ScaleWidth      =   6180
   StartUpPosition =   2  'CenterScreen
   Begin vsViewLib.vsPrinter vs 
      Height          =   195
      Left            =   5460
      TabIndex        =   20
      Top             =   660
      Visible         =   0   'False
      Width           =   555
      _Version        =   196608
      _ExtentX        =   979
      _ExtentY        =   344
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      MarginLeft      =   0
      MarginRight     =   0
      MarginTop       =   0
      MarginBottom    =   0
   End
   Begin VB.CommandButton cmdetiquetas 
      Caption         =   "E&tiquetas"
      Height          =   555
      Left            =   4980
      TabIndex        =   19
      Top             =   1620
      Width           =   1155
   End
   Begin VB.CommandButton cmdRealizacionAct 
      Caption         =   "&Realizaci�n Actuaciones"
      Height          =   555
      Left            =   4980
      TabIndex        =   18
      Top             =   900
      Width           =   1110
   End
   Begin VB.Frame Frame3 
      Caption         =   "Actuaci�n"
      ForeColor       =   &H00C00000&
      Height          =   735
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   4875
      Begin SSDataWidgets_B.SSDBCombo cboAct 
         Height          =   315
         Left            =   1020
         TabIndex        =   16
         Top             =   270
         Width           =   3645
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5927
         Columns(1).Caption=   "Act"
         Columns(1).Name =   "Act"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ReqDoc"
         Columns(2).Name =   "ReqDoc"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6429
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin VB.Label Label1 
         Caption         =   "Actuaci�n:"
         Height          =   255
         Index           =   2
         Left            =   180
         TabIndex        =   17
         Top             =   345
         Width           =   795
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Imprimir"
      ForeColor       =   &H00C00000&
      Height          =   735
      Left            =   60
      TabIndex        =   11
      Top             =   2340
      Width           =   4815
      Begin VB.CheckBox chkHojaFiliacion 
         Caption         =   "Hoja de Filiaci�n"
         Height          =   195
         Left            =   360
         TabIndex        =   14
         Top             =   300
         Width           =   1695
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "&Imprimir"
         Height          =   375
         Left            =   3780
         TabIndex        =   13
         Top             =   240
         Width           =   915
      End
      Begin VB.CheckBox chkHojaVerde 
         Caption         =   "Hoja Verde"
         Height          =   195
         Left            =   2280
         TabIndex        =   12
         Top             =   300
         Width           =   1155
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Paciente"
      ForeColor       =   &H00C00000&
      Height          =   1530
      Left            =   60
      TabIndex        =   3
      Top             =   780
      Width           =   4815
      Begin VB.CommandButton cmdVisionGlobal 
         Caption         =   "&Visi�n Global"
         Enabled         =   0   'False
         Height          =   495
         Left            =   3780
         TabIndex        =   4
         Top             =   240
         Width           =   855
      End
      Begin VB.Frame fraPac 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   1275
         Left            =   60
         TabIndex        =   5
         Top             =   180
         Width           =   4695
         Begin VB.TextBox txtNH 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   720
            TabIndex        =   0
            Top             =   240
            Width           =   795
         End
         Begin VB.TextBox txtPac 
            BackColor       =   &H00C0C0C0&
            Height          =   315
            Left            =   720
            Locked          =   -1  'True
            TabIndex        =   8
            Top             =   840
            Width           =   3930
         End
         Begin VB.CommandButton cmdBuscarPac 
            Caption         =   "&Buscar"
            Height          =   375
            Left            =   2340
            TabIndex        =   7
            Top             =   180
            Width           =   810
         End
         Begin VB.TextBox txtCodPers 
            Height          =   315
            Left            =   60
            TabIndex        =   6
            Top             =   60
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.Label Label1 
            Caption         =   "N� Hist.:"
            Height          =   255
            Index           =   0
            Left            =   60
            TabIndex        =   10
            Top             =   300
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Nombre:"
            Height          =   255
            Index           =   1
            Left            =   60
            TabIndex        =   9
            Top             =   900
            Width           =   615
         End
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4980
      TabIndex        =   2
      Top             =   2700
      Width           =   1110
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Citar"
      Height          =   375
      Left            =   4980
      TabIndex        =   1
      Top             =   120
      Width           =   1110
   End
   Begin Crystal.CrystalReport crtCrystalReport1 
      Left            =   5640
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
End
Attribute VB_Name = "frmCitaUrgencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strNH$, strAhora$
Dim strCodProc$, strCodAsist$
Dim strConsultasUrgencias$ 'CodAct1, CodAct2,... (para b�squeda de posibles consultas ya citadas)
Dim msgImpresos$

Private Sub cmdBuscarPac_Click()
    frmBuscaPersonas.Show vbModal
    Set frmBuscaPersonas = Nothing
    If objPipe.PipeExist("AG0214_CodPersona") Then
        Call pCargarPaciente(objPipe.PipeGet("AG0214_CodPersona"))
        Call objPipe.PipeRemove("AG0214_CodPersona")
        Call objPipe.PipeRemove("AG0214_NumHistoria")
    End If
End Sub

Private Sub cmdCitar_Click()
    Call pCitar
End Sub

Private Sub cmdetiquetas_Click()
Dim IntRes As Integer
Dim strFila As String

If Not Hay_Persona Then Exit Sub
'    IntRes = MsgBox("�Est� seguro de imprimir etiquetas de la persona?", vbQuestion + vbYesNoCancel, Me.Caption)
    strFila = Trim$(InputBox("�Est� seguro de imprimir etiquetas de la persona?" _
    & Chr$(13) & "Se imprimiran en la fila:", "Anular Actuaci�n", 1))
    If strFila <> "" Then
        If IsNumeric(strFila) Then
            If Val(strFila) >= 1 Or Val(strFila) <= 14 Then
               Call pImprimirEtiquetas(txtCodPers.Text, Val(strFila))
            Else
                MsgBox "La fila debe estar entre 1 y 14", vbInformation, Me.Caption
            End If
        Else
           MsgBox "La fila debe estar entre 1 y 14", vbInformation, Me.Caption
        End If
    End If
End Sub
Public Function Hay_Persona() As Boolean
If txtPac.Text <> "" Then
    Hay_Persona = True
Else
    MsgBox "No hay ninguna persona seleccionada", vbInformation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Hay_Persona = False
End If
End Function

Private Sub pImprimirEtiquetas(CodPersona As String, intF As Integer)
Dim fila As Integer
Dim col As Integer
Dim intPosX As Integer
Dim intPosY As Integer
Dim intIncX As Integer
Dim intIncY As Integer
Dim intposYIni As Integer
Dim X As Integer
Dim strnombre As String
Dim strFecha As String
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim strNombre2 As String

intposYIni = 200
intPosX = 45
intPosY = 50
intIncX = 2925
intIncY = 1200
vs.StartDoc
vs.CurrentX = intPosX
intPosY = intPosY + (intIncY * (intF - 1))
vs.CurrentY = intPosY

'datos previos
sql = "SELECT CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI22FECNACIM FROM CI2200 WHERE "
sql = sql & "CI21CODPERSONA = ? "
'
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = CodPersona
Set rs = qry.OpenResultset()
If rs.EOF Then
    MsgBox "Persona Fisica inexistente", vbInformation + vbOKOnly, Right(Me.Caption, Len(Me.Caption) - 6)
    Exit Sub
End If
If Not IsNull(rs!CI22SEGAPEL) Then
    strnombre = rs!CI22PRIAPEL & " " & rs!CI22SEGAPEL & ", " & rs!CI22NOMBRE
Else
  strnombre = rs!CI22PRIAPEL & ", " & rs!CI22NOMBRE
End If
If Len(strnombre) > 25 Then
    strNombre2 = Right(strnombre, Len(strnombre) - 25)
    strnombre = Left(strnombre, 25)
End If
If Not IsNull(rs!CI22FECNACIM) Then
    strFecha = Format(rs!CI22FECNACIM, "dd/mm/yyyy")
Else
    strFecha = ""
End If
If Len(strNombre2 & " " & strFecha) > 25 Then _
strNombre2 = Left(strNombre2, 14)
rs.Close
qry.Close
For fila = 1 To 3
    For col = 1 To 4
      X = vs.CurrentX
      vs.FontSize = 12
      vs.Text = "Hist: "
      vs.FontBold = True
      vs.Text = txtNH.Text
      vs.FontBold = False
      vs.Text = Chr$(13)
      vs.CurrentX = X
      vs.FontSize = 8
      vs.Text = strnombre
      vs.Text = Chr$(13)
      vs.CurrentX = X
      vs.Text = strNombre2 & " " & strFecha
      X = X + intIncX
      vs.CurrentX = X
      vs.CurrentY = intPosY
    Next col
    vs.CurrentX = intPosX
    intPosY = intPosY + intIncY
    If intPosY > 16000 Then
        intPosY = intposYIni
        vs.EndDoc
        vs.PrintDoc
        vs.StartDoc
    End If
    vs.CurrentY = intPosY
Next fila
vs.EndDoc
vs.PrintDoc
End Sub


Private Sub cmdImprimir_Click()
    Dim msg$
    Call pImprimirHojas
    If msgImpresos <> "" Then MsgBox msgImpresos, vbInformation, Me.Caption
End Sub

Private Sub cmdRealizacionAct_Click()
    Dim vntdatos(1 To 1) As Variant
    
    'Acceso a la Realizaci�n de Actuaciones
    vntdatos(1) = constDPTO_URGENCIAS
    Call objSecurity.LaunchProcess("PR0500", vntdatos)
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdVisionGlobal_Click()
    Dim vntData()
    If txtCodPers.Text <> "" Then
        Screen.MousePointer = vbHourglass
        ReDim vntData(1 To 2) As Variant
        vntData(1) = txtCodPers.Text
        vntData(2) = 2
        Call objSecurity.LaunchProcess("HC02", vntData())
        Screen.MousePointer = vbDefault
    End If
End Sub

Private Sub Form_Load()
    Call pCargarActuaciones
    If objPipe.PipeExist("AG0215_CI21CODPERSONA") Then
        txtNH.BackColor = objApp.objUserColor.lngReadOnly
'        txtNH.Locked = True
        fraPac.Enabled = False
        Call pCargarPaciente(objPipe.PipeGet("AG0215_CI21CODPERSONA"))
        Call objPipe.PipeRemove("AG0215_CI21CODPERSONA")
    Else
        txtNH.BackColor = objApp.objUserColor.lngNormal
'        txtNH.Locked = False
    End If
End Sub










Private Sub txtNH_LostFocus()
    If strNH <> txtNH.Text Then
        If txtNH.Text = "" Then
            strNH = ""
            txtCodPers.Text = ""
            txtPac.Text = ""
            strCodProc = ""
            strCodAsist = ""
            cmdVisionGlobal.Enabled = False
        Else
            Call pCargarPaciente(txtNH.Text, True)
        End If
    End If
End Sub

Private Sub txtNH_GotFocus()
    txtNH.SelStart = 0
    txtNH.SelLength = Len(txtNH.Text)
End Sub

Private Sub txtNH_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case 8
    Case 13: SendKeys "{TAB}"
    Case Is < Asc("0"), Is > Asc("9"): KeyAscii = 0
    End Select
End Sub

Private Function fBuscarProceso(strCodDpto$, strCodDr$) As String
'************************************************************************************
'*  Devuelve el proceso al que hay que asociar la petici�n
'*  Si existe un proceso activo para el Dpto/Dr solicitante, se toma �ste. Si no existe,
'*  se crea
'************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strCodProc$
    
    sql = "SELECT AD0700.AD07CODPROCESO"
    sql = sql & " FROM AD0400, AD0700"
    sql = sql & " WHERE CI21CODPERSONA = ?"
    sql = sql & " AND SYSDATE BETWEEN AD07FECHORAINICI AND NVL(AD07FECHORAFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    sql = sql & " AND AD0400.AD07CODPROCESO = AD0700.AD07CODPROCESO"
    sql = sql & " AND AD02CODDPTO = ?"
    sql = sql & " AND SG02COD = ?"
    sql = sql & " AND AD04FECFINRESPON IS NULL"
    sql = sql & " ORDER BY AD0700.AD07CODPROCESO DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = txtCodPers.Text
    qry(1) = strCodDpto
    qry(2) = strCodDr
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        strCodProc = rs!AD07CODPROCESO
    Else
        strCodProc = fNextClave("AD07CODPROCESO")
        sql = "INSERT INTO AD0700 (CI21CODPERSONA, AD07CODPROCESO, AD07FECHORAINICI,"
        sql = sql & " CI22NUMHISTORIA, AD34CODESTADO)"
        sql = sql & " VALUES (?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ?, 1)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = txtCodPers.Text
        qry(1) = strCodProc
        qry(2) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
        qry(3) = strNH 'siempre debe existir n� de historia ya que si el paciente es nuevo, se la habr� creado
        qry.Execute
        
        sql = "INSERT INTO AD0400 (AD07CODPROCESO, AD04FECINIRESPON, AD02CODDPTO, SG02COD)"
        sql = sql & " VALUES(?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ?, ?)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = strCodProc
        qry(1) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
        qry(2) = strCodDpto
        qry(3) = strCodDr
        qry.Execute
    End If
    
    fBuscarProceso = strCodProc
End Function

Private Sub pCargarActuaciones()
'****************************************************************************************
'*  Se cargan todas las Consultas de Urgencias definidas
'*  Se selecciona por defecto la Consulta de Urgencias (4293) que es la m�s habitual
'****************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim i%
    
    sql = "SELECT DISTINCT PR0100.PR01CODACTUACION, PR01DESCORTA, PR01INDREQDOC"
    sql = sql & " FROM PR0100, PR0200"
    sql = sql & " WHERE PR0100.PR12CODACTIVIDAD = ?"
    sql = sql & " AND PR01FECINICO <= SYSDATE"
    sql = sql & " AND (PR01FECFIN >= SYSDATE OR PR01FECFIN IS NULL)"
    sql = sql & " AND PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0200.AD02CODDPTO = ?"
    sql = sql & " ORDER BY PR01DESCORTA"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = constACTIV_CONSULTA
    qry(1) = constDPTO_URGENCIAS
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        strConsultasUrgencias = strConsultasUrgencias & rs!PR01CODACTUACION & ","
        cboAct.AddItem rs!PR01CODACTUACION & Chr$(9) & rs!PR01DESCORTA & Chr$(9) & rs!PR01INDREQDOC
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    For i = 1 To cboAct.Rows
        If i = 1 Then cboAct.MoveFirst Else cboAct.MoveNext
        If cboAct.Columns("Cod").Text = constURGENCIAS_PR_CONSULTA Then
            cboAct.Text = cboAct.Columns("Act").Text
            Exit For
        End If
    Next i
    If strConsultasUrgencias <> "" Then
        strConsultasUrgencias = Left$(strConsultasUrgencias, Len(strConsultasUrgencias) - 1)
    End If
    
    cboAct.BackColor = objApp.objUserColor.lngMandatory
End Sub

Private Sub pCargarPaciente(strIdentifPac$, Optional blnNH As Boolean)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    
    'se inicializa el P/A para la impresi�n de Hojas
    strCodProc = "": strCodAsist = ""
    
    'datos del paciente
    sql = "SELECT CI21CODPERSONA, CI22NUMHISTORIA,"
    sql = sql & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC"
    sql = sql & " FROM CI2200"
    If blnNH Then
        sql = sql & " WHERE CI22NUMHISTORIA = ?"
    Else
        sql = sql & " WHERE CI21CODPERSONA = ?"
    End If
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strIdentifPac
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        txtCodPers.Text = rs!CI21CODPERSONA
        If Not IsNull(rs!CI22NUMHISTORIA) Then txtNH.Text = rs!CI22NUMHISTORIA Else txtNH.Text = ""
        txtPac.Text = rs!PAC
        cmdVisionGlobal.Enabled = True
    Else
        txtCodPers.Text = ""
        txtNH.Text = ""
        txtPac.Text = ""
        cmdVisionGlobal.Enabled = False
    End If
    rs.Close
    qry.Close
    strNH = txtNH.Text
End Sub

Private Sub pCitar()
'***************************************************************************************
'*  Genera en la base de datos la Petici�n y la Cita de una Consulta de Urgencias inmediata
'***************************************************************************************
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strNumPetic$, lngNAPedi&, lngNAPlan&, strNumSolicit$
    Dim strCodPers$, strCodDpto$, strCodAct$, intReqDoc%, strCodRec$, strDr$
    Dim intNPet%, intNCita%
    Dim vntData(1 To 7)
    
    'se comprueba que hay un paciente seleccionado
    If txtCodPers.Text = "" Then
        sql = "No se ha seleccionado ning�n paciente para citar."
        MsgBox sql, vbExclamation, Me.Caption
        Exit Sub
    End If
    
    strAhora = fAhora
    strCodPers = txtCodPers.Text
    strCodDpto = constDPTO_URGENCIAS
    strCodAct = cboAct.Columns("Cod").Text
    intReqDoc = cboAct.Columns("ReqDoc").Text
    strCodRec = constURGENCIAS_REC_CONSCOLURG
    strDr = constURGENCIAS_DR_CONSCOLURG
    intNPet = 1
    intNCita = 1
        
    'se comprueba que el paciente no tenga ya una Consulta de Urgencias citada o realiz�ndose
    sql = "SELECT CI01FECCONCERT, PR04FECINIACT, PR0400.PR37CODESTADO"
    sql = sql & " FROM CI0100, PR0400"
    sql = sql & " WHERE PR0400.CI21CODPERSONA = ?"
    sql = sql & " AND PR0400.AD02CODDPTO = ?"
    sql = sql & " AND PR0400.PR01CODACTUACION IN (" & strConsultasUrgencias & ")"
    sql = sql & " AND PR0400.PR37CODESTADO IN (" & constESTACT_CITADA & "," & constESTACT_REALIZANDOSE & ")"
    sql = sql & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
    sql = sql & " AND CI0100.CI01SITCITA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodPers
    qry(1) = strCodDpto
    qry(2) = constESTCITA_CITADA
    Set rs = qry.OpenResultset()
    sql = ""
    If Not rs.EOF Then
        Select Case rs!PR37CODESTADO
        Case constESTACT_CITADA
            sql = "El paciente tiene ya una Consulta de Urgencias citada para el " & rs!CI01FECCONCERT & "."
        Case constESTACT_REALIZANDOSE
            sql = "El paciente tiene ya una Consulta de Urgencias realiz�ndose desde el " & rs!PR04FECINIACT & "."
        End Select
    End If
    rs.Close
    qry.Close
    If sql = "" Then
        'se pregunta al usuario para que confirme que quiere realizar la operaci�n
        sql = "�Desea Ud. citar una " & cboAct.Text & " para el paciente seleccionado?"
        If MsgBox(sql, vbQuestion + vbYesNo, Me.Caption) = vbNo Then Exit Sub
    Else
        MsgBox sql, vbExclamation, Me.Caption
        Exit Sub
    End If
        
    Screen.MousePointer = vbHourglass
    On Error GoTo label
    objApp.BeginTrans
    
    'si el paciente todav�a no tiene n� de historia, se genera
    If strNH = "" Then
        'CI2200
        sql = "UPDATE CI2200 SET CI22NUMHISTORIA = ?"
        sql = sql & " WHERE CI21CODPERSONA = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        strNH = fNextClave("CI22NUMHISTORIA")
        txtNH.Text = strNH
        Me.Refresh
        qry(0) = strNH
        qry(1) = strCodPers
        qry.Execute
        qry.Close
        
        'AR0400: archivo I
        sql = "INSERT INTO AR0400 (CI22NUMHISTORIA, AR00CODTIPOLOGIA, AR04FECINICIO, AR06CODESTADO)"
        sql = sql & " VALUES (?, '01', TO_DATE(?,'DD/MM/YYYY HH24:MI'), 'A')"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = strNH
        qry(1) = Format(strAhora, "dd/mm/yyyy hh:mm")
        qry.Execute
        qry.Close
        
        'AR0700: archivo II
        sql = "INSERT INTO AR0700 (AR04FECINICIO, CI22NUMHISTORIA, AD02CODDPTO, "
        sql = sql & " AR07FECMVTO, AR00CODTIPOLOGIA, SG02COD)"
        sql = sql & " VALUES (TO_DATE(?,'DD/MM/YYYY HH24:MI'), ?, ?,"
        sql = sql & " TO_DATE(?,'DD/MM/YYYY HH24: MI '), '01', ?)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Format(strAhora, "dd/mm/yyyy hh:mm")
        qry(1) = strNH
        qry(2) = strCodDpto
        qry(3) = Format(strAhora, "dd/mm/yyyy hh:mm")
        qry(4) = objSecurity.strUser
        qry.Execute
        qry.Close
    End If
    
    'PR0900: Petici�n
    sql = "INSERT INTO PR0900 (PR09NUMPETICION, CI21CODPERSONA, AD02CODDPTO, SG02COD,"
    sql = sql & " PR09FECPETICION, PR09NUMGRUPO)"
    sql = sql & " VALUES (?, ?, ?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    strNumPetic = fNextClave("PR09NUMPETICION")
    qry(0) = strNumPetic
    qry(1) = strCodPers
    qry(2) = strCodDpto
    qry(3) = strDr
    qry(4) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
    qry(5) = strNumPetic
    qry.Execute
    qry.Close

    'PR0300: Actuaci�n pedida
    sql = "INSERT INTO PR0300 (PR03NUMACTPEDI, AD02CODDPTO, CI21CODPERSONA, PR09NUMPETICION,"
    sql = sql & " PR01CODACTUACION, PR03INDCONSFIRM, PR03INDCITABLE, PR03INDCITAANT)"
    sql = sql & " VALUES (?, ?, ?, ?, ?, 0, -1, -1)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    lngNAPedi = fNextClave("PR03NUMACTPEDI")
    strCodProc = fBuscarProceso(strCodDpto, constURGENCIAS_DR_PASTRANA)
    qry(0) = lngNAPedi
    qry(1) = strCodDpto
    qry(2) = strCodPers
    qry(3) = strNumPetic
    qry(4) = strCodAct
    qry.Execute
    qry.Close

    'PR0800: Petici�n - Actuaci�n pedida
    sql = "INSERT INTO PR0800 (PR09NUMPETICION, PR03NUMACTPEDI, PR08NUMSECUENCIA, AD07CODPROCESO)"
    sql = sql & " VALUES (?, ?, ?, ?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNumPetic
    qry(1) = lngNAPedi
    qry(2) = intNPet
    qry(3) = strCodProc
    qry.Execute
    qry.Close

    'PR0600: Fase pedida
    sql = "INSERT INTO PR0600 (PR03NUMACTPEDI, PR06NUMFASE, PR06DESFASE, PR06NUMMINOCUPAC,"
    sql = sql & " PR06NUMFASE_PRE, PR06NUMMINFPRE, PR06NUMMAXFPRE, PR06INDHABNATU, PR06INDINIFIN) "
    sql = sql & " SELECT ?, PR05NUMFASE, PR05DESFASE, PR05NUMOCUPACI, PR05NUMFASE_PRE,"
    sql = sql & " PR05NUMTMINFPRE, PR05NUMTMAXFPRE, PR05INDHABILNATU, PR05INDINICIOFIN"
    sql = sql & " FROM PR0500"
    sql = sql & " WHERE PR01CODACTUACION = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNAPedi
    qry(1) = strCodAct
    qry.Execute
    qry.Close
    
    'PR1400: Tipo recurso pedido
    sql = "INSERT INTO PR1400 (PR03NUMACTPEDI, PR06NUMFASE, PR14NUMNECESID, AG14CODTIPRECU,"
    sql = sql & " AG11CODRECURSO, AD02CODDPTO, PR14NUMUNIREC, PR14NUMMINOCU, PR14NUMMINDESREC,"
    sql = sql & " PR14INDRECPREFE, PR14INDPLANIF)"
    sql = sql & " SELECT ?, PR05NUMFASE, PR13NUMNECESID, AG14CODTIPRECU, ?, ?,"
    sql = sql & " PR13NUMUNIREC, PR13NUMTIEMPREC, PR13NUMMINDESF, PR13INDPREFEREN, PR13INDPLANIF"
    sql = sql & " FROM PR1300"
    sql = sql & " WHERE PR01CODACTUACION = ?"
''    SQL = SQL & " AND PR14INDRECPREFE = -1"
''    SQL = SQL & " AND PR14INDPLANIF = -1"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngNAPedi
    qry(1) = strCodRec
    qry(2) = strCodDpto 'se coge strCodDpto en lugar de AD02CODDPTO de PR1300 ya que puede ser NULL
    qry(3) = strCodAct
    qry.Execute
    qry.Close

    'PR0400: Actuaci�n planificada
    sql = "INSERT INTO PR0400 (PR04NUMACTPLAN, PR01CODACTUACION, AD02CODDPTO, CI21CODPERSONA,"
    sql = sql & " PR37CODESTADO, PR03NUMACTPEDI, PR04INDREQDOC, AD07CODPROCESO, PR04FECENTRCOLA)"
    sql = sql & " VALUES (?, ?, ?, ?, ?, ?, ?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'))"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    lngNAPlan = fNextClave("PR04NUMACTPLAN")
    qry(0) = lngNAPlan
    qry(1) = strCodAct
    qry(2) = strCodDpto
    qry(3) = strCodPers
    qry(4) = constESTACT_CITADA
    qry(5) = lngNAPedi
    qry(6) = intReqDoc
    qry(7) = strCodProc
    qry(8) = Format(strAhora, "dd/mm/yyyy hh:mm:ss")
    qry.Execute
    qry.Close
    
    'PR4400
    sql = "UPDATE PR4400 SET PR44NUMCUENTA = PR44NUMCUENTA + 1"
    sql = sql & " WHERE AD02CODDPTO = ?"
    sql = sql & " AND PR01CODACTUACION = ?"
    sql = sql & " AND AD02CODDPTO_REA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodDpto
    qry(1) = strCodAct
    qry(2) = strCodDpto
    qry.Execute
    If qry.RowsAffected = 0 Then
        sql = "INSERT INTO PR4400 (AD02CODDPTO, PR01CODACTUACION, AD02CODDPTO_REA, PR44NUMCUENTA)"
        sql = sql & " VALUES (?, ?, ?, 1)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = strCodDpto
        qry(1) = strCodAct
        qry(2) = strCodDpto
        qry.Execute
    End If
    qry.Close
    
    'CI3100: a�adir la solicitud
    sql = "INSERT INTO CI3100 (CI31NUMSOLICIT, CI31INDLUNPREF, CI31INDMARPREF, CI31INDMIEPREF,"
    sql = sql & " CI31INDJUEPREF, CI31INDVIEPREF, CI31INDSABPREF, CI31INDDOMPREF)"
    sql = sql & " VALUES (?, 1, 1, 1, 1, 1, 1, 1)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    strNumSolicit = fNextClave("CI31NUMSOLICIT")
    qry(0) = strNumSolicit
    qry.Execute
    qry.Close
    
    'CI0100: a�adir cita
    sql = "INSERT INTO CI0100 (CI31NUMSOLICIT, CI01NUMCITA, AG11CODRECURSO, PR04NUMACTPLAN,"
    sql = sql & " CI01FECCONCERT, CI01SITCITA, CI01INDRECORDA, CI01TIPRECORDA, CI01NUMENVRECO,"
    sql = sql & " CI01INDLISESPE, CI01INDASIG)"
    sql = sql & " VALUES (?, ?, ?, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'), ?, -1, 1, 1, 0, -1)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNumSolicit
    qry(1) = intNCita
    qry(2) = strCodRec
    qry(3) = lngNAPlan
    qry(4) = Format(strAhora, "dd/mm/yyyy hh:mm")
    qry(5) = constESTCITA_CITADA
    qry.Execute
    qry.Close

    'CI1500: a�adir fase citada
    sql = "INSERT INTO CI1500 (CI31NUMSOLICIT, CI01NUMCITA, CI15FECCONCPAC, CI15NUMFASECITA,"
    sql = sql & " CI15DESFASECITA, CI15NUMDIASPAC, CI15NUMHORAPAC,"
    sql = sql & " CI15NUMMINUPAC)"
    sql = sql & " SELECT ?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI'), PR05NUMFASE,"
    sql = sql & " PR05DESFASE, FLOOR(PR05NUMOCUPACI/1440), FLOOR(MOD(PR05NUMOCUPACI,1440)/60),"
    sql = sql & " MOD(MOD(PR05NUMOCUPACI,1440),60)"
    sql = sql & " FROM PR0500"
    sql = sql & " WHERE PR01CODACTUACION = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNumSolicit
    qry(1) = intNCita
    qry(2) = Format(strAhora, "dd/mm/yyyy hh:mm")
    qry(3) = strCodAct
    qry.Execute
    qry.Close

    'CI2700: A�adir recurso citado
    sql = "INSERT INTO CI2700 (CI31NUMSOLICIT, CI01NUMCITA, CI15NUMFASECITA, AG11CODRECURSO,"
    sql = sql & " CI27FECOCUPREC, CI27NUMDIASREC, CI27NUMHORAREC, CI27NUMMINUREC)"
    sql = sql & " SELECT ?, ?, PR05NUMFASE, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI'),"
    sql = sql & " FLOOR(PR13NUMTIEMPREC/1440), FLOOR(MOD(PR13NUMTIEMPREC,1440)/60),"
    sql = sql & " MOD(MOD(PR13NUMTIEMPREC,1440),60)"
    sql = sql & " FROM PR1300"
    sql = sql & " WHERE PR01CODACTUACION = ?"
    sql = sql & " AND PR13INDPREFEREN = -1"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strNumSolicit
    qry(1) = intNCita
    qry(2) = strCodRec
    qry(3) = Format(strAhora, "dd/mm/yyyy hh:mm")
    qry(4) = strCodAct
    qry.Execute
    qry.Close

    'Se asocia la Asistencia
    'Se busca si existe un P/A abierto, y si no, hay que crear la asistencia
    strCodAsist = ""
    sql = "SELECT AD01CODASISTENCI"
    sql = sql & " FROM AD0800"
    sql = sql & " WHERE AD07CODPROCESO = ?"
    sql = sql & " AND AD08FECFIN IS NULL"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strCodProc
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then strCodAsist = rs(0)
    rs.Close
    qry.Close
    
    If strCodAsist <> "" Then 'si ya tiene un P/A abierto...
        '... se asocia la Asistencia a la prueba
        sql = "UPDATE PR0400 SET AD01CODASISTENCI = ?"
        sql = sql & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = strCodAsist
        qry(1) = lngNAPlan
        qry.Execute
    Else 'si no tiene un P/A abierto...
        '... se crea la Asistencia y se asocia
        vntData(1) = strCodPers
        vntData(2) = lngNAPedi
        vntData(7) = lngNAPlan
        vntData(3) = strCodProc
        vntData(4) = strNH
        vntData(5) = constURGENCIAS_DPTO_NAME
        vntData(6) = constURGENCIAS_DR_PASTRANA_NAME
        vntData(7) = lngNAPlan
        Call objSecurity.LaunchProcess("AD1020", vntData)
        'se comprueba que se ha asociado la asistencia
        sql = "SELECT AD01CODASISTENCI FROM PR0400 WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNAPlan
        Set rs = qry.OpenResultset()
        If IsNull(rs(0)) Then
            Screen.MousePointer = vbDefault
            objApp.RollBackTrans
            sql = "Es necesario asociar una asistencia para poder realizar la cita."
            MsgBox sql, vbExclamation, Me.Caption
            Exit Sub
        Else
            strCodAsist = rs(0)
        End If
    End If

label:
    Screen.MousePointer = vbDefault
    If Err > 0 Then
        objApp.RollBackTrans
        sql = "Se ha producido un error al realizar la cita."
        sql = sql & Chr$(13)
        sql = sql & "Int�ntelo de nuevo."
        MsgBox sql, vbExclamation, Me.Caption
    Else
        objApp.CommitTrans
        Call pImprimirHojas
        sql = "Cita realizada correctamente."
        If msgImpresos <> "" Then sql = sql & Chr$(13) & Chr$(13) & msgImpresos
        MsgBox sql, vbInformation, Me.Caption
'        Unload Me
    End If
End Sub

Private Sub pImprimirHojas()
    Dim strWhere$
   
    Screen.MousePointer = vbHourglass
    msgImpresos = "" 'Informaci�n sobre los listados que se han sacado
    
    'Por si se quieren volver imprimir las Hojas de una Consulta que se cit� anteriormente _
    se busca el P/A correspondiente
    If (strCodAsist = "" Or strCodProc = "") And txtCodPers.Text <> "" Then
        Dim sql$, qry As rdoQuery, rs As rdoResultset
        sql = "SELECT PR0400.AD01CODASISTENCI, PR0400.AD07CODPROCESO"
        sql = sql & " FROM CI0100, PR0400"
        sql = sql & " WHERE PR0400.CI21CODPERSONA = ?"
        sql = sql & " AND PR0400.AD02CODDPTO = ?"
        sql = sql & " AND PR0400.PR01CODACTUACION IN (" & strConsultasUrgencias & ")"
        sql = sql & " AND PR0400.PR37CODESTADO IN (" & constESTACT_CITADA & "," & constESTACT_REALIZANDOSE & ")"
        sql = sql & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
        sql = sql & " AND CI0100.CI01SITCITA = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = txtCodPers.Text
        qry(1) = constDPTO_URGENCIAS
        qry(2) = constESTCITA_CITADA
        Set rs = qry.OpenResultset()
        If Not rs.EOF Then
            strCodProc = rs!AD07CODPROCESO
            strCodAsist = rs!AD01CODASISTENCI
        End If
        rs.Close
        qry.Close
    End If
    
    If strCodAsist <> "" And strCodProc <> "" Then
        'Hoja de Filiaci�n
        If Not chkHojaFiliacion.Value = 0 Then
            strWhere = "{AD0823J.AD01CODASISTENCI} = " & strCodAsist & _
                    " AND {AD0823J.AD07CODPROCESO} = " & strCodProc
            With crtCrystalReport1
                .ReportFileName = objApp.strReportsPath & "filiacion2.rpt"
                .PrinterCopies = 1
                .Destination = crptToPrinter
                .SelectionFormula = objGen.ReplaceStr(strWhere, "#", Chr(34), 0)
                .Destination = crptToPrinter
                .Connect = objApp.rdoConnect.Connect
                .DiscardSavedData = True
                .Action = 1
            End With
            msgImpresos = msgImpresos & "Se ha impreso la Hoja de Filiaci�n." & Chr$(13)
        End If
        
        'Hoja Verde
        If Not chkHojaVerde.Value = 0 Then
            strWhere = "{AD0823J.AD01CODASISTENCI} = " & strCodAsist & _
                    " AND {AD0823J.AD07CODPROCESO} = " & strCodProc
            With crtCrystalReport1
                .ReportFileName = objApp.strReportsPath & "hojaverde2.rpt"
                .PrinterCopies = 1
                .Destination = crptToPrinter
                .SelectionFormula = objGen.ReplaceStr(strWhere, "#", Chr(34), 0)
                .Destination = crptToPrinter
                .Connect = objApp.rdoConnect.Connect
                .DiscardSavedData = True
                .Action = 1
            End With
            msgImpresos = msgImpresos & "Se ha impreso la Hoja Verde." & Chr$(13)
        End If
    Else
        Screen.MousePointer = vbDefault
        MsgBox "No se ha impreso ninguna Hoja.", vbInformation, Me.Caption
    End If
    
    Screen.MousePointer = vbDefault
End Sub

Private Sub vsPrinter1_StartDoc()

End Sub
