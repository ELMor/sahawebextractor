Attribute VB_Name = "modFuncionesGenerales"
Option Explicit
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long 'para cargar los TreeViews a gran velocidad

Public Function fAhora() As String
'Devuelve la fecha y hora actual obtenida del servidor
    Dim SQL$, rs As rdoResultset
    
    SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS') FROM DUAL"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    fAhora = rs(0)
    rs.Close
End Function

Public Function fNextClave(strCampo$) As String
'Devuelve el pr�ximo n� de secuencia del campo strCampo
    Dim rs As rdoResultset, SQL$, strEndName$
    
    Select Case UCase(strCampo)
    Case "AD01CODASISTENCI", "AD07CODPROCESO", "AD18CODDOCUMENTO", "AD30CODCATEGORIA", "CI22NUMHISTORIA"
        strEndName = "_S"
    Case Else
        strEndName = "_SEQUENCE"
    End Select
    SQL = "SELECT " & strCampo & strEndName & ".NEXTVAL FROM DUAL"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    fNextClave = rs(0)
    rs.Close
End Function

Public Function fIsItemInCll(cll As Collection, strItem As String) As Boolean
'Determina si un item se en encuentra en una colecci�n
    Dim item
    
    For Each item In cll
        If item = strItem Then
            fIsItemInCll = True
            Exit Function
        End If
    Next
End Function

Public Sub pCabeceraDocCUN(vsp As vsPrinter, bmpEscudo As PictureBox)
    'escudo y cabecera
    vsp.X1 = 1700: vsp.Y1 = 800: vsp.X2 = 2900: vsp.Y2 = 2400
    vsp.Picture = bmpEscudo.Picture
    vsp.MarginLeft = 1000
    vsp.FontSize = 10
    vsp.CurrentX = 9000
    vsp.CurrentY = 1750
    vsp.Text = "Apartado 4209" & Chr$(13)
    vsp.CurrentX = 9000
    vsp.Text = "Tel�fono 948 25 54 00" & Chr$(13)
    vsp.CurrentY = 2200
    vsp.FontSize = 12
    vsp.Text = "CLINICA UNIVERSITARIA"
    vsp.FontSize = 10
    vsp.CurrentX = 9000
    vsp.Text = "Fax 948 29 65 00" & Chr$(13)
    vsp.CurrentX = vsp.CurrentX + 250
    vsp.Text = "FACULTAD DE MEDICINA"
    vsp.CurrentX = 9000
    vsp.Text = "31080 PAMPLONA" & Chr$(13)
    vsp.CurrentX = vsp.CurrentX + 100
    vsp.Text = "UNIVERSIDAD DE NAVARRA" & Chr$(13)
End Sub

Public Function fQuitarCRLF_Extremos(texto$) As String
    texto = Trim(texto)
    If texto <> "" Then
        Do While Asc(Right$(texto, 1)) = 10 Or Asc(Right$(texto, 1)) = 13
            texto = Left$(texto, Len(texto) - 1)
        Loop
        Do While Asc(Left$(texto, 1)) = 10 Or Asc(Left$(texto, 1)) = 13
            texto = Mid$(texto, 2)
        Loop
    End If
    fQuitarCRLF_Extremos = texto
End Function
