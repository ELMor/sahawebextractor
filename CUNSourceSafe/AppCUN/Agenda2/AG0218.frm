VERSION 5.00
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmImprimirCitas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Impresi�n de citas programadas del paciente"
   ClientHeight    =   3345
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9525
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3345
   ScaleWidth      =   9525
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox bmpEscudo 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   3840
      Picture         =   "AG0218.frx":0000
      ScaleHeight     =   315
      ScaleWidth      =   675
      TabIndex        =   24
      Top             =   3000
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.Frame Frame2 
      Caption         =   "Paciente"
      ForeColor       =   &H00C00000&
      Height          =   855
      Left            =   120
      TabIndex        =   18
      Top             =   0
      Width           =   6135
      Begin VB.TextBox txtNH 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   300
         Width           =   855
      End
      Begin VB.TextBox txtPac 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1140
         Locked          =   -1  'True
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   300
         Width           =   4830
      End
   End
   Begin VB.Frame Frame1 
      Height          =   855
      Left            =   6300
      TabIndex        =   16
      Top             =   0
      Width           =   2055
      Begin VB.OptionButton optActivar 
         Caption         =   "Activar Todas"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   240
         Value           =   -1  'True
         Width           =   1395
      End
      Begin VB.OptionButton optActivar 
         Caption         =   "Desactivar Anteriores"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   540
         Width           =   1815
      End
   End
   Begin vsViewLib.vsPrinter vsp 
      Height          =   345
      Left            =   300
      TabIndex        =   14
      Top             =   3000
      Visible         =   0   'False
      Width           =   8595
      _Version        =   196608
      _ExtentX        =   15161
      _ExtentY        =   609
      _StockProps     =   229
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1413783674
      TableSep        =   "|#"
      PageBorder      =   0
      TableBorder     =   0
      MarginLeft      =   500
      MarginRight     =   500
      MarginTop       =   500
      MarginBottom    =   500
      PhysicalPage    =   -1  'True
      Zoom            =   60
   End
   Begin VB.CommandButton cmdImprimir 
      Cancel          =   -1  'True
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   8460
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   60
      Width           =   915
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   8460
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   480
      Width           =   915
   End
   Begin VB.Frame fraActProg 
      Caption         =   "Actuaciones programadas"
      ForeColor       =   &H00C00000&
      Height          =   2235
      Left            =   120
      TabIndex        =   2
      Top             =   900
      Width           =   9255
      Begin VB.Frame fraContainer 
         BorderStyle     =   0  'None
         Height          =   1695
         Left            =   60
         TabIndex        =   3
         Top             =   480
         Width           =   9135
         Begin VB.VScrollBar vscNivel 
            Height          =   1275
            LargeChange     =   3
            Left            =   8880
            Max             =   10
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   60
            Width           =   240
         End
         Begin VB.Frame fraNivel 
            BorderStyle     =   0  'None
            Caption         =   "Frame3"
            Height          =   1515
            Index           =   0
            Left            =   60
            TabIndex        =   4
            Top             =   120
            Width           =   8835
            Begin VB.TextBox txtNoImprimir 
               Alignment       =   2  'Center
               BackColor       =   &H00C0C0C0&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   39
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   915
               Index           =   0
               Left            =   1740
               TabIndex        =   21
               TabStop         =   0   'False
               Text            =   "NO   IMPRIMIR"
               Top             =   300
               Visible         =   0   'False
               Width           =   7035
            End
            Begin VB.CheckBox chkImprimir 
               Height          =   195
               Index           =   0
               Left            =   420
               TabIndex        =   5
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.TextBox txtIP 
               Height          =   915
               Index           =   0
               Left            =   1740
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   1
               Top             =   300
               Width           =   7035
            End
            Begin VB.Label lblDpto 
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   0
               Left            =   1260
               TabIndex        =   22
               Top             =   0
               Width           =   2355
            End
            Begin VB.Line lnLine2 
               BorderColor     =   &H00C00000&
               BorderWidth     =   2
               Index           =   0
               X1              =   120
               X2              =   8760
               Y1              =   1380
               Y2              =   1380
            End
            Begin VB.Label lblIP 
               Caption         =   "Instrucciones paciente:"
               Height          =   195
               Index           =   0
               Left            =   60
               TabIndex        =   15
               Top             =   300
               Width           =   1695
            End
            Begin VB.Label lblFecha 
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   0
               Left            =   7260
               TabIndex        =   6
               Top             =   0
               Width           =   1515
            End
            Begin VB.Label lblAct 
               BorderStyle     =   1  'Fixed Single
               Height          =   255
               Index           =   0
               Left            =   3660
               TabIndex        =   13
               Top             =   0
               Width           =   3555
            End
         End
      End
      Begin VB.Label Label2 
         Caption         =   "Dpto."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   1380
         TabIndex        =   23
         Top             =   300
         Width           =   1935
      End
      Begin VB.Label Label2 
         Caption         =   "Cita"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   7380
         TabIndex        =   9
         Top             =   300
         Width           =   675
      End
      Begin VB.Label Label2 
         Caption         =   "Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   3780
         TabIndex        =   8
         Top             =   300
         Width           =   1935
      End
      Begin VB.Label Label2 
         Caption         =   "Imprimir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   360
         TabIndex        =   7
         Top             =   300
         Width           =   675
      End
   End
End
Attribute VB_Name = "frmImprimirCitas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim arDatos() As typeDatos
Dim strNAPlan$, lngCodPers&, strPac$

Private Const constNIVEL_NUMMAX As Integer = 4
Private Const constNIVEL_TOP As Long = 120
Private Const constNIVEL_HEIGHT As Long = 1515

Private Sub chkImprimir_Click(Index As Integer)
    If chkImprimir(Index).Value = 1 Then
        txtNoImprimir(Index).Visible = False
    Else
        txtNoImprimir(Index).Visible = True
    End If
    Me.Refresh
End Sub

Private Sub cmdImprimir_Click()
    Call pImprimir
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    On Error Resume Next
    txtIP(0).SetFocus
    On Error GoTo 0
End Sub

Private Sub Form_Load()
    Screen.MousePointer = vbHourglass
    
    'Se recogen las actuaciones citadas
    strNAPlan = objPipe.PipeGet("AG0218_PR04NUMACTPLAN")
    Call objPipe.PipeRemove("AG0218_PR04NUMACTPLAN")
    
    'Se cargan los datos del paciente
    Call pCargarPaciente
    
    'se cargan las actuaciones citadas del paciente
    Call pCargarCitasPac
    
    Screen.MousePointer = vbDefault
End Sub

Private Sub optActivar_Click(Index As Integer)
    If Index = 0 Then
        Call pActivarTodasCitas
    Else
        Call pDesactivarCitasAnteriores
    End If
End Sub

Private Sub txtIP_GotFocus(Index As Integer)
    On Error Resume Next
    If fraNivel(Index).Top >= fraContainer.Height Then
        vscNivel.Value = vscNivel.Value + ((fraNivel(Index).Top - constNIVEL_TOP) / constNIVEL_HEIGHT) - constNIVEL_NUMMAX + 1
    End If
    If fraNivel(Index).Top < fraContainer.Top Then
        vscNivel.Value = vscNivel.Value + (fraNivel(Index).Top - constNIVEL_TOP) / constNIVEL_HEIGHT
    End If
End Sub

Private Sub vscNivel_Change()
    Dim i%
    
    vscNivel.Max = fraNivel.Count - constNIVEL_NUMMAX
    For i = 0 To fraNivel.Count - 1
        fraNivel(i).Top = constNIVEL_TOP + i * constNIVEL_HEIGHT - vscNivel.Value * constNIVEL_HEIGHT
    Next i
End Sub

Private Sub pActivarTodasCitas()
    Dim iAct%
    
    For iAct = 0 To fraNivel.Count - 1
        chkImprimir(iAct).Value = 1
    Next iAct
End Sub

Private Sub pCargarCitasPac()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim iAct%, strPC$
        
    SQL = "SELECT PR0100.PR01DESCORTA, PR0100.PR01DESINSTRPACI, PR0100.PR12CODACTIVIDAD,"
    SQL = SQL & " CI01FECCONCERT, AG1100.AG11DESRECURSO, AD0200.AD02DESDPTO, PR0400.PR04NUMACTPLAN,"
    SQL = SQL & " DECODE(AD0200.AD02CODDPTO_UC,4,'Unidad de Coordinaci�n de la '||AD41DESSECCION,AD02DESDPTO) DPE"
    SQL = SQL & " FROM PR0300, PR0900, AD0200, AD4100, AG1100, PR0100, CI0100, PR0400"
    SQL = SQL & " WHERE PR0400.CI21CODPERSONA = ?"
    SQL = SQL & " AND PR37CODESTADO = " & constESTACT_CITADA
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
    SQL = SQL & " AND CI0100.CI01SITCITA = '" & constESTCITA_CITADA & "'"
    SQL = SQL & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    SQL = SQL & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0900.PR09NUMPETICION = PR0300.PR09NUMPETICION"
    SQL = SQL & " AND AD0200.AD02CODDPTO = PR0900.AD02CODDPTO"
    SQL = SQL & " AND AD4100.AD02CODDPTO (+)= AD0200.AD02CODDPTO_UC"
    SQL = SQL & " AND AD4100.AD41CODSECCION (+)= AD0200.AD41CODSECCION"
    SQL = SQL & " ORDER BY AD02DESDPTO, CI01FECCONCERT,"
    SQL = SQL & " PR0100.PR12CODACTIVIDAD, PR0100.PR01DESCORTA"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngCodPers
    Set rs = qry.OpenResultset()
    iAct = -1
    Do While Not rs.EOF
        Select Case rs!PR12CODACTIVIDAD
        Case constACTIV_CONSULTA, constACTIV_PRUEBA, constACTIV_INFORME
            iAct = iAct + 1
            If iAct > fraNivel.Count - 1 Then Call pCrearNivel
            chkImprimir(iAct).Value = 1
            lblDpto(iAct).Caption = rs!AD02DESDPTO
            lblAct(iAct).Caption = rs!PR01DESCORTA
            lblFecha(iAct).Caption = Format(rs!CI01FECCONCERT, "dd/mm/yyyy hh:mm")
            If Not IsNull(rs!PR01DESINSTRPACI) Then txtIP(iAct).Text = rs!PR01DESINSTRPACI
            ReDim Preserve arDatos(0 To iAct)
            arDatos(iAct).strDpto = rs!DPE
            arDatos(iAct).strRec = rs!AG11DESRECURSO
            arDatos(iAct).intCodActiv = rs!PR12CODACTIVIDAD
            arDatos(iAct).lngNAPlan = rs!PR04NUMACTPLAN
        End Select
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    If iAct <= constNIVEL_NUMMAX - 1 Then vscNivel.Visible = False Else iAct = constNIVEL_NUMMAX - 1
    fraContainer.Height = constNIVEL_TOP + (iAct + 1) * constNIVEL_HEIGHT
    vscNivel.Height = constNIVEL_TOP + (iAct + 1) * constNIVEL_HEIGHT
    fraActProg.Height = 650 + (iAct + 1) * constNIVEL_HEIGHT
    Me.Height = fraActProg.Top + fraActProg.Height + 500
End Sub

Private Sub pCargarPaciente()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT CI22NUMHISTORIA, CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC,"
    SQL = SQL & " NVL(CI34DESTRATAMI,DECODE(CI30CODSEXO,1,'D.','D�a.'))||' '||CI22NOMBRE||' '||CI22PRIAPEL||' '||CI22SEGAPEL PAC1,"
    SQL = SQL & " CI30CODSEXO, PR0400.CI21CODPERSONA,"
    SQL = SQL & " CI10CALLE||' '||CI10PORTAL||' '||CI10RESTODIREC DIRECC,"
    SQL = SQL & " CI07CODPOSTAL||' '||CI10DESLOCALID CI10DESLOCALID,"
    SQL = SQL & " CI26DESPROVI, CI1000.CI19CODPAIS, CI19DESPAIS"
    SQL = SQL & " FROM CI1000, CI2600, CI1900, CI3400, CI2200, PR0400"
    SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " AND CI3400.CI34CODTRATAMI (+)= CI2200.CI34CODTRATAMI"
    SQL = SQL & " AND CI1000.CI21CODPERSONA (+)= CI2200.CI21CODPERSONA"
    SQL = SQL & " AND CI1000.CI10INDDIRPRINC (+)= -1"
    SQL = SQL & " AND CI2600.CI26CODPROVI (+)= CI1000.CI26CODPROVI"
    SQL = SQL & " AND CI1900.CI19CODPAIS (+)= CI1000.CI19CODPAIS"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = Val(strNAPlan)
    Set rs = qry.OpenResultset()
    lngCodPers = rs!CI21CODPERSONA
    txtNH.Tag = rs!CI30CODSEXO
    If Not IsNull(rs!CI22NUMHISTORIA) Then txtNH.Text = rs!CI22NUMHISTORIA
    txtPac.Text = rs!PAC
    strPac = rs!PAC1 & Chr$(13)
    If Trim$(rs!DIRECC) <> "" Then strPac = strPac & "C/ " & Trim$(rs!DIRECC) & Chr$(13)
    If Trim$(rs!CI10DESLOCALID) <> "" Then strPac = strPac & Trim$(rs!CI10DESLOCALID) & Chr$(13)
    If Not IsNull(rs!CI26DESPROVI) Then strPac = strPac & Trim$(rs!CI26DESPROVI) & Chr$(13)
    If Not IsNull(rs!CI19CODPAIS) Then
        If rs!CI19CODPAIS <> constPAIS_ESPA�A Then
            strPac = strPac & Trim$(rs!CI19DESPAIS) & Chr$(13)
        End If
    End If
    strPac = Left$(strPac, Len(strPac) - 1)
    rs.Close
    qry.Close
End Sub

Private Sub pCrearNivel()
    Dim intN%
    
    LockWindowUpdate Me.hWnd
    
    intN = fraNivel.Count
    
    Load fraNivel(intN)
    Set fraNivel(intN).Container = fraContainer
    fraNivel(intN).Top = constNIVEL_TOP + intN * constNIVEL_HEIGHT
    fraNivel(intN).Visible = True

    Load chkImprimir(intN)
    Set chkImprimir(intN).Container = fraNivel(intN)
    chkImprimir(intN).Top = chkImprimir(intN - 1).Top
    chkImprimir(intN).Visible = True
    
    Load lblDpto(intN)
    Set lblDpto(intN).Container = fraNivel(intN)
    lblDpto(intN).Top = lblDpto(intN - 1).Top
    lblDpto(intN).Visible = True
    
    Load lblAct(intN)
    Set lblAct(intN).Container = fraNivel(intN)
    lblAct(intN).Top = lblAct(intN - 1).Top
    lblAct(intN).Visible = True
    
    Load lblFecha(intN)
    Set lblFecha(intN).Container = fraNivel(intN)
    lblFecha(intN).Top = lblFecha(intN - 1).Top
    lblFecha(intN).Visible = True
    
    Load lblIP(intN)
    Set lblIP(intN).Container = fraNivel(intN)
    lblIP(intN).Top = lblIP(intN - 1).Top
    lblIP(intN).Visible = True
    
    Load txtIP(intN)
    Set txtIP(intN).Container = fraNivel(intN)
    txtIP(intN).Top = txtIP(intN - 1).Top
    txtIP(intN).Visible = True
    
    Load lnLine2(intN)
    Set lnLine2(intN).Container = fraNivel(intN)
    lnLine2(intN).X1 = lnLine2(intN - 1).X1
    lnLine2(intN).X2 = lnLine2(intN - 1).X2
    lnLine2(intN).Y1 = lnLine2(intN - 1).Y1
    lnLine2(intN).Y2 = lnLine2(intN - 1).Y2
    lnLine2(intN).Visible = True
        
    Load txtNoImprimir(intN)
    Set txtNoImprimir(intN).Container = fraNivel(intN)
    txtNoImprimir(intN).Top = txtNoImprimir(intN - 1).Top
    txtNoImprimir(intN).Visible = False

    LockWindowUpdate 0&
End Sub

Private Sub pDesactivarCitasAnteriores()
    Dim iAct%, strNAP$
    
    strNAP = "," & strNAPlan & ","
    For iAct = 0 To fraNivel.Count - 1
        If InStr(strNAP, "," & arDatos(iAct).lngNAPlan & ",") = 0 Then
            chkImprimir(iAct).Value = 0
        End If
    Next iAct
End Sub

Private Sub pImprimir()
    Dim texto$, iAct%, strAhora$, strDpto$, blnDpto As Boolean
    Dim f0$, f1$, f2$, f3$
    
    For iAct = 0 To fraNivel.Count - 1
        If chkImprimir(iAct).Value = 1 Then Exit For
    Next iAct
    If iAct = fraNivel.Count Then
        texto = "No se ha encontrado ninguna cita para ser impresa."
        MsgBox texto, vbExclamation, Me.Caption
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    f0 = "500|4000|5500"
    f1 = "1000|9000"
    f2 = "1500|8500"
    f3 = "2000|8000"
    
    vsp.StartDoc
    vsp.Font = "Arial"
    
    'escudo y cabecera
    Call pCabeceraDocCUN(vsp, bmpEscudo)
    
    'paciente
    vsp.CurrentY = 3500
    vsp.FontSize = 10
    vsp.AddTable f0, "", "|N� Historia: " & txtNH.Text & "|" & strPac, 0, 0
    'vsp.Text = Chr$(13)
    vsp.FontSize = 12
    vsp.CurrentY = 5000
    vsp.CurrentX = 1500
    If txtNH.Tag = "1" Then
        vsp.Text = "Ser� atendido en el Departamento de:" & Chr$(13)
    Else
        vsp.Text = "Ser� atendida en el Departamento de:" & Chr$(13)
    End If

    'citas
    For iAct = 0 To fraNivel.Count - 1
        If chkImprimir(iAct).Value = 1 Then
            If strDpto <> lblDpto(iAct).Caption Then
                vsp.Text = Chr$(13)
                strDpto = lblDpto(iAct).Caption
                blnDpto = False
            End If
            Select Case arDatos(iAct).intCodActiv
            Case constACTIV_CONSULTA
                blnDpto = True
                texto = "* " & lblDpto(iAct).Caption
                texto = texto & " (" & arDatos(iAct).strRec & ")"
                texto = texto & ", para la consulta " & lblAct(iAct).Caption
                texto = texto & " el d�a " & Format(lblFecha(iAct).Caption, "dd/mm/yyyy")
                texto = texto & " a las " & Format(lblFecha(iAct).Caption, "hh:mm") & " horas."
                vsp.AddTable f1, "", "|" & texto, 0, 0
                texto = "Deber� presentarse en: " & arDatos(iAct).strDpto
                vsp.AddTable f2, "", "|" & texto, 0, 0
                If Trim$(txtIP(iAct).Text) <> "" Then
                    texto = "Instrucciones: " & fQuitarCRLF_Extremos(txtIP(iAct).Text)
                    vsp.AddTable f2, "", "|" & texto, 0, 0
                End If
                vsp.Text = Chr$(13)
            Case Else
                If Not blnDpto Then
                    texto = "* " & lblDpto(iAct).Caption
                    vsp.AddTable f1, "", "|" & texto, 0, 0
                    texto = "Deber� presentarse en: " & arDatos(iAct).strDpto
                    vsp.AddTable f2, "", "|" & texto, 0, 0
                    vsp.Text = Chr$(13)
                    blnDpto = True
                End If
                texto = "- " & lblAct(iAct).Caption
                texto = texto & " el d�a " & Format(lblFecha(iAct).Caption, "dd/mm/yyyy")
                texto = texto & " a las " & Format(lblFecha(iAct).Caption, "hh:mm") & " horas."
                vsp.AddTable f2, "", "|" & texto, 0, 0
                If Trim$(txtIP(iAct).Text) <> "" Then
                    texto = "Instrucciones: " & fQuitarCRLF_Extremos(txtIP(iAct).Text)
                    vsp.AddTable f3, "", "|" & texto, 0, 0
                End If
                vsp.Text = Chr$(13)
            End Select
        End If
    Next iAct
    
    'lugar y fecha
    vsp.Text = Chr$(13)
    vsp.CurrentX = 7000
    strAhora = Format(fAhora, "long date")
    vsp.Text = "Pamplona, " & Mid$(strAhora, InStr(strAhora, " ") + 1)
    
    vsp.EndDoc
    vsp.PrintDoc
    
    Screen.MousePointer = vbDefault
End Sub

