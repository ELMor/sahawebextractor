VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Los valores deben coincidir con el nombre del archivo en disco
Const AG02WinRecursos               As String = "AG0201"
Const AG02WinCitacionLibre          As String = "AG0210"
Const AG02WinCitacionNAPlan         As String = "AG0211" '--> se llama al frm AG0210
Const AG02WinCitacionPeticion       As String = "AG0212" '--> se llama al frm AG0210
Const AG02WinCitacionPersona        As String = "AG0213" '--> se llama al frm AG0210
'AG0211 --> no tiene proceso, se llama directamente
'AG0212 --> no tiene proceso, se llama directamente
'AG0213 --> no tiene proceso, se llama directamente
'AG0214 --> no tiene proceso, se llama directamente
Const AG02WinCitaUrgencias          As String = "AG0215"
Const AG02WinBuscadorHuecos         As String = "AG0216"
Const AG02WinReservaHuecos          As String = "AG0217"
Const AG02WinImpimirCitas           As String = "AG0218"

Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub

' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

    On Error Resume Next
  
    ' fija el valor de retorno a verdadero
    LaunchProcess = True
 
    ' comienza la selecci�n del proceso
    Select Case strProcess
    Case AG02WinRecursos
        frmRecursos.Show vbModal
        Set frmRecursos = Nothing
        
    Case AG02WinCitacionLibre
        frmCitacion.Show vbModal
        Set frmCitacion = Nothing
        
    Case AG02WinCitacionNAPlan
        'vntData = NAPlan, NAPlan,..., NAPlan
        Call objPipe.PipeSet("AG0210_PR04NUMACTPLAN", vntData)
        frmCitacion.Show vbModal
        Set frmCitacion = Nothing
        
    Case AG02WinCitacionPeticion
        Call objPipe.PipeSet("AG0210_PR09NUMPETICION", vntData)
        frmCitacion.Show vbModal
        Set frmCitacion = Nothing
        
    Case AG02WinCitacionPersona
        Call objPipe.PipeSet("AG0210_CI21CODPERSONA", vntData)
        frmCitacion.Show vbModal
        Set frmCitacion = Nothing
        
    Case AG02WinCitaUrgencias
        If Not IsMissing(vntData) Then
            Call objPipe.PipeSet("AG0215_CI21CODPERSONA", vntData)
        End If
        frmCitaUrgencias.Show vbModal
        Set frmCitaUrgencias = Nothing
        
    Case AG02WinBuscadorHuecos
        frmBuscadorHuecos.Show vbModal
        Set frmBuscadorHuecos = Nothing
        
    Case AG02WinReservaHuecos
        frmReservaHuecos.Show vbModal
        Set frmReservaHuecos = Nothing
        
    Case AG02WinImpimirCitas
        Call objPipe.PipeSet("AG0218_PR04NUMACTPLAN", vntData)
        frmImprimirCitas.Show vbModal
        Set frmImprimirCitas = Nothing
        
    End Select
  
    Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar el primer par�metro de la matriz al n� de procesos
  ReDim aProcess(1 To 9, 1 To 4) As Variant
      
  ' VENTANAS
  aProcess(1, 1) = AG02WinRecursos
  aProcess(1, 2) = "Recursos"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = AG02WinCitacionLibre
  aProcess(2, 2) = "Citaci�n"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = AG02WinCitacionNAPlan
  aProcess(3, 2) = "Citaci�n (cita/recita)"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow
  
  aProcess(4, 1) = AG02WinCitacionPeticion
  aProcess(4, 2) = "Citaci�n (petici�n)"
  aProcess(4, 3) = False
  aProcess(4, 4) = cwTypeWindow
  
  aProcess(5, 1) = AG02WinCitacionPersona
  aProcess(5, 2) = "Citaci�n (persona)"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow
  
  aProcess(6, 1) = AG02WinCitaUrgencias
  aProcess(6, 2) = "Citaci�n (Consulta de Urgencias)"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = AG02WinBuscadorHuecos
  aProcess(7, 2) = "Buscador de Huecos para Citas"
  aProcess(7, 3) = True
  aProcess(7, 4) = cwTypeWindow
  
  aProcess(8, 1) = AG02WinReservaHuecos
  aProcess(8, 2) = "Reserva de Huecos para Citas"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow
  
  aProcess(9, 1) = AG02WinImpimirCitas
  aProcess(9, 2) = "Imprimir Citas"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeWindow

End Sub
