VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsFarmacia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Public Function Familia(codFamilia As String)
'*******************************************************
'* Las grupos terapeúticos de CUN a las tablas de      *
'* Picis                                               *
'*******************************************************
Dim sql$
Dim Rs As rdoResultset
Dim Qy As rdoQuery

Screen.MousePointer = vbHourglass
If Not LOG Then Exit Function

If ExisteFamilia(Mid(codFamilia, 1, 3)) Then
    Call ModificarFamilia(Mid(codFamilia, 1, 3))
Else
    Call NuevaFamilia(Mid(codFamilia, 1, 3))
End If
'Se realiza el log off
ierrcode = PcsLogDetachHDBC(hLog, 0, 0)
ierrcode = PcsLogFree(hLog)

Screen.MousePointer = vbDefault
End Function
Public Function Medicamentos(codMedicamento As Long)
Dim sql$
Dim Rs As rdoResultset
Dim Qy As rdoQuery

Screen.MousePointer = vbHourglass
sql = "SELECT FR73CODPRODUCTO FROM FR7300 WHERE SUBSTR(FR00CODGRPTERAP,1,1)<> ?" & _
" AND FR73INDVISIBLE=? AND FR73CODPRODUCTO=?"
Set Qy = objApp.rdoConnect.CreateQuery("", sql)
    Qy(0) = "Q"
    Qy(1) = -1
    Qy(2) = codMedicamento
Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If Rs.RowCount > 0 Then 'Solo se modifican si debe existir en Picis
    If Not LOG Then Exit Function
    If ExisteMedicamento(codMedicamento) Then
        Call ModificarMedicamento(codMedicamento)
    Else
        Call NuevoMedicamento(codMedicamento)
    End If
    ierrcode = PcsLogDetachHDBC(hLog, 0, 0)
    ierrcode = PcsLogFree(hLog)
End If
Screen.MousePointer = vbDefault
End Function

Public Property Set objClsCW(new_objCW As Object)
    Set objcw = new_objCW
    Set objApp = objcw.objApp
    Set objPipe = objcw.objPipe
    Set objGen = objcw.objGen
    Set objEnv = objcw.objEnv
    Set objError = objcw.objError
    Set objSecurity = objcw.objSecurity
    Set objMRes = CreateObject("CodeWizard.clsCWMRES")
End Property
Public Property Get strUser() As String
    strUser = strCodUser
End Property
Public Property Let strUser(new_strUser As String)
    strCodUser = new_strUser
End Property
Public Function OrdenMedica(CodPeticion As Long, blnfr28 As Boolean, _
blnfrp4 As Boolean)
Dim strSql As String
Dim qyLinea As rdoQuery
Dim rsLinea As rdoResultset
Dim OrderDboid As String * 21

If Not LOG Then Exit Function 'log-on de Picis

If blnfr28 Then
    strSql = "SELECT FR28NUMLINEA FROM FR2800 WHERE FR66CODPETICION = ? "
    Set qyLinea = objApp.rdoConnect.CreateQuery(" ", strSql)
    qyLinea(0) = CodPeticion
    Set rsLinea = qyLinea.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    
    rsLinea.MoveFirst
    Do While Not rsLinea.EOF
        Call OrdenProducto(CodPeticion, rsLinea!FR28NUMLINEA, True, False)
        rsLinea.MoveNext
    Loop
    rsLinea.Close
    qyLinea.Close
ElseIf blnfrp4 Then
    Call OrdenProducto(CodPeticion, 0, False, True)
End If

'log off de Picis
ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
ierrcode = PcsLogFree(hLog)
End Function

Public Function AnularOM(Historia As Long, codPetición As Long, _
blnfr28 As Boolean, blnfrp4 As Boolean)

Dim strSql As String
Dim qyLinea As rdoQuery
Dim rsLinea As rdoResultset

strSql = "SELECT FR28NUMLINEA FROM FR2800 WHERE FR66CODPETICION = ? "
Set qyLinea = objApp.rdoConnect.CreateQuery(" ", strSql)
qyLinea(0) = codPetición
Set rsLinea = qyLinea.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

If Not LOG Then Exit Function 'log-on de Picis
rsLinea.MoveFirst
Do While Not rsLinea.EOF
    Call AnularOP(Historia, codPetición, rsLinea!FR28NUMLINEA, blnfr28, blnfrp4)
    rsLinea.MoveNext
Loop
rsLinea.Close
qyLinea.Close
'log off de Picis
ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
ierrcode = PcsLogFree(hLog)
End Function
Public Function PasoPicisFamilias()
Dim sql As String
Dim Qy As rdoQuery
Dim Rs As rdoResultset

If Not LOG Then Exit Function 'log-on de Picis

sql = "SELECT FR00CODGRPTERAP FROM FR0000 WHERE LENGTH(FR00CODGRPTERAP) =? " & _
" AND SUBSTR(FR00CODGRPTERAP,1,1)<>?"
Set Qy = objApp.rdoConnect.CreateQuery("", sql)
Qy(0) = 3
Qy(1) = "Q"
Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
Rs.MoveFirst
Do While Not Rs.EOF
    Call NuevaFamilia(Rs!FR00CODGRPTERAP)
    Rs.MoveNext
Loop
Rs.Close
Qy.Close
'log off de Picis
ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
ierrcode = PcsLogFree(hLog)
End Function

Public Function PasoPicisMedicamentos(objcw As clsCW)
Dim sql As String
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim RD1 As rdoResultset
Dim Qy1 As rdoQuery
Set objetoCW = objcw
objetoCW.blnAutoDisconnect = False
objetoCW.SetClockEnable False
'*******Para introducir los medicamentos por primera vez
If Not LOG Then Exit Function 'log-on de Picis
sql = "SELECT FR73CODPRODUCTO FROM FR7300 WHERE SUBSTR(FR00CODGRPTERAP,1,1)<> ?" & _
" AND FR73INDVISIBLE=?"
Set Qy = objApp.rdoConnect.CreateQuery("", sql)
    Qy(0) = "Q"
    Qy(1) = -1
Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
Rs.MoveFirst
Do While Not Rs.EOF
   Call NuevoMedicamento(Rs!FR73CODPRODUCTO)
   Rs.MoveNext
Loop
Rs.Close
Qy.Close
'log off de Picis
ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
ierrcode = PcsLogFree(hLog)

'***********Depurar los medicamentos una vez pasados
'Dim PosEspacio%, DesMedicamento$
'sql = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID FROM FR7300,FRP200 WHERE " & _
'"FR7300.FR73CODPRODUCTO=FRP200.FR73CODPRODUCTO"
'Set Rs = objApp.rdoConnect.OpenResultset(sql, rdOpenKeyset, rdConcurReadOnly)
'Rs.MoveFirst
'Do While Not Rs.EOF
'  PosEspacio = InStr(1, Rs!FR73DESPRODUCTO, " ")
'  If PosEspacio > 0 Then
'  DesMedicamento = Left(Rs!FR73DESPRODUCTO, PosEspacio - 1)
'  DesMedicamento = UCase(Left(DesMedicamento, 1)) & LCase(Right(DesMedicamento, PosEspacio - 2))
'  Else:   DesMedicamento = UCase(Left(Rs!FR73DESPRODUCTO, 1)) & LCase(Right(Rs!FR73DESPRODUCTO, Len(Rs!FR73DESPRODUCTO) - 1))
'  End If
'  sql = "UPDATE TREATMENTS SET BRANDNAME= ?, GENERICNAME=? WHERE TREATMENTDBOID= ? "
'  Set Qy1 = objApp.rdoConnect.CreateQuery("", sql)
'  Qy1(0) = DesMedicamento
'  Qy1(1) = DesMedicamento
'  Qy1(2) = Rs!TREATMENTDBOID
'  Qy1.Execute
'  Qy1.Close
'  Rs.MoveNext
'Loop
'Rs.Close
'Qy.Close

End Function

Public Sub CambiarProducto(CodPeticion As Long, numLinea As Integer, _
velocidad As String, Dosis As String, disolucion As Long, UniDosis As String, _
Observaciones As String, Mininf As Integer, Horainf As Integer, Route, _
blnfr28 As Boolean, blnfrp4 As Boolean, rd As rdoResultset)

Dim OrderDboid As String
Dim RouteDboid As String * 21
Dim staffdboid As String
If Not LOG Then
    MsgBox "Estos cambios no han pasado a Picis" & Chr(13) & Chr(10) _
    & "Usuario desconocido en Picis", vbCritical, "Aviso"
    Exit Sub
End If
Err = 0
objApp.BeginTrans
RouteDboid = ObtenerRouteDboid(Route)
staffdboid = ObtenerStaffDesdeCodigo(strCodUser)
OrderDboid = ObtenerOrderDboidDesdePeticion(CodPeticion, numLinea, _
            blnfr28, blnfrp4)
Call ModificarTask(OrderDboid, staffdboid, velocidad, Dosis, _
    disolucion, UniDosis, CodPeticion, Observaciones, RouteDboid, _
    blnfr28, blnfrp4, rd)
'Call ModificarTaskMemo(OrderDboid, staffdboid, _
'    CodPeticion, Observaciones, _
'    blnfr28, blnfrp4, rd)
 If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "ERROR: La modificación no ha pasado a Picis" & Chr(13) & _
    "Contacte con su administrador del sistema"
    blnError = True
    Exit Sub
Else: objApp.CommitTrans
End If
'log off de Picis
Dim ierrcode%
ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
ierrcode = PcsLogFree(hLog)

End Sub

