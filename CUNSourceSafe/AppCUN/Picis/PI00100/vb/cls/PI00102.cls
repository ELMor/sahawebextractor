VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsUsuarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Property Set objClsCW(new_objCW As Object)
   Set objCW = new_objCW
    Set objApp = objCW.objApp
    Set objPipe = objCW.objPipe
    Set objGen = objCW.objGen
    Set objEnv = objCW.objEnv
    Set objError = objCW.objError
    Set objSecurity = objCW.objSecurity
    Set objMRes = CreateObject("CodeWizard.clsCWMRES")
End Property
Public Property Get strUser() As String
    strUser = strCodUser
End Property
Public Property Let strUser(new_strUser As String)
    strCodUser = new_strUser
End Property

Public Function Usuarios(codUsuario As String, groupdboid As String, blnExisteFechaFin As Boolean)
'*******************************************************
'* INTRODUCIR LOS USUARIOS/DEPARTAMENTOS/PUESTO        *
'* SI NO EXISTEN,                                      *
'* Y SI EXISTEN MODIFICARLOS                           *
'*******************************************************
Dim staffdboid As String * 21
'Dim groupdboid As String * 21
Dim blnExisteStaff As Boolean
Dim sNull As String

If Not LOG Then
    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
    Exit Function
End If


If ExisteStaff(codUsuario) Then
    Call modificarStaff(codUsuario, staffdboid)
Else
    Call NuevoStaff(codUsuario, staffdboid)
End If

'staffdboid = BuscarStaffDboid(codUsuario) 'Se busca  el staffdboid del staff

If ExisteGroupStaff(staffdboid, groupdboid) Then 'Se comprueba si ya existe o no
    'si esta se modifica si existe alguna modificación
    Call ModificarGroupStaff(staffdboid, groupdboid, blnExisteFechaFin) 'Fecha Fin Grupo
Else
 'sino esta se crea
    Call NuevoGroupStaff(groupdboid, staffdboid, blnExisteFechaFin) 'fecha fin del grupo
End If
'Se realiza el log off
ierrcode = PcsLogDetachHDBC(hLog, 0, 0)
ierrcode = PcsLogFree(hLog)
End Function

'''''Public Function UsuariosTodos()
'''''
'''''Dim staffdboid As String * 21
''''''Dim groupdboid As String * 21
'''''Dim blnExisteStaff As Boolean
'''''Dim sNull As String
'''''Dim strSql As String
'''''
'''''If Not LOG Then
'''''    ierrcode = PcsCfgHandleFree(CurrentCfgHandle)
'''''    ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'''''    Exit Function
'''''End If
'''''
'''''strSql = "SELECT * FROM SG0200,SG0600" & _
'''''" WHERE SG0200.SG01COD=SG0600.SG01COD ORDER BY SG0200.SG01COD"
'''''Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
'''''Set Rs = Qy.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'''''
'''''    While Rs.EOF = False
'''''            codUsuario = Rs!SG01COD
'''''            codRol = Rs!SG03COD
'''''
'''''
'''''        If Not ExisteStaff(codUsuario) Then
'''''            Call NuevoUser(codUsuario, codRol)
'''''        End If
'''''
'''''    Rs.MoveNext
'''''    Wend
'''''
'''''Rs.Close
'''''Qy.Close
'''''
'''''ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
'''''ierrcode = PcsLogFree(hLog)
'''''End Function


