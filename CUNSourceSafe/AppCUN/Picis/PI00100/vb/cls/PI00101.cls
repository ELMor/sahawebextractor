VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsAdmision"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Public Property Set objClsCW(new_objCW As Object)
    Set objcw = new_objCW
    Set objApp = objcw.objApp
    Set objPipe = objcw.objPipe
    Set objGen = objcw.objGen
    Set objEnv = objcw.objEnv
    Set objError = objcw.objError
    Set objSecurity = objcw.objSecurity
    Set objMRes = CreateObject("CodeWizard.clsCWMRES")
End Property
Public Property Get strUser() As String
    strUser = strCodUser
End Property
Public Property Let strUser(new_strUser As String)
    strCodUser = new_strUser
End Property


Public Function Alta(codAsistencia As Long)
Dim condition As String
Dim Asistencia As String
Dim cama As String
Dim Systime As Long
Dim codPersona As Long
Dim numHistoria As Long
Dim Historia As String
Dim sNull As String
If Not LOGGeneral Then Exit Function
Call BuscarPacienteDesdeAsistencia(codAsistencia, numHistoria)
ierrcode = PcsListObjectsAlloc(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If

ierrcode = PcsPatientAlloc(hPatient)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
Historia = CStr(numHistoria)
'Build the condition string
condition = "$='" + Historia + "'"
'The only condition is the first identifier, so the other fields are empty strings
ierrcode = PcsPatientFindWithMembers(hLog, hList, condition, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull, sNull)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsListObjectsGotoHeadPosition(hList)
ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hPatient)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsListObjectsFree(hList)

'ierrcode = PcsDischargeAlloc(hDis)
ierrcode = PcsListObjectsAlloc(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
condition = "$='" + "Desconocido" + "'" 'MASIN: Cambiar por Unknown cuando
'se cambie la base de datos
ierrcode = PcsDischargeFindWithMembers(hLog, hList, condition)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsListObjectsGotoHeadPosition(hList)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hDis)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
ierrcode = PcsAdtObjectAlloc(hDischarge, hLog)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error"
End If
Systime = 0
ierrcode = PcsAdtObjectDischarge(hDischarge, hPatient, hDis, hLog, ByVal Systime)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al dar el alta al paciente en picis", vbExclamation, "PICIS"
End If
ierrcode = PcsListObjectsFree(hList)
ierrcode = PcsPatientFree(hPatient)
ierrcode = PcsAdtObjectFree(hDischarge)

ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
ierrcode = PcsLogFree(hLog)

End Function

Public Function PreAdmision(codAsistencia As Long, _
blnvisualcare As Boolean, camaCun As String, codDepart As String)

Dim EntornoDboid As String * 21
Dim rs As rdoResultset
Dim qy As rdoQuery
Dim strsql$
strsql = "SELECT AD38PESO,AD38ALTURA FROM AD3800 WHERE AD38FECFIN IS NULL "
strsql = strsql & " AND AD01CODASISTENCI=?"
Set qy = objApp.rdoConnect.CreateQuery("", strsql)
qy(0) = codAsistencia
Set rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rs.EOF Then
  MsgBox "Picis necesita los datos: Peso y Altura." & Chr(13) & "Introduzcalos para entrar", vbExclamation, ""
  Exit Function
End If
Call objPipe.PipeSet("PI_CodAsistencia", codAsistencia)
Call objPipe.PipeSet("PI_blnVisualcare", blnvisualcare)
Call objPipe.PipeSet("PI_CodCama", camaCun)
Call objPipe.PipeSet("PI_CodDepart", codDepart)

If codDepart = "213" And Mid(camaCun, 1, 4) <> "0105" Then
    frmEntorno.Show (vbModal)
Else
    Call Admision(codAsistencia, blnvisualcare, EntornoDboid)
End If
Call objPipe.PipeRemove("PI_CodAsistencia")
Call objPipe.PipeRemove("PI_blnVisualcare")
Call objPipe.PipeRemove("PI_CodCama")
Call objPipe.PipeRemove("PI_CodDepart")

End Function
