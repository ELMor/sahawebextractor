Attribute VB_Name = "Module3"
#If DebugVersion Then

Public Declare Function PcsDbGetDefaultSourceName Lib "PCSDB500D.DLL" (ByVal out_pdb_source_name As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectAllocFromIdentifier Lib "PCSDB500D.DLL" (ByRef in_phdbobject As Long, ByVal in_pidentifier As String) As Integer
Public Declare Function PcsDbObjectAllocFromIdentifierType Lib "PCSDB500D.DLL" (ByRef in_phdbobject As Long, ByVal in_type As Long) As Integer
Public Declare Function PcsDbObjectAreTheSameObject Lib "PCSDB500D.DLL" (ByVal in_hdbobject1 As Long, ByVal in_hdbobject2 As Long, ByRef out_pyes As Long) As Integer
Public Declare Function PcsDbObjectCopyIdentifier Lib "PCSDB500D.DLL" (ByVal in_hdbobject1 As Long, ByVal in_hdbobject2 As Long) As Integer
Public Declare Function PcsDbObjectCreateNew Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pData As Long) As Integer
Public Declare Function PcsDbObjectDelete Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectDeleteFromLog Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsDbObjectGenerateIdentifier Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectGetAllMemberNames Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal out_hslist As Long) As Integer
Public Declare Function PcsDbObjectGetAllMembersInReportView Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal out_hslist As Long) As Integer
Public Declare Function PcsDbObjectGetChildren Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_pkey As String, ByVal out_hobjectlist As Long, ByVal out_hstringlist As Long) As Integer
Public Declare Function PcsDbObjectGetClassNameFromIdentifierType Lib "PCSDB500D.DLL" (ByVal in_type As Long, ByVal out_pclassname As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetColumnText Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_column As Long, ByVal out_pheadertext As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetColumnWidth Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_column As Long, ByRef out_pcolumn_width As Long) As Integer
Public Declare Function PcsDbObjectGetDataText Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_column As Long, ByVal out_ptext As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetDateMember Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByRef out_pyear As Long, ByRef out_pmonth As Long, _
ByRef out_pday As Long, ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsDbObjectGetDoubleMember Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByRef out_pmember_value As Double) As Integer
Public Declare Function PcsDbObjectGetIcon Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByRef out_phicon As Long, ByVal is_small As Long) As Integer
Public Declare Function PcsDbObjectGetIdentifier Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal out_szIdentifier As String, ByVal in_max_chars As Long) As Integer
Public Declare Function PcsDbObjectGetLog Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByRef out_phlog As Long) As Integer
Public Declare Function PcsDbObjectGetLongMember Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByRef out_pmember_value As Long) As Integer
Public Declare Function PcsDbObjectGetMemberAsString Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal out_pmember_value As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetMemberIconInReportView Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_pcolumnname As String, ByRef out_phicon As Long, _
ByVal in_want_small As Long) As Integer
Public Declare Function PcsDbObjectGetMemberTitleInReportView Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_pcolumnname As String, ByVal out_pheadertext As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetMemberType Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_pmember_name As String, ByRef out_pmember_type As Long) As Integer
Public Declare Function PcsDbObjectGetMemberWidthInReportView Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_pcolumnname As String, ByRef out_pcolumn_width As Long) As Integer
Public Declare Function PcsDbObjectGetNumberOfColumnsInReportView Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByRef out_pnum_columns As Long) As Integer
Public Declare Function PcsDbObjectGetObjectMenu Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_hmenu As Long) As Integer
Public Declare Function PcsDbObjectGetReferenceMemberHandle Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal out_member_value As Long) As Integer
Public Declare Function PcsDbObjectGetReferenceMemberId Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal out_pmember_value As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetStringMember Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal out_pmember_value As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetTypeFromTableName Lib "PCSDB500D.DLL" (ByVal in_strTableName As String, ByRef out_lType As Long) As Integer
Public Declare Function PcsDbObjectInsert Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectInsertToLog Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsDbObjectInvalidateData Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectProcessMenu Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_idmenu As Long) As Integer
Public Declare Function PcsDbObjectRead Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectReadFromLog Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsDbObjectRemoveStrangeChars Lib "PCSDB500D.DLL" (ByVal in_pstring As String, ByVal out_pstring As String, ByVal in_max_num As Long) As Integer
Public Declare Function PcsDbObjectSetDateMember Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsDbObjectSetDoubleMember Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_member_value As Double) As Integer
Public Declare Function PcsDbObjectSetIdentifier Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_szIdentifier As String) As Integer
Public Declare Function PcsDbObjectSetLog Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsDbObjectSetLongMember Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_member_value As Long) As Integer
Public Declare Function PcsDbObjectSetReferenceMemberHandle Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_member_value As Long) As Integer
Public Declare Function PcsDbObjectSetReferenceMemberId Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_pmember_value As String) As Integer
Public Declare Function PcsDbObjectSetStringMember Lib "PCSDB500D.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_pmember_value As String) As Integer
Public Declare Function PcsDbObjectShowProperties Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_hwnd_parent As Long) As Integer
Public Declare Function PcsDbObjectWrite Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectWriteToLog Lib "PCSDB500D.DLL" (ByVal in_hdbobject As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsDboidAlloc Lib "PCSDB500D.DLL" (ByRef out_hdboid As Long) As Integer
Public Declare Function PcsDboidAreTheSame Lib "PCSDB500D.DLL" (ByVal hdboid1 As Long, ByVal hdboid2 As Long, ByRef out_pbool As Long) As Integer
Public Declare Function PcsDboidFree Lib "PCSDB500D.DLL" (ByVal in_hdboid As Long) As Integer
Public Declare Function PcsDboidGenerateFromType Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_type As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDboidGetLength Lib "PCSDB500D.DLL" (ByRef out_pnumcharacters As Long) As Integer
Public Declare Function PcsDboidGetType Lib "PCSDB500D.DLL" (ByVal in_pdboid As String, ByRef out_ptype As Long) As Integer
Public Declare Function PcsDboidGetValue Lib "PCSDB500D.DLL" (ByVal in_hdboid As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDboidNormalizeRepresentingString Lib "PCSDB500D.DLL" (ByVal in_pdboid As String, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDboidSetValue Lib "PCSDB500D.DLL" (ByVal in_hdboid As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsDbSetDefaultSourceName Lib "PCSDB500D.DLL" (ByVal in_pdb_source_name As String, ByVal in_for_this_session_only As Long) As Integer
Public Declare Function PcsDepartmentAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsDepartmentCreateFromScratch Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_cDescription As String, ByRef out_hDepartment As Long) As Integer
Public Declare Function PcsDepartmentFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsDepartmentFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsDepartmentGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHDEPARTMENT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDepartmentListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsDepartmentSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHDEPARTMENT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsDiagnosisAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsDiagnosisFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pType_cond As String) As Integer
Public Declare Function PcsDiagnosisFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsDiagnosisGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDiagnosisGetType Lib "PCSDB500D.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsDiagnosisGetTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDiagnosisListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsDiagnosisSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsDiagnosisSetType Lib "PCSDB500D.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsDiagnosisSetTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsDiagnosisTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsDiagnosisTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsDiagnosisTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsDiagnosisTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHDIAGNOSISTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDiagnosisTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsDiagnosisTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHDIAGNOSISTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsDischargeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsDischargeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsDischargeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsDischargeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHDISCHARGE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDischargeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsDischargeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHDISCHARGE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsDungleDetect Lib "PCSDB500D.DLL" (ByVal in_iRegister As Long, ByRef out_pdetected As Long) As Integer
Public Declare Function PcsDunglePauseDetecting Lib "PCSDB500D.DLL" () As Integer
Public Declare Function PcsDungleStartDetecting Lib "PCSDB500D.DLL" (ByVal in_pcallback As Long) As Integer
Public Declare Function PcsDungleStopDetecting Lib "PCSDB500D.DLL" () As Integer
Public Declare Function PcsElementAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsElementCostAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsElementCostFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pValue_cond As String, ByVal in_pQuantity_cond As String, _
ByVal in_pCost_cond As String, ByVal in_pElement_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsElementCostFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsElementCostGetCost Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsElementCostGetCostIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementCostGetElement Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsElementCostGetElementIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementCostGetQuantity Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsElementCostGetUnit Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsElementCostGetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementCostGetValue Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsElementCostListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsElementCostSetCost Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsElementCostSetCostIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsElementCostSetElement Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsElementCostSetElementIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsElementCostSetQuantity Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsElementCostSetUnit Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsElementCostSetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsElementCostSetValue Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsElementFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pElementType_cond As String) As Integer
Public Declare Function PcsElementFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsElementGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHELEMENT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementGetElementType Lib "PCSDB500D.DLL" (ByVal in_hHELEMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsElementGetElementTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHELEMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsElementSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHELEMENT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsElementSetElementType Lib "PCSDB500D.DLL" (ByVal in_hHELEMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsElementSetElementTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHELEMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsElementTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsElementTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsElementTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsElementTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsElementTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHELEMENTTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEnvironmentAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEnvironmentByPassAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEnvironmentByPassFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pCardioplegiaDose_cond As String, _
ByVal in_pHeparinDose_cond As String, ByVal in_pPriming_cond As String, ByVal in_pEstimatedPumpFlow_cond As String, ByVal in_pEstimatedBloodVolume_cond As String, ByVal in_pTimer1_cond As String, _
ByVal in_pTimer2_cond As String, ByVal in_pTimer3_cond As String, ByVal in_pEnvironment_cond As String) As Integer
Public Declare Function PcsEnvironmentByPassFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetCardioplegiaDose Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetEstimatedBloodVolume Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetEstimatedPumpFlow Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetHeparinDose Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetPriming Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetTimer1 Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetTimer2 Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetTimer3 Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetCardioplegiaDose Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEnvironmentByPassSetEstimatedBloodVolume Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetEstimatedPumpFlow Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetHeparinDose Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetPriming Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetTimer1 Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetTimer2 Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetTimer3 Lib "PCSDB500D.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pEnded_cond As String, _
ByVal in_pPicisData_cond As String, ByVal in_pEnvironmentType_cond As String, ByVal in_pASAType_cond As String) As Integer
Public Declare Function PcsEnvironmentFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEnvironmentGetASAType Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvironmentGetASATypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentGetEnded Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsEnvironmentGetEnvironmentType Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvironmentGetEnvironmentTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentGetFirstREnvironmentLocation Lib "PCSDB500D.DLL" (ByVal in_henvironment As Long, ByRef out_phrenvironmentlocation As Long) As Integer
Public Declare Function PcsEnvironmentGetOpenREnvironmentLocation Lib "PCSDB500D.DLL" (ByVal in_henvironment As Long, ByRef out_phrenvironmentlocation As Long) As Integer
Public Declare Function PcsEnvironmentGetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvironmentGetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsEnvironmentListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsEnvironmentSetASAType Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvironmentSetASATypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEnvironmentSetEnded Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsEnvironmentSetEnvironmentType Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvironmentSetEnvironmentTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEnvironmentSetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvironmentSetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEnvironmentSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsEnvironmentTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEnvironmentTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pShortName_cond As String) As Integer
Public Declare Function PcsEnvironmentTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEnvironmentTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENTTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentTypeGetShortName Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENTTYPE As Long, ByVal out_pShortName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsEnvironmentTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENTTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEnvironmentTypeSetShortName Lib "PCSDB500D.DLL" (ByVal in_hHENVIRONMENTTYPE As Long, ByVal in_pShortName As String) As Integer
Public Declare Function PcsEnvMedProcedureAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEnvMedProcedureFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pEnvironment_cond As String, _
ByVal in_pMedProcedure_cond As String) As Integer
Public Declare Function PcsEnvMedProcedureFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEnvMedProcedureGetEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvMedProcedureGetEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvMedProcedureGetMedProcedure Lib "PCSDB500D.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvMedProcedureGetMedProcedureIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvMedProcedureListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsEnvMedProcedureSetEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvMedProcedureSetEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEnvMedProcedureSetMedProcedure Lib "PCSDB500D.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvMedProcedureSetMedProcedureIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pPatientPosition_cond As String, ByVal in_pNeedle_cond As String, _
ByVal in_pLocation_cond As String, ByVal in_pDepth_cond As String, ByVal in_pAttempts_cond As String, ByVal in_pMultiple_cond As String, ByVal in_pPlacementDescription_cond As String, _
ByVal in_pInjectionTime_cond As String, ByVal in_pLevel5_cond As String, ByVal in_pLevel10_cond As String, ByVal in_pLevel20_cond As String, ByVal in_pLevel60_cond As String, _
ByVal in_pComments_cond As String, ByVal in_pEpiduralAgent_cond As String, ByVal in_pAmount_cond As String, ByVal in_pDepthUnit_cond As String, ByVal in_pAmountUnit_cond As String, _
ByVal in_pAdmission_cond As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAmount Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAmountUnit Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAmountUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAttempts Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetComments Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pComments As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetDepth Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetDepthUnit Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetDepthUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetEpiduralAgent Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pEpiduralAgent As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetInjectionTime Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetLevel10 Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pLevel10 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetLevel20 Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pLevel20 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetLevel5 Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pLevel5 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetLevel60 Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pLevel60 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetLocation Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pLocation As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetMultiple Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pMultiple As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetNeedle Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pNeedle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetPatientPosition Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pPatientPosition As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetPlacementDescription Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pPlacementDescription As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAdmission Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAdmissionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAmount Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAmountUnit Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAmountUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAttempts Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetComments Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pComments As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetDepth Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetDepthUnit Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetDepthUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetEpiduralAgent Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pEpiduralAgent As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetInjectionTime Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetLevel10 Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pLevel10 As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetLevel20 Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pLevel20 As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetLevel5 Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pLevel5 As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetLevel60 Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pLevel60 As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetLocation Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pLocation As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetMultiple Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pMultiple As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetNeedle Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pNeedle As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetPatientPosition Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pPatientPosition As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetPlacementDescription Lib "PCSDB500D.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pPlacementDescription As String) As Integer
Public Declare Function PcsEquipmentAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEquipmentFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIsDeleted_cond As String, ByVal in_pDescription_cond As String, _
ByVal in_pEquipmentId_cond As String, ByVal in_pEquipmentType_cond As String) As Integer
Public Declare Function PcsEquipmentFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEquipmentGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentGetEquipmentId Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal out_pEquipmentId As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentGetEquipmentType Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEquipmentGetEquipmentTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsEquipmentSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEquipmentSetEquipmentId Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal in_pEquipmentId As String) As Integer
Public Declare Function PcsEquipmentSetEquipmentType Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEquipmentSetEquipmentTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEquipmentSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsEquipmentTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEquipmentTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIsDeleted_cond As String, _
ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsEquipmentTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEquipmentTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENTTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentTypeGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENTTYPE As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsEquipmentTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENTTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEquipmentTypeSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHEQUIPMENTTYPE As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsEraseCallback Lib "PCSDB500D.DLL" (ByVal in_callback As Long, ByRef in_puserdata As Long) As Integer
Public Declare Function PcsErrorGetLast Lib "PCSDB500D.DLL" () As Integer
Public Declare Function PcsErrorGetLastString Lib "PCSDB500D.DLL" (ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsErrorHandlerAlloc Lib "PCSERR500D.DLL" (ByRef out_phandle As Long) As Integer
Public Declare Function PcsErrorHandlerClear Lib "PCSERR500D.DLL" () As Integer
Public Declare Function PcsErrorHandlerFree Lib "PCSERR500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsErrorHandlerGetGlobalLogAllowStatus Lib "PCSERR500D.DLL" (ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetGlobalUIAllowStatus Lib "PCSERR500D.DLL" (ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetLast Lib "PCSERR500D.DLL" () As Integer
Public Declare Function PcsErrorHandlerGetLastString Lib "PCSERR500D.DLL" (ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsErrorHandlerGetLogAllowStatus Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetLogStatus Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_errcode As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetString Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_errorcode As Long, ByVal out_pstr As String, ByVal in_maxchars As Long) As Integer
Public Declare Function PcsErrorHandlerGetUIAllowStatus Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetUIStatus Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_errcode As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetZone Lib "PCSERR500D.DLL" (ByVal in_handle As Long, ByVal out_pName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsErrorHandlerHasBeenLogged Lib "PCSERR500D.DLL" (ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerHasBeenUIed Lib "PCSERR500D.DLL" (ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerLog Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_pstr As String, ByVal in_tell_errorzone As Long) As Integer
Public Declare Function PcsErrorHandlerPrompt Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_pstr As String, ByVal in_type As Long, ByVal in_tell_errorzone As Long) As Integer
Public Declare Function PcsErrorHandlerSetGlobalLogAllowStatus Lib "PCSERR500D.DLL" (ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetGlobalUIAllowStatus Lib "PCSERR500D.DLL" (ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetLogAllowStatus Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetLogStatus Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_errcode As Long, ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetUIAllowStatus Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetUIStatus Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_errcode As Long, ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetZone Lib "PCSERR500D.DLL" (ByVal in_handle As Long, ByVal in_pnewzone As String) As Integer
Public Declare Function PcsErrorHandlerSignal Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_errorcode As Long) As Integer
Public Declare Function PcsErrorHandlerSignalEx Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_errorcode As Long, ByVal bForceUI As Long, ByVal bForceLog As Long) As Integer
Public Declare Function PcsErrorHandlerSignalString Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_errorcode As Long, ByVal in_pstr As String) As Integer
Public Declare Function PcsErrorHandlerSignalStringEx Lib "PCSERR500D.DLL" (ByVal in_hhandler As Long, ByVal in_errorcode As Long, ByVal in_pstr As String, ByVal bForceUI As Long, _
ByVal bForceLog As Long) As Integer
Public Declare Function PcsErrorSignal Lib "PCSDB500D.DLL" (ByVal in_errorcode As Long) As Integer
Public Declare Function PcsErrorSignalEx Lib "PCSDB500D.DLL" (ByVal in_errorcode As Long, ByVal in_bForceUI As Long, ByVal in_bForceLog As Long) As Integer
Public Declare Function PcsErrorSignalString Lib "PCSDB500D.DLL" (ByVal in_errorcode As Long, ByVal in_pstr As String) As Integer
Public Declare Function PcsErrorSignalStringEx Lib "PCSDB500D.DLL" (ByVal in_errorcode As Long, ByVal in_pstr As String, ByVal in_bForceUI As Long, ByVal in_bForceLog As Long) As Integer
Public Declare Function PcsEthnicAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEthnicFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsEthnicFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEthnicGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHETHNIC As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEthnicListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsEthnicSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHETHNIC As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEventAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEventFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String, _
ByVal in_pEventType_cond As String) As Integer
Public Declare Function PcsEventFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEventGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHEVENT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEventGetEventType Lib "PCSDB500D.DLL" (ByVal in_hHEVENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEventGetEventTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEVENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEventGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHEVENT As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEventListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsEventSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHEVENT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEventSetEventType Lib "PCSDB500D.DLL" (ByVal in_hHEVENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEventSetEventTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHEVENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEventSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHEVENT As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsEventTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEventTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pIsDeleted_cond As String) As Integer
Public Declare Function PcsEventTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEventTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHEV_TYP As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEventTypeGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHEV_TYP As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEventTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsEventTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHEV_TYP As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEventTypeSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHEV_TYP As Long, ByVal in_pIsDeleted As String) As Integer

#Else

Public Declare Function PcsDbGetDefaultSourceName Lib "PCSDB500.DLL" (ByVal out_pdb_source_name As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectAllocFromIdentifier Lib "PCSDB500.DLL" (ByRef in_phdbobject As Long, ByVal in_pidentifier As String) As Integer
Public Declare Function PcsDbObjectAllocFromIdentifierType Lib "PCSDB500.DLL" (ByRef in_phdbobject As Long, ByVal in_type As Long) As Integer
Public Declare Function PcsDbObjectAreTheSameObject Lib "PCSDB500.DLL" (ByVal in_hdbobject1 As Long, ByVal in_hdbobject2 As Long, ByRef out_pyes As Long) As Integer
Public Declare Function PcsDbObjectCopyIdentifier Lib "PCSDB500.DLL" (ByVal in_hdbobject1 As Long, ByVal in_hdbobject2 As Long) As Integer
Public Declare Function PcsDbObjectCreateNew Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pData As Long) As Integer
Public Declare Function PcsDbObjectDelete Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectDeleteFromLog Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsDbObjectGenerateIdentifier Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectGetAllMemberNames Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal out_hslist As Long) As Integer
Public Declare Function PcsDbObjectGetAllMembersInReportView Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal out_hslist As Long) As Integer
Public Declare Function PcsDbObjectGetChildren Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_pkey As String, ByVal out_hobjectlist As Long, ByVal out_hstringlist As Long) As Integer
Public Declare Function PcsDbObjectGetClassNameFromIdentifierType Lib "PCSDB500.DLL" (ByVal in_type As Long, ByVal out_pclassname As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetColumnText Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_column As Long, ByVal out_pheadertext As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetColumnWidth Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_column As Long, ByRef out_pcolumn_width As Long) As Integer
Public Declare Function PcsDbObjectGetDataText Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_column As Long, ByVal out_ptext As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetDateMember Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByRef out_pyear As Long, ByRef out_pmonth As Long, _
ByRef out_pday As Long, ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsDbObjectGetDoubleMember Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByRef out_pmember_value As Double) As Integer
Public Declare Function PcsDbObjectGetIcon Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByRef out_phicon As Long, ByVal is_small As Long) As Integer
Public Declare Function PcsDbObjectGetIdentifier Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal out_szIdentifier As String, ByVal in_max_chars As Long) As Integer
Public Declare Function PcsDbObjectGetLog Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByRef out_phlog As Long) As Integer
Public Declare Function PcsDbObjectGetLongMember Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByRef out_pmember_value As Long) As Integer
Public Declare Function PcsDbObjectGetMemberAsString Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal out_pmember_value As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetMemberIconInReportView Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_pcolumnname As String, ByRef out_phicon As Long, _
ByVal in_want_small As Long) As Integer
Public Declare Function PcsDbObjectGetMemberTitleInReportView Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_pcolumnname As String, ByVal out_pheadertext As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetMemberType Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_pmember_name As String, ByRef out_pmember_type As Long) As Integer
Public Declare Function PcsDbObjectGetMemberWidthInReportView Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_pcolumnname As String, ByRef out_pcolumn_width As Long) As Integer
Public Declare Function PcsDbObjectGetNumberOfColumnsInReportView Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByRef out_pnum_columns As Long) As Integer
Public Declare Function PcsDbObjectGetObjectMenu Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_hmenu As Long) As Integer
Public Declare Function PcsDbObjectGetReferenceMemberHandle Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal out_member_value As Long) As Integer
Public Declare Function PcsDbObjectGetReferenceMemberId Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal out_pmember_value As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetStringMember Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal out_pmember_value As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsDbObjectGetTypeFromTableName Lib "PCSDB500.DLL" (ByVal in_strTableName As String, ByRef out_lType As Long) As Integer
Public Declare Function PcsDbObjectInsert Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectInsertToLog Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsDbObjectInvalidateData Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectProcessMenu Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_idmenu As Long) As Integer
Public Declare Function PcsDbObjectRead Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectReadFromLog Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsDbObjectRemoveStrangeChars Lib "PCSDB500.DLL" (ByVal in_pstring As String, ByVal out_pstring As String, ByVal in_max_num As Long) As Integer
Public Declare Function PcsDbObjectSetDateMember Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsDbObjectSetDoubleMember Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_member_value As Double) As Integer
Public Declare Function PcsDbObjectSetIdentifier Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_szIdentifier As String) As Integer
Public Declare Function PcsDbObjectSetLog Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsDbObjectSetLongMember Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_member_value As Long) As Integer
Public Declare Function PcsDbObjectSetReferenceMemberHandle Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_member_value As Long) As Integer
Public Declare Function PcsDbObjectSetReferenceMemberId Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_pmember_value As String) As Integer
Public Declare Function PcsDbObjectSetStringMember Lib "PCSDB500.DLL" (ByVal in_phdbobject As Long, ByVal in_pmember_name As String, ByVal in_pmember_value As String) As Integer
Public Declare Function PcsDbObjectShowProperties Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_hwnd_parent As Long) As Integer
Public Declare Function PcsDbObjectWrite Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long) As Integer
Public Declare Function PcsDbObjectWriteToLog Lib "PCSDB500.DLL" (ByVal in_hdbobject As Long, ByVal in_hlog As Long) As Integer
Public Declare Function PcsDboidAlloc Lib "PCSDB500.DLL" (ByRef out_hdboid As Long) As Integer
Public Declare Function PcsDboidAreTheSame Lib "PCSDB500.DLL" (ByVal hdboid1 As Long, ByVal hdboid2 As Long, ByRef out_pbool As Long) As Integer
Public Declare Function PcsDboidFree Lib "PCSDB500.DLL" (ByVal in_hdboid As Long) As Integer
Public Declare Function PcsDboidGenerateFromType Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_type As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDboidGetLength Lib "PCSDB500.DLL" (ByRef out_pnumcharacters As Long) As Integer
Public Declare Function PcsDboidGetType Lib "PCSDB500.DLL" (ByVal in_pdboid As String, ByRef out_ptype As Long) As Integer
Public Declare Function PcsDboidGetValue Lib "PCSDB500.DLL" (ByVal in_hdboid As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDboidNormalizeRepresentingString Lib "PCSDB500.DLL" (ByVal in_pdboid As String, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDboidSetValue Lib "PCSDB500.DLL" (ByVal in_hdboid As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsDbSetDefaultSourceName Lib "PCSDB500.DLL" (ByVal in_pdb_source_name As String, ByVal in_for_this_session_only As Long) As Integer
Public Declare Function PcsDepartmentAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsDepartmentCreateFromScratch Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_cDescription As String, ByRef out_hDepartment As Long) As Integer
Public Declare Function PcsDepartmentFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsDepartmentFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsDepartmentGetDescription Lib "PCSDB500.DLL" (ByVal in_hHDEPARTMENT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDepartmentListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsDepartmentSetDescription Lib "PCSDB500.DLL" (ByVal in_hHDEPARTMENT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsDiagnosisAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsDiagnosisFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pType_cond As String) As Integer
Public Declare Function PcsDiagnosisFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsDiagnosisGetDescription Lib "PCSDB500.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDiagnosisGetType Lib "PCSDB500.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsDiagnosisGetTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDiagnosisListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsDiagnosisSetDescription Lib "PCSDB500.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsDiagnosisSetType Lib "PCSDB500.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsDiagnosisSetTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHDIAGNOSIS As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsDiagnosisTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsDiagnosisTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsDiagnosisTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsDiagnosisTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHDIAGNOSISTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDiagnosisTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsDiagnosisTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHDIAGNOSISTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsDischargeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsDischargeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsDischargeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsDischargeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHDISCHARGE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsDischargeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsDischargeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHDISCHARGE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsDungleDetect Lib "PCSDB500.DLL" (ByVal in_iRegister As Long, ByRef out_pdetected As Long) As Integer
Public Declare Function PcsDunglePauseDetecting Lib "PCSDB500.DLL" () As Integer
Public Declare Function PcsDungleStartDetecting Lib "PCSDB500.DLL" (ByVal in_pcallback As Long) As Integer
Public Declare Function PcsDungleStopDetecting Lib "PCSDB500.DLL" () As Integer
Public Declare Function PcsElementAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsElementCostAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsElementCostFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pValue_cond As String, ByVal in_pQuantity_cond As String, _
ByVal in_pCost_cond As String, ByVal in_pElement_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsElementCostFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsElementCostGetCost Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsElementCostGetCostIdentifier Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementCostGetElement Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsElementCostGetElementIdentifier Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementCostGetQuantity Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsElementCostGetUnit Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsElementCostGetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementCostGetValue Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsElementCostListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsElementCostSetCost Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsElementCostSetCostIdentifier Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsElementCostSetElement Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsElementCostSetElementIdentifier Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsElementCostSetQuantity Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsElementCostSetUnit Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsElementCostSetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsElementCostSetValue Lib "PCSDB500.DLL" (ByVal in_hHELEMENTCOST As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsElementFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pElementType_cond As String) As Integer
Public Declare Function PcsElementFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsElementGetDescription Lib "PCSDB500.DLL" (ByVal in_hHELEMENT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementGetElementType Lib "PCSDB500.DLL" (ByVal in_hHELEMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsElementGetElementTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHELEMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsElementSetDescription Lib "PCSDB500.DLL" (ByVal in_hHELEMENT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsElementSetElementType Lib "PCSDB500.DLL" (ByVal in_hHELEMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsElementSetElementTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHELEMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsElementTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsElementTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsElementTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsElementTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHELEMENTTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsElementTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsElementTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHELEMENTTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEnvironmentAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEnvironmentByPassAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEnvironmentByPassFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pCardioplegiaDose_cond As String, _
ByVal in_pHeparinDose_cond As String, ByVal in_pPriming_cond As String, ByVal in_pEstimatedPumpFlow_cond As String, ByVal in_pEstimatedBloodVolume_cond As String, ByVal in_pTimer1_cond As String, _
ByVal in_pTimer2_cond As String, ByVal in_pTimer3_cond As String, ByVal in_pEnvironment_cond As String) As Integer
Public Declare Function PcsEnvironmentByPassFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetCardioplegiaDose Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetEnvironment Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetEstimatedBloodVolume Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetEstimatedPumpFlow Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetHeparinDose Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetPriming Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetTimer1 Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetTimer2 Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassGetTimer3 Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEnvironmentByPassListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetCardioplegiaDose Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetEnvironment Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEnvironmentByPassSetEstimatedBloodVolume Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetEstimatedPumpFlow Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetHeparinDose Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetPriming Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetTimer1 Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetTimer2 Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentByPassSetTimer3 Lib "PCSDB500.DLL" (ByVal in_hHENV_BYP As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEnvironmentFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pEnded_cond As String, _
ByVal in_pPicisData_cond As String, ByVal in_pEnvironmentType_cond As String, ByVal in_pASAType_cond As String) As Integer
Public Declare Function PcsEnvironmentFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEnvironmentGetASAType Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvironmentGetASATypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentGetEnded Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsEnvironmentGetEnvironmentType Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvironmentGetEnvironmentTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentGetFirstREnvironmentLocation Lib "PCSDB500.DLL" (ByVal in_henvironment As Long, ByRef out_phrenvironmentlocation As Long) As Integer
Public Declare Function PcsEnvironmentGetOpenREnvironmentLocation Lib "PCSDB500.DLL" (ByVal in_henvironment As Long, ByRef out_phrenvironmentlocation As Long) As Integer
Public Declare Function PcsEnvironmentGetPicisData Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvironmentGetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentGetStarted Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsEnvironmentListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsEnvironmentSetASAType Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvironmentSetASATypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEnvironmentSetEnded Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsEnvironmentSetEnvironmentType Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvironmentSetEnvironmentTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEnvironmentSetPicisData Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvironmentSetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEnvironmentSetStarted Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENT As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsEnvironmentTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEnvironmentTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pShortName_cond As String) As Integer
Public Declare Function PcsEnvironmentTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEnvironmentTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENTTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentTypeGetShortName Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENTTYPE As Long, ByVal out_pShortName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvironmentTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsEnvironmentTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENTTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEnvironmentTypeSetShortName Lib "PCSDB500.DLL" (ByVal in_hHENVIRONMENTTYPE As Long, ByVal in_pShortName As String) As Integer
Public Declare Function PcsEnvMedProcedureAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEnvMedProcedureFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pEnvironment_cond As String, _
ByVal in_pMedProcedure_cond As String) As Integer
Public Declare Function PcsEnvMedProcedureFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEnvMedProcedureGetEnvironment Lib "PCSDB500.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvMedProcedureGetEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvMedProcedureGetMedProcedure Lib "PCSDB500.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEnvMedProcedureGetMedProcedureIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEnvMedProcedureListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsEnvMedProcedureSetEnvironment Lib "PCSDB500.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvMedProcedureSetEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEnvMedProcedureSetMedProcedure Lib "PCSDB500.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEnvMedProcedureSetMedProcedureIdentifier Lib "PCSDB500.DLL" (ByVal in_hHENVMEDPROCEDURE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pPatientPosition_cond As String, ByVal in_pNeedle_cond As String, _
ByVal in_pLocation_cond As String, ByVal in_pDepth_cond As String, ByVal in_pAttempts_cond As String, ByVal in_pMultiple_cond As String, ByVal in_pPlacementDescription_cond As String, _
ByVal in_pInjectionTime_cond As String, ByVal in_pLevel5_cond As String, ByVal in_pLevel10_cond As String, ByVal in_pLevel20_cond As String, ByVal in_pLevel60_cond As String, _
ByVal in_pComments_cond As String, ByVal in_pEpiduralAgent_cond As String, ByVal in_pAmount_cond As String, ByVal in_pDepthUnit_cond As String, ByVal in_pAmountUnit_cond As String, _
ByVal in_pAdmission_cond As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAdmission Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAmount Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAmountUnit Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAmountUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetAttempts Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetComments Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pComments As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetDepth Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetDepthUnit Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetDepthUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetEpiduralAgent Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pEpiduralAgent As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetInjectionTime Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetLevel10 Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pLevel10 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetLevel20 Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pLevel20 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetLevel5 Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pLevel5 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetLevel60 Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pLevel60 As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetLocation Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pLocation As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetMultiple Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pMultiple As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetNeedle Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pNeedle As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetPatientPosition Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pPatientPosition As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaGetPlacementDescription Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal out_pPlacementDescription As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAdmission Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAdmissionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAmount Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAmountUnit Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAmountUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetAttempts Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetComments Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pComments As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetDepth Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetDepthUnit Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetDepthUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetEpiduralAgent Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pEpiduralAgent As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetInjectionTime Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetLevel10 Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pLevel10 As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetLevel20 Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pLevel20 As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetLevel5 Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pLevel5 As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetLevel60 Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pLevel60 As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetLocation Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pLocation As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetMultiple Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pMultiple As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetNeedle Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pNeedle As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetPatientPosition Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pPatientPosition As String) As Integer
Public Declare Function PcsEpiduralAnesthesiaSetPlacementDescription Lib "PCSDB500.DLL" (ByVal in_hHEPIDURALANESTHESIA As Long, ByVal in_pPlacementDescription As String) As Integer
Public Declare Function PcsEquipmentAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEquipmentFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIsDeleted_cond As String, ByVal in_pDescription_cond As String, _
ByVal in_pEquipmentId_cond As String, ByVal in_pEquipmentType_cond As String) As Integer
Public Declare Function PcsEquipmentFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEquipmentGetDescription Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentGetEquipmentId Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal out_pEquipmentId As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentGetEquipmentType Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEquipmentGetEquipmentTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsEquipmentSetDescription Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEquipmentSetEquipmentId Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal in_pEquipmentId As String) As Integer
Public Declare Function PcsEquipmentSetEquipmentType Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEquipmentSetEquipmentTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEquipmentSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENT As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsEquipmentTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEquipmentTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIsDeleted_cond As String, _
ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsEquipmentTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEquipmentTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENTTYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentTypeGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENTTYPE As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEquipmentTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsEquipmentTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENTTYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEquipmentTypeSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHEQUIPMENTTYPE As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsEraseCallback Lib "PCSDB500.DLL" (ByVal in_callback As Long, ByRef in_puserdata As Long) As Integer
Public Declare Function PcsErrorGetLast Lib "PCSDB500.DLL" () As Integer
Public Declare Function PcsErrorGetLastString Lib "PCSDB500.DLL" (ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsErrorHandlerAlloc Lib "PCSERR500.DLL" (ByRef out_phandle As Long) As Integer
Public Declare Function PcsErrorHandlerClear Lib "PCSERR500.DLL" () As Integer
Public Declare Function PcsErrorHandlerFree Lib "PCSERR500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsErrorHandlerGetGlobalLogAllowStatus Lib "PCSERR500.DLL" (ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetGlobalUIAllowStatus Lib "PCSERR500.DLL" (ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetLast Lib "PCSERR500.DLL" () As Integer
Public Declare Function PcsErrorHandlerGetLastString Lib "PCSERR500.DLL" (ByVal out_pstring As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsErrorHandlerGetLogAllowStatus Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetLogStatus Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_errcode As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetString Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_errorcode As Long, ByVal out_pstr As String, ByVal in_maxchars As Long) As Integer
Public Declare Function PcsErrorHandlerGetUIAllowStatus Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetUIStatus Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_errcode As Long, ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerGetZone Lib "PCSERR500.DLL" (ByVal in_handle As Long, ByVal out_pName As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsErrorHandlerHasBeenLogged Lib "PCSERR500.DLL" (ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerHasBeenUIed Lib "PCSERR500.DLL" (ByRef out_pstatus As Long) As Integer
Public Declare Function PcsErrorHandlerLog Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_pstr As String, ByVal in_tell_errorzone As Long) As Integer
Public Declare Function PcsErrorHandlerPrompt Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_pstr As String, ByVal in_type As Long, ByVal in_tell_errorzone As Long) As Integer
Public Declare Function PcsErrorHandlerSetGlobalLogAllowStatus Lib "PCSERR500.DLL" (ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetGlobalUIAllowStatus Lib "PCSERR500.DLL" (ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetLogAllowStatus Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetLogStatus Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_errcode As Long, ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetUIAllowStatus Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetUIStatus Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_errcode As Long, ByVal in_status As Long) As Integer
Public Declare Function PcsErrorHandlerSetZone Lib "PCSERR500.DLL" (ByVal in_handle As Long, ByVal in_pnewzone As String) As Integer
Public Declare Function PcsErrorHandlerSignal Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_errorcode As Long) As Integer
Public Declare Function PcsErrorHandlerSignalEx Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_errorcode As Long, ByVal bForceUI As Long, ByVal bForceLog As Long) As Integer
Public Declare Function PcsErrorHandlerSignalString Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_errorcode As Long, ByVal in_pstr As String) As Integer
Public Declare Function PcsErrorHandlerSignalStringEx Lib "PCSERR500.DLL" (ByVal in_hhandler As Long, ByVal in_errorcode As Long, ByVal in_pstr As String, ByVal bForceUI As Long, _
ByVal bForceLog As Long) As Integer
Public Declare Function PcsErrorSignal Lib "PCSDB500.DLL" (ByVal in_errorcode As Long) As Integer
Public Declare Function PcsErrorSignalEx Lib "PCSDB500.DLL" (ByVal in_errorcode As Long, ByVal in_bForceUI As Long, ByVal in_bForceLog As Long) As Integer
Public Declare Function PcsErrorSignalString Lib "PCSDB500.DLL" (ByVal in_errorcode As Long, ByVal in_pstr As String) As Integer
Public Declare Function PcsErrorSignalStringEx Lib "PCSDB500.DLL" (ByVal in_errorcode As Long, ByVal in_pstr As String, ByVal in_bForceUI As Long, ByVal in_bForceLog As Long) As Integer
Public Declare Function PcsEthnicAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEthnicFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String) As Integer
Public Declare Function PcsEthnicFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEthnicGetDescription Lib "PCSDB500.DLL" (ByVal in_hHETHNIC As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEthnicListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsEthnicSetDescription Lib "PCSDB500.DLL" (ByVal in_hHETHNIC As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEventAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEventFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String, _
ByVal in_pEventType_cond As String) As Integer
Public Declare Function PcsEventFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEventGetDescription Lib "PCSDB500.DLL" (ByVal in_hHEVENT As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEventGetEventType Lib "PCSDB500.DLL" (ByVal in_hHEVENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsEventGetEventTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEVENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEventGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHEVENT As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEventListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsEventSetDescription Lib "PCSDB500.DLL" (ByVal in_hHEVENT As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEventSetEventType Lib "PCSDB500.DLL" (ByVal in_hHEVENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsEventSetEventTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHEVENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsEventSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHEVENT As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsEventTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsEventTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pIsDeleted_cond As String) As Integer
Public Declare Function PcsEventTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsEventTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHEV_TYP As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEventTypeGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHEV_TYP As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsEventTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsEventTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHEV_TYP As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsEventTypeSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHEV_TYP As Long, ByVal in_pIsDeleted As String) As Integer


#End If
