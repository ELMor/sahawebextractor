Attribute VB_Name = "FuncionesFarmacia"
Option Explicit
Public blnError As Boolean
Global objetoCW As clsCW
Dim rdmedicamento As rdoResultset
Dim qymedicamento As rdoQuery

Public Function NuevaFamilia(codFamilia As String)
Dim FAMILYDBOID As String * 21
Dim strSql As String
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim sql As String
Dim qyIN As rdoQuery
Dim i As Integer
Dim DescripcionFamilia As String
Dim longitud As Integer

strSql = "SELECT FR00DESGRPTERAP FROM FR0000 WHERE FR00CODGRPTERAP=?"
Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
Qy(0) = codFamilia
Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
longitud = Len(Rs!FR00DESGRPTERAP)
DescripcionFamilia = UCase(Mid(Rs!FR00DESGRPTERAP, 1, 1)) & LCase(Mid(Rs!FR00DESGRPTERAP, 2, longitud - 1))
Rs.Close
Qy.Close

i = 0
For i = 0 To 3 ' Se van a crear cuatro familias en Picis
'Alloc Staff handle
ierrcode = PcsFamilyAlloc(hFamily)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error allocating Family", vbExclamation
End If
'Generar el dboid
ierrcode = PcsDbObjectGenerateIdentifier(hFamily)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al crear el DBOID de familia de medicamentos", vbExclamation
End If
'Introducir el dboid
ierrcode = PcsDbObjectGetIdentifier(hFamily, FAMILYDBOID, 21)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al obtener el DBOID de Familia de medicamentos", vbExclamation
End If
ierrcode = PcsFamilySetDescription(hFamily, DescripcionFamilia)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al introducir el usario", vbExclamation
End If
Select Case i
Case 0 'Para que la familia sea medicacion
    'Si es medicacion el Behaviour es nulo
    ierrcode = PcsFamilySetCategoryIdentifier(hFamily, "157000000000001000000")
    If ierrcode <> 0 Then
        MsgBox "Error al introducir la categoria", vbExclamation
    End If

Case 1    'Para que la familia sea drips
    ierrcode = PcsFamilySetFamilyBehaviorIdentifier(hFamily, "184000000000002000000")
    If ierrcode <> 0 Then
        MsgBox "Error al introducir el behaviour", vbExclamation
    End If

    ierrcode = PcsFamilySetCategoryIdentifier(hFamily, "157000000000002000000")
    If ierrcode <> 0 Then
        MsgBox "Error al introducir la categoria", vbExclamation
    End If
Case 2 'para que la familia sea Aditivo
    ierrcode = PcsFamilySetFamilyBehaviorIdentifier(hFamily, "184000000000007000000")
    If ierrcode <> 0 Then
        MsgBox "Error al introducir el behaviour", vbExclamation
    End If

    ierrcode = PcsFamilySetCategoryIdentifier(hFamily, "157000000000100000000")
    If ierrcode <> 0 Then
        MsgBox "Error al introducir la categoria", vbExclamation
    End If
Case 3 'para que la familia sea Fluidoterapia
    ierrcode = PcsFamilySetFamilyBehaviorIdentifier(hFamily, "184000000000003000000")
    If ierrcode <> 0 Then
        MsgBox "Error al introducir el behaviour", vbExclamation
    End If

    ierrcode = PcsFamilySetCategoryIdentifier(hFamily, "157000000000002000000")
    If ierrcode <> 0 Then
        MsgBox "Error al introducir la categoria", vbExclamation
    End If
End Select
ierrcode = PcsFamilySetIsDeleted(hFamily, False)
If ierrcode <> 0 Then
    MsgBox "Error", vbExclamation
End If
'We send all the changes to the database
ierrcode = PcsDbObjectInsert(hFamily)
If ierrcode <> ERRCODE_ALL_OK Then
    MsgBox "Error al ESCRIBIR", vbExclamation
End If
ierrcode = PcsFamilyFree(hFamily)
                                                                                                     
'Inserta un registro en la tabla FRP100 para unir famila de CUN y de Picis
sql = "INSERT INTO FRP100 (FR00CODGRPTERAP, FAMILYDBOID)" & _
" VALUES (?, ?)"
Set qyIN = objApp.rdoConnect.CreateQuery("", sql)
 qyIN(0) = codFamilia
 qyIN(1) = FAMILYDBOID
 qyIN.Execute
 qyIN.Close
Next i
End Function

Public Function NuevoMedicamento(codMedicamento As Long)
Dim TreatDboid As String * 21
Dim strSql As String
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim sql As String
Dim qyUp As rdoQuery
Dim qyP1 As rdoQuery
Dim RsP1 As rdoResultset
Dim FAMILYDBOID As String
Dim DesMedicamentos As String
Dim blnError As Boolean
Dim longitud As Integer
Dim PosEspacio%
strSql = "SELECT FR7300.FR73DESPRODUCTO,FR7300.FR00CODGRPTERAP, " & _
"FR7300.FR73INDINFIV, FR7300.FR73DOSIS, " & _
"FR7300.FR93CODUNIMEDIDA,FRH7CODFORMFAR, FR7300.FR73VOLUMEN,FR7300.FR34CODVIA " & _
"FROM FR7300 WHERE " & _
"FR7300.FR73CODPRODUCTO=? "
Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
Qy(0) = codMedicamento
Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
PosEspacio = InStr(1, Rs!FR73DESPRODUCTO, " ")
If PosEspacio > 0 Then
  DesMedicamentos = Left(Rs!FR73DESPRODUCTO, PosEspacio - 1)
Else
  DesMedicamentos = Rs!FR73DESPRODUCTO
End If
'If Not IsNull(rs!FR73DOSIS) Then ' Si existe la dosis
'    DesMedicamentos = DesMedicamentos & " " & rs!FR73DOSIS
'    DesMedicamentos = DesMedicamentos & " " & rs!FR93CODUNIMEDIDA
'    If Not IsNull(rs!FRH7CODFORMFAR) Then
'        DesMedicamentos = DesMedicamentos & ", " & rs!FRH7CODFORMFAR
'    End If
'    If Not IsNull(IsNull(rs!FR73VOLUMEN)) And rs!FR73VOLUMEN <> 0 Then
'        DesMedicamentos = DesMedicamentos & " " & rs!FR73VOLUMEN & " ml"
'    End If
'ElseIf IsNull(rs!FR73DOSIS) And (Not IsNull(rs!FR73VOLUMEN) And rs!FR73VOLUMEN <> 0) Then
''Si no existe la dosis se pasa directamente el volumen
'    DesMedicamentos = DesMedicamentos & " " & rs!FR73VOLUMEN & " ml"
'    If Not IsNull(rs!FRH7CODFORMFAR) Then
'        DesMedicamentos = DesMedicamentos & ", " & rs!FRH7CODFORMFAR
'    End If
'Else
'    If Not IsNull(rs!FRH7CODFORMFAR) Then
'        DesMedicamentos = DesMedicamentos & ", " & rs!FRH7CODFORMFAR
'    End If
'End If

longitud = Len(DesMedicamentos)
DesMedicamentos = UCase(Mid(DesMedicamentos, 1, 1)) & LCase(Mid(DesMedicamentos, 2, longitud - 1))

strSql = "SELECT FRP100.FAMILYDBOID FROM FRP100, FAMILIES, CATEGORIES " & _
"WHERE FRP100.FR00CODGRPTERAP=? AND " & _
"(CATEGORIES.CATEGORYDBOID=? OR CATEGORIES.CATEGORYDBOID=? OR " & _
"CATEGORIES.CATEGORYDBOID=?) " & _
"AND FAMILIES.FAMILYDBOID=FRP100.FAMILYDBOID AND " & _
"CATEGORIES.CATEGORYDBOID=FAMILIES.CATEGORYDBOID"
Set qyP1 = objApp.rdoConnect.CreateQuery("", strSql)
    qyP1(0) = Mid(Rs!FR00CODGRPTERAP, 1, 3)
    qyP1(1) = "157000000000002000000" 'CATEGORIA DE PERFUSI�N
    qyP1(2) = "157000000000100000000" 'CATEGOR�A DE ADITIVOS
    qyP1(3) = "157000000000001000000" 'CATEGORIA DE MEDICAMENTOS
Set RsP1 = qyP1.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
Rs.Close
Qy.Close
If RsP1.EOF Then 'no tiene familia
    'MsgBox "No tiene familia" & codMedicamento & " " & DesMedicamentos
    Exit Function
End If
RsP1.MoveFirst
Do While Not RsP1.EOF
    FAMILYDBOID = RsP1!FAMILYDBOID
    'Alloc Staff handle
    ierrcode = PcsTreatmentAlloc(hTreat)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error allocating Medicamento", vbExclamation
    End If
       
    'Generar el dboid
    ierrcode = PcsDbObjectGenerateIdentifier(hTreat)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error al crear el DBOid de medicamentos", vbExclamation
    End If
    'Introducir el dboid
    ierrcode = PcsDbObjectGetIdentifier(hTreat, TreatDboid, 21)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error al obtener el DBOID de medicamentos", vbExclamation
    End If
    ierrcode = PcsTreatmentSetBrandName(hTreat, DesMedicamentos)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error", vbExclamation
    End If
    ierrcode = PcsTreatmentSetGenericName(hTreat, DesMedicamentos)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error", vbExclamation
    End If
    ierrcode = PcsTreatmentSetFamilyIdentifier(hTreat, FAMILYDBOID)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error al introducir la descripcion", vbExclamation
    End If
     'Masin: provisional
     'Se introduce el dboid de Prescripci�n Enfermeras de Anestesia
    ierrcode = PcsTreatmentSetGroupIdentifier(hTreat, "21000000000019000511")
     If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error al introducir el grupo", vbExclamation
    End If
    
    ierrcode = PcsTreatmentSetIsDeleted(hTreat, False)
    If ierrcode <> 0 Then
        MsgBox "Error", vbExclamation
    End If
    'We send all the changes to the database
    ierrcode = PcsDbObjectInsert(hTreat)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error al ESCRIBIR", vbExclamation
        blnError = True
    End If
    ierrcode = PcsTreatmentFree(hTreat)

        
    'Inserta un registro en la tabla FRP200 para unir un medicamento
    ' de CUN y el de Picis
    If Not blnError Then
    sql = "INSERT INTO FRP200 (FR73CODPRODUCTO, TREATMENTDBOID)" & _
    " VALUES (?, ?)"
    Set qyUp = objApp.rdoConnect.CreateQuery("", sql)
     qyUp(0) = codMedicamento
     qyUp(1) = TreatDboid
     qyUp.Execute
     qyUp.Close
     blnError = False
     End If
    RsP1.MoveNext
Loop
RsP1.Close
qyP1.Close
End Function

Public Function Task(FechaTask As String, rdcun As rdoResultset, _
OrderDboid As String, FechaInicio As String, _
FechaFin As String, FechaAdministracion As String, blnPrimeraAdministracion As Boolean, _
intTipoOm As Integer, _
FechaUltimaAdministracion As String, _
blnfr28 As Boolean, blnfrp4 As Boolean)

Dim strTask As String
Dim qytask As rdoQuery
Dim TaskDboid As String * 21
Dim strHora As String
Dim qyHora As rdoQuery
Dim rdHora As rdoResultset
Dim HoraAdministracion As String
Dim UnidadDosis As String
Dim CantidadDil As Long
Dim Dosis As Long
Dim CodPeticion As Long
Dim i%
'******************************************************************
'Esta funcion escribe sobre la tabla Tasks.
'Despues manda a escribir sobre la tabla TaskData si
'es una perfusion o un fluido
'*******************************************************************

If blnfr28 Then
    If Not IsNull(rdcun!FR28CANTIDADDIL) Then CantidadDil = rdcun!FR28CANTIDADDIL Else CantidadDil = 0
    If Not IsNull(rdcun!FR28DOSIS) Then Dosis = rdcun!FR28DOSIS Else Dosis = 0
    If Not IsNull(rdcun!FR66CODPETICION) Then CodPeticion = rdcun!FR66CODPETICION Else CodPeticion = 0
ElseIf blnfrp4 Then
    If Not IsNull(rdcun!FRP4CANTIDADDIL) Then CantidadDil = rdcun!FRP4CANTIDADDIL Else CantidadDil = 0
    If Not IsNull(rdcun!FRP4DOSIS) Then Dosis = rdcun!FRP4DOSIS Else Dosis = 0
    If Not IsNull(rdcun!FRP4CODPETICION) Then CodPeticion = rdcun!FRP4CODPETICION Else CodPeticion = 0
End If

If Not IsNull(rdcun!FRG4CODFRECUENCIA) Then
    If rdcun!FRG4CODFRECUENCIA <> "Manual" Then
        strHora = "SELECT to_date(replace(to_char(FRG5HORA*100,'00,00'),',',':'),'hh24:mi') FROM " & _
        "FRG500 WHERE FRG4CODFRECUENCIA= ? ORDER BY FRG5HORA ASC"
        Set qyHora = objApp.rdoConnect.CreateQuery("", strHora)
        qyHora(0) = rdcun!FRG4CODFRECUENCIA
        Set rdHora = qyHora.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        rdHora.MoveFirst
        
        Do While Not rdHora.EOF
            HoraAdministracion = Format(rdHora(0), "HH:MM:SS")
            FechaAdministracion = Format(FechaTask & " " & HoraAdministracion, "YYYY-MM-DD HH:MM:SS")
            If (DateDiff("s", CDate(FechaAdministracion), CDate(FechaFin)) >= 0) And _
            (DateDiff("s", CDate(FechaInicio), CDate(FechaAdministracion)) >= 0) Then
                ierrcode = PcsTaskAlloc(hTask)
                ierrcode = PcsDbObjectGenerateIdentifier(hTask)
                ierrcode = PcsDbObjectGetIdentifier(hTask, TaskDboid, 21)
                ierrcode = PcsTaskFree(hTask)
                
                strTask = "INSERT INTO TASKS (TASKDBOID,CREATIONDATE,TODODATE, DONEDATE, " & _
                "VALIDATEDDATE, DOSE, HASAMEMO, STAFFDBOID, " & _
                "ORDERDBOID, TASKSTATUSDBOID, GROUPDBOID, UNITDBOID) " & _
                "VALUES (?, SYSDATE, TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), " & _
                "TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?, " & _
                "?, ?, ?, ?)"
                Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
                    qytask(0) = TaskDboid
                    qytask(1) = Format(FechaAdministracion, "DD/MM/YYYY HH:MM:SS")
                    qytask(2) = Format(FechaAdministracion, "DD/MM/YYYY HH:MM:SS")
                    qytask(3) = "1/1/1980 00:00:00"
                    If intTipoOm = 7 Or intTipoOm = 8 Then  '****Si es fluidoterapia o mezcla, la dosis es el volumen.
                        qytask(4) = CStr(CantidadDil) 'Higherdose
                    ElseIf Dosis <> 0 Then
                        qytask(4) = CalcularDosis(rdcun, intTipoOm, UnidadDosis, rdcun!FR34CODVIA, blnfr28, blnfrp4) 'Higherdose
                    End If
                    qytask(5) = "F"
                    qytask(6) = ObtenerStaff(CodPeticion, blnfr28, blnfrp4)
                    qytask(7) = OrderDboid
                    qytask(8) = "170000000000000000000" 'TODO
                    qytask(9) = "21000000000029000511" 'validaci�n enfermeras
                    If intTipoOm <> 7 And intTipoOm <> 8 Then  'And rdcun!FR34CODVIA <> "PC"
                        qytask(10) = ObtenerUnitDboid(rdcun!FR93CODUNIMEDIDA)
                    Else
                        qytask(10) = "45000000000008000000" '**************ml
                    End If
                qytask.Execute
                qytask.Close
                If blnPrimeraAdministracion Then
                    FechaInicio = FechaAdministracion
                    blnPrimeraAdministracion = False
                End If
                FechaUltimaAdministracion = FechaAdministracion
                If intTipoOm = 2 Or intTipoOm = 4 Or intTipoOm = 6 Or intTipoOm = 7 _
                Or intTipoOm = 8 Then
                    Call TaskData(TaskDboid, rdcun, intTipoOm, blnfr28, blnfrp4)
                End If
            End If
            rdHora.MoveNext
        Loop
        rdHora.Close
        qyHora.Close
    Else 'Si la frecuencia es manual
        strHora = "SELECT FRP4HORAS FROM FRP400 WHERE FRP4CODPETICION=?"
        Set qyHora = objApp.rdoConnect.CreateQuery("", strHora)
            qyHora(0) = CodPeticion
        Set rdHora = qyHora.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        frmEntorno.Awk = rdHora!FRP4HORAS
        rdHora.Close
        qyHora.Close
        For i = 1 To frmEntorno.Awk.NF
            HoraAdministracion = Format(frmEntorno.Awk.F(i) & ":00:00", "HH:MM:SS")
            FechaAdministracion = Format(FechaTask & " " & HoraAdministracion, "YYYY-MM-DD HH:MM:SS")
            If (DateDiff("s", CDate(FechaAdministracion), CDate(FechaFin)) >= 0) And _
            (DateDiff("s", CDate(FechaInicio), CDate(FechaAdministracion)) >= 0) Then
                ierrcode = PcsTaskAlloc(hTask)
                ierrcode = PcsDbObjectGenerateIdentifier(hTask)
                ierrcode = PcsDbObjectGetIdentifier(hTask, TaskDboid, 21)
                ierrcode = PcsTaskFree(hTask)
                
                strTask = "INSERT INTO TASKS (TASKDBOID,CREATIONDATE,TODODATE, DONEDATE, " & _
                "VALIDATEDDATE, DOSE, HASAMEMO, STAFFDBOID, " & _
                "ORDERDBOID, TASKSTATUSDBOID, GROUPDBOID, UNITDBOID) " & _
                "VALUES (?, SYSDATE, TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), " & _
                "TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?, " & _
                "?, ?, ?, ?)"
                Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
                    qytask(0) = TaskDboid
                    qytask(1) = Format(FechaAdministracion, "DD/MM/YYYY HH:MM:SS")
                    qytask(2) = Format(FechaAdministracion, "DD/MM/YYYY HH:MM:SS")
                    qytask(3) = "1/1/1980 00:00:00"
                    If intTipoOm = 7 Or intTipoOm = 8 Then  '****Si es fluidoterapia o mezcla, la dosis es el volumen.
                        qytask(4) = CStr(CantidadDil) 'Higherdose
                    ElseIf Dosis <> 0 Then
                        qytask(4) = CalcularDosis(rdcun, intTipoOm, UnidadDosis, rdcun!FR34CODVIA, blnfr28, blnfrp4) 'Higherdose
                    End If
                    qytask(5) = "F"
                    qytask(6) = ObtenerStaff(CodPeticion, blnfr28, blnfrp4)
                    qytask(7) = OrderDboid
                    qytask(8) = "170000000000000000000" 'TODO
                    qytask(9) = "21000000000029000511" 'validaci�n enfermeras
                    If intTipoOm <> 7 And intTipoOm <> 8 Then  'And rdcun!FR34CODVIA <> "PC"
                        qytask(10) = ObtenerUnitDboid(rdcun!FR93CODUNIMEDIDA)
                    Else
                        qytask(10) = "45000000000008000000" '**************ml
                    End If
                qytask.Execute
                qytask.Close
                If blnPrimeraAdministracion Then
                    FechaInicio = FechaAdministracion
                    blnPrimeraAdministracion = False
                End If
                FechaUltimaAdministracion = FechaAdministracion
                If intTipoOm = 2 Or intTipoOm = 4 Or intTipoOm = 6 Or intTipoOm = 7 _
                Or intTipoOm = 8 Then
                    Call TaskData(TaskDboid, rdcun, intTipoOm, blnfr28, blnfrp4)
                End If
            End If
        Next i
    End If
Else
  FechaAdministracion = Format(FechaInicio, "YYYY-MM-DD HH:MM:SS")
    If (DateDiff("s", CDate(FechaAdministracion), CDate(FechaFin)) >= 0) And _
    (DateDiff("s", CDate(FechaInicio), CDate(FechaAdministracion)) >= 0) Then
        ierrcode = PcsTaskAlloc(hTask)
        ierrcode = PcsDbObjectGenerateIdentifier(hTask)
        ierrcode = PcsDbObjectGetIdentifier(hTask, TaskDboid, 21)
        ierrcode = PcsTaskFree(hTask)
        
        strTask = "INSERT INTO TASKS (TASKDBOID,CREATIONDATE,TODODATE, DONEDATE, " & _
        "VALIDATEDDATE, DOSE, HASAMEMO, STAFFDBOID, " & _
        "ORDERDBOID, TASKSTATUSDBOID, GROUPDBOID, UNITDBOID) " & _
        "VALUES (?, SYSDATE, TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), " & _
        "TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), ?, ?, ?, " & _
        "?, ?, ?, ?)"
        Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
            qytask(0) = TaskDboid
            qytask(1) = Format(FechaAdministracion, "DD/MM/YYYY HH:MM:SS")
            qytask(2) = Format(FechaAdministracion, "DD/MM/YYYY HH:MM:SS")
            qytask(3) = "1/1/1980 00:00:00"
            If intTipoOm = 7 Or intTipoOm = 8 Then  '****Si es fluidoterapia o mezcla, la dosis es el volumen.
                qytask(4) = CStr(CantidadDil) 'Higherdose
            ElseIf Dosis <> 0 Then
                qytask(4) = CalcularDosis(rdcun, intTipoOm, UnidadDosis, rdcun!FR34CODVIA, blnfr28, blnfrp4) 'Higherdose
            End If
            qytask(5) = "F"
            qytask(6) = ObtenerStaff(CodPeticion, blnfr28, blnfrp4)
            qytask(7) = OrderDboid
            qytask(8) = "170000000000000000000" 'TODO
            qytask(9) = "21000000000029000511" 'validaci�n enfermeras
            If intTipoOm <> 7 And intTipoOm <> 8 Then  'And rdcun!FR34CODVIA <> "PC"
                qytask(10) = ObtenerUnitDboid(rdcun!FR93CODUNIMEDIDA)
            Else
                qytask(10) = "45000000000008000000" '**************ml
            End If
        qytask.Execute
        qytask.Close
        If blnPrimeraAdministracion Then
            FechaInicio = FechaAdministracion
            blnPrimeraAdministracion = False
        End If
        FechaUltimaAdministracion = FechaAdministracion
        If intTipoOm = 2 Or intTipoOm = 4 Or intTipoOm = 6 Or intTipoOm = 7 _
        Or intTipoOm = 8 Then
            Call TaskData(TaskDboid, rdcun, intTipoOm, blnfr28, blnfrp4)
        End If
    End If
End If
End Function
Public Function AnularOP(numHistoria As Long, CodPeticion As Long, _
numLinea As Long, blnfr28 As Boolean, blnfrp4 As Boolean)
Dim codAsistencia As Long
Dim OrderDboid As String
Dim strOrder As String
Dim qyOrder As rdoQuery
Dim rdOrder As rdoResultset
Dim strUp, strTask, strSql As String
Dim qyUp As rdoQuery
Dim qytask As rdoQuery
Dim qySql As rdoQuery
Dim rdSql As rdoResultset
Dim blnFirst As Boolean
Dim strTodo As String
Dim rdTodo As rdoResultset
Dim qyTodo As rdoQuery
Dim qyDoing As rdoQuery
Dim rdDoing As rdoResultset
Dim qyVia As rdoQuery
Dim rdVia As rdoResultset
Dim strVia As String
Dim strMed As String
Dim qyMed As rdoQuery
Dim rdMed As rdoResultset
Dim staff As String
Dim str1 As String
Dim Qy1 As rdoQuery
Dim RD1 As rdoResultset
Dim FechaInicio As String
Dim TaskDboid As String * 21

Err = 0

Call BuscarAsistenciaDesdePaciente(numHistoria, codAsistencia)
strOrder = "SELECT ORDERDBOID FROM FRP300 WHERE FR66CODPETICION=? " & _
"AND FR28NUMLINEA=?"
Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
    qyOrder(0) = CodPeticion
    qyOrder(1) = numLinea
Set rdOrder = qyOrder.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
strVia = "SELECT FR34CODVIA FROM FR2800 WHERE FR66CODPETICION=? AND " & _
"FR28NUMLINEA=?"
Set qyVia = objApp.rdoConnect.CreateQuery("", strVia)
    qyVia(0) = CodPeticion
    qyVia(1) = numLinea
Set rdVia = qyVia.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    
objApp.BeginTrans

While Not rdOrder.EOF
OrderDboid = rdOrder!OrderDboid
    If rdVia!FR34CODVIA <> "PC" Then
        'Obtener la ultima fecha sin validar
        strTodo = "SELECT TODODATE FROM TASKS WHERE ORDERDBOID=? AND TASKSTATUSDBOID=? " & _
        "ORDER BY TODODATE ASC"
        Set qyTodo = objApp.rdoConnect.CreateQuery("", strTodo)
            qyTodo(0) = OrderDboid
            qyTodo(1) = "170000000000000000000" 'todo
        Set rdTodo = qyTodo.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        If rdTodo.EOF Then
            MsgBox "Esta orden m�dica no se puede eliminar", vbCritical, "Aviso"
            rdTodo.Close
            qyTodo.Close
            Exit Function
        Else
        rdTodo.MoveFirst
        'Comprobar si ser�a una orden cancelada o anulada. Para ello vemos
        'si la orden se est� haciendo
        strSql = "SELECT COUNT(*) FROM ORDERS WHERE ORDERDBOID=? " & _
        "AND ORDERSTATUSDBOID =?"
        Set qyDoing = objApp.rdoConnect.CreateQuery("", strSql)
            qyDoing(0) = OrderDboid
            qyDoing(1) = "170000000000006000000" 'doing
        Set rdDoing = qyDoing.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        End If
        'Modificar el registro para anular la OM
        strUp = "UPDATE ORDERS SET LASTDATE=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ORDERSTATUSDBOID=?"
        strUp = strUp & " WHERE ORDERDBOID =?"
         Set qyUp = objApp.rdoConnect.CreateQuery("", strUp)
         qyUp(0) = Format(rdTodo!TODODATE, "DD/MM/YYYY HH:MM:SS")
         If rdDoing.rdoColumns(0).Value >= 1 Then
            qyUp(1) = "170000000000010000000" 'Discontinued
         Else
            qyUp(1) = "170000000000008000000" 'Cancel
         End If
         qyUp(2) = OrderDboid
         qyUp.Execute
        rdTodo.Close
        qyTodo.Close
        qyUp.Close
        
        strSql = "SELECT TASKDBOID FROM TASKS WHERE ORDERDBOID=? " & _
        "AND TASKSTATUSDBOID NOT IN (?,?) "
        strSql = strSql & " ORDER BY TODODATE"
        Set qySql = objApp.rdoConnect.CreateQuery("", strSql)
            qySql(0) = OrderDboid
            qySql(1) = "170000000000001000000" 'done
            qySql(2) = "170000000000003000000" 'Hold
        
        Set rdSql = qySql.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        rdSql.MoveFirst
        blnFirst = True
        Do While Not rdSql.EOF
            strTask = "UPDATE TASKS SET DONEDATE=SYSDATE, TASKSTATUSDBOID=? " & _
            "WHERE TASKDBOID= ?"
            Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
                If blnFirst Then
                    qytask(0) = "170000000000009000000" 'firstDiscontinued (la primera linea tiene este status
                    blnFirst = False
                Else
                    qytask(0) = "170000000000010000000" 'Discontinued
                End If
                qytask(1) = rdSql.rdoColumns(0).Value
            qytask.Execute
            rdSql.MoveNext
        Loop
        rdSql.Close
        qySql.Close
        qytask.Close
    Else
        'Comprobar si ser�a una orden cancelada o anulada. Para ello vemos
        'si la orden se est� haciendo
        str1 = "SELECT INITDATE FROM ORDERS WHERE ORDERDBOID=?"
        Set Qy1 = objApp.rdoConnect.CreateQuery("", str1)
        Qy1(0) = OrderDboid
        Set RD1 = Qy1.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        FechaInicio = Format(RD1!INITDATE, "DD/MM/YYYY HH:MM:SS")
        RD1.Close
        Qy1.Close
        
        If FechaInicio < strFechaHora_Sistema Then
            FechaInicio = strFechaHora_Sistema
        End If
        
        'Comprobar si ser�a una orden cancelada o anulada. Para ello vemos
        'si la orden se est� haciendo
        strSql = "SELECT COUNT(*) FROM ORDERS WHERE ORDERDBOID=? " & _
        "AND ORDERSTATUSDBOID =?"
        Set qyDoing = objApp.rdoConnect.CreateQuery("", strSql)
            qyDoing(0) = OrderDboid
            qyDoing(1) = "170000000000006000000" 'doing
        Set rdDoing = qyDoing.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        
        'Modificar el registro para anular la OM
        strUp = "UPDATE ORDERS SET LASTDATE=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ORDERSTATUSDBOID=?"
        strUp = strUp & " WHERE ORDERDBOID =?"
         Set qyUp = objApp.rdoConnect.CreateQuery("", strUp)
         qyUp(0) = FechaInicio
         If rdDoing.rdoColumns(0).Value >= 1 Then
            qyUp(1) = "170000000000010000000" 'Discontinued
         Else
            qyUp(1) = "170000000000008000000" 'Cancel
         End If
         qyUp(2) = OrderDboid
         qyUp.Execute
         qyUp.Close
                  
        If rdDoing.rdoColumns(0).Value >= 1 Then 'Ya se esta haciendo: Discontinuar
            strUp = "UPDATE TASKS SET TASKSTATUSDBOID= ?, TODODATE=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), DONEDATE= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
            "WHERE ORDERDBOID= ? AND TASKSTATUSDBOID= ?"
            Set qyUp = objApp.rdoConnect.CreateQuery("", strUp)
                qyUp(0) = "170000000000001000000" 'Done
                qyUp(1) = FechaInicio
                qyUp(2) = FechaInicio
                qyUp(3) = OrderDboid
                qyUp(4) = "170000000000011000000" 'CurrentTime
            qyUp.Execute
            qyUp.Close
        Else 'Cancelar
            ierrcode = PcsTaskAlloc(hTask)
            ierrcode = PcsDbObjectGenerateIdentifier(hTask)
            ierrcode = PcsDbObjectGetIdentifier(hTask, TaskDboid, 21)
            ierrcode = PcsTaskFree(hTask)
            
            strTask = "INSERT INTO TASKS (TASKDBOID,CREATIONDATE,TODODATE, DONEDATE, " & _
            "VALIDATEDDATE, DOSE, HASAMEMO, STAFFDBOID, " & _
            "ORDERDBOID, TASKSTATUSDBOID, GROUPDBOID, UNITDBOID) " & _
            "VALUES (?, SYSDATE, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), " & _
            "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),SYSDATE, ?, ?, ?, ?, ?, ?, ?)"
                
            Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
            
            qytask(0) = TaskDboid
            qytask(1) = FechaInicio
            qytask(2) = FechaInicio
            qytask(3) = 0
            qytask(4) = "F"
            qytask(5) = ObtenerStaff(CodPeticion, blnfr28, blnfrp4)
            qytask(6) = OrderDboid
            qytask(7) = "170000000000001000000" 'DONE
            qytask(8) = "21000000000025000511" 'validaci�n COM�N
            qytask(9) = Null 'unitdboid
            qytask.Execute
            qytask.Close

        End If
        rdOrder.MoveNext
    End If
Wend
rdDoing.Close
qyDoing.Close
    If Err > 0 Then
        objApp.RollbackTrans
        MsgBox "ERROR: La cancelaci�n de orden m�dica no se ha pasado en Picis"
        Exit Function
    Else: objApp.CommitTrans
    End If
rdOrder.Close
qyOrder.Close
End Function

Public Function AnularOPFR3200(numHistoria As Long, CodPeticion As Long, _
numLinea As Long)
Dim codAsistencia As Long
Dim OrderDboid As String
Dim strOrder As String
Dim qyOrder As rdoQuery
Dim rdOrder As rdoResultset
Dim strUp, strTask, strSql As String
Dim qyUp As rdoQuery
Dim qytask As rdoQuery
Dim qySql As rdoQuery
Dim rdSql As rdoResultset
Dim blnFirst As Boolean
Dim strTodo As String
Dim rdTodo As rdoResultset
Dim qyTodo As rdoQuery
Dim qyDoing As rdoQuery
Dim rdDoing As rdoResultset

Err = 0

Call BuscarAsistenciaDesdePaciente(numHistoria, codAsistencia)
objApp.BeginTrans
strOrder = "SELECT ORDERDBOID FROM FR3200 WHERE FR66CODPETICION=? " & _
"AND FR28NUMLINEA=?"
Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
    qyOrder(0) = CodPeticion
    qyOrder(1) = numLinea
Set rdOrder = qyOrder.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
OrderDboid = rdOrder!OrderDboid
rdOrder.Close
qyOrder.Close

'Obtener la ultima fecha sin validar
strTodo = "SELECT TODODATE FROM TASKS WHERE ORDERDBOID=? AND TASKSTATUSDBOID=? " & _
"ORDER BY TODODATE ASC"
Set qyTodo = objApp.rdoConnect.CreateQuery("", strTodo)
    qyTodo(0) = OrderDboid
    qyTodo(1) = "170000000000000000000" 'todo
Set rdTodo = qyTodo.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rdTodo.EOF Then
    MsgBox "Esta orden m�dica no se puede eliminar", vbCritical, "Aviso"
    rdTodo.Close
    qyTodo.Close
    Exit Function
Else
rdTodo.MoveFirst
End If
'Comprobar si ser�a una orden cancelada o anulada. Para ello vemos
'si la orden se est� haciendo
strSql = "SELECT COUNT(*) FROM ORDERS WHERE ORDERDBOID=? " & _
"AND ORDERSTATUSDBOID =?"
Set qyDoing = objApp.rdoConnect.CreateQuery("", strSql)
    qyDoing(0) = OrderDboid
    qyDoing(1) = "170000000000006000000" 'doing
Set rdDoing = qyDoing.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

'Modificar el registro para anular la OM

strUp = "UPDATE ORDERS SET LASTDATE=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ORDERSTATUSDBOID=?"
strUp = strUp & " WHERE ORDERDBOID =?"
 Set qyUp = objApp.rdoConnect.CreateQuery("", strUp)
 qyUp(0) = Format(rdTodo!TODODATE, "DD/MM/YYYY HH:MM:SS")
 If rdDoing.rdoColumns(0).Value >= 1 Then
    qyUp(1) = "170000000000010000000" 'Discontinued
 Else
    qyUp(1) = "170000000000008000000" 'Cancel
 End If
 qyUp(2) = OrderDboid
 qyUp.Execute
rdDoing.Close
rdTodo.Close
qyDoing.Close
qyTodo.Close
qyUp.Close

strSql = "SELECT TASKDBOID FROM TASKS WHERE ORDERDBOID=? " & _
"AND TASKSTATUSDBOID NOT IN (?,?) "
strSql = strSql & " ORDER BY TODODATE"
Set qySql = objApp.rdoConnect.CreateQuery("", strSql)
    qySql(0) = OrderDboid
    qySql(1) = "170000000000001000000" 'done
    qySql(2) = "170000000000003000000" 'Hold

Set rdSql = qySql.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
rdSql.MoveFirst
blnFirst = True
Do While Not rdSql.EOF
    strTask = "UPDATE TASKS SET DONEDATE=SYSDATE, TASKSTATUSDBOID=? " & _
    "WHERE TASKDBOID= ?"
    Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
        If blnFirst Then
            qytask(0) = "170000000000009000000" 'firstDiscontinued (la primera linea tiene este status
            blnFirst = False
        Else
            qytask(0) = "170000000000010000000" 'Discontinued
        End If
        qytask(1) = rdSql.rdoColumns(0).Value
    qytask.Execute
    rdSql.MoveNext
Loop
rdSql.Close
qySql.Close
qytask.Close

If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "ERROR: La cancelaci�n de orden m�dica no se ha pasado en Picis"
    Exit Function
Else: objApp.CommitTrans
End If

End Function


'Public Function OrdenProductoFR3200(numHistoria As Long, CodPeticion As Long, _
'numLinea As Long, OrderDboid As String)
'Dim codAsistencia As Long
'Dim strCUN As String
'Dim strMedicamento As String
'Dim strPiDboid, strMed, strUnit, strRuta, strForma As String
'Dim rdcun As rdoResultset
'Dim rdmedicamento As rdoResultset
'Dim rdPiDboid As rdoResultset
'Dim rdMed As rdoResultset
'Dim rdunit As rdoResultset
'Dim rdRuta As rdoResultset
'Dim rdforma As rdoResultset
'Dim qyCun As rdoQuery
'Dim qymedicamento As rdoQuery
'Dim qyPiDboid As rdoQuery
'Dim qyMed As rdoQuery
'Dim qyUnit As rdoQuery
'Dim qyRuta As rdoQuery
'Dim qyForma As rdoQuery
'Dim strPicis As String
'Dim qyPicis As rdoQuery
'Dim FechaInicio As String
'Dim FechaFin As String
'Dim FechaTask As String
'Dim rdTask As rdoResultset
'Dim qytask As rdoQuery
'Dim strTask, Unidades As String
'Dim i As Integer
'Dim strDia As String
'Dim blnPrimeraAdministracion As Boolean
'Dim strInicio As String
'Dim qyInicio As rdoQuery
'Dim staff As String
'Dim ExtraInfo As Double
''On Error Resume Next
'
'Err = 0
'blnError = False
'Call BuscarAsistenciaDesdePaciente(numHistoria, codAsistencia)
'strCUN = "SELECT FR73CODPRODUCTO,FR28DOSIS,FR34CODVIA,FRG4CODFRECUENCIA, " & _
'"FR93CODUNIMEDIDA,FR32INDISPEN,FR32INDCOMIENINMED,FRH5CODPERIODICIDAD , " & _
'"FRH7CODFORMFAR, FR32OPERACION, FR32CANTIDAD, FR32FECINICIO, FR32HORAINICIO, " & _
'"FR32FECFIN, FR32HORAFIN FROM FR3200 WHERE " & _
'"FR66CODPETICION= ? AND FR28NUMLINEA= ? "
'Set qyCun = objApp.rdoConnect.CreateQuery("", strCUN)
'    qyCun(0) = CodPeticion
'    qyCun(1) = numLinea
'Set rdcun = qyCun.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'
''Obtener OrderDBOID
'ierrcode = PcsOrderAlloc(hOrder)
'ierrcode = PcsDbObjectGenerateIdentifier(hOrder)
'ierrcode = PcsDbObjectGetIdentifier(hOrder, OrderDboid, 21)
'ierrcode = PcsOrderFree(hOrder)
'
''Obtener Nombre de medicamento
'strMedicamento = "SELECT FR73DESPRODUCTO,TREATMENTDBOID FROM FR7300 WHERE FR73CODPRODUCTO = ? "
'Set qymedicamento = objApp.rdoConnect.CreateQuery("", strMedicamento)
'qymedicamento(0) = rdcun!fr73codproducto
'Set rdmedicamento = qymedicamento.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'
''OBTENER LAS FECHAS DE INICIO Y FIN
'FechaInicio = Format(Format(rdcun!FR32FECINICIO, "dd/mm/yyyy") & " " & Format(rdcun!FR32HORAINICIO & ":00:00", "HH:MM:SS"), "DD/MM/YYYY HH:MM:SS")
'If Not IsNull(rdcun!FR32FECFIN) Then
'    FechaFin = Format$(rdcun!FR32FECFIN & " " & Format(rdcun!FR32HORAFIN & ":00:00", "HH:MM:SS"), "YYYY-MM-DD HH")
'Else
'    FechaFin = Format$(DateAdd("d", 1, CDate(FechaInicio)), "DD/MM/YYYY HH:MM:SS")
'    'Fecha provisional que a�ade dos dias a la fecha fin sino
'    'existe.
'End If
'
''Obtener el PicisdataDboid
'strPiDboid = "SELECT PICISDATA.PICISDATADBOID From ADMISSIONS, PICISDATA " & _
'"Where ADMISSIONS.ADMISSIONDBOID = PICISDATA.ADMISSIONDBOID " & _
'"AND ADMISSIONS.ADMID1= ?"
'Set qyPiDboid = objApp.rdoConnect.CreateQuery("", strPiDboid)
'qyPiDboid(0) = codAsistencia
'Set rdPiDboid = qyPiDboid.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'
''OBTENER EL MEDICO QUE LO HA RECETADO
'strMed = "SELECT STAFF.STAFFDBOID FROM FR3200, FR6600, STAFF " & _
'"Where FR3200.FR66CODPETICION = FR6600.FR66CODPETICION And (FR3200.FR66CODPETICION = ?) " & _
'"AND FR6600.SG02COD_MED=STAFF.USERNAME"
'Set qyMed = objApp.rdoConnect.CreateQuery("", strMed)
'qyMed(0) = CodPeticion
'Set rdMed = qyMed.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'    staff = rdMed!StaffDboid
'rdMed.Close
'qyMed.Close
''Obtener el UnitDboid
'strUnit = "SELECT UNITDBOID FROM FR9300 WHERE FR93CODUNIMEDIDA=?"
'Set qyUnit = objApp.rdoConnect.CreateQuery("", strUnit)
'qyUnit(0) = rdcun!FR93CODUNIMEDIDA
'Set rdunit = qyUnit.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'Unidades = rdunit!UNITDBOID
'rdunit.Close
'qyUnit.Close
'
''Obtener el RouteDboid
'If Not IsNull(rdcun!FR34CODVIA) Then
'    strRuta = "SELECT ROUTEDBOID FROM  FR3400 WHERE FR34CODVIA=?"
'    Set qyRuta = objApp.rdoConnect.CreateQuery("", strRuta)
'    qyRuta(0) = rdcun!FR34CODVIA
'    Set rdRuta = qyRuta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'End If
'
''Obtener el FormDboid
'If Not IsNull(rdcun!FRH7CODFORMFAR) Then
'    strForma = "SELECT FORMDBOID FROM  FRH700 WHERE FRH7CODFORMFAR=?"
'    Set qyForma = objApp.rdoConnect.CreateQuery("", strForma)
'    qyForma(0) = rdcun!FRH7CODFORMFAR
'    Set rdforma = qyForma.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'End If
'If rdcun!FR32INDISPEN Then
'    '*******************************************
'    'Modificar Calcularextrainfo si se modifica alg�na frecuencia
'    ExtraInfo = CalcularExtraInfo(rdcun!FRG4CODFRECUENCIA)
'    '*******************************************
'Else
'    ExtraInfo = 4
'End If
'
'objApp.BeginTrans
'
'strPicis = "INSERT INTO ORDERS (ORDERDBOID,ORDERDESC,CREATIONDATE, CONDITION, " & _
'"INITDATE, LASTDATE, HASAMEMO, HIGHERDOSE, " & _
'"LOWERDOSE, ISFROMRESCHEDULE, PICISDATADBOID, STAFFDBOID, ORDERTYPEDBOID, " & _
'"ORDERSTATUSDBOID, TREATMENTDBOID, ORDERSETDBOID, GROUPDBOID, UNITDBOID, " & _
'"ROUTEDBOID, FORMDBOID, TASKGROUPDBOID, STDORDERDBOID, EXTRAINFO) " & _
'"VALUES (?, ?, SYSDATE, ?, " & _
'"TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), " & _
'"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
'
'Set qyPicis = objApp.rdoConnect.CreateQuery("", strPicis)
'qyPicis(0) = OrderDboid
'qyPicis(1) = CStr(rdmedicamento!FR73DESPRODUCTO) 'OrderDesc
'qyPicis(2) = Null 'Condicion de la orden m�dica
'qyPicis(3) = FechaInicio 'Initdate
'qyPicis(4) = FechaFin 'lastdate
'qyPicis(5) = "F" 'hasaMemo
'qyPicis(6) = CStr(rdcun!FR28DOSIS) 'Higherdose
'qyPicis(7) = CStr(rdcun!FR28DOSIS) 'lowerdose
'qyPicis(8) = "F" 'IsfromReschedule
'qyPicis(9) = rdPiDboid!PICISDATADBOID 'PicisdataDboid
'qyPicis(10) = staff 'Usuario que ha creado la orden m�dica
'If rdcun!FR32INDISPEN = True Then 'eS UNA ORDEN PRN
'    qyPicis(11) = "169000000000002000000" 'orden PRN
'Else 'eS PERIODICO
'    qyPicis(11) = "169000000000000000000" 'orden peri�dica
'End If
'qyPicis(12) = "170000000000000000000" 'TODO siempre asi cuando se crean
'qyPicis(13) = rdmedicamento!TREATMENTDBOID 'TratmentDboid
'qyPicis(14) = Null 'orderSetdboid
'qyPicis(15) = "21117459741110005511" 'Masin:Revisar cuando este creada la nueva base de
'                                    'datos. Grupo= Sin Usuarios
'If Not IsNull(rdcun!FR93CODUNIMEDIDA) Then
'    qyPicis(16) = Unidades 'Unitdboid
'Else
'    qyPicis(16) = "45000000000000000000" 'Unidad de medida no especificada
'End If
'
'If Not IsNull(rdcun!FR34CODVIA) Then
'    qyPicis(17) = rdRuta!RouteDboid 'routedboid
'    rdRuta.Close
'    qyRuta.Close
'Else
'    qyPicis(17) = "155000000000000000000" 'RoutDboid Desconocida
'End If
'If Not IsNull(rdcun!FRH7CODFORMFAR) Then
'    qyPicis(18) = rdforma!FORMDBOID
'    rdforma.Close
'    qyForma.Close
'Else
'    qyPicis(18) = "151000000000000000000" 'Desconocido FormDboid
'End If
'qyPicis(19) = "21000000000210000511" 'Grupo validaci�n enfermeras.
'                                     'MASIN:Revisar para la pr�xima base de datos
'qyPicis(20) = Null 'StdOrderDboid
'qyPicis(21) = ExtraInfo
'
'qyPicis.Execute
'
'rdmedicamento.Close
'rdPiDboid.Close
'qymedicamento.Close
'qyPiDboid.Close
'qyPicis.Close
'
'If Not rdcun!FR32INDISPEN Then 'Si no es PRN escribir los Tasks
'    'escribir los registros en la tabla de Task
'    'FechaTask = rdCUN!FR32FECINICIO
'    blnPrimeraAdministracion = True
'    FechaTask = FechaInicio
'    Dim FechaUltima As Date
'    Select Case rdcun!FRH5CODPERIODICIDAD
'    Case "Alternos"
'        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= 0
'            'funcion
'            Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun!FRG4CODFRECUENCIA, _
'                rdcun!FR28DOSIS, OrderDboid, staff, Unidades, _
'                FechaInicio, FechaFin, blnPrimeraAdministracion)
'            FechaTask = DateAdd("d", 2, CDate(FechaTask))
'        Loop
'    Case "Diario"
'        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= 0
'            'funcion
'            Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun!FRG4CODFRECUENCIA, _
'                rdcun!FR28DOSIS, OrderDboid, staff, Unidades, _
'                FechaInicio, FechaFin, blnPrimeraAdministracion)
'            FechaTask = DateAdd("d", 1, CDate(FechaTask))
'        Loop
'    Case "Domingo No"
'        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= 0
'            strDia = Format(FechaTask, "dddd")
'            If strDia <> "domingo" Then
'                'Funcion
'                Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun!FRG4CODFRECUENCIA, _
'                rdcun!FR28DOSIS, OrderDboid, staff, Unidades, _
'                FechaInicio, FechaFin, blnPrimeraAdministracion)
'            End If
'            FechaTask = DateAdd("d", 1, CDate(FechaTask))
'        Loop
'    Case "S�bado No"
'        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= 0
'            strDia = Format(FechaTask, "dddd")
'            If strDia <> "s�bado" Then
'             'funcion
'                Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun!FRG4CODFRECUENCIA, _
'                rdcun!FR28DOSIS, OrderDboid, staff, Unidades, _
'                FechaInicio, FechaFin, blnPrimeraAdministracion)
'            End If
'            FechaTask = DateAdd("d", 1, CDate(FechaTask))
'        Loop
'    Case "Lunes No"
'        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= 0
'            strDia = Format(FechaTask, "dddd")
'            If strDia <> "lunes" Then
'                Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun!FRG4CODFRECUENCIA, _
'                    rdcun!FR28DOSIS, OrderDboid, staff, Unidades, _
'                    FechaInicio, FechaFin, blnPrimeraAdministracion)
'            End If
'            FechaTask = DateAdd("d", 1, CDate(FechaTask))
'        Loop
'    Case "Pares"
'        If EsPar(rdcun!FR32FECINICIO) Then
'            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= 0
'                'Funcion
'                Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun!FRG4CODFRECUENCIA, _
'                    rdcun!FR28DOSIS, OrderDboid, staff, Unidades, _
'                    FechaInicio, FechaFin, blnPrimeraAdministracion)
'                FechaTask = DateAdd("d", 2, CDate(FechaTask))
'            Loop
'        Else
'            FechaTask = DateAdd("d", 1, FechaTask)
'            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= 0
'                Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun!FRG4CODFRECUENCIA, _
'                    rdcun!FR28DOSIS, OrderDboid, staff, Unidades, _
'                    FechaInicio, FechaFin, blnPrimeraAdministracion)
'                FechaTask = DateAdd("d", 2, CDate(FechaTask))
'            Loop
'        End If
'    Case "Impares"
'        If Not EsPar(rdcun!FR32FECINICIO) Then
'            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= 0
'                'Funcion
'                Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun!FRG4CODFRECUENCIA, _
'                    rdcun!FR28DOSIS, OrderDboid, staff, Unidades, _
'                    FechaInicio, FechaFin, blnPrimeraAdministracion)
'                FechaTask = DateAdd("d", 2, CDate(FechaTask))
'            Loop
'        Else
'            FechaTask = DateAdd("d", 1, FechaTask)
'            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= 0
'                'Funcion
'                Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun!FRG4CODFRECUENCIA, _
'                    rdcun!FR28DOSIS, OrderDboid, staff, Unidades, _
'                    FechaInicio, FechaFin, blnPrimeraAdministracion)
'                FechaTask = DateAdd("d", 2, CDate(FechaTask))
'            Loop
'        End If
'    End Select
'End If
'rdcun.Close
'qyCun.Close
''Actualizamos las orden m�dica en Picis para que cada orden de producto
''empiece con la primera task y acabe con la �ltima
'strInicio = "UPDATE ORDERS SET INITDATE=TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS') " & _
'" , LASTDATE= TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS') WHERE ORDERDBOID=?"
'Set qyInicio = objApp.rdoConnect.CreateQuery("", strInicio)
'    qyInicio(0) = Format(FechaInicio, "DD/MM/YYYY HH:MM:SS")
'    qyInicio(1) = Format(FechaFin, "DD/MM/YYYY HH:MM:SS")
'    qyInicio(2) = OrderDboid
'qyInicio.Execute
'qyInicio.Close
'
''Aqui escribiriamos lo de los memos llamando a una funci�n
''Call EscribirMemo(OrderDboid, rdcun!FR28INSTRADMIN, ObtenerStaff(rdcun!FR66CODPETICION))
'
'    If Err > 0 Then
'        objApp.RollbackTrans
'        MsgBox "ERROR: La orden m�dica no ha pasado a Picis"
'        blnError = True
'        Exit Function
'    Else: objApp.CommitTrans
'    End If
'End Function

Public Sub OrdenProducto(CodPeticion As Long, _
numLinea As Long, blnfr28 As Boolean, blnfrp4 As Boolean)
'*************************************************
'/* Esta funci�n crea las ordenes m�dicas de Picis*/
'Saca de la tabla FR2800 todos los datos que necesita
'y los guarda en el resulset rdCun.
'Si la orden es periodica continua se desvia la funcion para
'crear varias ordenes medicas, siendo cada una de ellas continuas.
'De esta forma creamos las OM periodicas-continuas.
'Si la orden medica no es un Prn ni tiene como via la
'Perfusion continua, se crean tasks en picis.
'A esto se accede por medio de la funcion Periodicidad.
'Despues se a�ade el memo.
'*************************************************
Dim OrderDboid As String * 21
Dim codAsistencia As Long
Dim strCUN As String
Dim strRuta, strForma As String
Dim rdRuta As rdoResultset
Dim rdforma As rdoResultset
Dim rdcun As rdoResultset
Dim qyCun As rdoQuery
Dim qyRuta As rdoQuery
Dim qyForma As rdoQuery
Dim strPicis As String
Dim qyPicis As rdoQuery
Dim FechaInicio As String
Dim FechaFin As String
Dim rdTask As rdoResultset
Dim qytask As rdoQuery
Dim strTask, Unidades As String
Dim i As Integer
Dim strDia As String
Dim strInicio As String
Dim qyInicio As rdoQuery
Dim staff As String
Dim ExtraInfo As Double
Dim Operacion As String
Dim codProducto1 As Long
Dim IndPerfusion As Boolean
Dim via As String
Dim CodProducto2 As Long
Dim codProducto3 As Long
Dim rdAditivo As rdoResultset
Dim rdAditivo2 As rdoResultset
Dim strOrder As String
Dim qyOrder As rdoQuery
Dim intTipoOm As Integer 'Determina que tipo de OM es
Dim FechaAdministracion As String
Dim FechaUltimaAdministracion As String 'Variable que almacena la fecha del ultimo Task
Dim Observaciones As String
Dim blnPrnfluido As Boolean
'On Error Resume Next

Err = 0
blnError = False
If blnfr28 Then
    strCUN = "SELECT * FROM FR2800 WHERE " & _
    "FR66CODPETICION= ? AND FR28NUMLINEA= ? "
    Set qyCun = objApp.rdoConnect.CreateQuery("", strCUN)
        qyCun(0) = CodPeticion
        qyCun(1) = numLinea
    Set rdcun = qyCun.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    Operacion = rdcun!FR28OPERACION
    If Not IsNull(rdcun!FR28INDPERF) Then
        IndPerfusion = rdcun!FR28INDPERF
    Else
        IndPerfusion = False
    End If
    'OBTENER LAS FECHAS DE INICIO Y FIN
    FechaInicio = Format(Format(rdcun!FR28FECINICIO, "dd/mm/yyyy") & " " & Format(rdcun!FR28HORAINICIO & ":00:00", "HH:MM:SS"), "DD/MM/YYYY HH:MM:SS")
    If Not IsNull(rdcun!FR28FECFIN) Then
        FechaFin = Format$(Format(rdcun!FR28FECFIN, "dd/mm/yyyy") & " " & Format(rdcun!FR28HORAFIN & ":00:00", "HH:MM:SS"), "DD/MM/YYYY HH:MM:SS")
    Else
      If Operacion = "P" Then 'Si es PRN la orden dura 7 dias
        FechaFin = Format$(DateAdd("d", 7, CDate(FechaInicio)), "DD/MM/YYYY HH:MM:SS")
      Else 'sino dura 1 dia
        FechaFin = Format$(DateAdd("d", 1, CDate(FechaInicio)), "DD/MM/YYYY HH:MM:SS")
      End If
        FechaFin = Format$(DateAdd("n", -1, CDate(FechaFin)), "DD/MM/YYYY HH:MM:SS")
        'Fecha provisional que a�ade dos dias a la fecha fin sino
        'existe.
    End If
    If Not IsNull(rdcun!FR28INSTRADMIN) Then
        Observaciones = rdcun!FR28INSTRADMIN
    Else
        Observaciones = ""
    End If
ElseIf blnfrp4 Then
    strCUN = "SELECT * FROM FRP400 WHERE " & _
    "FRP4CODPETICION= ? "
    Set qyCun = objApp.rdoConnect.CreateQuery("", strCUN)
        qyCun(0) = CodPeticion
    Set rdcun = qyCun.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    Operacion = rdcun!FRP4OPERACION
    If Not IsNull(rdcun!FRP4INDPERF) Then
        IndPerfusion = rdcun!FRP4INDPERF
    Else
        IndPerfusion = False
    End If
    'OBTENER LAS FECHAS DE INICIO Y FIN
    FechaInicio = Format(Format(rdcun!FRP4FECINICIO, "dd/mm/yyyy") & " " & Format(rdcun!FRP4HORAINICIO & ":00:00", "HH:MM:SS"), "DD/MM/YYYY HH:MM:SS")
    If Not IsNull(rdcun!FRP4FECFIN) Then
        FechaFin = Format$(Format(rdcun!FRP4FECFIN, "dd/mm/yyyy") & " " & Format(rdcun!FRP4HORAFIN & ":00:00", "HH:MM:SS"), "DD/MM/YYYY HH:MM:SS")
    Else
      If Operacion <> "P" Then
          FechaFin = Format$(DateAdd("d", 1, CDate(FechaInicio)), "DD/MM/YYYY HH:MM:SS")
      Else
          FechaFin = Format$(DateAdd("d", 2, CDate(FechaInicio)), "DD/MM/YYYY HH:MM:SS")
      End If
        FechaFin = Format$(DateAdd("n", -1, CDate(FechaFin)), "DD/MM/YYYY HH:MM:SS")
        'Fecha provisional que a�ade dos dias a la fecha fin sino
        'existe.
    End If
    If Not IsNull(rdcun!FRP4INSTRADMIN) Then
        Observaciones = rdcun!FRP4INSTRADMIN
    Else
        Observaciones = ""
    End If
End If

'Obtener Nombre de medicamento
''Operacion = rdcun!FR28OPERACION
Select Case Operacion
Case "F"
    codProducto1 = rdcun!FR73CODPRODUCTO_DIL 'Importante el fluido
    If Not IsNull(rdcun!FR73CODPRODUCTO) Then
        CodProducto2 = rdcun!FR73CODPRODUCTO
    Else: CodProducto2 = 0
    End If
    If Not IsNull(rdcun!fr73codproducto_2) Then
        codProducto3 = rdcun!fr73codproducto_2
    Else: codProducto3 = 0
    End If
Case "M"
    codProducto1 = rdcun!FR73CODPRODUCTO_DIL 'Importante el fluido
    If Not IsNull(rdcun!FR73CODPRODUCTO) Then
        CodProducto2 = rdcun!FR73CODPRODUCTO
    Else: CodProducto2 = 0
    End If
    If Not IsNull(rdcun!fr73codproducto_2) Then
        codProducto3 = rdcun!fr73codproducto_2
    Else: codProducto3 = 0
    End If
Case "P"
'Si el PRN fuera medicamento
'  If IsNull(rdcun!FR73CODPRODUCTO) Then 'PRN con fluido
'      codProducto1 = rdcun!FR73CODPRODUCTO_DIL 'Importante el fluido
'      blnPRNFluido = True
'      If Not IsNull(rdcun!FR73CODPRODUCTO) Then
'        CodProducto2 = rdcun!FR73CODPRODUCTO
'      Else: CodProducto2 = 0
'      End If
'      If Not IsNull(rdcun!fr73codproducto_2) Then
'          codProducto3 = rdcun!fr73codproducto_2
'      Else: codProducto3 = 0
'      End If
'  Else
'      codProducto1 = rdcun!FR73CODPRODUCTO 'Importante el medicamento
'      If Not IsNull(rdcun!FR73CODPRODUCTO_DIL) Then
'         CodProducto2 = rdcun!FR73CODPRODUCTO_DIL
'      Else: CodProducto2 = 0
'      End If
'  End If

'PRN de fluidoterapia
    blnPrnfluido = True
    codProducto1 = rdcun!FR73CODPRODUCTO_DIL 'Importante el fluido
    If Not IsNull(rdcun!FR73CODPRODUCTO) Then
        CodProducto2 = rdcun!FR73CODPRODUCTO
    Else: CodProducto2 = 0
    End If
    If Not IsNull(rdcun!fr73codproducto_2) Then
        codProducto3 = rdcun!fr73codproducto_2
    Else: codProducto3 = 0
    End If
Case "/"
 codProducto1 = rdcun!FR73CODPRODUCTO 'Importante el medicamento
  If Not IsNull(rdcun!FR73CODPRODUCTO_DIL) Then
     CodProducto2 = rdcun!FR73CODPRODUCTO_DIL
  Else: CodProducto2 = 0
  End If
End Select

via = rdcun!FR34CODVIA
intTipoOm = ObtenerMedicamento(Operacion, via, codProducto1, IndPerfusion, _
rdmedicamento, CodProducto2, codProducto3, rdAditivo, rdAditivo2, blnPrnfluido)

'Obtener el FormDboid
If Not IsNull(rdcun!FRH7CODFORMFAR) Then
    strForma = "SELECT FORMDBOID FROM  FRH700 WHERE FRH7CODFORMFAR=?"
    Set qyForma = objApp.rdoConnect.CreateQuery("", strForma)
    qyForma(0) = rdcun!FRH7CODFORMFAR
    Set rdforma = qyForma.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
End If

If Operacion = "P" Then
    If Not IsNull(rdcun!FRG4CODFRECUENCIA) Then
        '*******************************************
        'Modificar Calcularextrainfo si se modifica alg�na frecuencia
        ExtraInfo = CalcularExtraInfo(rdcun!FRG4CODFRECUENCIA, CodPeticion)
        '*******************************************
    Else
        ExtraInfo = 4
    End If
Else
    ExtraInfo = 4
End If

objApp.BeginTrans
If (intTipoOm = 7 Or intTipoOm = 2 Or intTipoOm = 6 Or intTipoOm = 8) _
And rdcun!FR34CODVIA = "PC" Then
'Si la orden es Periodica Continiua (en fluidoterapia) el codigo se desvia
'por este camino
    Call OrdenPeriodicaContinua(CodPeticion, numLinea, _
                    rdcun, FechaInicio, FechaFin, via, _
                    rdforma, rdmedicamento, OrderDboid, _
                    ExtraInfo, intTipoOm, rdAditivo, codProducto1, CodProducto2, _
                    codProducto3, rdAditivo2, blnfr28, blnfrp4)
    If Not IsNull(rdcun!FRH7CODFORMFAR) Then
        rdforma.Close
        qyForma.Close
    End If
    rdmedicamento.Close
    qymedicamento.Close
    rdcun.Close
    qyCun.Close
    Exit Sub
End If

Call Orders(rdcun, FechaInicio, FechaFin, via, _
rdforma, rdmedicamento, OrderDboid, _
ExtraInfo, intTipoOm, rdAditivo, codProducto1, CodProducto2, _
codProducto3, rdAditivo2, blnfr28, blnfrp4, blnPrnfluido)

'Si no es PRN escribir los Tasks escribir los registros en la tabla de Task
If (Not Operacion = "P") And (rdcun!FR34CODVIA <> "PC") Then
    'FechaTask = rdCUN!FR28FECINICIO
    Call Periodicidad(rdcun, FechaInicio, OrderDboid, FechaFin, _
    FechaAdministracion, intTipoOm, FechaUltimaAdministracion, blnfr28, blnfrp4)
    
    'Actualizamos las orden m�dica en Picis para que cada orden de producto
    'empiece con la primera task y acabe con la �ltima
    strInicio = "UPDATE ORDERS SET INITDATE=TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS') " & _
    " , LASTDATE= TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS') WHERE ORDERDBOID=?"
    Set qyInicio = objApp.rdoConnect.CreateQuery("", strInicio)
        qyInicio(0) = Format(FechaInicio, "DD/MM/YYYY HH:MM:SS")
        qyInicio(1) = Format(FechaUltimaAdministracion, "DD/MM/YYYY HH:MM:SS")
        qyInicio(2) = OrderDboid
    qyInicio.Execute
    qyInicio.Close
End If
    
'Aqui escribiriamos lo de los memos llamando a una funci�n
If Trim(Observaciones) <> "" Then
    Call EscribirMemo(OrderDboid, Observaciones, ObtenerStaff(CodPeticion, blnfr28, blnfrp4))
End If
If Not IsNull(rdcun!FRH7CODFORMFAR) Then
    rdforma.Close
    qyForma.Close
End If
rdmedicamento.Close
qymedicamento.Close
rdcun.Close
qyCun.Close

If Err > 0 Then
    objApp.RollbackTrans
    MsgBox "ERROR: La orden m�dica no ha pasado a Picis" & Chr(13) & _
    "Contacte con su administrador de sistema"
    blnError = True
    Exit Sub
Else: objApp.CommitTrans
End If
'Introducir el Orderdboid en la tabla si no ha habido ningun error
If Not blnError Then
    If blnfr28 Then
            strOrder = "INSERT INTO FRP300 (FR66CODPETICION, FR28NUMLINEA, ORDERDBOID)"
            strOrder = strOrder & " VALUES(?,?,?)"
            Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
                qyOrder(0) = CodPeticion
                qyOrder(1) = numLinea
                qyOrder(2) = OrderDboid
            qyOrder.Execute
    ElseIf blnfrp4 Then
            strOrder = "INSERT INTO FRP500 (FRP4CODPETICION,  ORDERDBOID)"
            strOrder = strOrder & " VALUES(?,?)"
            Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
                qyOrder(0) = CodPeticion
                qyOrder(1) = OrderDboid
            qyOrder.Execute
    End If
End If
End Sub

Public Function ObtenerDescripcionOrden(rdcun As rdoResultset, _
codProducto1 As Long, CodProducto2 As Long, codProducto3 As Long, _
intTipoOm As Integer, blnfr28 As Boolean, blnfrp4 As Boolean, blnPrnfluido As Boolean)
'****************************************************************
'En esta funcion fabricamos el string de la orden medica
'****************************************************************
Dim DesOrdenMedica As String
Dim strProducto2 As String
Dim qyProducto As rdoQuery
Dim rdProducto As rdoResultset
Dim strProductoM As String
Dim qyProductoM As rdoQuery
Dim rdProductoM As rdoResultset
Dim longitud As Long
Dim TIEMMININF As Long
Dim CantidadDil As Long
Dim Dosis As Long
Dim Dosis_2 As Long
'Obtenemos el nombre del producto secundario de la orden: sueros
'y aditivos
strProducto2 = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=?"
Set qyProducto = objApp.rdoConnect.CreateQuery("", strProducto2)
    qyProducto(0) = CodProducto2
Set rdProducto = qyProducto.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

If blnfr28 Then
    If Not IsNull(rdcun!FR28TIEMMININF) Then TIEMMININF = rdcun!FR28TIEMMININF Else TIEMMININF = 0
    If Not IsNull(rdcun!FR28CANTIDADDIL) Then CantidadDil = rdcun!FR28CANTIDADDIL Else CantidadDil = 0
    If Not IsNull(rdcun!FR28DOSIS) Then Dosis = rdcun!FR28DOSIS Else Dosis = 0
    If Not IsNull(rdcun!FR28DOSIS_2) Then Dosis_2 = rdcun!FR28DOSIS_2 Else Dosis_2 = 0
ElseIf blnfrp4 Then
    If Not IsNull(rdcun!FRP4TIEMMININF) Then TIEMMININF = rdcun!FRP4TIEMMININF Else TIEMMININF = 0
    If Not IsNull(rdcun!FRP4CANTIDADDIL) Then CantidadDil = rdcun!FRP4CANTIDADDIL Else CantidadDil = 0
    If Not IsNull(rdcun!FRP4DOSIS) Then Dosis = rdcun!FRP4DOSIS Else Dosis = 0
    If Not IsNull(rdcun!FRP4DOSIS_2) Then Dosis_2 = rdcun!FRP4DOSIS_2 Else Dosis_2 = 0
End If

    If intTipoOm <> 7 And intTipoOm <> 8 And (intTipoOm <> 3) Then 'Si hay PRN no fluido hay que cambiarlo
        longitud = Len(rdmedicamento!FR73DESPRODUCTO)
        DesOrdenMedica = UCase(Mid(rdmedicamento!FR73DESPRODUCTO, 1, 1)) & _
        LCase(Mid(rdmedicamento!FR73DESPRODUCTO, 2, longitud - 1))
        DesOrdenMedica = DesOrdenMedica & "; "
        If Dosis <> 0 Then
            DesOrdenMedica = DesOrdenMedica & CStr(Dosis) & ". " & _
            rdcun!FR93CODUNIMEDIDA
        End If
        If intTipoOm = 2 Or intTipoOm = 4 Or intTipoOm = 6 Then
          If CodProducto2 <> 0 Then 'perfusiones sin suero
            longitud = Len(rdProducto!FR73DESPRODUCTO)
              DesOrdenMedica = DesOrdenMedica & " (+" & _
                                UCase(Mid(rdProducto!FR73DESPRODUCTO, 1, 1)) & _
                                LCase(Mid(rdProducto!FR73DESPRODUCTO, 2, longitud - 1)) & _
                                "; " & CantidadDil & "ml"
           Else
            DesOrdenMedica = DesOrdenMedica & " " & CantidadDil & "ml"
           End If
              If TIEMMININF <> 0 Then
                    DesOrdenMedica = DesOrdenMedica & "). Tiempo inf.=" & TIEMMININF & " m."
              ElseIf CodProducto2 <> 0 Then
                    DesOrdenMedica = DesOrdenMedica & ")"
              End If
        
        End If
        If Not IsNull(rdcun!FR34CODVIA) Then
            DesOrdenMedica = DesOrdenMedica & " " & rdcun!FR34CODVIA & ". "
        End If
        If Not IsNull(rdcun!FRG4CODFRECUENCIA) Then
            DesOrdenMedica = DesOrdenMedica & rdcun!FRG4CODFRECUENCIA
        End If
    
    ElseIf (intTipoOm = 7 Or intTipoOm = 3) And codProducto3 = 0 Then 'si es fluidoterapia : primero el fluido y despues el aditivo
    
        longitud = Len(rdmedicamento!FR73DESPRODUCTO)
        DesOrdenMedica = UCase(Mid(rdmedicamento!FR73DESPRODUCTO, 1, 1)) & _
        LCase(Mid(rdmedicamento!FR73DESPRODUCTO, 2, longitud - 1))
        DesOrdenMedica = DesOrdenMedica & "; "
    
        If CantidadDil <> 0 Then
            DesOrdenMedica = DesOrdenMedica & CStr(CantidadDil) & " ml"
        End If
        If Not IsNull(rdProducto!FR73DESPRODUCTO) And CodProducto2 <> 0 Then
              longitud = Len(rdmedicamento!FR73DESPRODUCTO)
              DesOrdenMedica = DesOrdenMedica & " (+" & _
                                UCase(Mid(rdProducto!FR73DESPRODUCTO, 1, 1)) & _
                                LCase(Mid(rdProducto!FR73DESPRODUCTO, 2, longitud - 1)) & _
                                "; " & Dosis & " " & rdcun!FR93CODUNIMEDIDA
    
              If TIEMMININF <> 0 Then
                    DesOrdenMedica = DesOrdenMedica & "). Tiempo inf.=" & TIEMMININF & " m."
              Else
                    DesOrdenMedica = DesOrdenMedica & ")"
              End If
        Else
              If TIEMMININF <> 0 Then
                    DesOrdenMedica = DesOrdenMedica & ". Tiempo inf.=" & TIEMMININF & " m."
              End If
        End If
        If Not IsNull(rdcun!FR34CODVIA) Then
            DesOrdenMedica = DesOrdenMedica & " " & rdcun!FR34CODVIA & "."
        End If
        If Not IsNull(rdcun!FRG4CODFRECUENCIA) Then
            DesOrdenMedica = DesOrdenMedica & rdcun!FRG4CODFRECUENCIA
        End If
    
    Else 'Si es una mezcla: fluido + aditivos o un fluido con dos aditivos
        strProductoM = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=?"
        Set qyProductoM = objApp.rdoConnect.CreateQuery("", strProductoM)
            qyProductoM(0) = codProducto3
        Set rdProductoM = qyProductoM.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    
        longitud = Len(rdmedicamento!FR73DESPRODUCTO)
        DesOrdenMedica = UCase(Mid(rdmedicamento!FR73DESPRODUCTO, 1, 1)) & _
        LCase(Mid(rdmedicamento!FR73DESPRODUCTO, 2, longitud - 1))
        DesOrdenMedica = DesOrdenMedica & "; "
    
        If CantidadDil <> 0 Then
            DesOrdenMedica = DesOrdenMedica & CStr(CantidadDil) & " ml"
        End If
        If Not IsNull(rdProducto!FR73DESPRODUCTO) And CodProducto2 <> 0 _
        And Not IsNull(rdProductoM!FR73DESPRODUCTO) And codProducto3 <> 0 Then
              DesOrdenMedica = DesOrdenMedica & " (+ " & _
                                UCase(Mid(rdProducto!FR73DESPRODUCTO, 1, 1)) & _
                                LCase(Mid(rdProducto!FR73DESPRODUCTO, 2, longitud - 1)) & _
                                "; " & Dosis & " " & rdcun!FR93CODUNIMEDIDA
             DesOrdenMedica = DesOrdenMedica & " + " & _
                            UCase(Mid(rdProductoM!FR73DESPRODUCTO, 1, 1)) & _
                            LCase(Mid(rdProductoM!FR73DESPRODUCTO, 2, longitud - 1)) & _
                            "; " & Dosis_2 & " " & rdcun!FR93CODUNIMEDIDA_2
    
              If TIEMMININF <> 0 Then
                    DesOrdenMedica = DesOrdenMedica & "). Tiempo inf.=" & TIEMMININF & " m."
              Else
                    DesOrdenMedica = DesOrdenMedica & ")"
              End If
        Else
              If TIEMMININF <> 0 Then
                    DesOrdenMedica = DesOrdenMedica & ". Tiempo inf.=" & TIEMMININF & " m."
              End If
        End If
        If Not IsNull(rdcun!FR34CODVIA) Then
            DesOrdenMedica = DesOrdenMedica & " " & rdcun!FR34CODVIA & "."
        End If
        If Not IsNull(rdcun!FRG4CODFRECUENCIA) Then
            DesOrdenMedica = DesOrdenMedica & rdcun!FRG4CODFRECUENCIA
        End If
    rdProductoM.Close
    qyProductoM.Close
    End If
rdProducto.Close
qyProducto.Close
ObtenerDescripcionOrden = DesOrdenMedica
End Function
'Public Function OrdenProductoPRN(numHistoria As Long, codPeticion As Long, _
'numlinea As Long, OrderDboid As String)
'Dim codAsistencia As Long
'Dim strCUN As String
'Dim strMedicamento As String
'Dim strPiDboid, strMed, strUnit, strRuta, strForma As String
'Dim rdCUN As rdoResultset
'Dim rdMedicamento As rdoResultset
'Dim rdPiDboid As rdoResultset
'Dim rdMed As rdoResultset
'Dim rdUnit As rdoResultset
'Dim rdRuta As rdoResultset
'Dim rdForma As rdoResultset
'Dim qyCUN As rdoQuery
'Dim qyMedicamento As rdoQuery
'Dim qyPiDboid As rdoQuery
'Dim qyMed As rdoQuery
'Dim qyUnit As rdoQuery
'Dim qyRuta As rdoQuery
'Dim qyForma As rdoQuery
'Dim strPicis As String
'Dim qyPicis As rdoQuery
'Dim FechaInicio As String
'Dim FechaFin As String
'Dim FechaTask As String
'Dim rdTask As rdoResultset
'Dim qytask As rdoQuery
'Dim strTask, Unidades As String
'Dim i As Integer
'Dim strDia As String
'Dim blnPrimeraAdministracion As Boolean
'Dim strInicio As String
'Dim qyInicio As rdoQuery
'Dim ExtraInfo As Double
'Dim Staff As String
''On Error Resume Next
'
'Err = 0
'blnError = False
'Call BuscarAsistenciaDesdePaciente(numHistoria, codAsistencia)
'strCUN = "SELECT FR73CODPRODUCTO,FR28DOSIS,FR34CODVIA,FRG4CODFRECUENCIA, " & _
'"FR93CODUNIMEDIDA,FR32INDISPEN,FR32INDCOMIENINMED,FRH5CODPERIODICIDAD , " & _
'"FRH7CODFORMFAR, FR32OPERACION, FR32CANTIDAD, FR32FECINICIO, FR32HORAINICIO, " & _
'"FR32FECFIN, FR32HORAFIN FROM FR3200 WHERE " & _
'"FR66CODPETICION= ? AND FR28NUMLINEA= ? "
'Set qyCUN = objApp.rdoConnect.CreateQuery("", strCUN)
'    qyCUN(0) = codPeticion
'    qyCUN(1) = numlinea
'Set rdCUN = qyCUN.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'
''Obtener OrderDBOID
'ierrcode = PcsOrderAlloc(hOrder)
'ierrcode = PcsDbObjectGenerateIdentifier(hOrder)
'ierrcode = PcsDbObjectGetIdentifier(hOrder, OrderDboid, 21)
'ierrcode = PcsOrderFree(hOrder)
'
''Obtener Nombre de medicamento
'strMedicamento = "SELECT FR73DESPRODUCTO,TREATMENTDBOID FROM FR7300 WHERE FR73CODPRODUCTO = ? "
'Set qyMedicamento = objApp.rdoConnect.CreateQuery("", strMedicamento)
'qyMedicamento(0) = rdCUN!fr73codproducto
'Set rdMedicamento = qyMedicamento.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'
''OBTENER LAS FECHAS DE INICIO Y FIN
'FechaInicio = Format(Format(rdCUN!FR32FECINICIO, "dd/mm/yyyy") & " " & Format(rdCUN!FR32HORAINICIO & ":00:00", "HH:MM:SS"), "DD/MM/YYYY HH:MM:SS")
'If Not IsNull(rdCUN!FR32FECFIN) Then
'    FechaFin = Format$(rdCUN!FR32FECFIN & " " & Format(rdCUN!FR32HORAFIN & ":00:00", "HH:MM:SS"), "YYYY-MM-DD HH")
'Else
'    FechaFin = Format$(DateAdd("d", 1, CDate(FechaInicio)), "DD/MM/YYYY HH:MM:SS")
'    'Fecha provisional que a�ade dos dias a la fecha fin sino
'    'existe.
'End If
'
''Obtener el PicisdataDboid
'strPiDboid = "SELECT PICISDATA.PICISDATADBOID From ADMISSIONS, PICISDATA " & _
'"Where ADMISSIONS.ADMISSIONDBOID = PICISDATA.ADMISSIONDBOID " & _
'"AND ADMISSIONS.ADMID1= ?"
'Set qyPiDboid = objApp.rdoConnect.CreateQuery("", strPiDboid)
'qyPiDboid(0) = codAsistencia
'Set rdPiDboid = qyPiDboid.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'
''OBTENER EL MEDICO QUE LO HA RECETADO
'strMed = "SELECT STAFF.STAFFDBOID FROM FR3200, FR6600, STAFF " & _
'"Where FR3200.FR66CODPETICION = FR6600.FR66CODPETICION And (FR3200.FR66CODPETICION = ?) " & _
'"AND FR6600.SG02COD_MED=STAFF.USERNAME"
'Set qyMed = objApp.rdoConnect.CreateQuery("", strMed)
'qyMed(0) = codPeticion
'Set rdMed = qyMed.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'    Staff = rdMed!staffdboid
'rdMed.Close
'qyMed.Close
'
''Obtener el UnitDboid
'strUnit = "SELECT UNITDBOID FROM FR9300 WHERE FR93CODUNIMEDIDA=?"
'Set qyUnit = objApp.rdoConnect.CreateQuery("", strUnit)
'qyUnit(0) = rdCUN!FR93CODUNIMEDIDA
'Set rdUnit = qyUnit.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'Unidades = rdUnit!UNITDBOID
'rdUnit.Close
'qyUnit.Close
'
''Obtener el RouteDboid
'If Not IsNull(rdCUN!FR34CODVIA) Then
'    strRuta = "SELECT ROUTEDBOID FROM  FR3400 WHERE FR34CODVIA=?"
'    Set qyRuta = objApp.rdoConnect.CreateQuery("", strRuta)
'    qyRuta(0) = rdCUN!FR34CODVIA
'    Set rdRuta = qyRuta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'End If
'
''Obtener el FormDboid
'If Not IsNull(rdCUN!FRH7CODFORMFAR) Then
'    strForma = "SELECT FORMDBOID FROM  FRH700 WHERE FRH7CODFORMFAR=?"
'    Set qyForma = objApp.rdoConnect.CreateQuery("", strForma)
'    qyForma(0) = rdCUN!FRH7CODFORMFAR
'    Set rdForma = qyForma.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'End If
''*******************************************
''Modificar Calcularextrainfo si se modifica alg�na frecuencia
'ExtraInfo = CalcularExtraInfo(rdCUN!FRG4CODFRECUENCIA)
''*******************************************
'objApp.BeginTrans
'
'strPicis = "INSERT INTO ORDERS (ORDERDBOID,ORDERDESC,CREATIONDATE, CONDITION, " & _
'"INITDATE, LASTDATE, HASAMEMO, HIGHERDOSE, " & _
'"LOWERDOSE, ISFROMRESCHEDULE, PICISDATADBOID, STAFFDBOID, ORDERTYPEDBOID, " & _
'"ORDERSTATUSDBOID, TREATMENTDBOID, ORDERSETDBOID, GROUPDBOID, UNITDBOID, " & _
'"ROUTEDBOID, FORMDBOID, TASKGROUPDBOID, STDORDERDBOID, EXTRAINFO) " & _
'"VALUES (?, ?, SYSDATE, ?, " & _
'"TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), " & _
'"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
'
'Set qyPicis = objApp.rdoConnect.CreateQuery("", strPicis)
'qyPicis(0) = OrderDboid
'qyPicis(1) = CStr(rdMedicamento!FR73DESPRODUCTO) 'OrderDesc
'qyPicis(2) = Null 'Condicion de la orden m�dica
'qyPicis(3) = FechaInicio 'Initdate
'qyPicis(4) = FechaFin 'lastdate
'qyPicis(5) = "F" 'hasaMemo
'qyPicis(6) = CStr(rdCUN!FR28DOSIS) 'Higherdose
'qyPicis(7) = CStr(rdCUN!FR28DOSIS) 'lowerdose
'qyPicis(8) = "F" 'IsfromReschedule
'qyPicis(9) = rdPiDboid!PICISDATADBOID 'PicisdataDboid
'qyPicis(10) = Staff 'Usuario que ha creado la orden m�dica
'qyPicis(11) = "169000000000002000000" 'orden PRN
'qyPicis(12) = "170000000000000000000" 'TODO siempre asi cuando se crean
'qyPicis(13) = rdMedicamento!TREATMENTDBOID 'TratmentDboid
'qyPicis(14) = Null 'orderSetdboid
'qyPicis(15) = "21117459741110005511" 'Masin:Revisar cuando este creada la nueva base de
'                                    'datos. Grupo= Sin Usuarios
'If Not IsNull(rdCUN!FR93CODUNIMEDIDA) Then
'    qyPicis(16) = Unidades 'Unitdboid
'Else
'    qyPicis(16) = "45000000000000000000" 'Unidad de medida no especificada
'End If
'
'If Not IsNull(rdCUN!FR34CODVIA) Then
'    qyPicis(17) = rdRuta!ROUTEDBOID 'routedboid
'    rdRuta.Close
'    qyRuta.Close
'Else
'    qyPicis(17) = "155000000000000000000" 'RoutDboid Desconocida
'End If
'If Not IsNull(rdCUN!FRH7CODFORMFAR) Then
'    qyPicis(18) = rdForma!FORMDBOID
'    rdForma.Close
'    qyForma.Close
'Else
'    qyPicis(18) = "151000000000000000000" 'Desconocido FormDboid
'End If
'qyPicis(19) = "21000000000210000511" 'Grupo validaci�n enfermeras.
'                                    'MASIN:Revisar para la pr�xima base de datos
'qyPicis(20) = Null 'StdOrderDboid
'qyPicis(21) = ExtraInfo 'ExtraInfo
'qyPicis.Execute
''La funci�n de memos
'Call EscribirMemo(codPeticion, numlinea, _
'OrderDboid, Staff)
'    If Err > 0 Then
'        objApp.RollbackTrans
'        MsgBox "ERROR: La orden m�dica PRN no ha pasado a Picis"
'        blnError = True
'        Exit Function
'    Else: objApp.CommitTrans
'    End If
'
'rdMedicamento.Close
'rdPiDboid.Close
'qyMedicamento.Close
'qyPiDboid.Close
'qyPicis.Close
'End Function

Public Function CalcularExtraInfo(CODFRECUENCIA As String, CodPeticion&) As Double
Dim Minutos As Double
Dim strHora$, i%, Num%, DifOriginal%, Dif%
Dim rdHora As rdoResultset
Dim qyHora As rdoQuery
    Select Case CODFRECUENCIA
    'Hay que modificar esta funcion si se a�ade alguna frecuencia m�s
    'en la base de datos de CUN
        Case "C/24", "De", "Co", "Me", "Ce"
            Minutos = 1440
        Case "C/12"
            Minutos = 720
        Case "C/8", "De/Me"
            Minutos = 480
        Case "C/6"
            Minutos = 360
        Case "C/4", "De/Co", "De/Co/Me", "De/Co/Ce", "Co/Me"
            Minutos = 240
        Case "C/2"
            Minutos = 160
        Case "Noche"
            Minutos = 1440
        Case "De/Co/Me/Ce", "Co/Me/Ce", "Me/Ce"
            Minutos = 180
        Case "Co/Ce"
            Minutos = 300
        Case "De/Ce"
            Minutos = 660
        Case "Manual"
            strHora = "SELECT FRP4HORAS FROM FRP400 WHERE FRP4CODPETICION=?"
            Set qyHora = objApp.rdoConnect.CreateQuery("", strHora)
                qyHora(0) = CodPeticion
            Set rdHora = qyHora.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            frmEntorno.Awk = rdHora!FRP4HORAS
            rdHora.Close
            qyHora.Close
            For i = 1 To frmEntorno.Awk.NF
                Select Case i
                    Case 1
                        DifOriginal = 24
                    Case Else
                        Dif = frmEntorno.Awk.F(i) - frmEntorno.Awk.F(i - 1)
                        If Dif < DifOriginal Then DifOriginal = Dif
                End Select
            Next
            Minutos = DifOriginal * 60
    End Select
CalcularExtraInfo = (Minutos * (2 ^ 16)) + 4
End Function
Public Function EscribirMemo(OrderDboid As String, Observaciones As String, _
staffdboid As String)
Dim strMemo
Dim qyMemo As rdoQuery
Dim rdMemo As rdoResultset
Dim strInsertMemo
Dim qyInsertMemo As rdoQuery
Dim memodboid As String * 21
Dim hMemo As Long

'Obtener el dboid del memo
ierrcode = PcsMemoAlloc(hMemo)
ierrcode = PcsDbObjectGenerateIdentifier(hMemo)
ierrcode = PcsDbObjectGetIdentifier(hMemo, memodboid, 21)
ierrcode = PcsOrderFree(hMemo)
strInsertMemo = "INSERT INTO MEMOS (MEMODBOID,MEMODESC,STARTED, DONEDATE, " & _
"STAFFDBOID, DBOIDREF) " & _
"VALUES (?, ?, SYSDATE,SYSDATE,?,?)"
Set qyInsertMemo = objApp.rdoConnect.CreateQuery("", strInsertMemo)
    qyInsertMemo(0) = memodboid
    qyInsertMemo(1) = Observaciones
    qyInsertMemo(2) = staffdboid
    qyInsertMemo(3) = OrderDboid
qyInsertMemo.Execute
qyInsertMemo.Close
End Function

Public Function ObtenerMedicamento(Operacion As String, via As String, _
codProducto1 As Long, IndPerfusion As Boolean, rdmedicamento As rdoResultset, _
CodProducto2 As Long, codProducto3 As Long, rdAditivo As rdoResultset, _
rdAditivo2 As rdoResultset, blnPrnfluido As Boolean) As Integer
Dim strMedicamento As String
Dim strGrupo$
Dim rdGrupo As rdoResultset
Dim qyGrupo As rdoQuery
Dim strAditivo As String
Dim qyAditivo As rdoQuery
Dim strAditivo2 As String
Dim qyAditivo2 As rdoQuery
Dim intTipoOm As Integer    'intTipoOM =1 Medicamento como medicamento
                            'intTipoOM =2 Medicamento como perfusion
                            'intTipoOM =3 PRN como medicamento
                            'intTipoOM =4 PRN como perfusion
                            'intTipoOM =5 Estupefaciente como medicamento
                            'intTipoOM =6 Estupefaciente como perfusion
                            'intTipoOM =7 Fluido como fluidoterapia
                            'inttipoOM= 8 Mezcla

Select Case Operacion
    Case "/" 'Prescrito desde medicamento
    strGrupo = "SELECT FR7300.FR73CODPRODUCTO " & _
    "FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '4000' AND " & _
    "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO and " & _
    "FR7300.FR73CODPRODUCTO= ? " 'grupo perfusiones sin suero
    Set qyGrupo = objApp.rdoConnect.CreateQuery("", strGrupo)
        qyGrupo(0) = codProducto1
    Set rdGrupo = qyGrupo.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If CodProducto2 = 0 And rdGrupo.RowCount = 0 Then 'pasa el medicamento como una linea de
                                       'medicacion
            strMedicamento = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
            "From CATEGORIES, FAMILIES, FR7300, FRP200, TREATMENTS " & _
            "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
            "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
            "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
            "AND FAMILIES.CATEGORYDBOID = CATEGORIES.CATEGORYDBOID " & _
            "AND FRP200.FR73CODPRODUCTO = ? " & _
            "AND CATEGORIES.CATEGORYDBOID = ?"
            Set qymedicamento = objApp.rdoConnect.CreateQuery("", strMedicamento)
                qymedicamento(0) = codProducto1
                qymedicamento(1) = "157000000000001000000" 'Categoria de medicamentos
            Set rdmedicamento = qymedicamento.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            intTipoOm = 1
    Else
            strMedicamento = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
            "From FAMILIES, FAMILYBEHAVIOR, FR7300, FRP200, TREATMENTS " & _
            "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
            "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
            "AND FAMILIES.FAMILYBEHAVDBOID = FAMILYBEHAVIOR.FAMILYBEHAVDBOID " & _
            "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
            "AND FRP200.FR73CODPRODUCTO=? AND " & _
            "FAMILYBEHAVIOR.FAMILYBEHAVDBOID = ?"
            Set qymedicamento = objApp.rdoConnect.CreateQuery("", strMedicamento)
            qymedicamento(0) = codProducto1
            qymedicamento(1) = "184000000000002000000" 'behaviour de drips
            Set rdmedicamento = qymedicamento.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            intTipoOm = 2
    End If
    rdGrupo.Close
    qyGrupo.Close
    Case "M" 'Prescrito como mezcla MASIN: REVISAR CUANDO SE DECIDA
                        strMedicamento = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
            "From FAMILIES, FAMILYBEHAVIOR, FR7300, FRP200, TREATMENTS " & _
            "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
            "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
            "AND FAMILIES.FAMILYBEHAVDBOID = FAMILYBEHAVIOR.FAMILYBEHAVDBOID " & _
            "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
            "AND FRP200.FR73CODPRODUCTO=? AND " & _
            "FAMILYBEHAVIOR.FAMILYBEHAVDBOID = ?"
            Set qymedicamento = objApp.rdoConnect.CreateQuery("", strMedicamento)
            qymedicamento(0) = codProducto1
            qymedicamento(1) = "184000000000003000000" 'behaviour de Fluidos
            Set rdmedicamento = qymedicamento.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            intTipoOm = 8
            If CodProducto2 <> 0 Then 'existe un aditivo
                    strAditivo = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
                    "From CATEGORIES, FAMILIES, FR7300, FRP200, TREATMENTS " & _
                    "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
                    "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
                    "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
                    "AND FAMILIES.CATEGORYDBOID = CATEGORIES.CATEGORYDBOID " & _
                    "AND FRP200.FR73CODPRODUCTO = ? " & _
                    "AND CATEGORIES.CATEGORYDBOID = ?"
                    Set qyAditivo = objApp.rdoConnect.CreateQuery("", strAditivo)
                        qyAditivo(0) = CodProducto2
                        qyAditivo(1) = "157000000000100000000" 'Categoria de aditivos
                    Set rdAditivo = qyAditivo.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            End If
            If codProducto3 <> 0 Then 'existe el segundo aditivo
                    strAditivo2 = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
                    "From CATEGORIES, FAMILIES, FR7300, FRP200, TREATMENTS " & _
                    "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
                    "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
                    "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
                    "AND FAMILIES.CATEGORYDBOID = CATEGORIES.CATEGORYDBOID " & _
                    "AND FRP200.FR73CODPRODUCTO = ? " & _
                    "AND CATEGORIES.CATEGORYDBOID = ?"
                    Set qyAditivo2 = objApp.rdoConnect.CreateQuery("", strAditivo2)
                        qyAditivo2(0) = codProducto3
                        qyAditivo2(1) = "157000000000100000000" 'Categoria de aditivos
                    Set rdAditivo2 = qyAditivo2.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            End If
    Case "P"
    If CodProducto2 = 0 And Not blnPrnfluido Then 'pasa el medicamento como una linea de
                                       'medicacion
            strMedicamento = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
            "From CATEGORIES, FAMILIES, FR7300, FRP200, TREATMENTS " & _
            "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
            "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
            "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
            "AND FAMILIES.CATEGORYDBOID = CATEGORIES.CATEGORYDBOID " & _
            "AND FRP200.FR73CODPRODUCTO = ? " & _
            "AND CATEGORIES.CATEGORYDBOID = ?"
            Set qymedicamento = objApp.rdoConnect.CreateQuery("", strMedicamento)
                qymedicamento(0) = codProducto1
                qymedicamento(1) = "157000000000001000000" 'Categoria de medicamentos
            Set rdmedicamento = qymedicamento.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            intTipoOm = 3
    ElseIf blnPrnfluido Then
              strMedicamento = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
            "From FAMILIES, FAMILYBEHAVIOR, FR7300, FRP200, TREATMENTS " & _
            "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
            "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
            "AND FAMILIES.FAMILYBEHAVDBOID = FAMILYBEHAVIOR.FAMILYBEHAVDBOID " & _
            "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
            "AND FRP200.FR73CODPRODUCTO=? AND " & _
            "FAMILYBEHAVIOR.FAMILYBEHAVDBOID = ?"
            Set qymedicamento = objApp.rdoConnect.CreateQuery("", strMedicamento)
            qymedicamento(0) = codProducto1
            qymedicamento(1) = "184000000000003000000" 'behaviour de Fluidos
            Set rdmedicamento = qymedicamento.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            If CodProducto2 <> 0 Then 'existe un aditivo
                    strAditivo = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
                    "From CATEGORIES, FAMILIES, FR7300, FRP200, TREATMENTS " & _
                    "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
                    "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
                    "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
                    "AND FAMILIES.CATEGORYDBOID = CATEGORIES.CATEGORYDBOID " & _
                    "AND FRP200.FR73CODPRODUCTO = ? " & _
                    "AND CATEGORIES.CATEGORYDBOID = ?"
                    Set qyAditivo = objApp.rdoConnect.CreateQuery("", strAditivo)
                        qyAditivo(0) = CodProducto2
                        qyAditivo(1) = "157000000000100000000" 'Categoria de aditivos
                    Set rdAditivo = qyAditivo.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            End If
            If codProducto3 <> 0 Then 'existe el segundo aditivo
                    strAditivo2 = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
                    "From CATEGORIES, FAMILIES, FR7300, FRP200, TREATMENTS " & _
                    "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
                    "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
                    "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
                    "AND FAMILIES.CATEGORYDBOID = CATEGORIES.CATEGORYDBOID " & _
                    "AND FRP200.FR73CODPRODUCTO = ? " & _
                    "AND CATEGORIES.CATEGORYDBOID = ?"
                    Set qyAditivo2 = objApp.rdoConnect.CreateQuery("", strAditivo2)
                        qyAditivo2(0) = codProducto3
                        qyAditivo2(1) = "157000000000100000000" 'Categoria de aditivos
                    Set rdAditivo2 = qyAditivo2.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            End If
            intTipoOm = 3

    Else
            strMedicamento = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
            "From FAMILIES, FAMILYBEHAVIOR, FR7300, FRP200, TREATMENTS " & _
            "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
            "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
            "AND FAMILIES.FAMILYBEHAVDBOID = FAMILYBEHAVIOR.FAMILYBEHAVDBOID " & _
            "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
            "AND FRP200.FR73CODPRODUCTO=? AND " & _
            "FAMILYBEHAVIOR.FAMILYBEHAVDBOID = ?"
            Set qymedicamento = objApp.rdoConnect.CreateQuery("", strMedicamento)
            qymedicamento(0) = codProducto1
            qymedicamento(1) = "184000000000002000000" 'behaviour de drips
            Set rdmedicamento = qymedicamento.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            intTipoOm = 4
    End If
    Case "F" 'prescrito como fluido
            strMedicamento = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
            "From FAMILIES, FAMILYBEHAVIOR, FR7300, FRP200, TREATMENTS " & _
            "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
            "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
            "AND FAMILIES.FAMILYBEHAVDBOID = FAMILYBEHAVIOR.FAMILYBEHAVDBOID " & _
            "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
            "AND FRP200.FR73CODPRODUCTO=? AND " & _
            "FAMILYBEHAVIOR.FAMILYBEHAVDBOID = ?"
            Set qymedicamento = objApp.rdoConnect.CreateQuery("", strMedicamento)
            qymedicamento(0) = codProducto1
            qymedicamento(1) = "184000000000003000000" 'behaviour de Fluidos
            Set rdmedicamento = qymedicamento.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            intTipoOm = 7
            If CodProducto2 <> 0 Then 'existe un aditivo
                    strAditivo = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
                    "From CATEGORIES, FAMILIES, FR7300, FRP200, TREATMENTS " & _
                    "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
                    "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
                    "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
                    "AND FAMILIES.CATEGORYDBOID = CATEGORIES.CATEGORYDBOID " & _
                    "AND FRP200.FR73CODPRODUCTO = ? " & _
                    "AND CATEGORIES.CATEGORYDBOID = ?"
                    Set qyAditivo = objApp.rdoConnect.CreateQuery("", strAditivo)
                        qyAditivo(0) = CodProducto2
                        qyAditivo(1) = "157000000000100000000" 'Categoria de aditivos
                    Set rdAditivo = qyAditivo.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            End If
            If codProducto3 <> 0 Then 'existe el segundo aditivo
                    strAditivo2 = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
                    "From CATEGORIES, FAMILIES, FR7300, FRP200, TREATMENTS " & _
                    "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
                    "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
                    "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
                    "AND FAMILIES.CATEGORYDBOID = CATEGORIES.CATEGORYDBOID " & _
                    "AND FRP200.FR73CODPRODUCTO = ? " & _
                    "AND CATEGORIES.CATEGORYDBOID = ?"
                    Set qyAditivo2 = objApp.rdoConnect.CreateQuery("", strAditivo2)
                        qyAditivo2(0) = codProducto3
                        qyAditivo2(1) = "157000000000100000000" 'Categoria de aditivos
                    Set rdAditivo2 = qyAditivo2.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            End If

    Case "E" 'Prescrito como estupefaciente
    If CodProducto2 = 0 Then 'pasa el medicamento como una linea de
                                       'medicacion
            strMedicamento = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
            "From CATEGORIES, FAMILIES, FR7300, FRP200, TREATMENTS " & _
            "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
            "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
            "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
            "AND FAMILIES.CATEGORYDBOID = CATEGORIES.CATEGORYDBOID " & _
            "AND FRP200.FR73CODPRODUCTO = ? " & _
            "AND CATEGORIES.CATEGORYDBOID = ?"
            Set qymedicamento = objApp.rdoConnect.CreateQuery("", strMedicamento)
                qymedicamento(0) = codProducto1
                qymedicamento(1) = "157000000000001000000" 'Categoria de medicamentos
            Set rdmedicamento = qymedicamento.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            intTipoOm = 5
    Else
            strMedicamento = "SELECT FR7300.FR73DESPRODUCTO, FRP200.TREATMENTDBOID " & _
            "From FAMILIES, FAMILYBEHAVIOR, FR7300, FRP200, TREATMENTS " & _
            "Where FRP200.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID " & _
            "AND TREATMENTS.FAMILYDBOID = FAMILIES.FAMILYDBOID " & _
            "AND FAMILIES.FAMILYBEHAVDBOID = FAMILYBEHAVIOR.FAMILYBEHAVDBOID " & _
            "AND FR7300.FR73CODPRODUCTO = FRP200.FR73CODPRODUCTO " & _
            "AND FRP200.FR73CODPRODUCTO=? AND " & _
            "FAMILYBEHAVIOR.FAMILYBEHAVDBOID = ?"
            Set qymedicamento = objApp.rdoConnect.CreateQuery("", strMedicamento)
            qymedicamento(0) = codProducto1
            qymedicamento(1) = "184000000000002000000" 'behaviour de drips
            Set rdmedicamento = qymedicamento.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
            intTipoOm = 6
    End If
    Case "L" 'Formula magistral
        
End Select
ObtenerMedicamento = intTipoOm
End Function

Public Function Periodicidad(rdcun As rdoResultset, FechaInicio As String, _
OrderDboid As String, FechaFin As String, _
FechaAdministracion As String, intTipoOm As Integer, _
FechaUltimaAdministracion As String, blnfr28 As Boolean, blnfrp4 As Boolean)

Dim blnPrimeraAdministracion As Boolean
Dim FechaTask As String
Dim FechaUltima As Date
Dim strDia As String

    blnPrimeraAdministracion = True
    FechaTask = FechaInicio
If Not IsNull(rdcun!FRG4CODFRECUENCIA) Then
    Select Case rdcun!FRH5CODPERIODICIDAD
    Case "Alternos"
        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
            'funcion
             Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun, _
                OrderDboid, FechaInicio, FechaFin, FechaAdministracion, _
                blnPrimeraAdministracion, intTipoOm, _
                FechaUltimaAdministracion, blnfr28, blnfrp4)
            FechaTask = DateAdd("d", 2, CDate(FechaTask))
        Loop
    Case "Diario"
        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
            'funcion
             Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun, _
                OrderDboid, FechaInicio, FechaFin, FechaAdministracion, _
                blnPrimeraAdministracion, intTipoOm, _
                FechaUltimaAdministracion, blnfr28, blnfrp4)
            FechaTask = DateAdd("d", 1, CDate(FechaTask))
        Loop
    Case "Domingo No"
        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
            strDia = Format(FechaTask, "dddd")
            If strDia <> "domingo" Then
                'Funcion
             Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun, _
                OrderDboid, FechaInicio, FechaFin, FechaAdministracion, _
                blnPrimeraAdministracion, intTipoOm, _
                FechaUltimaAdministracion, blnfr28, blnfrp4)
            End If
            FechaTask = DateAdd("d", 1, CDate(FechaTask))
        Loop
    Case "S�bado No"
        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
            strDia = Format(FechaTask, "dddd")
            If strDia <> "s�bado" Then
             'funcion
             Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun, _
                OrderDboid, FechaInicio, FechaFin, FechaAdministracion, _
                blnPrimeraAdministracion, intTipoOm, _
                FechaUltimaAdministracion, blnfr28, blnfrp4)
            End If
            FechaTask = DateAdd("d", 1, CDate(FechaTask))
        Loop
    Case "Lunes No"
        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
            strDia = Format(FechaTask, "dddd")
            If strDia <> "lunes" Then
             Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun, _
                OrderDboid, FechaInicio, FechaFin, FechaAdministracion, _
                blnPrimeraAdministracion, intTipoOm, _
                FechaUltimaAdministracion, blnfr28, blnfrp4)
            End If
            FechaTask = DateAdd("d", 1, CDate(FechaTask))
        Loop
    Case "Pares"
        If EsPar(FechaInicio) Then
            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
                'Funcion
             Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun, _
                OrderDboid, FechaInicio, FechaFin, FechaAdministracion, _
                blnPrimeraAdministracion, intTipoOm, _
                FechaUltimaAdministracion, blnfr28, blnfrp4)
                FechaTask = DateAdd("d", 2, CDate(FechaTask))
            Loop
        Else
            FechaTask = DateAdd("d", 1, FechaTask)
            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
             Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun, _
                OrderDboid, FechaInicio, FechaFin, FechaAdministracion, _
                blnPrimeraAdministracion, intTipoOm, _
                FechaUltimaAdministracion, blnfr28, blnfrp4)
                FechaTask = DateAdd("d", 2, CDate(FechaTask))
            Loop
        End If
    Case "Impares"
        If Not EsPar(FechaInicio) Then
            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
                'Funcion
             Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun, _
                OrderDboid, FechaInicio, FechaFin, FechaAdministracion, _
                blnPrimeraAdministracion, intTipoOm, _
                FechaUltimaAdministracion, blnfr28, blnfrp4)
                FechaTask = DateAdd("d", 2, CDate(FechaTask))
            Loop
        Else
            FechaTask = DateAdd("d", 1, FechaTask)
            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
                'Funcion
             Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun, _
                OrderDboid, FechaInicio, FechaFin, FechaAdministracion, _
                blnPrimeraAdministracion, intTipoOm, _
                FechaUltimaAdministracion, blnfr28, blnfrp4)
                FechaTask = DateAdd("d", 2, CDate(FechaTask))
            Loop
        End If
    End Select
Else 'Si no hay frecuencia tampoco periodicidad: es un ahora
       'Funcion
        Call Task(Format(FechaTask, "DD/MM/YYYY"), rdcun, _
           OrderDboid, FechaInicio, FechaFin, FechaAdministracion, _
           blnPrimeraAdministracion, intTipoOm, _
           FechaUltimaAdministracion, blnfr28, blnfrp4)
       FechaTask = DateAdd("d", 2, CDate(FechaTask))
End If
End Function

Public Function OrderData(rdcun As rdoResultset, OrderDboid As String, _
intTipoOm As Integer, rdAditivo As rdoResultset, blnfr28 As Boolean, blnfrp4 As Boolean)
'*******************************************************************
'En esta funcion se introducen los datos de las tabla Orderdata.
'Si es una orden tipo 2,4 o 6 se rellenan 4 indices.
'Si no son de este tipo no entran a esta funcion.
'********************************************************************
Dim hOrderData As Long
Dim OrderDataDboid As String * 21
Dim i As Integer
Dim StrOrderData As String
Dim qyOrderData As rdoQuery
Dim UnidadesDil As String
Dim CantidadDil As String
Dim Dosis As String
If blnfr28 Then
    If Not IsNull(rdcun!FR28CANTIDADDIL) Then CantidadDil = rdcun!FR28CANTIDADDIL Else CantidadDil = 0
    If Not IsNull(rdcun!FR28DOSIS) Then Dosis = rdcun!FR28DOSIS Else Dosis = 0
ElseIf blnfrp4 Then
    If Not IsNull(rdcun!FRP4CANTIDADDIL) Then CantidadDil = rdcun!FRP4CANTIDADDIL Else CantidadDil = 0
    If Not IsNull(rdcun!FRP4DOSIS) Then Dosis = rdcun!FRP4DOSIS Else Dosis = 0
End If


'Obtener OrderDBOID
If intTipoOm = 2 Or intTipoOm = 4 Or intTipoOm = 6 Then
    For i = 1 To 4
        ierrcode = PcsOrderDataAlloc(hOrderData)
        ierrcode = PcsDbObjectGenerateIdentifier(hOrderData)
        ierrcode = PcsDbObjectGetIdentifier(hOrderData, OrderDataDboid, 21)
        ierrcode = PcsOrderDataFree(hOrderData)
           
        StrOrderData = "INSERT INTO ORDERDATA (ORDERDATADBOID, ORDERDBOID, " & _
        "ORDERDATAINDEX, ADDITIONALINFO) VALUES (?,?,?,?)"
        Set qyOrderData = objApp.rdoConnect.CreateQuery("", StrOrderData)
        qyOrderData(0) = OrderDataDboid
        qyOrderData(1) = OrderDboid
        qyOrderData(2) = i
            Select Case i
            Case 1
                qyOrderData(3) = CDbl(Dosis)
            Case 2
                qyOrderData(3) = ObtenerUnitDboid(rdcun!FR93CODUNIMEDIDA)
            Case 3
                qyOrderData(3) = CDbl(CantidadDil)
            Case 4
                qyOrderData(3) = "45000000000008000000" ' dboid de mililitros
            End Select
        qyOrderData.Execute
    Next i
Else
        ierrcode = PcsOrderDataAlloc(hOrderData)
        ierrcode = PcsDbObjectGenerateIdentifier(hOrderData)
        ierrcode = PcsDbObjectGetIdentifier(hOrderData, OrderDataDboid, 21)
        ierrcode = PcsOrderDataFree(hOrderData)
           
        StrOrderData = "INSERT INTO ORDERDATA (ORDERDATADBOID, ORDERDBOID, " & _
        "ORDERDATAINDEX, ADDITIONALINFO) VALUES (?,?,?,?)"
        Set qyOrderData = objApp.rdoConnect.CreateQuery("", StrOrderData)
        qyOrderData(0) = OrderDataDboid
        qyOrderData(1) = OrderDboid
        qyOrderData(2) = 0
        qyOrderData(3) = 1 'numero de aditivo (siempre uno)
        qyOrderData.Execute
        Call OrderAdditive(rdAditivo, rdcun, OrderDboid, blnfr28, blnfrp4)
End If
End Function
Public Function OrderAdditive(rdAditivo As rdoResultset, rdcun As rdoResultset, _
OrderDboid As String, blnfr28 As Boolean, blnfrp4 As Boolean)
'**********************************************************
'Esta funcion rellena la tabla OrderAddtive cuando la fluidoterapia
'lleva incluido un aditivo
'**************************************************************
Dim hOrderAddtive As Long
Dim OrderAdditiveDboid As String * 21
Dim strAditivo As String
Dim qyAditivo As rdoQuery
Dim Dosis As Long

If blnfr28 Then
    If Not IsNull(rdcun!FR28DOSIS) Then Dosis = rdcun!FR28DOSIS Else Dosis = 0
ElseIf blnfrp4 Then
    If Not IsNull(rdcun!FRP4DOSIS) Then Dosis = rdcun!FRP4DOSIS Else Dosis = 0
End If

ierrcode = PcsROrderAdditiveAlloc(hOrderAddtive)
ierrcode = PcsDbObjectGenerateIdentifier(hOrderAddtive)
ierrcode = PcsDbObjectGetIdentifier(hOrderAddtive, OrderAdditiveDboid, 21)
ierrcode = PcsROrderAdditiveFree(hOrderAddtive)

strAditivo = "INSERT INTO ORDERADDITIVE (DBOID, HIGHERDOSE, LOWERDOSE, CONCENTRATION, " & _
"ORDERDBOID, ADDITIVEDBOID, UNITDBOID, CONCENTRATIONUNITDBOID) VALUES " & _
"(?,?,?,?,?,?,?,?)"
Set qyAditivo = objApp.rdoConnect.CreateQuery("", strAditivo)
    qyAditivo(0) = OrderAdditiveDboid
    qyAditivo(1) = CStr(Dosis)
    qyAditivo(2) = CStr(Dosis)
    qyAditivo(3) = Null
    qyAditivo(4) = OrderDboid
    qyAditivo(5) = rdAditivo!TREATMENTDBOID
    qyAditivo(6) = ObtenerUnitDboid(rdcun!FR93CODUNIMEDIDA)
    qyAditivo(7) = Null
qyAditivo.Execute
End Function
Public Function CalcularDosis(rdcun As rdoResultset, intTipoOm _
As Integer, UnidadDosis As String, via As String, blnfr28 As Boolean, _
blnfrp4 As Boolean) As String

Dim str As String
Dim rd As rdoResultset
Dim Qy As rdoQuery
Dim CodPeticion As Long
Dim VelPerfusion As Long
Dim Dosis$
Dim CantidadDil As Long
If blnfr28 Then
    If Not IsNull(rdcun!FR28VELPERFUSION) Then VelPerfusion = rdcun!FR28VELPERFUSION Else VelPerfusion = 0
    If Not IsNull(rdcun!FR28DOSIS) Then Dosis = rdcun!FR28DOSIS Else Dosis = "0"
    If Not IsNull(rdcun!FR66CODPETICION) Then CodPeticion = rdcun!FR66CODPETICION Else CodPeticion = 0
    If Not IsNull(rdcun!FR28CANTIDADDIL) Then CantidadDil = rdcun!FR28CANTIDADDIL Else CantidadDil = 0
ElseIf blnfrp4 Then
    If Not IsNull(rdcun!FRP4VELPERFUSION) Then VelPerfusion = rdcun!FRP4VELPERFUSION Else VelPerfusion = 0
    If Not IsNull(rdcun!FRP4DOSIS) Then Dosis = rdcun!FRP4DOSIS Else Dosis = "0"
    If Not IsNull(rdcun!FRP4CODPETICION) Then CodPeticion = rdcun!FRP4CODPETICION Else CodPeticion = 0
    If Not IsNull(rdcun!FRP4CANTIDADDIL) Then CantidadDil = rdcun!FRP4CANTIDADDIL Else CantidadDil = 0
End If

str = "select ADMISSIONS.WEIGHT FROM ADMISSIONS,PICISDATA WHERE " & _
"ADMISSIONS.ADMISSIONDBOID =PICISDATA.ADMISSIONDBOID AND " & _
" PICISDATA.PICISDATADBOID= ?"
Set Qy = objApp.rdoConnect.CreateQuery("", str)
Qy(0) = ObtenerPicisData(CodPeticion, blnfr28, blnfrp4)
Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

If (intTipoOm = 2 Or intTipoOm = 4 Or intTipoOm = 6) And Not IsNull(rd!Weight) And _
via = "PC" Then
Dim resto As Double
    CalcularDosis = Format$((VelPerfusion * (CDbl(Dosis) / (CantidadDil))) / (rd!Weight), "###0.00")
'    CalcularDosis = CalcularDosis * 10000
'    resto = CStr(CDbl(CalcularDosis) - CInt(CalcularDosis))
'    If resto >= 0.5 Then CalcularDosis = (CInt(CalcularDosis) + 1) Else CalcularDosis = CInt(CalcularDosis)
'    CalcularDosis = CalcularDosis / 10000
    Select Case rdcun!FR93CODUNIMEDIDA
    Case "mg" 'se baja una unidad en el caso de los g. y los mg. para que no haya
              'problemas de redondeo en Picis
        UnidadDosis = "45000000000043000000" 'microgramo/kilo/hora
    Case "mcg"
        UnidadDosis = "45000000000043000000" 'microgramo/kilo/hora
    Case "g"
        UnidadDosis = "45000000000042000000" 'miligramo/kilo/hora
    Case "ui"
        UnidadDosis = "45000000000057000511" 'unidad internacional/kilo/hora
    End Select
Else
    CalcularDosis = Dosis
    UnidadDosis = ObtenerUnitDboid(rdcun!FR93CODUNIMEDIDA)
End If
End Function
Public Function Orders(rdcun As rdoResultset, FechaInicio As String, _
FechaFin As String, via As String, _
rdforma As rdoResultset, _
rdmedicamento As rdoResultset, _
OrderDboid As String, ExtraInfo As Double, intTipoOm As Integer, _
rdAditivo As rdoResultset, codProducto1 As Long, CodProducto2 As Long, _
codProducto3 As Long, rdAditivo2 As rdoResultset, _
blnfr28 As Boolean, blnfrp4 As Boolean, blnPrnfluido As Boolean)
'***********************************************************++
'esta funcion introduce los datos en la tabla Orders.
'Una vez que se han introducido, si es una prescripcio
'tipo perfusion pasa a la funcion OrderData que introduce`
'los datos de la perfusion. Solo pasan a OrderData los medicamentos
'tipo 2,4 y 6. Los demas no utilizan esta tabla.
'*************************************************************
Dim qyPicis As rdoQuery
Dim strPicis As String
Dim UnidadDosis As String
Dim VelPerfusion As String
Dim Observaciones As String
Dim CantidadDil As Long
Dim Operacion As String
Dim Dosis$
Dim CodPeticion As Long
Dim str$
Dim rd As rdoResultset
Dim Qy As rdoQuery
If blnfr28 Then
    If Not IsNull(rdcun!FR28VELPERFUSION) Then VelPerfusion = rdcun!FR28VELPERFUSION Else VelPerfusion = 0
    If Not IsNull(rdcun!FR28CANTIDADDIL) Then CantidadDil = rdcun!FR28CANTIDADDIL Else CantidadDil = 0
    If Not IsNull(rdcun!FR28DOSIS) Then Dosis = rdcun!FR28DOSIS Else Dosis = "0"
    If Not IsNull(rdcun!FR28INSTRADMIN) Then Observaciones = rdcun!FR28INSTRADMIN Else Observaciones = ""
    If Not IsNull(rdcun!FR66CODPETICION) Then CodPeticion = rdcun!FR66CODPETICION Else CodPeticion = 0
    Operacion = rdcun!FR28OPERACION
ElseIf blnfrp4 Then
    If Not IsNull(rdcun!FRP4VELPERFUSION) Then VelPerfusion = rdcun!FRP4VELPERFUSION Else VelPerfusion = 0
    If Not IsNull(rdcun!FRP4CANTIDADDIL) Then CantidadDil = rdcun!FRP4CANTIDADDIL Else CantidadDil = 0
    If Not IsNull(rdcun!FRP4DOSIS) Then Dosis = rdcun!FRP4DOSIS Else Dosis = "0"
    If Not IsNull(rdcun!FRP4INSTRADMIN) Then Observaciones = rdcun!FRP4INSTRADMIN Else Observaciones = ""
    If Not IsNull(rdcun!FRP4CODPETICION) Then CodPeticion = rdcun!FRP4CODPETICION Else CodPeticion = 0
    Operacion = rdcun!FRP4OPERACION
End If

'Obtener OrderDBOID
ierrcode = PcsOrderAlloc(hOrder)
ierrcode = PcsDbObjectGenerateIdentifier(hOrder)
ierrcode = PcsDbObjectGetIdentifier(hOrder, OrderDboid, 21)
ierrcode = PcsOrderFree(hOrder)

strPicis = "INSERT INTO ORDERS (ORDERDBOID,ORDERDESC,CREATIONDATE, CONDITION, " & _
"INITDATE, LASTDATE, HASAMEMO, HIGHERDOSE, " & _
"LOWERDOSE, ISFROMRESCHEDULE, PICISDATADBOID, STAFFDBOID, ORDERTYPEDBOID, " & _
"ORDERSTATUSDBOID, TREATMENTDBOID, ORDERSETDBOID, GROUPDBOID, UNITDBOID, " & _
"ROUTEDBOID, FORMDBOID, TASKGROUPDBOID, STDORDERDBOID, EXTRAINFO) " & _
"VALUES (?, ?, SYSDATE, ?, " & _
"TO_DATE(? ,'DD/MM/YYYY HH24:MI:SS'), TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), " & _
"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
    
Set qyPicis = objApp.rdoConnect.CreateQuery("", strPicis)
qyPicis(0) = OrderDboid
qyPicis(1) = ObtenerDescripcionOrden(rdcun, codProducto1, CodProducto2, _
codProducto3, intTipoOm, blnfr28, blnfrp4, blnPrnfluido) 'OrderDesc
If (intTipoOm = 2 Or intTipoOm = 4 Or intTipoOm = 6) And via <> "PC" Then
    qyPicis(2) = VelPerfusion & " ml/h"  'Condicion de la orden m�dica
Else 'Cuando es una perfusion con via PC
    qyPicis(2) = Null 'Condicion de la orden m�dica
End If
qyPicis(3) = FechaInicio 'Initdate
qyPicis(4) = FechaFin 'lastdate
If Trim(Observaciones) = "" Then
  qyPicis(5) = "F" 'NO hasaMemo
Else
  qyPicis(5) = "T" 'hasaMemo
End If
If intTipoOm = 7 Or intTipoOm = 8 Then  'Si es fluidoterapia o mezcla, la dosis es el volumen.
  qyPicis(6) = CStr(CantidadDil)  'Higherdose
  qyPicis(7) = CStr(CantidadDil)  'lowerdose
ElseIf intTipoOm = 3 And blnPrnfluido Then
    qyPicis(6) = CStr(CantidadDil) 'Higherdose
    qyPicis(7) = 0 'lowerdose
ElseIf Dosis <> "0" Then
  qyPicis(6) = CalcularDosis(rdcun, intTipoOm, UnidadDosis, via, blnfr28, blnfrp4) 'Higherdose
  qyPicis(7) = CalcularDosis(rdcun, intTipoOm, UnidadDosis, via, blnfr28, blnfrp4) 'lowerdose
'ElseIf (IsNull(rdcun!FR28DOSIS) And intTipoOm <> 7) Or (IsNull(rdcun!FR28CANTIDADDIL) And intTipoOm = 7) Then
'    qyPicis(6) = Null 'Higherdose
'    qyPicis(7) = Null 'lowerdose
End If
qyPicis(8) = "F" 'IsfromReschedule
qyPicis(9) = ObtenerPicisData(CodPeticion, blnfr28, blnfrp4) 'PicisdataDboid
qyPicis(10) = ObtenerStaff(CodPeticion, blnfr28, blnfrp4) 'Usuario que ha creado la orden m�dica
If Operacion = "P" Then 'eS UNA ORDEN PRN
  qyPicis(11) = "169000000000002000000" 'orden PRN
ElseIf rdcun!FR34CODVIA = "PC" Then 'Si es una via perfusion continua
  qyPicis(11) = "169000000000001000000" 'orden continua
Else 'eS PERIODICO
  qyPicis(11) = "169000000000000000000" 'orden peri�dica
End If
qyPicis(12) = "170000000000000000000" 'TODO siempre asi cuando se crean
qyPicis(13) = rdmedicamento!TREATMENTDBOID 'TreatmentDboid
qyPicis(14) = Null 'orderSetdboid
qyPicis(15) = "21000000000018000511" 'prescripcion enfermeras
If intTipoOm = 7 Or intTipoOm = 8 Or (intTipoOm = 3 And blnPrnfluido) Then 'si es fluidoterapia
    qyPicis(16) = "45000000000008000000" 'Dboid de mililitros. Siempre es ml cuando es                                           'fluidoterapia
ElseIf via <> "PC" Then
    qyPicis(16) = ObtenerUnitDboid(rdcun!FR93CODUNIMEDIDA)
Else
    qyPicis(16) = UnidadDosis 'Unitdboid
End If
qyPicis(17) = ObtenerRouteDboid(via) 'routedboid
If Not IsNull(rdcun!FRH7CODFORMFAR) Then
    qyPicis(18) = rdforma!FORMDBOID
    'rdForma.Close
Else
    qyPicis(18) = "151000000000000000000" 'Desconocido FormDboid
End If
qyPicis(19) = "21000000000031000511" 'Grupo validaci�n enfermeras.
                                     'MASIN:Revisar para la pr�xima base de datos
qyPicis(20) = Null 'StdOrderDboid
qyPicis(21) = ExtraInfo
qyPicis.Execute

If intTipoOm = 2 Or intTipoOm = 4 Or intTipoOm = 6 Or _
(intTipoOm = 7 And CodProducto2 <> 0 And codProducto3 = 0) Or (intTipoOm = 3 And blnPrnfluido And codProducto3 = 0 And CodProducto2 <> 0) Then
        Call OrderData(rdcun, OrderDboid, intTipoOm, rdAditivo, blnfr28, blnfrp4)
ElseIf ((intTipoOm = 8 Or intTipoOm = 7) And codProducto3 <> 0 And CodProducto2 <> 0) Or (intTipoOm = 3 And blnPrnfluido And codProducto3 <> 0 And CodProducto2 <> 0) Then
        Call OrderDataMezcla(rdcun, OrderDboid, intTipoOm, rdAditivo, rdAditivo2, blnfr28, blnfrp4)
End If
qyPicis.Close
End Function
Public Sub OrderDataMezcla(rdcun As rdoResultset, OrderDboid As String, _
intTipoOm As Integer, rdAditivo As rdoResultset, rdAditivo2 As rdoResultset, _
 blnfr28 As Boolean, blnfrp4 As Boolean)
Dim hOrderData As Long
Dim OrderDataDboid As String * 21
Dim i As Integer
Dim StrOrderData As String
Dim qyOrderData As rdoQuery
Dim UnidadesDil As String
'Obtener OrderDBOID
        ierrcode = PcsOrderDataAlloc(hOrderData)
        ierrcode = PcsDbObjectGenerateIdentifier(hOrderData)
        ierrcode = PcsDbObjectGetIdentifier(hOrderData, OrderDataDboid, 21)
        ierrcode = PcsOrderDataFree(hOrderData)
           
        StrOrderData = "INSERT INTO ORDERDATA (ORDERDATADBOID, ORDERDBOID, " & _
        "ORDERDATAINDEX, ADDITIONALINFO) VALUES (?,?,?,?)"
        Set qyOrderData = objApp.rdoConnect.CreateQuery("", StrOrderData)
        qyOrderData(0) = OrderDataDboid
        qyOrderData(1) = OrderDboid
        qyOrderData(2) = 0
        qyOrderData(3) = 2 'numero de aditivo
        qyOrderData.Execute
        Call OrderAdditiveMezcla(rdAditivo, rdAditivo2, rdcun, OrderDboid, _
        blnfr28, blnfrp4)
End Sub
Public Sub OrderAdditiveMezcla(rdAditivo As rdoResultset, rdAditivo2 As rdoResultset, _
rdcun As rdoResultset, OrderDboid As String, blnfr28 As Boolean, blnfrp4 As Boolean)

'**********************************************************
'Esta funcion rellena la tabla OrderAddtive cuando la fluidoterapia
'lleva incluido un aditivo
'**************************************************************

Dim hOrderAddtive As Long
Dim OrderAdditiveDboid As String * 21
Dim strAditivo As String
Dim qyAditivo As rdoQuery
Dim Dosis As Long
Dim Dosis_2 As Long

If blnfr28 Then
    If Not IsNull(rdcun!FR28DOSIS) Then Dosis = rdcun!FR28DOSIS Else Dosis = 0
    If Not IsNull(rdcun!FR28DOSIS_2) Then Dosis_2 = rdcun!FR28DOSIS_2 Else Dosis_2 = 0
ElseIf blnfrp4 Then
    If Not IsNull(rdcun!FRP4DOSIS) Then Dosis = rdcun!FRP4DOSIS Else Dosis = 0
    If Not IsNull(rdcun!FRP4DOSIS_2) Then Dosis_2 = rdcun!FRP4DOSIS_2 Else Dosis_2 = 0
End If

ierrcode = PcsROrderAdditiveAlloc(hOrderAddtive)
ierrcode = PcsDbObjectGenerateIdentifier(hOrderAddtive)
ierrcode = PcsDbObjectGetIdentifier(hOrderAddtive, OrderAdditiveDboid, 21)
ierrcode = PcsROrderAdditiveFree(hOrderAddtive)

strAditivo = "INSERT INTO ORDERADDITIVE (DBOID, HIGHERDOSE, LOWERDOSE, CONCENTRATION, " & _
"ORDERDBOID, ADDITIVEDBOID, UNITDBOID, CONCENTRATIONUNITDBOID) VALUES " & _
"(?,?,?,?,?,?,?,?)"
Set qyAditivo = objApp.rdoConnect.CreateQuery("", strAditivo)
    qyAditivo(0) = OrderAdditiveDboid
    qyAditivo(1) = CStr(Dosis)
    qyAditivo(2) = CStr(Dosis)
    qyAditivo(3) = Null
    qyAditivo(4) = OrderDboid
    qyAditivo(5) = rdAditivo!TREATMENTDBOID
    qyAditivo(6) = ObtenerUnitDboid(rdcun!FR93CODUNIMEDIDA)
    qyAditivo(7) = Null
qyAditivo.Execute
qyAditivo.Close

'Segundo aditivo
ierrcode = PcsROrderAdditiveAlloc(hOrderAddtive)
ierrcode = PcsDbObjectGenerateIdentifier(hOrderAddtive)
ierrcode = PcsDbObjectGetIdentifier(hOrderAddtive, OrderAdditiveDboid, 21)
ierrcode = PcsROrderAdditiveFree(hOrderAddtive)
strAditivo = "INSERT INTO ORDERADDITIVE (DBOID, HIGHERDOSE, LOWERDOSE, CONCENTRATION, " & _
"ORDERDBOID, ADDITIVEDBOID, UNITDBOID, CONCENTRATIONUNITDBOID) VALUES " & _
"(?,?,?,?,?,?,?,?)"
Set qyAditivo = objApp.rdoConnect.CreateQuery("", strAditivo)
    qyAditivo(0) = OrderAdditiveDboid
    qyAditivo(1) = CStr(Dosis_2)
    qyAditivo(2) = CStr(Dosis_2)
    qyAditivo(3) = Null
    qyAditivo(4) = OrderDboid
    qyAditivo(5) = rdAditivo2!TREATMENTDBOID
    qyAditivo(6) = ObtenerUnitDboid(rdcun!FR93CODUNIMEDIDA_2)
    qyAditivo(7) = Null
qyAditivo.Execute
qyAditivo.Close
End Sub
 Public Function ObtenerUnitDboid(codUnidad As String) As String
 Dim strUnit As String
 Dim qyUnit As rdoQuery
 Dim rdunit As rdoResultset
 
    strUnit = "SELECT UNITDBOID FROM FR9300 WHERE FR93CODUNIMEDIDA=?"
    Set qyUnit = objApp.rdoConnect.CreateQuery("", strUnit)
    qyUnit(0) = codUnidad
    Set rdunit = qyUnit.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If rdunit.RowCount <> 0 Then
        ObtenerUnitDboid = rdunit!UNITDBOID 'Unitdboid
    Else
        ObtenerUnitDboid = "45000000000000000000" 'Unidad de medida no especificada
    End If
    rdunit.Close
    qyUnit.Close
    
 End Function
 

Public Function TaskData(TaskDboid As String, rdcun As rdoResultset, _
intTipoOm As Integer, blnfr28 As Boolean, blnfrp4 As Boolean)
'*********************************************************
'Esta funcion escribe sobre la tabla TaskData
'Si es una perfusion escribe 7 indices.
'Si es una perfusion escribe 3 �ndices
'********************************************************
Dim hTaskData As Long
Dim TaskDataDboid As String * 21
Dim strTaskData As String
Dim qyTaskData As rdoQuery
Dim i As Integer
Dim str As String
Dim Qy As rdoQuery
Dim rd As rdoResultset
Dim CantidadDil As Long
Dim Dosis As Long
Dim CodPeticion As Long
Dim via As String
If blnfr28 Then
    If Not IsNull(rdcun!FR28CANTIDADDIL) Then CantidadDil = rdcun!FR28CANTIDADDIL Else CantidadDil = 0
    If Not IsNull(rdcun!FR28DOSIS) Then Dosis = rdcun!FR28DOSIS Else Dosis = 0
    If Not IsNull(rdcun!FR66CODPETICION) Then CodPeticion = rdcun!FR66CODPETICION Else CodPeticion = 0
ElseIf blnfrp4 Then
    If Not IsNull(rdcun!FRP4CANTIDADDIL) Then CantidadDil = rdcun!FRP4CANTIDADDIL Else CantidadDil = 0
    If Not IsNull(rdcun!FRP4DOSIS) Then Dosis = rdcun!FRP4DOSIS Else Dosis = 0
    If Not IsNull(rdcun!FRP4CODPETICION) Then CodPeticion = rdcun!FRP4CODPETICION Else CodPeticion = 0
End If


If intTipoOm <> 7 And intTipoOm <> 8 Then 'No es fluidoterapia ni mezcla
    str = "select ADMISSIONS.WEIGHT FROM ADMISSIONS,PICISDATA WHERE " & _
    "ADMISSIONS.ADMISSIONDBOID =PICISDATA.ADMISSIONDBOID AND " & _
    " PICISDATA.PICISDATADBOID= ?"
    Set Qy = objApp.rdoConnect.CreateQuery("", str)
        Qy(0) = ObtenerPicisData(CodPeticion, blnfr28, blnfrp4)
    Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

    For i = 0 To 8
        If i <> 7 Then
            ierrcode = PcsTaskDataAlloc(hTaskData)
            ierrcode = PcsDbObjectGenerateIdentifier(hTaskData)
            ierrcode = PcsDbObjectGetIdentifier(hTaskData, TaskDataDboid, 21)
            ierrcode = PcsTaskDataFree(hTaskData)
            
            strTaskData = "INSERT INTO TASKDATA (TASKDATADBOID, TASKDATAINDEX, " & _
            "TASKDBOID, ADDITIONALINFO) VALUES (?, ?, ?, ?)"
            Set qyTaskData = objApp.rdoConnect.CreateQuery("", strTaskData)
            qyTaskData(0) = TaskDataDboid
            qyTaskData(1) = i
            qyTaskData(2) = TaskDboid
                Select Case i
                Case 0
                    qyTaskData(3) = CStr(Dosis)
                Case 1
                    qyTaskData(3) = ObtenerUnitDboid(rdcun!FR93CODUNIMEDIDA)
                Case 2
                    qyTaskData(3) = CStr(CantidadDil)
                Case 3
                    qyTaskData(3) = "45000000000008000000" 'masin:dboid de mililitros
                Case 4
                    qyTaskData(3) = CStr(rd!Weight)
                Case 5
                    qyTaskData(3) = "45000000000017000000" 'Dboid de Kilogramos
                Case 6
                    qyTaskData(3) = -1
                Case 8
                    qyTaskData(3) = ObtenerRouteDboid(rdcun!FR34CODVIA)
                End Select
           
            qyTaskData.Execute
        End If
    Next
rd.Close
Qy.Close

Else 'Cuando es fluidoterapia o mezcla solo se rellenan los indices 5,6 y 8.
    For i = 5 To 8
        If i <> 7 Then
            ierrcode = PcsTaskDataAlloc(hTaskData)
            ierrcode = PcsDbObjectGenerateIdentifier(hTaskData)
            ierrcode = PcsDbObjectGetIdentifier(hTaskData, TaskDataDboid, 21)
            ierrcode = PcsTaskDataFree(hTaskData)
            
            strTaskData = "INSERT INTO TASKDATA (TASKDATADBOID, TASKDATAINDEX, " & _
            "TASKDBOID, ADDITIONALINFO) VALUES (?, ?, ?, ?)"
            Set qyTaskData = objApp.rdoConnect.CreateQuery("", strTaskData)
            qyTaskData(0) = TaskDataDboid
            qyTaskData(1) = i
            qyTaskData(2) = TaskDboid
                Select Case i
                Case 5
                    qyTaskData(3) = "45000000000017000000" 'Dboid de Kilogramos
                Case 6
                    qyTaskData(3) = -1
                Case 8
                    qyTaskData(3) = ObtenerRouteDboid(rdcun!FR34CODVIA)
                End Select
            qyTaskData(3) = TaskDataDboid
            qyTaskData.Execute
        End If
    Next
End If
End Function

Public Function OrdenPeriodicaContinua(CodPeticion As Long, numLinea As Long, _
rdcun As rdoResultset, FechaInicio As String, _
FechaFin As String, via As String, _
rdforma As rdoResultset, _
rdmedicamento As rdoResultset, OrderDboid As String, _
ExtraInfo As Double, intTipoOm As Integer, _
rdAditivo As rdoResultset, codProducto1 As Long, CodProducto2 As Long, _
codProducto3 As Long, rdAditivo2 As rdoResultset, blnfr28 As Boolean, _
blnfrp4 As Boolean)

Dim FechaTask As String
Dim FechaUltima As Date
Dim strDia As String
Dim FechaAdministracion As String

FechaTask = FechaInicio
If Not IsNull(rdcun!FRG4CODFRECUENCIA) Then
    Select Case rdcun!FRH5CODPERIODICIDAD
    Case "Alternos"
        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
            Call HoraPeriodicaContinua(FechaTask, rdcun, OrderDboid, rdforma, _
                                        FechaInicio, FechaFin, FechaAdministracion, _
                                         ExtraInfo, intTipoOm, rdAditivo, _
                                        codProducto1, CodProducto2, CodPeticion, _
                                        numLinea, codProducto3, rdAditivo2, blnfr28, blnfrp4)
            FechaTask = DateAdd("d", 2, CDate(FechaTask))
        Loop
   Case "Diario"
        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
            Call HoraPeriodicaContinua(FechaTask, rdcun, OrderDboid, rdforma, _
                                        FechaInicio, FechaFin, FechaAdministracion, _
                                         ExtraInfo, intTipoOm, rdAditivo, _
                                        codProducto1, CodProducto2, CodPeticion, _
                                        numLinea, codProducto3, rdAditivo2, blnfr28, blnfrp4)
            FechaTask = DateAdd("d", 1, CDate(FechaTask))
        Loop
    Case "Domingo No"
        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
            strDia = Format(FechaTask, "dddd")
            If strDia <> "domingo" Then
                'Funcion
            Call HoraPeriodicaContinua(FechaTask, rdcun, OrderDboid, rdforma, _
                                        FechaInicio, FechaFin, FechaAdministracion, _
                                         ExtraInfo, intTipoOm, rdAditivo, _
                                        codProducto1, CodProducto2, CodPeticion, _
                                        numLinea, codProducto3, rdAditivo2, blnfr28, blnfrp4)
            End If
            FechaTask = DateAdd("d", 1, CDate(FechaTask))
        Loop
    Case "S�bado No"
        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
            strDia = Format(FechaTask, "dddd")
            If strDia <> "s�bado" Then
             'funcion
            Call HoraPeriodicaContinua(FechaTask, rdcun, OrderDboid, rdforma, _
                                        FechaInicio, FechaFin, FechaAdministracion, _
                                         ExtraInfo, intTipoOm, rdAditivo, _
                                        codProducto1, CodProducto2, CodPeticion, _
                                        numLinea, codProducto3, rdAditivo2, blnfr28, blnfrp4)
            End If
            FechaTask = DateAdd("d", 1, CDate(FechaTask))
        Loop
    Case "Lunes No"
        Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
            strDia = Format(FechaTask, "dddd")
            If strDia <> "lunes" Then
            Call HoraPeriodicaContinua(FechaTask, rdcun, OrderDboid, rdforma, _
                                        FechaInicio, FechaFin, FechaAdministracion, _
                                         ExtraInfo, intTipoOm, rdAditivo, _
                                        codProducto1, CodProducto2, CodPeticion, _
                                        numLinea, codProducto3, rdAditivo2, blnfr28, blnfrp4)
            End If
            FechaTask = DateAdd("d", 1, CDate(FechaTask))
        Loop
    Case "Pares"
        If EsPar(FechaInicio) Then
            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
                'Funcion
            Call HoraPeriodicaContinua(FechaTask, rdcun, OrderDboid, rdforma, _
                                        FechaInicio, FechaFin, FechaAdministracion, _
                                         ExtraInfo, intTipoOm, rdAditivo, _
                                        codProducto1, CodProducto2, CodPeticion, _
                                        numLinea, codProducto3, rdAditivo2, blnfr28, blnfrp4)
                FechaTask = DateAdd("d", 2, CDate(FechaTask))
            Loop
        Else
            FechaTask = DateAdd("d", 1, FechaTask)
            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
            Call HoraPeriodicaContinua(FechaTask, rdcun, OrderDboid, rdforma, _
                                        FechaInicio, FechaFin, FechaAdministracion, _
                                         ExtraInfo, intTipoOm, rdAditivo, _
                                        codProducto1, CodProducto2, CodPeticion, _
                                        numLinea, codProducto3, rdAditivo2, blnfr28, blnfrp4)
                FechaTask = DateAdd("d", 2, CDate(FechaTask))
            Loop
        End If
    Case "Impares"
        If Not EsPar(FechaInicio) Then
            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
                'Funcion
            Call HoraPeriodicaContinua(FechaTask, rdcun, OrderDboid, rdforma, _
                                        FechaInicio, FechaFin, FechaAdministracion, _
                                         ExtraInfo, intTipoOm, rdAditivo, _
                                        codProducto1, CodProducto2, CodPeticion, _
                                        numLinea, codProducto3, rdAditivo2, blnfr28, blnfrp4)
                FechaTask = DateAdd("d", 2, CDate(FechaTask))
            Loop
        Else
            FechaTask = DateAdd("d", 1, FechaTask)
            Do While DateDiff("s", CDate(FechaTask), CDate(FechaFin)) >= -60
                'Funcion
            Call HoraPeriodicaContinua(FechaTask, rdcun, OrderDboid, rdforma, _
                                        FechaInicio, FechaFin, FechaAdministracion, _
                                         ExtraInfo, intTipoOm, rdAditivo, _
                                        codProducto1, CodProducto2, CodPeticion, _
                                        numLinea, codProducto3, rdAditivo2, blnfr28, blnfrp4)
                FechaTask = DateAdd("d", 2, CDate(FechaTask))
            Loop
        End If
    End Select
Else 'Si no hay frecuencia tampoco periodicidad. Es un ahora
    Call HoraPeriodicaContinua(FechaTask, rdcun, OrderDboid, rdforma, _
                                FechaInicio, FechaFin, FechaAdministracion, _
                                 ExtraInfo, intTipoOm, rdAditivo, _
                                codProducto1, CodProducto2, CodPeticion, _
                                numLinea, codProducto3, rdAditivo2, blnfr28, blnfrp4)
End If
End Function
Public Function HoraPeriodicaContinua(FechaTask As String, rdcun As rdoResultset, _
OrderDboid As String, rdforma As rdoResultset, FechaInicio As String, _
FechaFin As String, FechaAdministracion As String, _
ExtraInfo As Double, intTipoOm As Integer, rdAditivo As rdoResultset, _
codProducto1 As Long, CodProducto2 As Long, CodPeticion As Long, numLinea As Long, _
codProducto3 As Long, rdAditivo2 As rdoResultset, blnfr28 As Boolean, blnfrp4 As Boolean)

Dim strHora As String
Dim qyHora As rdoQuery
Dim rdHora As rdoResultset
Dim strOrder As String
Dim qyOrder As rdoQuery
Dim HoraAdministracion As String
Dim TIEMMININF As Long
Dim Observaciones As String
Dim FecFin$, s$, i%
If blnfr28 Then
    If Not IsNull(rdcun!FR28TIEMMININF) Then TIEMMININF = rdcun!FR28TIEMMININF Else TIEMMININF = 0
    If Not IsNull(rdcun!FR28INSTRADMIN) Then Observaciones = rdcun!FR28INSTRADMIN Else Observaciones = ""
    If Not IsNull(rdcun!FR66CODPETICION) Then CodPeticion = rdcun!FR66CODPETICION Else CodPeticion = 0
ElseIf blnfrp4 Then
    If Not IsNull(rdcun!FRP4TIEMMININF) Then TIEMMININF = rdcun!FRP4TIEMMININF Else TIEMMININF = 0
    If Not IsNull(rdcun!FRP4INSTRADMIN) Then Observaciones = rdcun!FRP4INSTRADMIN Else Observaciones = ""
    If Not IsNull(rdcun!FRP4CODPETICION) Then CodPeticion = rdcun!FRP4CODPETICION Else CodPeticion = 0
End If
If Not IsNull(rdcun!FRG4CODFRECUENCIA) Then
    If rdcun!FRG4CODFRECUENCIA <> "Manual" Then
        strHora = "SELECT to_date(replace(to_char(FRG5HORA*100,'00,00'),',',':'),'hh24:mi') " & _
        "FROM FRG500 WHERE FRG4CODFRECUENCIA= ? ORDER BY FRG5HORA ASC"
        Set qyHora = objApp.rdoConnect.CreateQuery("", strHora)
            qyHora(0) = rdcun!FRG4CODFRECUENCIA
        Set rdHora = qyHora.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        rdHora.MoveFirst
            Do While Not rdHora.EOF
                HoraAdministracion = Format(rdHora(0), "HH:MM:SS")
                FechaAdministracion = Format(Format(FechaTask, "DD/MM/YYYY") & " " & HoraAdministracion, "DD/MM/YYYY HH:MM:SS")
                    If (DateDiff("s", CDate(FechaAdministracion), CDate(FechaFin)) >= 0) And _
                    (DateDiff("s", CDate(FechaInicio), CDate(FechaAdministracion)) >= 0) Then
                            objApp.BeginTrans
                            If intTipoOm = 2 Or intTipoOm = 4 Or intTipoOm = 6 Then
                                FecFin = Format(DateAdd("d", 1, FechaAdministracion), "DD/MM/YYYY HH:MM:SS")
                            Else
                                FecFin = Format(DateAdd("n", TIEMMININF, FechaAdministracion), "DD/MM/YYYY HH:MM:SS")
                            End If
                            Call Orders(rdcun, FechaAdministracion, FecFin, _
                            rdcun!FR34CODVIA, rdforma, rdmedicamento, _
                            OrderDboid, ExtraInfo, intTipoOm, rdAditivo, codProducto1, _
                            CodProducto2, codProducto3, rdAditivo2, blnfr28, blnfrp4, False)
                            If Trim(Observaciones) <> "" Then
                                Call EscribirMemo(OrderDboid, Observaciones, ObtenerStaff(CodPeticion, blnfr28, blnfrp4))
                            End If
                            If Err > 0 Then
                                objApp.RollbackTrans
                                MsgBox "ERROR: La orden m�dica no ha pasado a Picis" & Chr(13) & _
                                "Contacte con su administrador de sistema"
                                blnError = True
                                Exit Function
                            Else: objApp.CommitTrans
                            End If
                            If Not blnError Then
                                If blnfr28 Then
                                    strOrder = "INSERT INTO FRP300 (FR66CODPETICION, FR28NUMLINEA, ORDERDBOID)"
                                    strOrder = strOrder & " VALUES(?,?,?)"
                                    Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
                                        qyOrder(0) = CodPeticion
                                        qyOrder(1) = numLinea
                                        qyOrder(2) = OrderDboid
                                    qyOrder.Execute
                                    qyOrder.Close
                                ElseIf blnfrp4 Then
                                    strOrder = "INSERT INTO FRP500 (FRP4CODPETICION, ORDERDBOID)"
                                    strOrder = strOrder & " VALUES(?,?)"
                                    Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
                                        qyOrder(0) = CodPeticion
                                        qyOrder(1) = OrderDboid
                                    qyOrder.Execute
                                    qyOrder.Close
                                End If
                            End If
                    End If
                rdHora.MoveNext
            Loop
    Else ' Si la periodicidad es Manual, se buscan las horas por otro camino
        strHora = "SELECT FRP4HORAS FROM FRP400 WHERE FRP4CODPETICION=?"
        Set qyHora = objApp.rdoConnect.CreateQuery("", strHora)
            qyHora(0) = CodPeticion
        Set rdHora = qyHora.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        frmEntorno.Awk = rdHora!FRP4HORAS
        rdHora.Close
        qyHora.Close
        For i = 1 To frmEntorno.Awk.NF
            HoraAdministracion = Format(frmEntorno.Awk.F(i) & ":00:00", "HH:MM:SS")
                FechaAdministracion = Format(Format(FechaTask, "DD/MM/YYYY") & " " & HoraAdministracion, "DD/MM/YYYY HH:MM:SS")
                    If (DateDiff("s", CDate(FechaAdministracion), CDate(FechaFin)) >= 0) And _
                    (DateDiff("s", CDate(FechaInicio), CDate(FechaAdministracion)) >= 0) Then
                            objApp.BeginTrans
                            If intTipoOm = 2 Or intTipoOm = 4 Or intTipoOm = 6 Then
                                FecFin = Format(DateAdd("d", 1, FechaAdministracion), "DD/MM/YYYY HH:MM:SS")
                            Else
                                FecFin = Format(DateAdd("n", TIEMMININF, FechaAdministracion), "DD/MM/YYYY HH:MM:SS")
                            End If
                            Call Orders(rdcun, FechaAdministracion, FecFin, _
                            rdcun!FR34CODVIA, rdforma, rdmedicamento, _
                            OrderDboid, ExtraInfo, intTipoOm, rdAditivo, codProducto1, _
                            CodProducto2, codProducto3, rdAditivo2, blnfr28, blnfrp4, False)
                            If Trim(Observaciones) <> "" Then
                                Call EscribirMemo(OrderDboid, Observaciones, ObtenerStaff(CodPeticion, blnfr28, blnfrp4))
                            End If
                            If Err > 0 Then
                                objApp.RollbackTrans
                                MsgBox "ERROR: La orden m�dica no ha pasado a Picis" & Chr(13) & _
                                "Contacte con su administrador de sistema"
                                blnError = True
                                Exit Function
                            Else: objApp.CommitTrans
                            End If
                            If Not blnError Then
                                If blnfr28 Then
                                    strOrder = "INSERT INTO FRP300 (FR66CODPETICION, FR28NUMLINEA, ORDERDBOID)"
                                    strOrder = strOrder & " VALUES(?,?,?)"
                                    Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
                                        qyOrder(0) = CodPeticion
                                        qyOrder(1) = numLinea
                                        qyOrder(2) = OrderDboid
                                    qyOrder.Execute
                                    qyOrder.Close
                                ElseIf blnfrp4 Then
                                    strOrder = "INSERT INTO FRP500 (FRP4CODPETICION, ORDERDBOID)"
                                    strOrder = strOrder & " VALUES(?,?)"
                                    Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
                                        qyOrder(0) = CodPeticion
                                        qyOrder(1) = OrderDboid
                                    qyOrder.Execute
                                    qyOrder.Close
                                End If
                            End If
                    End If
        Next i
    End If
Else
FechaAdministracion = Format(FechaInicio, "DD/MM/YYYY HH:MM:SS")
    If (DateDiff("s", CDate(FechaAdministracion), CDate(FechaFin)) >= 0) And _
    (DateDiff("s", CDate(FechaInicio), CDate(FechaAdministracion)) >= 0) Then
            objApp.BeginTrans
            If intTipoOm = 2 Or intTipoOm = 4 Or intTipoOm = 6 Then
                FecFin = Format(DateAdd("d", 1, FechaAdministracion), "DD/MM/YYYY HH:MM:SS")
            Else
                FecFin = Format(DateAdd("n", TIEMMININF, FechaAdministracion), "DD/MM/YYYY HH:MM:SS")
            End If
            Call Orders(rdcun, FechaAdministracion, _
            FecFin, _
            rdcun!FR34CODVIA, rdforma, rdmedicamento, _
            OrderDboid, ExtraInfo, intTipoOm, rdAditivo, codProducto1, _
            CodProducto2, codProducto3, rdAditivo2, blnfr28, blnfrp4, False)
            If Trim(Observaciones) <> "" Then
                Call EscribirMemo(OrderDboid, Observaciones, ObtenerStaff(CodPeticion, blnfr28, blnfrp4))
            End If
            If Err > 0 Then
                objApp.RollbackTrans
                MsgBox "ERROR: La orden m�dica no ha pasado a Picis" & Chr(13) & _
                "Contacte con su administrador de sistema"
                blnError = True
                Exit Function
            Else: objApp.CommitTrans
            End If
            If Not blnError Then
                If blnfr28 Then
                    strOrder = "INSERT INTO FRP300 (FR66CODPETICION, FR28NUMLINEA, ORDERDBOID)"
                    strOrder = strOrder & " VALUES(?,?,?)"
                    Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
                        qyOrder(0) = CodPeticion
                        qyOrder(1) = numLinea
                        qyOrder(2) = OrderDboid
                    qyOrder.Execute
                    qyOrder.Close
                ElseIf blnfrp4 Then
                    strOrder = "INSERT INTO FRP500 (FRP4CODPETICION, ORDERDBOID)"
                    strOrder = strOrder & " VALUES(?,?)"
                    Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
                        qyOrder(0) = CodPeticion
                        qyOrder(1) = OrderDboid
                    qyOrder.Execute
                    qyOrder.Close
                End If
            End If
    End If
End If
End Function

Public Function ObtenerStaff(CodPeticion As Long, blnfr28 As Boolean, _
blnfrp4 As Boolean) As String
Dim strMed As String
Dim rdMed As rdoResultset
Dim qyMed As rdoQuery

'OBTENER EL MEDICO QUE REALIZA EL PROCESO
If blnfr28 Then
    strMed = "SELECT STAFF.STAFFDBOID FROM FR2800, FR6600, STAFF " & _
    "Where FR2800.FR66CODPETICION = FR6600.FR66CODPETICION And (FR2800.FR66CODPETICION = ?) " & _
    "AND FR6600.SG02COD_MED=STAFF.USERNAME"
    Set qyMed = objApp.rdoConnect.CreateQuery("", strMed)
    qyMed(0) = CodPeticion
    Set rdMed = qyMed.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        ObtenerStaff = rdMed!staffdboid
    rdMed.Close
    qyMed.Close
ElseIf blnfrp4 Then
    strMed = "SELECT STAFF.STAFFDBOID FROM FRP400, STAFF " & _
    "Where FRP400.FRP4CODPETICION = ? " & _
    "AND FRP400.SG02COD=STAFF.USERNAME"
    Set qyMed = objApp.rdoConnect.CreateQuery("", strMed)
    qyMed(0) = CodPeticion
    Set rdMed = qyMed.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        ObtenerStaff = rdMed!staffdboid
    rdMed.Close
    qyMed.Close
End If
End Function

Public Function ObtenerPicisData(CodPeticion As Long, blnfr28 As Boolean, _
blnfrp4 As Boolean) As String

Dim strPiDboid As String
Dim rdPiDboid As rdoResultset
Dim qyPiDboid As rdoQuery
Dim codAsistencia As Long
codAsistencia = ObtenerAsistencia(CodPeticion, blnfr28, blnfrp4)
'Obtener el PicisdataDboid
If blnfr28 Then
    strPiDboid = "SELECT PICISDATA.PICISDATADBOID From ADMISSIONS, PICISDATA " & _
    "Where ADMISSIONS.ADMISSIONDBOID = PICISDATA.ADMISSIONDBOID " & _
    "AND ADMISSIONS.ADMID1= ?"
    Set qyPiDboid = objApp.rdoConnect.CreateQuery("", strPiDboid)
    qyPiDboid(0) = codAsistencia
    Set rdPiDboid = qyPiDboid.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    ObtenerPicisData = rdPiDboid!PICISDATADBOID
    rdPiDboid.Close
    qyPiDboid.Close
ElseIf blnfrp4 Then
    strPiDboid = "SELECT PICISDATA.PICISDATADBOID From ADMISSIONS, PICISDATA " & _
    "Where ADMISSIONS.ADMISSIONDBOID = PICISDATA.ADMISSIONDBOID " & _
    "AND ADMISSIONS.ADMID1= ?"
    Set qyPiDboid = objApp.rdoConnect.CreateQuery("", strPiDboid)
    qyPiDboid(0) = codAsistencia
    Set rdPiDboid = qyPiDboid.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    ObtenerPicisData = rdPiDboid!PICISDATADBOID
    rdPiDboid.Close
    qyPiDboid.Close
End If
End Function
Public Function ObtenerAsistencia(CodPeticion As Long, blnfr28 As Boolean, _
blnfrp4 As Boolean) As Long

Dim strAs As String
Dim rdAs As rdoResultset
Dim qyAs As rdoQuery
If blnfr28 Then
    strAs = "SELECT AD01CODASISTENCI FROM FR6600 WHERE FR66CODPETICION=?"
    Set qyAs = objApp.rdoConnect.CreateQuery("", strAs)
        qyAs(0) = CodPeticion
    Set rdAs = qyAs.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    ObtenerAsistencia = rdAs!ad01codasistenci
    rdAs.Close
    qyAs.Close
ElseIf blnfrp4 Then
    strAs = "SELECT AD01CODASISTENCI FROM FRP400 WHERE FRP4CODPETICION=?"
    Set qyAs = objApp.rdoConnect.CreateQuery("", strAs)
        qyAs(0) = CodPeticion
    Set rdAs = qyAs.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    ObtenerAsistencia = rdAs!ad01codasistenci
    rdAs.Close
    qyAs.Close
End If
End Function

Public Function ObtenerStaffDesdeCodigo(codUser As String) As String
Dim strMed As String
Dim rdMed As rdoResultset
Dim qyMed As rdoQuery

'OBTENER el usuario que intenta realizar cambios en Picis
strMed = "SELECT STAFFDBOID FROM  STAFF " & _
"WHERE USERNAME=?"
Set qyMed = objApp.rdoConnect.CreateQuery("", strMed)
qyMed(0) = codUser
Set rdMed = qyMed.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    ObtenerStaffDesdeCodigo = rdMed!staffdboid
rdMed.Close
qyMed.Close
End Function

Public Function ObtenerOrderDboidDesdePeticion(CodPeticion As Long, _
numLinea As Integer, blnfr28 As Boolean, blnfrp4 As Boolean) As String

Dim strOrder As String
Dim qyOrder As rdoQuery
Dim rdOrder As rdoResultset
If blnfr28 Then
    strOrder = "SELECT ORDERDBOID FROM FRP300 WHERE " & _
    "FR66CODPETICION= ? AND FR28NUMLINEA=?"
    
    Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
        qyOrder(0) = CodPeticion
        qyOrder(1) = numLinea
    Set rdOrder = qyOrder.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
ElseIf blnfrp4 Then
    strOrder = "SELECT ORDERDBOID FROM FRP500 WHERE " & _
    "FRP4CODPETICION= ? "
    
    Set qyOrder = objApp.rdoConnect.CreateQuery("", strOrder)
        qyOrder(0) = CodPeticion
    Set rdOrder = qyOrder.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
End If
    ObtenerOrderDboidDesdePeticion = rdOrder!OrderDboid
    
rdOrder.Close
qyOrder.Close
End Function

Public Sub ModificarTaskData(TaskDboid As String, _
Dosis As String, disolucion As Long, UniMedicamento As String, _
CodPeticion As Long, RouteDboid As String, blnfr28 As Boolean, _
blnfrp4 As Boolean)
'*********************************************************
'Esta funcion MODIFICA la velocidad de la perfusion sobre la tabla TaskData
'Si es una perfusion escribe 7 indices.
'Si no es una perfusion escribe 3 �ndices
'********************************************************
Dim hTaskData As Long
Dim TaskDataDboid As String * 21
Dim strTaskData As String
Dim qyTaskData As rdoQuery
Dim i As Integer
Dim str As String
Dim Qy As rdoQuery
Dim rd As rdoResultset

str = "select ADMISSIONS.WEIGHT FROM ADMISSIONS,PICISDATA WHERE " & _
"ADMISSIONS.ADMISSIONDBOID =PICISDATA.ADMISSIONDBOID AND " & _
" PICISDATA.PICISDATADBOID= ?"
Set Qy = objApp.rdoConnect.CreateQuery("", str)
Qy(0) = ObtenerPicisData(CodPeticion, blnfr28, blnfrp4)
Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

    For i = 0 To 8
        If i <> 7 Then
            ierrcode = PcsTaskDataAlloc(hTaskData)
            ierrcode = PcsDbObjectGenerateIdentifier(hTaskData)
            ierrcode = PcsDbObjectGetIdentifier(hTaskData, TaskDataDboid, 21)
            ierrcode = PcsTaskDataFree(hTaskData)
            
            strTaskData = "INSERT INTO TASKDATA (TASKDATADBOID, TASKDATAINDEX, " & _
            "TASKDBOID, ADDITIONALINFO) VALUES (?, ?, ?, ?)"
            Set qyTaskData = objApp.rdoConnect.CreateQuery("", strTaskData)
            qyTaskData(0) = TaskDataDboid
            qyTaskData(1) = i
            qyTaskData(2) = TaskDboid
                Select Case i
                Case 0
                    qyTaskData(3) = Dosis
                Case 1
                    qyTaskData(3) = ObtenerUnitDboid(UniMedicamento)
                Case 2
                    qyTaskData(3) = disolucion
                Case 3
                    qyTaskData(3) = "45000000000008000000" 'masin:dboid de mililitros
                Case 4
                    qyTaskData(3) = CStr(rd!Weight)
                Case 5
                    qyTaskData(3) = "45000000000017000000" 'Dboid de Kilogramos
                Case 6
                    qyTaskData(3) = -1
                Case 8
                    qyTaskData(3) = RouteDboid
                End Select
           
            qyTaskData.Execute
        End If
    Next
    rd.Close
    Qy.Close
End Sub



Public Function ModificarTask(OrderDboid As String, staffdboid As String, _
velocidad As String, _
Dosis As String, disolucion As Long, UniMedicamento As String, _
CodPeticion As Long, Observaciones As String, RouteDboid As String, _
blnfr28 As Boolean, blnfrp4 As Boolean, rdpat As rdoResultset)

Dim strTask As String
Dim qytask As rdoQuery
Dim TaskDboid As String * 21
Dim HoraAdministracion As String
Dim UnidadDosis As String
Dim DosisTask As String
Dim str$
Dim Qy As rdoQuery
Dim rd As rdoResultset
Dim DosisHigh$, strPims$
Dim DosisLow$
'******************************************************************
'Esta funcion modifica la dosis de la Task para las perfusiones PC
'Despues manda a escribir sobre la tabla TaskData
'*******************************************************************
If Dosis = "" Then Dosis = "0"
DosisTask = ModificarDosis(velocidad, Dosis, disolucion, UniMedicamento, _
CodPeticion, UnidadDosis, blnfr28, blnfrp4)

str = "SELECT HIGHERDOSE, LOWERDOSE FROM ORDERS WHERE ORDERDBOID=?"
Set Qy = objApp.rdoConnect.CreateQuery("", str)
    Qy(0) = OrderDboid
Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
DosisHigh = rd!HIGHERDOSE
DosisLow = rd!LOWERDOSE
rd.Close
Qy.Close
If DosisTask > DosisHigh Then
    DosisHigh = DosisTask
ElseIf DosisTask < DosisLow Then
    DosisLow = DosisTask
End If

str = "UPDATE ORDERS SET HIGHERDOSE=?, LOWERDOSE=?," & _
"CONDITION=? WHERE ORDERDBOID= ? "
Set Qy = objApp.rdoConnect.CreateQuery("", str)
    Qy(0) = DosisHigh
    Qy(1) = DosisLow
    Qy(2) = Observaciones
    Qy(3) = OrderDboid
Qy.Execute
Qy.Close
Dim j As Long
j = PcsVbPimsSendAllOrders(rdpat!PatientDboid, 1)

    ierrcode = PcsTaskAlloc(hTask)
    ierrcode = PcsDbObjectGenerateIdentifier(hTask)
    ierrcode = PcsDbObjectGetIdentifier(hTask, TaskDboid, 21)
    ierrcode = PcsTaskFree(hTask)
    
    strTask = "INSERT INTO TASKS (TASKDBOID,CREATIONDATE,TODODATE, DONEDATE, " & _
    "VALIDATEDDATE, DOSE, HASAMEMO, STAFFDBOID, " & _
    "ORDERDBOID, TASKSTATUSDBOID, GROUPDBOID, UNITDBOID) " & _
    "VALUES (?, SYSDATE, SYSDATE, SYSDATE, " & _
    "SYSDATE, ?, ?, ?, " & _
    "?, ?, ?, ?)"
    Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
        qytask(0) = TaskDboid
        qytask(1) = DosisHigh
        If Observaciones <> "" Then
            qytask(2) = "T"
            Call EscribirMemo(TaskDboid, Observaciones, staffdboid)
        Else
            qytask(2) = "F"
        End If
        qytask(3) = staffdboid
        qytask(4) = OrderDboid
        qytask(5) = "170000000000004000000" 'Connect
        qytask(6) = "21000000000031000511" 'validaci�n enfermeras
        qytask(7) = UnidadDosis
    qytask.Execute
    qytask.Close
  'j = PcsVbPimsSendAllOrders(rdpat!PatientDboid, 1)
    strTask = "UPDATE TASKS SET TODODATE=SYSDATE WHERE TASKSTATUSDBOID=?"
    Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
        qytask(0) = "170000000000011000000" 'current Time
    qytask.Execute
    qytask.Close
    
    Call ModificarTaskData(TaskDboid, Dosis, disolucion, UniMedicamento, _
        CodPeticion, RouteDboid, blnfr28, blnfrp4)
 '   j = PcsVbPimsFree()
End Function
Public Function ModificarTaskMemo(OrderDboid As String, staffdboid As String, _
CodPeticion As Long, Observaciones As String, _
blnfr28 As Boolean, blnfrp4 As Boolean, rdpat As rdoResultset)

Dim strTask As String
Dim qytask As rdoQuery
Dim TaskDboid As String * 21
Dim HoraAdministracion As String
Dim UnidadDosis As String
Dim DosisTask As String
Dim str$
Dim Qy As rdoQuery
Dim rd As rdoResultset
Dim DosisHigh$, strPims$
Dim DosisLow$
'******************************************************************
'Esta funcion modifica la dosis de la Task para las perfusiones PC
'Despues manda a escribir sobre la tabla TaskData
'*******************************************************************
str = "SELECT HIGHERDOSE, LOWERDOSE FROM ORDERS WHERE ORDERDBOID=?"
Set Qy = objApp.rdoConnect.CreateQuery("", str)
    Qy(0) = OrderDboid
Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

    ierrcode = PcsTaskAlloc(hTask)
    ierrcode = PcsDbObjectGenerateIdentifier(hTask)
    ierrcode = PcsDbObjectGetIdentifier(hTask, TaskDboid, 21)
    ierrcode = PcsTaskFree(hTask)
    
    strTask = "INSERT INTO TASKS (TASKDBOID,CREATIONDATE,TODODATE, DONEDATE, " & _
    "VALIDATEDDATE, DOSE, HASAMEMO, STAFFDBOID, " & _
    "ORDERDBOID, TASKSTATUSDBOID, GROUPDBOID, UNITDBOID) " & _
    "VALUES (?, SYSDATE, SYSDATE, SYSDATE, " & _
    "SYSDATE, ?, ?, ?, " & _
    "?, ?, ?, ?)"
    Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
        qytask(0) = TaskDboid
        qytask(1) = rd!HIGHERDOSE
        If Observaciones <> "" Then
            qytask(2) = "T"
            Call EscribirMemo(TaskDboid, Observaciones, staffdboid)
        Else
            qytask(2) = "F"
        End If
        qytask(3) = staffdboid
        qytask(4) = OrderDboid
        qytask(5) = "170000000000004000000" 'Connect
        qytask(6) = "21000000000031000511" 'validaci�n enfermeras
        qytask(7) = UnidadDosis
    qytask.Execute
    qytask.Close
  'j = PcsVbPimsSendAllOrders(rdpat!PatientDboid, 1)
    strTask = "UPDATE TASKS SET TODODATE=SYSDATE WHERE TASKSTATUSDBOID=?"
    Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
        qytask(0) = "170000000000011000000" 'current Time
    qytask.Execute
    qytask.Close
    
End Function
Public Function ModificarDosis(velocidad As String, _
Dosis As String, disolucion As Long, UniMedicamento As String, _
 CodPeticion As Long, UnidadDosis, blnfr28 As Boolean, _
 blnfrp4 As Boolean) As String

Dim str As String
Dim rd As rdoResultset
Dim Qy As rdoQuery
Dim resto As Double

str = "select ADMISSIONS.WEIGHT FROM ADMISSIONS,PICISDATA WHERE " & _
"ADMISSIONS.ADMISSIONDBOID =PICISDATA.ADMISSIONDBOID AND " & _
" PICISDATA.PICISDATADBOID= ?"
Set Qy = objApp.rdoConnect.CreateQuery("", str)
    Qy(0) = ObtenerPicisData(CodPeticion, blnfr28, blnfrp4)
Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    ModificarDosis = Format$((velocidad * (CDbl(Dosis) / (disolucion))) / (rd!Weight), "###0.000")
    ModificarDosis = ModificarDosis * 100
    resto = CStr(CDbl(ModificarDosis) - CInt(ModificarDosis))
    If resto >= 0.5 Then ModificarDosis = (CInt(ModificarDosis) + 1) Else ModificarDosis = CInt(ModificarDosis)
    ModificarDosis = ModificarDosis / 100
'ModificarDosis = Format((velocidad * (Dosis / disolucion)) / (rd!Weight), "###0.00")
    Select Case UniMedicamento
    Case "mg"
        UnidadDosis = "45000000000042000000" 'miligramo/kilo/hora
    Case "mcg"
        UnidadDosis = "45000000000043000000" 'microgramo/kilo/hora
    Case "g"
        UnidadDosis = "45000000000014000000" 'gramo/kilo/hora
    Case "ui"
        UnidadDosis = "45000000000057000511" 'unidad internacional/kilo/hora
    End Select
End Function
Public Function ObtenerRouteDboid(Route) As String
Dim strRuta As String
Dim qyRuta As rdoQuery
Dim rdRuta As rdoResultset
'Obtener el RouteDboid
strRuta = "SELECT ROUTEDBOID FROM  FR3400 WHERE FR34CODVIA=?"
Set qyRuta = objApp.rdoConnect.CreateQuery("", strRuta)
qyRuta(0) = Route
Set rdRuta = qyRuta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If IsNull(rdRuta(0)) Then
    ObtenerRouteDboid = "155000000000000000000" 'Ruta desconocida
Else
    ObtenerRouteDboid = rdRuta(0)
End If
rdRuta.Close
qyRuta.Close
End Function

Public Function ExisteMedicamento(codMedicamento As Long) As Boolean
Dim sql As String
Dim rd As rdoResultset
Dim Qy As rdoQuery

sql = "SELECT COUNT (*) FROM FRP200 WHERE FR73CODPRODUCTO=?"
Set Qy = objApp.rdoConnect.CreateQuery("", sql)
    Qy(0) = codMedicamento
Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rd.rdoColumns(0).Value > 0 Then
    ExisteMedicamento = True
Else
    ExisteMedicamento = False
End If
rd.Close
Qy.Close
End Function

Public Function ExisteFamilia(codFamilia As String) As Boolean
Dim str As String
Dim rd As rdoResultset
Dim Qy As rdoQuery

str = "SELECT COUNT(*) FROM FRP100 WHERE FR00CODGRPTERAP=?"
Set Qy = objApp.rdoConnect.CreateQuery("", str)
    Qy(0) = codFamilia
Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If rd.rdoColumns(0).Value > 0 Then
    ExisteFamilia = True
Else
    ExisteFamilia = False
End If
rd.Close
Qy.Close
End Function
Public Sub ModificarMedicamento(codMedicamento As Long)
Dim TreatDboid As String * 21
Dim strSql As String
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim qyUp As rdoQuery
Dim qyP1 As rdoQuery
Dim RsP1 As rdoResultset
Dim FAMILYDBOID As String
Dim DesMedicamentos As String
Dim longitud As Integer, PosEspacio%
Dim RsTr As rdoResultset
Dim qyTr As rdoQuery
Dim sql As String

strSql = "SELECT FR7300.FR73DESPRODUCTO,FR7300.FR00CODGRPTERAP, " & _
"FR7300.FR73FECFINVIG, FRP200.TREATMENTDBOID " & _
"FROM FR7300, FRP200 WHERE " & _
"FR7300.FR73CODPRODUCTO=? AND " & _
"FR7300.FR73CODPRODUCTO=FRP200.FR73CODPRODUCTO"
'"FR7300.FR73INDINFIV, FR7300.FR73DOSIS, " & _
'"FR7300.FR93CODUNIMEDIDA,FRH7CODFORMFAR, FR7300.FR73VOLUMEN,FR7300.FR34CODVIA "
Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
Qy(0) = codMedicamento
Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

strSql = "SELECT FRP100.FAMILYDBOID FROM FRP100, FAMILIES, CATEGORIES " & _
"WHERE FRP100.FR00CODGRPTERAP=? AND " & _
"(CATEGORIES.CATEGORYDBOID=? OR CATEGORIES.CATEGORYDBOID=? OR " & _
"CATEGORIES.CATEGORYDBOID=?) " & _
"AND FAMILIES.FAMILYDBOID=FRP100.FAMILYDBOID AND " & _
"CATEGORIES.CATEGORYDBOID=FAMILIES.CATEGORYDBOID"
Set qyP1 = objApp.rdoConnect.CreateQuery("", strSql)
    qyP1(0) = Mid(Rs!FR00CODGRPTERAP, 1, 3)
    qyP1(1) = "157000000000002000000" 'CATEGORIA DE PERFUSI�N
    qyP1(2) = "157000000000100000000" 'CATEGOR�A DE ADITIVOS
    qyP1(3) = "157000000000001000000" 'CATEGORIA DE MEDICAMENTOS
Set RsP1 = qyP1.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

PosEspacio = InStr(1, Rs!FR73DESPRODUCTO, " ")
DesMedicamentos = Left(Rs!FR73DESPRODUCTO, PosEspacio - 1)

longitud = Len(DesMedicamentos)
DesMedicamentos = UCase(Mid(DesMedicamentos, 1, 1)) & LCase(Mid(DesMedicamentos, 2, longitud - 1))
    Do While Not Rs.EOF And Not RsP1.EOF
        sql = "UPDATE TREATMENTS SET BRANDNAME=?, GENERICNAME=?, ISDELETED=?, " & _
        "FAMILYDBOID=?, GROUPDBOID=? WHERE TREATMENTDBOID=?"
        Set qyTr = objApp.rdoConnect.CreateQuery("", sql)
            qyTr(0) = DesMedicamentos
            qyTr(1) = DesMedicamentos
            If IsNull(Rs!FR73FECFINVIG) Or Rs!FR73FECFINVIG > strFechaHora_Sistema Then
                qyTr(2) = "F"
            Else
                qyTr(2) = "T"
            End If
            qyTr(3) = RsP1!FAMILYDBOID
            qyTr(4) = "21000000000019000511" 'gRUPO ENFERMERAS ANESTESIA
            qyTr(5) = Rs!TREATMENTDBOID
        qyTr.Execute
        RsP1.MoveNext
        Rs.MoveNext
    Loop
RsP1.Close
Rs.Close
Qy.Close
qyTr.Close
End Sub


Public Sub ModificarFamilia(codFamilia As String)
Dim FAMILYDBOID$
Dim strSql As String
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim sql As String
Dim qyIN As rdoQuery
Dim DescripcionFamilia As String
Dim longitud As Integer

strSql = "SELECT FR0000.FR00DESGRPTERAP,FRP100.FAMILYDBOID " & _
"FROM FR0000,FRP100 WHERE FR0000.FR00CODGRPTERAP=? AND " & _
"FR0000.FR00CODGRPTERAP=FRP100.FR00CODGRPTERAP"
Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
Qy(0) = codFamilia
Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
longitud = Len(Rs!FR00DESGRPTERAP)
DescripcionFamilia = UCase(Mid(Rs!FR00DESGRPTERAP, 1, 1)) & LCase(Mid(Rs!FR00DESGRPTERAP, 2, longitud - 1))
Do While Not Rs.EOF
    strSql = "UPDATE FAMILIES SET FAMILYDESC=?, ISDELETED=? " & _
    "WHERE FAMILYDBOID=?"
    Set qyIN = objApp.rdoConnect.CreateQuery("", strSql)
    qyIN(0) = DescripcionFamilia
    qyIN(1) = "F"
    qyIN(2) = Rs!FAMILYDBOID
    qyIN.Execute
    Rs.MoveNext
Loop
Rs.Close
Qy.Close
qyIN.Close
End Sub


