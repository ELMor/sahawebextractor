Attribute VB_Name = "GENERAL"
Option Explicit
''Public objCW As clsCW       ' referencia al objeto CodeWizard
''Public objApp As clsCWApp
''Public objPipe As clsCWPipeLine
''Public objGen As clsCWGen
''Public objEnv As clsCWEnvironment
''Public objError As clsCWError
''Public objMouse As clsCWMouse
''Public objSecurity As clsCWSecurity
Public objcw As Object      ' referencia al objeto CodeWizard
Public objApp As clsCWApp
Public objPipe As Object
Public objGen As Object
Public objEnv As clsCWEnvironment
Public objError As Object
Public objMouse As Object
Public objSecurity As Object
Public objMRes As Object


'Declare Function GetModuleHandle Lib "kernel32" Alias "getmodulehandle" (ByVal lpModuleName As String) As Integer
Declare Function GetModuleFilename Lib "kernel32" (ByVal hModule As Integer, ByVal lpFileName As String, ByVal nSize As Integer) As Integer
Declare Function getmoduleusage Lib "kernel32" (ByVal hModule As Integer) As Integer
Declare Function GetModuleHandle Lib "kernel32" Alias "GetModuleHandleA" (ByVal lpModuleName As String) As Long

Declare Function Ini Lib "EnumWindows.dll" _
    Alias "_Ini@0" () As Integer
Declare Sub Fin Lib "EnumWindows.dll" _
    Alias "_Fin@0" ()
Declare Function AppTitle Lib "EnumWindows.dll" _
    Alias "_AppTitle@8" (i&, ByVal title&) As Integer

'Function ConexionesChartPlus() As Integer
'Dim ejecutable As String, hndl As Integer
'    ejecutable = "C:\Picis\ChartPlus\ChartPlus.exe"
'    hndl = GetModuleHandle(ejecutable)
'    Conexiones = getmoduleusage(hndl)
'End Function


'Function ConexionesVisualCare() As Integer
'Dim ejecutable As String, hndl As Integer
'    ejecutable = "C:\Picis\VisualCare\VisualCare.exe"
'    hndl = GetModuleHandle(ejecutable)
'    ConexionesVisualCare = getmoduleusage(hndl)
'End Function

'*******************************************************
'*                                                     *
'* FUNCIONES GENERALES                                 *
'*                                                     *
'*******************************************************

Public Function LOG() As Boolean
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim strSql As String
Dim Password As String
Dim PassDes As String
Dim PassEnc As String * 255
Dim PassEncNue As String
Dim UsuarioDefecto As Long
Dim hParent As Long


'*******************************************************
'* CONECTARSE A LA BASE DE DATOS                       *
'*******************************************************
strSql = "SELECT SG02PASSW FROM SG0200 WHERE SG02COD=?"

Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
Qy(0) = strCodUser
Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

ierrcode = PcsLogAlloc(hLog)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error allocating Log", vbExclamation
        LOG = False
        Exit Function
End If
'Conexion a la base de datos
ierrcode = PcsLogAttachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
If ierrcode <> 0 Then
            MsgBox "Error conectandose a la base de datos", vbExclamation
            LOG = False
            Exit Function
End If
ierrcode = PcsLogSetVirtualUserName(hLog, strCodUser)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error conectandose a la base de datos", vbExclamation
        LOG = False
        Exit Function
End If

PassDes = Desencriptar(Rs!SG02PASSW)

ierrcode = PcsLogSetVirtualPassword(hLog, PassDes) 'Rs!SG02PASSW)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error conectandose a la base de datos", vbExclamation
        LOG = False
        Exit Function
End If
'conexion virtual
ierrcode = PcsLogUseDefaultUser(hLog, False)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error usando el logon virtual", vbExclamation
        LOG = False
        Exit Function
End If
ierrcode = PcsLogLogOn(hLog)
'******************************
If ierrcode = 76 Then


ElseIf ierrcode = 77 Then       'Contrase�a incorrecta

        Call InsertarContrase�a(strCodUser, PassDes)
        
        ierrcode = PcsLogSetVirtualPassword(hLog, PassDes) 'Rs!SG02PASSW)
        If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error conectandose a la base de datos", vbExclamation
                LOG = False
                Exit Function
        End If
        'conexion virtual
        ierrcode = PcsLogUseDefaultUser(hLog, False)
        If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error usando el logon virtual", vbExclamation
                LOG = False
                Exit Function
        End If
        ierrcode = PcsLogLogOn(hLog)
        If ierrcode <> ERRCODE_ALL_OK Then
                MsgBox "Error conectandose a la base de datos", vbExclamation
                LOG = False
                Exit Function
        End If
        
Else
    If ierrcode <> ERRCODE_ALL_OK Then
       ' MsgBox "Usuario o contrase�a incorrectas para picis", vbExclamation
        LOG = False
        Exit Function
    End If
End If
'****************************************************************
ierrcode = PcsLogSetDefault(hLog)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error conectandose a la base de datos", vbExclamation
        LOG = False
        Exit Function
End If
Rs.Close
Qy.Close
LOG = True
End Function

Public Function BuscarPacienteDesdeAsistencia(codAsistencia As Long, numHistoria As Long)
Dim strSql As String
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim codPersona As Long

'Esta funci�n consigue el n�mero de historia desde el n�mero de asistencia

    strSql = "SELECT CI21CODPERSONA FROM AD0100 WHERE AD01CODASISTENCI=?"
    Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
    Qy(0) = codAsistencia
    Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    codPersona = Rs!CI21CODPERSONA
    Rs.Close
    Qy.Close
    
    strSql = "SELECT CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA=?"
    Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
    Qy(0) = codPersona
    Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    numHistoria = Rs!CI22NUMHISTORIA
    Rs.Close
    Qy.Close
End Function

Public Function BuscarAsistenciaDesdePaciente(numHistoria As Long, codAsistencia As Long)
'Esta funci�n consigue el n�mero de asistencia desde el n�mero de historia
Dim strSql As String
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim codPersona As Long
    strSql = "SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA=?"
    Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
    Qy(0) = numHistoria
    Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    codPersona = Rs!CI21CODPERSONA
    Rs.Close
    Qy.Close
    
    strSql = "SELECT AD01CODASISTENCI FROM AD0100 WHERE CI21CODPERSONA=? " & _
    "AND (AD01FECFIN IS NULL)" ' OR AD01FECFIN < SYSDATE)"
    Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
    Qy(0) = codPersona
    Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    codAsistencia = Rs!ad01codasistenci
    Rs.Close
    Qy.Close
End Function

Public Function EsPar(FechaInicio As String) As Boolean
Dim diadelmes As Integer

diadelmes = Format(FechaInicio, "dd")
If (diadelmes / 2) = Int(diadelmes / 2) Then
    EsPar = True
Else
    EsPar = False
End If
End Function

Public Function strHora_Sistema() As String
  Dim Rs As rdoResultset
  Set Rs = objApp.rdoConnect.OpenResultset("select " & _
"to_char(sysdate,'hh24:mi:ss') from dual")
  strHora_Sistema = Rs.rdoColumns(0)
  Rs.Close
  Set Rs = Nothing
End Function

Public Function strFecha_Sistema() As String
  Dim Rs As rdoResultset
  Set Rs = objApp.rdoConnect.OpenResultset("select " & _
"to_char(sysdate,'DD/MM/YYYY') from dual")
  strFecha_Sistema = Rs.rdoColumns(0)
  Rs.Close
  Set Rs = Nothing
End Function

Public Function strFechaHora_Sistema() As String
  Dim Rs As rdoResultset
  Set Rs = objApp.rdoConnect.OpenResultset("select " & _
"to_char(sysdate,'DD/MM/YYYY HH24:MI:SS') from dual")
  strFechaHora_Sistema = Rs.rdoColumns(0)
  Rs.Close
  Set Rs = Nothing
End Function

Public Function Desencriptar(texto As String) As String
Dim objRES As New clsCWMRES

    Desencriptar = objRES.DesEncript(texto)
End Function

Public Function Encriptar(texto As String) As String
Dim objRES As New clsCWMRES

    Encriptar = objRES.Encript(texto)
End Function

Public Function LOGGeneral() As Boolean
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim strSql As String
Dim Password As String
Dim PassDes As String
Dim PassEnc As String
Dim UsuarioDefecto As Long
Dim hParent As Long


'*******************************************************
'* CONECTARSE A LA BASE DE DATOS                       *
'*******************************************************
strSql = "SELECT SG02PASSW FROM SG0200 WHERE SG02COD=?"

Set Qy = objApp.rdoConnect.CreateQuery("", strSql)
Qy(0) = "NOTOC" 'Usuario generico
Set Rs = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

ierrcode = PcsLogAlloc(hLog)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error allocating Log", vbExclamation
        LOGGeneral = False
        Exit Function
End If
'Conexion a la base de datos
ierrcode = PcsLogAttachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
If ierrcode <> 0 Then
            MsgBox "Error conectandose a la base de datos", vbExclamation
            LOGGeneral = False
            Exit Function
End If
ierrcode = PcsLogSetVirtualUserName(hLog, "NOTOC")
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error conectandose a la base de datos", vbExclamation
        LOGGeneral = False
        Exit Function
End If

'Passdes = Desencriptar(rs!SG02PASSW)
ierrcode = PcsLogSetVirtualPassword(hLog, "NOTOC") 'Rs!SG02PASSW)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error conectandose a la base de datos", vbExclamation
        LOGGeneral = False
        Exit Function
End If
'conexion virtual
ierrcode = PcsLogUseDefaultUser(hLog, False)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error usando el logon virtual", vbExclamation
        LOGGeneral = False
        Exit Function
End If
ierrcode = PcsLogLogOn(hLog)
If ierrcode <> ERRCODE_ALL_OK Then
       ' MsgBox "Usuario o contrase�a incorrectas para picis", vbExclamation
        LOGGeneral = False
        Exit Function
End If
ierrcode = PcsLogSetDefault(hLog)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error conectandose a la base de datos", vbExclamation
        LOGGeneral = False
        Exit Function
End If
Rs.Close
Qy.Close
LOGGeneral = True
End Function


Public Function InsertarContrase�a(CodUser As String, PassDes As String)
Dim strSql As String
Dim Rs As rdoResultset
Dim Qy As rdoQuery
Dim condition As String
'Dim PassDes As String
Dim PassEnc As String * 255
Dim PassEncNue As String
Dim sNull As String

    ierrcode = PcsListObjectsAlloc(hList)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error"
    End If
    
    'Build the condition string
    condition = "$='" + strCodUser + "'"
    'The only condition is the Patient name, so the other fields are empty strings
    ierrcode = PcsStaffFindWithMembers(hLog, hList, sNull, sNull, sNull, sNull, sNull, condition, sNull, sNull, sNull, sNull, sNull)
    If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error"
    End If
          ierrcode = PcsListObjectsGotoHeadPosition(hList)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "ERROR"
        End If
        ierrcode = PcsStaffAlloc(hStaff)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
        ierrcode = PcsListObjectsRemoveObjectFromPosition(hList, hStaff)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If

        ierrcode = PcsStaffEncryptPassword(strCodUser, PassDes, PassEnc, 255)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
        
        PassEncNue = Mid(Trim(PassEnc), 1, InStr(PassEnc, Chr(0)) - 1)
        
        ierrcode = PcsStaffSetVirtualPassword(hStaff, PassEncNue)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
        
        ierrcode = PcsDbObjectWrite(hStaff)
        If ierrcode <> 0 Then
            MsgBox "Error al ESCRIBIR", vbExclamation
        End If
            
        ierrcode = PcsListObjectsFree(hList)
        ierrcode = PcsStaffFree(hStaff)
        If ierrcode <> ERRCODE_ALL_OK Then
            MsgBox "Error"
        End If
 

End Function

