Attribute VB_Name = "Module6"
#If DebugVersion Then

Public Declare Function PcsREnvironmentLocationAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsREnvironmentLocationFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pEnded_cond As String, _
ByVal in_pEnvironment_cond As String, ByVal in_pLocation_cond As String) As Integer
Public Declare Function PcsREnvironmentLocationFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetEnded Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetLocation Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetLocationIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsREnvironmentLocationListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsREnvironmentLocationSetEnded Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsREnvironmentLocationSetEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREnvironmentLocationSetEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREnvironmentLocationSetLocation Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREnvironmentLocationSetLocationIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREnvironmentLocationSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsREquipmentPicisDataAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsREquipmentPicisDataFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pInternalId_cond As String, ByVal in_pStarted_cond As String, _
ByVal in_pEnded_cond As String, ByVal in_pEquipment_cond As String, ByVal in_pPicisData_cond As String) As Integer
Public Declare Function PcsREquipmentPicisDataFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetEnded Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetEquipment Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetEquipmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetInternalId Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal out_pInternalId As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsREquipmentPicisDataListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsREquipmentPicisDataSetEnded Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsREquipmentPicisDataSetEquipment Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREquipmentPicisDataSetEquipmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREquipmentPicisDataSetInternalId Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_pInternalId As String) As Integer
Public Declare Function PcsREquipmentPicisDataSetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREquipmentPicisDataSetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREquipmentPicisDataSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsREventDataAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsREventDataFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pDescription_cond As String, _
ByVal in_pPicisData_cond As String, ByVal in_pEvent_cond As String, ByVal in_pStaff_cond As String) As Integer
Public Declare Function PcsREventDataFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsREventDataGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREventDataGetEvent Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREventDataGetEventIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREventDataGetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREventDataGetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREventDataGetStaff Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREventDataGetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREventDataGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsREventDataListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsREventDataSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsREventDataSetEvent Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREventDataSetEventIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREventDataSetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREventDataSetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREventDataSetStaff Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREventDataSetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREventDataSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRGroupGroupAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRGroupGroupFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIsDeleted_cond As String, ByVal in_pGroup_cond As String, _
ByVal in_pGroupSon_cond As String) As Integer
Public Declare Function PcsRGroupGroupFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRGroupGroupGetGroup Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRGroupGroupGetGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupGroupGetGroupSon Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRGroupGroupGetGroupSonIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupGroupGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupGroupListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRGroupGroupSetGroup Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRGroupGroupSetGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRGroupGroupSetGroupSon Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRGroupGroupSetGroupSonIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRGroupGroupSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsRGroupStaffAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRGroupStaffFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIsDeleted_cond As String, ByVal in_pGroup_cond As String, _
ByVal in_pStaff_cond As String) As Integer
Public Declare Function PcsRGroupStaffFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRGroupStaffGetGroup Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRGroupStaffGetGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupStaffGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupStaffGetStaff Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRGroupStaffGetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupStaffListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRGroupStaffSetGroup Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRGroupStaffSetGroupIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRGroupStaffSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsRGroupStaffSetStaff Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRGroupStaffSetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsROrderAdditiveAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsROrderAdditiveFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pHigherDose_cond As String, ByVal in_pLowerDose_cond As String, _
ByVal in_pOrder_cond As String, ByVal in_pAdditive_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsROrderAdditiveFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsROrderAdditiveGetAdditive Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveGetAdditiveIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsROrderAdditiveGetHigherDose Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsROrderAdditiveGetLowerDose Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsROrderAdditiveGetOrder Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveGetOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsROrderAdditiveGetUnit Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveGetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsROrderAdditiveListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsROrderAdditiveSetAdditive Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveSetAdditiveIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsROrderAdditiveSetHigherDose Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsROrderAdditiveSetLowerDose Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsROrderAdditiveSetOrder Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveSetOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsROrderAdditiveSetUnit Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveSetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRouteAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRouteFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String, _
ByVal in_pRouteType_cond As String) As Integer
Public Declare Function PcsRouteFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRouteGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHROUTE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRouteGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHROUTE As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRouteGetRouteType Lib "PCSDB500D.DLL" (ByVal in_hHROUTE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRouteGetRouteTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHROUTE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRouteListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRouteSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHROUTE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsRouteSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHROUTE As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsRouteSetRouteType Lib "PCSDB500D.DLL" (ByVal in_hHROUTE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRouteSetRouteTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHROUTE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRouteTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRouteTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pIsDeleted_cond As String) As Integer
Public Declare Function PcsRouteTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRouteTypeGetDescription Lib "PCSDB500D.DLL" (ByVal in_hHROUTETYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRouteTypeGetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHROUTETYPE As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRouteTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRouteTypeSetDescription Lib "PCSDB500D.DLL" (ByVal in_hHROUTETYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsRouteTypeSetIsDeleted Lib "PCSDB500D.DLL" (ByVal in_hHROUTETYPE As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsRPartComponentAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRPartComponentFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pPart_cond As String, ByVal in_pComponent_cond As String) As Integer
Public Declare Function PcsRPartComponentFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRPartComponentGetComponent Lib "PCSDB500D.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRPartComponentGetComponentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRPartComponentGetPart Lib "PCSDB500D.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRPartComponentGetPartIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRPartComponentListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRPartComponentSetComponent Lib "PCSDB500D.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRPartComponentSetComponentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRPartComponentSetPart Lib "PCSDB500D.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRPartComponentSetPartIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRPatientPrecautionAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRPatientPrecautionFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pEnded_cond As String, _
ByVal in_pPatient_cond As String, ByVal in_pPrecaution_cond As String) As Integer
Public Declare Function PcsRPatientPrecautionFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetEnded Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetPatient Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetPatientIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetPrecaution Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetPrecautionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRPatientPrecautionListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRPatientPrecautionSetEnded Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRPatientPrecautionSetPatient Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRPatientPrecautionSetPatientIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRPatientPrecautionSetPrecaution Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRPatientPrecautionSetPrecautionIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRPatientPrecautionSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, _
ByVal in_pEnvironment_cond As String, ByVal in_pStaffAttendingType_cond As String) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentGetEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentGetEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentGetStaffAttendingType Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentGetStaffAttendingTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, _
ByRef out_pday As Long, ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentSetEnvironment Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentSetEnvironmentIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentSetStaffAttendingType Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentSetStaffAttendingTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal in_year As Long, ByVal in_month As Long, _
ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStaff_cond As String, _
ByVal in_pAttendingType_cond As String) As Integer
Public Declare Function PcsRStaffAttendingTypeFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeGetAttendingType Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeGetAttendingTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeGetStaff Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeGetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeSetAttendingType Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeSetAttendingTypeIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStaffAttendingTypeSetStaff Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeSetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStdOrderAdditiveAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pHigherDose_cond As String, ByVal in_pLowerDose_cond As String, _
ByVal in_pAdditive_cond As String, ByVal in_pStdOrder_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsRStdOrderAdditiveFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetAdditive Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetAdditiveIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetHigherDose Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsRStdOrderAdditiveGetLowerDose Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsRStdOrderAdditiveGetStdOrder Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetStdOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetUnit Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveSetAdditive Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveSetAdditiveIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStdOrderAdditiveSetHigherDose Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsRStdOrderAdditiveSetLowerDose Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsRStdOrderAdditiveSetStdOrder Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveSetStdOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStdOrderAdditiveSetUnit Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveSetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStdOrderOrderSetAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIndex_cond As String, ByVal in_pOrderSet_cond As String, _
ByVal in_pStdOrder_cond As String) As Integer
Public Declare Function PcsRStdOrderOrderSetFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetGetIndex Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetGetOrderSet Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetGetOrderSetIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetGetStdOrder Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetGetStdOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetSetIndex Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetSetOrderSet Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetSetOrderSetIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStdOrderOrderSetSetStdOrder Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetSetStdOrderIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskAdditiveAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTaskAdditiveFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pValue_cond As String, ByVal in_pTask_cond As String, _
ByVal in_pAdditive_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsRTaskAdditiveFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetAdditive Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetAdditiveIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetTask Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetTaskIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetUnit Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetValue Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsRTaskAdditiveListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTaskAdditiveSetAdditive Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveSetAdditiveIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskAdditiveSetTask Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveSetTaskIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskAdditiveSetUnit Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveSetUnitIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskAdditiveSetValue Lib "PCSDB500D.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsRTaskAssessmentAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTaskAssessmentFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pTask_cond As String, _
ByVal in_pAssessmentItem_cond As String) As Integer
Public Declare Function PcsRTaskAssessmentFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTaskAssessmentGetAssessmentItem Lib "PCSDB500D.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskAssessmentGetAssessmentItemIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskAssessmentGetTask Lib "PCSDB500D.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskAssessmentGetTaskIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskAssessmentListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTaskAssessmentSetAssessmentItem Lib "PCSDB500D.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskAssessmentSetAssessmentItemIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskAssessmentSetTask Lib "PCSDB500D.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskAssessmentSetTaskIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskScoreAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTaskScoreFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pValue_cond As String, ByVal in_pTask_cond As String, _
ByVal in_pScoreItem_cond As String) As Integer
Public Declare Function PcsRTaskScoreFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTaskScoreGetScoreItem Lib "PCSDB500D.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskScoreGetScoreItemIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskScoreGetTask Lib "PCSDB500D.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskScoreGetTaskIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskScoreGetValue Lib "PCSDB500D.DLL" (ByVal in_hHRTASKSCORE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsRTaskScoreListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTaskScoreSetScoreItem Lib "PCSDB500D.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskScoreSetScoreItemIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskScoreSetTask Lib "PCSDB500D.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskScoreSetTaskIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskScoreSetValue Lib "PCSDB500D.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsRTDataAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTDataAuditedAlloc Lib "PCSDB500D.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTDataAuditedFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pMemo_cond As String, _
ByVal in_pRTData_cond As String, ByVal in_pStaff_cond As String) As Integer
Public Declare Function PcsRTDataAuditedFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTDataAuditedGetClcnLnkNValue Lib "PCSDB500D.DLL" (ByVal in_hRTDataAudited As Long, ByVal in_iClkCode As Long, ByRef out_pfClkValue As Long) As Integer
Public Declare Function PcsRTDataAuditedGetClcnLnkTValue Lib "PCSDB500D.DLL" (ByVal in_hRTDataAudited As Long, ByVal in_iClkCode As Long, ByVal out_pcClkValue As String) As Integer
Public Declare Function PcsRTDataAuditedGetMemo Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal out_pMemo As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataAuditedGetRTData Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTDataAuditedGetRTDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataAuditedGetStaff Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTDataAuditedGetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataAuditedGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRTDataAuditedInitialize Lib "PCSDB500D.DLL" (ByVal in_hRTDataAudited As Long, ByVal in_hRTData As Long) As Integer
Public Declare Function PcsRTDataAuditedListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTDataAuditedReset Lib "PCSDB500D.DLL" (ByVal in_hRTDataAudited As Long) As Integer
Public Declare Function PcsRTDataAuditedSetClcnLnkNValue Lib "PCSDB500D.DLL" (ByVal in_hRTDataAudited As Long, ByVal in_iClkCode As Long, ByVal in_fClkValue As Long) As Integer
Public Declare Function PcsRTDataAuditedSetClcnLnkTValue Lib "PCSDB500D.DLL" (ByVal in_hRTDataAudited As Long, ByVal in_iClkCode As Long, ByVal in_pcClkValue As String) As Integer
Public Declare Function PcsRTDataAuditedSetMemo Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_pMemo As String) As Integer
Public Declare Function PcsRTDataAuditedSetRTData Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTDataAuditedSetRTDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTDataAuditedSetStaff Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTDataAuditedSetStaffIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTDataAuditedSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRTDataFindWithMembers Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pValidated_cond As String, _
ByVal in_pAudited_cond As String, ByVal in_pPicisData_cond As String) As Integer
Public Declare Function PcsRTDataFindWithPicisDataAndTime Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_hpicisdata As Long, ByVal in_lYear As Long, ByVal in_lMonth As Long, ByVal in_lDay As Long, _
ByVal in_lHour As Long, ByVal in_lMinute As Long, ByVal in_lSecond As Long, ByRef out_phRTData As Long) As Integer
Public Declare Function PcsRTDataFree Lib "PCSDB500D.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTDataGetAudited Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA As Long, ByVal out_pAudited As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataGetClcnLnkNValue Lib "PCSDB500D.DLL" (ByVal in_hRTData As Long, ByVal in_iClkCode As Long, ByRef out_pfClkValue As Long) As Integer
Public Declare Function PcsRTDataGetClcnLnkTValue Lib "PCSDB500D.DLL" (ByVal in_hRTData As Long, ByVal in_iClkCode As Long, ByVal out_pcClkValue As String) As Integer
Public Declare Function PcsRTDataGetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTDataGetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataGetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRTDataGetValidated Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA As Long, ByVal out_pValidated As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataInitialize Lib "PCSDB500D.DLL" (ByVal in_hRTData As Long) As Integer
Public Declare Function PcsRTDataListRead Lib "PCSDB500D.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTDataReset Lib "PCSDB500D.DLL" (ByVal in_hRTData As Long) As Integer
Public Declare Function PcsRTDataSetAudited Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA As Long, ByVal in_pAudited As String) As Integer
Public Declare Function PcsRTDataSetClcnLnkNValue Lib "PCSDB500D.DLL" (ByVal in_hRTData As Long, ByVal in_iClkCode As Long, ByVal in_fClkValue As Long) As Integer
Public Declare Function PcsRTDataSetClcnLnkTValue Lib "PCSDB500D.DLL" (ByVal in_hRTData As Long, ByVal in_iClkCode As Long, ByVal in_pcClkValue As String) As Integer
Public Declare Function PcsRTDataSetPicisData Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTDataSetPicisDataIdentifier Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTDataSetStarted Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRTDataSetValidated Lib "PCSDB500D.DLL" (ByVal in_hHRTDATA As Long, ByVal in_pValidated As String) As Integer

#Else

Public Declare Function PcsREnvironmentLocationAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsREnvironmentLocationFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pEnded_cond As String, _
ByVal in_pEnvironment_cond As String, ByVal in_pLocation_cond As String) As Integer
Public Declare Function PcsREnvironmentLocationFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetEnded Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetEnvironment Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetLocation Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetLocationIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREnvironmentLocationGetStarted Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsREnvironmentLocationListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsREnvironmentLocationSetEnded Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsREnvironmentLocationSetEnvironment Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREnvironmentLocationSetEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREnvironmentLocationSetLocation Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREnvironmentLocationSetLocationIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREnvironmentLocationSetStarted Lib "PCSDB500.DLL" (ByVal in_hHRENVIRONMENTLOCATION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsREquipmentPicisDataAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsREquipmentPicisDataFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pInternalId_cond As String, ByVal in_pStarted_cond As String, _
ByVal in_pEnded_cond As String, ByVal in_pEquipment_cond As String, ByVal in_pPicisData_cond As String) As Integer
Public Declare Function PcsREquipmentPicisDataFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetEnded Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetEquipment Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetEquipmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetInternalId Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal out_pInternalId As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetPicisData Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREquipmentPicisDataGetStarted Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsREquipmentPicisDataListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsREquipmentPicisDataSetEnded Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsREquipmentPicisDataSetEquipment Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREquipmentPicisDataSetEquipmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREquipmentPicisDataSetInternalId Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_pInternalId As String) As Integer
Public Declare Function PcsREquipmentPicisDataSetPicisData Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREquipmentPicisDataSetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREquipmentPicisDataSetStarted Lib "PCSDB500.DLL" (ByVal in_hHREQUIPMENTPICISDATA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsREventDataAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsREventDataFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pDescription_cond As String, _
ByVal in_pPicisData_cond As String, ByVal in_pEvent_cond As String, ByVal in_pStaff_cond As String) As Integer
Public Declare Function PcsREventDataFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsREventDataGetDescription Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREventDataGetEvent Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREventDataGetEventIdentifier Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREventDataGetPicisData Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREventDataGetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREventDataGetStaff Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsREventDataGetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsREventDataGetStarted Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsREventDataListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsREventDataSetDescription Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsREventDataSetEvent Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREventDataSetEventIdentifier Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREventDataSetPicisData Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREventDataSetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREventDataSetStaff Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsREventDataSetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsREventDataSetStarted Lib "PCSDB500.DLL" (ByVal in_hHREVENTDATA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRGroupGroupAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRGroupGroupFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIsDeleted_cond As String, ByVal in_pGroup_cond As String, _
ByVal in_pGroupSon_cond As String) As Integer
Public Declare Function PcsRGroupGroupFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRGroupGroupGetGroup Lib "PCSDB500.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRGroupGroupGetGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupGroupGetGroupSon Lib "PCSDB500.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRGroupGroupGetGroupSonIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupGroupGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupGroupListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRGroupGroupSetGroup Lib "PCSDB500.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRGroupGroupSetGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRGroupGroupSetGroupSon Lib "PCSDB500.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRGroupGroupSetGroupSonIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRGroupGroupSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHRGROUPGROUP As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsRGroupStaffAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRGroupStaffFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIsDeleted_cond As String, ByVal in_pGroup_cond As String, _
ByVal in_pStaff_cond As String) As Integer
Public Declare Function PcsRGroupStaffFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRGroupStaffGetGroup Lib "PCSDB500.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRGroupStaffGetGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupStaffGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupStaffGetStaff Lib "PCSDB500.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRGroupStaffGetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRGroupStaffListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRGroupStaffSetGroup Lib "PCSDB500.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRGroupStaffSetGroupIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRGroupStaffSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsRGroupStaffSetStaff Lib "PCSDB500.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRGroupStaffSetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRGROUPSTAFF As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsROrderAdditiveAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsROrderAdditiveFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pHigherDose_cond As String, ByVal in_pLowerDose_cond As String, _
ByVal in_pOrder_cond As String, ByVal in_pAdditive_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsROrderAdditiveFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsROrderAdditiveGetAdditive Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveGetAdditiveIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsROrderAdditiveGetHigherDose Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsROrderAdditiveGetLowerDose Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsROrderAdditiveGetOrder Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveGetOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsROrderAdditiveGetUnit Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveGetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsROrderAdditiveListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsROrderAdditiveSetAdditive Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveSetAdditiveIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsROrderAdditiveSetHigherDose Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsROrderAdditiveSetLowerDose Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsROrderAdditiveSetOrder Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveSetOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsROrderAdditiveSetUnit Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsROrderAdditiveSetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRouteAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRouteFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, ByVal in_pIsDeleted_cond As String, _
ByVal in_pRouteType_cond As String) As Integer
Public Declare Function PcsRouteFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRouteGetDescription Lib "PCSDB500.DLL" (ByVal in_hHROUTE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRouteGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHROUTE As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRouteGetRouteType Lib "PCSDB500.DLL" (ByVal in_hHROUTE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRouteGetRouteTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHROUTE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRouteListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRouteSetDescription Lib "PCSDB500.DLL" (ByVal in_hHROUTE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsRouteSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHROUTE As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsRouteSetRouteType Lib "PCSDB500.DLL" (ByVal in_hHROUTE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRouteSetRouteTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHROUTE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRouteTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRouteTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pDescription_cond As String, _
ByVal in_pIsDeleted_cond As String) As Integer
Public Declare Function PcsRouteTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRouteTypeGetDescription Lib "PCSDB500.DLL" (ByVal in_hHROUTETYPE As Long, ByVal out_pDescription As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRouteTypeGetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHROUTETYPE As Long, ByVal out_pIsDeleted As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRouteTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRouteTypeSetDescription Lib "PCSDB500.DLL" (ByVal in_hHROUTETYPE As Long, ByVal in_pDescription As String) As Integer
Public Declare Function PcsRouteTypeSetIsDeleted Lib "PCSDB500.DLL" (ByVal in_hHROUTETYPE As Long, ByVal in_pIsDeleted As String) As Integer
Public Declare Function PcsRPartComponentAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRPartComponentFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pPart_cond As String, ByVal in_pComponent_cond As String) As Integer
Public Declare Function PcsRPartComponentFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRPartComponentGetComponent Lib "PCSDB500.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRPartComponentGetComponentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRPartComponentGetPart Lib "PCSDB500.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRPartComponentGetPartIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRPartComponentListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRPartComponentSetComponent Lib "PCSDB500.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRPartComponentSetComponentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRPartComponentSetPart Lib "PCSDB500.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRPartComponentSetPartIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRPARTCOMPONENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRPatientPrecautionAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRPatientPrecautionFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pEnded_cond As String, _
ByVal in_pPatient_cond As String, ByVal in_pPrecaution_cond As String) As Integer
Public Declare Function PcsRPatientPrecautionFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetEnded Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetPatient Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetPatientIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetPrecaution Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetPrecautionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRPatientPrecautionGetStarted Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRPatientPrecautionListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRPatientPrecautionSetEnded Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRPatientPrecautionSetPatient Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRPatientPrecautionSetPatientIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRPatientPrecautionSetPrecaution Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRPatientPrecautionSetPrecautionIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRPatientPrecautionSetStarted Lib "PCSDB500.DLL" (ByVal in_hHRPATIENTPRECAUTION As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, _
ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, _
ByVal in_pEnvironment_cond As String, ByVal in_pStaffAttendingType_cond As String) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentGetEnvironment Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentGetEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentGetStaffAttendingType Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentGetStaffAttendingTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal out_pdboid As String, _
ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentGetStarted Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, _
ByRef out_pday As Long, ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentSetEnvironment Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentSetEnvironmentIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentSetStaffAttendingType Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentSetStaffAttendingTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStaffAttendingTypeEnvironmentSetStarted Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPEENVIRONMENT As Long, ByVal in_year As Long, ByVal in_month As Long, _
ByVal in_day As Long, ByVal in_hour As Long, ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStaff_cond As String, _
ByVal in_pAttendingType_cond As String) As Integer
Public Declare Function PcsRStaffAttendingTypeFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeGetAttendingType Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeGetAttendingTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeGetStaff Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeGetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeSetAttendingType Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeSetAttendingTypeIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStaffAttendingTypeSetStaff Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStaffAttendingTypeSetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTAFFATTENDINGTYPE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStdOrderAdditiveAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pHigherDose_cond As String, ByVal in_pLowerDose_cond As String, _
ByVal in_pAdditive_cond As String, ByVal in_pStdOrder_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsRStdOrderAdditiveFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetAdditive Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetAdditiveIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetHigherDose Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsRStdOrderAdditiveGetLowerDose Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsRStdOrderAdditiveGetStdOrder Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetStdOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetUnit Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveGetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveSetAdditive Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveSetAdditiveIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStdOrderAdditiveSetHigherDose Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsRStdOrderAdditiveSetLowerDose Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsRStdOrderAdditiveSetStdOrder Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveSetStdOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStdOrderAdditiveSetUnit Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStdOrderAdditiveSetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStdOrderOrderSetAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pIndex_cond As String, ByVal in_pOrderSet_cond As String, _
ByVal in_pStdOrder_cond As String) As Integer
Public Declare Function PcsRStdOrderOrderSetFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetGetIndex Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByRef out_plong As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetGetOrderSet Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetGetOrderSetIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetGetStdOrder Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetGetStdOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetSetIndex Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal in_long As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetSetOrderSet Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetSetOrderSetIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRStdOrderOrderSetSetStdOrder Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRStdOrderOrderSetSetStdOrderIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRSTDORDERORDERSET As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskAdditiveAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTaskAdditiveFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pValue_cond As String, ByVal in_pTask_cond As String, _
ByVal in_pAdditive_cond As String, ByVal in_pUnit_cond As String) As Integer
Public Declare Function PcsRTaskAdditiveFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetAdditive Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetAdditiveIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetTask Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetTaskIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetUnit Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskAdditiveGetValue Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsRTaskAdditiveListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTaskAdditiveSetAdditive Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveSetAdditiveIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskAdditiveSetTask Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveSetTaskIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskAdditiveSetUnit Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskAdditiveSetUnitIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskAdditiveSetValue Lib "PCSDB500.DLL" (ByVal in_hHRTASKADDITIVE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsRTaskAssessmentAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTaskAssessmentFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pTask_cond As String, _
ByVal in_pAssessmentItem_cond As String) As Integer
Public Declare Function PcsRTaskAssessmentFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTaskAssessmentGetAssessmentItem Lib "PCSDB500.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskAssessmentGetAssessmentItemIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskAssessmentGetTask Lib "PCSDB500.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskAssessmentGetTaskIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskAssessmentListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTaskAssessmentSetAssessmentItem Lib "PCSDB500.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskAssessmentSetAssessmentItemIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskAssessmentSetTask Lib "PCSDB500.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskAssessmentSetTaskIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKASSESSMENT As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskScoreAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTaskScoreFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pValue_cond As String, ByVal in_pTask_cond As String, _
ByVal in_pScoreItem_cond As String) As Integer
Public Declare Function PcsRTaskScoreFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTaskScoreGetScoreItem Lib "PCSDB500.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskScoreGetScoreItemIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskScoreGetTask Lib "PCSDB500.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTaskScoreGetTaskIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTaskScoreGetValue Lib "PCSDB500.DLL" (ByVal in_hHRTASKSCORE As Long, ByRef out_pdouble As Double) As Integer
Public Declare Function PcsRTaskScoreListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTaskScoreSetScoreItem Lib "PCSDB500.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskScoreSetScoreItemIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskScoreSetTask Lib "PCSDB500.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTaskScoreSetTaskIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTaskScoreSetValue Lib "PCSDB500.DLL" (ByVal in_hHRTASKSCORE As Long, ByVal in_double As Double) As Integer
Public Declare Function PcsRTDataAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTDataAuditedAlloc Lib "PCSDB500.DLL" (ByRef out_handle As Long) As Integer
Public Declare Function PcsRTDataAuditedFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pMemo_cond As String, _
ByVal in_pRTData_cond As String, ByVal in_pStaff_cond As String) As Integer
Public Declare Function PcsRTDataAuditedFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTDataAuditedGetClcnLnkNValue Lib "PCSDB500.DLL" (ByVal in_hRTDataAudited As Long, ByVal in_iClkCode As Long, ByRef out_pfClkValue As Long) As Integer
Public Declare Function PcsRTDataAuditedGetClcnLnkTValue Lib "PCSDB500.DLL" (ByVal in_hRTDataAudited As Long, ByVal in_iClkCode As Long, ByVal out_pcClkValue As String) As Integer
Public Declare Function PcsRTDataAuditedGetMemo Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal out_pMemo As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataAuditedGetRTData Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTDataAuditedGetRTDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataAuditedGetStaff Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTDataAuditedGetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataAuditedGetStarted Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, _
ByRef out_phour As Long, ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRTDataAuditedInitialize Lib "PCSDB500.DLL" (ByVal in_hRTDataAudited As Long, ByVal in_hRTData As Long) As Integer
Public Declare Function PcsRTDataAuditedListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, _
ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTDataAuditedReset Lib "PCSDB500.DLL" (ByVal in_hRTDataAudited As Long) As Integer
Public Declare Function PcsRTDataAuditedSetClcnLnkNValue Lib "PCSDB500.DLL" (ByVal in_hRTDataAudited As Long, ByVal in_iClkCode As Long, ByVal in_fClkValue As Long) As Integer
Public Declare Function PcsRTDataAuditedSetClcnLnkTValue Lib "PCSDB500.DLL" (ByVal in_hRTDataAudited As Long, ByVal in_iClkCode As Long, ByVal in_pcClkValue As String) As Integer
Public Declare Function PcsRTDataAuditedSetMemo Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_pMemo As String) As Integer
Public Declare Function PcsRTDataAuditedSetRTData Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTDataAuditedSetRTDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTDataAuditedSetStaff Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTDataAuditedSetStaffIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTDataAuditedSetStarted Lib "PCSDB500.DLL" (ByVal in_hHRTDATA_AUDITED As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRTDataFindWithMembers Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal out_hlist As Long, ByVal in_pStarted_cond As String, ByVal in_pValidated_cond As String, _
ByVal in_pAudited_cond As String, ByVal in_pPicisData_cond As String) As Integer
Public Declare Function PcsRTDataFindWithPicisDataAndTime Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_hpicisdata As Long, ByVal in_lYear As Long, ByVal in_lMonth As Long, ByVal in_lDay As Long, _
ByVal in_lHour As Long, ByVal in_lMinute As Long, ByVal in_lSecond As Long, ByRef out_phRTData As Long) As Integer
Public Declare Function PcsRTDataFree Lib "PCSDB500.DLL" (ByVal in_handle As Long) As Integer
Public Declare Function PcsRTDataGetAudited Lib "PCSDB500.DLL" (ByVal in_hHRTDATA As Long, ByVal out_pAudited As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataGetClcnLnkNValue Lib "PCSDB500.DLL" (ByVal in_hRTData As Long, ByVal in_iClkCode As Long, ByRef out_pfClkValue As Long) As Integer
Public Declare Function PcsRTDataGetClcnLnkTValue Lib "PCSDB500.DLL" (ByVal in_hRTData As Long, ByVal in_iClkCode As Long, ByVal out_pcClkValue As String) As Integer
Public Declare Function PcsRTDataGetPicisData Lib "PCSDB500.DLL" (ByVal in_hHRTDATA As Long, ByVal out_hreference As Long) As Integer
Public Declare Function PcsRTDataGetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTDATA As Long, ByVal out_pdboid As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataGetStarted Lib "PCSDB500.DLL" (ByVal in_hHRTDATA As Long, ByRef out_pyear As Long, ByRef out_pmonth As Long, ByRef out_pday As Long, ByRef out_phour As Long, _
ByRef out_pminute As Long, ByRef out_psecond As Long) As Integer
Public Declare Function PcsRTDataGetValidated Lib "PCSDB500.DLL" (ByVal in_hHRTDATA As Long, ByVal out_pValidated As String, ByVal in_num_max As Long) As Integer
Public Declare Function PcsRTDataInitialize Lib "PCSDB500.DLL" (ByVal in_hRTData As Long) As Integer
Public Declare Function PcsRTDataListRead Lib "PCSDB500.DLL" (ByVal in_hlog As Long, ByVal in_Select As String, ByVal in_From As String, ByVal in_Where As String, ByRef out_phlist As Long) As Integer
Public Declare Function PcsRTDataReset Lib "PCSDB500.DLL" (ByVal in_hRTData As Long) As Integer
Public Declare Function PcsRTDataSetAudited Lib "PCSDB500.DLL" (ByVal in_hHRTDATA As Long, ByVal in_pAudited As String) As Integer
Public Declare Function PcsRTDataSetClcnLnkNValue Lib "PCSDB500.DLL" (ByVal in_hRTData As Long, ByVal in_iClkCode As Long, ByVal in_fClkValue As Long) As Integer
Public Declare Function PcsRTDataSetClcnLnkTValue Lib "PCSDB500.DLL" (ByVal in_hRTData As Long, ByVal in_iClkCode As Long, ByVal in_pcClkValue As String) As Integer
Public Declare Function PcsRTDataSetPicisData Lib "PCSDB500.DLL" (ByVal in_hHRTDATA As Long, ByVal in_hreference As Long) As Integer
Public Declare Function PcsRTDataSetPicisDataIdentifier Lib "PCSDB500.DLL" (ByVal in_hHRTDATA As Long, ByVal in_pdboid As String) As Integer
Public Declare Function PcsRTDataSetStarted Lib "PCSDB500.DLL" (ByVal in_hHRTDATA As Long, ByVal in_year As Long, ByVal in_month As Long, ByVal in_day As Long, ByVal in_hour As Long, _
ByVal in_minute As Long, ByVal in_second As Long) As Integer
Public Declare Function PcsRTDataSetValidated Lib "PCSDB500.DLL" (ByVal in_hHRTDATA As Long, ByVal in_pValidated As String) As Integer

#End If
