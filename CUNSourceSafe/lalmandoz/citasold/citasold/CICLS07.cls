VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsFasesActuacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public intKeyFase As Integer
Public intNumFase As Integer
Public strDesFase As String
Public datFechaFase As Date
Public intNumDiasPac As Integer
Public intNumHoraPac As Integer
Public intNumMinPac As Integer

Public cllRecursosFase   As New Collection

Public Function AddNewRecurso(Optional intKey As Integer = 0) As clsRecursoFase
  Dim objRecurso     As New clsRecursoFase
  
  objRecurso.intKeyRec = intKey
  Call Me.cllRecursosFase.Add(objRecurso, Str(intKey))
  Set AddNewRecurso = Me.cllRecursosFase(Str(intKey))
  Set objRecurso = Nothing
End Function
Public Function GetRecurso(ByVal intKey As Integer) As clsRecursoFase

  Dim objRecurso        As New clsRecursoFase
  Dim blnExistRecurso   As Boolean

  blnExistRecurso = False
  For Each objRecurso In Me.cllRecursosFase
    If objRecurso.intKeyRec = intKey Then
      blnExistRecurso = True
      Exit For
    End If
  Next
  If blnExistRecurso Then
    Set GetRecurso = objRecurso
  Else
     'GetRecurso = Nothing
  End If
  Set objRecurso = Nothing

End Function

