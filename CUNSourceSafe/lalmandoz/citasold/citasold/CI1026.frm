VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "comct232.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmSolicitud 
   Caption         =   "CITAS. Solicitud de Cita"
   ClientHeight    =   8385
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11640
   ControlBox      =   0   'False
   HelpContextID   =   145
   Icon            =   "CI1026.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8385
   ScaleWidth      =   11640
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCitar 
      Caption         =   " C&itar sin Planificar"
      Height          =   725
      Index           =   0
      Left            =   2850
      Picture         =   "CI1026.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   7530
      Width           =   1410
   End
   Begin VB.CommandButton cmdRecordatorio 
      Caption         =   "&Emitir Recordatorio"
      Height          =   725
      Index           =   0
      Left            =   8660
      Picture         =   "CI1026.frx":0316
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   7530
      Width           =   1350
   End
   Begin VB.CommandButton cmdAgenda 
      Caption         =   "A&genda Recurso"
      Height          =   725
      Index           =   0
      Left            =   4320
      Picture         =   "CI1026.frx":06A0
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   7530
      Width           =   1350
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Solicitud N� "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5535
      Index           =   2
      Left            =   0
      TabIndex        =   58
      Top             =   0
      Width           =   11620
      Begin TabDlg.SSTab TabSSTab1 
         Height          =   5070
         Index           =   0
         Left            =   75
         TabIndex        =   59
         Top             =   360
         Width           =   11445
         _ExtentX        =   20188
         _ExtentY        =   8943
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "&Actuaciones"
         TabPicture(0)   =   "CI1026.frx":09E2
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "grdSSDBGrid1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "S&oluciones"
         TabPicture(1)   =   "CI1026.frx":09FE
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdSSDBGrid1(1)"
         Tab(1).Control(1)=   "SSMonth1(0)"
         Tab(1).Control(2)=   "frmFrame1"
         Tab(1).ControlCount=   3
         Begin VB.Frame frmFrame1 
            Height          =   1095
            Left            =   -74850
            TabIndex        =   85
            Top             =   3405
            Width           =   2055
            Begin VB.OptionButton OptOption1 
               Caption         =   "Soluci�n N�"
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   37
               Top             =   600
               Value           =   -1  'True
               Width           =   1215
            End
            Begin VB.TextBox txtText1 
               Height          =   285
               Index           =   7
               Left            =   1320
               Locked          =   -1  'True
               TabIndex        =   38
               Top             =   600
               Width           =   375
            End
            Begin VB.OptionButton OptOption1 
               Caption         =   "Asignado"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   36
               Top             =   240
               Width           =   1215
            End
            Begin ComCtl2.UpDown UpDown2 
               Height          =   300
               Left            =   1680
               TabIndex        =   39
               Top             =   600
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   529
               _Version        =   327681
               Max             =   0
               Enabled         =   -1  'True
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Fecha"
            Height          =   960
            Index           =   9
            Left            =   -74820
            TabIndex        =   80
            Top             =   405
            Width           =   3840
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "hiredate"
               Height          =   330
               Index           =   4
               Left            =   135
               TabIndex        =   81
               Tag             =   "Fecha de Alta"
               Top             =   495
               WhatsThisHelpID =   10013
               Width           =   1680
               _Version        =   65537
               _ExtentX        =   2963
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "hiredate"
               Height          =   330
               Index           =   5
               Left            =   1980
               TabIndex        =   82
               Tag             =   "Fecha de Alta"
               Top             =   495
               WhatsThisHelpID =   10013
               Width           =   1680
               _Version        =   65537
               _ExtentX        =   2963
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Hasta"
               Height          =   195
               Index           =   22
               Left            =   2025
               TabIndex        =   84
               Top             =   270
               Width           =   420
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Desde"
               Height          =   195
               Index           =   23
               Left            =   135
               TabIndex        =   83
               Top             =   270
               Width           =   465
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Dias Semana"
            Height          =   960
            Index           =   10
            Left            =   -74820
            TabIndex        =   65
            Top             =   1485
            Width           =   3840
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   14
               Left            =   250
               TabIndex        =   72
               Top             =   600
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   15
               Left            =   750
               TabIndex        =   71
               Top             =   600
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   16
               Left            =   1250
               TabIndex        =   70
               Top             =   600
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   17
               Left            =   1750
               TabIndex        =   69
               Top             =   600
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   18
               Left            =   2250
               TabIndex        =   68
               Top             =   600
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   19
               Left            =   2750
               TabIndex        =   67
               Top             =   600
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   20
               Left            =   3250
               TabIndex        =   66
               Top             =   600
               Width           =   240
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "L"
               Height          =   195
               Index           =   24
               Left            =   315
               TabIndex        =   79
               Top             =   325
               Width           =   90
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "M"
               Height          =   195
               Index           =   25
               Left            =   815
               TabIndex        =   78
               Top             =   325
               Width           =   135
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "X"
               Height          =   195
               Index           =   26
               Left            =   1315
               TabIndex        =   77
               Top             =   325
               Width           =   105
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "J"
               Height          =   195
               Index           =   27
               Left            =   1815
               TabIndex        =   76
               Top             =   325
               Width           =   75
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "V"
               Height          =   195
               Index           =   28
               Left            =   2315
               TabIndex        =   75
               Top             =   325
               Width           =   105
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "S"
               Height          =   195
               Index           =   29
               Left            =   2815
               TabIndex        =   74
               Top             =   325
               Width           =   105
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "D"
               Height          =   195
               Index           =   30
               Left            =   3315
               TabIndex        =   73
               Top             =   325
               Width           =   120
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Hora"
            Height          =   960
            Index           =   11
            Left            =   -70905
            TabIndex        =   60
            Top             =   405
            Width           =   2130
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "DEPTNO"
               Height          =   330
               Index           =   4
               Left            =   90
               TabIndex        =   62
               Tag             =   "C�digo Departamento|Departamento"
               Top             =   495
               WhatsThisHelpID =   10007
               Width           =   885
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "DEPTNO"
               Height          =   330
               Index           =   5
               Left            =   1080
               TabIndex        =   61
               Tag             =   "C�digo Departamento|Departamento"
               Top             =   495
               WhatsThisHelpID =   10007
               Width           =   885
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Desde"
               Height          =   195
               Index           =   31
               Left            =   90
               TabIndex        =   64
               Top             =   270
               WhatsThisHelpID =   10021
               Width           =   465
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Hasta"
               Height          =   195
               Index           =   32
               Left            =   1125
               TabIndex        =   63
               Top             =   270
               WhatsThisHelpID =   10021
               Width           =   420
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdSSDBGrid1 
            Height          =   4470
            Index           =   0
            Left            =   120
            TabIndex        =   86
            Top             =   480
            Width           =   11265
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   6
            stylesets.count =   4
            stylesets(0).Name=   "styleRowSelected"
            stylesets(0).ForeColor=   16777215
            stylesets(0).BackColor=   8388608
            stylesets(0).Picture=   "CI1026.frx":0A1A
            stylesets(1).Name=   "StyleNormal"
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "CI1026.frx":0A36
            stylesets(2).Name=   "StyleRowHead"
            stylesets(2).ForeColor=   0
            stylesets(2).BackColor=   12632256
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "CI1026.frx":0A52
            stylesets(3).Name=   "StyleCabecera"
            stylesets(3).ForeColor=   0
            stylesets(3).BackColor=   12632256
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "CI1026.frx":0A6E
            stylesets(3).AlignmentPicture=   2
            DividerType     =   1
            BevelColorFrame =   8421504
            BevelColorShadow=   8421504
            RowSelectionStyle=   1
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeRow   =   1
            SelectByCell    =   -1  'True
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ActiveRowStyleSet=   "styleRowSelected"
            Columns.Count   =   6
            Columns(0).Width=   3704
            Columns(0).Caption=   "Paciente"
            Columns(0).Name =   "Paciente"
            Columns(0).CaptionAlignment=   2
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   8454143
            Columns(0).HeadStyleSet=   "StyleCabecera"
            Columns(0).StyleSet=   "StyleNormal"
            Columns(1).Width=   3704
            Columns(1).Caption=   "Actuaci�n"
            Columns(1).Name =   "Actuacion"
            Columns(1).CaptionAlignment=   2
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   8454143
            Columns(1).HeadStyleSet=   "StyleCabecera"
            Columns(1).StyleSet=   "StyleNormal"
            Columns(2).Width=   1402
            Columns(2).Name =   "Cabecera"
            Columns(2).Alignment=   2
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(2).Style=   4
            Columns(2).ButtonsAlways=   -1  'True
            Columns(2).HasBackColor=   -1  'True
            Columns(2).BackColor=   12632256
            Columns(3).Width=   3493
            Columns(3).Caption=   "Asignado"
            Columns(3).Name =   "Asignado"
            Columns(3).CaptionAlignment=   2
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(3).Style=   3
            Columns(3).HasBackColor=   -1  'True
            Columns(3).BackColor=   16776960
            Columns(3).HeadStyleSet=   "StyleCabecera"
            Columns(3).StyleSet=   "StyleNormal"
            Columns(4).Width=   3493
            Columns(4).Caption=   "Preferencias"
            Columns(4).Name =   "Preferencias"
            Columns(4).CaptionAlignment=   2
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Style=   3
            Columns(4).ButtonsAlways=   -1  'True
            Columns(4).HasBackColor=   -1  'True
            Columns(4).BackColor=   16777215
            Columns(4).HeadStyleSet=   "StyleCabecera"
            Columns(4).StyleSet=   "StyleNormal"
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "Indice"
            Columns(5).Name =   "Indice"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            _ExtentX        =   19870
            _ExtentY        =   7885
            _StockProps     =   79
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSMonth SSMonth1 
            Height          =   2415
            Index           =   0
            Left            =   -74865
            TabIndex        =   87
            Top             =   450
            Width           =   11175
            _Version        =   65537
            _ExtentX        =   19711
            _ExtentY        =   4260
            _StockProps     =   76
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DayName(1)      =   "D"
            DayName(2)      =   "L"
            DayName(3)      =   "M"
            DayName(4)      =   "X"
            DayName(5)      =   "J"
            DayName(6)      =   "V"
            DayName(7)      =   "S"
            MonthName(1)    =   "ENERO"
            MonthName(2)    =   "FEBRERO"
            MonthName(3)    =   "MARZO"
            MonthName(4)    =   "ABRIL"
            MonthName(5)    =   "MAYO"
            MonthName(6)    =   "JUNIO"
            MonthName(7)    =   "JULIO"
            MonthName(8)    =   "AGOSTO"
            MonthName(9)    =   "SEPTIEMBRE"
            MonthName(10)   =   "OCTUBRE"
            MonthName(11)   =   "NOVIEMBRE"
            MonthName(12)   =   "DICIEMBRE"
            DefaultDate     =   ""
            stylesets.count =   3
            stylesets(0).Name=   "styleDaySelected"
            stylesets(0).ForeColor=   0
            stylesets(0).Picture=   "CI1026.frx":0A8A
            stylesets(1).Name=   "styleDomingo"
            stylesets(1).ForeColor=   255
            stylesets(1).Picture=   "CI1026.frx":0AA6
            stylesets(2).Name=   "stylecaption"
            stylesets(2).ForeColor=   8388608
            stylesets(2).Picture=   "CI1026.frx":0AC2
            DayOfWeek(1).StyleSet=   "styleDomingo"
            StartofWeek     =   2
            Font3D          =   3
            DayCaptionAlignment=   2
            DayNumberAlignment=   3
            NumberOfMonths  =   3
            ShowSelectedDate=   0   'False
            ShowTodaysDate  =   0   'False
            AllowNullDate   =   -1  'True
            SelectionType   =   1
            AutoRestore     =   0   'False
         End
         Begin SSDataWidgets_B.SSDBGrid grdSSDBGrid1 
            Height          =   1770
            Index           =   1
            Left            =   -72720
            TabIndex        =   40
            Top             =   3060
            Width           =   9015
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   11
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   11
            Columns(0).Width=   1508
            Columns(0).Caption=   "Fecha"
            Columns(0).Name =   "Fecha"
            Columns(0).Alignment=   2
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(1).Width=   1085
            Columns(1).Caption=   "Hora"
            Columns(1).Name =   "Hora"
            Columns(1).Alignment=   2
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   4339
            Columns(2).Caption=   "Paciente"
            Columns(2).Name =   "Paciente"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   5450
            Columns(3).Caption=   "Actuaci�n"
            Columns(3).Name =   "Actuaci�n"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(4).Width=   4233
            Columns(4).Caption=   "Recurso"
            Columns(4).Name =   "Recurso"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Locked=   -1  'True
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "NumActPlan"
            Columns(5).Name =   "NumActPlan"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Locked=   -1  'True
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "NumActPedi"
            Columns(6).Name =   "NumActPedi"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).Locked=   -1  'True
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "NumSolicit"
            Columns(7).Name =   "NumSolicit"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(7).Locked=   -1  'True
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "Numcita"
            Columns(8).Name =   "Numcita"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(8).Locked=   -1  'True
            Columns(9).Width=   1931
            Columns(9).Caption=   "Recordatorio"
            Columns(9).Name =   "Recordatorio"
            Columns(9).Alignment=   2
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   11
            Columns(9).FieldLen=   256
            Columns(9).Style=   2
            Columns(10).Width=   1931
            Columns(10).Caption=   "Lista Espera"
            Columns(10).Name=   "Lista Espera"
            Columns(10).Alignment=   2
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   11
            Columns(10).FieldLen=   256
            Columns(10).Style=   2
            _ExtentX        =   15901
            _ExtentY        =   3122
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Image Image1 
         Appearance      =   0  'Flat
         Height          =   255
         Left            =   11040
         Top             =   120
         Width           =   495
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   725
      Index           =   0
      Left            =   10270
      Picture         =   "CI1026.frx":0ADE
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   7530
      Width           =   1350
   End
   Begin VB.CommandButton cmdConfirmarSolicitud 
      Caption         =   "&Confirmar Citas"
      Height          =   725
      Index           =   0
      Left            =   1425
      Picture         =   "CI1026.frx":0D0E
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   7530
      Width           =   1350
   End
   Begin VB.CommandButton cmdPedirSoluciones 
      Caption         =   "&Pedir Soluciones"
      Height          =   725
      Index           =   0
      Left            =   0
      Picture         =   "CI1026.frx":1158
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   7530
      Width           =   1350
   End
   Begin VB.CommandButton cmdDietario 
      Caption         =   "&Dietario  Recurso"
      Height          =   725
      Index           =   0
      Left            =   5750
      Picture         =   "CI1026.frx":1D9A
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   7530
      Width           =   1350
   End
   Begin VB.CommandButton cmdVerFases 
      Caption         =   "&Fases Recursos"
      Height          =   725
      Index           =   0
      Left            =   7205
      Picture         =   "CI1026.frx":200C
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   7530
      Width           =   1350
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Agenda"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Index           =   1
      Left            =   0
      TabIndex        =   41
      Top             =   5535
      Width           =   11620
      Begin TabDlg.SSTab TabSSTab1 
         Height          =   1485
         Index           =   1
         Left            =   135
         TabIndex        =   42
         Top             =   360
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   2619
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "&Restricciones"
         TabPicture(0)   =   "CI1026.frx":222E
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label2(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "UpDown1"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "fraFrame1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "fraFrame1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "fraFrame1(4)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "&Preferencias"
         TabPicture(1)   =   "CI1026.frx":224A
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraFrame1(7)"
         Tab(1).Control(1)=   "fraFrame1(6)"
         Tab(1).Control(2)=   "fraFrame1(0)"
         Tab(1).ControlCount=   3
         Begin VB.Frame fraFrame1 
            Caption         =   "Fecha"
            Height          =   960
            Index           =   7
            Left            =   -74880
            TabIndex        =   99
            Top             =   360
            Width           =   3690
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "hiredate"
               Height          =   330
               Index           =   2
               Left            =   120
               TabIndex        =   23
               Tag             =   "Fecha de Alta"
               Top             =   480
               WhatsThisHelpID =   10013
               Width           =   1650
               _Version        =   65537
               _ExtentX        =   2910
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "Pr09FecPeticion"
               Height          =   330
               Index           =   3
               Left            =   1920
               TabIndex        =   24
               Tag             =   "Fecha Petici�n"
               Top             =   480
               Width           =   1650
               _Version        =   65537
               _ExtentX        =   2910
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Desde"
               Height          =   195
               Index           =   21
               Left            =   120
               TabIndex        =   101
               Top             =   240
               Width           =   465
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Hasta"
               Height          =   195
               Index           =   20
               Left            =   1920
               TabIndex        =   100
               Top             =   240
               Width           =   420
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Dias Semana"
            Height          =   960
            Index           =   6
            Left            =   -68880
            TabIndex        =   91
            Top             =   360
            Width           =   3570
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   13
               Left            =   3165
               TabIndex        =   35
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   12
               Left            =   2655
               TabIndex        =   34
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   11
               Left            =   2160
               TabIndex        =   33
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   10
               Left            =   1665
               TabIndex        =   32
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   9
               Left            =   1155
               TabIndex        =   31
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   8
               Left            =   660
               TabIndex        =   30
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   7
               Left            =   165
               TabIndex        =   29
               Top             =   600
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "D"
               Height          =   195
               Index           =   19
               Left            =   3225
               TabIndex        =   98
               Top             =   300
               Width           =   120
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "S"
               Height          =   195
               Index           =   18
               Left            =   2730
               TabIndex        =   97
               Top             =   300
               Width           =   105
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "V"
               Height          =   195
               Index           =   17
               Left            =   2220
               TabIndex        =   96
               Top             =   300
               Width           =   105
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "J"
               Height          =   195
               Index           =   16
               Left            =   1725
               TabIndex        =   95
               Top             =   300
               Width           =   75
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "X"
               Height          =   195
               Index           =   15
               Left            =   1230
               TabIndex        =   94
               Top             =   300
               Width           =   105
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "M"
               Height          =   195
               Index           =   14
               Left            =   720
               TabIndex        =   93
               Top             =   300
               Width           =   135
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "L"
               Height          =   195
               Index           =   13
               Left            =   225
               TabIndex        =   92
               Top             =   300
               Width           =   90
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Hora"
            Height          =   960
            Index           =   0
            Left            =   -71040
            TabIndex        =   88
            Top             =   360
            Width           =   1995
            Begin ComCtl2.UpDown UpDown3 
               Height          =   330
               Index           =   2
               Left            =   720
               TabIndex        =   26
               TabStop         =   0   'False
               Top             =   480
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   582
               _Version        =   327681
               Increment       =   0
               Max             =   1440
               Enabled         =   -1  'True
            End
            Begin MSMask.MaskEdBox MaskEdBox1 
               Height          =   330
               Index           =   2
               Left            =   120
               TabIndex        =   25
               Top             =   480
               Width           =   615
               _ExtentX        =   1085
               _ExtentY        =   582
               _Version        =   327681
               MaxLength       =   5
               Format          =   "hh:mm"
               Mask            =   "##:##"
               PromptChar      =   "_"
            End
            Begin MSMask.MaskEdBox MaskEdBox1 
               Height          =   330
               Index           =   3
               Left            =   1080
               TabIndex        =   27
               Top             =   480
               Width           =   615
               _ExtentX        =   1085
               _ExtentY        =   582
               _Version        =   327681
               MaxLength       =   5
               Format          =   "hh:mm"
               Mask            =   "##:##"
               PromptChar      =   "_"
            End
            Begin ComCtl2.UpDown UpDown3 
               Height          =   330
               Index           =   3
               Left            =   1680
               TabIndex        =   28
               TabStop         =   0   'False
               Top             =   480
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   582
               _Version        =   327681
               Increment       =   0
               Max             =   1440
               Enabled         =   -1  'True
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Hasta"
               Height          =   195
               Index           =   12
               Left            =   1080
               TabIndex        =   90
               Top             =   240
               WhatsThisHelpID =   10021
               Width           =   420
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Desde"
               Height          =   195
               Index           =   11
               Left            =   120
               TabIndex        =   89
               Top             =   240
               WhatsThisHelpID =   10021
               Width           =   465
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Hora"
            Height          =   960
            Index           =   4
            Left            =   3960
            TabIndex        =   54
            Top             =   360
            Width           =   1995
            Begin ComCtl2.UpDown UpDown3 
               Height          =   330
               Index           =   0
               Left            =   720
               TabIndex        =   3
               TabStop         =   0   'False
               Top             =   480
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   582
               _Version        =   327681
               OrigLeft        =   720
               OrigTop         =   480
               OrigRight       =   960
               OrigBottom      =   810
               Increment       =   0
               Max             =   1440
               Enabled         =   -1  'True
            End
            Begin MSMask.MaskEdBox MaskEdBox1 
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   2
               Top             =   480
               Width           =   600
               _ExtentX        =   1058
               _ExtentY        =   582
               _Version        =   327681
               MaxLength       =   5
               Format          =   "hh:mm"
               Mask            =   "##:##"
               PromptChar      =   "_"
            End
            Begin MSMask.MaskEdBox MaskEdBox1 
               Height          =   330
               Index           =   1
               Left            =   1080
               TabIndex        =   4
               Top             =   480
               Width           =   600
               _ExtentX        =   1058
               _ExtentY        =   582
               _Version        =   327681
               MaxLength       =   5
               Format          =   "hh:mm"
               Mask            =   "##:##"
               PromptChar      =   "_"
            End
            Begin ComCtl2.UpDown UpDown3 
               Height          =   330
               Index           =   1
               Left            =   1680
               TabIndex        =   5
               TabStop         =   0   'False
               Top             =   480
               Width           =   240
               _ExtentX        =   423
               _ExtentY        =   582
               _Version        =   327681
               OrigLeft        =   1680
               OrigTop         =   465
               OrigRight       =   1920
               OrigBottom      =   795
               Increment       =   0
               Max             =   1440
               Enabled         =   -1  'True
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Desde"
               Height          =   195
               Index           =   5
               Left            =   120
               TabIndex        =   56
               Top             =   240
               WhatsThisHelpID =   10021
               Width           =   465
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Hasta"
               Height          =   195
               Index           =   0
               Left            =   1080
               TabIndex        =   55
               Top             =   240
               WhatsThisHelpID =   10021
               Width           =   420
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Dias Semana"
            Height          =   960
            Index           =   5
            Left            =   6120
            TabIndex        =   46
            Top             =   360
            Width           =   3570
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   0
               Left            =   175
               TabIndex        =   6
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   1
               Left            =   675
               TabIndex        =   7
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   2
               Left            =   1165
               TabIndex        =   8
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   3
               Left            =   1665
               TabIndex        =   9
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   4
               Left            =   2160
               TabIndex        =   10
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   5
               Left            =   2665
               TabIndex        =   11
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.CheckBox chkCheck1 
               Height          =   240
               Index           =   6
               Left            =   3175
               TabIndex        =   12
               Top             =   570
               Value           =   1  'Checked
               Width           =   240
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "L"
               Height          =   195
               Index           =   3
               Left            =   225
               TabIndex        =   53
               Top             =   300
               Width           =   90
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "M"
               Height          =   195
               Index           =   4
               Left            =   720
               TabIndex        =   52
               Top             =   300
               Width           =   135
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "X"
               Height          =   195
               Index           =   6
               Left            =   1230
               TabIndex        =   51
               Top             =   300
               Width           =   105
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "J"
               Height          =   195
               Index           =   7
               Left            =   1725
               TabIndex        =   50
               Top             =   300
               Width           =   75
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "V"
               Height          =   195
               Index           =   8
               Left            =   2220
               TabIndex        =   49
               Top             =   300
               Width           =   105
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "S"
               Height          =   195
               Index           =   9
               Left            =   2730
               TabIndex        =   48
               Top             =   300
               Width           =   105
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "D"
               Height          =   195
               Index           =   10
               Left            =   3225
               TabIndex        =   47
               Top             =   300
               Width           =   120
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Fecha"
            Height          =   960
            Index           =   3
            Left            =   120
            TabIndex        =   43
            Top             =   360
            Width           =   3690
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "hiredate"
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   0
               Tag             =   "Fecha de Alta"
               Top             =   480
               WhatsThisHelpID =   10013
               Width           =   1650
               _Version        =   65537
               _ExtentX        =   2910
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16776960
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "Pr09FecPeticion"
               Height          =   330
               Index           =   1
               Left            =   1920
               TabIndex        =   1
               Tag             =   "Fecha Petici�n"
               Top             =   480
               Width           =   1650
               _Version        =   65537
               _ExtentX        =   2910
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Hasta"
               Height          =   195
               Index           =   1
               Left            =   1920
               TabIndex        =   45
               Top             =   240
               Width           =   420
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Desde"
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   44
               Top             =   240
               Width           =   465
            End
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   285
            Index           =   6
            Left            =   10200
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   13
            Text            =   "CI1026.frx":2266
            Top             =   825
            Width           =   360
         End
         Begin ComCtl2.UpDown UpDown1 
            Height          =   285
            Left            =   10590
            TabIndex        =   14
            Top             =   825
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   503
            _Version        =   327681
            Value           =   1
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtText1(6)"
            BuddyDispid     =   196615
            BuddyIndex      =   6
            OrigLeft        =   10695
            OrigTop         =   840
            OrigRight       =   10935
            OrigBottom      =   1140
            Min             =   1
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin VB.Label Label2 
            Caption         =   "N� Soluciones"
            Height          =   255
            Index           =   0
            Left            =   9960
            TabIndex        =   57
            Top             =   480
            Width           =   1095
         End
      End
   End
End
Attribute VB_Name = "frmSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Base 1
Dim imgX                As ListImage
Dim btnx                As Button


Dim blnDaySelected      As Boolean   'permitir seleccionar dias del mes haciendo click
Dim datDaysSelected()   As Date
Dim intNumDaysSel       As Integer
Dim blnConfirmarCitas   As Boolean
Dim blnFasesPorObj      As Boolean
Dim blnSolutionAsked   As Boolean
Dim intTotActs          As Integer
Dim blnNewSolutions As Boolean
Public blnNoSolutions As Boolean
Public blnSecondTime  As Boolean
'Variable para pruebas con Ilog
Public objIlog             As Object
Dim intTotSols As Integer
Dim intOldSols As Integer
Dim intMoreSols As Integer

Private Sub CrearFasesBD()
  Dim objPeticion     As clsSelPeticion
  Dim objActuacion    As clsSelActuacion
  Dim objFase         As clsFasesActuacion
  Dim objRecurso      As clsRecursoFase
  Dim objSolucion     As clsSolucion

  Dim intIndAct As Integer
  Dim intIndFase As Integer
  Dim intIndRec As Integer
  
  Dim lngIlog As Long
  Dim lngIlogFases As Long
  Dim lngIlogRecursos As Long
  
  Dim rdoActuacion As rdoResultset
  Dim rdoFase As rdoResultset
  
  Dim strSql As String
  Dim intPos As Integer
  Dim lngNumMinutos As Long
  
  With objIlog
    intIndAct = 0
    For Each objPeticion In objSolicitud.cllPeticiones
      For Each objActuacion In objPeticion.cllActuaciones
        intIndFase = 0
        
        strSql = "SELECT PR06NUMFASE,PR06DESFASE FROM PR0600 WHERE  PR03NUMACTPEDI=" & objActuacion.lngNumActPedi
        Set rdoActuacion = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)

        Set objSolucion = objActuacion.AddNewSolucion(CLng(1))
        
       
        Do While Not rdoActuacion.EOF
          lngIlogFases = rdoActuacion("PR06NUMFASE")
          '
          Set objFase = objSolucion.AddNewFaseSol(intIndFase)
          
          objFase.intNumFase = rdoActuacion("PR06NUMFASE")
          objFase.strDesFase = rdoActuacion("PR06DESFASE")
          'objFase.intNumDiasPac = .GetDurationPhaseDias(intIndAct, intIndFase)
          'objFase.intNumHoraPac = .GetDurationPhaseHoras(intIndAct, intIndFase)
          'objFase.intNumMinPac = .GetDurationPhaseMin(intIndAct, intIndFase)
            
          'lngIlogRecursos = rdoActuacion("AG14CODTIPRECU_PED")
          strSql = "SELECT AG14CODTIPRECU,PR14NUMUNIREC,PR14INDRECPREFE,PR14NUMMINOCU FROM PR1400 WHERE PR03NUMACTPEDI=" & objActuacion.lngNumActPedi & " AND  PR06NUMFASE=" & rdoActuacion("PR06NUMFASE")
          Set rdoFase = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
          intIndRec = 0
          Do While Not rdoFase.EOF
            For intPos = 1 To rdoFase("PR14NUMUNIREC")
              Set objRecurso = objFase.AddNewRecurso(intIndRec)
              objRecurso.lngCodRecursoFase = rdoFase("AG14CODTIPRECU")
              'lngNumMinutos = rdoFase("PR14NUMMINOCU")
              'objRecurso.intNumDiasRec = CInt(lngNumMinutos / 1440)
              'lngNumMinutos = lngNumMinutos - (objRecurso.intNumDiasRec * 1440)
              'objRecurso.intNumHoraRec = CInt(lngNumMinutos / 60)
              'objRecurso.intNumMinRec = CInt(lngNumMinutos)
              objSolucion.lngRecPrefSol = IIf(rdoFase("PR14INDRECPREFE") = -1, rdoFase("AG14CODTIPRECU"), objSolucion.lngRecPrefSol)
              objActuacion.intNumFaseRecPref = IIf(rdoFase("PR14INDRECPREFE") = -1, rdoActuacion("PR06NUMFASE"), objActuacion.intNumFaseRecPref)
              intIndRec = intIndRec + 1 'Next intIndRec
            Next intPos
            rdoFase.MoveNext
            
           Loop
           rdoActuacion.MoveNext
           intIndFase = intIndFase + 1
          Loop
          If rdoFase.RowCount > 0 Then rdoFase.Close
          If rdoActuacion.RowCount > 0 Then rdoActuacion.Close
        intIndAct = intIndAct + 1
      Next
    Next
  End With

End Sub
Private Sub CrearEstructuraFases()
  Dim objPeticion     As clsSelPeticion
  Dim objActuacion    As clsSelActuacion
  Dim objFase         As clsFasesActuacion
  Dim objRecurso      As clsRecursoFase
  Dim objSolucion     As clsSolucion

  Dim intIndAct As Integer
  Dim intIndFase As Integer
  Dim intIndRec As Integer
  
  Dim lngIlog As Long
  Dim lngIlogFases As Long
  Dim lngIlogRecursos As Long
  
  
  With objIlog
    intIndAct = 0
    For Each objPeticion In objSolicitud.cllPeticiones
      For Each objActuacion In objPeticion.cllActuaciones
        Call objGen.RemoveCollection(objActuacion.cllSoluciones)
        Call objGen.RemoveCollection(objActuacion.cllFasesActuacion)
        Set objSolucion = objActuacion.AddNewSolucion(CLng(1))
        
        lngIlogFases = .GetNumPhases(intIndAct)
  '     MsgBox "N� de Fases: " & lngIlogFases
        objActuacion.intNumFaseRecPref = .GetNumPhaseRecPref(intIndAct)
  '     MsgBox "Num Fase Rec Pref:" & objActuacion.intNumFaseRecPref
        For intIndFase = 0 To lngIlogFases - 1
          Set objFase = objSolucion.AddNewFaseSol(intIndFase)
          
          objFase.intNumFase = .GetNumPhase(intIndAct, intIndFase)
          objFase.strDesFase = .GetDescPhase(intIndAct, intIndFase)
          objFase.intNumDiasPac = .GetDurationPhaseDias(intIndAct, intIndFase)
          objFase.intNumHoraPac = .GetDurationPhaseHoras(intIndAct, intIndFase)
          objFase.intNumMinPac = .GetDurationPhaseMin(intIndAct, intIndFase)
            
    '     MsgBox "Fase creada :" & objFase.intNumDiasPac & "-" & objFase.intNumHoraPac & "-" & objFase.intNumMinPac
          lngIlogRecursos = .GetNumRecursos(intIndAct, intIndFase)
    '     MsgBox "N� de Recursos:" & lngIlogRecursos
          For intIndRec = 0 To lngIlogRecursos - 1
            Set objRecurso = objFase.AddNewRecurso(intIndRec)
    '       MsgBox "GetCodRecurso: " & intIndAct & "-" & intIndFase & "-" & intIndRec
            objRecurso.lngCodRecursoFase = .GetCodRecurso(-1, intIndAct, intIndFase, intIndRec)
            'objRecurso.lngTipRec = Val(GetTableColumn("SELECT AG14CODTIPRECU FROM AG1100 WHERE AG11CODRECURSO=" & objRecurso.lngCodRecursoFase)("AG14CIDTIPRECU"))
            objRecurso.intNumDiasRec = CInt(.GetDurationRecursoDias(intIndAct, intIndFase, intIndRec))
    '       MsgBox "Num Dias:" & objRecurso.intNumDiasRec
            objRecurso.intNumHoraRec = CInt(.GetDurationRecursoHoras(intIndAct, intIndFase, intIndRec))
    '       MsgBox "Num Horas:" & objRecurso.intNumHoraRec
            objRecurso.intNumMinRec = CInt(.GetDurationRecursoMin(intIndAct, intIndFase, intIndRec))
    '       MsgBox "Num Minutos:" & objRecurso.intNumMinRec
    '       MsgBox "Recurso : " & objRecurso.lngCodRecursoFase & "-" & objRecurso.datFechaRecurso
          Next intIndRec
        Next intIndFase
        objSolucion.lngRecPrefSol = objIlog.GetRecursoPreferente(-1, intIndAct)

        intIndAct = intIndAct + 1
      Next
    Next
  End With

End Sub
Private Sub CambioSoluciones(blnTypeMore As Boolean)
  If blnTypeMore Then
    cmdPedirSoluciones(0).Caption = "M�s &Soluciones"
    If Dir(App.Path & "\mas.bmp") = "mas.bmp" Then
      cmdPedirSoluciones(0).Picture = LoadPicture(App.Path & "\mas.bmp")
    End If
  Else
    cmdPedirSoluciones(0).Caption = "&Pedir Soluciones"
    If Dir(App.Path & "\help.bmp") = "help.bmp" Then
      cmdPedirSoluciones(0).Picture = LoadPicture(App.Path & "\help.bmp")
    End If
  End If
End Sub
Public Sub CitarSinPlanificar()
  Dim blnCancelTrans          As Boolean
  Dim blnSolicitudIsOk        As Boolean
  Dim vntFirstRow             As Variant
  Dim objPeticion             As clsSelPeticion
  Dim objActuacion            As clsSelActuacion

  vntFirstRow = grdSSDBGrid1(0).FirstRow
  grdSSDBGrid1(0).Redraw = False

  If grdSSDBGrid1(0).Cols = 6 And Not blnNoSolutions Then
     blnNoSolutions = True
     Call CrearFasesBD
     blnFasesPorObj = True
     grdSSDBGrid1(0).MoveFirst
     For Each objPeticion In objSolicitud.cllPeticiones
       For Each objActuacion In objPeticion.cllActuaciones
         Call ShowFases
         grdSSDBGrid1(0).MoveNext
         grdSSDBGrid1(0).MoveNext
         grdSSDBGrid1(0).MoveNext
         grdSSDBGrid1(0).MoveNext
       Next
     Next
     If Not blnAceptar Then Exit Sub
  End If
  blnSolicitudIsOk = SolicitudIsOk
  If blnSolicitudIsOk Then
   
    objApp.rdoConnect.BeginTrans
    On Error GoTo Canceltrans

   'Grabo los datos en la BD
    Call SolicitudAdd

    If blnCancelTrans Then
      objApp.rdoConnect.RollbackTrans
      Call objError.SetError(cwCodeMsg, ciErrRollback & Err.Description)
      Call objError.Raise
    Else
      objApp.rdoConnect.CommitTrans
      fraFrame1(2).Caption = "Solicitud N� " & Str(objSolicitud.lngSolicitud)
      MsgBox ciMsgConfirmarOk, vbInformation, "SOLICITUD N� " & Str(objSolicitud.lngSolicitud)
      blnConfirmarCitas = False
      blnCommit = True
      Call objLoadFromSolicit(objSolicitud.lngSolicitud, ciOpcVerSolicitud)
      Call SolicitudRefresh
    End If
  End If
  
  grdSSDBGrid1(0).FirstRow = vntFirstRow
  grdSSDBGrid1(0).Redraw = True

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next

End Sub
Public Sub ActualizarDatosIlog()
  Dim objPeticion         As clsSelPeticion
  Dim objActuacion        As clsSelActuacion
  Dim objFase             As clsFasesActuacion
  Dim objRecurso          As clsRecursoFase
  Dim intRow              As Integer
  Dim strValor            As String
  Dim intIndAct           As Integer
  Dim intIndFase          As Integer
  Dim intIndRec           As Integer
  Dim intNumFirstFase     As Integer
  Dim datFechaGrid        As Date
  
  On Error GoTo Error:
  intIndAct = 0
  grdSSDBGrid1(0).MoveFirst
 ' MsgBox "Actualizando Datos Ilog"
  For Each objPeticion In objSolicitud.cllPeticiones
 '   MsgBox "Peticion"
    For Each objActuacion In objPeticion.cllActuaciones
      'Recorro las fases y recursos para pasarlos a Ilog
     '************************
      'objActuacion.lngNumActPlan = 56
     '****************************
      
      intNumFirstFase = -1
'      MsgBox "Actuacion: " & objActuacion.lngNumActPlan
      intIndFase = 0
      For Each objFase In objActuacion.cllFasesActuacion
        'paso los datos de la fase a Ilog
         If intNumFirstFase = -1 Then
           For intRow = 0 To 3
              If intRow = 0 Then
                strValor = grdSSDBGrid1(0).Columns("Asignado").Value
                  If InStr(1, strValor, "/#") <> 0 Then
                    If objActuacion.lngRecurso <> Mid(strValor, InStr(1, strValor, "/#") + 2, InStr(1, strValor, "#/") - InStr(1, strValor, "/#") - 2) Then
                      objActuacion.lngRecurso = Mid(strValor, InStr(1, strValor, "/#") + 2, InStr(1, strValor, "#/") - InStr(1, strValor, "/#") - 2)
                      objActuacion.strSituacion = ciAnulada 'ha variado y ser� anulada y recitada si se viene de Versolicitud
                      'LLamada a funcion Ilog para actualizar recurso Preferente
                    End If
                  End If
                  strValor = grdSSDBGrid1(0).Columns("Preferencias").Value
                  If InStr(1, strValor, "/#") <> 0 Then
                    If objActuacion.lngRecursoPref <> Mid(strValor, InStr(1, strValor, "/#") + 2, InStr(1, strValor, "#/") - InStr(1, strValor, "/#") - 2) Then
                      objActuacion.lngRecursoPref = Mid(strValor, InStr(1, strValor, "/#") + 2, InStr(1, strValor, "#/") - InStr(1, strValor, "/#") - 2)
                    End If
                End If
              End If
              If intRow = 1 Then
                'If objActuacion.datFechaCita <> grdSSDBGrid1(0).Columns("Asignado").Value Then
                  objActuacion.datFechaCita = grdSSDBGrid1(0).Columns("Asignado").Value
                  objActuacion.strSituacion = ciAnulada 'ha variado y ser� anulada y recitada si se viene de Versolicitud
       
                  'LLamada a funcion Ilog para actualizar fecha primera fase
'                  MsgBox "Fecha Act: " & objActuacion.datFechaCita
                  Call objIlog.SetStartPhaseDia(objActuacion.lngNumActPlan, objFase.intNumFase, CInt(Format(objActuacion.datFechaCita, "DD")))
                  Call objIlog.SetStartPhaseMes(objActuacion.lngNumActPlan, objFase.intNumFase, CInt(Format(objActuacion.datFechaCita, "MM")))
                  Call objIlog.SetStartPhaseAnio(objActuacion.lngNumActPlan, objFase.intNumFase, CInt(Format(objActuacion.datFechaCita, "YYYY")))
                'End If
              End If
              If intRow = 2 Then
                'If objActuacion.strHoraCita <> grdSSDBGrid1(0).Columns("Asignado").Text Then
                  
                  objActuacion.strHoraCita = grdSSDBGrid1(0).Columns("Asignado").Text
                  objActuacion.strSituacion = ciAnulada 'ha variado y ser� anulada y recitada si se viene de Versolicitud
                  'LLamada a funcion Ilog para actualizar Hora primera fase
'                  MsgBox "Hora ACT: " & objActuacion.strHoraCita
                  Call objIlog.SetStartPhaseHora(objActuacion.lngNumActPlan, objFase.intNumFase, CInt(Format(CDate(objActuacion.strHoraCita), "HH")))
                  Call objIlog.SetStartPhaseMin(objActuacion.lngNumActPlan, objFase.intNumFase, CInt(Format(CDate(objActuacion.strHoraCita), "NN")))
            
                'End If
             End If
             grdSSDBGrid1(0).MoveNext
           Next intRow

         
       Else
                 
'         MsgBox "Fase Fecha: " & objFase.datFechaFase
         Call objIlog.SetStartPhaseDia(objActuacion.lngNumActPlan, objFase.intNumFase, CInt(Format(objFase.datFechaFase, "DD")))
'         MsgBox "Fase dia: " & CInt(Format(objFase.datFechaFase, "DD"))
         Call objIlog.SetStartPhaseMes(objActuacion.lngNumActPlan, objFase.intNumFase, CInt(Format(objFase.datFechaFase, "MM")))
'         MsgBox "Fase mes: " & CInt(Format(objFase.datFechaFase, "MM"))
         Call objIlog.SetStartPhaseAnio(objActuacion.lngNumActPlan, objFase.intNumFase, CInt(Format(objFase.datFechaFase, "YYYY")))
'         MsgBox "Fase a�o: " & CInt(Format(objFase.datFechaFase, "YYYY"))
         Call objIlog.SetStartPhaseHora(objActuacion.lngNumActPlan, objFase.intNumFase, CInt(Format(objFase.datFechaFase, "HH")))
'         MsgBox "Fase hora: " & CInt(Format(objFase.datFechaFase, "HH"))
         Call objIlog.SetStartPhaseMin(objActuacion.lngNumActPlan, objFase.intNumFase, CInt(Format(objFase.datFechaFase, "NN")))
'         MsgBox "Fase minutos: " & CInt(Format(objFase.datFechaFase, "NN"))
       End If
'       MsgBox "Fase:" & intIndFase & " Fecha: " & objFase.datFechaFase
       intNumFirstFase = IIf(intNumFirstFase = -1, objFase.intNumFase, intNumFirstFase)
       intIndRec = 0
       For Each objRecurso In objFase.cllRecursosFase
         'paso datos recurso a Ilog
'          MsgBox "Recurso Fecha: " & objRecurso.datFechaRecurso

          Call objIlog.SetStartRecursoDia(objActuacion.lngNumActPlan, objFase.intNumFase, objRecurso.lngCodRecursoFase, CInt(Format(objRecurso.datFechaRecurso, "DD")))
'          MsgBox "Recurso dia: " & CInt(Format(objRecurso.datFechaRecurso, "DD"))
          Call objIlog.SetStartRecursoMes(objActuacion.lngNumActPlan, objFase.intNumFase, objRecurso.lngCodRecursoFase, CInt(Format(objRecurso.datFechaRecurso, "MM")))
'          MsgBox "Recurso mes: " & CInt(Format(objRecurso.datFechaRecurso, "MM"))
         Call objIlog.SetStartRecursoAnio(objActuacion.lngNumActPlan, objFase.intNumFase, objRecurso.lngCodRecursoFase, CInt(Format(objRecurso.datFechaRecurso, "YYYY")))
'         MsgBox "Recurso a�o: " & CInt(Format(objRecurso.datFechaRecurso, "YYYY"))
          Call objIlog.SetStartRecursoHora(objActuacion.lngNumActPlan, objFase.intNumFase, objRecurso.lngCodRecursoFase, CInt(Format(objRecurso.datFechaRecurso, "HH")))
'         MsgBox "Recurso hora: " & CInt(Format(objRecurso.datFechaRecurso, "HH"))
          Call objIlog.SetStartRecursoMin(objActuacion.lngNumActPlan, objFase.intNumFase, objRecurso.lngCodRecursoFase, CInt(Format(objRecurso.datFechaRecurso, "NN")))
'           MsgBox "Recurso min: " & CInt(Format(objRecurso.datFechaRecurso, "NN"))
  
'          MsgBox "Fase:" & intIndFase & " Recurso: " & intIndRec & " Fecha: " & objRecurso.datFechaRecurso
         
          intIndRec = intIndRec + 1
        Next
        intIndFase = intIndFase + 1
      Next
      
    intIndAct = intIndAct + 1
    Next
  Next
  Set objPeticion = Nothing
  Set objActuacion = Nothing
  Set objFase = Nothing
  Set objRecurso = Nothing
  Exit Sub
  
Error:
 MsgBox "Error en ActualizarIlog ( " & Err.Number & "-" & Err.Description & " )"
 Resume Next
End Sub
Public Function ShowFasesPorObj(lngActuacion As Long)
  Dim objPetic      As clsSelPeticion
  Dim objAct        As clsSelActuacion
  Dim objFase       As clsFasesActuacion
  Dim objRec        As clsRecursoFase
  Dim objSolucion   As clsSolucion
  Dim cllFasesSol As Collection
  Dim intNumSol     As Integer
  
  'On Error Resume Next
  Load frmFasesSolucion
  With frmFasesSolucion
    For Each objPetic In objSolicitud.cllPeticiones
      Set objAct = objPetic.GetActuacion(lngActuacion)
      If objAct Is Nothing Then
        'No ha encontrado la actuaci�n
      Else
        If objAct.lngNumActPlan = lngActuacion Then
         'intNumSol = grdSSDBGrid1(0).Col - 4
         'intNumSol = IIf(intNumSol < 1, 1, intNumSol)
         If frmSolicitud.blnNoSolutions Then
           .Caption = "CITAS. Fases de la Actuaci�n" & ": " & objAct.strDescripci�n
         Else
           .Caption = "CITAS. Fases de la Actuaci�n" & ": " & objAct.strDescripci�n & " (Soluci�n 1)"
         End If
         .grdDBGrid1(0).RemoveAll
         .grdDBGrid1(1).RemoveAll
         Set .objActElegida = objAct
         Set .objSolucion = objAct.GetSolucion(1)
         'Set .objSolucion.cllFasesSolucion = cllFasesSol
         .intSolucion = 1
         .intTotSol = grdSSDBGrid1(0).Cols - 6
         If blnNoSolutions Then .intTotSol = 1
         If .intTotSol > 1 Then
           .cmdCommand1(1).Visible = True
           .cmdCommand1(2).Enabled = False
           .cmdCommand1(4).Enabled = False
           .cmdCommand1(2).Visible = True
           .cmdCommand1(4).Visible = True
           .cmdCommand1(5).Visible = True
         Else
           .cmdCommand1(1).Visible = False
           .cmdCommand1(2).Visible = False
           .cmdCommand1(4).Visible = False
           .cmdCommand1(5).Visible = False
         End If

         If .objSolucion.cllFasesSolucion.Count > 0 Then
          For Each objFase In .objSolucion.cllFasesSolucion
           If blnNoSolutions And Not blnSecondTime Then
             
             .grdDBGrid1(0).AddItem objFase.intNumFase & "," _
                               & objFase.strDesFase & "," _
                               & " ," _
                               & " ," _
                               & objFase.intNumDiasPac & "," _
                               & objFase.intNumHoraPac & "," _
                               & objFase.intNumMinPac
           
             For Each objRec In objFase.cllRecursosFase
               objRec.lngTipRec = objRec.lngCodRecursoFase
               objRec.lngCodRecursoFase = 0
               objRec.datFechaRecurso = 1
             
             Next
           Else
             If objFase.datFechaFase = 1 Or objFase.datFechaFase = "0.00.00" Then
               .grdDBGrid1(0).AddItem objFase.intNumFase & "," _
                               & objFase.strDesFase & "," _
                               & " ," _
                               & " ," _
                               & objFase.intNumDiasPac & "," _
                               & objFase.intNumHoraPac & "," _
                               & objFase.intNumMinPac
             Else
                .grdDBGrid1(0).AddItem objFase.intNumFase & "," _
                               & objFase.strDesFase & "," _
                               & Format(objFase.datFechaFase, "DD/MM/YYYY") & "," _
                               & Format(objFase.datFechaFase, "HH:MM") & "," _
                               & objFase.intNumDiasPac & "," _
                               & objFase.intNumHoraPac & "," _
                               & objFase.intNumMinPac
              End If
            End If
          Next
          
          Exit For
         End If
        End If
      End If
    Next
    Call .grdDBGrid1_Click(0)
    
    .Show (vbModal)
    
   End With
   Unload frmFasesSolucion
   Set frmFasesSolucion = Nothing
End Function
Private Function SolicitudIsOk() As Boolean
   
   Dim objPeticion          As clsSelPeticion
   Dim objActuacion         As clsSelActuacion
   Dim obSolucion           As clsSolucion
   Dim objFase              As clsFasesActuacion
   Dim objRec               As clsRecursoFase
   Dim intRow               As Integer
   Dim vntBookmark          As Variant
   'Dim intRowSelected      As Integer
   Dim strMensaje           As String
   Dim intActiveRow         As Integer
   
   grdSSDBGrid1(0).Redraw = False
   intActiveRow = grdSSDBGrid1(0).Row
   
   vntBookmark = grdSSDBGrid1(0).FirstRow
   strMensaje = "Error!!! . No se han realizado las siguientes asignaciones:" & vbCrLf
   SolicitudIsOk = True
   
   grdSSDBGrid1(0).MoveFirst
   
   For Each objPeticion In objSolicitud.cllPeticiones
      For Each objActuacion In objPeticion.cllActuaciones
         For intRow = 0 To 3
           Select Case grdSSDBGrid1(0).Columns("Cabecera").Value
              Case "Recurso" 'validar el recurso
                  'If grdSSDBGrid1(0).Columns("Asignado").Value = "" Then
                  '   SolicitudIsOk = False
                  '   strMensaje = strMensaje & vbTab & "-RECURSO para " & vbTab & objActuacion.strDescripci�n & vbCrLf
                  'Else
                  'End If
              Case "Fecha" 'validar el recurso
                  If grdSSDBGrid1(0).Columns("Asignado").Value = "" Then
                     SolicitudIsOk = False
                     strMensaje = strMensaje & vbTab & "-FECHA para   " & vbTab & objActuacion.strDescripci�n & vbCrLf
                  End If
              Case "Hora" 'validar el recurso
                  If grdSSDBGrid1(0).Columns("Asignado").Value = "" Then
                     SolicitudIsOk = False
                     strMensaje = strMensaje & vbTab & "-HORA para    " & vbTab & objActuacion.strDescripci�n & vbCrLf
                  End If
              Case Else
            End Select
            grdSSDBGrid1(0).MoveNext
         Next
      Next
   Next
   
   For Each objPeticion In objSolicitud.cllPeticiones
     For Each objActuacion In objPeticion.cllActuaciones

       'Set objSolucion = objActuacion.GetSolucion(intNumSol)
       For Each objFase In objActuacion.cllFasesActuacion
         If Val(objFase.datFechaFase) = 0 Or _
                Not IsDate(objFase.datFechaFase) Then
          Call objError.SetError(cwCodeMsg, "El campo Fecha de la fase: " & objFase.strDesFase & " no tiene un valor v�lido.")
          Call objError.Raise
          SolicitudIsOk = False
         Exit Function
         End If
         For Each objRec In objFase.cllRecursosFase
           If Val(objRec.lngCodRecursoFase) = 0 Then
             Call objError.SetError(cwCodeMsg, "El campo C�digo de Recurso  la Fase: " & objFase.strDesFase & " est� vacio.")
             Call objError.Raise
             SolicitudIsOk = False
             Exit Function
           End If
           If Val(objRec.datFechaRecurso) = 0 Or _
                Not IsDate(objRec.datFechaRecurso) Then
             Call objError.SetError(cwCodeMsg, "El campo Fecha del Recurso: " & objRec.lngCodRecursoFase & " de la Fase: " & objFase.strDesFase & " est� vacio.")
             Call objError.Raise
             SolicitudIsOk = False
             Exit Function
           End If
         Next
       Next
     Next
   Next
   If SolicitudIsOk = False Then
      MsgBox strMensaje, vbExclamation, "Confirmar Citas"
   End If
   
   'grdSSDBGrid1(0).Row = intActiveRow
   
   grdSSDBGrid1(0).FirstRow = vntBookmark

   grdSSDBGrid1(0).Redraw = True
   Set objPeticion = Nothing
   Set objActuacion = Nothing

End Function
Private Function CallAgenda(ByVal intRespuestas As Integer) As Boolean
  Dim intResp         As Integer
  Dim intRow          As Integer
  Dim intTotCol       As Integer
  Dim objPeticion     As clsSelPeticion
  Dim objActuacion    As clsSelActuacion
  Dim objFase         As clsFasesActuacion
  Dim objRecurso      As clsRecursoFase
  Dim objSolucion     As clsSolucion
  Dim dattoday        As Date
  Dim intvalor        As Integer
  Dim datfechavalor   As Date
  Dim intconta        As Integer
  Dim vntFirstRow     As Variant
  Dim intMaxvalue     As Integer
  Dim intActivecol    As Integer
  Dim strDescRec      As String
  Dim strValor        As String
  Dim datFechaAct As Date
  
  Dim intIndAct As Integer
  Dim intIndFase As Integer
  Dim intIndRec As Integer
  
  Dim lngIlog As Long
  Dim lngIlogFases As Long
  Dim lngIlogRecursos As Long
  
  Dim strFechaPref As String
  
  On Error GoTo ErrorIlog
  
  On Error GoTo 0
  CallAgenda = True
  Me.MousePointer = vbHourglass
  'frmDelay.Show
  grdSSDBGrid1(0).Columns("Asignado").Locked = True

  dattoday = Date + Str(Int(90 * Rnd) + 1)
  vntFirstRow = grdSSDBGrid1(0).FirstRow
  grdSSDBGrid1(0).Redraw = False
  intRow = grdSSDBGrid1(0).Row
  intActivecol = grdSSDBGrid1(0).Col  'no hace lo que debe
  grdSSDBGrid1(0).MoveFirst
  
    'Introducimos las actuaciones
    If blnNewSolutions Then
    intTotActs = 0
    For Each objPeticion In objSolicitud.cllPeticiones
      For Each objActuacion In objPeticion.cllActuaciones
'        MsgBox "Actuacion: " & objActuacion.lngNumActPlan
       With objIlog
          strFechaPref = ""
         'Actuaci�n
         'If Not blnSolutionAsked Then
          Call .SetCodAct(objActuacion.lngNumActPlan)
          ' Call .SetCodAct(56)
         'End If
        'Preferencias
         'Recurso
         If grdSSDBGrid1(0).Columns("Preferencias").Value <> "" Then
            strValor = grdSSDBGrid1(0).Columns("Preferencias").Value
            If InStr(1, strValor, "/#") <> 0 Then
               If objActuacion.lngRecursoPref <> Mid(strValor, InStr(1, strValor, "/#") + 2, InStr(1, strValor, "#/") - InStr(1, strValor, "/#") - 2) Then
                  objActuacion.lngRecursoPref = Mid(strValor, InStr(1, strValor, "/#") + 2, InStr(1, strValor, "#/") - InStr(1, strValor, "/#") - 2)
               End If
               Call .SetRecursoPreferente(intTotActs, objActuacion.lngRecursoPref)
               
            End If
         End If
         grdSSDBGrid1(0).MoveNext
         'Dia/Mes/A�o
         If grdSSDBGrid1(0).Columns("Preferencias").Value <> " " And IsDate(grdSSDBGrid1(0).Columns("Preferencias").Value) Then
           If CDate(grdSSDBGrid1(0).Columns("Preferencias").Value) > dtcDateCombo1(1).Date Then
              MsgBox "La fecha Preferente " & grdSSDBGrid1(0).Columns("Preferencias").Value & " no est� entre las fechas de estudio.", vbInformation
              CallAgenda = False
              Exit Function
           End If
           If CDate(grdSSDBGrid1(0).Columns("Preferencias").Value) >= dtcDateCombo1(0).Date Then
             strFechaPref = grdSSDBGrid1(0).Columns("Preferencias").Value
             grdSSDBGrid1(0).MoveNext
             If Trim(grdSSDBGrid1(0).Columns("Preferencias").Text) <> "" Then
                If CDate(strFechaPref & " " & grdSSDBGrid1(0).Columns("Preferencias").Text) > Now Then
                 Call .SetDiaPrefAct(intTotActs, CInt(Format(strFechaPref, "DD")))
                 Call .SetMesPrefAct(intTotActs, CInt(Format(strFechaPref, "MM")))
                 Call .SetAnioPrefAct(intTotActs, CInt(Format(strFechaPref, "YYYY")))
                End If
             Else
               If CDate(strFechaPref) > dtcDateCombo1(0).Date Then
                 Call .SetDiaPrefAct(intTotActs, CInt(Format(strFechaPref, "DD")))
                 Call .SetMesPrefAct(intTotActs, CInt(Format(strFechaPref, "MM")))
                 Call .SetAnioPrefAct(intTotActs, CInt(Format(strFechaPref, "YYYY")))
               End If
             End If
           Else
             grdSSDBGrid1(0).MoveNext
           End If
         
         Else
          grdSSDBGrid1(0).MoveNext
         End If
         
         
         'Hora y Minutos
         If strFechaPref <> "" Then
           If CDate(strFechaPref) >= dtcDateCombo1(0).Date Then
            'grdSSDBGrid1(0).MoveNext
             If Trim(grdSSDBGrid1(0).Columns("Preferencias").Text) <> "" Then
             
               If strFechaPref = "" Then
                  MsgBox "No se puede asignar una hora de preferencia sin seleccionar una fecha.", vbInformation
                  CallAgenda = False
                  Exit Function
               End If
               If CDate(strFechaPref & " " & grdSSDBGrid1(0).Columns("Preferencias").Text) > Now Then
                 Call .SetHoraPrefAct(intTotActs, CInt(Format(CDate(grdSSDBGrid1(0).Columns("Preferencias").Value), "HH")))
                 Call .SetMinPrefAct(intTotActs, CInt(Format(CDate(grdSSDBGrid1(0).Columns("Preferencias").Value), "NN")))
                 grdSSDBGrid1(0).MoveNext
               End If
             Else
               grdSSDBGrid1(0).MoveNext
             End If
           Else
             grdSSDBGrid1(0).MoveNext
           End If
         Else
          grdSSDBGrid1(0).MoveNext
         End If
         grdSSDBGrid1(0).MoveNext
         'grdSSDBGrid1(0).MoveNext
         intTotActs = intTotActs + 1
        End With
      Next
    Next
    End If
    grdSSDBGrid1(0).MoveFirst
    
'    MsgBox "Actuaciones introducidas :" & intTotActs
    'Ejecutamos la Petici�n de soluci�n
      If blnNewSolutions Then
        grdSSDBGrid1(0).Columns("Asignado").RemoveAll
        intTotSols = CInt(intRespuestas)

        If intTotSols > 50 Then
          Call objError.SetError(cwCodeMsg, "Ha excedido el n�mero m�ximo de soluciones posibles.")
          Call objError.Raise
        End If

        lngIlog = objIlog.Run(CInt(intRespuestas))
        'MsgBox "Run:" & lngIlog
        If lngIlog > 0 Then
          blnNewSolutions = False
          intMoreSols = 0
          'intOldSols = grdSSDBGrid1(0).Columns.Count - 6 + intOldSols
        End If
      Else
        intTotSols = intTotSols + CInt(intRespuestas)
        'intOldSols = 0
        
        If intTotSols > 50 Then
          Call objError.SetError(cwCodeMsg, "No se pueden obtener mas soluciones con estas restricciones.")
          Call objError.Raise
        End If
        lngIlog = objIlog.MoreSolutions(CInt(intRespuestas))
        'MsgBox "More Solutions:" & lngIlog
        
      End If
      
      If lngIlog > 0 Then
         

      'Obtenemos las actuaciones, fases y recursos
        For intResp = 1 To lngIlog
         intTotCol = grdSSDBGrid1(0).Columns.Count - 1
         grdSSDBGrid1(0).Columns.Add (intTotCol)
         grdSSDBGrid1(0).Columns(intTotCol).Name = "Soluci�n" & LTrim(Str((intTotCol - 4)))
        'grdSSDBGrid1(0).Columns(intTotCol).BackColor = objApp.objColor.lngReadOnly
         grdSSDBGrid1(0).Columns(intTotCol).StyleSet = "styleCabecera"
         grdSSDBGrid1(0).Columns(intTotCol).Locked = True

         grdSSDBGrid1(0).Columns(intTotCol).Style = ssStyleEditButton
         grdSSDBGrid1(0).Columns(intTotCol).ButtonsAlways = True
         grdSSDBGrid1(0).Columns(intTotCol).width = 2000
         grdSSDBGrid1(0).Columns(intTotCol).Caption = "Soluci�n " & (intTotCol - 4)
         grdSSDBGrid1(0).MoveFirst
      
         With objIlog
           intIndAct = 0
          For Each objPeticion In objSolicitud.cllPeticiones
           For Each objActuacion In objPeticion.cllActuaciones
            datFechaAct = 1
            Set objSolucion = objActuacion.AddNewSolucion(CLng(intTotCol - 4))
            lngIlogFases = .GetNumPhases(intIndAct)
         '   MsgBox "N� de Fases: " & lngIlogFases
            objActuacion.intNumFaseRecPref = .GetNumPhaseRecPref(intIndAct)
         '   MsgBox "Num Fase Rec Pref:" & objActuacion.intNumFaseRecPref
            For intIndFase = 0 To lngIlogFases - 1
              Set objFase = objSolucion.AddNewFaseSol(intIndFase)
         '     MsgBox "Fase: IndAct" & intIndAct & " - IndFase" & intIndFase
              objFase.datFechaFase = Format(.GetStartPhaseDia(intTotCol - 5 - intOldSols + intMoreSols, intIndAct, intIndFase) & "/" _
                                       & .GetStartPhaseMes(intTotCol - 5 - intOldSols + intMoreSols, intIndAct, intIndFase) & "/" _
                                       & .GetStartPhaseAnio(intTotCol - 5 - intOldSols + intMoreSols, intIndAct, intIndFase) & " " _
                                       & .GetStartPhaseHora(intTotCol - 5 - intOldSols + intMoreSols, intIndAct, intIndFase) & ":" _
                                       & .GetStartPhaseMin(intTotCol - 5 - intOldSols + intMoreSols, intIndAct, intIndFase), "DD/MM/YYYY HH:NN")

         '     MsgBox "Fecha Fase:" & objFase.datFechaFase
              
              datFechaAct = IIf(datFechaAct = 1, objFase.datFechaFase, datFechaAct)
              objFase.intNumFase = .GetNumPhase(intIndAct, intIndFase)
              objFase.strDesFase = .GetDescPhase(intIndAct, intIndFase)
              objFase.intNumDiasPac = .GetDurationPhaseDias(intIndAct, intIndFase)
              objFase.intNumHoraPac = .GetDurationPhaseHoras(intIndAct, intIndFase)
              objFase.intNumMinPac = .GetDurationPhaseMin(intIndAct, intIndFase)
              
         '     MsgBox "Fase creada :" & objFase.intNumDiasPac & "-" & objFase.intNumHoraPac & "-" & objFase.intNumMinPac
              lngIlogRecursos = .GetNumRecursos(intIndAct, intIndFase)
         '     MsgBox "N� de Recursos:" & lngIlogRecursos
              For intIndRec = 0 To lngIlogRecursos - 1
                Set objRecurso = objFase.AddNewRecurso(intIndRec)
         '       MsgBox "GetCodRecurso: " & intIndAct & "-" & intIndFase & "-" & intIndRec
                objRecurso.lngCodRecursoFase = .GetCodRecurso(intTotCol - 5 - intOldSols + intMoreSols, intIndAct, intIndFase, intIndRec)
                objRecurso.lngTipRec = Val(GetTableColumn("SELECT AG14CODTIPRECU FROM AG1100 WHERE AG11CODRECURSO=" & objRecurso.lngCodRecursoFase)("AG14CODTIPRECU"))

         '       MsgBox "Recurso:" & objRecurso.lngCodRecursoFase
         '       MsgBox "Dia Recurso: " & .GetStartRecursoDia(intTotCol - 5, intIndAct, intIndFase, intIndRec)
         '       MsgBox "Mes Recurso: " & .GetStartRecursoMes(intTotCol - 5, intIndAct, intIndFase, intIndRec)
         '       MsgBox "A�o Recurso: " & .GetStartRecursoAnio(intTotCol - 5, intIndAct, intIndFase, intIndRec)
         '       MsgBox "Hora Recurso: " & .GetStartRecursoHora(intTotCol - 5, intIndAct, intIndFase, intIndRec)
         '       MsgBox "Minutos Recurso: " & .GetStartRecursoMin(intTotCol - 5, intIndAct, intIndFase, intIndRec)
                objRecurso.datFechaRecurso = CDate(Format(.GetStartRecursoDia(intTotCol - 5 - intOldSols + intMoreSols, intIndAct, intIndFase, intIndRec) & "/" _
                                       & .GetStartRecursoMes(intTotCol - 5 - intOldSols + intMoreSols, intIndAct, intIndFase, intIndRec) & "/" _
                                       & .GetStartRecursoAnio(intTotCol - 5 - intOldSols + intMoreSols, intIndAct, intIndFase, intIndRec) & " " _
                                       & .GetStartRecursoHora(intTotCol - 5 - intOldSols + intMoreSols, intIndAct, intIndFase, intIndRec) & ":" _
                                       & .GetStartRecursoMin(intTotCol - 5 - intOldSols + intMoreSols, intIndAct, intIndFase, intIndRec), "DD/MM/YYYY HH:NN"))

                objRecurso.intNumDiasRec = CInt(.GetDurationRecursoDias(intIndAct, intIndFase, intIndRec))
         '       MsgBox "Num Dias:" & objRecurso.intNumDiasRec
                objRecurso.intNumHoraRec = CInt(.GetDurationRecursoHoras(intIndAct, intIndFase, intIndRec))
         '       MsgBox "Num Horas:" & objRecurso.intNumHoraRec
                objRecurso.intNumMinRec = CInt(.GetDurationRecursoMin(intIndAct, intIndFase, intIndRec))
         '       MsgBox "Num Minutos:" & objRecurso.intNumMinRec
         '       MsgBox "Recurso : " & objRecurso.lngCodRecursoFase & "-" & objRecurso.datFechaRecurso
              Next intIndRec
            Next intIndFase
            objSolucion.lngRecPrefSol = objIlog.GetRecursoPreferente(intTotCol - 5 - intOldSols + intMoreSols, intIndAct)
           
           'Mostramos la actuaci�n
           strDescRec = GetTableColumn("SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO=" & objSolucion.lngRecPrefSol)("AG11DESRECURSO") & Space(100) & "/#" & objRecurso.lngCodRecursoFase & "#/"
                
           grdSSDBGrid1(0).Columns(intTotCol).Text = strDescRec
           grdSSDBGrid1(0).MoveNext
           grdSSDBGrid1(0).Columns(intTotCol).Text = Format(datFechaAct, "DD/MM/YYYY")
           grdSSDBGrid1(0).MoveNext
           grdSSDBGrid1(0).Columns(intTotCol).Text = Format(datFechaAct, "HH:NN")
           grdSSDBGrid1(0).MoveNext
           grdSSDBGrid1(0).MoveNext
           blnSolutionAsked = True
           intIndAct = intIndAct + 1
          Next
         Next
        End With
       Next intResp
        'Unload frmDelay
       cmdConfirmarSolicitud(0).Enabled = True
       Call CambioSoluciones(True)
      Else
        Me.MousePointer = vbDefault
        
        'Unload frmDelay
        If lngIlog = 0 Then
         If Not blnSolutionAsked And grdSSDBGrid1(0).Cols < 7 Then
           If MsgBox("No existen soluciones posibles. Puede cambiar las restricciones o crear la estructura de fases y recursos para citar sin planificar." & vbCrLf _
               & "Desea crear la estructura de fases y registos?", vbExclamation + vbYesNo) = vbYes Then
             blnNoSolutions = True
             blnSecondTime = False
             Call CrearEstructuraFases
             cmdConfirmarSolicitud(0).Enabled = False
             blnFasesPorObj = True
             Call ShowFases
           End If
         Else
           MsgBox "No existen soluciones posibles. Cambie las restricciones."
         End If
        Else
          'MsgBox "Error. Se ha producido un error al intentar generar una soluci�n."
          Call RaiseIlogError(lngIlog)
          blnNewSolutions = True
          Call CambioSoluciones(False)

        End If
        
      End If

    
    If intResp = 1 Then
      intActivecol = intTotCol
    End If
  
  Me.MousePointer = vbDefault
  intOldSols = grdSSDBGrid1(0).Columns.Count - 6
  intMoreSols = intMoreSols + lngIlog
  grdSSDBGrid1(0).FirstRow = vntFirstRow
  'grdSSDBGrid1(0).Row = intRow
  grdSSDBGrid1(0).Col = intActivecol
  grdSSDBGrid1(0).Redraw = True
  grdSSDBGrid1(0).Columns("Asignado").Locked = True
  UpDown2.Max = IIf(grdSSDBGrid1(0).Columns.Count - 6 = 0, 1, grdSSDBGrid1(0).Columns.Count - 6)
  If UpDown2.Min = 0 Then
    UpDown2.Min = 1
  End If
  UpDown2.Value = UpDown2.Max
  Set objPeticion = Nothing
  Set objActuacion = Nothing
  Set objFase = Nothing
  Set objRecurso = Nothing
  Exit Function
ErrorIlog:
  'If Err.Number = -244 Then
    MsgBox "Se ha producido un error al procesar las Actuaciones." & vbCrLf & "Verifique la definici�n de las Actuaciones y los Recursos afectados de la Solicitud."
  'Else
  '  MsgBox "Se ha producido el error : " & Err.Number & " - " & Err.Description
  'End If
  Me.MousePointer = vbDefault
  'Resume Next
  'Unload frmDelay
'End
End Function

Private Function CallAgendaRandomize(ByVal intRespuestas As Integer)
  Dim intResp         As Integer
  Dim intRow          As Integer
  Dim intTotCol       As Integer
  Dim objPeticion     As clsSelPeticion
  Dim objActuacion    As clsSelActuacion
  Dim dattoday        As Date
  Dim intvalor        As Integer
  Dim datfechavalor   As Date
  Dim intconta        As Integer
  Dim vntFirstRow     As Variant
  Dim intMaxvalue     As Integer
  Dim intActivecol    As Integer

  dattoday = Date + Str(Int(90 * Rnd) + 1)
  vntFirstRow = grdSSDBGrid1(0).FirstRow
  Randomize
  grdSSDBGrid1(0).Redraw = False
  intRow = grdSSDBGrid1(0).Row
  intActivecol = grdSSDBGrid1(0).Col  'no hace lo que debe
  For intResp = 1 To intRespuestas
    intTotCol = grdSSDBGrid1(0).Columns.Count - 1
    grdSSDBGrid1(0).Columns.Add (intTotCol)
    grdSSDBGrid1(0).Columns(intTotCol).Name = "Soluci�n" & LTrim(Str((intTotCol - 4)))
   'grdSSDBGrid1(0).Columns(intTotCol).BackColor = objApp.objColor.lngReadOnly
    grdSSDBGrid1(0).Columns(intTotCol).StyleSet = "styleCabecera"
    grdSSDBGrid1(0).Columns(intTotCol).Locked = True

    grdSSDBGrid1(0).Columns(intTotCol).Style = ssStyleEditButton
    grdSSDBGrid1(0).Columns(intTotCol).ButtonsAlways = True
    grdSSDBGrid1(0).Columns(intTotCol).width = 2000
    grdSSDBGrid1(0).Columns(intTotCol).Caption = "Soluci�n " & (intTotCol - 4)
    grdSSDBGrid1(0).MoveFirst

    For Each objPeticion In objSolicitud.cllPeticiones
      For Each objActuacion In objPeticion.cllActuaciones
        grdSSDBGrid1(0).Columns(intTotCol).Text = "RECURSO " & Str(Int(25 * Rnd) + 1)
        grdSSDBGrid1(0).MoveNext
        intvalor = IIf(CDate(dtcDateCombo1(1).Date) >= CDate(dtcDateCombo1(0).Date), DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date), 60)
        For intconta = 0 To 1000
          Randomize
          datfechavalor = IIf(intvalor = 0, dtcDateCombo1(0).Date, DateAdd("d", (Int(intvalor * Rnd) + 1), dtcDateCombo1(0).Date))
          If chkCheck1(WeekDay(datfechavalor, vbMonday) - 1) Then
            Exit For
          End If
        Next
        grdSSDBGrid1(0).Columns(intTotCol).Text = datfechavalor
        grdSSDBGrid1(0).MoveNext
        grdSSDBGrid1(0).Columns(intTotCol).Text = Right("00" & LTrim(Str(Int(23 * Rnd) + 1)), 2) & ":" & Right("00" & LTrim(Str(Int(59 * Rnd) + 1)), 2)
        grdSSDBGrid1(0).MoveNext
        grdSSDBGrid1(0).MoveNext
      Next
    Next
    If intResp = 1 Then
      intActivecol = intTotCol
    End If
  Next
  grdSSDBGrid1(0).FirstRow = vntFirstRow
  grdSSDBGrid1(0).Row = intRow
  grdSSDBGrid1(0).Col = intActivecol
  grdSSDBGrid1(0).Redraw = True
  UpDown2.Max = IIf(grdSSDBGrid1(0).Columns.Count - 6 = 0, 1, grdSSDBGrid1(0).Columns.Count - 6)
  If UpDown2.Min = 0 Then
    UpDown2.Min = 1
  End If
  UpDown2.Value = UpDown2.Max
  Set objPeticion = Nothing
  Set objActuacion = Nothing
End Function
Private Sub ShowFases()
   Me.MousePointer = vbHourglass
   
  
   If TabSSTab1(0).Tab = 0 Then
      Dim intFirstRow As Integer
      Dim intActivecol As Integer
      Dim intRow As Integer
      Dim vntBookmark As Variant
      vntBookmark = grdSSDBGrid1(0).Bookmark
      grdSSDBGrid1(0).Redraw = False

      While grdSSDBGrid1(0).Columns("Cabecera").Text <> "Recurso"
          grdSSDBGrid1(0).MovePrevious
      Wend
      vntNumActPlan = grdSSDBGrid1(0).Columns("Indice").Text
      grdSSDBGrid1(0).MoveNext
        
      vntNumActPedi = grdSSDBGrid1(0).Columns("Indice").Text
      grdSSDBGrid1(0).MoveNext
          
      vntNumSolicit = grdSSDBGrid1(0).Columns("Indice").Text
      grdSSDBGrid1(0).MoveNext
          
      vntNumCita = grdSSDBGrid1(0).Columns("Indice").Text
      grdSSDBGrid1(0).MoveNext
          
      grdSSDBGrid1(0).MoveRecords -4
          
      grdSSDBGrid1(0).Bookmark = vntBookmark
      'grdSSDBGrid1(0).FirstRow = vntBookmark
      grdSSDBGrid1(0).Redraw = True
   Else
      vntNumActPlan = grdSSDBGrid1(1).Columns("NumActPlan").Value
      vntNumActPedi = grdSSDBGrid1(1).Columns("NumActPedi").Value
      vntNumSolicit = grdSSDBGrid1(1).Columns("NumSolicit").Value
      vntNumCita = grdSSDBGrid1(1).Columns("NumCita").Value
   End If
   
   If objSolicitud.strOpcion = ciOpcAgenda And blnFasesPorObj Then
     
     'Call ShowFasesPorObj(CLng(vntNumActPlan))
     
     Call objSecurity.LaunchProcess(ciFasesSolucion, vntNumActPlan)
   Else
     'Load frmFasesPCR
     'Me.MousePointer = vbDefault
     'frmFasesPCR.Show vbModal
     'Unload frmFasesPCR
     'Set frmFasesPCR = Nothing
     
     Call objSecurity.LaunchProcess(ciFasesRecursos, False)
   End If
   Me.MousePointer = vbDefault
End Sub

'Muestra los datos del n�mero de selecci�n que se le pasa (updown)

Private Sub ShowSolution(ByVal intSolution As Integer)
  Dim strNameCol As String
  Dim datSelected As Date
  Dim intRow As Integer
  Dim DayNum As Integer

  grdSSDBGrid1(0).Redraw = False
  If TabSSTab1(0).Tab = 1 Then
    blnDaySelected = True
  '  For DayNum = 0 To SSMonth1(0).SelectedDays.Count - 1
  '    SSMonth1(0).SelectedDays(DayNum).Caption = ""
  '    SSMonth1(0).SelectedDays(DayNum).StyleSet = ""
  ' Next
   
   For DayNum = 1 To intNumDaysSel
     SSMonth1(0).Day(datDaysSelected(DayNum)).Caption = ""
     SSMonth1(0).Day(datDaysSelected(DayNum)).StyleSet = ""
   Next DayNum
   intNumDaysSel = 0
   Erase datDaysSelected()
   SSMonth1(0).SelectedDays.RemoveAll
'   SSMonth1(0).Refresh
   SSMonth1(0).BackColorSelected = 8454143
   SSMonth1(0).ForeColorSelected = 0
   SSMonth1(0).DayCaptionStyleSet = "styleCaption"
   strNameCol = IIf(intSolution = 0, "Asignado", "Soluci�n " & LTrim(RTrim(Str(intSolution)))) 'intSolution + 4
   'strNameCol = IIf(grdSSDBGrid1(0).Cols = 6, "Asignado", "Soluci�n " & LTrim(RTrim(Str(intSolution)))) 'intSolution + 4
   grdSSDBGrid1(0).MoveFirst
   grdSSDBGrid1(0).MoveNext
   If grdSSDBGrid1(0).Columns(strNameCol).Value <> "" Then
     SSMonth1(0).Date = grdSSDBGrid1(0).Columns(strNameCol).Value
     For intRow = 1 To grdSSDBGrid1(0).Rows - 1
       'grdSSDBGrid1(0).Col = intTotCol
       If grdSSDBGrid1(0).Columns("cabecera").Text = "Fecha" Then
          If grdSSDBGrid1(0).Columns(strNameCol).Value <> "" Then
             datSelected = grdSSDBGrid1(0).Columns(strNameCol).Text
             SSMonth1(0).Day(datSelected).Caption = LTrim(Str(Val(SSMonth1(0).Day(datSelected).Caption) + 1))
             SSMonth1(0).SelectedDays.Add datSelected
             SSMonth1(0).Day(datSelected).StyleSet = "DaySelected"
             intNumDaysSel = intNumDaysSel + 1
             ReDim Preserve datDaysSelected(intNumDaysSel)
             datDaysSelected(intNumDaysSel) = datSelected
          End If
       End If
       grdSSDBGrid1(0).MoveNext
     Next
     Call ShowDayDetail(SSMonth1(0).Date)
   End If
   blnDaySelected = False
    
   SSMonth1(0).Refresh
   
 End If
 grdSSDBGrid1(0).Redraw = True
End Sub

'Muestra las actuacioes de un dia seleccionado con el rat�n
'en el control SSMONTH
Private Sub ShowDayDetail(ByVal datDaySelected As Date)
  Dim objPeticion                   As New clsSelPeticion
  Dim objActuacion                   As New clsSelActuacion
  Dim intTotCol As Integer
  Dim datSelected As Date
  Dim intRow As Integer
  Dim DayNum As Integer
  Dim strHora As String
  Dim strPaciente As String
  Dim strActuacion As String
  Dim strRecurso As String
  Dim lngActPedi As Long
  Dim lngActPlan As Long
  Dim lngSolicit As Long
  Dim intCita As Integer
  Dim strColname As String
  Dim intRecordatorio As Integer
  Dim intListaEspera As Integer
  
  grdSSDBGrid1(0).Redraw = False
  If TabSSTab1(0).Tab = 1 Then
    blnDaySelected = True
    grdSSDBGrid1(0).MoveFirst
    strPaciente = ""
    strHora = ""
    strActuacion = ""
    strRecurso = ""
    lngActPedi = 0
    lngActPlan = 0
    lngSolicit = 0
    intCita = 0
    grdSSDBGrid1(1).RemoveAll
    strColname = IIf(OptOption1(0).Value Or OptOption1(1).Value And UpDown2.Value = 0, "Asignado", "Soluci�n " & LTrim(RTrim(Str(UpDown2.Value))))
    For intRow = 0 To grdSSDBGrid1(0).Rows - 1
      Select Case grdSSDBGrid1(0).Columns("Cabecera").Value  ' (intRow + 4) Mod 4
         Case "Recurso"
            strPaciente = strPaciente & grdSSDBGrid1(0).Columns("Paciente").Text & " "
            strActuacion = strActuacion & grdSSDBGrid1(0).Columns("Actuacion").Text & " "
            strRecurso = grdSSDBGrid1(0).Columns(strColname).Text
            lngActPlan = grdSSDBGrid1(0).Columns("Indice").Value
         Case "Fecha"
            strPaciente = strPaciente & grdSSDBGrid1(0).Columns("Paciente").Text & " "
            strActuacion = strActuacion & grdSSDBGrid1(0).Columns("Actuacion").Text & " "
            datSelected = grdSSDBGrid1(0).Columns(strColname).Text
            lngActPedi = grdSSDBGrid1(0).Columns("Indice").Value
         Case "Hora"
            strPaciente = RTrim(strPaciente & grdSSDBGrid1(0).Columns("Paciente").Text)
            strActuacion = RTrim(strActuacion & grdSSDBGrid1(0).Columns("Actuacion").Text)
            strHora = grdSSDBGrid1(0).Columns(strColname).Text
            lngSolicit = grdSSDBGrid1(0).Columns("Indice").Value
         Case Else
            intCita = grdSSDBGrid1(0).Columns("Indice").Value
            If datSelected = datDaySelected Then
              For Each objPeticion In objSolicitud.cllPeticiones
                For Each objActuacion In objPeticion.cllActuaciones
                    If objActuacion.lngNumActPlan = lngActPlan And objActuacion.lngNumActPedi = lngActPedi Then
                       intRecordatorio = objActuacion.intRecordatorio
                       intListaEspera = objActuacion.intLisEspera
                    End If
                  Next
                Next
                grdSSDBGrid1(1).AddItem datDaySelected & vbTab & strHora & vbTab & strPaciente & vbTab & strActuacion & vbTab & strRecurso _
                              & vbTab & lngActPlan & vbTab & lngActPedi & vbTab & lngSolicit & vbTab & intCita & vbTab & intRecordatorio _
                              & vbTab & intListaEspera
            End If
            strPaciente = ""
            strHora = ""
            strActuacion = ""
            strRecurso = ""
            lngActPedi = 0
            lngActPlan = 0
            lngSolicit = 0
            intCita = 0
      End Select
      grdSSDBGrid1(0).MoveNext
    Next

    blnDaySelected = False
  End If
  grdSSDBGrid1(0).Redraw = True
End Sub
Private Sub SolicitudAdd()
  Dim strSql              As String
  Dim rsSolicitud         As rdoResultset
  Dim rsCitas             As rdoResultset
  Dim rsFases             As rdoResultset
  Dim rsFasesPedidas      As rdoResultset
  Dim rsRecursos          As rdoResultset
  Dim rsRecursosCitados   As rdoResultset
  Dim rsActPedPlan        As rdoResultset
  Dim intCantRecu         As Integer
  Dim datFechaConcert     As Date
  Dim datFechaTemporal    As Date
  Dim intRow              As Integer
  Dim lngSolicitud        As Long
  Dim lngCita             As Long
  Dim objPeticion         As clsSelPeticion
  Dim objActuacion        As clsSelActuacion
  Dim objFaseActual       As clsFasesActuacion
  Dim objRecActual        As clsRecursoFase
  Dim strValor            As String
  Dim vntBookmark         As Variant
  Dim intActiveRow        As Integer
  Dim lngIncidencia       As Long

  Dim intIndFases         As Integer
  Dim intIndRecursos      As Integer
  Dim intIndActuacion     As Integer
    
  Dim intIndColumn        As Integer
  Dim intChkIndex         As Integer
  On Error GoTo Error
  
  'Comprobaci�n de Fechas Validas
  If dtcDateCombo1(0).Date < objGen.GetDBDateTime Then
    Call objError.SetError(cwCodeMsg, "La Fecha Desde tiene que ser mayor o igual a la Fecha Actual")
    Call objError.Raise
    dtcDateCombo1(0).SetFocus
  End If
  If dtcDateCombo1(2).Date < objGen.GetDBDateTime Then
    Call objError.SetError(cwCodeMsg, "La Fecha Desde tiene que ser mayor o igual a la Fecha Actual")
    Call objError.Raise
    dtcDateCombo1(2).SetFocus
  End If
 
 'Compruebo si se han pasado las Preferencias
  If TabSSTab1(1).TabEnabled(1) = False Then
    dtcDateCombo1(2).Text = dtcDateCombo1(0).Text
    dtcDateCombo1(3).Text = dtcDateCombo1(1).Text
    MaskEdBox1(2).Text = MaskEdBox1(0).Text
    MaskEdBox1(3).Text = MaskEdBox1(1).Text
    For intChkIndex = 0 To 6
      chkCheck1(intChkIndex + 7) = chkCheck1(intChkIndex + 7)
    Next
    TabSSTab1(1).TabEnabled(1) = True
  End If

 
 
 
 'Proceso de Solicitud
  grdSSDBGrid1(0).Redraw = False
  vntBookmark = grdSSDBGrid1(0).FirstRow
  intActiveRow = grdSSDBGrid1(0).Row
  lngSolicitud = IIf(objSolicitud.lngSolicitud = 0 Or objSolicitud.strOpcion = ciOpcAgenda Or objSolicitud.strOpcion = ciOpcModificar Or objSolicitud.strOpcion = ciOpcRecitar, GetNewCode("Select max(ci31numsolicit) from CI3100"), objSolicitud.lngSolicitud)

  
  strSql = "Select CI31NumSolicit, CI31FecPrefDes, CI31FecPrefHas, CI31HorPrDesHH, " & _
                "CI31HorPrDesMM, CI31HorPrHasHH, CI31HorPrHasMM, CI31IndLunPref, " & _
                "CI31IndMarPref, CI31IndMiePref, CI31IndJuePref, CI31IndViePref, " & _
                "CI31IndSabPref, CI31IndDomPref " & _
                "from CI3100 where CI31NUMSOLICIT = " & lngSolicitud

  Set rsSolicitud = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
  
  'Si la solicitud no existe se crea una nueva y se asignan las
  'preferencias
 
  If rsSolicitud.RowCount = 0 Then
    rsSolicitud.AddNew
    rsSolicitud.rdoColumns("CI31NumSolicit") = lngSolicitud
  Else
    rsSolicitud.Edit
  End If
  rsSolicitud.rdoColumns("CI31FecPrefDes") = dtcDateCombo1(2).Date
  rsSolicitud.rdoColumns("CI31FecPrefHas") = dtcDateCombo1(3).Date
  If MaskEdBox1(2) <> "__:__" Then
    datFechaTemporal = Date & " " & MaskEdBox1(2)
    rsSolicitud.rdoColumns("CI31HorPrDesHH") = Format(datFechaTemporal, "HH")
    rsSolicitud.rdoColumns("CI31HorPrDesMM") = Format(datFechaTemporal, "NN")
  Else
    rsSolicitud.rdoColumns("CI31HorPrDesHH") = Null
    rsSolicitud.rdoColumns("CI31HorPrDesMM") = Null
  End If
  If MaskEdBox1(3) <> "__:__" Then
    datFechaTemporal = Date & " " & MaskEdBox1(3)
    rsSolicitud.rdoColumns("CI31HorPrHasHH") = Format(datFechaTemporal, "HH")
    rsSolicitud.rdoColumns("CI31HorPrHasMM") = Format(datFechaTemporal, "NN")
  Else
    rsSolicitud.rdoColumns("CI31HorPrHasHH") = Null
    rsSolicitud.rdoColumns("CI31HorPrHasMM") = Null
  End If
  rsSolicitud.rdoColumns("CI31IndLunPref") = chkCheck1(7)
  rsSolicitud.rdoColumns("CI31IndMarPref") = chkCheck1(8)
  rsSolicitud.rdoColumns("CI31IndMiePref") = chkCheck1(9)
  rsSolicitud.rdoColumns("CI31IndJuePref") = chkCheck1(10)
  rsSolicitud.rdoColumns("CI31IndViePref") = chkCheck1(11)
  rsSolicitud.rdoColumns("CI31IndSabPref") = chkCheck1(12)
  rsSolicitud.rdoColumns("CI31IndDomPref") = chkCheck1(13)
  rsSolicitud.Update
  rsSolicitud.Close
  
  'Actualizo la columna indice con el n�mero de solicitud
  grdSSDBGrid1(0).MoveFirst
  For Each objPeticion In objSolicitud.cllPeticiones
    For Each objActuacion In objPeticion.cllActuaciones
      For intRow = 0 To 3
         If intRow = 1 Then
           If Val(objActuacion.datFechaCita) = 0 Then
             objActuacion.datFechaCita = CDate(Format(grdSSDBGrid1(0).Columns("Asignado").Value, "DD/MM/YYYY"))
           End If
         End If
         If intRow = 2 Then
           If objActuacion.strHoraCita = "" Then
             objActuacion.strHoraCita = Format(CDate(grdSSDBGrid1(0).Columns("Asignado").Value), "HH:NN")
           End If
         End If
         If intRow = 2 Then
           grdSSDBGrid1(0).Columns("Indice").Value = lngSolicitud
         End If
         grdSSDBGrid1(0).MoveNext
      Next
    Next
  Next

  
  'Proceso de Actuaciones
  lngIncidencia = 0
  For Each objPeticion In objSolicitud.cllPeticiones
    For Each objActuacion In objPeticion.cllActuaciones
     
     'Proceso de Actuaciones si venimos por Ver Solicitud
     If objActuacion.lngCita <> 0 And objSolicitud.strOpcion = ciOpcVerSolicitud Then
        If objActuacion.strSituacion = ciAnulada Then
           If lngIncidencia = 0 Then
              lngIncidencia = AddInciden(ciCodInciRecitacion, True) 'esta variable se encuentra en el m�dulo general
           End If
           If lngIncidencia <> -1 And lngIncidencia <> 0 Then         'ha generado la incidencia n�mero lngIncidencia
                strSql = "Select ROWID, CI01SITCITA, AG05NUMINCIDEN " & _
                        "from CI0100 where CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & objActuacion.lngCita
                Set rsCitas = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
                If rsCitas.RowCount <> 0 Then
                   rsCitas.Edit
                   rsCitas.rdoColumns("CI01SITCITA") = ciAnulada
                   rsCitas.rdoColumns("AG05NUMINCIDEN") = lngIncidencia
                   rsCitas.Update
                End If
                rsCitas.Close
                objActuacion.lngCita = 0
           End If
        End If
     End If
     'Fin proceso Ver Solicitud
     
     
     lngCita = IIf(objActuacion.lngCita = 0 Or objSolicitud.lngSolicitud = 0, _
                   GetNewCode("Select max(CI01NUMCITA) from CI0100 where CI31NUMSOLICIT = " & lngSolicitud), objActuacion.lngCita)
     strSql = "Select CI31NUMSOLICIT, CI01NUMCITA, PR04NUMACTPLAN, CI01FECCONCERT, CI01SITCITA, " & _
              "CI01PERSCONFIR, CI01FECCONFIR, AG05NUMINCIDEN, CI01INDRECORDA, CI01TIPRECORDA, " & _
              "CI01FECEMIRECO, CI01FECEMIRECO, CI01NUMENVRECO, CI01FECCOMRECI, CI01INDLISESPE, " & _
              "AG11CODRECURSO from CI0100 where CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngCita

      Set rsCitas = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
      'Si la Actuaci�n no existe se crea una nueva
      If rsCitas.RowCount = 0 Then
         rsCitas.AddNew
         rsCitas.rdoColumns("CI31NUMSOLICIT").Value = lngSolicitud
         rsCitas.rdoColumns("CI01NUMCITA").Value = lngCita
        
      Else
         
         rsCitas.Edit
      End If
      rsCitas.rdoColumns("PR04NUMACTPLAN").Value = objActuacion.lngNumActPlan
      datFechaConcert = Format(objActuacion.datFechaCita & " " & objActuacion.strHoraCita, "dd/mm/yyyy hh:nn:ss")
      rsCitas.rdoColumns("CI01FECCONCERT").Value = datFechaConcert
      rsCitas.rdoColumns("AG11CODRECURSO").Value = objActuacion.lngRecurso
      rsCitas.rdoColumns("CI01SITCITA").Value = ciConfirmada
      rsCitas.rdoColumns("CI01PERSCONFIR").Value = objSecurity.strFullName
      rsCitas.rdoColumns("CI01FECCONFIR").Value = objGen.GetDBDateTime
      rsCitas.rdoColumns("CI01INDRECORDA").Value = objActuacion.intRecordatorio                       '#101
      rsCitas.rdoColumns("CI01TIPRECORDA").Value = ciRecordaMail          '#102
      rsCitas.rdoColumns("CI01NUMENVRECO").Value = 1                      '#103
      rsCitas.rdoColumns("CI01INDLISESPE").Value = objActuacion.intLisEspera                    '#0
      rsCitas.Update
      rsCitas.Close
      
      '#109 Actualizar el recurso preferente de cada actuaci�n en las tablas PR0400 Y PR0300
      'If objActuacion.lngRecursoPref > 0 Then
      '  strSql = "SELECT PR04NUMACTPLAN, AG11CODRECURSO FROM PR0400 WHERE PR04NUMACTPLAN=" & objActuacion.lngNumActPlan
      '  Set rsActPedPlan = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
      '  If Not rsActPedPlan.EOF Then
      '    rsActPedPlan("AG11CODRECURSO") = objActuacion.lngRecursoPref
      '    rsActPedPlan.Close
      '  End If
      
      '  strSql = "SELECT PR03NUMACTPEDI, AG11CODRECURSO FROM PR0300 WHERE PR03NUMACTPEDI=" & objActuacion.lngNumActPedi
      '  Set rsActPedPlan = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
      '  If Not rsActPedPlan.EOF Then
      '    rsActPedPlan("AG11CODRECURSO") = objActuacion.lngRecursoPref
      '    rsActPedPlan.Close
      '  End If
      '  Set rsActPedPlan = Nothing
      'End If
     
     'Proceso de las FASES
      strSql = "select * from CI1500 where CI31NumSolicit = " & lngSolicitud & " and CI01NUMCITA = " & lngCita
      Set rsFases = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurRowVer)
      If objActuacion.lngCita = lngCita Then
         'ojo tratamiento cuando ya existen las fases
         '#0
         
         '*************************************
         'Mucho Cuidado con las ORDER BY !!!!!
         '*************************************
         
         intIndFases = 0
         
         Do While Not rsFases.EOF
           Set objFaseActual = objActuacion.GetFase(intIndFases)
           If objFaseActual Is Nothing Then
           'No se Ha encontrado esa Fase
           Else
             
             rsFases("CI15FECCONCPAC") = objFaseActual.datFechaFase
             rsFases("CI15NUMDIASPAC") = objFaseActual.intNumDiasPac
             rsFases("CI15NUMHORAPAC") = objFaseActual.intNumHoraPac
             rsFases("CI15NUMMINUPAC") = objFaseActual.intNumMinPac
             rsFases.Update
             strSql = "select * from CI2700 where CI31NUMSOLICIT = " & rsFases("CI31NUMSOLICIT") & " and CI01NUMCITA = " & rsFases("CI01NUMCITA") & " and CI15NUMFASECITA = " & rsFases("CI15NUMFASECITA")
             Set rsRecursosCitados = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
             intIndRecursos = 0
             Do While Not rsRecursosCitados.EOF
                Set objRecActual = objFaseActual.GetRecurso(intIndRecursos)
      
                If objRecActual Is Nothing Then
                 'No se Ha encontrado ese recurso
                Else
                  If rsFases.rdoColumns("CI31NumSolicit").Value <> lngNumSolicitud Then
                    rsFases.rdoColumns("CI31NumSolicit").Value = lngNumSolicitud
                    rsFases.rdoColumns("CI01NumCita").Value = lngCita
                  End If
                  
                  rsRecursosCitados("AG11CODRECURSO") = objRecActual.lngCodRecursoFase
                  rsRecursosCitados("CI27FECOCUPREC") = objRecActual.datFechaRecurso
                  rsRecursosCitados("CI27NUMDIASREC") = objRecActual.intNumDiasRec
                  rsRecursosCitados("CI27NUMHORAREC") = objRecActual.intNumHoraRec
                  rsRecursosCitados("CI27NUMMINUREC") = objRecActual.intNumMinRec
                  rsRecursosCitados.Update
               End If
               rsRecursosCitados.MoveNext
               intIndRecursos = intIndRecursos + 1
             Loop
           End If
           intIndFases = intIndFases + 1
           rsFases.MoveNext
         Loop
      Else
         'Tratamiento cuando no existan Fases
        ' For intIndFases = 0 To objIlog.GetNumPhases(intIndActuacion) - 1
         For intIndFases = 0 To objActuacion.cllFasesActuacion.Count - 1
           Set objFaseActual = objActuacion.GetFase(intIndFases)
           If objFaseActual Is Nothing Then
           'No se Ha encontrado esa Fase
           Else

             rsFases.AddNew
             rsFases.rdoColumns("CI31NumSolicit").Value = lngSolicitud
             rsFases.rdoColumns("CI01NumCita").Value = lngCita
             rsFases.rdoColumns("CI15NumFaseCita").Value = objFaseActual.intNumFase
             rsFases.rdoColumns("CI15DesFaseCita").Value = objFaseActual.strDesFase
             rsFases.rdoColumns("CI15FecConcPac").Value = objFaseActual.datFechaFase
             rsFases.rdoColumns("CI15NumDiasPac").Value = objFaseActual.intNumDiasPac
             rsFases.rdoColumns("CI15NumHoraPac").Value = objFaseActual.intNumHoraPac
             rsFases.rdoColumns("CI15NumMinuPac").Value = objFaseActual.intNumMinPac
             rsFases.Update
             
             'RECURSOS CITADOS
             strSql = "select * from CI2700 where CI31NUMSOLICIT = 0" '" & lngSolicitud & " and CI01NUMCITA = " & lngCita & " and CI15NUMFASECITA = " & rsFasesPedidas.rdoColumns("PR06NUMFASE").Value
             Set rsRecursos = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
         
            
             For intIndRecursos = 0 To objFaseActual.cllRecursosFase.Count - 1
                Set objRecActual = objFaseActual.GetRecurso(intIndRecursos)
      
                If objRecActual Is Nothing Then
                 'No se Ha encontrado ese recurso
                Else
                  rsRecursos.AddNew
                  rsRecursos.rdoColumns("ci31numsolicit").Value = lngSolicitud
                  rsRecursos.rdoColumns("CI01NumCita").Value = lngCita
                  rsRecursos.rdoColumns("CI15NUMFASECITA").Value = objFaseActual.intNumFase
                  rsRecursos.rdoColumns("AG11CODRECURSO").Value = objRecActual.lngCodRecursoFase
                  rsRecursos.rdoColumns("CI27FECOCUPREC").Value = objRecActual.datFechaRecurso
                  rsRecursos.rdoColumns("CI27NUMDIASREC").Value = objRecActual.intNumDiasRec
                  rsRecursos.rdoColumns("CI27NUMHORAREC").Value = objRecActual.intNumHoraRec
                  rsRecursos.rdoColumns("CI27NUMMINUREC").Value = objRecActual.intNumMinRec
                  rsRecursos.Update
                End If
             Next intIndRecursos
            End If
          Next intIndFases
          rsRecursos.Close
        End If
        rsFases.Close
        objActuacion.lngCita = lngCita
    Next
  Next
  objSolicitud.lngSolicitud = lngSolicitud
  objSolicitud.strOpcion = ciOpcVerSolicitud  'ya se ha grabado
  Set rsSolicitud = Nothing
  Set objPeticion = Nothing
  Set objActuacion = Nothing
  Set rsFases = Nothing
  Set rsFasesPedidas = Nothing
  Set rsRecursos = Nothing
   
  grdSSDBGrid1(0).FirstRow = vntBookmark
  'grdSSDBGrid1(0).Row = intActiveRow
  'For intIndColumn = 5 To grdSSDBGrid1(0).Cols - 2
  '  Call grdSSDBGrid1(0).Columns.Remove(intIndColumn)
  'Next
  grdSSDBGrid1(0).Redraw = True

  Exit Sub
Error:
  MsgBox Err.Number & " - " & Err.Description, vbCritical
  Resume Next
End Sub

Private Sub SolicitudAdd2()
  Dim strSql              As String
  Dim rsSolicitud         As rdoResultset
  Dim rsCitas             As rdoResultset
  Dim rsFases             As rdoResultset
  Dim rsFasesPedidas      As rdoResultset
  Dim rsRecursos          As rdoResultset
  Dim rsRecursosPedidos   As rdoResultset
  Dim rsRecursosMaestro   As rdoResultset
  Dim intCantRecu         As Integer
  Dim datFechaConcert     As Date
  Dim datFechaTemporal    As Date
  Dim intRow              As Integer
  Dim lngSolicitud        As Long
  Dim lngCita             As Long
  Dim objPeticion         As clsSelPeticion
  Dim objActuacion        As clsSelActuacion
  Dim strValor            As String
  Dim vntBookmark         As Variant
  Dim intActiveRow        As Integer
  Dim lngIncidencia       As Long

  
  If dtcDateCombo1(0).Date < objGen.GetDBDateTime Then
    Call objError.SetError(cwCodeMsg, "La Fecha Desde tiene que ser mayor o igual a la Fecha Actual")
    Call objError.Raise
    dtcDateCombo1(0).SetFocus
  End If
  If dtcDateCombo1(2).Date < objGen.GetDBDateTime Then
    Call objError.SetError(cwCodeMsg, "La Fecha Desde tiene que ser mayor o igual a la Fecha Actual")
    Call objError.Raise
    dtcDateCombo1(2).SetFocus
  End If

  
  grdSSDBGrid1(0).Redraw = False
  vntBookmark = grdSSDBGrid1(0).FirstRow
  intActiveRow = grdSSDBGrid1(0).Row
  lngSolicitud = IIf(objSolicitud.lngSolicitud = 0 Or objSolicitud.strOpcion = ciOpcAgenda Or objSolicitud.strOpcion = ciOpcModificar Or objSolicitud.strOpcion = ciOpcRecitar, GetNewCode("Select max(ci31numsolicit) from CI3100"), objSolicitud.lngSolicitud)

  strSql = "Select CI31NumSolicit, CI31FecPrefDes, CI31FecPrefHas, CI31HorPrDesHH, " & _
                "CI31HorPrDesMM, CI31HorPrHasHH, CI31HorPrHasMM, CI31IndLunPref, " & _
                "CI31IndMarPref, CI31IndMiePref, CI31IndJuePref, CI31IndViePref, " & _
                "CI31IndSabPref, CI31IndDomPref " & _
                "from CI3100 where CI31NUMSOLICIT = " & lngSolicitud

  Set rsSolicitud = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
  
  If rsSolicitud.RowCount = 0 Then
    rsSolicitud.AddNew
    rsSolicitud.rdoColumns("CI31NumSolicit") = lngSolicitud
  Else
    rsSolicitud.Edit
  End If
  rsSolicitud.rdoColumns("CI31FecPrefDes") = dtcDateCombo1(2)
  rsSolicitud.rdoColumns("CI31FecPrefHas") = dtcDateCombo1(3)
  If MaskEdBox1(2) <> "__:__" Then
    datFechaTemporal = Date & " " & MaskEdBox1(2)
    rsSolicitud.rdoColumns("CI31HorPrDesHH") = Format(datFechaTemporal, "HH")
    rsSolicitud.rdoColumns("CI31HorPrDesMM") = Format(datFechaTemporal, "NN")
  Else
    rsSolicitud.rdoColumns("CI31HorPrDesHH") = Null
    rsSolicitud.rdoColumns("CI31HorPrDesMM") = Null
  End If
  If MaskEdBox1(3) <> "__:__" Then
    datFechaTemporal = Date & " " & MaskEdBox1(3)
    rsSolicitud.rdoColumns("CI31HorPrHasHH") = Format(datFechaTemporal, "HH")
    rsSolicitud.rdoColumns("CI31HorPrHasMM") = Format(datFechaTemporal, "NN")
  Else
    rsSolicitud.rdoColumns("CI31HorPrHasHH") = Null
    rsSolicitud.rdoColumns("CI31HorPrHasMM") = Null
  End If
  rsSolicitud.rdoColumns("CI31IndLunPref") = chkCheck1(7)
  rsSolicitud.rdoColumns("CI31IndMarPref") = chkCheck1(8)
  rsSolicitud.rdoColumns("CI31IndMiePref") = chkCheck1(9)
  rsSolicitud.rdoColumns("CI31IndJuePref") = chkCheck1(10)
  rsSolicitud.rdoColumns("CI31IndViePref") = chkCheck1(11)
  rsSolicitud.rdoColumns("CI31IndSabPref") = chkCheck1(12)
  rsSolicitud.rdoColumns("CI31IndDomPref") = chkCheck1(13)
  rsSolicitud.Update
  rsSolicitud.Close
  grdSSDBGrid1(0).MoveFirst
  For Each objPeticion In objSolicitud.cllPeticiones
    For Each objActuacion In objPeticion.cllActuaciones
      For intRow = 0 To 3
         If intRow = 0 Then
            strValor = grdSSDBGrid1(0).Columns("Asignado").Value
            If InStr(1, strValor, "/#") <> 0 Then
               If objActuacion.lngRecurso <> Mid(strValor, InStr(1, strValor, "/#") + 2, InStr(1, strValor, "#/") - InStr(1, strValor, "/#") - 2) Then
                  objActuacion.lngRecurso = Mid(strValor, InStr(1, strValor, "/#") + 2, InStr(1, strValor, "#/") - InStr(1, strValor, "/#") - 2)
                  objActuacion.strSituacion = ciAnulada 'ha variado y ser� anulada y recitada si se viene de Versolicitud
               End If
            End If
         End If
         If intRow = 1 Then
            If objActuacion.datFechaCita <> grdSSDBGrid1(0).Columns("Asignado").Value Then
               objActuacion.datFechaCita = grdSSDBGrid1(0).Columns("Asignado").Value
               objActuacion.strSituacion = ciAnulada 'ha variado y ser� anulada y recitada si se viene de Versolicitud
            End If
         End If
         If intRow = 2 Then
            If objActuacion.strHoraCita <> grdSSDBGrid1(0).Columns("Asignado").Text Then
               objActuacion.strHoraCita = grdSSDBGrid1(0).Columns("Asignado").Text
               objActuacion.strSituacion = ciAnulada 'ha variado y ser� anulada y recitada si se viene de Versolicitud
            End If
            grdSSDBGrid1(0).Columns("Indice").Value = lngSolicitud
         End If
          grdSSDBGrid1(0).MoveNext
      Next
    Next
  Next
  lngIncidencia = 0
  For Each objPeticion In objSolicitud.cllPeticiones
    For Each objActuacion In objPeticion.cllActuaciones
     
     If objActuacion.lngCita <> 0 And objSolicitud.strOpcion = ciOpcVerSolicitud Then
        If objActuacion.strSituacion = ciAnulada Then
           If lngIncidencia = 0 Then
              lngIncidencia = AddInciden(ciCodInciRecitacion, True) 'esta variable se encuentra en el m�dulo general
           End If
           If lngIncidencia <> -1 And lngIncidencia <> 0 Then         'ha generado la incidencia n�mero lngIncidencia
                strSql = "Select ROWID, CI01SITCITA, AG05NUMINCIDEN " & _
                        "from CI0100 where CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & objActuacion.lngCita
                Set rsCitas = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
                If rsCitas.RowCount <> 0 Then
                   rsCitas.Edit
                   rsCitas.rdoColumns("CI01SITCITA") = ciAnulada
                   rsCitas.rdoColumns("AG05NUMINCIDEN") = lngIncidencia
                   rsCitas.Update
                End If
                rsCitas.Close
                objActuacion.lngCita = 0
           End If
        End If
     End If
     
     lngCita = IIf(objActuacion.lngCita = 0 Or objSolicitud.lngSolicitud = 0, _
                   GetNewCode("Select max(CI01NUMCITA) from CI0100 where CI31NUMSOLICIT = " & lngSolicitud), objActuacion.lngCita)
     strSql = "Select CI31NUMSOLICIT, CI01NUMCITA, PR04NUMACTPLAN, CI01FECCONCERT, CI01SITCITA, " & _
              "CI01PERSCONFIR, CI01FECCONFIR, AG05NUMINCIDEN, CI01INDRECORDA, CI01TIPRECORDA, " & _
              "CI01FECEMIRECO, CI01FECEMIRECO, CI01NUMENVRECO, CI01FECCOMRECI, CI01INDLISESPE, " & _
              "AG11CODRECURSO from CI0100 where CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngCita

      Set rsCitas = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
         
      If rsCitas.RowCount = 0 Then
         rsCitas.AddNew
         rsCitas.rdoColumns("CI31NUMSOLICIT").Value = lngSolicitud
         rsCitas.rdoColumns("CI01NUMCITA").Value = lngCita
      Else
         
         rsCitas.Edit
      End If
      rsCitas.rdoColumns("PR04NUMACTPLAN").Value = objActuacion.lngNumActPlan
      datFechaConcert = Format(objActuacion.datFechaCita & " " & objActuacion.strHoraCita, "dd/mm/yyyy hh:nn:ss")
      rsCitas.rdoColumns("CI01FECCONCERT").Value = datFechaConcert
      rsCitas.rdoColumns("AG11CODRECURSO").Value = objActuacion.lngRecurso
      rsCitas.rdoColumns("CI01SITCITA").Value = ciConfirmada
      rsCitas.rdoColumns("CI01PERSCONFIR").Value = objSecurity.strFullName
      rsCitas.rdoColumns("CI01FECCONFIR").Value = objGen.GetDBDateTime
      rsCitas.rdoColumns("CI01INDRECORDA").Value = objActuacion.intRecordatorio                       '#101
      rsCitas.rdoColumns("CI01TIPRECORDA").Value = ciRecordaMail          '#102
      rsCitas.rdoColumns("CI01NUMENVRECO").Value = 1                      '#103
      rsCitas.rdoColumns("CI01INDLISESPE").Value = objActuacion.intLisEspera                    '#0
      '#109 Actualizar el recurso preferente de cada actuaci�n en las tablas PR0400 Y PR0300
      rsCitas.Update
      rsCitas.Close
      'FASES
      strSql = "select * from CI1500 where CI31NumSolicit = " & lngSolicitud & " and CI01NUMCITA = " & lngCita
      Set rsFases = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
      If objActuacion.lngCita = lngCita Then
         'ojo tratamiento cuando ya existen las fases
         '#0
      Else
         strSql = "select * from PR0600 where PR03NUMACTPEDI = " & objActuacion.lngNumActPedi
         Set rsFasesPedidas = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
         While Not rsFasesPedidas.EOF
            rsFases.AddNew
            rsFases.rdoColumns("CI31NumSolicit").Value = lngSolicitud
            rsFases.rdoColumns("CI01NumCita").Value = lngCita
            rsFases.rdoColumns("CI15NumFaseCita").Value = rsFasesPedidas.rdoColumns("PR06NUMFASE").Value
            rsFases.rdoColumns("CI15DesFaseCita").Value = rsFasesPedidas.rdoColumns("PR06DESFASE").Value
            datFechaConcert = Format(objActuacion.datFechaCita & " " & objActuacion.strHoraCita, "dd/mm/yyyy hh:nn:ss")
            rsFases.rdoColumns("CI15FecConcPac").Value = datFechaConcert       '#105
            rsFases.rdoColumns("CI15NumDiasPac").Value = Int(rsFasesPedidas.rdoColumns("PR06NUMMINOCUPAC").Value / (60 * 24))       '#105
            rsFases.rdoColumns("CI15NumHoraPac").Value = (rsFasesPedidas.rdoColumns("PR06NUMMINOCUPAC").Value Mod (60 * 24)) / 60     '#105
            rsFases.rdoColumns("CI15NumMinuPac").Value = (rsFasesPedidas.rdoColumns("PR06NUMMINOCUPAC").Value Mod 60 * 24) Mod 60   '#105
            rsFases.Update
            'RECURSOS CITADOS
            strSql = "select * from CI2700 where CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngCita & " and CI15NUMFASECITA = " & rsFasesPedidas.rdoColumns("PR06NUMFASE").Value
            Set rsRecursos = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
         
            strSql = "select * from PR1400 where PR03NUMACTPEDI = " & objActuacion.lngNumActPedi & " AND PR06NUMFASE = " & rsFasesPedidas.rdoColumns("PR06NUMFASE")
            Set rsRecursosPedidos = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
            While Not rsRecursosPedidos.EOF
               strSql = "select * from AG1100 where AG14CODTIPRECU = " & rsRecursosPedidos.rdoColumns("AG14CODTIPRECU")
               Set rsRecursosMaestro = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
               If rsRecursosMaestro.RowCount > 0 Then
                  intCantRecu = 1 ' rsRecursosPedidos.rdoColumns("PR14NUMUNIREC").Value
                  Do While Not rsRecursosMaestro.EOF
                        rsRecursos.AddNew
                        rsRecursos.rdoColumns("ci31numsolicit").Value = lngSolicitud
                        rsRecursos.rdoColumns("CI01NumCita").Value = lngCita
                        rsRecursos.rdoColumns("CI15NUMFASECITA").Value = rsFasesPedidas.rdoColumns("PR06NUMFASE")
                        rsRecursos.rdoColumns("AG11CODRECURSO").Value = rsRecursosMaestro.rdoColumns("AG11CODRECURSO")    '#108
                     
                        datFechaConcert = Format(objActuacion.datFechaCita & " " & objActuacion.strHoraCita, "dd/mm/yyyy hh:nn:ss")   '#106
                        
                        rsRecursos.rdoColumns("CI27FECOCUPREC").Value = datFechaConcert      '#106
                        If Not IsNull(rsRecursosPedidos.rdoColumns("PR14NUMMINOCU").Value) Then
                           rsRecursos.rdoColumns("CI27NUMDIASREC").Value = Int(rsRecursosPedidos.rdoColumns("PR14NUMMINOCU").Value / (60 * 24))         '#107
                           rsRecursos.rdoColumns("CI27NUMHORAREC").Value = (rsRecursosPedidos.rdoColumns("PR14NUMMINOCU").Value Mod (60 * 24)) / 60     '#107
                           rsRecursos.rdoColumns("CI27NUMMINUREC").Value = (rsRecursosPedidos.rdoColumns("PR14NUMMINOCU").Value Mod 60 * 24) Mod 60  '#107
                        Else
                           rsRecursos.rdoColumns("CI27NUMDIASREC").Value = 0         '#107
                           rsRecursos.rdoColumns("CI27NUMHORAREC").Value = 0         '#107
                           rsRecursos.rdoColumns("CI27NUMMINUREC").Value = 0         '#107
                        End If
                        rsRecursos.Update
                        intCantRecu = intCantRecu + 1
                        If intCantRecu > rsRecursosPedidos.rdoColumns("PR14NUMUNIREC").Value Then
                           Exit Do
                        End If
                        rsRecursosMaestro.MoveNext
                  Loop
               End If
               rsRecursosMaestro.Close
               rsRecursosPedidos.MoveNext
            Wend
            '
            rsRecursos.Close
            rsRecursosPedidos.Close
            
            rsFasesPedidas.MoveNext
         Wend
         rsFasesPedidas.Close
      End If
      rsFases.Close
    Next
  Next
  objSolicitud.lngSolicitud = lngSolicitud
  objSolicitud.strOpcion = ciOpcVerSolicitud  'ya se ha grabado
  Set rsSolicitud = Nothing
  Set objPeticion = Nothing
  Set objActuacion = Nothing
  Set rsFases = Nothing
  Set rsFasesPedidas = Nothing
  Set rsRecursos = Nothing
  Set rsRecursosPedidos = Nothing
  Set rsRecursosMaestro = Nothing
   
  grdSSDBGrid1(0).FirstRow = vntBookmark
  grdSSDBGrid1(0).Row = intActiveRow
  grdSSDBGrid1(0).Redraw = True

End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  If intIndex < 7 And blnSolutionAsked Then
    blnNewSolutions = True
    Call CambioSoluciones(False)
  End If
End Sub

Private Sub cmdAgenda_Click(intIndex As Integer)
  Dim strWhereRecursos(2)        As Variant
  Dim objPeticion                As clsSelPeticion
  Dim objActuacion               As clsSelActuacion
   
  strWhereRecursos(1) = ""
  strWhereRecursos(2) = ""
  
  For Each objPeticion In objSolicitud.cllPeticiones
    For Each objActuacion In objPeticion.cllActuaciones
       If objActuacion.lngRecurso > 0 Then
         strWhereRecursos(1) = "," & objActuacion.lngRecurso & strWhereRecursos(1)
       End If
       If objActuacion.lngRecursoPref > 0 Then
         strWhereRecursos(1) = "," & objActuacion.lngRecursoPref & strWhereRecursos(1)
       End If
    Next
  Next
  If strWhereRecursos(1) <> "" Then
    strWhereRecursos(1) = Mid(strWhereRecursos(1), 2)
    strWhereRecursos(1) = "AG11CODRECURSO IN ( " & strWhereRecursos(1) & " )"
  
    If TabSSTab1(0).Tab = 1 Then
      If grdSSDBGrid1(1).Columns("Recurso").Value <> "" Then
        strWhereRecursos(2) = "AG11CODRECURSO=" & Val(GetTableColumn("SELECT AG11CODRECURSO FROM AG1100 WHERE AG11DESRECURSO='" & grdSSDBGrid1(1).Columns("Recurso").Value & "'")("AG11CODRECURSO"))
      End If
    End If
    
 End If
 'llamada a Agenda de recursos
 Call objSecurity.LaunchProcess("AG1008", strWhereRecursos())

End Sub

Private Sub cmdCitar_Click(Index As Integer)
    'Versi�n EXE
    Call CitarSinPlanificar
    
    'Versi�n DLL
    'Call objSecurity.LaunchProcess(ciCitarSinPlanificar)
End Sub

Private Sub cmdConfirmarSolicitud_Click(Index As Integer)
  Dim blnCancelTrans          As Boolean
  Dim blnSolicitudIsOk        As Boolean
  Dim vntFirstRow             As Variant
  Dim lngRespuestaIlog        As Long
  
  objApp.rdoConnect.QueryTimeout = 600
  
  Me.MousePointer = vbHourglass
  vntFirstRow = grdSSDBGrid1(0).FirstRow
  grdSSDBGrid1(0).Redraw = False

  blnSolicitudIsOk = SolicitudIsOk
  If blnSolicitudIsOk Then
    'Actualizo los datos de cada actuaci�n
           
    Call objIlog.Initialize
    Call ActualizarDatosIlog
    lngRespuestaIlog = objIlog.Confirm
'    MsgBox "Respuesta Ilog: " & lngRespuestaIlog
    
    If lngRespuestaIlog = 1 Then
    
    
      objApp.rdoConnect.BeginTrans
      On Error GoTo Canceltrans

      Call SolicitudAdd

     If blnCancelTrans Then
        objApp.rdoConnect.RollbackTrans
        'Call objError.SetError(cwCodeMsg, ciErrRollback & Err.Description)
        Call objError.SetError(cwCodeMsg, "Confirmaci�n Cancelada")
        Call objError.Raise
     Else
        objApp.rdoConnect.CommitTrans
        fraFrame1(2).Caption = "Solicitud N� " & Str(objSolicitud.lngSolicitud)
        MsgBox ciMsgConfirmarOk, vbInformation, "SOLICITUD N� " & Str(objSolicitud.lngSolicitud)
        blnConfirmarCitas = False
        blnCommit = True
        'Call objLoadFromSolicit(objSolicitud.lngSolicitud, ciOpcVerSolicitud)
        'grdSSDBGrid1(0).Redraw = False
        'Call SolicitudRefresh
        'grdSSDBGrid1(0).Redraw = True
        'cmdCitar(0).Enabled = True
     End If
   Else
     Me.MousePointer = vbDefault
     If lngRespuestaIlog = 0 Then
       MsgBox "No se ha podido confirmar la solicitud. Vuelva a a seleccionar otra soluci�n.", vbInformation
     Else
       Call RaiseIlogError(lngRespuestaIlog)
       blnNewSolutions = True
       Call CambioSoluciones(False)
     End If
   End If
   
  End If
  grdSSDBGrid1(0).FirstRow = vntFirstRow
  grdSSDBGrid1(0).Redraw = True
  Me.MousePointer = vbDefault
  objApp.rdoConnect.QueryTimeout = 30
  
  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next
      
End Sub


Private Sub cmdDietario_Click(intIndex As Integer)
  Dim strWhereRecursos(2)        As Variant
  Dim objPeticion                As clsSelPeticion
  Dim objActuacion               As clsSelActuacion
   
  strWhereRecursos(1) = ""
  strWhereRecursos(2) = ""
  
  For Each objPeticion In objSolicitud.cllPeticiones
    For Each objActuacion In objPeticion.cllActuaciones
       If objActuacion.lngRecurso > 0 Then
         strWhereRecursos(1) = "," & objActuacion.lngRecurso & strWhereRecursos(1)
       End If
       If objActuacion.lngRecursoPref > 0 Then
         strWhereRecursos(1) = "," & objActuacion.lngRecursoPref & strWhereRecursos(1)
       End If
    Next
  Next
  If strWhereRecursos(1) <> "" Then
    strWhereRecursos(1) = Mid(strWhereRecursos(1), 2)
    strWhereRecursos(1) = "AG11CODRECURSO IN ( " & strWhereRecursos(1) & " )"
  
    If TabSSTab1(0).Tab = 1 Then
      If grdSSDBGrid1(1).Columns("Recurso").Value <> "" Then
        strWhereRecursos(2) = "AG11CODRECURSO=" & Val(GetTableColumn("SELECT AG11CODRECURSO FROM AG1100 WHERE AG11DESRECURSO='" & grdSSDBGrid1(1).Columns("Recurso").Value & "'")("AG11CODRECURSO"))
      End If
    End If
 End If
 'llamada a dietario de recursos
 
 Call objSecurity.LaunchProcess("AG1009", strWhereRecursos())
End Sub

Private Sub cmdPedirSoluciones_Click(intIndex As Integer)
  Dim objPeticion As clsSelPeticion
  Dim objActuacion As clsSelActuacion
  Dim blnCancelTrans        As Boolean
  Dim intPos                As Integer
  Dim blnActiveCheck        As Boolean
  Dim vntBook As Variant
  Dim objCol As Object
  Dim intNumSol As Integer
'  objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans
  
  
  Me.MousePointer = vbHourglass
  'Comprobaciones de Fechas Validas
  
  If dtcDateCombo1(1).Date = Null Then
    dtcDateCombo1(1).Date = DateAdd("d", ciIntervaloFechas, dtcDateCombo1(0).Date)
  End If
  If dtcDateCombo1(3).Date = Null Then
    dtcDateCombo1(3).Date = DateAdd("d", ciIntervaloFechas, dtcDateCombo1(0).Date)
  End If
  
  If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) < 0 Then
    Call objError.SetError(cwCodeMsg, "La Fecha Desde no puede ser mayor que la Fecha Hasta.")
    Call objError.Raise
    dtcDateCombo1(0).SetFocus
    Exit Sub
  End If
  If DateDiff("d", CDate(objGen.GetDBDateTime), dtcDateCombo1(0).Date) < 0 Then
    Call objError.SetError(cwCodeMsg, "La Fecha Desde no puede ser menor que la Fecha Actual.")
    Call objError.Raise
    dtcDateCombo1(0).SetFocus
    Exit Sub
  Else
    If DateDiff("d", dtcDateCombo1(0).Date, dtcDateCombo1(1).Date) <= ciIntervaloFechas Then
      
      'Comprobamos que se ha seleccionado alg�n dia de la semana
      For intPos = 0 To 6
        If chkCheck1(intPos).Value = 1 Then
          blnActiveCheck = True
          Exit For
        End If
      Next intPos
      
      If Not blnActiveCheck Then
        Call objError.SetError(cwCodeMsg, "No se ha seleccionado ning�n d�a de la semana.")
        Call objError.Raise
        Exit Sub
      End If
      
      'Una vez comprobadas las fechas se asignan preferencias y se llama
      ' a la agenda
      If grdSSDBGrid1(0).Cols > 6 And blnNewSolutions Then
      '  grdSSDBGrid1(0).SplitterPos = grdSSDBGrid1(0).Cols
      '  intNumSol = grdSSDBGrid1(0).Cols - 6
      '  While intNumSol > 0
      '    Call grdSSDBGrid1(0).Columns.Remove("Soluci�n" & intNumSol)
      '    intNumSol = intNumSol - 1
      '  Wend
                                  
        
      '  For Each objPeticion In objSolicitud.cllPeticiones
      '    For Each objActuacion In objPeticion.cllActuaciones
      '      Call objGen.RemoveCollection(objActuacion.cllSoluciones)
      '    Next
      '  Next
        blnSolutionAsked = True
        intTotSols = 0
      End If
      If blnNoSolutions Then
       For Each objPeticion In objSolicitud.cllPeticiones
          For Each objActuacion In objPeticion.cllActuaciones
            Call objGen.RemoveCollection(objActuacion.cllSoluciones)
          Next
        Next
        blnNoSolutions = False
      End If
      If blnNewSolutions Then
        blnNoSolutions = False
        cmdConfirmarSolicitud(0).Enabled = False
        'cmdCitar(0).Enabled = False
         
        'grdSSDBGrid1(0).Columns("Asignado").RemoveAll
        grdSSDBGrid1(0).Redraw = False
        
        grdSSDBGrid1(0).MoveFirst
        For intPos = 1 To grdSSDBGrid1(0).Rows
          grdSSDBGrid1(0).Columns("Asignado").Value = ""
          grdSSDBGrid1(0).MoveNext
        Next intPos
        
        grdSSDBGrid1(0).Redraw = True
        grdSSDBGrid1(0).MoveFirst
        Call AsignPreferences
      End If
      If Not CallAgenda(Val(txtText1(6).Text)) Then
        Me.MousePointer = vbDefault
        Exit Sub
      End If
    Else
      Call objError.SetError(cwCodeMsg, "La Fecha Hasta excede el n�mero m�ximo de d�as estudio.")
      Call objError.Raise
      dtcDateCombo1(0).SetFocus
      Me.MousePointer = vbDefault
      Exit Sub
    End If
  End If
  
  
  If blnCancelTrans Then
 '   objApp.rdoConnect.RollbackTrans
    Call objError.SetError(cwCodeMsg, ciErrRollback)
    Call objError.Raise
  Else
'    objApp.rdoConnect.CommitTrans
    If grdSSDBGrid1(0).Cols > 6 Then
      blnFasesPorObj = True
    End If

  End If
  Me.MousePointer = vbDefault
  Exit Sub

Canceltrans:
   blnCancelTrans = True
   MsgBox "Error : " & Err.Number & "-" & Err.Description
   Resume Next
     
End Sub

Private Sub cmdRecordatorio_Click(Index As Integer)
  If blnConfirmarCitas Then
    Call objError.SetError(cwCodeQuery, "Los cambios realizados en las Citas no han sido confirmados." & vbCrLf & "�Desea confirmar las modificaciones?")
    If objError.Raise = vbYes Then
      Call cmdConfirmarSolicitud_Click(0)
      blnCommit = True
    Else
      blnCommit = False
    End If
  End If
  If objSolicitud.lngSolicitud > 0 Then
   'Versi�n EXE
   ' Load frmRecordatorio
   ' With frmRecordatorio
   '   .lngNumSolicit = objSolicitud.lngSolicitud
   '   .dtcDateCombo1(0).Date = ""
   '   .dtcDateCombo1(1).Date = ""
   '   .dtcDateCombo1(0).BackColor = objApp.objColor.lngReadOnly
   '   .dtcDateCombo1(1).BackColor = objApp.objColor.lngReadOnly
   '   .dtcDateCombo1(0).Enabled = False
   '   .dtcDateCombo1(1).Enabled = False
   '   .txtText1(0).BackColor = objApp.objColor.lngReadOnly
   '   .txtText1(1).BackColor = objApp.objColor.lngReadOnly
   '   .txtText1(2).BackColor = objApp.objColor.lngReadOnly
   '   .txtText1(4).BackColor = objApp.objColor.lngReadOnly
   '   .chkCheck1(0).Enabled = False
   '   .chkCheck1(1).Enabled = False
   '   .cmdCommand1(0).Enabled = False
   '   .cmdCommand1(1).Enabled = False
   '   .cmdCommand1(2).Enabled = False
   '   .cboSSDBCombo1(0).BackColor = objApp.objColor.lngReadOnly
   '   .cboSSDBCombo1(0).Enabled = False
   '   .fraFrame1(0).Enabled = False
   '   .fraFrame1(2).Enabled = False
   '   .Show vbModal
   ' End With
   ' Unload frmRecordatorio
   ' Set frmRecordatorio = Nothing
     
   'Versi�n DLL
    Call objSecurity.LaunchProcess(ciRecordatorio, objSolicitud.lngSolicitud)
  Else
    Call objError.SetError(cwCodeMsg, "La Solicitud a�n no est� creada.")
    Call objError.Raise

  End If
End Sub

Private Sub cmdSalir_Click(Index As Integer)
  If blnConfirmarCitas Then
    Call objError.SetError(cwCodeQuery, "Los cambios realizados en las Citas no han sido confirmados." & vbCrLf & "�Desea confirmar las modificaciones?")
    If objError.Raise = vbYes Then
      Call cmdConfirmarSolicitud_Click(0)
      blnCommit = True
      Exit Sub
    Else
      blnCommit = False
    End If
  'Else
  '  blnCommit = False
  End If
  Me.Hide
  Call objIlog.Close
  Set objIlog = Nothing
End Sub

Private Sub cmdVerFases_Click(Index As Integer)
  Dim blnCancelTrans As Boolean

  objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans
  
  Call ShowFases
  
  If blnCancelTrans Then
    objApp.rdoConnect.RollbackTrans
    'Call objError.SetError(cwCodeMsg, ciErrRollback)
    Call objError.SetError(cwCodeMsg, "Ver Fases Cancelado")
    Call objError.Raise
  Else
    objApp.rdoConnect.CommitTrans
  End If

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next
End Sub



Private Sub dtcDateCombo1_Change(intIndex As Integer)
  If intIndex < 2 And blnSolutionAsked Then
    blnNewSolutions = True
    Call CambioSoluciones(False)
  End If

End Sub

Private Sub dtcDateCombo1_Click(Index As Integer)
  If Index < 2 Then
    blnNewSolutions = True
    Call CambioSoluciones(False)
  End If

End Sub


Private Sub Form_Load()
  Dim strBdUserName As String * 15
  Dim strBdUserPasswd As String * 15
  Dim strBdServer As String
  Dim rdoParametro As rdoResultset
  Dim strSql As String
  
  
  strSql = "SELECT AG25NUMSOLUC,AG25NUMDIAESTUD,AG25NUMMAXRAMA,AG25GRANOTIEMPO FROM AG2500"
  Set rdoParametro = objApp.rdoConnect.OpenResultset(strSql)
  If Not rdoParametro.EOF Then
    ciIntervaloFechas = rdoParametro("AG25NUMDIAESTUD")
    ciIntervaloCitaMM = rdoParametro("AG25GRANOTIEMPO")
  Else
    ciIntervaloFechas = 15
    ciIntervaloCitaMM = 15
  End If
  
  strBdUserName = String(15, " ")
  strBdUserPasswd = String(15, " ")
  TabSSTab1(0).Tab = 0
  UpDown3(0).Increment = ciIntervaloCitaMM
  UpDown3(1).Increment = ciIntervaloCitaMM
  dtcDateCombo1(1).Date = DateAdd("d", ciIntervaloFechas, dtcDateCombo1(0).Date)
  blnDaySelected = False
  blnFasesPorObj = False
  blnNewSolutions = True
  If objSolicitud.lngSolicitud <> 0 Then
   'en nuevas solicitudes las preferencias permanecen deshabilitadas hasta que
   'se llame a la agenda por primera vez (Se pasan las restricciones a las preferencias)
    TabSSTab1(1).TabEnabled(1) = True
  Else
    TabSSTab1(1).TabEnabled(1) = False
  End If


  Call SolicitudRefresh

  grdSSDBGrid1(0).SplitterPos = 4
  grdSSDBGrid1(0).SplitterVisible = True
  grdSSDBGrid1(0).Visible = True


  fraFrame1(2).Caption = "Solicitud N� " & IIf(Str(objSolicitud.lngSolicitud) = 0, "( * )", Str(objSolicitud.lngSolicitud))

  SSMonth1(0).StyleSets.Add "DaySelected"
  SSMonth1(0).StyleSets("DaySelected").BackColor = 8454143
  SSMonth1(0).StyleSets("DaySelected").ForeColor = 0
  blnCommit = False
  
  On Error GoTo Error:
  Set objIlog = CreateObject("Servidor.Document")
  'MsgBox "Objeto Ilog Creado"
 
  With objIlog
     strBdUserName = objSecurity.GetDataBaseUser
 
     Call .SetUsuarioBD(strBdUserName)
     'MsgBox "Usuario: " & strBdUserName
     strBdUserPasswd = objSecurity.GetDataBasePassword
     Call .SetPasswdBd(strBdUserPasswd)
     'MsgBox "Password: " & strBdUserPasswd
     strBdServer = GetServer(objApp.rdoConnect.Connect)
     Call .SetServerBd(strBdServer)
  End With
  grdSSDBGrid1(0).Columns("Asignado").Locked = True
  cmdConfirmarSolicitud(0).Enabled = False
  'cmdCitar(0).Enabled = False
  Exit Sub
  
Error:
  MsgBox "Se ha producido el error : " & Err.Number & " - " & Err.Description, vbCritical
  'Resume Next
End Sub


Private Sub grdSSDBGrid1_BtnClick(intIndex As Integer)
  Dim intFirstRow As Integer
  Dim intActivecol As Integer
  Dim intRow As Integer
  Dim vntBookmark As Variant
  
  intActivecol = grdSSDBGrid1(intIndex).Col
  If intIndex = 0 Then
    If intActivecol > 4 Then
      Call AsignOneSolution(intActivecol)
    End If
    If intActivecol = 2 Then
      Call ClearSolution
    End If
    blnConfirmarCitas = True
  End If
  
End Sub

Private Sub grdSSDBGrid1_Change(intIndex As Integer)
  Dim objPeticion       As clsSelPeticion
  Dim objActuacion      As clsSelActuacion

  If intIndex = 1 Then
    For Each objPeticion In objSolicitud.cllPeticiones
      For Each objActuacion In objPeticion.cllActuaciones
        If objActuacion.lngNumActPlan = grdSSDBGrid1(intIndex).Columns("NumActPlan").Value And objActuacion.lngNumActPedi = grdSSDBGrid1(intIndex).Columns("NumActPedi").Value Then
           objActuacion.intRecordatorio = grdSSDBGrid1(intIndex).Columns("Recordatorio").Value
           objActuacion.intLisEspera = grdSSDBGrid1(intIndex).Columns("Lista Espera").Value
        End If
      Next
    Next

 End If
 If intIndex = 0 And grdSSDBGrid1(0).Col = 4 Then
    blnNewSolutions = True
    Call CambioSoluciones(False)
 End If
End Sub

Private Sub grdSSDBGrid1_Click(intIndex As Integer)
  Dim rsRecurso               As rdoResultset
  Dim strSql                  As String
  Dim objPeticion             As clsSelPeticion
  Dim objActuacion            As clsSelActuacion
  Dim INTCOUNT                As Integer
  Dim datFechaIni             As Date
  Dim datFechaFin             As Date
  Dim datFechaWhile           As Date
  Dim datFechaDesde           As Date
  Dim datFechaHasta           As Date
  Dim strHoraDesde            As String
  Dim strHorahasta            As String
  Dim lngNumActPlan           As Long
  Dim lngNumActPedi           As Long

  If intIndex = 0 Then
      'Si las columnas actuales son Asignado o Preferencias
      'actualizo los datos de la estructura de objetos
    If grdSSDBGrid1(0).Col = 4 Then 'Or grdSSDBGrid1(0).Col = 3 Then
      grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).Style = ssStyleComboBox
      grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).Locked = False
      grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).RemoveAll
      Select Case grdSSDBGrid1(0).Columns("Cabecera").Text
        
        Case "Recurso"
          For Each objPeticion In objSolicitud.cllPeticiones
            lngNumActPlan = grdSSDBGrid1(0).Columns("Indice").Value
            Set objActuacion = objPeticion.GetActuacion(lngNumActPlan)
            If objActuacion Is Nothing Then
            '
            Else
              '***************
              'objActuacion.lngNumActPedi = 57
              '***************
              strSql = "Select PR14INDRECPREFE, AG11CodRecurso_Val, AG11DesRecurso_Val from PR1131J where PR03NumActPedi = " & objActuacion.lngNumActPedi & " and PR14IndRecPrefe = -1 AND AG11INDPLANIFI_VAL=-1"
              Set rsRecurso = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
              If Not rsRecurso.EOF Then
                While Not rsRecurso.EOF
                 grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).AddItem rsRecurso.rdoColumns("AG11DESRECURSO_VAL").Value & Space(100) & "/#" & rsRecurso.rdoColumns("AG11CODRECURSO_VAL").Value & "#/"
                 rsRecurso.MoveNext
                Wend
                rsRecurso.Close
              Else
                strSql = "Select AG11CODRECURSO,AG11DESRECURSO from AG1100 WHERE AG14CODTIPRECU IN (SELECT AG14CODTIPRECU FROM PR1400 where PR03NumActPedi = " & objActuacion.lngNumActPedi & " and PR14IndRecPrefe = -1) AND AG11INDPLANIFI=-1 AND (AG11FECFINVREC IS NULL OR AG11FECFINVREC > " & objGen.ValueToSQL(objGen.GetDBDateTime, cwDate) & ")"
                Set rsRecurso = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
                While Not rsRecurso.EOF
                  grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).AddItem rsRecurso.rdoColumns("AG11DESRECURSO").Value & Space(100) & "/#" & rsRecurso.rdoColumns("AG11CODRECURSO").Value & "#/"
                  rsRecurso.MoveNext
                Wend
                rsRecurso.Close
              End If
              
              Exit For
            End If
          Next
       
       Case "Fecha"
         datFechaIni = IIf(dtcDateCombo1(0).Date = "", Date, dtcDateCombo1(0).Date)
         datFechaFin = IIf(dtcDateCombo1(1).Date = "", DateAdd("d", 15, dtcDateCombo1(0).Date), dtcDateCombo1(1).Date)
         datFechaWhile = datFechaIni
         While datFechaWhile <= CDate(datFechaFin)
           grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).AddItem datFechaWhile
           datFechaWhile = DateAdd("d", 1, datFechaWhile)
         Wend
      
      Case "Hora"
        datFechaDesde = Format(Date & " " & "00:00", "DD/MM/YYYY HH:NN")
        datFechaHasta = Format(DateAdd("d", 1, datFechaDesde), "DD/MM/YYYY HH:NN")
        'If grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).Caption = "Preferencias" Then
        '  strHoraDesde = objGen.ReplaceStr(MaskEdBox1(0).Text, "_", "0", 0)
        '  If Mid(strHoraDesde, 1, 2) * 60 + Mid(strHoraDesde, 4, 2) <= UpDown3(0).Max Then
        '    datFechaDesde = Format(Date & " " & strHoraDesde, "DD/MM/YYYY HH:NN")
        '  End If
        '  strHorahasta = objGen.ReplaceStr(MaskEdBox1(1).Text, "_", "0", 0)
        '  If Mid(strHorahasta, 1, 2) * 60 + Mid(strHorahasta, 4, 2) <= UpDown3(1).Max Then
        '    datFechaHasta = Format(Date & " " & strHorahasta, "DD/MM/YYYY HH:NN")
        '  End If
        '  If datFechaHasta < datFechaDesde Or (datFechaHasta = datFechaDesde And Format(datFechaHasta, "hh:nn") = "00:00") Then
        '    datFechaHasta = Format(Date & " 23:59", "DD/MM/YYYY HH:NN")
        '  End If
        'End If
        While datFechaDesde <= datFechaHasta
          grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).AddItem Format(datFechaDesde, "HH:NN")
          datFechaDesde = Format(DateAdd("n", ciIntervaloCitaMM, datFechaDesde), "DD/MM/YYYY HH:NN")
        Wend
     Case 3
       grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).Style = ssStyleEdit
       grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).Locked = True

    End Select
    If grdSSDBGrid1(0).Col = 4 Then blnConfirmarCitas = True
   End If
  End If
  Set rsRecurso = Nothing
End Sub
Private Sub AsignOneSolution(intColIndex)
  Dim objPeticion     As clsSelPeticion
  Dim objActuacion    As clsSelActuacion
  Dim objFase         As clsFasesActuacion
  Dim objRecurso      As clsRecursoFase
  Dim objSolucion     As clsSolucion
  Dim intFirstRow      As Integer
  Dim intRow           As Integer
  Dim vntBookmark      As Variant
  Dim intActiveRow     As Integer
   
      vntBookmark = grdSSDBGrid1(0).FirstRow
      intActiveRow = grdSSDBGrid1(0).Row
      grdSSDBGrid1(0).Redraw = False

      While grdSSDBGrid1(0).Columns("Cabecera").Text <> "Recurso"
         grdSSDBGrid1(0).MovePrevious
      Wend
      For Each objPeticion In objSolicitud.cllPeticiones
        Set objActuacion = objPeticion.GetActuacion(Val(grdSSDBGrid1(0).Columns("Indice").Value))
        If objActuacion Is Nothing Then
           
        Else
          'If objActuacion.cllFasesActuacion.Count > 0 Then
          '   For Each objFase In objActuacion.cllFasesActuacion
          '      Call objGen.RemoveCollection(objFase.cllRecursosFase)
          '   Next
          '   Call objGen.RemoveCollection(objActuacion.cllFasesActuacion)
          ' End If
          If objActuacion.lngNumActPlan > 0 Then
           Set objSolucion = objActuacion.GetSolucion(Val(intColIndex - 4))
           If Not objSolucion Is Nothing Then
             Set objActuacion.cllFasesActuacion = objSolucion.cllFasesSolucion
           End If
           Exit For
          End If
        End If
      Next
      grdSSDBGrid1(0).Columns("Asignado").Style = ssStyleEdit
      For intRow = 0 To 2
         'If intRow = 0 Then
         '  If grdSSDBGrid1(0).Columns("Asignado").Text <> "" Then
         '    grdSSDBGrid1(0).Columns("Asignado").Text = grdSSDBGrid1(0).Columns(intColIndex).Text
         '  End If
         'Else
           grdSSDBGrid1(0).Columns("Asignado").Text = grdSSDBGrid1(0).Columns(intColIndex).Text
         'End If
         grdSSDBGrid1(0).MoveNext
      Next
      grdSSDBGrid1(0).FirstRow = vntBookmark
      grdSSDBGrid1(0).Row = intActiveRow
      grdSSDBGrid1(0).Redraw = True
  
  Set objPeticion = Nothing
  Set objActuacion = Nothing
  Set objFase = Nothing
  Set objRecurso = Nothing
  
End Sub
Private Sub ClearSolution()
   Dim vntBookmark      As Variant
   Dim intRow           As Integer
   Dim intActiveRow     As Integer
        
      vntBookmark = grdSSDBGrid1(0).FirstRow
      intActiveRow = grdSSDBGrid1(0).Row
      grdSSDBGrid1(0).Redraw = False
      If grdSSDBGrid1(0).Columns("Cabecera").Text = "" Then
         While grdSSDBGrid1(0).Columns("Cabecera").Text <> "Recurso"
            grdSSDBGrid1(0).MovePrevious
         Wend
         For intRow = 0 To 2
           grdSSDBGrid1(0).Columns("Asignado").Text = ""
           grdSSDBGrid1(0).MoveNext
         Next
      Else
         grdSSDBGrid1(0).Columns("Asignado").Text = ""
      End If
      grdSSDBGrid1(0).FirstRow = vntBookmark
      grdSSDBGrid1(0).Row = intActiveRow
      grdSSDBGrid1(0).Redraw = True
End Sub
'Asigna las preferencias al Tab de Preferencias
Private Sub AsignPreferences()
  Dim intChkIndex As Integer
  Dim dteHoraDesde As Date
  Dim dteHoraHasta As Date
  Dim intPos As Integer
  
  
  If TabSSTab1(1).TabEnabled(1) = False Then
    dtcDateCombo1(2).Text = dtcDateCombo1(0).Text
    dtcDateCombo1(3).Text = dtcDateCombo1(1).Text
    MaskEdBox1(2).Text = MaskEdBox1(0).Text
    MaskEdBox1(3).Text = MaskEdBox1(1).Text
    For intChkIndex = 0 To 6
      chkCheck1(intChkIndex + 7) = chkCheck1(intChkIndex + 7)
    Next
    TabSSTab1(1).TabEnabled(1) = True
  End If
  With objIlog
    Call .Initialize
    If dtcDateCombo1(0).Text <> "" Then
      Call .SetDiaDesde(CInt(Format(dtcDateCombo1(0).Text, "DD")))
      'MsgBox "Dia Desde:" & CInt(Format(dtcDateCombo1(0).Text, "DD"))
      Call .SetMesDesde(CInt(Format(dtcDateCombo1(0).Text, "MM")))
      'MsgBox "Mes Desde:" & CInt(Format(dtcDateCombo1(0).Text, "MM"))
      Call .SetAnioDesde(CInt(Format(dtcDateCombo1(0).Text, "YYYY")))
      'MsgBox "A�o Desde:" & CInt(Format(dtcDateCombo1(0).Text, "YYYY"))
    End If
   
    If dtcDateCombo1(1).Text <> "" Then
      Call .SetDiaHasta(CInt(Format(dtcDateCombo1(1).Text, "DD")))
      'MsgBox "Dia Hasta:" & CInt(Format(dtcDateCombo1(1).Text, "DD"))
      Call .SetMesHasta(CInt(Format(dtcDateCombo1(1).Text, "MM")))
      'MsgBox "Mes Hasta:" & CInt(Format(dtcDateCombo1(1).Text, "MM"))
      Call .SetAnioHasta(CInt(Format(dtcDateCombo1(1).Text, "YYYY")))
      'MsgBox "A�o Hasta:" & CInt(Format(dtcDateCombo1(1).Text, "YYYY"))
    End If
   
    If MaskEdBox1(0).Text <> "__:__" Then
      dteHoraDesde = MaskEdBox1(0).Text
      Call .SetHoraDesde(CInt(Format(dteHoraDesde, "HH")))
      'MsgBox "Hora Desde :" & CInt(Format(dteHoraDesde, "HH"))
      Call .SetMinDesde(CInt(Format(dteHoraDesde, "NN")))
      'MsgBox "Min Desde :" & CInt(Format(dteHoraDesde, "NN"))
    End If
    If MaskEdBox1(1).Text <> "__:__" Then
      dteHoraHasta = MaskEdBox1(1).Text
      Call .SetHoraHasta(CInt(Format(dteHoraHasta, "HH")))
      'MsgBox "Hora Hasta :" & CInt(Format(dteHoraHasta, "HH"))
      Call .SetMinHasta(CInt(Format(dteHoraHasta, "NN")))
      'MsgBox "Min Hasta :" & CInt(Format(dteHoraHasta, "NN"))
    
    End If
    For intPos = 0 To 6
      If chkCheck1(intPos).Value = 1 Then
        Call .SetDiaSemana(intPos)
      End If
    Next intPos
  End With
  
  'MsgBox "Preferencias Asignadas"
End Sub

Private Sub SolicitudRefresh()
  Dim objPeticion             As clsSelPeticion
  Dim objActuacion            As clsSelActuacion
  Dim strnombre               As String
  Dim lngActuacion            As Long
  Dim datFechaEmpty           As Date

  Dim strSql                  As String
  Dim strRecursoAsig          As String
  Dim strRecursoPref          As String
  Dim strDepto                As String
  Dim strPaciente             As String
  Dim strTexto                As String
  Dim blnNewSolicitud         As Boolean

  Dim cllColumnValues         As Collection

  Dim vntFirstBookmark        As Variant
  Dim intRow                  As Integer
   
  'grdSSDBGrid1(0).Redraw = False

  blnNewSolicitud = IIf(grdSSDBGrid1(0).Rows = 0, True, False)

  If blnNewSolicitud Then
    For Each objPeticion In objSolicitud.cllPeticiones
      For Each objActuacion In objPeticion.cllActuaciones
        grdSSDBGrid1(0).AddItem ("")
        grdSSDBGrid1(0).AddItem ("")
        grdSSDBGrid1(0).AddItem ("")
        grdSSDBGrid1(0).AddItem ("")
      Next
    Next
    grdSSDBGrid1(0).MoveFirst
  End If

  intRow = grdSSDBGrid1(0).Row
  vntFirstBookmark = grdSSDBGrid1(0).FirstRow

  grdSSDBGrid1(0).MoveFirst
  For Each objPeticion In objSolicitud.cllPeticiones
    For Each objActuacion In objPeticion.cllActuaciones
      strSql = "Select AG11DESRECURSO from AG1100 where AG11CODRECURSO = " & objActuacion.lngRecurso
      strRecursoAsig = GetTableColumn(strSql)("AG11DESRECURSO")
      
      strSql = "Select AG11DesRecurso from AG1100 WHERE AG11CodRecurso = " & objActuacion.lngRecursoPref
      strRecursoPref = GetTableColumn(strSql)("AG11DESRECURSO") & Space(100) & "/#" & objActuacion.lngRecursoPref & "#/"
           
      strSql = "Select AD02CODDPTO, AD02DESDPTO from AD0200 WHERE AD02CODDPTO = " & objActuacion.intDepartamento
      strDepto = GetTableColumn(strSql)("AD02DESDPTO")
      strDepto = IIf(strDepto = "", strDepto, "(" & strDepto & ")")
      
      strSql = "Select CI22NOMBRE, CI22PRIAPEL,CI22SEGAPEL from CI2200 where CI21CODPERSONA = " & objPeticion.lngPersona
      Set cllColumnValues = GetTableColumn(strSql)
      strPaciente = cllColumnValues("CI22NOMBRE") & " " & cllColumnValues("CI22PRIAPEL") & " " & cllColumnValues("CI22SEGAPEL")
      Set cllColumnValues = Nothing
           
      strTexto = DivideStringRows(objActuacion.strDescripci�n & " " & strDepto, grdSSDBGrid1(0).Columns(1).width, 1)
      grdSSDBGrid1(0).Columns("Paciente").Value = DivideStringRows(strPaciente, grdSSDBGrid1(0).Columns(0).width, 1)
      grdSSDBGrid1(0).Columns("Actuacion").Value = strTexto
      grdSSDBGrid1(0).Columns("Cabecera").Value = "Recurso"
      grdSSDBGrid1(0).Columns("Asignado").Value = strRecursoAsig
      grdSSDBGrid1(0).Columns("Preferencias").Value = strRecursoPref
      grdSSDBGrid1(0).Columns("Indice").Value = objActuacion.lngNumActPlan
      
      grdSSDBGrid1(0).MoveNext
      strTexto = DivideStringRows(objActuacion.strDescripci�n & " " & strDepto, grdSSDBGrid1(0).Columns(1).width, 2)
      grdSSDBGrid1(0).Columns("Paciente").Value = DivideStringRows(strPaciente, grdSSDBGrid1(0).Columns(0).width, 2)
      grdSSDBGrid1(0).Columns("Actuacion").Value = strTexto
      grdSSDBGrid1(0).Columns("Cabecera").Value = "Fecha"
      grdSSDBGrid1(0).Columns("Asignado").Value = IIf(objActuacion.datFechaCita = datFechaEmpty, "", Format(objActuacion.datFechaCita, "DD/MM/YYYY"))
      grdSSDBGrid1(0).Columns("Preferencias").Value = IIf(objActuacion.datFechaPref = datFechaEmpty, "", Format(objActuacion.datFechaPref, "DD/MM/YYYY"))
      grdSSDBGrid1(0).Columns("Indice").Value = objActuacion.lngNumActPedi
      
      grdSSDBGrid1(0).MoveNext
      strTexto = DivideStringRows(objActuacion.strDescripci�n & " " & strDepto, grdSSDBGrid1(0).Columns(1).width, 3)
      grdSSDBGrid1(0).Columns("Paciente").Value = DivideStringRows(strPaciente, grdSSDBGrid1(0).Columns(0).width, 3)
      grdSSDBGrid1(0).Columns("Actuacion").Value = strTexto
      grdSSDBGrid1(0).Columns("Cabecera").Value = "Hora"
      grdSSDBGrid1(0).Columns("Asignado").Value = objActuacion.strHoraCita
      grdSSDBGrid1(0).Columns("Preferencias").Value = IIf(objActuacion.strHoraPref <> "00.00.00", objActuacion.strHoraPref, " ")
      grdSSDBGrid1(0).Columns("Indice").Value = objPeticion.lngSolicitud
      
      grdSSDBGrid1(0).MoveNext
      grdSSDBGrid1(0).Columns("Indice").Value = objActuacion.lngCita
      
      grdSSDBGrid1(0).MoveNext
    Next
  Next
  
  grdSSDBGrid1(0).FirstRow = vntFirstBookmark
  'grdSSDBGrid1(0).Redraw = True
  If blnNewSolicitud Then
    grdSSDBGrid1(0).MoveFirst
  Else
    grdSSDBGrid1(0).Row = intRow
  End If
  Set objPeticion = Nothing
  Set objActuacion = Nothing
End Sub
Private Sub AsignAllSolutions(intColIndex As Integer)
   Dim objPeticion            As clsSelPeticion
   Dim objActuacion           As clsSelActuacion
   Dim objFase                As clsFasesActuacion
   Dim objSolucion            As clsSolucion
   Dim intRow                 As Integer
   Dim vntBookmark            As Variant
   Dim intActiveRow           As Integer
   
   grdSSDBGrid1(0).Redraw = False
   
   vntBookmark = grdSSDBGrid1(0).FirstRow
   intActiveRow = grdSSDBGrid1(0).Row
   grdSSDBGrid1(0).MoveFirst
   
   For Each objPeticion In objSolicitud.cllPeticiones
      For Each objActuacion In objPeticion.cllActuaciones
         'If objActuacion.cllFasesActuacion.Count > 0 Then
         '  For Each objFase In objActuacion.cllFasesActuacion
         '    Call objGen.RemoveCollection(objFase.cllRecursosFase)
         '  Next
         '  Call objGen.RemoveCollection(objActuacion.cllFasesActuacion)
         'End If
         Set objSolucion = objActuacion.GetSolucion(Val(intColIndex - 4))
         If Not objSolucion Is Nothing Then
           Set objActuacion.cllFasesActuacion = objSolucion.cllFasesSolucion
         End If
         For intRow = 0 To 3
            grdSSDBGrid1(0).Columns("Asignado").Value = grdSSDBGrid1(0).Columns(intColIndex).Value
            grdSSDBGrid1(0).MoveNext
         Next
      Next
   Next
   grdSSDBGrid1(0).FirstRow = vntBookmark
   'grdSSDBGrid1(0).Row = intActiveRow
   grdSSDBGrid1(0).Redraw = True
   Set objPeticion = Nothing
   Set objActuacion = Nothing

End Sub

Private Sub grdSSDBGrid1_ComboCloseUp(Index As Integer)
  blnConfirmarCitas = True
End Sub

Private Sub grdSSDBGrid1_HeadClick(intIndex As Integer, ByVal intColIndex As Integer)
  If intIndex = 0 Then
    If intColIndex > 4 Then
      Call AsignAllSolutions(intColIndex)
    End If
    blnConfirmarCitas = True
  End If
End Sub

Private Sub grdSSDBGrid1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
  If intIndex = 0 Then
   If KeyCode = 46 And grdSSDBGrid1(0).Col = 4 Then
      grdSSDBGrid1(0).Columns(4).Value = ""
   End If
  End If

End Sub


Private Sub grdSSDBGrid1_RowColChange(intIndex As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  If intIndex = 0 Then
    If grdSSDBGrid1(0).Col <> -1 Then
      If intLastCol <> grdSSDBGrid1(0).Col Then
        If Left(grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).Name, 5) = "Soluc" Then
          UpDown2.Value = grdSSDBGrid1(0).Col - 4
          OptOption1(0).Value = False
          OptOption1(1).Value = True
        End If
        If grdSSDBGrid1(0).Columns(grdSSDBGrid1(0).Col).Name = "Asignado" Then
          OptOption1(0).Value = True
          OptOption1(1).Value = False
        End If
      End If
    End If
  End If
End Sub


Private Function DivideStringRows(ByVal strRow As String, ByVal intWidth As Long, ByVal intRow As Integer) As String
  Dim intbucle As Integer
  Dim intPos As Integer
  Dim oldintpos As Integer
  Dim oldDivideStringRows As String
  
  intPos = 1
  intbucle = 1
  intWidth = intWidth - 50
  DivideStringRows = strRow & IIf(Right(strRow, 1) <> " ", " ", "")
  oldDivideStringRows = DivideStringRows
  oldintpos = intPos
  For intbucle = 1 To intRow
    DivideStringRows = Mid(oldDivideStringRows, oldintpos, Len(oldDivideStringRows) + 1)
    oldDivideStringRows = DivideStringRows
    If TextWidth(DivideStringRows) > intWidth Then
      oldintpos = intPos
      intPos = InStr(1, oldDivideStringRows, " ")
      If intPos = 0 Then intPos = Len(oldDivideStringRows) + 1
      
      DivideStringRows = Mid(oldDivideStringRows, 1, intPos)
      While TextWidth(Mid(oldDivideStringRows, 1, intPos)) < intWidth
        intPos = InStr(intPos + 1, oldDivideStringRows, " ")
        If intPos = 0 Then intPos = Len(oldDivideStringRows) + 1
        If TextWidth(LTrim(RTrim(Mid(oldDivideStringRows, 1, intPos)))) < intWidth Then
          DivideStringRows = LTrim(RTrim(Mid(oldDivideStringRows, 1, intPos)))
          oldintpos = intPos
        End If
      Wend
       'DivideStringRows = oldDivideStringRows
       'intpos = oldintpos2
    Else
      oldintpos = Len(oldDivideStringRows) + 1
    End If
  Next
  DivideStringRows = LTrim(RTrim(DivideStringRows))
End Function


Private Sub grdSSDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  Dim objPeticion                   As clsSelPeticion
  Dim objActuacion                   As clsSelActuacion

  If intIndex = 1 Then
    For Each objPeticion In objSolicitud.cllPeticiones
      For Each objActuacion In objPeticion.cllActuaciones
        If objActuacion.lngNumActPlan = grdSSDBGrid1(intIndex).Columns("NumActPlan").Value And objActuacion.lngNumActPedi = grdSSDBGrid1(intIndex).Columns("NumActPedi").Value Then
           grdSSDBGrid1(intIndex).Columns("Recordatorio").Value = objActuacion.intRecordatorio
           grdSSDBGrid1(intIndex).Columns("Lista Espera").Value = objActuacion.intLisEspera
        End If
      Next
    Next

 End If

End Sub

Private Sub Image1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    frmCapacidad.Show vbModal

  End If
End Sub

Private Sub MaskEdBox1_Change(intIndex As Integer)
  Dim datFechaDesde As Date
  Dim datFechaHasta As Date
  Dim strHora As String
  Dim intUpdownValue As Integer
  
  strHora = objGen.ReplaceStr(MaskEdBox1(intIndex).Text, "_", "0", 0)
  If Mid(strHora, 1, 2) * 60 + Mid(strHora, 4, 2) <= UpDown3(intIndex).Max And IsDate(strHora) Then
    datFechaDesde = "00:00"
    datFechaHasta = CDate(Format(objGen.ReplaceStr(MaskEdBox1(intIndex).Text, "_", "0", 0), "hh:mm"))
    intUpdownValue = DateDiff("n", datFechaDesde, datFechaHasta) - (DateDiff("n", datFechaDesde, datFechaHasta) Mod ciIntervaloCitaMM)
    If intUpdownValue >= UpDown3(intIndex).Min And intUpdownValue <= UpDown3(intIndex).Max Then
      UpDown3(intIndex).Value = intUpdownValue
    End If
  End If
  
  If intIndex < 2 And blnSolutionAsked Then
    blnNewSolutions = True
    Call CambioSoluciones(False)
    
  End If

End Sub

Private Sub MaskEdBox1_LostFocus(intIndex As Integer)
  If Not IsDate(objGen.ReplaceStr(MaskEdBox1(intIndex).Text, "_", "0", 0)) Then
    Call objError.SetError(cwCodeMsg, "La hora introducida no es correcta")
    Call objError.Raise
    MaskEdBox1(intIndex).SetFocus
  End If
End Sub

Private Sub OptOption1_Click(Index As Integer)
  If OptOption1(1) Then
    UpDown2.Enabled = True
    txtText1(7).Enabled = True
    
    If UpDown2.Value > 0 Then
      SSMonth1(0).SelectedDays.RemoveAll
      Call ShowSolution(UpDown2.Value)
    End If
  Else
    UpDown2.Enabled = False
    txtText1(7).Enabled = False
    Call ShowSolution(0)
  End If
End Sub

Private Sub SSMonth1_FocusChange(Index As Integer, FocusDate As String, OldFocusDate As String, MonthNum As Integer, YearNum As Integer, DayNum As Integer)
 'If Not blnDaySelected Then
 '  If SSMonth1(0).Day(OldFocusDate).Caption <> "" And FocusDate <> OldFocusDate Then
      'Call SSMonth1(0).SelectedDays.Add(OldFocusDate)
'   End If
' End If
End Sub

Private Sub SSMonth1_SelChange(Index As Integer, SelDate As String, OldSelDate As String, Selected As Integer, RtnCancel As Integer)
  Dim DayNum As Integer
 
  If Not blnDaySelected Then
    If SSMonth1(0).Day(SelDate).Caption <> "" Then
      Call ShowDayDetail(SelDate)
      RtnCancel = False
    Else
      RtnCancel = True
    End If
  Else
    RtnCancel = Not blnDaySelected
  End If

'SSMonth1(0).Day = SelDate


End Sub


Private Sub tabSSTab1_Click(Index As Integer, PreviousTab As Integer)
'Dim intNumCol As Integer
'Dim intSolution As Integer
  If TabSSTab1(0).Tab = 1 And PreviousTab = 0 Then
    If grdSSDBGrid1(0).Cols > 6 And Not blnNoSolutions Then
      OptOption1(1).Enabled = True
      txtText1(7).Enabled = True
      Call ShowSolution(IIf(OptOption1(0), 0, UpDown2.Value))
    Else
      OptOption1(0).Value = True
      OptOption1(1).Enabled = False
      txtText1(7) = ""
      txtText1(7).Enabled = False
      Call ShowSolution(0)
    End If
  End If
End Sub


Private Sub UpDown1_Change()
'txtText1(6).Text = UpDown1.Value + 1
End Sub

Private Sub UpDown2_Change()
  txtText1(7).Text = UpDown2.Value
  If TabSSTab1(0).Tab = 1 Then
    Call ShowSolution(UpDown2.Value)
  End If
End Sub

Private Sub UpDown3_DownClick(intIndex As Integer)
  Dim datFecha As Date
'incremento del spin para hora y minuto preferente
  UpDown3(intIndex).Increment = ciIntervaloCitaMM
  
  datFecha = CDate(Format("00:00", "HH:MM"))
  datFecha = DateAdd("n", Val(UpDown3(intIndex).Value), datFecha)
  MaskEdBox1(intIndex).Text = objGen.ReplaceStr(Format(datFecha, "HH:MM"), ".", ":", 0)
End Sub

Private Sub UpDown3_UpClick(intIndex As Integer)
  Dim datFecha As Date
 'incremento del spin para hora y minuto preferente
  UpDown3(intIndex).Increment = ciIntervaloCitaMM

  datFecha = CDate(Format("00:00", "HH:MM"))
  datFecha = DateAdd("n", Val(UpDown3(intIndex).Value), datFecha)
  MaskEdBox1(intIndex).Text = objGen.ReplaceStr(Format(datFecha, "HH:MM"), ".", ":", 0)
End Sub
