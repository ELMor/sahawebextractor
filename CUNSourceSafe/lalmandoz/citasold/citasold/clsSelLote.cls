VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsSelLote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarintLote As Long 'local copy
Private mvarcllSelPeticiones As Collection 'local copy
Public Property Set cllSelPeticiones(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.cllSelPeticiones = Form1
    Set mvarcllSelPeticiones = vData
End Property


Public Property Get cllSelPeticiones() As Collection
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.cllSelPeticiones
    Set cllSelPeticiones = mvarcllSelPeticiones
End Property



Public Property Let intLote(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.intLote = 5
    mvarintLote = vData
End Property


Public Property Get intLote() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.intLote
    intLote = mvarintLote
End Property



