VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsSolucion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public intNumSolucion As Long
Public lngRecPrefSol  As Long
Public lngSolNumsolicit As Long
Public lngSolNumCita As Long

Public cllFasesSolucion As New Collection

Public Function AddNewFaseSol(Optional intCodFase As Integer = 0) As clsFasesActuacion
  Dim objFase     As New clsFasesActuacion
  
  objFase.intKeyFase = intCodFase
  Call Me.cllFasesSolucion.Add(objFase, Str(intCodFase))
  Set AddNewFaseSol = Me.cllFasesSolucion(Str(intCodFase))
  Set objFase = Nothing
End Function

Public Function GetFaseSol(ByVal intKeyFase As Integer) As clsFasesActuacion

  Dim objFase        As New clsFasesActuacion
  Dim blnExistFase   As Boolean

  blnExistFase = False
  For Each objFase In Me.cllFasesSolucion
    If objFase.intKeyFase = intKeyFase Then
      blnExistFase = True
      Exit For
    End If
  Next
  If blnExistFase Then
    Set GetFaseSol = objFase
  Else
    Set GetFaseSol = Nothing
  End If
  Set objFase = Nothing

End Function


