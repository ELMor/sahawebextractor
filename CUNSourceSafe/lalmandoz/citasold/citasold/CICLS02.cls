VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsConnect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public blnError As Boolean
Public rdoError As rdoResultset
Public rdoAppEnv As rdoEnvironment
Public rdoAppConnectEnt As rdoConnection
Public rdoAppConnectSal As rdoConnection
Public Query As rdoQuery


'Se conecta con los Ficheros de texto y con SQL Server
'Se crea el cursor de errores sobre la tabla ERRORES

Public Sub emsOpen()
  Dim strInfo As String
  Dim strSql As String
  Dim strProcedure As String
  'On Error Resume Next
  'Set rdoAppEnv = rdoEnvironments(0)

  'strInfo = "DSN=" & "Carga Texto" & ";" & _
            "UID=" & "admin" & ";"
  frmCargaTextos.StatusBar2.Panels(1).Text = "Abriendo conexiones ODBC..."
  DoEvents

  Set rdoAppConnectEnt = objApp.rdoEnv.OpenConnection("") ', rdDriverPrompt, False, strInfo)
  blnError = (Err.Number <> 0)
  
  If blnError Then
    Call objError.SetError(cwCodeMsg, "No se ha podido abrir la conexi�n ODBC de los ficheros de Texto")
    Call objError.Raise

    Unload frmCargaTextos
  End If
  If Not blnError Then
    Set rdoAppConnectSal = objApp.rdoConnect 'rdoAppEnv.OpenConnection("", rdDriverNoPrompt, False, strInfo)
    
    blnError = (Err.Number <> 0)
  
  End If
  
  If Not blnError Then
   ' strSQL = "SELECT FICHERO, FECINSER, DESERROR, NIVEL, REGISTRO FROM EMS..ERRORES"
   ' Set rdoError = rdoAppConnectSal.OpenResultset(strSQL, rdOpenKeyset, rdConcurValues)
   ' blnError = (Err.Number <> 0)
  End If
  
  End Sub

'Se cierra el cursor de errores

Public Sub emsClose()
 ' rdoError.Close
 ' Set rdoError = Nothing
End Sub
