VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Object = "{643F1353-1D07-11CE-9E52-0000C0554C0A}#1.0#0"; "sscalb32.ocx"
Begin VB.Form frmCapacidad 
   Caption         =   "Capacidades de los Recursos"
   ClientHeight    =   8595
   ClientLeft      =   960
   ClientTop       =   345
   ClientWidth     =   10350
   Icon            =   "Capac.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8595
   ScaleWidth      =   10350
   StartUpPosition =   2  'CenterScreen
   Begin SSCalendarWidgets_B.SSDay SSDay1 
      Height          =   5040
      Index           =   0
      Left            =   2400
      TabIndex        =   3
      Top             =   3360
      Width           =   4155
      _Version        =   65537
      _ExtentX        =   7329
      _ExtentY        =   8890
      _StockProps     =   79
      Caption         =   "Capacidades"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TimeSelectionBarFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TimeInterval    =   2
      TimeBegin       =   "0.00 "
      TimeEnd         =   "0.00 "
      TimeFormat      =   "1"
      AllowEdit       =   0   'False
      AllowAdd        =   0   'False
      AllowDelete     =   0   'False
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Salir"
      Height          =   375
      Index           =   1
      Left            =   7800
      TabIndex        =   2
      Top             =   120
      Width           =   2175
   End
   Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   3735
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldSeparator  =   ","
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1349
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "Codigo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4445
      Columns(1).Caption=   "Recurso"
      Columns(1).Name =   "Recurso"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6588
      _ExtentY        =   661
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSCalendarWidgets_A.SSMonth SSMonth1 
      Height          =   2715
      Left            =   0
      TabIndex        =   0
      Top             =   600
      Width           =   10335
      _Version        =   65537
      _ExtentX        =   18230
      _ExtentY        =   4789
      _StockProps     =   76
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      StartofWeek     =   2
      NumberOfMonths  =   2
   End
End
Attribute VB_Name = "frmCapacidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim lngNumMarks As Long
Dim lngRecSelec As Long

Private Sub Command1_Click(Index As Integer)
  Unload Me
End Sub

Private Sub Form_Load()
  Dim lngNumRecs As Long
  Dim lngCodRec As Long
  Dim lngLonDay As Long
  Dim intPos As Integer
  Dim strRecurso As String
  On Error Resume Next
  lngNumRecs = frmSolicitud.objIlog.GetNumRecCapacity
  For intPos = 0 To lngNumRecs - 1
    lngCodRec = frmSolicitud.objIlog.GetCodRecCapacity(intPos)
    strRecurso = GetTableColumn("SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO=" & lngCodRec)("AG11DESRECURSO")
    SSDBCombo1.AddItem CStr(lngCodRec) & "," & strRecurso
  Next
  lngNumMarks = frmSolicitud.objIlog.GetLongDay
  
  lngLonDay = (24 * 60) / lngNumMarks
  Select Case lngLonDay
     Case 5
       SSDay1(0).TimeInterval = ssTimeInterval5
      ' SSDay1(1).TimeInterval = ssTimeInterval5
     Case 10
      SSDay1(0).TimeInterval = ssTimeInterval10
      'SSDay1(1).TimeInterval = ssTimeInterval10
     Case 15
      SSDay1(0).TimeInterval = ssTimeInterval15
      'SSDay1(1).TimeInterval = ssTimeInterval15
     Case 30
      SSDay1(0).TimeInterval = ssTimeInterval30
      'SSDay1(1).TimeInterval = ssTimeInterval30
     Case 60
      SSDay1(0).TimeInterval = ssTimeInterval60
      'SSDay1(1).TimeInterval = ssTimeInterval60

   End Select
  SSDay1(0).Tasks.RemoveAll
 ' SSDay1(1).Tasks.RemoveAll
  
End Sub


Private Sub SSMonth1_Click()
  Dim intPos As Integer
  Dim datHoraI As Date
  Dim datHoraF As Date
  Dim lngCapacidad As Long
  On Error Resume Next
  SSDay1(0).Tasks.RemoveAll
  datHoraI = Format("00:00", "HH:MM")
  For intPos = 0 To lngNumMarks - 1
   datHoraF = DateAdd("n", (24 * 60) / lngNumMarks, datHoraI)
   lngCapacidad = frmSolicitud.objIlog.GetCapacity(CLng(SSDBCombo1.Columns("Codigo").Value), CInt(Format(SSMonth1.Date, "DD")), CInt(Format(SSMonth1.Date, "MM")), CInt(Format(SSMonth1.Date, "YYYY")), intPos)
   If lngCapacidad > 0 Then
     Call SSDay1(0).Tasks.Add(objGen.ReplaceStr(Format(datHoraI, "HH:MM"), ".", ":", 0), objGen.ReplaceStr(Format(datHoraF, "HH:MM"), ".", ":", 0), lngCapacidad)
   End If
   datHoraI = datHoraF
   
  Next
End Sub

