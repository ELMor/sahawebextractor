VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{4D2E11F7-A12E-11D1-9D0F-00C04FA619D6}#1.0#0"; "idperson.ocx"
Begin VB.Form frmCitasPaciente 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Actuaciones Planificadas por Paciente"
   ClientHeight    =   8070
   ClientLeft      =   1050
   ClientTop       =   870
   ClientWidth     =   11580
   ControlBox      =   0   'False
   Icon            =   "CI1024.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8070
   ScaleWidth      =   11580
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   11580
      _ExtentX        =   20426
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4815
      Index           =   9
      Left            =   120
      TabIndex        =   15
      Top             =   2880
      Width           =   11295
      Begin TabDlg.SSTab tabtab1 
         Height          =   4320
         HelpContextID   =   90001
         Index           =   2
         Left            =   150
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   375
         Width           =   11040
         _ExtentX        =   19473
         _ExtentY        =   7620
         _Version        =   327681
         Style           =   1
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Si&n Citar"
         TabPicture(0)   =   "CI1024.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraFrame1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "&Citadas"
         TabPicture(1)   =   "CI1024.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraFrame1(2)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "&Pendientes de Recitar"
         TabPicture(2)   =   "CI1024.frx":0044
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "cmdDetalle(2)"
         Tab(2).Control(1)=   "fraFrame1(3)"
         Tab(2).ControlCount=   2
         Begin VB.CommandButton cmdDetalle 
            Caption         =   "De&talle"
            Height          =   375
            Index           =   2
            Left            =   -65985
            TabIndex        =   4
            Top             =   3210
            Width           =   1815
         End
         Begin VB.Frame fraFrame1 
            BorderStyle     =   0  'None
            Caption         =   "Citadas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3900
            Index           =   2
            Left            =   -74850
            TabIndex        =   26
            Top             =   360
            Width           =   10695
            Begin VB.CommandButton cmdVerListaEspera 
               Caption         =   "Ver L&ista de Espera"
               Height          =   375
               Index           =   0
               Left            =   8880
               TabIndex        =   35
               Top             =   2280
               Width           =   1815
            End
            Begin VB.CommandButton cmdDetalle 
               Caption         =   "De&talle"
               Height          =   375
               Index           =   1
               Left            =   8880
               TabIndex        =   3
               Top             =   2835
               Width           =   1815
            End
            Begin VB.CommandButton cmdVerFases 
               Caption         =   "Ver Fa&ses/Recursos"
               Height          =   375
               Index           =   0
               Left            =   8880
               TabIndex        =   32
               Top             =   1710
               Width           =   1815
            End
            Begin VB.CommandButton cmdAnular 
               Caption         =   "&Anular"
               Enabled         =   0   'False
               Height          =   375
               Index           =   0
               Left            =   8880
               TabIndex        =   30
               Top             =   135
               Width           =   1815
            End
            Begin VB.CommandButton cmdModificar 
               Caption         =   "&Modificar"
               Enabled         =   0   'False
               Height          =   375
               Index           =   0
               Left            =   8880
               TabIndex        =   29
               Top             =   660
               Width           =   1815
            End
            Begin VB.CommandButton cmdVerSeleccion 
               Caption         =   "Ver Se&lecci�n "
               Height          =   375
               Index           =   1
               Left            =   8880
               TabIndex        =   28
               Top             =   3360
               Width           =   1815
            End
            Begin VB.CommandButton cmdVerSolicitud 
               Caption         =   "Ver S&olicitud"
               Height          =   375
               Index           =   0
               Left            =   8865
               TabIndex        =   27
               Top             =   1185
               Width           =   1815
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   3615
               Index           =   2
               Left            =   90
               TabIndex        =   31
               Top             =   135
               Width           =   8655
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               AllowUpdate     =   0   'False
               RowSelectionStyle=   1
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               SelectByCell    =   -1  'True
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   15266
               _ExtentY        =   6376
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraFrame1 
            BorderStyle     =   0  'None
            Caption         =   "PdtesRecitar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3885
            Index           =   3
            Left            =   -74880
            TabIndex        =   21
            Top             =   360
            Width           =   10815
            Begin VB.CommandButton cmdVerFases 
               Caption         =   "Ver Fa&ses/Recursos"
               Height          =   375
               Index           =   2
               Left            =   8910
               TabIndex        =   2
               Top             =   1680
               Width           =   1815
            End
            Begin VB.CommandButton cmdVerSolicitud 
               Caption         =   "Ver S&olicitud"
               Height          =   375
               Index           =   1
               Left            =   8910
               TabIndex        =   33
               Top             =   1170
               Width           =   1815
            End
            Begin VB.CommandButton cmdRecitarAgenda 
               Caption         =   "&Concertar Citas"
               Height          =   375
               Index           =   0
               Left            =   8895
               TabIndex        =   24
               Top             =   660
               Width           =   1815
            End
            Begin VB.CommandButton cmdRecitarPlan 
               Caption         =   "Rec&itaci�n Planificada"
               Height          =   375
               Index           =   0
               Left            =   8880
               TabIndex        =   23
               Top             =   135
               Width           =   1815
            End
            Begin VB.CommandButton cmdVerSeleccion 
               Caption         =   "Ver Se&lecci�n "
               Height          =   375
               Index           =   2
               Left            =   8895
               TabIndex        =   22
               Top             =   3360
               Width           =   1815
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   3615
               Index           =   3
               Left            =   90
               TabIndex        =   25
               Top             =   135
               Width           =   8655
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               AllowUpdate     =   0   'False
               RowSelectionStyle=   1
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               SelectByCell    =   -1  'True
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   15266
               _ExtentY        =   6376
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraFrame1 
            BorderStyle     =   0  'None
            Caption         =   "SinCitar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3885
            Index           =   1
            Left            =   150
            TabIndex        =   17
            Top             =   420
            Width           =   10755
            Begin VB.CommandButton cmdVerFases 
               Caption         =   "Ver Fa&ses/Recursos"
               Height          =   375
               Index           =   1
               Left            =   8895
               TabIndex        =   1
               Top             =   630
               Width           =   1815
            End
            Begin VB.CommandButton cmdDetalle 
               Caption         =   "De&talle"
               Height          =   375
               Index           =   0
               Left            =   8895
               TabIndex        =   34
               Top             =   2775
               Width           =   1815
            End
            Begin VB.CommandButton cmdAgenda 
               Caption         =   "&Concertar Citas"
               Height          =   375
               Index           =   0
               Left            =   8895
               TabIndex        =   19
               Top             =   90
               Width           =   1815
            End
            Begin VB.CommandButton cmdVerSeleccion 
               Caption         =   "Ver Se&lecci�n "
               Height          =   375
               Index           =   0
               Left            =   8895
               TabIndex        =   18
               Top             =   3315
               Width           =   1815
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   3615
               Index           =   1
               Left            =   120
               TabIndex        =   20
               Top             =   90
               Width           =   8640
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               AllowUpdate     =   0   'False
               RowSelectionStyle=   1
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               SelectTypeCol   =   0
               SelectByCell    =   -1  'True
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   15240
               _ExtentY        =   6376
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pacientes"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Index           =   0
      Left            =   120
      TabIndex        =   12
      Top             =   480
      Width           =   11295
      Begin TabDlg.SSTab tabPacientes 
         Height          =   1815
         HelpContextID   =   90001
         Left            =   150
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   360
         Width           =   11040
         _ExtentX        =   19473
         _ExtentY        =   3201
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "CI1024.frx":0060
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "txtText1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txtText1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(4)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "IdPersona1"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "CI1024.frx":007C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin IdPerson.IdPersona IdPersona1 
            Height          =   1335
            Left            =   150
            TabIndex        =   0
            Tag             =   "Persona"
            Top             =   360
            Width           =   10335
            _ExtentX        =   18230
            _ExtentY        =   2355
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   8790
            TabIndex        =   9
            Tag             =   "N�mero Historia"
            Top             =   420
            Visible         =   0   'False
            Width           =   1410
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22DNI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   6840
            TabIndex        =   8
            Tag             =   "D.N.I|D.N.I "
            Top             =   405
            Visible         =   0   'False
            Width           =   1845
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   8010
            TabIndex        =   7
            Tag             =   "Segundo Apellido|Segundo Apellido"
            Top             =   1200
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   7140
            TabIndex        =   6
            Tag             =   "Primer Apellido|Primer Apellido"
            Top             =   855
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NOMBRE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   1
            Left            =   4575
            TabIndex        =   5
            Tag             =   "Nombre Persona|Nombre Persona"
            Top             =   405
            Visible         =   0   'False
            Width           =   1950
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1305
            Index           =   0
            Left            =   -74880
            TabIndex        =   14
            Top             =   450
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   18865
            _ExtentY        =   2302
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   360
      Left            =   0
      TabIndex        =   11
      Top             =   7710
      Width           =   11580
      _ExtentX        =   20426
      _ExtentY        =   635
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
   Begin VB.Menu mnuEstado 
      Caption         =   "Estados Cita"
      Visible         =   0   'False
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&0. Sin citar"
         Index           =   0
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&1. Confirmar"
         Index           =   1
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&2. Anular"
         Index           =   2
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&3. Recitar"
         Index           =   3
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&4. Pendiente de Recitar"
         Index           =   4
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "-"
         Index           =   5
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "Lista de Espera ?  (S/N)"
         Index           =   6
      End
   End
End
Attribute VB_Name = "frmCitasPaciente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cmdAgenda_Click(intIndex As Integer)

  Dim blnCancelTrans As Boolean

  'objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans

  Call objSelRefresh(grdDBGrid1(tabTab1(2).Tab + 1), , ciOpcAgenda)

  If grdDBGrid1(tabTab1(2).Tab + 1).SelBookmarks.Count > 0 Then
   'LLamar a la pantalla de Solicitud de citas
    Me.MousePointer = vbHourglass
    'Load frmSolicitud
    'frmSolicitud.Show vbModal
    'Unload frmSolicitud
    'Set frmSolicitud = Nothing
    
    Call objSecurity.LaunchProcess(ciSoluciones)
    Me.MousePointer = vbDefault
  Else
    Call objError.SetError(cwCodeMsg, "No hay ninguna Actuaci�n seleccionada")
    Call objError.Raise
    Exit Sub
  End If
  If blnCancelTrans Then
    'objApp.rdoConnect.RollbackTrans
    'Call objError.SetError(cwCodeMsg, ciErrRollback)
    Call objError.SetError(cwCodeMsg, "Citar Actuaciones Cancelado")
    Call objError.Raise
  Else
    If blnCommit Then
      Me.MousePointer = vbHourglass
      'objApp.rdoConnect.CommitTrans
      Call objWinInfo.DataRefresh
      Call Me.Refresh
      Me.MousePointer = vbDefault
      DoEvents
      blnCommit = False
    End If
  End If

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next

End Sub

Private Sub cmdAnular_Click(Index As Integer)
  Dim lngIncidencia       As Long
  Dim blnCancelTrans      As Boolean
  Dim blnReturn           As Boolean
  Dim intGridIndex        As Integer
  Dim blnAnularSolicitud As Boolean
  
  On Error GoTo Canceltrans
  
 lngNumSolicitud = grdDBGrid1(2).Columns("Solicitud").Value
 Select Case ViewSolicitud
   Case 1 'han pulsado anular actuacion
      blnAnularSolicitud = False
   Case 2 'han pulsado anular SOLICITUD
      blnAnularSolicitud = True
   Case 3 'han pulsado cancelar
     Exit Sub
 End Select
 
 blnActive = True
  objApp.rdoConnect.BeginTrans
  intGridIndex = tabTab1(2).Tab + 1
  lngIncidencia = AddInciden(ciCodInciAnulacion) 'esta variable se encuentra en el m�dulo general
  If lngIncidencia <> -1 And lngIncidencia <> 0 Then  'ha generado la incidencia n�mero lngIncidencia
    If grdDBGrid1(intGridIndex).SelBookmarks.Count <> CitasChangeStatus(grdDBGrid1(intGridIndex), ciAnulada, lngIncidencia, blnAnularSolicitud) Then
      blnCancelTrans = True
    End If
  Else
    blnCancelTrans = True
  End If

  If blnCancelTrans Then
    objApp.rdoConnect.RollbackTrans
    Call objError.SetError(cwCodeMsg, IIf(blnActive, "Anulaci�n Cancelada", ciErrInciNoActive & vbCrLf))
    Call objError.Raise
  Else
    Me.MousePointer = vbHourglass
    objApp.rdoConnect.CommitTrans
    Call objWinInfo.DataRefresh
    Call Me.Refresh
    Me.MousePointer = vbDefault
    DoEvents
  End If
 
  
 Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next

End Sub


Private Sub cmdDetalle_Click(intIndex As Integer)
  If Val(grdDBGrid1(tabTab1(2).Tab + 1).Columns("N�mero").Value) > 0 Then
    If tabTab1(2).Tab + 1 = 1 Then
      Call ShowDetail(Val(grdDBGrid1(tabTab1(2).Tab + 1).Columns("N�mero").Value))
    Else
      Call ShowDetail(Val(grdDBGrid1(tabTab1(2).Tab + 1).Columns("N�mero").Value), Val(grdDBGrid1(tabTab1(2).Tab + 1).Columns("Solicitud").Value), Val(grdDBGrid1(tabTab1(2).Tab + 1).Columns("N�mero Cita").Value))
    End If
    Call IdPersona1.BeginControl(objApp, objGen)

  End If
End Sub

Private Sub cmdModificar_Click(Index As Integer)

  Dim lngIncidencia       As Long
  Dim blnCancelTrans      As Boolean
  Dim blnReturn           As Boolean
  Dim intGridIndex        As Integer

  objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans
  blnActive = True
  
  lngNumSolicitud = grdDBGrid1(2).Columns("Solicitud").Value
  If ViewSolicitud(True) = 2 Then
    
    intGridIndex = tabTab1(2).Tab + 1
    lngIncidencia = AddInciden(ciCodInciModificacion) 'esta variable se encuentra en el m�dulo general
    If lngIncidencia <> -1 And lngIncidencia <> 0 Then  'ha generado la incidencia n�mero lngIncidencia
      If grdDBGrid1(intGridIndex).SelBookmarks.Count <> CitasChangeStatus(grdDBGrid1(intGridIndex), ciAnulada, lngIncidencia) Then
        blnCancelTrans = True
      Else
       Me.MousePointer = vbHourglass
       Call objSelRefresh(grdDBGrid1(tabTab1(2).Tab + 1), 0, ciOpcModificar)
      
      
      'Load frmSolicitud
      'frmSolicitud.Show vbModal
      'Unload frmSolicitud
      'Set frmSolicitud = Nothing
      
      Call objSecurity.LaunchProcess(ciSoluciones)
      Me.MousePointer = vbDefault
 
     End If
   Else
     blnCancelTrans = True
   End If

   If blnCancelTrans Or Not blnCommit Then
     objApp.rdoConnect.RollbackTrans
    
     Call objError.SetError(cwCodeMsg, IIf(blnActive, "Modificaci�n Cancelada", ciErrInciNoActive & vbCrLf))
     Call objError.Raise
   Else
     objApp.rdoConnect.CommitTrans
     Call objWinInfo.DataRefresh
     Call Me.Refresh
     DoEvents
   End If

  End If
  Exit Sub

Canceltrans:
   blnReturn = False
   blnCancelTrans = True
   Resume Next

End Sub

Private Sub cmdRecitarPlan_Click(Index As Integer)

  Dim blnCancelTrans As Boolean

  'objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans
  blnRecMode = False
  
  
  'Load frmRecitaCion
  'frmRecitaCion.Show vbModal
  'Unload frmRecitaCion
  'Set frmRecitaCion = Nothing
    
  Call objSecurity.LaunchProcess(ciRecitacion)

  If blnCancelTrans Then
  '  objApp.rdoConnect.RollbackTrans
    'Call objError.SetError(cwCodeMsg, ciErrRollback)
    Call objError.SetError(cwCodeMsg, "Recitaci�n Planificada Cancelada")
    Call objError.Raise
  Else
    If blnCommit Then
   '   objApp.rdoConnect.CommitTrans
      Call objWinInfo.DataRefresh
      Call Me.Refresh
      DoEvents
      blnCommit = False
    End If
  End If

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next

End Sub

Private Sub cmdRecitarAgenda_Click(Index As Integer)

  Dim blnCancelTrans      As Boolean
  Dim blnReturn           As Boolean
  Dim intGridIndex        As Integer

  'objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans
  blnActive = True

  intGridIndex = tabTab1(2).Tab + 1
  If grdDBGrid1(intGridIndex).SelBookmarks.Count <> CitasChangeStatus(grdDBGrid1(intGridIndex), ciRecitada) Then
    blnCancelTrans = True
  Else
    Call objSelRefresh(grdDBGrid1(tabTab1(2).Tab + 1), 0, ciOpcRecitar)
   ' Load frmSolicitud
   ' frmSolicitud.Show vbModal
   ' Unload frmSolicitud
   ' Set frmSolicitud = Nothing
    
    Call objSecurity.LaunchProcess(ciSoluciones)

  End If

  If blnCancelTrans Then
   ' objApp.rdoConnect.RollbackTrans
    'Call objError.SetError(cwCodeMsg, ciErrRollback)
    Call objError.SetError(cwCodeMsg, "Citar Actuaciones Cancelada")
    Call objError.Raise
  Else
   ' objApp.rdoConnect.CommitTrans
    If blnCommit Then
      Call objWinInfo.DataRefresh
      Call Me.Refresh
      DoEvents
    End If
  End If

  Exit Sub

Canceltrans:
   blnReturn = False
   blnCancelTrans = True
   Resume Next

End Sub

Private Sub cmdVerFases_Click(Index As Integer)

  Dim blnCancelTrans As Boolean
  Dim intGridIndex As Integer

'  objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans
   
  
  intGridIndex = tabTab1(2).Tab + 1
  vntNumActPlan = grdDBGrid1(intGridIndex).Columns("N�mero").Value
  vntNumActPedi = grdDBGrid1(intGridIndex).Columns("Actuaci�n Pedida").Value
  vntNumSolicit = grdDBGrid1(intGridIndex).Columns("Solicitud").Value
  vntNumCita = grdDBGrid1(intGridIndex).Columns("N�mero Cita").Value
  Me.MousePointer = vbHourglass
  Load frmFasesPCR
  frmFasesPCR.Show vbModal
  
  Unload frmFasesPCR
  Set frmFasesPCR = Nothing
  
  'Call objSecurity.LaunchProcess(ciFasesRecursos)
  Me.MousePointer = vbDefault


  If blnCancelTrans Then
'    objApp.rdoConnect.RollbackTrans
    Call objError.SetError(cwCodeMsg, ciErrRollback)
    Call objError.Raise
  Else
'    objApp.rdoConnect.CommitTrans
   'Call objWinInfo.DataRefresh
   ' Call Me.Refresh
   ' DoEvents
  End If

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next

End Sub

Private Sub cmdVerListaEspera_Click(Index As Integer)
  Call objSecurity.LaunchProcess(ciListaEsperaPac, IdPersona1.Text)
End Sub

Private Sub cmdVerSeleccion_Click(Index As Integer)
  Call objGen.RemoveCollection(objSolicitud.cllPeticiones)
  Call objSelRefresh(grdDBGrid1(tabTab1(2).Tab + 1))
  Call objShowSelected
  If blnBorrarSel Then
    grdDBGrid1(tabTab1(2).Tab + 1).SelBookmarks.RemoveAll
    blnBorrarSel = False
  End If
End Sub

Private Sub Form_Load()
  
  Dim objMasterInfo     As New clsCWForm
  Dim objMultiInfo1     As New clsCWForm
  Dim objMultiInfo2     As New clsCWForm
  Dim objMultiInfo3     As New clsCWForm
  Dim intGridIndex      As Integer
  Call objApp.SplashOn
  
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)
  Set objWinInfo = New clsCWWin
  tabTab1(2).Tab = 0
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objWinInfo.objDoc
    .cwPRJ = "Citas"
    .cwMOD = "Actuaciones seg�n su situaci�n por Recurso o Paciente"
    .cwDAT = "18-09-97"
    .cwAUT = "Rober"
    
    .cwDES = ""
    
    .cwUPD = "18-09-97 - Rober - Creaci�n del m�dulo"
    
    .cwEVT = "En el evento de PreValidate se realizar�..."
  End With
  
  With objMasterInfo
    '.strName = "Paciente"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPacientes
    Set .grdGrid = grdDBGrid1(0)
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2200"
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("CI21CODPERSONA", False)

  End With
  
  With objMultiInfo1
    '.strName = "SinCitar"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .strWhere = "CI01NUMCITA IS NULL"
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .intCursorSize = -1
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "PR0431J"
    .intAllowance = cwAllowReadOnly

    Call .FormAddOrderField("PR03FECPREFEREN", False)
    Call .FormAddRelation("CI21CODPERSONA", IdPersona1)
        
  End With

  With objMultiInfo2
    '.strName = "Confirmadas"
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .strWhere = "CI01SITCITA = '" & ciConfirmada & "' AND PR37CODESTADO IN (1,2,4,7)"
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .intCursorSize = -1
    
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "PR0431J"
    .intAllowance = cwAllowReadOnly
    '.intAllowance = cwAllowAll
    .blnEnabled = False
    Call .FormAddOrderField("CI01FECCONCERT", False)
    Call .FormAddRelation("CI21CODPERSONA", IdPersona1)
        
  End With

  With objMultiInfo3
    '.strName = "Pendientes"
    Set .objFormContainer = fraFrame1(3)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(3)
    .strWhere = "CI01SITCITA = '" & ciPdteRecitar & "'"
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .intCursorSize = -1
    
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "PR0431J"
    .intAllowance = cwAllowReadOnly
    .blnEnabled = False
    Call .FormAddOrderField("CI01FECCONCERT", False)
    Call .FormAddRelation("CI21CODPERSONA", IdPersona1)
    
  End With


  With objWinInfo
    
    Call .FormAddInfo(objMasterInfo, cwFormDetail)

    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo1, "Citable", "PR03INDCITABLE", cwBoolean)
    Call .GridAddColumn(objMultiInfo1, "Persona", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo1, "N�mero", "PR04NUMACTPLAN")
    Call .GridAddColumn(objMultiInfo1, "C�digo", "PR01CODACTUACION")
    Call .GridAddColumn(objMultiInfo1, "Descripci�n", "")
    Call .GridAddColumn(objMultiInfo1, "C�dDepartamento", "AD02CODDPTO")
    Call .GridAddColumn(objMultiInfo1, "Departamento", "")
    Call .GridAddColumn(objMultiInfo1, "Fecha Cita", "CI01FECCONCERT")
    Call .GridAddColumn(objMultiInfo1, "C�dDepartamentoPed", "AD02CODDPTO_PED")
    Call .GridAddColumn(objMultiInfo1, "Departamento Solicitante", "")
    Call .GridAddColumn(objMultiInfo1, "Persona Solicitante", "SG02COD")
    
    Call .GridAddColumn(objMultiInfo1, "Lista Espera", "CI01INDLISESPE", cwBoolean)
    Call .GridAddColumn(objMultiInfo1, "Fecha Preferencia", "PR03FECPREFEREN")
    Call .GridAddColumn(objMultiInfo1, "Solicitud", "CI31NUMSOLICIT")
    Call .GridAddColumn(objMultiInfo1, "N�mero Cita", "CI01NUMCITA")
    Call .GridAddColumn(objMultiInfo1, "Petici�n", "PR09NUMPETICION")
    Call .GridAddColumn(objMultiInfo1, "Actuaci�n Pedida", "PR03NUMACTPEDI")
    Call .GridAddColumn(objMultiInfo1, "Situaci�n", "CI01SITCITA")
    Call .GridAddColumn(objMultiInfo1, "CodRecAsi", "AG11CODRECURSO_ASI")
    Call .GridAddColumn(objMultiInfo1, "CodPref", "AG11CODRECURSO_PRE")
    Call .GridAddColumn(objMultiInfo1, "Recurso Asignado", "")
    Call .GridAddColumn(objMultiInfo1, "Recurso Preferente", "")
    Call .GridAddColumn(objMultiInfo1, "Recordatorio", "CI01INDRECORDA", cwBoolean)
    
    Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo2, "Persona", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo2, "N�mero", "PR04NUMACTPLAN")
    Call .GridAddColumn(objMultiInfo2, "C�digo", "PR01CODACTUACION")
    Call .GridAddColumn(objMultiInfo2, "Descripci�n", "")
    Call .GridAddColumn(objMultiInfo2, "C�dDepartamento", "AD02CODDPTO")
    Call .GridAddColumn(objMultiInfo2, "Departamento", "")
    Call .GridAddColumn(objMultiInfo2, "Fecha Cita", "CI01FECCONCERT")
    Call .GridAddColumn(objMultiInfo2, "C�dDepartamentoPed", "AD02CODDPTO_PED")
    Call .GridAddColumn(objMultiInfo2, "Departamento Solicitante", "")
    Call .GridAddColumn(objMultiInfo2, "Persona Solicitante", "SG02COD")
    Call .GridAddColumn(objMultiInfo2, "Lista Espera", "CI01INDLISESPE", cwBoolean)
    Call .GridAddColumn(objMultiInfo2, "Fecha Preferencia", "PR03FECPREFEREN")
    Call .GridAddColumn(objMultiInfo2, "Solicitud", "CI31NUMSOLICIT")
    Call .GridAddColumn(objMultiInfo2, "N�mero Cita", "CI01NUMCITA")
    Call .GridAddColumn(objMultiInfo2, "Petici�n", "PR09NUMPETICION")
    Call .GridAddColumn(objMultiInfo2, "Actuaci�n Pedida", "PR03NUMACTPEDI")
    Call .GridAddColumn(objMultiInfo2, "Situaci�n", "CI01SITCITA")
    Call .GridAddColumn(objMultiInfo2, "CodRecAsi", "AG11CODRECURSO_ASI")
    Call .GridAddColumn(objMultiInfo2, "CodPref", "AG11CODRECURSO_PRE")
    Call .GridAddColumn(objMultiInfo2, "Recurso Asignado", "")
    Call .GridAddColumn(objMultiInfo2, "Recurso Preferente", "")
    Call .GridAddColumn(objMultiInfo2, "Recordatorio", "CI01INDRECORDA", cwBoolean)
  
    Call .FormAddInfo(objMultiInfo3, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo3, "Persona", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo3, "N�mero", "PR04NUMACTPLAN")
    Call .GridAddColumn(objMultiInfo3, "C�digo", "PR01CODACTUACION")
    Call .GridAddColumn(objMultiInfo3, "Descripci�n", "")
    Call .GridAddColumn(objMultiInfo3, "C�dDepartamento", "AD02CODDPTO")
    Call .GridAddColumn(objMultiInfo3, "Departamento", "")
    Call .GridAddColumn(objMultiInfo3, "Fecha Cita", "CI01FECCONCERT")
    Call .GridAddColumn(objMultiInfo3, "C�dDepartamentoPed", "AD02CODDPTO_PED")
    Call .GridAddColumn(objMultiInfo3, "Departamento Solicitante", "")
    Call .GridAddColumn(objMultiInfo3, "Persona Solicitante", "SG02COD")
    Call .GridAddColumn(objMultiInfo3, "Lista Espera", "CI01INDLISESPE", cwBoolean)
    Call .GridAddColumn(objMultiInfo3, "Fecha Preferencia", "PR03FECPREFEREN")
    Call .GridAddColumn(objMultiInfo3, "Solicitud", "CI31NUMSOLICIT")
    Call .GridAddColumn(objMultiInfo3, "N�mero Cita", "CI01NUMCITA")
    Call .GridAddColumn(objMultiInfo3, "Petici�n", "PR09NUMPETICION")
    Call .GridAddColumn(objMultiInfo3, "Actuaci�n Pedida", "PR03NUMACTPEDI")
    Call .GridAddColumn(objMultiInfo3, "Incidencia", "AG05NUMINCIDEN")
    Call .GridAddColumn(objMultiInfo3, "Situaci�n", "CI01SITCITA")
    Call .GridAddColumn(objMultiInfo3, "CodRecAsi", "AG11CODRECURSO_ASI")
    Call .GridAddColumn(objMultiInfo3, "CodPref", "AG11CODRECURSO_PRE")
    Call .GridAddColumn(objMultiInfo3, "Recurso Asignado", "")
    Call .GridAddColumn(objMultiInfo3, "Recurso Preferente", "")
    Call .GridAddColumn(objMultiInfo3, "Recordatorio", "CI01INDRECORDA", cwBoolean)
    
    
    Call .FormCreateInfo(objMasterInfo)
    
    IdPersona1.ToolTipText = ""
    .CtrlGetInfo(IdPersona1).blnForeign = True
    
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    
    grdDBGrid1(1).Columns("grddbgrid1(1).Citable").width = 615
    grdDBGrid1(1).Columns("Fecha Cita").Visible = False
    grdDBGrid1(1).Columns("Solicitud").Visible = False
    grdDBGrid1(1).Columns("Recurso Asignado").Visible = False

    For intGridIndex = 1 To 3
       
       'comentarizados para realizar pruebas luego seran invisibles
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Persona").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�digo").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodRecAsi").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodPref").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�dDepartamento").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�dDepartamentoPed").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero Cita").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Situaci�n").Visible = False
       
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Petici�n").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Actuaci�n Pedida").Visible = False
       
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Descripci�n").width = TextWidth(String(30, "O"))
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Departamento").width = TextWidth(String(22, "O"))
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Departamento Solicitante").width = TextWidth(String(25, "O"))
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Recurso Asignado").width = TextWidth(String(30, "O"))
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Recurso Preferente").width = TextWidth(String(30, "O"))
       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�digo")), "PR01CODACTUACION", "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�digo")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Descripci�n"), "PR01DESCORTA")
    
       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�dDepartamento")), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�dDepartamento")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Departamento"), "AD02DESDPTO")
       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�dDepartamentoPed")), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�dDepartamentoPed")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Departamento Solicitante"), "AD02DESDPTO")
       
       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodRecAsi")), "AG11CODRECURSO_ASI", "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodRecAsi")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Recurso Asignado"), "AG11DESRECURSO")
    
       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodPref")), "AG11CODRECURSO_PRE", "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodPref")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Recurso Preferente"), "AG11DESRECURSO")
   
    
    Next
    Call .WinRegister
    Call .WinStabilize
    IdPersona1.BackColor = objApp.objUserColor.lngKey
    blnCommit = False
  End With
      
      cmdAgenda(0).Enabled = False
      cmdVerFases(0).Enabled = False
      cmdVerFases(1).Enabled = False
      cmdVerFases(2).Enabled = False
      cmdDetalle(0).Enabled = False
      cmdDetalle(1).Enabled = False
      cmdDetalle(2).Enabled = False
      cmdVerSeleccion(0).Enabled = False
      cmdVerSeleccion(1).Enabled = False
      cmdVerSeleccion(2).Enabled = False
      cmdAnular(0).Enabled = False
      cmdModificar(0).Enabled = False
      cmdVerSolicitud(0).Enabled = False
      cmdVerSolicitud(1).Enabled = False
      cmdRecitarPlan(0).Enabled = False
      cmdRecitarAgenda(0).Enabled = False

 Call objApp.SplashOff
End Sub

Private Sub grdDBGrid1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdDBGrid1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub grdDBGrid1_HeadClick(intIndex As Integer, ByVal ColIndex As Integer)
Dim strFieldOrder       As String
  strFieldOrder = objWinInfo.CtrlGetInfo(grdDBGrid1(intIndex).Columns(grdDBGrid1(intIndex).Columns(ColIndex).Name)).objControl.DataField
  If strFieldOrder <> "" Then
    Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
    Call objWinInfo.objWinActiveForm.FormAddOrderField(strFieldOrder, False)
    Call objWinInfo.DataRefresh
  End If
End Sub

Private Sub grdDBGrid1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'  If Button = 2 Then
'    PopupMenu mnuEstado
'  End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  If intIndex = 1 Then
    grdDBGrid1(1).Columns("Fecha Preferencia").Text = Format(grdDBGrid1(1).Columns("Fecha Preferencia").Text, "DD/MM/YYYY  HH:NN")
    grdDBGrid1(1).Columns("Recordatorio").Value = -1
    grdDBGrid1(1).Columns("Persona Solicitante").Text = GetStaffName(Val(grdDBGrid1(1).Columns("Persona Solicitante").Value))
  End If
  If intIndex = 2 Then
    grdDBGrid1(2).Columns("Fecha Cita").Text = Format(grdDBGrid1(2).Columns("Fecha Cita").Text, "DD/MM/YYYY HH:NN")
    grdDBGrid1(2).Columns("Fecha Preferencia").Text = Format(grdDBGrid1(2).Columns("Fecha Preferencia").Text, "DD/MM/YYYY  HH:NN")
    grdDBGrid1(2).Columns("Persona Solicitante").Text = GetStaffName(Val(grdDBGrid1(2).Columns("Persona Solicitante").Value))
  End If
  If intIndex = 3 Then
    grdDBGrid1(3).Columns("Fecha Cita").Text = Format(grdDBGrid1(3).Columns("Fecha Cita").Text, "DD/MM/YYYY - HH:NN")
    grdDBGrid1(3).Columns("Fecha Preferencia").Text = Format(grdDBGrid1(3).Columns("Fecha Preferencia").Text, "DD/MM/YYYY  HH:NN")
    grdDBGrid1(3).Columns("Persona Solicitante").Text = GetStaffName(Val(grdDBGrid1(3).Columns("Persona Solicitante").Value))
  End If
  
End Sub

Private Sub grdDBGrid1_SelChange(intIndex As Integer, ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
'Select Case intIndex
'   Case 2
'      If grdDBGrid1(2).SelBookmarks.Count > 0 Then
         cmdAnular(0).Enabled = True
         cmdModificar(0).Enabled = True
         cmdVerSolicitud(0).Enabled = True
'      Else
'         cmdCommand1(0).Enabled = False
'         cmdCommand1(1).Enabled = False
'         cmdCommand1(7).Enabled = False
'      End If
'End Select
 If intIndex = 1 Then
   If grdDBGrid1(intIndex).Columns("Citable").Value = 0 Then
     Cancel = True
    End If
 End If
End Sub



Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
 
End Sub

Private Sub IdPersona1_GotFocus()
'  frmCitasPaciente.MousePointer = vbHourglass
'  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call objWinInfo.CtrlGotFocus
'  frmCitasPaciente.MousePointer = vbDefault
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub mnuEstadoOpcion_Click(intIndex As Integer)
  Dim intGridIndex As Integer
  intGridIndex = tabTab1(2).Tab + 1
  Call CitasChangeStatus(grdDBGrid1(intGridIndex), LTrim(Str(intIndex)))
  Call objWinInfo.DataRefresh
End Sub

'Private Sub IdPersona1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
'End Sub

'Private Sub IdPersona1_LostFocus(intIndex As Integer)
'  Call objWinInfo.CtrlLostFocus
'End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  If strFormName = "fraFrame1(0)" And strCtrl = "IdPersona1" Then
     IdPersona1.SearchPersona
     objWinInfo.DataRefresh
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
'Poner el color en titulo del frame exterior de actuaciones
  fraFrame1(9).ForeColor = fraFrame1(tabTab1(2).Tab + 1).ForeColor
  If strFormName = "fraFrame1(0)" Then
    IdPersona1.blnAvisos = True
    cmdAgenda(0).Enabled = False
    cmdVerFases(0).Enabled = False
    cmdVerFases(1).Enabled = False
    cmdVerFases(2).Enabled = False
    cmdDetalle(0).Enabled = False
    cmdDetalle(1).Enabled = False
    cmdDetalle(2).Enabled = False
    cmdVerSeleccion(0).Enabled = False
    cmdVerSeleccion(1).Enabled = False
    cmdVerSeleccion(2).Enabled = False
    cmdAnular(0).Enabled = False
    cmdModificar(0).Enabled = False
    cmdVerSolicitud(0).Enabled = False
    cmdVerSolicitud(1).Enabled = False
    cmdRecitarPlan(0).Enabled = False
    cmdRecitarAgenda(0).Enabled = False

  Else
    IdPersona1.blnAvisos = False
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) = 0 Then
      objWinInfo.DataRefresh
    End If
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
      cmdAgenda(0).Enabled = True
      cmdVerFases(0).Enabled = True
      cmdVerFases(1).Enabled = True
      cmdVerFases(2).Enabled = True
      cmdDetalle(0).Enabled = True
      cmdDetalle(1).Enabled = True
      cmdDetalle(2).Enabled = True
      cmdVerSeleccion(0).Enabled = True
      cmdVerSeleccion(1).Enabled = True
      cmdVerSeleccion(2).Enabled = True
      cmdAnular(0).Enabled = True
      cmdModificar(0).Enabled = True
      cmdVerSolicitud(0).Enabled = True
      cmdVerSolicitud(1).Enabled = True
      cmdRecitarPlan(0).Enabled = True
      cmdRecitarAgenda(0).Enabled = True
    Else
      cmdAgenda(0).Enabled = False
      cmdVerFases(0).Enabled = False
      cmdVerFases(1).Enabled = False
      cmdVerFases(2).Enabled = False
      cmdDetalle(0).Enabled = False
      cmdDetalle(1).Enabled = False
      cmdDetalle(2).Enabled = False
      cmdVerSeleccion(0).Enabled = False
      cmdVerSeleccion(1).Enabled = False
      cmdVerSeleccion(2).Enabled = False
      cmdAnular(0).Enabled = False
      cmdModificar(0).Enabled = False
      cmdVerSolicitud(0).Enabled = False
      cmdVerSolicitud(1).Enabled = False
      cmdRecitarPlan(0).Enabled = False
      cmdRecitarAgenda(0).Enabled = False
    End If
  End If
  
End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'  If strFormName = "fraFrame1(0)" Then
'    Call IdPersona1.ReadPersona
'  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabPacientes_Click(PreviousTab As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Me.MousePointer = vbDefault
End Sub

Private Sub tabPacientes_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)

End Sub

Private Sub tabtab1_Click(intIndex As Integer, PreviousTab As Integer)
  Dim intTab As Integer
  Dim strMensaje As String
  'For intTab = 0 To tabtab1(intIndex).Tabs - 1
       objWinInfo.cllWinForms("fraFrame1(" & PreviousTab + 1 & ")").blnEnabled = False
       objWinInfo.cllWinForms("fraFrame1(" & tabTab1(intIndex).Tab + 1 & ")").blnEnabled = True
      ' grdDBGrid1(PreviousTab + 1).Visible = False
      ' grdDBGrid1(tabtab1(intIndex).Tab + 1).Visible = True

  'Next intTab
  'objWinInfo.cllWinForms("fraframe1(" & tabtab1(intIndex).Tab + 1 & ")").blnEnabled = True
  'grdDBGrid1(intIndex + 1).Visible = True
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
 ' If btnButton.Key = "b9" And objWinInfo.objWinActiveForm.strName = "fraFrame1(0)" Then
 '   IdPersona1.SearchPersona
 ' Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
 ' End If
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  'Me.MousePointer = vbDefault
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   
End Sub

Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)

  Me.MousePointer = vbHourglass
  Call objWinInfo.FormChangeActive(fraFrame1(tabTab1(intIndex).Tab + 1), False, True)

  Me.MousePointer = vbDefault
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
   frmCitasPaciente.MousePointer = vbHourglass
   If intIndex = 9 Then
      Call objWinInfo.FormChangeActive(fraFrame1(tabTab1(2).Tab + 1), False, True)
   Else
       Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
   End If
   frmCitasPaciente.MousePointer = vbDefault
End Sub

Private Sub cmdVerSolicitud_Click(intIndex As Integer)

  Dim blnCancelTrans      As Boolean
  Dim intGridIndex        As Integer
  Dim lngSolicitud        As Long

'  objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans

  intGridIndex = tabTab1(2).Tab + 1
  lngSolicitud = grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Solicitud").Value

  objSolicitud.RemovePeticiones

  Me.MousePointer = vbHourglass
  Call objLoadFromSolicit(lngSolicitud, ciOpcVerSolicitud)
 
  'Load frmSolicitud
  'frmSolicitud.Show vbModal
  'Unload frmSolicitud
  'Set frmSolicitud = Nothing

  Call objSecurity.LaunchProcess(ciSoluciones)
  Me.MousePointer = vbDefault
  
  If blnCancelTrans Or Not blnCommit Then
 '   objApp.rdoConnect.RollbackTrans
    If blnCancelTrans Then
     Call objError.SetError(cwCodeMsg, ciErrRollback)
     Call objError.Raise
    End If
  Else
 '    objApp.rdoConnect.CommitTrans
     Call objWinInfo.DataRefresh
     Call Me.Refresh
     DoEvents
     blnCommit = False
    
  End If

  Call objGen.RemoveCollection(objSolicitud.cllPeticiones)

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next

End Sub
Private Function CitasChangeStatus(objGrid As SSDBGrid, strNewStatus As String, _
                                  Optional lngIncidencia As Long = -1, Optional blnDelSol As Boolean = False) As Integer
  Dim intRow As Integer
  Dim vntRowBookmark As Variant
  Dim lngSolicitud As Long
  Dim lngCita As Long
  Dim strSql As String
  Dim rsActuacionCitada As rdoResultset
  Dim intRowsChanged As Integer
   
  intRowsChanged = 0
  If objGrid.SelBookmarks.Count > 0 Then
    For intRow = 0 To objGrid.SelBookmarks.Count - 1
      vntRowBookmark = objGrid.SelBookmarks(intRow)
      lngSolicitud = objGrid.Columns(objGrid.Name & "(" & objGrid.Index & ").Solicitud").CellValue(vntRowBookmark)
      lngCita = objGrid.Columns(objGrid.Name & "(" & objGrid.Index & ").N�mero Cita").CellValue(vntRowBookmark)
      If Not blnDelSol Then
        strSql = "Select ROWID, CI01SITCITA, AG05NUMINCIDEN " & _
               "from CI0100 " & _
               "where CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngCita
      Else
        strSql = "Select ROWID, CI01SITCITA, AG05NUMINCIDEN " & _
               "from CI0100 " & _
               "where CI31NUMSOLICIT = " & lngSolicitud
      End If
      Set rsActuacionCitada = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, _
                              rdConcurRowVer)
      If rsActuacionCitada.RowCount > 0 Then
       While Not rsActuacionCitada.EOF
         rsActuacionCitada.Edit
         rsActuacionCitada.rdoColumns("CI01SITCITA") = strNewStatus
         If lngIncidencia <> -1 Then
            rsActuacionCitada.rdoColumns("AG05NUMINCIDEN") = lngIncidencia
         End If
         rsActuacionCitada.Update
         rsActuacionCitada.MoveNext
       Wend
      End If
      rsActuacionCitada.Close
      intRowsChanged = intRowsChanged + 1
    Next
  End If
  Set rsActuacionCitada = Nothing

  CitasChangeStatus = intRowsChanged

End Function



