VERSION 5.00
Object = "{4D2E11F7-A12E-11D1-9D0F-00C04FA619D6}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frmDetalleCita 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Detalle Cita"
   ClientHeight    =   6570
   ClientLeft      =   675
   ClientTop       =   1410
   ClientWidth     =   10470
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "CI1030.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6570
   ScaleWidth      =   10470
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Actuaci�n"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3990
      Index           =   0
      Left            =   60
      TabIndex        =   24
      Top             =   2475
      Width           =   10350
      Begin VB.CommandButton Command1 
         Caption         =   "&Aceptar"
         Height          =   450
         Left            =   8415
         TabIndex        =   0
         Top             =   3420
         Width           =   1800
      End
      Begin VB.Frame Frame2 
         Caption         =   "Incidencias"
         Height          =   900
         Left            =   270
         TabIndex        =   40
         Top             =   2895
         Width           =   6945
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   345
            Index           =   16
            Left            =   1650
            Locked          =   -1  'True
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "Puesto"
            Text            =   "     "
            Top             =   435
            Width           =   5040
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   15
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "N�mero Direcci�n"
            Top             =   435
            Width           =   1170
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   1650
            TabIndex        =   21
            Top             =   225
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N�mero"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   240
            TabIndex        =   3
            Top             =   225
            Width           =   660
         End
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   2
         Left            =   255
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Tag             =   "N�mero Direcci�n"
         Top             =   525
         Width           =   1215
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   3
         Left            =   1725
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Tag             =   "N�mero Direcci�n"
         Top             =   525
         Width           =   1230
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   345
         Index           =   6
         Left            =   255
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "Puesto"
         Text            =   "     "
         Top             =   1140
         Width           =   4545
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Fecha Preferente"
         Height          =   900
         Index           =   5
         Left            =   7815
         TabIndex        =   28
         Top             =   300
         Width           =   2385
         Begin VB.TextBox txtText1 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   11
            Left            =   225
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "N�mero Direcci�n"
            Top             =   420
            Width           =   1245
         End
         Begin VB.TextBox txtText1 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   12
            Left            =   1530
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "N�mero Direcci�n"
            Top             =   420
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   225
            TabIndex        =   30
            Top             =   225
            Width           =   300
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   1530
            TabIndex        =   29
            Top             =   210
            Width           =   420
         End
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   7
         Left            =   4980
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "N�mero Direcci�n"
         Top             =   1140
         Width           =   2220
      End
      Begin VB.CheckBox chkCheck1 
         Caption         =   "Lista de Espera"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   4995
         TabIndex        =   11
         TabStop         =   0   'False
         Tag             =   "Persona Provisional"
         Top             =   1750
         Width           =   1665
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Fecha Concertada"
         Height          =   960
         Index           =   4
         Left            =   7830
         TabIndex        =   25
         Top             =   1395
         Width           =   2385
         Begin VB.TextBox txtText1 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   14
            Left            =   1545
            Locked          =   -1  'True
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "N�mero Direcci�n"
            Top             =   450
            Width           =   660
         End
         Begin VB.TextBox txtText1 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   13
            Left            =   220
            Locked          =   -1  'True
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "N�mero Direcci�n"
            Top             =   450
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   1545
            TabIndex        =   27
            Top             =   225
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   220
            TabIndex        =   26
            Top             =   225
            Width           =   300
         End
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   5
         Left            =   4980
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "N�mero Direcci�n"
         Top             =   525
         Width           =   1170
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   4
         Left            =   3315
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Tag             =   "N�mero Direcci�n"
         Top             =   525
         Width           =   1290
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   8
         Left            =   270
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "Puesto"
         Top             =   1785
         Width           =   3350
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   10
         Left            =   3840
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Tag             =   "Puesto"
         Top             =   2445
         Width           =   3350
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   9
         Left            =   285
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Tag             =   "Puesto"
         Top             =   2445
         Width           =   3350
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�m.Act.Ped."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   255
         TabIndex        =   39
         Top             =   300
         Width           =   1185
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�m.Act.Plan."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   24
         Left            =   1725
         TabIndex        =   38
         Top             =   300
         Width           =   1230
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   255
         TabIndex        =   37
         Top             =   915
         Width           =   1020
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Situaci�n Cita"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   25
         Left            =   4980
         TabIndex        =   36
         Top             =   915
         Width           =   1200
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�m. Cita"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   4980
         TabIndex        =   35
         Top             =   300
         Width           =   840
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�m. Solicitud"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   3315
         TabIndex        =   34
         Top             =   300
         Width           =   1245
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   17
         Left            =   255
         TabIndex        =   33
         Top             =   1545
         Width           =   1200
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Recurso Asignado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   22
         Left            =   3840
         TabIndex        =   32
         Top             =   2205
         Width           =   1560
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Recurso Preferente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   270
         TabIndex        =   31
         Top             =   2190
         Width           =   1665
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Petici�n"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   2355
      Left            =   75
      TabIndex        =   20
      Top             =   90
      Width           =   10350
      Begin IdPerson.IdPersona IdPersona1 
         Height          =   1335
         Left            =   210
         TabIndex        =   41
         Top             =   960
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   2355
         BackColor       =   12648384
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Datafield       =   "CI21CodPersona"
         MaxLength       =   7
         blnAvisos       =   0   'False
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   1
         Left            =   1650
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Tag             =   "N�mero Direcci�n"
         Top             =   540
         Width           =   1170
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   0
         Left            =   210
         Locked          =   -1  'True
         TabIndex        =   1
         TabStop         =   0   'False
         Tag             =   "N�mero Petici�n"
         Top             =   540
         Width           =   1155
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   1650
         TabIndex        =   23
         Top             =   330
         Width           =   525
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�mero"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   210
         TabIndex        =   22
         Top             =   315
         Width           =   660
      End
   End
End
Attribute VB_Name = "frmDetalleCita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub Command1_Click()
  Me.Hide
End Sub


Private Sub Form_Load()
  Dim intPos As Integer
  
  Call IdPersona1.BeginControl(objApp, objGen)
  IdPersona1.BackColor = objApp.objUserColor.lngReadOnly
  For intPos = 0 To 16
    txtText1(intPos).BackColor = objApp.objUserColor.lngReadOnly
  Next intPos
  IdPersona1.Locked = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Call IdPersona1.EndControl
End Sub

