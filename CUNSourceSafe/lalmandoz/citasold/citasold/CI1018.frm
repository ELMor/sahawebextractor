VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmPersJur�dicas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Personas Jur�dicas"
   ClientHeight    =   7560
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   11910
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "CI1018.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Height          =   420
      Left            =   -15
      TabIndex        =   23
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Direcciones"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4050
      Index           =   1
      Left            =   75
      TabIndex        =   29
      Tag             =   "Mantenimiento de Direcciones de  Personas F�sicas"
      Top             =   3075
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   3540
         Index           =   1
         Left            =   120
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   360
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   6244
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "CI1018.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "tabTab1(26)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "CI1018.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin TabDlg.SSTab tabTab1 
            Height          =   3300
            Index           =   26
            Left            =   135
            TabIndex        =   31
            Top             =   135
            Width           =   10995
            _ExtentX        =   19394
            _ExtentY        =   5821
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabHeight       =   520
            TabCaption(0)   =   "Do&micilio"
            TabPicture(0)   =   "CI1018.frx":0044
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(2)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(3)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(40)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(39)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(38)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(4)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(37)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(36)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(35)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(34)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(33)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "lblLabel1(7)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "lblLabel1(0)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "dtcDateCombo1(3)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "dtcDateCombo1(2)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "cboSSDBCombo1(11)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "cboSSDBCombo1(10)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(33)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(19)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(20)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(21)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(28)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(5)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "chkCheck1(0)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txtText1(27)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "txtText1(22)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).Control(26)=   "txtText1(37)"
            Tab(0).Control(26).Enabled=   0   'False
            Tab(0).Control(27)=   "txtText1(24)"
            Tab(0).Control(27).Enabled=   0   'False
            Tab(0).Control(28)=   "txtText1(23)"
            Tab(0).Control(28).Enabled=   0   'False
            Tab(0).Control(29)=   "txtText1(18)"
            Tab(0).Control(29).Enabled=   0   'False
            Tab(0).ControlCount=   30
            TabCaption(1)   =   "Observa&ciones"
            TabPicture(1)   =   "CI1018.frx":0060
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lblLabel1(41)"
            Tab(1).Control(1)=   "txtText1(25)"
            Tab(1).ControlCount=   2
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10NUMDIRECCI"
               Height          =   330
               Index           =   18
               Left            =   240
               TabIndex        =   4
               Tag             =   "N�mero Direcci�n"
               Top             =   600
               Width           =   885
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10DESPROVI"
               Height          =   330
               Index           =   23
               Left            =   6675
               TabIndex        =   8
               Tag             =   "Provincia"
               Top             =   600
               Width           =   3570
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10DESLOCALID"
               Height          =   330
               Index           =   24
               Left            =   1710
               TabIndex        =   10
               Tag             =   "Localidad"
               Top             =   1290
               Width           =   5550
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10TELEFONO"
               Height          =   330
               Index           =   37
               Left            =   270
               TabIndex        =   14
               Tag             =   "Tel�fono"
               Top             =   2760
               Width           =   2610
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   22
               Left            =   2595
               Locked          =   -1  'True
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Pa�s"
               Top             =   600
               Width           =   2610
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10OBSERVACIO"
               Height          =   1485
               HelpContextID   =   30104
               Index           =   25
               Left            =   -74700
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   35
               Tag             =   "Observaciones"
               Top             =   810
               Width           =   10365
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   27
               Left            =   9255
               TabIndex        =   34
               TabStop         =   0   'False
               Tag             =   "C�digo Persona"
               Top             =   2250
               Visible         =   0   'False
               Width           =   780
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Direcci�n Principal"
               DataField       =   "CI10INDDIRPRINC"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   8265
               TabIndex        =   17
               Tag             =   "Indicador Perfil Gen�rico"
               Top             =   2700
               Width           =   2100
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI07CODPOSTAL"
               Height          =   330
               Index           =   5
               Left            =   240
               TabIndex        =   9
               Tag             =   "C�digo Postal"
               Top             =   1290
               Width           =   1230
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI17CODMUNICIP"
               Height          =   330
               Index           =   28
               Left            =   10230
               TabIndex        =   33
               TabStop         =   0   'False
               Tag             =   "N�mero Direcci�n"
               Top             =   1800
               Visible         =   0   'False
               Width           =   525
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10RESTODIREC"
               Height          =   330
               Index           =   21
               Left            =   6330
               TabIndex        =   13
               Tag             =   "Resto Direcci�n"
               Top             =   2010
               Width           =   2730
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10PORTAL"
               Height          =   330
               Index           =   20
               Left            =   5010
               TabIndex        =   12
               Tag             =   "Portal"
               Top             =   2010
               Width           =   1050
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10CALLE"
               Height          =   330
               Index           =   19
               Left            =   240
               TabIndex        =   11
               Tag             =   "Calle"
               Top             =   2010
               Width           =   4530
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI16CODLOCALID"
               Height          =   330
               Index           =   33
               Left            =   10245
               TabIndex        =   32
               TabStop         =   0   'False
               Tag             =   "N�mero Direcci�n"
               Top             =   2325
               Visible         =   0   'False
               Width           =   525
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI26CODPROVI"
               Height          =   330
               Index           =   10
               Left            =   5460
               TabIndex        =   7
               Tag             =   "C�digo Provincia"
               Top             =   600
               Width           =   885
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Provincia"
               Columns(1).Name =   "Provincia"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1561
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 0"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI19CODPAIS"
               Height          =   330
               Index           =   11
               Left            =   1395
               TabIndex        =   5
               Tag             =   "C�digo Pa�s"
               Top             =   600
               Width           =   885
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3889
               Columns(1).Caption=   "Pa�s"
               Columns(1).Name =   "Pa�s"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1561
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 0"
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "CI10FECINIVALID"
               Height          =   330
               Index           =   2
               Left            =   3240
               TabIndex        =   15
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   2760
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2050/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               BevelColorFace  =   12632256
               ShowCentury     =   -1  'True
               Mask            =   2
               NullDateLabel   =   "__/__/____"
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "CI10FECFINVALID"
               Height          =   330
               Index           =   3
               Left            =   5475
               TabIndex        =   16
               Tag             =   "Fecha Fin Vigencia"
               Top             =   2745
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2050/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               BevelColorFace  =   12632256
               ShowCentury     =   -1  'True
               Mask            =   2
               NullDateLabel   =   "__/__/____"
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   0
               Left            =   240
               TabIndex        =   49
               Top             =   360
               Width           =   600
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Postal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   7
               Left            =   240
               TabIndex        =   48
               Top             =   1050
               Width           =   1185
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Localidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   33
               Left            =   1710
               TabIndex        =   47
               Top             =   1050
               Width           =   840
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�d. Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   34
               Left            =   1395
               TabIndex        =   46
               Top             =   360
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   35
               Left            =   5475
               TabIndex        =   45
               Top             =   360
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   36
               Left            =   6675
               TabIndex        =   44
               Top             =   360
               Width           =   1875
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   37
               Left            =   270
               TabIndex        =   43
               Top             =   2520
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   7800
               TabIndex        =   42
               Top             =   480
               Width           =   75
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   38
               Left            =   2595
               TabIndex        =   41
               Top             =   360
               Width           =   405
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Inicio Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   39
               Left            =   3255
               TabIndex        =   40
               Top             =   2505
               Width           =   1860
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Fin Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   40
               Left            =   5475
               TabIndex        =   39
               Top             =   2490
               Width           =   1650
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   41
               Left            =   -74700
               TabIndex        =   38
               Top             =   570
               Width           =   1275
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Portal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   5010
               TabIndex        =   37
               Top             =   1770
               Width           =   510
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Calle"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   240
               TabIndex        =   36
               Top             =   1770
               Width           =   435
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3240
            Index           =   1
            Left            =   -74910
            TabIndex        =   50
            TabStop         =   0   'False
            Top             =   90
            Width           =   11055
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19500
            _ExtentY        =   5715
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Personas Jur�dicas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2610
      Index           =   0
      Left            =   90
      TabIndex        =   21
      Tag             =   "Mantenimiento de Personas Jur�dicas"
      Top             =   465
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   2145
         HelpContextID   =   90001
         Index           =   0
         Left            =   135
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   375
         Width           =   11490
         _ExtentX        =   20267
         _ExtentY        =   3784
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "CI1018.frx":007C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(10)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(3)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(4)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "CI1018.frx":0098
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI23NUMDIRPRINC"
            Height          =   330
            HelpContextID   =   30101
            Index           =   4
            Left            =   1440
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "N�m.DirPrincipla"
            Top             =   1695
            Visible         =   0   'False
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI23OBSERVACIO"
            Height          =   765
            HelpContextID   =   30104
            Index           =   3
            Left            =   4290
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Tag             =   "Observaciones"
            Top             =   1200
            Width           =   6315
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI23DIREMAIL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   2
            Tag             =   "E-mail"
            Top             =   1215
            Width           =   3690
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI23RAZONSOCIAL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   1
            Left            =   2040
            TabIndex        =   1
            Tag             =   "Raz�n Social"
            Top             =   420
            Width           =   8550
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   240
            TabIndex        =   0
            Tag             =   "C�digo Persona"
            Top             =   420
            Width           =   1425
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1920
            Index           =   0
            Left            =   -74910
            TabIndex        =   20
            Top             =   105
            Width           =   10965
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19341
            _ExtentY        =   3387
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "NumDirPrinc"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   240
            TabIndex        =   28
            Top             =   1770
            Visible         =   0   'False
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   4275
            TabIndex        =   27
            Top             =   960
            Width           =   1275
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Direcci�n E-Mail"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   240
            TabIndex        =   26
            Top             =   975
            Width           =   1410
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Raz�n Social"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   2070
            TabIndex        =   25
            Top             =   195
            Width           =   1140
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   240
            TabIndex        =   24
            Top             =   195
            Width           =   600
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   22
      Top             =   7275
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPersJur�dicas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim WithEvents objSearchCalles As clsCWSearch
Attribute objSearchCalles.VB_VarHelpID = -1
Dim strTipoPoblacion As String
Dim lngPortalImpCom As Long
Dim lngPortalImpFin As Long
Dim lngPortalParCom As Long
Dim lngPortalParFin As Long
Dim blnControlPortal As Boolean
Dim blnCalles As Boolean
Dim blnActDirPrinc As Boolean



Public Sub LocalizarCalles(intCodProvi As Integer, strTipoPob As String)
  Dim objFieldCalle As clsCWFieldSearch

  Set objSearchCalles = New clsCWSearch
  
  With objSearchCalles
    .strTable = "CI0700"
    .strWhere = "WHERE CI26CODPROVI=" & cboSSDBCombo1(10).Value & " AND CI07TIPPOBLA='" & strTipoPob & "'"
    .strOrder = "ORDER BY CI07CALLE"
    
    Set objFieldCalle = .AddField("CI07CODPOSTAL")
    objFieldCalle.strSmallDesc = "C�DIGO_POSTAL"
    Set objFieldCalle = .AddField("CI07DESPOBLA")
    objFieldCalle.blnInGrid = False
    
    Set objFieldCalle = .AddField("CI07CALLE")
    objFieldCalle.strSmallDesc = "CALLE                               "
    
    Set objFieldCalle = .AddField("CI07NUMIMPCOM")
    objFieldCalle.strSmallDesc = "COMIENZO IMPAR"
      
    Set objFieldCalle = .AddField("CI07NUMIMPFIN")
    objFieldCalle.strSmallDesc = "FIN IMPAR"
      
    Set objFieldCalle = .AddField("CI07NUMPARCOM")
    objFieldCalle.strSmallDesc = "COMIENZO PAR"
      
    Set objFieldCalle = .AddField("CI07NUMPARFIN")
    objFieldCalle.strSmallDesc = "FIN PAR"
        
    If .ViewSelect Then
      Call objWinInfo.CtrlSet(txtText1(19), .cllValues("CI07CALLE"))
      Call objWinInfo.CtrlSet(txtText1(24), .cllValues("CI07DESPOBLA"))
      
      Call objWinInfo.CtrlSet(txtText1(5), .cllValues("CI07CODPOSTAL"))
      If .cllValues("CI07CALLE") <> "" Then
        lngPortalImpCom = .cllValues("CI07NUMIMPCOM")
        lngPortalImpFin = .cllValues("CI07NUMIMPFIN")
        lngPortalParCom = .cllValues("CI07NUMPARCOM")
        lngPortalParFin = .cllValues("CI07NUMPARFIN")
        blnControlPortal = True
      End If

    End If
  End With
  Set objSearchCalles = Nothing
 

End Sub

Private Sub cboSSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
'  If (intIndex = 10 Or intIndex = 11) And KeyCode = 46 Then
'    cboSSDBCombo1(intIndex).Text = ""
'  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form padre
  Dim objMasterInfo As New clsCWForm
' Form hijo
  Dim objDetailInfo As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
  
' **********************************
' Fin declaraci�n de variables
' **********************************
  
' Se visualiza el formulario de splash
  Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
' Documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Mantenimiento de Personas Jur�dicas"
    .cwDAT = "7-08-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite mantener las diferentes personas jur�dicas de la CUN"
    
    .cwUPD = "7-08-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
' Declaraci�n de las caracter�sticas del form padre
  With objMasterInfo
    .strName = "PersJur�dica"

' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2300"
    .blnAskPrimary = False
' Reports generados por el form
      Call .objPrinter.Add("CI0010", "Listado 1 Relaci�n de Personas Jur�dicas")
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Personas Jur�dicas")
    Call .FormAddFilterWhere(strKey, "CI23RAZONSOCIAL", "Raz�n Social", cwString)
   
    Call .FormAddFilterOrder(strKey, "CI23RAZONSOCIAL", "Raz�n Social")
 
    
' M�todo de ordenacion del form
    Call .FormAddOrderField("CI21CODPERSONA", cwAscending)
  
  End With
  
' Declaraci�n de las caracter�sticas del form hijo
  With objDetailInfo
    .strName = "Direcci�n"
 ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
 ' Asignaci�n del contenedor(frame)padre del form
    Set .objFatherContainer = fraFrame1(0)
' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(1)
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(1)
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI1000"
    .blnAskPrimary = False
' M�todo de ordenacion del form
    Call .FormAddOrderField("CI10INDDIRPRINC", cwAscending)
    Call .FormAddOrderField("CI10NUMDIRECCI", cwAscending)
' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("CI21CODPERSONA", txtText1(0))
   
  End With

' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
    
' Se a�aden los formularios a la ventana
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
' Se obtiene informaci�n de las caracter�sticas de
' los controles del formulario
    Call .FormCreateInfo(objMasterInfo)
        
' Campos que intervienen en busquedas
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    
' Valores con los que se cargara la DbCombo
    .CtrlGetInfo(cboSSDBCombo1(11)).strSql = "SELECT CI19CODPAIS, CI19DESPAIS FROM " & objEnv.GetValue("Database") & "CI1900 ORDER BY CI19DESPAIS"
    .CtrlGetInfo(cboSSDBCombo1(10)).strSql = "SELECT CI26CODPROVI, CI26DESPROVI FROM " & objEnv.GetValue("Database") & "CI2600 ORDER BY CI26DESPROVI"
  
' Eliminamos campo del grid
    .CtrlGetInfo(txtText1(27)).blnInGrid = False
    .CtrlGetInfo(txtText1(18)).blnInGrid = False
'    .CtrlGetInfo(dtcDateCombo1(11)).blnInGrid = False
'    .CtrlGetInfo(dtcDateCombo1(10)).blnInGrid = False

' Propiedades del campo clave para asignaci�n autom�tica
    .CtrlGetInfo(txtText1(0)).blnValidate = False
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(18)).blnValidate = False
    
    
    .CtrlGetInfo(txtText1(5)).blnForeign = True
    .CtrlGetInfo(txtText1(19)).blnForeign = True
    .CtrlGetInfo(txtText1(24)).blnForeign = True


' Definici�n de controles relacionados entre s�
   'Localidad Direcci�n
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(24)), "CI10DESLOCALID", "SELECT CI17CODMUNICIP, CI16CODLOCALID, CI16DESLOCALID FROM CI1600 WHERE CI16DESLOCALID = UPPER(?)")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(28), "CI17CODMUNICIP")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(33), "CI16CODLOCALID")
    
    'Pais (Direcci�n)
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(11)), "CI19CODPAIS", "SELECT CI19CODPAIS, CI19DESPAIS FROM CI1900 WHERE CI19CODPAIS = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(11)), txtText1(22), "CI19DESPAIS")
           
    'Provincia (Direcci�n)
    'Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(10)), "CI26CODPROVI", "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 WHERE CI26CODPROVI = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(10)), txtText1(23), "CI26DESPROVI")
    '.CtrlGetInfo(txtText1(23)).blnReadOnly = False
   
   'Fecha actual por defecto para Fecha Inicio Vigencia Direcci�n
    .CtrlGetInfo(dtcDateCombo1(2)).vntDefaultValue = Format(objGen.GetDBDateTime, "DD/MM/YYYY")

' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
' Se estabiliza la ventana configurando las propiedades
' de los controles
    Call .WinStabilize
  
  End With
  
' Se oculta el formulario de splash
  Call objApp.SplashOff
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  Dim strTabla As String
  Dim strWhereCond As String
  Dim strOrderCond As String
  Dim blnObtenerCalles As Boolean
  
  If strFormName = "Direcci�n" Then
    
    If cboSSDBCombo1(10).Value = "" Then
       Call objError.SetError(cwCodeMsg, "No se ha seleccionado ninguna Provincia")
       Call objError.Raise
       Exit Sub
    End If
     
   If strCtrl = "txtText1(5)" Then
     Set objSearch = New clsCWSearch
     If Len(Trim(txtText1(5))) > 2 Then
       strTabla = "CI0700"
       strWhereCond = "WHERE CI07CODPOSTAL LIKE '" & Trim(txtText1(5)) & "%'"
       strOrderCond = "ORDER BY CI07CALLE"
     Else
      strTabla = "CI0701"
      strWhereCond = "WHERE CI26CODPROVI =" & cboSSDBCombo1(10).Value
      strOrderCond = "ORDER BY CI07DESPOBLA"
    End If
    With objSearch
      .strTable = strTabla
      .strWhere = strWhereCond
      .strOrder = strOrderCond
      
      Set objField = .AddField("CI07CODPOSTAL")
      objField.strSmallDesc = "C�DIGO POSTAL"
      
      Set objField = .AddField("CI07TIPPOBLA")
      objField.blnInGrid = False
      
      
      Set objField = .AddField("CI26CODPROVI")
      objField.blnInGrid = False
      
      Set objField = .AddField("CI07DESPOBLA")
      objField.strSmallDesc = "POBLACI�N                     "
     
      If strTabla = "CI0700" Then
        
     
        Set objField = .AddField("CI07NUMIMPCOM")
        objField.blnInGrid = False
      
        Set objField = .AddField("CI07NUMIMPFIN")
        objField.blnInGrid = False
      
        Set objField = .AddField("CI07NUMPARCOM")
        objField.blnInGrid = False
      
        Set objField = .AddField("CI07NUMPARFIN")
        objField.blnInGrid = False
        
        Set objField = .AddField("CI07CALLE")
        objField.strSmallDesc = "CALLE                       "
      End If
      If .ViewSelect Then
        If .cllValues("CI07CODPOSTAL") = " " Or Val(.cllValues("CI07CODPOSTAL")) = 0 Then
          If Val(.cllValues("CI07CODPOSTAL")) = 0 Then
            strTipoPoblacion = "0"
          Else
            strTipoPoblacion = .cllValues("CI07TIPPOBLA")
          End If
          blnObtenerCalles = True
        Else
          Call objWinInfo.CtrlSet(txtText1(5), .cllValues("CI07CODPOSTAL"))
          Call objWinInfo.CtrlSet(txtText1(24), .cllValues("CI07DESPOBLA"))
          blnObtenerCalles = False
        End If
        If strTabla = "CI0700" Then
          Call objWinInfo.CtrlSet(txtText1(19), .cllValues("CI07CALLE"))
          If .cllValues("CI07CALLE") <> "" Then
            lngPortalImpCom = .cllValues("CI07NUMIMPCOM")
            lngPortalImpFin = .cllValues("CI07NUMIMPFIN")
            lngPortalParCom = .cllValues("CI07NUMPARCOM")
            lngPortalParFin = .cllValues("CI07NUMPARFIN")
            blnControlPortal = True
          End If
        End If
      End If
    End With
    
    Set objSearch = Nothing
    If blnObtenerCalles Then
      Call LocalizarCalles(cboSSDBCombo1(10).Value, strTipoPoblacion)
    End If
  End If
    
    
 'Lista de Valores por Localidades
   If strCtrl = "txtText1(24)" Then
    Set objSearch = New clsCWSearch
      strTabla = "CI0701"
      strWhereCond = "WHERE CI26CODPROVI =" & cboSSDBCombo1(10).Value & " AND CI07DESPOBLA LIKE '%" & UCase(Trim(txtText1(24))) & "%'"
      strOrderCond = "ORDER BY CI07DESPOBLA"
    With objSearch
      .strTable = strTabla
      .strWhere = strWhereCond
      .strOrder = strOrderCond
      
      Set objField = .AddField("CI07CODPOSTAL")
      objField.strSmallDesc = "C�DIGO POSTAL"
      
      Set objField = .AddField("CI07TIPPOBLA")
      objField.blnInGrid = False
      
      
      Set objField = .AddField("CI26CODPROVI")
      objField.blnInGrid = False
      
      Set objField = .AddField("CI07DESPOBLA")
      objField.strSmallDesc = "POBLACI�N                     "
     
      If .ViewSelect Then
        If .cllValues("CI07CODPOSTAL") = " " Or Val(.cllValues("CI07CODPOSTAL")) = 0 Then
          If Val(.cllValues("CI07CODPOSTAL")) = 0 Then
            strTipoPoblacion = "0"
          Else
            strTipoPoblacion = .cllValues("CI07TIPPOBLA")
          End If
          blnObtenerCalles = True
        Else
          Call objWinInfo.CtrlSet(txtText1(5), .cllValues("CI07CODPOSTAL"))
          Call objWinInfo.CtrlSet(txtText1(24), .cllValues("CI07DESPOBLA"))
          blnObtenerCalles = False
        End If
      End If
      
    End With
    
    Set objSearch = Nothing
    If blnObtenerCalles Then
      Call LocalizarCalles(cboSSDBCombo1(10).Value, strTipoPoblacion)
    End If
  End If

    

 'Lista de Valores de Calles
  If strCtrl = "txtText1(19)" Then
    Set objSearch = New clsCWSearch
     With objSearch
      .strTable = "CI0700"
      .strWhere = "WHERE CI26CODPROVI=" & cboSSDBCombo1(10).Value & " AND CI07TIPPOBLA != '9' AND CI07CALLE LIKE '%" & UCase(Trim(txtText1(19))) & "%'"
      .strOrder = "ORDER BY CI07CALLE"
      
      Set objField = .AddField("CI07CODPOSTAL")
      objField.strSmallDesc = "C�DIGO POSTAL"
      
      Set objField = .AddField("CI07TIPPOBLA")
      objField.blnInGrid = False
      
      
      Set objField = .AddField("CI26CODPROVI")
      objField.blnInGrid = False
      
      Set objField = .AddField("CI07DESPOBLA")
      objField.strSmallDesc = "POBLACI�N           "
      
      Set objField = .AddField("CI07CALLE")
      objField.strSmallDesc = "CALLE                           "
           
      Set objField = .AddField("CI07NUMIMPCOM")
      objField.strSmallDesc = "COMIENZO IMPAR"
      
      Set objField = .AddField("CI07NUMIMPFIN")
      objField.strSmallDesc = "FIN IMPAR"
      
      Set objField = .AddField("CI07NUMPARCOM")
      objField.strSmallDesc = "COMIENZO PAR"
      
      Set objField = .AddField("CI07NUMPARFIN")
      objField.strSmallDesc = "FIN PAR"
        
                                  
      
      If .ViewSelect Then
        Call objWinInfo.CtrlSet(txtText1(5), .cllValues("CI07CODPOSTAL"))
        strTipoPoblacion = .cllValues("CI07TIPPOBLA")
        Call objWinInfo.CtrlSet(txtText1(5), .cllValues("CI07CODPOSTAL"))
        Call objWinInfo.CtrlSet(txtText1(24), .cllValues("CI07DESPOBLA"))
        Call objWinInfo.CtrlSet(txtText1(19), .cllValues("CI07CALLE"))
        lngPortalImpCom = .cllValues("CI07NUMIMPCOM")
        lngPortalImpFin = .cllValues("CI07NUMIMPFIN")
        lngPortalParCom = .cllValues("CI07NUMPARCOM")
        lngPortalParFin = .cllValues("CI07NUMPARFIN")
        blnControlPortal = True
      End If
        
    End With
    
    Set objSearch = Nothing
    If blnObtenerCalles Then
      Call LocalizarCalles(cboSSDBCombo1(10).Value, strTipoPoblacion)
    End If
  End If
 End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'******************************************************************
' Control del form Direcciones
'******************************************************************
  If strFormName = "Direcci�n" Then
    If chkCheck1(0) = 1 Then
      objWinInfo.CtrlGetInfo(chkCheck1(0)).blnReadOnly = True
    Else
      objWinInfo.CtrlGetInfo(chkCheck1(0)).blnReadOnly = False
    
    End If
    objWinInfo.WinPrepareScr
  End If
End Sub

Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)
 
 If strFormName = "Direcci�n" Then
   'si es la primera Direcci�n la hago principal
   If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) = 0 Then
     Call objWinInfo.CtrlSet(chkCheck1(0), 1)
   End If
   
   'Control de N�mero de Portal
 '  If IsNumeric(txtText1(20)) And blnControlPortal Then
     'Si el n�mero de portal es par
 '    If Val(objWinInfo.CtrlGet(txtText1(20))) Mod 2 = 0 Then
       
 '      If Val(objWinInfo.CtrlGet(txtText1(20))) < lngPortalParCom Or _
 '         Val(objWinInfo.CtrlGet(txtText1(20))) > lngPortalParFin Then
       
 '        blnControlPortal = False
 '        Call objError.SetError(cwCodeMsg, "El n�mero de portal no coincide con los rangos del C�digo Postal")
 '        Call objError.Raise
 '        blnCancel = True
 '        Exit Sub
 '      End If
 '    Else
     'Si el n�mero de portal es impar
 '      If Val(objWinInfo.CtrlGet(txtText1(20))) < lngPortalImpCom Or _
 '             Val(objWinInfo.CtrlGet(txtText1(20))) > lngPortalImpFin Then
         
 '        blnControlPortal = False
 '        Call objError.SetError(cwCodeMsg, "El n�mero de portal no coincide con los rangos del C�digo Postal")
 '        Call objError.Raise
 '        blnCancel = True
 '        Exit Sub
 '      End If
 '    End If
 '  End If
 End If

End Sub



Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
  If strFormName = "Direcci�n" And blnRefrescar = True Then
    blnRefrescar = False
    objWinInfo.DataRefresh
  End If

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "PersJur�dica" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(True)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
 
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
' Si se a�ade una nueva direcci�n le paso
' el c�digo de la persona
 If strFormName = "Direcci�n" Then
 ' Le paso el c�digo de persona al nuevo registro de direcci�n
   Call objWinInfo.CtrlSet(txtText1(27), txtText1(0))
 End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
' Control del form Personas Jur�dicas
'  If strFormName = "PersJur�dica" Then
    If blnError = True Then
      Call objApp.rdoEnv.RollbackTrans
    Else
     'Actualizo los cambios
      Call objApp.rdoEnv.CommitTrans
    End If
'  End If
 
'******************************************************************
' Control del form Direcciones
'******************************************************************
  If strFormName = "Direcci�n" Then
    If chkCheck1(0).Value = 1 Then
      chkCheck1(0).Enabled = False
    Else
      chkCheck1(0).Enabled = True
    End If
  End If
 
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
' Variable que guarda la sentencia Sql
  Dim strSql As String
' Variable para el resultset para dar de alta en la tabla personas
  Dim rdoPersonas As rdoResultset
  Dim rdoQueryPersonas As rdoQuery
  Dim lngNuevoCod As Long
  Dim rdoDireccion As rdoResultset
  If strFormName = "PersJur�dica" Then
  ' Si estamos a�adiendo un registro nuevo creamos un nuevo registro en
  ' la tabla persona (CI2100)
  ' Abrimos cursor
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then

      strSql = "select CI21CODPERSONA from CI2100 order by  CI21CODPERSONA desc"
 
    ' Empieza transacci�n
      Call objApp.rdoEnv.BeginTrans
      lngNuevoCod = GetNewCode(strSql)
     
    ' Creamos el cursor
      strSql = "select CI21CODPERSONA,CI33CODTIPPERS from " _
             & " CI2100"
      Set rdoQueryPersonas = objApp.rdoConnect.CreateQuery("", strSql)
      rdoQueryPersonas.MaxRows = 1
     
      Set rdoPersonas = rdoQueryPersonas.OpenResultset(rdOpenKeyset, rdConcurRowVer)

      rdoPersonas.AddNew
      rdoPersonas("CI21CODPERSONA") = lngNuevoCod
      rdoPersonas("CI33CODTIPPERS") = 2 ' NO SE LOS CODIGOS DE TIPO PERSONA (supongo 2 como Pers. Jur�dica)
      rdoPersonas.Update
      rdoPersonas.Close
     'Pasamos el nuevo c�digo al cursor y a la textbox
      Call objWinInfo.CtrlStabilize(txtText1(0), lngNuevoCod)
    
    End If
    
  End If
  
  If strFormName = "Direcci�n" Then
    
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then

      strSql = "select CI10NUMDIRECCI from CI1000 " _
               & "where CI21CODPERSONA=" & Val(txtText1(0)) _
               & " order by  CI10NUMDIRECCI desc"
    ' Empieza transacci�n
      Call objApp.rdoEnv.BeginTrans
     'Obtengo el nuevo c�digo
     
      lngNuevoCod = GetNewCode(strSql)
     'Le paso el nuevo c�digo a la nueva persona f�sica
      Call objWinInfo.CtrlStabilize(txtText1(18), lngNuevoCod)
    End If

   
   
   
   'Actualizamos la direcci�n principal
    If blnActDirPrinc Then
      blnActDirPrinc = False
      strSql = "select CI10NUMDIRECCI, CI10INDDIRPRINC from CI1000 where CI21CODPERSONA=" & Val(txtText1(0)) & " AND CI10INDDIRPRINC=-1"
      Set rdoDireccion = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
      Call objApp.rdoEnv.BeginTrans

      If objGen.GetRowCount(rdoDireccion) > 0 Then
        While Not rdoDireccion.EOF
          rdoDireccion.Edit
          rdoDireccion("CI10INDDIRPRINC") = 0
          rdoDireccion.Update
          rdoDireccion.MoveNext
        Wend
        blnRefrescar = True
      End If
      With objWinInfo.objWinMainForm
        .rdoCursor.Edit
        .rdoCursor("CI23NUMDIRPRINC") = Val(objWinInfo.CtrlGet(txtText1(18)))
        .rdoCursor.Update
      End With
    End If
      
    Call objWinInfo.CtrlSearchLinked(objWinInfo.CtrlGetInfo(txtText1(24)))
       
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 10 Then
    With objWinInfo
      .CtrlGetInfo(txtText1(5)).blnForeign = True
      If Len(.CtrlGet(cboSSDBCombo1(10))) = 1 Then
        Call objWinInfo.CtrlSet(txtText1(5), "0" & .CtrlGet(cboSSDBCombo1(10)))
      Else
        Call objWinInfo.CtrlSet(txtText1(5), .CtrlGet(cboSSDBCombo1(10)))
        
      End If
    End With
    txtText1(5).SetFocus
    Call objWinInfo.CtrlSet(txtText1(24), "")
    Call objWinInfo.CtrlSet(txtText1(28), "")
    Call objWinInfo.CtrlSet(txtText1(33), "")

  End If
  If intIndex = 11 Then
    If cboSSDBCombo1(11).Text = "34" Then
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(10)).blnReadOnly = False
     Call objWinInfo.WinPrepareScr
     Call cboSSDBCombo1(10).SetFocus
     
    Else
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(10)).blnReadOnly = True
      cboSSDBCombo1(10).Text = ""
     Call objWinInfo.WinPrepareScr
    
    End If
 
  End If

End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 10 And objWinInfo.intWinStatus <> cwModeSingleEmpty Then
      If Len(cboSSDBCombo1(10).Text) = 1 Then
        txtText1(5) = "0" & cboSSDBCombo1(10).Text
      Else
        txtText1(5) = cboSSDBCombo1(10).Text
      End If

  End If
  If intIndex = 11 Then
    If cboSSDBCombo1(11).Text = "34" Then
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(10)).blnReadOnly = False
     
    Else
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(10)).blnReadOnly = True
      cboSSDBCombo1(10).Text = ""
    End If
  End If

End Sub
Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 10 Then
    Call objWinInfo.CtrlSet(txtText1(23), cboSSDBCombo1(10).Columns(1).Text)
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus

End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 0 And chkCheck1(0).Value = 1 Then
     blnActDirPrinc = True
  Else
     blnActDirPrinc = False
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
