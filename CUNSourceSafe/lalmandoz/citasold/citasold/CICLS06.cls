VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsSelActuacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public intRow               As Integer
Public lngNumActPlan        As Long
Public lngNumActPedi        As Long
Public lngCodigo            As Long
Public strDescripción       As String
Public lngCita              As Long
Public datFechaCita         As Date
Public strHoraCita          As String
Public datFechaPref         As Date
Public strHoraPref          As String
Public lngRecurso           As Long
Public lngRecursoPref       As Long
Public intDepartamento      As Integer
Public strSituacion         As String
Public intLisEspera         As Integer
Public intRecordatorio      As Integer
Public intNumFaseRecPref    As Integer

Public cllFasesActuacion    As New Collection
Public cllSoluciones        As New Collection

Public Function AddNewFase(Optional intCodFase As Integer = 0) As clsFasesActuacion
  Dim objFase     As New clsFasesActuacion
  
  objFase.intKeyFase = intCodFase
  Call Me.cllFasesActuacion.Add(objFase, Str(intCodFase))
  Set AddNewFase = Me.cllFasesActuacion(Str(intCodFase))
  Set objFase = Nothing
End Function

Public Function GetFase(ByVal intKeyFase As Integer) As clsFasesActuacion

  Dim objFase        As New clsFasesActuacion
  Dim blnExistFase   As Boolean

  blnExistFase = False
  For Each objFase In Me.cllFasesActuacion
    If objFase.intKeyFase = intKeyFase Then
      blnExistFase = True
      Exit For
    End If
  Next
  If blnExistFase Then
    Set GetFase = objFase
  Else
    Set GetFase = Nothing
  End If
  Set objFase = Nothing

End Function

Public Function AddNewSolucion(Optional intCodSol As Long = 0) As clsSolucion
  Dim objSolucion     As New clsSolucion
  
  objSolucion.intNumSolucion = intCodSol
  Call Me.cllSoluciones.Add(objSolucion, Str(intCodSol))
  Set AddNewSolucion = Me.cllSoluciones(Str(intCodSol))
  Set objSolucion = Nothing
End Function

Public Function GetSolucion(ByVal intKeySol As Long) As clsSolucion

  Dim objSolucion    As New clsSolucion
  Dim blnExistFase   As Boolean

  blnExistFase = False
  For Each objSolucion In Me.cllSoluciones
    If objSolucion.intNumSolucion = intKeySol Then
      blnExistFase = True
      Exit For
    End If
  Next
  If blnExistFase Then
    Set GetSolucion = objSolucion
  Else
    Set GetSolucion = Nothing
  End If
  Set objSolucion = Nothing

End Function

