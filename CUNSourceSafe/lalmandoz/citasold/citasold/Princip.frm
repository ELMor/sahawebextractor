VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmMain 
   Caption         =   "Carga Ficheros de Texto"
   ClientHeight    =   4320
   ClientLeft      =   1965
   ClientTop       =   3495
   ClientWidth     =   8385
   Icon            =   "Princip.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4320
   ScaleWidth      =   8385
   Begin VB.FileListBox File1 
      Height          =   2820
      Left            =   2820
      MultiSelect     =   2  'Extended
      TabIndex        =   6
      Top             =   615
      Width           =   2355
   End
   Begin VB.DirListBox Dir1 
      Height          =   2340
      Left            =   300
      TabIndex        =   5
      Top             =   1125
      Width           =   2205
   End
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   315
      TabIndex        =   4
      Top             =   630
      Width           =   2235
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Importar"
      Enabled         =   0   'False
      Height          =   390
      Left            =   5550
      TabIndex        =   3
      Top             =   630
      Width           =   1755
   End
   Begin ComctlLib.ProgressBar PBar1 
      Height          =   285
      Left            =   4590
      TabIndex        =   2
      Top             =   3990
      Visible         =   0   'False
      Width           =   3750
      _ExtentX        =   6615
      _ExtentY        =   503
      _Version        =   327682
      Appearance      =   1
      Max             =   5000
   End
   Begin ComctlLib.StatusBar StatusBar2 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   1
      Top             =   3945
      Width           =   8385
      _ExtentX        =   14790
      _ExtentY        =   609
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Object.Width           =   8008
            MinWidth        =   8008
            TextSave        =   ""
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin ComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   30
      Left            =   0
      TabIndex        =   0
      Top             =   4290
      Width           =   8385
      _ExtentX        =   14790
      _ExtentY        =   53
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog cdbCommonDialog1 
      Left            =   7170
      Top             =   90
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   5820
      TabIndex        =   8
      Top             =   1800
      Width           =   45
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   5715
      TabIndex        =   7
      Top             =   1305
      Width           =   45
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuFicheros 
         Caption         =   "Personal Universitario"
         Index           =   1
      End
      Begin VB.Menu mnuFicheros 
         Caption         =   "Asegurados ACUNSA"
         Index           =   2
      End
      Begin VB.Menu mnuFicheros 
         Caption         =   "Códigos Postales"
         Index           =   3
      End
      Begin VB.Menu mnuFicheros 
         Caption         =   "-"
         Index           =   4
      End
      Begin VB.Menu mnuFicheros 
         Caption         =   "Salir"
         Index           =   5
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strPath As String
Private Sub ComenzarProceso()
  
  Dim objConnect As New clsConnect
  Dim objems1 As New clsProcess
  Dim strFichero As String
  Dim strProceso As String
  Dim intFichero As Integer
  
  frmMain.StatusBar2.Panels(1).Text = "Abriendo conexiones ODBC"
 ' frmMain.Timer1.Enabled = True
  DoEvents
  objConnect.emsOpen
 ' frmMain.Timer1.Enabled = False

  'Se abre la conexion y se crea el cursor de errores
  
  If Not objConnect.blnError Then
  
       
  'Borramos todos los Errores de las tablas de ERRORES
    
  'strDelError = "DELETE FROM EMS..ERRORES"
  
  'objConnect.rdoAppConnectSal.Execute strDelError
  
 ' strDelError = "DELETE FROM EMS..ERRORES2 "
  
  'objConnect.rdoAppConnectSal.Execute strDelError
  
  'Comienza el proceso de Datos
  Label1(0) = "Comienzo=" & Format(Now, "hh:mm:ss")
PBar1.Visible = True
For intFichero = 1 To cllFicherosProcesos.Count
     
         strProceso = cllNombresProcesos(intFichero)
         strFichero = cllFicherosProcesos(intFichero)
     
         frmMain.StatusBar2.Panels(1).Text = "Volcando datos del fichero " & strFichero
         DoEvents
         If stdBuscarFichero(gstrPathTxt, strFichero) Then
            
              Call Reprocesando2(strProceso, objConnect)
                
              'cuando se procesa un archivo se mueve al directorio de procesados
                
             ' FileCopy gstrPathTxt & strFichero, gstrPathProcesados & strFichero
             ' If stdBuscarFichero(gstrPathProcesados, strFichero) Then
              
             '       If strProceso <> "CargaUso" And _
             '          strProceso <> "CargaDestino" And _
             '          strProceso <> "CargaAgencias" And _
             '          strProceso <> "CargaTipoAgente" And _
             ''          strProceso <> "CargaAccesorios" Then
                       
             '           Kill gstrPathTxt & strFichero
                        
             '       End If
             ' End If
         End If
         
         Next intFichero
    Label1(1) = "Fin=" & Format(Now, "hh:mm")
  PBar1.Visible = False
 ' Call ReprocesoErrores(objConnect, objems1)    'Proceso de Depuración de Errores
     
  Call objConnect.emsClose                      'Se cierra la conexion
 
  End If

End Sub

Private Sub Command1_Click()
INTCOUNT = 0

Dim strParametro As String
Dim intPosicion As Integer
Dim i As Integer
' Se recogen los Path de la linea de comandos y se guardan
' en las variables correspondientes

'strParametro = Trim(Command)
strParametro = Trim("i:\ems\datostxt\ i:\ems\datostxt\procesados\")
'intPosicion = InStr(strParametro, " ")

'gstrPathTxt = Mid(strParametro, 1, intPosicion - 1)
gstrPathTxt = Dir1.Path & "\"
'gstrPathProcesados = Mid(strParametro, intPosicion + 1)

' Se llama a la función que carga las colecciones de procesos
' y ficheros

'       PROCESOS DE AUTOS


For i = 0 To (File1.ListCount - 1)
If File1.Selected(i) = True Then
  Select Case UCase(File1.List(i))
     Case "ACUNSA.TXT"
       Call AñadirProceso("CargaPolizaAcunsa", "ACUNSA.TXT")
     Case "CPOSTAL.TXT"
       Call AñadirProceso("CargaCodigosPostales", "CPOSTAL.TXT")
  End Select
End If
Next i
'Call AñadirProceso("CargaCentrosPoliza", "FCSCM17.TXT")
  

Call ComenzarProceso

End Sub

Private Sub Dir1_Change()
  File1.Path = Dir1.Path
End Sub

Private Sub Drive1_Change()
  Dir1.Path = Drive1.Drive
End Sub

Private Sub Timer1_Timer()
  StatusBar2.Panels(1).Text = StatusBar2.Panels(1).Text & "."
  DoEvents
End Sub

Private Sub File1_Click()
  If File1.ListIndex <> -1 Then
   Command1.Enabled = True
  Else
   Command1.Enabled = False
  End If
  
End Sub

Private Sub mnuFicheros_Click(intIndex As Integer)
  cdbCommonDialog1.ShowOpen
  
End Sub
