Attribute VB_Name = "Validaciones"
Option Explicit

Public Function UpdatePolizaACUNSA(ByVal objObjeto As clsProcess, _
                               ByVal blnModo As Boolean)
  With objObjeto
    If blnModo Then
      .objOutput.AddNew
      .objOutput.rdoColumns("CI03NUMPOLIZA") = .objInput.rdoColumns("NUMPOLIZA")
      .objOutput.rdoColumns("CI03NUMASEGURA") = .objInput.rdoColumns("NUMASEG")
    Else
      .objOutput.Edit
    End If
    '************
    'Que Hago con los Null !!!!!!??????* --> & ""
    '********++
    .objOutput.rdoColumns("CI03FECINIPOLI") = ConvertirFecha(.objInput.rdoColumns("FECINIPOL"))
    If Not IsNull(.objInput.rdoColumns("FECFINPOL")) Then
      .objOutput.rdoColumns("CI03FECFINPOLI") = ConvertirFecha(.objInput.rdoColumns("FECFINPOL"))
    End If
    .objOutput.rdoColumns("CI03NOMBRTOMAD") = Nombre(.objInput.rdoColumns("NOMBRETOM"))
    .objOutput.rdoColumns("CI03APELLITOMAD") = Apellidos(.objInput.rdoColumns("NOMBRETOM"))
    .objOutput.rdoColumns("CI03NOMBRASEGU") = Nombre(.objInput.rdoColumns("NOMBREASEG"))
    .objOutput.rdoColumns("CI03APELLIASEGU") = Apellidos(.objInput.rdoColumns("NOMBREASEG"))
    '.objOutput.rdoColumns("CI22NUMHISTORIA") = 0 ' ????????
    If Not IsNull(.objInput.rdoColumns("DNI")) Then
      .objOutput.rdoColumns("CI03DNI") = .objInput.rdoColumns("DNI")
    End If
    .objOutput.rdoColumns("CI30CODSEXO") = ConvertirSexo(.objInput.rdoColumns("SEXO"))
    .objOutput.rdoColumns("CI03FECNACIMIE") = ConvertirFecha(.objInput.rdoColumns("FECNACIMIENTO"))
    .objOutput.rdoColumns("CI03CORRIPAGO") = ConvertirCorPago(.objInput.rdoColumns("CORPAGO"))
    .objOutput.rdoColumns("CI36CODTIPPOLI") = .objInput.rdoColumns("TIPOPOLIZA")
     If Not IsNull(.objInput.rdoColumns("EXCLUSIONES")) Then
       .objOutput.rdoColumns("CI03EXCLUSION") = .objInput.rdoColumns("EXCLUSIONES")
     End If
  ' .objOutput.rdoColumns("CI03OBSERVACIO") = .objInput.rdoColumns("OBSERVACIONES")
    .objOutput.Update
  End With
End Function

Public Function UpdateCodigosPostales(ByVal objObjeto As clsProcess, _
                               ByVal blnModo As Boolean)
  Static cont As Long
  With objObjeto
   If .objInput.rdoColumns("CODPOSTAL") <> "00000" Then
    If blnModo Then
      .objOutput.AddNew
      lngSecuencia = lngSecuencia + 1
      .objOutput("CI07SECUENCIA") = lngSecuencia
     ' .objOutput.rdoColumns("CI03NUMPOLIZA") = .objInput.rdoColumns("NUMPOLIZA")
     ' .objOutput.rdoColumns("CI03NUMASEGURA") = .objInput.rdoColumns("NUMASEG")
    Else
      .objOutput.Edit
    End If
    '************
    'Que Hago con los Null !!!!!!??????* --> & ""
    '********++
    .objOutput.rdoColumns("CI07CODPOSTAL") = .objInput.rdoColumns("CODPOSTAL")
    .objOutput.rdoColumns("CI26CODPROVI") = .objInput.rdoColumns("CODPROV")
    .objOutput.rdoColumns("CI07TIPPOBLA") = .objInput.rdoColumns("CODPOB")
    If .objOutput.rdoColumns("CI07TIPPOBLA") <> "9" Then
      .objOutput.rdoColumns("CI07DESPOBLA") = NombreCiudad(.objInput.rdoColumns("NOMCIUDAD"))
      .objOutput.rdoColumns("CI07CALLE") = Trim(.objInput.rdoColumns("CALLE") & " " & .objInput.rdoColumns("NOMCALLE"))
    Else
      .objOutput.rdoColumns("CI07DESPOBLA") = .objInput.rdoColumns("NOMCALLE")
      .objOutput.rdoColumns("CI07CALLE") = " "
    End If
    .objOutput.rdoColumns("CI07NUMIMPCOM") = .objInput.rdoColumns("COMIMPAR")
    .objOutput.rdoColumns("CI07NUMIMPFIN") = .objInput.rdoColumns("FINIMPAR")
    .objOutput.rdoColumns("CI07NUMPARCOM") = .objInput.rdoColumns("COMPAR")
    .objOutput.rdoColumns("CI07NUMPARFIN") = .objInput.rdoColumns("FINPAR")
    .objOutput.Update
   End If
  End With
  
End Function

