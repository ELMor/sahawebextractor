VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmListaEsperaRecurso 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Mantenimiento de Actuaciones en Lista de Espera"
   ClientHeight    =   7395
   ClientLeft      =   1050
   ClientTop       =   870
   ClientWidth     =   11910
   ControlBox      =   0   'False
   Icon            =   "CI1034.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7395
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones en Lista de Espera"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4005
      Index           =   1
      Left            =   120
      TabIndex        =   9
      Top             =   2880
      Width           =   11670
      Begin VB.CommandButton cmdDetalle 
         Caption         =   "De&talle"
         Height          =   375
         Index           =   0
         Left            =   9660
         TabIndex        =   17
         Top             =   2970
         Width           =   1815
      End
      Begin VB.CommandButton cmdVerFases 
         Caption         =   "Ver Fa&ses/Recursos"
         Height          =   375
         Index           =   0
         Left            =   9660
         TabIndex        =   16
         Top             =   1545
         Width           =   1815
      End
      Begin VB.CommandButton cmdVerSeleccion 
         Caption         =   "Ver Selecci�n "
         Height          =   375
         Index           =   0
         Left            =   9660
         TabIndex        =   5
         Top             =   3465
         Width           =   1815
      End
      Begin VB.CommandButton cmdResolverLista 
         Caption         =   "&Resolver"
         Height          =   375
         Index           =   0
         Left            =   9660
         TabIndex        =   4
         Top             =   1035
         Width           =   1815
      End
      Begin VB.CommandButton cmdAnalizarLista 
         Caption         =   "&Analizar"
         Height          =   375
         Index           =   0
         Left            =   9660
         TabIndex        =   3
         Top             =   510
         Width           =   1815
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3360
         Index           =   1
         Left            =   135
         TabIndex        =   15
         Top             =   495
         Width           =   9180
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         AllowUpdate     =   0   'False
         RowSelectionStyle=   1
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         SelectTypeCol   =   0
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   16192
         _ExtentY        =   5927
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   540
         Index           =   2
         Left            =   9480
         TabIndex        =   18
         Top             =   2160
         Visible         =   0   'False
         Width           =   1830
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ","
         AllowUpdate     =   0   'False
         RowSelectionStyle=   1
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         SelectTypeCol   =   0
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   6
         Columns(0).Width=   7117
         Columns(0).Caption=   "Actuaci�n"
         Columns(0).Name =   "Actuaci�n"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3916
         Columns(1).Caption=   "Paciente"
         Columns(1).Name =   "Paciente"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Fecha"
         Columns(2).Name =   "Fecha"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "NumActPlan"
         Columns(3).Name =   "NumActPlan"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "NumSolicit"
         Columns(4).Name =   "NumSolicit"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "NumCita"
         Columns(5).Name =   "NumCita"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         _ExtentX        =   3228
         _ExtentY        =   952
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Recursos"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2400
      Index           =   0
      Left            =   120
      TabIndex        =   8
      Top             =   480
      Width           =   11670
      Begin TabDlg.SSTab tabTab1 
         Height          =   1515
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   795
         Width           =   11385
         _ExtentX        =   20082
         _ExtentY        =   2672
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "CI1034.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(6)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "CI1034.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AG11CODRECURSO"
            Height          =   330
            Index           =   0
            Left            =   390
            TabIndex        =   1
            Tag             =   "C�digo Recurso|C�digo"
            Top             =   735
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG11DESRECURSO"
            Height          =   330
            Index           =   1
            Left            =   1800
            TabIndex        =   2
            Tag             =   "Descripci�n Recurso"
            Top             =   750
            Width           =   5340
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1020
            Index           =   0
            Left            =   -74895
            TabIndex        =   11
            Top             =   435
            Width           =   11115
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19606
            _ExtentY        =   1799
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   390
            TabIndex        =   13
            Top             =   495
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   1800
            TabIndex        =   12
            Top             =   495
            Width           =   1020
         End
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         Height          =   330
         Index           =   0
         Left            =   7155
         TabIndex        =   0
         Tag             =   "Departamento"
         Top             =   435
         Width           =   4290
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AutoRestore     =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   7620
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasForeColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   7567
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   7125
         TabIndex        =   14
         Top             =   225
         Width           =   1200
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   360
      Left            =   0
      TabIndex        =   7
      Top             =   7035
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   635
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
   Begin VB.Menu mnuEstado 
      Caption         =   "Estados Cita"
      Visible         =   0   'False
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&0. Sin citar"
         Index           =   0
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&1. Confirmar"
         Index           =   1
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&2. Anular"
         Index           =   2
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&3. Recitar"
         Index           =   3
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "&4. Pendiente de Recitar"
         Index           =   4
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "-"
         Index           =   5
      End
      Begin VB.Menu mnuEstadoOpcion 
         Caption         =   "Lista de Espera ?  (S/N)"
         Index           =   6
      End
   End
End
Attribute VB_Name = "frmListaEsperaRecurso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objIlog As Object
Dim objActuacion As New clsSelActuacion
Dim blnFasesIlog As Boolean
Dim objSolucion As New clsSolucion

Private Function CitasChangeStatus(objGrid As SSDBGrid, strNewStatus As String, _
                                  Optional lngIncidencia As Long = -1) As Integer
  Dim intRow As Integer
  Dim vntRowBookmark As Variant
  Dim lngSolicitud As Long
  Dim lngCita As Long
  Dim strSql As String
  Dim rsActuacionCitada As rdoResultset
  Dim intRowsChanged As Integer
   
  intRowsChanged = 0
  If objGrid.SelBookmarks.Count > 0 Then
    For intRow = 0 To objGrid.SelBookmarks.Count - 1
      vntRowBookmark = objGrid.SelBookmarks(intRow)
      lngSolicitud = objGrid.Columns(objGrid.Name & "(" & objGrid.Index & ").Solicitud").CellValue(vntRowBookmark)
      lngCita = objGrid.Columns(objGrid.Name & "(" & objGrid.Index & ").N�mero Cita").CellValue(vntRowBookmark)
      strSql = "Select ROWID, CI01SITCITA, AG05NUMINCIDEN " & _
               "from CI0100 " & _
               "where CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngCita
               
      Set rsActuacionCitada = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, _
                              rdConcurRowVer)
      If rsActuacionCitada.RowCount > 0 Then
         rsActuacionCitada.Edit
         rsActuacionCitada.rdoColumns("CI01SITCITA") = strNewStatus
         If lngIncidencia <> -1 Then
            rsActuacionCitada.rdoColumns("AG05NUMINCIDEN") = lngIncidencia
         End If
         rsActuacionCitada.Update
      End If
      rsActuacionCitada.Close
      intRowsChanged = intRowsChanged + 1
    Next
  End If
  Set rsActuacionCitada = Nothing

  CitasChangeStatus = intRowsChanged

End Function


Private Sub VerFasesSolucion(lngNumActPlanSol As Long)
  Dim objSolucion As clsSolucion
  Dim objFase As clsFasesActuacion
  
  Load frmFasesSolucion
  With frmFasesSolucion
    
     .Caption = "CITAS. Fases de la Actuaci�n" & ": " & grdDBGrid1(2).Columns("Actuaci�n").Value
     .grdDBGrid1(0).RemoveAll
     .grdDBGrid1(1).RemoveAll
     .grdDBGrid1(0).AllowUpdate = False
     .grdDBGrid1(1).AllowUpdate = False
     Set .objActElegida = objActuacion
     Set .objSolucion = objActuacion.GetSolucion(lngNumActPlanSol)
     
     .intSolucion = 1
     .intTotSol = 1
     If .intTotSol > 1 Then
       .cmdCommand1(1).Visible = True
       .cmdCommand1(2).Enabled = False
       .cmdCommand1(2).Visible = True
     Else
       .cmdCommand1(1).Visible = False
       .cmdCommand1(2).Visible = False
     End If

     If .objSolucion.cllFasesSolucion.Count > 0 Then
        For Each objFase In .objSolucion.cllFasesSolucion
           .grdDBGrid1(0).AddItem objFase.intNumFase & "," _
                               & objFase.strDesFase & "," _
                               & Format(objFase.datFechaFase, "DD/MM/YYYY") & "," _
                               & Format(objFase.datFechaFase, "HH:MM") & "," _
                               & objFase.intNumDiasPac & "," _
                               & objFase.intNumHoraPac & "," _
                               & objFase.intNumMinPac
          Next
         
          'Exit For
      End If
        
    
    Call .grdDBGrid1_Click(0)
   
    .Show (vbModal)
    
   End With
   Unload frmFasesSolucion
   Set frmFasesSolucion = Nothing

End Sub

Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
  If intIndex = 0 And KeyCode = 46 Then
    cboSSDBCombo1(intIndex).Text = ""
    objWinInfo.objWinActiveForm.strWhere = ""
    objWinInfo.DataRefresh
  End If

End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)
  If intIndex = 0 And cboSSDBCombo1(0).Text <> "" Then
'******************************************************************
  ' Cambio de departamento
'******************************************************************
  ' Modifico la strWhere para obtener los nuevos registro del
  ' departamento elegido y refresco el cursor
    With objWinInfo
      .objWinActiveForm.strInitialWhere = "AG1100.AD02CODDPTO=" & cboSSDBCombo1(0).Value
      .objWinActiveForm.strWhere = "AG1100.AD02CODDPTO=" & cboSSDBCombo1(0).Value
      .DataRefresh
    End With
  End If

End Sub



Private Sub cmdAnalizarLista_Click(Index As Integer)
  Dim intRow As Integer
  Dim strSql As String
  Dim rdoSolicitud As rdoResultset
  Dim vntBookmark As Variant
  Dim lngIlog As Long
  Dim datFechaAct As Date
  
  Dim objFase As clsFasesActuacion
  Dim objRecurso As clsRecursoFase
  Dim intIndFase As Integer
  Dim intIndRec As Integer
  Dim intIndAct As Integer
  Dim intPos As Integer
  Dim lngIlogFases As Long
  Dim lngIlogRecursos As Long
  
  intIndAct = 0
  Call objGen.RemoveCollection(objSolucion.cllFasesSolucion)
  If grdDBGrid1(1).SelBookmarks.Count > 0 Then
    Me.MousePointer = vbHourglass
    For intRow = 0 To grdDBGrid1(1).SelBookmarks.Count - 1
      vntBookmark = grdDBGrid1(1).SelBookmarks(intRow)
      'paso los campos necesarios a Ilog
      'habra que buscar las preferenecias en la tabla de
      'solicitudes y el recurso preferente(elegido) en la
      'tabla de actuaciones pedidas o planificadas
       strSql = "Select CI31NUMSOLICIT, " _
              & "CI31FECPREFDES,CI31FECPREFHAS,CI31HORPRDESHH," _
              & "CI31HORPRDESMM,CI31HORPRHASHH,CI31HORPRHASMM," _
              & "CI31INDLUNPREF,CI31INDMARPREF,CI31INDMIEPREF," _
              & "CI31INDJUEPREF,CI31INDVIEPREF,CI31INDSABPREF," _
              & "CI31INDDOMPREF FROM CI3100 " _
              & "WHERE CI31NUMSOLICIT=" & Val(grdDBGrid1(1).Columns("Solicitud").CellValue(vntBookmark))
              
             
              
                          
       Set rdoSolicitud = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
       If Not rdoSolicitud.EOF Then
         With objIlog
            Call .Initialize
            If Not IsNull(rdoSolicitud(1)) Then
              Call .SetDiaDesde(CInt(Format(rdoSolicitud(1), "DD")))
              'MsgBox "Dia Desde:" & CInt(Format(dtcDateCombo1(0).Text, "DD"))
              Call .SetMesDesde(CInt(Format(rdoSolicitud(1), "MM")))
              'MsgBox "Mes Desde:" & CInt(Format(dtcDateCombo1(0).Text, "MM"))
              Call .SetAnioDesde(CInt(Format(rdoSolicitud(1), "YYYY")))
              'MsgBox "A�o Desde:" & CInt(Format(dtcDateCombo1(0).Text, "YYYY"))
            End If
   
            If Not IsNull(rdoSolicitud(2)) Then
              Call .SetDiaHasta(CInt(Format(rdoSolicitud(2), "DD")))
              'MsgBox "Dia Hasta:" & CInt(Format(dtcDateCombo1(1).Text, "DD"))
              Call .SetMesHasta(CInt(Format(rdoSolicitud(2), "MM")))
              'MsgBox "Mes Hasta:" & CInt(Format(dtcDateCombo1(1).Text, "MM"))
              Call .SetAnioHasta(CInt(Format(rdoSolicitud(2), "YYYY")))
              'MsgBox "A�o Hasta:" & CInt(Format(dtcDateCombo1(1).Text, "YYYY"))
            End If
   
            If Not IsNull(rdoSolicitud(3)) Then
              Call .SetHoraDesde(CInt(Format(rdoSolicitud(3).Value, "HH")))
             'MsgBox "Hora Desde :" & CInt(Format(dteHoraDesde, "HH"))
              Call .SetMinDesde(CInt(Format(rdoSolicitud(3).Value, "NN")))
             'MsgBox "Min Desde :" & CInt(Format(dteHoraDesde, "NN"))
            End If
            If Not IsNull(rdoSolicitud(4)) Then
              Call .SetHoraHasta(CInt(Format(rdoSolicitud(4), "HH")))
             'MsgBox "Hora Hasta :" & CInt(Format(dteHoraHasta, "HH"))
              Call .SetMinHasta(CInt(Format(rdoSolicitud(4), "NN")))
             'MsgBox "Min Hasta :" & CInt(Format(dteHoraHasta, "NN"))
    
            End If
            For intPos = 0 To 6
              If Not IsNull(rdoSolicitud(intPos + 7)) Then
                 Call .SetDiaSemana(intPos)
              End If
            Next intPos
        End With
       'Se pide que se resuelva:
       'Si: copio los datos al grid 2
       'no:paso a la siguiente actuaci�n
        Call objIlog.SetCodAct(Val(grdDBGrid1(1).Columns("N�mero").CellValue(vntBookmark)))
        'Call objIlog.SetCodAct(56)
        lngIlog = objIlog.Run(1)
        'MsgBox "Run:" & lngIlog
        If lngIlog > 0 Then
          With objIlog
          datFechaAct = 1
          Set objSolucion = objActuacion.AddNewSolucion(Val(grdDBGrid1(1).Columns("N�mero").CellValue(vntBookmark)))
          objSolucion.lngSolNumsolicit = rdoSolicitud("CI31NUMSOLICIT")
          objSolucion.lngSolNumCita = Val(grdDBGrid1(1).Columns("N�mero Cita").CellValue(vntBookmark))
          lngIlogFases = .GetNumPhases(intIndAct)
 '         MsgBox "N� de Fases: " & lngIlogFases
          'objActuacion.intNumFaseRecPref = .GetNumPhaseRecPref(intIndAct)
 '         MsgBox "Num Fase Rec Pref:" & objActuacion.intNumFaseRecPref
          For intIndFase = 0 To lngIlogFases - 1
            Set objFase = objSolucion.AddNewFaseSol(intIndFase)
 '           MsgBox "Fase: IndAct" & intIndAct & " - IndFase" & intIndFase
            objFase.datFechaFase = Format(.GetStartPhaseDia(0, intIndAct, intIndFase) & "/" _
                             & .GetStartPhaseMes(0, intIndAct, intIndFase) & "/" _
                             & .GetStartPhaseAnio(0, intIndAct, intIndFase) & " " _
                             & .GetStartPhaseHora(0, intIndAct, intIndFase) & ":" _
                             & .GetStartPhaseMin(0, intIndAct, intIndFase), "DD/MM/YYYY HH:NN")

 '           MsgBox "Fecha Fase:" & objFase.datFechaFase
              
            datFechaAct = IIf(datFechaAct = 1, objFase.datFechaFase, datFechaAct)
            objFase.intNumFase = .GetNumPhase(intIndAct, intIndFase)
            objFase.strDesFase = .GetDescPhase(intIndAct, intIndFase)
            objFase.intNumDiasPac = .GetDurationPhaseDias(intIndAct, intIndFase)
            objFase.intNumHoraPac = .GetDurationPhaseHoras(intIndAct, intIndFase)
            objFase.intNumMinPac = .GetDurationPhaseMin(intIndAct, intIndFase)
            
 '           MsgBox "Fase creada :" & objFase.intNumDiasPac & "-" & objFase.intNumHoraPac & "-" & objFase.intNumMinPac
            lngIlogRecursos = .GetNumRecursos(intIndAct, intIndFase)
 '           MsgBox "N� de Recursos:" & lngIlogRecursos
            For intIndRec = 0 To lngIlogRecursos - 1
              Set objRecurso = objFase.AddNewRecurso(intIndRec)
'              MsgBox "GetCodRecurso: " & intIndAct & "-" & intIndFase & "-" & intIndRec
              objRecurso.lngCodRecursoFase = .GetCodRecurso(0, intIndAct, intIndFase, intIndRec)
'              MsgBox "Recurso:" & objRecurso.lngCodRecursoFase
'              MsgBox "Dia Recurso: " & .GetStartRecursoDia(0, intIndAct, intIndFase, intIndRec)
'              MsgBox "Mes Recurso: " & .GetStartRecursoMes(0, intIndAct, intIndFase, intIndRec)
'              MsgBox "A�o Recurso: " & .GetStartRecursoAnio(0, intIndAct, intIndFase, intIndRec)
'              MsgBox "Hora Recurso: " & .GetStartRecursoHora(0, intIndAct, intIndFase, intIndRec)
'              MsgBox "Minutos Recurso: " & .GetStartRecursoMin(0, intIndAct, intIndFase, intIndRec)
              objRecurso.datFechaRecurso = CDate(Format(.GetStartRecursoDia(0, intIndAct, intIndFase, intIndRec) & "/" _
                                  & .GetStartRecursoMes(0, intIndAct, intIndFase, intIndRec) & "/" _
                                  & .GetStartRecursoAnio(0, intIndAct, intIndFase, intIndRec) & " " _
                                  & .GetStartRecursoHora(0, intIndAct, intIndFase, intIndRec) & ":" _
                                  & .GetStartRecursoMin(0, intIndAct, intIndFase, intIndRec), "DD/MM/YYYY HH:NN"))

              objRecurso.intNumDiasRec = CInt(.GetDurationRecursoDias(intIndAct, intIndFase, intIndRec))
'              MsgBox "Num Dias:" & objRecurso.intNumDiasRec
              objRecurso.intNumHoraRec = CInt(.GetDurationRecursoHoras(intIndAct, intIndFase, intIndRec))
'              MsgBox "Num Horas:" & objRecurso.intNumHoraRec
              objRecurso.intNumMinRec = CInt(.GetDurationRecursoMin(intIndAct, intIndFase, intIndRec))
'              MsgBox "Num Minutos:" & objRecurso.intNumMinRec
               'MsgBox "Recurso : " & objRecurso.lngCodRecursoFase & "-" & objRecurso.datFechaRecurso
            Next intIndRec
          Next intIndFase
          objSolucion.lngRecPrefSol = objIlog.GetRecursoPreferente(0, intIndAct)

          'A�adir al segundo grid
          grdDBGrid1(2).AddItem grdDBGrid1(1).Columns("Descripci�n").CellValue(vntBookmark) & "," & grdDBGrid1(1).Columns("Persona").CellValue(vntBookmark) & "," & datFechaAct _
                       & "," & objSolucion.intNumSolucion & "," & objSolucion.lngSolNumsolicit _
                       & "," & objSolucion.lngSolNumCita
          blnFasesIlog = True
         End With
      End If
      rdoSolicitud.Close
     End If
    Next intRow
    If blnFasesIlog Then
      grdDBGrid1(2).width = grdDBGrid1(1).width
      grdDBGrid1(2).height = grdDBGrid1(1).height
      grdDBGrid1(2).Left = grdDBGrid1(1).Left
      grdDBGrid1(2).Top = grdDBGrid1(1).Top
      grdDBGrid1(2).Visible = True
      cmdResolverLista(0).Enabled = True
      cmdAnalizarLista(0).Enabled = False
    Else
      Me.MousePointer = vbDefault
      MsgBox "No se puede resolver ninguna actuaci�n", vbInformation, "CITAS. Lista de Espera"
    End If
    Me.MousePointer = vbDefault
  End If
End Sub

Private Sub cmdDetalle_Click(intIndex As Integer)
  If Val(grdDBGrid1(1).Columns("N�mero").Value) > 0 Then
    Call ShowDetail(Val(grdDBGrid1(1).Columns("N�mero").Value), Val(grdDBGrid1(1).Columns("Solicitud").Value), Val(grdDBGrid1(1).Columns("N�mero Cita").Value))
  End If
End Sub


Private Sub cmdResolverLista_Click(Index As Integer)
  Dim intRow            As Integer
  Dim objSolucion       As clsSolucion
  Dim objFase           As clsFasesActuacion
  Dim objRecurso        As clsRecursoFase
  Dim vntBookmark       As Variant
  Dim intIndFase        As Integer
  Dim intIndRec         As Integer

  If grdDBGrid1(2).SelBookmarks.Count > 0 Then
    Me.MousePointer = vbHourglass
    For intRow = 0 To grdDBGrid1(2).SelBookmarks.Count - 1
      With objIlog
     'Confirmo la actuaci�n
      vntBookmark = grdDBGrid1(2).SelBookmarks(intRow)
      Set objSolucion = objActuacion.GetSolucion(CStr(grdDBGrid1(2).Columns("NumActPlan").CellValue(vntBookmark)))
      If Not objSolucion Is Nothing Then
        intIndFase = 0
        For Each objFase In objActuacion.cllFasesActuacion
'         MsgBox "Fase Fecha: " & objFase.datFechaFase
         Call objIlog.SetStartPhaseDia(objSolucion.intNumSolucion, objFase.intNumFase, CInt(Format(objFase.datFechaFase, "DD")))
'         MsgBox "Fase dia: " & CInt(Format(objFase.datFechaFase, "DD"))
         Call objIlog.SetStartPhaseMes(objSolucion.intNumSolucion, objFase.intNumFase, CInt(Format(objFase.datFechaFase, "MM")))
'         MsgBox "Fase mes: " & CInt(Format(objFase.datFechaFase, "MM"))
         Call objIlog.SetStartPhaseAnio(objSolucion.intNumSolucion, objFase.intNumFase, CInt(Format(objFase.datFechaFase, "YYYY")))
'         MsgBox "Fase a�o: " & CInt(Format(objFase.datFechaFase, "YYYY"))
         Call objIlog.SetStartPhaseHora(objSolucion.intNumSolucion, objFase.intNumFase, CInt(Format(objFase.datFechaFase, "HH")))
'         MsgBox "Fase hora: " & CInt(Format(objFase.datFechaFase, "HH"))
         Call objIlog.SetStartPhaseMin(objSolucion.intNumSolucion, objFase.intNumFase, CInt(Format(objFase.datFechaFase, "NN")))
'         MsgBox "Fase minutos: " & CInt(Format(objFase.datFechaFase, "NN"))
         intIndRec = 0
         For Each objRecurso In objFase.cllRecursosFase
         'paso datos recurso a Ilog
'          MsgBox "Recurso Fecha: " & objRecurso.datFechaRecurso
        
           Call objIlog.SetStartRecursoDia(objSolucion.intNumSolucion, objFase.intNumFase, objRecurso.lngCodRecursoFase, CInt(Format(objRecurso.datFechaRecurso, "DD")))
'           MsgBox "Recurso dia: " & CInt(Format(objRecurso.datFechaRecurso, "DD"))
           Call objIlog.SetStartRecursoMes(objSolucion.intNumSolucion, objFase.intNumFase, objRecurso.lngCodRecursoFase, CInt(Format(objRecurso.datFechaRecurso, "MM")))
'           MsgBox "Recurso mes: " & CInt(Format(objRecurso.datFechaRecurso, "MM"))
           Call objIlog.SetStartRecursoAnio(objSolucion.intNumSolucion, objFase.intNumFase, objRecurso.lngCodRecursoFase, CInt(Format(objRecurso.datFechaRecurso, "YYYY")))
'          MsgBox "Recurso a�o: " & CInt(Format(objRecurso.datFechaRecurso, "YYYY"))
           Call objIlog.SetStartRecursoHora(objSolucion.intNumSolucion, objFase.intNumFase, objRecurso.lngCodRecursoFase, CInt(Format(objRecurso.datFechaRecurso, "HH")))
'          MsgBox "Recurso hora: " & CInt(Format(objRecurso.datFechaRecurso, "HH"))
           Call objIlog.SetStartRecursoMin(objSolucion.intNumSolucion, objFase.intNumFase, objRecurso.lngCodRecursoFase, CInt(Format(objRecurso.datFechaRecurso, "NN")))
'           MsgBox "Recurso min: " & CInt(Format(objRecurso.datFechaRecurso, "NN"))
  
'          MsgBox "Fase:" & intIndFase & " Recurso: " & intIndRec & " Fecha: " & objRecurso.datFechaRecurso
         
          intIndRec = intIndRec + 1
        Next
        intIndFase = intIndFase + 1
      Next
      If .Confirm = 1 Then
        If ListaEsperaAdd(objSolucion) Then
          Me.MousePointer = vbDefault
          MsgBox "Actuaci�n  " & grdDBGrid1(2).Columns("Actuaci�n").CellValue(vntBookmark) _
                 & " citada correctamente"
        Else
          Me.MousePointer = vbDefault
          MsgBox "Se ha producido un error al intentar grabar los datos." & vbCrLf _
             & "Transacci�n cancelada"
        End If
      Else
        Me.MousePointer = vbDefault
        MsgBox "No se ha podido confirmar la Actuaci�n " _
              & grdDBGrid1(2).Columns("Actuaci�n").CellValue(vntBookmark)
      End If
     
      End If
      End With
    Next intRow
    
  Else
    Me.MousePointer = vbDefault
    MsgBox "No se ha seleccionado ninguna actuaci�n", vbInformation
    Exit Sub
  End If
  objWinInfo.DataRefresh
  grdDBGrid1(2).Visible = False
  fraFrame1(1).Caption = "Actuaciones en Lista de Espera"

  cmdResolverLista(0).Enabled = False
  cmdAnalizarLista(0).Enabled = True
  blnFasesIlog = False
  Me.MousePointer = vbDefault

End Sub

Private Sub cmdVerFases_Click(Index As Integer)

  Dim blnCancelTrans As Boolean
  Dim intGridIndex As Integer

'  objApp.rdoConnect.BeginTrans
  On Error GoTo Canceltrans
   
  
If blnFasesIlog Then
  intGridIndex = 2
  Call VerFasesSolucion(grdDBGrid1(intGridIndex).Columns("NumActPlan").Value)
Else
  intGridIndex = 1
  vntNumActPlan = grdDBGrid1(intGridIndex).Columns("N�mero").Value
  vntNumActPedi = grdDBGrid1(intGridIndex).Columns("Actuaci�n Pedida").Value
  vntNumSolicit = grdDBGrid1(intGridIndex).Columns("Solicitud").Value
  vntNumCita = grdDBGrid1(intGridIndex).Columns("N�mero Cita").Value
  Me.MousePointer = vbHourglass
  'Load frmFasesPCR
  'frmFasesPCR.Show vbModal
  
  'Unload frmFasesPCR
  'Set frmFasesPCR = Nothing
  Call objSecurity.LaunchProcess(ciFasesRecursos, True)
  Me.MousePointer = vbDefault


  If blnCancelTrans Then
'    objApp.rdoConnect.RollbackTrans
    Call objError.SetError(cwCodeMsg, ciErrRollback)
    Call objError.Raise
  Else
'    objApp.rdoConnect.CommitTrans
   'Call objWinInfo.DataRefresh
    Call Me.Refresh
    DoEvents
  End If
End If

  Exit Sub

Canceltrans:
   blnCancelTrans = True
   Resume Next
End Sub

Private Sub cmdVerSeleccion_Click(Index As Integer)
  Call objGen.RemoveCollection(objSolicitud.cllPeticiones)
  Call objSelRefresh(grdDBGrid1(1))
  Call objShowSelected
  If blnBorrarSel Then
    grdDBGrid1(1).SelBookmarks.RemoveAll
    blnBorrarSel = False
  End If
End Sub

Private Sub Form_Load()
  
  Dim objMasterInfo     As New clsCWForm
  Dim objMultiInfo1     As New clsCWForm
  Dim intGridIndex      As Integer
  Dim strBdUserName     As String
  Dim strBdUserPasswd   As String
  Dim strBdServer       As String
  Dim intMode As Integer
  Call objApp.SplashOn
  
  
  If lngUserCode > 0 Then
    intMode = cwModeSingleEdit
  Else
    intMode = cwModeSingleEmpty
  End If

  Set objWinInfo = New clsCWWin
  Call objWinInfo.WinCreateInfo(intMode, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objWinInfo.objDoc
    .cwPRJ = "Citas"
    .cwMOD = "Mantenimiento de Actuaciones en Lista de Espera (Recurso) "
    .cwDAT = "2-12-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = ""
    
    .cwUPD = "2-12-97 - I�aki - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
  With objMasterInfo
   ' Asigno nombre al frame
   ' .strName = "Recurso"

  ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
  ' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
  ' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
  ' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
  ' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "AG1100"
    If lngUserCode > 0 Then
      .strInitialWhere = "AG11CODRECURSO=" & lngUserCode
      lngUserCode = 0
    End If

  
  ' Asignaci�n del nivel de acceso del usuario al formulario
    .intAllowance = cwAllowReadOnly
  ' M�todo de ordenacion del form
    Call .FormAddOrderField("AG11CODRECURSO", cwAscending)
  ' Creaci�n de los filtros de busqueda
  '  Call .FormCreateFilterWhere(strKey, "Tabla de Calendarios")
  '  Call .FormAddFilterWhere(strKey, "AG02DESCALENDA", "Descripci�n", cwString)
  '  Call .FormAddFilterWhere(strKey, "AG02INDCALPRIN", "Indicador Calendario Principal", cwString)
  '  Call .FormAddFilterWhere(strKey, "AG02FECBAJA", "Fecha Baja", cwString)
    
  ' Creaci�n de los criterios de ordenaci�n
  '  Call .FormAddFilterOrder(strKey,"AG02DESCALENDA", "Descripci�n")
  '  Call .FormAddFilterOrder(strKey,"AG02INDCALPRIN", "Indicador Calendario Principal")
 '   Call .FormAddFilterOrder(strKey,"AG02FECBAJA", "Fecha Baja")
  
  End With
  

  With objMultiInfo1
    '.strName = "Confirmadas"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .strWhere = "CI01INDLISESPE=-1 AND CI01SITCITA='1'"
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .intCursorSize = -1
    
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "PR0431J"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("CI01FECCONCERT", False)
    Call .FormAddRelation("AG11CODRECURSO_ASI", txtText1(0))
        
  End With



  With objWinInfo
    
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
     
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo1, "Persona", "CI21CODPERSONA")
    Call .GridAddColumn(objMultiInfo1, "N�mero", "PR04NUMACTPLAN")
    Call .GridAddColumn(objMultiInfo1, "C�digo", "PR01CODACTUACION")
    Call .GridAddColumn(objMultiInfo1, "Descripci�n", "")
    Call .GridAddColumn(objMultiInfo1, "C�dDepartamento", "AD02CODDPTO")
    Call .GridAddColumn(objMultiInfo1, "Departamento", "")
    Call .GridAddColumn(objMultiInfo1, "Fecha Cita", "CI01FECCONCERT")
    Call .GridAddColumn(objMultiInfo1, "Lista Espera", "CI01INDLISESPE", cwBoolean)
    Call .GridAddColumn(objMultiInfo1, "Fecha Preferencia", "PR03FECPREFEREN")
    Call .GridAddColumn(objMultiInfo1, "Solicitud", "CI31NUMSOLICIT")
    Call .GridAddColumn(objMultiInfo1, "N�mero Cita", "CI01NUMCITA")
    Call .GridAddColumn(objMultiInfo1, "Petici�n", "PR09NUMPETICION")
    Call .GridAddColumn(objMultiInfo1, "Actuaci�n Pedida", "PR03NUMACTPEDI")
    Call .GridAddColumn(objMultiInfo1, "Situaci�n", "CI01SITCITA")
    Call .GridAddColumn(objMultiInfo1, "CodRecAsi", "AG11CODRECURSO_ASI")
    Call .GridAddColumn(objMultiInfo1, "CodPref", "AG11CODRECURSO_PRE")
    Call .GridAddColumn(objMultiInfo1, "Recurso Asignado", "")
    Call .GridAddColumn(objMultiInfo1, "Recurso Preferente", "")
    Call .GridAddColumn(objMultiInfo1, "Recordatorio", "CI01INDRECORDA", cwBoolean)
  
    
    
    Call .FormCreateInfo(objMasterInfo)
    
    
     intGridIndex = 1
       
       'comentarizados para realizar pruebas luego seran invisibles
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Persona").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�digo").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�dDepartamento").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodRecAsi").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodPref").Visible = False

       If intGridIndex = 1 Then
          grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Solicitud").Visible = False
       End If
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero Cita").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Situaci�n").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Petici�n").Visible = False
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Actuaci�n Pedida").Visible = False
       
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Persona").width = TextWidth(String(30, "O"))
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Descripci�n").width = TextWidth(String(30, "O"))
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Departamento").width = TextWidth(String(25, "O"))
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Recurso Asignado").width = TextWidth(String(30, "O"))
       grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Recurso Preferente").width = TextWidth(String(30, "O"))

       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�digo")), "PR01CODACTUACION", "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�digo")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Descripci�n"), "PR01DESCORTA")
    
       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�dDepartamento")), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").C�dDepartamento")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Departamento"), "AD02DESDPTO")
       
       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodRecAsi")), "AG11CODRECURSO_ASI", "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodRecAsi")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Recurso Asignado"), "AG11DESRECURSO")
    
       Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodPref")), "AG11CODRECURSO_PRE", "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO = ?")
       Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").CodPref")), grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Recurso Preferente"), "AG11DESRECURSO")
       
    .CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objEnv.GetValue("Database") & "AD0200 WHERE AD02INDRESPONPROC=-1 ORDER BY AD02DESDPTO"

    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(0)).blnNegotiated = False
    
    Call .WinRegister
    Call .WinStabilize
    blnCommit = False
  End With
  
  cmdVerSeleccion(0).Enabled = False
  cmdAnalizarLista(0).Enabled = False
  cmdResolverLista(0).Enabled = False
  cmdDetalle(0).Enabled = False
  cmdVerFases(0).Enabled = False
  Set objIlog = CreateObject("Servidor.Document")
  With objIlog
     strBdUserName = objSecurity.GetDataBaseUser
     Call .SetUsuarioBD(strBdUserName)
     'MsgBox "Usuario: " & strBdUserName
     strBdUserPasswd = objSecurity.GetDataBasePassword
     Call .SetPasswdBd(strBdUserPasswd)
     'MsgBox "Password: " & strBdUserPasswd
     strBdServer = GetServer(objApp.rdoConnect.Connect)
     Call .SetServerBd(strBdServer)

   End With

 If lngDeptCode > 0 Then
    cboSSDBCombo1(0).Value = lngDeptCode
 End If
 
 Call objApp.SplashOff
End Sub


Private Sub grdDBGrid1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdDBGrid1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub grdDBGrid1_HeadClick(intIndex As Integer, ByVal ColIndex As Integer)
Dim strFieldOrder       As String
  strFieldOrder = objWinInfo.CtrlGetInfo(grdDBGrid1(intIndex).Columns(grdDBGrid1(intIndex).Columns(ColIndex).Name)).objControl.DataField
  If strFieldOrder <> "" Then
    Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
    Call objWinInfo.objWinActiveForm.FormAddOrderField(strFieldOrder, False)
    Call objWinInfo.DataRefresh
  End If
End Sub

Private Sub grdDBGrid1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'  If Button = 2 Then
'    PopupMenu mnuEstado
'  End If
End Sub

Private Sub grdDBGrid1_RowColChange(Index As Integer, ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)

End Sub

Private Sub grdDBGrid1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  Dim strSql As String
  Dim cllPaciente As New Collection
  If intIndex = 1 Then
    grdDBGrid1(1).Columns("Fecha Cita").Text = Format(grdDBGrid1(1).Columns("Fecha Cita").Text, "DD/MM/YYYY - HH:NN")
    grdDBGrid1(1).Columns("Fecha Preferencia").Text = Format(grdDBGrid1(1).Columns("Fecha Preferencia").Text, "DD/MM/YYYY - HH:NN")
    strSql = "SELECT CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI21CODPERSONA=" & Val(grdDBGrid1(1).Columns("Persona").Value)
    Set cllPaciente = GetTableColumn(strSql)
    grdDBGrid1(1).Columns("Persona").Value = cllPaciente("CI22NOMBRE") & " " & cllPaciente("CI22PRIAPEL") & " " & cllPaciente("CI22SEGAPEL")
  End If

End Sub


Private Sub mnuEstadoOpcion_Click(intIndex As Integer)
  Dim intGridIndex As Integer
  intGridIndex = 1
  Call CitasChangeStatus(grdDBGrid1(intGridIndex), LTrim(Str(intIndex)))
  Call objWinInfo.DataRefresh
End Sub

'Private Sub IdPersona1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
'End Sub

'Private Sub IdPersona1_LostFocus(intIndex As Integer)
'  Call objWinInfo.CtrlLostFocus
'End Sub



Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Set objIlog = Nothing
 
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
'Poner el color en titulo del frame exterior de actuaciones
  If strFormName = "fraFrame1(0)" Then
    cmdVerSeleccion(0).Enabled = False
    cmdAnalizarLista(0).Enabled = False
    cmdResolverLista(0).Enabled = False
    cmdDetalle(0).Enabled = False
    cmdVerFases(0).Enabled = False

  Else
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
      cmdVerSeleccion(0).Enabled = True
      cmdAnalizarLista(0).Enabled = True
      cmdResolverLista(0).Enabled = True
      cmdDetalle(0).Enabled = True
      cmdVerFases(0).Enabled = True

    Else
      cmdVerSeleccion(0).Enabled = False
      cmdAnalizarLista(0).Enabled = False
      cmdResolverLista(0).Enabled = False
      cmdDetalle(0).Enabled = False
      cmdVerFases(0).Enabled = False

     End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
 ' If btnButton.Key = "b9" And objWinInfo.objWinActiveForm.strName = "fraFrame1(0)" Then
 '   IdPersona1.SearchPersona
 ' Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
 ' End If
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)

    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
 End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
       Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub




