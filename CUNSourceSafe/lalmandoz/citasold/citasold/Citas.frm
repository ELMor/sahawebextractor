VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "comdlg32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.9#0"; "crystl19.tlb"
Begin VB.Form frmCitas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Citaciones"
   ClientHeight    =   765
   ClientLeft      =   2175
   ClientTop       =   2895
   ClientWidth     =   3255
   ClipControls    =   0   'False
   Icon            =   "Citas.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   765
   ScaleWidth      =   3255
   Begin Crystal.CrystalReport crCrystalReport1 
      Left            =   1680
      Top             =   240
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog1 
      Left            =   870
      Top             =   225
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
      HelpFile        =   "csc.hlp"
   End
   Begin ComctlLib.ImageList imlImageList1 
      Left            =   135
      Top             =   135
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Tratamiento"
         Index           =   10
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Estados Civiles"
         Index           =   20
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "V�nculos Familiares"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Profesi�n"
         Index           =   40
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Centros Cargo"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Colectivos"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Asegurados"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Tipo Econ�mico"
         Index           =   80
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Personas F�sicas"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Tipos de P�liza"
         Index           =   100
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Entidad Colaboradora"
         Index           =   110
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Motivos de Descuento"
         Index           =   120
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Relaci�n Titular"
         Index           =   130
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Conciertos Econ�micos"
         Index           =   140
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Personal de Universidad"
         Index           =   150
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Paises"
         Index           =   160
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Provincias - Municipios - Localidades"
         Index           =   170
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Personas Jur�dicas"
         Index           =   180
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Patolog�as"
         Index           =   210
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Carga Localidades"
         Index           =   230
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Carga Ficheros Texto"
         Index           =   240
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Emitir Recordatorio"
         Index           =   250
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "Detalle Cita"
         Index           =   260
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   800
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   900
      End
   End
   Begin VB.Menu mnuCitaciones 
      Caption         =   "&Citaciones"
      Begin VB.Menu mnuCit 
         Caption         =   "&Peticiones"
         Index           =   10
      End
      Begin VB.Menu mnuCit 
         Caption         =   "&Incidencias Cita (Pacientes)"
         Index           =   20
      End
      Begin VB.Menu mnuCit 
         Caption         =   "Incidencias Citas (Recursos)"
         Index           =   30
      End
      Begin VB.Menu mnuCit 
         Caption         =   "Lista Espera (Pacientes)"
         Index           =   40
      End
      Begin VB.Menu mnuCit 
         Caption         =   "Lista Espera (Recursos)"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmCitas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Dim strEntorno1 As String
  Dim strEntorno2 As String
  
  Call InitApp
 
  With objApp
  
    Call .Register(App, Screen)
    
    Set .frmFormMain = Me
    Set .imlImageList = imlImageList1
    Set .crCrystal = crCrystalReport1
    Set .dlgCommon = dlgCommonDialog1
    
  End With
  
  strEntorno1 = "Entorno de Desarrollo"
  strEntorno2 = "Entorno de Explotaci�n"
  
  With objEnv
    Call .AddEnv(strEntorno1)
    Call .AddValue(strEntorno1, "Database", "")
  
    Call .AddEnv(strEntorno2)
    Call .AddValue(strEntorno2, "Database", "")
  
  End With
  
  objApp.blnUseRegistry = True
  
  'objApp.strPassword = "bgpgoz"
  'objApp.blnAskPassword = False
  
  If Not objApp.CreateInfo Then
    Call ExitApp
  End If
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, intUnloadMode As Integer)
  Call ExitApp
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call objApp.HelpContext
    Case 20
      Call objApp.About
  End Select
End Sub

Private Sub mnuCit_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Load frmPeticion3
      Call frmPeticion3.Show(vbModal)
      Unload frmPeticion3
      Set frmPeticion3 = Nothing
    Case 20
      Load frmCitasPaciente
      Call frmCitasPaciente.Show(vbModal)
      Unload frmCitasPaciente
      Set frmCitasPaciente = Nothing
    Case 30
      Load frmCitasRecursos
      Call frmCitasRecursos.Show(vbModal)
      Unload frmCitasRecursos
      Set frmCitasRecursos = Nothing
    Case 40
      Load frmListaEsperaPaciente
      Call frmListaEsperaPaciente.Show(vbModal)
      Unload frmListaEsperaPaciente
      Set frmListaEsperaPaciente = Nothing
    Case 50
      Load frmListaEsperaRecurso
      Call frmListaEsperaRecurso.Show(vbModal)
      Unload frmListaEsperaRecurso
      Set frmListaEsperaRecurso = Nothing

  End Select
  
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call Tratamientos
    Case 20
      Call Estados_Civiles
    Case 30
      Call V�nculos_Familiares
    Case 40
      Call Profesiones
    Case 50
      Call Centros_Cargo
    Case 60
      Call Colectivos
    Case 70
      Load frmAsegurados
      Call frmAsegurados.Show(vbModal)
      Unload frmAsegurados
      Set frmAsegurados = Nothing
    Case 80
      Load frmTipoEcon
      Call frmTipoEcon.Show(vbModal)
      Unload frmTipoEcon
      Set frmTipoEcon = Nothing
    Case 90
      Load frmPersF�sicas
      Call frmPersF�sicas.Show(vbModal)
      Unload frmPersF�sicas
      Set frmPersF�sicas = Nothing
    Case 100
      Call Tipo_Poliza
    Case 110
      Call Entidad_Colaboradora
    Case 120
      Call Motivo_Descuento
    Case 130
      Call Relaci�n_Titular
    Case 140
      Load frmConcier
      Call frmConcier.Show(vbModal)
      Unload frmConcier
      Set frmConcier = Nothing
    Case 150
      Load frmPersoUni
      Call frmPersoUni.Show(vbModal)
      Unload frmPersoUni
      Set frmPersoUni = Nothing
    Case 160
      Call Paises
    Case 170
      Load frmProvMuniLoc
      Call frmProvMuniLoc.Show(vbModal)
      Unload frmProvMuniLoc
      Set frmProvMuniLoc = Nothing
    Case 180
      Load frmPersJur�dicas
      Call frmPersJur�dicas.Show(vbModal)
      Unload frmPersJur�dicas
      Set frmPersJur�dicas = Nothing
    Case 210
      Load frmPatActu
      Call frmPatActu.Show(vbModal)
      Unload frmPatActu
      Set frmPatActu = Nothing
    Case 230
      Load frmCargaMunicipios
      Call frmCargaMunicipios.Show(vbModal)
      Unload frmCargaMunicipios
      Set frmCargaMunicipios = Nothing
     Case 240
      Load frmCargaTextos
      Call frmCargaTextos.Show(vbModal)
      Unload frmCargaTextos
      Set frmCargaTextos = Nothing
     Case 250
      Load frmRecordatorio
      Call frmRecordatorio.Show(vbModal)
      Unload frmRecordatorio
      Set frmRecordatorio = Nothing
     Case 260
    '  Call ShowDetail(22) ', 13, 2)
  
   Case 900
      Unload Me
  End Select
End Sub


