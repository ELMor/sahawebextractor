VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmDetallePersona 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Detalle Persona F�sica"
   ClientHeight    =   3720
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11100
   ControlBox      =   0   'False
   Icon            =   "DetPers.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3720
   ScaleWidth      =   11100
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Volver"
      Height          =   435
      Left            =   9420
      TabIndex        =   65
      Top             =   3015
      Width           =   1515
   End
   Begin TabDlg.SSTab tabTab1 
      Height          =   2775
      Index           =   2
      Left            =   30
      TabIndex        =   0
      Tag             =   "Fecha Nacimiento"
      Top             =   105
      Width           =   10935
      _ExtentX        =   19288
      _ExtentY        =   4895
      _Version        =   327681
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "Datos &Personales"
      TabPicture(0)   =   "DetPers.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblLabel1(43)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblLabel1(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblLabel1(5)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblLabel1(6)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblLabel1(8)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblLabel1(10)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblLabel1(11)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblLabel1(12)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblLabel1(13)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblLabel1(14)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lblLabel1(15)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "lblLabel1(17)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "lblLabel1(18)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "cboSSDBCombo1(12)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "cboSSDBCombo1(2)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "cboSSDBCombo1(1)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "cboSSDBCombo1(0)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "chkCheck1(1)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "txtText1(0)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "txtText1(1)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "txtText1(2)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "txtText1(3)"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "txtText1(4)"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "txtText1(6)"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "txtText1(7)"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "txtText1(9)"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "txtText1(8)"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "txtText1(26)"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).ControlCount=   28
      TabCaption(1)   =   "Datos &Nacimiento"
      TabPicture(1)   =   "DetPers.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "txtText1(12)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "txtText1(11)"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "txtText1(10)"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "dtcDateCombo1(0)"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "dtcDateCombo1(1)"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "cboSSDBCombo1(3)"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "cboSSDBCombo1(4)"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "cboSSDBCombo1(5)"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "lblLabel1(25)"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "lblLabel1(24)"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "lblLabel1(23)"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).Control(11)=   "lblLabel1(22)"
      Tab(1).Control(11).Enabled=   0   'False
      Tab(1).Control(12)=   "lblLabel1(21)"
      Tab(1).Control(12).Enabled=   0   'False
      Tab(1).Control(13)=   "lblLabel1(20)"
      Tab(1).Control(13).Enabled=   0   'False
      Tab(1).Control(14)=   "lblLabel1(19)"
      Tab(1).Control(14).Enabled=   0   'False
      Tab(1).Control(15)=   "lblLabel1(42)"
      Tab(1).Control(15).Enabled=   0   'False
      Tab(1).ControlCount=   16
      TabCaption(2)   =   "Datos Profe&sionales"
      TabPicture(2)   =   "DetPers.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "txtText1(15)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "txtText1(14)"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "txtText1(13)"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "cboSSDBCombo1(6)"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "lblLabel1(29)"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "lblLabel1(28)"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "lblLabel1(27)"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "lblLabel1(26)"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).ControlCount=   8
      TabCaption(3)   =   "Persona Responsa&ble"
      TabPicture(3)   =   "DetPers.frx":0060
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "txtText1(16)"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "txtText1(29)"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "txtText1(30)"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).Control(3)=   "txtText1(31)"
      Tab(3).Control(3).Enabled=   0   'False
      Tab(3).Control(4)=   "cboSSDBCombo1(7)"
      Tab(3).Control(4).Enabled=   0   'False
      Tab(3).Control(5)=   "lblLabel1(31)"
      Tab(3).Control(5).Enabled=   0   'False
      Tab(3).Control(6)=   "lblLabel1(30)"
      Tab(3).Control(6).Enabled=   0   'False
      Tab(3).Control(7)=   "lblLabel1(16)"
      Tab(3).Control(7).Enabled=   0   'False
      Tab(3).Control(8)=   "lblLabel1(44)"
      Tab(3).Control(8).Enabled=   0   'False
      Tab(3).Control(9)=   "lblLabel1(45)"
      Tab(3).Control(9).Enabled=   0   'False
      Tab(3).ControlCount=   10
      TabCaption(4)   =   "Obser&vaciones"
      TabPicture(4)   =   "DetPers.frx":007C
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "txtText1(17)"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "lblLabel1(32)"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).ControlCount=   2
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22OBSERVAC"
         Height          =   1410
         HelpContextID   =   30104
         Index           =   17
         Left            =   -74520
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   22
         Tag             =   "Observaciones"
         Top             =   720
         Width           =   10170
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   16
         Left            =   -73005
         TabIndex        =   21
         Tag             =   "PersonaResponsable"
         Top             =   750
         Width           =   5970
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22TFNOEMPRESA"
         Height          =   330
         HelpContextID   =   30104
         Index           =   15
         Left            =   -70800
         TabIndex        =   20
         Tag             =   "Tel�fono Empresa"
         Top             =   1680
         Width           =   2610
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22EMPRESA"
         Height          =   330
         HelpContextID   =   30104
         Index           =   14
         Left            =   -74640
         TabIndex        =   19
         Tag             =   "Empresa"
         Top             =   1680
         Width           =   3330
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22PUESTO"
         Height          =   330
         HelpContextID   =   30104
         Index           =   13
         Left            =   -70800
         TabIndex        =   18
         Tag             =   "Puesto"
         Top             =   960
         Width           =   4050
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22DESLOCALID"
         Height          =   330
         HelpContextID   =   30104
         Index           =   12
         Left            =   -73560
         TabIndex        =   17
         Tag             =   "Municipio"
         Top             =   2280
         Width           =   5130
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22DESPROVI"
         Height          =   330
         HelpContextID   =   30104
         Index           =   11
         Left            =   -69120
         TabIndex        =   16
         Tag             =   "Provincia"
         Top             =   1560
         Width           =   3570
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22NUMDIRPRINC"
         Height          =   330
         HelpContextID   =   30101
         Index           =   26
         Left            =   10305
         MaxLength       =   4
         TabIndex        =   15
         Tag             =   "C�digo Empleado|C�digo"
         Top             =   345
         Visible         =   0   'False
         Width           =   330
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22NUMHISTORIA"
         Height          =   330
         HelpContextID   =   30104
         Index           =   8
         Left            =   7020
         TabIndex        =   14
         Tag             =   "N�mero Historia"
         Top             =   2175
         Width           =   1680
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI21CODPERSONA_REA"
         Height          =   330
         HelpContextID   =   30104
         Index           =   9
         Left            =   8940
         MaxLength       =   10
         TabIndex        =   13
         Tag             =   "C�d. Persona Real"
         Top             =   1470
         Width           =   1635
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22TFNOMOVIL"
         Height          =   330
         HelpContextID   =   30104
         Index           =   7
         Left            =   6720
         TabIndex        =   12
         Tag             =   "Tel�fono Movil"
         Top             =   1440
         Width           =   2025
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22NUMSEGSOC"
         Height          =   330
         HelpContextID   =   30104
         Index           =   6
         Left            =   3720
         TabIndex        =   11
         Tag             =   "N� Seguridad Social"
         Top             =   1440
         Width           =   2730
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22DNI"
         Height          =   330
         HelpContextID   =   30104
         Index           =   4
         Left            =   240
         TabIndex        =   10
         Tag             =   "D.N.I|D.N.I "
         Top             =   1440
         Width           =   1725
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22SEGAPEL"
         Height          =   330
         HelpContextID   =   30104
         Index           =   3
         Left            =   7800
         TabIndex        =   9
         Tag             =   "Segundo Apellido|Segundo Apellido"
         Top             =   720
         Width           =   2850
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22PRIAPEL"
         Height          =   330
         HelpContextID   =   30104
         Index           =   2
         Left            =   4680
         TabIndex        =   8
         Tag             =   "Primer Apellido|Primer Apellido"
         Top             =   720
         Width           =   2850
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI22NOMBRE"
         Height          =   330
         HelpContextID   =   30104
         Index           =   1
         Left            =   1850
         TabIndex        =   7
         Tag             =   "Nombre Persona|Nombre Persona"
         Top             =   720
         Width           =   2610
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI21CODPERSONA"
         Height          =   330
         HelpContextID   =   30101
         Index           =   0
         Left            =   270
         TabIndex        =   6
         Tag             =   "C�digo Persona|C�digo"
         Top             =   720
         Width           =   1275
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   10
         Left            =   -73560
         TabIndex        =   5
         Tag             =   "Pa�s"
         Top             =   1560
         Width           =   2850
      End
      Begin VB.CheckBox chkCheck1 
         Caption         =   "Provisional"
         DataField       =   "CI22INDPROVISI"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   9270
         TabIndex        =   4
         Tag             =   "Indicador Perfil Gen�rico"
         Top             =   2190
         Width           =   1290
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   29
         Left            =   -72960
         TabIndex        =   3
         Tag             =   "PersonaResponsable"
         Top             =   2250
         Width           =   5970
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI21CODPERSONA_RSP"
         Height          =   330
         HelpContextID   =   30101
         Index           =   30
         Left            =   -74580
         TabIndex        =   2
         Tag             =   "C�digo Persona|C�digo"
         Top             =   750
         Width           =   1275
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         DataField       =   "CI21CODPERSONA_REC"
         Height          =   330
         HelpContextID   =   30101
         Index           =   31
         Left            =   -74520
         TabIndex        =   1
         Tag             =   "C�digo Persona|C�digo"
         Top             =   2250
         Width           =   1275
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         DataField       =   "CI30CODSEXO"
         Height          =   330
         Index           =   0
         Left            =   2205
         TabIndex        =   23
         Tag             =   "Sexo"
         Top             =   1440
         Width           =   1290
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2355
         Columns(1).Caption=   "Sexo"
         Columns(1).Name =   "Sexo"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2275
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         DataField       =   "CI14CODESTCIVI"
         Height          =   330
         Index           =   1
         Left            =   240
         TabIndex        =   24
         Tag             =   "Estado Civil"
         Top             =   2160
         Width           =   1725
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3043
         Columns(1).Caption=   "Estado Civil"
         Columns(1).Name =   "Estado Civil"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3043
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         DataField       =   "CI28CODRELUDN"
         Height          =   330
         Index           =   2
         Left            =   2205
         TabIndex        =   25
         Tag             =   "Relaci�n UDN"
         Top             =   2160
         Width           =   2880
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4551
         Columns(1).Caption=   "Relaci�n UDN"
         Columns(1).Name =   "Relaci�n UDN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5080
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         DataField       =   "CI22FECNACIM"
         Height          =   330
         Index           =   0
         Left            =   -74640
         TabIndex        =   26
         Tag             =   "Fecha de Alta"
         Top             =   840
         Width           =   1980
         _Version        =   65537
         _ExtentX        =   3492
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         DataField       =   "CI22FECFALLE"
         Height          =   330
         Index           =   1
         Left            =   -72120
         TabIndex        =   27
         Tag             =   "Fecha de Fallecimiento"
         Top             =   840
         Width           =   1980
         _Version        =   65537
         _ExtentX        =   3492
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         DataField       =   "CI19CODPAIS"
         Height          =   330
         Index           =   3
         Left            =   -74640
         TabIndex        =   28
         Tag             =   "C�digo Pa�s"
         Top             =   1560
         Width           =   885
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4022
         Columns(1).Caption=   "Pa�s"
         Columns(1).Name =   "Pa�s"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1561
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         DataField       =   "CI26CODPROVI"
         Height          =   330
         Index           =   4
         Left            =   -70320
         TabIndex        =   29
         Tag             =   "C�digo Provincia"
         Top             =   1560
         Width           =   915
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3625
         Columns(1).Caption=   "Provincia"
         Columns(1).Name =   "Provincia"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1614
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         DataField       =   "CI17CODMUNICIP"
         Height          =   330
         Index           =   5
         Left            =   -74640
         TabIndex        =   30
         Tag             =   "C�digo Municipio"
         Top             =   2280
         Width           =   885
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4313
         Columns(1).Caption=   "Municipio"
         Columns(1).Name =   "Municipio"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1561
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         DataField       =   "CI25CODPROFESI"
         Height          =   330
         Index           =   6
         Left            =   -74640
         TabIndex        =   31
         Tag             =   "Profesi�n"
         Top             =   960
         Width           =   3330
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5874
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         DataField       =   "CI35CODTIPVINC"
         Height          =   330
         Index           =   7
         Left            =   -74580
         TabIndex        =   32
         Tag             =   "Tipo de Vinculaci�n"
         Top             =   1500
         Width           =   3045
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3916
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5371
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
         DataField       =   "CI34CODTRATAMI"
         Height          =   330
         Index           =   12
         Left            =   5250
         TabIndex        =   33
         Tag             =   "Estado Civil"
         Top             =   2160
         Width           =   1590
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3043
         Columns(1).Caption=   "Estado Civil"
         Columns(1).Name =   "Estado Civil"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2805
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Observaciones"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   32
         Left            =   -74520
         TabIndex        =   64
         Top             =   480
         Width           =   1275
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Vinculac�on"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   31
         Left            =   -74565
         TabIndex        =   63
         Top             =   1200
         Width           =   1740
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   " Responsable Familiar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   30
         Left            =   -73050
         TabIndex        =   62
         Top             =   495
         Width           =   1875
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Tel�fono"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   29
         Left            =   -70800
         TabIndex        =   61
         Top             =   1440
         Width           =   765
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Empresa"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   28
         Left            =   -74640
         TabIndex        =   60
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Puesto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   27
         Left            =   -70800
         TabIndex        =   59
         Top             =   720
         Width           =   600
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Profesi�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   26
         Left            =   -74640
         TabIndex        =   58
         Top             =   720
         Width           =   810
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n Municipio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   25
         Left            =   -73560
         TabIndex        =   57
         Top             =   2040
         Width           =   1890
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Municipio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   24
         Left            =   -74640
         TabIndex        =   56
         Top             =   2040
         Width           =   825
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n Provincia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   -69120
         TabIndex        =   55
         Top             =   1320
         Width           =   1875
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Provincia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   22
         Left            =   -70320
         TabIndex        =   54
         Top             =   1320
         Width           =   810
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Pa�s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   21
         Left            =   -74640
         TabIndex        =   53
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de Fallecimiento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   20
         Left            =   -72120
         TabIndex        =   52
         Top             =   600
         Width           =   1980
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de Naciminento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   19
         Left            =   -74640
         TabIndex        =   51
         Top             =   600
         Width           =   1920
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�mero de Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   18
         Left            =   7050
         TabIndex        =   50
         Top             =   1950
         Width           =   1635
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   " C�d. Persona Real"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   17
         Left            =   8880
         TabIndex        =   49
         Top             =   1215
         Width           =   1665
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Relaci�n UDN"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   15
         Left            =   2205
         TabIndex        =   48
         Top             =   1920
         Width           =   1230
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Estado Civil"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   240
         TabIndex        =   47
         Top             =   1920
         Width           =   1020
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Tel�fono Movil"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   6720
         TabIndex        =   46
         Top             =   1200
         Width           =   1275
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�mero Seguridad Social"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   3720
         TabIndex        =   45
         Top             =   1200
         Width           =   2160
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Sexo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   2205
         TabIndex        =   44
         Top             =   1200
         Width           =   435
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "D.N.I"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   240
         TabIndex        =   43
         Top             =   1200
         Width           =   465
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Segundo Apellido"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   7800
         TabIndex        =   42
         Top             =   480
         Width           =   1500
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Primer Apellido"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   4680
         TabIndex        =   41
         Top             =   480
         Width           =   1275
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   1845
         TabIndex        =   40
         Top             =   480
         Width           =   660
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   39
         Top             =   480
         Width           =   600
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Pa�s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   42
         Left            =   -73560
         TabIndex        =   38
         Top             =   1320
         Width           =   405
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Tratamiento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   43
         Left            =   5235
         TabIndex        =   37
         Top             =   1935
         Width           =   1020
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   " Responsable Econ�mico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   -72990
         TabIndex        =   36
         Top             =   1980
         Width           =   2160
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   44
         Left            =   -74550
         TabIndex        =   35
         Top             =   495
         Width           =   600
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   45
         Left            =   -74475
         TabIndex        =   34
         Top             =   1995
         Width           =   600
      End
   End
End
Attribute VB_Name = "frmDetallePersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
  Me.Hide
End Sub
