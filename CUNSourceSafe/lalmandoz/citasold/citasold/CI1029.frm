VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "comct232.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmRecitaCion 
   Caption         =   "CITAS. Recitaci�n Planificada"
   ClientHeight    =   1680
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10665
   ControlBox      =   0   'False
   Icon            =   "CI1029.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   1680
   ScaleWidth      =   10665
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraFrame1 
      Caption         =   "Fecha"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   1
      Left            =   3360
      TabIndex        =   12
      Top             =   225
      Width           =   3135
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         Height          =   315
         Index           =   0
         Left            =   2160
         TabIndex        =   13
         Top             =   600
         Width           =   615
      End
      Begin ComCtl2.UpDown UpDown1 
         Height          =   330
         Index           =   0
         Left            =   2760
         TabIndex        =   14
         Top             =   585
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         OrigLeft        =   2760
         OrigTop         =   600
         OrigRight       =   3000
         OrigBottom      =   925
         Max             =   365
         Min             =   -365
         Enabled         =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         DataField       =   "Pr09FecPeticion"
         Height          =   330
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Tag             =   "Fecha Petici�n"
         Top             =   600
         Width           =   1815
         _Version        =   65537
         _ExtentX        =   3201
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Nueva"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Dias +/-"
         Height          =   255
         Index           =   2
         Left            =   2160
         TabIndex        =   16
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Hora"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   2
      Left            =   6600
      TabIndex        =   5
      Top             =   225
      Width           =   2175
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         Height          =   315
         Index           =   2
         Left            =   1200
         TabIndex        =   6
         Top             =   600
         Width           =   615
      End
      Begin ComCtl2.UpDown UpDown1 
         Height          =   330
         Index           =   2
         Left            =   1800
         TabIndex        =   7
         Top             =   600
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         OrigLeft        =   1920
         OrigTop         =   600
         OrigRight       =   2160
         OrigBottom      =   930
         Max             =   1440
         Min             =   -1440
         Enabled         =   -1  'True
      End
      Begin ComCtl2.UpDown UpDown1 
         Height          =   330
         Index           =   1
         Left            =   720
         TabIndex        =   8
         Top             =   600
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         Increment       =   15
         Max             =   1440
         Min             =   -1440
         Enabled         =   -1  'True
      End
      Begin MSMask.MaskEdBox MaskEdBox1 
         Height          =   330
         Index           =   1
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   582
         _Version        =   327681
         MaxLength       =   5
         Format          =   "hh:mm"
         Mask            =   "##:##"
         PromptChar      =   "_"
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Hora"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Min.+/-"
         Height          =   255
         Index           =   4
         Left            =   1200
         TabIndex        =   10
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Recurso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   225
      Width           =   3135
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   600
         Width           =   2895
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5292
         Columns(0).Caption=   "Recurso Preferente"
         Columns(0).Name =   "Recurso Preferente"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "Codigo"
         Columns(1).Name =   "Codigo"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasForeColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   5106
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Nuevo"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Index           =   0
      Left            =   9000
      TabIndex        =   1
      Top             =   1065
      Width           =   1575
   End
   Begin VB.CommandButton cmdRecitar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   9000
      TabIndex        =   0
      Top             =   585
      Width           =   1575
   End
End
Attribute VB_Name = "frmRecitaCion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
  Dim blnError As Boolean

Private Sub RecitacionPlanificada()
  Dim strSql          As String
  Dim intRow          As Integer
  Dim vntRowBookmark  As Variant
  Dim intGridIndex    As Integer

  Dim rsCitas         As rdoResultset
  Dim rsCitasNew      As rdoResultset

  Dim rsFases         As rdoResultset
  Dim rsFasesNew      As rdoResultset

  Dim rsRecursos      As rdoResultset
  Dim rsRecursosNew   As rdoResultset
 
  Dim lngSolicitud    As Long
  Dim lngNumCita      As Long
  Dim lngNumFase      As Long

  Dim lngSolicitudNew As Long
  Dim lngNumCitaNew   As Long

  Dim lngRecursoNew   As Long
  Dim lngRecurso      As Long

  Dim lngInidencia    As Long

  Dim objColumn       As rdoColumn    'para bucles for each de fases y recursos

  Dim dblMinDif       As Double       'minutos de direrencia para recitar
  Dim dblDayDif       As Double       'minutos de direrencia para recitar
  Dim dblTotMinDif    As Double       'minutos de direrencia para recitar

  Dim cllColumnValues As New Collection

  Dim blnFechaFija As Boolean
  Dim blnHoraFija As Boolean
  Dim strFechaFija As String
  Dim strHoraFija As String
  
  Dim datOldDate As Date
  
  blnFechaFija = False
  blnHoraFija = False
  
  Dim frmSource As Object
  
  'With frmCitasRecursos
  If blnRecMode Then
    Set frmSource = frmCitasRecursos
  Else
    Set frmSource = frmCitasPaciente
  End If
  With frmSource

    intGridIndex = .tabtab1(2).Tab + 1
  
    If .grdDBGrid1(intGridIndex).SelBookmarks.Count > 0 Then
   
     If dtcDateCombo1(0).Date <> "" Then
       blnFechaFija = True
       strFechaFija = Format(dtcDateCombo1(0).Date, "DD/MM/YYYY")
       If MaskEdBox1(1).Text <> "__:__" Then
         blnHoraFija = True
         strHoraFija = Format(objGen.ReplaceStr(MaskEdBox1(1).Text, "_", "0", 0), "hh:nn")
       End If
     End If
     'Nuevo recurso
      If cboDBCombo1(0).Value <> "" Then
        lngRecursoNew = cboDBCombo1(0).Columns(1).Value
      Else
        lngRecursoNew = Empty
      End If
   '
      For intRow = 0 To .grdDBGrid1(intGridIndex).SelBookmarks.Count - 1
       
        vntRowBookmark = .grdDBGrid1(intGridIndex).SelBookmarks(intRow)
        
        lngSolicitud = .grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Solicitud").CellValue(vntRowBookmark)
        lngNumCita = .grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero Cita").CellValue(vntRowBookmark)
       
        lngIncidencia = .grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Incidencia").CellValue(vntRowBookmark)
        If lngIncidencia = Empty Then
          lngRecurso = Empty
        Else
          Set cllColumnValues = GetTableColumn("select AG11CODRECURSO from AG0500 where AG05NUMINCIDEN = " & lngIncidencia)
          lngRecurso = Val(cllColumnValues(1))
        End If
       'ACTUACIONES CITADAS
        strSql = "select * from CI0100 where CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngNumCita
        Set rsCitas = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
        While Not rsCitas.EOF
          rsCitas.Edit
          rsCitas.rdoColumns("CI01SITCITA").Value = ciRecitada
          rsCitas.Update
          
          ' calcular los d�as y horas/minutos que se desplazar� la cita
          If Not blnHoraFija And Not blnFechaFija Then
            dblDayDif = 0
            dblMinDif = 0
            If txtText1(0).Text <> "" Then
              dblDayDif = Val(txtText1(0).Text)
            Else
             If Not IsNull(dtcDateCombo1(0).Date) Then
                dblDayDif = DateDiff("d", rsCitas.rdoColumns("CI01FECCONCERT").Value, dtcDateCombo1(0).Date)
             End If
            End If
          
            If txtText1(2).Text <> "" Then
             dblMinDif = Val(txtText1(2).Text)
            Else
              If MaskEdBox1(1).Text <> "__:__" Then
                Dim datFecha1 As Date
                Dim datFecha2 As Date
                datFecha1 = Date & " " & MaskEdBox1(1).Text
                datFecha2 = Date & " " & Format(rsCitas.rdoColumns("CI01FECCONCERT").Value, "hh:nn:ss")
                dblMinDif = DateDiff("n", datFecha1, datFecha2)
             End If
           End If
           dblTotMinDif = (dblDayDif * 60 * 24) + dblMinDif
          End If
          '
          datOldDate = rsCitas.rdoColumns("CI01FECCONCERT")
          strSql = "select * from CI0100 where CI31NUMSOLICIT is null "
          Set rsCitasNew = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
          rsCitasNew.AddNew
          rsCitasNew.rdoColumns("CI31NUMSOLICIT").Value = rsCitas.rdoColumns("CI31NUMSOLICIT").Value
          rsCitasNew.rdoColumns("PR04NUMACTPLAN").Value = rsCitas.rdoColumns("PR04NUMACTPLAN").Value
          If blnFechaFija Then
            If blnHoraFija Then
               rsCitasNew.rdoColumns("CI01FECCONCERT").Value = CDate(strFechaFija & " " & strHoraFija)
            Else
              strHoraFija = Format(rsCitas.rdoColumns("CI01FECCONCERT"), "HH:NN")
              
              rsCitasNew.rdoColumns("CI01FECCONCERT").Value = CDate(strFechaFija & " " & strHoraFija)
            End If
          Else
            rsCitasNew.rdoColumns("CI01FECCONCERT").Value = DateAdd("n", dblTotMinDif, rsCitas.rdoColumns("CI01FECCONCERT").Value)
          End If
          If CDate(rsCitasNew.rdoColumns("CI01FECCONCERT").Value) < CDate(objGen.GetDBDateTime) Then
               MsgBox "Se ha producido un error al intentar grabar una fecha anterior a la fecha actual del sistema." & vbCrLf & "Verifique la asignaci�n de fechas", vbExclamation, "CITAS.Recitaci�n Planificada"
               blnError = True
               GoTo Fin
          End If

          rsCitasNew.rdoColumns("CI01SITCITA").Value = ciConfirmada
          rsCitasNew.rdoColumns("CI01PERSCONFIR").Value = rsCitas.rdoColumns("CI01PERSCONFIR").Value
          rsCitasNew.rdoColumns("CI01FECCONFIR").Value = objGen.GetDBDateTime
          'rsCitasNew.rdoColumns("AG05NUMINCIDEN").Value = rsCitas.rdoColumns("AG05NUMINCIDEN").Value
          rsCitasNew.rdoColumns("CI01INDRECORDA").Value = 0
          'rsCitasNew.rdoColumns("CI01TIPRECORDA").Value =
          'rsCitasNew.rdoColumns("CI01FECEMIRECO").Value = rsCitas.rdoColumns("CI01FECCONCERT").Value
          rsCitasNew.rdoColumns("CI01NUMENVRECO").Value = rsCitas.rdoColumns("CI01NUMENVRECO").Value
          'rsCitasNew.rdoColumns("CI01FECCOMRECI").Value = rsCitas.rdoColumns("CI01FECCONCERT").Value
          rsCitasNew.rdoColumns("CI01INDLISESPE").Value = rsCitas.rdoColumns("CI01SITCITA").Value
          
          'Comprobar si el Recurso ha cambiado y sustituirlo por el nuevo
          If lngRecursoNew > 0 Then
             rsCitasNew.rdoColumns("AG11CODRECURSO").Value = lngRecursoNew
          Else
             rsCitasNew.rdoColumns("AG11CODRECURSO").Value = rsCitas.rdoColumns("AG11CODRECURSO").Value
          End If
          
          lngNumCitaNew = GetNewCode("Select max(CI01NUMCITA) from CI0100 where CI31NUMSOLICIT = " & lngSolicitud)
          rsCitasNew.rdoColumns("CI01NUMCITA").Value = lngNumCitaNew
          rsCitasNew.Update
          rsCitasNew.Close
          
          'FASES POR ACTUACION CITADA
          strSql = "Select * from CI1500 WHERE CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngNumCita & " order by CI15NUMFASECITA"
          Set rsFases = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
          strSql = "Select * from CI1500 WHERE CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngNumCitaNew & " order by CI15NUMFASECITA"
          Set rsFasesNew = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
          While Not rsFases.EOF
             lngNumFase = rsFases.rdoColumns("CI15NUMFASECITA").Value
             
             rsFasesNew.AddNew
             For Each objColumn In rsFases.rdoColumns
                rsFasesNew.rdoColumns(objColumn.Name).Value = objColumn.Value
             Next
             rsFasesNew.rdoColumns("CI01NUMCITA").Value = lngNumCitaNew
             If blnFechaFija Then
               If blnHoraFija Then
                 dblTotMinDif = DateDiff("n", datOldDate, rsFases("CI15FECCONCPAC"))
                 rsFasesNew("CI15FECCONCPAC") = DateAdd("n", dblTotMinDif, CDate(strFechaFija & " " & strHoraFija))
               Else
                 rsFasesNew("CI15FECCONCPAC") = CDate(strFechaFija & " " & Format(rsFases("CI15FECCONCPAC"), "HH:NN"))
               End If
             Else
               rsFasesNew.rdoColumns("CI15FECCONCPAC").Value = DateAdd("n", dblTotMinDif, rsFases.rdoColumns("CI15FECCONCPAC").Value)
             End If
             If CDate(rsFasesNew.rdoColumns("CI15FECCONCPAC").Value) < CDate(objGen.GetDBDateTime) Then
               MsgBox "Se ha producido un error al intentar grabar una fecha anterior a la fecha actual del sistema." & vbCrLf & "Verifique la asignaci�n de fechas", vbExclamation, "CITAS.Recitaci�n Planificada"
               blnError = True
               GoTo Fin
             End If
             rsFasesNew.Update
             
             'RECURSOS POR FASE
             strSql = "Select * from CI2700 WHERE CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngNumCita & " AND CI15NUMFASECITA = " & lngNumFase & " order by AG11CODRECURSO"
             Set rsRecursos = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
             strSql = "Select * from CI2700 WHERE CI31NUMSOLICIT = " & lngSolicitud & " and CI01NUMCITA = " & lngNumCitaNew & " AND CI15NUMFASECITA = " & lngNumFase & " order by AG11CODRECURSO"
             Set rsRecursosNew = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
             While Not rsRecursos.EOF
                rsRecursosNew.AddNew
                For Each objColumn In rsRecursos.rdoColumns
                   rsRecursosNew.rdoColumns(objColumn.Name).Value = objColumn.Value
                Next
                If blnFechaFija Then
                  If blnHoraFija Then
                    dblTotMinDif = DateDiff("n", datOldDate, rsRecursos("CI27FECOCUPREC"))
                    rsRecursosNew("CI27FECOCUPREC") = DateAdd("n", dblTotMinDif, CDate(strFechaFija & " " & strHoraFija))
                  Else
                    rsRecursosNew("CI27FECOCUPREC") = CDate(strFechaFija & " " & Format(rsRecursos("CI27FECOCUPREC"), "HH:NN"))
                  End If
                Else
                  rsRecursosNew.rdoColumns("CI27FECOCUPREC").Value = DateAdd("n", dblTotMinDif, rsRecursosNew.rdoColumns("CI27FECOCUPREC").Value)
                End If
                If CDate(rsRecursosNew.rdoColumns("CI27FECOCUPREC").Value) < CDate(objGen.GetDBDateTime) Then
                   MsgBox "Se ha producido un error al intentar grabar una fecha anterior a la fecha actual del sistema." & vbCrLf & "Verifique la asignaci�n de fechas", vbExclamation, "CITAS.Recitaci�n Planificada"
                   blnError = True
                  GoTo Fin
                End If
                
                rsRecursosNew.rdoColumns("CI01NUMCITA").Value = lngNumCitaNew
                
                'Comprobar si el Recurso ha cambiado y sustituirlo por el nuevo
                If lngRecursoNew > 0 Then
                   rsRecursosNew.rdoColumns("AG11CODRECURSO").Value = lngRecursoNew
                End If
                '
                rsRecursosNew.Update
                
                rsRecursos.MoveNext
             Wend
             rsRecursos.Close
             rsRecursosNew.Close
             '
             rsFases.MoveNext
          Wend
          rsFases.Close
          rsFasesNew.Close
          '
          
          rsCitas.MoveNext
          
       Wend
       rsCitas.Close
      Next
    End If

  End With

Fin:
  Set rsCitas = Nothing
  Set rsCitasNew = Nothing

  Set rsFases = Nothing
  Set rsFasesNew = Nothing
End Sub

Private Sub cmdCancelar_Click(Index As Integer)
  blnCommit = False
  Me.Hide
End Sub

Private Sub cmdRecitar_Click()

  On Error GoTo ErrorRecit

  blnError = False
  objApp.rdoEnv.BeginTrans

  Call RecitacionPlanificada

  If blnError Then
    objApp.rdoEnv.RollbackTrans
    Call objError.SetError(cwCodeMsg, ciErrRollback)
    Call objError.Raise
  Else
    objApp.rdoEnv.CommitTrans
    blnCommit = True
    Me.Hide
    MsgBox "Recitaci�n realizada correctamente.", vbInformation, "CITAS.Recitaci�n Planificada"
  End If
  Exit Sub

ErrorRecit:
  blnError = True
  Resume Next

End Sub


Private Sub dtcDateCombo1_Change(Index As Integer)
  If Index = 0 Then
    If dtcDateCombo1(0).Date <> "" Then
      txtText1(0) = ""
      txtText1(2) = ""
      txtText1(0).Enabled = False
      txtText1(2).Enabled = False
      UpDown1(0).Enabled = False
      UpDown1(2).Enabled = False
    Else
      If MaskEdBox1(1).Text = "__:__" Then
        txtText1(0).Enabled = True
        txtText1(2).Enabled = True
        UpDown1(0).Enabled = True
        UpDown1(2).Enabled = True
      End If
    End If
  End If
End Sub

Private Sub dtcDateCombo1_CloseUp(Index As Integer)
  If Index = 0 Then
    If dtcDateCombo1(0).Date <> "" Then
      txtText1(0) = ""
      txtText1(2) = ""
      txtText1(0).Enabled = False
      txtText1(2).Enabled = False
      UpDown1(0).Enabled = False
      UpDown1(2).Enabled = False
    Else
      If MaskEdBox1(1).Text = "__:__" Then
        txtText1(0).Enabled = True
        txtText1(2).Enabled = True
        UpDown1(0).Enabled = True
        UpDown1(2).Enabled = True
      End If
    End If
  End If
End Sub

Private Sub Form_Load()
  Dim rsRecursos As rdoResultset
  Dim strSql As String
  Dim intGridIndex As Integer
  Dim intRow As Integer
  Dim vntBookmark As Variant
  Dim lngActPedi As Long
  Dim lngCodAct As Long

  Dim lngIncidencia As Long
  Dim lngRecurso As Long
  Dim intDepartamento As Integer
  Dim cllColumnValues As Collection

  Dim intTipoRecurso As Integer

  Dim intFirstTipoRecurso As Integer
  Dim intFirstDepartamento As Integer

  Dim lngCodActWhile As Long
  Dim blncboRecursoEnabled As Boolean
  Dim rdoParametro As rdoResultset
  Dim frmSource As Object
  
  If blnRecMode Then
    Set frmSource = frmCitasRecursos
  Else
    Set frmSource = frmCitasPaciente
  End If
  With frmSource
    intGridIndex = .tabtab1(2).Tab + 1
   
    If .grdDBGrid1(intGridIndex).SelBookmarks.Count > 0 Then
      blncboRecursoEnabled = True
      
      For intRow = 0 To .grdDBGrid1(intGridIndex).SelBookmarks.Count - 1
         vntRowBookmark = .grdDBGrid1(intGridIndex).SelBookmarks(intRow)
         lngIncidencia = Val(.grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Incidencia").CellValue(vntRowBookmark))
         Set cllColumnValues = GetTableColumn("select AG11CODRECURSO from AG0500 where AG05NUMINCIDEN = " & lngIncidencia)
         If cllColumnValues(1) <> "" Then
            lngRecurso = cllColumnValues(1)
            Set cllColumnValues = GetTableColumn("select AG14CODTIPRECU, AD02CODDPTO from AG1100 where AG11CODRECURSO = " & lngRecurso)
            intTipoRecurso = cllColumnValues(1)
            intDepartamento = cllColumnValues(2)
         End If
         If intRow = 0 Then
            'intFirstTipoRecurso = intTipoRecurso
            'intFirstDepartamento = intDepartamento
            intFirstTipoRecurso = GetTableColumn("select AG14CODTIPRECU, AD02CODDPTO from AG1100 where AG11CODRECURSO = " & .grdDBGrid1(intGridIndex).Columns("CodRecAsi").CellValue(vntRowBookmark))("AG14CODTIPRECU")
            intFirstDepartamento = .grdDBGrid1(intGridIndex).Columns("C�dDepartamento").CellValue(vntRowBookmark)
         
         Else
            If intFirstTipoRecurso <> intTipoRecurso Or intFirstDepartamento = intDepartamento Then
               blncboRecursoEnabled = False
            End If
         End If
         strSql = "Select distinct AG11CODRECURSO FROM CI2700 WHERE CI31NUMSOLICIT=" & Val(.grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").Solicitud").CellValue(vntRowBookmark)) & " AND CI01NUMCITA= " & Val(.grdDBGrid1(intGridIndex).Columns("grddbgrid1(" & intGridIndex & ").N�mero Cita").CellValue(vntRowBookmark))
         Set rsRecursos = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
         If rsRecursos.RowCount > 1 Then
           blncboRecursoEnabled = False
           rsRecursos.Close
         End If
         
      Next
      
      
      If blncboRecursoEnabled Then
                        
         
         strSql = "Select AG11CODRECURSO, AG11DESRECURSO from AG1100 where AG14CODTIPRECU = " & intFirstTipoRecurso & " and AD02CODDPTO = " & intFirstDepartamento
         Set rsRecursos = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
         While Not rsRecursos.EOF
            cboDBCombo1(0).AddItem rsRecursos.rdoColumns("AG11DESRECURSO").Value & vbTab & rsRecursos.rdoColumns("AG11CODRECURSO").Value
            rsRecursos.MoveNext
         Wend
         rsRecursos.Close
      Else
         cboDBCombo1(0).Enabled = False
      End If
    End If
  End With
  strSql = "SELECT AG25NUMSOLUC,AG25NUMDIAESTUD,AG25NUMMAXRAMA,AG25GRANOTIEMPO FROM AG2500"
  Set rdoParametro = objApp.rdoConnect.OpenResultset(strSql)
  If Not rdoParametro.EOF Then
    ciIntervaloCitaMM = rdoParametro("AG25GRANOTIEMPO")
  Else
    ciIntervaloCitaMM = 15
  End If

  UpDown1(1).Increment = ciIntervaloCitaMM
  blnCommit = False
  Set rsRecursos = Nothing
End Sub

Private Sub MaskEdBox1_Change(intIndex As Integer)
  Dim datFechaDesde As Date
  Dim datFechaHasta As Date
  Dim strHora As String
  Dim intUpdownValue As Integer
  
 If MaskEdBox1(1).Text = "__:__" Then
   txtText1(0).Enabled = True
   txtText1(2).Enabled = True
   UpDown1(0).Enabled = True
   UpDown1(2).Enabled = True
 Else
   txtText1(0) = ""
   txtText1(2) = ""
   txtText1(0).Enabled = False
   txtText1(2).Enabled = False
   UpDown1(0).Enabled = False
   UpDown1(2).Enabled = False
 End If

  If InStr(MaskEdBox1(intIndex).Text, "_") = 0 Then
    strHora = MaskEdBox1(intIndex).Text
    If Mid(strHora, 1, 2) * 60 + Mid(strHora, 4, 2) <= UpDown1(intIndex).Max Then
      datFechaDesde = Date & " 00:00"
      datFechaHasta = Date & " " & Format(objGen.ReplaceStr(MaskEdBox1(intIndex).Text, "_", "0", 0), "hh:nn")
      intUpdownValue = DateDiff("n", datFechaDesde, datFechaHasta) - (DateDiff("n", datFechaDesde, datFechaHasta) Mod ciIntervaloCitaMM)
      If intUpdownValue >= UpDown1(intIndex).Min And intUpdownValue <= UpDown1(intIndex).Max And UpDown1(intIndex).Value <> intUpdownValue Then
        UpDown1(intIndex).Value = intUpdownValue
      End If
    End If
  End If
End Sub

Private Sub MaskEdBox1_LostFocus(Index As Integer)
  If MaskEdBox1(Index).Text <> "__:__" Then
     If InStr(MaskEdBox1(Index).Text, "_") <> 0 Then
       MaskEdBox1(Index).Text = objGen.ReplaceStr(MaskEdBox1(Index).Text, "_", "0", 0)
     End If
  End If
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  If Val(txtText1(intIndex).Text) <= UpDown1(intIndex).Max And Val(txtText1(intIndex).Text) >= UpDown1(intIndex).Min Then
    UpDown1(intIndex).Value = Val(txtText1(intIndex).Text)
  End If
End Sub

Private Sub UpDown1_Change(intIndex As Integer)
  Dim datFecha As Date

  If intIndex = 1 Then
   'incremento del spin para hora y minuto preferente
    UpDown1(intIndex).Increment = ciIntervaloCitaMM

    datFecha = Format(Date, "dd-mm-yyyy") & " 00:00"
    datFecha = DateAdd("n", UpDown1(intIndex).Value, Date)
    If MaskEdBox1(intIndex).Text <> objGen.ReplaceStr(Format(datFecha, "HH:NN"), ".", ":", 0) Then
      MaskEdBox1(intIndex).Text = objGen.ReplaceStr(Format(datFecha, "HH:NN"), ".", ":", 0)
    End If
  Else
    txtText1(intIndex).Text = UpDown1(intIndex).Value
  End If
End Sub
