VERSION 5.00
Object = "{02B5E320-7292-11CF-93D5-0020AF99504A}#1.0#0"; "MSCHART.OCX"
Begin VB.Form InformeCal2Form 
   BackColor       =   &H80000009&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   11520
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11325
   LinkTopic       =   "Form1"
   ScaleHeight     =   11520
   ScaleWidth      =   11325
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "&Imprimir Página"
      Height          =   375
      Left            =   9240
      TabIndex        =   9
      Top             =   480
      Width           =   1815
   End
   Begin VB.CommandButton Command3 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   9240
      TabIndex        =   8
      Top             =   1080
      Width           =   1815
   End
   Begin VB.Data Data2 
      Caption         =   "Data2"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   360
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   480
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Data Data1 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   360
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   120
      Visible         =   0   'False
      Width           =   2175
   End
   Begin MSChartLib.MSChart MSChart2 
      Height          =   3015
      Left            =   360
      OleObjectBlob   =   "InformeCal2Form.frx":0000
      TabIndex        =   5
      Top             =   5160
      Width           =   10695
   End
   Begin MSChartLib.MSChart MSChart1 
      Height          =   2535
      Left            =   2880
      OleObjectBlob   =   "InformeCal2Form.frx":2350
      TabIndex        =   3
      Top             =   1920
      Width           =   5655
   End
   Begin MSChartLib.MSChart MSChart3 
      Height          =   2895
      Left            =   360
      OleObjectBlob   =   "InformeCal2Form.frx":46A0
      TabIndex        =   6
      Top             =   8400
      Width           =   10575
   End
   Begin VB.Label OTPendBox 
      BackColor       =   &H80000009&
      Height          =   255
      Left            =   4200
      TabIndex        =   7
      Top             =   1440
      Width           =   855
   End
   Begin VB.Label Label3 
      BackColor       =   &H80000009&
      Caption         =   "Distribución de tiempos de reparación:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   600
      TabIndex        =   4
      Top             =   4680
      Width           =   4215
   End
   Begin VB.Label Label2 
      BackColor       =   &H80000009&
      Caption         =   "Órdenes de Trabajo Pendientes:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   600
      TabIndex        =   2
      Top             =   1440
      Width           =   3495
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      Caption         =   "Informe de Calidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4200
      TabIndex        =   1
      Top             =   0
      Width           =   2775
   End
   Begin VB.Label UniBox 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3240
      TabIndex        =   0
      Top             =   480
      Width           =   4695
   End
End
Attribute VB_Name = "InformeCal2Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command2_Click()
    Command2.Visible = False
    Command3.Visible = False
    InformeCal2Form.PrintForm
    Command2.Visible = True
    Command3.Visible = True
End Sub

Private Sub Command3_Click()
    Unload Me
    
End Sub
