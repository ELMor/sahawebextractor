VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form PedSATForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Petici�n Reparaci�n Externa"
   ClientHeight    =   6315
   ClientLeft      =   150
   ClientTop       =   675
   ClientWidth     =   8520
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6315
   ScaleWidth      =   8520
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8520
      _ExtentX        =   15028
      _ExtentY        =   1535
      ButtonWidth     =   1349
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   11
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "prim"
            Description     =   "PrimBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "SAT"
            Key             =   "SAT"
            Description     =   "SATBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "P.T."
            Key             =   "PT"
            Object.Tag             =   ""
            ImageIndex      =   10
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Imprimir"
            Key             =   "Impr"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "ulti"
            Description     =   "UltBoton"
            Object.Tag             =   ""
            ImageIndex      =   11
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.Data PedSATData 
      Caption         =   "SELECT * FROM PedidoSAT WHERE NPetSAT<>0"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   0
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM PedidoSAT WHERE NPetSAT<>0 ORDER BY NPetSAT"
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data SATData 
      Caption         =   "SAT"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   4800
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT Nombre FROM SAT ORDER BY Nombre"
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data DptoData 
      Caption         =   "Departamento"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   6000
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "Lugar"
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data OperData 
      Caption         =   "Operarios"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   2520
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT CodigoOperario, Nombre FROM Operarios WHERE Activo=TRUE ORDER BY Nombre"
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5175
      Left            =   120
      TabIndex        =   1
      Top             =   1080
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   9128
      _Version        =   327681
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Datos Pedido"
      TabPicture(0)   =   "PedSATForm.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Line2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label2"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label3"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "DptoLabel"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Line1"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label4"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label5"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "OperLabel"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label6"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label7"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "NPetBox"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Label9"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "SATBox"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "DptoBox"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "SSDateCombo1"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "COpeBox"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "ContBox"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Text1"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "NOrdenBox"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "SalidaBox"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).ControlCount=   21
      TabCaption(1)   =   "Datos Aparato"
      TabPicture(1)   =   "PedSATForm.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "AreaTBox"
      Tab(1).Control(1)=   "Label12"
      Tab(1).Control(2)=   "Label11"
      Tab(1).Control(3)=   "Label10"
      Tab(1).Control(4)=   "Label8"
      Tab(1).Control(5)=   "NSerie"
      Tab(1).Control(6)=   "Aparato"
      Tab(1).Control(7)=   "Marca"
      Tab(1).Control(8)=   "AreaT"
      Tab(1).Control(9)=   "NSerieBox"
      Tab(1).Control(10)=   "MarcaBox"
      Tab(1).Control(11)=   "AparatoBox"
      Tab(1).ControlCount=   12
      Begin VB.CheckBox SalidaBox 
         Alignment       =   1  'Right Justify
         Caption         =   "Salida Aparato:"
         DataField       =   "Salida"
         DataSource      =   "PedSATData"
         Height          =   255
         Left            =   2400
         TabIndex        =   12
         Top             =   4800
         Width           =   1575
      End
      Begin VB.TextBox NOrdenBox 
         DataField       =   "NOrden"
         DataSource      =   "PedSATData"
         Height          =   285
         Left            =   6000
         TabIndex        =   11
         Top             =   480
         Width           =   1215
      End
      Begin VB.TextBox Text1 
         DataField       =   "Descripcion"
         DataSource      =   "PedSATData"
         Height          =   1935
         Left            =   1920
         MultiLine       =   -1  'True
         TabIndex        =   10
         Top             =   2640
         Width           =   5415
      End
      Begin VB.TextBox ContBox 
         DataField       =   "ContactoSAT"
         DataSource      =   "PedSATData"
         Height          =   285
         Left            =   2640
         TabIndex        =   9
         Top             =   1200
         Width           =   2655
      End
      Begin VB.TextBox AparatoBox 
         DataField       =   "Aparato"
         DataSource      =   "PedSATData"
         Height          =   285
         Left            =   -72120
         TabIndex        =   8
         Top             =   2040
         Width           =   4815
      End
      Begin VB.TextBox MarcaBox 
         DataField       =   "Marca"
         DataSource      =   "PedSATData"
         Height          =   285
         Left            =   -72120
         TabIndex        =   7
         Top             =   2520
         Width           =   4815
      End
      Begin VB.TextBox NSerieBox 
         DataField       =   "NSerie"
         DataSource      =   "PedSATData"
         Height          =   285
         Left            =   -72120
         TabIndex        =   6
         Top             =   3000
         Width           =   4815
      End
      Begin VB.TextBox AreaT 
         Height          =   285
         Left            =   -73800
         TabIndex        =   5
         Top             =   4080
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox Marca 
         Height          =   285
         Left            =   -72720
         TabIndex        =   4
         Top             =   4080
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.TextBox Aparato 
         Height          =   285
         Left            =   -71520
         TabIndex        =   3
         Top             =   4080
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox NSerie 
         Height          =   285
         Left            =   -70320
         TabIndex        =   2
         Top             =   4080
         Visible         =   0   'False
         Width           =   495
      End
      Begin SSDataWidgets_B.SSDBCombo COpeBox 
         Bindings        =   "PedSATForm.frx":0038
         DataField       =   "CodigoOperario"
         DataSource      =   "PedSATData"
         Height          =   255
         Left            =   2640
         TabIndex        =   13
         Top             =   2280
         Width           =   1335
         DataFieldList   =   "CodigoOperario"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Columns(0).Width=   3200
         _ExtentX        =   2355
         _ExtentY        =   450
         _StockProps     =   93
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "CodigoOperario"
      End
      Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
         DataField       =   "FechaEnvio"
         DataSource      =   "PedSATData"
         Height          =   255
         Left            =   2640
         TabIndex        =   14
         Top             =   1920
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo DptoBox 
         Bindings        =   "PedSATForm.frx":004B
         DataField       =   "CodigoDpto"
         DataSource      =   "PedSATData"
         Height          =   255
         Left            =   2640
         TabIndex        =   15
         Top             =   1560
         Width           =   1335
         DataFieldList   =   "CodigoLugar"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Columns(0).Width=   3200
         _ExtentX        =   2355
         _ExtentY        =   450
         _StockProps     =   93
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "CodigoLugar"
      End
      Begin SSDataWidgets_B.SSDBCombo SATBox 
         Bindings        =   "PedSATForm.frx":005E
         DataField       =   "SAT"
         DataSource      =   "PedSATData"
         Height          =   255
         Left            =   2640
         TabIndex        =   16
         Top             =   840
         Width           =   2655
         DataFieldList   =   "Nombre"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Columns(0).Width=   3200
         _ExtentX        =   4683
         _ExtentY        =   450
         _StockProps     =   93
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Nombre"
      End
      Begin VB.Label Label9 
         Caption         =   "N�Orden:"
         Height          =   255
         Left            =   5160
         TabIndex        =   32
         Top             =   480
         Width           =   735
      End
      Begin VB.Label NPetBox 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         DataField       =   "NPetSAT"
         DataSource      =   "PedSATData"
         Height          =   255
         Left            =   2640
         TabIndex        =   31
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label7 
         Caption         =   "N� Pedido SAT:"
         Height          =   255
         Left            =   480
         TabIndex        =   30
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Label6 
         Caption         =   "Descripci�n:"
         Height          =   255
         Left            =   840
         TabIndex        =   29
         Top             =   2640
         Width           =   975
      End
      Begin VB.Label OperLabel 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Nombre"
         DataSource      =   "OperData"
         Height          =   255
         Left            =   4320
         TabIndex        =   28
         Top             =   2280
         Width           =   2655
      End
      Begin VB.Label Label5 
         Caption         =   "C�digo Operario:"
         Height          =   255
         Left            =   480
         TabIndex        =   27
         Top             =   2280
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Fecha de Env�o"
         Height          =   255
         Left            =   480
         TabIndex        =   26
         Top             =   1920
         Width           =   2055
      End
      Begin VB.Line Line1 
         X1              =   3960
         X2              =   4320
         Y1              =   1680
         Y2              =   1680
      End
      Begin VB.Label DptoLabel 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Localizacion"
         DataSource      =   "DptoData"
         Height          =   255
         Left            =   4320
         TabIndex        =   25
         Top             =   1560
         Width           =   2655
      End
      Begin VB.Label Label3 
         Caption         =   "Departamento"
         Height          =   255
         Left            =   480
         TabIndex        =   24
         Top             =   1560
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "Persona Contacto SAT"
         Height          =   255
         Left            =   480
         TabIndex        =   23
         Top             =   1200
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "Servicio Asistencia T�cnica"
         Height          =   255
         Left            =   480
         TabIndex        =   22
         Top             =   840
         Width           =   2055
      End
      Begin VB.Label Label8 
         Caption         =   "Aparato:"
         Height          =   255
         Left            =   -73440
         TabIndex        =   21
         Top             =   2040
         Width           =   735
      End
      Begin VB.Label Label10 
         Caption         =   "Marca-Modelo:"
         Height          =   255
         Left            =   -73440
         TabIndex        =   20
         Top             =   2520
         Width           =   1095
      End
      Begin VB.Label Label11 
         Caption         =   "N�mero de Serie:"
         Height          =   255
         Left            =   -73440
         TabIndex        =   19
         Top             =   3000
         Width           =   1335
      End
      Begin VB.Label Label12 
         Caption         =   "Area de Trabajo:"
         Height          =   255
         Left            =   -74040
         TabIndex        =   18
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label AreaTBox 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   -72600
         TabIndex        =   17
         Top             =   840
         Width           =   5415
      End
      Begin VB.Line Line2 
         X1              =   3960
         X2              =   4320
         Y1              =   2400
         Y2              =   2400
      End
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   11
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedSATForm.frx":0070
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedSATForm.frx":038A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedSATForm.frx":06A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedSATForm.frx":09BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedSATForm.frx":0CD8
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedSATForm.frx":0FF2
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedSATForm.frx":130C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedSATForm.frx":1626
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedSATForm.frx":1940
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedSATForm.frx":1C5A
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PedSATForm.frx":1F74
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu prg 
      Caption         =   "&Programa"
      Begin VB.Menu exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu reg 
      Caption         =   "&Registros"
      Begin VB.Menu ira 
         Caption         =   "Ir a "
         Shortcut        =   {F1}
      End
      Begin VB.Menu first 
         Caption         =   "Primero"
      End
      Begin VB.Menu ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu sigui 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu last 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu add 
         Caption         =   "A�adir"
      End
      Begin VB.Menu eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu confi 
      Caption         =   "&Configuraciones"
      Begin VB.Menu sate 
         Caption         =   "S.A.T."
      End
   End
   Begin VB.Menu listdfg 
      Caption         =   "&Listados"
      Begin VB.Menu lista 
         Caption         =   "Listado"
      End
   End
   Begin VB.Menu imp1 
      Caption         =   "&Impresi�n"
      Begin VB.Menu imp2 
         Caption         =   "Imprimir Pedido WORD"
      End
   End
End
Attribute VB_Name = "PedSATForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub add_Click()
    Dim nada As Integer
    nada = OtroPetSAT()
End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()
End Sub

Private Sub COpeBox_Change()
On Error GoTo msgError
    If COpeBox.Text <> "" Then
        OperData.Recordset.FindFirst ("CodigoOperario=" & COpeBox.Text)
    Else
        COpeBox.Text = "0"
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub COpeBox_CloseUp()
On Error GoTo msgError
    OperData.Recordset.FindFirst ("CodigoOperario=" & COpeBox.Text)
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub DptoBox_Change()
On Error GoTo msgError
    If DptoBox.Text <> "" Then
        DptoData.Recordset.FindFirst ("CodigoLugar=" & DptoBox.Text)
    Else
        DptoBox.Text = "0"
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub DptoBox_CloseUp()
On Error GoTo msgError
    DptoData.Recordset.FindFirst ("CodigoLugar=" & DptoBox.Text)
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub


Private Sub eraser_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar el pedido SAT?", vbYesNo)
    If nada = vbYes Then
        PedSATData.Recordset.Delete
        nada = Anterior()
    End If
    
End Sub

Private Sub exit_Click()
    Unload Me
    
End Sub

Private Sub first_Click()
    Dim nada As Integer
    nada = Primero()
    
End Sub

Private Sub ira_Click()
    Set Objeto = PedSATForm.PedSATData
    Display(0) = "NPetSAT"
    Selct = False
    IraForm.Show modal
    
End Sub

Private Sub last_Click()
    Dim nada As Integer
    nada = Ultimo()
End Sub

Private Sub lista_Click()
    Dim nada As Integer
    nada = ListaPSAT()
End Sub

Private Sub sate_Click()
    SATForm.Show modal
    SATForm.IndiceBox.Text = "2"
End Sub

Private Sub sigui_Click()
    Dim nada As Integer
    nada = Siguiente()
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
On Error GoTo msgError
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        Unload Me
    Case "Add"
        nada = OtroPetSAT()
    Case "SAT"
        SATForm.Show modal
        SATForm.IndiceBox.Text = "2"
        
    Case "Lista"
        nada = ListaPSAT()
    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar el pedido SAT?", vbYesNo)
        If nada = vbYes Then
            PedSATData.Recordset.Delete
            nada = Anterior()
        End If

    Case "PT"
        Orden(2) = CLng(NPetBox.Caption)
        With SATAlbForm
            .Show modal
            .SATAlbData.RecordSource = "SELECT * FROM SATAlbaran WHERE NPetSAT=" & NPetBox.Caption
            .SATAlbData.Refresh
            If .SATAlbData.Recordset.RecordCount <> 0 Then
                nada = .SATAlbData.Recordset.Fields("NAlbaranSAT").Value
                .SATAAlb2Data.RecordSource = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT=" & nada & " ORDER BY Linea"
                .SATAAlb2Data.Refresh
                .SSDBGrid1.Refresh
            End If
            .ProveedorBox.Text = SATBox.Text
        End With
    
    Case "prim"
        nada = Primero()
    Case "ulti"
        nada = Ultimo()
        
    Case "Impr"
        nada = ImPedSAT()
        

    End Select
    
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Function Anterior()
    
    With PedSATData
        If .Recordset.RecordCount = 0 Then
            Exit Function
        Else
            .Recordset.MovePrevious
            If .Recordset.BOF = True Then
                .Recordset.MoveNext
            End If
        End If
    End With
                   
End Function

Private Function Siguiente()
    
    With PedSATData
        If .Recordset.RecordCount = 0 Then
            Exit Function
        Else
            .Recordset.MoveNext
            If .Recordset.EOF = True Then
                .Recordset.MovePrevious
            End If
        End If
    End With
                   
End Function
Private Function OtroPetSAT() As Integer

    Dim num As Integer, nada As String
    
On Error GoTo msgError
    num = UltPedSat()
    
    If Orden(1) <> 0 Then
        Load DBase
        With DBase
            .PedidoData.RecordSource = "SELECT CodigoMaquina, SAT FROM InventMaquinas WHERE CodigoMaquina=" & Orden(1)
            .PedidoData.Refresh
            If .PedidoData.Recordset.Fields("SAT").Value <> Null Then
                nada = .PedidoData.Recordset.Fields("SAT").Value
            End If
            
        End With
        Unload DBase
    End If
    
    If nada = "" Then
        nada = "No Especificada"
    End If
       
    With PedSATData
        .Recordset.AddNew
        .Recordset.Fields("NPetSAT").Value = num
        .Recordset.Fields("SAT").Value = nada
        .Recordset.Fields("FechaEnvio").Value = Date
        .Recordset.Fields("CodigoDpto").Value = Orden(3)
        .Recordset.Fields("CodigoOperario").Value = 0
        .Recordset.Fields("NOrden").Value = Orden(0)
        If Aparato.Text = "" Then
            .Recordset.Fields("Aparato").Value = "No Especificado"
        Else
            .Recordset.Fields("Aparato").Value = Aparato.Text
        End If
        If Marca.Text = "" Then
            .Recordset.Fields("Marca").Value = "No especificado"
        Else
            .Recordset.Fields("Marca").Value = Marca.Text
        End If
        If NSerie.Text = "" Then
            .Recordset.Fields("NSerie").Value = "No especificado"
        Else
            .Recordset.Fields("NSerie").Value = NSerie.Text
        End If
        .Recordset.Update
        .Recordset.MoveLast
    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function ListaPSAT()

    Dim nada As Integer
        
    nada = NuevaBusq()
    Display(0) = "NPetSAT"
    Display(3) = "SAT"
    Display(2) = "FechaEnvio"
    Display(1) = "CodigoOperario"
        
    TypeCampos(0) = 1
    TypeCampos(1) = 3
    TypeCampos(2) = 2
    TypeCampos(3) = 3
    TypeCampos(4) = 0
    TypeCampos(5) = 0
    TypeCampos(6) = 0
    TypeCampos(7) = 0
        
    For nada = 0 To 3
        campos(nada) = Display(nada)
    Next
        
    Lists(3, 0) = "Nombre"
    Lists(3, 1) = "Nombre"
    Lists(3, 2) = "SAT"
    Lists(1, 0) = "CodigoOperario, Nombre"
    Lists(1, 1) = "CodigoOperario"
    Lists(1, 2) = "Operarios"
        
    FRO = "PedidoSAT"
    SELE = campos(0) & "," & campos(1) & "," & campos(2) & "," & campos(3)
                
    Set Objeto = PedSATForm.PedSATData
    
    Selct = False
        
    nada = Listado()
    

End Function

Private Function Primero()
    PedSATData.Recordset.MoveFirst
    
End Function

Private Function Ultimo()
    PedSATData.Recordset.MoveLast
End Function

Private Function ImPedSAT()
    
    Dim ATc As Long, ATn As String
    Dim ScaW As Double, ScaH As Double, Ant As Double, Wi As Double
    Dim Seg As Double, i As Long
On Error GoTo msgError
    With ParteTrabajoI
            .Show modal
            .NPetSATBox.Text = NPetBox.Caption
            .SATBox.Text = SATBox.Text
            .AreaTCBox.Caption = AreaT.Text
            .AreaTBox.Caption = AreaTBox.Caption
            .OrdenBox.Text = NOrdenBox.Text
            .PrintForm
    End With
    Unload ParteTrabajoI
    
    If SalidaBox.Value = 1 Then
        Printer.PaperBin = 2
        Printer.Orientation = 1
        Printer.FontName = "Times New Roman"
        Printer.Font.Size = 12
        Printer.Font.Bold = True
        ScaW = Printer.ScaleWidth
        ScaH = Printer.ScaleHeight
        Seg = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXX")
        Printer.Print
        Printer.Print
        Printer.Print
        Ant = Printer.CurrentY
        Printer.Print "CL�NICA UNIVERSITARIA"
        Wi = Printer.TextWidth("Apartado 192")
        Printer.CurrentY = Ant
        Printer.CurrentX = ScaW - Wi
        Printer.Print "Apartado 192"
        Ant = Printer.CurrentY
        Printer.Print "FACULTAD DE MEDICINA"
        Wi = Printer.TextWidth("Tlfno. 948/ 25 54 00")
        Printer.CurrentY = Ant
        Printer.CurrentX = ScaW - Wi
        Printer.Print "Tlfno. 948/ 25 54 00"
        Ant = Printer.CurrentY
        Printer.Print "UNIVERSIDAD DE NAVARRA"
        Wi = Printer.TextWidth("Fax. 948/ 29 65 00")
        Printer.CurrentY = Ant
        Printer.CurrentX = ScaW - Wi
        Printer.Print "Fax. 948/ 29 65 00"
        Ant = Printer.CurrentY
        Printer.Print "SERVICIO DE MANTENIMIENTO"
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Font.Bold = False
        Wi = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXX")
        Printer.CurrentX = ScaW - Wi
        Printer.Print "Pamplona, " & CStr(Date)
        Printer.Print
        Printer.Print
        Printer.Font.Bold = True
        Printer.Font.Underline = True
        Wi = Printer.TextWidth("SERVICIOS T�CNICOS EXTERIORES- Solicitud de asistencia t�cnica N� " & NPetBox.Caption)
        Printer.CurrentX = (ScaW - Wi) / 2
        Printer.Print "SERVICIOS T�CNICOS EXTERIORES- Solicitud de asistencia t�cnica N� " & NPetBox.Caption
        Printer.Font.Underline = False
        Printer.Print
        Printer.Print
        Printer.Print
        Ant = Printer.CurrentY
        Printer.Print "USUARIO:"
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Font.Bold = False
        Printer.Print DptoLabel.Caption
        Printer.Print
        Printer.Font.Bold = True
        Ant = Printer.CurrentY
        Printer.Print "APARATO:"
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Font.Bold = False
        Printer.Print AparatoBox.Text
        Printer.Print
        Printer.Font.Bold = True
        Ant = Printer.CurrentY
        Printer.Print "MARCA-MODELO:"
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Font.Bold = False
        Printer.Print MarcaBox.Text
        Printer.Print
        Printer.Font.Bold = True
        Ant = Printer.CurrentY
        Printer.Print "N�MERO DE SERIE:"
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Font.Bold = False
        Printer.Print NSerieBox.Text
        Printer.Print
        Printer.Font.Bold = True
        Ant = Printer.CurrentY
        Printer.Print "S.A.T.:"
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Font.Bold = False
        Printer.Print SATBox.Text
        Printer.Print
        Printer.Font.Bold = True
        Ant = Printer.CurrentY
        Printer.Print "PERSONA DE CONTACTO:"
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Font.Bold = False
        Printer.Print ContBox.Text
        Printer.Print
        Printer.Font.Bold = True
        Ant = Printer.CurrentY
        Printer.Print "DESCRIPCI�N:"
        Printer.Font.Bold = False
        Wi = Printer.TextWidth(Text1.Text)
        If Wi < ScaW Then
            Printer.Print Text1.Text
        Else
            ATn = ""
            For i = 1 To Len(Text1.Text)
                ATn = ATn & Mid(Text1.Text, i, 1)
                If Printer.TextWidth(ATn) >= ScaW Then
                    Printer.Print ATn
                    ATn = ""
                End If
            Next
        End If
                
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print "     Rogamos que tanto si prev�n un problema serio, reparaci�n costosa o plazo largo, se"
        Printer.Print "   pongan en contacto con el Sr." & OperLabel.Caption & ", llamando al n�mero 948/ 25 54 00"
        Printer.Print "   ext.2910 en horario de oficina."
        Printer.Print
        Printer.Font.Bold = True
        Printer.Print "     Tanto el equipo una vez reparado como la correspondiente factura ( a la que se acompa�ar�n"
        Printer.Print "   dos copias de la misma y el PARTE DE TRABAJO RELLENADO que se adjunta) deber�n remitirlas a la "
        Printer.Print "   siguiente direcci�n:"
        Printer.Print
        Printer.Font.Bold = False
        Wi = Printer.TextWidth("CLINICA UNIVERSITARIA")
        Printer.CurrentX = (ScaW - Wi) / 2
        Printer.Print "CLINICA UNIVERSITARIA"
        Wi = Printer.TextWidth("Servicio de Mantenimiento")
        Printer.CurrentX = (ScaW - Wi) / 2
        Printer.Print "Servicio de Mantenimiento"
        Wi = Printer.TextWidth("Avda.P�o XII s/n")
        Printer.CurrentX = (ScaW - Wi) / 2
        Printer.Print "Avda.P�o XII s/n"
        Wi = Printer.TextWidth("31008 PAMPLONA")
        Printer.CurrentX = (ScaW - Wi) / 2
        Printer.Print "31008 PAMPLONA"
 
        Printer.EndDoc

    End If
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function
