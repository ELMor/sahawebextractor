VERSION 5.00
Begin VB.Form MenuConForm 
   Caption         =   "Men� Configuraciones"
   ClientHeight    =   4860
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5805
   LinkTopic       =   "Form1"
   ScaleHeight     =   4860
   ScaleWidth      =   5805
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2040
      TabIndex        =   12
      Top             =   4320
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   1320
      TabIndex        =   5
      Top             =   240
      Width           =   975
   End
   Begin VB.CommandButton Command3 
      Height          =   375
      Left            =   1320
      TabIndex        =   4
      Top             =   840
      Width           =   975
   End
   Begin VB.CommandButton Command6 
      Height          =   375
      Left            =   1320
      TabIndex        =   3
      Top             =   1440
      Width           =   975
   End
   Begin VB.CommandButton Command9 
      Height          =   375
      Left            =   1320
      TabIndex        =   2
      Top             =   2040
      Width           =   975
   End
   Begin VB.CommandButton Command10 
      Height          =   375
      Left            =   1320
      TabIndex        =   1
      Top             =   2640
      Width           =   975
   End
   Begin VB.CommandButton Command11 
      Height          =   375
      Left            =   1320
      TabIndex        =   0
      Top             =   3240
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Mantenimiento Proveedores"
      Height          =   255
      Left            =   2520
      TabIndex        =   11
      Top             =   360
      Width           =   2055
   End
   Begin VB.Label Label3 
      Caption         =   "Mantenimiento Articulos"
      Height          =   255
      Left            =   2520
      TabIndex        =   10
      Top             =   960
      Width           =   1815
   End
   Begin VB.Label Label7 
      Caption         =   "Mantenimiento Inventario"
      Height          =   255
      Left            =   2520
      TabIndex        =   9
      Top             =   1560
      Width           =   1935
   End
   Begin VB.Label Label10 
      Caption         =   "Mantenimiento Preventivo"
      Height          =   255
      Left            =   2520
      TabIndex        =   8
      Top             =   2160
      Width           =   1935
   End
   Begin VB.Label Label11 
      Caption         =   "Mantenimiento Secciones"
      Height          =   255
      Left            =   2520
      TabIndex        =   7
      Top             =   2760
      Width           =   1935
   End
   Begin VB.Label Label12 
      Caption         =   "Mantenimiento Operadores"
      Height          =   255
      Left            =   2520
      TabIndex        =   6
      Top             =   3360
      Width           =   2055
   End
End
Attribute VB_Name = "MenuConForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    
    MantProv.Show modal
    Unload Me

End Sub

Private Sub Command11_Click()
    
    MantOperariosForm.Show modal
    Unload Me
End Sub

Private Sub Command2_Click()
    
    Unload Me
    
End Sub

Private Sub Command3_Click()
    
    MantArt.Show modal
    Unload Me
End Sub

Private Sub Command6_Click()
    
    MantInvForm.Show modal
    Unload Me
End Sub
Private Sub Command9_Click()
    
    MP2Form.Show modal
    Unload Me
End Sub


Private Sub Command10_Click()
    
    ManTarifasForm.Show modal
    Unload Me
End Sub

