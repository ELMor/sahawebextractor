VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form ParteTrabajoI 
   BackColor       =   &H80000009&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   11520
   ClientLeft      =   1875
   ClientTop       =   0
   ClientWidth     =   11445
   LinkTopic       =   "Form1"
   ScaleHeight     =   11520
   ScaleWidth      =   11445
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox OrdenBox 
      Appearance      =   0  'Flat
      DataField       =   "NAlbaranP"
      DataSource      =   "SATAlbData"
      Height          =   405
      Left            =   9480
      TabIndex        =   46
      Top             =   2760
      Width           =   1575
   End
   Begin VB.TextBox Text4 
      Appearance      =   0  'Flat
      DataField       =   "GastosDesp"
      DataSource      =   "SATAlbData"
      Height          =   285
      Left            =   5760
      TabIndex        =   44
      Top             =   4680
      Width           =   1455
   End
   Begin VB.TextBox Text3 
      Appearance      =   0  'Flat
      Height          =   495
      Left            =   1320
      TabIndex        =   41
      Top             =   10080
      Width           =   9735
   End
   Begin VB.TextBox Text2 
      Appearance      =   0  'Flat
      Height          =   495
      Left            =   1320
      TabIndex        =   40
      Top             =   9480
      Width           =   9735
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   495
      Left            =   1320
      TabIndex        =   37
      Top             =   8880
      Width           =   9735
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   1290
      Left            =   840
      Picture         =   "ParteTrabajoI.frx":0000
      ScaleHeight     =   1290
      ScaleWidth      =   1110
      TabIndex        =   31
      Top             =   240
      Width           =   1110
   End
   Begin VB.TextBox STotalHTDBox 
      Appearance      =   0  'Flat
      DataField       =   "SubTotalHTD"
      DataSource      =   "SATAlbData"
      Height          =   405
      Left            =   8760
      TabIndex        =   21
      Top             =   4080
      Width           =   2055
   End
   Begin VB.TextBox HTBox 
      Appearance      =   0  'Flat
      DataField       =   "HorasTecnico"
      DataSource      =   "SATAlbData"
      Height          =   285
      Left            =   1920
      TabIndex        =   20
      Top             =   3840
      Width           =   1455
   End
   Begin VB.TextBox PUniHTBox 
      Appearance      =   0  'Flat
      DataField       =   "PrecioUnitarioHT"
      DataSource      =   "SATAlbData"
      Height          =   285
      Left            =   6480
      TabIndex        =   19
      Top             =   3840
      Width           =   1455
   End
   Begin VB.TextBox DietasBox 
      Appearance      =   0  'Flat
      DataField       =   "GastosDesp"
      DataSource      =   "SATAlbData"
      Height          =   285
      Left            =   3720
      TabIndex        =   18
      Top             =   4200
      Width           =   1455
   End
   Begin VB.TextBox STotalCBox 
      Appearance      =   0  'Flat
      DataField       =   "SubTotalC"
      DataSource      =   "SATAlbData"
      Enabled         =   0   'False
      Height          =   285
      Left            =   9840
      TabIndex        =   16
      Top             =   8280
      Width           =   1215
   End
   Begin VB.TextBox FechaParteBox 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000E&
      Height          =   405
      Left            =   1560
      TabIndex        =   14
      Top             =   2760
      Width           =   2055
   End
   Begin VB.TextBox SATBox 
      Appearance      =   0  'Flat
      Height          =   405
      Left            =   5280
      TabIndex        =   12
      Top             =   2760
      Width           =   2655
   End
   Begin VB.TextBox SuNASAT 
      Appearance      =   0  'Flat
      DataField       =   "NAlbaranP"
      DataSource      =   "SATAlbData"
      Height          =   405
      Left            =   5280
      TabIndex        =   4
      Top             =   2280
      Width           =   2055
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H80000009&
      Caption         =   "TOTAL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   4320
      TabIndex        =   2
      Top             =   10680
      Width           =   3255
      Begin VB.TextBox TotalBox 
         Appearance      =   0  'Flat
         DataField       =   "Total"
         DataSource      =   "SATAlbData"
         Enabled         =   0   'False
         Height          =   405
         Left            =   720
         TabIndex        =   3
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.TextBox NASATBox 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H8000000E&
      DataField       =   "NAlbaranSAT"
      DataSource      =   "SATAlbData"
      Enabled         =   0   'False
      Height          =   405
      Left            =   1200
      TabIndex        =   1
      Top             =   2280
      Width           =   2295
   End
   Begin VB.TextBox NPetSATBox 
      Appearance      =   0  'Flat
      DataField       =   "NPetSAT"
      DataSource      =   "SATAlbData"
      Height          =   405
      Left            =   9360
      TabIndex        =   0
      Top             =   2280
      Width           =   1695
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Bindings        =   "ParteTrabajoI.frx":1DCA
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   5520
      Width           =   10935
      ScrollBars      =   2
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   12383
      Columns(0).Caption=   "Art�culo"
      Columns(0).Name =   "Articulo"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "Articulo"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1508
      Columns(1).Caption=   "Cantidad"
      Columns(1).Name =   "Cantidad"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Cantidad"
      Columns(1).DataType=   3
      Columns(1).FieldLen=   256
      Columns(2).Width=   1720
      Columns(2).Caption=   "Pr.Unitario"
      Columns(2).Name =   "PrecioUnit"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "PrecioUnit"
      Columns(2).DataType=   6
      Columns(2).NumberFormat=   "CURRENCY"
      Columns(2).FieldLen=   256
      Columns(3).Width=   1032
      Columns(3).Caption=   "Dto"
      Columns(3).Name =   "Descuento"
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "Descuento"
      Columns(3).DataType=   3
      Columns(3).FieldLen=   256
      Columns(4).Width=   1693
      Columns(4).Caption=   "Parcial"
      Columns(4).Name =   "PParcial"
      Columns(4).CaptionAlignment=   0
      Columns(4).DataField=   "PParcial"
      Columns(4).DataType=   6
      Columns(4).NumberFormat=   "CURRENCY"
      Columns(4).FieldLen=   256
      UseDefaults     =   -1  'True
      _ExtentX        =   19288
      _ExtentY        =   450
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label26 
      BackColor       =   &H80000009&
      Caption         =   "Orden N�:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8280
      TabIndex        =   45
      Top             =   2880
      Width           =   1095
   End
   Begin VB.Label Label25 
      BackColor       =   &H80000009&
      Caption         =   "Horas de paro del equipo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2880
      TabIndex        =   43
      Top             =   4680
      Width           =   2775
   End
   Begin VB.Label Label24 
      BackColor       =   &H80000009&
      Caption         =   "Material utilizado en la reparaci�n:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   42
      Top             =   5160
      Width           =   3615
   End
   Begin VB.Line Line2 
      BorderWidth     =   2
      X1              =   8040
      X2              =   8040
      Y1              =   3840
      Y2              =   4560
   End
   Begin VB.Label Label23 
      BackColor       =   &H80000009&
      Caption         =   "Acci�n:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   0
      TabIndex        =   39
      Top             =   9960
      Width           =   1215
   End
   Begin VB.Label Label22 
      BackColor       =   &H80000009&
      Caption         =   "Causa:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   0
      TabIndex        =   38
      Top             =   9480
      Width           =   1215
   End
   Begin VB.Label Label21 
      BackColor       =   &H80000009&
      Caption         =   "Diagn�stico:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   0
      TabIndex        =   36
      Top             =   9000
      Width           =   1215
   End
   Begin VB.Label Label20 
      BackColor       =   &H80000009&
      Caption         =   "Fax: 17.22.94"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      TabIndex        =   35
      Top             =   720
      Width           =   1695
   End
   Begin VB.Label Label19 
      BackColor       =   &H80000009&
      Caption         =   "T�lex: 37.963 CUDN-E"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      TabIndex        =   34
      Top             =   480
      Width           =   2175
   End
   Begin VB.Label Label18 
      BackColor       =   &H80000009&
      Caption         =   "Apartado, 192 "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      TabIndex        =   33
      Top             =   960
      Width           =   1335
   End
   Begin VB.Label Label11 
      BackColor       =   &H80000009&
      Caption         =   "31080 PAMPLONA (Espa�a) "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      TabIndex        =   32
      Top             =   1200
      Width           =   2655
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      Caption         =   "Servicio de Mantenimiento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3600
      TabIndex        =   30
      Top             =   600
      Width           =   3015
   End
   Begin VB.Label Label16 
      BackColor       =   &H80000009&
      Caption         =   "Tel�fono: 948 { 27.33.11 - 27.43.11"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      TabIndex        =   29
      Top             =   240
      Width           =   3135
   End
   Begin VB.Label Label14 
      BackColor       =   &H80000009&
      Caption         =   "     Universidad de Navarra"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3720
      TabIndex        =   28
      Top             =   1320
      Width           =   2655
   End
   Begin VB.Label Label13 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      Caption         =   "Facultad de Medicina "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3960
      TabIndex        =   27
      Top             =   960
      Width           =   2175
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      Caption         =   "Cl�nica Universitaria"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3240
      TabIndex        =   26
      Top             =   120
      Width           =   3735
   End
   Begin VB.Line Line17 
      BorderWidth     =   2
      X1              =   120
      X2              =   11040
      Y1              =   8280
      Y2              =   8280
   End
   Begin VB.Line Line16 
      BorderWidth     =   2
      X1              =   120
      X2              =   11040
      Y1              =   6480
      Y2              =   6480
   End
   Begin VB.Line Line15 
      BorderWidth     =   2
      X1              =   120
      X2              =   11040
      Y1              =   6840
      Y2              =   6840
   End
   Begin VB.Line Line14 
      BorderWidth     =   2
      X1              =   120
      X2              =   11040
      Y1              =   7200
      Y2              =   7200
   End
   Begin VB.Line Line13 
      BorderWidth     =   2
      X1              =   120
      X2              =   11040
      Y1              =   7560
      Y2              =   7560
   End
   Begin VB.Line Line12 
      BorderWidth     =   2
      X1              =   120
      X2              =   11040
      Y1              =   7920
      Y2              =   7920
   End
   Begin VB.Line Line11 
      BorderWidth     =   2
      X1              =   120
      X2              =   11040
      Y1              =   6120
      Y2              =   6120
   End
   Begin VB.Line Line10 
      BorderWidth     =   2
      X1              =   7440
      X2              =   7440
      Y1              =   5760
      Y2              =   8280
   End
   Begin VB.Line Line9 
      BorderWidth     =   2
      X1              =   8280
      X2              =   8280
      Y1              =   5760
      Y2              =   8280
   End
   Begin VB.Line Line8 
      BorderWidth     =   2
      X1              =   9240
      X2              =   9240
      Y1              =   5760
      Y2              =   8280
   End
   Begin VB.Line Line7 
      BorderWidth     =   2
      X1              =   9840
      X2              =   9840
      Y1              =   5760
      Y2              =   8280
   End
   Begin VB.Line Line6 
      BorderWidth     =   2
      X1              =   11040
      X2              =   11040
      Y1              =   5760
      Y2              =   8280
   End
   Begin VB.Line Line4 
      BorderWidth     =   2
      X1              =   120
      X2              =   120
      Y1              =   5760
      Y2              =   8280
   End
   Begin VB.Label Label7 
      BackColor       =   &H80000009&
      Caption         =   "Gastos desplazamiento y dietas:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   25
      Top             =   4200
      Width           =   3495
   End
   Begin VB.Label Label6 
      BackColor       =   &H80000009&
      Caption         =   "Precio Unitario Hora T�c.:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3720
      TabIndex        =   24
      Top             =   3840
      Width           =   2775
   End
   Begin VB.Label Label5 
      BackColor       =   &H80000009&
      Caption         =   "Horas T�cnico:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   23
      Top             =   3840
      Width           =   1695
   End
   Begin VB.Label Label8 
      BackColor       =   &H80000009&
      Caption         =   "SubTotal (Horas T�c. y Dietas):"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8160
      TabIndex        =   22
      Top             =   3840
      Width           =   3375
   End
   Begin VB.Label Label9 
      BackColor       =   &H80000009&
      Caption         =   "SubTotal (material):"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7680
      TabIndex        =   17
      Top             =   8280
      Width           =   2175
   End
   Begin VB.Label AreaTCBox 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   2160
      TabIndex        =   13
      Top             =   3240
      Width           =   975
   End
   Begin VB.Label Label15 
      BackColor       =   &H80000009&
      Caption         =   "N�Parte SAT:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3720
      TabIndex        =   11
      Top             =   2400
      Width           =   1455
   End
   Begin VB.Line Line1 
      X1              =   3120
      X2              =   3480
      Y1              =   3480
      Y2              =   3480
   End
   Begin VB.Label AreaTBox 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      DataField       =   "Descripcion"
      DataSource      =   "MaquinaData"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   3480
      TabIndex        =   10
      Top             =   3240
      Width           =   7575
   End
   Begin VB.Label Label4 
      BackColor       =   &H80000009&
      Caption         =   "�rea de Trabajo :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   3360
      Width           =   1935
   End
   Begin VB.Label Label3 
      BackColor       =   &H80000009&
      Caption         =   "S.A.T."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4440
      TabIndex        =   8
      Top             =   2880
      Width           =   735
   End
   Begin VB.Label Label2 
      BackColor       =   &H80000009&
      Caption         =   "Fecha Parte:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   2880
      Width           =   1335
   End
   Begin VB.Label Label1 
      BackColor       =   &H80000009&
      Caption         =   "N� Parte:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   2400
      Width           =   975
   End
   Begin VB.Label Label10 
      BackColor       =   &H80000009&
      Caption         =   "N�Pedido SAT:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7560
      TabIndex        =   5
      Top             =   2400
      Width           =   1695
   End
End
Attribute VB_Name = "ParteTrabajoI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
