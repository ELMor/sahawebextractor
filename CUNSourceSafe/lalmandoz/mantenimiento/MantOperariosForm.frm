VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form MantOperariosForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Mantenimiento Operarios"
   ClientHeight    =   2640
   ClientLeft      =   150
   ClientTop       =   675
   ClientWidth     =   6210
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2640
   ScaleWidth      =   6210
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   6210
      _ExtentX        =   10954
      _ExtentY        =   1535
      ButtonWidth     =   1349
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   8
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "first"
            Description     =   "PrimBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntBoton"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "last"
            Description     =   "UltBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox Check1 
      Alignment       =   1  'Right Justify
      Caption         =   "Activo:"
      DataField       =   "Activo"
      DataSource      =   "OperData"
      Height          =   255
      Left            =   1200
      TabIndex        =   6
      Top             =   2160
      Width           =   1095
   End
   Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
      Bindings        =   "MantOperariosForm.frx":0000
      DataField       =   "CodigoEspecialidad"
      DataSource      =   "OperData"
      Height          =   255
      Left            =   2040
      TabIndex        =   5
      Top             =   1800
      Width           =   2175
      DataFieldList   =   "CodigoEspecialidad"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Caption=   "C�digo Especialidad"
      Columns(0).Name =   "CodigoEspecialidad"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoEspecialidad"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   4948
      Columns(1).Caption=   "Especialidad"
      Columns(1).Name =   "Especialidad"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Especialidad"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3836
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "CodigoEspecialidad"
   End
   Begin VB.TextBox OpeBox 
      DataField       =   "Nombre"
      DataSource      =   "OperData"
      Height          =   285
      Left            =   2040
      TabIndex        =   4
      Top             =   1440
      Width           =   3375
   End
   Begin VB.Data OperData 
      Caption         =   "Operarios"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   2880
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Operarios"
      Top             =   0
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Data EspeData 
      Caption         =   "Especialidad"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   0
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Tarifas"
      Top             =   0
      Visible         =   0   'False
      Width           =   1455
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantOperariosForm.frx":0013
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantOperariosForm.frx":032D
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantOperariosForm.frx":0647
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantOperariosForm.frx":0961
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantOperariosForm.frx":0C7B
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantOperariosForm.frx":0F95
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantOperariosForm.frx":12AF
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantOperariosForm.frx":15C9
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label CodOpBox 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      DataField       =   "CodigoOperario"
      DataSource      =   "OperData"
      Height          =   255
      Left            =   2040
      TabIndex        =   3
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "Codigo Especialidad:"
      Height          =   255
      Left            =   360
      TabIndex        =   2
      Top             =   1800
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "Nombre Operario:"
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   1440
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Operario:"
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   1080
      Width           =   1335
   End
   Begin VB.Menu Prg 
      Caption         =   "&Programa"
      Begin VB.Menu exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu reg 
      Caption         =   "&Registros"
      Begin VB.Menu prim 
         Caption         =   "Primero"
      End
      Begin VB.Menu sigui 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu Ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu ulti 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu add 
         Caption         =   "A�adir"
      End
      Begin VB.Menu eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu listados 
      Caption         =   "&Listados"
      Begin VB.Menu all 
         Caption         =   "Todos"
      End
   End
End
Attribute VB_Name = "MantOperariosForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Function Anterior()
    OperData.Recordset.MovePrevious
 
    If OperData.Recordset.BOF = True Then
        OperData.Recordset.MoveNext
    End If
    
End Function

Private Function OtroOp()
    
    Dim i As Long
    
    OperData.Recordset.MoveLast
    i = OperData.Recordset.Fields("CodigoOperario").Value
    OperData.Recordset.AddNew
    OperData.Recordset.Fields("CodigoOperario").Value = i + 1
    OperData.Recordset.Fields("CodigoEspecialidad").Value = 0
    OperData.Recordset.Fields("Activo").Value = True
    OperData.Recordset.Update
    OperData.Recordset.MoveLast
    
End Function

Private Function Siguiente()
    
    OperData.Recordset.MoveNext
    
    If OperData.Recordset.EOF = True Then
        OperData.Recordset.MovePrevious
    End If

End Function




Private Sub add_Click()
    Dim nada As Integer
    nada = OtroOp()
End Sub

Private Sub all_Click()
    Dim nada As Integer
    nada = ListaOp()
End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()
End Sub

Private Sub eraser_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar el operario?", vbYesNo)
    If nada = vbYes Then
        OperData.Recordset.Delete
        nada = Anterior()
    End If

End Sub

Private Sub exit_Click()
    Unload Me
End Sub

Private Sub prim_Click()
    OperData.Recordset.MoveFirst
End Sub

Private Sub sigui_Click()
    Dim nada As Integer
    nada = Siguiente()
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        Unload Me
    Case "Add"
        nada = OtroOp()
    
    Case "Lista"
        nada = ListaOp()
    
    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar el operario?", vbYesNo)
        If nada = vbYes Then
            OperData.Recordset.Delete
            nada = Anterior()
        End If
    
    Case "last"
        OperData.Recordset.MoveLast
    Case "first"
        OperData.Recordset.MoveFirst

    End Select
    

End Sub


Private Sub ulti_Click()
    OperData.Recordset.MoveLast
End Sub

Private Function ListaOp()

    Dim nada As Integer
    
    nada = NuevaBusq()
    campos(0) = "CodigoOperario"
    campos(1) = "Nombre"
    campos(2) = "CodigoEspecialidad"
    campos(3) = "Activo"
    For nada = 0 To 3
        Display(nada) = campos(nada)
    Next
        
    SELE = "CodigoOperario, Nombre, CodigoEspecialidad, Activo"
    FRO = "Operarios"
        
    Set Objeto = MantOperariosForm.OperData
    
    Selct = False
    
    MosBusForm.Show modal

End Function
