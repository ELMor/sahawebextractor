VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form MosBusForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Resultado"
   ClientHeight    =   4560
   ClientLeft      =   1920
   ClientTop       =   1905
   ClientWidth     =   8715
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4560
   ScaleWidth      =   8715
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "&Excel"
      Height          =   375
      Left            =   5640
      TabIndex        =   6
      Top             =   3960
      Width           =   1095
   End
   Begin VB.TextBox OpcionBox 
      Height          =   285
      Left            =   840
      TabIndex        =   5
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.CommandButton ImpBoton 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   3840
      TabIndex        =   2
      Top             =   3960
      Width           =   1095
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Bindings        =   "MosBusForm.frx":0000
      Height          =   3255
      Left            =   240
      TabIndex        =   1
      Top             =   600
      Width           =   8175
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowUpdate     =   0   'False
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      MaxSelectedRows =   999
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      UseDefaults     =   0   'False
      _ExtentX        =   14420
      _ExtentY        =   5741
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Data ResData 
      Caption         =   "Resultado"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   0
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   3840
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton SalirBoton 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2160
      TabIndex        =   0
      Top             =   3960
      Width           =   1095
   End
   Begin VB.Label RegBox 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   4080
      TabIndex        =   4
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "N� Registros:"
      Height          =   255
      Left            =   2880
      TabIndex        =   3
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "MosBusForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
    
    Dim col As Integer, j As Integer, i As Integer
    
    If Display(4) = "CodigoMaquina (SECUENCIAL)" Then
        campos(4) = ""
    End If
    
    col = ResData.Recordset.Fields.Count - 1
    
    With Excel.Application
        .Workbooks.add
        .Worksheets.add
        .Visible = True
        fil = ResData.Recordset.RecordCount
            
        For i = 1 To col + 1
            .Cells(1, i) = ResData.Recordset.Fields(i - 1).Name
        Next
        
        ResData.Recordset.MoveFirst
        j = 2
    
        While ResData.Recordset.EOF = False
            For i = 0 To col
                If IsNull(ResData.Recordset.Fields(i).Value) = False Then
                    .Cells(j, i + 1) = ResData.Recordset.Fields(i).Value
                End If
            
            Next
            j = j + 1
            ResData.Recordset.MoveNext
        Wend
    
    End With
    

End Sub

Private Sub Form_Load()

    Dim Busqueda As String
    
    Busqueda = "SELECT " & SELE & " FROM " & FRO
    
    If WHER <> "" Then
        Busqueda = Busqueda & " WHERE " + WHER
    End If
    
    If ORD <> "" Then
        Busqueda = Busqueda & " ORDER BY " & ORD
    End If
    
    ResData.RecordSource = Busqueda
    ResData.Refresh
    
    RegBox.Caption = ResData.Recordset.RecordCount
    
End Sub

Private Sub ImpBoton_Click()

    Dim wdDoc As Word.Document
    Dim wdApp As Word.Application
    Dim fil As Long, col As Long
    Dim Rango As Range
    Dim i As Integer, j As Integer
                   
    Set wdApp = New Word.Application
    Set wdDoc = wdApp.Documents.add("c:\archivos de programa\almacen\doc\listados.dot")
    
    wdDoc.Activate
    
    While campos(col) <> ""
        col = col + 1
    Wend
    col = col - 1
    
    fil = ResData.Recordset.RecordCount
            
    Set Rango = wdDoc.Bookmarks("listado").Range
    wdDoc.Tables.add Rango, (fil + 1), (col + 1)
    
    For i = 1 To col + 1
        wdDoc.Tables(1).Cell(1, i).Select
        Selection.Text = Display(i - 1)
        Selection.Font.Bold = True
    Next
    
    ResData.Recordset.MoveFirst
    j = 2
        
    While ResData.Recordset.EOF = False
        For i = 0 To col
            wdDoc.Tables(1).Cell(j, i + 1).Select
            If IsNull(ResData.Recordset.Fields(Display(i)).Value) = False Then
                Selection.Text = ResData.Recordset.Fields(Display(i)).Value
            End If
        Next
        j = j + 1
        ResData.Recordset.MoveNext
    Wend
        
    wdApp.Visible = True

    Set wdApp = Nothing
    Set wdDoc = Nothing
    
    
End Sub

Private Sub SalirBoton_Click()

    Unload Me

End Sub

Private Sub SSDBGrid1_DblClick()

    Dim Aal As Variant
    
    If OpcionBox.Text = "0" Then
        Exit Sub
    End If
      
    Aal = ResData.Recordset.Fields(Display(0)).Value
    
    If Selct2 = 1 Then
        MantArt.GrArtCombo.Text = ResData.Recordset.Fields("GrupoArt").Value
        Selct2 = 0
    End If
        
    If Selct2 = 2 Then
        Objeto.Recordset.FindFirst (Display(0) & "='" & Aal & "'")
    Else
        Objeto.Recordset.FindFirst (Display(0) & "=" & Aal)
    End If
    
    If Selct = True Then
        If DataCarac2 = "" Then
            Objeto2.RecordSource = DataCarac & Aal
        Else
            Objeto2.RecordSource = DataCarac & Aal & DataCarac2
        End If
            Objeto2.Refresh
    End If
    
    Selct2 = 0
    
    Unload Me
    
End Sub

