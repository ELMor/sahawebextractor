VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form MantDptoForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Mantenimiento Departamentos"
   ClientHeight    =   1860
   ClientLeft      =   150
   ClientTop       =   675
   ClientWidth     =   6150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1860
   ScaleWidth      =   6150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   6150
      _ExtentX        =   10848
      _ExtentY        =   1535
      ButtonWidth     =   1349
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   8
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "first"
            Description     =   "PrimBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigButon"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "last"
            Description     =   "UltBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
      EndProperty
   End
   Begin VB.TextBox DptoBox 
      DataField       =   "Localizacion"
      DataSource      =   "DptoData"
      Height          =   285
      Left            =   1320
      TabIndex        =   3
      Top             =   1440
      Width           =   4695
   End
   Begin VB.TextBox CodDptoBox 
      DataField       =   "CodigoLugar"
      DataSource      =   "DptoData"
      Height          =   285
      Left            =   2160
      TabIndex        =   1
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Data DptoData 
      Caption         =   "Dptos"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   3240
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Lugar"
      Top             =   120
      Visible         =   0   'False
      Width           =   1935
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantDptoForm.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantDptoForm.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantDptoForm.frx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantDptoForm.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantDptoForm.frx":0C68
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantDptoForm.frx":0F82
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantDptoForm.frx":129C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantDptoForm.frx":15B6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label2 
      Caption         =   "Departamento:"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo Depto.:"
      Height          =   255
      Left            =   840
      TabIndex        =   0
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Menu prgz 
      Caption         =   "&Programa"
      Begin VB.Menu exitz 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu regz 
      Caption         =   "&Registros"
      Begin VB.Menu primz 
         Caption         =   "Primero"
      End
      Begin VB.Menu antz 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu sigz 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu ultz 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu addz 
         Caption         =   "A�adir"
      End
      Begin VB.Menu erasez 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu listaz 
      Caption         =   "&Listados"
      Begin VB.Menu Listz 
         Caption         =   "Listado General"
      End
   End
End
Attribute VB_Name = "MantDptoForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub addz_Click()
    Dim nada As Integer
    nada = OtroDpto()

End Sub

Private Sub antz_Click()
    Dim nada As Integer
    nada = Anterior()
    
End Sub

Private Sub erasez_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar el departamento?", vbYesNo)
    If nada = vbYes Then
        DptoData.Recordset.Delete
        nada = Anterior()
    End If
End Sub

Private Sub exitz_Click()
    Unload Me
End Sub

Private Sub Listz_Click()
    Dim nada As Integer
    nada = ListDpto()
End Sub

Private Sub primz_Click()
    DptoData.Recordset.MoveFirst
End Sub

Private Sub sigz_Click()
    Dim nada As Integer
    nada = Siguiente()

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
    
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        Unload Me
    Case "Add"
        nada = OtroDpto()
    
    Case "Lista"
        nada = ListDpto()

    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar el departamento?", vbYesNo)
        If nada = vbYes Then
            DptoData.Recordset.Delete
            nada = Anterior()
        End If
    Case "first"
        DptoData.Recordset.MoveFirst
    Case "last"
        DptoData.Recordset.MoveLast


    End Select
    
End Sub


Private Function Anterior()
    With DptoData.Recordset
        If .EOF = False And .BOF = False Then
            .MovePrevious
            If .BOF = True Then
                .MoveNext
            End If
        End If
    End With
End Function

Private Function Siguiente()
    With DptoData.Recordset
        If .EOF = False And .BOF = False Then
            .MoveNext
            If .EOF = True Then
                .MovePrevious
            End If
        End If
    End With
End Function

Private Function OtroDpto()
    
    Dim nada As Integer
    With DptoData.Recordset
        .MoveLast
        nada = .Fields("CodigoLugar").Value
        .AddNew
        .Fields("CodigoLugar").Value = nada + 1
        .Update
        .MoveLast
    End With
        
End Function

Private Function ListDpto()
    
    Dim nada As Integer
        
    nada = NuevaBusq()
    campos(0) = "CodigoLugar"
    campos(1) = "Localizacion"
    For nada = 0 To 2
        Display(nada) = campos(nada)
    Next
        
    SELE = "CodigoLugar, Localizacion"
    FRO = "Lugar"
        
    Set Objeto = MantDptoForm.DptoData
    
    Selct = False

    MosBusForm.Show modal
        

End Function

Private Sub ultz_Click()
    DptoData.Recordset.MoveLast
End Sub
