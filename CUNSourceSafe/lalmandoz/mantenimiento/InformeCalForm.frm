VERSION 5.00
Object = "{02B5E320-7292-11CF-93D5-0020AF99504A}#1.0#0"; "MSCHART.OCX"
Begin VB.Form InformeCalForm 
   BackColor       =   &H80000009&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   11115
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11325
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11115
   ScaleWidth      =   11325
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox CodigoBox 
      Height          =   375
      Left            =   720
      TabIndex        =   21
      Text            =   "Text1"
      Top             =   720
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox Fecha2Box 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   6000
      TabIndex        =   20
      Top             =   1200
      Width           =   1575
   End
   Begin VB.TextBox Fecha1Box 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3720
      TabIndex        =   19
      Top             =   1200
      Width           =   1695
   End
   Begin VB.CommandButton Command3 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   8880
      TabIndex        =   18
      Top             =   1440
      Width           =   1815
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Imprimir P�gina"
      Height          =   375
      Left            =   8880
      TabIndex        =   17
      Top             =   840
      Width           =   1815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Siguiente P�gina"
      Height          =   375
      Left            =   8880
      TabIndex        =   16
      Top             =   240
      Width           =   1815
   End
   Begin VB.Data Data1 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   0
      Visible         =   0   'False
      Width           =   2295
   End
   Begin MSChartLib.MSChart MSChart1 
      Height          =   2775
      Left            =   720
      OleObjectBlob   =   "InformeCalForm.frx":0000
      TabIndex        =   7
      Top             =   3240
      Width           =   4695
   End
   Begin MSChartLib.MSChart MSChart2 
      Height          =   2775
      Left            =   6480
      OleObjectBlob   =   "InformeCalForm.frx":2350
      TabIndex        =   8
      Top             =   3240
      Width           =   4455
   End
   Begin MSChartLib.MSChart MSChart3 
      Height          =   2775
      Left            =   480
      OleObjectBlob   =   "InformeCalForm.frx":46A0
      TabIndex        =   14
      Top             =   8040
      Width           =   4695
   End
   Begin MSChartLib.MSChart MSChart4 
      Height          =   2775
      Left            =   6480
      OleObjectBlob   =   "InformeCalForm.frx":69F0
      TabIndex        =   15
      Top             =   8040
      Width           =   4455
   End
   Begin VB.Label TiMePa2Box 
      BackColor       =   &H80000009&
      Height          =   255
      Left            =   3840
      TabIndex        =   13
      Top             =   7440
      Width           =   1455
   End
   Begin VB.Label Label8 
      BackColor       =   &H80000009&
      Caption         =   "Tiempo medio de paro equipos:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   12
      Top             =   7440
      Width           =   2895
   End
   Begin VB.Label TiMeRe2Box 
      BackColor       =   &H80000009&
      Height          =   255
      Left            =   3840
      TabIndex        =   11
      Top             =   7080
      Width           =   1455
   End
   Begin VB.Label Label6 
      BackColor       =   &H80000009&
      Caption         =   "Tiempo medio de reparaci�n:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   10
      Top             =   7080
      Width           =   2775
   End
   Begin VB.Label Label4 
      BackColor       =   &H80000009&
      Caption         =   "Criticidad 2:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   6600
      Width           =   1335
   End
   Begin VB.Label TiMePaBox 
      BackColor       =   &H80000009&
      Height          =   255
      Left            =   4080
      TabIndex        =   6
      Top             =   2760
      Width           =   1455
   End
   Begin VB.Label Label5 
      BackColor       =   &H80000009&
      Caption         =   "Tiempo medio de paro equipos:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1080
      TabIndex        =   5
      Top             =   2760
      Width           =   2895
   End
   Begin VB.Label TiMeReBox 
      BackColor       =   &H80000009&
      Height          =   255
      Left            =   4080
      TabIndex        =   4
      Top             =   2400
      Width           =   1455
   End
   Begin VB.Label Label3 
      BackColor       =   &H80000009&
      Caption         =   "Tiempo medio de reparaci�n:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1080
      TabIndex        =   3
      Top             =   2400
      Width           =   2775
   End
   Begin VB.Label Label2 
      BackColor       =   &H80000009&
      Caption         =   "Criticidad 1:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   2
      Top             =   1920
      Width           =   1335
   End
   Begin VB.Label UniBox 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3360
      TabIndex        =   1
      Top             =   600
      Width           =   4695
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      Caption         =   "Informe de Calidad"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      TabIndex        =   0
      Top             =   120
      Width           =   2775
   End
End
Attribute VB_Name = "InformeCalForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    Dim nada As Integer
    If Mid(UniBox.Caption, 1, 3) = "SAT" Then
        nada = CalidadSAT2(CodigoBox.Text, CDate(Fecha1Box.Text), CDate(Fecha2Box.Text))
    Else
        nada = CalidadEsp2(CLng(CodigoBox.Text), CDate(Fecha1Box.Text), CDate(Fecha2Box.Text))
    End If
    Unload Me
    
End Sub

Private Sub Command2_Click()
    Command1.Visible = False
    Command2.Visible = False
    Command3.Visible = False
    InformeCalForm.PrintForm
    Command1.Visible = True
    Command2.Visible = True
    Command3.Visible = True
    
End Sub

Private Sub Command3_Click()
    Unload Me
    
End Sub
