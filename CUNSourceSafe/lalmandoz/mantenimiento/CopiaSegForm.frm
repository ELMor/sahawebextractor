VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{B26F6243-4C7D-11D1-910E-00600807163F}#2.78#0"; "xceedzip.ocx"
Begin VB.Form CopiaSegForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Copia de Seguridad"
   ClientHeight    =   2490
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4860
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2490
   ScaleWidth      =   4860
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.ProgressBar ProgressBar1 
      Height          =   330
      Left            =   210
      TabIndex        =   2
      Top             =   1050
      Width           =   4425
      _ExtentX        =   7805
      _ExtentY        =   582
      _Version        =   327682
      Appearance      =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Comenzar"
      Height          =   540
      Left            =   1575
      TabIndex        =   0
      Top             =   1680
      Width           =   1380
   End
   Begin XCEEDZIPLib.XceedZip ZipMain 
      Left            =   3960
      Top             =   1800
      _Version        =   131150
      _ExtentX        =   794
      _ExtentY        =   794
      _StockProps     =   0
      Compression     =   6
      ClearDisks      =   0   'False
      ExtractDirectory=   ""
      FilesToProcess  =   ""
      IncludeDirectoryEntries=   0   'False
      IncludeHiddenFiles=   0   'False
      IncludeVolumeLabel=   0   'False
      ModifiedDate    =   "01011980"
      MoveFiles       =   0   'False
      MultidiskMode   =   0   'False
      Overwrite       =   0
      Password        =   ""
      Recurse         =   0   'False
      SelfExtracting  =   0   'False
      SfxBinary       =   ""
      SfxConfigFile   =   ""
      StoredExtensions=   ".ZIP;.LZH;.ARC;.ARJ;.ZOO"
      TempPath        =   ""
      UsePaths        =   -1  'True
      UseTempFile     =   -1  'True
      ZipFileName     =   ""
      InternalState   =   "f1483cb6"
      SfxExtractDirectory=   ""
      SfxRunExePath   =   ""
      SfxReadmePath   =   ""
      SfxDefaultPassword=   ""
      SfxOverwrite    =   0
      SfxPromptForDirectory=   -1  'True
      SfxShowProgress =   -1  'True
      SfxPromptForPassword=   -1  'True
      SfxPromptCreateDirectory=   -1  'True
      SfxProgramGroup =   ""
      SfxProgramGroupItems=   ""
      SfxRegisterExtensions=   ""
      SfxIcon         =   "CopiaSegForm.frx":0000
      SfxInstallMode  =   0   'False
   End
   Begin VB.Label Label1 
      Caption         =   "Introduzca el primer disco y pulse el bot�n..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   420
      TabIndex        =   1
      Top             =   315
      Width           =   3795
   End
End
Attribute VB_Name = "CopiaSegForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()

  Dim ResultCode As Integer
  
  ZipMain.ClearDisks = True
  ZipMain.MultidiskMode = True
  ZipMain.ZipFileName = "a:\mantcun.zip"
  ZipMain.FilesToProcess = "d:\*.mdb" & Chr$(13)
  
  ResultCode = ZipMain.Add(xecAll)
  
  If (ResultCode >= XcdFirstError) Then
    MsgBox ("Ha ocurrido un error en el proceso...")
  Else
    MsgBox ("La copia ha terminado con �xito")
    Unload Me
  End If

End Sub


Private Sub Form_Load()

    With ProgressBar1
        .Max = 100
        .Min = 0
    End With
    
End Sub


Private Sub ZipMain_GlobalStatus(ByVal TotalFiles As Long, ByVal ProcessedFiles As Long, ByVal CompletionFiles As Integer, ByVal TotalBytes As Long, ByVal ProcessedBytes As Long, ByVal CompletionBytes As Integer)
'    ProgressBar1.Value = (ProcessedBytes * 100) / TotalBytes
End Sub

Private Sub ZipMain_Newdisk(ByVal DiskNumber As Integer)

    MsgBox ("Introduzca el disco " & DiskNumber & "...")
    
End Sub
