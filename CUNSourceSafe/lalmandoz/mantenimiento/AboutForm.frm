VERSION 5.00
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Begin VB.Form AboutForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Programa"
   ClientHeight    =   5250
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   9960
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5250
   ScaleWidth      =   9960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   2295
      Left            =   5400
      TabIndex        =   3
      Top             =   1080
      Width           =   4455
      Begin VB.Label Label4 
         Caption         =   "Servicio de Mantenimiento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   720
         TabIndex        =   6
         Top             =   1440
         Width           =   3015
      End
      Begin VB.Label Label2 
         Caption         =   "Realizado por:Salvador Linares"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   5
         Top             =   480
         Width           =   3375
      End
      Begin VB.Label Label3 
         Caption         =   "Para Cl�nica Universitaria de Pamplona"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   4
         Top             =   960
         Width           =   4215
      End
   End
   Begin VB.CommandButton AboutForm 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4680
      TabIndex        =   1
      Top             =   4680
      Width           =   1095
   End
   Begin ComCtl2.Animation Animation1 
      Height          =   4095
      Left            =   360
      TabIndex        =   0
      Top             =   240
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   7223
      _Version        =   327681
      AutoPlay        =   -1  'True
      Center          =   -1  'True
      FullWidth       =   321
      FullHeight      =   273
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "ManCUN v1.0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   19.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   6120
      TabIndex        =   2
      Top             =   240
      Width           =   3135
   End
End
Attribute VB_Name = "AboutForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub AboutForm_Click()
    Unload Me
    
End Sub

Private Sub Form_Load()
    Animation1.Open "c:\Archivos de Programa\Almacen\hb-t-r.avi"

End Sub
