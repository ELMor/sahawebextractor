VERSION 5.00
Object = "{FAEEE763-117E-101B-8933-08002B2F4F5A}#1.1#0"; "DBLIST32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form SATFForm 
   BackColor       =   &H80000004&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Factura S.A.T."
   ClientHeight    =   7950
   ClientLeft      =   1935
   ClientTop       =   1680
   ClientWidth     =   11850
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7950
   ScaleWidth      =   11850
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   45
      Top             =   0
      Width           =   11850
      _ExtentX        =   20902
      _ExtentY        =   1535
      ButtonWidth     =   1693
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   10
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "prim"
            Description     =   "primeroBoto"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntrBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Mant.S.A.T."
            Key             =   "ManSAT"
            Description     =   "ManSATBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "C.M./A.T."
            Key             =   "Maq"
            Description     =   "CMATBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "ulti"
            Description     =   "UltimoBoton"
            Object.Tag             =   ""
            ImageIndex      =   10
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid2 
      Bindings        =   "SATFForm.frx":0000
      Height          =   1095
      Left            =   6360
      TabIndex        =   44
      Top             =   960
      Width           =   4935
      ScrollBars      =   2
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowUpdate     =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      SelectTypeRow   =   1
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1746
      Columns(0).Caption=   "NAlbaran"
      Columns(0).Name =   "NAlbaranP"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "NAlbaranP"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1429
      Columns(1).Caption=   "Fecha"
      Columns(1).Name =   "FechaAlbaran"
      Columns(1).Alignment=   1
      Columns(1).CaptionAlignment=   1
      Columns(1).DataField=   "FechaAlbaran"
      Columns(1).DataType=   7
      Columns(1).FieldLen=   256
      Columns(2).Width=   4895
      Columns(2).Caption=   "SAT"
      Columns(2).Name =   "SAT"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "SAT"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   8705
      _ExtentY        =   1931
      _StockProps     =   79
      Caption         =   "Partes de Trabajo Pendientes de Facturar"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo CMBox 
      Bindings        =   "SATFForm.frx":0019
      DataField       =   "CodigoMaquina"
      DataSource      =   "SATFacturaData"
      Height          =   255
      Left            =   6240
      TabIndex        =   43
      Top             =   2880
      Width           =   1335
      DataFieldList   =   "CodigoMaquina"
      _Version        =   131078
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2302
      Columns(0).Caption=   "C�digo M�quina"
      Columns(0).Name =   "CodigoMaquina"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "CodigoMaquina"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6985
      Columns(1).Caption=   "Descripci�n"
      Columns(1).Name =   "Descripcion"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Descripcion"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   2355
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "CodigoMaquina"
   End
   Begin VB.CommandButton AsoBoton 
      Caption         =   "A&sociar"
      Height          =   375
      Left            =   4320
      TabIndex        =   42
      Top             =   1320
      Width           =   1095
   End
   Begin VB.TextBox NAABox 
      Height          =   285
      Left            =   2640
      TabIndex        =   40
      Top             =   1320
      Width           =   1335
   End
   Begin VB.TextBox SuNFSAT 
      DataField       =   "NFacturaP"
      DataSource      =   "SATFacturaData"
      Height          =   285
      Left            =   5160
      TabIndex        =   14
      Top             =   2400
      Width           =   1575
   End
   Begin VB.Frame Frame2 
      Caption         =   "TOTAL"
      Height          =   735
      Left            =   7200
      TabIndex        =   12
      Top             =   7200
      Width           =   2775
      Begin VB.TextBox TotalBox 
         DataField       =   "Total"
         DataSource      =   "SATFacturaData"
         Enabled         =   0   'False
         Height          =   285
         Left            =   720
         TabIndex        =   39
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.TextBox IVABox 
      DataField       =   "IVA"
      DataSource      =   "SATFacturaData"
      Height          =   285
      Left            =   5280
      TabIndex        =   11
      Top             =   7320
      Width           =   495
   End
   Begin VB.TextBox DtoBox 
      DataField       =   "Dto"
      DataSource      =   "SATFacturaData"
      Height          =   285
      Left            =   2520
      TabIndex        =   10
      Top             =   7320
      Width           =   495
   End
   Begin VB.Data SATData 
      Caption         =   "SAT"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   5400
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT Nombre FROM SAT ORDER BY Nombre"
      Top             =   240
      Width           =   2055
   End
   Begin MSDBCtls.DBCombo SATBox 
      Bindings        =   "SATFForm.frx":002F
      DataField       =   "SAT"
      DataSource      =   "SATFacturaData"
      Height          =   315
      Left            =   1080
      TabIndex        =   7
      Top             =   2880
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   556
      _Version        =   327681
      Style           =   2
      ListField       =   "Nombre"
      Text            =   ""
   End
   Begin SSCalendarWidgets_A.SSDateCombo FFacturaBox 
      DataField       =   "FechaFactura"
      DataSource      =   "SATFacturaData"
      Height          =   255
      Left            =   8520
      TabIndex        =   6
      Top             =   2400
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Data MaquinaData 
      Caption         =   "Maquinas"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   5400
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT CodigoMaquina, Descripcion, SAT FROM InventMaquinas"
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data SATFFacturaData 
      Caption         =   "SATFFactura"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   2760
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SATFFactura"
      Top             =   240
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Data SATFacturaData 
      Caption         =   "SATFactura"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   2760
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM SATFact WHERE NFacturaSAT<>0 ORDER BY NFacturaSAT"
      Top             =   0
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.TextBox NFSATBox 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      DataField       =   "NFacturaSAT"
      DataSource      =   "SATFacturaData"
      Enabled         =   0   'False
      Height          =   285
      Left            =   2040
      TabIndex        =   5
      Top             =   2400
      Width           =   1455
   End
   Begin VB.Data SATFFactura2Data 
      Caption         =   "SATFFactura2"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   9600
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT Linea,Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT=1"
      Top             =   120
      Visible         =   0   'False
      Width           =   2055
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3615
      Left            =   120
      TabIndex        =   15
      Top             =   3480
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   6376
      _Version        =   327681
      Tabs            =   2
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Material Utilizado"
      TabPicture(0)   =   "SATFForm.frx":0041
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "ActualizarBoton"
      Tab(0).Control(1)=   "BorrarBoton"
      Tab(0).Control(2)=   "DescuentoBox"
      Tab(0).Control(3)=   "PUnitBox"
      Tab(0).Control(4)=   "CantBox"
      Tab(0).Control(5)=   "MaterialBox"
      Tab(0).Control(6)=   "STotalCBox"
      Tab(0).Control(7)=   "A�adirBoton"
      Tab(0).Control(8)=   "IdBox"
      Tab(0).Control(9)=   "SSDBGrid1"
      Tab(0).Control(10)=   "Label14"
      Tab(0).Control(11)=   "label90"
      Tab(0).Control(12)=   "Label13"
      Tab(0).Control(13)=   "Label12"
      Tab(0).Control(14)=   "Label9"
      Tab(0).ControlCount=   15
      TabCaption(1)   =   "Desplazamiento y Dietas"
      TabPicture(1)   =   "SATFForm.frx":005D
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Label8"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Line3"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Line2"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Label5"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Label6"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "Label7"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "DietasBox"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "PUniHTBox"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "HTBox"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "STotalHTDBox"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).ControlCount=   10
      Begin VB.TextBox STotalHTDBox 
         DataField       =   "SubTotalHTD"
         DataSource      =   "SATFacturaData"
         Height          =   285
         Left            =   6960
         TabIndex        =   38
         Top             =   1800
         Width           =   1455
      End
      Begin VB.TextBox HTBox 
         DataField       =   "HorasTecnico"
         DataSource      =   "SATFacturaData"
         Height          =   285
         Left            =   3600
         TabIndex        =   27
         Top             =   1080
         Width           =   735
      End
      Begin VB.TextBox PUniHTBox 
         DataField       =   "PrecioUnitarioHT"
         DataSource      =   "SATFacturaData"
         Height          =   285
         Left            =   3600
         TabIndex        =   26
         Top             =   1560
         Width           =   1095
      End
      Begin VB.TextBox DietasBox 
         DataField       =   "GastosDesp"
         DataSource      =   "SATFacturaData"
         Height          =   285
         Left            =   3600
         TabIndex        =   25
         Top             =   2040
         Width           =   1095
      End
      Begin VB.CommandButton ActualizarBoton 
         Caption         =   "A&ctualizar"
         Height          =   375
         Left            =   -65160
         TabIndex        =   24
         Top             =   3000
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton BorrarBoton 
         Caption         =   "&Borrar"
         Height          =   375
         Left            =   -66600
         TabIndex        =   23
         Top             =   3000
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.TextBox DescuentoBox 
         Height          =   285
         Left            =   -67440
         TabIndex        =   22
         Text            =   "0"
         Top             =   3000
         Width           =   495
      End
      Begin VB.TextBox PUnitBox 
         Height          =   285
         Left            =   -68880
         TabIndex        =   21
         Text            =   "0"
         Top             =   3000
         Width           =   1215
      End
      Begin VB.TextBox CantBox 
         Height          =   285
         Left            =   -69960
         TabIndex        =   20
         Text            =   "0"
         Top             =   3000
         Width           =   855
      End
      Begin VB.TextBox MaterialBox 
         Height          =   285
         Left            =   -74880
         TabIndex        =   19
         Top             =   3000
         Width           =   4695
      End
      Begin VB.TextBox STotalCBox 
         DataField       =   "SubTotalC"
         DataSource      =   "SATFacturaData"
         Enabled         =   0   'False
         Height          =   285
         Left            =   -65400
         TabIndex        =   18
         Top             =   2400
         Width           =   1215
      End
      Begin VB.CommandButton A�adirBoton 
         Caption         =   "&A�adir"
         Height          =   375
         Left            =   -65880
         TabIndex        =   17
         Top             =   3000
         Width           =   1095
      End
      Begin VB.TextBox IdBox 
         Height          =   285
         Left            =   -72960
         TabIndex        =   16
         Top             =   2640
         Visible         =   0   'False
         Width           =   495
      End
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Bindings        =   "SATFForm.frx":0079
         Height          =   1815
         Left            =   -74880
         TabIndex        =   28
         Top             =   600
         Width           =   10935
         ScrollBars      =   2
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   12330
         Columns(0).Caption=   "Art�culo"
         Columns(0).Name =   "Articulo"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "Articulo"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1376
         Columns(1).Caption=   "Cantidad"
         Columns(1).Name =   "Cantidad"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Cantidad"
         Columns(1).DataType=   3
         Columns(1).FieldLen=   256
         Columns(2).Width=   1561
         Columns(2).Caption=   "Pr.Unitario"
         Columns(2).Name =   "PrecioUnit"
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "PrecioUnit"
         Columns(2).DataType=   6
         Columns(2).FieldLen=   256
         Columns(3).Width=   1693
         Columns(3).Caption=   "Descuento"
         Columns(3).Name =   "Descuento"
         Columns(3).CaptionAlignment=   0
         Columns(3).DataField=   "Descuento"
         Columns(3).DataType=   3
         Columns(3).FieldLen=   256
         Columns(4).Width=   1270
         Columns(4).Caption=   "Parcial"
         Columns(4).Name =   "PParcial"
         Columns(4).CaptionAlignment=   0
         Columns(4).DataField=   "PParcial"
         Columns(4).DataType=   6
         Columns(4).FieldLen=   256
         UseDefaults     =   -1  'True
         _ExtentX        =   19288
         _ExtentY        =   3201
         _StockProps     =   79
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label7 
         Caption         =   "Gastos desplazamiento y dietas:"
         Height          =   255
         Left            =   1200
         TabIndex        =   36
         Top             =   2040
         Width           =   2295
      End
      Begin VB.Label Label6 
         Caption         =   "Precio Unitario Hora T�c.:"
         Height          =   255
         Left            =   1680
         TabIndex        =   35
         Top             =   1560
         Width           =   1935
      End
      Begin VB.Label Label5 
         Caption         =   "Horas T�cnico:"
         Height          =   255
         Left            =   2520
         TabIndex        =   34
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label14 
         Caption         =   "Dto"
         Height          =   255
         Left            =   -67440
         TabIndex        =   33
         Top             =   2760
         Width           =   375
      End
      Begin VB.Label label90 
         Caption         =   "P.Unitario"
         Height          =   255
         Left            =   -68880
         TabIndex        =   32
         Top             =   2760
         Width           =   855
      End
      Begin VB.Label Label13 
         Caption         =   "Cantidad"
         Height          =   255
         Left            =   -69960
         TabIndex        =   31
         Top             =   2760
         Width           =   735
      End
      Begin VB.Label Label12 
         Caption         =   "Material"
         Height          =   255
         Left            =   -74760
         TabIndex        =   30
         Top             =   2760
         Width           =   735
      End
      Begin VB.Label Label9 
         Caption         =   "SubTotal (material):"
         Height          =   255
         Left            =   -66840
         TabIndex        =   29
         Top             =   2400
         Width           =   1455
      End
      Begin VB.Line Line2 
         BorderWidth     =   3
         X1              =   5280
         X2              =   5280
         Y1              =   1080
         Y2              =   2400
      End
      Begin VB.Line Line3 
         X1              =   5280
         X2              =   8400
         Y1              =   1800
         Y2              =   1800
      End
      Begin VB.Label Label8 
         Caption         =   "SubTotal (Horas T�c. y Dietas):"
         Height          =   255
         Left            =   5520
         TabIndex        =   37
         Top             =   1560
         Width           =   2295
      End
   End
   Begin VB.Data SATAAlbaranData 
      Caption         =   "SATAAlbaran"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   7200
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SATAAlbaran"
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Data SATAlbaranData 
      Caption         =   "SATAlbaran"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   7200
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM SATAlbaran WHERE Facturado=False AND NAlbaranSAT<>0 ORDER BY SAT,FechaAlbaran"
      Top             =   240
      Visible         =   0   'False
      Width           =   2175
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   10
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATFForm.frx":0094
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATFForm.frx":03AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATFForm.frx":06C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATFForm.frx":09E2
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATFForm.frx":0CFC
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATFForm.frx":1016
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATFForm.frx":1330
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATFForm.frx":164A
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATFForm.frx":1964
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "SATFForm.frx":1C7E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label16 
      Caption         =   "N� Albar�n asociado:"
      Height          =   255
      Left            =   960
      TabIndex        =   41
      Top             =   1320
      Width           =   1575
   End
   Begin VB.Line Line4 
      X1              =   120
      X2              =   11640
      Y1              =   2160
      Y2              =   2160
   End
   Begin VB.Label Label15 
      Caption         =   "N�Factura SAT:"
      Height          =   255
      Left            =   3840
      TabIndex        =   13
      Top             =   2400
      Width           =   1215
   End
   Begin VB.Label Label11 
      Caption         =   "Descuento (%):"
      Height          =   255
      Left            =   1320
      TabIndex        =   9
      Top             =   7320
      Width           =   1095
   End
   Begin VB.Line Line1 
      X1              =   7560
      X2              =   8040
      Y1              =   3000
      Y2              =   3000
   End
   Begin VB.Label ModeloBox 
      BorderStyle     =   1  'Fixed Single
      DataField       =   "Descripcion"
      DataSource      =   "MaquinaData"
      Height          =   255
      Left            =   8040
      TabIndex        =   8
      Top             =   2880
      Width           =   3495
   End
   Begin VB.Label Label10 
      Caption         =   "I.V.A. (%)"
      Height          =   255
      Left            =   4440
      TabIndex        =   4
      Top             =   7320
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "C�digo M�quina / �rea Trabajo:"
      Height          =   255
      Left            =   3840
      TabIndex        =   3
      Top             =   2880
      Width           =   2295
   End
   Begin VB.Label Label3 
      Caption         =   "S.A.T."
      Height          =   255
      Left            =   480
      TabIndex        =   2
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "Fecha Factura:"
      Height          =   255
      Left            =   7320
      TabIndex        =   1
      Top             =   2400
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "N� Factura:"
      Height          =   255
      Left            =   1080
      TabIndex        =   0
      Top             =   2400
      Width           =   855
   End
   Begin VB.Menu prg 
      Caption         =   "&Programa"
      Begin VB.Menu exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu reg 
      Caption         =   "&Registros"
      Begin VB.Menu ira 
         Caption         =   "Ir a"
         Shortcut        =   {F1}
      End
      Begin VB.Menu first 
         Caption         =   "Primero"
      End
      Begin VB.Menu ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu sigui 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu last 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu add 
         Caption         =   "A�adir"
      End
      Begin VB.Menu eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu conf 
      Caption         =   "&Configuraci�n"
      Begin VB.Menu sate 
         Caption         =   "S.A.T."
      End
      Begin VB.Menu at 
         Caption         =   "�reas de Trabajo"
      End
   End
   Begin VB.Menu listard 
      Caption         =   "&Listados"
      Begin VB.Menu lista 
         Caption         =   "Listado"
      End
   End
End
Attribute VB_Name = "SATFForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub ActualizarBoton_Click()
    
    Dim id As Integer, nf As Long, na As Integer, Ant As Long
On Error GoTo msgError
    id = IdBox.Text
    nf = NFSATBox.Text
        
     With SATFFacturaData
        .Recordset.FindFirst ("NFacturaSAT=" & nf & " AND Linea=" & id)
        .Recordset.Edit
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("Articulo").Value = MaterialBox.Text
        .Recordset.Fields("Cantidad").Value = CantBox.Text
        .Recordset.Fields("PrecioUnit").Value = PUnitBox.Text
        .Recordset.Fields("Descuento").Value = DescuentoBox.Text
        .Recordset.Fields("PParcial").Value = (Val(PUnitBox.Text) * Val(CantBox.Text)) * (1 - (Val(DescuentoBox.Text) / 100))
        .UpdateRecord
    
    End With
        
    SATFFactura2Data.RecordSource = "SELECT Linea, Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT=" & NFSATBox.Text & " ORDER BY Linea"
    SATFFactura2Data.Refresh
        
    With SATFacturaData
        .Recordset.Edit
        .Recordset.Fields("SubTotalC").Value = CaTotalSATFac(nf)
        .Recordset.Fields("Total").Value = ((.Recordset.Fields("SubTotalHTD").Value) + (.Recordset.Fields("SubTotalC").Value)) * (1 - (Val(.Recordset.Fields("Dto").Value) / 100)) * (1 + (Val(.Recordset.Fields("IVA").Value) / 100))
        .Recordset.Update
    End With
    
    A�adirBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True
    
    id = ClearFields()
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub add_Click()
    Dim nada As Integer
    nada = OtraSATFactura()

End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()
End Sub

Private Sub A�adirBoton_Click()
   
    Dim id As Integer, Total As Integer, Cantidad As Integer, PrecU As Integer, np As Integer, na As Integer, Ant As Integer
    Dim sub1 As Double, sub2 As Double
On Error GoTo msgError
    With SATFFactura2Data
    
        If .Recordset.BOF = False And .Recordset.EOF = False Then
            .Recordset.MoveLast
            id = .Recordset.Fields("Linea").Value
        Else
            id = 0
        End If
    
    End With
    
    
    With SATFFacturaData
               
        id = id + 1
        
        .Recordset.AddNew
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("NFacturaSAT").Value = NFSATBox.Text
        .Recordset.Fields("Articulo").Value = MaterialBox.Text
        .Recordset.Fields("Cantidad").Value = CantBox.Text
        .Recordset.Fields("PrecioUnit").Value = PUnitBox.Text
        .Recordset.Fields("Descuento").Value = DescuentoBox.Text
        .Recordset.Fields("PParcial").Value = (Val(PUnitBox.Text) * Val(CantBox.Text)) * (1 - (Val(DescuentoBox.Text) / 100))
        .Recordset.Fields("NOrden").Value = 0
        .Recordset.Fields("NAlbaranSAT").Value = 0
        .UpdateRecord
        
    End With
    
    If STotalHTDBox.Text = "" Then
        STotalHTDBox.Text = "0"
    End If
    
    With SATFacturaData
        .Recordset.Edit
        .Recordset.Fields("SubTotalC").Value = CaTotalSATFac(Val(NFSATBox.Text))
        sub1 = .Recordset.Fields("SubTotalC").Value
        sub2 = CDbl(STotalHTDBox.Text)
        TotalBox.Text = (sub1 + sub2) * (1 - (Val(.Recordset.Fields("Dto").Value) / 100)) * (1 + (Val(.Recordset.Fields("IVA").Value) / 100))
        .Recordset.Update
    End With
    
    SATFFactura2Data.RecordSource = "SELECT Linea, Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT=" & NFSATBox.Text & " ORDER BY Linea"
    SATFFactura2Data.Refresh
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub AsoBoton_Click()
    
    Dim id As Integer, sub1 As Double, sub2 As Double
On Error GoTo msgError
    If NAABox.Text <> "" Then
        
        With SATFFactura2Data
            If .Recordset.EOF = False Then
                .Recordset.MoveLast
                id = .Recordset.Fields("Linea").Value
            Else
                id = 0
            End If
        End With
        
        SATAlbaranData.Recordset.FindFirst ("NAlbaranSAT=" & NAABox.Text)
        
        If SATAlbaranData.Recordset.NoMatch = False Then
            
            SATAAlbaranData.Recordset.FindFirst ("NAlbaranSAT=" & NAABox.Text)
            
            If id <> 0 And Val(CMBox.Text) <> SATAlbaranData.Recordset.Fields("CodigoMaquina").Value Then
                MsgBox ("Albar�n no perteneciente al SAT de la factura")
            Else
                CMBox.Text = SATAlbaranData.Recordset.Fields("CodigoMaquina").Value
                SATBox.Text = SATAlbaranData.Recordset.Fields("SAT").Value
                
                While SATAAlbaranData.Recordset.NoMatch = False
               
                    With SATFFacturaData
                                                
                        id = id + 1
        
                        .Recordset.AddNew
                        .Recordset.Fields("Linea").Value = id
                        .Recordset.Fields("NFacturaSAT").Value = NFSATBox.Text
                        .Recordset.Fields("Articulo").Value = SATAAlbaranData.Recordset.Fields("Articulo").Value
                        .Recordset.Fields("Cantidad").Value = SATAAlbaranData.Recordset.Fields("Cantidad").Value
                        .Recordset.Fields("PrecioUnit").Value = SATAAlbaranData.Recordset.Fields("PrecioUnit").Value
                        .Recordset.Fields("Descuento").Value = SATAAlbaranData.Recordset.Fields("Descuento").Value
                        .Recordset.Fields("PParcial").Value = SATAAlbaranData.Recordset.Fields("PParcial").Value
                        .Recordset.Fields("NAlbaranSAT").Value = NAABox.Text
                        .UpdateRecord
        
                    End With
    
                    With SATAAlbaranData
                        .Recordset.FindNext ("NAlbaranSAT=" & NAABox.Text)
                    End With
               
                Wend
                
                With SATAlbaranData
                    .Recordset.Edit
                    .Recordset.Fields("Facturado") = True
                    .Recordset.Update
                End With
                
                SATFFactura2Data.RecordSource = "SELECT Linea, Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT=" & NFSATBox.Text & " ORDER BY Linea"
                SATFFactura2Data.Refresh

                With SATFacturaData
                    .Recordset.Edit
                    .Recordset.Fields("HorasTecnico").Value = CDbl(HTBox.Text) + (SATAlbaranData.Recordset.Fields("HorasTecnico").Value)
                    .Recordset.Fields("PrecioUnitarioHT").Value = SATAlbaranData.Recordset.Fields("PrecioUnitarioHT").Value
                    .Recordset.Fields("GastosDesp").Value = CDbl(DietasBox.Text) + SATAlbaranData.Recordset.Fields("GastosDesp").Value
                    .Recordset.Fields("SubTotalC").Value = CaTotalSATFac(CDbl(NFSATBox.Text))
                    sub1 = .Recordset.Fields("SubTotalC").Value
                    sub2 = CDbl(STotalHTDBox.Text)
                    TotalBox.Text = (sub1 + sub2) * (1 - (Val(.Recordset.Fields("Dto").Value) / 100)) * (1 + (Val(.Recordset.Fields("IVA").Value) / 100))
                    .Recordset.Update
                End With
            
            End If
        
        End If
                       
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub at_Click()
    MantInvForm.Show modal
    
End Sub

Private Sub BorrarBoton_Click()
    Dim id As Integer, nf As Long, na As Long, np As Long, ca As Long
On Error GoTo msgError
    id = IdBox.Text
    nf = NFSATBox.Text
     
    With SATFFacturaData
        .Recordset.FindFirst ("NFacturaSAT=" & nf & " AND Linea=" & id)
        na = .Recordset.Fields("NFacturaSAT").Value
        .Recordset.Delete
        .Refresh
    End With
    
    With SATFacturaData
        .Recordset.Edit
        .Recordset.Fields("SubTotalC").Value = CaTotalSATFac(Val(NFSATBox.Text))
        .Recordset.Fields("Total").Value = ((.Recordset.Fields("SubTotalHTD").Value) + (.Recordset.Fields("SubTotalC").Value)) * (1 - (Val(.Recordset.Fields("Dto").Value) / 100)) * (1 + (Val(.Recordset.Fields("IVA").Value) / 100))
        .Recordset.Update
    End With
    
    A�adirBoton.Visible = True
    BorrarBoton.Visible = False
    ActualizarBoton.Visible = False
    SSDBGrid1.Enabled = True
    
    SATFFactura2Data.RecordSource = "SELECT Linea, Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT=" & NFSATBox.Text & " ORDER BY Linea"
    SATFFactura2Data.Refresh
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Function Anterior()
    
    Dim nf As Integer
On Error GoTo msgError
    Flag = True
    
    With SATFacturaData
        .Recordset.MovePrevious
        If .Recordset.BOF = True Then
            .Recordset.MoveNext
        End If
        nf = .Recordset.Fields("NFacturaSAT").Value
    End With
                
    SATFFactura2Data.RecordSource = "SELECT Linea, Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT=" & nf & " ORDER BY Linea"
    SATFFactura2Data.Refresh
    
    Flag = False
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Siguiente()
   
    Dim nf As Integer
On Error GoTo msgError
    Flag = True
    
    With SATFacturaData
        .Recordset.MoveNext
        If .Recordset.EOF = True Then
            .Recordset.MovePrevious
        End If
        nf = .Recordset.Fields("NFacturaSAT").Value
    End With
    
    SATFFactura2Data.RecordSource = "SELECT Linea, Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT=" & nf & " ORDER BY Linea"
    SATFFactura2Data.Refresh
    
    Flag = False
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Sub CMBox_Change()
On Error GoTo msgError
    If CMBox.Text <> "" Then
        MaquinaData.Recordset.FindFirst ("CodigoMaquina=" & CMBox.Text)
        If MaquinaData.Recordset.NoMatch = True Then
            ModeloBox.Caption = "No existente"
        Else
        If MaquinaData.Recordset.Fields("SAT").Value <> "" Then
            SATBox.Text = MaquinaData.Recordset.Fields("SAT").Value
        End If
        End If
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CMBox_CloseUp()
On Error GoTo msgError
    MaquinaData.Recordset.FindFirst ("CodigoMaquina=" & CMBox.Text)
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub DietasBox_Change()

    Dim res As Long

    res = ActualSub()

End Sub

Private Sub DtoBox_Change()

    Dim res As Long

    res = CambioIVAD()

End Sub

Private Sub eraser_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar la factura SAT?", vbYesNo)
    If nada = vbYes Then
        SATFacturaData.Recordset.Delete
        nada = Anterior()
    End If
    
End Sub

Private Sub exit_Click()
    Unload Me
End Sub

Private Sub first_Click()
    Dim nada As Integer
    nada = Primero()
End Sub

Private Sub Form_Load()
    Flag = True
    
End Sub

Private Sub HTBox_Change()
    
    Dim res As Long

    res = ActualSub()

End Sub

Private Sub ira_Click()
    
    Dim nada As Integer
    
    Set Objeto = SATFForm.SATFacturaData
    Set Objeto2 = SATFForm.SATFFactura2Data
    
    Selct = True
    
    Display(0) = "NFacturaSAT"
    DataCarac = "SELECT Linea, Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT="
    DataCarac2 = " ORDER BY Linea"
    
    IraForm.Show modal
    
End Sub

Private Sub IVABox_Change()
    
    Dim res As Long

    res = CambioIVAD()

End Sub

Private Sub last_Click()
    Dim nada As Integer
    nada = Ultimo()

End Sub

Private Sub lista_Click()
    Dim nada As Integer
    nada = ListaFac()
End Sub

Private Sub PUniHTBox_Change()
    
    Dim res As Long

    res = ActualSub()

End Sub

Private Sub sate_Click()
    SATForm.Show modal
    SATForm.IndiceBox.Text = "3"

End Sub

Private Sub sigui_Click()
    Dim nada As Integer
    nada = Siguiente()

End Sub

Private Sub SSDBGrid1_DblClick()
    
    Dim id As Integer
    
    With SATFFactura2Data
        id = .Recordset.Fields("Linea").Value
        IdBox.Text = id
        MaterialBox.Text = .Recordset.Fields("Articulo").Value
        CantBox.Text = .Recordset.Fields("Cantidad").Value
        If .Recordset.Fields("PrecioUnit").Value <> 0 Then
            PUnitBox.Text = .Recordset.Fields("PrecioUnit").Value
        Else
            PUnitBox.Text = "0"
        End If
        
        If .Recordset.Fields("Descuento").Value <> 0 Then
            DescuentoBox.Text = .Recordset.Fields("Descuento").Value
        Else
            DescuentoBox.Text = "0"
        End If
        
    End With
    
    A�adirBoton.Visible = False
    BorrarBoton.Visible = True
    ActualizarBoton.Visible = True
    SSDBGrid1.Enabled = False
        
End Sub


Private Sub SSDBGrid2_DblClick()
    NAABox.Text = SATAlbaranData.Recordset.Fields("NAlbaranSAT").Value
End Sub

Private Sub STotalCBox_Change()
    
    Dim res As Integer
    
    res = ActualSub()
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        Unload Me
    Case "Add"
        nada = OtraSATFactura()
    Case "Lista"
        nada = ListaFac()
    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar la factura SAT?", vbYesNo)
        If nada = vbYes Then
            SATFacturaData.Recordset.Delete
            nada = Anterior()
        End If
    
    Case "ManSAT"
        SATForm.Show modal
        SATForm.IndiceBox.Text = "3"
        
    Case "Maq"
        MantInvForm.Show modal
    Case "ulti"
        nada = Ultimo()
    Case "prim"
        nada = Primero()
    
    
    End Select
    

End Sub

Private Function OtraSATFactura() As Integer

    Dim num As Integer
On Error GoTo msgError
    Flag = True
    
    num = 1 + UltFactura()
    
    With SATFacturaData
        .Recordset.MoveLast
        .Recordset.AddNew
        .Recordset.Fields("NFacturaSAT").Value = num
    End With

    SATBox.Text = "No Especificada"
    CMBox.Text = 0
    IVABox.Text = 16
    DtoBox.Text = 0
    TotalBox.Text = 0
    
    SATFacturaData.Recordset.Fields("FechaFactura") = Date
        
    SATFacturaData.Recordset.Update
    SATFacturaData.Recordset.MoveLast
    
    SATFFactura2Data.RecordSource = "SELECT Linea, Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT=" & num & " ORDER BY Linea"
    SATFFactura2Data.Refresh

    Flag = False
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function


Private Function ClearFields()

    MaterialBox.Text = ""
    CantBox.Text = ""
    PUnitBox.Text = ""
    DescuentoBox.Text = ""
    
End Function


Private Function ActualSub()
    
    Dim a1 As Double, a2 As Double, a3 As Double, Ant As Double, ant2 As Double, dto As Double, iva As Double
On Error GoTo msgError
    If Flag = True Then
        Exit Function
    End If
    
    If SATFacturaData.Recordset.EOF = True Or SATFacturaData.Recordset.BOF = True Then
        Exit Function
    End If
    
    
    If HTBox.Text <> "" Then
        a1 = CDbl(HTBox.Text)
    End If
    If PUniHTBox.Text <> "" Then
        a2 = CDbl(PUniHTBox.Text)
    End If
    If PUniHTBox.Text <> "" Then
        a3 = CDbl(DietasBox.Text)
    End If
    
    With SATFacturaData
        If STotalHTDBox.Text = "" Then
            STotalHTDBox.Text = "0"
        End If
        If STotalCBox.Text = "" Then
            STotalCBox.Text = "0"
        End If
        Ant = CDbl(STotalHTDBox.Text)
        ant2 = CDbl(STotalCBox.Text)
        STotalHTDBox.Text = (a1 * a2) + a3
        dto = (1 - (Val(DtoBox.Text) / 100))
        iva = (1 + (Val(IVABox.Text) / 100))
        TotalBox.Text = CDbl(TotalBox.Text) - ((Ant + ant2) * (dto * iva)) + ((CDbl(STotalHTDBox.Text) + CDbl(STotalCBox.Text)) * (dto * iva))
        
    End With
    
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function CambioIVAD() As Integer

    Dim iva As Double, dto As Double, sub1 As Double, sub2 As Double
On Error GoTo msgError
    If Flag = True Then
        Exit Function
    End If
    
    iva = 1 + (Val(IVABox.Text) / 100)
    dto = 1 - (Val(DtoBox.Text) / 100)
    
    If STotalHTDBox.Text = "" Then
        STotalHTDBox.Text = "0"
    End If
    If STotalCBox.Text = "" Then
        STotalCBox.Text = "0"
    End If
    
    sub1 = CDbl(STotalHTDBox.Text)
    sub2 = CDbl(STotalCBox.Text)
            
    TotalBox.Text = (sub1 + sub2) * iva * dto
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function ListaFac()

    Dim nada As Integer
    
    nada = NuevaBusq()
    Display(0) = "NFacturaSAT"
    Display(1) = "NFacturaP"
    Display(2) = "FechaFactura"
    Display(3) = "SAT"
    Display(4) = "CodigoMaquina"
        
    TypeCampos(0) = 1
    TypeCampos(1) = 1
    TypeCampos(2) = 2
    TypeCampos(3) = 3
    TypeCampos(4) = 3
    TypeCampos(5) = 0
    TypeCampos(6) = 0
    TypeCampos(7) = 0
        
    For nada = 0 To 4
        campos(nada) = Display(nada)
    Next
        
    Lists(4, 0) = "CodigoMaquina, Descripcion"
    Lists(4, 1) = "CodigoMaquina"
    Lists(4, 2) = "InventMaquinas"
    Lists(3, 0) = "Nombre"
    Lists(3, 1) = "Nombre"
    Lists(3, 2) = "SAT"
        
    FRO = "SATFact"
    SELE = campos(0) & "," & campos(1) & "," & campos(2) & "," & campos(3) & "," & campos(4) & ",Total"
    WHER = ""
    ORD = "NFacturaSAT"
        
    Set Objeto = SATFForm.SATFacturaData
    Set Objeto2 = SATFForm.SATFFactura2Data
    
    Selct = True
        
    DataCarac = "SELECT Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT="
    
    nada = Listado()
    
End Function
    
Private Function Primero()
    
    Dim nada As Long
On Error GoTo msgError
    Flag = True
    
    SATFacturaData.Recordset.MoveFirst
    
    nada = SATFacturaData.Recordset.Fields("NFacturaSAT").Value

    SATFFactura2Data.RecordSource = "SELECT Linea, Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT=" & nada & " ORDER BY Linea"
    SATFFactura2Data.Refresh

    Flag = False
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Ultimo()
    
    Dim nada As Long
On Error GoTo msgError
    Flag = True
    
    SATFacturaData.Recordset.MoveLast
    
    nada = SATFacturaData.Recordset.Fields("NFacturaSAT").Value

    SATFFactura2Data.RecordSource = "SELECT Linea, Articulo, Cantidad, PrecioUnit, Descuento, PParcial FROM SATFFactura WHERE NFacturaSAT=" & nada & " ORDER BY Linea"
    SATFFactura2Data.Refresh
    
    Flag = False
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

