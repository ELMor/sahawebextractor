# Microsoft Visual C++ generated build script - Do not modify

PROJ = TCP4WLIB
DEBUG = 0
PROGTYPE = 4
CALLER = 
ARGS = 
DLLS = 
D_RCDEFINES = -d_DEBUG
R_RCDEFINES = -dNDEBUG
ORIGIN = MSVC
ORIGIN_VER = 1.00
PROJPATH = F:\WORK\MOUNT16\TCP4U.331\TCP4W16L\
USEMFC = 0
CC = cl
CPP = cl
CXX = cl
CCREATEPCHFLAG = 
CPPCREATEPCHFLAG = 
CUSEPCHFLAG = 
CPPUSEPCHFLAG = 
FIRSTC = HTP4_ERR.C  
FIRSTCPP =             
RC = rc
CFLAGS_D_LIB = /nologo /G2 /W3 /Z7 /AL /Od /D "_DEBUG" /GA -I../Include
CFLAGS_R_LIB = /nologo /Gs /G2 /W3 /AL /O2 /D "NDEBUG" /GA -I../Include /FR
RCFLAGS = /nologo
RESFLAGS = /nologo
RUNFLAGS = 
OBJS_EXT = 
LIBS_EXT = 
!if "$(DEBUG)" == "1"
CFLAGS = $(CFLAGS_D_LIB)
LFLAGS = 
LIBS = 
MAPFILE = nul
RCDEFINES = $(D_RCDEFINES)
!else
CFLAGS = $(CFLAGS_R_LIB)
LFLAGS = 
LIBS = 
MAPFILE = nul
RCDEFINES = $(R_RCDEFINES)
!endif
!if [if exist MSVC.BND del MSVC.BND]
!endif
SBRS = HTP4_ERR.SBR \
		HTP4_URL.SBR \
		HTTP4U.SBR \
		SKT4U.SBR \
		TCP4_ERR.SBR \
		TCP4_LOG.SBR \
		TCP4U.SBR \
		TCP4U_EX.SBR \
		TN4U.SBR \
		TN_PROTO.SBR \
		UDP4U.SBR \
		SMTP4U.SBR \
		UTIL.SBR


HTP4_ERR_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


HTP4_URL_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


HTTP4U_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


SKT4U_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


TCP4_ERR_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


TCP4_LOG_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


TCP4U_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


TCP4U_EX_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


TN4U_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


TN_PROTO_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


UDP4U_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


SMTP4U_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


UTIL_DEP = f:\work\mount16\tcp4u.331\src\build.h \
	c:\apps\devapps\msvc16\include\winsock.h \
	sys/ioctl.h \
	sys/socket.h \
	netinet/in.h \
	arpa/inet.h \
	sys/select.h \
	sys/sockio.h \
	f:\work\mount16\tcp4u.331\src\port.h \
	f:\work\mount16\tcp4u.331\src\skt4u.h \
	f:\work\mount16\tcp4u.331\src\dimens.h


all:	$(PROJ).LIB $(PROJ).BSC

HTP4_ERR.OBJ:	..\SRC\HTP4_ERR.C $(HTP4_ERR_DEP)
	$(CC) $(CFLAGS) $(CCREATEPCHFLAG) /c ..\SRC\HTP4_ERR.C

HTP4_URL.OBJ:	..\SRC\HTP4_URL.C $(HTP4_URL_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\HTP4_URL.C

HTTP4U.OBJ:	..\SRC\HTTP4U.C $(HTTP4U_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\HTTP4U.C

SKT4U.OBJ:	..\SRC\SKT4U.C $(SKT4U_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\SKT4U.C

TCP4_ERR.OBJ:	..\SRC\TCP4_ERR.C $(TCP4_ERR_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\TCP4_ERR.C

TCP4_LOG.OBJ:	..\SRC\TCP4_LOG.C $(TCP4_LOG_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\TCP4_LOG.C

TCP4U.OBJ:	..\SRC\TCP4U.C $(TCP4U_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\TCP4U.C

TCP4U_EX.OBJ:	..\SRC\TCP4U_EX.C $(TCP4U_EX_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\TCP4U_EX.C

TN4U.OBJ:	..\SRC\TN4U.C $(TN4U_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\TN4U.C

TN_PROTO.OBJ:	..\SRC\TN_PROTO.C $(TN_PROTO_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\TN_PROTO.C

UDP4U.OBJ:	..\SRC\UDP4U.C $(UDP4U_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\UDP4U.C

SMTP4U.OBJ:	..\SRC\SMTP4U.C $(SMTP4U_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\SMTP4U.C

UTIL.OBJ:	..\SRC\UTIL.C $(UTIL_DEP)
	$(CC) $(CFLAGS) $(CUSEPCHFLAG) /c ..\SRC\UTIL.C

$(PROJ).LIB::	HTP4_ERR.OBJ HTP4_URL.OBJ HTTP4U.OBJ SKT4U.OBJ TCP4_ERR.OBJ TCP4_LOG.OBJ \
	TCP4U.OBJ TCP4U_EX.OBJ TN4U.OBJ TN_PROTO.OBJ UDP4U.OBJ SMTP4U.OBJ UTIL.OBJ $(OBJS_EXT)
	echo >NUL @<<$(PROJ).CRF
$@ /PAGESIZE:64
y
+HTP4_ERR.OBJ &
+HTP4_URL.OBJ &
+HTTP4U.OBJ &
+SKT4U.OBJ &
+TCP4_ERR.OBJ &
+TCP4_LOG.OBJ &
+TCP4U.OBJ &
+TCP4U_EX.OBJ &
+TN4U.OBJ &
+TN_PROTO.OBJ &
+UDP4U.OBJ &
+SMTP4U.OBJ &
+UTIL.OBJ &
;
<<
	if exist $@ del $@
	lib @$(PROJ).CRF

$(PROJ).BSC: $(SBRS)
	bscmake @<<
/o$@ $(SBRS)
<<
