VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.UserControl AxCIE9 
   Alignable       =   -1  'True
   ClientHeight    =   4965
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5325
   ScaleHeight     =   4965
   ScaleWidth      =   5325
   ToolboxBitmap   =   "AxCIE9.ctx":0000
   Begin VB.Frame frm1 
      Caption         =   "Asistente para la Codificaci�n de Diagn�sticos CIE-9-CM"
      Height          =   4935
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5295
      Begin TabDlg.SSTab SSTab1 
         Height          =   3645
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   5055
         _ExtentX        =   8916
         _ExtentY        =   6429
         _Version        =   327681
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   520
         TabCaption(0)   =   "Asistente para diagn�sticos"
         TabPicture(0)   =   "AxCIE9.ctx":0312
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label4(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "tv(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtDiag"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Action(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtNMatch"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "Indice"
         TabPicture(1)   =   "AxCIE9.ctx":032E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "chkSubCla"
         Tab(1).Control(1)=   "chkPreAli"
         Tab(1).Control(2)=   "tv(1)"
         Tab(1).Control(3)=   "txtNewDiag"
         Tab(1).Control(4)=   "Action(3)"
         Tab(1).Control(5)=   "Action(1)"
         Tab(1).Control(6)=   "txtCodRef"
         Tab(1).Control(7)=   "Label3"
         Tab(1).Control(8)=   "Label1"
         Tab(1).ControlCount=   9
         TabCaption(2)   =   "Diccionario"
         TabPicture(2)   =   "AxCIE9.ctx":034A
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Action(4)"
         Tab(2).Control(1)=   "txtSin"
         Tab(2).Control(2)=   "chkSin"
         Tab(2).Control(3)=   "Action(2)"
         Tab(2).Control(4)=   "txtPal"
         Tab(2).Control(5)=   "tv(2)"
         Tab(2).Control(6)=   "Label2(1)"
         Tab(2).Control(7)=   "Label2(0)"
         Tab(2).ControlCount=   8
         TabCaption(3)   =   "Conexi�n"
         TabPicture(3)   =   "AxCIE9.ctx":0366
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "Label4(0)"
         Tab(3).Control(1)=   "Label4(1)"
         Tab(3).Control(2)=   "Label4(5)"
         Tab(3).Control(3)=   "Label4(6)"
         Tab(3).Control(4)=   "txtHost"
         Tab(3).Control(5)=   "txtPass"
         Tab(3).Control(6)=   "txtUser"
         Tab(3).Control(7)=   "txtPort"
         Tab(3).ControlCount=   8
         Begin VB.CheckBox chkSubCla 
            Caption         =   "Mostrar subclases"
            Height          =   255
            Left            =   -74880
            TabIndex        =   32
            Top             =   480
            Width           =   1695
         End
         Begin VB.CheckBox chkPreAli 
            Caption         =   "Presentar alias"
            Height          =   195
            Left            =   -74880
            TabIndex        =   31
            Top             =   750
            Width           =   1335
         End
         Begin ComctlLib.TreeView tv 
            Height          =   1815
            Index           =   1
            Left            =   -74880
            TabIndex        =   26
            Top             =   1680
            Width           =   4815
            _ExtentX        =   8493
            _ExtentY        =   3201
            _Version        =   327682
            Indentation     =   266
            Style           =   7
            Appearance      =   1
         End
         Begin VB.TextBox txtPort 
            Height          =   285
            Left            =   -71400
            TabIndex        =   25
            Text            =   "7100"
            Top             =   960
            Width           =   615
         End
         Begin VB.CommandButton Action 
            Caption         =   "Introducir"
            Height          =   255
            Index           =   4
            Left            =   -70920
            TabIndex        =   24
            Top             =   840
            Width           =   855
         End
         Begin VB.TextBox txtNewDiag 
            Height          =   285
            Left            =   -74880
            MultiLine       =   -1  'True
            TabIndex        =   23
            Top             =   1320
            Width           =   3855
         End
         Begin VB.CommandButton Action 
            Caption         =   "Introducir"
            Height          =   255
            Index           =   3
            Left            =   -70920
            TabIndex        =   22
            Top             =   1320
            Width           =   855
         End
         Begin VB.TextBox txtSin 
            Height          =   285
            Left            =   -73440
            TabIndex        =   20
            Top             =   840
            Width           =   1215
         End
         Begin VB.TextBox txtUser 
            Height          =   285
            Left            =   -73800
            TabIndex        =   17
            Text            =   "Default"
            Top             =   600
            Width           =   1455
         End
         Begin VB.TextBox txtPass 
            Height          =   285
            IMEMode         =   3  'DISABLE
            Left            =   -71400
            PasswordChar    =   "*"
            TabIndex        =   16
            Top             =   600
            Width           =   615
         End
         Begin VB.CheckBox chkSin 
            Caption         =   "Diag.?"
            Height          =   255
            Left            =   -72120
            TabIndex        =   15
            Top             =   480
            Width           =   1095
         End
         Begin VB.TextBox txtNMatch 
            Height          =   285
            Left            =   4560
            TabIndex        =   13
            Text            =   "8"
            Top             =   480
            Width           =   375
         End
         Begin VB.TextBox txtHost 
            Height          =   285
            Left            =   -73800
            TabIndex        =   11
            Text            =   "159.237.4.120"
            Top             =   960
            Width           =   1455
         End
         Begin VB.CommandButton Action 
            Caption         =   "Buscar"
            Height          =   255
            Index           =   2
            Left            =   -70920
            TabIndex        =   7
            Top             =   480
            Width           =   855
         End
         Begin VB.TextBox txtPal 
            Height          =   285
            Left            =   -73440
            TabIndex        =   6
            Top             =   465
            Width           =   1215
         End
         Begin VB.CommandButton Action 
            Caption         =   "Buscar"
            Height          =   255
            Index           =   1
            Left            =   -70920
            TabIndex        =   5
            Top             =   720
            Width           =   855
         End
         Begin VB.TextBox txtCodRef 
            Height          =   285
            Left            =   -72720
            TabIndex        =   4
            Top             =   705
            Width           =   1695
         End
         Begin VB.CommandButton Action 
            Caption         =   "Buscar"
            Height          =   255
            Index           =   0
            Left            =   3960
            TabIndex        =   3
            Top             =   840
            Width           =   975
         End
         Begin VB.TextBox txtDiag 
            Height          =   645
            Left            =   120
            MultiLine       =   -1  'True
            TabIndex        =   2
            Top             =   480
            Width           =   3735
         End
         Begin ComctlLib.TreeView tv 
            Height          =   2295
            Index           =   0
            Left            =   120
            TabIndex        =   27
            Top             =   1200
            Width           =   4815
            _ExtentX        =   8493
            _ExtentY        =   4048
            _Version        =   327682
            Indentation     =   266
            Style           =   7
            Appearance      =   1
         End
         Begin ComctlLib.TreeView tv 
            Height          =   2295
            Index           =   2
            Left            =   -74880
            TabIndex        =   28
            Top             =   1200
            Width           =   4815
            _ExtentX        =   8493
            _ExtentY        =   4048
            _Version        =   327682
            Indentation     =   266
            Style           =   7
            Appearance      =   1
         End
         Begin VB.Label Label3 
            Caption         =   "Alias para esta referencia:"
            Height          =   255
            Left            =   -74880
            TabIndex        =   29
            Top             =   1080
            Width           =   1935
         End
         Begin VB.Label Label2 
            Caption         =   "Introducir sin�nimo"
            Height          =   255
            Index           =   1
            Left            =   -74880
            TabIndex        =   21
            Top             =   855
            Width           =   1455
         End
         Begin VB.Label Label4 
            Caption         =   "Usuario"
            Height          =   285
            Index           =   6
            Left            =   -74520
            TabIndex        =   19
            Top             =   615
            Width           =   615
         End
         Begin VB.Label Label4 
            Caption         =   "Password:"
            Height          =   285
            Index           =   5
            Left            =   -72240
            TabIndex        =   18
            Top             =   615
            Width           =   855
         End
         Begin VB.Label Label4 
            Caption         =   " M�x.Ref."
            Height          =   195
            Index           =   2
            Left            =   3840
            TabIndex        =   14
            Top             =   525
            Width           =   735
         End
         Begin VB.Label Label4 
            Caption         =   "Port:"
            Height          =   285
            Index           =   1
            Left            =   -72240
            TabIndex        =   12
            Top             =   975
            Width           =   375
         End
         Begin VB.Label Label4 
            Caption         =   "Host"
            Height          =   285
            Index           =   0
            Left            =   -74520
            TabIndex        =   10
            Top             =   975
            Width           =   375
         End
         Begin VB.Label Label2 
            Caption         =   "Escriba la Palabra:"
            Height          =   255
            Index           =   0
            Left            =   -74880
            TabIndex        =   9
            Top             =   480
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Ref:"
            Height          =   255
            Left            =   -73080
            TabIndex        =   8
            Top             =   720
            Width           =   375
         End
      End
      Begin VB.Label Logger 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   120
         TabIndex        =   30
         Top             =   3960
         Width           =   5055
      End
   End
End
Attribute VB_Name = "AxCIE9"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Private Sub Action_Click(Index As Integer)
    Dim data$
    Select Case Index
        Case 0 ' De un texto libre, entresacar diagn�sticos
            data = Pack3("GetRefTxt", txtDiag.Text, txtNMatch.Text)
        Case 1 ' De un c�digo CIE9, extraer el enunciado
            data = Pack4("GetDiaFroCod", txtCodRef.Text, CStr(chkPreAli.Value), CStr(chkSubCla.Value))
        Case 2 ' De una palabra, ver en qu� referencias interviene, y sus sin�nimos
            data = Pack3("GetPalPal", txtPal.Text, CStr(chkSin.Value))
        Case 3 ' Insertar nuevo diagn�stico (personal) para una referencia
            data = Pack3("InsDiaForRef", txtCodRef.Text, txtNewDiag.Text)
        Case 4 ' Insertar nuevo sin�nimo (personal) para una referencia
            data = Pack3("InsSinForPal", txtPal.Text, txtSin.Text)
    End Select
    ' Empaquetar Usr/Pass
    data = Pack3(txtUser.Text, txtPass.Text, data)
    ' Procesar comandos
    Call Process(data, Choose(Index + 1, tv(0), tv(1), tv(2), tv(1), tv(2)), Logger)
End Sub

Private Sub Process(com$, tv As TreeView, Logger As Control)
    Dim Cmds$(120), Cmd$(4), Arg$(3), n&, i&, j&, Nod As Node, sck As Winsock
    tv.Nodes.Clear: Set Nod = tv.Nodes.Add(, tvwFirst, "Raiz", "Resultados:")
    Nod.Expanded = True
    Set sck = New Winsock
    Call Conexion(sck, txtHost.Text, CInt(txtPort.Text), Logger)
    Cmds(1) = Pack1("ContinueListening"): i = 1: n = 1
    Do While i <= n
        Call UnPackAndGetBody(Cmd, j, Cmds(i))
        Select Case Cmd(1)
            Case "TooManyResults"
                Logger = "Hay demasiados resultados, por favor, acote la pregunta."
            Case "InsNod"
                On Error Resume Next
                Set Nod = tv.Nodes.Add(Cmd(2), tvwChild, Cmd(3), Cmd(4))
                On Error GoTo 0
            Case "MsgBox"
                Call MsgBox(Cmd(2))
            Case "LogMsg"
                Logger = Logger & Cmd(2)
            Case "ContinueListening"
                'Enviar y recibir comandos al/del servidor
                com = PutGet(com, sck, Logger)
                Call UnPack(Cmds, n, com)
                com = ""
                i = 0
        End Select
        i = i + 1
    Loop
    Call Desconexion(sck, Logger)
    Set sck = Nothing
End Sub

Private Sub tv_NodeClick(Index As Integer, ByVal Node As ComctlLib.Node)
    Logger.Caption = Node
End Sub
