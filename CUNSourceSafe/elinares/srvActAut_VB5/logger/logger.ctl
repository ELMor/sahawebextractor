VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.UserControl logger 
   ClientHeight    =   3600
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   ScaleHeight     =   3600
   ScaleWidth      =   4800
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   360
      Top             =   3000
   End
   Begin ComctlLib.TreeView tv1 
      Height          =   2655
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   4683
      _Version        =   327682
      Indentation     =   706
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "il1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.ImageList il1 
      Left            =   960
      Top             =   2880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   4
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "logger.ctx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "logger.ctx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "logger.ctx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "logger.ctx":094E
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "logger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private logfname$
Private msg&
Private strs$(4)
Private nivel%

Private Sub Timer1_Timer()
    Dim a As Boolean
    Err.Clear
    a = Timer1.Enabled
    Timer1.Enabled = False
    Screen.MousePointer = 11
    If UserControl.tv1.Visible = True And logfname <> "" Then
        Call LoadTree(tv1, logfname, strs, 0)
    End If
    Screen.MousePointer = 0
    Timer1.Enabled = a
End Sub

Public Property Get LogFile() As String
    LogFile = logfname
End Property

Public Property Let LogFile(ByVal vNewValue As String)
    logfname = vNewValue
    Call Reload
End Property


Public Sub Reload()
    tv1.Nodes.Clear
    msg = 0
    Call Timer1_Timer
End Sub

Private Sub UserControl_Initialize()
    Timer1.Enabled = False
    strs(1) = "@!"
    strs(2) = "Comprobando..."
    strs(3) = "Actualizacion Completada"
    strs(4) = "REINICIALIZADA"
    nivel = 4
End Sub

Private Sub UserControl_Resize()
    tv1.Height = UserControl.Height
    tv1.Width = UserControl.Width
    tv1.Top = 0
    tv1.Left = 0
End Sub

Public Property Get RefrAuto() As Long
    RefrAuto = (Timer1.Enabled = True)
End Property

Public Property Let RefrAuto(ByVal vNewValue As Long)
    If vNewValue = 0 Then
        Timer1.Enabled = False
    Else
        Timer1.Interval = 5000 + vNewValue * 1000
        Timer1.Enabled = True
    End If
End Property

Public Sub Refrescar()
    Screen.MousePointer = 11
    If UserControl.tv1.Visible = True And logfname <> "" Then
        Call LoadTree(tv1, logfname, strs, nivel)
    End If
    Screen.MousePointer = 0
End Sub

Public Sub Rama(s$, stat%)
    AppendBranch tv1, s, stat, nivel
End Sub

Public Property Get NivDetalle() As Integer
    NivDetalle = nivel
End Property

Public Property Let NivDetalle(ByVal vNewValue As Integer)
    nivel = vNewValue
End Property

Public Sub VaciarRamas()
    tv1.Nodes.Clear
End Sub
