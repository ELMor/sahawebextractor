

#include <Windows.h>
#include <lmcons.h>
#include <lmserver.h>

LPBYTE buf=NULL;
long eread,totale;

void copyDNS(char* dst,char* org){
	dst[0]=dst[1]=0;
	while( org[0] || org[1] ){
		dst[0]=org[0];
		dst[1]=org[1];
		dst+=2;
		org+=2;
	}
}

long __declspec(dllexport) __stdcall InitThunk(void){

	long bufsize=5000*sizeof(SERVER_INFO_101);
	long ret=NetServerEnum(NULL,101,&buf,bufsize,&eread,&totale,0x1000,NULL,0);
	return eread;
}

int __declspec(dllexport) __stdcall ReadComputer(long* i,char* Name, char* Comment){
	if(*i<0 || *i>=eread)
		return 1;
	copyDNS(Name,   ((SERVER_INFO_101*)buf)[*i].sv101_name);
	copyDNS(Comment,((SERVER_INFO_101*)buf)[*i].sv101_comment);
	return 0;
}

void __declspec(dllexport) __stdcall EndThunk(void){
}

