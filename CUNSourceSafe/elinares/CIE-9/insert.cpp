#include "cie9.h"
#include "search.h"

#define  Global extern
#define  GlobalIni
#include "globals.h"

long InsPal(char *p){
	long pos;
	if( (pos=BusPal(p))!=-1 )
		return pos;
	// Hay que insertar
	Dic[TamDic].Palabra=strdup(p);
	Dic[TamDic].Referencias=(long*)NULL;
	Dic[TamDic].enDiag=(char*)NULL;
	Dic[TamDic].NReferencias=0;
	Dic[TamDic].Palabras=(long*)NULL;
	Dic[TamDic].NPalabras=0;
	IndDic_Pal[TamDic]=&(Dic[TamDic]);
	TamDicSO++;
	TamDic++;
	if( (TamDicSO)%NGAPSORT == 0 ){
		qsort(IndDic_Pal,TamDic,sizeof(Pal*),cmpPal);
		TamDicSO=0;
	}
	return TamDic-1;
}

long InsRef(char *p){
	long pos;
	if( (pos=BusRef(p))!=-1 )
		return pos;
	//Insertar
	Cie[TamCie].Referencia=strdup(p);
	Cie[TamCie].Palabras=(long*)NULL;
	Cie[TamCie].NPalabras=0;
	Cie[TamCie].Diagnos=(char*)elmmalloc(1);
	Cie[TamCie].Diagnos[0]=0;
	IndCie_Ref[TamCie]=&(Cie[TamCie]);
	TamCieSO++;
	TamCie++;
	if((TamCieSO)%NGAPSORT == 0 ){
		qsort(IndCie_Ref,TamCie,sizeof(Ref*),cmpRef);
		TamCieSO=0;
	}
	return TamCie-1;
}

void InsMatch(long np,long nr,char EnDiag){
	long i,insert;

	insert=1;
	for(i=0;insert && i<Dic[np].NReferencias;i++)
		if( Dic[np].Referencias[i]==nr ){
			if(EnDiag) 
				Dic[np].enDiag[i]=1;
			insert=0;
		}
	if( insert ){
		if( Dic[np].NReferencias % NGAPMN == 0 ){
			Dic[np].Referencias=(long*)elmrealloc(Dic[np].Referencias, 
											sizeof(long)*(Dic[np].NReferencias+NGAPMN));
			Dic[np].enDiag=(char*)elmrealloc(Dic[np].enDiag,
											sizeof(char)*(Dic[np].NReferencias+NGAPMN));
		}
		Dic[np].Referencias[Dic[np].NReferencias]=nr;
		Dic[np].enDiag     [Dic[np].NReferencias]=EnDiag;
		Dic[np].NReferencias++;
	}

	insert=1;
	for(i=0;insert && i<Cie[nr].NPalabras;i++)
		if(Cie[nr].Palabras[i]==np)
			insert=0;
	if( insert ){
		if( Cie[nr].NPalabras % NGAPMN ==0 )
			Cie[nr].Palabras=(long*)elmrealloc(Cie[nr].Palabras, 
											sizeof(long)*(Cie[nr].NPalabras+NGAPMN));
		Cie[nr].Palabras[Cie[nr].NPalabras]=np;
		Cie[nr].NPalabras++;
	}
}

void InsLink(long n1,long n2){
	long i,insert;
	insert=1;
	for(i=0;insert && i<Dic[n1].NPalabras;i++)
		if( Dic[n1].Palabras[i]==n2 )
			insert=0;
	if( insert ){
		if( Dic[n1].NPalabras % NGAPMN == 0 )
			Dic[n1].Palabras=
				(long*)elmrealloc(Dic[n1].Palabras,sizeof(long)*(Dic[n1].NPalabras+NGAPMN));
		Dic[n1].Palabras[Dic[n1].NPalabras]=n2;
		Dic[n1].NPalabras++;
	}
	insert=1;
	for(i=0;insert && i<Dic[n2].NPalabras;i++)
		if( Dic[n2].Palabras[i]==n1 )
			insert=0;
	if( insert ){
		if( Dic[n2].NPalabras % NGAPMN == 0 )
			Dic[n2].Palabras=
				(long*)elmrealloc(Dic[n2].Palabras,sizeof(long)*(Dic[n2].NPalabras+NGAPMN));
		Dic[n2].Palabras[Dic[n2].NPalabras]=n1;
		Dic[n2].NPalabras++;
	}
}

