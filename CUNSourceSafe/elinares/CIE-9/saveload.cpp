#include "cie9.h"
#include "search.h"
#include "ndxcie.h"

#define  Global extern
#define  GlobalIni
#include "globals.h"

#ifdef UNIX
#include "sys/ipc.h"
#include "sys/shm.h"
#endif

int cmpLong(const void *p1, const void *p2 ){
	return *(long*)p1-*(long*)p2;
}

void SaveData( char *fname ){
	FILE *fp;
	long offset=0,i,p;
	Pal dumPal;
	Ref dumRef;

	if( (fp=fopen(fname,"wb"))==NULL ){
		perror(fname);
		exit(1);
	}
	//C�lculo del tama�o fijo ocupado, o sea, 
	//inicio del bloque de tama�o variable.
	offset= TamDic*(sizeof(Pal) +		// Palabras
					sizeof(long*) ) +	// �ndice
			TamCie*(sizeof(Ref) +		// Referencias
					sizeof(long*) ) +	// Indice
			NumPCo*(sizeof(char*) ) +	// Palabras comunes ordenadas.
			sizeof(long)*4;				// TamCie, TamDic, NumPCo, OffSet
	//Grabar tama�o fijo
	fwrite(&TamDic,sizeof(long),1,fp);
	fwrite(&TamCie,sizeof(long),1,fp);
	fwrite(&NumPCo,sizeof(long),1,fp);
	fwrite(&offset,sizeof(long),1,fp);
	for(i=0;i<TamDic;i++){
		dumPal.Palabra		=(char*)offset; offset+=1+strlen( Dic[i].Palabra );
		dumPal.Referencias	=(long*)offset; offset+=(Dic[i].NReferencias)*sizeof(long);
		dumPal.enDiag		=(char*)offset; offset+=(Dic[i].NReferencias)*sizeof(char);
		dumPal.NReferencias =Dic[i].NReferencias;
		dumPal.Palabras		=(long*)offset; offset+=(Dic[i].NPalabras)*sizeof(long);
		dumPal.NPalabras	=Dic[i].NPalabras;
		qsort( Dic[i].Referencias, Dic[i].NReferencias, sizeof(long), cmpLong );
		qsort( Dic[i].Palabras,	   Dic[i].NPalabras,    sizeof(long), cmpLong );
		fwrite(&dumPal,sizeof(Pal),1,fp);
	}
	for(i=0;i<TamCie;i++){
		dumRef.Referencia	=(char*)offset; offset+=1+strlen( Cie[i].Referencia );
		dumRef.Palabras		=(long*)offset; offset+=(Cie[i].NPalabras)*sizeof(long*);
		dumRef.NPalabras	=Cie[i].NPalabras;
		dumRef.Diagnos		=(char*)offset; offset+=1+astrlen(Cie[i].Diagnos);
		qsort( Cie[i].Palabras, Cie[i].NPalabras, sizeof(long), cmpLong );
		fwrite(&dumRef,sizeof(Ref),1,fp);
	}
	for(i=0;i<TamDic;i++){
		p= ((char*)(IndDic_Pal[i]) - (char*)(&(Dic[0])) );
		fwrite(&p,sizeof(char*),1,fp);
	}
	for(i=0;i<TamCie;i++){
		p= ((char*)(IndCie_Ref[i]) - (char*)(&(Cie[0])) );
		fwrite(&p,sizeof(char*),1,fp);
	}
	for(i=0;i<NumPCo;i++){
		fwrite(&offset,sizeof(long),1,fp);
		offset+=(1+strlen(PalCom[i]));
	}
	//Grabar Tama�o variable
	for(i=0;i<TamDic;i++){
		fwrite(Dic[i].Palabra, sizeof(char), 1+strlen(Dic[i].Palabra), fp);
		fwrite(Dic[i].Referencias, sizeof(long), Dic[i].NReferencias,fp);
		fwrite(Dic[i].enDiag,sizeof(char), Dic[i].NReferencias,fp);
		fwrite(Dic[i].Palabras,sizeof(long),Dic[i].NPalabras,fp);
	}
	for(i=0;i<TamCie;i++){
		fwrite(Cie[i].Referencia,sizeof(char),1+strlen(Cie[i].Referencia),fp);
		fwrite(Cie[i].Palabras,sizeof(long),Cie[i].NPalabras,fp);
		fwrite(Cie[i].Diagnos,sizeof(char),1+astrlen(Cie[i].Diagnos),fp);
	}
	for(i=0;i<NumPCo;i++)
		fwrite(PalCom[i],sizeof(char),1+strlen(PalCom[i]),fp);
	fclose(fp);
}

void LoadData( char *fname ){
	FILE *fp;
	long Offset,i;
	char *p;
	struct stat sstat;

	stat(fname, &sstat);
	i=sstat.st_size;					// Tama�o en bytes del archivo
	if( (fp=fopen(fname,"rb"))==NULL){
		perror( fname );
		exit(-1);
	}
	#ifdef UNIX
		shmid=shmget(SHAREDMEMSEGID,i,IPC_CREAT|0666);
		if(shmid==-1){
			perror("IPC:");
			exit(-1);
		}
		p=(char*)shmat(shmid,0,0);
	#else
		p=(char*)elmmalloc(i);				// Reserva de memoria cont�nua
	#endif
	base=(long)p;
	fread(p,sizeof(char),i,fp);			// Lectura del disco
	fclose(fp);
	//Variables
	TamDic    =*((long*)(p));	p+=sizeof(long);
	TamCie    =*((long*)(p));	p+=sizeof(long);
	NumPCo    =*((long*)(p));	p+=sizeof(long);
	Offset    =*((long*)(p));	p+=sizeof(long);
	Dic       =   (Pal*)(p) ;	p+=TamDic*sizeof(Pal);				
	Cie       =   (Ref*)(p) ;	p+=TamCie*sizeof(Ref);
	IndDic_Pal=  (Pal**)(p) ;	p+=TamDic*sizeof(long*);
	IndCie_Ref=  (Ref**)(p) ;	p+=TamCie*sizeof(long*);
	PalCom    = (char**)(p) ;	p+=NumPCo*sizeof(char*);
	//Correcci�n de la direcci�n de los �ndices
	for(i=0;i<TamDic;i++)
		IndDic_Pal[i]=(Pal*)((char*)IndDic_Pal[i] + (long)(&(Dic[0])) );
	for(i=0;i<TamCie;i++)
		IndCie_Ref[i]=(Ref*)((char*)IndCie_Ref[i] + (long)(&(Cie[0])) );
	//Correcci�n punteros a maestros
	for(i=0;i<TamDic;i++){
		Dic[i].Palabra		=(char*)((char*)(Dic[i].Palabra )	 + base);
		Dic[i].Referencias	=(long*)((char*)(Dic[i].Referencias) + base);
		Dic[i].enDiag		=(char*)((char*)(Dic[i].enDiag)      + base);
		Dic[i].Palabras		=(long*)((char*)(Dic[i].Palabras)	 + base);
	}
	for(i=0;i<TamCie;i++){
		Cie[i].Referencia	=(char*)((char*)(Cie[i].Referencia ) + base);
		Cie[i].Palabras		=(long*)((char*)(Cie[i].Palabras   ) + base);
		Cie[i].Diagnos		=(char*)((char*)(Cie[i].Diagnos	   ) + base);
	}
	//Correcci�n a Palabras Comunes
	for(i=0;i<NumPCo;i++)
		PalCom[i]=PalCom[i] + base;
}

void FreeData(void){
	#ifdef UNIX
		shmctl(shmid,IPC_RMID,(shmid_ds*)NULL);
	#else
		free((void*)base);
	#endif
}

void ReloadData(int signal){
	FreeData();
	LoadData(findciename);
}

void LoadNomCie( char *fname ){
	FILE *fp;
	char Buf[256],*aux;
	long pos,pospal,t1,t2,gen[3],tamgen;

	if( NULL==(fp=fopen(fname,"rt")) ){
		perror(fname);
		exit(-2);
	}
	while( elmfgets(Buf,255,fp) )
		if( *Buf=='D' || *Buf=='d' ){
			aux=strtok(Buf ," ");
			aux=strtok((char*)NULL,"\r\n");
			pos=InsRef(Buf+1);
			tamgen=0;
			RefTree(Buf+1,gen,&tamgen);
			t1=astrlen(Cie[pos].Diagnos);
			t2=strlen(aux);
			Cie[pos].Diagnos=(char*)elmrealloc(Cie[pos].Diagnos,2+t1+t2);
			strcpy(Cie[pos].Diagnos+t1,aux);
			Cie[pos].Diagnos[1+t1+t2]=0;
			Translate(aux);
			aux=Tokenize(aux,TokPalabras);
			while(aux){
				if( !BusPalCom(aux) ){
					pospal=InsPal(aux);
					for(t1=0;t1<tamgen;t1++)
						if(gen[t1]>=0)
							InsMatch(pospal,gen[t1],1);
				}
				aux=Tokenize((char*)NULL,TokPalabras);
			}
		}
	fclose(fp);
}

void LoadPalCom( char *fname ){
	struct stat sstat;				// Para el tama�o del archivo de palabras comunes
	char *basePalCom,*s;			// Archivo Palabras Comunes
	FILE *fp;

	stat(fname, &sstat);
	basePalCom  =(char*) elmmalloc( sizeof(char)*(1+sstat.st_size) );
	if( (fp=fopen(fname,"rt"))==NULL){
		perror(fname);
		exit( -1);
	}
	fread(basePalCom,sizeof(char),sstat.st_size,fp);
	*(basePalCom+sstat.st_size)=0;	// Finalizar con un 0 la cadena.
	fclose(fp);
	s=Tokenize(basePalCom,TokPalabras);
	while(s){
		PalCom[NumPCo++]=s;
		s=Tokenize((char*)NULL,TokTodo);
	}
	IndexPalCom();					//Ordenar palabras comunes.
}

void LoadIndCie( void ){
	FILE *fp;
	char Buf[10][256],Aux[256],*s,	// Auxiliar
			 *PalPerLin[128],		// Palabras por L�nea
			  nCoPPLin[128],		// Nombre de columna para la palabra.
			 *pCamp[10];			// Inicios de Campo en una l�nea de texto
	long NPalPerLin,				// Numero de palabras en una l�nea
			 i,j,k,l,m,					// Auxiliar
			 nCamp,					// N�mero de campos de la l�nea
			 NPal,					// C�digo de Palabra
			 NRef,					// C�digo de referencia
			 NumLinPro,				// Numero de Lineas de Texto le�das
			 PalLev[10],			// Indice de comienzo de Nivel de Palabras (los ��)
			 OldLev,NewLev;			// Antiguo y nuevo nivel.

	//Inicializaci�n.
	NPalPerLin=NumLinPro=0;
	OldLev=NewLev=0;
	PalLev[0]=0;

	for(k=0;Files[k].nomf[0];k++){
		if( (fp=fopen(Files[k].nomf,"rt"))==NULL ){
			perror(Files[k].nomf);
			exit(-2);
		}
		printf("Fichero %s, NLi:%ld,Dic:%ld,Cie:%ld\n",
			Files[k].nomf,NumLinPro,TamDic,TamCie);
		nCamp=strlen( Files[k].estf );
		//Lectura ppal
		while(elmfgets(Aux,255,fp)){
			++NumLinPro;
			// Nuevo nivel (n�mero de � en este formato)
			NewLev=Level(Aux);						
			// Error de transcripci�n?
			// Presentar Warning y continuar. ( como un nivel sup.)
			if(NewLev-OldLev>1)			PalLev[(NewLev=OldLev+1)]=NPalPerLin;
			// Restaurar punteros NPalPerLin y NRefPerLin
			else if(NewLev==OldLev)		NPalPerLin=PalLev[OldLev];
			// Guardamos en la pila la posici�n de los punteros 
			else if(NewLev==OldLev+1)	PalLev[NewLev]=NPalPerLin;
			// Corregir punteros NRefPerLin y NPalPerLin
			else if(NewLev<OldLev)		NPalPerLin=PalLev[NewLev];
			// Guardar linea seg�n nivel
			strcpy(Buf[NewLev],Aux);				
			// Cambiar acentos, di�resis, etc...
			Translate(Buf[NewLev]);					
			// Extraer Campos
			pCamp[0]=Buf[NewLev];
			l=1;
			s=strchr(Buf[NewLev],'\t');
			while(s){
				*s=0;
				pCamp[l++]=++s;
				s=strchr(s,'\t');
			}
			// Para cada campo se extraen palabras
			for(m=0;m<l;m++){						
				s=Tokenize(pCamp[m],TokPalabras);
				while(s){
					// Si no es una palabra com�n (de,el,la...)
					if(!BusPalCom(s)){				
						nCoPPLin[NPalPerLin]=Files[k].estf[m]-'0';
						PalPerLin[NPalPerLin++]=s;
					}
					// Siguiente palabra
					s=Tokenize((char*)NULL,TokPalabras);	
				}
			}
			//Insertar referencias, palabras y 'matches'.
			//Es posible que haya que ampliar palabras, dependiendo 
			//del c�digo del campo que estamos tratando.
			for(i=0;i<NPalPerLin;i++){
				if( nomCol[nCoPPLin[i]].esRef=='1' ){
					NRef=InsRef(PalPerLin[i]);
					for(j=0;j<NPalPerLin;j++)
						if(nomCol[nCoPPLin[j]].esRef=='0'){
							NPal=InsPal(PalPerLin[j]);
							InsMatch(NPal,NRef);
						}
					//A�adir cabeceras de tablas
					for(j=0;j<3;j++)
						if(nomCol[nCoPPLin[i]].Def[j][0]){
							NPal=InsPal(nomCol[nCoPPLin[i]].Def[j]);
							InsMatch(NPal,NRef);
						}
				}else{
					//Palabra pertenece al campo Aclaraci�n?
					if( nCoPPLin[i]==fldAclaracion ){
					//Adem�s es un (Rango)referencia ,al menos el formato [A]nnn[{./}n][n]
						if( isRef(PalPerLin[i],0,0,0) ){
							NRef=InsRef(PalPerLin[i]);
							//Linkar con las palabras del campo Aclaracion
							for(j=0;j<NPalPerLin;j++)
								if(nCoPPLin[j]==fldAclaracion)
									InsMatch(InsPal(PalPerLin[j]),NRef);
						}else{ //Si no es una referencia, se linkan palabras
							   //Pero solo con las de su nivel.
							NPal=InsPal(PalPerLin[i]);
							for(j=0;j<NPalPerLin;j++)
								if(nCoPPLin[j]==fldAclaracion)
									InsLink(InsPal(PalPerLin[j]),NPal);
						}
					}
				}
			}
			// Para la siguiente iteraci�n
			OldLev=NewLev;							
		}
		fclose(fp);
	}

}
