#include "cie9.h"

#include "send.h"



#define  Global extern

#define  GlobalIni

#include "globals.h"



void SendAll(char **sou, int flagpal, int flagref){

	SendAllRef(sou,flagref);

	SendAllPal(sou,flagpal);

}



void SendAllPal(char **sou,  int flags){

	int i;

	for(i=0;i<TamDic;i++)

		SendPal(sou,"Raiz",IndDic_Pal[i]-&Dic[0],flags);

}



void  SendAllRef(char **sou,int flags){

	int i;

	for(i=0;i<TamCie;i++)

		SendRef(sou,"Raiz",IndCie_Ref[i]-&Cie[0],flags);

}



void SendFrase(char **sou, char *Padre, long nf, char *s, Match *MRef, long ns){

	char buf[12];

	sprintf(buf,"Frase%02ld",nf);

	WriteBinary(sou,Padre,buf,s);

	for(int i=0;MRef[i].Veces&&i<ns;i++)

		SendRef( sou, buf, MRef[i].Valor,0);

}



void SendRef(char **sou, char *Padre, long i, int flags){

	long k,gen[4],tamgen=0,ndia;

	char *aux;

	if( i==-1 ){

		WriteBinary(sou,"Raiz","RNE","Referencia no encontrada");

	}else{

		char *buf1=(char*)malloc(1024),

			 *buf2=(char*)malloc(1024),

			 *buf3=(char*)malloc(1024);

		RefTree(Cie[i].Referencia,gen,&tamgen);

		strcpy(buf1,Padre);

		for(k=0;k<tamgen;k++){

			if( flags & 0x02 ){	// Presentar todos los alias

				sprintf(buf2,"Ref %s Multiple",Cie[gen[k]].Referencia);

				sprintf(buf3,"%s",Cie[gen[k]].Referencia);

				WriteBinary(sou,buf1,buf2,buf3);

				sprintf(buf3,"Ref %s Alias",Cie[gen[k]].Referencia);

				WriteBinary(sou,buf2,buf3,"Alias");

				sprintf(buf3,"Ref %s",Cie[gen[k]].Referencia);

				WriteBinary(sou,buf2,buf3,"Diagnosticos");

				sprintf(buf3,"Ref %s Alias",Cie[gen[k]].Referencia);

				aux=Cie[gen[k]].Diagnos;

				ndia=0;

				while(strlen(aux)){

					sprintf(buf2,"Ref %s Dia %02ld",Cie[gen[k]].Referencia,++ndia);

					WriteBinary(sou,buf3,buf2,aux);

					aux+=1+strlen(aux);

				}

			}else{

				sprintf(buf2,"Ref %s",Cie[gen[k]].Referencia);

				sprintf(buf3,"%s:%s",buf2,Cie[gen[k]].Diagnos);

				WriteBinary(sou,buf1,buf2,buf3);

			}

			sprintf(buf1,"Ref %s",Cie[gen[k]].Referencia);

		}

		if( flags & 0x01 ){	//Presentar las palabras que lo componen

			sprintf(buf1,"Ref %s",Cie[i].Referencia);

			for(k=0;k<Cie[i].NPalabras;k++)

				SendPal(sou,buf1,Cie[i].Palabras[k],0);

		}

		if( flags & 0x04 ){ //Presentar subclases de esta

			sprintf(buf1,"Ref %s",Cie[i].Referencia);

			Ref **ini,**fin;

			if( SearchSubClas(Cie[i].Referencia,&ini,&fin) )

				while(ini<=fin)

					SendRef(sou,buf1,*(ini++)-&(Cie[0]), flags & 0x0b);



		}

		free(buf1); free(buf2); free(buf3);

	}

}



void SendPal(char **sou, char *Padre, long n, int flags){

	long k;

	if(n==-1){

		WriteBinary(sou,Padre,"PNC","Palabra no encontrada");

	}else{

		WriteBinary(sou,Padre,Dic[n].Palabra,Dic[n].Palabra);

		if( flags & 0x01 )

			for(k=0;k<Dic[n].NReferencias;k++)

				SendRef(sou,Dic[n].Palabra,Dic[n].Referencias[k],0);

		if( flags & 0x02 )

			for(k=0;k<Dic[n].NPalabras;k++)

				SendPal(sou,Dic[n].Palabra,Dic[n].Palabras[k],0);

	}

}



void SendRootPal(char **sou, char *Padre, char *s){

	Pal **ini=(Pal**)NULL,**fin=(Pal**)NULL,dpal,*pdpal;

    return;

	char buf[128];

	pdpal=&dpal;

	dpal.Palabra=s;

	SearchRange(s,&ini,&fin);

	strcpy(buf,"Palabras similares:");

	WriteBinary(sou,Padre,buf,buf);

	if(ini)

		for(; ini && ini<=fin; ini++)

			SendPal(sou,buf,*ini-&(Dic[0]),0);

}



void WriteBinary(char **sou, char *RefPadre, char *Ref, char *Texto){

	char *packed=(char*)NULL,buf1[5],buf2[5];

	long lon;



	lon=strlen(Pack(&packed,"InsNod",RefPadre,Ref,Texto,NULL));

	strncpy(buf1,*sou  ,4); buf1[4]=0; sprintf(buf1,"%04ld",1  +atol(buf1));

	strncpy(buf2,*sou+4,4); buf2[4]=0; sprintf(buf2,"%04ld",lon+atol(buf2));

	*sou=(char*)elmrealloc(*sou,1+strlen(*sou)+lon);

	strncpy(*sou  ,buf1,4);

	strncpy(*sou+4,buf2,4);

	strcat(*sou,packed);

	free(packed);

}



