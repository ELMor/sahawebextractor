

#include "cie9.h"

#include "search.h"

#include "tcp4u.h"

#include "search.h"

#include "strbin.h"



#define  Global extern

#define  GlobalIni

#include "globals.h"



#define SZBUFFER 16*1024



void ThreadProc( void *lpv ){

	SOCKET *cs=(SOCKET*)lpv;

	while( SocketTextSearch( *cs ) )

		;

	TcpClose(cs);



	#ifdef UNIX

		Tcp4uCleanup();

		exit(0);



	#else

		free(lpv);

		_endthread();



	#endif

}





int SocketTextSearch( SOCKET CSock ){

	Match *MRef;

	char  *Frases[128],*sout,*pszBuf;

	int ret;



	pszBuf=(char*)elmmalloc(SZBUFFER);

	sout=  (char*)elmmalloc(9);

	if( TcpRecv(CSock, pszBuf, SZBUFFER, 0, HFILE_ERROR)==

		TCP4U_SOCKETCLOSED){

		ret=0;

	}else{

		//Reserva para la b�squeda

		MRef=(Match*)elmmalloc(sizeof(Match)*TamCie);

		//Busqueda

		strcpy(sout,"00000000");

		ret=TextSearch(pszBuf,MRef,Frases,&sout);	// 1:Binary

		//Liberar

		elmfree(MRef);	

		if( ret!=0 ){

            if( strlen(sout)>9999 ){

                char *tmr1=(char*)NULL,*tmr2=(char*)NULL;

                Pack(&tmr1,"TooManyResults",(char*)NULL);

                Pack(&tmr2,tmr1,(char*)NULL);

                TcpSend(CSock,tmr2,strlen(tmr2),0,HFILE_ERROR);

                free(tmr1); free(tmr2);

            }else{

			    TcpSend(CSock,sout,strlen(sout),0,HFILE_ERROR);

            }

            #ifdef UNIX

            #ifdef DEBUG_QUERY

            FILE *fp=fopen("/tmp/cie9serv.log","at");

            fprintf(fp,"Query:\n\t%s\nResponse:\n\t%s",pszBuf,sout);

            fclose(fp);

            #endif

            #endif

        }

	}

	free(sout);

	free(pszBuf);

	return ret;

}



