#include "cie9.h"

#include "ndxcie.h"

#include "search.h"



#define  Global

#define  GlobalIni =0

#include "globals.h"



int main( void ){



	//Reserva inicial de memoria

	Dic     	=(Pal*)  elmmalloc( sizeof(Pal ) *NPALMX );

	IndDic_Pal	=(Pal**) elmmalloc( sizeof(Pal*) *NPALMX );

	Cie			=(Ref*)  elmmalloc( sizeof(Ref ) *NREFMX );

	IndCie_Ref	=(Ref**) elmmalloc( sizeof(Ref*) *NREFMX );

	PalCom		=(char**)elmmalloc( sizeof(char*)*NPALCOM);

	//Apertura archivos de texto, seg�n formato Roberto Deza.

	if(chdir(NDATADIR)){

		perror(NDATADIR); return -1;

	}

	//Carga de Palabras Comunes

	LoadPalCom(FPALCOM);

	//Carga de los nombres de las referencias CIE9 (Volumen 1)

	LoadNomCie(FCIE9);

	//Carga del indice de referencias (Volumen 2)

	LoadIndCie( );

	//Vuelta al directorio inicial

	chdir("..");

	//Algo que imprimir.

	printf("TamDic:%ld;TamCie:%ld\n",TamDic,TamCie);

	//Indexar todo.

	printf("Indexando...");

	IndexAll();

	printf("Fin.\n"); 

	//Grabar

	printf("Grabando...");

	SaveData(FINDCIE);

	printf("%s grabado.\n",FINDCIE);

	return 0;

}

