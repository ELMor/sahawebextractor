
#include "valor.h"
#include "cursor.h"
#include "indice.h"
#include <stdarg.h>

CCur::CCur(CFic *f){
	regact=-1;
	fiche=f;
	ibuffer=(uchar*)malloc(32*1024);	//Maximo tama�o de un registro IBM
	if( fiche->esInd() ){ //Es un indice?
		pcVal=(CVal**)malloc( sizeof(CVal*)*fiche->numCamInd() );
		long k;
		for(k=0;k<fiche->numCamInd();k++)
			pcVal[k]=new CVal( CVal::parse(this,fiche->nomCamInd(k)) );
	}else
		pcVal=(CVal**)NULL;
}

CCur::~CCur(){
	long k;
	if( pcVal ){
		for(k=0;k<fiche->numCamInd();k++)
			delete pcVal[k];
		free(pcVal);
	}
}

bool 
CCur::lee( long nr, long np ){
	if(nr==RegSig)
		regact+=np;
	else if(nr==RegAnt)	
		regact-=np;
	else				
		regact=nr;
	return fiche->getReg(this) ? true : false;
}
long CCur::tamReg(){
	if( regact>=0 )
		return fiche->getTamReg(regact);
	return 0;
}
bool 
CCur::busca(long PosReg, int numvals, ...){
	bool ret=true;
	static int lastNVals=0;
	if(fiche->esInd()){
		va_list marker;
		va_start(marker,numvals);
		if(numvals) lastNVals=numvals;
		if( !fiche->busca(this,PosReg,lastNVals,marker) )
			ret=false;
		va_end(marker);
	}else{
		puts("B�squeda indexada:Indice no existe.");
		exit(-1);
	}
	return ret;
}

