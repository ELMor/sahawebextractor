

#include "pack.h"
#include <string.h>

// La tira de referencia es: (6 bits por caracter)
// Bytes:   1        2        3
// Bits:    11111122 22223333 33444444
static opmask m43[4]={
	{ 0xFC, 0x00, 0 , 0 , 2 , 0},		// 1º caracter
	{ 0x03, 0xF0, 4 , 4 , 0 , 1},		// 2º caracter
	{ 0x0F, 0xC0, 2 , 6 , 0 , 1},		// 3º caracter	
	{ 0x3F, 0x00, 0 , 0 , 0 , 1}	};	// 4º caracter

// La tira de referencia es (5 bits por caracter)
// Bytes:  1        2        3        4        5
// Bits:   11111222 22333334 44445555 56666677 77788888
static opmask m85[8]={
	{ 0xF8, 0x00, 0 , 0 , 3 , 0},		// 1º caracter
	{ 0x07, 0xC0, 2 , 6 , 0 , 1},		// 2º caracter
	{ 0x3E, 0x00, 0 , 0 , 1 , 0},		// 3º caracter
	{ 0x01, 0xF0, 4 , 4 , 0 , 1},		// 4º caracter
	{ 0x0F, 0x80, 1 , 7 , 0 , 1},		// 5º caracter
	{ 0x7C, 0x00, 0 , 0 , 2 , 0},		// 6º caracter
	{ 0x03, 0xE0, 3 , 5 , 0 , 1},		// 7º caracter
	{ 0x1F, 0x00, 0 , 0 , 0 , 1}	};	// 8º caracter

/**
 * Descompresión 5/8
 */
void unpack8en5(uchar *dst, uchar *org, ulong tam){
	unpackgen(dst,org,tam,m85,8);
}
void unpack4en3(uchar *dst, uchar *org, ulong tam){
	unpackgen(dst,org,tam,m43,4);
}
void unpackgen(uchar *dst, uchar *org, ulong tam, opmask *om, int lm){
	uchar rot=0;
	while(tam){
		*dst = ( ((org[0] & om[rot].mask1)<<om[rot].rotl1) |
			     ((org[1] & om[rot].mask2)>>om[rot].rotr2)   )
				>> om[rot].rotr;
		dst++;
		org+=om[rot].sumbase;
		tam-=om[rot].sumbase;
		if(++rot>=lm)
			rot=0;
	}
}

/**
 * Descompresion 2/3: Juego de caracteres de 40.
 */
void unpackb40(uchar *dst, uchar *org, ulong tam){
	for(uint i=0; i<tam; i+=2){
		uint k=org[0]*256+org[1];
		dst[0]=k/40/40;
		dst[1]=(k/40-dst[0]*40);
		dst[2]=(k-dst[0]*40*40-dst[1]*40);
		org+=2;
		dst+=3;
	}
}

/**
 * Desempaquetar nibbles sin signo
 */
long updcbss(uchar *p, long nnibb){
	long n,ret=0;
	for(n=0;n<nnibb/2;n++){
		ret*=100;
		ret+= ((p[n]&0xf0)>>4)*10 + (p[n]&0x0f);
	}
	return ret;
}

char getHexChar(int i){
	if(i>=0  && i<10)
		return i+'0';
	if(i>=10 && i<16)
		return i-10+'A';
	return 'X';
}

void updcbss(uchar *p, long nnibb, char *dst){
	long n;
	for(n=0;n<nnibb/2;n++){
		dst[2*n+0]=getHexChar((p[n]&0xf0)>>4);
		dst[2*n+1]=getHexChar((p[n]&0x0f))   ;
	}
	dst[2*n+0]=0;
}

/**
 * Desempaquetar nibbles con signo
 */
long updcbcs(uchar *p, long nnibb){
	long k,n=nnibb/2,ret=0;
	char signo= ((p[n-1]&0x0B)==0xB) ? -1 : 1 ;
	for(k=0;k<n;k++){
		ret*=10;
		ret+= ((p[k]&0xf0)>>4); 
		if(k!=n-1){
			ret*=10;
			ret+= (p[k]&0x0f);
		}
	}
	return ret*signo;
}

void updcbcs(uchar *p, long nnibb, char *dst){
	long n=nnibb/2-1,c=0;
	memset(dst,0,nnibb+1);
	if( (p[n]&0x0f)==0x0B )
		p[c++]='-';
	for(;c<=n;c++){
		dst[2*c+0]=getHexChar((p[c]&0xf0)>>4);
		if(c!=n)
			dst[2*c+1]=getHexChar((p[c]&0x0f))   ;
	}
}

/**
 * Desempaquetar Binarios
 */
long upbinss(uchar *p, long bytes){		//Sin Signo
	long ret,pos=1;
	ret = p[0];
	while(--bytes){
		ret = ret << 8;
		ret+= p[pos++];
	}
	return ret;
}

long upbincs(uchar *p, long bytes){
	long ret,pos=1,nb=bytes;
	ret = p[0];
	while(--bytes){
		ret = ret << 8;
		ret+= p[pos++];
	}
	if( p[0] & 0x80 )
		ret-= ( 2L << (nb*8-1) );
	return ret;
}


