
#ifndef __VARIABLE_H__
#define __VARIABLE_H__

#include "dimens.h"

class CCam;
class CCur;

class CVar : public CObj {
	private:
		char*	nomvar;
		CDim*	ninsta;
	public:
		CVar()	{ 
			nomvar=(char*)NULL; 
		};
		CVar(char *c, CDim* d=(CDim*)NULL)	{ 
			nomvar=c;
			ninsta=d;
		};
		~CVar()	{
			if(nomvar)
				free(nomvar);
			delete ninsta;
		}
		char*	getNom	()			{ return nomvar; }
		void	setNom	(char* n)	{ nomvar=n; }
		CDim*	getIns	(void)		{ return ninsta; }
		void	setIns	(CDim* c)	{ ninsta=c; }
		long	getTam	(CCur *c=(CCur*)NULL);

		void	dump	(char *s=(char*)NULL, FILE *fp=stdout);

friend bool operator==(const CCam&,const CCam&);
friend bool operator!=(const CCam&,const CCam&);
friend bool operator< (const CCam&,const CCam&);
friend bool operator> (const CCam&,const CCam&);
};

#endif
