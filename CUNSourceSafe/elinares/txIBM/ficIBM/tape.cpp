
#include "tape.h"
#ifdef UNIX
#include <sys/types.h>
#include <sys/mtio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#endif

static char *tblk[]={
	"VOL1",
	"DITTOTMDITTOTMDITTOTMDITTOTMDITTOTMDITTOTMDITTOTMDITTOTMDITTOTMDITTOTMDITTOTMDIT",
	"HDR1",
	"HDR2",
	"EOF1",
	"EOF2",
	"EOV1",
	"EOV2",
	""
};
CBlk::CBlk(long os, long s){
	size=s;
	offset=os;
}
long CTReader::tipoBlk(long nb){
	long i=0;
	char s[81];
	ReadBlock(s,nb,0,80);
	se2a((uchar*)s,(uchar*)s,80);
	s[80]=0;
	for(i=0;tblk[i][0];i++)
		if( !strncmp(s,tblk[i],strlen(tblk[i])) )
			break;
	if( tblk[i][0] )
		return i+1;
	if( !memcmp(s+4," \xAC  VOL1",8) )
		return TAPE_IVO1;
	return 0;
}
void CTReader::fnameBlk( long nb, char *fna ){
	char s[81];
	ReadBlock(s,nb,0,70);
	se2a((uchar*)s,(uchar*)s,80);
	s[21]=0;
	strcpy(fna,s+4);
}
CTReader::CTReader(char *fnam, long szcmsv){
	tipo=fromFile;
	inner=this;
	fname=strdup(fnam);
	struct stat st1;
	if( stat(fnam,&st1) ){
		perror(fname); 
		exit(-1);
	}
	st_size=st1.st_size;
	fp=fopen(fname,"rb");
	szCMSV=szcmsv;
	curpos=0;
	nblk=0;
	fixedtam=0;
	fixedblk=NULL;
	blk=NULL;
}
CTReader::CTReader(){
	tipo=fromConBlk;
	nblk=0;
	blk=NULL;
	fixedtam=0;
	fixedblk=NULL;
	curpos=0;
	inner=this;
	fname=NULL;
	fp=NULL;
	szCMSV=0;
	st_size=0;
}
CTReader::CTReader(CTReader* cr){
	tipo=cr->gettipo();
	nblk=0;
	blk=NULL;
	fixedtam=0;
	fixedblk=NULL;
	curpos=0;
	inner=cr->getinner();
	fname=NULL;
	fp=NULL;
	szCMSV=0;
	st_size=0;
	fixedblk=NULL;
}
CTReader::CTReader( CTReader* cr, long tr, long szt ){
	long bsz;
	blk=NULL;
	nblk=0;
	fixedtam=tr;
	fixedblk=NULL;
	inner=cr;
	curpos=0;
	tipo=fromConBlk;
	if( fixedtam>0 ){		//Tama�o fijo
		nblk= cr->getTam()/fixedtam;
		fixedblk=new CBlk(0,0);
		fname=strdup(cr->getnom());
	}else{					//Tama�o variable
		fixedtam=0;
		while( (bsz=cr->readSize())!=EOF ){
			if( bsz==0 )
				break;
			cr->Read(NULL,-1,szt==2 ? 0 : 2);
			AddBlk( new CBlk(cr->tell(),szt==2 ? bsz : bsz-4 ) );
			cr->Read(NULL,-1,szt==2 ? bsz : bsz-4 );
		}
		fname=NULL;
	}
	st_size=0;
	szCMSV=0;
	fp=NULL;
}
CTReader::CTReader( FILE *fp2 ){
	long sl;
	fread(&tipo,1,sizeof(tipo),fp2);
	if(tipo==fromFile){
		fread(&sl,1,sizeof(sl),fp2);
		fname=(char*)malloc(sl);
		fread(fname,1,sl,fp2);
		if(!(fp=fopen(fname,"rb"))){
			perror(fname); exit(-1);
		}
		curpos=0;
		fread(&szCMSV,1,sizeof(szCMSV),fp2);
		inner=this;
	}else if(tipo==fromConBlk){
		long i,o,s,aux;
		fixedblk=NULL;
		fread(&aux,1,sizeof(aux),fp2);
		fread(&fixedtam,1,sizeof(fixedtam),fp2);
		curpos=0;
		blk=NULL;
		if(fixedtam<=0){
			nblk=0;
			for(i=0;i<aux;i++){
				fread(&s,1,sizeof(s),fp2);
				fread(&o,1,sizeof(o),fp2);
				AddBlk( new CBlk(o,s) );
			}
		}else{
			fread(&sl,1,sizeof(sl),fp2);
			fname= sl ? (char*)malloc(sl) : NULL ;
			if(sl)
				fread(fname,1,sl,fp2);
			if(!(fp=fopen(fname,"rb"))){
				perror(fname); exit(-1);
			}
			nblk=aux;
			curpos=0;
			fread(&szCMSV,1,sizeof(szCMSV),fp2);
			fixedblk=new CBlk( 0,0);
		}
		fread(&sl,1,sizeof(sl),fp2);
		if( !sl )
			inner=new CTReader(fp2);
		else
			inner=this;
	}
}
void CTReader::Serialize( FILE *fp2 ){
	long sl;
	fwrite(&tipo,1,sizeof(tipo),fp2);
	if( tipo==fromFile){
		//Grabar Nombre de archivo
		sl=strlen(fname)+1;
		fwrite(&sl,1,sizeof(sl),fp2);
		fwrite(fname,1,sl,fp2);
		fwrite(&szCMSV,1,sizeof(szCMSV),fp2);
	}else{
		long i,o,s;
		fwrite(&nblk,sizeof(nblk),1,fp2);
		fwrite(&fixedtam,sizeof(fixedtam),1,fp2);
		if(fixedtam<=0){
			for(i=0;i<nblk;i++){
				o=blk[i]->getoffset();
				s=blk[i]->getsize();
				fwrite( &s,1,sizeof(s),fp2);
				fwrite( &o,1,sizeof(o),fp2);
			}
		}else{
			sl= fname ? strlen(fname)+1 : 0 ;
			fwrite(&sl,1,sizeof(sl),fp2);
			if(sl) 
				fwrite(fname,1,sl,fp2);
			fwrite(&szCMSV,1,sizeof(szCMSV),fp2);
		}
		sl= (this==inner) ? 1 : 0;
		fwrite(&sl,1,sizeof(sl),fp2);
		if( !sl )
			inner->Serialize( fp2 );
	}
}
CTReader::~CTReader(){
	if(fname) free(fname);
	if(fp) fclose(fp);
}
long CTReader::OfSz2Blks(long os, long sz, long *ini, long *fin){
	long bi,oi,si,bf,of,sf,szparcial;
	//Situarnos en el primer bloque segun el os
	for(bi=0,szparcial=0;
		bi<nblk && os>=szparcial+blk[bi]->getsize(); 
		szparcial+=blk[bi++]->getsize())
		;
	if( bi==nblk )
		return EOF;
	oi = os - szparcial ;
	si = blk[bi]->getsize() - oi < sz ? blk[bi]->getsize() - oi : sz;
	//Ahora en el final segun os+s
	for(bf=bi; 
		os+sz>szparcial+blk[bf]->getsize() && bf<nblk ; 
		szparcial+=blk[bf++]->getsize())
		;
	of = 0 ;
	sf = os+sz-szparcial;
	ini[0]=bi; ini[1]=oi; ini[2]=si;
	fin[0]=bf; fin[1]=of; fin[2]=sf;
	return szparcial;
}
long CTReader::Read(char *c, long o, long s){
	long pos,ret;
	//Posicionar si o!=-1 (si es igual, entonces es continuar)
	if( o!=-1 )
		curpos=o;
	if(tipo==fromFile){
		if(szCMSV)
			fseek(fp,(curpos/(szCMSV-5))*szCMSV+5+curpos%(szCMSV-5),SEEK_SET);
		else
			fseek(fp,curpos,SEEK_SET);
		if(c){
			if(szCMSV){
				for(pos=0; pos<s && !feof(fp); pos++){
					c[pos]=fgetc(fp);
					curpos++;
					if( szCMSV && ftell(fp)%szCMSV==0 ) 
						fseek(fp,5,SEEK_CUR);
				}
			}else{
				curpos+=fread(c,1,s,fp);
			}
		}else{
			curpos+=s;
		}
		ret = feof(fp) ? EOF : s;
	}else if(tipo==fromConBlk){
		long b,ini[3],fin[3],szparcial;
		if( OfSz2Blks(curpos,s,&ini[0],&fin[0])!=EOF ){
			//Efectuamos lectura sobre los bloques.
				//Bloque inicial
			szparcial=inner->Read(c,blk[ini[0]]->getoffset()+ini[1],ini[2]);
				//Bloques completos intermedios
			for(b=ini[0]+1;b<fin[0];szparcial+=blk[b++]->getsize())
				inner->Read(c+szparcial,blk[b]->getoffset(),blk[b]->getsize());
				//Bloque final si es dto del inicial
			if(ini[0]!=fin[0])
				szparcial+=inner->Read(c+szparcial,blk[fin[0]]->getoffset()+fin[1],fin[2]);
			curpos+=szparcial;
		}else{
			szparcial=EOF;
		}
		ret=szparcial;
	}
	return ret;
}
long CTReader::ReadBlock( char *out, long nb, long oinblk, long szinblk ){
	return inner->Read(out,(*this)[nb]->getoffset()+oinblk, szinblk==-1 ? (*this)[nb]->getsize() : szinblk);
}

long CTReader::readSize(void){
	long tam;
	uchar c;
	if(Read((char*)&c,-1,1)==EOF)
		return EOF;
	tam=256*c;
	if(Read((char*)&c,-1,1)==EOF)
		return EOF;
	return tam+c;	
}
void CTReader::AddBlk( CBlk *b ){
	blk=(CBlk**)realloc(blk,(1+nblk)*sizeof(CBlk*));
	blk[nblk]=b;
	nblk++;
}
long CTReader::getTam(void){
	if( tipo==fromConBlk ){
		long i,sz=0;
		for(i=0;i<nblk;i++)
			sz+=blk[i]->getsize();
		return sz;
	}
	return st_size;
}
CBlk* CTReader::operator[](long i){ 
	if(fixedtam<=0)
		return i<nblk ? blk[i] : NULL ; 
	else{
		fixedblk->setparm(i*fixedtam,fixedtam);
		return fixedblk;
	}
}
CTFile::CTFile(){
	nhijos=0;
	hijos=NULL;
	tr=NULL;
	fname=NULL;
}
CTFile::CTFile(char *fn, CTReader *c){
	tr=c;
	nhijos=0;
	hijos=NULL;
	fname=strdup(fn);
}
CTFile::CTFile(FILE *fp2){
	long sl,i;
	fread(&sl,1,sizeof(sl),fp2);
	fname=(char*)malloc(sl);
	fread(fname,1,sl,fp2);
	fread(&nhijos,1,sizeof(nhijos),fp2);
	tr=new CTReader( fp2 );
	if( nhijos>0 ){
		hijos=(CTFile**)malloc(nhijos*sizeof(CTFile));
		for(i=0;i<nhijos;i++)
			hijos[i]=new CTFile( fp2 );
	}else{
		hijos=NULL;
	}
}
void CTFile::Serialize( FILE *fp2 ){
	long sl,i;
	//Grabar Nombre de archivo
	sl=strlen(fname)+1;
	fwrite(&sl,1,sizeof(sl),fp2);
	fwrite(fname,1,sl,fp2);
	fwrite(&nhijos,1,sizeof(nhijos),fp2);
	tr->Serialize( fp2 );
	for(i=0;i<nhijos;i++)
		hijos[i]->Serialize( fp2 );
}
CTFile* CTFile::AddHijo( CTFile *hijo ){
	hijos=(CTFile**)realloc(hijos,(1+nhijos)*sizeof(CTFile*));
	hijos[nhijos++]=hijo;
	return hijo;
}
void CTFile::Parse(){
	char     fname[128];
	CTReader  *cb1=NULL;
	long     i=0;

	if( tr->getnblk()==0 )
		return;
	switch( tr->tipoBlk(0) ){
	case TAPE_VOL1:
		i++;
		while( i<tr->getnblk() && tr->tipoBlk(i)==TAPE_HDR1){
			tr->fnameBlk(i,fname);
			i+=2;
			if(tr->tipoBlk(i)==TAPE_TMA)
				i++;
			cb1=new CTReader(tr);
			while( i<tr->getnblk() && tr->tipoBlk(i)!=TAPE_EOF1 && tr->tipoBlk(i)!=TAPE_TMA)
				cb1->AddBlk( (*tr)[i++] );
			AddHijo( new CTFile(fname,cb1) )->Parse();
			if( i<tr->getnblk() ){
				if( tr->tipoBlk(i)==TAPE_EOF1 )
					i+=2;
				else
					i+=4;
			}else{
				break;
			}
		}
		break;
	case TAPE_IVO1:
		tr = new CTReader( tr, 0, 4);
		tr = new CTReader( tr, 0, 4);
		Parse();
		break;
	default:
		return;
	}
}
void CTFile::List(char *ppar, long level){
	char aux[128],*q;
	long i;
	memset(aux,' ',127);
	sprintf(aux+level,"%s",*ppar ? ppar : "");
	printf("%s (%8ld,%10ld) %s\n",aux,getnblk(),getTam(),fname);
	q=aux+strlen(aux);
	for(i=0;i<nhijos;i++){
		sprintf(q,".%02d",i+1);
		hijos[i]->List(aux,level+1);
	}
}
void CTFile::Extract(void){
	FILE *fp;
	char *c=(char*)malloc(32*1024);
	long sz=getTam(),szb,i;

	if( !(fp=fopen(fname,"wb")) ){
		perror(fname);
		exit(-1);
	}
	//Hacer rewind
	Read(NULL,0,0);
	szb=sz>>15;
	for(i=0;i<szb;i++){
		Read(c,-1,32*1024);
		fwrite(c,32,1024,fp);
	}
	Read(c,-1,sz % (32*1024) );
	fwrite(c,1,sz % (32*1024),fp);
	fclose(fp);
	free(c);
}
CTape::CTape(char *fna,long szcmsv,char *TapeDev){
	struct stat buf1,buf2;
	char info[256];
	if( szcmsv>0 )
		szCMSV=szcmsv-1;		//OJO A ESTO!!!!!!!!!!!!!!!!!!!!!!!!
	else
		szCMSV=0;
	fname=strdup(fna);
	if( !TapeDev && stat(fname,&buf1) ){
		perror(fname);
		exit(-1);
	}
	strcat(strcpy(info,fname),".info");
	if( stat(info,&buf2) ){
		GenInfo(TapeDev);
		needsaveinfo=1;
	}else if( buf1.st_ctime>buf2.st_ctime){
		GenInfo(TapeDev);
		needsaveinfo=1;
	}else{
		LoadInfo();
		needsaveinfo=0;
	}
}
void CTape::Serialize(FILE *fp2){
	long sl;
	//Grabar Nombre de archivo
	sl=strlen(fname)+1;
	fwrite(&sl,1,sizeof(sl),fp2);
	fwrite(fname,1,sl,fp2);
	fwrite(&szCMSV,1,sizeof(szCMSV),fp2);
	root->Serialize(fp2);
}
CTape::~CTape(){
	if(needsaveinfo)
		SaveInfo();
}
void CTape::writeBlock(FILE *fp, short sz, char *s){
	char *s2=(char*)&sz;
	fwrite(s2+1,1, 1,fp);
	fwrite(s2+0,1, 1,fp);
	fwrite(s   ,1,sz,fp);
}
void CTape::parseHDR2(char *buf, short &sz, long &nb){
	char s[10];
	se2a((uchar*)buf,(uchar*)buf,80);
	memcpy(s,buf+05,5);
	s[5]=0;
	sz=atoi(s);
	memcpy(s,buf+10,5);
	s[5]=0;
	nb=atoi(s);
}
int CTape::isEOV(char *buf){
	se2a((uchar*)buf,(uchar*)buf,80);
	return !strncmp(buf,tblk[7],strlen(tblk[7]));
}
void CTape::GenInfo(char *TapeDev){
	if( TapeDev ){
		#ifdef UNIX
		FILE *fp11=fopen(fname,"wb");
		char *buffer=(char*)malloc(MAXTAMBLK);
		CTapeDev ctd(TapeDev);
		long szb;

		ctd.rebobina();
		do{
			szb=ctd.getBlock(buffer);
			if(szb>0)
				writeBlock(fp11,szb,buffer);
		}while(szb>=0);
		
		free(buffer);
		fclose(fp11);
		szCMSV=0;
		#else
		puts("No puedo manipular 'Tapes' desde WinNT");
		exit(-1);
		#endif
	}
	CTReader *ctr1=new CTReader(fname,szCMSV);		//Lector de datos
	CTReader *cblk=new CTReader(ctr1,0,2) ;
	root=new CTFile(fname,cblk);
	root->Parse();
}
void CTape::LoadInfo(void){
	long sl;
	FILE *fp2;
	char aux[1024];

	strcat(strcpy(aux,fname),".info");
	fp2=fopen(aux,"rb");
	fread(&sl,1,sizeof(sl),fp2);
	fname=(char*)malloc(sl);
	fread(fname,1,sl,fp2);
	fread(&szCMSV,1,sizeof(szCMSV),fp2);
	root=new CTFile( fp2 );
	fclose( fp2 );
}
void CTape::SaveInfo(void){
	FILE *fp2;
	char aux[1024];
	strcat(strcpy(aux,fname),".info");
	fp2=fopen(aux,"wb");
	Serialize(fp2);
	fclose(fp2);
}
void CTape::splitpath(char *path, ulong *sdepth, ulong *depth){
	char gpath[1024],*aux;
	ulong i;

	*sdepth=0;
	strcpy(gpath,path);
	gpath[strlen(gpath)]=0;	//Poner doble cero final
	for(i=0;i<strlen(path);i++)
		if( gpath[i]=='.' ){
			gpath[i]=0;
			(*sdepth)++;
		}
	(*sdepth)++;
	aux=gpath;
	for(i=0;i<*sdepth;i++){
		depth[i]=atol( aux );
		aux+=strlen(aux)+1;
	}
}
CTFile* CTape::getFile(char *path){
	ulong depth[20],sdepth=0,i;
	CTFile *tf;
	splitpath(path,&sdepth,depth);
	for(tf=root,i=1;i<sdepth;i++)
		tf=(*tf)[ depth[i]-1 ];
	return tf;
}
void CTape::List(void){
	printf("Identificacion   (NumReg, FileSize)   Nombre\n");
	root->List("1",0);
}
CTapeDev::CTapeDev(char *device){
	#ifdef UNIX
		iodevice=strdup(device);
		if( -1==(filedescriptor=open(iodevice,O_RDONLY)) ){
			perror(iodevice);
			exit(-1);
		}
		setBlockSize(0);
	#else
		printf("No puede manejar Cartuchos IBM desde WinNT!!");
		exit(-1);
	#endif
}
CTapeDev::~CTapeDev(){
	#ifdef UNIX
		close(filedescriptor);
		free(iodevice);
	#endif
}
int CTapeDev::setBlockSize(int size){
	#ifdef UNIX
		mtop mt_cmd;
		mt_cmd.mt_op=MTSETBLK;
		mt_cmd.mt_count=size;		//Tama�o de bloque variable
		if( -1==ioctl(filedescriptor,MTIOCTOP,&mt_cmd)){
			perror(iodevice);
			exit(-1);
		}
		blocksize=size;
	#endif
	return 0;
}
int CTapeDev::getBlock(char *out){
	#ifdef UNIX
	if( blocksize ){
		return read(filedescriptor,out,blocksize);
	}else{
		return read(filedescriptor,out,MAXTAMBLK);
	}
	#else
		return -1;
	#endif
}
void CTapeDev::nextFile(){
	#ifdef UNIX
		mtop mt_cmd;
		mt_cmd.mt_op=MTFSF;
		mt_cmd.mt_count=1;		
		if( -1==ioctl(filedescriptor,MTIOCTOP,&mt_cmd)){
			perror(iodevice);
			exit(-1);
		}
	#endif
}
void CTapeDev::rebobina(void){
	#ifdef UNIX
		mtop mt_cmd;
		mt_cmd.mt_op=MTREW;
		mt_cmd.mt_count=0;
		if( -1==ioctl(filedescriptor,MTIOCTOP,&mt_cmd)){
			perror(iodevice);
			exit(-1);
		}
	#endif
}
