
#include "variab.h"
#include "cursor.h"

void CVar::dump(char *s, FILE *fp){
	char aux[200];
	if(nomvar)
		sprintf(aux,"%s",nomvar);
	else
		sprintf(aux,"Error:CVar sin nombre");
	if(ninsta)
		ninsta->dump(aux+strlen(aux),fp);
	if(s)
		strcpy(s,aux);
	else
		fprintf(fp,"%s",aux);
}


long
CVar::getTam(CCur *c){
	return ninsta->getTam(c);
}

