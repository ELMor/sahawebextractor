

#ifndef __FMANIP_H__
#define __FMANIP_H__


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "defgen.h"

void LeeLong(FILE* fp, long* lp);
void LeeLoAr(FILE* fp, long num, long* ar);
void LeeStri(FILE* fp, char *str);

void EscLong(FILE* fp, long lp);
void EscLoAr(FILE* fp, long num, long* ar);
void EscStri(FILE* fp, char *str);


#endif