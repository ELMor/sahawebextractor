

#include <stdio.h>
#include <stdlib.h>
#include "tablas.h"

FILE *fpExport;

CLisTab ltab;

int yyparse(void);

int main(int argc, char *argv[]){

	if( argc!=2) { puts("Uso: TablaMem tablas.txt"); return 0; }

	fpExport=fopen(argv[1],"rt");

	if(!fpExport){ perror(argv[1]); return -1; }

	//Carga de las tablas desde fichero IBM
	yyparse();
	fclose(fpExport);

	//Grabar convertidas y ordenadas
	ltab.graba("tablas.bin");
	FILE *fpo=fopen("tablas.log","wt");
	ltab.Dump(fpo);
	fclose(fpo);
	return 0;
}
