
#include "defgen.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void parg(int argc, char *argv[], FILE *&fp1, FILE *& fp2, bool& edit, bool &check){
		//Comprobar argumentos
	if( argc<3 ){
		puts("Uso:");
		printf("%s 'Fichero PL1' 'Fichero Salida' [-opc] [-chk]",argv[0]);
		exit(0);
	}
	//Abrir archivo de texto PLI
	if( NULL==(fp1=fopen(argv[1],"rt"))){
		perror(argv[1]);
		exit(-1);
	}
	if(NULL==(fp2=fopen(argv[2],"wt"))){
		perror(argv[2]);
		exit(-1);
	}
	int i;
	for(i=3; i<argc;i++){
		if(!stricmp(argv[i],"-opc"))
			edit=true;
		if(!stricmp(argv[i],"-chk"))
			check=true;
	}
}

