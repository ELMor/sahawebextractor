
#include "ctlserv.h"
#include "decompile.h"
#include "srvsocks.h"
#include "logging.h"

extern SERVICE_STATUS          ssStatus;       
extern SERVICE_STATUS_HANDLE   sshStatusHandle;
extern DWORD                   dwErr ;
extern BOOL                    bDebug ;
extern TCHAR                   szErr[256];


void CmdDebugService(int argc, char ** argv)
{
    DWORD dwArgc;
    LPTSTR *lpszArgv;

#ifdef UNICODE
    lpszArgv = CommandLineToArgvW(GetCommandLineW(), &(dwArgc) );
#else
    dwArgc   = (DWORD) argc;
    lpszArgv = argv;
#endif

    _tprintf(TEXT("Depurando %s.\n"), TEXT(SZSERVICEDISPLAYNAME));

    SetConsoleCtrlHandler( ControlHandler, TRUE );

    ServiceStart( dwArgc, lpszArgv );
}


