#include <windows.h>
#include "logging.h"
#include "dump.h"
#include "setupqueue.h"
#include "ctlserv.h"
#include "srvsocks.h"
#include "registrar.h"
#include "version.h"

extern BOOL bDebug;

//En este caso cmd es el nombre de la CPU remota
int Ping(char *cname, char *version){
	struct sockaddr_in server;
	unsigned long inetaddr;
	struct hostent *hep;
	SOCKET sock;
	WSADATA wsaData;

	WSAStartup(0x202,&wsaData);
	if(NULL==(hep=gethostbyname(cname))){
		if(INADDR_NONE==(inetaddr=inet_addr(cname))){
			int ret=WSAGetLastError();
			WSACleanup();
			return ret;
		}
		hep=gethostbyaddr((const char*)&inetaddr,4,AF_INET);
	}
	memset(&server,0,sizeof(server));
	memcpy(&(server.sin_addr),hep->h_addr,hep->h_length);
	server.sin_family = hep->h_addrtype;
	server.sin_port = htons(DEFAULT_SERVICE_PORT);
	sock=socket(AF_INET,SOCK_STREAM,0);
	if(connect(sock,(struct sockaddr*)&server,sizeof(server))){
		int ret=WSAGetLastError();
		WSACleanup();
		return ret;
	}else{
		unsigned long longitud;
		char buf[256];
		longitud=1+strlen(szRsPing);
		send(sock,(const char*)&longitud,sizeof(longitud),0);
		send(sock,szRsPing,longitud,0);
		recv(sock,buf,256,0);
		closesocket(sock);
		WSACleanup();
		if(version)
			strcpy(version,buf);
		if(stricmp(SERVICE_VERSION,buf)<0)
			return NEWER_REMOTE_VERSION;
		else if(stricmp(SERVICE_VERSION,buf)>0)
			return OLDER_REMOTE_VERSION;
		else
			return SAME_REMOTE_VERSION;
	}
}

int RsPing(SOCKET sock, char *cmd){
	char buf[128];
	sprintf(buf,"%s",SERVICE_VERSION);
	send(sock,buf,strlen(buf)+1,0);
	closesocket(sock);
	return 0;
}
