
#include <windows.h>
#include "logging.h"
#include "dump.h"
#include "setupqueue.h"
#include "ctlserv.h"
#include "srvsocks.h"
#include "registrar.h"
#include "filecrc32.h"
#include <time.h>

extern BOOL bDebug;

void SendFile(SOCKET CSock, char* locfname){
	HANDLE hFile;
	send(CSock,szSendFile,1+strlen(szSendFile),0);	//Notifica que se va a enviar.
	//Envio de la fecha de modificacion del archivo
	struct _stat t;
	_stat(locfname,&t);
	char tiempo[128];
	sprintf(tiempo+1,"%ld",t.st_mtime);
	*tiempo=strlen(tiempo+1);
	send(CSock,tiempo,2+*tiempo,0);
	//Uso de TransmitFile de Windows NT (NO ES ESANDAR)
	hFile=CreateFile(locfname,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,NULL,NULL);
	TransmitFile(CSock,hFile,0,0,NULL,NULL,TF_DISCONNECT);
	CloseHandle(hFile);
}

//Estructura es QrGetFile,PathLocal,FileName,Tamano
int QrGetFile(SOCKET CSock, char *cmd){
	long *tamficrem,tamfic;
	unsigned long crc32,remcrc32;
	char locfname[512],*path,*fname;
	struct _stat st1;
	//Path y filename
	path=             cmd  +strlen(cmd)  +1;
	fname=            path +strlen(path) +1;
	tamficrem=(long*)(fname+strlen(fname)+1);
	strcat(strcat(strcpy(locfname,path),"\\"),fname);
	//Obtenemos las fechas del archivo (para mantenerlas en la copia remota).
	if(_stat(locfname,&st1)!=0){
		send(CSock,szFileNotFound,1+strlen(szFileNotFound),0);
		return 0;		//Archivo no existe aqu� - Avisar al administrador (Seguridad)
	}else{
		tamfic=st1.st_size;
		if(tamfic==*tamficrem){	//Comprobar el CRC32 de los archivos
			//Ordena al cliente que calcule su CRC32
			send(CSock,szFileCRC32,1+strlen(szFileCRC32),0);
			GetFileCRC32(locfname,&crc32);
			recv(CSock,(char*)&remcrc32,4,0);	//Recibir el CRC del remoto
			if(crc32!=remcrc32)			//CRC32 son distintos, enviar fichero.
				SendFile(CSock,locfname);
			else
				send(CSock,szRsFileOK,1+strlen(szRsFileOK),0);
		}else{					
			SendFile(CSock,locfname);
		}
	}
	return 0;
}
