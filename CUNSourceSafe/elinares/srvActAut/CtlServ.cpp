/*
 * Fichero CtlServ.cpp
 */

#include "ctlserv.h"
#include "decompile.h"
#include "srvsocks.h"
#include "logging.h"
#include "version.h"

SERVICE_STATUS          ssStatus;       
SERVICE_STATUS_HANDLE   sshStatusHandle;
DWORD                   dwErr = 0;
BOOL                    bDebug = FALSE;
TCHAR                   szErr[256];

void _CRTAPI1 main(int argc, char **argv)
{
	char *aux,version[32];
	int pret;
    SERVICE_TABLE_ENTRY dispatchTable[] = {
        { TEXT(SZSERVICENAME), (LPSERVICE_MAIN_FUNCTION)service_main },
        { NULL, NULL }
    };

	//Correccion del BUG en debug/Release: en debug argv[0] es un path completo
	if(NULL!=(aux=strrchr(argv[0],'\\')))
		strcpy(argv[0],aux+1);
    if ( (argc > 1) &&  ((*argv[1] == '-') || (*argv[1] == '/')) ){
		char szNC[32],*pszNC=szNC,szJobName[64];
		strcpy(szNC,argc>2 ? argv[2] : getenv("COMPUTERNAME"));
		strcpy(szJobName, argc>3 ? argv[3] : "" );
        if ( _stricmp( "i",         argv[1]+1 ) == 0 ){			//Instalar
            if(CmdInstallService(pszNC,argv[0]))
				CmdStartService(pszNC);
        }else if ( _stricmp( "r",         argv[1]+1 ) == 0 ){	//Reinstalar
            CmdRemoveService(pszNC);
            if(CmdInstallService(pszNC,argv[0]))
				CmdStartService(pszNC);
		}else if ( _stricmp( "b",    argv[1]+1 ) == 0 ){		//Borrar servicio
            CmdRemoveService(pszNC);
		}else if ( _stricmp( "l",    argv[1]+1 ) == 0 ){		//Borrar Log
            CmdRemoveLog(pszNC,szJobName);
        }else if ( _stricmp( "d",     argv[1]+1 ) == 0 ){		//Depurar
            bDebug = TRUE; CmdDebugService(argc, argv);
        }else if ( _stricmp( "j",       argv[1]+1 ) == 0 ){		//Enviar job
			exit( CmdSendJob( argv[2] ) ) ;
        }else if ( _stricmp( "p",       argv[1]+1 ) == 0 ){		//Ping
			pret=Ping(szNC,version);
			if( pret>1024 )
				printf("%s no responde al ping\n",szNC);
			else{
				printf("Ping a %s OK, version %s es %s a esta (%s)\n",
					szNC,
					version,
					pret==SAME_REMOTE_VERSION ? "igual" : 
						(pret==OLDER_REMOTE_VERSION ? "ANTERIOR" : "POSTERIOR"),
					SERVICE_VERSION);
			}
        }else{
			Ayuda(argv[0]);
        }
        exit(0);
    }
    // Si no se pasaron argumentos, debe ser que el Service Control Manager
    // debe estar arrancando el servicio, por lo que llamamos a
	{
		char szErr[SIZESENDDATA];
		if (!StartServiceCtrlDispatcher(dispatchTable))
			printf("Fallo StartServiceCtrlDispatcher <%s>.",GetLastErrorText(szErr,256));
	}
}

void WINAPI service_main(DWORD dwArgc, LPTSTR *lpszArgv)
{
    sshStatusHandle = RegisterServiceCtrlHandler( TEXT(SZSERVICENAME), service_ctrl);
    if (!sshStatusHandle)
        goto cleanup;
    ssStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    ssStatus.dwServiceSpecificExitCode = 0;
    if (!ReportStatusToSCMgr(SERVICE_START_PENDING,NO_ERROR,3000))
        goto cleanup;
    ServiceStart( dwArgc, lpszArgv );
cleanup:
    if (sshStatusHandle)
        (VOID)ReportStatusToSCMgr(SERVICE_STOPPED,dwErr,0);
    return;
}


VOID WINAPI service_ctrl(DWORD dwCtrlCode)
{
    switch(dwCtrlCode)
    {
        case SERVICE_CONTROL_STOP:
            ReportStatusToSCMgr(SERVICE_STOP_PENDING, NO_ERROR, 0);
            ServiceStop();
            return;
        case SERVICE_CONTROL_INTERROGATE:
            break;
        default:
            break;

    }
    ReportStatusToSCMgr(ssStatus.dwCurrentState, NO_ERROR, 0);
}

BOOL ReportStatusToSCMgr(DWORD dwCurrentState,DWORD dwWin32ExitCode,DWORD dwWaitHint)
{
    static DWORD dwCheckPoint = 1;
    BOOL fResult = TRUE;
    if ( !bDebug ) // when debugging we don't report to the SCM
    {
        if (dwCurrentState == SERVICE_START_PENDING)
            ssStatus.dwControlsAccepted = 0;
        else
            ssStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;
        ssStatus.dwCurrentState = dwCurrentState;
        ssStatus.dwWin32ExitCode = dwWin32ExitCode;
        ssStatus.dwWaitHint = dwWaitHint;

        if ( ( dwCurrentState == SERVICE_RUNNING ) ||
             ( dwCurrentState == SERVICE_STOPPED ) )
            ssStatus.dwCheckPoint = 0;
        else
            ssStatus.dwCheckPoint = dwCheckPoint++;
        if (!(fResult = SetServiceStatus( sshStatusHandle, &ssStatus))) {
            AddToMessageLog(TEXT("SetServiceStatus"));
        }
    }
    return fResult;
}

BOOL WINAPI ControlHandler ( DWORD dwCtrlType )
{
    switch( dwCtrlType )
    {
        case CTRL_BREAK_EVENT:  // use Ctrl+C or Ctrl+Break to simulate
        case CTRL_C_EVENT:      // SERVICE_CONTROL_STOP in debug mode
            _tprintf(TEXT("Deteniendo %s.\n"), TEXT(SZSERVICEDISPLAYNAME));
            ServiceStop();
            return TRUE;
            break;

    }
    return FALSE;
}

LPTSTR GetLastErrorText( LPTSTR lpszBuf, DWORD dwSize )
{
    DWORD dwRet;
    LPTSTR lpszTemp = NULL;

    dwRet = FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |FORMAT_MESSAGE_ARGUMENT_ARRAY,
                           NULL,
                           GetLastError(),
                           LANG_NEUTRAL,
                           (LPTSTR)&lpszTemp,
                           0,
                           NULL );

    // supplied buffer is not long enough
    if ( !dwRet || ( (long)dwSize < (long)dwRet+14 ) )
        lpszBuf[0] = TEXT('\0');
    else
    {
        lpszTemp[lstrlen(lpszTemp)-2] = TEXT('\0');  //remove cr and newline character
        _stprintf( lpszBuf, TEXT("%s (0x%x)"), lpszTemp, GetLastError() );
    }

    if ( lpszTemp )
        LocalFree((HLOCAL) lpszTemp );

    return lpszBuf;
}

void Ayuda(char *arg){
	puts("");
	printf("%s -i [CPU_NAME]       |Instalar y arrancar el servicio.\n", arg );
	printf("%s -r [CPU_NAME]       |Borrar, Instalar y arrancar el servicio.\n", arg );
	printf("%s -b [CPU_NAME]       |Detener y borrar el servicio.\n", arg );
	printf("%s -l [CPU_NAME] [JOB] |Purgar el archivo de log.\n", arg );
	printf("%s -p [CPU_NAME]       |Ping a CPU_NAME o a local.\n", arg);
	printf("%s -j jobfile          |Ejecutar trabajo definido en jobfile.\n",arg);
	printf("%s -d <params>         |Ejecutar en modo consola (depuracion)\n", arg );
	printf("\n%s Version %s\n",arg,SERVICE_VERSION);
}
