/*
 * Fichero registrar.cpp
 */

#include <windows.h>
#include "registrar.h"
#include "ctlserv.h"
#include "logging.h"
#include "srvsocks.h"

//Lee del Registry los ficheros que han de ser registrados.
void ProcDelayedFic( void ){
	char fname[128],path[512],cmd[1024],jname[64],notsrv[64],cpuname[128];
	unsigned long size,tipo;
	HKEY MasterKey,FileKey;
	DWORD Result;
	int i;
	char lastJob[64];

	RegCreateKeyEx(HKEY_LOCAL_MACHINE,szPathReg,0,"Folder",REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,NULL,&MasterKey,&Result);

	strcpy(lastJob,"");

	size=128;
	for(i=0;ERROR_SUCCESS==RegEnumKeyEx(MasterKey,i,fname,&size,0,NULL,0,NULL);){
		RegCreateKeyEx(MasterKey,(const char *)fname,0,"File",
			REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&FileKey,&Result);
		
		size=128;
		RegQueryValueEx(FileKey,"path",0,&tipo,(unsigned char*)path,&size);

		size=1024;
		RegQueryValueEx(FileKey,"cmd" ,0,&tipo,(unsigned char*)cmd ,&size);

		size=64;
		RegQueryValueEx(FileKey,"job" ,0,&tipo,(unsigned char*)jname,&size);

		size=64;
		RegQueryValueEx(FileKey,"noti" ,0,&tipo,(unsigned char*)notsrv,&size);

		RegCloseKey( FileKey );

		LisAct lis;
		lis.jobname=strdup(jname);
		lis.notifysrv=strdup(notsrv);
		lis.cpu=(CPU*)malloc(sizeof(CPU));
		gethostname(cpuname,127);
		lis.cpu->cname=strdup(cpuname);

		if(stricmp(lastJob,jname)){
			SendMessageTo(&lis,"","","Post-Proceso tras REBOOT");
			strcpy(lastJob,jname);
		}

		RegistrarArchivos(&lis, fname,path,cmd,0);
		RegDeleteKey(MasterKey,fname);

		free(lis.cpu->cname);
		free(lis.cpu);
		free(lis.jobname);
		free(lis.notifysrv);

		size=128;
	}

	RegCloseKey( MasterKey );
}

void DelayFic( LisAct *plis, char *fname, char *path, char *cmd ){
	DWORD Result;
	HKEY MasterKey,FileKey;
	RegCreateKeyEx(HKEY_LOCAL_MACHINE,szPathReg,0,"Folder",REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,NULL,&MasterKey,&Result);
	RegCreateKeyEx(MasterKey,fname,0,"File",REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,NULL,&FileKey,&Result);
	RegSetValueEx( FileKey,"path",0,REG_SZ,(const unsigned char*)path,strlen(path)+1);
	RegSetValueEx( FileKey,"cmd" ,0,REG_SZ,(const unsigned char*)cmd ,strlen( cmd)+1);
	RegSetValueEx( FileKey,"job", 0,REG_SZ,(const unsigned char*)plis->jobname,strlen(plis->jobname)+1);
	RegSetValueEx( FileKey,"noti",0,REG_SZ,(const unsigned char*)plis->notifysrv,strlen(plis->notifysrv)+1);
	RegCloseKey  ( FileKey );
	RegCloseKey  ( MasterKey );
}

void __cdecl LlamarDllRegisterServer(void* parg){		
	FARPROC lpDllEntry= NULL;
	HINSTANCE hLib;
	char   **p;
	LisAct *plis;
	char   *path;
	char   *locfname;
	char   *fname;
	//Inicializacion de Sockets debe ser per-thread
	WSADATA wsaData;
	WSAStartup(0x202,&wsaData);

	p=(char**)(parg);
	plis=(LisAct*)p[0],path=p[1],locfname=p[2],fname=p[3];

	if(	NULL!=(hLib=LoadLibrary(locfname))
			&& //Se carga
		NULL!=(lpDllEntry=GetProcAddress(hLib,"DllRegisterServer"))    //Se puede
		){
		(*lpDllEntry)();												//Registrar
		FreeLibrary( hLib );
	}
	SendMessageTo(plis,path,fname,"DllRegisterServer O.K.");

	free(plis->cpu->cname);
	free(plis->cpu);
	free(plis->jobname);
	free(plis->notifysrv);
	free(plis);
	
	free(path);
	free(locfname);
	free(fname);

	free(parg);

	WSACleanup();

	_endthread();
}

void RegistrarArchivos(LisAct* plis, char *fname, char *path, char* cmd, int delay){
	char locfname[1024];
	char msg[512];

	strcat(strcat(strcpy(locfname,path),"\\"),fname);
	if( delay==0													&& //No pospuesto
		(	!stricmp(fname+strlen(fname)-4,".dll") || 
		    !stricmp(fname+strlen(fname)-4,".ocx")    )				&& //DLL u OCX
			stricmp(fname,"oc25.dll")								   //Esa DLL da problemas
	   ){
		char **p=(char**)malloc(4*sizeof(char*));

		LisAct* plis2=(LisAct*)malloc(sizeof(LisAct));
		plis2->jobname=strdup(plis->jobname);
		plis2->notifysrv=strdup(plis->notifysrv);
		plis2->cpu=(CPU*)malloc(sizeof(CPU));
		plis2->cpu->cname=strdup(plis->cpu->cname);

		p[0]=(char*)plis2;
		p[1]=strdup(path);
		p[2]=strdup(locfname);
		p[3]=strdup(fname);

		_beginthread(LlamarDllRegisterServer,NULL,p);

	}else if(delay==0 && cmd!=NULL && *cmd!=0 ){
		sprintf(msg,"Ejecutando '%s'.",cmd);
		SendMessageTo(plis,path,fname,
			msg);
		if(system( cmd )==-1)
			sprintf(msg,"Hubo un error en la ejecución.");
		else
			strcpy(msg,"Ejecución correcta");
		SendMessageTo(plis,path,fname,
			msg);
	}else if(delay==1){
		sprintf(msg,"Pospuesto.");
		SendMessageTo(plis,path,fname,
			msg);
		DelayFic(plis,fname,path,cmd);
	}
}

void RegistrarArchivos( LisAct* plis ){
	int i;
	Fics* fic=plis->fic;
	for(i=0;i<fic->carfic;i++)
		RegistrarArchivos(plis, fic->fname[i],fic->path[i],fic->cmd[i],fic->delay[i]);
}		
			
void SaveAutoJob(LisAct *pl){
	DWORD Result;
	HKEY MasterKey;
	char *buf=(char*)malloc(SizeCommandBuffer);

	RegCreateKeyEx(HKEY_LOCAL_MACHINE,szAutoReg,0,
		"Folder",REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&MasterKey,&Result);
	BuildCommand(pl->jobname,pl->notifysrv,pl->cpu->cname,pl->cpu->cpus,pl->fic,buf);
	RegSetValueEx( MasterKey,pl->jobname,0,REG_SZ,(const unsigned char*)buf,strlen(buf)+1);
	RegCloseKey  ( MasterKey );
	free(buf);
}
