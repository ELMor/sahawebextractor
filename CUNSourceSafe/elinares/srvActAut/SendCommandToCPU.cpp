#include <windows.h>
#include "logging.h"
#include "dump.h"
#include "setupqueue.h"
#include "ctlserv.h"
#include "srvsocks.h"
#include "registrar.h"

extern BOOL bDebug;

SOCKET SendCommandToCPU(char *cpu, char *command, unsigned long longitud, int Puerto){
	struct sockaddr_in server;
	unsigned long inetaddr,enviados;
	struct hostent *hep;
	SOCKET sock;
	int Ret;

	if(NULL==(hep=gethostbyname(cpu))){
		if(INADDR_NONE==(inetaddr=inet_addr(cpu)))
			return -1;
		hep=gethostbyaddr((const char*)&inetaddr,4,AF_INET);
	}
	memset(&server,0,sizeof(server));
	memcpy(&(server.sin_addr),hep->h_addr,hep->h_length);
	server.sin_family = hep->h_addrtype;
	server.sin_port = htons(Puerto);
	sock=socket(AF_INET,SOCK_STREAM,0);
	Ret=connect(sock,(struct sockaddr*)&server,sizeof(server));

	//Enviar primero el tama�o del buffer
	send(sock,(const char*)&longitud,sizeof(longitud),0);
	//Y luego el buffer
	for(enviados=0;
		enviados<longitud;
		enviados+=send(sock,command+enviados,longitud-enviados,0)
		)
		;	//EmptyBody
	return sock;
}

int SendCommandToCPU(LisAct* la, CPU *cpu, char *cmd){
	char *buf,*gbuf;
	int lon=0;
	Fics* fic=la->fic;

	gbuf=buf=(char*)malloc(SizeCommandBuffer);

	strcpy(buf,cmd);
	lon+=strlen(buf)+1;
	buf=gbuf+lon;

	BuildCommand(la->jobname,la->notifysrv,cpu->cname,cpu->cpus,fic,buf);
	lon+=strlen(buf)+1;
	buf=gbuf+lon;

	SOCKET sck=SendCommandToCPU(cpu->cname,gbuf,lon);
	closesocket(sck);
	//Enviar log
	char msg[128];
	if( !stricmp(cmd,szQrBatch) )
		sprintf(msg,"Ordena a %s de que se actualice ha sido enviada.",
			cpu->cname);
	else
		sprintf(msg,"Orden a %s de que actualice a sus hijas ha sido enviada.",
			cpu->cname);
	SendMessageTo(la,"","",msg);

	free(gbuf);
	return 1;
}

