# Microsoft Developer Studio Project File - Name="srvActAut" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=srvActAut - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "srvActAut.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "srvActAut.mak" CFG="srvActAut - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "srvActAut - Win32 Release" (based on\
 "Win32 (x86) Console Application")
!MESSAGE "srvActAut - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""$/elinares/srvActAut", VOFAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "srvActAut - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /FR /YX /FD /c
# ADD BASE RSC /l 0xc0a /d "NDEBUG"
# ADD RSC /l 0xc0a /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 netapi32.lib mswsock.lib ws2_32.lib setupapi.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# Begin Special Build Tool
SOURCE=$(InputPath)
PostBuild_Desc=Recordatorio:
PostBuild_Cmds=echo Recuerda cambiar la version-string, atontao	copy\
        Release\*.exe .
# End Special Build Tool

!ELSEIF  "$(CFG)" == "srvActAut - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FR /YX /FD /c
# ADD BASE RSC /l 0xc0a /d "_DEBUG"
# ADD RSC /l 0xc0a /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 netapi32.lib mswsock.lib ws2_32.lib setupapi.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# Begin Special Build Tool
SOURCE=$(InputPath)
PostBuild_Desc=Recordatorio:
PostBuild_Cmds=echo Recuerda cambiar la version-string, atontao	copy\
        Debug\*.exe .
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "srvActAut - Win32 Release"
# Name "srvActAut - Win32 Debug"
# Begin Group "Include"

# PROP Default_Filter "h"
# Begin Source File

SOURCE=.\configdir.h
# End Source File
# Begin Source File

SOURCE=.\crccache.h
# End Source File
# Begin Source File

SOURCE=.\CtlServ.h
# End Source File
# Begin Source File

SOURCE=.\Decompile.h
# End Source File
# Begin Source File

SOURCE=.\direx.h
# End Source File
# Begin Source File

SOURCE=.\dump.h
# End Source File
# Begin Source File

SOURCE=.\filecrc32.h
# End Source File
# Begin Source File

SOURCE=.\logging.h
# End Source File
# Begin Source File

SOURCE=.\monitor.h
# End Source File
# Begin Source File

SOURCE=.\registrar.h
# End Source File
# Begin Source File

SOURCE=.\setupqueue.h
# End Source File
# Begin Source File

SOURCE=.\srvsocks.h
# End Source File
# Begin Source File

SOURCE=.\version.h
# End Source File
# End Group
# Begin Group "Source"

# PROP Default_Filter "cpp"
# Begin Source File

SOURCE=.\BuildCommand.cpp
# End Source File
# Begin Source File

SOURCE=.\cDebugService.cpp
# End Source File
# Begin Source File

SOURCE=.\cInstallService.cpp
# End Source File
# Begin Source File

SOURCE=.\Compinstalacion.cpp
# End Source File
# Begin Source File

SOURCE=.\crc32.c
# End Source File
# Begin Source File

SOURCE=.\cRemoveLog.cpp
# End Source File
# Begin Source File

SOURCE=.\cRemoveService.cpp
# End Source File
# Begin Source File

SOURCE=.\cSendJob.cpp
# End Source File
# Begin Source File

SOURCE=.\cStartService.cpp
# End Source File
# Begin Source File

SOURCE=.\CtlServ.cpp
# End Source File
# Begin Source File

SOURCE=.\Decompile.cpp
# End Source File
# Begin Source File

SOURCE=.\direx.c
# End Source File
# Begin Source File

SOURCE=.\GetRemoteFiles.cpp
# End Source File
# Begin Source File

SOURCE=.\gramm.cpp
# End Source File
# Begin Source File

SOURCE=.\gramm.y

!IF  "$(CFG)" == "srvActAut - Win32 Release"

# Begin Custom Build
InputPath=.\gramm.y

"gramm.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	bison -v -o gramm.cpp gramm.y

# End Custom Build

!ELSEIF  "$(CFG)" == "srvActAut - Win32 Debug"

# Begin Custom Build
InputPath=.\gramm.y

"gramm.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	bison -v -o gramm.cpp gramm.y

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\logging.cpp
# End Source File
# Begin Source File

SOURCE=.\MainSock.cpp
# End Source File
# Begin Source File

SOURCE=.\monitor.cpp
# End Source File
# Begin Source File

SOURCE=.\NetThunk.cpp
# End Source File
# Begin Source File

SOURCE=.\Ping.cpp
# End Source File
# Begin Source File

SOURCE=.\QrBatch.cpp
# End Source File
# Begin Source File

SOURCE=.\QrGetFiles.cpp
# End Source File
# Begin Source File

SOURCE=.\registrar.cpp
# End Source File
# Begin Source File

SOURCE=.\RsBatch.cpp
# End Source File
# Begin Source File

SOURCE=.\SendCommandToCPU.cpp
# End Source File
# Begin Source File

SOURCE=.\SendCommandToCPUs.cpp
# End Source File
# Begin Source File

SOURCE=.\Servicio.cpp
# End Source File
# Begin Source File

SOURCE=.\setupqueue.cpp
# End Source File
# Begin Source File

SOURCE=.\ThreadSock.cpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\job.txt
# End Source File
# End Target
# End Project
