CREATE OR REPLACE PACKAGE FAPK01 IS
   /******************************************************************************
   NAME:       FAPK01
   PURPOSE:    Generaci�n de apuntes contables a partir de un rango de facturas

   PUBLIC INTERFACE:
   Object Name				   Object Type		        Description
   -----------------------	   -------------------		-----------
   t_RNF_record				   USER DEFINED RECORD 		Utilizada para pasar un registro RNF a las funciones
   							   							FAFN14 y FAFN15
   FAPR01					   PROCEDURE   	   			Generaci�n de apuntes contables
   FAPR02					   PROCEDURE   	   			Generaci�n de apuntes Diciembre/99
   FAFN14					   FUNCTION					Funci�n para calcular el precio
   FAFN15					   FUNCTION					Funci�n para calcular el coste

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        05/04/2000             	   1. Creaci�n del paquete
   1.1        10/04/2000                   1. Modificado el procedimiento FAPR01
   			  							   	  - recibe solo una fecha de inicio elaboraci�n en vez de
											    fechas inicio y fin y codigos factura
											  - el descuadre genera una excepci�n solo si no es debido
											    a importes decimales
   ******************************************************************************/
   	-- tipo registro para pasar los datos de un RNF a las funciones para el c�lculo de PRECIO y COSTE
	TYPE t_RNF_record IS RECORD (
		NUMRNF 		               FA0300.FA03NUMRNF%TYPE,
		CODDPTO 	                  FA0300.AD02CODDPTO_R%TYPE,
		CODCATEG 	               FA0300.FA05CODCATEG%TYPE,
		PRECIOFACT 	               FA0300.FA03PRECIOFACT%TYPE,
		COSTECATEG 	               FA3500.FA35COSTE%TYPE,
		CANTFACT	                  FA0300.FA03CANTFACT%TYPE,
		CODFACT					      FA0400.FA04CODFACT%TYPE,
		NUMLINEA				         FA1600.FA16NUMLINEA%TYPE,
		DESCUENTO                  FA1600.FA16DCTO%TYPE
		                        );
   PROCEDURE FAPR01           (
   	p_fec_fact1   			      FA0400.FA04FECFACTURA%TYPE,
   	p_numero_asiento 		      FA2900.FA29NUMASIENTO%TYPE,
   	p_sufijo_concepto 		   FA2200.FA22CONCEPTO%TYPE,
   	p_departamento_diferencias FA2900.AD02CODDPTO%TYPE,
   	p_numero_operacion		   FA4500.FA45NUMOPERACION%TYPE,
   	p_aviso_RNF_lineas 		   BOOLEAN,
   	p_aviso_RNF_departamento   BOOLEAN,
   	p_aviso_RNF_precio 		   BOOLEAN,
   	p_aviso_RNF_coste 		   BOOLEAN,
   	p_aviso_insert_apunte	   BOOLEAN,
   	p_imprimir_factura 		   BOOLEAN,
   	p_imprimir_linea		      BOOLEAN,
   	p_imprimir_RNF 			   BOOLEAN
	                           );
   PROCEDURE FAPR02           (
   	p_numero_asiento 		      FA2900.FA29NUMASIENTO%TYPE,
   	p_sufijo_concepto 		   FA2200.FA22CONCEPTO%TYPE,
   	p_departamento_diferencias FA2900.AD02CODDPTO%TYPE
	                           );
	FUNCTION FAFN14            (
	   p_RNF                      t_RNF_record
	                           ) RETURN NUMBER;
	FUNCTION FAFN15            (
	   p_RNF                      t_RNF_record
	                           ) RETURN NUMBER;
END FAPK01;
/

CREATE OR REPLACE PACKAGE BODY FAPK01 AS


/**********************************************************************************************************************
  FUNCTION      : Insertar_Asiento()
  SCOPE         : PRIVATE

  DESCRIPTION   : la funci�n extrae el n�mero de cuenta a utilizar para insertar el apunte y luego inserta el apunte
                  utilizando los valores pasados como par�metros.
  PARAMETERS IN : los valores para extraer el n�mero de cuenta y los valores a insertar en la tabla FA2900
  RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (SELECT or INSERT error)
 *********************************************************************************************************************/
FUNCTION Insertar_Asiento(
	   p_departamento_input	 FA2900.AD02CODDPTO%TYPE,
	   p_importe_apunte	 	 FA2900.FA29IMPORTE%TYPE,
	   p_tipo_economico	 	 CI3200.CI32CODTIPECON%TYPE,
	   p_entidad			 CI1300.CI13CODENTIDAD%TYPE,
	   p_tipo_asistencia	 AD1200.AD12CODTIPOASIST%TYPE,
	   p_numero_asiento	 	 FA2900.FA29NUMASIENTO%TYPE,
	   p_sufijo_concepto	 FA2200.FA22CONCEPTO%TYPE,
	   p_codigo_factura	 	 FA2900.FA04CODFACT%TYPE,
	   p_linea_factura		 FA2900.FA16NUMLINEA%TYPE,
	   p_numero_RNF		 	 FA2900.FA03NUMRNF%TYPE ) RETURN BOOLEAN IS

   -- Para extraer el n�mero de cuenta es necesario un cursor, puesto que los parametros pueden extraer m�s de un registro,
   -- aunque non interese solo el primero extraido (el m�s completo).
   CURSOR curCuenta(v_debe_haber VARCHAR2) IS
   SELECT
       FA2200.FA24CODCUENTA COD_CUENTA,
       FA2200.FA22INDDPTOAPUNTE IND_DPTO_APUNTE,
       FA2200.AD02CODDPTO_APU COD_DPTO_APU,
       FA2200.FA23CODTIPACTIVIDAD COD_TIP_ACTIVIDAD,
       FA2200.FA22CONCEPTO CONCEPTO,
       FA2200.FA22FIL FIL,
       FA2200.FA24CODCUENTA_E ENLACE
   FROM
       CI3200, CI1300, AD0200, AD1200, FA3100, FA2200
   WHERE
       CI3200.CI32CODTIPECON   = DECODE(FA2200.CI32CODTIPECON, NULL, CI3200.CI32CODTIPECON, FA2200.CI32CODTIPECON)
   AND CI1300.CI32CODTIPECON   = CI3200.CI32CODTIPECON
   AND CI1300.CI13CODENTIDAD   = DECODE(FA2200.CI13CODENTIDAD, NULL, CI1300.CI13CODENTIDAD, FA2200.CI13CODENTIDAD)
   AND AD0200.AD02CODDPTO      = DECODE(FA2200.AD02CODDPTO_FAC, NULL, AD0200.AD02CODDPTO, FA2200.AD02CODDPTO_FAC)
   AND AD1200.AD12CODTIPOASIST = DECODE(FA2200.AD12CODTIPOASIST, NULL, AD1200.AD12CODTIPOASIST, FA2200.AD12CODTIPOASIST)
   AND FA3100.FA31CODTIPDEBEHABER =
      DECODE(FA2200.FA31CODTIPDEBEHABER, NULL, FA3100.FA31CODTIPDEBEHABER, FA2200.FA31CODTIPDEBEHABER)
   AND CI3200.CI32CODTIPECON = p_tipo_economico
   AND CI1300.CI13CODENTIDAD = p_entidad
   AND AD0200.AD02CODDPTO = p_departamento_input
   AND AD1200.AD12CODTIPOASIST = p_tipo_asistencia
   AND FA3100.FA31CODTIPDEBEHABER = v_debe_haber
   ORDER BY DECODE(FA2200.AD02CODDPTO_FAC,
               NULL, DECODE(FA2200.CI13CODENTIDAD,
                NULL, DECODE(FA2200.CI32CODTIPECON,
   			              NULL, 3, 2), 1), 0);

   rec_curCuenta_debe          curCuenta%ROWTYPE;
   rec_curCuenta_haber         curCuenta%ROWTYPE;
   v_departamento_apunte_debe  FA2900.AD02CODDPTO%TYPE;
   v_departamento_apunte_haber FA2900.AD02CODDPTO%TYPE;

BEGIN
	-- Abre el cursor, extrae el primer registro y cierra el cursor
	OPEN curCuenta('D');
	FETCH curCuenta INTO rec_curCuenta_debe;
	CLOSE curCuenta;

	OPEN curCuenta('H');
	FETCH curCuenta INTO rec_curCuenta_haber;
	CLOSE curCuenta;

	-- Departamento del apunte :
	-- si FA2200.FA22INDDPTOAPUNTE = TRUE (= -1) --> se utiliza el DEPARTAMENTO del RNF
	-- si FA2200.FA22INDDPTOAPUNTE = FALSE (<> -1, incluso NULL) --> se utiliza FA2200.AD02CODDPTO_APU
	SELECT DECODE(rec_curCuenta_debe.IND_DPTO_APUNTE, -1 , p_departamento_input, rec_curCuenta_debe.COD_DPTO_APU)
	INTO   v_departamento_apunte_debe
	FROM   DUAL;

	SELECT DECODE(rec_curCuenta_haber.IND_DPTO_APUNTE, -1 , p_departamento_input, rec_curCuenta_haber.COD_DPTO_APU)
	INTO   v_departamento_apunte_haber
	FROM   DUAL;

   IF rec_curCuenta_debe.ENLACE IS NULL THEN -- No tiene cuenta de enlace
      	-- Inserta el APUNTE DEBE
      	INSERT INTO FA2900 (FA29NUMASIENTO,FA24CODCUENTA,AD02CODDPTO,FA23CODTIPACTIVIDAD,
              					  FA29IMPORTE,FA29DESCRIPCION,FA31CODTIPDEBEHABER,FA04CODFACT,FA16NUMLINEA,
      						     FA03NUMRNF,FA29FIL)
      	VALUES (p_numero_asiento,rec_curCuenta_debe.COD_CUENTA,v_departamento_apunte_debe,
      			  rec_curCuenta_debe.COD_TIP_ACTIVIDAD,(+1)*p_importe_apunte,
      			  rec_curCuenta_debe.CONCEPTO || ' ' || p_sufijo_concepto,
      			  'D',p_codigo_factura,p_linea_factura,p_numero_RNF,rec_curCuenta_debe.FIL);

      	-- Inserta el APUNTE HABER
      	INSERT INTO FA2900 (FA29NUMASIENTO,FA24CODCUENTA,AD02CODDPTO,FA23CODTIPACTIVIDAD,
              					  FA29IMPORTE,FA29DESCRIPCION,FA31CODTIPDEBEHABER,FA04CODFACT,FA16NUMLINEA,
      						     FA03NUMRNF,FA29FIL)
      	VALUES (p_numero_asiento,rec_curCuenta_haber.COD_CUENTA,v_departamento_apunte_haber,
      			  rec_curCuenta_haber.COD_TIP_ACTIVIDAD,(-1)*p_importe_apunte,
      			  rec_curCuenta_haber.CONCEPTO || ' ' || p_sufijo_concepto,
      			  'H',p_codigo_factura,p_linea_factura,p_numero_RNF,rec_curCuenta_haber.FIL);
   ELSE --Tiene cuenta de enlace
      	-- Inserta el APUNTE DEBE
      	INSERT INTO FA2900 (FA29NUMASIENTO,FA24CODCUENTA,AD02CODDPTO,FA23CODTIPACTIVIDAD,
              					  FA29IMPORTE,FA29DESCRIPCION,FA31CODTIPDEBEHABER,FA04CODFACT,FA16NUMLINEA,
      						     FA03NUMRNF,FA29FIL)
      	VALUES (p_numero_asiento,rec_curCuenta_debe.COD_CUENTA,v_departamento_apunte_debe,
      			  rec_curCuenta_debe.COD_TIP_ACTIVIDAD,(+1)*p_importe_apunte,
      			  rec_curCuenta_debe.CONCEPTO || ' ' || p_sufijo_concepto,
      			  'D',p_codigo_factura,p_linea_factura,p_numero_RNF,rec_curCuenta_debe.FIL);
      	INSERT INTO FA2900 (FA29NUMASIENTO,FA24CODCUENTA,AD02CODDPTO,FA23CODTIPACTIVIDAD,
              					  FA29IMPORTE,FA29DESCRIPCION,FA31CODTIPDEBEHABER,FA04CODFACT,FA16NUMLINEA,
      						     FA03NUMRNF,FA29FIL)
      	VALUES (p_numero_asiento,rec_curCuenta_debe.ENLACE,NULL,
      			  rec_curCuenta_debe.COD_TIP_ACTIVIDAD,(-1)*p_importe_apunte,
      			  rec_curCuenta_debe.CONCEPTO || ' ' || p_sufijo_concepto,
      			  'H',p_codigo_factura,p_linea_factura,p_numero_RNF,rec_curCuenta_debe.FIL);

      	-- Inserta el APUNTE HABER
      	INSERT INTO FA2900 (FA29NUMASIENTO,FA24CODCUENTA,AD02CODDPTO,FA23CODTIPACTIVIDAD,
              					  FA29IMPORTE,FA29DESCRIPCION,FA31CODTIPDEBEHABER,FA04CODFACT,FA16NUMLINEA,
      						     FA03NUMRNF,FA29FIL)
      	VALUES (p_numero_asiento,rec_curCuenta_haber.COD_CUENTA,v_departamento_apunte_haber,
      			  rec_curCuenta_haber.COD_TIP_ACTIVIDAD,(-1)*p_importe_apunte,
      			  rec_curCuenta_haber.CONCEPTO || ' ' || p_sufijo_concepto,
      			  'H',p_codigo_factura,p_linea_factura,p_numero_RNF,rec_curCuenta_haber.FIL);
      	INSERT INTO FA2900 (FA29NUMASIENTO,FA24CODCUENTA,AD02CODDPTO,FA23CODTIPACTIVIDAD,
              					  FA29IMPORTE,FA29DESCRIPCION,FA31CODTIPDEBEHABER,FA04CODFACT,FA16NUMLINEA,
      						     FA03NUMRNF,FA29FIL)
      	VALUES (p_numero_asiento,rec_curCuenta_haber.ENLACE,NULL,
      			  rec_curCuenta_haber.COD_TIP_ACTIVIDAD,(+1)*p_importe_apunte,
      			  rec_curCuenta_haber.CONCEPTO || ' ' || p_sufijo_concepto,
      			  'D',p_codigo_factura,p_linea_factura,p_numero_RNF,rec_curCuenta_haber.FIL);

   END IF;

	RETURN TRUE;

-- La funci�n devuelve FALSE si hay errores (por ejemplo, falla la INSERT en la tabla FA2900)
EXCEPTION

	WHEN OTHERS THEN
		 -- Si se genera una excepci�n, devuelve FALSE
		 RETURN FALSE;
END Insertar_Asiento;


/**********************************************************************************************************************
  PROCEDURE     : FAPR02() SOLO DICIEMBRE
  SCOPE         : PUBLIC

  DESCRIPTION   : ver comentarios en el c�digo
  PARAMETERS IN : ver comentarios en el c�digo
 *********************************************************************************************************************/
PROCEDURE FAPR02(
	p_numero_asiento 		   FA2900.FA29NUMASIENTO%TYPE,   -- n�mero asiento
	p_sufijo_concepto 		   FA2200.FA22CONCEPTO%TYPE,     -- sufijo que hay que a�adir al campo FA2900.FA22CONCEPTO
	p_departamento_diferencias FA2900.AD02CODDPTO%TYPE 	     -- departamento del apunte de diferencias
	) IS
	Cursor cFacDic is
		   Select * from regfact where substr(ffech,1,6)='199912' order by ffech;
	bStopRep boolean;
	bRetIA   boolean;
	nSumRep  number;
BEGIN
	for rFacDic in cFacDic loop
	  bStopRep:=false;
	  nSumRep:=0;
	  if rFacDic.fserv   <>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ   ,1,3)),to_number(rFacDic.fImps  ),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_02<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_02,1,3)),to_number(rFacDic.fImps_02),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_02);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_03<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_03,1,3)),to_number(rFacDic.fImps_03),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_03);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_04<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_04,1,3)),to_number(rFacDic.fImps_04),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_04);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_05<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_05,1,3)),to_number(rFacDic.fImps_05),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_05);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_06<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_06,1,3)),to_number(rFacDic.fImps_06),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_06);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_07<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_07,1,3)),to_number(rFacDic.fImps_07),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_07);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_08<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_08,1,3)),to_number(rFacDic.fImps_08),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_08);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_09<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_09,1,3)),to_number(rFacDic.fImps_09),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_09);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_10<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_10,1,3)),to_number(rFacDic.fImps_10),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_10);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_11<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_11,1,3)),to_number(rFacDic.fImps_11),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_11);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_12<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_12,1,3)),to_number(rFacDic.fImps_12),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_12);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_13<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_13,1,3)),to_number(rFacDic.fImps_13),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_13);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_14<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_14,1,3)),to_number(rFacDic.fImps_14),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_14);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_15<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_15,1,3)),to_number(rFacDic.fImps_15),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_15);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_16<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_16,1,3)),to_number(rFacDic.fImps_16),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_16);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_17<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_17,1,3)),to_number(rFacDic.fImps_17),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_17);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_18<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_18,1,3)),to_number(rFacDic.fImps_18),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_18);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_19<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_19,1,3)),to_number(rFacDic.fImps_19),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_19);
	  else bStopRep:=true; end if;
	  if rFacDic.fserv_20<>'FFFF' and not bStopRep then
	    bRetIA:=Insertar_Asiento(to_number(substr(rFacDic.fServ_20,1,3)),to_number(rFacDic.fImps_20),rFacDic.fTeco,
                rFacDic.fEntidad,to_number(rFacDic.fTasi),p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
        nSumRep:=nSumRep+to_number(rFacDic.fimps_20);
	  else bStopRep:=true; end if;
	  if rFacDic.fimpor<>nSumRep then
	    bRetIA:=Insertar_Asiento(p_departamento_diferencias,rFacDic.fImpor-nSumRep,rFacDic.fTeco,rFacDic.fEntidad,
						 rFacDic.fTasi,p_numero_asiento,p_sufijo_concepto,rFacDic.fFact,1,0);
	  end if;
	  commit;
	end loop;
END FAPR02;


/**********************************************************************************************************************
  PROCEDURE     : FAPR01()
  SCOPE         : PUBLIC

  DESCRIPTION   : ver comentarios en el c�digo
  PARAMETERS IN : ver comentarios en el c�digo
 *********************************************************************************************************************/
PROCEDURE FAPR01(
	p_fec_fact1   			      FA0400.FA04FECFACTURA%TYPE,   -- fecha de contabilizaci�n
	p_numero_asiento 		      FA2900.FA29NUMASIENTO%TYPE,   -- n�mero asiento
	p_sufijo_concepto 		   FA2200.FA22CONCEPTO%TYPE,     -- sufijo que hay que a�adir al campo FA2900.FA22CONCEPTO
	p_departamento_diferencias FA2900.AD02CODDPTO%TYPE,	     -- departamento del apunte de diferencias
	p_numero_operacion		   FA4500.FA45NUMOPERACION%TYPE, -- n�mero de operaci�n en la tabla de log
	p_aviso_RNF_lineas 		   BOOLEAN, -- si queremos un registro de log por cada linea factura sin RNFs
	p_aviso_RNF_departamento   BOOLEAN, -- si queremos un registro de log por cada RNF sin departamento
	p_aviso_RNF_precio 		   BOOLEAN, -- si queremos un registro de log por cada RNF sin precio
	p_aviso_RNF_coste 		   BOOLEAN, -- si queremos un registro de log por cada RNF sin coste
	p_aviso_insert_apunte	   BOOLEAN, -- si queremos un registro de log al fallar la INSERT de un apunte
	p_imprimir_factura 		   BOOLEAN, -- si queremos un registro de log por cada factura elaborada
	p_imprimir_linea		      BOOLEAN, -- si queremos un registro de log por cada l�nea factura elaborada
	p_imprimir_RNF 			   BOOLEAN  -- si queremos un registro de log por cada RNF elaborado
	 ) IS
-- -----------------------------------------------------
-- Declaraci�n de excepciones y codigos de error ORACLE
-- de momento se utilizan los codigos de -20001 a -20003
-- -----------------------------------------------------
   ENTIDAD_NOT_FOUND          EXCEPTION;
   TIPO_ASISTENCIA_NOT_FOUND  EXCEPTION;
   DESCUADRE_DETECTED		   EXCEPTION;
   INSERT_DEBE_FAILED		   EXCEPTION;
   INSERT_HABER_FAILED		   EXCEPTION;
   DEPARTAMENTO_RNF_NOT_FOUND EXCEPTION;
   INVALID_PARAMETER          EXCEPTION;
   PURGE_LOG_FAILED           EXCEPTION;
   c_GENERIC_ERROR            CONSTANT NUMBER := -20001;
   c_INVALID_PARAMETER_ERROR  CONSTANT NUMBER := -20002;
   c_PURGE_LOG_ERROR          CONSTANT NUMBER := -20003;

/*
-- Update para poner fecha de contabilizaci�n a las facturas
   update fa0400
   set fa04feccontabil=to_date('31012000','ddmmyyyy')
   where fa04feccontabil is null
     and fa04numfacreal is null
     and to_date('31012000','ddmmyyyy')>=(select trunc(last_day(max(fa03fecha)))
                                         from fa0300 where fa0300.fa04numfact=fa0400.fa04codfact)
     and fa04codfact>=1600000
*/
-- Esta es la SELECT 'verdadera' (con fecha de contabilizaci�n)
CURSOR curF IS
   SELECT  FA04NUMFACT, FA04CODFACT, FA04CANTFACT, FA04FECFACTURA,
           CI32CODTIPECON, CI13CODENTIDAD, AD01CODASISTENCI, AD07CODPROCESO
   FROM    FA0400
   WHERE   FA04FECCONTABIL = p_fec_fact1
   ORDER BY FA04NUMFACT;


-- cursor para extraer las lineas factura asociadas a una factura (par�metro <cod_fact>)
CURSOR curLF(cod_fact FA1600.FA04CODFACT%TYPE) IS
   SELECT 
      FA04CODFACT, 
      FA16NUMLINEA, 
      FA16IMPORTE,
      FA16DCTO 
   FROM 
      FA1600 
   WHERE 
      FA04CODFACT = cod_fact;

-- cursor para extraer los RNFs asociados a una linea factura (par�metros <cod_fact> y <linea_fact>)
CURSOR curRNF(cod_fact FA0300.FA04NUMFACT%TYPE, linea_fact FA0300.FA16NUMLINEA%TYPE) IS
   SELECT 
      FA03NUMRNF, 
      AD02CODDPTO_R, 
      FA0300.FA05CODCATEG CODCATEG, 
      FA03PRECIOFACT, 
      FA35COSTE, 
      FA03CANTFACT
   FROM   
      FA0300, 
      FA3500
   WHERE  
         FA0300.FA05CODCATEG =  FA3500.FA05CODCATEG(+) 
     AND FA04NUMFACT = cod_fact 
     AND FA16NUMLINEA = linea_fact;

-- cursor para extraer los movimientos de investigaci�n
CURSOR curMINV(F1 DATE, F2 DATE) is
   SELECT 
      AD02CODDPTO,
      PRECIO,
      AD12CODTIPOASIST
   FROM   
      FAINV1J
   WHERE  
         FECHA>=F1 
     AND FECHA<=F2;

-- variables de tipo registro para la fetch de los cursores y para las funciones PRECIO y COSTE
   rec_curF                 curF%ROWTYPE;
   rec_curLF                curLF%ROWTYPE;
   rec_curRNF               curRNF%ROWTYPE;
   rec_RNF	                t_RNF_record;

   v_tipo_economico         AD1100.CI32CODTIPECON%TYPE;
   v_codigo_entidad         AD1100.CI13CODENTIDAD%TYPE;
   v_tipo_asistencia        AD2500.AD12CODTIPOASIST%TYPE;
   v_importe_apunte         NUMBER;
   v_precio                 NUMBER;
   v_coste                  NUMBER;
   v_total_importe_apuntes  NUMBER;
   v_total_importe_lineas   NUMBER;
   v_numero_lineas			 NUMBER;
   v_ultima_linea_elaborada FA1600.FA16NUMLINEA%TYPE;
   v_diferencia			    NUMBER;
   v_departamento_factura   AD0500.AD02CODDPTO%TYPE;
   v_departamento_haber     AD0500.AD02CODDPTO%TYPE;
   v_utilizar_precio  		 BOOLEAN;
   v_total_importe_RNF      NUMBER(15,3);
   v_numero_RNF             NUMBER;
   v_return				       BOOLEAN;
   v_msg                    FA4500.FA45DESSUCESO%TYPE;

BEGIN
-- -----------------------------------------------------------
-- Comprobaci�n de los par�metros en entrada
-- Si un par�metro es incorrecto --> EXCEPTION y ORACLE ERROR
-- <p_fec_fact1> no debe ser nulo
-- <p_numero_asiento> no debe ser nulo (not null)
-- <p_sufijo_concepto> es obligatorio (not null)
-- <p_numero_operacion> es obligatorio (not null)
-- -----------------------------------------------------------
   IF (p_fec_fact1 IS NULL) OR
      (p_numero_asiento IS NULL) OR
      (p_sufijo_concepto IS NULL OR p_sufijo_concepto = '') OR
      (p_numero_operacion IS NULL OR p_numero_operacion = 0) THEN
   
      RAISE INVALID_PARAMETER;
   
   END IF;

-- ----------------------------------------------
-- Borra el contenido de la tabla de log
-- Si falla --> EXCEPTION and ORACLE ERROR
-- ----------------------------------------------
   IF NOT Purge_Log(p_numero_operacion) THEN
   
      RAISE PURGE_LOG_FAILED;
   
   END IF;

-- -----------------------------------------------------------
-- Corregir RNF de telefonos, lavander�a y dietas acompa�ante
-- -----------------------------------------------------------
   CorregirTelef;
   
-- ---------------------------
-- MAIN LOOP ("FACTURAS")
-- ---------------------------
FOR rec_curF IN curF LOOP

--	----------------------------------------------------------------------------------------------------------
--  Produce una entrada de log con los datos de la factura (COMMIT - a partir de aqu� empieza la transaci�n, que
--  es a nivel de factura)
--	-----------------------------------------------------------------------------------------------------------
	IF p_imprimir_factura THEN
	   v_msg := 'Elaboraci�n de la factura ' || TO_CHAR(rec_curF.FA04CODFACT) || ' ...';
	   Write_To_Log(p_numero_operacion, v_msg, 1, rec_curF.FA04CODFACT, NULL, NULL, TRUE);
	END IF;

	BEGIN  -- Inicio del bloque de elaboraci�n de una FACTURA

--	-----------------------------------------------------------------------------------
--	Comprueba ENTIDAD y TIPO ECON�MICO
--	si no hay ENTIDAD o TIPO ECONOMICO validos --> EXCEPCI�N (y ROLLBACK)
--	-----------------------------------------------------------------------------------
--  ELM suponemos que son los que tiene la factura; no comprobamos con la AD11

-- 	SELECT CI32CODTIPECON, CI13CODENTIDAD
-- 	INTO   v_tipo_economico, v_codigo_entidad
-- 	FROM   AD1100
-- 	WHERE  AD07CODPROCESO = rec_curF.AD07CODPROCESO
--       AND  AD01CODASISTENCI = rec_curF.AD01CODASISTENCI
-- 	  AND  AD11FECFIN IS NULL;
--
-- 	IF SQL%NOTFOUND OR (v_tipo_economico IS NULL) OR (v_codigo_entidad IS NULL) THEN
--
-- 		RAISE ENTIDAD_NOT_FOUND;
--
-- 	END IF;
	v_tipo_economico:=rec_curF.ci32codtipecon;
	v_codigo_entidad:=rec_curF.ci13codentidad;


--	-----------------------------------------------------------------------------------
--	Extrae el DEPARTAMENTO associado a la factura.
-- 	Elige el DEPARTAMENTO a utilizar cuando se inserten APUNTES de diferencia.
--	------------------------------------------------------------------------------------
	SELECT AD02CODDPTO
	INTO   v_departamento_factura
	FROM   AD0500
	WHERE  AD07CODPROCESO = rec_curF.AD07CODPROCESO
	  AND  AD01CODASISTENCI = rec_curF.AD01CODASISTENCI
	  AND  AD05FECFINRESPON IS NULL;

	IF p_departamento_diferencias IS NULL THEN
		v_departamento_haber := v_departamento_factura;
	ELSE
		v_departamento_haber := p_departamento_diferencias;
	END IF;

--	--------------------------------------------------------------------------------------------------------
--	Extrae el TIPO ASITENCIA correspondiente desde la tabla AD2500 y comprueba si es v�lido
-- Si alguna vez fue hospitalizado, se toma como hospitalizado, en otro caso como ambulatorio
--	si no es valido --> EXCEPCI�N (y ROLLBACK)
--	--------------------------------------------------------------------------------------------------------
   SELECT DECODE(MIN(AD12CODTIPOASIST),1,1,2)
	INTO   v_tipo_asistencia
	FROM   AD2500
	WHERE  AD01CODASISTENCI = rec_curF.AD01CODASISTENCI;

	IF SQL%NOTFOUND THEN RAISE TIPO_ASISTENCIA_NOT_FOUND; END IF;

--	-----------------------------------------------------------------------------------------------------------
--	Las variables siguientes sirven para comprobar si hay un error de "descuadre", es decir si el importe
--  factura es distincto de la suma de los importes de las l�neas.
--	-----------------------------------------------------------------------------------------------------------
	v_total_importe_lineas := 0;
	v_numero_lineas := 0;

--		-------------------------------------------------------------------------------------------------
--		Extrae el total de los importes de los RNFs asociados a la linea factura.
--		Sirve para decidir si se contabiliza PRECIO o COSTE, dependiendo de si el valor coincide o no
--		coincide con el importe total de la l�nea.
--		-------------------------------------------------------------------------------------------------

--    Usamos la vista FA0420J  que es la siguiente:
/*
      CREATE OR REPLACE VIEW FA0420J ( FA04CODFACT,FA04NUMFACT,FA04CANTFACT,IMPORTEMOV ) AS 
      SELECT /* CANTIDAD FACTURADA FRENTE IMPORTE DE MOVIMIENTOS*/  
         FA0400.FA04CODFACT,  
         FA0400.FA04NUMFACT,  
         FA0400.FA04CANTFACT,  
         SUM(DECODE(FA0300.FA05CODCATEG,
                     35352,FA16IMPORTE,
                     35353,FA16IMPORTE,
                     35354,FA16IMPORTE,
                     FA0300.FA03CANTFACT*FA0300.FA03PRECIOFACT*(100-FA16DCTO)/100) ) IMPORTEMOV  
      FROM  
         FA0300,  
         FA1600,  
         FA0400  
      WHERE  
             FA0400.FA04NUMFACREAL IS NULL  
         AND FA0400.FA04CANTFACT<>0  
         AND FA0300.FA04NUMFACT=FA0400.FA04CODFACT  
         AND FA1600.FA04CODFACT=FA0400.FA04CODFACT  
         AND FA0300.FA16NUMLINEA=FA1600.FA16NUMLINEA  
      GROUP BY  
         FA0400.FA04CODFACT,  
         FA0400.FA04NUMFACT,  
         FA0400.FA04CANTFACT
*/
		SELECT TRUNC(FA04CANTFACT)-TRUNC(IMPORTEMOV)
		INTO   v_total_importe_RNF
		FROM   FA0420J /*ver comentarios en la vista*/
		WHERE  FA04CODFACT  = rec_curF.FA04CODFACT;

		IF (v_total_importe_RNF IS NULL) THEN
			v_utilizar_precio := FALSE;
	   ELSE
         v_utilizar_precio := (v_total_importe_RNF = 0);
		END IF;

-- ------------------------------
-- 	LEVEL 1 LOOP (LINEAS FACTURA)
-- ------------------------------
	FOR rec_curLF IN curLF(rec_curF.FA04CODFACT) LOOP

--		------------------------------------------------------------------------
--  	Produce una entrada de log con los datos de la linea factura
--		------------------------------------------------------------------------
		IF p_imprimir_linea THEN
		   v_msg := 'Elaboraci�n de la linea ' || TO_CHAR(rec_curLF.FA16NUMLINEA);
		   v_msg := v_msg || ' de la factura ' || TO_CHAR(rec_curF.FA04CODFACT) || ' ...';
		   Write_To_Log(p_numero_operacion, v_msg, 1, rec_curF.FA04CODFACT, rec_curLF.FA16NUMLINEA, NULL, FALSE);
		END IF;

		BEGIN -- Inicio del bloque de elaboraci�n de una LINEA FACTURA

		v_numero_lineas := v_numero_lineas + 1;
		v_total_importe_lineas := v_total_importe_lineas + rec_curLF.FA16IMPORTE;
		v_ultima_linea_elaborada := rec_curLF.FA16NUMLINEA;

--		------------------------------------------------------------------------------------------------------
--		La variable siguiente sirve para acumular el n�mero de RNFs elaborados y por consiguiente para
--		comprobar si hay o no hay RNFs asociados a la linea factura que estamos elaborando
--		------------------------------------------------------------------------------------------------------
		v_numero_RNF := 0;

--		------------------------------------------------------------------------------------------------------
--		La variable siguiente sirve para acumular el total de los apuntes que se van insertando. Este total es
--		distincto del total calculado antes (<v_total_importe_RNF>) puesto que tiene en cuenta PRECIO o COSTE y
--		no solamente PRECIO.
--	    Se utiliza para comprobar si es necesario insertar APUNTES de diferencia.
--		------------------------------------------------------------------------------------------------------
		v_total_importe_apuntes := 0;

-- 		------------------------------
-- 		LEVEL 2 LOOP (RNFs)
-- 		------------------------------
		FOR rec_curRNF IN curRNF(rec_curLF.FA04CODFACT, rec_curLF.FA16NUMLINEA) LOOP

--			----------------------------------------------------------------
--  		Produce una entrada de log con los datos del RNF
--			----------------------------------------------------------------
			IF p_imprimir_RNF THEN
				v_msg := 'Elaboraci�n del RNF ' || TO_CHAR(rec_curRNF.FA03NUMRNF);
				v_msg := v_msg || ' de la linea ' || TO_CHAR(rec_curLF.FA16NUMLINEA);
				v_msg := v_msg || ' de la factura ' || TO_CHAR(rec_curF.FA04CODFACT) || ' ...';
				Write_To_Log(p_numero_operacion, v_msg, 1, rec_curF.FA04CODFACT, rec_curLF.FA16NUMLINEA, rec_curRNF.FA03NUMRNF, FALSE);
			END IF;

			BEGIN  -- Inicio del bloque de elaboraci�n de un RNF

			v_numero_RNF := v_numero_RNF + 1;

--			-----------------------
-- 			C�lculo PRECIO y COSTE
--			-----------------------
			rec_RNF.NUMRNF    	 := rec_curRNF.FA03NUMRNF;
			rec_RNF.CODDPTO   	 := rec_curRNF.AD02CODDPTO_R;
			rec_RNF.CODCATEG   	 := rec_curRNF.CODCATEG;
			rec_RNF.PRECIOFACT 	 := rec_curRNF.FA03PRECIOFACT;
			rec_RNF.COSTECATEG 	 := rec_curRNF.FA35COSTE;
			rec_RNF.CANTFACT 	    := rec_curRNF.FA03CANTFACT;
			rec_RNF.CODFACT 	    := rec_curLF.FA04CODFACT;
			rec_RNF.NUMLINEA 	    := rec_curLF.FA16NUMLINEA;
			rec_RNF.DESCUENTO     := rec_curLF.FA16DCTO;

			v_precio              := FAFN14(rec_RNF);
			v_coste               := FAFN15(rec_RNF);

--			--------------------------------------------------------------------------------------------------------
--			Comprueba COSTE: si es nulo --> COSTE = 0, se produce una entrada en el log y se sigue elaborando el RNF
--			---------------------------------------------------------------------------------------------------------
			IF (v_coste IS NULL) THEN

				IF p_aviso_RNF_coste THEN

					v_msg := 'Coste de CATEGORIA ' || TO_CHAR(rec_curRNF.CODCATEG) || ' sin valor en RNF. ';
					v_msg := v_msg || 'RNF ' || TO_CHAR(rec_curRNF.FA03NUMRNF);
					v_msg := v_msg || ' de la linea ' || TO_CHAR(rec_curLF.FA16NUMLINEA);
					v_msg := v_msg || ' de la factura ' || TO_CHAR(rec_curF.FA04CODFACT);
					Write_To_Log(p_numero_operacion, v_msg, 2,rec_curF.FA04CODFACT,rec_curLF.FA16NUMLINEA,rec_curRNF.FA03NUMRNF, FALSE);

				END IF;

				v_coste := 0;

			END IF;

--			--------------------------------------------------------------------------------------------------------
--			Comprueba PRECIO: si es nulo --> PRECIO = 0, se produce una entrada en el log y se sigue elaborando el RNF
--			---------------------------------------------------------------------------------------------------------
			IF (v_precio IS NULL) THEN

				IF p_aviso_RNF_precio THEN

					v_msg := 'Precio nulo en RNF ' || TO_CHAR(rec_curRNF.FA03NUMRNF);
					v_msg := v_msg || ' de la linea ' || TO_CHAR(rec_curLF.FA16NUMLINEA);
					v_msg := v_msg || ' de la factura ' || TO_CHAR(rec_curF.FA04CODFACT);
					Write_To_Log(p_numero_operacion, v_msg, 2, rec_curF.FA04CODFACT, rec_curLF.FA16NUMLINEA, rec_curRNF.FA03NUMRNF, FALSE);

				END IF;

				v_precio := 0;

			END IF;

--			------------------------------------------------------------------------------------------------
--			Contabilizar PRECIO o COSTE ? <v_utilizar_precio> es TRUE si el importe de la linea coincide con
--			la suma de los importes de los RNFs
--			------------------------------------------------------------------------------------------------
			IF v_utilizar_precio THEN

				v_importe_apunte := v_precio;

			ELSE

				v_importe_apunte := v_coste;

			END IF;


--			------------------------------------------------------------------------------------------
--			Comprobaci�n de si hay un DEPARTAMENTO asociado al RNF
--			Si no hay --> no contabiliza el RNF
--			Si hay    --> inserta los apuntes
--			------------------------------------------------------------------------------------------
			IF rec_curRNF.AD02CODDPTO_R IS NULL THEN

				RAISE DEPARTAMENTO_RNF_NOT_FOUND;

			END IF;

--			-------------------------------------------------------
--			Se a�ade el importo del apunte al total
--			-------------------------------------------------------
			v_total_importe_apuntes := v_total_importe_apuntes + v_importe_apunte;

--			-----------------------------------------------------------------------------------
--			Inserta el Asiento
--			-----------------------------------------------------------------------------------
			v_return :=  Insertar_Asiento(rec_curRNF.AD02CODDPTO_R,
									v_importe_apunte,
									v_tipo_economico,
									v_codigo_entidad,
									v_tipo_asistencia,
									p_numero_asiento,
									p_sufijo_concepto,
									rec_curF.FA04CODFACT,
									rec_curLF.FA16NUMLINEA,
									rec_curRNF.FA03NUMRNF);
			IF NOT v_return THEN

			   RAISE INSERT_DEBE_FAILED;

			END IF;

			EXCEPTION  -- Bloque de gesti�n excepciones a nivel RNF

				WHEN DEPARTAMENTO_RNF_NOT_FOUND THEN

					-- El DEPARTAMENTO asociado al RNF no es valido
					IF p_aviso_RNF_departamento THEN

						v_msg := 'DEPARTAMENTO no definido en RNF ' || TO_CHAR(rec_curRNF.FA03NUMRNF);
						v_msg := v_msg || ' de la linea ' || TO_CHAR(rec_curLF.FA16NUMLINEA);
						v_msg := v_msg || ' de la factura ' || TO_CHAR(rec_curF.FA04CODFACT);
						Write_To_Log(p_numero_operacion, v_msg, 2, rec_curF.FA04CODFACT, rec_curLF.FA16NUMLINEA, rec_curRNF.FA03NUMRNF, FALSE);

					END IF;

				WHEN INSERT_DEBE_FAILED THEN

					-- Si falla la INSERT de un APUNTE RNF en DEBE
					IF p_aviso_insert_apunte THEN

						v_msg := 'INSERT del APUNTE DEBE terminada con un error. ';
						v_msg := v_msg || 'RNF ' || TO_CHAR(rec_curRNF.FA03NUMRNF);
						v_msg := v_msg || ' de la linea ' || TO_CHAR(rec_curLF.FA16NUMLINEA);
						v_msg := v_msg || ' de la factura ' || TO_CHAR(rec_curF.FA04CODFACT);
						Write_To_Log(p_numero_operacion, v_msg, 2, rec_curF.FA04CODFACT, rec_curLF.FA16NUMLINEA, rec_curRNF.FA03NUMRNF, FALSE);

					END IF;

				WHEN INSERT_HABER_FAILED THEN

					-- Si falla la INSERT de un APUNTE RNF en HABER
					IF p_aviso_insert_apunte THEN

						v_msg := 'INSERT del APUNTE HABER terminada con un error. ';
						v_msg := v_msg || 'RNF ' || TO_CHAR(rec_curRNF.FA03NUMRNF);
						v_msg := v_msg || ' de la linea ' || TO_CHAR(rec_curLF.FA16NUMLINEA);
						v_msg := v_msg || ' de la factura ' || TO_CHAR(rec_curF.FA04CODFACT);
						Write_To_Log(p_numero_operacion, v_msg, 2, rec_curF.FA04CODFACT, rec_curLF.FA16NUMLINEA, rec_curRNF.FA03NUMRNF, FALSE);

					END IF;

			END;

		END LOOP;  -- End LEVEL 2 LOOP (RNF)

--		-----------------------------------------------------------------------------------------------------
--		Si no hay ningun RNF asociado a la linea factura que estamos elaborando, inserta un registro en el log
--		-----------------------------------------------------------------------------------------------------
		IF (v_numero_RNF = 0 ) THEN

		   IF p_aviso_RNF_lineas THEN

		   	  v_msg := 'No hay lineas de RNF para la linea ' || TO_CHAR(rec_curLF.FA16NUMLINEA);
			  v_msg := v_msg || ' de la factura ' || TO_CHAR(rec_curF.FA04CODFACT);
			  Write_To_Log(p_numero_operacion, v_msg, 2, rec_curF.FA04CODFACT, rec_curLF.FA16NUMLINEA, NULL, FALSE);

		   END IF;

		END IF;

--		----------------------------------------------------------------------------------------------------------
--		Si la suma del importe de los apuntes insertados (<v_total_importe_apuntes>) no coincide con el importe de
--		la linea de factura (rec_curLF.FA16IMPORTE) que estamos elaborando, es necesario insertar un apunte DEBE y
--		uno HABER de importe igual (en valor absoluto) a la diferencia entre
--		<v_total_importe_apuntes> y rec_curLF.FA16IMPORTE.
--		----------------------------------------------------------------------------------------------------------
		IF (v_total_importe_apuntes <> rec_curLF.FA16IMPORTE) THEN

			v_importe_apunte := rec_curLF.FA16IMPORTE - v_total_importe_apuntes;

--			---------------------------------
--			Apunte de diferencia en DEBE
--			---------------------------------
			v_return := Insertar_Asiento(v_departamento_haber,
									v_importe_apunte,
									v_tipo_economico,
									v_codigo_entidad,
									v_tipo_asistencia,
									p_numero_asiento,
									p_sufijo_concepto,
									rec_curF.FA04CODFACT,
									rec_curLF.FA16NUMLINEA,
									NULL);

			IF NOT v_return THEN

			   RAISE INSERT_DEBE_FAILED;

			END IF;

		END IF;

		EXCEPTION -- Bloque de gesti�n de excepciones a nivel LINEA FACTURA

				WHEN INSERT_DEBE_FAILED THEN

					-- Si falla la INSERT de un APUNTE RNF en DEBE
					IF p_aviso_insert_apunte THEN

						v_msg := 'INSERT del APUNTE de diferencia en DEBE terminada con un error. ';
						v_msg := v_msg || 'Linea ' || TO_CHAR(rec_curLF.FA16NUMLINEA);
						v_msg := v_msg || ' de la factura ' || TO_CHAR(rec_curF.FA04CODFACT);
						Write_To_Log(p_numero_operacion, v_msg, 2, rec_curF.FA04CODFACT, rec_curLF.FA16NUMLINEA, NULL, FALSE);

					END IF;

				WHEN INSERT_HABER_FAILED THEN

					-- Si falla la INSERT de un APUNTE RNF en HABER
					IF p_aviso_insert_apunte THEN

						v_msg := 'INSERT del APUNTE de diferencia en HABER terminada con un error. ';
						v_msg := v_msg || 'Linea ' || TO_CHAR(rec_curLF.FA16NUMLINEA);
						v_msg := v_msg || ' de la factura ' || TO_CHAR(rec_curF.FA04CODFACT);
						Write_To_Log(p_numero_operacion, v_msg, 2, rec_curF.FA04CODFACT, rec_curLF.FA16NUMLINEA, NULL, FALSE);

					END IF;

		END;

	END LOOP; -- End of LEVEL 1 LOOP (LINEA FACTURA)

--	--------------------------------------------------------------------------------------------------------------
--	Si la suma del importe de las lineas factura no coincide con el importe de la factura hay que
--  comprobar si la diferencia es debida a importes decimales o no.
--  SI --> se cambia el importe de una linea de factura cualquiera (la �ltima elaborada, por ejemplo),
--  a�adiendo o quitando la diferencia
--  NO --> se genera una excepci�n de "descuadre"
--
--  REDONDEO MAX = N�MERO LINEAS FACTURA * 0.9
--	-----------------------------------------------------------------------------------------------------
	v_diferencia := (rec_curF.FA04CANTFACT - v_total_importe_lineas);

	IF v_diferencia <> 0 THEN

		IF ABS(v_diferencia) <= (v_numero_lineas * 0.9) THEN

		    UPDATE FA1600 SET FA16IMPORTE = (FA16IMPORTE + v_diferencia)
		   		  WHERE FA04CODFACT = rec_curF.FA04CODFACT AND FA16NUMLINEA = v_ultima_linea_elaborada;

			v_msg := 'Descuadre en factura ' || TO_CHAR(rec_curF.FA04CODFACT) || ' debido a importes decimales. ';
			v_msg := v_msg || '(total lineas: ' || TO_CHAR(v_total_importe_lineas) || ', ';
			v_msg := v_msg || 'total factura: ' || TO_CHAR(rec_curF.FA04CANTFACT) || '). ';
			v_msg := v_msg || 'La cantidad ' || TO_CHAR(v_diferencia) || ' ha sido a�adida al importe de la l�nea ';
			v_msg := v_msg || TO_CHAR(v_ultima_linea_elaborada);
			Write_To_Log(p_numero_operacion, v_msg, 2, rec_curF.FA04CODFACT, NULL, NULL, FALSE);

		ELSE

		    RAISE DESCUADRE_DETECTED;

		END IF;

	END IF;

--	---------------------------------------------
--	Termina la transaci�n con un COMMIT
--	---------------------------------------------
	COMMIT;

	EXCEPTION  -- Bloque de gesti�n excepciones a nivel de FACTURA (cada excepci�n produce un ROLLBACK de la transaci�n
			   -- y la inserci�n de un registro en la tabla de log)

		WHEN ENTIDAD_NOT_FOUND THEN

 		 	-- ENTIDAD o TIPO ECONOMICO son nulos
			ROLLBACK;

			v_msg := 'ENTIDAD no definida en factura ' || TO_CHAR(rec_curF.FA04CODFACT);
			Write_To_Log(p_numero_operacion, v_msg, 3, rec_curF.FA04CODFACT, NULL, NULL, TRUE);

		WHEN TIPO_ASISTENCIA_NOT_FOUND THEN

			-- TIPO ASISTENCIA incorrecto
			ROLLBACK;

			v_msg := 'Situaci�n de ASISTENCIA no definida en factura ' || TO_CHAR(rec_curF.FA04CODFACT);
			Write_To_Log(p_numero_operacion, v_msg, 3, rec_curF.FA04CODFACT, NULL, NULL, TRUE);

		WHEN DESCUADRE_DETECTED THEN

			-- DIFERENCIA entre el importe de la factura y la suma del importe de las lineas asociadas
			ROLLBACK;

			v_msg := 'Descuadre en factura ' || TO_CHAR(rec_curF.FA04CODFACT) || '. ';
			v_msg := v_msg || 'Total lineas: ' || TO_CHAR(v_total_importe_lineas) || ', ';
			v_msg := v_msg || 'total factura: ' || TO_CHAR(rec_curF.FA04CANTFACT) || '. ';
			Write_To_Log(p_numero_operacion, v_msg, 3, rec_curF.FA04CODFACT, NULL, NULL, TRUE);

		WHEN OTHERS THEN

			-- Cualquier otro error SQL o situaci�n de datos incorrectos que no ha sido gestionada a nivel
			-- de factura, linea factura o RNF
			ROLLBACK;

			v_msg := 'Error SQL o datos incorrectos en factura ' || TO_CHAR(rec_curF.FA04CODFACT) || '( '
			         || rec_curF.FA04NUMFACT || ').';
			Write_To_Log(p_numero_operacion, v_msg, 3, rec_curF.FA04CODFACT, NULL, NULL, TRUE);
	END;

END LOOP;   -- END MAIN LOOP (FACTURAS)
-- ---------------------------------------------------------------------------
--  Repartos de Investigaci�n
-- ---------------------------------------------------------------------------

--   FOR recINV in curMINV(trunc(p_fec_fact1,'MONTH'),last_day(p_fec_fact1)+0.9999) LOOP
--		v_return := Insertar_Asiento(recINV.AD02CODDPTO,
--								recINV.PRECIO,
--								'H',
--								'H',
--								recINV.AD12CODTIPOASIST,
--								p_numero_asiento,
--								p_sufijo_concepto,
--								NULL,
--								NULL,
--								NULL);
--      COMMIT;
--   END LOOP;


EXCEPTION

	WHEN INVALID_PARAMETER THEN

		-- parametros err�neos
		v_msg := 'Invalid parameter supplied ';
		v_msg := v_msg || '(invalid p_fec_fact, p_numero_asiento, p_sufijo_concepto or p_numero_operacion)';
		RAISE_APPLICATION_ERROR(c_INVALID_PARAMETER_ERROR, v_msg);

	WHEN PURGE_LOG_FAILED THEN

		-- error al borrar el contenido de la tabla de log
		v_msg := 'Purge log table failed (FALSE returned from Purge_Log())';
		RAISE_APPLICATION_ERROR(c_PURGE_LOG_ERROR, v_msg);

	WHEN OTHERS THEN

		-- excepci�n gen�rica
		v_msg := 'Generic error (OUTERMOST LEVEL EXCEPTION)';
		RAISE_APPLICATION_ERROR(c_GENERIC_ERROR, v_msg, TRUE);

END FAPR01;


/**********************************************************************************************************************
  FUNCTION      : FAFN14()
  SCOPE         : PUBLIC

  DESCRIPTION   : calcula el precio de un RFN basandose en la formula utilizada en la funci�n. Si el precio es nulo,
                  devuelve NULL
  PARAMETERS IN : p_RNF, registro de tipo t_RNF_record con los datos de un RNF
  RETURN VALUE  : (NUMBER) el precio calculado o NULL si el precio es nulo
 *********************************************************************************************************************/
FUNCTION FAFN14(p_RNF t_RNF_record) RETURN NUMBER IS
-- De momento la formula es : PRECIO = FA03PRECIOFACT

p_return NUMBER;

BEGIN
if p_RNF.CODCATEG>=35352 and p_RNF.CODCATEG<=35354 then
    select fa16importe
	into   p_return
	from   fa1600
	where  fa1600.fa04codfact =p_RNF.CODFACT
	   and fa1600.fa16nuMlinea=p_RNF.NUMLINEA;
else
	p_return := p_RNF.PRECIOFACT*p_RNF.CANTFACT;
end if;
RETURN p_return;

END FAFN14;


/**********************************************************************************************************************
  FUNCTION      : FAFN15()
  SCOPE         : PUBLIC

  DESCRIPTION   : calcula el coste de un RFN basandose en la formula utilizada en la funci�n. Si el coste es nulo,
                  devuelve NULL
  PARAMETERS IN : p_RNF, registro de tipo t_RNF_record con los datos de un RNF
  RETURN VALUE  : (NUMBER) el coste calculado o NULL si el coste es nulo
 *********************************************************************************************************************/
FUNCTION FAFN15(p_RNF t_RNF_record) RETURN NUMBER IS
-- De momento la formula es : COSTE = (FA3500.FA35COSTE * 1.05)
-- El campo COSTECATEG de la estructura de tipo registro t_RNF_record es equivalente a FA3500.FA35COSTE

p_return NUMBER;

BEGIN

IF p_RNF.CODCATEG IS NULL THEN
   p_return := NULL;
elsif p_RNF.CODCATEG>=35352 and p_RNF.CODCATEG<=35354 then
    select fa16importe
	into   p_return
	from   fa1600
	where  fa1600.fa04codfact =p_RNF.CODFACT
	   and fa1600.fa16nuMlinea=p_RNF.NUMLINEA;
else
	p_return := p_RNF.COSTECATEG * p_RNF.CANTFACT * 1.05;
END IF;

RETURN p_return;

END FAFN15;

END FAPK01;
/

/**********************************************************************************************************************
  FUNCTION      : Purge_Log()
  SCOPE         : PRIVATE

  DESCRIPTION   : elimina de la tabla de log todos los registros correspondientes al n�mero de operaci�n pasado como
                  parametro. Si por cualquier motivo falla la operaci�n fallase, hace ROLLBACK y devuelve FALSE
  PARAMETERS IN : p_numero_operacion
  RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (error)
 *********************************************************************************************************************/
FUNCTION Purge_Log(p_numero_operacion FA4500.FA45NUMOPERACION%TYPE) RETURN BOOLEAN IS
BEGIN
   DELETE FROM FA4500 WHERE FA45NUMOPERACION = p_numero_operacion;
   COMMIT;

   RETURN TRUE;

   EXCEPTION
   		 WHEN OTHERS THEN
   		 	  ROLLBACK;
   		 	  RETURN FALSE;
END Purge_Log;


/**********************************************************************************************************************
  PROCEDURE     : CorregirTelef()
  SCOPE         : PRIVATE

  DESCRIPTION   : Inserta los RNF debidos a tel�fonos,Dietas acompa�ante y lavander�a
  PARAMETERS IN : (ninguno)
  RETURN VALUE  : BOOLEAN, TRUE (OK), FALSE (SELECT or INSERT error)
 *********************************************************************************************************************/
PROCEDURE CorregirTelef is BEGIN
   insert into fa0300
     select
        fa03numrnf_sequence.nextval,         --numrnf
        42284,                               --categ
        1,                                   --estado
        fa0400.fa04codfact,                  --codfact
        fa0400.ad01codasistenci,
        1,                                   --cantidad
        null,
        fa16precio,
        2,                                   --facturado
        fa16precio,
        null,
        null,
        null,                                --franqsuma
        0,                                   --descuento
        null,                                --garant�a
        null,
        fa04fecfactura,
        10,                                  --dpto realiz
        null,
        null,
        null,
        null,
        fa16numlinea,
        0,
        null,
        fa0400.ad07codproceso,
        null,
        fa16precio,
        null,
   	  null,
   	  null,
   	  null,
   	  null,
	  null
      from
         fa0400,fa1600
      where
            fa1600.fa04codfact=fa0400.fa04codfact
        and fa1600.fa14codnodoconc=0
        and upper(fa1600.fa16descrip) like '%TELEF%'
        and fa0400.fa04cantfact>0
        and fa0400.fa04numfacreal is null
        and not exists (select fa05codcateg
                        from fa0300
                        where fa0300.fa04numfact=fa0400.fa04codfact
                          and fa0300.fa05codcateg=42284);
   commit;
   insert into fa0300
     select
        fa03numrnf_sequence.nextval,         --numrnf
        42286,                               --categ
        1,                                   --estado
        fa0400.fa04codfact,                  --codfact
        fa0400.ad01codasistenci,
        1,                                   --cantidad
        null,
        fa16precio,
        2,                                   --facturado
        fa16precio,
        null,
        null,
        null,                                --franqsuma
        0,                                   --descuento
        null,                                --garant�a
        null,
        fa04fecfactura,
        10,                                  --dpto realiz
        null,
        null,
        null,
        null,
        fa16numlinea,
        0,
        null,
        fa0400.ad07codproceso,
        null,
        fa16precio,
        null,
   	  null,
   	  null,
   	  null,
   	  null,
	  null
      from
         fa0400,fa1600
      where
            fa1600.fa04codfact=fa0400.fa04codfact
        and fa1600.fa14codnodoconc=0
        and upper(fa1600.fa16descrip) like '%LAVAND%'
        and fa0400.fa04cantfact>0
        and fa0400.fa04numfacreal is null
        and not exists (select fa05codcateg
                        from fa0300
                        where fa0300.fa04numfact=fa0400.fa04codfact
                          and fa0300.fa05codcateg=42286);
   commit;
   insert into fa0300
     select
        fa03numrnf_sequence.nextval,         --numrnf
        42285,                               --categ
        1,                                   --estado
        fa0400.fa04codfact,                  --codfact
        fa0400.ad01codasistenci,
        1,                                   --cantidad
        null,
        fa16precio,
        2,                                   --facturado
        fa16precio,
        null,
        null,
        null,                                --franqsuma
        0,                                   --descuento
        null,                                --garant�a
        null,
        fa04fecfactura,
        10,                                  --dpto realiz
        null,
        null,
        null,
        null,
        fa16numlinea,
        0,
        null,
        fa0400.ad07codproceso,
        null,
        fa16precio,
        null,
   	  null,
   	  null,
   	  null,
   	  null,
	     null
      from
         fa0400,fa1600
      where
            fa1600.fa04codfact=fa0400.fa04codfact
        and fa1600.fa14codnodoconc=0
        and upper(fa1600.fa16descrip) like '%COMIDAS%'
        and fa0400.fa04cantfact>0
        and fa0400.fa04numfacreal is null
        and not exists (select fa05codcateg
                        from fa0300
                        where fa0300.fa04numfact=fa0400.fa04codfact
                          and fa0300.fa05codcateg=42285);
   commit;
END CorregirTelef;

/**********************************************************************************************************************
  PROCEDURE     : Write_To_Log()
  SCOPE         : PRIVATE

  DESCRIPTION   : inserta un registro en la tabla de log. Si el par�metro p_commit_log es TRUE hace tambi�n un COMMIT
                  (es necesario cuando el registro es generado por un suceso a nivel de factura)
  PARAMETERS IN : p_numero_operacion, p_mensaje, p_tipo_suceso, p_cod_factura, p_numero_linea, p_numero_RNF
                  p_commit_log (TRUE = si hay que hacer COMMIT)
 *********************************************************************************************************************/
PROCEDURE Write_To_Log(
	p_numero_operacion FA4500.FA45NUMOPERACION%TYPE,
	p_mensaje   	   FA4500.FA45DESSUCESO%TYPE,
	p_tipo_suceso 	   FA4500.FA45TIPOSUCESO%TYPE,
	p_cod_factura 	   FA4500.FA04CODFACT%TYPE,
	p_numero_linea 	   FA4500.FA16NUMLINEA%TYPE,
	p_numero_RNF 	   FA4500.FA03NUMRNF%TYPE,
	p_commit_log       BOOLEAN
 ) IS

-- Cualquier error en el procedimiento se propaga al procedimiento llamante.
BEGIN

   -- INSERT en la tabla de log
   INSERT INTO FA4500
   	   		  (FA45CODREGISTRO,
   			   FA45NUMOPERACION,
   			   FA45FECREGISTRO,
   			   FA04CODFACT,
   			   FA16NUMLINEA,
   			   FA03NUMRNF,
   			   FA45TIPOSUCESO,
   			   FA45DESSUCESO)
   	   VALUES (FA45CODREGISTRO_SEQUENCE.NEXTVAL,
   	   		   p_numero_operacion,
   	   		   SYSDATE,
   	   		   p_cod_factura,
   	   		   p_numero_linea,
   			   p_numero_RNF,
   			   p_tipo_suceso,
   			   p_mensaje);

   -- Dependiendo del valor del par�metro correspondiente, COMMIT
   IF p_commit_log THEN

   COMMIT;

   END IF;

END Write_To_Log;

