
CREATE OR REPLACE VIEW FA0411J ( FA48CODDISKACUNSA, 
MOVACUN ) AS SELECT  
FA4800.FA48CODDISKACUNSA,  
'M'                                                         || -- 'M'  
SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de Factura  
'8'                                                         || -- Tipo 8: Pruebas Menos las de Taller Ortopedico  
TO_CHAR(FA03FECHA,'YYYYMMDD')                               || -- fecha de la prueba  
LPAD(FA0500.FA05CODCATEG,6,'0')                             || -- Codigo de la prueba : Number(6)  
LPAD(FA03CANTFACT,4,'0')                                    || -- Cantidad Facturada: Number(4)  
LPAD(FA03CANTFACT*FA03PRECIOFACT,10,'0')                    || -- Precio total Facturado: Number(10)  
LPAD(FA0300.AD02CODDPTO_R,3,'0')                            || -- Departamento que realiza: Number(3)  
LPAD(FA0300.SG02COD_R,4,' ')                                   -- Doctor que realiza  
FROM  
FA0400,FA0300,FA0500,FA4800,FA4900  
WHERE  
FA0400.FA04CODFACT=FA0300.FA04NUMFACT  
AND FA0300.FA05CODCATEG=FA0500.FA05CODCATEG  
AND FA0500.FA08CODGRUPO IN (7,9)  
AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA  
AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT  
AND FA0300.AD02CODDPTO_R<>404 --TALLER ORTOPEDICO  
AND FA04NUMFACREAL IS NULL  
UNION ALL  
SELECT  
FA4800.FA48CODDISKACUNSA,  
'M'                                                         || -- 'M'  
SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de factura  
'1'                                                         || -- Tipo 1: Farmacia sin Protesis  
LPAD(SUM(FA03CANTFACT*FA03PRECIOFACT),10,'0')                  -- Suma de Farmacia sin Protesis  
FROM  
FA0400,FA0300,FA0500,FR7300,FA4800,FA4900  
WHERE  
FA0400.FA04CODFACT=FA0300.FA04NUMFACT  
AND FA0300.FA05CODCATEG=FA0500.FA05CODCATEG  
AND FA0500.FA08CODGRUPO=1  
AND FA0500.FA05CODORIGEN=FR7300.FR73CODINTFAR  
AND FR7300.FR00CODGRPTERAP NOT LIKE 'QI%'  
AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA  
AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT  
AND FA04NUMFACREAL IS NULL  
GROUP BY  
FA4800.FA48CODDISKACUNSA,FA0400.FA04NUMFACT  
UNION ALL  
SELECT  
FA4800.FA48CODDISKACUNSA,  
'M'                                                         || -- 'M'  
SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de factura  
'9'                                                         || -- Tipo 9: Farmacia Prótesis + Taller Ortopédico  
LPAD(SUM(FA03CANTFACT*FA03PRECIOFACT),10,'0')                  -- Suma Prótesis + Taller Ortopédico  
FROM  
FA0400,FA0300,FA0500,FA4800,FA4900  
WHERE  
FA0400.FA04CODFACT=FA0300.FA04NUMFACT  
AND FA04NUMFACREAL IS NULL  
AND FA0300.FA05CODCATEG=FA0500.FA05CODCATEG  
AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA  
AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT  
AND ( (FA0300.AD02CODDPTO_R=404  
AND FA0500.FA08CODGRUPO=8  
)  
OR  
(FA0500.FA05CODORIGEN IN  
(SELECT FR73CODINTFAR -- PROTESIS  
FROM FR7300  
WHERE FR00CODGRPTERAP LIKE 'QI%')  
)  
AND FA0500.FA08CODGRUPO=1  
)  
GROUP BY  
FA4800.FA48CODDISKACUNSA,FA0400.FA04NUMFACT  
UNION ALL  
SELECT  
FA4800.FA48CODDISKACUNSA,  
'M'                                                         || -- 'M'  
SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de Factura  
'2'                                                         || -- Tipo 2: Intervenciones  
TO_CHAR(FA03FECHA,'YYYYMMDD')                               || -- fecha de la Intervencion  
LPAD(FA0500.FA05CODCATEG,6,'0')                             || -- Codigo de la intervencion : Number(6)  
LPAD(FA03CANTFACT,4,'0')                                    || -- Cantidad Facturada: Number(4)  
LPAD(FA03CANTFACT*FA03PRECIOFACT,10,'0')                    || -- Precio total Facturado: Number(10)  
LPAD(AD02CODDPTO_R,3,'0')                                   || -- Departamento que realiza: Number(3)  
LPAD(SG02COD_R,4,' ')                                          -- Doctor que realiza  
FROM  
FA0400,FA0300,FA0500,FA4800,FA4900  
WHERE  
FA0400.FA04CODFACT=FA0300.FA04NUMFACT  
AND FA0300.FA05CODCATEG=FA0500.FA05CODCATEG  
AND FA0500.FA08CODGRUPO=6  
AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA  
AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT  
AND FA04NUMFACREAL IS NULL  
UNION ALL  
SELECT  
FA4800.FA48CODDISKACUNSA,  
'M'                                                         || -- 'M'  
SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de Factura  
'3'                                                         || -- Tipo 3: Derechos de quirofano  
TO_CHAR(FA03FECHA,'YYYYMMDD')                               || -- fecha de la Intervencion  
LPAD(FA05CODCATEG,6,'0')                                    || -- Codigo de derechos de quirofano : Number(6)  
LPAD(FA03CANTFACT*15,4,'0')                                    -- Cantidad Facturada (Minutos): Number(4)  
FROM  
FA0400,FA0300,FA4800,FA4900  
WHERE  
FA0400.FA04CODFACT=FA0300.FA04NUMFACT  
AND FA0300.FA05CODCATEG=35354  
AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA  
AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT  
AND FA04NUMFACREAL IS NULL  
UNION ALL  
SELECT  
FA4800.FA48CODDISKACUNSA,  
'M'                                                         || -- 'M'  
SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de Factura  
'4'                                                         || -- Tipo 4: Anestesista  
TO_CHAR(FA03FECHA,'YYYYMMDD')                               || -- fecha de la Intervencion  
LPAD(FA05CODCATEG,6,'0')                                    || -- Codigo de honorarios anestesista : Number(6)  
LPAD(FA03CANTFACT,4,'0')                                    || -- Cantidad Facturada: Number(4)  
LPAD(FA03CANTFACT*FA03PRECIOFACT,10,'0')                       -- Precio total Facturado: Number(10)  
FROM  
FA0400,FA0300,FA4800,FA4900  
WHERE  
FA0400.FA04CODFACT=FA0300.FA04NUMFACT  
AND FA0300.FA05CODCATEG=35353  
AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA  
AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT  
AND FA04NUMFACREAL IS NULL  
UNION ALL  
SELECT  
FA4800.FA48CODDISKACUNSA,  
'M'                                                         || -- 'M'  
SUBSTR(FA0400.FA04NUMFACT,4)                                || -- Numero de Factura  
'5'                                                         || -- Tipo 5: Estancias  
TO_CHAR(FA03FECHA,'YYYYMMDD')                               || -- fecha de la estancia  
LPAD(FA0500.FA05CODCATEG,6,'0')                             || -- Codigo de la estancia : Number(6)  
LPAD(FA03CANTFACT,4,'0')                                    || -- Cantidad Facturada: Number(4)  
LPAD(FA03CANTFACT*FA03PRECIOFACT,10,'0')                    || -- Precio total Facturado: Number(10)  
LPAD(AD02CODDPTO_R,3,'0')                                   || -- Departamento que realiza: Number(3)  
LPAD(SG02COD_R,4,' ')                                          -- Doctor que realiza  
FROM  
FA0400,FA0300,FA0500,FA4800,FA4900  
WHERE  
FA0400.FA04CODFACT=FA0300.FA04NUMFACT  
AND FA0300.FA05CODCATEG=FA0500.FA05CODCATEG  
AND FA0500.FA08CODGRUPO=5  
AND FA4800.FA48CODDISKACUNSA=FA4900.FA48CODDISKACUNSA  
AND FA4900.FA04NUMFACT=FA0400.FA04NUMFACT  
AND FA04NUMFACREAL IS NULL
