VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsLabel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private intLeft As Integer, lngTop As Long, strValor As String, blnMayor As Boolean
Private intFranja As Integer

Public Property Let Left(intLeftNuevo As Integer)
   intLeft = intLeftNuevo
End Property

Public Property Let Top(lngTopNuevo As Long)
   lngTop = lngTopNuevo
End Property

Public Property Let Valor(strValorNuevo As String)
   strValor = strValorNuevo
End Property

Public Property Let Mayor(blnMayorNuevo As Boolean)
   blnMayor = blnMayorNuevo
End Property

Public Property Let Franja(intFranjaNuevo As Integer)
   intFranja = intFranjaNuevo
End Property

Public Property Get Left() As Integer
   Left = intLeft
End Property

Public Property Get Top() As Long
   Top = lngTop
End Property

Public Property Get Valor() As String
   Valor = strValor
End Property

Public Property Get Mayor() As Boolean
   Mayor = blnMayor
End Property

Public Property Get Franja() As Integer
   Franja = intFranja
End Property

