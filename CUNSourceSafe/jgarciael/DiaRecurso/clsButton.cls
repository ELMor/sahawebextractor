VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsButton"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private intFranja As Integer, intCita As Integer, intLeft As Integer, lngTop As Long
Private lngHeight As Long, lngColor As Long, strPaciente As String
Private strDescAct As String, strDescripAct As String, lngCodPaciente As Long
Private bytCitas As Byte

Public Property Let Franja(intFranjaNuevo As Integer)
   intFranja = intFranjaNuevo
End Property

Public Property Let Cita(intCitaNuevo As Integer)
   intCita = intCitaNuevo
End Property

Public Property Let Left(intLeftNuevo As Integer)
   intLeft = intLeftNuevo
End Property

Public Property Let Top(lngTopNuevo As Long)
   lngTop = lngTopNuevo
End Property

Public Property Let Height(lngHeightNuevo As Long)
   lngHeight = lngHeightNuevo
End Property

Public Property Let Color(lngColorNuevo As Long)
   lngColor = lngColorNuevo
End Property

Public Property Let Actividad(strDescripActNuevo As String)
   strDescripAct = strDescripActNuevo
End Property

Public Property Let Paciente(strPacienteNuevo As String)
   strPaciente = strPacienteNuevo
End Property

Public Property Let codPaciente(lngCodPacienteNuevo As Long)
   lngCodPaciente = lngCodPacienteNuevo
End Property

Public Property Let desActuacion(strDescActNuevo As String)
   strDescAct = strDescActNuevo
End Property

Public Property Let citas(bytCitasNuevo As Byte)
   bytCitas = bytCitasNuevo
End Property

Public Property Get Franja() As Integer
   Franja = intFranja
End Property

Public Property Get Cita() As Integer
   Cita = intCita
End Property

Public Property Get Top() As Long
   Top = lngTop
End Property

Public Property Get Left() As Integer
   Left = intLeft
End Property

Public Property Get Height() As Long
   Height = lngHeight
End Property

Public Property Get Color() As Long
   Color = lngColor
End Property

Public Property Get Actividad() As String
   Actividad = strDescripAct
End Property

Public Property Get Paciente() As String
   Paciente = strPaciente
End Property

Public Property Get codPaciente() As Long
   codPaciente = lngCodPaciente
End Property

Public Property Get desActuacion() As String
   desActuacion = strDescAct
End Property

Public Property Get citas() As Byte
   citas = bytCitas
End Property

