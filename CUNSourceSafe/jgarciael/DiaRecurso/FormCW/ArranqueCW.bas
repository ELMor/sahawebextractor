Attribute VB_Name = "ModuleCW"
Option Explicit

Const cwMsgConnectError As String = "Error durante la conexi�n con el servidor de CodeWizard"

Public objCW       As clsCW
Public objApp      As clsCWApp
Public objPipe     As clsCWPipeLine
Public objGen      As clsCWGen
Public objError    As clsCWError
Public objEnv      As clsCWEnvironment
Public objMouse    As clsCWMouse
Public objSecurity As clsCWSecurity

Public Function InitApp() As Boolean
  On Error GoTo InitError
  Set objCW = New clsCW
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objError = objCW.objError
  Set objEnv = objCW.objEnv
  Set objMouse = objCW.objMouse
  Set objSecurity = objCW.objSecurity
  InitApp = True
  Exit Function

InitError:
  Call MsgBox(cwMsgConnectError, vbCritical)
  InitApp = False
End Function


