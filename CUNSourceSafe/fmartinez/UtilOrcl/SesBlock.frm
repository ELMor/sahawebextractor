VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmSesBlock 
   Caption         =   "Form1"
   ClientHeight    =   9105
   ClientLeft      =   90
   ClientTop       =   375
   ClientWidth     =   9510
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   9.75
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   9105
   ScaleWidth      =   9510
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   7755
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   11655
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   0
      AllowUpdate     =   0   'False
      SelectTypeCol   =   3
      SelectTypeRow   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   3200
      _ExtentX        =   20558
      _ExtentY        =   13679
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdRefrescar 
      Caption         =   "&Refrescar"
      Height          =   495
      Left            =   5400
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   8040
      Width           =   1575
   End
   Begin VB.CommandButton cmdKill 
      Caption         =   "Kill"
      Height          =   495
      Left            =   3720
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   8040
      Width           =   1575
   End
   Begin VB.CommandButton cmdTraceOff 
      Caption         =   "Trace OFF"
      Height          =   495
      Left            =   2040
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   8040
      Width           =   1575
   End
   Begin VB.CommandButton cmdTraceOn 
      Caption         =   "Trace On"
      Height          =   495
      Left            =   120
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   8040
      Width           =   1695
   End
   Begin VB.CommandButton cmdContinuar 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9840
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   8040
      Width           =   1935
   End
End
Attribute VB_Name = "frmSesBlock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' mostrar las sesiones activas (con estadisticas de un intervalo)
' con posibilidad de:
'   - tirar sesiones
'   - activar el trace de una sesion

Option Explicit

' identificacion del formulario "frmSesActiv" que ha llamado a este
Public fSesActiv As frmSesActiv

Private Sub cmdContinuar_Click()
    Screen.MousePointer = vbHourglass
    Me.Hide
End Sub

Private Sub cmdKill_Click()
  ' terminar una sesi�n oracle
  
  Dim strA As String
  Dim qd As rdoQuery
  Dim rs As rdoResultset
  Dim strSID As String
  Dim strSERIAL As String
  Dim strUSER As String
  
  strSID = SSDBGrid1.Columns(0).Text
  strSERIAL = SSDBGrid1.Columns(1).Text
  strUSER = SSDBGrid1.Columns(2).Text
  
  
  strA = "Desea terminar la sesi�n:" & vbCrLf & _
         "SID: " & strSID & vbCrLf & _
         "SERIAL: " & strSERIAL & vbCrLf & _
         "USER: " & strUSER & vbCrLf
  If MsgBox(strA, vbYesNo + vbQuestion + vbDefaultButton2, _
            "Terminar una Sesi�n ?") = vbYes Then
     Set qd = conBaseDatos.CreateQuery("", " ")
     Screen.MousePointer = vbHourglass
     qd.SQL = "alter system kill session '" & _
              strSID & "," & _
              strSERIAL & "'"
     On Error Resume Next
     qd.Execute rdExecDirect
     Screen.MousePointer = vbDefault
     If Err.Number <> 0 Then
        MsgBox (R_ErrorRdoMsg)
        On Error GoTo 0
     Else
        On Error GoTo 0
        MsgBox ("Sesi�n terminada")
        ' refrescar la pantalla
           cmdRefrescar_Click
     End If
     qd.Close
  End If
End Sub

Public Sub cmdRefrescar_Click()
    ' mostrar las sesiones que se estan ejecutando en este momento
    ' con las estadisticas de ejecucion de un intervalo de tiempo
    
    Dim strA As String
    Dim strB As String
    Dim strC As String
    Dim strD As String
    Dim qd As rdoQuery
    Dim rs As rdoResultset
    Dim lngResult As Long
    Dim lngNSesiones As Long

    Screen.MousePointer = vbHourglass

    strA = "SELECT SUBSTR(V$SESSION.SID,1,4) SID,"
    strA = strA & "       SUBSTR(V$SESSION.SERIAL#,1,6) SERIAL,"
    strA = strA & "       SUBSTR(V$SESSION.USERNAME,1,10) USER_,"
    strA = strA & "       V$SESSION.OSUSER USER_OS,"
    strA = strA & "       V$SESSION.STATUS STATUS,"
    strA = strA & "       SUBSTR(TO_CHAR(V$SESSION.LOGON_TIME,'DD/MM HH24:MI'),1,11) LOGON_TIME,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'user calls') USER_CALLS,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'user commits') COMMITS,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'session logical reads') LOGICAL_READS,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'table fetch by rowid') FETCH_BY_ROWID,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'sorts (rows)') SORTS_ROWS,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'CPU used by this session') CPU,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'redo size') redo_size,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'session pga memory') memory,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'opened cursors current') opened_cursors,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'bytes sent via SQL*Net to client') bs_SQL_Net,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'bytes received via SQL*Net from client') br_SQL_Net,"
    strA = strA & "       V$SESS_IO.BLOCK_GETS BLOCK_GETS,"
    strA = strA & "       V$SESS_IO.BLOCK_CHANGES BLOCK_CHANGES,"
    strA = strA & "       V$SESSION.PROGRAM PROGRAM,"
    strA = strA & "       V$SESSION.MACHINE MACHINE,"
    strA = strA & "       V$SESSION.TERMINAL TERMINAL_,"
    strA = strA & "       DECODE(V$SESSION.COMMAND,0,'0,',"
    strA = strA & "                                1,'1, CREATE TABLE',"
    strA = strA & "                                2,'2, INSERT',"
    strA = strA & "                                3,'3, SELECT',"
    strA = strA & "                                4,'4, CREATE CLUSTER',"
    strA = strA & "                                5,'5, ALTER CLUSTER',"
    strA = strA & "                                6,'6, UPDATE',"
    strA = strA & "                                7,'7, DELETE',"
    strA = strA & "                                  ' ??????????') COMANDO,"
    strA = strA & "       V$SESSION_WAIT.EVENT,"
    strA = strA & "       V$SESSION_WAIT.SECONDS_IN_WAIT,"
    strA = strA & "       V$SESSION_WAIT.STATE"
    strA = strA & "       FROM V$SESSION,V$SESS_IO,V$SESSION_WAIT"
    strA = strA & "       WHERE V$SESSION.SID = V$SESSION_WAIT.SID (+)  AND"
    strA = strA & "             ( V$SESSION.STATUS = 'ACTIVE' OR "
    strA = strA & "               V$SESSION_WAIT.SECONDS_IN_WAIT < 10 ) AND"
    strA = strA & "             V$SESSION.SID = V$SESS_IO.SID           AND"
    strA = strA & "             V$SESSION.USERNAME IS NOT NULL"
    strA = strA & "       ORDER BY V$SESSION.USERNAME,V$SESSION.LOGON_TIME"

    lngResult = R_OpenResultset(qd, rs, rdOpenForwardOnly, rdConcurReadOnly, _
                                strA, 999999, 0)
    SSDBGrid1.Redraw = False
    SSDBGrid1.RemoveAll
    lngNSesiones = 0 ' numero de sesiones
    Do Until rs.EOF
       lngNSesiones = lngNSesiones + 1 ' numero de sesiones
       strC = "K" & Format(Val(rs("SID")), "0000") & Format(Val(rs("SERIAL")), "000000")
       strB = rs("SID")
       strB = strB & vbTab & rs("SERIAL")
       strB = strB & vbTab & rs("USER_")
       strB = strB & vbTab & rs("USER_OS")
       strB = strB & vbTab & rs("STATUS")
       strB = strB & vbTab & rs("LOGON_TIME")
       ' USER_CALLS
          On Error Resume Next
          strD = rs("USER_CALLS") - Val(fSesActiv.colUSER_CALLS.Item(strC))
          If Err.Number <> 0 Then
             strD = -1
          End If
          On Error GoTo 0
          strB = strB & vbTab & strD
       ' COMMITS
          On Error Resume Next
          strD = rs("COMMITS") - Val(fSesActiv.colCOMMITS.Item(strC))
          If Err.Number <> 0 Then
             strD = -1
          End If
          On Error GoTo 0
          strB = strB & vbTab & strD
       ' LOGICAL_READS
          On Error Resume Next
          strD = rs("LOGICAL_READS") - Val(fSesActiv.colLOGICAL_READS.Item(strC))
          If Err.Number <> 0 Then
             strD = -1
          End If
          On Error GoTo 0
          strB = strB & vbTab & strD
       ' FETCH_BY_ROWID
          On Error Resume Next
          strD = rs("FETCH_BY_ROWID") - Val(fSesActiv.colFETCH_BY_ROWID.Item(strC))
          If Err.Number <> 0 Then
             strD = -1
          End If
          On Error GoTo 0
          strB = strB & vbTab & strD
       ' SORTS_ROWS
          On Error Resume Next
          strD = rs("SORTS_ROWS") - Val(fSesActiv.colSORTS_ROWS.Item(strC))
          If Err.Number <> 0 Then
             strD = -1
          End If
          On Error GoTo 0
          strB = strB & vbTab & strD
       ' CPU
          On Error Resume Next
          strD = rs("CPU") - Val(fSesActiv.colCPU.Item(strC))
          If Err.Number <> 0 Then
             strD = -1
          End If
          On Error GoTo 0
          strB = strB & vbTab & strD
       strB = strB & vbTab & rs("REDO_SIZE")
       strB = strB & vbTab & rs("MEMORY")
       strB = strB & vbTab & rs("OPENED_CURSORS")
       ' BS_SQL_NET
          On Error Resume Next
          strD = rs("BS_SQL_NET") - Val(fSesActiv.colBS_SQL.Item(strC))
          If Err.Number <> 0 Then
             strD = -1
          End If
          On Error GoTo 0
          strB = strB & vbTab & strD
       ' BR_SQL_NET
          On Error Resume Next
          strD = rs("BR_SQL_NET") - Val(fSesActiv.colBR_SQL.Item(strC))
          If Err.Number <> 0 Then
             strD = -1
          End If
          On Error GoTo 0
          strB = strB & vbTab & strD
       ' BLOCK_GETS
          On Error Resume Next
          strD = rs("BLOCK_GETS") - Val(fSesActiv.colBLOCK_GETS.Item(strC))
          If Err.Number <> 0 Then
             strD = -1
          End If
          On Error GoTo 0
          strB = strB & vbTab & strD
       ' BLOCK_CHANGES
          On Error Resume Next
          strD = rs("BLOCK_CHANGES") - Val(fSesActiv.colBLOCK_CHANGES.Item(strC))
          If Err.Number <> 0 Then
             strD = -1
          End If
          On Error GoTo 0
          strB = strB & vbTab & strD
       strB = strB & vbTab & rs("PROGRAM")
       strB = strB & vbTab & rs("MACHINE")
       strB = strB & vbTab & rs("TERMINAL_")
       strB = strB & vbTab & rs("COMANDO")
       strB = strB & vbTab & rs("EVENT")
       strB = strB & vbTab & rs("SECONDS_IN_WAIT")
       strB = strB & vbTab & rs("STATE")
       
       SSDBGrid1.AddItem strB
       rs.MoveNext
    Loop
    rs.Close
    qd.Close
    SSDBGrid1.Redraw = True
    
    Caption = "Sesiones que est�n ejecutandose en este momento " & _
              "(Periodo de estadisticas: " & _
              R_SegundosHMS(DateDiff("s", fSesActiv.HoraEstadisticas, Now)) & _
              "   Numero de Sesiones: " & CStr(lngNSesiones) & " )        " & _
              Format(Now, "dd/mm/yyyy hh:nn:ss")
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdTraceOff_Click()
  ' desactivar el trace de una sesi�n oracle
  
  Dim strA As String
  Dim qd As rdoQuery
  Dim strSID As String
  Dim strSERIAL As String
  Dim strUSER As String
  
  strSID = SSDBGrid1.Columns(0).Text
  strSERIAL = SSDBGrid1.Columns(1).Text
  strUSER = SSDBGrid1.Columns(2).Text
  
  strA = "Desea desactivar el Trace de la sesi�n:" & vbCrLf & _
         "SID: " & strSID & vbCrLf & _
         "SERIAL: " & strSERIAL & vbCrLf & _
         "USER: " & strUSER & vbCrLf
  If MsgBox(strA, vbYesNo + vbQuestion + vbDefaultButton2, _
            "Desactivar el trace de una Sesi�n ?") = vbYes Then
     Set qd = conBaseDatos.CreateQuery("", " ")
     Screen.MousePointer = vbHourglass
     qd.SQL = "{ call GC_TRACE(? , ? , ? )}"
     qd(0).Value = strSID
     qd(1).Value = strSERIAL
     qd(2).Value = "FALSE"
     On Error Resume Next
     qd.Execute
     Screen.MousePointer = vbDefault
     If Err.Number <> 0 Then
        MsgBox (R_ErrorRdoMsg)
        On Error GoTo 0
     Else
        On Error GoTo 0
        MsgBox ("Trace desactivado")
     End If
     qd.Close
  End If
End Sub

Private Sub cmdTraceOn_Click()
  ' activar el trace de una sesi�n oracle
  
  Dim strA As String
  Dim qd As rdoQuery
  Dim strSID As String
  Dim strSERIAL As String
  Dim strUSER As String
  
  strSID = SSDBGrid1.Columns(0).Text
  strSERIAL = SSDBGrid1.Columns(1).Text
  strUSER = SSDBGrid1.Columns(2).Text
  
  strA = "Desea activar el Trace de la sesi�n:" & vbCrLf & _
         "SID: " & strSID & vbCrLf & _
         "SERIAL: " & strSERIAL & vbCrLf & _
         "USER: " & strUSER & vbCrLf
  If MsgBox(strA, vbYesNo + vbQuestion + vbDefaultButton2, _
            "Activar el trace de una Sesi�n ?") = vbYes Then
     Set qd = conBaseDatos.CreateQuery("", " ")
     Screen.MousePointer = vbHourglass
     qd.SQL = "{ call GC_TRACE(? , ? , ? )}"
     qd(0).Value = strSID
     qd(1).Value = strSERIAL
     qd(2).Value = "TRUE"
     On Error Resume Next
     qd.Execute
     Screen.MousePointer = vbDefault
     If Err.Number <> 0 Then
        MsgBox (R_ErrorRdoMsg)
        On Error GoTo 0
     Else
        On Error GoTo 0
        MsgBox ("Trace activado")
     End If
     qd.Close
  End If

End Sub

Private Sub Form_Load()
  cmdContinuar.ToolTipText = "Salir de la ventana"
  cmdContinuar.Picture = frmMenu.imlIcons.ListImages("salir").Picture
  cmdKill.ToolTipText = "Terminar la sesi�n seleccionada"
  cmdTraceOn.ToolTipText = "Activar el Trace de la sesi�n seleccionada"
  cmdTraceOff.ToolTipText = "Desactivar el Trace de la sesi�n seleccionada"
  cmdRefrescar.ToolTipText = "Refrescar la pantalla"
End Sub

Private Sub Form_Resize()
    SSDBGrid1.Width = Me.ScaleWidth - (SSDBGrid1.Left * 2)
    SSDBGrid1.Height = Me.ScaleHeight - _
        (SSDBGrid1.Top * 3) - cmdContinuar.Height
    cmdTraceOn.Top = SSDBGrid1.Height + (SSDBGrid1.Top * 2)
    cmdTraceOff.Top = SSDBGrid1.Height + (SSDBGrid1.Top * 2)
    cmdKill.Top = SSDBGrid1.Height + (SSDBGrid1.Top * 2)
    cmdRefrescar.Top = SSDBGrid1.Height + (SSDBGrid1.Top * 2)
    cmdContinuar.Top = SSDBGrid1.Height + (SSDBGrid1.Top * 2)
End Sub

Private Sub SSDBGrid1_KeyDown(KeyCode As Integer, Shift As Integer)
   ' Copiar las columnas seleccionadas al Clipboard
   Dim lngI As Long
   Dim lngJ As Long
   Dim lngK As Long
   Dim strA As String
   Dim NColumnasSeleccionadas As Long
   
   If KeyCode = vbKeyC And Shift = vbCtrlMask Then
      ' Se ha pulsado Ctrl-C para copiar las columnas seleccionadas al Clipboard
      Screen.MousePointer = vbHourglass
      
      ' verificar si hay alguna columna seleccionada
      NColumnasSeleccionadas = 0
      For lngI = 0 To SSDBGrid1.Cols - 1
         If SSDBGrid1.Columns(lngI).Selected = True Then
            NColumnasSeleccionadas = NColumnasSeleccionadas + 1
         End If
      Next
      
      ' copiar las columnas seleccionadas
      If NColumnasSeleccionadas > 0 Then
         strA = ""
         
         ' cabeceras de las columnas
         lngJ = 0
         For lngI = 0 To SSDBGrid1.Cols - 1
            If SSDBGrid1.Columns(lngI).Selected = True Then
               strA = strA & SSDBGrid1.Columns(lngI).Caption
               lngJ = lngJ + 1
               If lngJ < NColumnasSeleccionadas Then
                  strA = strA & vbTab
               End If
            End If
         Next
         strA = strA & vbCrLf
          
         ' datos de las columnas seleccionadas
         SSDBGrid1.Redraw = False
         SSDBGrid1.MoveFirst
         For lngK = 0 To SSDBGrid1.Rows - 1
            lngJ = 0
            For lngI = 0 To SSDBGrid1.Cols - 1
               If SSDBGrid1.Columns(lngI).Selected = True Then
                  strA = strA & SSDBGrid1.Columns(lngI).Text
                  lngJ = lngJ + 1
                  If lngJ < NColumnasSeleccionadas Then
                     strA = strA & vbTab
                  End If
               End If
            Next
            strA = strA & vbCrLf
            SSDBGrid1.MoveNext
         Next lngK
         SSDBGrid1.Redraw = True
          
      End If
      
      Clipboard.Clear
      Clipboard.SetText strA
      Screen.MousePointer = vbDefault
   End If
End Sub
