VERSION 5.00
Begin VB.Form frmEstGraf 
   ClientHeight    =   3135
   ClientLeft      =   90
   ClientTop       =   375
   ClientWidth     =   4620
   LinkTopic       =   "Form1"
   ScaleHeight     =   3135
   ScaleWidth      =   4620
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdMostrar 
      Caption         =   "&Mostrar Sesiones"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   720
      TabIndex        =   4
      Top             =   7920
      Width           =   3675
   End
   Begin VB.TextBox txtFecha 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   1680
      Width           =   2715
   End
   Begin VB.CommandButton cmdCapturar 
      Caption         =   "&Capturar Estadisticas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   720
      TabIndex        =   1
      Top             =   6720
      Width           =   3675
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10200
      TabIndex        =   0
      Top             =   8040
      Width           =   1575
   End
   Begin VB.Label lblFecha 
      Caption         =   "Estadisticas Memorizadas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   1380
      Width           =   3615
   End
End
Attribute VB_Name = "frmEstGraf"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' mostrar las sesiones que se estan ejecutando en este momento
' con las estadisticas de ejecucion de un intervalo de tiempo
Option Explicit

' estadisticas memorizadas de las sesiones activas
Public colUSER_CALLS As New Collection
Public colCOMMITS As New Collection
Public colLOGICAL_READS As New Collection
Public colFETCH_BY_ROWID As New Collection
Public colSORTS_ROWS As New Collection
Public colCPU As New Collection
Public colBS_SQL As New Collection
Public colBR_SQL As New Collection
Public colBLOCK_GETS As New Collection
Public colBLOCK_CHANGES As New Collection

' control de si se han capturado las estadistivas
Public bolEstadisticasCapturadas As Boolean

' Hora en la que se han memorizado las estadisticas
Public HoraEstadisticas As Date


Private Sub cmdCapturar_Click()
    ' capturar las estadisticas de las sesiones activas
    
    Dim strA As String
    Dim strB As String
    Dim qd As rdoQuery
    Dim rs As rdoResultset
    Dim lngResult As Long
    Dim lngI As Long
    
    Screen.MousePointer = vbHourglass
    
    ' borrar estadisticas anteriores
    For lngI = 1 To colUSER_CALLS.Count
        colUSER_CALLS.Remove 1
        colCOMMITS.Remove 1
        colLOGICAL_READS.Remove 1
        colFETCH_BY_ROWID.Remove 1
        colSORTS_ROWS.Remove 1
        colCPU.Remove 1
        colBS_SQL.Remove 1
        colBR_SQL.Remove 1
        colBLOCK_GETS.Remove 1
        colBLOCK_CHANGES.Remove 1
    Next lngI

    
    strA = "SELECT V$SESSION.SID SID,"
    strA = strA & "       V$SESSION.SERIAL# SERIAL,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'user calls') USER_CALLS,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'user commits') COMMITS,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'session logical reads') LOGICAL_READS,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'table fetch by rowid')  FETCH_BY_ROWID,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'sorts (rows)') SORTS_ROWS,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'CPU used by this session') CPU,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'bytes sent via SQL*Net to client') BS_SQL,"
    strA = strA & "       SYSTEM.GC_SESSTAT(v$session.sid,'bytes received via SQL*Net from client') BR_SQL,"
    strA = strA & "       V$SESS_IO.BLOCK_GETS BLOCK_GETS,"
    strA = strA & "       V$SESS_IO.BLOCK_CHANGES BLOCK_CHANGES"
    strA = strA & "       FROM V$SESSION,V$SESS_IO,V$SESSION_WAIT"
    strA = strA & "       WHERE V$SESSION.SID = V$SESSION_WAIT.SID (+)  AND"
    strA = strA & "             ( V$SESSION.STATUS = 'ACTIVE' OR "
    strA = strA & "               V$SESSION_WAIT.SECONDS_IN_WAIT < 10 ) AND"
    strA = strA & "             V$SESSION.SID = V$SESS_IO.SID           AND"
    strA = strA & "             V$SESSION.USERNAME IS NOT NULL"
    lngResult = R_OpenResultset(qd, rs, rdOpenForwardOnly, rdConcurReadOnly, _
                                strA, 999999, 0)
    Do Until rs.EOF
       strA = "K" & Format(Val(rs("SID")), "0000") & Format(Val(rs("SERIAL")), "000000")
       colUSER_CALLS.Add CStr(rs("USER_CALLS")), strA
       colCOMMITS.Add CStr(rs("COMMITS")), strA
       colLOGICAL_READS.Add CStr(rs("LOGICAL_READS")), strA
       colFETCH_BY_ROWID.Add CStr(rs("FETCH_BY_ROWID")), strA
       colSORTS_ROWS.Add CStr(rs("SORTS_ROWS")), strA
       colCPU.Add CStr(rs("CPU")), strA
       colBS_SQL.Add CStr(rs("BS_SQL")), strA
       colBR_SQL.Add CStr(rs("BR_SQL")), strA
       colBLOCK_GETS.Add CStr(rs("BLOCK_GETS")), strA
       colBLOCK_CHANGES.Add CStr(rs("BLOCK_CHANGES")), strA
       rs.MoveNext
    Loop
    rs.Close
    qd.Close

    ' mostrar la fecha/hora en la que se han capturado las estadisticas
    HoraEstadisticas = Now
    bolEstadisticasCapturadas = True
    txtFecha.Text = R_Fecha1(HoraEstadisticas)
    
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdMostrar_Click()
    ' mostrar las sesiones que se estan ejecutando en este momento
    ' con las estadisticas de ejecucion de un intervalo de tiempo
    
    Dim strA As String
    Dim strB As String
    Dim intI As Long
    Dim frmM As New frmSesActi1 ' mostrar Grid

    If bolEstadisticasCapturadas = False Then
       intI = MsgBox("Es Preciso capturar primero las estad�sticas", vbOKOnly, "")
       Exit Sub
    End If

    Screen.MousePointer = vbHourglass
         
  ' a�adir las columnas al GRID
  ' Quita las columnas antiguas
    For intI = frmM.SSDBGrid1.Columns.Count - 1 To 0 Step -1
       frmM.SSDBGrid1.Columns.Remove intI
    Next intI
  ' Agregar columnas
    frmM.SSDBGrid1.Columns.Add 0
    frmM.SSDBGrid1.Columns(0).Caption = "SID"
    frmM.SSDBGrid1.Columns(0).Width = 800
    frmM.SSDBGrid1.Columns.Add 1
    frmM.SSDBGrid1.Columns(1).Caption = "SERIAL"
    frmM.SSDBGrid1.Columns(1).Width = 800
    frmM.SSDBGrid1.Columns.Add 2
    frmM.SSDBGrid1.Columns(2).Caption = "USER"
    frmM.SSDBGrid1.Columns(2).Width = 800
    frmM.SSDBGrid1.Columns.Add 3
    frmM.SSDBGrid1.Columns(3).Caption = "USER_OS"
    frmM.SSDBGrid1.Columns(3).Width = 800
    frmM.SSDBGrid1.Columns.Add 4
    frmM.SSDBGrid1.Columns(4).Caption = "STATUS"
    frmM.SSDBGrid1.Columns(4).Width = 800
    frmM.SSDBGrid1.Columns.Add 5
    frmM.SSDBGrid1.Columns(5).Caption = "LOGON_TIME"
    frmM.SSDBGrid1.Columns(5).Width = 800
    frmM.SSDBGrid1.Columns.Add 6
    frmM.SSDBGrid1.Columns(6).Caption = "USER_CALLS"
    frmM.SSDBGrid1.Columns(6).Width = 800
    frmM.SSDBGrid1.Columns.Add 7
    frmM.SSDBGrid1.Columns(7).Caption = "COMMITS"
    frmM.SSDBGrid1.Columns(7).Width = 800
    frmM.SSDBGrid1.Columns.Add 8
    frmM.SSDBGrid1.Columns(8).Caption = "LOGICAL_READS"
    frmM.SSDBGrid1.Columns(8).Width = 800
    frmM.SSDBGrid1.Columns.Add 9
    frmM.SSDBGrid1.Columns(9).Caption = "FETCH_BY_ROWID"
    frmM.SSDBGrid1.Columns(9).Width = 800
    frmM.SSDBGrid1.Columns.Add 10
    frmM.SSDBGrid1.Columns(10).Caption = "SORTS_ROWS"
    frmM.SSDBGrid1.Columns(10).Width = 800
    frmM.SSDBGrid1.Columns.Add 11
    frmM.SSDBGrid1.Columns(11).Caption = "CPU"
    frmM.SSDBGrid1.Columns(11).Width = 800
    frmM.SSDBGrid1.Columns.Add 12
    frmM.SSDBGrid1.Columns(12).Caption = "REDO_SIZE"
    frmM.SSDBGrid1.Columns(12).Width = 800
    frmM.SSDBGrid1.Columns.Add 13
    frmM.SSDBGrid1.Columns(13).Caption = "MEMORY"
    frmM.SSDBGrid1.Columns(13).Width = 800
    frmM.SSDBGrid1.Columns.Add 14
    frmM.SSDBGrid1.Columns(14).Caption = "OPENED_CURSORS"
    frmM.SSDBGrid1.Columns(14).Width = 800
    frmM.SSDBGrid1.Columns.Add 15
    frmM.SSDBGrid1.Columns(15).Caption = "BS_SQL_NET"
    frmM.SSDBGrid1.Columns(15).Width = 800
    frmM.SSDBGrid1.Columns.Add 16
    frmM.SSDBGrid1.Columns(16).Caption = "BR_SQL_NET"
    frmM.SSDBGrid1.Columns(16).Width = 800
    frmM.SSDBGrid1.Columns.Add 17
    frmM.SSDBGrid1.Columns(17).Caption = "BLOCK_GETS"
    frmM.SSDBGrid1.Columns(17).Width = 800
    frmM.SSDBGrid1.Columns.Add 18
    frmM.SSDBGrid1.Columns(18).Caption = "BLOCK_CHANGES"
    frmM.SSDBGrid1.Columns(18).Width = 800
    frmM.SSDBGrid1.Columns.Add 19
    frmM.SSDBGrid1.Columns(19).Caption = "PROGRAM"
    frmM.SSDBGrid1.Columns(19).Width = 800
    frmM.SSDBGrid1.Columns.Add 20
    frmM.SSDBGrid1.Columns(20).Caption = "MACHINE"
    frmM.SSDBGrid1.Columns(20).Width = 800
    frmM.SSDBGrid1.Columns.Add 21
    frmM.SSDBGrid1.Columns(21).Caption = "TERMINAL_"
    frmM.SSDBGrid1.Columns(21).Width = 800
    frmM.SSDBGrid1.Columns.Add 22
    frmM.SSDBGrid1.Columns(22).Caption = "COMANDO"
    frmM.SSDBGrid1.Columns(22).Width = 800
    frmM.SSDBGrid1.Columns.Add 23
    frmM.SSDBGrid1.Columns(23).Caption = "EVENT"
    frmM.SSDBGrid1.Columns(23).Width = 800
    frmM.SSDBGrid1.Columns.Add 24
    frmM.SSDBGrid1.Columns(24).Caption = "SECONDS_IN_WAIT"
    frmM.SSDBGrid1.Columns(24).Width = 800
    frmM.SSDBGrid1.Columns.Add 25
    frmM.SSDBGrid1.Columns(25).Caption = "STATE"
    frmM.SSDBGrid1.Columns(25).Width = 800

    Set frmM.fSesActiv = Me
    frmM.cmdRefrescar_Click
    
    Screen.MousePointer = vbDefault
    frmM.Show vbModal
    Unload frmM
    Set frmM = Nothing
    
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdSalir_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    Me.Caption = "Sesiones que est�n ejecutandose en este momento " & _
                 "(con estadisticas de un intervalo de tiempo)"
End Sub
