VERSION 5.00
Begin VB.Form frmLogon 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Inicio de sesi�n ODBC"
   ClientHeight    =   3465
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3465
   ScaleWidth      =   4560
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkMemorizarConexion 
      Caption         =   "Memorizar Conexi�n"
      Height          =   255
      Left            =   480
      TabIndex        =   11
      Top             =   2520
      Width           =   2655
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   2760
      TabIndex        =   10
      Top             =   3000
      Width           =   1695
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   3000
      Width           =   1575
   End
   Begin VB.Frame fraConnection 
      Caption         =   "Valores de la conexi�n"
      Height          =   2175
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4335
      Begin VB.TextBox txtDatabase 
         Height          =   375
         Left            =   960
         TabIndex        =   8
         Top             =   1680
         Width           =   3255
      End
      Begin VB.TextBox txtPWD 
         Height          =   375
         IMEMode         =   3  'DISABLE
         Left            =   960
         PasswordChar    =   "*"
         TabIndex        =   7
         Top             =   1200
         Width           =   3255
      End
      Begin VB.TextBox txtUID 
         Height          =   375
         Left            =   960
         TabIndex        =   6
         Top             =   720
         Width           =   3255
      End
      Begin VB.TextBox txtDSN 
         Height          =   375
         Left            =   960
         TabIndex        =   1
         Top             =   240
         Width           =   3255
      End
      Begin VB.Label Label4 
         Caption         =   "Data&base:"
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   1680
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "&Password:"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   1200
         Width           =   1455
      End
      Begin VB.Label Label2 
         Caption         =   "&UID:"
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "&DSN:"
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmLogon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
   
' indica si se ha realizado la conexi�n con la base de datos
Public bolConBaseDatos As Boolean

Private Sub Form_Initialize()
  bolConBaseDatos = False
End Sub

Private Sub cmdCancel_Click()
 ' indicar que no se ha realizado la conexi�n
  bolConBaseDatos = False
  Me.Hide
  
End Sub
Private Sub cmdOK_Click()

  On Error GoTo LocalErr
  
  Dim strConnect As String
  
  Screen.MousePointer = vbHourglass

  strConnect = "ODBC;UID=" & txtUID.Text & _
               ";PWD=" & txtPWD.Text & _
               ";DBQ=" & txtDatabase.Text
  strConnect = "ODBC;UID=" & txtUID.Text & _
               ";PWD=" & txtPWD.Text & _
               ";SRVR=" & txtDatabase.Text
  Set conBaseDatos = rdoEngine.rdoEnvironments(0).OpenConnection( _
         txtDSN.Text, _
         rdDriverNoPrompt, False, strConnect)
  
 ' indicar que se ha realizado la conexi�n
  bolConBaseDatos = True

  Screen.MousePointer = vbDefault
  Me.Hide
   
  Exit Sub

LocalErr:
  Screen.MousePointer = vbDefault
  MsgBox (R_ErrorRdoMsg)

End Sub

Private Sub txtDatabase_GotFocus()
txtDatabase.SelStart = 0
txtDatabase.SelLength = Len(txtDatabase.Text)
End Sub

Private Sub txtDSN_GotFocus()
txtDSN.SelStart = 0
txtDSN.SelLength = Len(txtDSN.Text)
End Sub

Private Sub txtPWD_GotFocus()
txtPWD.SelStart = 0
txtPWD.SelLength = Len(txtPWD.Text)
End Sub

Private Sub txtUID_GotFocus()
txtUID.SelStart = 0
txtUID.SelLength = Len(txtUID.Text)
End Sub
