VERSION 5.00
Object = "{00028C01-0000-0000-0000-000000000046}#1.0#0"; "DBGRID32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "msrdc20.ocx"
Begin VB.Form frmSesOracl
   Caption         =   "Form1"
   ClientHeight    =   5310
   ClientLeft      =   90
   ClientTop       =   375
   ClientWidth     =   5895
   LinkTopic       =   "Form1"
   ScaleHeight     =   5310
   ScaleWidth      =   5895
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSDBGrid.DBGrid DBGrid1 
      Bindings        =   "SesOracl.frx":0000
      Height          =   4575
      Left            =   0
      OleObjectBlob   =   "SesOracl.frx":0011
      TabIndex        =   0
      Top             =   120
      Width           =   5775
   End
   Begin VB.CommandButton cmdContinuar 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   495
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   4800
      Width           =   1935
   End
   Begin MSRDC.MSRDC MSRDC1 
      Height          =   375
      Left            =   4200
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   327681
      Options         =   0
      CursorDriver    =   1
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   3
      LockType        =   1
      QueryType       =   0
      Prompt          =   3
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSesOracl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdContinuar_Click()
    Screen.MousePointer = vbHourglass
    Me.Hide
End Sub

Private Sub DBGrid1_KeyDown(KeyCode As Integer, Shift As Integer)
   Dim intI As Integer
   Dim strA As String
   Dim rs As rdoResultset
   
   
   If KeyCode = vbKeyC And Shift = vbCtrlMask Then
      ' Se ha pulsado Ctrl-C para copiar una celda o un intervalo
      ' de columnas al Clipboard
      If DBGrid1.SelStartCol = -1 And _
         DBGrid1.SelEndCol = -1 Then
         ' copiar una celda
         strA = DBGrid1.SelText
      Else
         ' copiar un intervalo de columnas
         Set rs = MSRDC1.Resultset
         strA = ""
         ' cabeceras de las columnas
            For intI = DBGrid1.SelStartCol To DBGrid1.SelEndCol
               strA = strA & DBGrid1.Columns(intI).Caption
               If intI < DBGrid1.SelEndCol Then
                  strA = strA & vbTab
               End If
            Next intI
            strA = strA & vbCrLf
         ' datos
            rs.MoveFirst
            Do Until rs.EOF
               For intI = DBGrid1.SelStartCol To DBGrid1.SelEndCol
                  strA = strA & rs(intI)
                  If intI < DBGrid1.SelEndCol Then
                     strA = strA & vbTab
                  End If
               Next intI
               strA = strA & vbCrLf
               rs.MoveNext
            Loop
      End If
      Clipboard.SetText strA
   End If
End Sub

Private Sub Form_Load()
  ' permitir fijar columnas en pantalla
  DBGrid1.Splits(0).AllowSizing = True
  cmdContinuar.ToolTipText = "Salir de la ventana"
  cmdContinuar.Picture = frmMenu.imlIcons.ListImages("salir").Picture
End Sub

Private Sub Form_Resize()
    DBGrid1.Width = Me.ScaleWidth - (DBGrid1.Left * 2)
    DBGrid1.Height = Me.ScaleHeight - _
        (DBGrid1.Top * 3) - cmdContinuar.Height
    cmdContinuar.Top = DBGrid1.Height + _
        (DBGrid1.Top * 2)
End Sub



