VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmMenu 
   BackColor       =   &H8000000C&
   Caption         =   "Form1"
   ClientHeight    =   2565
   ClientLeft      =   195
   ClientTop       =   765
   ClientWidth     =   3735
   Icon            =   "Menu.frx":0000
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   2565
   ScaleWidth      =   3735
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolBar 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "imlIcons"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   4
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "mnuConexion"
            Object.ToolTipText     =   "Objetos accesibles desde de un usuario Oracle"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "mnuSqlPrepa"
            Object.ToolTipText     =   "Ejecución de Sentencias Sql Preparadas"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "mnuSalir"
            Object.ToolTipText     =   "Salir de la Aplicación"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ImageList imlIcons 
      Left            =   600
      Top             =   1560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   3
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Menu.frx":030A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Menu.frx":0624
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "Menu.frx":093E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuArchivo 
      Caption         =   "&Archivo"
      Begin VB.Menu mnuSqlPrepa 
         Caption         =   "&Sentencias SQL Preparadas..."
      End
      Begin VB.Menu mnuConexion 
         Caption         =   "&Objetos Usuario Oracle..."
      End
      Begin VB.Menu mnuListados 
         Caption         =   "&Listados..."
      End
      Begin VB.Menu mnuSalir 
         Caption         =   "&Salir"
      End
   End
End
Attribute VB_Name = "frmMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdExit_Click()
    '
    ' end pr ogram
    '
    Unload Me
    '
End Sub

Private Sub mnuConexion_Click()

   Dim frmL As New frmLogon

   ' inicializar valores de conexion a la base de datos
   strDsnConnect = "ORACLE73"
   strUidConnect = "AG"
   strPwdConnect = ""
   strSrvConnect = "desa.cun"
   ' recuperar los valores de la ultima conexion
     Call R_RecuperarConexion("CONEXIO1")
   
   frmL.txtDSN.Text = strDsnConnect
   frmL.txtUID.Text = strUidConnect
   frmL.txtPWD.Text = strPwdConnect
   frmL.txtDatabase.Text = strSrvConnect
   
   frmL.Show vbModal
   
   If frmL.bolConBaseDatos = True Then
      
      strDsnConnect = frmL.txtDSN.Text
      strUidConnect = frmL.txtUID.Text
      strPwdConnect = frmL.txtPWD.Text
      strSrvConnect = frmL.txtDatabase.Text
      
      If frmL.chkMemorizarConexion.Value = 1 Then
         ' almacenar la conexion en un fichero de texto
         Call R_GuardarConexion("CONEXIO1")
      End If
      
      ' mostrar la relacion de objetos del usuario
      ' Oracle con el que nos hemos conectado
      frmRelTbls.Show vbModal
      
      conBaseDatos.Close
   End If
  
   Unload frmL
   Set frmL = Nothing

'  Debug.Print conBaseDatos.Connect

End Sub

Private Sub mnuListados_Click()

   Dim frmL As New frmLogon

   ' inicializar valores de conexion a la base de datos
   strDsnConnect = "ORACLE73"
   strUidConnect = "SYS"
   strPwdConnect = ""
   strSrvConnect = "desa.cun"
   ' recuperar los valores de la ultima conexion
     Call R_RecuperarConexion("CONEXIO3")
   
   frmL.txtDSN.Text = strDsnConnect
   frmL.txtUID.Text = strUidConnect
   frmL.txtPWD.Text = strPwdConnect
   frmL.txtDatabase.Text = strSrvConnect
   
   frmL.Show vbModal
   
   If frmL.bolConBaseDatos = True Then
      
      strDsnConnect = frmL.txtDSN.Text
      strUidConnect = frmL.txtUID.Text
      strPwdConnect = frmL.txtPWD.Text
      strSrvConnect = frmL.txtDatabase.Text
      
      If frmL.chkMemorizarConexion.Value = 1 Then
         ' almacenar la conexion en un fichero de texto
         Call R_GuardarConexion("CONEXIO3")
      End If
      
      ' mostrar listados preparados para que puedan
      ' ser ejecutadas
      frmListados.Show vbModal
      
      conBaseDatos.Close
   End If
  
   Unload frmL
   Set frmL = Nothing


End Sub

Private Sub mnuSalir_Click()
    cmdExit_Click ' fin de la aplicacion
End Sub

Private Sub mnuSqlPrepa_Click()

   Dim frmL As New frmLogon

   ' inicializar valores de conexion a la base de datos
   strDsnConnect = "ORACLE73"
   strUidConnect = "SYSTEM"
   strPwdConnect = ""
   strSrvConnect = "desa.cun"
   ' recuperar los valores de la ultima conexion
     Call R_RecuperarConexion("CONEXIO2")
   
   frmL.txtDSN.Text = strDsnConnect
   frmL.txtUID.Text = strUidConnect
   frmL.txtPWD.Text = strPwdConnect
   frmL.txtDatabase.Text = strSrvConnect
   
   frmL.Show vbModal
   
   If frmL.bolConBaseDatos = True Then
      
      strDsnConnect = frmL.txtDSN.Text
      strUidConnect = frmL.txtUID.Text
      strPwdConnect = frmL.txtPWD.Text
      strSrvConnect = frmL.txtDatabase.Text
      
      If frmL.chkMemorizarConexion.Value = 1 Then
         ' almacenar la conexion en un fichero de texto
         Call R_GuardarConexion("CONEXIO2")
      End If
      
      ' mostrar sentencias SQL preparadas para que puedan
      ' ser ejecutadas
      frmSqlPrepa.Show vbModal
      
      conBaseDatos.Close
   End If
  
   Unload frmL
   Set frmL = Nothing

End Sub

Private Sub tlbToolBar_ButtonClick(ByVal Button As ComctlLib.Button)
    Select Case Button.Key
        Case "mnuConexion"
            mnuConexion_Click
        Case "mnuSqlPrepa"
            mnuSqlPrepa_Click
        Case "mnuSalir"
            mnuSalir_Click
    End Select

End Sub
