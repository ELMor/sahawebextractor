VERSION 5.00
Object = "{00028C01-0000-0000-0000-000000000046}#1.0#0"; "DBGRID32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "msrdc20.ocx"
Begin VB.Form frmSesActi1
   Caption         =   "Form1"
   ClientHeight    =   5310
   ClientLeft      =   90
   ClientTop       =   375
   ClientWidth     =   5895
   LinkTopic       =   "Form1"
   ScaleHeight     =   8535
   ScaleWidth      =   11820
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdRefrescar 
      Caption         =   "Refrescar"
      Height          =   495
      Left            =   5400
      TabIndex        =   5
      Top             =   8040
      Width           =   1575
   End
   Begin VB.CommandButton cmdKill 
      Caption         =   "Kill"
      Height          =   495
      Left            =   3720
      TabIndex        =   4
      Top             =   8040
      Width           =   1575
   End
   Begin VB.CommandButton cmdTraceOff 
      Caption         =   "Trace OFF"
      Height          =   495
      Left            =   2040
      TabIndex        =   3
      Top             =   8040
      Width           =   1575
   End
   Begin VB.CommandButton cmdTraceOn 
      Caption         =   "Trace On"
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   8040
      Width           =   1695
   End
   Begin MSDBGrid.DBGrid DBGrid1 
      Bindings        =   "SesActi1.frx":0000
      Height          =   7695
      Left            =   0
      OleObjectBlob   =   "SesActi1.frx":0011
      TabIndex        =   0
      Top             =   120
      Width           =   11775
   End
   Begin VB.CommandButton cmdContinuar 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   495
      Left            =   9840
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   8040
      Width           =   1935
   End
   Begin MSRDC.MSRDC MSRDC1 
      Height          =   375
      Left            =   8040
      Top             =   8040
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   327681
      Options         =   0
      CursorDriver    =   1
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   3
      LockType        =   1
      QueryType       =   0
      Prompt          =   3
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSesActi1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' mostrar las sesiones de una instancia arrancada
' de oracle con posibilidad:
'   - tirar sesiones
'   - activar el trace de una sesion

Option Explicit

Private Sub cmdContinuar_Click()
    Screen.MousePointer = vbHourglass
    Me.Hide
End Sub

Private Sub cmdKill_Click()
  ' terminar una sesi�n oracle
  
  Dim strA As String
  Dim qd As rdoQuery
  Dim rs As rdoResultset
  
  strA = "Desea terminar la sesi�n:" & vbCrLf & _
         "SID: " & MSRDC1.Resultset("SID") & vbCrLf & _
         "SERIAL: " & MSRDC1.Resultset("SERIAL") & vbCrLf & _
         "USER: " & MSRDC1.Resultset("USER_") & vbCrLf
  If MsgBox(strA, vbYesNo + vbQuestion + vbDefaultButton2, _
            "Terminar una Sesi�n ?") = vbYes Then
     Set qd = conBaseDatos.CreateQuery("", " ")
     Screen.MousePointer = vbHourglass
     qd.SQL = "alter system kill session '" & _
              MSRDC1.Resultset("SID") & "," & _
              MSRDC1.Resultset("SERIAL") & "'"
     On Error Resume Next
     qd.Execute rdExecDirect
     Screen.MousePointer = vbDefault
     If Err.Number <> 0 Then
        MsgBox (R_ErrorRdoMsg)
        On Error GoTo 0
     Else
        On Error GoTo 0
        MsgBox ("Sesi�n terminada")
        ' refrescar la pantalla
           Screen.MousePointer = vbHourglass
           MSRDC1.Resultset.Requery
           DBGrid1.Refresh
           Screen.MousePointer = vbDefault
     End If
     qd.Close
  End If
End Sub

Private Sub cmdRefrescar_Click()
     Screen.MousePointer = vbHourglass
     MSRDC1.Resultset.Requery
     DBGrid1.Refresh
     Screen.MousePointer = vbDefault
End Sub

Private Sub cmdTraceOff_Click()
  ' desactivar el trace de una sesi�n oracle
  
  Dim strA As String
  Dim qd As rdoQuery
  Dim rs As rdoResultset
  
  strA = "Desea desactivar el Trace de la sesi�n:" & vbCrLf & _
         "SID: " & MSRDC1.Resultset("SID") & vbCrLf & _
         "SERIAL: " & MSRDC1.Resultset("SERIAL") & vbCrLf & _
         "USER: " & MSRDC1.Resultset("USER_") & vbCrLf
  If MsgBox(strA, vbYesNo + vbQuestion + vbDefaultButton2, _
            "Desactivar el trace de una Sesi�n ?") = vbYes Then
     Set qd = conBaseDatos.CreateQuery("", " ")
     Screen.MousePointer = vbHourglass
     qd.SQL = "{ call GC_TRACE(? , ? , ? )}"
     qd(0).Value = MSRDC1.Resultset("SID")
     qd(1).Value = MSRDC1.Resultset("SERIAL")
     qd(2).Value = "FALSE"
     On Error Resume Next
     qd.Execute
     Screen.MousePointer = vbDefault
     If Err.Number <> 0 Then
        MsgBox (R_ErrorRdoMsg)
        On Error GoTo 0
     Else
        On Error GoTo 0
        MsgBox ("Trace desactivado")
     End If
     qd.Close
  End If
End Sub

Private Sub cmdTraceOn_Click()
  ' activar el trace de una sesi�n oracle
  
  Dim strA As String
  Dim qd As rdoQuery
  Dim rs As rdoResultset
  
  strA = "Desea activar el Trace de la sesi�n:" & vbCrLf & _
         "SID: " & MSRDC1.Resultset("SID") & vbCrLf & _
         "SERIAL: " & MSRDC1.Resultset("SERIAL") & vbCrLf & _
         "USER: " & MSRDC1.Resultset("USER_") & vbCrLf
  If MsgBox(strA, vbYesNo + vbQuestion + vbDefaultButton2, _
            "Activar el trace de una Sesi�n ?") = vbYes Then
     Set qd = conBaseDatos.CreateQuery("", " ")
     Screen.MousePointer = vbHourglass
     qd.SQL = "{ call GC_TRACE(? , ? , ? )}"
     qd(0).Value = MSRDC1.Resultset("SID")
     qd(1).Value = MSRDC1.Resultset("SERIAL")
     qd(2).Value = "TRUE"
     On Error Resume Next
     qd.Execute
     Screen.MousePointer = vbDefault
     If Err.Number <> 0 Then
        MsgBox (R_ErrorRdoMsg)
        On Error GoTo 0
     Else
        On Error GoTo 0
        MsgBox ("Trace activado")
     End If
     qd.Close
  End If

End Sub

Private Sub Form_Load()
  ' permitir fijar columnas en pantalla
  DBGrid1.Splits(0).AllowSizing = True
  cmdContinuar.ToolTipText = "Salir de la ventana"
  cmdContinuar.Picture = frmMenu.imlIcons.ListImages("salir").Picture
  cmdKill.ToolTipText = "Terminar la sesi�n seleccionada"
  cmdTraceOn.ToolTipText = "Activar el Trace de la sesi�n seleccionada"
  cmdTraceOff.ToolTipText = "Desactivar el Trace de la sesi�n seleccionada"
  cmdRefrescar.ToolTipText = "Refrescar la pantalla"
End Sub
