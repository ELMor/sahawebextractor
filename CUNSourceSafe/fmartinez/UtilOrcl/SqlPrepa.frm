VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmSqlPrepa 
   Caption         =   "Form1"
   ClientHeight    =   3405
   ClientLeft      =   45
   ClientTop       =   270
   ClientWidth     =   3750
   LinkTopic       =   "Form1"
   ScaleHeight     =   3405
   ScaleWidth      =   3750
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2280
      TabIndex        =   1
      Top             =   3000
      Width           =   1335
   End
   Begin ComctlLib.TreeView trvUsuario 
      Height          =   2775
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   4895
      _Version        =   327682
      LabelEdit       =   1
      Style           =   7
      Appearance      =   1
   End
End
Attribute VB_Name = "frmSqlPrepa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'*****************************************************************
'* Muestra un TreeView con una relacion de sentencias SQL preparadas
'* para ser ejecutadas
'* - el fichero SQL\INDICE.TXT contiene la extructura del arbol a
'*   ser mostrada y los nombres de los ficheros que contienen las
'*   sentencias SQL
'*****************************************************************
Option Explicit

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  
  ' cargar al TREEVIEW la estructura contenida en
  ' el fichero SQL\INDICE.TXT
  
  Dim nodX As Node
  Dim nodY As Node
  Dim strCurDir As String
  Dim strFichero As String
  Dim intFileNum As Integer
  Dim strClavePadre As String
  Dim strClave As String
  Dim strTexto As String

  ' identificación de la base de datos conectada
  Me.Caption = "Base de datos conectada: " & strSrvConnect & _
             "  Usuario: " & strUidConnect

  ' primer nivel
  Set nodX = trvUsuario.Nodes.Add(, , "M1", _
                             "Sentencias SQL Preparadas")
  
  strCurDir = CurDir
  strFichero = strCurDir & "\SQL\INDICE.TXT"
  If Dir(strFichero) <> "" Then ' comprobar si existe del fichero
     intFileNum = FreeFile()
     Open strFichero For Input As intFileNum
     strClavePadre = ""
     strClave = ""
     strTexto = ""
     Do
        Input #intFileNum, strClavePadre, strClave, strTexto
        If Mid(strClavePadre, 1, 1) = "M" Then
           Set nodY = trvUsuario.Nodes.Add(strClavePadre, tvwChild, _
                        strClave, strTexto)
        End If
     Loop Until (EOF(intFileNum) = True)
     Close intFileNum
  End If


nodX.Expanded = True

End Sub


Private Sub Form_Resize()
    trvUsuario.Width = Me.ScaleWidth - (trvUsuario.Left * 2)
    trvUsuario.Height = Me.ScaleHeight - _
        (trvUsuario.Top * 3) - cmdCancelar.Height
    cmdCancelar.Top = trvUsuario.Height + _
        (trvUsuario.Top * 2)
End Sub
Private Sub trvUsuario_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then  ' enter
     trvUsuario_DblClick
  End If
End Sub

Private Sub trvUsuario_DblClick()
  Dim Node As Node
  Dim strFichero As String
  Dim strTexto As String
  Dim strModo As String ' modo de ejecucion de la sentencia
    
  Set Node = trvUsuario.SelectedItem ' nodo seleccionado
  If Mid(Node.Key, 1, 1) = "E" Or _
     Mid(Node.Key, 1, 1) = "G" Then
     ' el nodo identifica un fichero que contiene una sentencia SQL
     ' ejecutable
     strModo = Mid(Node.Key, 1, 1)
     strFichero = Mid(Node.Key, 2, 8)
     strTexto = Node.Text
     EjecutarSentencia strFichero, strTexto, strModo
  End If
    
End Sub
Private Sub EjecutarSentencia(Fichero As String, Mensaje As String, Modo As String)
  ' ejecutar la sentencia Sql contenida en el fichero indicado
  Dim strA As String
  Dim strB As String
  Dim strCurDir As String
  Dim strFichero As String
  Dim intFileNum As Integer
  
'  On Error GoTo LocalErr
  
  strCurDir = CurDir
  strFichero = strCurDir & "\SQL\" & Fichero & ".SQL"
  If Dir(strFichero) <> "" Then ' comprobar si existe del fichero
     intFileNum = FreeFile()
     Open strFichero For Input As intFileNum
     strA = ""
     Do
        Line Input #intFileNum, strB
        strA = strA & strB & vbCrLf
     Loop Until (EOF(intFileNum) = True)
     Close intFileNum
  End If
  
  
   
   ' editar la select, ejecutarla y mostrar la salida por pantalla
   EditarEjecutarSelect strA, Mensaje & "  (" & Fichero & ".SQL)", Modo
  
  Exit Sub
LocalErr:
   Call R_ErrorRdo
End Sub

