VERSION 5.00
Begin VB.Form frmMostrar 
   Caption         =   "Form1"
   ClientHeight    =   3135
   ClientLeft      =   90
   ClientTop       =   375
   ClientWidth     =   4620
   LinkTopic       =   "Form1"
   ScaleHeight     =   3135
   ScaleWidth      =   4620
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2880
      TabIndex        =   2
      Top             =   2640
      Width           =   1695
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "&Continuar"
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   2640
      Width           =   1575
   End
   Begin VB.TextBox txtMostrar 
      BeginProperty Font 
         Name            =   "Fixedsys"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Text            =   "Mostrar.frx":0000
      Top             =   120
      Width           =   4575
   End
End
Attribute VB_Name = "frmMostrar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'********************************************************************
'* - Muestra una caja de texto que ocupa toda la pantalla
'* - Permite cerrar la pantalla seleccionando
'*   uno de dos botones: Continuar, Salir
'* - Opcionalmente, al dar a continuar se puede crear un Resultset con
'*   la Select editada en la caja de texto. Si la Select est� mal
'*   construida se saca un mensaje con el error
'*   ( tiene que hacerse aqui la validacion para que la ventana del
'*     mensaje aparezca encima de la ventana donde se ha metido
'*     la sentencia )
'********************************************************************
Option Explicit

' indica si se ha salido de la pantalla pulsando el
' boton de continuar o el de cancelar
Public bolContinuar As Boolean

' indica si hay que crear un Resultset con la sentencia SQL que
' se ha preparado en "txtMostrar"
Public bolCrearResultset As Boolean

' indica si hay que ejecutar la sentencia SQL que
' se ha preparado en "txtMostrar"
Public bolEjecutarSQL As Boolean

Public qd As rdoQuery
Public rs As rdoResultset

Private Sub cmdCancelar_Click()
    bolContinuar = False ' se ha salido pulsando el boton: Salir
    Me.Hide
End Sub

Private Sub cmdContinuar_Click()
  ' validar campos
    
    If bolCrearResultset = True Then
       ' la SELECT puede ser incorrecta
       Screen.MousePointer = vbHourglass
       qd.SQL = txtMostrar.Text
       qd.MaxRows = 500 ' numero maximo de filas a mostrar
       On Error Resume Next
       Set rs = qd.OpenResultset(rdOpenKeyset, _
                                 rdConcurReadOnly)
       Screen.MousePointer = vbDefault
       If Err.Number <> 0 Then
          MsgBox (R_ErrorRdoMsg)
          On Error GoTo 0
          Exit Sub
       End If
       On Error GoTo 0
    End If
    
    If bolEjecutarSQL = True Then
       ' la SQL puede ser incorrecta
       Screen.MousePointer = vbHourglass
       qd.SQL = txtMostrar.Text
       On Error Resume Next
       qd.Execute rdExecDirect
       Screen.MousePointer = vbDefault
       If Err.Number <> 0 Then
          MsgBox (R_ErrorRdoMsg)
          On Error GoTo 0
          Exit Sub
       End If
       On Error GoTo 0
       MsgBox ("Sentencia ejecutada")
    End If
  
  ' cerrar la ventana
    bolContinuar = True ' se ha salido pulsando el boton: Continuar
    Me.Hide
End Sub

Private Sub Form_Initialize()
    bolContinuar = False
    bolCrearResultset = False
    bolEjecutarSQL = False
End Sub

Private Sub Form_Resize()
    txtMostrar.Width = Me.ScaleWidth - (txtMostrar.Left * 2)
    txtMostrar.Height = Me.ScaleHeight - _
        (txtMostrar.Top * 3) - cmdContinuar.Height
    cmdContinuar.Top = txtMostrar.Height + _
        (txtMostrar.Top * 2)
    cmdCancelar.Top = txtMostrar.Height + _
        (txtMostrar.Top * 2)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    bolContinuar = False
End Sub
