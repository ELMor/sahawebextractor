VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "EdiTabla"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' local enumerations

' button alignments
Enum bbAlign
    bbTop = 0
    bbBottom = 1
    bbLeft = 2
    bbRight = 3
End Enum

' Private intBBAlign As Integer ' button aligment
Private intBBAlign As bbAlign ' button aligment

' opcion seleccionada y procesandose
    Private strOptEditar As String
       '  A   - a�adir
       '  M   - modificar
       '  B   - buscar
       '  R   - borrar
       ' " "  - resto de opciones

' tipo de Resulset abierto
    Private intTipoResulset As Integer
       ' 0    - Resultset no creado
       ' 1    - Resultset para soportar el a�adido de registros
       ' 2    - Resultset para el resto de opciones

' indicar si el Resultset esta modificado y los cambios no han
' sido aplicados aun al Grid
    Private bolRSetModificado As Boolean

' indicar si se han borrado registros.
' si se han borrado registros, despues de pasar por el principio
' del resultset, (movetop) cambia el bookmark de los registros
    Private bolRecBorrados As Boolean

' Sql utilizada para crear el Resultset para a�adir registros
    Private strSqlAdd As String

' Sql utilizada para crear el Resultset para ir al
' primer registro de la tabla
    Private strSqlTop As String

' variables utilizadas por el procedimiento Validar_Campos
' para indicar los mensajes de error detectados en la validacion
' de los campos
    Private strMsg_Severos As String ' mensajes severos
    Private strMsg_Aviso   As String ' mensajes de aviso

Private rs As rdoResultset
Private qd As rdoQuery

Public Property Get SqlAdd() As String
    SqlAdd = strSqlAdd
End Property

Public Property Let SqlAdd(ByVal strA As String)
    strSqlAdd = strA
End Property

Public Property Get SqlTop() As String
    SqlTop = strSqlTop
End Property

Public Property Let SqlTop(ByVal strA As String)
    strSqlTop = strA
End Property
Public Property Get rsObject() As rdoResultset
    Set rsObject = rs
End Property

Public Property Let rsObject(ByVal vNewValue As Variant)
    ' na
End Property

Public Property Get BtnBarAlign() As bbAlign
    BtnBarAlign = intBBAlign
End Property

Public Property Let BtnBarAlign(ByVal vNewValue As bbAlign)
    intBBAlign = vNewValue
End Property

Public Property Get Msg_Severos() As Variant
    Msg_Severos = strMsg_Severos
End Property

Public Property Let Msg_Severos(ByVal vNewValue As Variant)
    strMsg_Severos = vNewValue
End Property
Public Property Get Msg_Aviso() As Variant
    Msg_Aviso = strMsg_Aviso
End Property

Public Property Let Msg_Aviso(ByVal vNewValue As Variant)
    strMsg_Aviso = vNewValue
End Property


Private Sub Class_Initialize()
    '
    ' set inital values
    '
    intBBAlign = bbTop
    '
    ' opcion seleccionada y procesandose
      strOptEditar = " "

    ' tipo de Resulset abierto
      intTipoResulset = 0  ' Resultset no creado

    bolRSetModificado = False ' resultset no modificado
    bolRecBorrados = False ' indicador de registros borrados

End Sub

Private Sub Class_Terminate()
    '
    ' close out class
    '
   On Error Resume Next
    '
    rs.Close
    Set rs = Nothing

End Sub

Public Function RSInit(frmTemp As Object)
    '
    ' clear all input controls on the form
    '
    frmTemp.CtlsInici strOptEditar
    Exit Function
    
End Function
Public Function RSRead(frmTemp As Object)
    '
    ' move data from recordset to form
    '
    If rs.BOF Or _
       rs.EOF Then
       ' no hay registros en el resulset
       Exit Function
    End If
    
    frmTemp.PintarPantalla rs
End Function
Private Function RSWrite(frmTemp As Object)
    '
    ' move values in controls to data set
    '
    frmTemp.GrabarRegistro rs
End Function
Public Function RSEnable(frmTemp As Object, Toggle As Boolean)
    '
    ' toggle the controls on/off
    '
    frmTemp.CtlsEnaDisa Toggle
End Function
Private Function RSDelete()
    '
    ' delete current record
    '
    Dim lngResult As Long
    '
    rs.Delete
    '
    RSDelete = 0
    Exit Function
    '
LocalErr:
    RSDelete = Err.Number
    '
End Function
Public Sub RSClose()
    '
    ' close down object
    '
    Class_Terminate
    '
End Sub


Private Function RSBack()
    '
    ' move back one record
    '
    If rs.BOF = True Then
        rs.MoveFirst
    Else
        rs.MovePrevious
        If rs.BOF Then
            rs.MoveFirst
        End If
    End If
    '
    RSBack = 0
    Exit Function
    '
LocalErr:
    RSBack = Err.Number
    '
End Function


Private Function RSNext()
    '
    ' move to next record
    '
    If rs.EOF = True Then
        rs.MoveLast
    Else
        rs.MoveNext
        If rs.EOF Then
            rs.MoveLast
        End If
    End If
    '
    RSNext = 0
    Exit Function
    '
LocalErr:
    RSNext = Err.Number
    '
End Function


Public Function BBInit(frmTemp As Object)
    '
    ' initialize a button bar on the form
    '
    Dim intBtnWidth As Integer
    Dim intBtnTop As Integer
    Dim intBtnleft As Integer
    Dim intBtnHeight As Integer
    Dim intLoop As Integer
    Dim varCap As Variant
    '
    varCap = Array("&A�adir", "&Modificar", "Bo&rrar", "&Buscar", _
                   "&Primero", "Si&guiente", "A&nterior", "&Salir")
'   varCap = Array("&Add", "&Edit", "&Del", "&Find", _
                   "&Top", "&Next", "&Back", "&Last")
    '
    ' compute btn locations
    intBtnWidth = 660
    intBtnHeight = 300
    '
    Select Case intBBAlign
        Case bbTop
            intBtnTop = 60
            intBtnWidth = (frmTemp.ScaleWidth - 60) / 8
            If intBtnWidth < 660 Then intBtnWidth = 660
            intBtnHeight = 300
        Case bbBottom
            intBtnTop = frmTemp.ScaleHeight - 360
            intBtnWidth = (frmTemp.ScaleWidth - 60) / 8
            If intBtnWidth < 660 Then intBtnWidth = 660
            intBtnHeight = 300
        Case bbLeft
            intBtnWidth = 660
            intBtnleft = 60
            intBtnHeight = (frmTemp.ScaleHeight - 60) / 8
            If intBtnHeight < 300 Then intBtnHeight = 300
        Case bbRight
            intBtnWidth = 660
            intBtnleft = frmTemp.ScaleWidth - 720
            intBtnHeight = (frmTemp.ScaleHeight - 60) / 8
            If intBtnHeight < 300 Then intBtnHeight = 300
    End Select
    '
    ' now place buttons on the form
    For intLoop = 0 To 7
        If intBBAlign = bbTop Or intBBAlign = bbBottom Then
            intBtnleft = intLoop * intBtnWidth
        Else
            intBtnTop = (intLoop * intBtnHeight) + 60
        End If
        '
        On Error Resume Next
        With frmTemp
            If intLoop <> 0 Then
                Load .cmdBtn(intLoop)
            End If
            .cmdBtn(intLoop).Width = intBtnWidth
            .cmdBtn(intLoop).Left = intBtnleft
            .cmdBtn(intLoop).Top = intBtnTop
            .cmdBtn(intLoop).Height = intBtnHeight
            .cmdBtn(intLoop).Caption = varCap(intLoop)
            .cmdBtn(intLoop).Visible = True
            .cmdBtn(intLoop).TabStop = False
        End With
    Next
    '
    BBInit = 0
    Exit Function
    '
LocalErr:
    BBInit = Err.Number
    '
End Function
Public Function BBEnable(frmTemp As Object, strList As String)
    '
    ' enable buttons
    '
'   On Error GoTo LocalErr
    '
    Dim intLoop As Integer
    '
    strList = Trim(strList)
    '
    For intLoop = 1 To Len(strList)
        If Mid(strList, intLoop, 1) = "1" Then
            frmTemp.cmdBtn(intLoop - 1).Enabled = True
        Else
            frmTemp.cmdBtn(intLoop - 1).Enabled = False
        End If
    Next
    '
    BBEnable = 0
    Exit Function
    '
LocalErr:
    BBEnable = Err.Number

    '
End Function
Public Function BBProcess(frmTemp As Object, intBtn As Integer)
    '
    ' handle all button clicks
    '
'    On Error GoTo LocalErr

    Dim lngResult As Long
    Dim rsBuscar As rdoResultset
    '
    
' opcion seleccionada y procesandose
Select Case strOptEditar
  Case "A" ' a�adir
    Select Case intBtn
        Case 0 ' save
                lngResult = RSWrite(frmTemp)
                If lngResult = 0 Then
                    rs.Update
                End If
                If lngResult = 0 Then
                    rs.Bookmark = rs.LastModified
                    lngResult = RSInit(frmTemp)
                End If
                If lngResult = 0 Then
                    lngResult = RSRead(frmTemp)
                End If
                If lngResult = 0 Then
                    lngResult = RSEnable(frmTemp, False)
                End If
                If lngResult = 0 Then
                    frmTemp.cmdBtn(0).Caption = "&A�adir"
                    frmTemp.cmdBtn(1).Caption = "&Modificar"
                    BBEnable frmTemp, "11111111"
                    strOptEditar = " " ' resto de opciones
                    bolRSetModificado = True ' resultset modificado
                    frmTemp.SSTab1.TabEnabled(1) = True ' Tabla
                End If
        Case 1 ' cancel
                strOptEditar = " " ' resto de opciones
                rs.CancelUpdate
                frmTemp.cmdBtn(0).Caption = "&A�adir"
                frmTemp.cmdBtn(1).Caption = "&Modificar"
                BBEnable frmTemp, "11111111"
                '
                lngResult = RSInit(frmTemp)
                If lngResult = 0 Then
                    lngResult = RSRead(frmTemp)
                End If
                If lngResult = 0 Then
                    lngResult = RSEnable(frmTemp, False)
                End If
                frmTemp.SSTab1.TabEnabled(1) = True ' Tabla
    End Select
  Case "M" ' modificar
    Select Case intBtn
        Case 0 ' save
                ' validaci�n de los campos
                    strMsg_Severos = " " ' mensajes severos
                    strMsg_Aviso = " "   ' mensajes de aviso
                    frmTemp.Validar_Campos
                If strMsg_Severos <> " " Then
                     lngResult = MsgBox(strMsg_Severos, vbOKOnly)
                   Else
                     lngResult = vbYes
                     If strMsg_Aviso <> " " Then
                        lngResult = MsgBox(strMsg_Aviso, _
                               vbYesNo + vbQuestion, "Actualizar ?")
                     End If
                     If lngResult = vbYes Then
                        rs.Edit
                        lngResult = RSWrite(frmTemp)
                        If lngResult = 0 Then
                           rs.Update
                        End If
                        If lngResult = 0 Then
                            lngResult = RSEnable(frmTemp, False)
                        End If
                        If lngResult = 0 Then
                            frmTemp.cmdBtn(0).Caption = "&A�adir"
                            frmTemp.cmdBtn(1).Caption = "&Modificar"
                            BBEnable frmTemp, "11111111"
                            strOptEditar = " " ' resto de opciones
                            bolRSetModificado = True ' resultset modificado
                            frmTemp.SSTab1.TabEnabled(1) = True ' Tabla
                        End If
                     End If
                End If
        Case 1 ' cancel
                strOptEditar = " " ' resto de opciones
                ' rs.CancelUpdate
                frmTemp.cmdBtn(0).Caption = "&A�adir"
                frmTemp.cmdBtn(1).Caption = "&Modificar"
                BBEnable frmTemp, "11111111"
                '
                lngResult = RSInit(frmTemp)
                If lngResult = 0 Then
                    lngResult = RSRead(frmTemp)
                End If
                If lngResult = 0 Then
                    lngResult = RSEnable(frmTemp, False)
                End If
                frmTemp.SSTab1.TabEnabled(1) = True ' Tabla
    End Select
  Case " " ' resto de opciones
    Select Case intBtn
        Case 0 ' seleccionada la opcion de a�adir
                CreaResutsetAdd
                strOptEditar = "A" ' a�adir
                rs.AddNew
                frmTemp.SSTab1.Tab = 0 ' Detalle
                frmTemp.SSTab1.TabEnabled(1) = False ' Tabla
                lngResult = RSInit(frmTemp)
                lngResult = RSEnable(frmTemp, True)
                frmTemp.cmdBtn(0).Caption = "&Aceptar"
                frmTemp.cmdBtn(1).Caption = "&Cancelar"
                BBEnable frmTemp, "11000000"
                bolRSetModificado = True ' resultset modificado
        Case 1 ' seleccionada la opcion de modificar
                If rs.BOF Or _
                   rs.EOF Then
                     ' no hay registros en el resulset
                Else
                     strOptEditar = "M" ' modificar
                     If frmTemp.SSTab1.Tab = 1 Then
                        ' ficha: "Tabla"
                        rs.Bookmark = frmTemp.DBGrid1.Bookmark
                        frmTemp.SSTab1.Tab = 0 ' Detalle
                     End If
                     frmTemp.SSTab1.TabEnabled(1) = False ' Tabla
                     lngResult = RSEnable(frmTemp, True)
                     frmTemp.cmdBtn(0).Caption = "&Aceptar"
                     frmTemp.cmdBtn(1).Caption = "&Cancelar"
                     BBEnable frmTemp, "11000000"
                     lngResult = RSInit(frmTemp)
                     lngResult = RSRead(frmTemp)
                End If
        Case 2 ' delete rec
                If rs.BOF Or _
                   rs.EOF Then
                 ' no hay registros en el resulset
                Else
                 strOptEditar = "R" ' borrar
                 If frmTemp.SSTab1.Tab = 1 Then
                    ' ficha: "Tabla"
                    rs.Bookmark = frmTemp.DBGrid1.Bookmark
                    frmTemp.SSTab1.Tab = 0 ' Detalle
                    lngResult = RSInit(frmTemp)
                    lngResult = RSRead(frmTemp)
                 End If
                 lngResult = MsgBox("Desea borrar el registro actual?", _
                       vbYesNo + vbQuestion)
                 If lngResult = vbYes Then
                   Screen.MousePointer = vbHourglass
                   lngResult = RSDelete()
                   lngResult = RSEnable(frmTemp, False)
                   lngResult = RSNext()
                   lngResult = RSInit(frmTemp)
                   lngResult = RSRead(frmTemp)
                   Screen.MousePointer = vbDefault
                 End If
                 BBEnable frmTemp, "11111111"
                 bolRSetModificado = True ' resultset modificado
                 bolRecBorrados = True ' registros borrados
                 strOptEditar = " " ' resto de opciones
                End If
        Case 3 ' buscar
                strOptEditar = "B" ' buscar
                frmTemp.SSTab1.Tab = 1 ' Tabla
                If frmTemp.Buscar(rsBuscar) Then
                If Not rsBuscar.BOF Then
                   If intTipoResulset > 0 Then
                      ' habia un Resultset abierto
                      rs.Close
                      Set rs = Nothing
                      qd.Close
                      Set qd = Nothing
                   End If
                   Set qd = db.CreateQuery("", "")
                   Set rs = rsBuscar
                   ' Resultset ver cualquier registro de la tabla
                   intTipoResulset = 2
                   frmTemp.DBGrid1.Refresh
                   BBEnable frmTemp, "11111001"
                End If
                End If
                strOptEditar = " " ' resto de opciones
        Case 4 ' move to top
                CreaResutsetTop
                rs.MoveFirst
                If frmTemp.SSTab1.Tab = 0 Then
                   ' ficha: "Detalle"
                   lngResult = RSInit(frmTemp)
                   If lngResult = 0 Then
                       lngResult = RSRead(frmTemp)
                   End If
                   BBEnable frmTemp, "11111111"
                Else
                   ' ficha: "Tabla"
                   frmTemp.DBGrid1.Refresh
                   BBEnable frmTemp, "11111001"
                End If
        Case 5 ' move next
                lngResult = RSNext()
                If lngResult = 0 Then
                    lngResult = RSInit(frmTemp)
                End If
                If lngResult = 0 Then
                    lngResult = RSRead(frmTemp)
                End If
                BBEnable frmTemp, "11111111"
        Case 6 ' move previous
'               rs.MovePrevious
                lngResult = RSBack()
                If lngResult = 0 Then
                    lngResult = RSInit(frmTemp)
                End If
                If lngResult = 0 Then
                    lngResult = RSRead(frmTemp)
                End If
                BBEnable frmTemp, "11111111"
        Case 7 ' Salir
'                rs.MoveLast
'                'lngResult = RSInit(frmTemp)
'                If lngResult = 0 Then
'                    lngResult = RSRead(frmTemp)
'                End If
'                BBEnable frmTemp, "11111111"
    End Select
End Select
    
    '
    BBProcess = 0
    Exit Function
    '
LocalErr:
    BBProcess = Err.Number
    '
End Function

Public Sub UnboundReadData(Formul As Object, _
   ByVal RowBuf As MSDBGrid.RowBuffer, _
   StartLocation As Variant, _
   ByVal ReadPriorRows As Boolean)
   
   ' se llama a esta rutina cada vez que DBGrid necesita registros
   ' para mostrar en pantalla
   
   Dim intI As Integer
   Dim intJ As Integer
   Dim intRegDevueltos As Integer
   Dim varBookmark As Variant
   
   If intTipoResulset = 0 Then
      ' no hay un Resultset abierto
      RowBuf.RowCount = 0
      Exit Sub
   End If
   
   If bolRSetModificado = True Then
      ' el Resultset ha sido modificado y los cambios no estan
      ' reflejados en el Grid
      RowBuf.RowCount = 0
      Exit Sub
   End If
   
   
'  MsgBox ("Startlocation: " & StartLocation & vbCrLf & _
           "Rowcount:" & RowBuf.RowCount)
   
   ' respetar la posicion original del resultset
   varBookmark = rs.Bookmark
   
   intRegDevueltos = 0
   
   If IsNull(StartLocation) And _
      ReadPriorRows = False Then
      ' leer desde el principio hacia adelante
      rs.MoveFirst
      For intI = 0 To RowBuf.RowCount - 1
         If rs.EOF Then
            Exit For
         End If
         Formul.LeerFilaGrid RowBuf, intI, rs
         RowBuf.Bookmark(intI) = rs.Bookmark
         intRegDevueltos = intRegDevueltos + 1
         rs.MoveNext
      Next intI
   ElseIf ReadPriorRows = False Then
      ' leer a partir de un registro hacia adelante
      rs.Bookmark = StartLocation
      rs.MoveNext
      For intI = 0 To RowBuf.RowCount - 1
         If rs.EOF Then
            Exit For
         End If
         Formul.LeerFilaGrid RowBuf, intI, rs
         RowBuf.Bookmark(intI) = rs.Bookmark
         intRegDevueltos = intRegDevueltos + 1
         rs.MoveNext
      Next intI
   ElseIf IsNull(StartLocation) And _
      ReadPriorRows = True Then
      ' leer desde el final hacia atras
      
'     rs.MoveFirst
'     intJ = 1 ' indica si hay suficientes registros
'     For intI = 0 To RowBuf.RowCount - 1
'        If rs.EOF Then
'           intJ = 0
'           Exit For
'        End If
'        rs.MoveNext
'     Next intI
'     If intJ = 1 Then
'        For intI = 0 To RowBuf.RowCount - 1
'           If rs.BOF Then
'              Exit For
'           End If
'           Formul.LeerFilaGrid RowBuf, intI, rs
'           RowBuf.Bookmark(intI) = rs.Bookmark
'           intRegDevueltos = intRegDevueltos + 1
'           rs.MovePrevious
'        Next intI
'     End If
      
      
      rs.MoveLast
      For intI = 0 To RowBuf.RowCount - 1
         If rs.BOF Then
            Exit For
         End If
         Formul.LeerFilaGrid RowBuf, intI, rs
         RowBuf.Bookmark(intI) = rs.Bookmark
         intRegDevueltos = intRegDevueltos + 1
         rs.MovePrevious
      Next intI
   ElseIf ReadPriorRows = True Then
      ' leer a partir de un registro hacia atras
      rs.Bookmark = StartLocation
      rs.MovePrevious
      For intI = 0 To RowBuf.RowCount - 1
         If rs.BOF Then
            Exit For
         End If
         Formul.LeerFilaGrid RowBuf, intI, rs
         RowBuf.Bookmark(intI) = rs.Bookmark
         intRegDevueltos = intRegDevueltos + 1
         rs.MovePrevious
      Next intI
   End If
   
   ' indicar el numero de registros realmente devueltos
   If RowBuf.RowCount <> intRegDevueltos Then
      RowBuf.RowCount = intRegDevueltos
   End If
   
   ' respetar la posicion original del resultset
   rs.Bookmark = varBookmark
   
End Sub

Public Sub SelecFichaTab(Formul As Object)
   
   ' se llama a esta rutina cuando se pasa de una ficha Tab
   ' a otra
   
 If intTipoResulset > 0 And _
    strOptEditar = " " Then ' resto de opciones
   
   If rs.BOF Or _
         rs.EOF Then
      ' no hay registros en el resulset
      Exit Sub
   End If
   
   If Formul.SSTab1.Tab = 0 Then
      ' se ha pasado a la ficha: "Detalle"
      ' posicionarse en el registro que estaba activo el el GRID
      rs.Bookmark = Formul.DBGrid1.Bookmark
      RSRead Formul
      BBEnable Formul, "11111111"
   Else
      ' se ha pasado a la ficha: "Tabla"
         ' cuando se intenta ir al ultimo registro se produce
         ' un error
      If bolRSetModificado = True Then
         ' el Resultset ha sido modificado y los cambios no estan
         ' reflejados en el Grid
         bolRSetModificado = False
         Formul.DBGrid1.Refresh
         If bolRecBorrados = False Then
            ' cuando se borran registros cambia el bookmark
            ' del resto de registros
            On Error Resume Next
            Formul.DBGrid1.Bookmark = rs.Bookmark
            On Error GoTo 0
         End If
         bolRecBorrados = False
      Else
            ' cuando se inicia en modo tabla en el primer
            ' registro se produce un error en esta instruccion
         On Error Resume Next
         Formul.DBGrid1.Bookmark = rs.Bookmark
         On Error GoTo 0
      End If
      BBEnable Formul, "11111001"
   End If
 End If
End Sub
Public Sub CreaResutsetAdd()
  If intTipoResulset <> 1 Then ' Resultset a�adido registros
     If intTipoResulset > 0 Then
        ' habia un Resultset abierto
        rs.Close
        Set rs = Nothing
        qd.Close
        Set qd = Nothing
     End If
     Set qd = db.CreateQuery("", "")
     qd.SQL = strSqlAdd
     Set rs = qd.OpenResultset(rdOpenKeyset, _
                               rdConcurValues)
     ' Resultset para soportar el a�adido de registros
        intTipoResulset = 1
  End If
End Sub
Public Sub CreaResutsetTop()
  If intTipoResulset <> 2 Then ' Resultset primer registro tabla
     If intTipoResulset > 0 Then
        ' habia un Resultset abierto
        rs.Close
        Set rs = Nothing
        qd.Close
        Set qd = Nothing
     End If
     Set qd = db.CreateQuery("", "")
     qd.SQL = strSqlTop
     Set rs = qd.OpenResultset(rdOpenKeyset, _
                               rdConcurValues)
     ' Resultset para podeer ver cualquier registro de la tabla
        intTipoResulset = 2
  End If
End Sub

Public Sub Empezar(Formul As Object, Modo As String, _
                   Poblado As String)
   ' Preparar la primera pantalla que se mostrara al operador
   ' Modo:    "Detalle"       mostrar la pantalla de Detalle
   '          "Tabla"         mostrar la pantalla de Tabla
   ' Poblado: "Vacio"         no abrir ningun Resultset al empezar
   '          "Con Registros" empezar mostrando el primer
   '                          registro de la tabla
    
   If Modo = "Detalle" Then
      ' empezar mostrando la pantalla de detalle
      Formul.SSTab1.Tab = 0 ' Detalle
      If Poblado = "Vacio" Then
         ' empezar sin mostrar ningun registro
         RSInit Formul
         RSEnable Formul, False
         BtnBarAlign = bbTop
         BBInit Formul
         BBEnable Formul, "10011001"
      Else
         ' empezar mostrando el primer registro de la tabla
         CreaResutsetTop
         RSInit Formul
         RSRead Formul
         RSEnable Formul, False
         BtnBarAlign = bbTop
         BBInit Formul
         BBEnable Formul, "11111111"
      End If
   Else
      ' empezar mostrando la pantalla de tabla
      Formul.SSTab1.Tab = 1 ' Tabla
      If Poblado = "Vacio" Then
         ' empezar sin mostrar ningun registro
         RSInit Formul
         RSEnable Formul, False
         BtnBarAlign = bbTop
         BBInit Formul
         BBEnable Formul, "10011001"
      Else
         ' empezar mostrando el primer registro de la tabla
         CreaResutsetTop
'        Formul.DBGrid1.Refresh
         RSInit Formul
         RSEnable Formul, False
         BtnBarAlign = bbTop
         BBInit Formul
         BBEnable Formul, "11111001"
      End If
   End If
    

End Sub

Public Sub UnboundGetRelativeBookmark(StartLocation As Variant, _
                               ByVal OffSet As Long, _
                               NewLocation As Variant, _
                               ApproximatePosition As Long)
    
'    MsgBox (OffSet)
    
    ' Este evento es opcional.
    ' Se ha creado para tratar de solucionar un problema
    ' que tiene el Grid cuando hay un solo registro para mostrar
    
    
    ' DBGrid1 calls this routine each time it
    ' needs to position a row.

    ' StartLocation is a bookmark supplied by the grid to
    ' indicate which records the grid wants to positioning from.
    ' If StartLocation is NULL, then data is requested either
    ' from the first row or the last row.
    
    Dim lngI As Long
    Dim varBookmark As Variant
    
    ' respetar la posicion original del resultset
    varBookmark = rs.Bookmark
    
    If IsNull(StartLocation) Then
        If OffSet < 0 Then
            ' Reading data from the last row
            rs.MoveLast
            For lngI = 1 To OffSet
               If rs.BOF Then
                  NewLocation = Null
                  Exit For
               End If
               NewLocation = rs.Bookmark
               rs.MovePrevious
            Next lngI
        Else
            ' Reading data from the first row
            rs.MoveFirst
            For lngI = 1 To OffSet
               If rs.EOF Then
                  NewLocation = Null
                  Exit For
               End If
               NewLocation = rs.Bookmark
               rs.MoveNext
            Next lngI
        End If
    Else
        ' Positioning from StartLocation plus the OffSet
        If OffSet < 0 Then
            ' leer para atras
            rs.Bookmark = StartLocation
            rs.MovePrevious
            For lngI = 1 To OffSet
               If rs.BOF Then
                  NewLocation = Null
                  Exit For
               End If
               NewLocation = rs.Bookmark
               rs.MovePrevious
            Next lngI
        Else
            ' leer para adelante
            If OffSet = 0 Then
               NewLocation = StartLocation
            Else
               rs.Bookmark = StartLocation
               rs.MoveNext
               For lngI = 1 To OffSet
                  If rs.EOF Then
                     NewLocation = Null
                     Exit For
                  End If
                  NewLocation = rs.Bookmark
                  rs.MoveNext
               Next lngI
            End If
        End If
    End If
   
    ' respetar la posicion original del resultset
    rs.Bookmark = varBookmark

End Sub

