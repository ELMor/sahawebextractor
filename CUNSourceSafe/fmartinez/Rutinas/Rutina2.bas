Attribute VB_Name = "Rutina2"
Option Explicit

Function EditarEjecutarSelect(ByVal strSentencia As String, _
                         ByVal strTitulo As String, _
                         ByVal strModo As String) As Integer
  ' ejecutar una sentencia SQL
  
  ' valores de strModo:
  '   'E' la sentencia SQL no devuelve registros
  '       por lo que unicamente hay que ejecutarla
  '   'G' la sentencia SQL devuelve registros por lo que hay que mostrar
  '       el resultado en un Grid
  
  ' devuelve:
  '    0 - si se ha salido sin ejecutar la sentencia
  '    1 - si se ha ejecutado la sentencia
  
  ' pasos:  - recibe como parametro una sentencia SQL
  '         - dar opci�n a modificar la sentencia SQL
  '         - ejecutar la SQL y si devuelve registros, mostrarlos en un GRID
   Dim frmM As New frmMostrar  ' mostrar caja de texto
   Dim frmMG As New frmMostGrid ' mostrar Grid
   Dim colX As Column
  
'  On Error GoTo LocalErr
  
  EditarEjecutarSelect = 0

  If strModo <> "E" And _
     strModo <> "G" Then
     MsgBox ("El procedimiento: EditarEjecutarSelect" & vbCrLf & _
            "ha recibido un parametro erroneo" & vbCrLf & _
            "strModo: " & strModo)
     Exit Function
  End If
  
  frmM.Caption = strTitulo
  frmM.txtMostrar.Text = strSentencia
  Set frmM.qd = conBaseDatos.CreateQuery("", " ")
  
  If strModo = "E" Then
     '   'E' la sentencia SQL no devuelve registros
     '       por lo que unicamente hay que ejecutarla
     
     frmM.bolEjecutarSQL = True ' pedir ejecutar la SQL
     ' dar opci�n a modificar la SQL y ejecutarla
     Screen.MousePointer = vbDefault
     frmM.Show vbModal
     If frmM.bolContinuar = True Then
        ' indicar que si se ha ejecutado la sentencia
        EditarEjecutarSelect = 1
     End If
  
  Else
     '   'G' la sentencia SQL devuelve registros por lo que hay que mostrar
     '       el resultado en un Grid

     frmM.bolCrearResultset = True ' pedir crear el Resultset
     ' permitir ejecutar la Select varias veces
     Do While True
       ' dar opci�n a modificar la SELECT y crear Resultset
       Screen.MousePointer = vbDefault
       frmM.Show vbModal
     
       If frmM.bolContinuar = False Then
          Exit Do
       End If
  
       ' mostrar el resultado de la SELECT
       Set frmMG.MSRDC1.Resultset = frmM.rs
       frmMG.Caption = strTitulo & "       (m�ximo 2000 registros)"
       ' permitir ver cada columna en varias lineas
       For Each colX In frmMG.DBGrid1.Columns
          colX.WrapText = True
       Next colX
       frmMG.Show vbModal
       frmM.rs.Close
     Loop
  
  End If
  frmM.qd.Close
  
  Unload frmM
  Set frmM = Nothing
  Unload frmMG
  Set frmMG = Nothing
  
  Exit Function
LocalErr:
   Call R_ErrorRdo
End Function



