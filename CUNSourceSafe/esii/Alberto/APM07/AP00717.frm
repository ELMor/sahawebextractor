VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_Organo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Organos/L�quido"
   ClientHeight    =   7800
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9735
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7800
   ScaleWidth      =   9735
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraTiOrg 
      Caption         =   "Tipo de muestra"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1755
      Left            =   75
      TabIndex        =   19
      Top             =   525
      Width           =   9465
      Begin TabDlg.SSTab tabTiOrg 
         Height          =   1380
         HelpContextID   =   90001
         Left            =   150
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   300
         Width           =   9045
         _ExtentX        =   15954
         _ExtentY        =   2434
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AP00717.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txtTiOrg(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtTiOrg(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AP00717.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssTiOrg(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtTiOrg 
            BackColor       =   &H00FFFF00&
            DataField       =   "PR49DesTipMuestr"
            Height          =   285
            Index           =   1
            Left            =   1725
            MaxLength       =   50
            TabIndex        =   0
            Tag             =   "Tipo de muestra"
            Top             =   600
            Width           =   5775
         End
         Begin VB.TextBox txtTiOrg 
            BackColor       =   &H00FFFF00&
            DataField       =   "PR49CodTipMuestr"
            Height          =   285
            Index           =   0
            Left            =   600
            MaxLength       =   20
            TabIndex        =   21
            Tag             =   "Tipo de muestra|C�digo del Tipo de muestra"
            Top             =   225
            Visible         =   0   'False
            Width           =   600
         End
         Begin SSDataWidgets_B.SSDBGrid grdssTiOrg 
            Height          =   1260
            Index           =   0
            Left            =   -74850
            TabIndex        =   23
            Top             =   75
            Width           =   8445
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   14896
            _ExtentY        =   2222
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo de muestra:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   225
            TabIndex        =   22
            Top             =   600
            Width           =   1500
         End
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   420
   End
   Begin VB.Frame fraDetalle 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2835
      Left            =   45
      TabIndex        =   10
      Top             =   4650
      Width           =   9510
      Begin TabDlg.SSTab tabDetalle 
         Height          =   2430
         Left            =   150
         TabIndex        =   11
         Top             =   300
         Width           =   9195
         _ExtentX        =   16219
         _ExtentY        =   4286
         _Version        =   327681
         Style           =   1
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Perfiles de t�cnicas"
         TabPicture(0)   =   "AP00717.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraPerfilTec"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Anal�tica"
         TabPicture(1)   =   "AP00717.frx":0054
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraAnalitica"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Perfiles de diagn�sticos"
         TabPicture(2)   =   "AP00717.frx":0070
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "fraPerfilDiag"
         Tab(2).ControlCount=   1
         Begin VB.Frame fraPerfilDiag 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   1980
            Left            =   -74775
            TabIndex        =   18
            Top             =   375
            Width           =   8820
            Begin SSDataWidgets_B.SSDBGrid grdssPerfilDiag 
               Height          =   1980
               Index           =   0
               Left            =   75
               TabIndex        =   5
               Top             =   0
               Width           =   8520
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15028
               _ExtentY        =   3492
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraAnalitica 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   1980
            Left            =   -74850
            TabIndex        =   17
            Top             =   375
            Width           =   8820
            Begin SSDataWidgets_B.SSDBGrid grdssAnalitica 
               Height          =   1980
               Index           =   0
               Left            =   150
               TabIndex        =   4
               Top             =   0
               Width           =   8520
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15028
               _ExtentY        =   3492
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraPerfilTec 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   1995
            Left            =   150
            TabIndex        =   16
            Top             =   375
            Width           =   8820
            Begin SSDataWidgets_B.SSDBGrid grdssPerfilTec 
               Height          =   1980
               Index           =   0
               Left            =   150
               TabIndex        =   3
               Top             =   0
               Width           =   8520
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   15028
               _ExtentY        =   3492
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraOrgano 
      Caption         =   "Organo/L�quido"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2280
      Left            =   75
      TabIndex        =   7
      Top             =   2325
      Width           =   9465
      Begin TabDlg.SSTab tabOrgano 
         Height          =   1860
         HelpContextID   =   90001
         Left            =   150
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   300
         Width           =   9045
         _ExtentX        =   15954
         _ExtentY        =   3281
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AP00717.frx":008C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(3)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtOrgano(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtOrgano(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtOrgano(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtOrgano(3)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AP00717.frx":00A8
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssOrgano(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtOrgano 
            BackColor       =   &H00FFFF00&
            DataField       =   "PR24DesCompleta"
            Height          =   285
            Index           =   3
            Left            =   1650
            MaxLength       =   200
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n de la muestra"
            Top             =   975
            Width           =   6855
         End
         Begin VB.TextBox txtOrgano 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR49CodTipMuestr"
            Height          =   285
            Index           =   1
            Left            =   3375
            TabIndex        =   15
            Tag             =   "Tipo Muestra|C�digo del Tipo de Muestra"
            Top             =   150
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtOrgano 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR24CodMuestra"
            Height          =   285
            Index           =   0
            Left            =   6300
            TabIndex        =   6
            Tag             =   "Tipo Muestra|C�digo del Tipo de Muestra"
            Top             =   150
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtOrgano 
            BackColor       =   &H00FFFF00&
            DataField       =   "PR24DesCorta"
            Height          =   285
            Index           =   2
            Left            =   1650
            MaxLength       =   200
            TabIndex        =   1
            Tag             =   "Muestra"
            Top             =   525
            Width           =   3480
         End
         Begin SSDataWidgets_B.SSDBGrid grdssOrgano 
            Height          =   1710
            Index           =   0
            Left            =   -74925
            TabIndex        =   14
            Top             =   75
            Width           =   8445
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   14896
            _ExtentY        =   3016
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   450
            TabIndex        =   24
            Top             =   975
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Muestra:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   825
            TabIndex        =   13
            Top             =   525
            Width           =   780
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   7515
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Organo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objTiOrg As New clsCWForm
Dim objOrgano As New clsCWForm
Dim objPerfilTec As New clsCWForm
Dim objAnalitica As New clsCWForm
Dim objPerfilDiag As New clsCWForm
Dim desigTiTec As String

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim strKey As String, sql As String
  
  Screen.MousePointer = 11
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objTiOrg
    .strName = "TiOrg"
    Set .objFormContainer = fraTiOrg
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTiOrg
    Set .grdGrid = grdssTiOrg(0)
    .strTable = "PR4900"
    .strWhere = "PR49CODTIPMUESTR IN (" _
              & "SELECT PR49CODTIPMUESTR FROM PR2400 WHERE PR24CodMuestra IN (" _
              & "SELECT PR2500.PR24CodMuestra FROM PR2500, PR0200 WHERE " _
              & "PR2500.PR01CodActuacion = PR0200.PR01CodActuacion AND " _
              & "PR0200.AD02CodDpto = " & cDptAP & "))"
    .intAllowance = cwAllowReadOnly
    Call .objPrinter.Add("AP1701.rpt", "Listado de Tipos de muestras")
    Call .FormAddOrderField("PR49DesTipMuestr", cwAscending)
    strKey = .strTable
    Call .FormCreateFilterWhere(strKey, "Muestras")
    Call .FormAddFilterWhere(strKey, "PR49DesTipMuestr", "Muestra", cwString)
  End With
  
  With objOrgano
    .strName = "Organo"
    Set .objFormContainer = fraOrgano
    Set .objFatherContainer = fraTiOrg
    Set .tabMainTab = tabOrgano
    Set .grdGrid = grdssOrgano(0)
    Call .objPrinter.Add("AP1702.rpt", "Listado de muestras/perfiles de t�cnicas")
    Call .objPrinter.Add("AP1703.rpt", "Listado de muestras/anal�tica")
    Call .objPrinter.Add("AP1704.rpt", "Listado de muestras/perfiles de diagn�sticos")
    .strTable = "PR2400"
    .strWhere = "PR24CodMuestra IN (" _
              & "SELECT PR2500.PR24CodMuestra FROM PR2500, PR0200 WHERE " _
              & "PR2500.PR01CodActuacion = PR0200.PR01CodActuacion AND " _
              & "PR0200.AD02CodDpto = " & cDptAP & ")"
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("PR24DesCorta", cwAscending)
    strKey = .strTable
    Call .FormCreateFilterWhere(strKey, "Muestras")
    Call .FormAddFilterWhere(strKey, "PR24DesCorta", "Muestra", cwString)
    Call .FormAddFilterWhere(strKey, "PR24DesCompleta", "Descripci�n", cwNumeric)
    Call .FormAddRelation("PR49CODTIPMUESTR", txtTiOrg(0))
  End With
  
  With objPerfilTec
    .strName = "PerfilTec"
    Set .objFormContainer = fraPerfilTec
    Set .objFatherContainer = fraOrgano
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssPerfilTec(0)
    .intAllowance = cwAllowAdd + cwAllowDelete
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP5800"
    Call .FormAddRelation("PR24CodMuestra", txtOrgano(0))
  End With

  With objAnalitica
    .strName = "Analitica"
    Set .objFormContainer = fraAnalitica
    Set .objFatherContainer = fraOrgano
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssAnalitica(0)
    .intAllowance = cwAllowAdd + cwAllowDelete
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP3200"
    Call .FormAddOrderField("PR01CodActuacion", cwAscending)
    Call .FormAddRelation("PR24CodMuestra", txtOrgano(0))
  End With
  
  With objPerfilDiag
    .strName = "PerfilDiag"
    Set .objFormContainer = fraPerfilDiag
    Set .objFatherContainer = fraOrgano
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssPerfilDiag(0)
    .intAllowance = cwAllowAdd + cwAllowDelete
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP5700"
    Call .FormAddRelation("PR24CodMuestra", txtOrgano(0))
  End With
  
  With objWinInfo
    Call .FormAddInfo(objTiOrg, cwFormDetail)
    Call .FormAddInfo(objOrgano, cwFormDetail)
    
    Call .FormAddInfo(objPerfilTec, cwFormMultiLine)
    Call .GridAddColumn(objPerfilTec, "Organo", "PR24CodMuestra", cwNumeric, 5)
    Call .GridAddColumn(objPerfilTec, "Perfil de t�cnicas", "AP01_CodPerf", cwString, 5)
    
    Call .FormAddInfo(objAnalitica, cwFormMultiLine)
    Call .GridAddColumn(objAnalitica, "C. Muestra", "PR24CodMuestra", cwNumeric, 5)
    Call .GridAddColumn(objAnalitica, "Prueba de laboratorio", "PR01CodActuacion", cwNumeric, 5)
    
    Call .FormAddInfo(objPerfilDiag, cwFormMultiLine)
    Call .GridAddColumn(objPerfilDiag, "Organo", "PR24CodMuestra", cwNumeric, 5)
    Call .GridAddColumn(objPerfilDiag, "Perfil de diagn�sticos", "AP55_CodPerf", cwString, 50)
    grdssPerfilDiag(0).Columns(3).Visible = False

    Call .FormCreateInfo(objTiOrg)

    .CtrlGetInfo(grdssAnalitica(0).Columns(4)).strSQL = "SELECT PR01CodActuacion, PR01DesCorta " _
    & "FROM PR0100 WHERE PR01CodActuacion IN (" _
    & "SELECT PR01CodActuacion FROM Pruebas) ORDER BY PR01DesCorta"
    grdssAnalitica(0).Columns(4).Width = 4000
    
    .CtrlGetInfo(grdssPerfilTec(0).Columns(4)).strSQL = "SELECT AP01_CodPerf, AP01_Descrip " _
    & "FROM AP0100 ORDER BY AP01_Descrip"
    grdssPerfilTec(0).Columns(4).Width = 4000
    
    .CtrlGetInfo(grdssPerfilDiag(0).Columns(4)).strSQL = "SELECT AP55_CodPerf, AP55_Descrip " _
    & "FROM AP5500 ORDER BY AP55_Descrip"
    grdssPerfilDiag(0).Columns(4).Width = 4000
    
    .CtrlGetInfo(grdssPerfilTec(0).Columns(4)).blnForeign = True
    .CtrlGetInfo(grdssPerfilDiag(0).Columns(4)).blnForeign = True
    
    .CtrlGetInfo(txtTiOrg(0)).blnInGrid = False
    .CtrlGetInfo(txtTiOrg(1)).blnInFind = True
    .CtrlGetInfo(txtOrgano(0)).blnInGrid = False
    .CtrlGetInfo(txtOrgano(1)).blnInGrid = False
    .CtrlGetInfo(txtOrgano(2)).blnInFind = True
    .CtrlGetInfo(txtOrgano(3)).blnInFind = True

    Call .WinRegister
    Call .WinStabilize
  End With
  grdssPerfilTec(0).Columns(3).Visible = False
  grdssAnalitica(0).Columns(3).Visible = False
  
  Screen.MousePointer = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub
Private Sub fraDetalle_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
 Select Case tabDetalle.Tab
  Case 0
    Call objWinInfo.FormChangeActive(fraPerfilTec, True, True)
  Case 2
    Call objWinInfo.FormChangeActive(fraAnalitica, True, True)
  Case 3
    Call objWinInfo.FormChangeActive(fraPerfilDiag, True, True)
  End Select
    'fraDetalle.ForeColor = cwAZUL
End Sub

Private Sub grdssAnalitica_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwAZUL
End Sub

Private Sub grdssAnalitica_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssAnalitica_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssAnalitica_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssPersonal_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwAZUL
End Sub

Private Sub grdssPersonal_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssPersonal_RowColChange(Index As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssPersonal_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssPerfilTec_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwAZUL
End Sub

Private Sub grdssPerfilTec_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub


Private Sub grdssPerfilTec_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssPerfilTec_RowColChange(Index As Integer, ByVal LastRow As Variant, ByVal LastCol As Integer)
  Call objWinInfo.GridChangeRowCol(LastRow, LastCol)
End Sub

Private Sub grdssPr_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwAZUL
End Sub

Private Sub grdssPr_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub


Private Sub grdssPr_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssPr_RowColChange(Index As Integer, ByVal LastRow As Variant, ByVal LastCol As Integer)
  Call objWinInfo.GridChangeRowCol(LastRow, LastCol)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim sql As String
  Select Case strFormName
    Case objPerfilTec.strName
      frmA_PerfilTecnica.Show vbModal
      Set frmA_PerfilTecnica = Nothing
    Case objPerfilDiag.strName
      frmA_PerfilDiag.Show vbModal
      Set frmA_PerfilDiag = Nothing
  End Select
  Call objWinInfo.DataRefresh

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                               objWinInfo.DataGetOrder(blnHasFilter, True))
  End If
  Set objPrinter = Nothing


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub cbossOrgano_GotFocus()
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwGRIS
End Sub

Private Sub cbossOrgano_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossOrgano_Click()
  Call objWinInfo.CtrlDataChange
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossOrgano_Change()
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub tabOrgano_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objWinInfo.FormChangeActive(tabOrgano, True, True)
End Sub

Private Sub tabTiOrg_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objWinInfo.FormChangeActive(tabTiOrg, True, True)
End Sub

Private Sub tabDetalle_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case tabDetalle.Tab
      Case 0
        Call objWinInfo.FormChangeActive(fraPerfilTec, True, True)
      Case 1
        Call objWinInfo.FormChangeActive(fraAnalitica, True, True)
      Case 2
        Call objWinInfo.FormChangeActive(fraPerfilDiag, True, True)
    End Select
    'fraDetalle.ForeColor = cwAZUL
End Sub

Private Sub Timer1_Timer()
   Select Case tabDetalle.Tab
    Case 0
        fraDetalle.ForeColor = fraPerfilTec.ForeColor
    Case 1
        fraDetalle.ForeColor = fraAnalitica.ForeColor
    Case 2
        fraDetalle.ForeColor = fraPerfilDiag.ForeColor
    End Select
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdssTiOrg_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwGRIS
End Sub

Private Sub grdssTiOrg_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTiOrg_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTiOrg_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssOrgano_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwGRIS
End Sub

Private Sub grdssOrgano_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssOrgano_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssOrgano_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssDatosOrg_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwAZUL
End Sub

Private Sub grdssDatosOrg_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssDatosOrg_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssDatosOrg_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraOrgano_Click()
  Call objWinInfo.FormChangeActive(fraOrgano, False, True)
  'fraDetalle.ForeColor = cwGRIS
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkDatosOrg_GotFocus()
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwAZUL
End Sub

Private Sub chkDatosOrg_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkDatosOrg_Click()
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub chkOrgano_GotFocus()
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwGRIS
End Sub

Private Sub chkOrgano_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkOrgano_Click()
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtOrgano_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwGRIS
End Sub

Private Sub txtOrgano_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtOrgano_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtTiOrg_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  'fraDetalle.ForeColor = cwGRIS
End Sub

Private Sub txtTiOrg_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtTiOrg_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtDatosOrg_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtDatosOrg_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtDatosOrg_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


