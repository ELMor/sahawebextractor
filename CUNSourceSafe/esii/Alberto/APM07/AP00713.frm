VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmA_TipoPrueba 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de los Tipos de Pruebas1"
   ClientHeight    =   7965
   ClientLeft      =   2550
   ClientTop       =   3090
   ClientWidth     =   9765
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7965
   ScaleWidth      =   9765
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   9765
      _ExtentX        =   17224
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   0
      Top             =   0
   End
   Begin VB.Frame fraDatos 
      Caption         =   "Datos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4815
      Left            =   75
      TabIndex        =   11
      Top             =   2850
      Width           =   9465
      Begin TabDlg.SSTab tabDatos 
         Height          =   4410
         HelpContextID   =   90001
         Left            =   150
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   300
         Width           =   9180
         _ExtentX        =   16193
         _ExtentY        =   7779
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   -2147483644
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Pruebas"
         TabPicture(0)   =   "AP00713.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraPr"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Resultados"
         TabPicture(1)   =   "AP00713.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraRes"
         Tab(1).Control(1)=   "fraResPos"
         Tab(1).ControlCount=   2
         Begin VB.Frame fraResPos 
            Caption         =   "Resultados Posibles"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1995
            Left            =   -74850
            TabIndex        =   25
            Top             =   2325
            Width           =   8535
            Begin TabDlg.SSTab tabResPos 
               Height          =   1590
               HelpContextID   =   90001
               Left            =   150
               TabIndex        =   26
               TabStop         =   0   'False
               Top             =   300
               Width           =   8190
               _ExtentX        =   14446
               _ExtentY        =   2805
               _Version        =   327681
               TabOrientation  =   3
               Style           =   1
               Tabs            =   2
               TabsPerRow      =   2
               TabHeight       =   520
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BackColor       =   12632256
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Detalle"
               TabPicture(0)   =   "AP00713.frx":0038
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(7)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "chkResPos(0)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "txtResPos(2)"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "txtResPos(3)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "txtResPos(1)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "txtResPos(0)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).ControlCount=   6
               TabCaption(1)   =   "Tabla"
               TabPicture(1)   =   "AP00713.frx":0054
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "grdssResPos(0)"
               Tab(1).ControlCount=   1
               Begin VB.TextBox txtResPos 
                  BackColor       =   &H0000FFFF&
                  DataField       =   "AP20_NUMRESULT"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   0
                  Left            =   6000
                  Locked          =   -1  'True
                  TabIndex        =   31
                  Tag             =   "C�digo|C�digo del Resultado Posible"
                  Top             =   225
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtResPos 
                  BackColor       =   &H00FFFF00&
                  DataField       =   "AP20_VALOR"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   1
                  Left            =   1020
                  Locked          =   -1  'True
                  MaxLength       =   20
                  TabIndex        =   27
                  Tag             =   "Valor Posible|Valor Posible del Resultado"
                  Top             =   480
                  Width           =   4320
               End
               Begin VB.TextBox txtResPos 
                  BackColor       =   &H0000FFFF&
                  DataField       =   "AP14_CODRESULT"
                  Height          =   285
                  Index           =   3
                  Left            =   5925
                  TabIndex        =   30
                  Top             =   975
                  Visible         =   0   'False
                  Width           =   720
               End
               Begin VB.TextBox txtResPos 
                  BackColor       =   &H0000FFFF&
                  DataField       =   "AP60_CODTIPR"
                  Height          =   285
                  Index           =   2
                  Left            =   5925
                  LinkTimeout     =   3
                  TabIndex        =   29
                  Top             =   600
                  Visible         =   0   'False
                  Width           =   720
               End
               Begin VB.CheckBox chkResPos 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Activo"
                  DataField       =   "AP20_INDACTIVA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   0
                  Left            =   240
                  TabIndex        =   28
                  Tag             =   "Activa|Activa"
                  Top             =   960
                  Width           =   990
               End
               Begin SSDataWidgets_B.SSDBGrid grdssResPos 
                  Height          =   1395
                  Index           =   0
                  Left            =   -74925
                  TabIndex        =   32
                  Top             =   120
                  Width           =   7650
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Col.Count       =   0
                  AllowUpdate     =   0   'False
                  AllowRowSizing  =   0   'False
                  SelectTypeRow   =   1
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  SplitterVisible =   -1  'True
                  Columns(0).Width=   3200
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   4096
                  UseDefaults     =   0   'False
                  _ExtentX        =   13494
                  _ExtentY        =   2461
                  _StockProps     =   79
                  ForeColor       =   0
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Valor:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   7
                  Left            =   360
                  TabIndex        =   33
                  Top             =   540
                  Width           =   555
               End
            End
         End
         Begin VB.Frame fraRes 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   1965
            Left            =   -74850
            TabIndex        =   15
            Top             =   375
            Width           =   8490
            Begin TabDlg.SSTab tabRes 
               Height          =   1905
               Left            =   0
               TabIndex        =   16
               TabStop         =   0   'False
               Top             =   0
               Width           =   8415
               _ExtentX        =   14843
               _ExtentY        =   3360
               _Version        =   327681
               TabOrientation  =   3
               Style           =   1
               Tabs            =   2
               TabsPerRow      =   2
               TabHeight       =   529
               WordWrap        =   0   'False
               ShowFocusRect   =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Detalle"
               TabPicture(0)   =   "AP00713.frx":0070
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "lblLabel1(5)"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).Control(1)=   "lblLabel1(4)"
               Tab(0).Control(1).Enabled=   0   'False
               Tab(0).Control(2)=   "cbossRes"
               Tab(0).Control(2).Enabled=   0   'False
               Tab(0).Control(3)=   "chkRes(0)"
               Tab(0).Control(3).Enabled=   0   'False
               Tab(0).Control(4)=   "txtRes(1)"
               Tab(0).Control(4).Enabled=   0   'False
               Tab(0).Control(5)=   "txtRes(0)"
               Tab(0).Control(5).Enabled=   0   'False
               Tab(0).Control(6)=   "txtRes(2)"
               Tab(0).Control(6).Enabled=   0   'False
               Tab(0).ControlCount=   7
               TabCaption(1)   =   "Tabla"
               TabPicture(1)   =   "AP00713.frx":008C
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "grdssRes(0)"
               Tab(1).ControlCount=   1
               Begin VB.TextBox txtRes 
                  BackColor       =   &H0000FFFF&
                  DataField       =   "AP60_CODTIPR"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   2
                  Left            =   7125
                  Locked          =   -1  'True
                  TabIndex        =   21
                  Top             =   600
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtRes 
                  BackColor       =   &H0000FFFF&
                  DataField       =   "AP14_CODRESULT"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   0
                  Left            =   7125
                  Locked          =   -1  'True
                  TabIndex        =   20
                  Tag             =   "C�digo|C�digo del Resultado"
                  Top             =   225
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtRes 
                  BackColor       =   &H00FFFF00&
                  DataField       =   "AP14_DESIG"
                  Height          =   285
                  HelpContextID   =   40101
                  Index           =   1
                  Left            =   2160
                  Locked          =   -1  'True
                  MaxLength       =   20
                  TabIndex        =   17
                  Tag             =   "Designaci�n|Nombre abreviado del Resultado"
                  Top             =   450
                  Width           =   4515
               End
               Begin VB.CheckBox chkRes 
                  Alignment       =   1  'Right Justify
                  Caption         =   "Activo"
                  DataField       =   "AP14_INDACTIVA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   0
                  Left            =   5700
                  TabIndex        =   19
                  Tag             =   "Activa|Activa"
                  Top             =   1050
                  Width           =   990
               End
               Begin SSDataWidgets_B.SSDBGrid grdssRes 
                  Height          =   1740
                  Index           =   0
                  Left            =   -74850
                  TabIndex        =   22
                  TabStop         =   0   'False
                  Top             =   75
                  Width           =   7755
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Col.Count       =   0
                  BevelColorFrame =   0
                  BevelColorHighlight=   16777215
                  AllowUpdate     =   0   'False
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   2
                  AllowGroupSwapping=   0   'False
                  AllowGroupShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   1
                  MaxSelectedRows =   0
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  SplitterVisible =   -1  'True
                  Columns(0).Width=   3200
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   4096
                  UseDefaults     =   -1  'True
                  _ExtentX        =   13679
                  _ExtentY        =   3069
                  _StockProps     =   79
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBCombo cbossRes 
                  DataField       =   "AP14_TIRESULT"
                  Height          =   330
                  Left            =   2175
                  TabIndex        =   18
                  Tag             =   "Tipo de Resultado|Tipo de Resultado"
                  Top             =   975
                  Width           =   2805
                  DataFieldList   =   "Column 0"
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  FieldSeparator  =   ","
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3387
                  Columns(1).Caption=   "Nombre"
                  Columns(1).Name =   "Descripci�n"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4948
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   16777215
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  DataFieldToDisplay=   "Column 1"
               End
               Begin VB.Label lblLabel1 
                  BackColor       =   &H00C0C0C0&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Designaci�n:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   855
                  TabIndex        =   24
                  Top             =   525
                  Width           =   1125
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Tipo de Resultado:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   5
                  Left            =   375
                  TabIndex        =   23
                  Top             =   1050
                  Width           =   1800
               End
            End
         End
         Begin VB.Frame fraPr 
            BorderStyle     =   0  'None
            Caption         =   "Pruebas"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3810
            Left            =   225
            TabIndex        =   13
            Top             =   450
            Width           =   8340
            Begin SSDataWidgets_B.SSDBGrid grdssPr 
               Height          =   3705
               Index           =   0
               Left            =   75
               TabIndex        =   14
               Top             =   0
               Width           =   7995
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   14102
               _ExtentY        =   6535
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraTiPr 
      Caption         =   "Tipos de Pruebas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2340
      Left            =   75
      TabIndex        =   5
      Top             =   450
      Width           =   9465
      Begin TabDlg.SSTab tabTiPr 
         Height          =   1935
         HelpContextID   =   90001
         Left            =   225
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   300
         Width           =   8970
         _ExtentX        =   15822
         _ExtentY        =   3413
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AP00713.frx":00A8
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "chkTiPr(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtTiPr(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtTiPr(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtTiPr(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AP00713.frx":00C4
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssTiPr(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtTiPr 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP60_DesTiPr"
            Height          =   285
            Index           =   2
            Left            =   2625
            MaxLength       =   200
            TabIndex        =   0
            Tag             =   "Clase de prueba"
            Top             =   300
            Width           =   2355
         End
         Begin VB.TextBox txtTiPr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AP60_CodTiPr"
            Height          =   285
            Index           =   0
            Left            =   150
            TabIndex        =   7
            Tag             =   "Tipo T�cnica|C�digo del Tipo de T�cnica"
            Top             =   450
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtTiPr 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP60_Descrip"
            Height          =   285
            Index           =   1
            Left            =   2625
            MaxLength       =   60
            TabIndex        =   1
            Tag             =   "Tipo de prueba"
            Top             =   825
            Width           =   3750
         End
         Begin VB.CheckBox chkTiPr 
            Alignment       =   1  'Right Justify
            Caption         =   "Intraoperatoria:"
            DataField       =   "AP60_IndIntra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   1125
            TabIndex        =   2
            Tag             =   "Intraoperatoria"
            Top             =   1275
            Width           =   1665
         End
         Begin SSDataWidgets_B.SSDBGrid grdssTiPr 
            Height          =   1770
            Index           =   0
            Left            =   -74775
            TabIndex        =   8
            Top             =   75
            Width           =   8175
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            _ExtentX        =   14420
            _ExtentY        =   3122
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Clase:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   1950
            TabIndex        =   10
            Top             =   300
            Width           =   630
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo de prueba:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   1125
            TabIndex        =   9
            Top             =   825
            Width           =   1500
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   7680
      Width           =   9765
      _ExtentX        =   17224
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_TipoPrueba"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objTiPr As New clsCWForm
Dim objPr As New clsCWForm
'Dim objPreg As New clsCWForm
'Dim objResp As New clsCWForm
Dim objRes As New clsCWForm
Dim objResPos As New clsCWForm


Private Sub cbossRes_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossRes_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossRes_Click()
  Call objWinInfo.CtrlDataChange
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossRes_Change()
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
Dim strKey As String, sql As String
  
  Screen.MousePointer = 11
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objTiPr
    .strName = "TiPr"
    Set .objFormContainer = fraTiPr
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTiPr
    Set .grdGrid = grdssTiPr(0)
    .strTable = "AP6000"
    .intAllowance = cwAllowReadOnly
'    Call .FormAddOrderField("AP60_DesTiPr", cwAscending)
    Call .objPrinter.Add("AP1301.rpt", "Listado de pruebas")
    Call .objPrinter.Add("AP1302.rpt", "Listado de Tipos de pruebas/resultados")
    Call .objPrinter.Add("AP1303.rpt", "Listado de pruebas no dadas de alta en AP")
    
    Call .FormCreateFilterWhere(.strTable, "Tipos de pruebas")
    Call .FormAddFilterWhere(.strTable, "AP60_DesTiPr", "Clase de Prueba", cwString)
    Call .FormAddFilterWhere(.strTable, "AP60_Descrip", "Tipo de Prueba", cwString)
    Call .FormAddFilterWhere(.strTable, "AP60_IndIntra", "Intraoperatoria", cwBoolean)
    
    Call .FormAddFilterOrder(.strTable, "AP60_DesTiPr", "Clase de Prueba")
    Call .FormAddFilterOrder(.strTable, "AP60_Descrip", "Tipo de Prueba")
    .blnAskPrimary = False

  End With
  
  With objPr
    .strName = "Pr"
    Set .objFormContainer = fraPr
    Set .objFatherContainer = fraTiPr
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssPr(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "AP0900"
    Call .FormAddOrderField("PR01CodActuacion", cwAscending)
    Call .FormAddRelation("AP60_CodTiPr", txtTiPr(0))
  End With
  
'  With objPreg
'    .strName = "Preg"
'    Set .objFormContainer = fraPreg
'    Set .objFatherContainer = fraPr
'    Set .tabMainTab = Nothing
'    Set .grdGrid = grdssPreg(0)
'    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
'    .strTable = "PR2900"
'    Call .FormAddOrderField("PR40CodPregunta", cwAscending)
'    Call .FormAddRelation("PR01CodActuacion", grdssPr(0).Columns("Cod. Prueba"))
'  End With
'
'  With objResp
'    .strName = "Resp"
'    Set .objFormContainer = fraResp
'    Set .objFatherContainer = fraPreg
'    Set .tabMainTab = Nothing
'    Set .grdGrid = grdssResp(0)
'    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
'    .strTable = "PR2800"
'    Call .FormAddOrderField("PR28DesRespuesta", cwAscending)
'    Call .FormAddRelation("PR28CodListResp", grdssPreg(0).Columns(4))
'  End With

  With objRes
    .strName = "Res"
    Set .objFormContainer = fraRes
    Set .objFatherContainer = fraTiPr
    Set .tabMainTab = tabRes
    Set .grdGrid = grdssRes(0)
    .strTable = "AP1400"
    .blnAskPrimary = False
    Call .FormAddOrderField("AP14_CodResult", cwAscending)
    Call .FormAddRelation("AP60_CodTiPr", txtTiPr(0))
  End With

  With objResPos
    .strName = "ResPos"
    Set .objFormContainer = fraResPos
    Set .objFatherContainer = fraRes
    Set .tabMainTab = tabResPos
    Set .grdGrid = grdssResPos(0)
    .strTable = "AP2000"
    .blnAskPrimary = False
    Call .FormAddOrderField("AP20_NumResult", cwAscending)
    Call .FormAddRelation("AP60_CodTiPr", txtTiPr(0))
    Call .FormAddRelation("AP14_CodResult", txtRes(0))
  End With

  With objWinInfo
    Call .FormAddInfo(objTiPr, cwFormDetail)
    
    Call .FormAddInfo(objPr, cwFormMultiLine)
    Call .GridAddColumn(objPr, "Cod. Actuaci�n", "AP60_CodTiPr", cwNumeric, 2)
    Call .GridAddColumn(objPr, "Prueba", "PR01CodActuacion", cwString, 20)
    Call .GridAddColumn(objPr, "Cod. Prueba", "", cwNumeric, 9)
    Call .GridAddColumn(objPr, "Descripci�n", "", cwString, 60)
    Call .GridAddColumn(objPr, "F. Activaci�n", "", cwString, 15)
    Call .GridAddColumn(objPr, "F. Desactivaci�n", "", cwString, 15)
        
'    Call .FormAddInfo(objPreg, cwFormMultiLine)
'    Call .GridAddColumn(objPreg, "Cod. Actuaci�n", "PR01CodActuacion", cwNumeric, 6)
'    Call .GridAddColumn(objPreg, "Cod. Preg", "PR40CodPregunta", cwNumeric, 6)
'    Call .GridAddColumn(objPreg, "Lista", "PR28CodListResp", cwNumeric, 6)
'    Call .GridAddColumn(objPreg, "Pregunta", "", cwString, 30)
'    Call .GridAddColumn(objPreg, "Tipo de respuesta", "", cwString, 30)
'
'    Call .FormAddInfo(objResp, cwFormMultiLine)
'    Call .GridAddColumn(objResp, "Lista", "PR28CodListResp", cwNumeric, 6)
'    Call .GridAddColumn(objResp, "Num", "PR28NumRespuesta", cwNumeric, 6)
'    Call .GridAddColumn(objResp, "Respuesta", "PR28DesRespuesta", cwString, 6)
        
    Call .FormAddInfo(objRes, cwFormDetail)
    Call .FormAddInfo(objResPos, cwFormDetail)
    
    Call .FormCreateInfo(objTiPr)
    .CtrlGetInfo(txtTiPr(1)).blnInFind = True
    .CtrlGetInfo(txtTiPr(2)).blnInFind = True
    .CtrlGetInfo(chkTiPr(0)).blnInFind = True
    .CtrlGetInfo(txtRes(0)).blnValidate = False
    .CtrlGetInfo(txtResPos(0)).blnValidate = False

    sql = "SELECT PR01CodActuacion, PR01DesCorta FROM PR0100 WHERE PR01CodActuacion IN (" _
        & "SELECT PR01CodActuacion FROM PR0200 WHERE AD02CodDpto = " & cDptAP & ")"
    .CtrlGetInfo(grdssPr(0).Columns(4)).strSQL = sql

    sql = "SELECT PR01CodActuacion, PR01DesCorta, PR01DesCompleta, PR01FecInico, PR01FecFin FROM PR0100 WHERE PR01CodActuacion = ?"
    Call .CtrlCreateLinked(.CtrlGetInfo(grdssPr(0).Columns(4)), "PR01CodActuacion", sql)
    Call .CtrlAddLinked(.CtrlGetInfo(grdssPr(0).Columns(4)), grdssPr(0).Columns(5), "PR01CodActuacion")
    Call .CtrlAddLinked(.CtrlGetInfo(grdssPr(0).Columns(4)), grdssPr(0).Columns(6), "PR01DesCompleta")
    Call .CtrlAddLinked(.CtrlGetInfo(grdssPr(0).Columns(4)), grdssPr(0).Columns(7), "PR01FecInico")
    Call .CtrlAddLinked(.CtrlGetInfo(grdssPr(0).Columns(4)), grdssPr(0).Columns(8), "PR01FecFin")

    .CtrlGetInfo(txtRes(0)).blnInGrid = False
    .CtrlGetInfo(txtRes(2)).blnInGrid = False
    .CtrlGetInfo(txtResPos(0)).blnInGrid = False
    .CtrlGetInfo(txtResPos(2)).blnInGrid = False
    .CtrlGetInfo(txtResPos(3)).blnInGrid = False
    cbossRes.AddItem "1,Opcional"
    cbossRes.AddItem "2,Obligatorio"
    
    Call .WinRegister
    Call .WinStabilize
  End With
  grdssPr(0).Columns(3).Visible = False
'  grdssTiPr(0).Columns(1).Visible = False
  
  Screen.MousePointer = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraResPosible_DragDrop(Source As Control, X As Single, Y As Single)

End Sub

Private Sub fraRes_Click()
  Call objWinInfo.FormChangeActive(fraRes, False, True)
End Sub

Private Sub fraResPos_Click()
  Call objWinInfo.FormChangeActive(fraResPos, False, True)
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Select Case strFormName
    Case objRes.strName
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        txtRes(0).Text = NuevoCodRes
        objRes.rdoCursor("AP14_CodResult") = txtRes(0).Text
      End If
    Case objResPos.strName
      If objWinInfo.intWinStatus = cwModeSingleAddRest Then
        txtResPos(0).Text = NuevoCodResPos
        objResPos.rdoCursor("AP20_NumResult") = txtResPos(0).Text
      End If
  End Select
End Sub
Function NuevoCodRes() As Long
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT Max(AP14_CodResult) FROM AP1400 WHERE AP60_CodTiPr = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(0) = txtTiPr(0).Text
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If IsNull(rdo(0)) Then NuevoCodRes = 1 Else NuevoCodRes = rdo(0) + 1
  rdo.Close
End Function
Function NuevoCodResPos() As Long
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT MAX(AP20_NumResult) FROM AP2000 WHERE AP60_CodTiPr = ? AND AP14_CodResult = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeINTEGER
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = txtTiPr(0).Text
  rdoQ(1) = txtRes(0).Text
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If IsNull(rdo(0)) Then NuevoCodResPos = 1 Else NuevoCodResPos = rdo(0) + 1
  rdo.Close
End Function
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer
Dim objPrinter As clsCWPrinter
Dim blnHasFilter As Boolean
  
  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
    blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
    Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                               objWinInfo.DataGetOrder(blnHasFilter, True))
  End If
  Set objPrinter = Nothing


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



Private Sub tabRes_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objWinInfo.FormChangeActive(tabRes, False, True)
End Sub

Private Sub tabResPos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objWinInfo.FormChangeActive(tabResPos, False, True)
End Sub

Private Sub Timer1_Timer()
  Select Case tabDatos.Tab
    Case 0
      fraDatos.ForeColor = fraPr.ForeColor
    Case 1
      fraDatos.ForeColor = fraRes.ForeColor
  End Select
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdssTiPr_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssTiPr_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTiPr_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTiPr_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssPr_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssPr_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssPr_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssPr_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssPreg_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssPreg_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssPreg_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssPreg_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssResp_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssResp_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssResp_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssResp_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssRes_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssRes_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssRes_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssRes_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssResPos_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssResPos_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssResPos_RowColChange(Index As Integer, ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssResPos_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTiPr_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTiPr, False, True)
End Sub
Private Sub tabDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Select Case tabDatos.Tab
    Case 0
      Call objWinInfo.FormChangeActive(fraPr, True, True)
    Case 1
      Call objWinInfo.FormChangeActive(fraRes, True, True)
  End Select
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraTiPr_Click()
  Call objWinInfo.FormChangeActive(fraTiPr, False, True)
End Sub

Private Sub fraPr_Click()
  Call objWinInfo.FormChangeActive(fraPr, False, True)
End Sub

'==========================================================================

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkTiPr_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkTiPr_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkTiPr_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkRes_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkRes_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkRes_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkResPos_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkResPos_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkResPos_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtTiPr_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtTiPr_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtTiPr_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtRes_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtRes_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtRes_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtResPos_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtResPos_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtResPos_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



