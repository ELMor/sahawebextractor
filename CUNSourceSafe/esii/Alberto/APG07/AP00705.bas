Attribute VB_Name = "modDiag"
Option Explicit

Sub LlenarOptSno(frm As Form, TiSno() As String)
Dim sql As String
Dim rdo As rdoResultset
Dim i As Integer, Tipo As Integer
Dim descrip As String
  
  sql = "SELECT AP5000.AP50_CodTiSno, AP5000.AP59_CodGrupo, AP5900.AP59_Desig " _
      & "FROM AP5000, AP5900 WHERE " _
      & "AP5000.AP59_CodGrupo = AP5900.AP59_CodGrupo " _
      & "ORDER BY AP5000.AP59_CodGrupo"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  i = -1
  descrip = ""
  Tipo = 0
  While rdo.EOF = False
    If Tipo = rdo(1) Then
      TiSno(i) = TiSno(i) & "', '" & rdo(0)
    Else
      i = i + 1
      ReDim Preserve TiSno(0 To i)
      TiSno(i) = rdo(0)
      frm.chkSno(i).Caption = rdo(2)
    End If
    Tipo = rdo(1)
    rdo.MoveNext
  Wend
  rdo.Close
End Sub
Function ComprobarSel(frm As Form, TiSno() As String) As Integer
Dim i As Integer
  
  ComprobarSel = False
  For i = 0 To UBound(TiSno)
    If frm.chkSno(i).Value = 1 Then
      ComprobarSel = True
      Exit Function
    End If
  Next i
End Function

Function chkSelec(frm As Form, TiSno() As String) As String
Dim i As Integer
  
  chkSelec = "('"
  For i = 0 To UBound(TiSno, 1)
    If frm.chkSno(i).Value = 1 Then
      chkSelec = chkSelec & TiSno(i) & "','"
    End If
  Next i
  chkSelec = Left(chkSelec, Len(chkSelec) - 2) & ")"
End Function

