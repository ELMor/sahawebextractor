Attribute VB_Name = "modImprimir"
Public Const interlinea1 = 500
Public Const interlinea2 = 300
Public Const margensuperior = 1300
Public Const margenizquierdo = 1000

Option Explicit

Sub ImprimirNegrita(X As Long, Y As Long, texto1 As String, texto2 As String)
  With Printer
    .CurrentX = X
    .CurrentY = Y
    .FontBold = True
    Printer.Print texto1
    .CurrentY = Y
    .CurrentX = X + Printer.TextWidth(texto1) + 70
    .FontBold = False
    Printer.Print texto2
  End With
End Sub
Sub ImprimirConSaltos(texto As String, margen As Integer, posi As Long)
Dim donde As Integer, linea As String, linea1 As String, linea2 As String, linea3 As String
Dim limite As Long, Y As Long
  
  limite = Printer.ScaleWidth - margen - 800
  texto = texto & Chr$(13)
  donde = InStr(texto, Chr$(13))
  While donde > 0
    linea1 = Left(texto, donde - 1)
    While Printer.TextWidth(linea1) > limite
      linea = ""
      While Printer.TextWidth(linea) < limite
        linea2 = linea
        linea = linea & Left(linea1, InStr(linea1, Chr$(32)))
        If linea = linea2 Then linea = linea & linea1
        linea3 = linea1
        linea1 = Right(linea1, Len(linea1) - InStr(linea1, Chr$(32)))
      Wend
      Printer.CurrentX = margen
      Printer.Print linea2
      linea1 = linea3
      posi = posicion(posi, 400)
    Wend
'    Printer.CurrentY = posi
    Printer.CurrentX = margen
    Printer.Print linea1
    If Len(texto) > InStr(texto, Chr$(13)) Then
      texto = Right(texto, Len(texto) - InStr(texto, Chr$(13)))
    Else
      texto = Left(texto, Len(texto) - 1)
    End If
    donde = InStr(texto, Chr$(13))
  Wend
End Sub

Sub ImprimirHojaRecepcion(nRef As String, TiPac As Integer)

Dim sql As String
Dim rdoQ As rdoQuery
Dim rdoQ1 As rdoQuery
Dim rdo As rdoResultset
Dim cama As String
Dim rdo1 As rdoResultset
Dim anam As String
Dim i As Integer, j As Integer
Dim posi As Long
Dim NH As Long, NC As Long
Dim Diag As String, res As String
Dim Org As String
Dim Ana() As ResAnalitica
  
  Call ImpresoraDefecto("Recepcion")
  With Printer
    .PaperSize = vbPRPSA4
    .DrawWidth = 5
  
    Printer.Line (margenizquierdo - 100, margensuperior + interlinea1 - 130)-(11000, margensuperior + interlinea1 + interlinea2 * 3 + 250), , B
    Printer.Line (margenizquierdo - 100, margensuperior + interlinea2 * 3 + interlinea1 * 2 - 150)-(11000, margensuperior + interlinea2 * 6 + interlinea1 * 2 + 250), , B
    Printer.Line (margenizquierdo + 4950, margensuperior + interlinea2 * 3 + interlinea1 * 2 - 150)-(margenizquierdo + 4950, margensuperior + interlinea2 * 6 + interlinea1 * 2 + 250)
   
    .FontName = 1
    .FontSize = 15
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = margensuperior
    Printer.Print "Resumen de la Peticion"
    
    If TiPac = 1 Then  ' Si es paciente de CUN
      sql = "SELECT Pac.NH, Asist.NC, Camas.Cama, " _
          & "CI2200.CI22SegApel||' '||CI2200.CI22PriApel||', '||CI2200.Nom, " _
          & "NVL(DrResp.txtFirma,DrResp.AP1), DptResp.Desig, Pr.Desig, " _
          & "NVL(DrSote.txtFirma,DrSote.AP1), DptSote.Desig, TO_CHAR(AP2100.AP21_FecSotud,'DD/MM/YYYY HH24:MI'), " _
          & "AP08Pat.AP08_Firma, AP08Res.AP08_Firma, AP2100.AP21_Anam, AP2100.AP21_Preg, " _
          & "AP2100.AP21_Observ, AP2100.cPr, AP2100.AP21_TiPr, AP08Cito.AP08_Firma " _
          & "FROM AP2100, Pac, Asist, Pr, Dr DrResp, Dr DrSote, Dpt DptResp, Dpt DptSote, " _
          & "SG0200 SG02Pat, SG0200 SG02Res, SG0200 SG02Cito, Camas WHERE " _
          & "AP2100.AP21_CodHist = Pac.NH AND " _
          & "AP2100.AP21_CodHist = Asist.NH AND " _
          & "AP2100.AP21_CodCaso = Asist.NC AND " _
          & "PR0400.AD01CodAsistenci = AD1600.AD01CodAsistenci (+) AND " _
          & "Asist.cDrResp = DrResp.cDr AND " _
          & "Asist.cDptResp = DptResp.cDpt AND " _
          & "AP2100.AP21_CodDr = DrSote.cDr AND " _
          & "AP2100.cDpt = DptSote.cDpt AND " _
          & "AP2100.cPr = Pr.cPr AND " _
          & "Pr.cdpt = " & cDptAP & " AND " _
          & "AP2100.SG02Cod_Pat = SG02Pat.SG02Cod AND " _
          & "AP2100.SG02Cod_Res = SG02Res.SG02Cod (+) AND " _
          & "AP2100.SG02Cod_Cito = SG02Cito.SG02Cod (+) AND " _
          & "AP2100.AP21_CodRef = ?"
    Else          ' Si es paciente externo
      sql = "SELECT Pac.NH, Asist.NC, Camas.Cama, Pac.AP1||' '||Pac.AP2||', '||Pac.Nom, " _
          & "NVL(DrResp.txtFirma,DrResp.AP1), DptResp.Desig, Pr.Desig, " _
          & "NVL(DrSote.AP15_Firma,DrSote.AP15_AP1), CtroSote.AP11_Desig, TO_CHAR(AP2100.AP21_FecSotud,'DD/MM/YYYY HH24:MI'), " _
          & "AP08Pat.AP08_Firma, AP08Res.AP08_Firma, AP2100.AP21_Anam, AP2100.AP21_Preg, " _
          & "AP2100.AP21_Observ, AP2100.cPr, AP2100.AP21_TiPr, AP08Cito.AP08_Firma " _
          & "FROM AP2100, Pac, Asist, PR0100, SG0200 SG02DrResp, AP1500 DrSote, Dpt DptResp, AP1100 CtroSote, " _
          & "SG0200 SG02Pat, SG0200 SG02Res, SG0200 SG02Cito, AD1600 WHERE " _
          & "AP2100.AP21_CodHist = Pac.NH AND " _
          & "AP2100.AP21_CodHist = Asist.NH AND " _
          & "AP2100.AP21_CodCaso = Asist.NC AND " _
          & "PR0400.AD01CodAsistenci = AD1600.AD01CodAsistenci (+) AND " _
          & "Asist.cDrResp = DrResp.cDr AND " _
          & "Asist.cDptResp = DptResp.cDpt AND " _
          & "AP2100.AP21_CodDr = DrSote.AP15_CodPat AND " _
          & "AP2100.AP11_CodCtro = CtroSote.AP11_CodCtro AND " _
          & "AP2100.cPr = Pr.cPr AND " _
          & "Pr.cdpt = " & cDptAP & " AND " _
          & "AP2100.AP08_CodPers_Pat = AP08Pat.AP08_CodPers AND " _
          & "AP2100.AP08_CodPers_Res = AP08Res.AP08_CodPers (+) AND " _
          & "AP2100.AP08_CodPers_Cito = AP08Cito.AP08_CodPers (+) AND " _
          & "AP2100.AP21_CodRef = ?"
    End If
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeVARCHAR
    rdoQ(0) = nRef
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    NH = rdo(0)
    NC = rdo(1)
    .FontSize = 12
    posi = margensuperior + interlinea1
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = posi
    Printer.Print "N� Ref: " & Right(nRef, 8)
    .FontSize = 9
    Call ImprimirNegrita(margenizquierdo + 3300, posi, "Historia/Asistencia: ", NH & "/" & NC)
    If IsNull(rdo(2)) Then cama = "" Else cama = rdo(2)
    Call ImprimirNegrita(margenizquierdo + 7200, posi, "Cama: ", cama)
    posi = posicion(interlinea2, posi)
    Call ImprimirNegrita(margenizquierdo, posi, "Paciente: ", rdo(3))
    posi = posicion(interlinea2, posi)
    Call ImprimirNegrita(margenizquierdo, posi, "Dpto. Responsable: ", rdo(5))
    Call ImprimirNegrita(margenizquierdo + 5400, posi, "Dr. Responsable: ", rdo(4))
    posi = posicion(posi, interlinea2)
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = posi
    Printer.Print "Prueba Solicitada: "
    .CurrentY = posi
    .CurrentX = margenizquierdo + Printer.TextWidth("Prueba Solicitada: ") + 70
    Printer.Print UCase(rdo(6))
    posi = posicion(posi, interlinea1)
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = posi
    Printer.Print "SOLICITUD"
    
    .CurrentX = margenizquierdo + 5000
    .CurrentY = posi
    Printer.Print "REALIZACION"
    .FontSize = 9
    posi = posicion(posi, interlinea2)
    Call ImprimirNegrita(margenizquierdo, posi, "Doctor Solicitante: ", rdo(7))
    If IsNull(rdo(11)) Then res = "" Else res = rdo(11)
    Call ImprimirNegrita(margenizquierdo + 5000, posi, "Macro: ", res)
    posi = posicion(posi, interlinea2)
    If TiPac = 1 Then
      Call ImprimirNegrita(margenizquierdo, posi, "Departamento Solicitante: ", rdo(8))
    Else
      Call ImprimirNegrita(margenizquierdo, posi, "Centro Solicitante: ", rdo(8))
    End If
    Call ImprimirNegrita(margenizquierdo + 5000, margensuperior + interlinea2 * 5 + interlinea1 * 2, "Micro:  ", rdo(10))
    posi = posicion(posi, interlinea2)
    Call ImprimirNegrita(margenizquierdo, margensuperior + interlinea2 * 6 + interlinea1 * 2, "Fecha/Hora Solicitud: ", rdo(9))
    If rdo(16) = 2 And Not IsNull(rdo(17)) Then ' Citolog�as
      Call ImprimirNegrita(margenizquierdo + 5000, margensuperior + interlinea2 * 6 + interlinea1 * 2, "Citot�cnico:  ", rdo(17))
    End If
    
    sql = "SELECT AP04_DESIG FROM AP0400,AP1700 " _
        & "WHERE AP0400.AP04_CODTITEC=AP1700.AP04_CODTITEC AND CPR = ?  "
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(0) = rdo(15)
    Set rdo1 = rdoQ.OpenResultset(rdOpenForwardOnly)
    If rdo1.EOF = False Then
      posi = posicion(posi, interlinea1)
      .FontBold = True
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "T�CNICAS ESPECIALES"
      .FontBold = False
      While rdo1.EOF = False
          i = i + 1
          posi = posicion(interlinea2, posi)
          .CurrentX = margenizquierdo + 300
          .CurrentY = posi
          Printer.Print rdo1(0)
          rdo1.MoveNext
      Wend
    End If
    'Printer.Line (margenizquierdo - 100, posi - i * interlinea2 - 100)-(margenizquierdo + 4000, posi + 250), , B
    
    posi = posicion(posi, interlinea1)
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = posi
    Printer.Print "MATERIAL A INVESTIGAR"
    .FontBold = False
    
    sql = "SELECT AP2500.PR52NumIDMuestra, AP23_DESIG||'. '||AP25_TiMues, AP25_FECEXTRA, " _
        & "AP25_FECRECEP,AP08_FIRMA, AP2500.AP25_Observ||' Diagn�stico de presunci�n: '||AP1600.AP16_Desig, " _
        & "AP1900.AP19_Desig, AP2200.AP22_Desig, AP2500.AP23_CodOrg " _
        & "FROM AP2300,AP2500,AP0800, AP1600, AP1900, AP2200 WHERE " _
        & "AP2300.AP23_CODORG = AP2500.AP23_CODORG AND " _
        & "AP2500.AP16_CodDiag = AP1600.AP16_CodDiag AND " _
        & "AP2500.AP19_CodFija = AP1900.AP19_CodFija (+) AND " _
        & "AP2500.AP22_CodDecal = AP2200.AP22_CodDecal (+) AND " _
        & "AP2500.AP08_CODPERS = AP0800.AP08_CODPERS (+) " _
        & "AND AP2500.AP21_CODREF = ?  "
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeVARCHAR
    rdoQ(0) = nRef
    Set rdo1 = rdoQ.OpenResultset(rdOpenForwardOnly)
   
    posi = posicion(posi, interlinea2)
    .CurrentX = margenizquierdo + 300
    .CurrentY = posi
    .FontUnderline = True
    Printer.Print "Muestra"
    
    .CurrentX = margenizquierdo + 4000 + 300
    .CurrentY = posi
    .FontUnderline = True
    Printer.Print "Extracci�n"
    
    .CurrentX = margenizquierdo + 6000 + 300
    .CurrentY = posi
    .FontUnderline = True
    Printer.Print "Recepci�n"
    
    .CurrentX = margenizquierdo + 8000 + 300
    .CurrentY = posi
    .FontUnderline = True
    Printer.Print "Recepciona"
    
    .FontUnderline = False
    Org = "("
    While rdo1.EOF = False
        Org = Org & rdo1(8) & ","
        posi = posicion(posi, interlinea2)
        .CurrentX = margenizquierdo + 300
        .CurrentY = posi
        .FontBold = True
        Printer.Print rdo1(0) & ".-" & TipoFrase(rdo1(1))
        
        .FontBold = False
        .CurrentX = margenizquierdo + 4000 + 300
        .CurrentY = posi
        Printer.Print rdo1(2)
        
        .CurrentX = margenizquierdo + 6000 + 300
        .CurrentY = posi
        Printer.Print rdo1(3)
        
        .CurrentX = margenizquierdo + 8000 + 300
        .CurrentY = posi
        Printer.Print rdo1(4)
        
        posi = posicion(posi, interlinea2)
        .CurrentX = margenizquierdo + 300
        .CurrentY = posi
        Call ImprimirConSaltos(TipoFrase(rdo1(5)), margenizquierdo + 300, posi)

        rdo1.MoveNext
    Wend
    Org = Left(Org, Len(Org) - 1) & ")"
    If IsNull(rdo(12)) Then
      anam = ""
    Else
      anam = rdo(12)
      posi = posicion(posi, interlinea1)
      .FontBold = True
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "ANAMNESIS"
      .FontBold = False
      Call ImprimirConSaltos(anam, margenizquierdo, posi)
    End If
    
    If Not IsNull(rdo(13)) Then
      posi = Printer.CurrentY
      .FontBold = True
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "DIAGN�STICO CL�NICO"
      .FontBold = False
      .CurrentX = margenizquierdo
      Printer.Print rdo(13)
    End If
    
    If Not IsNull(rdo(14)) Then
      posi = posicion(posi, interlinea1)
      .FontBold = True
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "OBSERVACIONES"
      .FontBold = False
      .CurrentX = margenizquierdo
      Printer.Print rdo(14)
    End If
    
    sql = "SELECT AP31_DESIG, AP43_DATO FROM AP3100, AP4300 " _
        & "WHERE AP3100.AP23_CODORG = AP4300.AP23_CODORG AND " _
        & "AP3100.AP31_CODDATO = AP4300.AP31_CODDATO AND AP4300.AP21_CODREF = ?  "
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeVARCHAR
    rdoQ(0) = nRef
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    If rdo.EOF = False Then
      posi = posicion(posi, interlinea1)
      .FontBold = True
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "DATOS ADICIONALES"
      .FontBold = False
      While rdo.EOF = False
          posi = posicion(posi, interlinea2)
          .CurrentX = margenizquierdo + 300
          .CurrentY = posi
          Printer.Print rdo(0)
          .CurrentX = margenizquierdo + 2000 + 300
          .CurrentY = posi
          Printer.Print rdo(1)
          rdo.MoveNext
      Wend
    End If
    
    .FontBold = False  ' Anal�tica
    
    If BuscarAnalitica(NH, Org, Ana()) = True Then
      posi = posicion(posi, interlinea2)
      .CurrentX = margenizquierdo + 300
      .CurrentY = posi
      .FontBold = True
      Printer.Print "Analitica"
      .FontBold = False
      For i = 1 To UBound(Ana)
        posi = posicion(posi, interlinea2)
        .CurrentX = margenizquierdo + 300
        .CurrentY = posi
        Printer.Print Ana(i).pr & "  " & Ana(i).fecha
        For j = 1 To UBound(Ana(i).ResPr)
          posi = posicion(posi, interlinea2)
          .CurrentX = margenizquierdo + 600 + 300
          .CurrentY = posi
          Printer.Print Ana(i).ResPr(j).res & ":  " & Ana(i).ResPr(j).Resultado & " " & Ana(i).ResPr(j).Unidades
        Next j
      Next i
    End If
    
    If TiPac = 1 Then
      .FontBold = True
      posi = posicion(posi, interlinea1)
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "BIOPSIAS Y CITOLOG�AS ANTERIORES"
      .FontBold = False
      sql = "SELECT AP21_CODREF,desig, AP2100.AP21_FecSotud FROM AP2100,pr WHERE " _
          & "AP2100.cPr = Pr.cPr AND Pr.cDpt = " & cDptAP & " AND " _
          & "AP21_CODREF <> ? AND AP21_CODHIST = ? ORDER BY AP2100.AP21_CodRef DESC"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(1).Type = rdTypeINTEGER
      rdoQ(0) = nRef
      rdoQ(1) = NH
      Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
      
      While rdo.EOF = False
          posi = posicion(.CurrentY, 100)
          .CurrentX = margenizquierdo
          .CurrentY = posi
          .FontBold = True
          Printer.Print rdo(1) & ". N� " & Right(rdo(0), 8) & " Solicitud: " & rdo(2)
          
          sql = "SELECT AP3700.AP50_CodTiSno||'-'||AP3700.AP37_NumSno||' ', " _
              & "AP37_DESCRIP||'. '||AP39_DESCRIP " _
              & "FROM AP3700,AP3900 WHERE " _
              & "AP3700.AP37_CODSNO = AP3900.AP37_CODSNO AND " _
              & "AP3900.AP21_CODREF= ? "
          Set rdoQ1 = objApp.rdoConnect.CreateQuery("", sql)
          rdoQ1(0).Type = rdTypeVARCHAR
          rdoQ1(0) = rdo(0)
          Set rdo1 = rdoQ1.OpenResultset(rdOpenForwardOnly)
          While rdo1.EOF = False
              .FontBold = False
  '              posi = posicion(Printer.CurrentY, interlinea2)
              .CurrentX = margenizquierdo + 600
  '              .CurrentY = posi
              Call ImprimirConSaltos(rdo1(0) & TipoFrase(rdo1(1)), margenizquierdo + 600, posi)
              rdo1.MoveNext
          Wend
          rdo.MoveNext
      Wend
    End If
    Printer.EndDoc
  End With
'    Call CerrarrdoQueries
End Sub
Sub ImprimirHojaRecepcion1(nRef As String, TiPac As Integer)

Dim sql As String, nRefAnt As String, cMues As Integer
Dim rdoQ As rdoQuery
Dim rdoQ1 As rdoQuery
Dim rdo As rdoResultset
Dim cama As String
Dim rdo1 As rdoResultset
Dim anam As String
Dim i As Integer, j As Integer
Dim posi As Long
Dim NH As Long, NC As Long
Dim Diag As String, res As String
Dim Org As String
Dim Ana() As ResAnalitica
  
  Call ImpresoraDefecto("Recepcion")
  With Printer
    .PaperSize = vbPRPSA4
    .DrawWidth = 5
  
    Printer.Line (margenizquierdo - 100, margensuperior + interlinea1 - 130)-(11000, margensuperior + interlinea1 + interlinea2 * 3 + 250), , B
    Printer.Line (margenizquierdo - 100, margensuperior + interlinea2 * 3 + interlinea1 * 2 - 150)-(11000, margensuperior + interlinea2 * 6 + interlinea1 * 2 + 250), , B
    Printer.Line (margenizquierdo + 4950, margensuperior + interlinea2 * 3 + interlinea1 * 2 - 150)-(margenizquierdo + 4950, margensuperior + interlinea2 * 6 + interlinea1 * 2 + 250)
   
    .FontName = 1
    .FontSize = 15
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = margensuperior
    Printer.Print "Resumen de la Peticion"
    
    If TiPac = 1 Then  ' Si es paciente de CUN
      sql = "SELECT Pac.NH, Asist.NC, Camas.Cama, Pac.AP1||' '||Pac.AP2||', '||Pac.Nom, " _
          & "NVL(DrResp.txtFirma,DrResp.AP1), DptResp.Desig, Pr.Desig, " _
          & "NVL(DrSote.txtFirma,DrSote.AP1), DptSote.Desig, TO_CHAR(AP2100.AP21_FecSotud,'DD/MM/YYYY HH24:MI'), " _
          & "AP08Pat.AP08_Firma, AP08Res.AP08_Firma, AP2100.AP21_Anam, AP2100.AP21_Preg, " _
          & "AP2100.AP21_Observ, AP2100.cPr, AP2100.AP21_TiPr, AP08Cito.AP08_Firma " _
          & "FROM AP2100, Pac, Asist, Pr, Dr DrResp, Dr DrSote, Dpt DptResp, Dpt DptSote, " _
          & "AP0800 AP08Pat, AP0800 AP08Res, AP0800 AP08Cito, Camas WHERE " _
          & "AP2100.AP21_CodHist = Pac.NH AND " _
          & "AP2100.AP21_CodHist = Asist.NH AND " _
          & "AP2100.AP21_CodCaso = Asist.NC AND " _
          & "Asist.NH = Camas.NH (+) AND " _
          & "Asist.NC = Camas.NC (+) AND " _
          & "Asist.cDrResp = DrResp.cDr AND " _
          & "Asist.cDptResp = DptResp.cDpt AND " _
          & "AP2100.AP21_CodDr = DrSote.cDr AND " _
          & "AP2100.cDpt = DptSote.cDpt AND " _
          & "AP2100.cPr = Pr.cPr AND " _
          & "Pr.cdpt = " & cDptAP & " AND " _
          & "AP2100.AP08_CodPers_Pat = AP08Pat.AP08_CodPers AND " _
          & "AP2100.AP08_CodPers_Res = AP08Res.AP08_CodPers (+) AND " _
          & "AP2100.AP08_CodPers_Cito = AP08Cito.AP08_CodPers (+) AND " _
          & "AP2100.AP21_CodRef = ?"
    Else          ' Si es paciente externo
      sql = "SELECT Pac.NH, Asist.NC, Camas.Cama, Pac.AP1||' '||Pac.AP2||', '||Pac.Nom, " _
          & "NVL(DrResp.txtFirma,DrResp.AP1), DptResp.Desig, Pr.Desig, " _
          & "NVL(DrSote.AP15_Firma,DrSote.AP15_AP1), CtroSote.AP11_Desig, TO_CHAR(AP2100.AP21_FecSotud,'DD/MM/YYYY HH24:MI'), " _
          & "AP08Pat.AP08_Firma, AP08Res.AP08_Firma, AP2100.AP21_Anam, AP2100.AP21_Preg, " _
          & "AP2100.AP21_Observ, AP2100.cPr, AP2100.AP21_TiPr, AP08Cito.AP08_Firma " _
          & "FROM AP2100, Pac, Asist, Pr, Dr DrResp, AP1500 DrSote, Dpt DptResp, AP1100 CtroSote, " _
          & "AP0800 AP08Pat, AP0800 AP08Res, AP0800 AP08Cito, Camas WHERE " _
          & "AP2100.AP21_CodHist = Pac.NH AND " _
          & "AP2100.AP21_CodHist = Asist.NH AND " _
          & "AP2100.AP21_CodCaso = Asist.NC AND " _
          & "Asist.NH = Camas.NH (+) AND " _
          & "Asist.NC = Camas.NC (+) AND " _
          & "Asist.cDrResp = DrResp.cDr AND " _
          & "Asist.cDptResp = DptResp.cDpt AND " _
          & "AP2100.AP21_CodDr = DrSote.AP15_CodPat AND " _
          & "AP2100.AP11_CodCtro = CtroSote.AP11_CodCtro AND " _
          & "AP2100.cPr = Pr.cPr AND " _
          & "Pr.cdpt = " & cDptAP & " AND " _
          & "AP2100.AP08_CodPers_Pat = AP08Pat.AP08_CodPers AND " _
          & "AP2100.AP08_CodPers_Res = AP08Res.AP08_CodPers (+) AND " _
          & "AP2100.AP08_CodPers_Cito = AP08Cito.AP08_CodPers (+) AND " _
          & "AP2100.AP21_CodRef = ?"
    End If
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeVARCHAR
    rdoQ(0) = nRef
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    NH = rdo(0)
    NC = rdo(1)
    .FontSize = 12
    posi = margensuperior + interlinea1
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = posi
    Printer.Print "N� Ref: " & Right(nRef, 8)
    .FontSize = 9
    Call ImprimirNegrita(margenizquierdo + 3300, posi, "Historia/Caso: ", NH & "/" & NC)
    If IsNull(rdo(2)) Then cama = "" Else cama = rdo(2)
    Call ImprimirNegrita(margenizquierdo + 7200, posi, "Cama: ", cama)
    posi = posicion(interlinea2, posi)
    Call ImprimirNegrita(margenizquierdo, posi, "Paciente: ", rdo(3))
    posi = posicion(interlinea2, posi)
    Call ImprimirNegrita(margenizquierdo, posi, "Dpto. Responsable: ", rdo(5))
    Call ImprimirNegrita(margenizquierdo + 5400, posi, "Dr. Responsable: ", rdo(4))
    posi = posicion(posi, interlinea2)
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = posi
    Printer.Print "Prueba Solicitada: "
    .CurrentY = posi
    .CurrentX = margenizquierdo + Printer.TextWidth("Prueba Solicitada: ") + 70
    Printer.Print UCase(rdo(6))
    posi = posicion(posi, interlinea1)
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = posi
    Printer.Print "SOLICITUD"
    
    .CurrentX = margenizquierdo + 5000
    .CurrentY = posi
    Printer.Print "REALIZACION"
    .FontSize = 9
    posi = posicion(posi, interlinea2)
    Call ImprimirNegrita(margenizquierdo, posi, "Doctor Solicitante: ", rdo(7))
    If IsNull(rdo(11)) Then res = "" Else res = rdo(11)
    Call ImprimirNegrita(margenizquierdo + 5000, posi, "Macro: ", res)
    posi = posicion(posi, interlinea2)
    If TiPac = 1 Then
      Call ImprimirNegrita(margenizquierdo, posi, "Departamento Solicitante: ", rdo(8))
    Else
      Call ImprimirNegrita(margenizquierdo, posi, "Centro Solicitante: ", rdo(8))
    End If
    Call ImprimirNegrita(margenizquierdo + 5000, margensuperior + interlinea2 * 5 + interlinea1 * 2, "Micro:  ", rdo(10))
    posi = posicion(posi, interlinea2)
    Call ImprimirNegrita(margenizquierdo, margensuperior + interlinea2 * 6 + interlinea1 * 2, "Fecha/Hora Solicitud: ", rdo(9))
    If rdo(16) = 2 And Not IsNull(rdo(17)) Then ' Citolog�as
      Call ImprimirNegrita(margenizquierdo + 5000, margensuperior + interlinea2 * 6 + interlinea1 * 2, "Citot�cnico:  ", rdo(17))
    End If
    
    sql = "SELECT AP04_DESIG FROM AP0400,AP1700 " _
        & "WHERE AP0400.AP04_CODTITEC=AP1700.AP04_CODTITEC AND CPR = ?  "
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(0) = rdo(15)
    Set rdo1 = rdoQ.OpenResultset(rdOpenForwardOnly)
    If rdo1.EOF = False Then
      posi = posicion(posi, interlinea1)
      .FontBold = True
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "T�CNICAS ESPECIALES"
      .FontBold = False
      While rdo1.EOF = False
          i = i + 1
          posi = posicion(interlinea2, posi)
          .CurrentX = margenizquierdo + 300
          .CurrentY = posi
          Printer.Print rdo1(0)
          rdo1.MoveNext
      Wend
    End If
    'Printer.Line (margenizquierdo - 100, posi - i * interlinea2 - 100)-(margenizquierdo + 4000, posi + 250), , B
    
    posi = posicion(posi, interlinea1)
    .FontBold = True
    .CurrentX = margenizquierdo
    .CurrentY = posi
    Printer.Print "MATERIAL A INVESTIGAR"
    .FontBold = False
    
    sql = "SELECT AP2500.PR52NumIDMuestra, AP23_DESIG||'. '||AP25_TiMues, AP25_FECEXTRA, " _
        & "AP25_FECRECEP,AP08_FIRMA, AP2500.AP25_Observ||' Diagn�stico de presunci�n: '||AP1600.AP16_Desig, " _
        & "AP1900.AP19_Desig, AP2200.AP22_Desig, AP2500.AP23_CodOrg " _
        & "FROM AP2300,AP2500,AP0800, AP1600, AP1900, AP2200 WHERE " _
        & "AP2300.AP23_CODORG = AP2500.AP23_CODORG AND " _
        & "AP2500.AP16_CodDiag = AP1600.AP16_CodDiag AND " _
        & "AP2500.AP19_CodFija = AP1900.AP19_CodFija (+) AND " _
        & "AP2500.AP22_CodDecal = AP2200.AP22_CodDecal (+) AND " _
        & "AP2500.AP08_CODPERS = AP0800.AP08_CODPERS (+) " _
        & "AND AP2500.AP21_CODREF = ?  "
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeVARCHAR
    rdoQ(0) = nRef
    Set rdo1 = rdoQ.OpenResultset(rdOpenForwardOnly)
   
    posi = posicion(posi, interlinea2)
    .CurrentX = margenizquierdo + 300
    .CurrentY = posi
    .FontUnderline = True
    Printer.Print "Muestra"
    
    .CurrentX = margenizquierdo + 4000 + 300
    .CurrentY = posi
    .FontUnderline = True
    Printer.Print "Extracci�n"
    
    .CurrentX = margenizquierdo + 6000 + 300
    .CurrentY = posi
    .FontUnderline = True
    Printer.Print "Recepci�n"
    
    .CurrentX = margenizquierdo + 8000 + 300
    .CurrentY = posi
    .FontUnderline = True
    Printer.Print "Recepciona"
    
    .FontUnderline = False
    Org = "("
    While rdo1.EOF = False
        Org = Org & rdo1(8) & ","
        posi = posicion(posi, interlinea2)
        .CurrentX = margenizquierdo + 300
        .CurrentY = posi
        .FontBold = True
        Printer.Print rdo1(0) & ".-" & TipoFrase(rdo1(1))
        
        .FontBold = False
        .CurrentX = margenizquierdo + 4000 + 300
        .CurrentY = posi
        Printer.Print rdo1(2)
        
        .CurrentX = margenizquierdo + 6000 + 300
        .CurrentY = posi
        Printer.Print rdo1(3)
        
        .CurrentX = margenizquierdo + 8000 + 300
        .CurrentY = posi
        Printer.Print rdo1(4)
        
        posi = posicion(posi, interlinea2)
        .CurrentX = margenizquierdo + 300
        .CurrentY = posi
        Call ImprimirConSaltos(TipoFrase(rdo1(5)), margenizquierdo + 300, posi)

        rdo1.MoveNext
    Wend
    Org = Left(Org, Len(Org) - 1) & ")"
    If IsNull(rdo(12)) Then
      anam = ""
    Else
      anam = rdo(12)
      posi = posicion(posi, interlinea1)
      .FontBold = True
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "ANAMNESIS"
      .FontBold = False
      Call ImprimirConSaltos(anam, margenizquierdo, posi)
    End If
    
    If Not IsNull(rdo(13)) Then
      posi = Printer.CurrentY
      .FontBold = True
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "DIAGN�STICO CL�NICO"
      .FontBold = False
      .CurrentX = margenizquierdo
      Printer.Print rdo(13)
    End If
    
    If Not IsNull(rdo(14)) Then
      posi = posicion(posi, interlinea1)
      .FontBold = True
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "OBSERVACIONES"
      .FontBold = False
      .CurrentX = margenizquierdo
      Printer.Print rdo(14)
    End If
    
    sql = "SELECT AP31_DESIG, AP43_DATO FROM AP3100, AP4300 " _
        & "WHERE AP3100.AP23_CODORG = AP4300.AP23_CODORG AND " _
        & "AP3100.AP31_CODDATO = AP4300.AP31_CODDATO AND AP4300.AP21_CODREF = ?  "
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeVARCHAR
    rdoQ(0) = nRef
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    If rdo.EOF = False Then
      posi = posicion(posi, interlinea1)
      .FontBold = True
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "DATOS ADICIONALES"
      .FontBold = False
      While rdo.EOF = False
          posi = posicion(posi, interlinea2)
          .CurrentX = margenizquierdo + 300
          .CurrentY = posi
          Printer.Print rdo(0)
          .CurrentX = margenizquierdo + 2000 + 300
          .CurrentY = posi
          Printer.Print rdo(1)
          rdo.MoveNext
      Wend
    End If
    
    .FontBold = False  ' Anal�tica
    
    If BuscarAnalitica(NH, Org, Ana()) = True Then
      posi = posicion(posi, interlinea2)
      .CurrentX = margenizquierdo + 300
      .CurrentY = posi
      .FontBold = True
      Printer.Print "Analitica"
      .FontBold = False
      For i = 1 To UBound(Ana)
        posi = posicion(posi, interlinea2)
        .CurrentX = margenizquierdo + 300
        .CurrentY = posi
        Printer.Print Ana(i).pr & "  " & Ana(i).fecha
        For j = 1 To UBound(Ana(i).ResPr)
          posi = posicion(posi, interlinea2)
          .CurrentX = margenizquierdo + 600 + 300
          .CurrentY = posi
          Printer.Print Ana(i).ResPr(j).res & ":  " & Ana(i).ResPr(j).Resultado & " " & Ana(i).ResPr(j).Unidades
        Next j
      Next i
    End If
    
    If TiPac = 1 Then
      .FontBold = True
      posi = posicion(posi, interlinea1)
      .CurrentX = margenizquierdo
      .CurrentY = posi
      Printer.Print "BIOPSIAS Y CITOLOG�AS ANTERIORES"
      .FontBold = False
      sql = "SELECT /*+ RULE */ AP2100.AP21_CODREF, Pr.Desig, AP2100.AP21_FecSotud, AP2500.PR52NumIDMuestra, " _
          & "AP2300.AP23_Descrip||'. '||AP2500.AP25_TiMues, AP2500.AP25_FecRecep, " _
          & "AP3700.AP50_CodTiSno||'-'||AP3700.AP37_NumSno||' ', " _
          & "AP37_DESCRIP||'. '||AP39_DESCRIP " _
          & "FROM AP2100, pr, AP2300, AP2500, AP3700, AP3900 WHERE " _
          & "AP2100.AP21_CodRef = AP2500.AP21_CodRef AND " _
          & "AP2500.AP23_CodOrg = AP2300.AP23_CodOrg AND " _
          & "AP2100.cPr = Pr.cPr AND " _
          & "AP2500.AP21_CodRef = AP3900.AP21_CODREF (+) AND " _
          & "AP2500.PR52NumIDMuestra = AP3900.PR52NumIDMuestra (+) AND " _
          & "AP3900.AP37_CODSNO = AP3700.AP37_CODSNO (+) AND " _
          & "Pr.cDpt = " & cDptAP & " AND " _
          & "AP2100.AP21_CODREF <> ? AND " _
          & "AP2100.AP21_CODHIST = ? " _
          & "ORDER BY AP2100.AP21_CodRef DESC, AP2500.PR52NumIDMuestra ASC, AP3700.AP50_CodTiSno"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(1).Type = rdTypeINTEGER
      rdoQ(0) = nRef
      rdoQ(1) = NH
      Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
      nRefAnt = ""
      cMues = 0
      While rdo.EOF = False
        If nRefAnt <> rdo(0) Then
          posi = posicion(.CurrentY, 100)
          .CurrentX = margenizquierdo
          .CurrentY = posi
          .FontBold = True
          Printer.Print rdo(1) & ". N� " & Right(rdo(0), 8) & " Solicitud: " & rdo(2)
          '.FontBold = False
          .CurrentX = margenizquierdo + 300
          Call ImprimirConSaltos(TipoFrase(rdo(3) & ".- " & rdo(4) & " " & rdo(5)), margenizquierdo + 300, posi)
          nRefAnt = rdo(0)
          cMues = rdo(3)
        Else
          If cMues <> rdo(3) Then
            .FontBold = True
            .CurrentX = margenizquierdo + 300
            Call ImprimirConSaltos(TipoFrase(rdo(3) & ".- " & rdo(4) & " " & rdo(5)), margenizquierdo + 300, posi)
            cMues = rdo(3)
          End If
        End If
        .CurrentX = margenizquierdo + 800
        If rdo(6) <> "- " Then
          .FontBold = False
          Call ImprimirConSaltos(rdo(6) & TipoFrase(rdo(7)), margenizquierdo + 800, posi)
        End If
        rdo.MoveNext
      Wend
    End If
    Printer.EndDoc
  End With
'    Call CerrarrdoQueries
End Sub


Function TipoFrase(texto As String) As String
Dim msg As String, frase As String, queda As String

  texto = Trim(texto)
  If Right(texto, 1) <> "." Then texto = texto & "."
  If Left(texto, 1) = "." Then texto = Right(texto, Len(texto) - 1)
  queda = LCase(texto)
  Do
    frase = Trim(Left(queda, InStr(queda, ".")))
    TipoFrase = TipoFrase & UCase(Left(frase, 1)) & Right(frase, Len(frase) - 1) & " "
    queda = Right(queda, Len(queda) - InStr(queda, "."))
  Loop While InStr(queda, ".") <> 0
End Function

Sub ImprimirEtiqueta(nRef As String, MuesRecep As String)
Dim crlf As String
Dim Nom As String, Ap1 As String, Ap2 As String
Dim sql As String, txt As String, txt1 As String
Dim Org As String, TiMues  As String, nPr As Integer
Dim xDots As Integer
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim partir As Integer
    
  On Error Resume Next
  crlf = Chr$(13) & Chr$(10)
  sql = "SELECT AP6000.AP60_DesTiPr, AP2100.CI22NUMHISTORIA, CI2200.CI22PriApel, " _
      & "CI2200.CI22SegApel, CI2200.CI22Nombre, " _
      & "PR2400.PR24DesCorta, PR5200.PR52DesMuestra, AP2500.PR52NUMIDMUESTRA " _
      & "FROM AP2500, AP2100, PR2400, PR0400, CI2200, AP0900, AP6000 WHERE " _
      & "AP2100.PR01CodActuacion = AP0900.PR01CodActuacion AND " _
      & "AP0900.AP60_CodTiPr = AP6000.AP60_CodTiPr AND " _
      & "AP2100.PR04NumActPlan = PR0400.PR04NumActPlan AND " _
      & "PR0400.CI21CodPersona = CI2200.CI21CodPersona AND " _
      & "AP2100.AP21_CodRef = AP2500.AP21_CodRef AND " _
      & "AP2500.PR52NumMuestra = PR5200.PR52NumMuestra AND " _
      & "PR5200.PR24CodMuestra = PR2400.PR24CodMuestra AND " _
      & "AP2100.AP21_CODREF = ?  AND " _
      & "AP2500.PR52NUMIDMUESTRA IN " & MuesRecep

  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)

'preparaci�n de los datos en funci�n de resultados de consulta
  
  nPr = Val(Right$(nRef, 5)) 'casilla 2
  Ap1 = transfCaracteres1(rdo(2))
  Ap2 = transfCaracteres1(rdo(3))
  Nom = transfCaracteres1(rdo(4))
  
  'preparar la variable que sit�a en el margen dcho el nPr
  xDots = 276 + (5 - Len(Trim(CStr(nPr)))) * 36
  txt = crlf
  txt = txt & "N" & crlf
  txt = txt & "I8,1,034" & crlf
  txt = txt & "Q320,24" & crlf
  txt = txt & "R184,0" & crlf
  txt = txt & "S2" & crlf
  txt = txt & "D8" & crlf
  txt = txt & "ZB" & crlf
  txt = txt & "A70,24,0,4,1,1,N," & Chr$(34) & rdo(0) & Chr$(34) & crlf
  txt = txt & "A" & xDots & ",10,0,5,1,1,N," & Chr$(34) & nPr & Chr$(34) & crlf
  txt = txt & "A30,90,0,4,1,1,N," & Chr$(34) & rdo(1) & Chr$(34) & crlf
    
  If ((Len(Ap1) + Len(Ap2) + Len(Nom)) < 25) Then
    txt = txt & "A30,120,0,4,1,1,N," & Chr$(34) & Ap1 _
              & " " & Ap2 & ", " & Nom & Chr$(34) & crlf
    partir = 0
  Else
    txt = txt & "A30,120,0,4,1,1,N," & Chr$(34) & Ap1 & " " & Ap2 & "," & Chr$(34) & crlf
    txt = txt & "A30,150,0,4,1,1,N," & Chr$(34) _
              & Nom & Chr$(34) & crlf
    partir = 30
  End If
  
  While rdo.EOF = False
    Org = transfCaracteres1(rdo(5)) 'casilla 5
    If IsNull(rdo(6)) Then TiMues = "" Else TiMues = transfCaracteres1(rdo(6)) 'casilla 6
    
    txt1 = txt & "A30," & 150 + partir & ",0,4,1,1,N," & Chr$(34) & rdo(7) _
              & " - " & Org & Chr$(34) & crlf
    txt1 = txt1 & romperEnFilas(180 + partir, TiMues)
    
    txt1 = txt1 & "P1" & crlf
    txt1 = txt1 & " " & crlf
        
    Call ImpresoraDefecto("Etiquetas")
    Printer.Print txt1
    Printer.EndDoc
    Call ImpresoraDefecto("Recepcion")
    rdo.MoveNext
  Wend

End Sub
Sub ImprimirEtiqueta2(codPrueba As String, codMues_5 As Integer)
Dim crlf$
Dim sql As String, txt As String, txt1 As String
Dim BioCito_1 As String, AP1_4 As String, AP2_4 As String, Nom_4   As String
Dim Org_5 As String, tiMues_6  As String
Dim numPr_2 As Long
Dim numHist_3 As Long
Dim xDots As Integer
Dim rdoQ As rdoQuery
Dim rdoR As rdoResultset
Dim partir As Integer
    
  On Error Resume Next
  crlf = Chr$(13) & Chr$(10)
  sql = "SELECT AP2100.AP21_TIPR, AP2100.AP21_CODHIST, PAC.AP1, " _
      & "PAC.AP2, PAC.NOM, AP2300.AP23_DESIG, AP2500.AP25_TIMUES, AP2500.PR52NumIDMuestra " _
      & "FROM AP2500, AP2100, AP2300, PAC " _
      & "WHERE " _
      & "AP2500.AP21_CODREF = ?  AND " _
      & "AP2500.PR52NumIDMuestra = ?  AND " _
      & "AP2500.AP23_CODORG = AP2300.AP23_CODORG AND " _
      & "AP2500.AP21_CODREF = AP2100.AP21_CODREF AND " _
      & "AP2100.AP21_CODHIST = PAC.NH"
        
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = codPrueba
  rdoQ(1) = codMues_5
  Set rdoR = rdoQ.OpenResultset(rdOpenForwardOnly)

'preparaci�n de los datos en funci�n de resultados de consulta
'casilla1
  Select Case rdoR(0)
    Case 1
      BioCito_1 = "BIOPSIA"
    Case 2
      BioCito_1 = "CITOLOGIA"
  End Select

  'casilla 2
  numPr_2 = Val(Right$(codPrueba, 5))
  'casilla 3
  numHist_3 = rdoR(1)
  'casilla 4
  AP1_4 = rdoR(2)
  AP2_4 = rdoR(3)
  Nom_4 = rdoR(4)
  'transformar caracteres para impresi�n de caracteres del alfabeto espa�ol
  AP1_4 = transfCaracteres1(AP1_4)
  AP2_4 = transfCaracteres1(AP2_4)
  Nom_4 = transfCaracteres1(Nom_4)
  
  'preparar la variable que sit�a en el margen dcho el numPr_2
  xDots = 276 + (5 - Len(Trim(CStr(numPr_2)))) * 36
  txt = crlf
  txt = txt & "N" & crlf
  txt = txt & "I8,1,034" & crlf
  txt = txt & "Q320,24" & crlf
  txt = txt & "R184,0" & crlf
  txt = txt & "S2" & crlf
  txt = txt & "D8" & crlf
  txt = txt & "ZB" & crlf
  txt = txt & "A70,24,0,4,1,1,N," & Chr$(34) & BioCito_1 & Chr$(34) & crlf
  txt = txt & "A" & xDots & ",10,0,5,1,1,N," & Chr$(34) & numPr_2 & Chr$(34) & crlf
  txt = txt & "A30,90,0,4,1,1,N," & Chr$(34) & numHist_3 & Chr$(34) & crlf
    
  If ((Len(AP1_4) + Len(AP2_4) + Len(Nom_4)) < 25) Then
    txt = txt & "A30,120,0,4,1,1,N," & Chr$(34) & AP1_4 _
              & " " & AP2_4 & ", " & Nom_4 & Chr$(34) & crlf
    partir = 0
'    txt = txt & "A30,150,0,4,1,1,N," & Chr$(34) & codMues_5 _
'              & " - " & Org_5 & Chr$(34) & crlf
'    txt = txt & romperEnFilas(180, tiMues_6)
  Else
    txt = txt & "A30,120,0,4,1,1,N," & Chr$(34) & AP1_4 & " " & AP2_4 & "," & Chr$(34) & crlf
    txt = txt & "A30,150,0,4,1,1,N," & Chr$(34) _
              & Nom_4 & Chr$(34) & crlf
    partir = 30
'    txt = txt & "A30,180,0,4,1,1,N," & Chr$(34) & codMues_5 _
'              & " - " & Org_5 & Chr$(34) & crlf
'    txt = txt & romperEnFilas(210, tiMues_6)
  End If
  
  While rdoR.EOF = False
    'casilla 5
    Org_5 = rdoR(5)
    'casilla 6
    If IsNull(rdoR(6)) Then
      tiMues_6 = ""
    Else
      tiMues_6 = rdoR(6)
    End If
    
    Org_5 = transfCaracteres1(Org_5)
    tiMues_6 = transfCaracteres1(tiMues_6)
    
    txt1 = txt & "A30," & 150 + partir & ",0,4,1,1,N," & Chr$(34) & rdoR(7) _
              & " - " & Org_5 & Chr$(34) & crlf
    txt1 = txt1 & romperEnFilas(180 + partir, tiMues_6)
    
    txt1 = txt1 & "P1" & crlf
    txt1 = txt1 & " " & crlf
        
  Call ImpresoraDefecto("Etiquetas")
    Printer.Print txt1
    Printer.EndDoc
  Call ImpresoraDefecto("Recepcion")
    rdoR.MoveNext
  Wend

End Sub
Sub ImpresoraDefecto(Nombre As String)
Dim X As Printer

For Each X In Printers
  If InStr(UCase(X.DeviceName), UCase(Nombre)) > 0 Then
    ' La define como predeterminada del sistema.
    Set Printer = X
    ' Sale del bucle.
    Exit For
  End If
Next
End Sub

Function transfCaracteres(palabra As String) As String

Dim i As Integer

For i = 1 To Len(palabra)
Select Case Mid(palabra, i, 1)
    Case "�"
        Mid(palabra, i, 1) = Chr$(96)
    Case "�"
        Mid(palabra, i, 1) = Chr$(123)
    Case "�"
        Mid(palabra, i, 1) = Chr$(124)
    Case "�"
        Mid(palabra, i, 1) = Chr$(125)
    Case "�"
        Mid(palabra, i, 1) = Chr$(126)
    Case "�"
        Mid(palabra, i, 1) = Chr$(93)
    Case "�"
        Mid(palabra, i, 1) = Chr$(92)
    Case "�"
        Mid(palabra, i, 1) = Chr$(91)
    Case "�"
        Mid(palabra, i, 1) = Chr$(94)

End Select
Next

transfCaracteres = palabra

End Function
Function transfCaracteres1(palabra As String) As String
Dim i As Integer

  For i = 1 To Len(palabra)
    Select Case Mid(palabra, i, 1)
      Case "�"
        Mid(palabra, i, 1) = Chr$(160)
      Case "�"
        Mid(palabra, i, 1) = Chr$(130)
      Case "�"
        Mid(palabra, i, 1) = Chr$(161)
      Case "�"
        Mid(palabra, i, 1) = Chr$(162)
      Case "�"
        Mid(palabra, i, 1) = Chr$(163)
      Case "�"
        Mid(palabra, i, 1) = Chr$(168)
      Case "�"
        Mid(palabra, i, 1) = Chr$(164)
      Case "�"
        Mid(palabra, i, 1) = Chr$(165)
      Case "�"
        Mid(palabra, i, 1) = Chr$(129)
      Case "�"
        Mid(palabra, i, 1) = Chr$(166)
      Case "�"
        Mid(palabra, i, 1) = Chr$(167)
    End Select
  Next

  transfCaracteres1 = palabra

End Function

Function romperEnFilas(Y As Integer, palabra As String) As String
Dim iAbsoluta As Integer, iLocal As Integer, posUltimoEspacio As Integer
Dim linea As String, inicioLinea As Integer, finLinea As Integer
Dim ciclo As Integer
Dim crlf$
crlf = Chr$(13) & Chr$(10)
ciclo = 0
iLocal = 0
inicioLinea = 1
posUltimoEspacio = 0
romperEnFilas = ""

If Len(palabra) < 28 Then
    romperEnFilas = romperEnFilas & "A30," & Y + 30 * (ciclo) & ",0,4,1,1,N,"
    romperEnFilas = romperEnFilas & Chr$(34) & palabra & Chr$(34) & crlf
Else

    For iAbsoluta = 1 To Len(palabra)
        iLocal = iLocal + 1
        If Mid(palabra, iAbsoluta, 1) = " " Then
            posUltimoEspacio = iAbsoluta
        End If

        If iLocal > 27 Then
            finLinea = posUltimoEspacio - 1
            linea = Mid(palabra, inicioLinea, posUltimoEspacio - inicioLinea)
            romperEnFilas = romperEnFilas & "A30," & Y + 30 * (ciclo) & ",0,4,1,1,N,"
            romperEnFilas = romperEnFilas & Chr$(34) & linea & Chr$(34) & crlf
            iLocal = iAbsoluta - finLinea
            inicioLinea = finLinea + 2
            ciclo = ciclo + 1
        End If
    Next
    linea = Mid(palabra, inicioLinea, iAbsoluta - inicioLinea)
            romperEnFilas = romperEnFilas & "A30," & Y + 30 * (ciclo) & ",0,4,1,1,N,"
            romperEnFilas = romperEnFilas & Chr$(34) & linea & Chr$(34) & crlf
    

End If
 
    

End Function

Public Sub ImprimirHoja(strNumRef As String)

Dim intY As Integer 'Valor de la cota Y de inicio del rect�ngulo
Dim blnPrimero As Boolean  'Si es el primer responsable de la solicitud: TRUE
'Consulta
Dim sql As String
Dim qryConsulta As rdoQuery
Dim rstConsulta As rdoResultset
'Tablas
Dim strFormato As String 'Formato de la tabla
Dim strEncabezamiento As String 'Encabezado de la tabla
Dim strDatos As String 'Datos de la tabla

Dim strFecGen As String 'Fecha de generaci�n

  Screen.MousePointer = vbHourglass

  With frmPrincipal.vsPrint
    .Device = "" 'Impresora predeterminada de Windows
    .PaperSize = 9 'A4
    .StartDoc 'Comienza el documento
    'T�tulo
    .FontSize = 15
    .FontBold = True
    strFormato = "<+5250|>+4500|<+750"
    strEncabezamiento = "Resumen de la petici�n|" & strNumRef & "|"
    strDatos = ""
    .AddTable strFormato, strEncabezamiento, strDatos
    .FontSize = 9
    .FontBold = False
    intY = .CurrentY - 275

'!!!!!!!!!!!!!!!!!!!!!!!!
'Encabezados

   'SELECT...
    sql = "SELECT AP2100.CI22NUMHISTORIA, PR0400.AD01CODASISTENCI, AD15CODCAMA, "
    sql = sql & "CI2200.CI22NOMBRE, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, " '...
    sql = sql & "CI2200FN.CI22FECNACIM, " 'Paciente
   'Responsable
    sql = sql & "AD0200.AD02DESDPTO, "
    sql = sql & "NVL(SG0200.SG02TXTFIRMA, SG0200.SG02APE1||' '||SG0200.SG02APE2||', '||SG0200.SG02NOM), "
    sql = sql & "PR01DESCORTA, " 'Prueba solicitada
    sql = sql & "AD0200SL.AD02DESDPTO, " '...
    sql = sql & "NVL(SG0200SL.SG02TXTFIRMA, SG0200SL.SG02APE1||' '||SG0200SL.SG02APE2||', '||SG0200SL.SG02NOM), "
    sql = sql & "AP21_FECSOTUD, " 'Solicitud
   'Realizaci�n
    sql = sql & "NVL(SG0200RLMA.SG02TXTFIRMA, SG0200RLMA.SG02APE1||' '||SG0200RLMA.SG02APE2||', '||SG0200RLMA.SG02NOM), " 'Macro
    sql = sql & "NVL(SG0200RLMI.SG02TXTFIRMA, SG0200RLMI.SG02APE1||' '||SG0200RLMI.SG02APE2||', '||SG0200RLMI.SG02NOM), " 'Micro
    sql = sql & "NVL(SG0200RLCI.SG02TXTFIRMA, SG0200RLCI.SG02APE1||' '||SG0200RLCI.SG02APE2||', '||SG0200RLCI.SG02NOM), " 'Citot.
    sql = sql & "AP21_FECGEN "
   'FROM...
    sql = sql & "FROM AP2100, PR0100, PR0400, AD1500, CI2200, CI2200 CI2200FN, "
    sql = sql & "AD0500, AD0200, SG0200, PR0800, PR0300, AD0200 AD0200SL, SG0200 SG0200SL, "
    sql = sql & "SG0200 SG0200RLMA, SG0200 SG0200RLMI, SG0200 SG0200RLCI "
   'WHERE...
    sql = sql & "WHERE AP2100.AP21_CODREF = ? " 'C�digo de referencia interno
    sql = sql & "AND AP2100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN "
    sql = sql & "AND PR0400.AD01CODASISTENCI = AD0500.AD01CODASISTENCI "
    sql = sql & "AND PR0400.AD07CODProceso = AD0500.AD07CODProceso "
    sql = sql & "AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI (+) "
    sql = sql & "AND PR0400.AD07CODProceso = AD1500.AD07CODProceso (+) "
    sql = sql & "AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA "
    sql = sql & "AND AP2100.CI22NUMHISTORIA = CI2200FN.CI22NUMHISTORIA "
    sql = sql & "AND AP2100.PR01CODACTUACION = PR0100.PR01CODACTUACION "
    sql = sql & "AND AD0500.AD02CODDPTO = AD0200.AD02CODDPTO "
    sql = sql & "AND AD0500.SG02COD = SG0200.SG02COD "
    sql = sql & "AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI "
    sql = sql & "AND PR0300.PR09NUMPETICION = PR0800.PR09NUMPETICION "
    sql = sql & "AND AP2100.AD02CODDPTO = AD0200SL.AD02CODDPTO "
    sql = sql & "AND AP2100.SG02COD_SOTE = SG0200SL.SG02COD "
    sql = sql & "AND AP2100.SG02COD_RES = SG0200RLMA.SG02COD(+) "
    sql = sql & "AND AP2100.SG02COD_PAT = SG0200RLMI.SG02COD "
    sql = sql & "AND AP2100.SG02COD_CITO = SG0200RLCI.SG02COD(+)"

    Set qryConsulta = objApp.rdoConnect.CreateQuery("", sql)
    qryConsulta(0) = strNumRef 'N�mero de referencia interno
    Set rstConsulta = qryConsulta.OpenResultset(rdOpenKeyset)
    
    'Primer encabezado
    
    '<Izquierda(horizontal), +Centrado(vertical)
    'Anchura de la celda en Twips, |Separador de columnas
    '{\b *****} negrita en formato RTF, |Separador de columnas
    strFormato = "<+4875|<+4875"
    strEncabezamiento = "{\b Historia/Asistencia: }" & rstConsulta(0) & "/" & _
                            rstConsulta(1) & "|{\b Cama: }"
    If Not IsNull(rstConsulta(2)) Then strEncabezamiento = strEncabezamiento _
                                                & rstConsulta(2)
    
    strDatos = "{\b Paciente: }" & rstConsulta(4) & " " & rstConsulta(5) & _
                ", " & rstConsulta(3) & " |{\b Fecha de nacimiento: }" & rstConsulta(6)

    Do While Not rstConsulta.EOF 'Mientras halla responsables...
      strDatos = strDatos & ";{\b Dpto. Responsable: }" & rstConsulta(7) & _
              "|{\b Dr. Responsable: }" & rstConsulta(8)
      rstConsulta.MoveNext
    Loop
    
    rstConsulta.MoveFirst
    strDatos = strDatos & ";{\b Prueba Solicitada: " & rstConsulta(9) & "}| "
    
    .AddTable strFormato, strEncabezamiento, strDatos 'Se a�ade la tabla
    .DrawRectangle 750, intY, 10500, (.CurrentY - 200)
    intY = .CurrentY - 100
    .Paragraph = ""

'Segundo encabezado

    '<Izquierda(horizontal), +Centrado(vertical)
    'Anchura de la celda en Twips, |Separador de columnas
    strFormato = "<+3250|<+3250|<+3250"
    strEncabezamiento = "{\b SOLICITUD}||"

    blnPrimero = True 'Primer resultado
    Do While Not rstConsulta.EOF
      If blnPrimero = True Then
        blnPrimero = False
        strDatos = "{\b Doctor: }" & rstConsulta(11) & "|{\b Departamento: }" & _
                        rstConsulta(10) & "|{\b Fecha/Hora: }" & rstConsulta(12)
      Else
        strDatos = strDatos & ";{\b Doctor: }" & rstConsulta(11) & _
                "|{\b Departamento: }" & rstConsulta(10) & "|{\b Fecha/Hora: }" & _
                rstConsulta(12)
      End If
      rstConsulta.MoveNext
    Loop
    
    .AddTable strFormato, strEncabezamiento, strDatos
    .DrawRectangle 750, intY, 10500, (.CurrentY - 200)
    intY = .CurrentY - 100
    .Paragraph = ""

'Tercer encabezado

    '<Izquierda(horizontal), +Centrado(vertical)
    'Anchura de la celda en Twips, |Separador de columnas
    strFormato = "<+3250|<+3250|<+3250"
    strEncabezamiento = "{\b REALIZACI�N}||"
    rstConsulta.MoveFirst
    strDatos = "{\b Macro: }"
    If rstConsulta(13) <> " , " Then strDatos = strDatos & rstConsulta(13)
    strDatos = strDatos & "|{\b Micro: }" & rstConsulta(14) & "|{\b Citot�cnico: }"
    If rstConsulta(15) <> " , " Then strDatos = strDatos & rstConsulta(15)

    .AddTable strFormato, strEncabezamiento, strDatos
    .DrawRectangle 750, intY, 10500, (.CurrentY - 200)
    intY = .CurrentY - 100
    .Paragraph = ""
        
    strFecGen = rstConsulta(16)
        
'!!!!!!!!!!!!!!!!!!!!!!!!
'Datos puros y duros...

    sql = "SELECT PR09DESOBSERVAC, PR09DESINDICACIO, PR03MOTPETCION, PR03DESINDICAC, "
    sql = sql & "PR03DESOBSERVAC, PR40DESPREGUNTA, PR41RESPUESTA "
    sql = sql & "FROM AP2100, PR0400, PR0300, PR0900, PR4000, PR4100 "
    sql = sql & "WHERE AP21_CODREF = ? "
    sql = sql & "AND AP2100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN "
    sql = sql & "AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI "
    sql = sql & "AND PR0300.PR09NUMPETICION = PR0900.PR09NUMPETICION "
    sql = sql & "AND PR0400.PR03NUMACTPEDI = PR4100.PR03NUMACTPEDI(+) "
    sql = sql & "AND PR4100.PR40CODPREGUNTA = PR4000.PR40CODPREGUNTA(+)"

    Set qryConsulta = objApp.rdoConnect.CreateQuery("", sql)
    qryConsulta(0) = strNumRef 'N�mero de referencia interno
    Set rstConsulta = qryConsulta.OpenResultset(rdOpenKeyset)

    If rstConsulta.EOF = False Then

    'Observaciones e indicaciones a la petici�n (1 o varias)
        'Si hay observs. e inds. a la petici�n...
      If Not IsNull(rstConsulta(0)) Or Not IsNull(rstConsulta(1)) Then
        .FontBold = True
        .Paragraph = "PETICION"
        .FontBold = False
        .Paragraph = ""

        '<Izquierda(horizontal), +Centrado(vertical)
        'Anchura de la celda en Twips, |Separador de columnas
        strFormato = "<+5250|<+5250"
        If rstConsulta.RowCount > 1 Then
          strEncabezamiento = "{\ul Observaciones}|{\ul Indicaciones}"
        Else
          strEncabezamiento = "{\ul Observaci�n}|{\ul Indicaci�n}"
        End If
        strDatos = ""
        .LineSpacing = 150
        .AddTable strFormato, strEncabezamiento, strDatos
        .LineSpacing = 100
        strEncabezamiento = ""
        blnPrimero = True 'Primer resultado
        Do While Not rstConsulta.EOF
          If blnPrimero = True Then
            blnPrimero = False
            strDatos = rstConsulta(0) & "|" & rstConsulta(1)
          Else
            strDatos = strDatos & ";" & rstConsulta(0) & "|" & rstConsulta(1)
          End If
          rstConsulta.MoveNext
        Loop
        .AddTable strFormato, strEncabezamiento, strDatos
        .Paragraph = ""
      End If

'Motivo petici�n e indicaciones y observaciones a la prueba
      rstConsulta.MoveFirst
      If Not IsNull(rstConsulta(2)) Or Not IsNull(rstConsulta(3)) Or _
              Not IsNull(rstConsulta(4)) Then
        .FontBold = True
        .Paragraph = "PRUEBA"
        .FontBold = False
        .Paragraph = ""
        
        '<Izquierda(horizontal), +Centrado(vertical)
        'Anchura de la celda en Twips, |Separador de columnas
        strFormato = "<+3250|<3250|<3250"
        strEncabezamiento = "{\ul Motivo}|{\ul Descripci�n}|{\ul Observaci�n}"
        strDatos = ""
        .LineSpacing = 150
        .AddTable strFormato, strEncabezamiento, strDatos
        .LineSpacing = 100
        strEncabezamiento = ""
        blnPrimero = True 'Primer resultado
        Do While Not rstConsulta.EOF
            If blnPrimero = True Then
                blnPrimero = False
                strDatos = rstConsulta(2) & "|" & rstConsulta(3) & "|" & rstConsulta(4)
            Else
                strDatos = strDatos & ";" & rstConsulta(2) & "|" & rstConsulta(3) & "|" & rstConsulta(4)
            End If
            rstConsulta.MoveNext
        Loop
        .AddTable strFormato, strEncabezamiento, strDatos
        .Paragraph = ""
      End If
    
'Resultados del cuestionario de la petici�n de la prueba
      rstConsulta.MoveFirst
        Do While Not rstConsulta.EOF
          If Not IsNull(rstConsulta(6)) Then
            strDatos = rstConsulta(5) & ": " & rstConsulta(6)
            .CurrentX = 1150
            .Paragraph = strDatos 'Se pone el cuestionario
            .CurrentX = 750
          End If
          rstConsulta.MoveNext
        Loop
    End If
    
''    'Datos de las muestras y cuestionario asociado a la extracci�n de la muestra
    sql = "SELECT AP2500.PR52NUMIDMUESTRA, PR24DESCORTA, PR52DESMUESTRA, "
    sql = sql & "NVL(SG0200EX.SG02TXTFIRMA, SG0200EX.SG02APE1||' '||SG0200EX.SG02APE2||', '||SG0200EX.SG02NOM), "
    sql = sql & "PR52FECEXTRAC, "
    sql = sql & "NVL(SG0200RC.SG02TXTFIRMA, SG0200RC.SG02APE1||' '||SG0200RC.SG02APE2||', '||SG0200RC.SG02NOM), "
    sql = sql & "AP25_FECRECEP, PR4000MP.PR40DESPREGUNTA , PR43RESPUESTA, "
    sql = sql & "PR4000MU.PR40DESPREGUNTA, PR54RESPUESTA "
    sql = sql & "FROM AP2500, PR2400, SG0200 SG0200EX, PR5200, SG0200 SG0200RC, "
    sql = sql & "PR4000 PR4000MP, PR4300, PR4000 PR4000MU, PR5400 "
    sql = sql & "WHERE AP21_CODREF = ? "
    sql = sql & "AND AP2500.PR52NUMMUESTRA = PR5200.PR52NUMMUESTRA "
    sql = sql & "AND PR5200.PR24CODMUESTRA = PR2400.PR24CODMUESTRA "
    sql = sql & "AND PR5200.SG02COD = SG0200EX.SG02COD "
    sql = sql & "AND AP2500.SG02COD = SG0200RC.SG02COD(+) "
    sql = sql & "AND AP2500.PR52NUMMUESTRA = PR4300.PR52NUMMUESTRA(+) "
    sql = sql & "AND PR4300.PR40CODPREGUNTA = PR4000MP.PR40CODPREGUNTA(+) "
    sql = sql & "AND AP2500.PR52NUMMUESTRA = PR5400.PR52NUMMUESTRA(+) "
    sql = sql & "AND PR5400.PR40CODPREGUNTA = PR4000MU.PR40CODPREGUNTA(+) "
    sql = sql & "ORDER BY AP2500.PR52NUMIDMUESTRA"

    Set qryConsulta = objApp.rdoConnect.CreateQuery("", sql)
    qryConsulta(0) = strNumRef 'N�mero de referencia interno
    Set rstConsulta = qryConsulta.OpenResultset(rdOpenKeyset)
    
    .FontBold = True
    .Paragraph = "MATERIAL A INVESTIGAR"
    .FontBold = False
    .Paragraph = ""
    '<Izquierda(horizontal), +Centrado(vertical)
    'Anchura de la celda en Twips, |Separador de columnas
    strFormato = "<+5250|<+2250|<+2250"
    If rstConsulta.RowCount = 1 Then
      strEncabezamiento = "{\ul Muestra}|{\ul Extracci�n}|{\ul Recepci�n}"
    Else
      strEncabezamiento = "{\ul Muestras}|{\ul Extracci�n}|{\ul Recepci�n}"
    End If
    strDatos = ""
    .LineSpacing = 150
    .AddTable strFormato, strEncabezamiento, strDatos 'Se a�aden los datos
    .LineSpacing = 100
    blnPrimero = True
    Do While Not rstConsulta.EOF 'Mientras halla registros...
      If sql <> rstConsulta(0) Then 'Si ha cambiado el n�mero de muestra...
        sql = rstConsulta(0) 'Se actualiza la variable para la comprobaci�n
        strEncabezamiento = "{\b " & rstConsulta(0) & ".-" & rstConsulta(1) & ". }" & _
            rstConsulta(2) & "|" & rstConsulta(3) & " (" & rstConsulta(4) & ")" & "|" & _
            rstConsulta(5) & " (" & rstConsulta(6) & ")"
        .AddTable strFormato, strEncabezamiento, strDatos 'Se a�aden los datos
      End If
      If Not IsNull(rstConsulta(8)) Then
        strDatos = rstConsulta(7) & ": " & rstConsulta(8)
        .CurrentX = 1150
        .Paragraph = strDatos 'Se pone el cuestionario
        .CurrentX = 750
      End If
      If Not IsNull(rstConsulta(10)) Then
        strDatos = rstConsulta(9) & ": " & rstConsulta(10)
        .CurrentX = 1150
        .Paragraph = strDatos 'Se pone el cuestionario
        .CurrentX = 750
      End If
      rstConsulta.MoveNext
'        rstConsultaAux.MoveNext
    Loop
    
    .Paragraph = ""

    'Hist�rico de muestras

    sql = "SELECT PR01DESCORTA, AP2100.AP21_CODREF, AP21_FECGEN, "
    sql = sql & "AP2500.PR52NUMIDMUESTRA, PR24DESCORTA, PR52DESMUESTRA, PR52FECEXTRAC, "
    sql = sql & "AP50_CODTISNO, AP37_NUMSNO , AP37_DESCRIP, AP39_OBSERV "
    sql = sql & "FROM PR0100, AP2100, AP2500, PR2400, PR5200, AP3700, AP3900 "
    sql = sql & "WHERE CI22NUMHISTORIA IN ("
        sql = sql & "SELECT CI22NUMHISTORIA "
        sql = sql & "FROM AP2100 "
        sql = sql & "WHERE AP21_CODREF = ? )"
    sql = sql & "AND AP2100.PR01CODACTUACION = PR0100.PR01CODACTUACION "
    sql = sql & "AND AP2100.AP21_CODREF = AP2500.AP21_CODREF "
    sql = sql & "AND AP2500.PR52NUMMUESTRA = PR5200.PR52NUMMUESTRA "
    sql = sql & "AND PR5200.PR24CODMUESTRA = PR2400.PR24CODMUESTRA "
    sql = sql & "AND AP2100.AP21_CODREF = AP3900.AP21_CODREF(+) "
    sql = sql & "AND AP3900.AP37_CODSNO = AP3700.AP37_CODSNO(+) "
    sql = sql & "AND AP2100.AP21_CodRef <> ? "
    sql = sql & "ORDER BY AP21_FECGEN, PR52NUMIDMUESTRA"

    Set qryConsulta = objApp.rdoConnect.CreateQuery("", sql)
    qryConsulta(0) = strNumRef 'N�mero de referencia interno
    qryConsulta(1) = strNumRef
    Set rstConsulta = qryConsulta.OpenResultset(rdOpenForwardOnly)
    If rstConsulta.EOF = False Then
      .FontBold = True
      .Paragraph = "BIOPSIAS Y CITOLOG�AS ANTERIORES"
      .FontBold = False
      .Paragraph = ""
      Do While Not rstConsulta.EOF
        If sql <> rstConsulta(1) Then
          sql = rstConsulta(1)
          .FontBold = True
          If .CurrentX <> 750 Then .CurrentX = 750
          .Paragraph = rstConsulta(0) & " . N� " & rstConsulta(1) & " Solicitud: " & _
                              rstConsulta(2)
          .FontBold = False
        End If
        If strFormato <> rstConsulta(3) Then
          strFormato = rstConsulta(3)
          .FontBold = True
          If .CurrentX <> 1000 Then .CurrentX = 1000
          .Paragraph = rstConsulta(3) & ".-" & rstConsulta(4) & ". " & rstConsulta(5) & _
                              " " & rstConsulta(6)
          .FontBold = False
        End If
        If strDatos <> (rstConsulta(7) & "-" & rstConsulta(8)) And Not IsNull(rstConsulta(7)) Then
          strDatos = rstConsulta(7) & "-" & rstConsulta(8)
          If .CurrentX <> 1250 Then .CurrentX = 1250
          If Not IsNull(rstConsulta(10)) Then
              .Paragraph = strDatos & " " & rstConsulta(9) & ", " & rstConsulta(10)
          Else
              .Paragraph = strDatos & " " & rstConsulta(9)
          End If
        End If
        rstConsulta.MoveNext
      Loop
    End If
    .EndDoc 'Finaliza el documento
    
    'Se cierra la consulta
    qryConsulta.Close
    If Not rstConsulta.ActiveConnection Is Nothing Then rstConsulta.Close

    Screen.MousePointer = vbDefault
  End With

End Sub
