Attribute VB_Name = "modCodeWizard"
Option Explicit

Public objCW As Object      ' referencia al objeto CodeWizard
Public objApp As Object
Public objPipe As Object
Public objGen As Object
Public objEnv As Object
Public objError As Object
Public objMouse As Object
Public objSecurity As Object
Public Sub InitApp()
  On Error GoTo InitError
  Set objCW = CreateObject("CodeWizard.clsCW")
  Set objApp = objCW.objApp
  Set objPipe = objCW.objPipe
  Set objGen = objCW.objGen
  Set objEnv = objCW.objEnv
  Set objError = objCW.objError
  Set objSecurity = objCW.objSecurity
  Exit Sub
InitError:
  Call MsgBox("Error durante la conexi�n con el servidor de CodeWizard", vbCritical)
  Call ExitApp
End Sub

Public Sub ExitApp()
  Set objCW = Nothing
  'End 'DLL
End Sub

