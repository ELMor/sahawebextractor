VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmDatos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Introducción de observaciones"
   ClientHeight    =   6870
   ClientLeft      =   15
   ClientTop       =   300
   ClientWidth     =   9960
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6870
   ScaleWidth      =   9960
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkEstado 
      Caption         =   "Anular"
      Height          =   240
      Left            =   6900
      TabIndex        =   22
      Top             =   1800
      Visible         =   0   'False
      Width           =   1440
   End
   Begin VB.Frame fraFoto 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   690
      Left            =   300
      TabIndex        =   20
      Top             =   3150
      Visible         =   0   'False
      Width           =   9540
      Begin VB.TextBox txtCarrete 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   6375
         MultiLine       =   -1  'True
         TabIndex        =   24
         Top             =   300
         Width           =   1590
      End
      Begin VB.TextBox txtPos 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   8025
         MultiLine       =   -1  'True
         TabIndex        =   23
         Top             =   300
         Width           =   1365
      End
      Begin VB.TextBox txtLugar 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   0
         MultiLine       =   -1  'True
         TabIndex        =   1
         Top             =   300
         Width           =   6315
      End
      Begin VB.Label Label5 
         Caption         =   "Carrete:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   165
         Left            =   6375
         TabIndex        =   26
         Top             =   75
         Width           =   1515
      End
      Begin VB.Label Label4 
         Caption         =   "Posición:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   165
         Left            =   8025
         TabIndex        =   25
         Top             =   75
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Lugar de la fotografía:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   0
         TabIndex        =   21
         Top             =   0
         Width           =   1965
      End
   End
   Begin VB.CheckBox chkProblemas 
      Caption         =   "Informado"
      Height          =   240
      Left            =   6900
      TabIndex        =   19
      Top             =   1425
      Visible         =   0   'False
      Width           =   1440
   End
   Begin VB.Frame fraObservaciones 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   990
      Left            =   300
      TabIndex        =   12
      Top             =   2175
      Width           =   9540
      Begin VB.TextBox txtObservaciones 
         Height          =   540
         Left            =   0
         MultiLine       =   -1  'True
         TabIndex        =   0
         Top             =   300
         Width           =   9390
      End
      Begin VB.Label lblObservaciones 
         Caption         =   "Observaciones:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   0
         TabIndex        =   13
         Top             =   0
         Width           =   1590
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Identificación"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1890
      Index           =   0
      Left            =   300
      TabIndex        =   5
      Top             =   150
      Width           =   6090
      Begin VB.TextBox txtTecnica 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1125
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   1275
         Width           =   4665
      End
      Begin VB.TextBox txtBloque 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1125
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   825
         Width           =   4665
      End
      Begin VB.TextBox txtMuestra 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1125
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   375
         Width           =   4665
      End
      Begin VB.Label Label2 
         Caption         =   "Técnica:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   3
         Left            =   225
         TabIndex        =   11
         Top             =   1350
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "Bloque:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   375
         TabIndex        =   10
         Top             =   900
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "Muestra:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   300
         TabIndex        =   9
         Top             =   450
         Width           =   765
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   390
      Left            =   8475
      TabIndex        =   3
      Top             =   225
      Width           =   1215
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Salir"
      Height          =   390
      Left            =   8475
      TabIndex        =   4
      Top             =   750
      Width           =   1215
   End
   Begin ComctlLib.TreeView tvwDatos 
      Height          =   4515
      Left            =   300
      TabIndex        =   2
      Top             =   2175
      Width           =   6315
      _ExtentX        =   11139
      _ExtentY        =   7964
      _Version        =   327682
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.Frame fraAlmacen 
      Caption         =   "Lugar"
      ForeColor       =   &H00800000&
      Height          =   1815
      Left            =   6750
      TabIndex        =   14
      Top             =   2100
      Visible         =   0   'False
      Width           =   1740
      Begin VB.TextBox txtAlmacen 
         Height          =   315
         Index           =   1
         Left            =   150
         MaxLength       =   10
         MultiLine       =   -1  'True
         TabIndex        =   17
         Top             =   1350
         Width           =   1365
      End
      Begin VB.TextBox txtAlmacen 
         Height          =   315
         Index           =   0
         Left            =   150
         MaxLength       =   10
         MultiLine       =   -1  'True
         TabIndex        =   15
         Top             =   600
         Width           =   1365
      End
      Begin VB.Label Label1 
         Caption         =   "Hueco:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   150
         TabIndex        =   18
         Top             =   1050
         Width           =   690
      End
      Begin VB.Label Label1 
         Caption         =   "Caja:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   150
         TabIndex        =   16
         Top             =   300
         Width           =   840
      End
   End
End
Attribute VB_Name = "frmDatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Tipo As String, cMuestra As Long, cBloque As Long, cTecnica As Long
Dim boton As String
Dim NodoPeticion As Node

Function Dato(num As Long) As String
  If num <> 0 Then
    Dato = num
  Else
    Dato = ""
  End If
End Function

Sub FormatProblemas()
  Me.Height = 7185
  fraObservaciones.Visible = True
  fraObservaciones.Top = 5900
  tvwDatos.Visible = True
  tvwDatos.Height = 3690
  chkProblemas.Visible = True
  chkEstado.Visible = True
  Set tvwDatos.ImageList = frmPrincipal.imgIcos
End Sub

Private Sub cmdAceptar_Click()
Dim sql As String, nRef As String
Dim rdo As rdoResultset
Dim nodo As Node
Dim res As Integer
    
  nRef = objPipe.PipeGet("NumRef")
  Select Case boton
    Case "Observaciones"
      res = IntroducirObservaciones(nRef)
    Case "Envio"
      Set nodo = tvwDatos.SelectedItem
      res = IntroducirEnvio(nRef, nodo)
    Case "Interes"
      Set nodo = tvwDatos.SelectedItem
      res = IntroducirInteres(nRef, nodo)
    Case "Almacen_Muestra"
      Set nodo = tvwDatos.SelectedItem
      res = IntroducirAlmacenMuestra(nRef, nodo)
    Case "Almacen_Bloque"
      Set nodo = tvwDatos.SelectedItem
      res = IntroducirAlmacenBloque(nRef, nodo)
    Case "Problemas"
      Set nodo = tvwDatos.SelectedItem
      res = IntroducirProblemas(nRef, nodo)
    Case "Fotos"
      res = IntroducirFotos(nRef)
  End Select
  
'  Set frmP_Peticion.tvwMuestra.SelectedItem = Nothing
  If res = 1 Then Unload Me
End Sub
Function IntroducirObservaciones(nRef As String) As Integer
Dim sql As String
Dim rdoQ As rdoQuery

  Select Case Tipo
    Case "M"
      sql = "UPDATE AP2500 SET AP25_Observ = ? WHERE AP21_CodRef = ? AND PR52NumIDMuestra = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(1).Type = rdTypeVARCHAR
      rdoQ(2).Type = rdTypeINTEGER
      rdoQ(0) = txtObservaciones.Text
      rdoQ(1) = nRef
      rdoQ(2) = cMuestra
    Case "B"
      sql = "UPDATE AP3400 SET AP34_Observ = ? WHERE " _
          & "AP21_CodRef = ? AND PR52NumIDMuestra = ? AND AP34_CodBloq = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(1).Type = rdTypeVARCHAR
      rdoQ(2).Type = rdTypeINTEGER
      rdoQ(3).Type = rdTypeINTEGER
      rdoQ(0) = txtObservaciones.Text
      rdoQ(1) = nRef
      rdoQ(2) = cMuestra
      rdoQ(3) = cBloque
    Case "T"
      sql = "UPDATE AP3600 SET AP36_Observ = ? WHERE " _
          & "AP21_CodRef = ? AND AP34_CodBloq = ? AND AP36_CodPorta = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(1).Type = rdTypeVARCHAR
      rdoQ(2).Type = rdTypeINTEGER
      rdoQ(3).Type = rdTypeINTEGER
      rdoQ(0) = txtObservaciones.Text
      rdoQ(1) = nRef
      rdoQ(2) = cBloque
      rdoQ(3) = cTecnica
  End Select
  rdoQ.Execute sql
  IntroducirObservaciones = rdoQ.RowsAffected
  Call CerrarrdoQueries
End Function
Function IntroducirEnvio(nRef As String, nodo As Node) As Integer
Dim sql As String
Dim codCtro As Integer, CodPat As Integer
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim num As Integer

  If Left(nodo.key, 1) = "P" Then
    CodPat = Right(nodo.key, Len(nodo.key) - 1)
    codCtro = Right(nodo.Parent.key, Len(nodo.Parent.key) - 1)
    sql = "SELECT MAX(AP44_CodEnvio) FROM AP4400 WHERE AP21_CodRef = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = nRef
    Set rdo = rdoQ.OpenResultset(sql)
    If IsNull(rdo(0)) Then num = 1 Else num = rdo(0) + 1
    rdo.Close
    
    sql = "BEGIN INSERT INTO AP4400 (AP21_CodRef, AP44_CodEnvio, AP34_CodBloq, "
    If Tipo = "T" Then sql = sql & "AP36_CodPorta, "
    sql = sql & "AP11_CodCtro, AP15_CodPat, AP44_FecEnvio, AP44_IndActiva) " _
      & "VALUES (?, ?, ?,"
    If Tipo = "T" Then
      sql = sql & " ?, ?, ?, SYSDATE, -1)"
    Else
      sql = sql & "?, ?, SYSDATE, -1)"
    End If
    If Tipo = "T" Then ' 5 es estado enviado
      sql = sql & "; UPDATE AP3600 SET AP48_CodEstPorta = " & apPORTAENVIADO & " WHERE " _
          & "AP21_CodRef = ? AND AP34_CodBloq = ? AND AP36_CodPorta = ?"
    Else
      sql = sql & "; UPDATE AP3400 SET AP47_CodEstBloq = " & apBLOQUEENVIADO & " WHERE " _
          & "AP21_CodRef = ? AND AP34_CodBloq = ?"
    End If
    sql = sql & "; END;"
    
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = nRef
    rdoQ(1) = num
    rdoQ(2) = cBloque
    If Tipo = "T" Then
      rdoQ(3) = cTecnica
      rdoQ(4) = codCtro
      rdoQ(5) = CodPat
      rdoQ(6) = nRef
      rdoQ(7) = cBloque
      rdoQ(8) = cTecnica
    Else
      rdoQ(3) = codCtro
      rdoQ(4) = CodPat
      rdoQ(5) = nRef
      rdoQ(6) = cBloque
    End If
    
    rdoQ.Execute sql
    IntroducirEnvio = rdoQ.RowsAffected
    Call CambiarNodoTxt("(Enviado)")
  Else
    MsgBox "Debe seleccionar un patólogo.", vbInformation, "Introducción incorrecta"
    IntroducirEnvio = 0
    Exit Function
  End If
  Call CerrarrdoQueries
End Function
Function IntroducirFotos(nRef As String) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim rdo As rdoResultset
Dim num As Integer

  sql = "SELECT MAX(AP38_CodFoto) FROM AP3800 WHERE AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(sql)
  If IsNull(rdo(0)) Then num = 1 Else num = rdo(0) + 1
  rdo.Close
  
  sql = "INSERT INTO AP3800 (AP21_CodRef, AP38_CodFoto, PR52NumIDMuestra, AP34_CodBloq, " _
      & "AP36_CodPorta, AP38_TiFoto, AP38_Lugar, AP38_FecReali, Ap38_Observ, AP38_Carrete, " _
      & "AP38_Pos) VALUES " _
      & "(?, ?, ?, ?, ?, ?, ?, SYSDATE, ?, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = num
  rdoQ(2) = cMuestra
  rdoQ(3) = Dato(cBloque)
  rdoQ(4) = Dato(cTecnica)
  If Tipo = "M" Then rdoQ(5) = apFOTO_MACRO Else rdoQ(5) = apFOTO_MICRO
  rdoQ(6) = txtLugar.Text
  rdoQ(7) = txtObservaciones.Text
  rdoQ(8) = txtCarrete.Text
  rdoQ(9) = txtPos.Text
      
  rdoQ.Execute sql
  IntroducirFotos = rdoQ.RowsAffected

  Call CerrarrdoQueries
End Function

Function IntroducirProblemas(nRef As String, nodo As Node) As Integer
Dim sql As String
Dim cProbl As Long
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim num As Integer

  On Error GoTo Error
  If Left(nodo.key, 1) = "P" Then
    cProbl = Right(nodo.key, Len(nodo.key) - 1)
    sql = "SELECT AP42_CODProblDET_SEQUENCE.NEXTVAL FROM DUAL"
    Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
    num = rdo(0)
    rdo.Close
    
    sql = "INSERT INTO AP4200 (AP42_CodProblDet, AP21_CodRef, PR52NumIDMuestra, AP34_CodBloq, " _
        & "AP36_CodPorta, AP41_CodProbl, AP42_Descrip, AP42_FecProbl, AP42_IndInfor) " _
      & "VALUES (?, ?, ?, ?, ?, ?, ?, SYSDATE, ?)"
    
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = num
    rdoQ(1) = nRef
    rdoQ(2) = Dato(cMuestra)
    rdoQ(3) = Dato(cBloque)
    rdoQ(4) = Dato(cTecnica)
    rdoQ(5) = Dato(cProbl)
    rdoQ(6) = txtObservaciones.Text
    If chkProblemas.Value = 1 Then rdoQ(7) = -1 Else rdoQ(7) = 0
    If chkEstado.Value = 1 Then Call Anular
    rdoQ.Execute sql
    IntroducirProblemas = rdoQ.RowsAffected
  
  Else
    IntroducirProblemas = 0
    MsgBox "Debe seleccionar un problema.", vbInformation, "Introducción incorrecta"
    Exit Function
  End If
  Call CerrarrdoQueries
  Exit Function
  
Error:
    MsgBox "Debe seleccionar un problema.", vbInformation, "Introducción incorrecta"
    IntroducirProblemas = 0
    Exit Function

End Function
Sub Anular()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim nRef As String, texto As String

  nRef = objPipe.PipeGet("NumRef")
  If cTecnica <> 0 Then
    sql = "UPDATE AP3600 SET AP48_CodEstPorta = ? WHERE " _
        & "AP21_CodRef = ? AND " _
        & "AP34_CodBloq = ? AND " _
        & "AP36_CodPorta = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(1).Type = rdTypeVARCHAR
    rdoQ(2).Type = rdTypeINTEGER
    rdoQ(3).Type = rdTypeINTEGER
    rdoQ(0) = apPORTAANULADO
    rdoQ(1) = nRef
    rdoQ(2) = cBloque
    rdoQ(3) = cTecnica
  Else
    If cBloque <> 0 Then
      sql = "UPDATE AP3400 SET AP47_CodEstBloq = ? WHERE " _
          & "AP21_CodRef = ? AND " _
          & "PR52NumIDMuestra = ? AND " _
          & "AP34_CodBloq = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeINTEGER
      rdoQ(1).Type = rdTypeVARCHAR
      rdoQ(2).Type = rdTypeINTEGER
      rdoQ(3).Type = rdTypeINTEGER
      rdoQ(0) = apBLOQUEANULADO
      rdoQ(1) = nRef
      rdoQ(2) = cMuestra
      rdoQ(3) = cBloque
    Else
      sql = "UPDATE AP2500 SET AP46_CodEstMues = ? WHERE " _
          & "AP21_CodRef = ? AND " _
          & "PR52NumIDMuestra = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeINTEGER
      rdoQ(1).Type = rdTypeVARCHAR
      rdoQ(2).Type = rdTypeINTEGER
      rdoQ(0) = apMUESTRAANULADA
      rdoQ(1) = nRef
      rdoQ(2) = cMuestra
    End If
  End If
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then
    texto = NodoPeticion.Text
    texto = Left(texto, InStr(texto, " (")) & " (Anulado)"
    NodoPeticion.Text = texto
  End If
End Sub
Sub CambiarNodoTxt(nuevo As String)
Dim texto As String, donde As Integer
  texto = NodoPeticion.Text
  donde = InStr(texto, " (")
  texto = Left(texto, donde)
  NodoPeticion.Text = texto & nuevo
End Sub
Function IntroducirInteres(nRef As String, nodo As Node) As Integer
Dim sql As String, texto As String
Dim codInteres As Integer
Dim rdoQ As rdoQuery

  If Left(nodo.key, 1) = "I" Then
    codInteres = Right(nodo.key, Len(nodo.key) - 1)
    Select Case Tipo
      Case "T"
        sql = "UPDATE AP3600 SET AP03_CodInt = ? WHERE " _
              & "AP21_CodRef = ? AND AP34_CodBloq = ? AND AP36_CodPorta = ?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0).Type = rdTypeINTEGER
        rdoQ(1).Type = rdTypeVARCHAR
        rdoQ(2).Type = rdTypeINTEGER
        rdoQ(3).Type = rdTypeINTEGER
        rdoQ(0) = codInteres
        rdoQ(1) = nRef
        rdoQ(2) = cBloque
        rdoQ(3) = cTecnica
    Case "B"
        sql = "UPDATE AP3400 SET AP03_CodInt = ? WHERE " _
            & "AP21_CodRef = ? AND AP34_CodBloq = ?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ(0).Type = rdTypeINTEGER
        rdoQ(1).Type = rdTypeVARCHAR
        rdoQ(2).Type = rdTypeINTEGER
        rdoQ(0) = codInteres
        rdoQ(1) = nRef
        rdoQ(2) = cBloque
    End Select
    rdoQ.Execute sql
    IntroducirInteres = rdoQ.RowsAffected
  Else
    MsgBox "Debe seleccionar un motivo de interés.", vbInformation, "Introducción incorrecta"
    IntroducirInteres = 0
    Exit Function
  End If
  Call CerrarrdoQueries
End Function

Function IntroducirAlmacenMuestra(nRef As String, nodo As Node) As Integer
Dim sql As String, texto As String
Dim codAlmacen As Integer
Dim rdoQ As rdoQuery

  If Left(nodo.key, 1) = "A" Then
    codAlmacen = Right(nodo.key, Len(nodo.key) - 1)
    sql = "UPDATE AP2500 SET AP18_CodMotivo = ?, " _
        & "AP25_FecAlm = SYSDATE, " _
        & "AP46_CODESTMUES = " & apMUESTRAALMACENADA & " WHERE " _
        & "AP21_CodRef = ? AND PR52NumIDMuestra = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = codAlmacen
    rdoQ(1) = nRef
    rdoQ(2) = cMuestra
    rdoQ.Execute sql
    IntroducirAlmacenMuestra = rdoQ.RowsAffected
    Call CambiarNodoTxt("(Almacenado)")
  Else
    MsgBox "Debe seleccionar un motivo de almacenamiento.", vbInformation, "Introducción incorrecta"
    IntroducirAlmacenMuestra = 0
    Exit Function
  End If
  Call CerrarrdoQueries
End Function

Function IntroducirAlmacenBloque(nRef As String, nodo As Node) As Integer
Dim sql As String, texto As String
Dim codAlmacen As Integer
Dim rdoQ As rdoQuery
  
  If Left(nodo.key, 1) = "A" Then
    codAlmacen = Right(nodo.key, Len(nodo.key) - 1)
    sql = "UPDATE AP3400 SET AP33_CodAlm = ?, AP47_CODESTBloq = " & apBLOQUEALMACENADO _
        & ", AP34_Caja = ?, AP34_Hueco = ? WHERE " _
        & "AP21_CodRef = ? AND AP34_CodBloq = ?"
    
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeINTEGER
    rdoQ(1).Type = rdTypeVARCHAR
    rdoQ(2).Type = rdTypeVARCHAR
    rdoQ(3).Type = rdTypeVARCHAR
    rdoQ(4).Type = rdTypeINTEGER
    rdoQ(0) = codAlmacen
    rdoQ(1) = txtAlmacen(0).Text
    rdoQ(2) = txtAlmacen(1).Text
    rdoQ(3) = nRef
    rdoQ(4) = cBloque
    rdoQ.Execute sql
    IntroducirAlmacenBloque = rdoQ.RowsAffected
    Call CambiarNodoTxt("(Almacenado)")
  Else
    IntroducirAlmacenBloque = 0
    MsgBox "Debe seleccionar un lugar de almacenamiento.", vbInformation, "Introducción incorrecta"
    Exit Function
  End If
  Call CerrarrdoQueries
End Function

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub Form_Load()
Dim padre As Node, abuelo As Node
Dim nRef As String
  
  On Error Resume Next
  Select Case objPipe.PipeGet("menu")
    Case "Petición"
      Set NodoPeticion = frmP_Peticion.tvwMuestra.SelectedItem
      Tipo = Left(NodoPeticion.key, 1)
      Select Case Tipo
        Case "R"
        Case "M"
          txtMuestra.Text = NodoPeticion.Text
          cMuestra = Right(NodoPeticion.key, Len(NodoPeticion.key) - 1)
        Case "B"
          Set padre = NodoPeticion.Parent
          txtMuestra.Text = padre.Text
          txtBloque.Text = NodoPeticion.Text
          cBloque = Right(NodoPeticion.key, Len(NodoPeticion.key) - 1)
          cMuestra = Right(NodoPeticion.Parent.key, Len(NodoPeticion.Parent.key) - 1)
        Case "T"
          Set padre = NodoPeticion.Parent
          Set abuelo = padre.Parent
          txtMuestra.Text = abuelo.Text
          txtBloque.Text = padre.Text
          txtTecnica.Text = NodoPeticion.Text
          cMuestra = Right(abuelo.key, Len(abuelo.key) - 1)
          cTecnica = Val(Right(NodoPeticion.key, Len(NodoPeticion.key) - 1))
          cBloque = Right(NodoPeticion.Parent.key, Len(NodoPeticion.Parent.key) - 1)
        Case Else
      End Select
  End Select
  
  boton = objPipe.PipeGet("Boton")
  nRef = objPipe.PipeGet("NumRef")
  Select Case boton
    Case "Observaciones"
      Me.Caption = "Introducción de observaciones"
      Call txtVisible
      txtObservaciones.Text = Observaciones(Tipo, nRef, cMuestra, cBloque, cTecnica)
    Case "Envio"
      Me.Caption = "Introducción de envío"
      Call txtInvisible
    Case "Interes"
      Me.Caption = "Introducción de motivos de interés"
      Call txtInvisible
    Case "Almacen_Muestra"
      Me.Caption = "Introducción de motivos de almacenamiento de muestras"
      Call txtInvisible
    Case "Almacen_Bloque"
      Me.Caption = "Introducción de lugar de almacenamiento de bloques"
      Call txtInvisible
      fraAlmacen.Visible = True
    Case "Problemas"
      Me.Caption = "Introducción de problemas"
      Call FormatProblemas
    Case "Fotos"
      Me.Caption = "Introducción de Fotografías"
      Call FormatFoto
'      Call LlenarTxtFoto
  End Select
  Call Llenartvw(objPipe.PipeGet("Boton"))
  
End Sub
Sub LlenarTxtFoto()
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim sql As String
  
  Select Case Tipo
    Case "M"
      sql = "SELECT AP38_Observ, AP38_Lugar FROM AP3800 WHERE " _
          & "AP21_CodRef = ? AND PR52NumIDMuestra = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(1).Type = rdTypeINTEGER
      rdoQ(0) = objPipe.PipeGet("NumRef")
      rdoQ(1) = cMuestra
    Case "T"
      sql = "SELECT AP38_Observ, AP38_Lugar FROM AP3800 WHERE " _
          & "AP21_CodRef = ? AND AP34_CodBloq = ? AND AP36_CodPorta = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeVARCHAR
      rdoQ(1).Type = rdTypeINTEGER
      rdoQ(2).Type = rdTypeINTEGER
      rdoQ(0) = objPipe.PipeGet("NumRef")
      rdoQ(1) = cBloque
      rdoQ(2) = cTecnica
  End Select
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    If IsNull(rdo(0)) Then txtObservaciones.Text = "" Else txtObservaciones.Text = rdo(0)
    If IsNull(rdo(1)) Then txtLugar.Text = "" Else txtLugar.Text = rdo(1)
  End If
  rdo.Close
  Call CerrarrdoQueries
End Sub
Sub FormatFoto()
  Me.Height = 4320
  fraObservaciones.Visible = True
  fraFoto.Visible = True
  tvwDatos.Visible = False
End Sub
Sub txtVisible()
  Me.Height = 3900
  fraObservaciones.Visible = True
  tvwDatos.Visible = False
End Sub
Sub txtInvisible()
  Me.Height = 7185
  fraObservaciones.Visible = False
  tvwDatos.Visible = True
  Set tvwDatos.ImageList = frmPrincipal.imgIcos
End Sub
Sub Llenartvw(boton As String)
Dim sql As String
Dim nodo As Node, nodoPat As Node, nodoProbl As Node
Dim rdo As rdoResultset, rdoPat As rdoResultset, rdoProbl As rdoResultset
Dim rdoQ As rdoQuery
  
  Select Case boton
    
    Case "Envio"
      Set nodo = tvwDatos.Nodes.Add(, , "Raiz", "Destino", apICONRAIZ)
      sql = "SELECT AP15_CodPat, NVL(AP15_Firma,AP15_Ap1||' '||AP15_Ap2||', '||AP15_Nom) " _
          & "FROM AP1500 WHERE AP11_CodCtro = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeINTEGER
      
      sql = "SELECT AP11_CodCtro, AP11_Desig FROM AP1100 WHERE AP11_CodCtro <> 1"
      Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
      While rdo.EOF = False
        Set nodo = tvwDatos.Nodes.Add("Raiz", tvwChild, "C" & rdo(0), rdo(1), apICONCENTROEXTERNO, apICONCENTROEXTERNO)
        rdoQ(0) = rdo(0)
        Set rdoPat = rdoQ.OpenResultset(rdOpenForwardOnly)
        While rdoPat.EOF = False
          Set nodoPat = tvwDatos.Nodes.Add("C" & rdo(0), tvwChild, "P" & rdoPat(0), rdoPat(1), apICONPAT, apICONPAT)
          rdoPat.MoveNext
        Wend
        rdoPat.Close
        rdo.MoveNext
      Wend
      rdo.Close
      Call CerrarrdoQueries
    
    Case "Interes"
      Set nodo = tvwDatos.Nodes.Add(, , "Raiz", "Motivos de interés", apICONRAIZ)
      sql = "SELECT AP03_CodInt, AP03_Desig FROM AP0300 WHERE AP03_IndActiva = -1 " _
          & "ORDER BY AP03_Desig"
      Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
      While rdo.EOF = False
        Set nodo = tvwDatos.Nodes.Add("Raiz", tvwChild, "I" & rdo(0), rdo(1), apICONINTERES, apICONINTERES)
        rdo.MoveNext
      Wend
      rdo.Close
    
    Case "Almacen_Muestra"
      Set nodo = tvwDatos.Nodes.Add(, , "Raiz", "Motivos de almacenamiento", apICONRAIZ)
      sql = "SELECT AP18_CodMotivo, AP18_Desig FROM AP1800 WHERE AP18_IndActiva = -1 " _
          & "ORDER BY AP18_Desig"
      Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
      While rdo.EOF = False
        Set nodo = tvwDatos.Nodes.Add("Raiz", tvwChild, "A" & rdo(0), rdo(1), apICONALMACENMUESTRA, apICONALMACENMUESTRA)
        rdo.MoveNext
      Wend
      rdo.Close
    
    Case "Almacen_Bloque"
      Set nodo = tvwDatos.Nodes.Add(, , "Raiz", "Lugar de almacenamiento", apICONRAIZ)
      sql = "SELECT AP33_CodAlm, AP33_Desig FROM AP3300 WHERE AP33_IndActiva = -1 " _
          & "ORDER BY AP33_Desig"
      Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
      While rdo.EOF = False
        Set nodo = tvwDatos.Nodes.Add("Raiz", tvwChild, "A" & rdo(0), rdo(1), apICONALMACENMUESTRA, apICONALMACENMUESTRA)
        rdo.MoveNext
      Wend
      rdo.Close
    
    Case "Problemas"
      sql = "SELECT AP41_CodProbl, AP41_Desig FROM AP4100 WHERE AP35_CodTiProbl = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0).Type = rdTypeINTEGER
      
      Set nodo = tvwDatos.Nodes.Add(, , "Raiz", "Problemas", apICONRAIZ)
      sql = "SELECT AP35_CodTiProbl, AP35_Desig FROM AP3500 WHERE AP35_IndActiva <> 0"
      Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
      While rdo.EOF = False
        Set nodo = tvwDatos.Nodes.Add("Raiz", tvwChild, "T" & rdo(0), rdo(1), apICONTIPOPROBLEMA, apICONTIPOPROBLEMA)
        rdoQ(0) = rdo(0)
        Set rdoProbl = rdoQ.OpenResultset(rdOpenForwardOnly)
        While rdoProbl.EOF = False
          Set nodoProbl = tvwDatos.Nodes.Add("T" & rdo(0), tvwChild, "P" & rdoProbl(0), rdoProbl(1), apICONPROBLEMA, apICONPROBLEMA)
          rdoProbl.MoveNext
        Wend
        rdoProbl.Close
        rdo.MoveNext
      Wend
      rdo.Close
      Call CerrarrdoQueries
  End Select
  
  If boton <> "Observaciones" And boton <> "Fotos" Then
    If tvwDatos.Nodes("Raiz").Children > 0 Then
      tvwDatos.Nodes(nodo.Index).EnsureVisible
    End If
  End If
End Sub

