Attribute VB_Name = "modFunciones"
Option Explicit
Public Color(0 To 11) As Long
Public Function fNextClave(campo$) As String
    Dim sql$, rsMaxClave As rdoResultset
    
    sql = "SELECT " & campo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(sql)
    fNextClave = rsMaxClave(0)
    rsMaxClave.Close
    
End Function
Public Function fMaxCodDetalle(tablaDetalle$, codigoDetalle$, _
  refMaestro$, valorMaestro$) As String
    
 Dim sql$, rsMaxClave As rdoResultset
    
    sql = "SELECT (MAX(" & codigoDetalle & ")+1) FROM " & tablaDetalle
    sql = sql + " WHERE " & refMaestro & " = " & valorMaestro
 
 Set rsMaxClave = objApp.rdoConnect.OpenResultset(sql)
  If IsNull(rsMaxClave(0)) Then
      fMaxCodDetalle = 1
  Else
      fMaxCodDetalle = rsMaxClave(0)
  End If

    rsMaxClave.Close
    
End Function
  
Sub IniVisor()
  Set vs = New clsVisor
'  Set vs = CreateObject("IMWord.clsVisor")
  Call vs.EstablecerConexion(objApp.rdoConnect)
  
  Set meca = New clsMeca
'  Set meca = CreateObject("IMWord.clsMeca")
  Call meca.EstablecerConexion(objApp.rdoConnect)
End Sub

Function SiguienteNum(Tipo As Integer) As String
Dim num As String
Dim sql As String
Dim rdo As rdoResultset

  Select Case Tipo
    Case 1 ' Biopsias
      sql = "SELECT AP21_CODREF_BIO_SEQUENCE.NEXTVAL FROM DUAL"
      num = "B"
    Case 2 ' Citolog�as
      sql = "SELECT AP21_CODREF_CITO_SEQUENCE.NEXTVAL FROM DUAL"
      num = "C"
    Case 3 ' Autopsias
      sql = "SELECT AP21_CODREF_AUT_SEQUENCE.NEXTVAL FROM DUAL"
      num = "A"
  End Select
  Set rdo = objApp.rdoConnect.OpenResultset(sql)
  SiguienteNum = num & String(5 - Len(CStr(rdo(0))), "0") & rdo(0)

End Function
Function SuperUser() As Integer
  If InStr(UCase(objApp.strNetUser), "ESII") = 0 And InStr(UCase(objApp.strNetUser), "JSOLA") = 0 Then
    SuperUser = False
  Else
    SuperUser = True
  End If
End Function

Function ComprobarDatos() As Integer
  Load frmIDDr
  With frmIDDr
    .fraId.Visible = True
    .fraPwd.Visible = False
    .fraNuevo.Visible = False
    .Show vbModal
    ComprobarDatos = frmIDDr.PwdCorrect = True
  End With
  Unload frmIDDr
  Set frmIDDr = Nothing
End Function
Sub CerrarrdoQueries()
Dim i As Integer

  For i = 0 To objApp.rdoConnect.rdoQueries.Count - 1
    objApp.rdoConnect.rdoQueries(0).Close
  Next i
End Sub
Function NumDiag() As Long
Dim sql As String
Dim rdo As rdoResultset

  sql = "SELECT MAX(AP39_CODDiag) FROM AP3900"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  NumDiag = rdo(0) + 1
  rdo.Close
  
End Function
Function posicion(pos As Long, interl As Long) As Long
If pos > 15000 Then
    Printer.NewPage
    posicion = 2000
    Printer.CurrentX = 11000
    Printer.CurrentY = 5000
'    Printer.Print Printer.Page
Else
    posicion = pos + interl
End If
End Function

Sub LlenarIDDr(Pat() As String, pantalla As Form)
Dim ctrl As Integer
Dim item As ListItem
Dim CodPat As Integer, NumPat As Integer

'  frmIDDr.Height = 2200
'  ReDim Pat(1 To 2, 1 To 1)
'  Pat(1, 1) = -1
'  ctrl = 0
'  For Each item In pantalla.lvwPat.ListItems
'    If item.Selected = True Then
'      ctrl = 1
'      With frmIDDr
'        CodPat = Val(Right(item.key, Len(item.key) - 1))
'        .txtPwd.Tag = CodPat
'        .txtPat.Text = item.Text
'        .Show vbModal
'        If .PwdCorrect = True Then
'          If UBound(Pat, 2) = 1 And Pat(1, 1) = -1 Then
'            Pat(1, 1) = CodPat
'            Pat(2, 1) = item.Text
'          Else
'            NumPat = UBound(Pat, 2) + 1
'            ReDim Preserve Pat(1 To 2, 1 To NumPat)
'            Pat(1, NumPat) = CodPat
'            Pat(2, NumPat) = item.Text
'          End If
'        End If
'      End With
'    End If
'  Next item
'  If ctrl = 0 Then
'    MsgBox "Debe seleccionar un pat�logo.", vbInformation, "Introducci�n incorrecta"
'  End If

End Sub

Function Hoy() As String
Dim sql As String
Dim rdo As rdoResultset
  
  sql = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS') FROM DUAL"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  Hoy = rdo(0)
  rdo.Close
End Function
Function ActualizarEstPr_Macro(nRef As String) As Integer
Dim sql As String, Estado As Integer, ctrl As Integer
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "UPDATE AP2100 SET AP45_CodEstPr = ? WHERE AP21_CodRef = ? AND AP45_CodEstPr < ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = apPRUEBAMACRO
  rdoQ(1) = nRef
  rdoQ(2) = apPRUEBAMACRO
  rdoQ.Execute
  ActualizarEstPr_Macro = rdoQ.RowsAffected
End Function
Sub ActualizarEstPr_ConfirmDiag(nRef As String, EstDiag As Integer)
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE AP2100 SET AP45_CodEstPr = ? WHERE AP21_CodRef IN " & nRef
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  If EstDiag = apDIAGPROVISIONAL Then rdoQ(0) = apPRUEBAPROVISIONAL Else rdoQ(0) = apPRUEBAFINALIZADA
  rdoQ.Execute

End Sub

Sub ActualizarEstPr_Diag(nRef As String, EstDiag As Integer)
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE AP2100 SET AP45_CodEstPr = ? WHERE AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  If EstDiag = apDIAGPROVISIONAL Then rdoQ(0) = apPRUEBAPROVISIONAL Else rdoQ(0) = apPRUEBAFINALIZADA
  rdoQ(1) = nRef
  rdoQ.Execute

End Sub
Function ActualizarEstPr_Micro(nRef As String) As Integer
Dim sql As String, Estado As Integer
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  ActualizarEstPr_Micro = 1
  sql = "SELECT DISTINCT AP48_CodEstPorta FROM AP3600 WHERE " _
      & "AP21_CodRef = ? AND AP36_IndIntra = 0 ORDER BY AP48_CodEstPorta"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    If rdo(0) = apPORTAREALIZADO Then
      sql = "UPDATE AP2100 SET AP45_CodEstPr = ? WHERE AP21_CodRef = ? AND AP45_CodEstPr < ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = apPRUEBAMICRO
      rdoQ(1) = nRef
      rdoQ(2) = apPRUEBAMICRO
      rdoQ.Execute
'      ActualizarEstPr_Micro = rdoQ.RowsAffected
    End If
  End If
  rdo.Close
End Function

Function ActualizarEstPr_Cont(nRef As String) As Integer
Dim sql As String, Estado As Integer
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  ActualizarEstPr_Cont = 0
  sql = "SELECT DISTINCT AP48_CodEstPorta FROM AP3600 WHERE " _
      & "AP36_IndIntra = 0 AND " _
      & "AP21_CodRef IN (" _
      & "SELECT AP21_CodRef FROM AP2100 WHERE AP21_CodRef = ? AND AP45_CodEstPr < ?) " _
      & "ORDER BY AP48_CodEstPorta"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = apPRUEBAPROVISIONAL
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    If rdo(0) = apPORTAREALIZADO Then
      sql = "UPDATE AP2100 SET AP45_CodEstPr = ? WHERE AP21_CodRef = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = apPRUEBACONTINUADA
      rdoQ(1) = nRef
      rdoQ.Execute
      ActualizarEstPr_Cont = rdoQ.RowsAffected
    End If
  End If
  rdo.Close
  
End Function


Function NombreFichpruebas(Historia As String, Caso As String, Secuencia As String) As String
Dim nombredoc As String, NombreDoc2 As String, i As Integer
  
  nombredoc = LTrim(Hex(Val(Right(Historia, 6))))
  If Len(nombredoc) < 5 Then
    For i = 1 To 5 - Len(nombredoc)
      nombredoc = "0" & nombredoc
    Next i
  End If
  
  NombreDoc2 = LTrim(Hex(Val(Caso) * 1000 + Val(Secuencia)))
  If Len(NombreDoc2) < 6 Then
    For i = 1 To 6 - Len(NombreDoc2)
      NombreDoc2 = "0" & NombreDoc2
    Next i
  End If
  NombreFichpruebas = nombredoc & Left(NombreDoc2, 3) & "." & Right(NombreDoc2, 3)
End Function

Function Historia(Nombre As String) As Long
    Historia = Val("&H" & Left(Nombre, 5))
End Function
Function Caso(Nombre As String) As Long
Dim CS As String

    CS = Left(Right(Nombre, 7), 3) & Right(Nombre, 3)
    Caso = Val("&H" & CS) / 1000

End Function
Function Secuencia(Nombre As String) As Long
Dim CS As String

    CS = Left(Right(Nombre, 7), 3) & Right(Nombre, 3)
    Secuencia = Right(CStr(Val("&H" & CS)), 3)

End Function
Sub ActualizarEstadoPrueba(nRef As String)
Dim sql As String
Dim rdoQ As rdoQuery
  
  sql = "UPDATE AP2100 SET AP45_CodEstPr = ? WHERE AP21_CodRef = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = apPRUEBARECEPCIONADA
  rdoQ(1) = nRef
  rdoQ.Execute sql

End Sub

Function BuscarAnalitica(NH As Long, cOrg As String, Ana() As ResAnalitica) As Integer
Dim sql As String
Dim rdo As rdoResultset, rdo1 As rdoResultset, rdo2 As rdoResultset
Dim rdoQ As rdoQuery, rdoQ1 As rdoQuery
Dim nRes As Integer, nPr As Integer
  
  nPr = 0 ' Indica el numero de pruebas encontradas
  nRes = 0 ' Indica el numero de resultados encontrados de cada prueba
  sql = "SELECT AP3200.PR01CodActuacion, PR0100.PR01DesCompleta FROM AP3200, PR0100 WHERE " _
      & "AP3200.PR01CodActuacion = PR0100.PR01CodActuacion AND " _
      & "AP3200.PR24CodMuestra IN " & cOrg
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  If rdo.EOF = False Then
    While rdo.EOF = False
      sql = "SELECT PA.PR04NumActPlan, PA.NRepeticion, SP.Fecha FROM " _
          & "PruebaAsistencia PA, SeguimientoPrueba SP WHERE " _
          & "PA.PR04NumActPlan = SP.PR04NumActPlan AND " _
          & "PA.NRepeticion = SP.NRepeticion AND " _
          & "PA.PR04NumActPlan IN (" _
          & "SELECT PR04NumActPlan FROM PR0400 WHERE CI21CodPersona IN ( " _
          & "SELECT CI21CodPersona FROM CI2200 WHERE CI22NumHistoria = ?)) AND " _
          & "SP.Proceso = 9 AND " _
          & "PA.PR01CodActuacion = ? AND " _
          & "ROWNUM = 1 " _
          & "ORDER BY SP.Fecha DESC"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = NH
      rdoQ(1) = rdo(0)
      Set rdo1 = rdoQ.OpenResultset(rdOpenForwardOnly)
      If rdo1.EOF = False Then
        nPr = nPr + 1
        ReDim Preserve Ana(1 To nPr)
        With Ana(nPr)
          .cPr = rdo(0)
          .pr = rdo(1)
          .fecha = rdo1(2)
        End With
        sql = "SELECT RA.cResultado, PR.Designacion, RA.ResultadoAlfanumerico, U.Designacion " _
            & "FROM PruebasResultados PR, ResultadoAsistencia RA, Unidades U WHERE " _
            & "RA.cResultado = PR.cResultado AND " _
            & "PR.PR01CodActuacion = ? AND " _
            & "RA.cUnidad = U.cUnidad AND " _
            & "RA.PR04NumActPlan = ? AND " _
            & "RA.NRepeticion = ?"
        Set rdoQ1 = objApp.rdoConnect.CreateQuery("", sql)
        rdoQ1(0) = rdo(0)
        rdoQ1(1) = rdo1(0)
        rdoQ1(2) = rdo1(1)
        Set rdo2 = rdoQ1.OpenResultset(rdOpenForwardOnly)
        nRes = 0
        While rdo2.EOF = False
          BuscarAnalitica = True
          nRes = nRes + 1
          ReDim Preserve Ana(nPr).ResPr(1 To nRes)
          With Ana(nPr).ResPr(nRes)
            .cRes = rdo2(0)
            .res = rdo2(1)
            .Resultado = rdo2(2)
            .Unidades = rdo2(3)
          End With
          rdo2.MoveNext
        Wend
        rdo2.Close
      End If
      rdo.MoveNext
    Wend
    rdo1.Close
    If nPr = 0 Then BuscarAnalitica = False Else BuscarAnalitica = True
  Else
    BuscarAnalitica = False
  End If
  rdo.Close
  
End Function

Function HayTecnicas(nRef As String) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AP36_CodPorta FROM AP3600 WHERE " _
      & "AP21_CodRef = ? AND " _
      & "AP48_CodEstPorta = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = apPORTAREALIZADO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then HayTecnicas = True Else HayTecnicas = False
  rdo.Close
End Function
Function HayDiagProvisionales(nRef As String) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AP39_CodDiag FROM AP3900 WHERE " _
      & "AP21_CodRef = ? AND " _
      & "AP49_CodEstDiag <> ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = apDIAGDEFINITIVO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then HayDiagProvisionales = True Else HayDiagProvisionales = False
  rdo.Close

End Function


Function FaltanTecnicas(nRef As String) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT AP36_CodPorta FROM AP3600 WHERE " _
      & "AP21_CodRef = ? AND " _
      & "AP48_CodEstPorta < ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = nRef
  rdoQ(1) = apPORTAREALIZADO
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    FaltanTecnicas = True
  Else
    sql = "SELECT COUNT(*) AP36_CodPorta FROM AP3600 WHERE " _
        & "AP21_CodRef = ? AND " _
        & "AP48_CodEstPorta < ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0).Type = rdTypeVARCHAR
    rdoQ(1).Type = rdTypeINTEGER
    rdoQ(0) = nRef
    rdoQ(1) = apPORTABORRADO
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    If rdo(0) = 0 Then
      FaltanTecnicas = True
    Else
      FaltanTecnicas = False
    End If
  End If
  rdo.Close
End Function


Public Sub llenarlblMuestra(lbl As Label, cMuestraActiva)
  
  With lbl
    .ForeColor = Color(cMuestraActiva Mod 12)
    .Visible = True
    .Caption = "Est� introduciendo la muestra n� " & cMuestraActiva + 1
  End With
End Sub

Public Function SigPorta(nRef As String, NBloq As Integer) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT MAX(AP36_CodPorta) FROM AP3600 WHERE AP21_CodRef = ? AND AP34_CodBloq = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nRef
  rdoQ(1) = NBloq
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If IsNull(rdo(0)) Then SigPorta = 1 Else SigPorta = rdo(0) + 1
  rdo.Close
End Function

Public Function Observaciones(Tipo As String, nRef As String, Optional cMuestra As Long, Optional cBloque As Long, Optional cTecnica As Long) As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim sql As String
  
  Select Case Tipo
    Case "R"
      sql = "SELECT AP21_Observ FROM AP2100 WHERE " _
          & "AP21_CodRef = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = nRef
    Case "M"
      sql = "SELECT AP25_Observ FROM AP2500 WHERE " _
          & "AP21_CodRef = ? AND PR52NumIDMuestra = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = nRef
      rdoQ(1) = cMuestra
    Case "B"
      sql = "SELECT AP34_Observ FROM AP3400 WHERE " _
          & "AP21_CodRef = ? AND AP34_CodBloq = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = nRef
      rdoQ(1) = cBloque
    Case "T"
      sql = "SELECT AP36_Observ FROM AP3600 WHERE " _
          & "AP21_CodRef = ? AND AP34_CodBloq = ? AND AP36_CodPorta = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = nRef
      rdoQ(1) = cBloque
      rdoQ(2) = cTecnica
  End Select
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If IsNull(rdo(0)) Then Observaciones = "" Else Observaciones = rdo(0)
  rdo.Close
  'Call CerrarrdoQueries
End Function

Public Function fechaActual() As String
    Dim sql As String, rdo As rdoResultset
    
    sql = "SELECT SYSDATE FROM DUAL"
    Set rdo = objApp.rdoConnect.OpenResultset(sql)
    fechaActual = Format$(rdo(0), "DD/MM/YYYY")
    rdo.Close
End Function

