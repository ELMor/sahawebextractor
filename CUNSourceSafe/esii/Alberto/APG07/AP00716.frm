VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmIDDr 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Identificación"
   ClientHeight    =   2055
   ClientLeft      =   1665
   ClientTop       =   2250
   ClientWidth     =   6450
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2055
   ScaleWidth      =   6450
   Begin VB.Frame fraId 
      Height          =   1440
      Left            =   150
      TabIndex        =   12
      Top             =   300
      Visible         =   0   'False
      Width           =   4665
      Begin VB.TextBox txtUsuario 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Index           =   1
         Left            =   1725
         PasswordChar    =   "*"
         TabIndex        =   13
         Top             =   900
         Width           =   2265
      End
      Begin SSDataWidgets_B.SSDBCombo cbossID 
         Bindings        =   "AP00716.frx":0000
         Height          =   330
         Left            =   1725
         TabIndex        =   16
         Top             =   375
         Width           =   2280
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Código"
         Columns(0).Name =   "Código"
         Columns(0).DataField=   "Column 0"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Personal"
         Columns(1).Name =   "Descripción"
         Columns(1).DataField=   "Column 1"
         Columns(1).FieldLen=   256
         _ExtentX        =   4022
         _ExtentY        =   582
         _StockProps     =   93
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label1 
         Caption         =   "Usuario:"
         Height          =   315
         Index           =   3
         Left            =   675
         TabIndex        =   15
         Top             =   375
         Width           =   915
      End
      Begin VB.Label Label1 
         Caption         =   "Contraseña:"
         Height          =   315
         Index           =   4
         Left            =   375
         TabIndex        =   14
         Top             =   900
         Width           =   1215
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Appearance      =   0  'Flat
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5025
      TabIndex        =   1
      Top             =   825
      Width           =   1275
   End
   Begin VB.CommandButton cmdAceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Aceptar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5025
      TabIndex        =   0
      Top             =   300
      Width           =   1275
   End
   Begin VB.Frame fraPwd 
      Height          =   1440
      Left            =   150
      TabIndex        =   2
      Top             =   225
      Width           =   4665
      Begin VB.CommandButton cmdNueva 
         Appearance      =   0  'Flat
         Caption         =   "&Cambiar..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3225
         TabIndex        =   6
         Top             =   900
         Visible         =   0   'False
         Width           =   1275
      End
      Begin VB.TextBox txtPat 
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   300
         Locked          =   -1  'True
         TabIndex        =   4
         Text            =   "Text1"
         Top             =   300
         Width           =   4140
      End
      Begin VB.TextBox txtPwd 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   1275
         PasswordChar    =   "*"
         TabIndex        =   3
         Top             =   900
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Contraseña:"
         Height          =   315
         Index           =   0
         Left            =   225
         TabIndex        =   5
         Top             =   900
         Width           =   1065
      End
   End
   Begin VB.Frame fraNuevo 
      Height          =   1440
      Left            =   150
      TabIndex        =   7
      Top             =   225
      Visible         =   0   'False
      Width           =   4665
      Begin VB.TextBox txtNuevoPwd 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Index           =   0
         Left            =   2175
         MaxLength       =   10
         PasswordChar    =   "*"
         TabIndex        =   9
         Top             =   375
         Width           =   1815
      End
      Begin VB.TextBox txtNuevoPwd 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Index           =   1
         Left            =   2175
         MaxLength       =   10
         PasswordChar    =   "*"
         TabIndex        =   10
         Top             =   900
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Nueva contraseña:"
         Height          =   315
         Index           =   2
         Left            =   375
         TabIndex        =   11
         Top             =   375
         Width           =   1665
      End
      Begin VB.Label Label1 
         Caption         =   "Repetir contraseña:"
         Height          =   315
         Index           =   1
         Left            =   375
         TabIndex        =   8
         Top             =   900
         Width           =   1740
      End
   End
End
Attribute VB_Name = "frmIDDr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PwdCorrect As Integer
Private Sub cmdAceptar_Click()
  
  If fraNuevo.Visible = True Then
    If UCase(Trim(txtNuevoPwd(0).Text)) <> UCase(Trim(txtNuevoPwd(1).Text)) Then
      MsgBox "No coinciden las contraseñas.", vbExclamation, "Introducción incorrecta"
    Else
'      frmA_Personal.txtPatologo(7).Text = txtNuevoPwd(0).Text
'      frmA_Personal.tlbToolbar1.Buttons(4).Enabled = True
'      frmA_Personal.objWinInfo.intWinStatus = cwModeSingleEdit
'      frmA_Personal.objWinInfo.DataRefresh
'      Call NuevaContrasena(Val(txtPwd.Tag), Trim(txtNuevoPwd(0).Text))
      Me.Hide
    End If
  ElseIf fraPwd.Visible = True Then
    If PwdOK(txtPwd.Tag, txtPwd.Text) = True Then
      PwdCorrect = True
      Me.Hide
    Else
      PwdCorrect = False
      MsgBox "Contraseña incorrecta.", vbExclamation, "Introducción incorrecta"
      txtPwd.SetFocus
      txtPwd.SelStart = 0
      txtPwd.SelLength = Len(txtPwd.Text)
    End If
  ElseIf fraId.Visible = True Then
    If UsuarioPwdOK(cbossID.Columns(0).Text, txtUsuario(1).Text) = True Then
      Me.Hide
      PwdCorrect = True
    Else
      PwdCorrect = False
      MsgBox "Nombre de usuario/contraseña incorrecta.", vbExclamation, "Introducción incorrecta"
      txtUsuario(1).SetFocus
      txtUsuario(1).SelStart = 0
      txtUsuario(1).SelLength = Len(txtUsuario(1).Text)
    End If
  End If
End Sub
Sub NuevaContrasena(cDr As Integer, NuevaPwd As String)
Dim sql As String
Dim rdoQ As rdoQuery

  sql = "UPDATE AP0800 SET AP08_Clave = ? WHERE AP08_CodPers = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0).Type = rdTypeVARCHAR
  rdoQ(1).Type = rdTypeINTEGER
  rdoQ(0) = NuevaPwd
  rdoQ(1) = cDr
  
  rdoQ.Execute sql
  
End Sub
Private Sub cmdCancelar_Click()
  PwdCorrect = False
  Me.Hide
End Sub

Private Sub cmdNueva_Click()
  ' Se comprueba que el dato sea correcto
  If PwdOK(Val(txtPwd.Tag), txtPwd.Text) = False Then
    MsgBox "Contraseña incorrecta.", vbExclamation, "Introducción incorrecta"
    txtPwd.SetFocus
  Else
    fraPwd.Visible = False
    fraNuevo.Visible = True
    txtNuevoPwd(0).SetFocus
  End If

End Sub
Function PwdOK(cDr As String, Pwd As String) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT SG02Cod FROM SG0200 WHERE UPPER(SG02Cod) = ? AND UPPER(SG02Passw) = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = UCase(cDr)
  rdoQ(1) = UCase(Pwd)
  
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    PwdOK = True
  Else
    PwdOK = False
  End If
  rdo.Close
End Function
Function UsuarioPwdOK(user As String, Pwd As String) As Integer
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery

  sql = "SELECT SG02Cod FROM SG0200 WHERE SG02Cod = ? AND UPPER(SG02Passw) = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = user
  rdoQ(1) = UCase(Pwd)
  
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    UsuarioPwdOK = True
  Else
    UsuarioPwdOK = False
  End If
  rdo.Close
End Function
Private Sub Form_Activate()
  If fraPwd.Visible = True Then
    txtPwd.SetFocus
    txtPwd.Text = ""
  ElseIf fraId.Visible = True Then
    Call LlenarCombo
    cbossID.SetFocus
  ElseIf fraNuevo.Visible = True Then
    txtNuevoPwd(0).SetFocus
  End If
End Sub
Sub LlenarCombo()
Dim sql As String
Dim rdo As rdoResultset

  sql = "SELECT AP0800.SG02Cod, " _
      & "NVL(SG02txtFirma, SG0200.SG02Ape1||' '||SG0200.SG02Ape2||', '||SG0200.SG02Nom) " _
      & "FROM SG0200, AP0800 WHERE " _
      & "AP0800.SG02Cod = SG0200.SG02Cod AND " _
      & "AP0800.AP07_CodTiPers IN (" & apCODPATOLOGO & ", " & apCODRES & ")"
  Set rdo = objApp.rdoConnect.OpenResultset(sql)
  While rdo.EOF = False
    cbossID.AddItem rdo(0) & Chr(9) & rdo(1)
    rdo.MoveNext
  Wend
  rdo.Close
End Sub
Private Sub Form_Load()
  PwdCorrect = False
End Sub



Private Sub txtNuevoPwd_KeyPress(Index As Integer, KeyAscii As Integer)
  If Index = 1 Then
    Select Case KeyAscii
      Case 13
        cmdAceptar_Click
      Case 27
        cmdCancelar_Click
    End Select
  Else
    Select Case KeyAscii
      Case 13
        txtNuevoPwd(1).SetFocus
      Case 27
        cmdCancelar_Click
    End Select
  End If
End Sub

Private Sub txtPwd_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then cmdAceptar_Click
End Sub

Private Sub txtUsuario_KeyPress(Index As Integer, KeyAscii As Integer)
  If Index = 1 Then
    Select Case KeyAscii
      Case 13
        cmdAceptar_Click
      Case 27
        cmdCancelar_Click
    End Select
  Else
    Select Case KeyAscii
      Case 13
        txtUsuario(1).SetFocus
      Case 27
        cmdCancelar_Click
    End Select
  End If
End Sub
