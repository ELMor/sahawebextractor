/*
 * Created on 11-sep-2003
 *
 
 
 */
package f8.platform.swa;

import f8.display.CSys;

import f8.kernel.rtExceptions.F8Exception;

import f8.platform.LCDControl;

import f8.types.Matrix;

import waba.fx.Color;
import waba.fx.Font;
import waba.fx.FontMetrics;
import waba.fx.Graphics;
import waba.fx.Image;
import waba.fx.Rect;

import waba.ui.Control;
import waba.ui.Event;
import waba.ui.PenEvent;

/**
 * @author elinares
 *

 
 */
public final class LCDControlImpl extends Control implements LCDControl {
   private double    alto;
   private double    ancho;
   boolean           connect     =false;
   private CSys.Point cursor;
   Font              font;
   private Graphics  gImg;
   private Graphics  gScr;
   private Image     img;
   private CSys      lcd         =new CSys("lcd");
   private CSys.Point mark;
   private CSys.Point currentPoint;
   private boolean   markOn      =false;
   private CSys.Point origin;
   int               ptype;
   Rect              realRect;
   private CSys      usr         =new CSys("usr");

   public LCDControlImpl(double _alto, double _ancho, Font f) {
      alto    =_alto;
      ancho   =_ancho;
      font    =f;
   }

   public void reset(double _ancho, double _alto, double ox, double oy) {
      alto          =_alto;
      ancho         =_ancho;
      this.origin   =lcd.coor(ox, oy);
      refactor();
   }

   public void box(CSys.Point c1, CSys.Point c2) {
      CSys lcdcs=CSys.getCoorSystem("lcd");
      try {
         c1   =lcdcs.transform(c1);
         c2   =lcdcs.transform(c2);
      } catch(F8Exception e) {
      }
      gImg.drawRect(
                    (int)Math.min(c1.v[0], c2.v[0]),
                    (int)Math.min(c1.v[1], c2.v[1]),
                    (int)Math.abs(c1.v[0]-c2.v[0]),
                    (int)Math.abs(c1.v[1]-c2.v[1])
                   );
      optimizedDraw(c1, c2);
   }

   public void circle(CSys.Point c1, CSys.Point c2) {
      CSys lcdcs=CSys.getCoorSystem("lcd");
      try {
         c1   =lcdcs.transform(c1);
         c2   =lcdcs.transform(c2);
      } catch(F8Exception e) {
      }
      gImg.drawCircle((int)c1.v[0], (int)c1.v[1], graphDistance(c1, c2));
      optimizedDraw(c1, c2);
   }

   public void clear() {
      gImg.fillRect(0, 0, width, height);
      gImg.setForeColor(Color.BLACK);
      gImg.drawRect(0, 0, width, height); // paint the frame.
      repaint();
   }

   /* (non-Javadoc)
    * @see f8.kernel.LCDControl#getXCursor()
    */
   public CSys.Point getCursor() {
      return cursor;
   }

   /* (non-Javadoc)
    * @see f8.kernel.LCDControl#getXMark()
    */
   public CSys.Point getMark() {
      return mark;
   }

   private int graphDistance(CSys.Point c1, CSys.Point c2) {
      CSys lcdcs=CSys.getCoorSystem("lcd");
      try {
         c1   =lcdcs.transform(c1);
         c2   =lcdcs.transform(c2);
      } catch(F8Exception e) {
      }
      return (int)(
                Math.sqrt(((c2.v[0]-c1.v[0])*(c2.v[0]-c1.v[0]))
                          +((c2.v[1]-c1.v[1])*(c2.v[1]-c1.v[1]))
                         )
             );
   }

   /* (non-Javadoc)
    * @see f8.platform.LCDControl#initPoint(f8.types.F8Vector)
    */
   public void initPoint(CSys.Point p) {
      currentPoint=p.copy();
   }

   /* (non-Javadoc)
    * @see f8.kernel.LCDControl#isMarkOn()
    */
   public boolean isMarkOn() {
      return markOn;
   }

   public void label() {
   }

   public void lineTo(CSys.Point c1, CSys.Point c2) {
      CSys lcdcs=CSys.getCoorSystem("lcd");
      try {
         c1   =lcdcs.transform(c1);
         c2   =lcdcs.transform(c2);
      } catch(F8Exception e) {
      }
      gImg.drawLine((int)c1.v[0], (int)c1.v[1], (int)c2.v[0], (int)c2.v[1]);
      optimizedDraw(c1, c2);
   }

   /* (non-Javadoc)
    * @see f8.platform.LCDControl#nextPoint(f8.types.F8Vector)
    */
   public void nextPoint(CSys.Point o) {
      lineTo(currentPoint, o);
      currentPoint=o;
   }

   // now that we now our bounds, we can create the image that will hold the drawing
   public void onBoundsChanged() {
      setImage(null);
      realRect=getRect();
      setOrigin(lcd.coor(realRect.width/2, realRect.height/2));
      refactor();
   }

   private void refactor() {
      realRect   =getRect();
      double[] zero=new double[2];
      cursor     =origin.copy();
      lcd.setRectangle(
                       lcd.vector(0, 0), lcd.vector(realRect.width, 0),
                       lcd.vector(0, realRect.height),
                       lcd.vector(realRect.width, realRect.height)
                      );
      double[][] usr2lcd=new double[2][2];
      double[][] des=new double[2][1];
      usr2lcd[0][0]   =realRect.width/ancho;
      usr2lcd[1][1]   =-realRect.height/alto;
      des[0][0]       =origin.v[0];
      des[1][0]       =origin.v[1];
      try {
         usr.setTransform("lcd", usr2lcd, des);
      } catch(F8Exception e) {
      }
   }

   public void onEvent(Event event) {
      switch(event.type) {
         case PenEvent.PEN_DOWN:
         case PenEvent.PEN_DRAG:
            PenEvent pe=(PenEvent)event;
            setCursor(lcd.coor(pe.x-1, pe.y-1));
            repaint();
            break;
      }
   }

   public void onPaint(Graphics g) {
      if(gScr==null) {
         gScr=createGraphics();
      }

      // create the graphics object that will be used to repaint the image
      g.drawImage(img, 0, 0); // draw the image...
      g.drawLine(
                 (int)cursor.v[0]-0, (int)cursor.v[1]-1, (int)cursor.v[0]+0,
                 (int)cursor.v[1]+1
                );
      g.drawLine(
                 (int)cursor.v[0]-1, (int)cursor.v[1]+0, (int)cursor.v[0]+1,
                 (int)cursor.v[1]-0
                );
      if(markOn) {
         g.drawLine(
                    (int)mark.v[0]-1, (int)mark.v[1]-1, (int)mark.v[0]+1,
                    (int)mark.v[1]+1
                   );
         g.drawLine(
                    (int)mark.v[0]-1, (int)mark.v[1]+1, (int)mark.v[0]+1,
                    (int)mark.v[1]-1
                   );
      }
   }

   private void optimizedDraw(CSys.Point c, CSys.Point m) {
      // optimization: draw only the area of the image that has changed
      int xx=(int)Math.max(0, Math.min(m.v[0], c.v[0]));
      int yy=(int)Math.max(0, Math.min(m.v[1], c.v[1]));
      int x2=(int)(Math.max(c.v[0], m.v[0]));
      int y2=(int)(Math.max(c.v[1], m.v[1]));
      int ww=x2-xx+1;
      int hh=y2-yy+1;
      if(((xx+ww)<width)&&((yy+hh)<height)) {
         gScr.copyRect(img, xx, yy, ww, hh, xx, yy);
      }
      repaint();
   }

   public void repl() {
   }

   /* (non-Javadoc)
    * @see f8.platform.LCDControl#setConnected(boolean)
    */
   public void setConnected(boolean c) {
      connect=c;
   }

   public void setCursor(CSys.Point c) {
      cursor.v[0]   =c.v[0];
      cursor.v[1]   =c.v[1];
      repaint();
   }

   public void setImage(Image image) {
      this.img    =(image==null) ? new Image(width, height) : image;
      this.gImg   =img.getGraphics();
      gImg.setForeColor(Color.BLACK);
      gImg.setBackColor(Color.WHITE);
      if(image==null) {
         gImg.fillRect(0, 0, width, height);
      }
      gImg.setForeColor(Color.BLACK);
      gImg.drawRect(0, 0, width, height); // paint the frame.
      repaint();
   }

   public void setMark(CSys.Point c) {
      if(isMarkOn()&&c.equals(mark)) {
         markOn=false;
      } else {
         mark     =c.copy();
         markOn   =true;
      }
      repaint();
   }

   public void setOrigin(CSys.Point c) {
      origin=c;
   }

   /* (non-Javadoc)
    * @see f8.platform.LCDControl#setPlotType(int)
    */
   public void setPlotType(int tipo) {
      ptype=tipo;
   }

   /* (non-Javadoc)
    * @see f8.platform.LCDControl#setProjectation(f8.types.Matrix)
    */
   public void setProjectation(Matrix pm) {
      // TODO Auto-generated method stub
   }

   /* (non-Javadoc)
    * @see f8.platform.LCDControl#showAxes()
    */
   public void showAxes() {
      CSys.Vector xstep=usr.vector(ancho/16, 0);
      CSys.Vector ysize=lcd.vector(0, 1);
      CSys.Vector ystep=usr.vector(0, alto/8);
      CSys.Vector xsize=lcd.vector(1, 0);

      lineTo(lcd.left(), lcd.right());
      lineTo(lcd.top(), lcd.bottom());

      CSys.Point left=lcd.left();

      for(int i=0; i<16; i++) {
         try {
            lineTo(left.minus(ysize.div(2)), left.plus(ysize));
            left=left.plus(xstep);
         } catch(F8Exception e) {
         }
      }

      CSys.Point top=lcd.bottom();

      for(int i=0; i<8; i++) {
         try {
            lineTo(top.minus(xsize.div(2)), top.plus(xsize));
            top=top.plus(ystep);
         } catch(F8Exception e) {
         }
      }
   }

   /* (non-Javadoc)
    * @see f8.platform.LCDControl#showLabels()
    */
   public void showLabels(String mx, String my, String Mx, String My) {
      Color       back       =Color.WHITE;
      Color       fore       =Color.BLACK;
      CSys.Point   l;
      FontMetrics fontmetrics=getFontMetrics(font);
      l=lcd.left();
      gImg.setFont(font);
      gImg.drawText(mx, (int)l.v[0], (int)l.v[1]);
      l=lcd.right();
      gImg.drawText(
                    Mx, (int)l.v[0]-(fontmetrics.getTextWidth(Mx)), (int)l.v[1]
                   );
      l=lcd.bottom();
      gImg.drawText(my, (int)l.v[0], (int)l.v[1]-(fontmetrics.getHeight()));
      l=lcd.top();
      gImg.drawText(My, (int)l.v[0], (int)l.v[1]);
   }

   public void subs() {
   }
}
