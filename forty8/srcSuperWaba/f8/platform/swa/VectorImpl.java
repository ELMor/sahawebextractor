/*
 * Created on 01-ago-2003
 *
 
 
 */
package f8.platform.swa;

import f8.platform.Vector;

/**
 * @author elinares
 *

 
 */
public final class VectorImpl implements Vector {
	waba.util.Vector v=new waba.util.Vector();
	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#add(java.lang.Object)
	 */
	public void add(Object obj) {
		v.add(obj);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#insert(int, java.lang.Object)
	 */
	public void insert(int index, Object obj) {
		v.insert(index,obj);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#del(java.lang.Object)
	 */
	public boolean del(Object obj) {
		return v.del(obj);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#get(int)
	 */
	public Object get(int index) {
		return v.get(index);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#set(int, java.lang.Object)
	 */
	public void set(int index, Object obj) {
		v.set(index,obj);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#getCount()
	 */
	public int getCount() {
		return v.getCount();
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#toObjectArray()
	 */
	public Object[] toObjectArray() {
		return v.toObjectArray();
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#push(java.lang.Object)
	 */
	public void push(Object obj) {
		v.push(obj);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#pop()
	 */
	public Object pop() {
		return v.pop();
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#peek()
	 */
	public Object peek() {
		return v.peek();
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#clear()
	 */
	public void clear() {
		v.clear();
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#size()
	 */
	public int size() {
		return v.size();
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#indexOf(java.lang.Object)
	 */
	public int indexOf(Object elem) {
		return v.indexOf(elem);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#indexOf(java.lang.Object, int)
	 */
	public int indexOf(Object elem, int index) {
		return v.indexOf(elem,index);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#elementAt(int)
	 */
	public Object elementAt(int index) {
		return v.elementAt(index);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#setElementAt(java.lang.Object, int)
	 */
	public void setElementAt(Object obj, int index) {
		v.setElementAt(obj,index);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#removeElementAt(int)
	 */
	public void removeElementAt(int index) {
		v.removeElementAt(index);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#insertElementAt(java.lang.Object, int)
	 */
	public void insertElementAt(Object obj, int index) {
		v.insertElementAt(obj,index);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#addElement(java.lang.Object)
	 */
	public void addElement(Object obj) {
		v.addElement(obj);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#removeElement(java.lang.Object)
	 */
	public boolean removeElement(Object obj) {
		return v.removeElement(obj);
	}

	/* (non-Javadoc)
	 * @see f8.platform.util.Vector#removeAllElements()
	 */
	public void removeAllElements() {
		v.removeAllElements();
	}
	
	public int find(Object k){
		return v.find(k);
	}

}
