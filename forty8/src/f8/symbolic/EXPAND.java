/*
 * Created on 28-ago-2003
 *
 
 
 */
package f8.symbolic;

import antlr.collections.AST;
import f8.Dispatch1;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.op.keyboard.mkl;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.InfixExp;

/**
 * @author elinares
 *

 
 */
public final class EXPAND extends Dispatch1 implements ObjectParserTokenTypes {
   /**
    *
    */
   public EXPAND() {
      super();
      // To-Do Auto-generated constructor stub
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getID()
    */
   public int getID() {
      // To-Do Auto-generated method stub
      return 107;
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getInstance()
    */
   public Storable getInstance() {
      // To-Do Auto-generated method stub
      return new EXPAND();
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
    */
   public void loadState(DataStream ds) {
      // To-Do Auto-generated method stub
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
    */
   public void saveState(DataStream ds) {
      // To-Do Auto-generated method stub
   }

   public static AST expand(AST nodo) {
      nodo=expandChildsOf(nodo);
      switch(nodo.getType()) {
         case MULT:
            return mkl.expandMul(mkl.joinProducts(nodo));
         case CARET:
            return mkl.expandCaret(nodo);
      }
      return nodo;
   }

   public static AST expandChildsOf(AST nodo) {
      AST child=nodo.getFirstChild();
      while(child!=null) {
         AST childExpanded=expand(child);
         if(child!=childExpanded) {
            mkl.subst(nodo, child, childExpanded);
         }
         child=child.getNextSibling();
      }
      return nodo;
   }

   public String toString() {
      return "EXPAND";
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch1#prfInfix(f8.kernel.types.InfixExp)
	 */
	public Stackable prfInfix(InfixExp a) throws F8Exception {
		AST tree=mkl.copyAST(a.getAST());
		return (new InfixExp(expand(tree)));
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
