package f8.types;

import f8.CL;
import f8.Command;
import f8.Stackable;
import f8.op.keyboard.FLECHA;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.platform.UtilFactory;
import f8.platform.Vector;
import f8.types.utils.CommandSequence;
import f8.types.utils.Ejecutable;

public final class Proc extends CommandSequence implements Ejecutable{
   public Proc(Vector a) {
      super(a);
   }

   public Stackable copia() {
      Vector pCopy=UtilFactory.newVector();
      for(int i=0; i<obList.size(); i++) {
         if(obList.elementAt(i) instanceof Stackable) {
            pCopy.add(((Stackable)obList.elementAt(i)).copia());
         } else {
            pCopy.add(obList.elementAt(i));
         }
      }
      return new Proc(pCopy);
   }

   public String getTypeName() {
      return "Proc";
   }

   public int getID() {
      return 40;
   }

   public Storable getInstance() {
      return new Proc(null);
   }

   public void loadState(DataStream ds) {
      super.loadState(ds);
      int sz       =ds.readInt();
      obList   =UtilFactory.newVector();
      for(int i=0; i<sz; i++) {
         obList.add((Command)Command.loadFromStorage(ds));
      }
   }

   public void saveState(DataStream ds) {
      super.saveState(ds);
      ds.writeInt(obList.size());
      for(int i=0; i<obList.size(); i++) {
      	Command c=((Command)(obList.elementAt(i)));
         ds.writeInt(c.getID());
         c.saveState(ds);
      }
   }

   public Command item(int i) {
      return (Command)(obList.elementAt(i));
   }

   public String toString() {
      String s="\u00AB ";

      for(int i=0; i<obList.size(); i++) {
         Command next=(Command)obList.elementAt(i);
         s+=(next.toString()+" ");
      }

      s+="\u00BB";

      String ret=getTag();
      if(!ret.equals("")) {
         ret+=":";
      }
      return ret+s;
   }

   /* (non-Javadoc)
    * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
    */
   public boolean decompose(CL cl) {
      return super.decompose(cl);
   }
   
   public int numOfLocalVars(){
   	if(obList.elementAt(0) instanceof FLECHA){
   		return ((FLECHA)obList.elementAt(0)).size();
   	}
   	return 0;
   }

	/* (non-Javadoc)
	 * @see f8.types.utils.Ejecutable#getEjecutable()
	 */
	public CommandSequence getEjecutable() {
		return this;
	}

}
