package f8.types;

import antlr.collections.AST;
import f8.CL;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.MathKernel;
import f8.platform.Storable;
import f8.platform.UtilFactory;
import f8.types.utils.ComplexValue;
import f8.types.utils.DoubleValue;
import f8.types.utils.IntValue;

public final class Int extends Stackable implements IntValue, DoubleValue,
                                                          ComplexValue {
   public int n;

   public Int(int n) {
      this.n=n;
   }

   public Int(AST definition) {
      String     def   =definition.getText();
      String     digits=def.substring(1, def.length()-1);
      MathKernel mk    =UtilFactory.getMath();
      char       type  =def.charAt(def.length()-1);
      switch(type) {
         case 'D':
         case 'd':
            n=mk.fromDEC(digits);
            break;
         case 'H':
         case 'h':
            n=mk.fromHEX(digits);
            break;
         case 'O':
         case 'o':
            n=mk.fromOCT(digits);
            break;
         case 'B':
         case 'b':
            n=mk.fromBIN(digits);
      }
   }

   public Stackable copia() {
      return new Int(n);
   }

   public String getTypeName() {
      return "Int";
   }

   public int getID() {
      return 26;
   }

   public Storable getInstance() {
      return new Int(1);
   }

   public void loadState(DataStream ds) {
      super.loadState(ds);
      n=ds.readInt();
   }

   public void saveState(DataStream ds) {
      super.saveState(ds);
      ds.writeInt(n);
   }

   public double[] complexValue() {
      double[] x=new double[2];
      x[0]   =n;
      x[1]   =0;

      return (x);
   }

   public double doubleValue() {
      return ((double)n);
   }

   public int intValue() {
      return (n);
   }

   public String toString() {
      CL         cl      =StaticCalcGUI.theGUI.getCalcLogica();
      MathKernel mk      =UtilFactory.getMath();
      String     retAfter=mk.format(n, cl.getSetting(CL.INT_MOD));
      switch(cl.getSetting(CL.INT_MOD)) {
         case 2:
            return "# "+retAfter+" h";
         case 3:
            return "# "+retAfter+" o";
         case 4:
            return "# "+retAfter+" b";
      }
      String ret=getTag();
      if(!ret.equals("")) {
         ret+=":";
      }
      return ret+"# "+retAfter+" d";
   }

   /* (non-Javadoc)
    * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
    */
   public boolean decompose(CL cl) {
      return false;
   }
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if(obj instanceof Int){
			return n==((Int)obj).n;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return equals(obj);
	}

}
