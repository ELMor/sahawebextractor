package f8.types.utils;

import f8.Command;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.IndexRangeException;

public abstract class Array extends Stackable {

   abstract public int size();

   abstract public void put(Command v, int n) throws F8Exception;

   abstract public Command get(int n) throws IndexRangeException;

   abstract public Stackable copia();

   public String getTypeName() {
      return "Array";
   }

}
