package f8.types.utils;

public interface DoubleValue {
   public double doubleValue();
}
