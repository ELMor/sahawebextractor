package f8.types.utils;

import antlr.collections.AST;
import f8.CL;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.op.keyboard.Derivable;
import f8.op.keyboard.mkl;
import f8.platform.DataStream;
import f8.types.Complex;


public abstract class Const extends Stackable implements Derivable {
   public void exec() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.push(new Complex((Complex)value()));
   }

   public abstract Stackable value();

   public String getTypeName() {
      return "Const";
   }

   public Stackable copia() {
      return this;
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST args, String var) {
      return mkl.num(0);
   }
	/* (non-Javadoc)
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DataStream ds) {
		super.loadState(ds);
	}

	/* (non-Javadoc)
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DataStream ds) {
		super.saveState(ds);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return this==obj;
	}

	/* (non-Javadoc)
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return this==obj;
	}

}
