package f8.op.units;

import f8.CL;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.TempUnit;
import f8.types.Unit;

public final class UBASE extends NonAlgebraic implements ObjectParserTokenTypes {
   public UBASE() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 106;
   }

   public Storable getInstance() {
      return new UBASE();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("UBASE");
   }

   /* (non-Javadoc)
    * @see f8.kernel.StkOb#exec()
    */
   public void exec() throws F8Exception {
      CL                cl =StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(1)){
      	Stackable u=cl.peek();
      	if(u instanceof Unit || u instanceof TempUnit){
      		cl.pop();
				Unit uu=(Unit)u;
				cl.push(uu.ubase());
      	}else{
			throw new BadArgumentTypeException(this);
      	}
      }else{
		throw new TooFewArgumentsException(this);
      }
   }

}
