package f8.op.prg.stk;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.platform.UtilFactory;
import f8.platform.Vector;
import f8.types.Double;

public final class ROLLD extends NonAlgebraic {
   public ROLLD() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 11;
   }

   public Storable getInstance() {
      return new ROLLD();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL  cl =StaticCalcGUI.theGUI.getCalcLogica();
      int rot;
      if(cl.check(1)) {
         Command a=cl.pop();
         if(a instanceof Double) {
            rot=(int)((Double)a).x;
            if(cl.check(rot)) {
               Vector gb=UtilFactory.newVector();
               for(int i=0; i<rot; i++) {
                  gb.add(cl.pop());
               }
               cl.push((Stackable)gb.elementAt(rot-1));
               for(int i=0; i<(rot-1); i++) {
                  cl.push((Stackable)gb.elementAt(i));
               }
            } else {
               throw new TooFewArgumentsException(this);
            }
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("ROLLD");
   }
}
