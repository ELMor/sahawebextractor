package f8.op.prg.stk;

import f8.CL;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class DROP extends NonAlgebraic {
   public DROP() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 8;
   }

   public Storable getInstance() {
      return new DROP();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(1)) {
         cl.pop(1);
      } else {
			throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("DROP");
   }
}
