package f8.op.prg.stk;

import f8.CL;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class ROT extends NonAlgebraic {
   public ROT() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 12;
   }

   public Storable getInstance() {
      return new ROT();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(3)) {
         Stackable c=cl.pop();
         Stackable b=cl.pop();
         Stackable a=cl.pop();
         cl.push(b);
         cl.push(c);
         cl.push(a);
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("ROT");
   }
}
