/*
 * Created on 22-ago-2003
 *
 
 
 */
package f8.op.prg.brch;

import f8.CL;
import f8.Command;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.MathKernel;
import f8.platform.UtilFactory;
import f8.types.Double;
import f8.types.Int;

/**
 * @author elinares
 *

 
 */
public final class UtilHelper {
   static MathKernel mk=UtilFactory.getMath();
   static CL         cl=StaticCalcGUI.theGUI.getCalcLogica();

   public static int evalCondition() throws F8Exception {
      if(cl.size()==0) {
			throw new TooFewArgumentsException(new IF(null));
      }

      Command c=cl.pop();

      if(
         (c instanceof Int||c instanceof Double)
             &&(mk.toDouble(c.toString())==0)
        ) {
         return 0;
      }

      return 1;
   }
}
