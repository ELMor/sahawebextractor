/*
 * Created on 14-sep-2003
 *
 
 
 */
package f8.op.prg.obj;

import f8.CL;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.F8String;

/**
 * @author elinares
 *

 
 */
public class TO_TAG extends NonAlgebraic {

	/* (non-Javadoc)
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 600;
	}

	/* (non-Javadoc)
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/* (non-Javadoc)
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DataStream ds) {

	}

	/* (non-Javadoc)
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DataStream ds) {

	}

	

	/* (non-Javadoc)
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec() throws F8Exception{
		CL cl=StaticCalcGUI.theGUI.getCalcLogica();
		if(cl.check(2)){
				Stackable obj=cl.peek(1);
				Stackable tag=cl.peek(0);
				if(tag instanceof F8String){
					cl.pop();
					tag.setTag(null);
					obj.setTag(tag.toString());
				}else{
					throw new BadArgumentTypeException(this);
				}
		}else{
			throw new TooFewArgumentsException(this);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "->TAG";
	}

}
