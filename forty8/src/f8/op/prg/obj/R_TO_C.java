/*
 * Created on 14-sep-2003
 *
 
 
 */
package f8.op.prg.obj;

import f8.CL;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;

/**
 * @author elinares
 *

 
 */
public class R_TO_C extends NonAlgebraic {
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
	   return "R->C";
	}
   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getID()
    */
   public int getID() {
      return 3061;
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getInstance()
    */
   public Storable getInstance() {
      return this;
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
    */
   public void loadState(DataStream ds) {
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
    */
   public void saveState(DataStream ds) {
   }

   /* (non-Javadoc)
    * @see f8.kernel.StkOb#exec()
    */
   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(2)) {
      	Stackable sre=cl.peek(1);
      	Stackable sim=cl.peek(0);
			if(sre instanceof Double && sim instanceof Double){
				cl.pop(2);
				cl.push(new Complex(((Double)sre).x,((Double)sim).x));
			}else{
				throw new BadArgumentTypeException(this);
			}
      }else{
      	throw new TooFewArgumentsException(this);
      }
   }

}
