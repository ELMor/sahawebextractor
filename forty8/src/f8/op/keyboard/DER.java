/*
 * Created on 28-ago-2003
 *
 
 
 */
package f8.op.keyboard;

import antlr.collections.AST;
import f8.CL;
import f8.Dispatch2;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.InfixExp;
import f8.types.Literal;

/**
 * @author elinares
 *

 
 */
public final class DER extends Dispatch2 
implements ObjectParserTokenTypes {
   /**
    *
    */
   public DER() {
      //Aqui estaba systemInstall
   }

   /* (non-Javadoc)
	* @see f8.kernel.Command#isAlgebraic()
	*/
   public boolean isAlgebraic() {
	   return true;
   }
   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getID()
    */
   public int getID() {
      // To-Do Auto-generated method stub
      return 101;
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getInstance()
    */
   public Storable getInstance() {
      // To-Do Auto-generated method stub
      return new DER();
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
    */
   public void loadState(DataStream ds) {
      // To-Do Auto-generated method stub
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
    */
   public void saveState(DataStream ds) {
      // To-Do Auto-generated method stub
   }

   public static AST deriveFunction(AST exp, String var)
                             throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      Literal lDer;
      switch(exp.getType()) {
         case UNIT:
         case NUMBER:
            return mkl.num(0);
         case ID:
            lDer=new Literal(exp.getText());
            return lDer.deriveWithArgs(exp.getFirstChild(), var);
         default:
            Derivable der=(Derivable)cl.lookup(exp.getText());
            if(der!=null) {
               return der.deriveWithArgs(exp.getFirstChild(), var);
            } else {
					lDer=new Literal(exp.getText());
					return lDer.deriveWithArgs(exp.getFirstChild(), var);
            }
      }
   }

   public String toString() {
      return "\u0088";
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfInfixExpInfixExp(f8.kernel.types.InfixExp, f8.kernel.types.InfixExp)
	 */
	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b)
		throws F8Exception {
			String          vder=null;
		   vder=InfixExp.isLiteral(b);
			if(vder==null) {
			   throw new BadArgumentTypeException(this);
			} else {
			   return new InfixExp(deriveFunction(a.getAST(), vder));
			}
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
