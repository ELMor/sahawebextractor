/*
 * Created on 01-oct-2003
 *
 
 
 */
package f8.op.keyboard;

import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.types.Double;
import f8.types.Int;
import f8.types.Unit;

/**
 * @author elinares
 *

 
 */
public abstract class Comparison extends Dispatch2 {
	public abstract boolean compare(double v1, double v2);
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfIntInt(f8.kernel.types.Int, f8.kernel.types.Int)
	 */
	public Stackable prfIntInt(Int a, Int b) throws F8Exception {
		boolean v=compare(a.n,b.n);
		return (new Double(v ? 1 : 0));
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfIntDouble(f8.kernel.types.Int, f8.kernel.types.Double)
	 */
	public Stackable prfIntDouble(Int a, Double b) throws F8Exception {
		boolean v=compare(a.n,b.x);
		return (new Double(v ? 1 : 0));
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfDoubleInt(f8.kernel.types.Double, f8.kernel.types.Int)
	 */
	public Stackable prfDoubleInt(Double a, Int b) throws F8Exception {
		boolean v=compare(a.x,b.n);
		return (new Double(v ? 1 : 0));
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfDoubleDouble(f8.kernel.types.Double, f8.kernel.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		boolean v=compare(a.x,b.x);
		return (new Double(v ? 1 : 0));
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfUnitUnit(f8.kernel.types.Unit, f8.kernel.types.Unit)
	 */
	public Stackable prfUnitUnit(Unit a, Unit b) throws F8Exception {
		Unit converted=b.convert(a);
		return new Double(compare(a.getValor(),converted.getValor())?1:0);
	}

	
	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
