package f8.op.keyboard;

import antlr.collections.AST;
import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.DivideByZeroException;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;
import f8.types.InfixExp;
import f8.types.Int;
import f8.types.Unit;
import f8.types.utils.ComplexValue;

public final class DIV extends Dispatch2 implements Derivable {
   public DIV() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 81;
   }

   public Storable getInstance() {
      return new DIV();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("/");
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST args, String var) throws F8Exception {
      AST a1;
      AST a2;
      a1   =args;
      a2   =a1.getNextSibling();
      return mkl.res(
                     mkl.div(DER.deriveFunction(a1, var), a2),
                     mkl.mul(
                             a1,
                             mkl.div(
                                     DER.deriveFunction(a2, var),
                                     mkl.ele(a2, mkl.num(2))
                                    )
                            )
                    );
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfIntInt(f8.kernel.types.Int, f8.kernel.types.Int)
	 */
	public Stackable prfIntInt(Int a, Int b) throws F8Exception {
		Int       A =(Int)a;
		Int       B =(Int)b;
		int       na=A.n;
		int       nb=B.n;
		Stackable c;

		if(nb!=0) {
		   if((na%nb)==0) {
			  c=new Int(na/nb);
		   } else {
			  double x=(double)na;
			  double y=(double)nb;
			  c=new Double(x/y);
		   }
			return c;
		} else {
		   throw new DivideByZeroException(this);
		}
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfDoubleDouble(f8.kernel.types.Double, f8.kernel.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		if(b.doubleValue()!=0) {
		   return new Double(a.doubleValue()/b.doubleValue());
		} else {
		   throw new DivideByZeroException(this);
		}
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfComplexComplex(f8.kernel.types.Complex, f8.kernel.types.Complex)
	 */
	public Stackable prfComplexComplex(Complex a, Complex b)
		throws F8Exception {
			double[] c=div(a,b);
			return new Complex(c[0], c[1]);
	}
	
	public static double[] div(ComplexValue a, ComplexValue b)
						throws DivideByZeroException {
	   double[] c=((ComplexValue)a).complexValue();
	   double[] d=((ComplexValue)b).complexValue();
	   double   r=(d[0]*d[0])+(d[1]*d[1]);

	   // (a+ib)/(c+id) = (a+ib)(c-id)/r^2
	   if(r!=0) {
		  double[] e=new double[2];
		  e[0]   =((c[0]*d[0])+(c[1]*d[1]))/r;
		  e[1]   =((c[1]*d[0])-(c[0]*d[1]))/r;

		  return (e);
	   } else {
		  throw new DivideByZeroException(new DIV());
	   }
	}


	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfDoubleUnit(f8.kernel.types.Double, f8.kernel.types.Unit)
	 */
	public Stackable prfDoubleUnit(Double a, Unit b) throws F8Exception {
		double val=a.x/b.getValor();
		return (new Unit(val, mkl.inv(b.getUnidad())));
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfUnitDouble(f8.kernel.types.Unit, f8.kernel.types.Double)
	 */
	public Stackable prfUnitDouble(Unit a, Double b) throws F8Exception {
		return  new Unit(a.getValor()/b.x,a.getUnidad());
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfUnitUnit(f8.kernel.types.Unit, f8.kernel.types.Unit)
	 */
	public Stackable prfUnitUnit(Unit a, Unit b) throws F8Exception {
		double v  =a.getValor()/b.getValor();
		AST    u  =mkl.div(a.getUnidad(), b.getUnidad());
		return new Unit(v, u);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfInfixExpInfixExp(f8.types.InfixExp, f8.types.InfixExp)
	 */
	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b)
		throws F8Exception {
		return new InfixExp(mkl.div(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfComplexInfixExp(f8.types.Complex, f8.types.InfixExp)
	 */
	public Stackable prfComplexInfixExp(Complex a, InfixExp b)
		throws F8Exception {
			return new InfixExp(mkl.div(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleInfixExp(f8.types.Double, f8.types.InfixExp)
	 */
	public Stackable prfDoubleInfixExp(Double a, InfixExp b)
		throws F8Exception {
			return new InfixExp(mkl.div(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfInfixExpComplex(f8.types.InfixExp, f8.types.Complex)
	 */
	public Stackable prfInfixExpComplex(InfixExp a, Complex b)
		throws F8Exception {
			return new InfixExp(mkl.div(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfInfixExpDouble(f8.types.InfixExp, f8.types.Double)
	 */
	public Stackable prfInfixExpDouble(InfixExp a, Double b)
		throws F8Exception {
			return new InfixExp(mkl.div(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfComplexDouble(f8.types.Complex, f8.types.Double)
	 */
	public Stackable prfComplexDouble(Complex a, Double b) throws F8Exception {
		return a.div(new Complex(b.x,0.0)).mutate();
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleComplex(f8.types.Double, f8.types.Complex)
	 */
	public Stackable prfDoubleComplex(Double a, Complex b) throws F8Exception {
		return new Complex(a.x,0).div(b).mutate();
	}

}
