package f8.op.keyboard;

import antlr.collections.AST;
import f8.CL;
import f8.Dispatch2;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.InvalidDimensionException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;
import f8.types.F8Vector;
import f8.types.InfixExp;
import f8.types.Int;
import f8.types.Matrix;
import f8.types.TempUnit;
import f8.types.Unit;

public final class MINUS extends Dispatch2 implements Derivable {
   public MINUS() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 87;
   }

   public Storable getInstance() {
      return new MINUS();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("-");
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST args, String var) throws F8Exception {
      AST a1;
      AST a2;
      AST a3;
      a1   =args;
      a2   =a1.getNextSibling();
      return mkl.res(DER.deriveFunction(a1, var), DER.deriveFunction(a2, var));
   }

   /* (non-Javadoc)
    * @see f8.kernel.Dispatch2#prfIntInt(f8.kernel.types.Int, f8.kernel.types.Int)
    */
   public Stackable prfIntInt(Int a, Int b) throws F8Exception {
      return new Int(a.n-b.n);
   }

   /* (non-Javadoc)
    * @see f8.kernel.Dispatch2#prfDoubleDouble(f8.kernel.types.Double, f8.kernel.types.Double)
    */
   public Stackable prfDoubleDouble(Double a, Double b)
                             throws F8Exception {
      return new Double(a.x-b.x);
   }

   /* (non-Javadoc)
    * @see f8.kernel.Dispatch2#prfComplexComplex(f8.kernel.types.Complex, f8.kernel.types.Complex)
    */
   public Stackable prfComplexComplex(Complex a, Complex b)
                               throws F8Exception {
      double[] x=a.complexValue();
      double[] y=b.complexValue();
      return new Complex(x[0]-y[0], x[1]-y[1]);
   }

   /* (non-Javadoc)
    * @see f8.kernel.Dispatch2#prfF8StringF8Vector(f8.kernel.types.F8String, f8.kernel.types.F8Vector)
    */
   public Stackable prfF8VectorF8Vector(F8Vector a, F8Vector it)
                                 throws F8Exception {
      double[] y =it.x;
      int      nx=a.x.length;
      int      ny=y.length;

      CL       cl=StaticCalcGUI.theGUI.getCalcLogica();

      if(nx==ny) {
         double[] z=new double[nx];

         for(int i=0; i<nx; i++) {
            z[i]=a.x[i]-y[i];
         }

         return (new F8Vector(z));
      } else {
         throw new InvalidDimensionException(this);
      }
   }

   /* (non-Javadoc)
    * @see f8.kernel.Dispatch2#prfMatrixMatrix(f8.kernel.types.Matrix, f8.kernel.types.Matrix)
    */
   public Stackable prfMatrixMatrix(Matrix a, Matrix it)
                             throws F8Exception {
      double[][] y=it.x;

      if((a.r==it.r)&&(a.c==it.c)) {
         double[][] z=new double[a.r][a.c];

         for(int i=0; i<a.r; i++) {
            for(int j=0; j<a.c; j++) {
               z[i][j]=a.x[i][j]-y[i][j];
            }
         }

         return (new Matrix(z));
      } else {
         throw new InvalidDimensionException(this);
      }
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfUnitUnit(f8.kernel.types.Unit, f8.kernel.types.Unit)
	 */
	public Stackable prfUnitUnit(Unit a, Unit b) throws F8Exception {
		Unit converted=b.convert(a);
		return new Unit(a.getValor()-converted.getValor(), a.getUnidad());
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfTempUnitTempUnit(f8.kernel.types.TempUnit, f8.kernel.types.TempUnit)
	 */
	public Stackable prfTempUnitTempUnit(TempUnit a, TempUnit b)
		throws F8Exception {
			return (new TempUnit(
								 a.ubase().getValor()-b.ubase().getValor(),
								 mkl.fca("K", null)
								)
				   );
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfInfixExpInfixExp(f8.types.InfixExp, f8.types.InfixExp)
	 */
	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b)
		throws F8Exception {
		return new InfixExp(mkl.res(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfComplexInfixExp(f8.types.Complex, f8.types.InfixExp)
	 */
	public Stackable prfComplexInfixExp(Complex a, InfixExp b)
		throws F8Exception {
			return new InfixExp(mkl.res(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleInfixExp(f8.types.Double, f8.types.InfixExp)
	 */
	public Stackable prfDoubleInfixExp(Double a, InfixExp b)
		throws F8Exception {
			return new InfixExp(mkl.res(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfInfixExpComplex(f8.types.InfixExp, f8.types.Complex)
	 */
	public Stackable prfInfixExpComplex(InfixExp a, Complex b)
		throws F8Exception {
			return new InfixExp(mkl.res(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfInfixExpDouble(f8.types.InfixExp, f8.types.Double)
	 */
	public Stackable prfInfixExpDouble(InfixExp a, Double b)
		throws F8Exception {
			return new InfixExp(mkl.res(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfComplexDouble(f8.types.Complex, f8.types.Double)
	 */
	public Stackable prfComplexDouble(Complex a, Double b) throws F8Exception {
		return new Complex(a.re+b.x,a.im).mutate();
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleComplex(f8.types.Double, f8.types.Complex)
	 */
	public Stackable prfDoubleComplex(Double a, Complex b) throws F8Exception {
		return new Complex(b.re+a.x,b.im).mutate();
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfInfixExpUnit(f8.types.InfixExp, f8.types.Unit)
	 */
	public Stackable prfInfixExpUnit(InfixExp a, Unit b) throws F8Exception {
		return new InfixExp(mkl.res(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfUnitInfixExp(f8.types.Unit, f8.types.InfixExp)
	 */
	public Stackable prfUnitInfixExp(Unit a, InfixExp b) throws F8Exception {
		return new InfixExp(mkl.res(a.getAST(),b.getAST()));
	}

}
