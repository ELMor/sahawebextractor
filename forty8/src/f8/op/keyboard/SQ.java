package f8.op.keyboard;

import antlr.collections.AST;
import f8.Dispatch1;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;
import f8.types.Unit;

public final class SQ extends Dispatch1 implements Derivable  {
   public SQ() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 512;
   }

   /* (non-Javadoc)
	* @see f8.kernel.Command#isAlgebraic()
	*/
   public boolean isAlgebraic() {
	   return true;
   }
   public Storable getInstance() {
      return new SQ();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public Stackable perform(double x) {
      return new Double(Math.pow(x, 2));
   }

   public String toString() {
      return ("SQ");
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST arg, String var) throws F8Exception {
      return mkl.mul(mkl.num(2), DER.deriveFunction(arg, var));
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#f(f8.kernel.types.Unit)
    */
   public Unit perform(Unit a) {
      return new Unit(
                      a.getValor()*a.getValor(),
                      mkl.ele(a.getUnidad(), mkl.num(2))
                     );
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#undo()
    */
   public String undo() {
      return "\u0083";
   }
}
