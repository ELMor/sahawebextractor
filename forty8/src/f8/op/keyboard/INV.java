package f8.op.keyboard;

import antlr.collections.AST;
import f8.Dispatch1;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;
import f8.types.Unit;

public final class INV extends Dispatch1 implements Derivable  {
   public INV() {
      //Aqui estaba systemInstall
   }

   /* (non-Javadoc)
	* @see f8.kernel.Command#isAlgebraic()
	*/
   public boolean isAlgebraic() {
	   return true;
   }
   public int getID() {
      return 105;
   }

   public Storable getInstance() {
      return new INV();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("INV");
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST args, String var) throws F8Exception {
      return DER.deriveFunction(mkl.div(mkl.num(1), args), var);
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#f(double)
    */
   public Stackable prfDouble(Double x) throws F8Exception {
      return new Double(1/x.x);
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#f(f8.kernel.types.Unit)
    */
   public Stackable prfUnit(Unit a) throws F8Exception {
      return new Unit(1/a.getValor(), mkl.div(mkl.num(1), a.getUnidad()));
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#undo()
    */
   public String undo() {
      return "INV";
   }
	/* (non-Javadoc)
	 * @see f8.Dispatch1#prfComplex(f8.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return new Complex(1,0).div(x);
	}

}
