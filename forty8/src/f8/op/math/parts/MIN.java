package f8.op.math.parts;

import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;
import f8.types.Int;
import f8.types.Unit;

public final class MIN extends Dispatch2 {
   public MIN() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 3004;
   }

   public String toString() {
	  return ("MIN");
   }

   public Storable getInstance() {
      return new MIN();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	


	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		if(a.x<b.x)
			return a;
		return b;
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleInt(f8.types.Double, f8.types.Int)
	 */
	public Stackable prfDoubleInt(Double a, Int b) throws F8Exception {
		if(a.x<b.n)
			return a;
		return b;
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfIntDouble(f8.types.Int, f8.types.Double)
	 */
	public Stackable prfIntDouble(Int a, Double b) throws F8Exception {
		if(a.n<b.x)
			return a;
		return b;
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfIntInt(f8.types.Int, f8.types.Int)
	 */
	public Stackable prfIntInt(Int a, Int b) throws F8Exception {
		if(a.n<b.n)
			return a;
		return b;
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfUnitInt(f8.types.Unit, f8.types.Int)
	 */
	public Stackable prfUnitUnit(Unit a, Unit b) throws F8Exception {
		Unit aub=a.ubase();
		Unit bub=b.ubase();
		if(aub.getValor()<bub.getValor())
			return a;
		return b;
	}

	/* (non-Javadoc)
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
