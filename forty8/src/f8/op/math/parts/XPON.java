package f8.op.math.parts;

import f8.Dispatch1;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;

public final class XPON extends Dispatch1 {
   public XPON() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 3011;
   }

   public String toString() {
	  return ("XPON");
   }

   public Storable getInstance() {
      return new XPON();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		int e=(int)(Math.log(a.x)/Math.log(10));
		return new Double(e);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
