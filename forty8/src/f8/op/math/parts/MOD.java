package f8.op.math.parts;

import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;
import f8.types.Int;

public final class MOD extends Dispatch2 {
   public MOD() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 3006;
   }

   public String toString() {
	  return ("MOD");
   }

   public Storable getInstance() {
      return new MOD();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

	public boolean isAlgebraic() {
		return true;
	}
	


	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		return new Double(a.x%b.x);
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleInt(f8.types.Double, f8.types.Int)
	 */
	public Stackable prfDoubleInt(Double a, Int b) throws F8Exception {
		return new Double(a.x%b.n);
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfIntDouble(f8.types.Int, f8.types.Double)
	 */
	public Stackable prfIntDouble(Int a, Double b) throws F8Exception {
		return new Double(a.n%b.x);
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfIntInt(f8.types.Int, f8.types.Int)
	 */
	public Stackable prfIntInt(Int a, Int b) throws F8Exception {
		return new Double(a.n%b.n);
	}

	/* (non-Javadoc)
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
