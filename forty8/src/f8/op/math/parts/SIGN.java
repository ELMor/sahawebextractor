package f8.op.math.parts;

import f8.Dispatch1;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;
import f8.types.Int;
import f8.types.utils.IntValue;

public final class SIGN extends Dispatch1 {
   public SIGN() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 3001;
   }

   public Storable getInstance() {
      return new SIGN();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("SIGN");
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch1#prfInt(f8.kernel.types.Int)
	 */
	public Stackable prfInt(Int a) throws F8Exception {
		int A=((IntValue)a).intValue();

		if(A<0) {
		   A=-A;
		}
		return new Int(A);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		double x=a.doubleValue();

		if(x<0) {
		   x=-1;
		}else if(x==0){
			x=0;
		}else{
			x=1;
		}

		return new Double(x);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex a) throws F8Exception {
		return a.div(new Complex(((Double)new ABS().prfComplex(a)).x,0));
	}


	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
