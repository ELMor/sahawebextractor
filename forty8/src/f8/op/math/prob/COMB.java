package f8.op.math.prob;

import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.BadArgumentValueException;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;

public final class COMB extends Dispatch2 {
   public COMB() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 3019;
   }

   public String toString() {
	  return ("COMB");
   }

   public Storable getInstance() {
      return new COMB();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		int n=(int)a.x,m=(int)b.x;
		if(n<m)
			throw new BadArgumentValueException(this);
		if(n==m)
			return new Double(1);
		double ret=1;
		for(int i=n;i>m;i--)
			ret*=i;
		for(int j=n-m;j>1;j--)
			ret/=j;
		return new Double(ret);
	}

}
