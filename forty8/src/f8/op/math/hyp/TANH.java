package f8.op.math.hyp;

import antlr.collections.AST;
import f8.Stackable;
import f8.Trigonometric;
import f8.kernel.rtExceptions.F8Exception;
import f8.op.keyboard.DER;
import f8.op.keyboard.mkl;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;

public final class TANH extends Trigonometric {

   /* (non-Javadoc)
    * @see f8.kernel.Command#isAlgebraic()
    */
   public boolean isAlgebraic() {
      return true;
   }

   public int getID() {
      return 3028;
   }
   /* (non-Javadoc)
	* @see f8.kernel.op.num.Fcn#undo()
	*/
   public String undo() {
	  return "ATANH";
   }

   public Storable getInstance() {
      return new TANH();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public Stackable prfDouble(Double x) {
		return new Complex(x.x,0).tanh().mutate();
   }

   public String toString() {
      return ("TANH");
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST args, String var) throws F8Exception {
      return mkl.res(
                     mkl.num(0),
                     mkl.mul(
                             DER.deriveFunction(args, var),
                             mkl.div(
                                     mkl.num(1),
                                     mkl.ele(
                                             mkl.res(
                                                     mkl.num(1),
                                                     mkl.ele(args, mkl.num(2))
                                                    ), mkl.num(0.5)
                                            )
                                    )
                            )
                    );
   }

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.tanh();
	}

}
