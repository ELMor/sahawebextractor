package f8.op.math.vectr;

import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.InvalidDimensionException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.F8Vector;

public final class DOT extends Dispatch2 {
   public DOT() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 3039;
   }

   public Storable getInstance() {
      return new DOT();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("DOT");
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfF8VectorF8Vector(f8.kernel.types.F8Vector, f8.kernel.types.F8Vector)
	 */
	public Stackable prfF8VectorF8Vector(F8Vector a, F8Vector it)
		throws F8Exception {
			double[] y =it.x;
			int      nx=a.x.length;
			int      ny=y.length;
			double[] z =new double[nx];

			// x[0] x[1] x[2]
			// y[0] y[1] y[2]
			if((nx==ny)&&(nx==3)) {
			   z[0]   =(a.x[1]*y[2])-(a.x[2]*y[1]);
			   z[1]   =(a.x[2]*y[0])-(a.x[0]*y[2]);
			   z[2]   =(a.x[0]*y[1])-(a.x[1]*y[0]);

			   return (new F8Vector(z));
			} else {
			   throw new InvalidDimensionException(this);
			}
	}


	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
