package f8.constants;

import f8.CL;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.utils.Const;

public final class I extends Const {
   public I() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 65;
   }

   public Storable getInstance() {
      return new I();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.push(new Complex(0, 1));
   }

   public String toString() {
      return ("i");
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.types.utils.Const#value()
	 */
	public Stackable value() {
		return new Complex(0,1);
	}

	/* (non-Javadoc)
	 * @see f8.Stackable#decompose(f8.CL)
	 */
	public boolean decompose(CL cl) throws F8Exception {
		return false;
	}

}
