package f8.constants;

import f8.CL;
import f8.Stackable;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;
import f8.types.utils.Const;

public final class PI extends Const {
   public PI() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 77;
   }

   public Storable getInstance() {
      return new PI();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public Stackable value() {
      return new Double(Math.PI);
   }

   public String toString() {
      return ("\u0087");
   }
	/* (non-Javadoc)
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose(CL cl) {
		return false;
	}

}
