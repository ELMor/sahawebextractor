/*
 * Created on 21-sep-2003
 *
 
 
 */
package f8.solver;

import antlr.collections.AST;

import f8.CL;
import f8.Stackable;
import f8.StaticCalcGUI;

import f8.kernel.rtExceptions.BadArgumentValueException;
import f8.kernel.rtExceptions.CircularReferenceException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.NonEmptyDirException;
import f8.kernel.rtExceptions.UndefinedException;

import f8.kernel.yacc.ObjectParserTokenTypes;

import f8.op.keyboard.mkl;

import f8.op.prg.stk.EVAL;
import f8.op.prg.stk.SWAP;

import f8.platform.Vector;

import f8.storage.PURGE;
import f8.storage.STO;

import f8.types.Double;
import f8.types.InfixExp;

import f8.types.utils.CommandSequence;

/**
 * @author elinares
 *

 
 */
public class EqSolv {
   static CL cl;

   public static void solve(InfixExp exp, String name)
                     throws F8Exception {
      //Primero evaluamos exp para comprobar que la unica variable
      //que puede no estar definida es name
      cl=StaticCalcGUI.theGUI.getCalcLogica();
      checkCircularity(exp, name);
      AST e=exp.getAST();
      if(e.getType()==ObjectParserTokenTypes.ASSIGN) {
         exp=
                        new InfixExp(mkl.res(
                                             e.getFirstChild(),
                                             e.getFirstChild().getNextSibling()
                                            )
                                    );
      }
      exp=optimize(exp);

      CommandSequence run=InfixExp.decode(exp.getAST());

      Solverable      initX=(Solverable)cl.lookup(name);
      if(initX==null) {
         initX=new Double(0);
      }
      try {
         PURGE.purgeVar(cl, name);
      } catch(NonEmptyDirException e1) {
      }

      Solverable leftX    =initX.amplia(-1);
      Solverable rightX   =initX.amplia(1);
      Solverable initY    =fedx(run, name, initX);
      Solverable lastInitY=null;
      Solverable leftY    =fedx(run, name, leftX);
      Solverable rightY   =fedx(run, name, rightX);

      while(
            (lastInitY==null)
                ||(
                   !initY.tolerance(initY.zero(), 1E-30)
                   &&!initY.tolerance(lastInitY, 1E-30)
                )
           ) {
         double compYs=leftY.compare(rightY);
         if((leftY.sign()<0)&&(rightY.sign()<0)) {
            if(compYs<=0) {
               leftX    =rightX;
               leftY    =rightY;
               rightX   =rightX.amplia(-compYs);
               rightY   =fedx(run, name, rightX);
            } else {
               rightX   =leftX;
               rightY   =leftY;
               leftX    =leftX.amplia(-compYs);
               leftY    =fedx(run, name, leftX);
            }
         } else if((leftY.sign()>0)&&(rightY.sign()>0)) {
            if(compYs<=0) {
               rightX   =leftX;
               rightY   =leftY;
               leftX    =leftX.amplia(compYs);
               leftY    =fedx(run, name, leftX);
            } else {
               leftX    =rightX;
               leftY    =rightY;
               rightX   =rightX.amplia(compYs);
               rightY   =fedx(run, name, rightX);
            }
         } else {
            initX   =initX.mean(leftX, rightX);
            initY   =fedx(run, name, initX);
            if((leftY.sign()!=initY.sign())) {
               rightX      =initX;
               rightY      =initY;
               lastInitY   =rightY.mean(leftY, initY);
            } else {
               leftX       =initX;
               leftY       =initY;
               lastInitY   =rightY.mean(rightY, initY);
            }
         }
      }
      try {
         STO.storeVar(name, (Stackable)initX);
      } catch(CircularReferenceException e3) {
      }
      if(initY.tolerance(initY.zero(), 1E-10)) { //Zero
         ((Stackable)initX).setTag(name+" "+"Zero");
      } else { //Max/Min
         ((Stackable)initX).setTag(name+" "+"Maximum / Minimum");
      }
      cl.push(((Stackable)initX));
   }

   public static InfixExp optimize(InfixExp exp) {
      cl.push(exp);
      cl.push(exp);
      try {
         new EVAL().exec();
      } catch(F8Exception e2) {
         try {
            new SWAP().exec();
         } catch(F8Exception e3) {
         }
      }
      exp=(InfixExp)cl.pop();
      cl.pop();
      return exp;
   }

   public static void checkCircularity(InfixExp exp, String name)
                                throws UndefinedException {
      Vector inco=mkl.incogOf(cl, exp.getAST());
      for(int i=0; i<inco.size(); i++) {
         String vn=(String)inco.elementAt(i);
         if((cl.lookup(vn)==null)&!vn.equals(name)) {
            throw new UndefinedException("Equation solver error|Some vars not defined!");
         }
      }
   }

   public static Solverable fedx(CommandSequence r, String n, Solverable x)
                          throws F8Exception {
      if(cl==null)
      	cl=StaticCalcGUI.theGUI.getCalcLogica();
      STO.storeVar(n, (Stackable)x);
      r.exec();
      return (Solverable)cl.pop();
   }

   public static double fedxDouble(CommandSequence r, String n, double x)
                            throws F8Exception {
      Solverable ret=fedx(r, n, new Double(x));
      if(ret instanceof Double) {
         return ((Double)ret).x;
      }
      throw new BadArgumentValueException("Expression does not| evaluate to double");
   }
}
