/*
 * Created on 29-jul-2003
 *
 
 
 */
package f8;

import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Hashtable;
import f8.platform.Stack;
import f8.types.Directory;
import f8.types.Unit;
import f8.types.utils.Debuggable;

/**
 * @author elinares
 *

 
 */
public interface CL {

   //Constantes de settings
   public final int ALG_MOD=0;
   public final int ANG_MOD=1;
   public final int BEEP   =2;
   public final int BIN=4;
   public final int CLOCK  =3;
   public final int CNCT   =4;
   public final int COMMA =1;
   public final int DEC=1;
   public final int DEG =1;
   public final int DOU_MOD=5;
   public final int DOU_PRE=6;
   public final int ENG=4;
   public final int FIX=2;
   public final int GRAD=3;
   public final int HEX=2;
   public final int INT_MOD=7;
   public final int LAS_ARG=8;
   public final int LAS_CMD=9;
   public final int LAS_STA=10;
   public final int MOD_3D =12;
   public final int MULTILI=11;
   public final int OCT=3;
   public final int PER_MOD=13;
   public final int PERIOD=2;
   public final int PRG_MOD=14;
   public final int RAA=3;
   public final int RAD =2;
   public final int RAZ=2;
   public final int SCI=3;
   public final int STD=1;
   public final int SYM_MOD=15;
   public final int XYZ=1;

   /**
    * Cambia al directorio
    * @param name nombre del directorio al que cambiar
    */
   public void changeToDir(String name);

   /**
    * Comprueba que existen StkObs en opStack
    * @param depth numero de StkObs a comprobar
    * @return true si al menos hay depth StkObs en opStack
    */
   public boolean check(int depth);

   /**
    * Obtener el diccionario de usuario actual
    * @return Hashtable con los pares (String,StkOb)
    */
   public Directory currentDict();

   /**
    * Devuelve el Diccionario
    * @param n Numero de diccionario a devolver
    * @return Diccionario local a devolver
    */
   public Hashtable dictPeek(int n);

   /**
    * Numero de entornos locales definidos en la calculadora
    * @return Numero de entornos de variables locales definidos
    */
   public int dictStackSize();

   /**
    * devuelve un String que contiene el tipo de los argumentos tomados de
    * la pila
    * @param nArg Numero de  argumentos a seleccionar
    * @param arg matriz que contiene los elementos sacados de la pila
    * @return Tipos concatenados.
    */
   public String dispatch(Stackable[] arg);

   /**
    * Marca fin de entrada y comienzo de ejecucion de la cadena en edicion
    *
    */
   public void enter(String input) throws F8Exception;

   /**
    * Notifica un error al ejecutar un StkOb
    * @param it StkOb en ejecucion
    * @param errCode Codigo de error
    */
   //public void error(Command it, int errCode);

   //public void error(int i);

   //public void error(String msg);
   
   public void error(F8Exception f);

   /**
    * Obtiene el nombre del directorio actual en la forma
    * "{HOME DIR1 DIR2 ... }"
    * @return Una lista con los directorios
    */
   public String getCurrentDirName();

   /**
    * Entorno local actual
    * @return Hashtable con los pares (String, F8Ob).
    */
   public Hashtable getCurrentLocalEnvir();
   
   public Debuggable getProcDebug() ;

   /**
    * Devuelve el valor del set. Equivalente a los flags de la 48
    * @param setting
    * @return
    */
   public int getSetting(int setting);

   /**
    * Obtiene la Unidad definida en el sistema
    * @param s Nombre de la Unidad de Medida
    * @return Definicion en el Sistema Internacional, null si no existe
    */
   public Unit getUnitDef(String s);
   
   /**
    * Diccionario de usuario
    * @return El diccionario de usuario, donde se graban las variables de usuario.
    */
   public Hashtable getUserDict();

   /**
    * Cambia a HOME
    */
   public void homeDir();

	/**
	 * Coloca el setting. ver valores SET_*
	 * @param setting
	 * @return
	 */
   public boolean isSetting(int setting);

   /**
    * Carga desde almacenamiento fijo los datos de la calculadora
    * @param ds DataStream desde/hacia el que van los datos
    * @return El contenido de la linea de edicion
    */
   public String loadFromStorage(DataStream ds);

   /**
    * Devuelve el numero de anidamientos locales
    * @return
    */
   public int localEnvirCount();

   /**
    * Devuelve el StkOb nombrado como name. Se busca primero en los locales,
    * luego en el diccionario de usuario y finalmente en el diccionario de
    * sistema
    * @param name Nombre del StkOb
    * @return el StkOb si lo hay o null si no lo encuentra
    */
   public Command lookup(String name);

	/**
	 * Busca en los objetos de usuario; por orden primero las locales, luego en el
	 * directorio actual y luego en cualquier directorio padre.
	 * @param name Nombre del objeto
	 * @param user En la cadena de directorios
	 * @param userDefault Solo en el directorio actual
	 * @param local entre las variables locales
	 * @return
	 */
	public Command lookupUser(String name, boolean user, boolean userDefault, boolean local);

   /**
    * Saca informacion sobre StkOb
    * @param it StkOb sobre el que informar
    */
   public void output(Command it);

   /**
    * Colocar a la calculadora en modo pausa
    *
    */
   public void pause();

   /**
    * Idem peek(0)
    * @return StkOb topmost
    */
   public Stackable peek();

   /**
    * Lee (sin quitar de la pila) el StkOb en la posicion at, 0 es el topmost
    * @param at posicion
    * @return el StkOb de esa posicion
    */
   public Stackable peek(int at);

   /**
    * Pone el StkOb element en la posicion at
    * @param at posicion en la que colocar, 0=topmost
    * @param element StkOb a colocar
    */
   public void poke(int at, Command element);

   /**
    * Retira el StkOb topmost de la pila
    *@return el StkOb retirado
    */
   public Stackable pop();

   /**
    * Retira n StkObs topmost de la pila
    * @param n numero de StkObs a retirar
    */
   public void pop(int n);

   /**
    * Borrar el diccionario/entorno de variables locales actual
    *
    */
   public void popLocalEnvir();

   /**
    * Coloca el StkOb it arriba de la pila (como topmost)
    * @param it El StkOb a colocar
    */
   public void push(Stackable it);

   /**
    * Crea un nuevo entorno (inner) de variables locales. Los STO que se produzcan
    * se meteran aqu�. La idea es que cuando se ejecute endLocal se borren todas.
    *
    */
   public void pushLocalEnvir();

   /**
    * Restaura la pila guardada si la hay
    *
    */
   public void restoreStack();

   /**
    * Guarda una copia por referencia de la pila
    *
    */
   public void saveStack();

   /**
    * Graba el estado y datos de la calculadora logica en el stream
    * @param edit el contenido actual de la linea de edicion
    * @param ds DataStream desde/hacia el que van los datos
    */
   public void saveToStorage(String edit, DataStream ds);

   /**
    * Sustituye el Stack de operaciones por opSt
    * @param opSt
    */
   public void setOpStack(Stack opSt);

   public void setProcDebug(Debuggable proc);

   public void setSettings(int setting, boolean val);

   public void setSettings(int setting, int val);

   /**
    * Devuelve el tama�o de la pila
    * @return Numero de StkObs en la pila
    */
   public int size();

   /**
    * Notifica a la calculadora que es un comando de sistema
    * @param it
    */
   public void systemInstall(Command it);

   /**
    * Sube un directorio
    */
   public void upDir();
   
}
