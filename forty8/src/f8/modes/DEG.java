package f8.modes;

import f8.CL;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class DEG extends CheckGroup {
   public DEG() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 489;
   }

   public Storable getInstance() {
      return new DEG();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public boolean execBefore() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.setSettings(CL.ANG_MOD, CL.DEG);
      return false;
   }

   public String toString() {
      return ("DEG");
   }

   /* (non-Javadoc)
    * @see f8.kernel.mod.CheckGroup#getGroupName()
    */
   public String getGroupName() {
      return "AM";
   }
}
