package f8.modes;

import f8.CL;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class STD extends CheckGroup {
   public STD() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 504;
   }

   public Storable getInstance() {
      return new STD();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public boolean execBefore() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.setSettings(CL.DOU_MOD, CL.STD);
      return false;
   }

   public String toString() {
      return ("STD");
   }

   /* (non-Javadoc)
    * @see f8.kernel.mod.CheckGroup#getGroupName()
    */
   public String getGroupName() {
      return "DM";
   }
}
