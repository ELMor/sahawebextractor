package f8.modes;

import f8.CL;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class ChkBEEP extends CheckGroup {
   public ChkBEEP() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 502;
   }

   public Storable getInstance() {
      return new ChkBEEP();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public boolean execBefore() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.setSettings(CL.BEEP, !cl.isSetting(CL.BEEP));
      return false;
   }

   public String toString() {
      return ("ChkBEEP_");
   }

   /* (non-Javadoc)
    * @see f8.kernel.mod.CheckGroup#getGroupName()
    */
   public String getGroupName() {
      return "BE";
   }
}
