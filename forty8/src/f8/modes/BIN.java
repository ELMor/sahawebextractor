package f8.modes;

import f8.CL;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class BIN extends CheckGroup {
   public BIN() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 480;
   }

   public Storable getInstance() {
      return new BIN();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public boolean execBefore() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.setSettings(CL.INT_MOD, CL.BIN);
      return false;
   }

   public String toString() {
      return ("BIN");
   }

   /* (non-Javadoc)
    * @see f8.kernel.mod.CheckGroup#getGroupName()
    */
   public String getGroupName() {
      return "IM";
   }
}
