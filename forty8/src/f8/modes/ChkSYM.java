package f8.modes;

import f8.CL;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class ChkSYM extends CheckGroup {
   public ChkSYM() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 501;
   }

   public Storable getInstance() {
      return new ChkSYM();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public boolean execBefore() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.setSettings(CL.SYM_MOD, !cl.isSetting(CL.SYM_MOD));
      return false;
   }

   public String toString() {
      return ("ChkSYM_");
   }

   /* (non-Javadoc)
    * @see f8.kernel.mod.CheckGroup#getGroupName()
    */
   public String getGroupName() {
      return "SY";
   }
}
