package f8.kernel.op.arr;

import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.InvalidDimensionException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;
import f8.types.Matrix;

public final class MkMatrix extends Dispatch2 {
   public MkMatrix() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 68;
   }

   public Storable getInstance() {
      return new MkMatrix();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("MATRIX");
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfDoubleDouble(f8.kernel.types.Double, f8.kernel.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		int r=(int)a.x;
		int c=(int)b.x;

		if((r>=0)&&(c>=0)) {
		   return (new Matrix(r, c));
		} else {
		   throw new InvalidDimensionException(this);
		}
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
