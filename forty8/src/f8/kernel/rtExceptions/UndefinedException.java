/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.kernel.rtExceptions;

import f8.Command;
import f8.ui.TeclaVirtual;

;

/**
 * @author elinares
 *

 
 */
public class UndefinedException extends f8.kernel.rtExceptions.F8Exception {

	/**
	 * 
	 */
	public UndefinedException(Command c) {
		super(c);
		
	}

	/**
	 * @param message
	 */
	public UndefinedException(String message) {
		super(message);
		
	}

	/**
	 * @param v
	 */
	public UndefinedException(TeclaVirtual v) {
		super(v);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		return ICalcErr.Undefined;
	}

}
