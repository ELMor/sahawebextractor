/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.kernel.rtExceptions;

import f8.Command;
import f8.ui.TeclaVirtual;

;

/**
 * @author elinares
 *

 
 */
public class TooFewArgumentsException extends f8.kernel.rtExceptions.F8Exception {

	/**
	 * 
	 */
	public TooFewArgumentsException(Command c) {
		super(c );
		
	}

	/**
	 * @param message
	 */
	public TooFewArgumentsException(String message) {
		super(message);
		
	}

	/**
	 * @param v
	 */
	public TooFewArgumentsException(TeclaVirtual v) {
		super(v);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		return ICalcErr.TooFewArguments;
	}

}
