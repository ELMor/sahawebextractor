/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui;

import f8.kernel.rtExceptions.F8Exception;
import f8.platform.CalcGUI;


/**
 * @author elinares
 *

 
 */
public interface TeclaVirtual {
   /**
    * Comprueba si es un directorio
    * @return true si lo es
    */
   public boolean isDir();

   /**
    * Retorna el titulo de la tecla
    * @return
    */
   public String getTitle();

   /**
    * Se llama cuando es presionada. CalcGUI debe llamar a esta.
    * @param ci
    */
   public void pressed(CalcGUI ci) throws F8Exception ;

   /**
    * Comando
    * @return Nombre del comando que se ejecuta si se pulsa la tecla
    */
   public String getEmit();
   
   public boolean isVarSolver();
}
