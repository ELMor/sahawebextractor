/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui;

import f8.Operation;
import f8.Stackable;
import f8.StaticCalcGUI;

import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;

import f8.platform.CalcGUI;
import f8.platform.Hashtable;
import f8.platform.UtilFactory;
import f8.platform.Vector;

import f8.types.Lista;


/**
 * @author elinares
 *

 
 */
public class Menu implements TeclaVirtual {
   public static Hashtable groups=UtilFactory.newHashtable();
   Operation               preset=null;
   Vector content=null;
   String nombre;

   public Menu(String n) {
      this(n, UtilFactory.newVector());
   }

   public Menu(String n, Operation op) {
      this(n, UtilFactory.newVector());
      preset=op;
   }

   public Menu(String title, Vector dir) {
      nombre    =title;
      content   =dir;
   }

   public static void createAndDisplayCustomMenu(Lista cst)
                                          throws F8Exception {
      Menu  m   =new Menu("Custom");
      Lista lcst=(Lista)cst;
      for(int i=0; i<lcst.size(); i++) {
         Stackable mnu=(Stackable)lcst.get(i);
         if(mnu instanceof Lista) {
            Lista lmnu=(Lista)mnu;
            if(lmnu.size()==0) {
               m.appendKey(new SinEfecto(""));
            } else if(lmnu.size()==1) {
               m.appendKey(new TeclaCST(lmnu.get(0)));
            } else if(lmnu.size()==2) {
               Stackable shift=(Stackable)lmnu.get(1);
               if(shift instanceof Lista) {
                  m.appendKey(new TeclaCST(lmnu.get(0), (Lista)lmnu.get(1)));
               } else {
                  throw new BadArgumentTypeException(m);
               }
            }
         } else {
            m.appendKey(new TeclaCST(mnu));
         }
      }
      StaticCalcGUI.theGUI.getCalcFisica().setUpMenu(m);
      StaticCalcGUI.theGUI.refreshProgMenu();
   }

   public void appendKey(TeclaVirtual vk) {
      content.add(vk);
   }

   /* (non-Javadoc)
    * @see f8.ui.TeclaVirtual#getColor()
    */
   public int getColor() {
      return 0;
   }

   public TeclaVirtual getContentAt(int i) {
      if(i>=content.size()) {
         return new SinEfecto("");
      }
      if(content.get(i)==null) {
         return new SinEfecto("");
      }
      return (TeclaVirtual)content.get(i);
   }

   /* (non-Javadoc)
    * @see f8.ui.TeclaVirtual#getEmit()
    */
   public String getEmit() {
      return nombre;
   }

   /* (non-Javadoc)
    * @see f8.ui.TeclaVirtual#getGroupCheck()
    */
   public String getGroupCheck() {
      return null;
   }

   public String[] getStringArray(int offset, int tam) {
      String[] ret=new String[tam];
      int      i=offset;
      int      j=0;

      for(; j<tam; i++, j++) {
         if(i<content.size()) {
            ret[j]=getContentAt(i).getTitle();
            if(ret[j]==null) {
               ret[j]="";
            }
         } else {
            ret[j]="";
         }
      }

      return ret;
   }

   public String getTitle() {
      return nombre;
   }

   /* (non-Javadoc)
    * @see f8.ui.TeclaVirtual#isChecked()
    */
   public boolean isChecked() {
      return false;
   }

   public boolean isDir() {
      return true;
   }

   /* (non-Javadoc)
    * @see f8.ui.TeclaVirtual#isVarSolver()
    */
   public boolean isVarSolver() {
      return false;
   }

   /**
    * Esta funci�n se llama para cada menu que este activado
    * si ha cambiado. Por ejemplo se borra una variable o se crea
    * en el menu VAR
    *
    */
   public void notifyChangeStatus(CalcGUI cg) {
   }

   public void pressed(CalcGUI cg) throws F8Exception{
      if(this instanceof VAR) {
         notifyChangeStatus(cg);
      }
      if(preset!=null) {
        preset.exec();
      }
      cg.setMenu(1, this, null, null);
   }

   /* (non-Javadoc)
    * @see f8.ui.TeclaVirtual#setChequed(boolean)
    */
   public void setChequed(boolean is) {
      //Nada!
   }

   public void setContent(Vector dir) {
      content=dir;
   }

   public void setVirtualKeyAt(int k, TeclaVirtual vk) {
      content.setElementAt(vk, k);
   }

   public int size() {
      return content.size();
   }
}
