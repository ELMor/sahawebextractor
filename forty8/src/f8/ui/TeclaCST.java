/*
 * Created on 28-jul-2003
 *
 
 
 */
package f8.ui;
import f8.CL;
import f8.Command;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.CalcGUI;
import f8.types.Lista;
import f8.types.Literal;
import f8.types.Unit;

/**
 * @author elinares
 *

 
 */
public final class TeclaCST extends UnitMenu {
   Command main;
   Lista   acc=null;

   public TeclaCST(Command ob) {
      super((ob instanceof Unit) ? ((Unit)ob).getUnidad().toString()
                                 : ob.toString()
           );
      main=ob;
   }

   public TeclaCST(Command ob, Lista l) {
      this(ob);
      acc=l;
   }

   public Command getAction(int shiftMode) throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(acc==null) {
      	Command lo=cl.lookup(toString());
         return lo==null? new Literal(toString()) : lo;
      }
      if(acc.size()>=shiftMode) {
         return acc.get(shiftMode);
      }
      return new Literal(toString());
   }

   public void pressed(CalcGUI cg) throws F8Exception {
      CL  cl   =cg.getCalcLogica();
      CF  cf   =cg.getCalcFisica();
      int index=cf.getShiftMode();
      if((acc==null)||(acc.size()==0)||(index>acc.size())) {
         if(main instanceof Unit) {
            super.pressed(cg);
         } else {
            main.exec();
         }
      } else {
         acc.get(index).exec();
      }
      cg.refresh(false);
   }

   /* (non-Javadoc)
    * @see f8.ui.TeclaVirtual#isDir()
    */
   public boolean isDir() {
      return false;
   }
}
