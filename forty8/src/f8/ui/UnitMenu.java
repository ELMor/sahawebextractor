/*
 * Created on 25-ago-2003
 *
 
 
 */
package f8.ui;

import f8.CL;
import f8.kernel.rtExceptions.F8Exception;
import f8.op.keyboard.DIV;
import f8.op.keyboard.TIMES;
import f8.op.units.CONVERT;
import f8.platform.CalcGUI;

/**
 * @author elinares
 *

 
 */
public class UnitMenu extends WithEnter {
   public UnitMenu(String name) {
      super(name);
   }

   public void pressed(CalcGUI cg) throws F8Exception {
      CL cl=cg.getCalcLogica();
      CF cf=cg.getCalcFisica();
      switch(cf.getShiftMode()) {
         case 0: //no shift
            cl.enter(cg.getEditField()+" 1_"+getTitle());
            new TIMES().exec();
            break;
         case 1: //left
            cl.enter(cg.getEditField()+" 1_"+getTitle());
            new CONVERT().exec();
            break;
         case 2: //right
            cl.enter(cg.getEditField()+" 1_"+getTitle());
            new DIV().exec();
            break;
      }
      cg.refresh(true);
   }
}
