/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui;
import f8.CL;
import f8.platform.CalcGUI;
import f8.platform.Hashtable;
import f8.platform.UtilFactory;
import f8.platform.Vector;
import f8.types.Directory;


/**
 * @author elinares
 *

 
 */
public final class VAR extends Menu {
   public VAR() {
      super("VAR"); //VAR
   }

   public void notifyChangeStatus(CalcGUI cg) {
      CL        cl =cg.getCalcLogica();
      Hashtable ht =cl.currentDict().getHT();
      Vector    udv=ht.getKeys();
      int       uds=udv.size();
      Vector    vkm=UtilFactory.newVector();
      for(int i=0; (i<6)||(i<uds); i++) {
         if(i<uds) {
            String name=(String)udv.get(i);
            if(ht.get(name) instanceof Directory) {
               vkm.add(new TeclaVar(name, true));
            } else {
               vkm.add(new TeclaVar(name, false));
            }
         } else {
            vkm.add(new SinEfecto(""));
         }
      }
      setContent(vkm);
   }
}
