/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui.hp48.keyb;

import f8.ui.Menu;
import f8.ui.Tecla;
import f8.ui.WithEnter;

/**
 * @author elinares
 *

 
 */
public final class RightMiddle extends Menu {
   public RightMiddle() {
      super(null);
      appendKey(new Tecla(""));
      appendKey(new Tecla(""));
      appendKey(new Tecla(""));
      appendKey(new Tecla(""));
      appendKey(new Tecla(""));
      appendKey(new Tecla(""));
      appendKey(new WithEnter("HOME"));
      appendKey(new WithEnter("RCL"));
      appendKey(new WithEnter("\u008DNUM","->NUM"));
      appendKey(new Tecla(""));
      appendKey(new Tecla(""));
      appendKey(new Tecla(""));
      appendKey(new WithEnter("\u0088"));
      appendKey(new WithEnter("\u0084"));
      appendKey(new WithEnter("\u0085"));
      appendKey(new WithEnter("xRy"));
      appendKey(new WithEnter("LOG"));
      appendKey(new WithEnter("LN"));
      appendKey(new Tecla("MATR"));
      appendKey(new Tecla("VISIT"));
      appendKey(new Tecla("3D"));
      appendKey(new Tecla(""));
      appendKey(new Tecla("CLR"));
   }
}
