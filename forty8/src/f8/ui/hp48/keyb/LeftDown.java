/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui.hp48.keyb;

import f8.Operation;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.ui.Menu;
import f8.ui.ShiftKey;
import f8.ui.Tecla;
import f8.ui.WithEnter;
import f8.ui.hp48.menu.PLOT;
import f8.ui.hp48.menu.SOLVE;
import f8.ui.hp48.menu.STAT; 
import f8.ui.hp48.menu.SYMB;
import f8.ui.hp48.menu.TIME;
import f8.ui.hp48.menu.UNITS;

/**
 * @author elinares
 *

 
 */
public final class LeftDown extends Menu {
   public LeftDown() {
      super(null);
      appendKey(new Tecla("USR"));
      appendKey(new Tecla(
                          "SOLVE",
                          new Operation() {
            /* (non-Javadoc)
             * @see f8.kernel.Operation#exec()
             */
            public void exec() {
               try {
                  StaticCalcGUI.theGUI.setMenu(0, new SOLVE(), null, null);
               } catch(F8Exception e) {
               }
            }
         }
                         )
               );
      appendKey(new PLOT());
      appendKey(new SYMB());
      appendKey(new Tecla("()",1));
      appendKey(new ShiftKey(1));
      appendKey(new TIME());
      appendKey(new STAT());
      appendKey(new UNITS());
      appendKey(new Tecla("[]", 1));
      appendKey(new ShiftKey(2));
      appendKey(new Tecla("RAD"));
      appendKey(new Tecla("STACK"));
      appendKey(new Tecla("CMD"));
      appendKey(new Tecla("\u00AB\u00BB", 1)); //Open-Close Prog <<>>
      appendKey(new Tecla("CONT"));
      appendKey(new WithEnter("="));
      appendKey(new Tecla(","));
      appendKey(new Tecla("\u0087", "Pi"));
      appendKey(new Tecla("{}", 1));
   }
}
