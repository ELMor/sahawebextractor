/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui.hp48.keyb;

import f8.ui.Menu;
import f8.ui.Tecla;

/**
 * @author elinares
 *

 
 */
public final class AlphUp extends Menu {
   public AlphUp() {
      super(null);
      appendKey(new Tecla("A"));
      appendKey(new Tecla("B"));
      appendKey(new Tecla("C"));
      appendKey(new Tecla("D"));
      appendKey(new Tecla("E"));
      appendKey(new Tecla("F"));
   }
}
