/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui.hp48.menu;

import f8.Operation;
import f8.StaticCalcGUI;
import f8.display.PPAR;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.CalcGUI;
import f8.ui.Menu;
import f8.ui.Tecla;
import f8.ui.WithEnter;

/**
 * @author elinares
 *

 
 */
public final class PLOT extends Menu {
   public PLOT() {
      super(
            "PLOT",
            new Operation() {
            public void exec() throws F8Exception {
               PPAR.initPPAR();
            }
         }
           );

      appendKey(PLOTR());
      appendKey(PTYPE());
      appendKey(new Tecla(
                          "NEW",
                          new Operation() {
            public void exec() throws F8Exception {
               // TODO Auto-generated method stub
            }
         }
                         )
               );
      appendKey(new Tecla(
                          "EDEQ",
                          new Operation() {
            public void exec() throws F8Exception {
               // TODO Auto-generated method stub
            }
         }
                         )
               );
      appendKey(new WithEnter("STEQ"));
      appendKey(new Tecla(
                          "CAT",
                          new Operation() {
            public void exec() throws F8Exception {
               // TODO Auto-generated method stub
            }
         }
                         )
               );
   }

   public Menu PLOTR() {
      Menu m=new Menu("PLOTR");
      m.appendKey(new WithEnter("ERASE"));
      m.appendKey(new WithEnter("DRAW"));
      m.appendKey(new WithEnter("AUTO"));
      m.appendKey(new WithEnter("XRNG"));
      m.appendKey(new WithEnter("YRNG"));
      m.appendKey(new WithEnter("INDEP"));
      m.appendKey(new WithEnter("DEPN"));
      m.appendKey(PTYPE());
      m.appendKey(new WithEnter("RES"));
      m.appendKey(new WithEnter("CENTR"));
      m.appendKey(new WithEnter("SCALE"));
      m.appendKey(new WithEnter("RESET"));
      m.appendKey(new WithEnter("AXES"));
      m.appendKey(new WithEnter("DRAX"));
      m.appendKey(new WithEnter("LABEL"));
      m.appendKey(new WithEnter("*H"));
      m.appendKey(new WithEnter("*W"));
      m.appendKey(new WithEnter("PDIM"));
      return m;
   }

   public Menu PTYPE() {
      Menu m=new Menu("PTYPE");
      m.appendKey(new Tecla("FUNCTION", new st(0)));
      m.appendKey(new Tecla("CONIC", new st(1)));
      m.appendKey(new Tecla("POLAR", new st(2)));
      m.appendKey(new Tecla("PARAMETRIC", new st(3)));
      m.appendKey(new Tecla("TRUTH", new st(4)));
      m.appendKey(new Tecla("BAR", new st(5)));
      m.appendKey(new Tecla("HISTOGRAM", new st(6)));
      m.appendKey(new Tecla("SCATTER", new st(7)));
      return m;
   }

   public class st extends Operation {
      int t;

      public st(int tipo) {
         t=tipo;
      }

      public void exec() throws F8Exception {
         CalcGUI cg=StaticCalcGUI.theGUI;
         cg.setLastMenu();
      }
   }
}
