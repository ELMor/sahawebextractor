/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui.hp48.menu;

import f8.ui.Menu;
import f8.ui.Tecla;

/**
 * @author elinares
 *

 
 */
public final class IO extends Menu {
   public IO() {
      super("IO");
      appendKey(new Tecla("UNIMP"));
   }
}
