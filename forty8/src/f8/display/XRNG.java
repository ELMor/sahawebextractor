/*
 * Created on 08-oct-2003
 *
 
 
 */
package f8.display;

import f8.CL;
import f8.Command;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;

/**
 * @author elinares
 *

 
 */
public class XRNG extends Command {
   /* (non-Javadoc)
    * @see f8.platform.Storable#getID()
    */
   public int getID() {
      return 6003;
   }

   /* (non-Javadoc)
    * @see f8.platform.Storable#getInstance()
    */
   public Storable getInstance() {
      return this;
   }

   /* (non-Javadoc)
    * @see f8.platform.Storable#loadState(f8.platform.DataStream)
    */
   public void loadState(DataStream ds) {
   }

   /* (non-Javadoc)
    * @see f8.platform.Storable#saveState(f8.platform.DataStream)
    */
   public void saveState(DataStream ds) {
   }

   /* (non-Javadoc)
    * @see f8.Command#isAlgebraic()
    */
   public boolean isAlgebraic() {
      return false;
   }

   /* (non-Javadoc)
    * @see f8.Command#isInfix()
    */
   public boolean isInfix() {
      return false;
   }

   /* (non-Javadoc)
    * @see f8.Command#isOperator()
    */
   public boolean isOperator() {
      return false;
   }

   /* (non-Javadoc)
    * @see f8.Operation#exec()
    */
   public void exec() throws F8Exception {
   	CL cl=StaticCalcGUI.theGUI.getCalcLogica();
   	if(cl.check(2)){
   		Stackable a=cl.peek(1),b=cl.peek(0);
   		if(a instanceof Double && b instanceof Double){
   			cl.pop(2);
   			PPAR.initPPAR();
   			PPAR.Xmax=((Double)a).x;
   			PPAR.Xmin=((Double)b).x;
   			PPAR.savePPAR();
				new ERASE().exec();
				new DRAW().exec();
   		}else{
   			throw new BadArgumentTypeException(this);
   		}
   	}else{
   		throw new TooFewArgumentsException(this);
   	}
   }

   /* (non-Javadoc)
    * @see java.lang.Object#toString()
    */
   public String toString() {
      return "XRNG";
   }
}
