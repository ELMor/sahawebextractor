/*
 * Created on 08-oct-2003
 *
 
 
 */
package f8.display;

import f8.Command;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;

/**
 * @author elinares
 *

 
 */
public class ERASE extends Command {
   /* (non-Javadoc)
    * @see f8.platform.Storable#getID()
    */
   public int getID() {
      return 6001;
   }

   /* (non-Javadoc)
    * @see f8.platform.Storable#getInstance()
    */
   public Storable getInstance() {
      return this;
   }

   /* (non-Javadoc)
    * @see f8.platform.Storable#loadState(f8.platform.DataStream)
    */
   public void loadState(DataStream ds) {
   }

   /* (non-Javadoc)
    * @see f8.platform.Storable#saveState(f8.platform.DataStream)
    */
   public void saveState(DataStream ds) {
   }

   /* (non-Javadoc)
    * @see f8.Command#isAlgebraic()
    */
   public boolean isAlgebraic() {
      return false;
   }

   /* (non-Javadoc)
    * @see f8.Command#isInfix()
    */
   public boolean isInfix() {
      return false;
   }

   /* (non-Javadoc)
    * @see f8.Command#isOperator()
    */
   public boolean isOperator() {
      return false;
   }

   /* (non-Javadoc)
    * @see f8.Operation#exec()
    */
   public void exec() throws F8Exception {
      StaticCalcGUI.theGUI.getLCD().clear();
   }

   /* (non-Javadoc)
    * @see java.lang.Object#toString()
    */
   public String toString() {
      return "ERASE";
   }
}
