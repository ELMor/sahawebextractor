package f8;

import antlr.collections.AST;

import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.ICalcErr;

import f8.kernel.sys.InsMain;

import f8.op.prg.stk.DUP;

import f8.platform.DataStream;
import f8.platform.Hashtable;
import f8.platform.Stack;
import f8.platform.Storable;
import f8.platform.UtilFactory;
import f8.platform.Vector;

import f8.types.Directory;
import f8.types.InfixExp;
import f8.types.Literal;
import f8.types.Unit;

import f8.types.utils.Debuggable;

public final class CLI2 implements CL {
   // pila de diccionarios de sistema y usuario
   private Stack dirStack;

   //Diccionario de usuario
   public Directory homeDir;
   private Stack    lastStack=null;

   // Pila de diccionarios de variables locales
   private Stack localStack;

   // operator stack: RPN operators and data to be processed
   private Stack   operStack;
   public Stack    procDebug        =null;
   private boolean isCatalogOnDevice=false;

   //Modos de representacion
   private int[] settings=
                 new int[]{
                    0, CL.DEG, 1, 1, 1, CL.STD, 0, CL.DEC, 1,
                    1, 1, 1, CL.XYZ, CL.COMMA, 0, 1
                 };

   //Diccionario de sistema
   public Directory systemDir;

   //Unidades
   private Hashtable units;

   public CLI2(boolean existFile, DataStream ds) {
      StaticCalcGUI.theGUI.setCalcLogica(this);
      operStack           =UtilFactory.newStack();
      dirStack            =UtilFactory.newStack();
      localStack          =UtilFactory.newStack();
      procDebug           =UtilFactory.newStack();
      isCatalogOnDevice   =existFile;
      systemDir           =new Directory(null, "System");
      homeDir             =new Directory(systemDir, "HOME");
      InsMain.installStkOb(this);
      dirStack.push(systemDir);
      dirStack.push(homeDir);

      units=UtilFactory.newHashtable();
      InsMain.installUnits(existFile, ds, units);
      if(existFile) {
         StaticCalcGUI.theGUI.setInitialEdit(loadFromStorage(ds));
      }
   }

   public void changeToDir(String dname) {
      Directory dir=null;

      try {
         dir=(Directory)currentDict().getHT().get(dname);
      } catch(Exception e) {
         dir=null;
      }

      if(dir!=null) {
         dirStack.push(dir);
         StaticCalcGUI.theGUI.refreshStatusLabels();
      }
   }

   // - operator stack -------------------------------
   public boolean check(int n) {
      if(operStack.size()<n) {
         return (false);
      } else {
         return (true);
      }
   }

   public Directory currentDict() {
      int N=dirStack.size();

      return ((Directory)dirStack.elementAt(N-1));
   }

   public Hashtable dictPeek(int n) {
      int N=dirStack.size();

      return ((Directory)dirStack.elementAt(N-n-1)).getHT();
   }

   public int dictStackSize() {
      return (dirStack.size());
   }

   public String dispatch(Stackable[] arg) {
      String ret="";

      if(operStack.size()<arg.length) {
         return null;
      }

      for(int i=arg.length-1; i>=0; i--) {
         String tn=null;
         if(arg[i]!=null) {
            tn=arg[i].getTypeName();
         }
         arg[i]=(Stackable)pop();
         String tnr=arg[i].getTypeName();
         if((tn!=null)&&!(tn.equals(tnr))) {
            error(ICalcErr.BadArgumentType);
            return null;
         }
         ret+=arg[i].getTypeName();
      }

      return ret;
   }

   public void enter(String input) throws F8Exception {
      if(input.equals("")) {
         new DUP().exec();
         return;
      }

      Vector vItems=UtilFactory.newVector();
      Stackable.parse(input, vItems);

      while(vItems.size()>0) {
         AST nxtObj=(AST)vItems.get(0);
         if(nxtObj!=null) {
            Command it=Command.createFromAST(nxtObj);
            if(!(it instanceof Stackable)||(it instanceof Literal)) {
               it.exec();
            } else {
               it.store();
            }
         }
         vItems.removeElementAt(0);
      }
   }

   // ---------------------------------------------
   public void error(Command it, int n) {
      error(ICalcErr.Name[n]+"\n with <"+it.toString()+">");
   }

   public void error(F8Exception f8e) {
      String com=f8e.getPartic();
      error(com+" Error: |"+f8e.toString());
   }

   public void error(int i) {
      output(ICalcErr.Name[i]);
   }

   public void error(String msg) {
      output(msg);
   }

   public String getCurrentDirName() {
      String dir="{";

      for(int i=1; i<dirStack.size(); i++) {
         dir+=(" "+((Directory)dirStack.elementAt(i)).getNombre());
      }

      return dir+" }";
   }

   public Hashtable getCurrentLocalEnvir() {
      if(0==localEnvirCount()) {
         return null;
      }

      return (Hashtable)localStack.peek();
   }

   /**
    * @return
    */
   public Debuggable getProcDebug() {
      return (Debuggable)procDebug.pop();
   }

   /* (non-Javadoc)
    * @see f8.kernel.CL#getSetting(int)
    */
   public int getSetting(int setting) {
      return settings[setting];
   }

   public Unit getUnitDef(String s) {
      Unit ret=(Unit)units.get(s);
      if(ret==null) {
         Command vUser=(Command)lookup(s);
         if(vUser instanceof Unit) {
            return (Unit)vUser;
         }
      }
      return ret;
   }

   public Hashtable getUserDict() {
      return homeDir.getHT();
   }

   public void homeDir() {
      while(!currentDict().getNombre().equals("HOME")) {
         dirStack.pop();
      }
   }

   public void install(Command it) {
      currentDict().put(it.toString(), it);
   }

   /* (non-Javadoc)
    * @see f8.kernel.CL#isSetting(int)
    */
   public boolean isSetting(int setting) {
      return (settings[setting]==1) ? true : false;
   }

   public String loadFromStorage(DataStream ds) {
      ds.setRecordPos(1);
      //Cargamos los modos
      for(int i=0; i<settings.length; i++) {
         settings[i]=ds.readInt();
      }

      //Cargamos el userDict
      int sz=ds.readInt();

      for(int i=0; i<sz; i++) {
         String   key=ds.readStringSafe();
         Storable val=Command.loadFromStorage(ds);
         homeDir.put(key, val);
      }

      //Y el Stack
      int szOs=ds.readInt();

      for(int posOs=0; posOs<szOs; posOs++) {
         operStack.push(Command.loadFromStorage(ds));
      }

      //Finalmente se retorna la linea de edicion
      String edit=ds.readStringSafe();
      ds.close();

      return edit;
   }

   public int localEnvirCount() {
      return localStack.size();
   }

   public Command lookup(String name) {
      int     n=localStack.size();
      Command x=null;

      //Primero buscamos en la pila de diccionarios locales
      while((x==null)&&(n>0)) {
         Hashtable h=((Hashtable)localStack.elementAt(--n));
         x=(Stackable)h.get(name);
      }

      n=dictStackSize();

      //luego en la de usuarios y en la de sistema
      while((x==null)&&(n>0)) {
         Hashtable h=((Directory)dirStack.elementAt(--n)).getHT();
         x=(Command)h.get(name);
      }
      return x;
   }

   public Command lookupUser(
                             String name, boolean user, boolean userDefault,
                             boolean local
                            ) {
      int     n;
      Command x=null;
      if(local) {
         n   =localStack.size();
         x   =null;
         //Primero buscamos en la pila de diccionarios locales
         while((x==null)&&(n>0)) {
            Hashtable h=((Hashtable)localStack.elementAt(--n));
            x=(Stackable)h.get(name);
         }
         if(x!=null) {
            return x;
         }
      }

      n=dictStackSize();
      if(userDefault) {
         Hashtable h=((Directory)dirStack.elementAt(--n)).getHT();
         x=(Command)h.get(name);
         if(x!=null) {
            return x;
         }
      }
      if(user) {
         //luego en la de usuarios (no en la de sistema:n>1)
         while((x==null)&&(n>1)) {
            Hashtable h=((Directory)dirStack.elementAt(--n)).getHT();
            x=(Command)h.get(name);
         }
      }

      return x;
   }

   public void output(Command it) {
      output(it.toString());
   }

   public void output(String msg) {
      StaticCalcGUI.theGUI.output(msg);
      StaticCalcGUI.theGUI.refresh(false);
   }

   // ---------------------------------------------
   public void pause() {
      ;
   }

   public void pause(int seconds) {
      seconds=seconds+1;
   }

   public Stackable peek() {
      int       n =operStack.size();
      Stackable it=(Stackable)operStack.elementAt(n-1);

      return (it);
   }

   public Stackable peek(int i) {
      int       n =operStack.size();
      Stackable it=(Stackable)operStack.elementAt(n-1-i);

      return (it);
   }

   public void poke(int i, Command it) {
      operStack.poke(i, it);
   }

   public Stackable pop() {
      Stackable ret=(Stackable)operStack.pop();

      if(ret==null) {
         error(ICalcErr.TooFewArguments);
      }

      return ret;
   }

   public void pop(int n) {
      operStack.pop(n);
   }

   public void popLocalEnvir() {
      localStack.pop();
   }

   public void push(Stackable it) {
      Stackable copy;
      if(it instanceof Literal) {
         copy=new InfixExp(it.getAST());
      } else {
         copy=it.copia();
      }
      copy.setTag(it.getTag());
      operStack.push(copy);
   }

   public void pushLocalEnvir() {
      Hashtable h=UtilFactory.newHashtable();
      localStack.push(h);
   }

   public void restoreStack() {
      if(lastStack!=null) {
         operStack   =lastStack;
         lastStack   =null;
      }
   }

   public void saveStack() {
      lastStack=UtilFactory.newStack();

      for(int i=0; i<operStack.size(); i++) {
         lastStack.push(operStack.elementAt(operStack.size()-1-i));
      }
   }

   public void saveToStorage(String edit, DataStream ds) {
      //Grabamos las unidades (si no lo est�n ya)
      ds.setRecordPos(0);
      if(!isCatalogOnDevice) {
         ds.addRecord();
         Vector unidades=units.getKeys();
         ds.writeInt(unidades.size());
         int tipo=new Unit(null).getID();
         for(int i=0; i<unidades.size(); i++) {
            ds.writeStringSafe((String)unidades.elementAt(i));
            ((Unit)units.get(unidades.elementAt(i))).saveState(ds);
         }
      }
      if(ds.setRecordPos(1)) {
         ds.deleteRecord();
      }
      ds.addRecord();

      //Ahora User y Stack
      //Grabamos los modos
      for(int i=0; i<settings.length; i++) {
         ds.writeInt(settings[i]);
      }

      //Grabamos el diccionario de datos
      Vector keys=homeDir.getHT().getKeys();
      int    sz=keys.size();
      ds.writeInt(sz);

      for(int i=0; i<sz; i++) {
         String skey=(String)keys.elementAt(i);
         ds.writeStringSafe(skey);

         Command value=(Command)homeDir.get(skey);
         ds.writeInt(value.getID());
         value.saveState(ds);
      }

      //Ahora opStack
      int szOs=operStack.size();
      ds.writeInt(szOs);

      for(int posOs=0; posOs<szOs; posOs++) {
         Command it=(Command)operStack.elementAt(posOs);
         ds.writeInt(it.getID());
         it.saveState(ds);
      }

      //Finalmente la linea de edicion
      ds.writeStringSafe(edit);
   }

   public void setOpStack(Stack ops) {
      operStack=ops;
   }

   /**
    * @param proc
    */
   public void setProcDebug(Debuggable proc) {
      procDebug.push(proc);
   }

   /* (non-Javadoc)
    * @see f8.kernel.CL#setSettings(int, boolean)
    */
   public void setSettings(int setting, boolean val) {
      settings[setting]=val ? 1 : 0;
   }

   /* (non-Javadoc)
    * @see f8.kernel.CL#setSettings(int, int)
    */
   public void setSettings(int setting, int val) {
      settings[setting]=val;
   }

   public int size() {
      return (operStack.size());
   }

   public String stackToString() {
      StringBuffer s=new StringBuffer();

      for(int i=0; i<operStack.size(); i++) {
         Command it=(Command)operStack.elementAt(i);
         s.append(it.toString());
         s.append('\n');
      }

      return (s.toString());
   }

   // - dict stack --------------------------------
   public void systemInstall(Command it) {
      String name=it.toString();
      if(systemDir.get(name)==null) {
         systemDir.put(it.toString(), it);
      } else {
         StaticCalcGUI.theGUI.log("Comando "+name+" repetido con id="
                                  +it.getID()
                                 );
      }
   }

   public String toString() {
      String s=new String();

      for(int i=0; i<operStack.size(); i++) {
         Command it=(Command)operStack.elementAt(i);
         s+=((operStack.size()-i)+": "+it.toString()+"\n");
      }

      return (s);
   }

   public void upDir() {
      if(!currentDict().getNombre().equals("HOME")) {
         dirStack.pop();
      }
   }
}
