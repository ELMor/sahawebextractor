/*
 * Created on 22-ago-2003
 *
 
 
 */
package f8;

import antlr.RecognitionException;
import antlr.TokenStreamException;
import antlr.collections.AST;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.ParseException;
import f8.kernel.yacc.ObjectLexer;
import f8.kernel.yacc.ObjectParser;
import f8.platform.DataStream;
import f8.platform.StringReader;
import f8.platform.UtilFactory;
import f8.platform.Vector;
import f8.types.utils.Copiable;

/**
 * @author elinares
 *

 
 */
public abstract class Stackable extends Command implements Copiable {
   public String tag=null;

   public abstract String getTypeName();

   public abstract Stackable copia();

	/**
	 * Descompone el objeto en sus elementos y los mete en la pila
	 * @param cl CalcLogica
	 * @return true si hubo un error
	 * @throws F8Exception
	 */
   public abstract boolean decompose(CL cl) throws F8Exception;

   public static void parse(String input, Vector vItems)
                     throws F8Exception {
      ObjectLexer  lexer     =new ObjectLexer(new StringReader(input));
      ObjectParser parser    =new ObjectParser(lexer);
      boolean      parseError=false;
      try {
         parser.hp48Object(vItems);
      } catch(RecognitionException e) {
			throw new ParseException(e.getMessage());
      } catch(TokenStreamException e) {
			throw new ParseException(e.getMessage());
      }
   }

   public AST getAST() {
      Vector v=UtilFactory.newVector();
      try {
         parse(toString(), v);
      } catch(F8Exception e) {
         return null;
      }
      return (AST)v.elementAt(0);
   }

   /**
    * @return
    */
   public String getTag() {
      return (tag==null) ? "" : tag;
   }

   /**
    * @param string
    */
   public void setTag(String string) {
      if(string==null) {
         tag="";
         return;
      }
      if(string.startsWith("\"")) {
         string=string.substring(1);
      }
      if(string.endsWith("\"")) {
         string=string.substring(0, string.length()-1);
      }

      tag=string;
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
    */
   public void loadState(DataStream ds) {
      tag=ds.readStringSafe();
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
    */
   public void saveState(DataStream ds) {
      ds.writeStringSafe(tag);
   }

   /* (non-Javadoc)
    * @see f8.kernel.Command#isAlgebraic()
    */
   public boolean isAlgebraic() {
      return true;
   }

   /* (non-Javadoc)
    * @see f8.kernel.Command#isInfix()
    */
   public boolean isInfix() {
      return false;
   }

   /* (non-Javadoc)
    * @see f8.kernel.Command#isOperator()
    */
   public boolean isOperator() {
      return false;
   }

}
