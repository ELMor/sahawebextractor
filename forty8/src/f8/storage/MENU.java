package f8.storage;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Lista;
import f8.types.Literal;
import f8.ui.Menu;

public final class MENU extends NonAlgebraic {
   public MENU() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 436;
   }

   public Storable getInstance() {
      return new MENU();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();

      if(cl.check(1)) {
         Command a=cl.peek(0);
         if(a instanceof Lista) {
            cl.push(new Literal("CST"));
            new STO().exec();
            showCustomMenu();
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("MENU");
   }

   public static void showCustomMenu() throws F8Exception {
      CL              cl =StaticCalcGUI.theGUI.getCalcLogica();
      Stackable cst=(Stackable)cl.lookup("CST");
      if(cst instanceof Lista) {
         Menu.createAndDisplayCustomMenu((Lista)cst);
      } else {
         throw new BadArgumentTypeException(new MENU());
      }
   }
}
