package f8.storage;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class STEQ extends NonAlgebraic {
   public STEQ() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 2094;
   }

   public Storable getInstance() {
      return new STEQ();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();

      if(cl.check(1)) {
         Command b   =cl.pop();
         String  name;
         name="EQ";
         STO.storeVar(name, b);
      } else {
         throw new TooFewArgumentsException(this);
      }
   }


   public String toString() {
      return ("STEQ");
   }
}
