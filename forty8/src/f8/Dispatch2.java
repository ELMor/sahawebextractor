package f8;

import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.types.Complex;
import f8.types.Double;
import f8.types.F8String;
import f8.types.F8Vector;
import f8.types.InfixExp;
import f8.types.Int;
import f8.types.Lista;
import f8.types.Matrix;
import f8.types.TempUnit;
import f8.types.Unit;

public abstract class Dispatch2 extends Command {
   public final void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(2)) {
         Stackable a  =cl.peek(1);
         Stackable b  =cl.peek(0);
         Stackable res=null;
         if(a instanceof Double&&b instanceof Double) {
            res=prfDoubleDouble((Double)a, (Double)b);
         } else if(a instanceof Double&&b instanceof Unit) {
            res=prfDoubleUnit((Double)a, (Unit)b);
         } else if(a instanceof Double&&b instanceof InfixExp) {
            res=prfDoubleInfixExp((Double)a, (InfixExp)b);
         } else if(a instanceof Double&&b instanceof Complex) {
            res=prfDoubleComplex((Double)a, (Complex)b);
         } else if(a instanceof Double&&b instanceof F8Vector) {
            res=prfDoubleF8Vector((Double)a, (F8Vector)b);
         } else if(a instanceof Double&&b instanceof Matrix) {
            res=prfDoubleMatrix((Double)a, (Matrix)b);
         } else if(a instanceof Double&&b instanceof Lista) {
            res=prfDoubleLista((Double)a, (Lista)b);
         } else if(a instanceof Double&&b instanceof F8String) {
            res=prfDoubleF8String((Double)a, (F8String)b);
         } else if(a instanceof Double&&b instanceof Int) {
            res=prfDoubleInt((Double)a, (Int)b);
         } else if(a instanceof Unit&&b instanceof Double) {
            res=prfUnitDouble((Unit)a, (Double)b);
         } else if(a instanceof Unit&&b instanceof Unit) {
            res=prfUnitUnit((Unit)a, (Unit)b);
         } else if(a instanceof Unit&&b instanceof InfixExp) {
            res=prfUnitInfixExp((Unit)a, (InfixExp)b);
         } else if(a instanceof Unit&&b instanceof Complex) {
            res=prfUnitComplex((Unit)a, (Complex)b);
         } else if(a instanceof Unit&&b instanceof F8Vector) {
            res=prfUnitF8Vector((Unit)a, (F8Vector)b);
         } else if(a instanceof Unit&&b instanceof Matrix) {
            res=prfUnitMatrix((Unit)a, (Matrix)b);
         } else if(a instanceof Unit&&b instanceof Lista) {
            res=prfUnitLista((Unit)a, (Lista)b);
         } else if(a instanceof Unit&&b instanceof F8String) {
            res=prfUnitF8String((Unit)a, (F8String)b);
         } else if(a instanceof Unit&&b instanceof Int) {
            res=prfUnitInt((Unit)a, (Int)b);
         } else if(a instanceof InfixExp&&b instanceof Double) {
            res=prfInfixExpDouble((InfixExp)a, (Double)b);
         } else if(a instanceof InfixExp&&b instanceof Unit) {
            res=prfInfixExpUnit((InfixExp)a, (Unit)b);
         } else if(a instanceof InfixExp&&b instanceof InfixExp) {
            res=prfInfixExpInfixExp((InfixExp)a, (InfixExp)b);
         } else if(a instanceof InfixExp&&b instanceof Complex) {
            res=prfInfixExpComplex((InfixExp)a, (Complex)b);
         } else if(a instanceof InfixExp&&b instanceof F8Vector) {
            res=prfInfixExpF8Vector((InfixExp)a, (F8Vector)b);
         } else if(a instanceof InfixExp&&b instanceof Matrix) {
            res=prfInfixExpMatrix((InfixExp)a, (Matrix)b);
         } else if(a instanceof InfixExp&&b instanceof Lista) {
            res=prfInfixExpLista((InfixExp)a, (Lista)b);
         } else if(a instanceof InfixExp&&b instanceof F8String) {
            res=prfInfixExpF8String((InfixExp)a, (F8String)b);
         } else if(a instanceof InfixExp&&b instanceof Int) {
            res=prfInfixExpInt((InfixExp)a, (Int)b);
         } else if(a instanceof Complex&&b instanceof Double) {
            res=prfComplexDouble((Complex)a, (Double)b);
         } else if(a instanceof Complex&&b instanceof Unit) {
            res=prfComplexUnit((Complex)a, (Unit)b);
         } else if(a instanceof Complex&&b instanceof InfixExp) {
            res=prfComplexInfixExp((Complex)a, (InfixExp)b);
         } else if(a instanceof Complex&&b instanceof Complex) {
            res=prfComplexComplex((Complex)a, (Complex)b);
         } else if(a instanceof Complex&&b instanceof F8Vector) {
            res=prfComplexF8Vector((Complex)a, (F8Vector)b);
         } else if(a instanceof Complex&&b instanceof Matrix) {
            res=prfComplexMatrix((Complex)a, (Matrix)b);
         } else if(a instanceof Complex&&b instanceof Lista) {
            res=prfComplexLista((Complex)a, (Lista)b);
         } else if(a instanceof Complex&&b instanceof F8String) {
            res=prfComplexF8String((Complex)a, (F8String)b);
         } else if(a instanceof Complex&&b instanceof Int) {
            res=prfComplexInt((Complex)a, (Int)b);
         } else if(a instanceof F8Vector&&b instanceof Double) {
            res=prfF8VectorDouble((F8Vector)a, (Double)b);
         } else if(a instanceof F8Vector&&b instanceof Unit) {
            res=prfF8VectorUnit((F8Vector)a, (Unit)b);
         } else if(a instanceof F8Vector&&b instanceof InfixExp) {
            res=prfF8VectorInfixExp((F8Vector)a, (InfixExp)b);
         } else if(a instanceof F8Vector&&b instanceof Complex) {
            res=prfF8VectorComplex((F8Vector)a, (Complex)b);
         } else if(a instanceof F8Vector&&b instanceof F8Vector) {
            res=prfF8VectorF8Vector((F8Vector)a, (F8Vector)b);
         } else if(a instanceof F8Vector&&b instanceof Matrix) {
            res=prfF8VectorMatrix((F8Vector)a, (Matrix)b);
         } else if(a instanceof F8Vector&&b instanceof Lista) {
            res=prfF8VectorLista((F8Vector)a, (Lista)b);
         } else if(a instanceof F8Vector&&b instanceof F8String) {
            res=prfF8VectorF8String((F8Vector)a, (F8String)b);
         } else if(a instanceof F8Vector&&b instanceof Int) {
            res=prfF8VectorInt((F8Vector)a, (Int)b);
         } else if(a instanceof Matrix&&b instanceof Double) {
            res=prfMatrixDouble((Matrix)a, (Double)b);
         } else if(a instanceof Matrix&&b instanceof Unit) {
            res=prfMatrixUnit((Matrix)a, (Unit)b);
         } else if(a instanceof Matrix&&b instanceof InfixExp) {
            res=prfMatrixInfixExp((Matrix)a, (InfixExp)b);
         } else if(a instanceof Matrix&&b instanceof Complex) {
            res=prfMatrixComplex((Matrix)a, (Complex)b);
         } else if(a instanceof Matrix&&b instanceof F8Vector) {
            res=prfMatrixF8Vector((Matrix)a, (F8Vector)b);
         } else if(a instanceof Matrix&&b instanceof Matrix) {
            res=prfMatrixMatrix((Matrix)a, (Matrix)b);
         } else if(a instanceof Matrix&&b instanceof Lista) {
            res=prfMatrixLista((Matrix)a, (Lista)b);
         } else if(a instanceof Matrix&&b instanceof F8String) {
            res=prfMatrixF8String((Matrix)a, (F8String)b);
         } else if(a instanceof Matrix&&b instanceof Int) {
            res=prfMatrixInt((Matrix)a, (Int)b);
         } else if(a instanceof Lista&&b instanceof Double) {
            res=prfListaDouble((Lista)a, (Double)b);
         } else if(a instanceof Lista&&b instanceof Unit) {
            res=prfListaUnit((Lista)a, (Unit)b);
         } else if(a instanceof Lista&&b instanceof InfixExp) {
            res=prfListaInfixExp((Lista)a, (InfixExp)b);
         } else if(a instanceof Lista&&b instanceof Complex) {
            res=prfListaComplex((Lista)a, (Complex)b);
         } else if(a instanceof Lista&&b instanceof F8Vector) {
            res=prfListaF8Vector((Lista)a, (F8Vector)b);
         } else if(a instanceof Lista&&b instanceof Matrix) {
            res=prfListaMatrix((Lista)a, (Matrix)b);
         } else if(a instanceof Lista&&b instanceof Lista) {
            res=prfListaLista((Lista)a, (Lista)b);
         } else if(a instanceof Lista&&b instanceof F8String) {
            res=prfListaF8String((Lista)a, (F8String)b);
         } else if(a instanceof Lista&&b instanceof Int) {
            res=prfListaInt((Lista)a, (Int)b);
         } else if(a instanceof F8String&&b instanceof Double) {
            res=prfF8StringDouble((F8String)a, (Double)b);
         } else if(a instanceof F8String&&b instanceof Unit) {
            res=prfF8StringUnit((F8String)a, (Unit)b);
         } else if(a instanceof F8String&&b instanceof InfixExp) {
            res=prfF8StringInfixExp((F8String)a, (InfixExp)b);
         } else if(a instanceof F8String&&b instanceof Complex) {
            res=prfF8StringComplex((F8String)a, (Complex)b);
         } else if(a instanceof F8String&&b instanceof F8Vector) {
            res=prfF8StringF8Vector((F8String)a, (F8Vector)b);
         } else if(a instanceof F8String&&b instanceof Matrix) {
            res=prfF8StringMatrix((F8String)a, (Matrix)b);
         } else if(a instanceof F8String&&b instanceof Lista) {
            res=prfF8StringLista((F8String)a, (Lista)b);
         } else if(a instanceof F8String&&b instanceof F8String) {
            res=prfF8StringF8String((F8String)a, (F8String)b);
         } else if(a instanceof F8String&&b instanceof Int) {
            res=prfF8StringInt((F8String)a, (Int)b);
         } else if(a instanceof Int&&b instanceof Double) {
            res=prfIntDouble((Int)a, (Double)b);
         } else if(a instanceof Int&&b instanceof Unit) {
            res=prfIntUnit((Int)a, (Unit)b);
         } else if(a instanceof Int&&b instanceof InfixExp) {
            res=prfIntInfixExp((Int)a, (InfixExp)b);
         } else if(a instanceof Int&&b instanceof Complex) {
            res=prfIntComplex((Int)a, (Complex)b);
         } else if(a instanceof Int&&b instanceof F8Vector) {
            res=prfIntF8Vector((Int)a, (F8Vector)b);
         } else if(a instanceof Int&&b instanceof Matrix) {
            res=prfIntMatrix((Int)a, (Matrix)b);
         } else if(a instanceof Int&&b instanceof Lista) {
            res=prfIntLista((Int)a, (Lista)b);
         } else if(a instanceof Int&&b instanceof F8String) {
            res=prfIntF8String((Int)a, (F8String)b);
         } else if(a instanceof Int&&b instanceof Int) {
            res=prfIntInt((Int)a, (Int)b);
         } else if(a instanceof TempUnit && b instanceof TempUnit){
         	res=prfTempUnitTempUnit((TempUnit)a,(TempUnit)b);
         }

         if(res!=null) {
            cl.pop(2);
            cl.push(res);
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public Stackable prfDoubleDouble(Double a, Double b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfDoubleUnit(Double a, Unit b) throws F8Exception {
      return null;
   }

   public Stackable prfDoubleInfixExp(Double a, InfixExp b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfDoubleComplex(Double a, Complex b)
                              throws F8Exception {
      return null;
   }

   public Stackable prfDoubleF8Vector(Double a, F8Vector b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfDoubleMatrix(Double a, Matrix b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfDoubleLista(Double a, Lista b) throws F8Exception {
      return null;
   }

   public Stackable prfDoubleF8String(Double a, F8String b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfDoubleInt(Double a, Int b) throws F8Exception {
      return null;
   }

   public Stackable prfUnitDouble(Unit a, Double b) throws F8Exception {
      return null;
   }

   public Stackable prfUnitUnit(Unit a, Unit b) throws F8Exception {
      return null;
   }

   public Stackable prfUnitInfixExp(Unit a, InfixExp b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfUnitComplex(Unit a, Complex b) throws F8Exception {
      return null;
   }

   public Stackable prfUnitF8Vector(Unit a, F8Vector b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfUnitMatrix(Unit a, Matrix b) throws F8Exception {
      return null;
   }

   public Stackable prfUnitLista(Unit a, Lista b) throws F8Exception {
      return null;
   }

   public Stackable prfUnitF8String(Unit a, F8String b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfUnitInt(Unit a, Int b) throws F8Exception {
      return null;
   }

   public Stackable prfInfixExpDouble(InfixExp a, Double b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfInfixExpUnit(InfixExp a, Unit b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b)
                                 throws F8Exception {
      return null;
   }

   public Stackable prfInfixExpComplex(InfixExp a, Complex b)
                                throws F8Exception {
      return null;
   }

   public Stackable prfInfixExpF8Vector(InfixExp a, F8Vector b)
                                 throws F8Exception {
      return null;
   }

   public Stackable prfInfixExpMatrix(InfixExp a, Matrix b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfInfixExpLista(InfixExp a, Lista b)
                              throws F8Exception {
      return null;
   }

   public Stackable prfInfixExpF8String(InfixExp a, F8String b)
                                 throws F8Exception {
      return null;
   }

   public Stackable prfInfixExpInt(InfixExp a, Int b) throws F8Exception {
      return null;
   }

   public Stackable prfComplexDouble(Complex a, Double b)
                              throws F8Exception {
      return null;
   }

   public Stackable prfComplexUnit(Complex a, Unit b) throws F8Exception {
      return null;
   }

   public Stackable prfComplexInfixExp(Complex a, InfixExp b)
                                throws F8Exception {
      return null;
   }

   public Stackable prfComplexComplex(Complex a, Complex b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfComplexF8Vector(Complex a, F8Vector b)
                                throws F8Exception {
      return null;
   }

   public Stackable prfComplexMatrix(Complex a, Matrix b)
                              throws F8Exception {
      return null;
   }

   public Stackable prfComplexLista(Complex a, Lista b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfComplexF8String(Complex a, F8String b)
                                throws F8Exception {
      return null;
   }

   public Stackable prfComplexInt(Complex a, Int b) throws F8Exception {
      return null;
   }

   public Stackable prfF8VectorDouble(F8Vector a, Double b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfF8VectorUnit(F8Vector a, Unit b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfF8VectorInfixExp(F8Vector a, InfixExp b)
                                 throws F8Exception {
      return null;
   }

   public Stackable prfF8VectorComplex(F8Vector a, Complex b)
                                throws F8Exception {
      return null;
   }

   public Stackable prfF8VectorF8Vector(F8Vector a, F8Vector b)
                                 throws F8Exception {
      return null;
   }

   public Stackable prfF8VectorMatrix(F8Vector a, Matrix b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfF8VectorLista(F8Vector a, Lista b)
                              throws F8Exception {
      return null;
   }

   public Stackable prfF8VectorF8String(F8Vector a, F8String b)
                                 throws F8Exception {
      return null;
   }

   public Stackable prfF8VectorInt(F8Vector a, Int b) throws F8Exception {
      return null;
   }

   public Stackable prfMatrixDouble(Matrix a, Double b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfMatrixUnit(Matrix a, Unit b) throws F8Exception {
      return null;
   }

   public Stackable prfMatrixInfixExp(Matrix a, InfixExp b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfMatrixComplex(Matrix a, Complex b)
                              throws F8Exception {
      return null;
   }

   public Stackable prfMatrixF8Vector(Matrix a, F8Vector b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfMatrixMatrix(Matrix a, Matrix b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfMatrixLista(Matrix a, Lista b) throws F8Exception {
      return null;
   }

   public Stackable prfMatrixF8String(Matrix a, F8String b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfMatrixInt(Matrix a, Int b) throws F8Exception {
      return null;
   }

   public Stackable prfListaDouble(Lista a, Double b) throws F8Exception {
      return null;
   }

   public Stackable prfListaUnit(Lista a, Unit b) throws F8Exception {
      return null;
   }

   public Stackable prfListaInfixExp(Lista a, InfixExp b)
                              throws F8Exception {
      return null;
   }

   public Stackable prfListaComplex(Lista a, Complex b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfListaF8Vector(Lista a, F8Vector b)
                              throws F8Exception {
      return null;
   }

   public Stackable prfListaMatrix(Lista a, Matrix b) throws F8Exception {
      return null;
   }

   public Stackable prfListaLista(Lista a, Lista b) throws F8Exception {
      return null;
   }

   public Stackable prfListaF8String(Lista a, F8String b)
                              throws F8Exception {
      return null;
   }

   public Stackable prfListaInt(Lista a, Int b) throws F8Exception {
      return null;
   }

   public Stackable prfF8StringDouble(F8String a, Double b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfF8StringUnit(F8String a, Unit b)
                             throws F8Exception {
      return null;
   }

   public Stackable prfF8StringInfixExp(F8String a, InfixExp b)
                                 throws F8Exception {
      return null;
   }

   public Stackable prfF8StringComplex(F8String a, Complex b)
                                throws F8Exception {
      return null;
   }

   public Stackable prfF8StringF8Vector(F8String a, F8Vector b)
                                 throws F8Exception {
      return null;
   }

   public Stackable prfF8StringMatrix(F8String a, Matrix b)
                               throws F8Exception {
      return null;
   }

   public Stackable prfF8StringLista(F8String a, Lista b)
                              throws F8Exception {
      return null;
   }

   public Stackable prfF8StringF8String(F8String a, F8String b)
                                 throws F8Exception {
      return null;
   }

   public Stackable prfF8StringInt(F8String a, Int b) throws F8Exception {
      return null;
   }

   public Stackable prfIntDouble(Int a, Double b) throws F8Exception {
      return null;
   }

   public Stackable prfIntUnit(Int a, Unit b) throws F8Exception {
      return null;
   }

   public Stackable prfIntInfixExp(Int a, InfixExp b) throws F8Exception {
      return null;
   }

   public Stackable prfIntComplex(Int a, Complex b) throws F8Exception {
      return null;
   }

   public Stackable prfIntF8Vector(Int a, F8Vector b) throws F8Exception {
      return null;
   }

   public Stackable prfIntMatrix(Int a, Matrix b) throws F8Exception {
      return null;
   }

   public Stackable prfIntLista(Int a, Lista b) throws F8Exception {
      return null;
   }

   public Stackable prfIntF8String(Int a, F8String b) throws F8Exception {
      return null;
   }

   public Stackable prfIntInt(Int a, Int b) throws F8Exception {
	  return null;
   }
   
   public Stackable prfTempUnitTempUnit(TempUnit a, TempUnit b) throws F8Exception {
	  return null;
   }
}
