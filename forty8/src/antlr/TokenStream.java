package antlr;


/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: TokenStream.java,v 1.1 2004/03/13 10:34:46 elinares Exp $
 */
public interface TokenStream {
   public Token nextToken() throws TokenStreamException;
}
