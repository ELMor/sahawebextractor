/* CounterImageOutputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import javax.imageio.stream.ImageOutputStreamImpl;

final class CounterImageOutputStream extends ImageOutputStreamImpl
{
    int c;
    
    public void write(byte[] is, int i, int i_0_) {
	c += i_0_;
    }
    
    public void write(int i) {
	c++;
    }
    
    public int read(byte[] is, int i, int i_1_) {
	throw new Error("Unsupported, write-only stream");
    }
    
    public int read() {
	throw new Error("Unsupported, write-only stream");
    }
    
    public void seek(long l) {
	throw new Error("Unsupported seek");
    }
    
    public int getCounter() {
	return c;
    }
}
