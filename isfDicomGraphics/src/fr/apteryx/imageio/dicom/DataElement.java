/* DataElement - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;

public final class DataElement implements Serializable
{
    public final int tag;
    private transient Attribute attribute;
    public final Object value;
    
    public DataElement(int i, Object object) {
	tag = i;
	value = object;
    }
    
    DataElement(int i, Object object, Attribute attribute) {
	this(i, object);
	this.attribute = attribute;
    }
    
    DataElement(int i, int i_0_) {
	this(i, new Integer(i_0_));
    }
    
    final synchronized Attribute getAttribute() {
	if (attribute == null)
	    attribute = Dictionary.getAttribute(tag);
	return attribute;
    }
    
    String toString(String string) {
	String string_1_;
	if (value == null)
	    string_1_ = "<null> ";
	else if (value.getClass().isArray()) {
	    StringBuffer stringbuffer = new StringBuffer();
	    for (int i = 0; i < Array.getLength(value); i++) {
		if (i > 0)
		    stringbuffer.append(", ");
		Object object = Array.get(value, i);
		stringbuffer
		    .append(object == null ? "<null>" : object.toString());
	    }
	    string_1_ = new String(stringbuffer);
	} else if (value instanceof Collection) {
	    StringBuffer stringbuffer = new StringBuffer("[\n");
	    Iterator iterator = ((Collection) value).iterator();
	    while (iterator.hasNext()) {
		Object object = iterator.next();
		if (object instanceof DataSet) {
		    stringbuffer
			.append(((DataSet) object).toString(string + " "));
		    if (iterator.hasNext()) {
			stringbuffer.append(string);
			stringbuffer.append(" ---\n");
		    }
		} else {
		    stringbuffer.append(string);
		    stringbuffer
			.append(object == null ? "<null>" : object.toString());
		    stringbuffer.append("\n");
		}
	    }
	    stringbuffer.append(string);
	    stringbuffer.append(']');
	    string_1_ = new String(stringbuffer);
	} else
	    string_1_ = value.toString();
	Attribute attribute = getAttribute();
	if (attribute != null && attribute.name != null)
	    return attribute.name + " = " + string_1_;
	return Tag.toString(tag) + " = " + string_1_;
    }
    
    public String getVR() {
	if (attribute == null)
	    attribute = Dictionary.getAttribute(tag);
	return attribute == null ? null : attribute.VR;
    }
    
    public String toString() {
	return toString("");
    }
    
    public boolean equals(Object object) {
	if (object == null || !(object instanceof DataElement))
	    return false;
	DataElement dataelement_2_ = (DataElement) object;
	if (dataelement_2_.tag != tag)
	    return false;
	if (value == null)
	    return dataelement_2_.value == null;
	return value.equals(dataelement_2_.value);
    }
}
