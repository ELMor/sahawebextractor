/* VOIWindowColorModel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.color.ColorSpace;

public final class VOIWindowColorModel extends VOIColorModel
{
    private final float cs_min;
    private final float cs_max;
    private final float w_center;
    private final float w_width;
    private final int x_low;
    private final int x_high;
    private final double slope;
    private final double intercept;
    private final double slope_unnorm;
    private final double intercept_unnorm;
    private final double slope_255;
    private final double intercept_255;
    
    public VOIWindowColorModel(int i, int i_0_, float f, float f_1_,
			       ColorSpace colorspace) {
	super(i, i_0_, i_0_, colorspace);
	cs_min = colorspace.getMinValue(0);
	cs_max = colorspace.getMaxValue(0);
	w_center = f;
	w_width = f_1_;
	x_low = ceil((double) w_center - 0.5 - (double) ((w_width - 1.0F)
							 / 2.0F));
	x_high = floor((double) w_center - 0.5 + (double) ((w_width - 1.0F)
							   / 2.0F));
	slope = (double) ((cs_max - cs_min) / (w_width - 1.0F));
	intercept = (((double) (cs_max - cs_min)
		      * (0.5 - ((double) w_center - 0.5) / (double) (w_width
								     - 1.0F)))
		     + (double) cs_min);
	slope_unnorm = (double) ((float) unnorm_max / (w_width - 1.0F));
	intercept_unnorm
	    = (double) unnorm_max * (0.5 - (((double) w_center - 0.5)
					    / (double) (w_width - 1.0F)));
	slope_255 = 255.0 / (double) (w_width - 1.0F);
	intercept_255
	    = 255.0 * (0.5 - ((double) w_center - 0.5) / (double) (w_width
								   - 1.0F));
    }
    
    public int getMin() {
	return x_low;
    }
    
    public int getMax() {
	return x_high;
    }
    
    public float getCenter() {
	return w_center;
    }
    
    public float getWidth() {
	return w_width;
    }
    
    int getComp(int i) {
	double d = (double) i * slope_unnorm + intercept_unnorm + 0.5;
	return d < 0.0 ? 0 : d > (double) unnorm_max ? unnorm_max : (int) d;
    }
    
    int get255(int i) {
	double d = (double) i * slope_255 + intercept_255 + 0.5;
	return d < 0.0 ? 0 : d > 255.0 ? 255 : (int) d;
    }
    
    float getNorm(int i) {
	float f = (float) ((double) i * slope + intercept);
	return f < cs_min ? cs_min : f > cs_max ? cs_max : f;
    }
    
    int getDataElement(float f) {
	double d = ((double) f - intercept) / slope;
	int i = (int) (d > 0.0 ? d + 0.5 : d - 0.5);
	return i > x_high ? x_high : i < x_low ? x_low : i;
    }
    
    private int ceil(double d) {
	int i = (int) d;
	if (d == (double) i)
	    return i;
	return (int) (d < 0.0 ? d : d + 1.0);
    }
    
    private int floor(double d) {
	int i = (int) d;
	if (d == (double) i)
	    return i;
	return (int) (d > 0.0 ? d : d - 1.0);
    }
}
