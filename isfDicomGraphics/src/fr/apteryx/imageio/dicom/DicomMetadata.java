/*
 * DicomMetadata - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.SampleModel;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DicomMetadata extends IIOMetadata implements Serializable {
    final DicomObject obj;

    /* synthetic */static Class array$Ljava$lang$Number;

    /* synthetic */static Class array$S;

    /* synthetic */static Class class$fr$apteryx$imageio$dicom$PixelData;

    /* synthetic */static Class array$Ljava$lang$Float;

    /* synthetic */static Class array$Ljava$lang$Integer;

    /* synthetic */static Class class$java$lang$Integer;

    /* synthetic */static Class class$java$util$Date;

    /* synthetic */static Class class$java$lang$Object;

    DicomMetadata(DataSet dataset, String string, String string_0_,
            TransferSyntax transfersyntax) {
        this(new DicomObject(dataset, string, string_0_, transfersyntax));
    }

    public DicomMetadata() {
        this(new DataSet(), null, null, null);
    }

    DicomMetadata(DicomObject dicomobject) {
        obj = dicomobject;
        nativeMetadataFormatClassName = "DicomMetadataFormat";
        nativeMetadataFormatName = "fr_apteryx_imageio_dicom_1.0";
        standardFormatSupported = true;
    }

    public boolean isReadOnly() {
        return true;
    }

    public Node getAsTree(String string) {
        if (string.equals("javax_imageio_1.0"))
            return getStandardTree();
        IIOMetadataNode iiometadatanode = new IIOMetadataNode(
                nativeMetadataFormatName);
        Iterator iterator = obj.ds.values().iterator();
        while (iterator.hasNext()) {
            DataElement dataelement = (DataElement) iterator.next();
            Attribute attribute = dataelement.getAttribute();
            IIOMetadataNode iiometadatanode_1_ = new IIOMetadataNode(
                    attribute == null ? Tag.toString(dataelement.tag)
                            : attribute.name);
            iiometadatanode_1_.setUserObject(dataelement);
            iiometadatanode.appendChild(iiometadatanode_1_);
        }
        return iiometadatanode;
    }

    private Node getChild(Node node, String string) {
        NodeList nodelist = node.getChildNodes();
        for (int i = 0; i < nodelist.getLength(); i++) {
            if (string.equalsIgnoreCase(nodelist.item(i).getNodeName()))
                return nodelist.item(i);
        }
        return null;
    }

    private String getAttr(Node node, String string) {
        return node.getAttributes().getNamedItem(string).getNodeValue();
    }

    public void mergeTree(String string, Node node)
            throws IIOInvalidTreeException {
        if (string.equals("javax_imageio_1.0")) {
            Node node_2_ = getChild(node, "Chroma");
            if (node_2_ != null) {
                Node node_3_ = getChild(node_2_, "BlackIsZero");
                if (node_3_ != null
                        && "false".equalsIgnoreCase(getAttr(node_3_, "value")))
                    obj.ds.add(2621444, "MONOCHROME1");
                node_3_ = getChild(node_2_, "Palette");
                if (node_3_ != null) {
                    NodeList nodelist = node_3_.getChildNodes();
                    int i = nodelist.getLength();
                    if (i > 0) {
                        short[] is = new short[i];
                        short[] is_4_ = new short[i];
                        short[] is_5_ = new short[i];
                        int i_6_ = 2147483647;
                        for (int i_7_ = 0; i_7_ < i; i_7_++) {
                            int i_8_ = Integer
                                    .parseInt(((IIOMetadataNode) nodelist
                                            .item(i_7_)).getAttribute("index"));
                            if (i_8_ < i_6_)
                                i_6_ = i_8_;
                        }
                        boolean bool = true;
                        try {
                            for (int i_9_ = 0; i_9_ < i; i_9_++) {
                                IIOMetadataNode iiometadatanode = (IIOMetadataNode) nodelist
                                        .item(i_9_);
                                int i_10_ = (Integer.parseInt(iiometadatanode
                                        .getAttribute("index")));
                                is[i_10_ - i_6_] = (short) (Integer
                                        .parseInt(iiometadatanode
                                                .getAttribute("red")));
                                is_4_[i_10_ - i_6_] = (short) (Integer
                                        .parseInt(iiometadatanode
                                                .getAttribute("green")));
                                is_5_[i_10_ - i_6_] = (short) (Integer
                                        .parseInt(iiometadatanode
                                                .getAttribute("blue")));
                                if (bool
                                        && (is[i_10_ - i_6_] > 255
                                                || is_4_[i_10_ - i_6_] > 255 || is_5_[i_10_
                                                - i_6_] > 255))
                                    bool = false;
                            }
                        } catch (Exception exception) {
                            throw new IIOInvalidTreeException(
                                    "Invalid specification", node_3_);
                        }
                        short[] is_11_ = { (short) i, (short) i_6_,
                                bool ? (short) 8 : (short) 16 };
                        obj.ds.add(2625793, is_11_);
                        obj.ds.add(2625794, is_11_);
                        obj.ds.add(2625795, is_11_);
                        obj.ds.add(2626049, is);
                        obj.ds.add(2626050, is_4_);
                        obj.ds.add(2626051, is_5_);
                    }
                }
            }
            node_2_ = getChild(node, "Dimension");
            if (node_2_ != null) {
                try {
                    Float var_float = (Float.valueOf(getAttr(getChild(node_2_,
                            "HorizontalPhysicalPixelSpacing"), "value")));
                    Float var_float_12_ = (Float.valueOf(getAttr(getChild(
                            node_2_, "VerticalPhysicalPixelSpacing"), "value")));
                    obj.ds.add(2621488,
                            new Float[] { var_float_12_, var_float });
                } catch (Exception exception) {
                    /* empty */
                }
            }
            node_2_ = getChild(node, "Document");
            if (node_2_ != null) {
                Node node_13_ = getChild(node_2_, "ImageCreationTime");
                if (node_13_ != null) {
                    Date date = standardToDate(node_13_);
                    obj.ds.add(524323, new Day(date));
                    obj.ds.add(524339, new Time(date));
                }
                node_13_ = getChild(node_2_, "ImageModificationTime");
                if (node_13_ != null) {
                    Date date = standardToDate(node_13_);
                    obj.ds.add(1576978, new Day(date));
                    obj.ds.add(1576980, new Time(date));
                }
            }
        } else if (string.equals(nativeMetadataFormatName)) {
            NodeList nodelist = node.getChildNodes();
            DataSet dataset = obj.ds;
            for (int i = 0; i < nodelist.getLength(); i++) {
                IIOMetadataNode iiometadatanode = (IIOMetadataNode) nodelist
                        .item(i);
                DataElement dataelement = (DataElement) iiometadatanode
                        .getUserObject();
                dataset.add(dataelement);
            }
        } else
            throw new IllegalArgumentException("Unsupported format name: "
                    + string);
    }

    public void reset() {
        obj.reset();
    }

    public String toString() {
        return obj.toString();
    }

    private void addNode(IIOMetadataNode iiometadatanode, String string,
            String string_14_, String string_15_) {
        IIOMetadataNode iiometadatanode_16_ = new IIOMetadataNode(string);
        iiometadatanode_16_.setAttribute(string_14_, string_15_);
        iiometadatanode.appendChild(iiometadatanode_16_);
    }

    protected IIOMetadataNode getStandardChromaNode() {
        DataSet dataset = obj.ds;
        String string = dataset.findString(2621444);
        if (string == null)
            return null;
        IIOMetadataNode iiometadatanode = new IIOMetadataNode("Chroma");
        String string_17_ = DicomColorSpace.tagToIIOStandard(string);
        if (string_17_ != null)
            addNode(iiometadatanode, "ColorSpaceType", "name", string_17_);
        string_17_ = dataset.findString(2621442);
        if (string_17_ != null)
            addNode(iiometadatanode, "NumChannels", "value", string_17_);
        addNode(iiometadatanode, "BlackIsZero", "value", string
                .equals("MONOCHROME1") ? "FALSE" : "TRUE");
        if ("PALETTE COLOR".equals(string)) {
            IIOMetadataNode iiometadatanode_18_ = new IIOMetadataNode("Palette");
            iiometadatanode.appendChild(iiometadatanode_18_);
            Number[] numbers = ((Number[]) dataset.findValue(2625793,
                    Number[].class));
            Number[] numbers_19_ = ((Number[]) dataset.findValue(2625794,
                    Number[].class));
            Number[] numbers_20_ = ((Number[]) dataset.findValue(2625795,
                    Number[].class));
            short[] is = ((short[]) dataset.findValue(2626049, short[].class));
            short[] is_21_ = ((short[]) dataset.findValue(2626050,
                    short[].class));
            short[] is_22_ = ((short[]) dataset.findValue(2626051,
                    short[].class));
            int i = 2147483647;
            int i_23_ = -2147483648;
            int i_24_ = numbers[1].intValue();
            int i_25_ = numbers[0].intValue();
            if (i_25_ == 0)
                i_25_ = 65536;
            int i_26_ = numbers_19_[1].intValue();
            int i_27_ = numbers_19_[0].intValue();
            if (i_27_ == 0)
                i_27_ = 65536;
            int i_28_ = numbers_20_[1].intValue();
            int i_29_ = numbers_20_[0].intValue();
            if (i_29_ == 0)
                i_29_ = 65536;
            if (i_24_ < i)
                i = i_24_;
            if (i_26_ < i)
                i = i_26_;
            if (i_28_ < i)
                i = i_28_;
            if (i_24_ + i_25_ > i_23_)
                i_23_ = i_24_ + i_25_;
            if (i_26_ + i_27_ > i_23_)
                i_23_ = i_26_ + i_27_;
            if (i_28_ + i_29_ > i_23_)
                i_23_ = i_28_ + i_29_;
            for (int i_30_ = i; i_30_ < i_23_; i_30_++) {
                IIOMetadataNode iiometadatanode_31_ = new IIOMetadataNode(
                        "PaletteEntry");
                iiometadatanode_18_.appendChild(iiometadatanode_31_);
                iiometadatanode_31_.setAttribute("index", Integer
                        .toString(i_30_));
                int i_32_ = i_30_ - i_24_;
                if (i_32_ < 0)
                    i_32_ = 0;
                if (i_32_ >= i_25_)
                    i_32_ = i_25_;
                int i_33_ = i_30_ - i_26_;
                if (i_33_ < 0)
                    i_33_ = 0;
                if (i_33_ >= i_27_)
                    i_33_ = i_27_;
                int i_34_ = i_30_ - i_28_;
                if (i_34_ < 0)
                    i_34_ = 0;
                if (i_34_ >= i_29_)
                    i_34_ = i_29_;
                iiometadatanode_31_.setAttribute("red", Integer
                        .toString(is[i_32_] & 0xffff));
                iiometadatanode_31_.setAttribute("green", Integer
                        .toString(is_21_[i_33_] & 0xffff));
                iiometadatanode_31_.setAttribute("blue", Integer
                        .toString(is_22_[i_34_] & 0xffff));
            }
        }
        return iiometadatanode;
    }

    protected IIOMetadataNode getStandardCompressionNode() {
        PixelData pixeldata = (PixelData) (obj.ds.findValue(2145386512,
                PixelData.class));
        if (pixeldata == null)
            return null;
        IIOMetadataNode iiometadatanode = null;
        TransferSyntax transfersyntax = pixeldata.TS;
        if (transfersyntax == TransferSyntax.RLE_COMPRESSION) {
            iiometadatanode = new IIOMetadataNode("Compression");
            addNode(iiometadatanode, "CompressionTypeName", "value", "RLE");
            addNode(iiometadatanode, "Lossless", "value", "TRUE");
        } else if (transfersyntax == TransferSyntax.JPEG_BASELINE_1
                || transfersyntax == TransferSyntax.JPEG_EXTENDED_2_4
                || (transfersyntax == TransferSyntax.JPEG_PROGRESSIVE_10_12)) {
            iiometadatanode = new IIOMetadataNode("Compression");
            addNode(iiometadatanode, "CompressionTypeName", "value", "JPEG");
            addNode(iiometadatanode, "Lossless", "value", "FALSE");
        } else if (transfersyntax == TransferSyntax.JPEG_LOSSLESS_14
                || transfersyntax == TransferSyntax.JPEG_LOSSLESS_15
                || transfersyntax == TransferSyntax.JPEG_LOSSLESS_14SV1) {
            iiometadatanode = new IIOMetadataNode("Compression");
            addNode(iiometadatanode, "CompressionTypeName", "value",
                    "JPEG-LOSSLESS");
            addNode(iiometadatanode, "Lossless", "value", "TRUE");
        } else if (transfersyntax == TransferSyntax.JPEG_LS_LOSSLESS) {
            iiometadatanode = new IIOMetadataNode("Compression");
            addNode(iiometadatanode, "CompressionTypeName", "value", "JPEG-LS");
            addNode(iiometadatanode, "Lossless", "value", "TRUE");
        } else if (transfersyntax == TransferSyntax.JPEG_LS_NEAR_LOSSLESS) {
            iiometadatanode = new IIOMetadataNode("Compression");
            addNode(iiometadatanode, "CompressionTypeName", "value", "JPEG-LS");
            addNode(iiometadatanode, "Lossless", "value", "FALSE");
        } else if (transfersyntax == TransferSyntax.JPEG2000_LOSSLESS) {
            iiometadatanode = new IIOMetadataNode("Compression");
            addNode(iiometadatanode, "CompressionTypeName", "value", "JPEG2000");
            addNode(iiometadatanode, "Lossless", "value", "TRUE");
        } else if (transfersyntax == TransferSyntax.JPEG2000) {
            iiometadatanode = new IIOMetadataNode("Compression");
            addNode(iiometadatanode, "CompressionTypeName", "value", "JPEG2000");
            addNode(iiometadatanode, "Lossless", "value", "FALSE");
        } else if (transfersyntax == TransferSyntax.DEFLATED_EXPLICIT_LITTLEENDIAN) {
            iiometadatanode = new IIOMetadataNode("Compression");
            addNode(iiometadatanode, "CompressionTypeName", "value", "DEFLATE");
            addNode(iiometadatanode, "Lossless", "value", "TRUE");
        }
        return iiometadatanode;
    }

    protected IIOMetadataNode getStandardDataNode() {
        DataSet dataset = obj.ds;
        int i = dataset.findInt(2621442);
        if (i <= 0)
            return null;
        IIOMetadataNode iiometadatanode = new IIOMetadataNode("Data");
        if (i > 1) {
            PixelData pixeldata = ((PixelData) (dataset.findValue(2145386512,
                    PixelData.class)));
            if (pixeldata == null)
                return null;
            TransferSyntax transfersyntax = pixeldata.TS;
            int i_35_ = dataset.findInt(2621446);
            addNode(
                    iiometadatanode,
                    "PlanarConfiguration",
                    "value",
                    (transfersyntax == TransferSyntax.RLE_COMPRESSION
                            || transfersyntax.uid
                                    .startsWith("1.2.840.10008.1.2.4.") || i_35_ == 0) ? "PixelInterleaved"
                            : "PlaneInterleaved");
        }
        int i_36_ = dataset.findInt(2621699);
        String string = dataset.findString(2621444);
        addNode(iiometadatanode, "SampleFormat", "value", ("PALETTE COLOR"
                .equals(string) ? "Index" : i_36_ == 0 ? "UnsignedIntegral"
                : "SignedIntegral"));
        addNode(iiometadatanode, "BitsPerSample", "value", dataset
                .findString(2621696));
        addNode(iiometadatanode, "SignificantBitsPerSample", "value", dataset
                .findString(2621697));
        addNode(iiometadatanode, "SampleMSB", "value", dataset
                .findString(2621698));
        return iiometadatanode;
    }

    protected IIOMetadataNode getStandardDimensionNode() {
        DataSet dataset = obj.ds;
        String string = dataset.findString(2621456);
        if (string == null)
            return null;
        IIOMetadataNode iiometadatanode = new IIOMetadataNode("Dimension");
        Float[] var_floats = ((Float[]) dataset.findValue(2621488,
                Float[].class));
        if (var_floats == null) {
            var_floats = ((Float[]) dataset.findValue(1577316, Float[].class));
            if (var_floats == null) {
                var_floats = ((Float[]) dataset.findValue(1581072,
                        Float[].class));
                if (var_floats == null)
                    dataset.findValue(805437457, Float[].class);
            }
        }
        Integer[] integers = ((Integer[]) dataset.findValue(2621492,
                Integer[].class));
        if (integers != null)
            addNode(iiometadatanode, "PixelAspectRatio", "value", Double
                    .toString(integers[1].doubleValue()
                            / (double) integers[0].intValue()));
        else if (var_floats != null)
            addNode(iiometadatanode, "PixelAspectRatio", "value", Double
                    .toString(var_floats[1].doubleValue()
                            / (double) var_floats[0].floatValue()));
        if (var_floats != null) {
            addNode(iiometadatanode, "HorizontalPhysicalPixelSpacing", "value",
                    var_floats[1].toString());
            addNode(iiometadatanode, "VerticalPhysicalPixelSpacing", "value",
                    var_floats[0].toString());
        }
        return iiometadatanode;
    }

    protected IIOMetadataNode getStandardDocumentNode() {
        DataSet dataset = obj.ds;
        IIOMetadataNode iiometadatanode = new IIOMetadataNode("Document");
        Integer integer = ((Integer) dataset.findValue(2621449, Integer.class));
        if (integer != null) {
            switch (integer.intValue()) {
            case 1577059:
            case 1577061:
            case 5505136:
            case 5505280:
                addNode(iiometadatanode, "SubimageInterpretation", "value",
                        "AnimationFrame");
                break;
            case 5505152:
                addNode(iiometadatanode, "SubimageInterpretation", "value",
                        "VolumeSlice");
                break;
            case 5505056:
            case 5505104:
            case 5505168:
                addNode(iiometadatanode, "SubimageInterpretation", "value",
                        "ObjectView");
                break;
            case 5505040:
                addNode(iiometadatanode, "SubimageInterpretation", "value",
                        "SpectralSlice");
                break;
            }
        }
        Date date = (Date) dataset.findValue(524323, Date.class);
        Date date_37_ = null;
        if (date != null)
            date_37_ = (Date) dataset.findValue(524339, Date.class);
        else {
            date = (Date) dataset.findValue(524322, Date.class);
            if (date != null)
                date_37_ = (Date) dataset.findValue(524338, Date.class);
            else {
                date = (Date) dataset.findValue(524321, Date.class);
                if (date != null)
                    date_37_ = ((Date) dataset.findValue(524337, Date.class));
                else {
                    date = ((Date) dataset.findValue(524320, Date.class));
                    if (date != null)
                        date_37_ = (Date) (dataset
                                .findValue(524336, Date.class));
                }
            }
        }
        if (date != null) {
            IIOMetadataNode iiometadatanode_38_ = new IIOMetadataNode(
                    "ImageCreationTime");
            dateToStandard(iiometadatanode_38_, date, date_37_);
            iiometadatanode.appendChild(iiometadatanode_38_);
        }
        date = (Date) dataset.findValue(1576978, Date.class);
        if (date != null) {
            date_37_ = (Date) dataset.findValue(1576980, Date.class);
            IIOMetadataNode iiometadatanode_39_ = new IIOMetadataNode(
                    "ImageModificationTime");
            dateToStandard(iiometadatanode_39_, date, date_37_);
            iiometadatanode.appendChild(iiometadatanode_39_);
        }
        return iiometadatanode;
    }

    private void dateToStandard(IIOMetadataNode iiometadatanode, Date date,
            Date date_40_) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        iiometadatanode.setAttribute("year", Integer.toString(calendar.get(1)));
        iiometadatanode.setAttribute("month", Integer
                .toString(calendar.get(2) + 1));
        iiometadatanode.setAttribute("day", Integer.toString(calendar.get(5)));
        if (date_40_ != null) {
            calendar.setTime(date_40_);
            iiometadatanode.setAttribute("hour", Integer.toString(calendar
                    .get(11)));
            iiometadatanode.setAttribute("minute", Integer.toString(calendar
                    .get(12)));
            iiometadatanode.setAttribute("second", Integer.toString(calendar
                    .get(13)));
        }
    }

    private Date standardToDate(Node node) throws IIOInvalidTreeException {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.set(1, Integer.parseInt(getAttr(node, "year")));
            calendar.set(2, Integer.parseInt(getAttr(node, "month")) - 1);
            calendar.set(5, Integer.parseInt(getAttr(node, "month")) - 1);
        } catch (Exception exception) {
            throw new IIOInvalidTreeException("Bad format", node);
        }
        try {
            calendar.set(10, Integer.parseInt(getAttr(node, "hour")));
        } catch (Exception exception) {
            calendar.set(10, 0);
        }
        try {
            calendar.set(12, Integer.parseInt(getAttr(node, "minute")));
        } catch (Exception exception) {
            calendar.set(12, 0);
        }
        try {
            calendar.set(13, Integer.parseInt(getAttr(node, "second")));
        } catch (Exception exception) {
            calendar.set(13, 0);
        }
        return calendar.getTime();
    }

    void updateFromImage(int i, int i_41_, SampleModel samplemodel,
            ColorModel colormodel) throws DicomException {
        DataSet dataset = obj.ds;
        dataset.add(2621456, i_41_);
        dataset.add(2621457, i);
        int i_42_ = samplemodel.getSampleSize(0);
        if (colormodel != null && colormodel.getComponentSize(0) < i_42_)
            i_42_ = colormodel.getComponentSize(0);
        switch (samplemodel.getDataType()) {
        case 0: {
            dataset.add(2621696, 8);
            dataset.add(2621697, i_42_);
            dataset.add(2621698, i_42_ - 1);
            dataset.add(2621699, 0);
            boolean bool = false;
            int i_43_ = (1 << i_42_) - 1;
            break;
        }
        case 1: {
            dataset.add(2621696, 16);
            dataset.add(2621697, i_42_);
            dataset.add(2621698, i_42_ - 1);
            dataset.add(2621699, 0);
            boolean bool = false;
            int i_44_ = (1 << i_42_) - 1;
            break;
        }
        case 2: {
            dataset.add(2621696, 16);
            dataset.add(2621697, i_42_);
            dataset.add(2621698, i_42_ - 1);
            dataset.add(2621699, 1);
            int i_45_ = (1 << i_42_ - 1) - 1;
            int i_46_ = -i_45_ - 1;
            break;
        }
        default:
            throw new DicomException("Unsupported", "DataType");
        }
        String string = dataset.findString(2621444);
        switch (samplemodel.getNumBands()) {
        case 1:
            dataset.add(2621442, 1);
            if (string == null
                    || (!string.equals("MONOCHROME1")
                            && !string.equals("MONOCHROME2") && !string
                            .equals("PALETTE COLOR")) || colormodel != null) {
                String string_47_;
                if (colormodel == null)
                    string_47_ = "MONOCHROME2";
                else if (colormodel instanceof IndexColorModel) {
                    obj.updateFromICM((IndexColorModel) colormodel);
                    string_47_ = "PALETTE COLOR";
                } else {
                    ColorSpace colorspace = colormodel.getColorSpace();
                    float[] fs = colorspace.toRGB(new float[] { 0.0F });
                    float f = fs[0] + fs[1] + fs[2];
                    fs = colorspace.toRGB(new float[] { 1.0F });
                    float f_48_ = fs[0] + fs[1] + fs[2];
                    string_47_ = f <= f_48_ ? "MONOCHROME2" : "MONOCHROME1";
                    if (colormodel instanceof VOIWindowColorModel) {
                        dataset.add(2625616, ((VOIWindowColorModel) colormodel)
                                .getCenter());
                        dataset.add(2625617, ((VOIWindowColorModel) colormodel)
                                .getWidth());
                    } else if (colormodel instanceof VOILUTColorModel) {
                        DataSet dataset_49_ = new DataSet();
                        Object object = ((VOILUTColorModel) colormodel)
                                .getLUT();
                        int i_50_ = ((VOILUTColorModel) colormodel).getFirst();
                        dataset_49_.add(2633730, new int[] {
                                Array.getLength(object), i_50_,
                                (object instanceof short[] ? 16 : 8) });
                        dataset_49_.add(2633734, object);
                        dataset.add(2633744, dataset_49_);
                    }
                }
                dataset.add(2621444, string_47_);
            }
            break;
        case 3:
            dataset.add(2621442, 3);
            dataset.add(2621446, 1);
            if (string == null
                    || colormodel != null
                    || (!string.equals("YBR_FULL")
                            && !string.equals("YBR_FULL_422")
                            && !string.equals("YBR_PARTIAL_422")
                            && !string.equals("YBR_PARTIAL_420")
                            && !string.equals("YBR_ICT")
                            && !string.equals("YBR_RCT")
                            && !string.equals("RGB") && !string.equals("HSV")))
                dataset.add(2621444, (colormodel == null ? "RGB"
                        : DicomColorSpace.getDicomType(colormodel
                                .getColorSpace())));
            break;
        case 4:
            dataset.add(2621442, 4);
            dataset.add(2621446, 1);
            if (string == null || colormodel != null || !string.equals("CMYK")
                    && !string.equals("ARGB"))
                dataset.add(2621444, (colormodel == null ? "CMYK"
                        : DicomColorSpace.getDicomType(colormodel
                                .getColorSpace())));
            break;
        }
    }

    void normalize(DicomWarningListener dicomwarninglistener)
            throws DicomException {
        obj.normalize(dicomwarninglistener);
    }

    public void setAttribute(int i, Object object) {
        obj.ds.add(i, object);
    }

    public void setAttribute(int i, int i_51_) {
        obj.ds.add(i, i_51_);
    }

    public void setAttribute(int i, long l) {
        obj.ds.add(i, l);
    }

    public void setModality(String string) throws DicomException {
        if (!Modality.mod2sop.containsKey(string) && !"OT".equals(string))
            throw new DicomException("Unsupported", "Unknown modality "
                    + string);
        obj.ds.add(524384, string);
    }

    public Object getAttribute(int i) {
        return obj.ds.findValue(i, Object.class);
    }

    public String getAttributeString(int i) {
        return obj.ds.findString(i);
    }

    public int getAttributeInt(int i) {
        return obj.ds.findInt(i);
    }

    public long getAttributeLong(int i) {
        return obj.ds.findLong(i);
    }

    public DataSet getAttributes() {
        return obj.ds;
    }

    public boolean hasAttribute(int i) {
        return obj.ds.has(i);
    }

    public void removeAttribute(int i) {
        obj.ds.remove(i);
    }

    public void setSOPClass(String string) {
        obj.sopClass = string;
    }

    public String getSOPClass() {
        return obj.sopClass;
    }

    public void setSOPInstance(String string) {
        obj.sopInstance = string;
    }

    public String getSOPInstance() {
        return obj.sopInstance;
    }

    public BufferedImage applyGrayscaleTransformations(
            BufferedImage bufferedimage, int i) {
        DicomObject.ApplyLUTTarget applyluttarget = new DicomObject.ApplyLUTTarget(
                bufferedimage);
        obj.applyLUT(applyluttarget, i, null);
        return applyluttarget.bi;
    }

    public void removeUnwritableElements() {
        obj.ds.removeUnwritableElements();
    }

}
