/* ShortToUShortRenderedImage - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.Rectangle;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.util.Vector;

final class ShortToUShortRenderedImage implements RenderedImage
{
    private final RenderedImage source;
    
    ShortToUShortRenderedImage(RenderedImage renderedimage) {
	source = renderedimage;
    }
    
    public Vector getSources() {
	Vector vector = new Vector(1);
	vector.add(source);
	return vector;
    }
    
    public Object getProperty(String string) {
	return source.getProperty(string);
    }
    
    public String[] getPropertyNames() {
	return source.getPropertyNames();
    }
    
    static ColorModel getCompatibleColorModel(ColorModel colormodel) {
	return new ComponentColorModel(colormodel.getColorSpace(), false,
				       false, 1, 1);
    }
    
    public ColorModel getColorModel() {
	return getCompatibleColorModel(source.getColorModel());
    }
    
    public SampleModel getSampleModel() {
	return Util.convertSampleModel(source.getSampleModel(), 1);
    }
    
    public int getWidth() {
	return source.getWidth();
    }
    
    public int getHeight() {
	return source.getHeight();
    }
    
    public int getMinX() {
	return source.getMinX();
    }
    
    public int getMinY() {
	return source.getMinY();
    }
    
    public int getNumXTiles() {
	return source.getNumXTiles();
    }
    
    public int getNumYTiles() {
	return source.getNumYTiles();
    }
    
    public int getMinTileX() {
	return source.getMinTileX();
    }
    
    public int getMinTileY() {
	return source.getMinTileY();
    }
    
    public int getTileWidth() {
	return source.getTileWidth();
    }
    
    public int getTileHeight() {
	return source.getTileHeight();
    }
    
    public int getTileGridXOffset() {
	return source.getTileGridXOffset();
    }
    
    public int getTileGridYOffset() {
	return source.getTileGridYOffset();
    }
    
    public Raster getTile(int i, int i_0_) {
	return Util.convertRaster(source.getTile(i, i_0_), 1);
    }
    
    public Raster getData() {
	return Util.convertRaster(source.getData(), 1);
    }
    
    public Raster getData(Rectangle rectangle) {
	return Util.convertRaster(source.getData(rectangle), 1);
    }
    
    public WritableRaster copyData(WritableRaster writableraster) {
	WritableRaster writableraster_1_
	    = (writableraster == null ? null
	       : (WritableRaster) Util.convertRaster(writableraster, 2));
	writableraster_1_ = source.copyData(writableraster_1_);
	return (WritableRaster) Util.convertRaster(writableraster_1_, 1);
    }
}
