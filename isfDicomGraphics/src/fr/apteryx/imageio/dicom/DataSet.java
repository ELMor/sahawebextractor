/* DataSet - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeMap;

import javax.imageio.stream.ImageOutputStream;

public class DataSet extends TreeMap
{
    transient long offset;
    static Comparator comparator = new Comp();
    /*synthetic*/ static Class class$java$lang$Object;
    
    static class Comp implements Serializable, Comparator
    {
	public final int compare(Object object, Object object_0_) {
	    return ((((long) ((Integer) object).intValue() & 0xffffffffL)
		     < ((long) ((Integer) object_0_).intValue() & 0xffffffffL))
		    ? -1 : object.equals(object_0_) ? 0 : 1);
	}
    }
    
    final void add(DataElement dataelement) {
	put(new Integer(dataelement.tag), dataelement);
    }
    
    public DataSet() {
	super(comparator);
    }
    
    final void write
	(ImageOutputStream imageoutputstream, TransferSyntax transfersyntax,
	 boolean bool, DicomWriter dicomwriter)
	throws DicomException, IOException {
	new DataSetWriter
	    (imageoutputstream, transfersyntax, this, bool, dicomwriter)
	    .write();
    }
    
    final int getLength() {
	CounterImageOutputStream counterimageoutputstream
	    = new CounterImageOutputStream();
	try {
	    new DataSetWriter
		(counterimageoutputstream,
		 TransferSyntax.IMPLICIT_LITTLEENDIAN, this, true, null)
		.write();
	} catch (IOException ioexception) {
	    throw new Error(ioexception);
	}
	return counterimageoutputstream.getCounter();
    }
    
    public final void add(int i, Object object) {
	add(new DataElement(i, object));
    }
    
    public final void add(int i, int i_1_) {
	add(new DataElement(i, new Integer(i_1_)));
    }
    
    public final void add(int i, long l) {
	add(new DataElement(i, new Long(l)));
    }
    
    public final void add(int i, float f) {
	add(new DataElement(i, new Float(f)));
    }
    
    public final void add(int i, double d) {
	add(new DataElement(i, new Double(d)));
    }
    
    public final void remove(int i) {
	this.remove(new Integer(i));
    }
    
    final String toString(String string) {
	StringBuffer stringbuffer = new StringBuffer();
	Iterator iterator = values().iterator();
	while (iterator.hasNext()) {
	    stringbuffer.append(string);
	    stringbuffer
		.append(((DataElement) iterator.next()).toString(string));
	    stringbuffer.append('\n');
	}
	return stringbuffer.toString();
    }
    
    public String toString() {
	return toString("");
    }
    
    final DataElement find(int i) {
	return (DataElement) get(new Integer(i));
    }
    
    public final boolean has(int i) {
	return containsKey(new Integer(i));
    }
    
    public final Object findValue(int i) {
	return findValue(i, java.lang.Object.class);
    }
    
    final Object findValue(int i, Class var_class) {
	DataElement dataelement = find(i);
	return (dataelement == null || !var_class.isInstance(dataelement.value)
		? null : dataelement.value);
    }
    
    public String findString(int i) {
	DataElement dataelement = find(i);
	return (dataelement == null || dataelement.value == null ? null
		: dataelement.value.toString());
    }
    
    public int findInt(int i) {
	DataElement dataelement = find(i);
	if (dataelement != null && dataelement.value instanceof Number)
	    return ((Number) dataelement.value).intValue();
	return 0;
    }
    
    public float findFloat(int i) {
	DataElement dataelement = find(i);
	if (dataelement != null && dataelement.value instanceof Number)
	    return ((Number) dataelement.value).floatValue();
	return 0.0F;
    }
    
    public double findDouble(int i) {
	DataElement dataelement = find(i);
	if (dataelement != null && dataelement.value instanceof Number)
	    return ((Number) dataelement.value).doubleValue();
	return 0.0;
    }
    
    public DataSet findDataSet(int i) {
	DataElement dataelement = find(i);
	if (dataelement == null)
	    return null;
	Object object = dataelement.value;
	if (object instanceof Collection && ((Collection) object).size() == 1)
	    object = ((Collection) object).iterator().next();
	if (object instanceof DataSet)
	    return (DataSet) object;
	return null;
    }
    
    public Collection findSequence(int i) {
	DataElement dataelement = find(i);
	if (dataelement != null) {
	    Object object = dataelement.value;
	    if (object instanceof Collection)
		return (Collection) object;
	    if (object instanceof DataSet) {
		ArrayList arraylist = new ArrayList(1);
		arraylist.add(object);
		return arraylist;
	    }
	}
	return null;
    }
    
    public long findLong(int i) {
	DataElement dataelement = find(i);
	if (dataelement != null && dataelement.value instanceof Number)
	    return ((Number) dataelement.value).longValue();
	return 0L;
    }
    
    public void removeUnwritableElements() {
	Iterator iterator = values().iterator();
	while (iterator.hasNext()) {
	    DataElement dataelement = (DataElement) iterator.next();
	    String string = dataelement.getVR();
	    if (string == null)
		iterator.remove();
	    else if (string.equals("SQ")) {
		if (dataelement.value instanceof DataSet)
		    ((DataSet) dataelement.value).removeUnwritableElements();
		else if (dataelement.value instanceof Collection) {
		    Iterator iterator_2_
			= ((Collection) dataelement.value).iterator();
		    while (iterator_2_.hasNext()) {
			Object object = iterator_2_.next();
			if (object instanceof DataSet)
			    ((DataSet) object).removeUnwritableElements();
		    }
		}
	    }
	}
    }
    
}
