/* DicomWarningListener - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

interface DicomWarningListener
{
    public void warning(String string, String string_0_);
    
    public void warning(int i, String string, String string_1_);
}
