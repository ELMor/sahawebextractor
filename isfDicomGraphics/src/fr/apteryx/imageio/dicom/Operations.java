/* Operations - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.Point;
import java.awt.image.ComponentSampleModel;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.PixelInterleavedSampleModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

abstract class Operations
{
    static void rescale(Raster raster, WritableRaster writableraster, float f,
			float f_0_) {
	ComponentSampleModel componentsamplemodel
	    = (ComponentSampleModel) raster.getSampleModel();
	ComponentSampleModel componentsamplemodel_1_
	    = (ComponentSampleModel) writableraster.getSampleModel();
	int i = componentsamplemodel.getBandOffsets()[0];
	int i_2_ = componentsamplemodel_1_.getBandOffsets()[0];
	int i_3_ = componentsamplemodel.getBankIndices()[0];
	int i_4_ = componentsamplemodel_1_.getBankIndices()[0];
	int i_5_ = componentsamplemodel.getPixelStride();
	int i_6_ = componentsamplemodel_1_.getPixelStride();
	int i_7_ = componentsamplemodel.getScanlineStride();
	int i_8_ = componentsamplemodel_1_.getScanlineStride();
	int i_9_ = componentsamplemodel.getDataType();
	int i_10_ = componentsamplemodel_1_.getDataType();
	int i_11_ = componentsamplemodel.getWidth();
	int i_12_ = componentsamplemodel.getHeight();
	switch (componentsamplemodel.getDataType()) {
	case 0: {
	    DataBufferByte databufferbyte
		= (DataBufferByte) raster.getDataBuffer();
	    byte[] is = databufferbyte.getData(i_3_);
	    switch (componentsamplemodel_1_.getDataType()) {
	    case 0: {
		DataBufferByte databufferbyte_13_
		    = (DataBufferByte) writableraster.getDataBuffer();
		byte[] is_14_ = databufferbyte_13_.getData(i_4_);
		for (int i_15_ = 0; i_15_ < i_12_; i_15_++) {
		    int i_16_ = i;
		    int i_17_ = i_2_;
		    for (int i_18_ = 0; i_18_ < i_11_; i_18_++) {
			int i_19_
			    = (int) ((float) (is[i_16_] & 0xff) * f + f_0_);
			is_14_[i_17_] = (byte) (i_19_ < 0 ? 0 : i_19_ > 255
						? 255 : i_19_);
			i_16_ += i_5_;
			i_17_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    case 2: {
		DataBufferShort databuffershort
		    = (DataBufferShort) writableraster.getDataBuffer();
		short[] is_20_ = databuffershort.getData(i_4_);
		for (int i_21_ = 0; i_21_ < i_12_; i_21_++) {
		    int i_22_ = i;
		    int i_23_ = i_2_;
		    for (int i_24_ = 0; i_24_ < i_11_; i_24_++) {
			int i_25_
			    = (int) ((float) (is[i_22_] & 0xff) * f + f_0_);
			is_20_[i_23_]
			    = (short) (i_25_ < -32768 ? -32768 : i_25_ > 32767
				       ? 32767 : i_25_);
			i_22_ += i_5_;
			i_23_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    case 1: {
		DataBufferUShort databufferushort
		    = (DataBufferUShort) writableraster.getDataBuffer();
		short[] is_26_ = databufferushort.getData(i_4_);
		for (int i_27_ = 0; i_27_ < i_12_; i_27_++) {
		    int i_28_ = i;
		    int i_29_ = i_2_;
		    for (int i_30_ = 0; i_30_ < i_11_; i_30_++) {
			int i_31_
			    = (int) ((float) (is[i_28_] & 0xff) * f + f_0_);
			is_26_[i_29_] = (short) (i_31_ < 0 ? 0 : i_31_ > 65535
						 ? 65535 : i_31_);
			i_28_ += i_5_;
			i_29_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    case 3: {
		DataBufferInt databufferint
		    = (DataBufferInt) writableraster.getDataBuffer();
		int[] is_32_ = databufferint.getData(i_4_);
		for (int i_33_ = 0; i_33_ < i_12_; i_33_++) {
		    int i_34_ = i;
		    int i_35_ = i_2_;
		    for (int i_36_ = 0; i_36_ < i_11_; i_36_++) {
			is_32_[i_35_]
			    = (int) ((float) (is[i_34_] & 0xff) * f + f_0_);
			i_34_ += i_5_;
			i_35_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    }
	    break;
	}
	case 2: {
	    DataBufferShort databuffershort
		= (DataBufferShort) raster.getDataBuffer();
	    short[] is = databuffershort.getData(i_3_);
	    switch (componentsamplemodel_1_.getDataType()) {
	    case 0: {
		DataBufferByte databufferbyte
		    = (DataBufferByte) writableraster.getDataBuffer();
		byte[] is_37_ = databufferbyte.getData(i_4_);
		for (int i_38_ = 0; i_38_ < i_12_; i_38_++) {
		    int i_39_ = i;
		    int i_40_ = i_2_;
		    for (int i_41_ = 0; i_41_ < i_11_; i_41_++) {
			int i_42_ = (int) ((float) is[i_39_] * f + f_0_);
			is_37_[i_40_] = (byte) (i_42_ < 0 ? 0 : i_42_ > 255
						? 255 : i_42_);
			i_39_ += i_5_;
			i_40_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    case 2: {
		DataBufferShort databuffershort_43_
		    = (DataBufferShort) writableraster.getDataBuffer();
		short[] is_44_ = databuffershort_43_.getData(i_4_);
		for (int i_45_ = 0; i_45_ < i_12_; i_45_++) {
		    int i_46_ = i;
		    int i_47_ = i_2_;
		    for (int i_48_ = 0; i_48_ < i_11_; i_48_++) {
			int i_49_ = (int) ((float) is[i_46_] * f + f_0_);
			is_44_[i_47_]
			    = (short) (i_49_ < -32768 ? -32768 : i_49_ > 32767
				       ? 32767 : i_49_);
			i_46_ += i_5_;
			i_47_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    case 1: {
		DataBufferUShort databufferushort
		    = (DataBufferUShort) writableraster.getDataBuffer();
		short[] is_50_ = databufferushort.getData(i_4_);
		for (int i_51_ = 0; i_51_ < i_12_; i_51_++) {
		    int i_52_ = i;
		    int i_53_ = i_2_;
		    for (int i_54_ = 0; i_54_ < i_11_; i_54_++) {
			int i_55_ = (int) ((float) is[i_52_] * f + f_0_);
			is_50_[i_53_] = (short) (i_55_ < 0 ? 0 : i_55_ > 65535
						 ? 65535 : i_55_);
			i_52_ += i_5_;
			i_53_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    case 3: {
		DataBufferInt databufferint
		    = (DataBufferInt) writableraster.getDataBuffer();
		int[] is_56_ = databufferint.getData(i_4_);
		for (int i_57_ = 0; i_57_ < i_12_; i_57_++) {
		    int i_58_ = i;
		    int i_59_ = i_2_;
		    for (int i_60_ = 0; i_60_ < i_11_; i_60_++) {
			is_56_[i_59_] = (int) ((float) is[i_58_] * f + f_0_);
			i_58_ += i_5_;
			i_59_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    }
	    break;
	}
	case 1: {
	    DataBufferUShort databufferushort
		= (DataBufferUShort) raster.getDataBuffer();
	    short[] is = databufferushort.getData(i_3_);
	    switch (componentsamplemodel_1_.getDataType()) {
	    case 0: {
		DataBufferByte databufferbyte
		    = (DataBufferByte) writableraster.getDataBuffer();
		byte[] is_61_ = databufferbyte.getData(i_4_);
		for (int i_62_ = 0; i_62_ < i_12_; i_62_++) {
		    int i_63_ = i;
		    int i_64_ = i_2_;
		    for (int i_65_ = 0; i_65_ < i_11_; i_65_++) {
			int i_66_
			    = (int) ((float) (is[i_63_] & 0xffff) * f + f_0_);
			is_61_[i_64_] = (byte) (i_66_ < 0 ? 0 : i_66_ > 255
						? 255 : i_66_);
			i_63_ += i_5_;
			i_64_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    case 2: {
		DataBufferShort databuffershort
		    = (DataBufferShort) writableraster.getDataBuffer();
		short[] is_67_ = databuffershort.getData(i_4_);
		for (int i_68_ = 0; i_68_ < i_12_; i_68_++) {
		    int i_69_ = i;
		    int i_70_ = i_2_;
		    for (int i_71_ = 0; i_71_ < i_11_; i_71_++) {
			int i_72_
			    = (int) ((float) (is[i_69_] & 0xffff) * f + f_0_);
			is_67_[i_70_]
			    = (short) (i_72_ < -32768 ? -32768 : i_72_ > 32767
				       ? 32767 : i_72_);
			i_69_ += i_5_;
			i_70_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    case 1: {
		DataBufferUShort databufferushort_73_
		    = (DataBufferUShort) writableraster.getDataBuffer();
		short[] is_74_ = databufferushort_73_.getData(i_4_);
		for (int i_75_ = 0; i_75_ < i_12_; i_75_++) {
		    int i_76_ = i;
		    int i_77_ = i_2_;
		    for (int i_78_ = 0; i_78_ < i_11_; i_78_++) {
			int i_79_
			    = (int) ((float) (is[i_76_] & 0xffff) * f + f_0_);
			is_74_[i_77_] = (short) (i_79_ < 0 ? 0 : i_79_ > 65535
						 ? 65535 : i_79_);
			i_76_ += i_5_;
			i_77_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    case 3: {
		DataBufferInt databufferint
		    = (DataBufferInt) writableraster.getDataBuffer();
		int[] is_80_ = databufferint.getData(i_4_);
		for (int i_81_ = 0; i_81_ < i_12_; i_81_++) {
		    int i_82_ = i;
		    int i_83_ = i_2_;
		    for (int i_84_ = 0; i_84_ < i_11_; i_84_++) {
			is_80_[i_83_]
			    = (int) ((float) (is[i_82_] & 0xffff) * f + f_0_);
			i_82_ += i_5_;
			i_83_ += i_6_;
		    }
		    i += i_7_;
		    i_2_ += i_8_;
		}
		break;
	    }
	    default:
		break;
	    }
	    break;
	}
	}
    }
    
    static int[] minmax(Raster raster) {
	ComponentSampleModel componentsamplemodel
	    = (ComponentSampleModel) raster.getSampleModel();
	int i = componentsamplemodel.getBandOffsets()[0];
	int i_85_ = componentsamplemodel.getBankIndices()[0];
	int i_86_ = componentsamplemodel.getPixelStride();
	int i_87_ = componentsamplemodel.getScanlineStride();
	int i_88_ = componentsamplemodel.getDataType();
	int i_89_ = componentsamplemodel.getWidth();
	int i_90_ = componentsamplemodel.getHeight();
	int i_91_ = 2147483647;
	int i_92_ = -2147483648;
	switch (componentsamplemodel.getDataType()) {
	case 0: {
	    DataBufferByte databufferbyte
		= (DataBufferByte) raster.getDataBuffer();
	    byte[] is = databufferbyte.getData(i_85_);
	    for (int i_93_ = 0; i_93_ < i_90_; i_93_++) {
		int i_94_ = i;
		for (int i_95_ = 0; i_95_ < i_89_; i_95_++) {
		    int i_96_ = is[i_94_] & 0xff;
		    if (i_96_ < i_91_)
			i_91_ = i_96_;
		    if (i_96_ > i_92_)
			i_92_ = i_96_;
		    i_94_ += i_86_;
		}
		i += i_87_;
	    }
	    break;
	}
	case 2: {
	    DataBufferShort databuffershort
		= (DataBufferShort) raster.getDataBuffer();
	    short[] is = databuffershort.getData(i_85_);
	    for (int i_97_ = 0; i_97_ < i_90_; i_97_++) {
		int i_98_ = i;
		for (int i_99_ = 0; i_99_ < i_89_; i_99_++) {
		    int i_100_ = is[i_98_];
		    if (i_100_ < i_91_)
			i_91_ = i_100_;
		    if (i_100_ > i_92_)
			i_92_ = i_100_;
		    i_98_ += i_86_;
		}
		i += i_87_;
	    }
	    break;
	}
	case 1: {
	    DataBufferUShort databufferushort
		= (DataBufferUShort) raster.getDataBuffer();
	    short[] is = databufferushort.getData(i_85_);
	    for (int i_101_ = 0; i_101_ < i_90_; i_101_++) {
		int i_102_ = i;
		for (int i_103_ = 0; i_103_ < i_89_; i_103_++) {
		    int i_104_ = is[i_102_] & 0xffff;
		    if (i_104_ < i_91_)
			i_91_ = i_104_;
		    if (i_104_ > i_92_)
			i_92_ = i_104_;
		    i_102_ += i_86_;
		}
		i += i_87_;
	    }
	    break;
	}
	case 3: {
	    DataBufferInt databufferint
		= (DataBufferInt) raster.getDataBuffer();
	    int[] is = databufferint.getData(i_85_);
	    for (int i_105_ = 0; i_105_ < i_90_; i_105_++) {
		int i_106_ = i;
		for (int i_107_ = 0; i_107_ < i_89_; i_107_++) {
		    int i_108_ = is[i_106_];
		    if (i_108_ < i_91_)
			i_91_ = i_108_;
		    if (i_108_ > i_92_)
			i_92_ = i_108_;
		    i_106_ += i_86_;
		}
		i += i_87_;
	    }
	    break;
	}
	}
	return new int[] { i_91_, i_92_ };
    }
    
    static WritableRaster lookup(Raster raster, int i, Object object) {
	ComponentSampleModel componentsamplemodel
	    = (ComponentSampleModel) raster.getSampleModel();
	int i_109_ = componentsamplemodel.getBandOffsets()[0];
	int i_110_ = 0;
	int i_111_ = componentsamplemodel.getBankIndices()[0];
	int i_112_ = componentsamplemodel.getPixelStride();
	int i_113_ = componentsamplemodel.getScanlineStride();
	int i_114_ = componentsamplemodel.getDataType();
	int i_115_ = componentsamplemodel.getWidth();
	int i_116_ = componentsamplemodel.getHeight();
	if (object instanceof byte[]) {
	    byte[] is = (byte[]) object;
	    int i_117_ = is.length - 1;
	    PixelInterleavedSampleModel pixelinterleavedsamplemodel
		= new PixelInterleavedSampleModel(0, i_115_, i_116_, 1, i_115_,
						  new int[] { 0 });
	    DataBufferByte databufferbyte
		= ((DataBufferByte)
		   pixelinterleavedsamplemodel.createDataBuffer());
	    byte[] is_118_ = databufferbyte.getData(0);
	    WritableRaster writableraster
		= Raster.createWritableRaster(pixelinterleavedsamplemodel,
					      databufferbyte,
					      new Point(raster.getMinX(),
							raster.getMinY()));
	    switch (i_114_) {
	    case 0: {
		DataBufferByte databufferbyte_119_
		    = (DataBufferByte) raster.getDataBuffer();
		byte[] is_120_ = databufferbyte_119_.getData(i_111_);
		for (int i_121_ = 0; i_121_ < i_116_; i_121_++) {
		    int i_122_ = i_109_;
		    int i_123_ = i_110_;
		    for (int i_124_ = 0; i_124_ < i_115_; i_124_++) {
			int i_125_ = (is_120_[i_122_] & 0xff) - i;
			if (i_125_ < 0)
			    i_125_ = 0;
			if (i_125_ > i_117_)
			    i_125_ = i_117_;
			is_118_[i_123_] = is[i_125_];
			i_122_ += i_112_;
			i_123_++;
		    }
		    i_109_ += i_113_;
		    i_110_ += i_115_;
		}
		break;
	    }
	    case 2: {
		DataBufferShort databuffershort
		    = (DataBufferShort) raster.getDataBuffer();
		short[] is_126_ = databuffershort.getData(i_111_);
		for (int i_127_ = 0; i_127_ < i_116_; i_127_++) {
		    int i_128_ = i_109_;
		    int i_129_ = i_110_;
		    for (int i_130_ = 0; i_130_ < i_115_; i_130_++) {
			int i_131_ = is_126_[i_128_] - i;
			if (i_131_ < 0)
			    i_131_ = 0;
			if (i_131_ > i_117_)
			    i_131_ = i_117_;
			is_118_[i_129_] = is[i_131_];
			i_128_ += i_112_;
			i_129_++;
		    }
		    i_109_ += i_113_;
		    i_110_ += i_115_;
		}
		break;
	    }
	    case 1: {
		DataBufferUShort databufferushort
		    = (DataBufferUShort) raster.getDataBuffer();
		short[] is_132_ = databufferushort.getData(i_111_);
		for (int i_133_ = 0; i_133_ < i_116_; i_133_++) {
		    int i_134_ = i_109_;
		    int i_135_ = i_110_;
		    for (int i_136_ = 0; i_136_ < i_115_; i_136_++) {
			int i_137_ = (is_132_[i_134_] & 0xffff) - i;
			if (i_137_ < 0)
			    i_137_ = 0;
			if (i_137_ > i_117_)
			    i_137_ = i_117_;
			is_118_[i_135_] = is[i_137_];
			i_134_ += i_112_;
			i_135_++;
		    }
		    i_109_ += i_113_;
		    i_110_ += i_115_;
		}
		break;
	    }
	    }
	    return writableraster;
	}
	short[] is = Util.toShortArray(object);
	if (is != null) {
	    int i_138_ = is.length - 1;
	    PixelInterleavedSampleModel pixelinterleavedsamplemodel
		= new PixelInterleavedSampleModel(1, i_115_, i_116_, 1, i_115_,
						  new int[] { 0 });
	    DataBufferUShort databufferushort
		= ((DataBufferUShort)
		   pixelinterleavedsamplemodel.createDataBuffer());
	    short[] is_139_ = databufferushort.getData(0);
	    WritableRaster writableraster
		= Raster.createWritableRaster(pixelinterleavedsamplemodel,
					      databufferushort,
					      new Point(raster.getMinX(),
							raster.getMinY()));
	    switch (i_114_) {
	    case 0: {
		DataBufferByte databufferbyte
		    = (DataBufferByte) raster.getDataBuffer();
		byte[] is_140_ = databufferbyte.getData(i_111_);
		for (int i_141_ = 0; i_141_ < i_116_; i_141_++) {
		    int i_142_ = i_109_;
		    int i_143_ = i_110_;
		    for (int i_144_ = 0; i_144_ < i_115_; i_144_++) {
			int i_145_ = (is_140_[i_142_] & 0xff) - i;
			if (i_145_ < 0)
			    i_145_ = 0;
			if (i_145_ > i_138_)
			    i_145_ = i_138_;
			is_139_[i_143_] = is[i_145_];
			i_142_ += i_112_;
			i_143_++;
		    }
		    i_109_ += i_113_;
		    i_110_ += i_115_;
		}
		break;
	    }
	    case 2: {
		DataBufferShort databuffershort
		    = (DataBufferShort) raster.getDataBuffer();
		short[] is_146_ = databuffershort.getData(i_111_);
		for (int i_147_ = 0; i_147_ < i_116_; i_147_++) {
		    int i_148_ = i_109_;
		    int i_149_ = i_110_;
		    for (int i_150_ = 0; i_150_ < i_115_; i_150_++) {
			int i_151_ = is_146_[i_148_] - i;
			if (i_151_ < 0)
			    i_151_ = 0;
			if (i_151_ > i_138_)
			    i_151_ = i_138_;
			is_139_[i_149_] = is[i_151_];
			i_148_ += i_112_;
			i_149_++;
		    }
		    i_109_ += i_113_;
		    i_110_ += i_115_;
		}
		break;
	    }
	    case 1: {
		DataBufferUShort databufferushort_152_
		    = (DataBufferUShort) raster.getDataBuffer();
		short[] is_153_ = databufferushort_152_.getData(i_111_);
		for (int i_154_ = 0; i_154_ < i_116_; i_154_++) {
		    int i_155_ = i_109_;
		    int i_156_ = i_110_;
		    for (int i_157_ = 0; i_157_ < i_115_; i_157_++) {
			int i_158_ = (is_153_[i_155_] & 0xffff) - i;
			if (i_158_ < 0)
			    i_158_ = 0;
			if (i_158_ > i_138_)
			    i_158_ = i_138_;
			is_139_[i_156_] = is[i_158_];
			i_155_ += i_112_;
			i_156_++;
		    }
		    i_109_ += i_113_;
		    i_110_ += i_115_;
		}
		break;
	    }
	    }
	    return writableraster;
	}
	return (WritableRaster) raster;
    }
    
    static void propagateSign(WritableRaster writableraster, int i) {
	ComponentSampleModel componentsamplemodel
	    = (ComponentSampleModel) writableraster.getSampleModel();
	int i_159_ = componentsamplemodel.getBandOffsets()[0];
	int i_160_ = componentsamplemodel.getBankIndices()[0];
	int i_161_ = componentsamplemodel.getPixelStride();
	int i_162_ = componentsamplemodel.getScanlineStride();
	int i_163_ = componentsamplemodel.getDataType();
	int i_164_ = componentsamplemodel.getWidth();
	int i_165_ = componentsamplemodel.getHeight();
	DataBufferShort databuffershort
	    = (DataBufferShort) writableraster.getDataBuffer();
	short[] is = databuffershort.getData(i_160_);
	short i_166_ = (short) (1 << i - 1);
	short i_167_ = (short) (0xffffffff ^ (1 << i) - 1);
	for (int i_168_ = 0; i_168_ < i_165_; i_168_++) {
	    int i_169_ = i_159_;
	    for (int i_170_ = 0; i_170_ < i_164_; i_170_++) {
		if ((is[i_169_] & i_166_) != 0)
		    is[i_169_] |= i_167_;
		i_169_ += i_161_;
	    }
	    i_159_ += i_162_;
	}
    }
}
