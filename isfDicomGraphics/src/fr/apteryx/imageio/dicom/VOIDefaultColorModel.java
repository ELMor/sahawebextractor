/* VOIDefaultColorModel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.color.ColorSpace;

public final class VOIDefaultColorModel extends VOIColorModel
{
    private final float cs_min;
    private final float cs_max;
    private final double slope;
    private final double intercept;
    
    public VOIDefaultColorModel(int i, int i_0_, ColorSpace colorspace) {
	super(i, i_0_, i_0_, colorspace);
	cs_min = colorspace.getMinValue(0);
	cs_max = colorspace.getMaxValue(0);
	slope
	    = (double) (cs_max - cs_min) / (double) (sample_max - sample_min);
	intercept = (double) cs_min - (double) sample_min * slope;
    }
    
    int getComp(int i) {
	switch (transferType) {
	case 2:
	case 3:
	    return i + (unnorm_max + 1) / 2;
	default:
	    return i;
	}
    }
    
    int get255(int i) {
	switch (transferType) {
	case 2:
	case 3:
	    return i + (unnorm_max + 1) / 2 >> pixel_bits - 8;
	case 1:
	    return i >> pixel_bits - 8;
	default:
	    return i;
	}
    }
    
    float getNorm(int i) {
	float f = (float) ((double) i * slope + intercept);
	return f < cs_min ? cs_min : f > cs_max ? cs_max : f;
    }
    
    int getDataElement(float f) {
	double d = ((double) f - intercept) / slope;
	int i = (int) (d > 0.0 ? d + 0.5 : d - 0.5);
	return i > sample_max ? sample_max : i < sample_min ? sample_min : i;
    }
}
