/* Util - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.Point;
import java.awt.image.BandedSampleModel;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.PixelInterleavedSampleModel;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.util.Collection;

import javax.imageio.IIOImage;

abstract class Util
{
    /*synthetic*/ static Class class$java$lang$Object;
    
    static int getNumBands(Object object) {
	return getSampleModel(object).getNumBands();
    }
    
    static SampleModel getSampleModel(Object object) {
	if (object instanceof Collection)
	    return getSampleModel(((Collection) object).iterator().next());
	IIOImage iioimage = (IIOImage) object;
	return (iioimage.hasRaster() ? iioimage.getRaster().getSampleModel()
		: iioimage.getRenderedImage().getSampleModel());
    }
    
    static ColorModel getColorModel(Object object) {
	if (object instanceof Collection)
	    return getColorModel(((Collection) object).iterator().next());
	IIOImage iioimage = (IIOImage) object;
	return (iioimage.hasRaster() ? null
		: iioimage.getRenderedImage().getColorModel());
    }
    
    static short[] toShortArray(Object object) {
	if (object instanceof short[])
	    return (short[]) object;
	if (object instanceof Number[]) {
	    Number[] numbers = (Number[]) object;
	    short[] is = new short[numbers.length];
	    for (int i = 0; i < is.length; i++)
		is[i] = numbers[i].shortValue();
	    return is;
	}
	return null;
    }
    
    static SampleModel convertSampleModel(SampleModel samplemodel, int i) {
	if (samplemodel instanceof PixelInterleavedSampleModel) {
	    PixelInterleavedSampleModel pixelinterleavedsamplemodel
		= (PixelInterleavedSampleModel) samplemodel;
	    samplemodel = (new PixelInterleavedSampleModel
			   (i, pixelinterleavedsamplemodel.getWidth(),
			    pixelinterleavedsamplemodel.getHeight(),
			    pixelinterleavedsamplemodel.getPixelStride(),
			    pixelinterleavedsamplemodel.getScanlineStride(),
			    pixelinterleavedsamplemodel.getBandOffsets()));
	} else if (samplemodel instanceof BandedSampleModel) {
	    BandedSampleModel bandedsamplemodel
		= (BandedSampleModel) samplemodel;
	    samplemodel
		= new BandedSampleModel(i, bandedsamplemodel.getWidth(),
					bandedsamplemodel.getHeight(),
					bandedsamplemodel.getScanlineStride(),
					bandedsamplemodel.getBankIndices(),
					bandedsamplemodel.getBandOffsets());
	}
	return samplemodel;
    }
    
    static Raster convertRaster(Raster raster, int i) {
	SampleModel samplemodel = raster.getSampleModel();
	if (samplemodel.getDataType() == i)
	    return raster;
	SampleModel samplemodel_0_ = convertSampleModel(samplemodel, i);
	DataBuffer databuffer = raster.getDataBuffer();
	DataBufferUShort databufferushort
	    = new DataBufferUShort(getBankData(databuffer),
				   databuffer.getSize(),
				   databuffer.getOffsets());
	return Raster.createWritableRaster(samplemodel_0_, databufferushort,
					   new Point(raster.getMinX(),
						     raster.getMinY()));
    }
    
    private static short[][] getBankData(DataBuffer databuffer) {
	return (databuffer instanceof DataBufferShort
		? ((DataBufferShort) databuffer).getBankData()
		: ((DataBufferUShort) databuffer).getBankData());
    }
    
    static void genericStatus
	(DataSet dataset, DicomWarningListener dicomwarninglistener)
	throws DicomException {
	int i = dataset.findInt(2304);
	String string = dataset.findString(2306);
	if (string == null)
	    string = "No additionnal message";
	String string_1_ = dataset.findString(4096);
	string_1_ = (string_1_ == null ? "SOP instance not specified"
		     : "SOP instance " + string_1_);
	String string_2_ = dataset.findString(2);
	string_1_ = (string_1_ == null ? "SOP class not specified"
		     : "SOP class " + string_2_);
	String string_3_
	    = "Attribute list: " + Tag.tagListToString(dataset.find(4101));
	switch (i) {
	case 65024:
	    throw new DicomException("ServiceError", "Canceled");
	case 290:
	    throw new DicomException("ServiceRefused",
				     ("SOP class not supported (" + string
				      + ')'));
	case 281: {
	    String string_4_ = dataset.findString(3);
	    String string_5_ = dataset.findString(4097);
	    throw new DicomException("ServiceError",
				     ("Class-instance conflict"
				      + (string_4_ != null && string_5_ != null
					 ? (" (" + string_5_
					    + " not an instance of "
					    + string_4_ + ")")
					 : "")));
	}
	case 273:
	    throw new DicomException("ServiceError",
				     ("Duplicate SOP instance (" + string_1_
				      + ")"));
	case 528:
	    throw new DicomException("ServiceError", "Duplicate invocation");
	case 277:
	    throw new DicomException("ServiceError",
				     ("Invalid argument value (" + string_1_
				      + ", " + string_2_ + ")"));
	case 262:
	    throw new DicomException("ServiceError",
				     "Invalid attribute value");
	case 279:
	    throw new DicomException("ServiceError",
				     ("Invalid object instance (" + string_1_
				      + ")"));
	case 288:
	    throw new DicomException("ServiceError",
				     "Missing attribute (" + string_3_ + ")");
	case 289:
	    throw new DicomException("ServiceError",
				     "Missing attribute value");
	case 530:
	    throw new DicomException("ServiceError", "Mistyped argument");
	case 276:
	    throw new DicomException("ServiceError",
				     "No such argument (" + string_2_ + ")");
	case 261:
	    throw new DicomException("ServiceError",
				     "No such attribute (" + string_3_ + ")");
	case 275:
	    throw new DicomException("ServiceError",
				     "No such event type (" + string_2_ + ")");
	case 274:
	    throw new DicomException("ServiceError",
				     ("No such object instance (" + string_1_
				      + ")"));
	case 280:
	    throw new DicomException("ServiceError",
				     "No such SOP class (" + string_2_ + ")");
	case 272: {
	    Object object
		= dataset.findValue(2307, Object.class);
	    throw new DicomException("ServiceError",
				     ("Processing failure (" + string + ", "
				      + string_2_ + ", " + string_1_
				      + (object == null ? ")"
					 : ", error code " + object + ")")));
	}
	case 531:
	    throw new DicomException("ServiceError", "Resource limitation");
	case 529:
	    throw new DicomException("ServiceError", "Unrecognized operation");
	case 291:
	    throw new DicomException("ServiceError", ("No such action type ("
						      + string_2_ + ")"));
	case 263:
	    if (dicomwarninglistener != null)
		dicomwarninglistener.warning("ServiceWarning",
					     ("attribute list error ("
					      + string_3_ + ")"));
	    break;
	case 278:
	    if (dicomwarninglistener != null)
		dicomwarninglistener.warning("ServiceWarning",
					     "attribute value out of range");
	    break;
	default:
	    throw new DicomException("ServiceError",
				     ("Unknown status code "
				      + Integer.toHexString(i)));
	}
    }
    
}
