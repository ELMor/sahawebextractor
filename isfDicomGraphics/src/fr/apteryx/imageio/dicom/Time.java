/* Time - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.text.DateFormat;
import java.util.Date;

public class Time extends Date
{
    public Time() {
	this(new Date());
    }
    
    public Time(Date date) {
	super(date.getTime());
    }
    
    public String toString() {
	return DateFormat.getTimeInstance().format(this);
    }
}
