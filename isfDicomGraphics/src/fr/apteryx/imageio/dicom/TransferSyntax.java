/*
 * TransferSyntax - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.imageio.spi.IIORegistry;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.spi.ImageReaderWriterSpi;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.spi.ServiceRegistry;

final class TransferSyntax implements Serializable {
    static final TransferSyntax RLE_COMPRESSION = new TransferSyntax(
            "1.2.840.10008.1.2.5", false, true, true);

    static final TransferSyntax EXPLICIT_BIGENDIAN = new TransferSyntax(
            "1.2.840.10008.1.2.2", true, false, true);

    static final TransferSyntax EXPLICIT_LITTLEENDIAN = new TransferSyntax(
            "1.2.840.10008.1.2.1", false, false, true);

    static final TransferSyntax IMPLICIT_LITTLEENDIAN = new TransferSyntax(
            "1.2.840.10008.1.2", false, false, false);

    static final TransferSyntax JPEG_EXTENDED_2_4 = new TransferSyntax(
            "1.2.840.10008.1.2.4.51", false, true, true);

    static final TransferSyntax JPEG_BASELINE_1 = new TransferSyntax(
            "1.2.840.10008.1.2.4.50", false, true, true);

    static final TransferSyntax JPEG_PROGRESSIVE_10_12 = new TransferSyntax(
            "1.2.840.10008.1.2.4.56", false, true, true);

    static final TransferSyntax JPEG_LOSSLESS_14 = new TransferSyntax(
            "1.2.840.10008.1.2.4.57", false, true, true);

    static final TransferSyntax JPEG_LOSSLESS_15 = new TransferSyntax(
            "1.2.840.10008.1.2.4.58", false, true, true);

    static final TransferSyntax JPEG_LOSSLESS_14SV1 = new TransferSyntax(
            "1.2.840.10008.1.2.4.70", false, true, true);

    static final TransferSyntax JPEG_LS_LOSSLESS = new TransferSyntax(
            "1.2.840.10008.1.2.4.80", false, true, true);

    static final TransferSyntax JPEG_LS_NEAR_LOSSLESS = new TransferSyntax(
            "1.2.840.10008.1.2.4.81", false, true, true);

    static final TransferSyntax JPEG2000_LOSSLESS = new TransferSyntax(
            "1.2.840.10008.1.2.4.90", false, true, true);

    static final TransferSyntax JPEG2000 = new TransferSyntax(
            "1.2.840.10008.1.2.4.91", false, true, true);

    static final TransferSyntax DEFLATED_EXPLICIT_LITTLEENDIAN = new TransferSyntax(
            "1.2.840.10008.1.2.1.99", false, false, true);

    private static final TransferSyntax[] ALWAYS_SUPPORTED_INPUT = {
            RLE_COMPRESSION, EXPLICIT_BIGENDIAN, EXPLICIT_LITTLEENDIAN,
            IMPLICIT_LITTLEENDIAN };

    private static final TransferSyntax[] POSSIBLY_SUPPORTED_INPUT = {
            JPEG_EXTENDED_2_4, JPEG_BASELINE_1, JPEG_PROGRESSIVE_10_12,
            JPEG_LOSSLESS_14, JPEG_LOSSLESS_15, JPEG_LOSSLESS_14SV1,
            JPEG_LS_LOSSLESS, JPEG_LS_NEAR_LOSSLESS, JPEG2000_LOSSLESS,
            JPEG2000 };

    private static final ImageReaderSpi JAI_JPEG_R_SPI = ((ImageReaderSpi) (getSpiByClassName("com.sun.media.imageioimpl.plugins.jpeg.CLibJPEGImageReaderSpi")));

    private static final ImageReaderSpi J2SE_JPEG_R_SPI = ((ImageReaderSpi) (getSpiByClassName("com.sun.imageio.plugins.jpeg.JPEGImageReaderSpi")));

    private static final ImageReaderSpi JAI_PREFERED_JPEG_R_SPI = JAI_JPEG_R_SPI == null ? J2SE_JPEG_R_SPI
            : JAI_JPEG_R_SPI;

    private static final ImageReaderSpi DEFAULT_JPEG_R_SPI;

    private static final ImageReaderSpi DEFAULT_JPEG2000_R_SPI;

    private static final HashMap TS_R_SPI;

    static final TransferSyntax[] SUPPORTED_INPUT;

    private static final HashMap SUPPORTED_INPUT_HASH;

    static final TransferSyntax[] ALWAYS_SUPPORTED_OUTPUT;

    private static final TransferSyntax[] POSSIBLY_SUPPORTED_OUTPUT;

    private static final ImageWriterSpi JAI_JPEG_W_SPI;

    private static final ImageWriterSpi J2SE_JPEG_W_SPI;

    private static final ImageWriterSpi JAI_PREFERED_JPEG_W_SPI;

    private static final ImageWriterSpi DEFAULT_JPEG_W_SPI;

    private static final ImageWriterSpi DEFAULT_JPEG2000_W_SPI;

    private static final HashMap TS_W_SPI;

    static final TransferSyntax[] SUPPORTED_OUTPUT;

    static final ArrayList ALWAYS_SUPPORTED_OUTPUT_UIDLIST;

    private static final HashMap SUPPORTED_OUTPUT_HASH;

    final String uid;

    final boolean bigEndian;

    final boolean encapsulated;

    final boolean explicitVR;

    /* synthetic */static Class class$javax$imageio$spi$ImageReaderSpi;

    /* synthetic */static Class class$javax$imageio$spi$ImageWriterSpi;

    private static ImageReaderWriterSpi getSpiByClassName(String string) {
        ImageReaderWriterSpi imagereaderwriterspi;
        try {
            Class var_class = Class.forName(string);
            imagereaderwriterspi = ((ImageReaderWriterSpi) IIORegistry
                    .getDefaultInstance().getServiceProviderByClass(var_class));
        } catch (Exception exception) {
            return null;
        }
        return imagereaderwriterspi;
    }

    private static ImageReaderWriterSpi getSpiByFormat(final String format,
            Class var_class) {
        Iterator iterator = IIORegistry.getDefaultInstance()
                .getServiceProviders(var_class, new ServiceRegistry.Filter() {

                    public boolean filter(Object object) {
                        String[] strings = ((ImageReaderWriterSpi) object)
                                .getFormatNames();
                        for (int i = 0; i < strings.length; i++) {
                            if (format.equals(strings[i]))
                                return true;
                        }
                        return false;
                    }
                }, true);
        if (!iterator.hasNext())
            return null;
        return (ImageReaderWriterSpi) iterator.next();
    }

    static ArrayList getSupportedInputAsUidList() {
        ArrayList arraylist = new ArrayList(SUPPORTED_INPUT.length);
        for (int i = 0; i < SUPPORTED_INPUT.length; i++)
            arraylist.add(SUPPORTED_INPUT[i].uid);
        return arraylist;
    }

    private static ArrayList getAlwaysSupportedOutputAsUidList() {
        ArrayList arraylist = new ArrayList(ALWAYS_SUPPORTED_OUTPUT.length);
        for (int i = 0; i < ALWAYS_SUPPORTED_OUTPUT.length; i++)
            arraylist.add(ALWAYS_SUPPORTED_OUTPUT[i].uid);
        return arraylist;
    }

    final ImageReader getReader() {
        ImageReaderSpi imagereaderspi = (ImageReaderSpi) TS_R_SPI.get(this);
        ImageReader imagereader;
        try {
            imagereader = imagereaderspi.createReaderInstance();
        } catch (Exception exception) {
            return null;
        }
        return imagereader;
    }

    final ImageWriterSpi getWriterSpi() {
        return (ImageWriterSpi) TS_W_SPI.get(this);
    }

    final ImageWriter getWriter() {
        ImageWriter imagewriter;
        try {
            imagewriter = getWriterSpi().createWriterInstance();
        } catch (Exception exception) {
            return null;
        }
        return imagewriter;
    }

    static final TransferSyntax getInstance(String string)
            throws DicomException {
        TransferSyntax transfersyntax = (TransferSyntax) SUPPORTED_INPUT_HASH
                .get(string);
        if (transfersyntax == null)
            throw new DicomException("Unsupported", "Transfer Syntax: "
                    + string);
        return transfersyntax;
    }

    static final TransferSyntax[] intersectWithSupported(String[] strings) {
        ArrayList arraylist = new ArrayList(strings.length);
        for (int i = 0; i < strings.length; i++) {
            TransferSyntax transfersyntax = (TransferSyntax) SUPPORTED_OUTPUT_HASH
                    .get(strings[i]);
            if (transfersyntax != null)
                arraylist.add(transfersyntax);
        }
        TransferSyntax[] transfersyntaxes = new TransferSyntax[arraylist.size()];
        arraylist.toArray(transfersyntaxes);
        return transfersyntaxes;
    }

    private TransferSyntax(String string, boolean bool, boolean bool_0_,
            boolean bool_1_) {
        uid = string;
        bigEndian = bool;
        encapsulated = bool_0_;
        explicitVR = bool_1_;
    }

    public String toString() {
        return uid;
    }

    static {
        DEFAULT_JPEG_R_SPI = ((ImageReaderSpi) getSpiByFormat("jpeg",
                ImageReaderSpi.class));
        ImageReaderSpi imagereaderspi = ((ImageReaderSpi) (getSpiByClassName("com.sun.media.imageioimpl.plugins.jpeg2000.J2KImageReaderCodecLibSpi")));
        if (imagereaderspi == null)
            imagereaderspi = ((ImageReaderSpi) (getSpiByFormat("jpeg 2000",
                    ImageReaderSpi.class)));
        DEFAULT_JPEG2000_R_SPI = imagereaderspi;
        TS_R_SPI = new HashMap();
        TS_R_SPI.put(JPEG_BASELINE_1, JAI_PREFERED_JPEG_R_SPI);
        TS_R_SPI.put(JPEG_EXTENDED_2_4, JAI_PREFERED_JPEG_R_SPI);
        TS_R_SPI.put(JPEG_LOSSLESS_14, JAI_JPEG_R_SPI);
        TS_R_SPI.put(JPEG_LOSSLESS_14SV1, JAI_JPEG_R_SPI);
        TS_R_SPI.put(JPEG_LOSSLESS_15, JAI_JPEG_R_SPI);
        TS_R_SPI.put(JPEG_LS_LOSSLESS, JAI_JPEG_R_SPI);
        TS_R_SPI.put(JPEG_LS_NEAR_LOSSLESS, JAI_JPEG_R_SPI);
        TS_R_SPI.put(JPEG_PROGRESSIVE_10_12, J2SE_JPEG_R_SPI);
        TS_R_SPI.put(JPEG2000_LOSSLESS, DEFAULT_JPEG2000_R_SPI);
        TS_R_SPI.put(JPEG2000, DEFAULT_JPEG2000_R_SPI);
        ArrayList arraylist = new ArrayList(ALWAYS_SUPPORTED_INPUT.length
                + POSSIBLY_SUPPORTED_INPUT.length);
        for (int i = 0; i < ALWAYS_SUPPORTED_INPUT.length; i++)
            arraylist.add(ALWAYS_SUPPORTED_INPUT[i]);
        for (int i = 0; i < POSSIBLY_SUPPORTED_INPUT.length; i++) {
            if (TS_R_SPI.get(POSSIBLY_SUPPORTED_INPUT[i]) != null)
                arraylist.add(POSSIBLY_SUPPORTED_INPUT[i]);
        }
        SUPPORTED_INPUT = new TransferSyntax[arraylist.size()];
        arraylist.toArray(SUPPORTED_INPUT);
        SUPPORTED_INPUT_HASH = new HashMap();
        for (int i = 0; i < SUPPORTED_INPUT.length; i++)
            SUPPORTED_INPUT_HASH
                    .put(SUPPORTED_INPUT[i].uid, SUPPORTED_INPUT[i]);
        ALWAYS_SUPPORTED_OUTPUT = new TransferSyntax[] { EXPLICIT_LITTLEENDIAN,
                EXPLICIT_BIGENDIAN, IMPLICIT_LITTLEENDIAN };
        POSSIBLY_SUPPORTED_OUTPUT = new TransferSyntax[] { JPEG_EXTENDED_2_4,
                JPEG_BASELINE_1, JPEG_LOSSLESS_14SV1, JPEG_LS_LOSSLESS,
                JPEG2000_LOSSLESS, JPEG2000 };
        JAI_JPEG_W_SPI = ((ImageWriterSpi) (getSpiByClassName("com.sun.media.imageioimpl.plugins.jpeg.CLibJPEGImageWriterSpi")));
        J2SE_JPEG_W_SPI = ((ImageWriterSpi) (getSpiByClassName("com.sun.imageio.plugins.jpeg.JPEGImageWriterSpi")));
        JAI_PREFERED_JPEG_W_SPI = JAI_JPEG_W_SPI == null ? J2SE_JPEG_W_SPI
                : JAI_JPEG_W_SPI;
        DEFAULT_JPEG_W_SPI = ((ImageWriterSpi) getSpiByFormat("jpeg",
                ImageWriterSpi.class));
        ImageWriterSpi imagewriterspi = ((ImageWriterSpi) (getSpiByClassName("com.sun.media.imageioimpl.plugins.jpeg2000.J2KImageWriterCodecLibSpi")));
        if (imagewriterspi == null)
            imagewriterspi = ((ImageWriterSpi) (getSpiByFormat("jpeg 2000",
                    ImageWriterSpi.class)));
        DEFAULT_JPEG2000_W_SPI = imagewriterspi;
        TS_W_SPI = new HashMap();
        TS_W_SPI.put(JPEG_BASELINE_1, JAI_PREFERED_JPEG_W_SPI);
        TS_W_SPI.put(JPEG_EXTENDED_2_4, JAI_PREFERED_JPEG_W_SPI);
        TS_W_SPI.put(JPEG_LOSSLESS_14SV1, JAI_JPEG_W_SPI);
        TS_W_SPI.put(JPEG_LS_LOSSLESS, JAI_JPEG_W_SPI);
        TS_W_SPI.put(JPEG2000_LOSSLESS, DEFAULT_JPEG2000_W_SPI);
        TS_W_SPI.put(JPEG2000, DEFAULT_JPEG2000_W_SPI);
        arraylist = new ArrayList(ALWAYS_SUPPORTED_OUTPUT.length
                + POSSIBLY_SUPPORTED_OUTPUT.length);
        for (int i = 0; i < ALWAYS_SUPPORTED_OUTPUT.length; i++)
            arraylist.add(ALWAYS_SUPPORTED_OUTPUT[i]);
        for (int i = 0; i < POSSIBLY_SUPPORTED_OUTPUT.length; i++) {
            if (TS_W_SPI.get(POSSIBLY_SUPPORTED_OUTPUT[i]) != null)
                arraylist.add(POSSIBLY_SUPPORTED_OUTPUT[i]);
        }
        SUPPORTED_OUTPUT = new TransferSyntax[arraylist.size()];
        arraylist.toArray(SUPPORTED_OUTPUT);
        ALWAYS_SUPPORTED_OUTPUT_UIDLIST = getAlwaysSupportedOutputAsUidList();
        SUPPORTED_OUTPUT_HASH = new HashMap();
        for (int i = 0; i < SUPPORTED_OUTPUT.length; i++)
            SUPPORTED_OUTPUT_HASH.put(SUPPORTED_OUTPUT[i].uid,
                    SUPPORTED_OUTPUT[i]);
    }
}
