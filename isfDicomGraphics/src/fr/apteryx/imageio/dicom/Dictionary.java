/* Dictionary - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StreamTokenizer;
import java.net.URL;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

public class Dictionary
{
    private static Hashtable dico;
    private static Vector groups = new Vector();
    /*synthetic*/ static Class class$fr$apteryx$imageio$dicom$Dictionary;
    
    static class Tokenizer extends StreamTokenizer
    {
	Tokenizer(InputStream inputstream) {
	    super(new InputStreamReader(inputstream));
	    resetSyntax();
	    eolIsSignificant(true);
	    wordChars(97, 122);
	    wordChars(65, 90);
	    wordChars(160, 255);
	    whitespaceChars(32, 32);
	    whitespaceChars(9, 9);
	    wordChars(48, 57);
	    commentChar(35);
	}
	
	void error(String string) {
	    System.err.println(lineno() + ": " + string);
	    System.exit(1);
	}
	
	void parse() throws IOException {
	    while (nextToken() != -1) {
		RepeatingGroup repeatinggroup = null;
		if (ttype != 10 && ttype != 13) {
		    if (ttype != 40)
			error("Expected (");
		    nextToken();
		    if (ttype != -3)
			error("Group number expected");
		    int i = Integer.parseInt(sval, 16);
		    nextToken();
		    if (ttype == 45) {
			repeatinggroup = new RepeatingGroup();
			repeatinggroup.group1 = i;
			nextToken();
			if (ttype != -3)
			    error("u, o or group number expected");
			if (sval.length() == 1) {
			    if (sval.equals("u")) {
				RepeatingGroup repeatinggroup_0_
				    = repeatinggroup;
				if (repeatinggroup != null) {
				    /* empty */
				}
				repeatinggroup_0_.gtype = 0;
			    } else if (sval.equals("o")) {
				RepeatingGroup repeatinggroup_1_
				    = repeatinggroup;
				if (repeatinggroup != null) {
				    /* empty */
				}
				repeatinggroup_1_.gtype = 1;
			    } else
				error("u or o expected");
			    nextToken();
			    if (ttype != 45)
				error("- expected");
			    nextToken();
			    if (ttype != -3)
				error("Group number expected");
			}
			repeatinggroup.group2 = Integer.parseInt(sval, 16);
			nextToken();
		    }
		    if (ttype != 44)
			error(", expected");
		    nextToken();
		    if (ttype != -3)
			error("Element number expected");
		    int i_2_ = Integer.parseInt(sval, 16);
		    nextToken();
		    if (ttype == 45) {
			if (repeatinggroup == null) {
			    repeatinggroup = new RepeatingGroup();
			    repeatinggroup.group1 = repeatinggroup.group2 = i;
			    RepeatingGroup repeatinggroup_3_ = repeatinggroup;
			    if (repeatinggroup != null) {
				/* empty */
			    }
			    repeatinggroup_3_.gtype = 0;
			}
			repeatinggroup.element1 = i_2_;
			nextToken();
			if (ttype != -3)
			    error("u, o or element number expected");
			if (sval.length() == 1) {
			    if (sval.equals("u")) {
				RepeatingGroup repeatinggroup_4_
				    = repeatinggroup;
				if (repeatinggroup != null) {
				    /* empty */
				}
				repeatinggroup_4_.etype = 0;
			    } else if (sval.equals("o")) {
				RepeatingGroup repeatinggroup_5_
				    = repeatinggroup;
				if (repeatinggroup != null) {
				    /* empty */
				}
				repeatinggroup_5_.etype = 1;
			    } else
				error("u or o expected");
			    nextToken();
			    if (ttype != 45)
				error("- expected");
			    nextToken();
			    if (ttype != -3)
				error("Element number expected");
			}
			repeatinggroup.element2 = Integer.parseInt(sval, 16);
			nextToken();
		    } else if (repeatinggroup != null) {
			repeatinggroup.element1 = repeatinggroup.element2
			    = i_2_;
			RepeatingGroup repeatinggroup_6_ = repeatinggroup;
			if (repeatinggroup != null) {
			    /* empty */
			}
			repeatinggroup_6_.etype = 0;
		    }
		    if (ttype != 41)
			error(") expected");
		    Attribute attribute = new Attribute();
		    nextToken();
		    if (ttype != -3)
			error("VR expected");
		    if (sval.length() != 2)
			error("VR is not 2 character long");
		    attribute.VR = sval;
		    nextToken();
		    if (ttype != -3)
			error("Name expected");
		    attribute.name = sval;
		    while (nextToken() != 10 && ttype != 13) {
			/* empty */
		    }
		    if (repeatinggroup != null)
			repeatinggroup.attribute = attribute;
		    else
			put(Tag.create(i, i_2_), attribute);
		}
	    }
	}
    }
    
    static final class RepeatingGroup implements Serializable
    {
	static final int ODD = 1;
	static final int EVEN = 2;
	static final int ALL = 0;
	int group1;
	int group2;
	int element1;
	int element2;
	int gtype = 2;
	int etype = 2;
	Attribute attribute;
	
	RepeatingGroup() {
	    Dictionary.groups.add(this);
	}
    }
    
    private static void put(int i, Attribute attribute) {
	dico.put(new Integer(i), attribute);
    }
    
    public static void learn(int i, String string, String string_7_) {
	Attribute attribute = new Attribute();
	attribute.tag = i;
	attribute.VR = string;
	attribute.name = string_7_;
	put(i, attribute);
    }
    
    public static boolean contains(int i) {
	return contains(new Integer(i));
    }
    
    public static boolean contains(Integer integer) {
	return dico.containsKey(integer);
    }
    
    static Attribute getAttribute(int i) {
	Attribute attribute = (Attribute) dico.get(new Integer(i));
	if (attribute != null)
	    return attribute;
	int i_8_ = Tag.getGroupNumber(i);
	int i_9_ = Tag.getElementNumber(i);
	Iterator iterator = groups.iterator();
	while (iterator.hasNext()) {
	    RepeatingGroup repeatinggroup = (RepeatingGroup) iterator.next();
	    int i_10_ = repeatinggroup.gtype;
	    if (repeatinggroup != null) {
		/* empty */
	    }
	    if (i_10_ != 1 || i_8_ % 2 != 0) {
		int i_11_ = repeatinggroup.gtype;
		if (repeatinggroup != null) {
		    /* empty */
		}
		if (i_11_ != 2 || i_8_ % 2 != 1) {
		    int i_12_ = repeatinggroup.etype;
		    if (repeatinggroup != null) {
			/* empty */
		    }
		    if (i_12_ != 1 || i_9_ % 2 != 0) {
			int i_13_ = repeatinggroup.etype;
			if (repeatinggroup != null) {
			    /* empty */
			}
			if (i_13_ != 2 || i_9_ % 2 != 1) {
			    if (i_8_ >= repeatinggroup.group1
				&& i_8_ <= repeatinggroup.group2
				&& i_9_ >= repeatinggroup.element1
				&& i_9_ <= repeatinggroup.element2)
				return repeatinggroup.attribute;
			}
		    }
		}
	    }
	}
	return null;
    }
    
    static {
	InputStream inputstream
	    = Dictionary.class.getResourceAsStream("dicom.dic.ser");
	if (inputstream != null) {
	    try {
		ObjectInputStream objectinputstream
		    = new ObjectInputStream(inputstream);
		dico = (Hashtable) objectinputstream.readObject();
		groups = (Vector) objectinputstream.readObject();
		inputstream.close();
	    } catch (Exception exception) {
		exception.printStackTrace();
	    }
	}
	if (dico == null) {
	    System.err.println("Cannot read serialized dicom dictionary");
	    URL url = Dictionary.class.getResource("dicom.dic");
	    dico = new Hashtable();
	    try {
		InputStream inputstream_14_ = url.openStream();
		new Tokenizer(inputstream_14_).parse();
		inputstream_14_.close();
		ObjectOutputStream objectoutputstream
		    = new ObjectOutputStream(new FileOutputStream(url.getPath()
								  + ".ser"));
		objectoutputstream.writeObject(dico);
		objectoutputstream.writeObject(groups);
		objectoutputstream.close();
	    } catch (IOException ioexception) {
		ioexception.printStackTrace();
		System.exit(1);
	    }
	}
    }
}
