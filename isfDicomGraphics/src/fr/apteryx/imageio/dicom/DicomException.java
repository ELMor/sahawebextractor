/* DicomException - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.util.HashMap;

import javax.imageio.IIOException;

public final class DicomException extends IIOException
{
    private String details;
    static final HashMap translated = new HashMap();
    
    DicomException(String string, String string_0_) {
	super(string);
	details = string_0_;
    }
    
    public String getDetails() {
	return details;
    }
    
    public String toString() {
	return (details != null ? getLocalizedMessage() + ": " + details
		: getLocalizedMessage());
    }
    
    static String localize(String string) {
	String string_1_ = (String) translated.get(string);
	return string_1_ == null ? string : string_1_;
    }
    
    public String getLocalizedMessage() {
	return localize(getMessage());
    }
    
    static {
	translated.put("Impossible", "Bug");
	translated.put("Unsupported", "Unsupported");
	translated.put("ServiceRefused",
		       "The service was refused by the remote application");
	translated.put("ServiceError",
		       "The application was unable to complete the operation");
	translated.put("AttributeCoercion",
		       "The value of an attribute was coerced");
	translated.put("EnvironmentError",
		       "The operation failed due to external reasons");
	translated.put("ServiceWarning",
		       "The operation completed with warnings");
	translated.put("MissingMandatoryAttribute",
		       "A mandatory attribute is missing");
	translated.put("ProtocoleViolation", "DICOM protocole violation");
    }
}
