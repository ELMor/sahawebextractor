/* AbstractSyntax - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.util.ArrayList;

final class AbstractSyntax
{
    static final int REASON_ACCEPTANCE = 0;
    static final int REASON_USER_REJECTION = 1;
    static final int REASON_NO_REASON = 2;
    static final int REASON_ABSTRACT_SYNTAX_NOT_SUPPORTED = 3;
    static final int REASON_TRANSFER_SYNTAXES_NOT_SUPPORTED = 4;
    static final String[] REASONS
	= { "acceptance", "user rejection", "no reason",
	    "abstract syntax not supported", "transfer syntax not supported" };
    static final int ROLE_DEFAULT = 0;
    static final int ROLE_SCU = 1;
    static final int ROLE_SCP = 2;
    static final int ROLE_SCU_SCP = 3;
    final int id;
    final String name;
    final ArrayList ts;
    int role;
    TransferSyntax selected_ts;
    int reason = 2;
    
    AbstractSyntax(int i, String string, ArrayList arraylist, int i_0_) {
	id = i;
	name = string;
	ts = arraylist;
	role = i_0_;
    }
    
    AbstractSyntax(int i, String string, ArrayList arraylist) {
	this(i, string, arraylist, 0);
    }
    
    public String toString() {
	String string;
	switch (role) {
	case 1:
	    string = "SCU";
	    break;
	case 2:
	    string = "SCP";
	    break;
	case 3:
	    string = "SCU/SCP";
	    break;
	default:
	    string = "default";
	}
	String string_1_;
	if (ts != null)
	    string_1_ = "\n Proposed TS:" + ts;
	else
	    string_1_ = "";
	return ("Abstract Syntax ID " + id + ", " + name + ", " + string
		+ " role" + string_1_);
    }
}
