/* Normalizer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

final class Normalizer implements Constants
{
    private final DicomWarningListener wl;
    private DicomObject obj;
    private DataSet ds;
    private int im;
    private String sopClass;
    private String sopInstance;
    private static final int[] DEFAULT_PIXEL_SPACING = { 1, 1 };
    private static final int[] DEFAULT_IMAGE_ORIENTATION_PATIENT = new int[6];
    private static final int[] DEFAULT_IMAGE_POSITION_PATIENT = new int[3];
    private static final String[] DEFAULT_CT_IMAGE_TYPE
	= { "DERIVED", "SECONDARY", "AXIAL" };
    private static final String[] DEFAULT_MR_IMAGE_TYPE
	= { "DERIVED", "SECONDARY", "OTHER" };
    private static final String[] DEFAULT_ENHCT_IMAGE_TYPE
	= { "DERIVED", "PRIMARY", "VOLUME", "NONE" };
    private static final String[] DEFAULT_ENHMR_IMAGE_TYPE
	= { "DERIVED", "PRIMARY", "T1", "NONE" };
    private static final String[] DEFAULT_NM_IMAGE_TYPE
	= { "DERIVED", "PRIMARY", "RECON TOMO" };
    private static final String[] DEFAULT_XA_IMAGE_TYPE
	= { "DERIVED", "SECONDARY", "SINGLE PLANE" };
    private static final String[] DEFAULT_RT_IMAGE_TYPE
	= { "DERIVED", "SECONDARY", "RADIOGRAPH" };
    private static final String[] DEFAULT_DX_IMAGE_TYPE
	= { "DERIVED", "SECONDARY", "" };
    private static final String[] DEFAULT_PATIENT_ORIENTATION = { "H", "R" };
    private static final int[] DEFAULT_FIELD_OF_VIEW_ORIGIN = { 0, 0 };
    private static final String[] DEFAULT_VL_IMAGE_TYPE
	= { "DERIVED", "SECONDARY" };
    private static final String[] DEFAULT_PT_SERIES_TYPE
	= { "STATIC", "IMAGE" };
    private static final String[] DEFAULT_PT_IMAGETYPE
	= { "DERIVED", "PRIMARY" };
    private static final String[] DEFAULT_OP_IMAGETYPE
	= { "DERIVED", "PRIMARY" };
    /*synthetic*/ static Class array$Ljava$lang$String;
    /*synthetic*/ static Class class$fr$apteryx$imageio$dicom$Day;
    /*synthetic*/ static Class class$fr$apteryx$imageio$dicom$Time;
    /*synthetic*/ static Class array$Ljava$lang$Object;
    
    Normalizer(DicomWarningListener dicomwarninglistener) {
	wl = dicomwarninglistener;
    }
    
    private int getNumberOfFrames() {
	Object object = ds.findValue(2145386512);
	return object instanceof Collection ? ((Collection) object).size() : 1;
    }
    
    synchronized void normalize(DicomObject dicomobject)
	throws DicomException {
	im = 0;
	ds = dicomobject.ds;
	obj = dicomobject;
	Object object = ds.findValue(2145386512);
	if (object == null)
	    throw new DicomException("MissingMandatoryAttribute", "PixelData");
	if (dicomobject.sopClass == null) {
	    String string = ds.findString(524384);
	    dicomobject.sopClass = Modality.modalityToSop(string, object);
	    if (dicomobject.sopClass == null
		|| dicomobject.sopClass.length() == 0)
		throw new DicomException
			  ("InvalidFormat",
			   "No available SOP class to encode this data format");
	}
	if (dicomobject.sopInstance == null
	    || dicomobject.sopInstance.length() == 0)
	    dicomobject.sopInstance = UID.createUID(1);
	sopClass = dicomobject.sopClass;
	sopInstance = dicomobject.sopInstance;
	if (sopClass.equals("1.2.840.10008.5.1.4.1.1.1")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modCRSeries();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modCRImage();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.2")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modFrameOfReference();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePlane();
	    modImagePixel();
	    modCTImage();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.2.1")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modCTSeries();
	    modFrameOfReference();
	    modGeneralEquipment();
	    modMultiframeDimension();
	    modAcquisitionContext();
	    ColorModel[] colormodels = getColorModels(object);
	    modEnhCTImage(colormodels);
	    modImagePixel();
	    modMultiframeFunctionalGroups(colormodels);
	    modSoftCopyPresentationLUT();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.4")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modFrameOfReference();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePlane();
	    modImagePixel();
	    modMRImage();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.4.1")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modMRSeries();
	    modFrameOfReference();
	    modGeneralEquipment();
	    modMultiframeDimension();
	    ColorModel[] colormodels = getColorModels(object);
	    modEnhMRImage(colormodels);
	    modImagePixel();
	    modMRPulseSeq();
	    modMultiframeFunctionalGroups(colormodels);
	    modSoftCopyPresentationLUT();
	    modAcquisitionContext();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.20")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modNMPETPatientOrientation();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modNMImagePixel();
	    modMultiFrame();
	    modNMMultiFrame();
	    modNMImage();
	    modNMIsotope();
	    modNMDetector();
	    String[] strings
		= ((String[])
		   ds.findValue(524296, String.class));
	    if ("TOMO".equals(strings[2]) || "GATED TOMO".equals(strings[2])
		|| "RECON TOMO".equals(strings[2])
		|| "RECON GATED TOMO".equals(strings[2]))
		modNMTOMOAcquisition();
	    if ("GATED".equals(strings[2]) || "GATED TOMO".equals(strings[2])
		|| "RECON GATED TOMO".equals(strings[2]))
		modNMMultigatedAcquisition();
	    if ("DYNAMIC".equals(strings[2]))
		modNMPhase();
	    if ("RECON TOMO".equals(strings[2])
		|| "RECON GATED TOMO".equals(strings[2]))
		modNMReconstruction();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.6.1")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modUSImage();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.3.1")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modMultiFrame();
	    modUSImage();
	    modCine();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.7")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modSCEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modSCImage();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.7.2")
		   || sopClass.equals("1.2.840.10008.5.1.4.1.1.7.3")
		   || sopClass.equals("1.2.840.10008.5.1.4.1.1.7.4")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modSCEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modMultiFrame();
	    modSCImage();
	    modSCMultiFrameImage();
	    modCine();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.12.1")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modMultiFrame();
	    modXRayImage();
	    modCine();
	    modXRayAcquisition();
	    modXAPositioner();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.12.2")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modMultiFrame();
	    modXRayImage();
	    modCine();
	    modXRayAcquisition();
	    if ("TOMO".equals(ds.findString(1572898)))
		modXRayTomoAcquisition();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.481.1")) {
	    modPatient();
	    modGeneralStudy();
	    modRTSeries();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    if (getNumberOfFrames() > 1) {
		modMultiFrame();
		modCine();
	    }
	    modRTImage();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.1.1")
		   || sopClass.equals("1.2.840.10008.5.1.4.1.1.1.1.1")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modDXSeries(sopClass.equals("1.2.840.10008.5.1.4.1.1.1.1")
			? "FOR PRESENTATION" : "FOR PROCESSING");
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modDXAnatomyImaged();
	    modDXImage();
	    modDXDetector();
	    modAcquisitionContext();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.1.2")
		   || sopClass.equals("1.2.840.10008.5.1.4.1.1.1.2.1")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modDXSeries(sopClass.equals("1.2.840.10008.5.1.4.1.1.1.2")
			? "FOR PRESENTATION" : "FOR PROCESSING");
	    modMammoSeries();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modDXImage();
	    modDXDetector();
	    modMammoImage();
	    modAcquisitionContext();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.1.2")
		   || sopClass.equals("1.2.840.10008.5.1.4.1.1.1.2.1")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modDXSeries(sopClass.equals("1.2.840.10008.5.1.4.1.1.1.2")
			? "FOR PRESENTATION" : "FOR PROCESSING");
	    modIntraOralSeries();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modDXImage();
	    modDXDetector();
	    modIntraOralImage();
	    modAcquisitionContext();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.77.1.1")
		   || sopClass.equals("1.2.840.10008.5.1.4.1.1.77.1.4")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modVLImage();
	    modAcquisitionContext();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.77.1.5.1")
		   || sopClass.equals("1.2.840.10008.5.1.4.1.1.77.1.5.2")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modOphthalmicPhotographySeries();
	    modSynchronization();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modMultiFrame();
	    modOphthalmicPhotographicParameters();
	    modOphthalmicPhotographyImage
		(sopClass.equals("1.2.840.10008.5.1.4.1.1.77.1.5.1") ? 8 : 16);
	    modOcularRegionImaged();
	    modOphthalmicPhotographyAcquisitionParameters();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.77.1.2")) {
	    modPatient();
	    modSpecimenIdentification();
	    modGeneralStudy();
	    modGeneralSeries();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modVLImage();
	    modAcquisitionContext();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.77.1.3")) {
	    modPatient();
	    modSpecimenIdentification();
	    modGeneralStudy();
	    modGeneralSeries();
	    modFrameOfReference();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePixel();
	    modVLImage();
	    modSlideCoordinates();
	    modAcquisitionContext();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.4.1.1.128")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modNMPETPatientOrientation();
	    modPETSeries();
	    modPETIsotope();
	    String[] strings
		= ((String[])
		   ds.findValue(5509120, String.class));
	    if (strings != null && strings.length > 1
		&& "GATED".equals(strings[1]))
		modPETMultiGatedAcquisition();
	    modFrameOfReference();
	    modGeneralEquipment();
	    modGeneralImage();
	    modImagePlane();
	    modImagePixel();
	    modPETImage();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.1.30")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modHardcopyEquipment();
	    modGeneralImage();
	    modHCGrayscaleImage();
	    modSOPCommon();
	} else if (sopClass.equals("1.2.840.10008.5.1.1.29")) {
	    modPatient();
	    modGeneralStudy();
	    modGeneralSeries();
	    modHardcopyEquipment();
	    modGeneralImage();
	    modHCColorImage();
	    modSOPCommon();
	} else
	    wl.warning("Unsupported",
		       ("SOP Class " + sopClass
			+ " is not supported, conformance cannot be checked"));
    }
    
    private ColorModel[] getColorModels(Object object) {
	ColorModel[] colormodels = new ColorModel[getNumberOfFrames()];
	if (object instanceof Collection) {
	    int i = 0;
	    Iterator iterator = ((Collection) object).iterator();
	    while (iterator.hasNext())
		colormodels[i++] = Util.getColorModel(iterator.next());
	} else
	    colormodels[0] = Util.getColorModel(object);
	return colormodels;
    }
    
    private boolean codeSequenceHasItem(int i, String string,
					String string_0_) {
	Collection collection = ds.findSequence(i);
	if (collection != null) {
	    Iterator iterator = collection.iterator();
	    while (iterator.hasNext()) {
		DataSet dataset = (DataSet) iterator.next();
		if (string.equals(dataset.findString(524546))
		    && string_0_.equals(dataset.findString(524544)))
		    return true;
	    }
	}
	return false;
    }
    
    private void checkType2(int i) {
	if (!ds.has(i))
	    ds.add(i, (Object) null);
    }
    
    private void checkType2(int i, int i_1_) {
	if (!ds.has(i))
	    ds.add(i, ds.findValue(i_1_));
    }
    
    private void checkType1(int i, Object object) {
	if (ds.findValue(i) == null)
	    coercion(i, object);
    }
    
    private void checkType1(int i, int i_2_) {
	if (!ds.has(i))
	    coercion(i, new Integer(i_2_));
    }
    
    private void checkType1(DataSet dataset, int i, int i_3_) {
	if (!dataset.has(i))
	    coercion(dataset, i, new Integer(i_3_));
    }
    
    private void coercion(int i, Object object) {
	coercion(ds, i, object);
    }
    
    private void coercion(DataSet dataset, int i, Object object) {
	DataElement dataelement = new DataElement(i, object);
	dataset.add(dataelement);
	if (wl != null)
	    wl.warning(im, "AttributeCoercion", dataelement.toString());
    }
    
    private void force(int i, Object object) {
	Object object_4_ = ds.findValue(i);
	if (object_4_ == null || !object_4_.equals(object))
	    coercion(i, object);
    }
    
    private void force(int i, int i_5_) {
	force(i, new Integer(i_5_));
    }
    
    private void modPatient() {
	checkType2(1048592);
	checkType2(1048608);
	checkType2(1048624);
	checkType2(1048640);
    }
    
    private void modGeneralStudy() {
	if (!ds.has(2097165))
	    ds.add(2097165, UID.createUID(2));
	checkType2(524320, 524323);
	checkType2(524336, 524339);
	checkType2(2097168);
	checkType2(524432);
	checkType2(524368);
    }
    
    private void modGeneralSeries() {
	String string = ds.findString(524384);
	if (string == null) {
	    string = Modality.sopToModality(sopClass);
	    if (string != null)
		ds.add(524384, string);
	    else
		coercion(ds, 524384, "OT");
	}
	if (!ds.has(2097166))
	    ds.add(2097166, UID.createUID(3));
	checkType2(2097169);
	if ("CT".equals(string) || "MR".equals(string))
	    checkType2(1593600);
    }
    
    private void modCRSeries() {
	checkType2(1572885);
	checkType2(1593601);
    }
    
    private void modGeneralEquipment() {
	if (!ds.has(524400) && !ds.has(528528) && !ds.has(1576992)) {
	    ds.add(524400, "Apteryx");
	    ds.add(528528, "Dicom Image I/O Plugin");
	    ds.add(1576992, "1.08pre");
	}
    }
    
    private void modGeneralImage() {
	checkType2(2097171);
	if (!"1.2.840.10008.5.1.4.1.1.2".equals(sopClass)
	    && !"1.2.840.10008.5.1.4.1.1.2.1".equals(sopClass)
	    && !"1.2.840.10008.5.1.4.1.1.4".equals(sopClass)
	    && !"1.2.840.10008.5.1.4.1.1.4.1".equals(sopClass)
	    && !"1.2.840.10008.5.1.4.1.1.128".equals(sopClass))
	    checkType2(2097184);
    }
    
    private void modImagePixel() throws DicomException {
	if (!ds.has(2621442) || !ds.has(2621444) || !ds.has(2621456)
	    || !ds.has(2621457) || !ds.has(2621696) || !ds.has(2621697)
	    || !ds.has(2621698) || !ds.has(2621699) || !ds.has(2145386512))
	    throw new DicomException("MissingMandatoryAttribute",
				     "Image Pixel module");
	if (ds.findInt(2621442) > 1 && !ds.has(2621446))
	    throw new DicomException("MissingMandatoryAttribute",
				     "Planar Configuration");
	if (("PALETTE COLOR".equals(ds.findString(2621444))
	     || "COLOR".equals(ds.findString(561669))
	     || "MIXED".equals(ds.findString(561669)))
	    && (!ds.has(2625793) || !ds.has(2625794) || !ds.has(2625795)
		|| !ds.has(2626049) || !ds.has(2626050) || !ds.has(2626051)))
	    throw new DicomException("MissingMandatoryAttribute", "Palette");
    }
    
    private void modCRImage() throws DicomException {
	int i = ds.findInt(2621442);
	String string = ds.findString(2621444);
	if (i != 1)
	    throw new DicomException("ProtocoleViolation",
				     "CR Images need to be 1 band");
	if (!"MONOCHROME1".equals(string) && !"MONOCHROME2".equals(string))
	    force(2621444, "MONOCHROME2");
	if (ds.has(2621488) && !ds.has(1577316))
	    ds.add(1577316, ds.findValue(2621488));
    }
    
    private void modSOPCommon() {
	ds.add(524310, sopClass);
	ds.add(524312, sopInstance);
    }
    
    private void modFrameOfReference() {
	checkType1(2097234, sopInstance);
	checkType2(2101312);
    }
    
    private void modSynchronization() {
	checkType1(2097664, "1.2.840.10008.15.1.1");
	checkType1(1577066, "NO TRIGGER");
	checkType1(1579008, "N");
    }
    
    private void modImagePlane() {
	checkType1(2621488, DEFAULT_PIXEL_SPACING);
	checkType1(2097207, DEFAULT_IMAGE_ORIENTATION_PATIENT);
	checkType1(2097202, DEFAULT_IMAGE_POSITION_PATIENT);
	checkType2(1572944);
    }
    
    private void modCTImage() throws DicomException {
	if (ds.findInt(2621442) != 1)
	    throw new DicomException("ProtocoleViolation",
				     "CT images need to be 1 band");
	String string = ds.findString(2621444);
	if (!"MONOCHROME1".equals(string) && !"MONOCHROME2".equals(string))
	    force(2621444, "MONOCHROME2");
	if (ds.findInt(2621696) != 16)
	    throw new DicomException("ProtocoleViolation",
				     "CT images need to be 16 bits");
	int i = ds.findInt(2621697);
	if (i <= 12) {
	    ds.add(2621697, 12);
	    ds.add(2621698, 11);
	} else {
	    ds.add(2621697, 16);
	    ds.add(2621698, 15);
	}
	checkType1(524296, DEFAULT_CT_IMAGE_TYPE);
	checkType1(2625618, 0);
	checkType1(2625619, 1);
	checkType2(1572960);
	checkType2(2097170);
    }
    
    private void modMRImage() throws DicomException {
	if (ds.findInt(2621442) != 1)
	    throw new DicomException("ProtocoleViolation",
				     "MR images need to be 1 band");
	String string = ds.findString(2621444);
	if (!"MONOCHROME1".equals(string) && !"MONOCHROME2".equals(string))
	    force(2621444, "MONOCHROME2");
	int i = ds.findInt(2621697);
	if (ds.findInt(2621696) != 16)
	    throw new DicomException("ProtocoleViolation",
				     "MR images need to be 16 bits");
	checkType1(524296, DEFAULT_MR_IMAGE_TYPE);
	checkType1(1572896, "RM");
	checkType1(1572897, "NONE");
	checkType2(1572898);
	checkType2(1572899);
	checkType2(2097170);
	checkType2(1572992);
	checkType2(1572993);
	checkType2(1573009);
    }
    
    private void commonCTMRImageDescriptionMacro(ColorModel[] colormodels) {
	IndexColorModel indexcolormodel = null;
	boolean bool = false;
	for (int i = 0; i < colormodels.length; i++) {
	    im = i;
	    if (colormodels[i] instanceof IndexColorModel) {
		indexcolormodel = (IndexColorModel) colormodels[i];
		if (bool)
		    break;
	    } else {
		bool = true;
		if (indexcolormodel != null)
		    break;
	    }
	}
	im = 0;
	String string;
	if (indexcolormodel != null) {
	    obj.updateFromICM(indexcolormodel);
	    string = bool ? "MIXED" : "COLOR";
	} else
	    string = "MONOCHROME";
	checkType1(561669, string);
	checkType1(561670, "VOLUME");
	checkType1(561671, "NONE");
    }
    
    private void modEnhCTImage(ColorModel[] colormodels)
	throws DicomException {
	String[] strings
	    = ((String[])
	       ds.findValue(524296, String.class));
	if (strings == null || strings.length != 4) {
	    coercion(524296, DEFAULT_ENHCT_IMAGE_TYPE);
	    strings = DEFAULT_ENHCT_IMAGE_TYPE;
	}
	String string = strings[0];
	commonCTMRImageDescriptionMacro(colormodels);
	if ("ORIGINAL".equals(string) || "MIXED".equals(string)) {
	    checkType1(524330, new Date());
	    checkType2(1609843);
	}
	if (ds.has(528704) && !ds.has(561298))
	    throw new DicomException("MissingMandatoryAttribute",
				     "ReferencedImageEvidenceSequence");
	if (ds.has(532754) && !ds.has(561492))
	    throw new DicomException("MissingMandatoryAttribute",
				     "SourceImageEvidenceSequence");
	if (ds.findInt(2621442) != 1)
	    throw new DicomException("ProtocoleViolation",
				     "Enh CT images need to be 1 band");
	String string_6_ = ds.findString(2621444);
	force(2621444, "MONOCHROME2");
	int i = ds.findInt(2621697);
	int i_7_ = ds.findInt(2621696);
	if (i_7_ != 16)
	    throw new DicomException("ProtocoleViolation",
				     "Enh CT images need to be 16 bits");
	if (i <= 12)
	    i = 12;
	else
	    i = 16;
	force(2621697, i);
	force(2621698, i - 1);
	checkType1(1609732, "RESEARCH");
	checkType1(2629904, "00");
	if ("01".equals(ds.findString(2629904)))
	    checkType1(2629906, 1);
    }
    
    private void modMRSeries() {
	/* empty */
    }
    
    private void modEnhMRImage(ColorModel[] colormodels)
	throws DicomException {
	String[] strings
	    = ((String[])
	       ds.findValue(524296, String.class));
	if (strings == null || strings.length != 4) {
	    coercion(524296, DEFAULT_ENHMR_IMAGE_TYPE);
	    strings = DEFAULT_ENHMR_IMAGE_TYPE;
	}
	String string = strings[0];
	commonCTMRImageDescriptionMacro(colormodels);
	checkType1(561672, "MAGNITUDE");
	checkType1(561673, "UNKNOWN");
	if ("ORIGINAL".equals(string) || "MIXED".equals(string)) {
	    checkType1(524330, new Date());
	    checkType1(1609843, 0);
	    checkType1(1609984, "1H");
	    checkType1(1572999, 0);
	    checkType1(1609828, "NONE");
	}
	if (ds.has(528704) && !ds.has(561298))
	    throw new DicomException("MissingMandatoryAttribute",
				     "ReferencedImageEvidenceSequence");
	if (ds.has(532754) && !ds.has(561492))
	    throw new DicomException("MissingMandatoryAttribute",
				     "SourceImageEvidenceSequence");
	checkType1(1609732, "RESEARCH");
	checkType1(1610100, "IEC");
	if (ds.findInt(2621442) != 1)
	    throw new DicomException("ProtocoleViolation",
				     "Enh MR images need to be 1 band");
	String string_8_ = ds.findString(2621444);
	force(2621444, "MONOCHROME2");
	int i = ds.findInt(2621697);
	int i_9_ = ds.findInt(2621696);
	if (i_9_ != 16 && i_9_ != 8)
	    throw new DicomException("ProtocoleViolation",
				     "Enh MR images need to be 8 or 16 bits");
	if (i <= 8)
	    i = 8;
	else if (i <= 12)
	    i = 12;
	else
	    i = 16;
	force(2621697, i);
	checkType1(2629904, "00");
	if ("01".equals(ds.findString(2629904)))
	    checkType1(2629906, 1);
    }
    
    private void modNMPETPatientOrientation() {
	checkType2(5506064);
	checkType2(5506068);
    }
    
    private void modNMImagePixel() throws DicomException {
	if (ds.findInt(2621442) != 1)
	    throw new DicomException("ProtocoleViolation",
				     "NM images need to be 1 band");
	String string = ds.findString(2621444);
	if (!"MONOCHROME2".equals(string) && !"PALETTE COLOR".equals(string))
	    force(2621444, "MONOCHROME2");
	int i = ds.findInt(2621696);
	if (i != 8 && i != 16)
	    throw new DicomException("ProtocoleViolation",
				     "NM images need to be 8 or 16 bits");
	ds.add(2621697, i);
	ds.add(2621698, i - 1);
	checkType2(2621488);
    }
    
    private void modMultiFrame() {
	ds.add(2621448, getNumberOfFrames());
    }
    
    private void modNMMultiFrame() {
	String[] strings
	    = ((String[])
	       ds.findValue(524296, String.class));
	if (strings == null || strings.length < 3 || !ds.has(2621449)) {
	    strings = DEFAULT_NM_IMAGE_TYPE;
	    coercion(524296, strings);
	    coercion(2621449, new Integer(5505152));
	    int i = ds.findInt(2621448);
	    int[] is = new int[i];
	    for (int i_10_ = 0; i_10_ < i; i_10_++)
		is[i_10_] = i_10_ + 1;
	    coercion(5505152, is);
	    force(5505153, i);
	}
	if ("TOMO".equals(strings[2]) || "GATED TOMO".equals(strings[2])
	    || "RECON TOMO".equals(strings[2])
	    || "RECON GATED TOMO".equals(strings[2]))
	    checkType1(5505105, 1);
	checkType1(5505041, 1);
	checkType1(5505057, 1);
    }
    
    private void modNMImage() {
	checkType2(1572976);
    }
    
    private void modNMIsotope() {
	checkType2(5505042);
	checkType2(5505046);
    }
    
    private void modNMDetector() {
	checkType2(5505058);
    }
    
    private void modNMTOMOAcquisition() {
	checkType2(5505106);
    }
    
    private void modNMMultigatedAcquisition() {
	/* empty */
    }
    
    private void modNMPhase() {
	/* empty */
    }
    
    private void modNMReconstruction() {
	checkType2(1573000);
	checkType2(1572944);
    }
    
    private void modUSImage() throws DicomException {
	int i = ds.findInt(2621442);
	String string = ds.findString(2621444);
	switch (i) {
	case 1:
	    if (!"MONOCHROME2".equals(string)
		&& !"PALETTE COLOR".equals(string))
		force(2621444, "MONOCHROME2");
	    break;
	case 3:
	    if ("YBR_FULL".equals(string))
		ds.add(2621446, 1);
	    else if ("YBR_FULL_422".equals(string)
		     || "YBR_PARTIAL_422".equals(string)
		     || "YBR_PARTIAL_420".equals(string)
		     || "YBR_ICT".equals(string) || "YBR_RCT".equals(string))
		ds.add(2621446, 0);
	    else
		force(2621444, "RGB");
	    break;
	default:
	    throw new DicomException("ProtocoleViolation",
				     "US images need to be 1 band or 3 bands");
	}
	if (ds.findInt(2621699) != 0)
	    throw new DicomException("ProtocoleViolation",
				     "US images need to be unsigned");
	int i_11_ = ds.findInt(2621696);
	if ("PALETTE COLOR".equals(string)) {
	    if (i_11_ != 16 && i_11_ != 8)
		throw new DicomException
			  ("ProtocoleViolation",
			   "US palette-color images need to be 8 or 16 bits");
	} else if (i_11_ != 8)
	    throw new DicomException
		      ("ProtocoleViolation",
		       "US non-palette-color images need to be 8 bits");
	ds.add(2621697, i_11_);
	ds.add(2621698, i_11_ - 1);
	if (ds.has(2621448))
	    checkType1(2621449, 1577059);
	checkType2(524296);
    }
    
    private void modCine() {
	switch (ds.findInt(2621449)) {
	case 1577059:
	    checkType1(1577059, 1);
	    break;
	case 1577061:
	    if (!ds.has(1577061)) {
		int i = ds.findInt(2621448);
		int[] is = new int[i];
		is[0] = 0;
		for (int i_12_ = 1; i_12_ < i; i_12_++)
		    is[i_12_] = 1;
		coercion(1577061, is);
	    }
	    break;
	}
    }
    
    private void modSCEquipment() {
	checkType1(524388, "WSD");
    }
    
    private void modSCImage() throws DicomException {
	if (ds.findInt(2621699) != 0)
	    throw new DicomException("ProtocoleViolation",
				     "SC images need to be unsigned");
	if (!"1.2.840.10008.5.1.4.1.1.7".equals(sopClass)) {
	    if ("1.2.840.10008.5.1.4.1.1.7.4".equals(sopClass)) {
		if (ds.findInt(2621442) != 3)
		    throw new DicomException
			      ("ProtocoleViolation",
			       "SC true color images need to be 3 bands");
		String string = ds.findString(2621444);
		if (string != null && string.startsWith("YBR"))
		    force(2621444, "YBR_FULL_422");
		else
		    force(2621444, "RGB");
		ds.add(2621446, 0);
		if (ds.findInt(2621696) != 8)
		    throw new DicomException
			      ("ProtocoleViolation",
			       "SC true color images must be 8 bits");
		ds.add(2621697, 8);
		ds.add(2621698, 7);
	    } else {
		if (ds.findInt(2621442) != 1)
		    throw new DicomException
			      ("ProtocoleViolation",
			       "SC grayscale images need to be 1 band");
		force(2621444, "MONOCHROME2");
		if ("1.2.840.10008.5.1.4.1.1.7.3".equals(sopClass)) {
		    if (ds.findInt(2621696) != 16)
			throw new DicomException
				  ("ProtocoleViolation",
				   "SC grayscale word images must be 16 bits");
		    int i = ds.findInt(2621697);
		    if (i < 9)
			i = 9;
		    ds.add(2621697, i);
		    ds.add(2621698, i - 1);
		} else {
		    if (ds.findInt(2621696) != 8)
			throw new DicomException
				  ("ProtocoleViolation",
				   "SC grayscale byte images must be 8 bits");
		    ds.add(2621697, 8);
		    ds.add(2621698, 7);
		}
	    }
	}
    }
    
    private void modSCMultiFrameImage() {
	checkType1(2622209, "NO");
	if ("MONOCHROME2".equals(ds.findString(2621444))
	    && ds.findInt(2621697) > 1) {
	    force(542113824, "IDENTITY");
	    force(2625618, 0);
	    force(2625619, 1);
	    force(2625620, "US");
	}
	int i = ds.findInt(2621448);
	if (i > 1)
	    checkType1(2621449, 1577061);
	String string = ds.findString(524388);
	if (string.equals("DF")
	    || ((string.equals("SD") || string.equals("SI"))
		&& ds.has(2621488))) {
	    Object object = ds.findValue(2621488);
	    if (object == null)
		object = ds.findValue(2621492);
	    coercion(1581072,
		     object == null ? (Object) (new int[] { 1, 1 }) : object);
	}
    }
    
    private void modXRayImage() throws DicomException {
	if (ds.findInt(2621442) != 1)
	    throw new DicomException("ProtocoleViolation",
				     "XRay images need to be 1 band");
	force(2621444, "MONOCHROME2");
	if (ds.findInt(2621699) != 0)
	    throw new DicomException("ProtocoleViolation",
				     "XRay images need to be unsigned");
	int i = ds.findInt(2621696);
	if (i != 8 && i != 16)
	    throw new DicomException("ProtocoleViolation",
				     "XRay images need to be 8 or 16 bits");
	int i_13_ = ds.findInt(2621697);
	if (i_13_ <= 8) {
	    ds.add(2621697, 8);
	    ds.add(2621698, 7);
	} else if (i_13_ <= 10) {
	    ds.add(2621697, 10);
	    ds.add(2621698, 9);
	} else if (i_13_ <= 12) {
	    ds.add(2621697, 12);
	    ds.add(2621698, 11);
	} else {
	    ds.add(2621697, 16);
	    ds.add(2621698, 15);
	}
	if (ds.findInt(2621448) > 1)
	    checkType1(2621449, 1577059);
	checkType1(524296, DEFAULT_XA_IMAGE_TYPE);
	checkType1(2625600, "LIN");
    }
    
    private void modXRayAcquisition() {
	checkType2(1572960);
	checkType1(1577301, "SC");
	if (!ds.has(1577298)) {
	    checkType2(1577297);
	    checkType2(1577296);
	}
	if (ds.has(2621488) && !ds.has(1577316))
	    ds.add(1577316, ds.findValue(2621488));
    }
    
    private void modXAPositioner() {
	if (ds.findInt(2621448) > 1)
	    checkType2(1578240);
	checkType2(1578256);
	checkType2(1578257);
	if ("DYNAMIC".equals(ds.findString(1578240))) {
	    checkType2(1578272);
	    checkType2(1578273);
	}
    }
    
    private void modXRayTomoAcquisition() {
	checkType1(1578080, 0);
    }
    
    private void modRTSeries() {
	if (sopClass.equals("1.2.840.10008.5.1.4.1.1.481.1"))
	    force(524384, "RTIMAGE");
	if (!ds.has(2097166))
	    ds.add(2097166, UID.createUID(3));
	checkType2(2097169);
    }
    
    private void modRTImage() throws DicomException {
	if (ds.findInt(2621442) != 1)
	    throw new DicomException("ProtocoleViolation",
				     "RT images need to be 1 band");
	force(2621444, "MONOCHROME2");
	if (ds.findInt(2621699) != 0)
	    throw new DicomException("ProtocoleViolation",
				     "RT images need to be unsigned");
	int i = ds.findInt(2621696);
	if (i != 8 && i != 16)
	    throw new DicomException("ProtocoleViolation",
				     "RT images need to be 8 or 16 bits");
	int i_14_ = ds.findInt(2621697);
	if (i_14_ <= 8)
	    i_14_ = 8;
	else if (i_14_ <= 12)
	    i_14_ = 12;
	ds.add(2621697, i_14_);
	ds.add(2621698, i_14_ - 1);
	checkType1(805437442, "No label");
	checkType2(528496);
	checkType1(524296, DEFAULT_RT_IMAGE_TYPE);
	checkType2(524388);
	String[] strings
	    = ((String[])
	       ds.findValue(524296, String.class));
	if (strings.length > 2
	    && ("SIMULATOR".equals(strings[2]) || "PORTAL".equals(strings[2])))
	    checkType2(805437450);
	checkType1(805437452, "NORMAL");
	checkType2(805437454);
	if ("NON_NORMAL".equals(ds.findString(805437452)))
	    checkType2(805437456);
	checkType2(805437457, 2621488);
	checkType2(805437458);
	checkType2(805437472);
	checkType2(805961907);
	checkType2(805437474);
	checkType2(805437478);
	String[] strings_15_
	    = ((String[])
	       ds.findValue(524296, String.class));
	if (strings_15_ != null && strings_15_.length >= 3
	    && "FLUENCE".equals(strings_15_[2])) {
	    DataSet dataset = ds.findDataSet(805437504);
	    if (dataset == null || !dataset.has(805437505)) {
		new DataSet();
		dataset.add(805437505, "CALCULATED");
		coercion(805437504, dataset);
	    }
	}
    }
    
    private void modDXSeries(String string) {
	checkType1(524392, string);
    }
    
    private void modDXAnatomyImaged() {
	checkType1(2097250, "U");
	checkType2(533016);
    }
    
    private void modDXImage() throws DicomException {
	if (ds.findInt(2621442) != 1)
	    throw new DicomException("ProtocoleViolation",
				     "DX images need to be 1 band");
	String string = ds.findString(2621444);
	if (!"MONOCHROME2".equals(string) && !"MONOCHROME1".equals(string))
	    force(2621444, "MONOCHROME2");
	if (ds.findInt(2621699) != 0)
	    throw new DicomException("ProtocoleViolation",
				     "DX images need to be unsigned");
	int i = ds.findInt(2621696);
	if (i != 8 && i != 16)
	    throw new DicomException("ProtocoleViolation",
				     "DX images need to be 8 or 16 bits");
	int i_16_ = ds.findInt(2621697);
	if (i_16_ <= 8)
	    i_16_ = 8;
	else if (i_16_ <= 12)
	    i_16_ = 12;
	ds.add(2621697, i_16_);
	ds.add(2621698, i_16_ - 1);
	checkType1(524296, DEFAULT_DX_IMAGE_TYPE);
	checkType1(2625600, "LIN");
	checkType1(2625601, 1);
	force(2625618, 0);
	force(2625619, 1);
	force(2625620, "US");
	force(542113824, ("MONOCHROME1".equals(ds.findString(2621444))
			  ? "INVERSE" : "IDENTITY"));
	checkType1(2629904, "00");
	if ("01".equals(ds.findString(2629904)))
	    checkType1(2629906, 1);
	checkType1(2097184, DEFAULT_PATIENT_ORIENTATION);
	checkType1(2622209, "NO");
	if ("FOR PRESENTATION".equals(ds.findString(524392))
	    && !ds.has(2633744)) {
	    int i_17_ = ds.findInt(2621697);
	    i_17_ = 1 << i_17_;
	    checkType1(2625616, i_17_ / 2);
	    checkType1(2625617, i_17_);
	}
    }
    
    private void modDXDetector() {
	checkType2(1601540);
	if (ds.has(1601588))
	    checkType1(1601586, 0);
	else if (ds.has(1601586))
	    checkType1(1601588, "NO");
	if (ds.has(1601588))
	    checkType1(1601584, DEFAULT_FIELD_OF_VIEW_ORIGIN);
	checkType1(1577316, (ds.has(2621488) ? (Object) ds.findValue(2621488)
			     : DEFAULT_PIXEL_SPACING));
    }
    
    private void modAcquisitionContext() {
	checkType2(4195669);
    }
    
    private void modMammoSeries() {
	/* empty */
    }
    
    private void modMammoImage() {
	checkType1(1578248, "NONE");
	checkType1(2097250, "B");
	force(4195096, "BREAST");
	checkType2(533016);
	if (ds.findValue(5505568) == null) {
	    DataSet dataset = new DataSet();
	    dataset.add(524544, "R-10224");
	    dataset.add(524546, "SNM3");
	    dataset.add(524548, "medio-lateral");
	    dataset.add(5505570, (Object) null);
	    coercion(5505568, dataset);
	}
    }
    
    private void modIntraOralSeries() {
	/* empty */
    }
    
    private void modIntraOralImage() {
	checkType1(1578248, "NONE");
	checkType1(2097250, "B");
	checkType2(533016);
    }
    
    private void modVLImage() throws DicomException {
	int i = ds.findInt(2621442);
	switch (i) {
	case 1:
	    force(2621444, "MONOCHROME2");
	    break;
	case 3: {
	    String string = ds.findString(2621444);
	    if (!"YBR_FULL_422".equals(string) && !"RGB".equals(string)
		&& !"YBR_ICT".equals(string) && !"YBR_RCT".equals(string))
		force(2621444, "RGB");
	    ds.add(2621446, 0);
	    break;
	}
	default:
	    throw new DicomException("ProtocoleViolation",
				     "VL images need to be 1 band or 3 bands");
	}
	if (ds.findInt(2621696) != 8)
	    throw new DicomException("ProtocoleViolation",
				     "VL images need to be 8 bits");
	ds.add(2621697, 8);
	ds.add(2621698, 7);
	checkType1(524296, DEFAULT_VL_IMAGE_TYPE);
	checkType2(2629904);
	String[] strings
	    = ((String[])
	       ds.findValue(524296, String.class));
	if (strings.length > 2 && strings[2] != null
	    && strings[2].startsWith("STEREO") && !ds.has(528704)) {
	    strings[2] = "";
	    coercion(524296, strings);
	}
    }
    
    private void modSpecimenIdentification() {
	checkType1(4195594, "No accession number");
	checkType2(4195664);
    }
    
    private void modSlideCoordinates() {
	checkType2(4196122);
    }
    
    private void modPETSeries() {
	Day day
	    = (Day) ds.findValue(524320,Day.class);
	if (day == null)
	    day = new Day();
	Time time
	    = ((Time)
	       ds.findValue(524336,
			    Time.class));
	if (time == null)
	    time = new Time();
	checkType1(524321, day);
	checkType1(524337, time);
	checkType1(5509121, "CNTS");
	checkType1(5509122, "EMISSION");
	checkType1(5509120, DEFAULT_PT_SERIES_TYPE);
	String[] strings
	    = ((String[])
	       ds.findValue(5509120, String.class));
	if (strings != null && strings.length > 1
	    && "REPROJECTION".equals(strings[1]))
	    checkType2(5509124);
	if (strings != null && strings.length > 0) {
	    if ("GATED".equals(strings[0])) {
		checkType1(5505121, 600);
		checkType1(5505137, 16);
	    } else if ("DYNAMIC".equals(strings[0]))
		checkType1(5505281, 16);
	}
	checkType1(5505153, 64);
	checkType2(2621521);
	checkType1(5509378, "START");
	checkType2(1577345);
    }
    
    private void modPETIsotope() {
	checkType2(5505046);
    }
    
    private void modPETMultiGatedAcquisition() {
	checkType2(1577088);
    }
    
    private void modPETImage() throws DicomException {
	if (ds.findInt(2621442) != 1)
	    throw new DicomException("ProtocoleViolation",
				     "PET images must be 1 band");
	force(2621444, "MONOCHROME2");
	if (ds.findInt(2621696) != 16)
	    throw new DicomException("ProtocoleViolation",
				     "PET images must be 16 bits");
	ds.add(2621697, 16);
	ds.add(2621698, 15);
	checkType1(524296, DEFAULT_PT_IMAGETYPE);
	force(2625618, 0);
	checkType1(2625619, 1);
	checkType1(5509888, 0);
	String[] strings
	    = ((String[])
	       ds.findValue(5509120, String.class));
	if (strings != null && strings.length > 0
	    && "GATED".equals(strings[1])) {
	    checkType1(1577056, 0);
	    checkType1(1577059, new Float(62.5));
	    if ("Y".equals(ds.findString(1577088))) {
		checkType1(1577089, 200);
		checkType1(1577090, 2000);
	    }
	}
	checkType1(5509936, 1);
	checkType2(524322, 524323);
	checkType2(524338, 524339);
	checkType2(1577538);
	if (!"NONE".equals(ds.findString(5509378)))
	    checkType1(5509921, 0);
    }
    
    private void modHardcopyEquipment() {
	/* empty */
    }
    
    private void modHCGrayscaleImage() throws DicomException {
	modImagePixel();
	if (ds.findInt(2621442) != 1)
	    throw new DicomException("ProtocoleViolation",
				     "HC grayscale images must be 1 band");
	if (ds.findInt(2621699) != 0)
	    throw new DicomException("ProtocoleViolation",
				     "HC grayscale images must be unsigned");
	String string = ds.findString(2621444);
	if (!"MONOCHROME1".equals(string) && !"MONOCHROME2".equals(string))
	    force(2621444, "MONOCHROME2");
	int i = ds.findInt(2621696);
	if (i != 8 && i != 16)
	    throw new DicomException
		      ("ProtocoleViolation",
		       "HC grayscale images must be 8 or 16 bits");
	if (ds.findInt(2621697) <= 8) {
	    ds.add(2621697, 8);
	    ds.add(2621698, 7);
	} else {
	    ds.add(2621697, 12);
	    ds.add(2621698, 11);
	}
    }
    
    private void modHCColorImage() throws DicomException {
	modImagePixel();
	if (ds.findInt(2621442) != 3)
	    throw new DicomException("ProtocoleViolation",
				     "HC color images must be 3 bands");
	if (ds.findInt(2621699) != 0)
	    throw new DicomException("ProtocoleViolation",
				     "HC color images must be unsigned");
	force(2621444, "RGB");
	ds.add(2621446, 1);
	if (ds.findInt(2621696) != 8)
	    throw new DicomException("ProtocoleViolation",
				     "HC color images must be 8 bits");
	ds.add(2621697, 8);
	ds.add(2621698, 7);
    }
    
    private DataSet getShared() {
	Object object = ds.findValue(1375769129);
	if (object instanceof Collection && ((Collection) object).size() >= 1)
	    object = ((Collection) object).iterator().next();
	if (object instanceof DataSet)
	    return (DataSet) object;
	DataSet dataset = new DataSet();
	ds.add(1375769129, dataset);
	return dataset;
    }
    
    private Collection getPerFrames() {
	Object object = ds.findValue(1375769136);
	int i = getNumberOfFrames();
	if (object instanceof Collection && ((Collection) object).size() == i)
	    return (Collection) object;
	ArrayList arraylist = new ArrayList(i);
	for (int i_18_ = 0; i_18_ < i; i_18_++)
	    arraylist.add(new DataSet());
	ds.add(1375769136, arraylist);
	return arraylist;
    }
    
    private void modMultiframeFunctionalGroups(ColorModel[] colormodels)
	throws DicomException {
	DataSet dataset = getShared();
	Collection collection = getPerFrames();
	String[] strings
	    = ((String[])
	       ds.findValue(524296, String.class));
	if (sopClass.equals("1.2.840.10008.5.1.4.1.1.2.1"))
	    enhancedCTFunctionalGroupMacros(dataset, collection, strings,
					    colormodels);
	else
	    enhancedMRFunctionalGroupMacros(dataset, collection, strings,
					    colormodels);
	HashSet hashset = new HashSet();
	Iterator iterator = collection.iterator();
	while (iterator.hasNext()) {
	    DataSet dataset_19_ = (DataSet) iterator.next();
	    Iterator iterator_20_ = dataset_19_.keySet().iterator();
	    while (iterator_20_.hasNext())
		hashset.add(iterator_20_.next());
	}
	iterator = hashset.iterator();
	while (iterator.hasNext()) {
	    Integer integer = (Integer) iterator.next();
	    if (integer.intValue() == 2134289)
		dataset.remove(integer);
	    else {
		Iterator iterator_21_ = collection.iterator();
		DataSet dataset_22_ = (DataSet) iterator_21_.next();
		Object object = dataset_22_.get(integer);
		if (object != null) {
		    boolean bool = true;
		    while (iterator_21_.hasNext()) {
			DataSet dataset_23_ = (DataSet) iterator_21_.next();
			if (!object.equals(dataset_23_.get(integer))) {
			    bool = false;
			    break;
			}
		    }
		    if (bool) {
			dataset.put(integer, object);
			iterator_21_ = collection.iterator();
			while (iterator_21_.hasNext())
			    ((DataSet) iterator_21_.next()).remove(integer);
		    } else
			dataset.remove(integer);
		}
	    }
	}
	checkType1(2097171, 1);
	Date date = new Date();
	checkType1(524323, date);
	checkType1(524339, date);
	ds.add(2621448, getNumberOfFrames());
	if (ds.has(2134369)) {
	    if (!ds.has(2134568))
		throw new DicomException("MissingMandatoryAttribute",
					 "ConcatenationFrameOffsetNumber");
	    if (!ds.has(2134370))
		throw new DicomException("MissingMandatoryAttribute",
					 "InConcatenationNumber");
	}
    }
    
    private void modCTSeries() {
	/* empty */
    }
    
    void enhancedCTFunctionalGroupMacros
	(DataSet dataset, Collection collection, String[] strings,
	 ColorModel[] colormodels) {
	int i = 0;
	Iterator iterator = collection.iterator();
	while (iterator.hasNext())
	    CTImageFrameTypeMacro(dataset, (DataSet) iterator.next(),
				  colormodels[i++]);
	iterator = collection.iterator();
	while (iterator.hasNext()) {
	    DataSet dataset_24_ = (DataSet) iterator.next();
	    pixelMeasuresMacro(dataset, dataset_24_, 1610537);
	    frameContentMacro(dataset, dataset_24_, 1610537);
	    planePositionMacro(dataset, dataset_24_, 1610537);
	    planeOrientationMacro(dataset, dataset_24_, 1610537);
	    frameAnatomyMacro(dataset, dataset_24_);
	    CTPixelValueTransformationMacro(dataset, dataset_24_);
	    if ("ORIGINAL".equals(strings[0]) || "MIXED".equals(strings[0])) {
		String string = ds.findString(1609783);
		if (string != null && !"NONE".equals(string))
		    cardiacTriggerMacro(dataset, dataset_24_);
		CTAcquisitionTypeMacro(dataset, dataset_24_);
		CTAcquisitionDetailsMacro(dataset, dataset_24_);
		CTTableDynamicsMacro(dataset, dataset_24_);
		CTPositionMacro(dataset, dataset_24_);
		CTGeometryMacro(dataset, dataset_24_);
		if (!"CONSTANT_ANGLE".equals(findStringInMacro(dataset,
							       dataset_24_,
							       1610497,
							       1610498)))
		    CTReconstructionMacro(dataset, dataset_24_);
		CTExposureMacro(dataset, dataset_24_);
		CTXRayDetailsMacro(dataset, dataset_24_);
	    }
	    im++;
	}
    }
    
    void enhancedMRFunctionalGroupMacros
	(DataSet dataset, Collection collection, String[] strings,
	 ColorModel[] colormodels) {
	int i = 0;
	Iterator iterator = collection.iterator();
	while (iterator.hasNext())
	    MRImageFrameTypeMacro(dataset, (DataSet) iterator.next(),
				  colormodels[i++]);
	iterator = collection.iterator();
	while (iterator.hasNext()) {
	    DataSet dataset_25_ = (DataSet) iterator.next();
	    pixelMeasuresMacro(dataset, dataset_25_, 1610278);
	    frameContentMacro(dataset, dataset_25_, 1610278);
	    planePositionMacro(dataset, dataset_25_, 1610278);
	    planeOrientationMacro(dataset, dataset_25_, 1610278);
	    frameAnatomyMacro(dataset, dataset_25_);
	    pixelValueTransformationMacro(dataset, dataset_25_);
	    if ("ORIGINAL".equals(strings[0]) || "MIXED".equals(strings[0])) {
		String string = ds.findString(1609783);
		if (string != null && !"NONE".equals(string))
		    cardiacTriggerMacro(dataset, dataset_25_);
		MRTimingAndRelatedParametersMacro(dataset, dataset_25_);
		if ("RECTILINEAR".equals(ds.findString(1609778)))
		    MRFOVGeometryMacro(dataset, dataset_25_);
		MREchoMacro(dataset, dataset_25_);
		MRModifierMacro(dataset, dataset_25_);
		MRImagingModifierMacro(dataset, dataset_25_);
		MRReceiveCoilMacro(dataset, dataset_25_);
		MRTransmitCoilMacro(dataset, dataset_25_);
		MRAveragesMacro(dataset, dataset_25_);
		if ("YES".equals(ds.findString(1609748)))
		    MRVelocityEncodingMacro(dataset, dataset_25_);
	    }
	    if ("METABOLITE_MAP".equals(strings[2]))
		MRMetaboliteMapMacro(dataset, dataset_25_);
	    im++;
	}
	im = 0;
	if ("ORIGINAL".equals(strings[0]) || "MIXED".equals(strings[0])) {
	    iterator = collection.iterator();
	    while (iterator.hasNext()) {
		DataSet dataset_26_ = (DataSet) iterator.next();
		if ("DIFFUSION".equals(findStringInMacro(null, dataset_26_,
							 1610278, 561673))) {
		    Iterator iterator_27_ = collection.iterator();
		    while (iterator_27_.hasNext())
			MRDiffusionMacro(dataset,
					 (DataSet) iterator_27_.next());
		    break;
		}
		im++;
	    }
	    im = 0;
	    iterator = collection.iterator();
	    while (iterator.hasNext()) {
		DataSet dataset_28_ = (DataSet) iterator.next();
		if ("SLAB".equals(findStringInMacro(null, dataset_28_, 1610005,
						    1609767))) {
		    Iterator iterator_29_ = collection.iterator();
		    while (iterator_29_.hasNext())
			MRSpatialSaturationMacro(dataset,
						 ((DataSet)
						  iterator_29_.next()));
		    break;
		}
		im++;
	    }
	    im = 0;
	}
    }
    
    private void checkType1Macro(DataSet dataset, DataSet dataset_30_, int i) {
	if (DicomObject.findMacro(i, dataset, dataset_30_) == null)
	    dataset_30_.add(i, new DataSet());
    }
    
    private void checkType1InMacro(DataSet dataset, DataSet dataset_31_, int i,
				   int i_32_, Object object) {
	DataSet dataset_33_ = dataset_31_.findDataSet(i);
	if (dataset_33_ != null) {
	    if (dataset_33_.findValue(i_32_) == null)
		coercion(dataset_33_, i_32_, object);
	} else {
	    DataSet dataset_34_ = dataset.findDataSet(i);
	    if (dataset_34_ == null)
		dataset_31_.add(i, dataset_33_ = new DataSet());
	    else {
		if (dataset_34_.findValue(i_32_) != null)
		    return;
		dataset_31_.add(i, (dataset_33_
				    = ((DataSet)
				       Cloner.cloneElement(dataset_34_))));
	    }
	    coercion(dataset_33_, i_32_, object);
	}
    }
    
    private void checkType2InMacro(DataSet dataset, DataSet dataset_35_, int i,
				   int i_36_) {
	DataSet dataset_37_ = dataset_35_.findDataSet(i);
	if (dataset_37_ != null) {
	    if (!dataset_37_.has(i_36_))
		dataset_37_.add(i_36_, (Object) null);
	} else {
	    DataSet dataset_38_ = dataset.findDataSet(i);
	    if (dataset_38_ == null)
		dataset_35_.add(i, dataset_37_ = new DataSet());
	    else {
		if (dataset_38_.has(i_36_))
		    return;
		dataset_35_.add(i, (dataset_37_
				    = ((DataSet)
				       Cloner.cloneElement(dataset_38_))));
	    }
	    dataset_37_.add(i_36_, (Object) null);
	}
    }
    
    private void checkType1InMacro(DataSet dataset, DataSet dataset_39_, int i,
				   int i_40_, int i_41_) {
	checkType1InMacro(dataset, dataset_39_, i, i_40_, new Integer(i_41_));
    }
    
    private void checkType1InMacro(DataSet dataset, DataSet dataset_42_, int i,
				   int i_43_, float f) {
	checkType1InMacro(dataset, dataset_42_, i, i_43_, new Float(f));
    }
    
    private void checkType1InMacro(DataSet dataset, DataSet dataset_44_, int i,
				   int i_45_, double d) {
	checkType1InMacro(dataset, dataset_44_, i, i_45_, new Double(d));
    }
    
    private Object findValueInMacro(DataSet dataset, DataSet dataset_46_,
				    int i, int i_47_, Class var_class) {
	DataSet dataset_48_ = DicomObject.findMacro(i, dataset, dataset_46_);
	return (dataset_48_ == null ? ds.findValue(i_47_, var_class)
		: dataset_48_.findValue(i_47_, var_class));
    }
    
    private String findStringInMacro(DataSet dataset, DataSet dataset_49_,
				     int i, int i_50_) {
	DataSet dataset_51_ = DicomObject.findMacro(i, dataset, dataset_49_);
	return (dataset_51_ == null ? ds.findString(i_50_)
		: dataset_51_.findString(i_50_));
    }
    
    private String getFrameTypeValue1(DataSet dataset, DataSet dataset_52_,
				      int i) {
	String[] strings
	    = (String[]) findValueInMacro(dataset, dataset_52_, i, 561159,
					  String.class);
	return strings[0];
    }
    
    private String getMRFrameTypeValue1(DataSet dataset, DataSet dataset_53_) {
	String[] strings
	    = ((String[])
	       findValueInMacro(dataset, dataset_53_, 1610278, 561159,
				String.class));
	return strings[0];
    }
    
    private String getCTFrameTypeValue1(DataSet dataset, DataSet dataset_54_) {
	String[] strings
	    = ((String[])
	       findValueInMacro(dataset, dataset_54_, 1610537, 561159,
				String.class));
	return strings[0];
    }
    
    private void pixelMeasuresMacro(DataSet dataset, DataSet dataset_55_,
				    int i) {
	checkType1Macro(dataset, dataset_55_, 2658576);
	String string = findStringInMacro(dataset, dataset_55_, i, 561670);
	if (!"DISTORTED".equals(string) && !"SAMPLED".equals(string))
	    checkType1InMacro(dataset, dataset_55_, 2658576, 2621488,
			      DEFAULT_PIXEL_SPACING);
	if ("VOLUME".equals(string) || "SAMPLED".equals(string))
	    checkType1InMacro(dataset, dataset_55_, 2658576, 1572944, 1);
    }
    
    private void frameContentMacro(DataSet dataset, DataSet dataset_56_,
				   int i) {
	checkType1Macro(null, dataset_56_, 2134289);
	if ("ORIGINAL".equals(getFrameTypeValue1(dataset, dataset_56_, i))) {
	    Date date = new Date();
	    checkType1InMacro(null, dataset_56_, 2134289, 1610065, date);
	    checkType1InMacro(null, dataset_56_, 2134289, 1609844, date);
	    checkType1InMacro(null, dataset_56_, 2134289, 1610272, 0);
	}
    }
    
    private void planePositionMacro(DataSet dataset, DataSet dataset_57_,
				    int i) {
	checkType1Macro(dataset, dataset_57_, 2134291);
	String string = findStringInMacro(dataset, dataset_57_, i, 561670);
	if ("ORIGINAL".equals(getFrameTypeValue1(dataset, dataset_57_, i))
	    && !"DISTORTED".equals(string))
	    checkType1InMacro(dataset, dataset_57_, 2134291, 2097202,
			      DEFAULT_IMAGE_POSITION_PATIENT);
    }
    
    private void planeOrientationMacro(DataSet dataset, DataSet dataset_58_,
				       int i) {
	checkType1Macro(dataset, dataset_58_, 2134294);
	String string = findStringInMacro(dataset, dataset_58_, i, 561670);
	if ("ORIGINAL".equals(getFrameTypeValue1(dataset, dataset_58_, i))
	    && !"DISTORTED".equals(string))
	    checkType1InMacro(dataset, dataset_58_, 2134294, 2097207,
			      DEFAULT_IMAGE_ORIENTATION_PATIENT);
    }
    
    private void cardiacTriggerMacro(DataSet dataset, DataSet dataset_59_) {
	checkType1InMacro(dataset, dataset_59_, 1610008, 2134355, 0);
    }
    
    private void frameAnatomyMacro(DataSet dataset, DataSet dataset_60_) {
	checkType1InMacro(dataset, dataset_60_, 2134129, 2134130, "U");
	checkType2InMacro(dataset, dataset_60_, 2134129, 533016);
    }
    
    private void pixelValueTransformationMacro(DataSet dataset,
					       DataSet dataset_61_) {
	checkType1InMacro(dataset, dataset_61_, 2658629, 2625618, 0.0F);
	checkType1InMacro(dataset, dataset_61_, 2658629, 2625619, 1.0F);
	checkType1InMacro(dataset, dataset_61_, 2658629, 2625620, "US");
    }
    
    private void commonCTMRImageDescriptionMacro
	(DataSet dataset, DataSet dataset_62_, int i, ColorModel colormodel) {
	checkType1InMacro(dataset, dataset_62_, i, 561669,
			  (colormodel instanceof IndexColorModel ? "COLOR"
			   : "MONOCHROME"));
	checkType1InMacro(dataset, dataset_62_, i, 561670, "VOLUME");
	checkType1InMacro(dataset, dataset_62_, i, 561671, "NONE");
    }
    
    private void MRImageFrameTypeMacro(DataSet dataset, DataSet dataset_63_,
				       ColorModel colormodel) {
	checkType1InMacro(dataset, dataset_63_, 1610278, 561159,
			  DEFAULT_ENHMR_IMAGE_TYPE);
	commonCTMRImageDescriptionMacro(dataset, dataset_63_, 1610278,
					colormodel);
	checkType1InMacro(dataset, dataset_63_, 1610278, 561672, "MAGNITUDE");
	checkType1InMacro(dataset, dataset_63_, 1610278, 561673, "UNKNOWN");
    }
    
    private void MRTimingAndRelatedParametersMacro(DataSet dataset,
						   DataSet dataset_64_) {
	checkType1Macro(dataset, dataset_64_, 1610002);
	if ("ORIGINAL".equals(getMRFrameTypeValue1(dataset, dataset_64_))) {
	    checkType1InMacro(dataset, dataset_64_, 1610002, 1572992, 0);
	    checkType1InMacro(dataset, dataset_64_, 1610002, 1577748, 0);
	    checkType1InMacro(dataset, dataset_64_, 1610002, 1573009, 0);
	    checkType1InMacro(dataset, dataset_64_, 1610002, 1610304, 0);
	    checkType1InMacro(dataset, dataset_64_, 1610002, 1610305, 0);
	}
    }
    
    private void MRFOVGeometryMacro(DataSet dataset, DataSet dataset_65_) {
	checkType1Macro(dataset, dataset_65_, 1610021);
	if ("ORIGINAL".equals(getMRFrameTypeValue1(dataset, dataset_65_))) {
	    checkType1InMacro(dataset, dataset_65_, 1610021, 1577746, "OTHER");
	    checkType1InMacro(dataset, dataset_65_, 1610021, 1609816, 0);
	    checkType1InMacro(dataset, dataset_65_, 1610021, 1610289, 0);
	    checkType1InMacro(dataset, dataset_65_, 1610021, 1610289, 0);
	    if ("3D".equals(ds.findString(1572899)))
		checkType1InMacro(dataset, dataset_65_, 1610021, 1610290, 0);
	    checkType1InMacro(dataset, dataset_65_, 1610021, 1573011, 100);
	    checkType1InMacro(dataset, dataset_65_, 1610021, 1573012, 100);
	}
    }
    
    private void MREchoMacro(DataSet dataset, DataSet dataset_66_) {
	checkType1Macro(dataset, dataset_66_, 1610004);
	if ("ORIGINAL".equals(getMRFrameTypeValue1(dataset, dataset_66_)))
	    checkType1InMacro(dataset, dataset_66_, 1610004, 1609858, 0);
    }
    
    private void MRModifierMacro(DataSet dataset, DataSet dataset_67_) {
	checkType1Macro(dataset, dataset_67_, 1610005);
	if ("ORIGINAL".equals(getMRFrameTypeValue1(dataset, dataset_67_))) {
	    checkType1InMacro(dataset, dataset_67_, 1610005, 1609737, "NO");
	    if ("YES".equals(findStringInMacro(dataset, dataset_67_, 1610005,
					       1609737)))
		checkType1InMacro(dataset, dataset_67_, 1610005, 1609849, 0);
	    checkType1InMacro(dataset, dataset_67_, 1610005, 1609744, "NONE");
	    if (!"NONE".equals(findStringInMacro(dataset, dataset_67_, 1610005,
						 1609744)))
		checkType1InMacro(dataset, dataset_67_, 1610005, 1610115,
				  "OTHER");
	    String string = ds.findString(1609736);
	    if ("GRADIENT".equals(string) || "BOTH".equals(string))
		checkType1InMacro(dataset, dataset_67_, 1610005, 1609750,
				  "NONE");
	    checkType1InMacro(dataset, dataset_67_, 1610005, 1609761, "NO");
	    checkType1InMacro(dataset, dataset_67_, 1610005, 1609766, "NONE");
	    checkType1InMacro(dataset, dataset_67_, 1610005, 1609767, "NONE");
	    checkType1InMacro(dataset, dataset_67_, 1610005, 1609857, "NO");
	    if ("YES".equals(findStringInMacro(dataset, dataset_67_, 1610005,
					       1609857)))
		checkType1InMacro(dataset, dataset_67_, 1610005, 1609782,
				  "PHASE");
	    checkType1InMacro(dataset, dataset_67_, 1610005, 1609847, "NO");
	    if ("YES".equals(findStringInMacro(dataset, dataset_67_, 1610005,
					       1609847))) {
		checkType1InMacro(dataset, dataset_67_, 1610005, 1609848,
				  "OTHER");
		checkType1InMacro(dataset, dataset_67_, 1610005, 1609833, 1);
		checkType1InMacro(dataset, dataset_67_, 1610005, 1610069, 1);
	    }
	}
    }
    
    private void MRImagingModifierMacro(DataSet dataset, DataSet dataset_68_) {
	checkType1Macro(dataset, dataset_68_, 1609734);
	if ("ORIGINAL".equals(getMRFrameTypeValue1(dataset, dataset_68_))) {
	    checkType1InMacro(dataset, dataset_68_, 1609734, 1609760, "NONE");
	    checkType1InMacro(dataset, dataset_68_, 1609734, 1609762, "NO");
	    checkType1InMacro(dataset, dataset_68_, 1609734, 1609768, "NONE");
	    String string
		= findStringInMacro(dataset, dataset_68_, 1609734, 1609768);
	    if ("GRID".equals(string) || "LINE".equals(string)) {
		checkType1InMacro(dataset, dataset_68_, 1609734, 1609776, 0);
		checkType1InMacro(dataset, dataset_68_, 1609734, 1609753, 0);
		if ("GRID".equals(string)) {
		    checkType1InMacro(dataset, dataset_68_, 1609734, 1610264,
				      0);
		    checkType1InMacro(dataset, dataset_68_, 1609734, 1610265,
				      0);
		}
		checkType1InMacro(dataset, dataset_68_, 1609734, 1609781, 0);
	    }
	    checkType1InMacro(dataset, dataset_68_, 1609734, 1609880, 0);
	    checkType1InMacro(dataset, dataset_68_, 1609734, 1573013, 0);
	}
    }
    
    private void MRReceiveCoilMacro(DataSet dataset, DataSet dataset_69_) {
	checkType1Macro(dataset, dataset_69_, 1609794);
	if ("ORIGINAL".equals(getMRFrameTypeValue1(dataset, dataset_69_))) {
	    checkType1InMacro(dataset, dataset_69_, 1609794, 1577552,
			      "Unknown");
	    checkType2InMacro(dataset, dataset_69_, 1609794, 1609793);
	    checkType1InMacro(dataset, dataset_69_, 1609794, 1609795, "BODY");
	    checkType1InMacro(dataset, dataset_69_, 1609794, 1609796, "NO");
	    if ("MULTICOIL".equals(findStringInMacro(dataset, dataset_69_,
						     1609794, 1609795))) {
		DataSet dataset_70_ = new DataSet();
		dataset_70_.add(1609799, "Unknown");
		dataset_70_.add(1609800, "NO");
		checkType1InMacro(dataset, dataset_69_, 1609794, 1609797,
				  dataset_70_);
	    }
	}
    }
    
    private void MRTransmitCoilMacro(DataSet dataset, DataSet dataset_71_) {
	checkType1Macro(dataset, dataset_71_, 1609801);
	if ("ORIGINAL".equals(getMRFrameTypeValue1(dataset, dataset_71_))) {
	    checkType1InMacro(dataset, dataset_71_, 1609801, 1577553,
			      "Unknown");
	    checkType2InMacro(dataset, dataset_71_, 1609801, 1609808);
	    checkType1InMacro(dataset, dataset_71_, 1609801, 1609809, "BODY");
	}
    }
    
    private void MRDiffusionMacro(DataSet dataset, DataSet dataset_72_) {
	checkType1Macro(dataset, dataset_72_, 1610007);
	String[] strings
	    = ((String[])
	       findValueInMacro(dataset, dataset_72_, 1610278, 561159,
				String.class));
	if ("ORIGINAL".equals(strings[0])) {
	    checkType1InMacro(dataset, dataset_72_, 1610007, 1609863, 0);
	    checkType1InMacro(dataset, dataset_72_, 1610007, 1609845, "NONE");
	}
	if ("DIRECTIONAL".equals(findStringInMacro(dataset, dataset_72_,
						   1610007, 1609845)))
	    checkType1InMacro(dataset, dataset_72_, 1610007, 1609846,
			      new DataSet());
	if ("DIFFUSION_ANISO".equals(strings[3]))
	    checkType1InMacro(dataset, dataset_72_, 1610007, 1610055,
			      "FRACTIONAL");
    }
    
    private void MRAveragesMacro(DataSet dataset, DataSet dataset_73_) {
	checkType1Macro(dataset, dataset_73_, 1610009);
	if ("ORIGINAL".equals(getMRFrameTypeValue1(dataset, dataset_73_)))
	    checkType1InMacro(dataset, dataset_73_, 1610009, 1572995, 1);
    }
    
    private void MRSpatialSaturationMacro(DataSet dataset,
					  DataSet dataset_74_) {
	checkType1Macro(dataset, dataset_74_, 1609991);
    }
    
    private void MRMetaboliteMapMacro(DataSet dataset, DataSet dataset_75_) {
	checkType1Macro(dataset, dataset_75_, 1610066);
	if ("ORIGINAL".equals(getMRFrameTypeValue1(dataset, dataset_75_)))
	    checkType1InMacro(dataset, dataset_75_, 1610066, 1609856,
			      "Unknown");
    }
    
    private void MRVelocityEncodingMacro(DataSet dataset,
					 DataSet dataset_76_) {
	checkType1Macro(dataset, dataset_76_, 1610278);
	if ("ORIGINAL".equals(getMRFrameTypeValue1(dataset, dataset_76_))) {
	    checkType1InMacro(dataset, dataset_76_, 1610135, 1609872, 0);
	    checkType1InMacro(dataset, dataset_76_, 1610135, 1609873, 0);
	    checkType1InMacro(dataset, dataset_76_, 1610135, 1610263, 0);
	}
    }
    
    private void modMRPulseSeq() {
	String[] strings
	    = ((String[])
	       ds.findValue(524296, String.class));
	if ("ORIGINAL".equals(strings[0]) || "MIXED".equals(strings[0])) {
	    checkType1(1609733, "Unknown");
	    checkType1(1572899, "2D");
	    checkType1(1609736, "SPIN");
	    String string = ds.findString(1609736);
	    if ("SPIN".equals(string) || "BOTH".equals(string))
		checkType1(1609745, "NO");
	    checkType1(1609746, "NO");
	    checkType1(1609748, "NO");
	    checkType1(1609749, "NO");
	    checkType1(1609751, "NONE");
	    checkType1(1609752, "NO");
	    checkType1(1609764, "NO");
	    checkType1(1609765, "NONE");
	    checkType1(1609769, "NONE");
	    checkType1(1609778, "RECTILINEAR");
	    if ("RECTILINEAR".equals(ds.findString(1609778)))
		checkType1(1609780, "LINEAR");
	    checkType1(1609779, "FULL");
	    if ("3D".equals(ds.findString(1572899)))
		checkType1(1609876, "FULL");
	    checkType1(1609875, 0);
	}
    }
    
    private void modMultiframeDimension() {
	checkType2(2134561);
	checkType2(2134562);
    }
    
    private void CTImageFrameTypeMacro(DataSet dataset, DataSet dataset_77_,
				       ColorModel colormodel) {
	checkType1InMacro(dataset, dataset_77_, 1610537, 561159,
			  DEFAULT_ENHCT_IMAGE_TYPE);
	commonCTMRImageDescriptionMacro(dataset, dataset_77_, 1610537,
					colormodel);
    }
    
    private void CTAcquisitionTypeMacro(DataSet dataset, DataSet dataset_78_) {
	if ("ORIGINAL".equals(getCTFrameTypeValue1(dataset, dataset_78_))) {
	    checkType1InMacro(dataset, dataset_78_, 1610497, 1610498,
			      "SEQUENCED");
	    if ("CONSTANT_ANGLE".equals(findStringInMacro(dataset, dataset_78_,
							  1610497, 1610498)))
		checkType1InMacro(dataset, dataset_78_, 1610497, 1610499, 0);
	    checkType1InMacro(dataset, dataset_78_, 1610497, 1610547, "NO");
	    checkType1InMacro(dataset, dataset_78_, 1610497, 1610548, "NO");
	}
    }
    
    private void CTAcquisitionDetailsMacro(DataSet dataset,
					   DataSet dataset_79_) {
	if ("ORIGINAL".equals(getCTFrameTypeValue1(dataset, dataset_79_))) {
	    if (!"CONSTANT_ANGLE".equals(findStringInMacro(dataset,
							   dataset_79_,
							   1610497,
							   1610498))) {
		checkType1InMacro(dataset, dataset_79_, 1610500, 1577280,
				  "CW");
		checkType1InMacro(dataset, dataset_79_, 1610500, 1610501, 0);
	    }
	    checkType1InMacro(dataset, dataset_79_, 1610500, 1610502, 0);
	    checkType1InMacro(dataset, dataset_79_, 1610500, 1610503, 0);
	    checkType1InMacro(dataset, dataset_79_, 1610500, 1577264, 0);
	    checkType1InMacro(dataset, dataset_79_, 1610500, 1577248, 0);
	    checkType1InMacro(dataset, dataset_79_, 1610500, 1573008, 0);
	}
    }
    
    private void CTTableDynamicsMacro(DataSet dataset, DataSet dataset_80_) {
	if ("ORIGINAL".equals(getCTFrameTypeValue1(dataset, dataset_80_))) {
	    String string
		= findStringInMacro(dataset, dataset_80_, 1610497, 1610498);
	    if ("SPIRAL".equals(string) || "CONSTANT_ANGLE".equals(string))
		checkType1InMacro(dataset, dataset_80_, 1610504, 1610505, 0);
	    if ("SPIRAL".equals(string)) {
		checkType1InMacro(dataset, dataset_80_, 1610504, 1610512, 0);
		checkType1InMacro(dataset, dataset_80_, 1610504, 1610513, 0);
	    }
	}
    }
    
    private void CTPositionMacro(DataSet dataset, DataSet dataset_81_) {
	if ("ORIGINAL".equals(getCTFrameTypeValue1(dataset, dataset_81_))) {
	    checkType1InMacro(dataset, dataset_81_, 1610534, 1610535, 0);
	    checkType1InMacro(dataset, dataset_81_, 1610534, 1610515,
			      new double[] { 0.0, 0.0, 0.0 });
	    checkType1InMacro(dataset, dataset_81_, 1610534, 1610520,
			      new double[] { 0.0, 0.0, 0.0 });
	}
    }
    
    private void CTGeometryMacro(DataSet dataset, DataSet dataset_82_) {
	if ("ORIGINAL".equals(getCTFrameTypeValue1(dataset, dataset_82_))) {
	    checkType1InMacro(dataset, dataset_82_, 1610514, 1577232, 0);
	    checkType1InMacro(dataset, dataset_82_, 1610514, 1610549, 0);
	}
    }
    
    private void CTReconstructionMacro(DataSet dataset, DataSet dataset_83_) {
	if ("ORIGINAL".equals(getCTFrameTypeValue1(dataset, dataset_83_))) {
	    checkType1InMacro(dataset, dataset_83_, 1610516, 1610517,
			      "FILTER_BACK_PROJ");
	    checkType1InMacro(dataset, dataset_83_, 1610516, 1577488,
			      "Unknown");
	    checkType1InMacro(dataset, dataset_83_, 1610516, 1610518,
			      "CONSTANT_ANGLE");
	    if (findValueInMacro(dataset, dataset_83_, 1610516, 1610519,
				 Object.class)
		== null)
		checkType1InMacro(dataset, dataset_83_, 1610516, 1577216, "0");
	    checkType1InMacro(dataset, dataset_83_, 1610516, 1610530,
			      new double[] { 0.0, 0.0 });
	    checkType1InMacro(dataset, dataset_83_, 1610516, 1610521, 360);
	    checkType1InMacro(dataset, dataset_83_, 1610516, 1610528,
			      "Unknown");
	}
    }
    
    private void CTExposureMacro(DataSet dataset, DataSet dataset_84_) {
	if ("ORIGINAL".equals(getCTFrameTypeValue1(dataset, dataset_84_))) {
	    checkType1InMacro(dataset, dataset_84_, 1610529, 1610536, 0);
	    checkType1InMacro(dataset, dataset_84_, 1610529, 1610544, 0);
	    checkType1InMacro(dataset, dataset_84_, 1610529, 1610546, 0);
	    checkType1InMacro(dataset, dataset_84_, 1610529, 1610531, "NONE");
	    if (!"NONE".equals(findStringInMacro(dataset, dataset_84_, 1610529,
						 1610531)))
		checkType2InMacro(dataset, dataset_84_, 1610529, 1610532);
	}
	checkType2InMacro(dataset, dataset_84_, 1610529, 1610565);
    }
    
    private void CTXRayDetailsMacro(DataSet dataset, DataSet dataset_85_) {
	if ("ORIGINAL".equals(getCTFrameTypeValue1(dataset, dataset_85_))) {
	    checkType1InMacro(dataset, dataset_85_, 1610533, 1572960, 0);
	    checkType1InMacro(dataset, dataset_85_, 1610533, 1577360, 0);
	    checkType1InMacro(dataset, dataset_85_, 1610533, 1577312, "NONE");
	    checkType1InMacro(dataset, dataset_85_, 1610533, 1601616, "LEAD");
	}
    }
    
    private void CTPixelValueTransformationMacro(DataSet dataset,
						 DataSet dataset_86_) {
	checkType1InMacro(dataset, dataset_86_, 2658629, 2625618, 0);
	checkType1InMacro(dataset, dataset_86_, 2658629, 2625619, 1.0);
	checkType1InMacro(dataset, dataset_86_, 2658629, 2625620, "HU");
    }
    
    private void modOphthalmicPhotographySeries() {
	/* empty */
    }
    
    private void modOphthalmicPhotographyImage(int i) throws DicomException {
	int i_87_ = ds.findInt(2621442);
	switch (i_87_) {
	case 1:
	    force(2621444, "MONOCHROME2");
	    checkType1(542113824, "IDENTITY");
	    break;
	case 3: {
	    String string = ds.findString(2621444);
	    if (!"RGB".equals(string) && !"YBR_FULL_422".equals(string)
		&& !"YBR_PARTIAL_420".equals(string)
		&& !"YBR_ICT".equals(string) && !"YBR_RCT".equals(string))
		force(2621444, "RGB");
	    ds.add(2621446, 0);
	    break;
	}
	default:
	    throw new DicomException("ProtocoleViolation",
				     "OP images need to be 1 band or 3 bands");
	}
	if (ds.findInt(2621696) != i)
	    throw new DicomException("ProtocoleViolation",
				     (i + "-bit OP images must be " + i
				      + " bit"));
	ds.add(2621697, i);
	ds.add(2621698, i - 1);
	if (ds.findInt(2621699) != 0)
	    throw new DicomException("ProtocoleViolation",
				     "OP images need to be unsigned");
	checkType1(524296, DEFAULT_OP_IMAGETYPE);
	if (codeSequenceHasItem(2228245, "SRT", "R-1021A"))
	    checkType1(2621488, DEFAULT_PIXEL_SPACING);
	Date date = new Date();
	checkType1(524323, date);
	checkType1(524339, date);
	String[] strings
	    = ((String[])
	       ds.findValue(524296, String.class));
	if (strings.length > 0 && "ORIGINAL".equals(strings[0]))
	    checkType1(524330, date);
	if (strings.length > 0 && "DERIVED".equals(strings[0]))
	    checkType2(532754);
	checkType1(2629904, "00");
	if ("01".equals(ds.findString(2629904))) {
	    checkType1(2629906, 1);
	    checkType1(2629908, "ISO_10918_1");
	}
	checkType1(2622209, "NO");
    }
    
    private void modOphthalmicPhotographicParameters() {
	if (ds.findValue(2228245) == null) {
	    DataSet dataset = new DataSet();
	    dataset.add(524544, "R-1021B");
	    dataset.add(524546, "SRT");
	    dataset.add(524548, "External Camera");
	    coercion(2228245, dataset);
	}
	checkType2(2228246);
	checkType2(2228247);
	checkType2(2228248);
	checkType2(2228249);
	checkType2(1601540);
    }
    
    private void modOphthalmicPhotographyAcquisitionParameters()
	throws DicomException {
	checkType2(2228229);
	if ("YES".equals(ds.findString(2228229)) && !ds.has(2228230))
	    throw new DicomException("MissingMandatoryAttribute",
				     "PatientEyeMovementCommandCodeSequence");
	checkType2(2228251);
	checkType2(2228234);
	checkType2(2228235);
	checkType2(2228236);
	checkType2(2228237);
	if ("YES".equals(ds.findString(2228237))) {
	    checkType2(2228252);
	    checkType2(2228238);
	}
    }
    
    private void modOcularRegionImaged() {
	checkType1(2097250, "B");
    }
    
    private void modSoftCopyPresentationLUT() {
	if (!ds.has(542113808))
	    checkType1(542113824, "IDENTITY");
    }
    
}
