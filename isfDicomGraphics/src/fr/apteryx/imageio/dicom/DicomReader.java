// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: fieldsfirst debugmode
//Source File Name: DicomReader.java
//Class Version: 48.0

package fr.apteryx.imageio.dicom;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.color.ICC_ColorSpace;
import java.awt.font.GlyphVector;
import java.awt.image.BandedSampleModel;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.PixelInterleavedSampleModel;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.event.IIOReadProgressListener;
import javax.imageio.event.IIOReadUpdateListener;
import javax.imageio.event.IIOReadWarningListener;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

//Referenced classes of package fr.apteryx.imageio.dicom:
//         ReceivedObject, DicomObject, StorageSCP, PixelData,
//         DicomMetadata, Warnings, DataSet, DicomWarningListener,
//         DicomException, FragmentConcatenerImageInputStream, VOIDefaultColorModel,
// RLEReader,
//         TransferSyntax, Util, DicomColorSpace, Operations,
//         RawReader

//flag ACC_SUPER is set
public class DicomReader extends ImageReader {
    // Constants: 356
    // Interfaces: 0
    // Fields: 9
    // Methods: 36
    // Class Attributes: 2

    private class ImageInfo {
        // Constants: 546
        // Interfaces: 0
        // Fields: 16
        // Methods: 3
        // Class Attributes: 2

        final int nbFrames;

        final int bitsAllocated;

        int bitsStored;

        final int height;

        final int width;

        final int shift;

        final int pixelRepresentation;

        final PixelData data;

        final int type ;

        final ColorModel colorModel;

        final SampleModel sampleModel;

        final long offsets[];

        final int br_subsampled;

        boolean need_sign_propagation;

        int nextIndex;

        /* private final DicomReader this$0; *//* synthetic field */
        // Parameter 0 added: Name this Type
        // Lfr/apteryx/imageio/dicom/DicomReader$ImageInfo; At 0 1433 Range 0
        // 1432 Init 0 fixed
        // Parameter 1 added: Name dicomreader Type
        // Lfr/apteryx/imageio/dicom/DicomReader; At 0 1433 Range 0 1432 Init 0
        // Parameter 2 added: Name i Type I At 0 1433 Range 0 1432 Init 0
        // Parameter 3 added: Name dataset Type
        // Lfr/apteryx/imageio/dicom/DataSet; At 0 1433 Range 0 1432 Init 0
        // RetValue 15 added: Name <returnValue> Type V At 0 1433 Range 0 1432
        // Init 0 fixed
        // LocalVar 5 added: Name j Type I At 32 1333 Range 32 1364 Init 32
        // LocalVar 6 added: Name s Type Ljava/lang/String; At 40 1301 Range 40
        // 1340 Init 40
        // LocalVar 7 added: Name k Type I At 48 874 Range 48 921 Init 48
        // LocalVar 8 added: Name l Type I At 96 16 Range 96 111 Init 96
        // LocalVar 9 added: Name obj1 Type A At 215 1199 Range 215 1413 Init
        // 215
        // LocalVar 10 added: Name iterator Type Ljava/util/Iterator; At 293 18
        // Range 293 310 Init 293
        // LocalVar 10 added: Name iterator1 Type Ljava/util/Iterator; At 341 18
        // Range 341 358 Init 341
        // LocalVar 10 added: Name iterator2 Type Ljava/util/Iterator; At 389 18
        // Range 389 406 Init 389
        // LocalVar 10 added: Name i1 Type I At 465 4 Range 465 468 Init 465
        // LocalVar 11 added: Name l1 Type J At 500 8 Range 500 507 Init 500
        // LocalVar 13 added: Name imagetypespecifier Type
        // Ljavax/imageio/ImageTypeSpecifier; At 540 34 Range 540 573 Init 540
        // LocalVar 14 added: Name samplemodel Type Ljava/awt/image/SampleModel;
        // At 570 130 Range 570 699 Init 570
        // LocalVar 9 chged: Name colormodel Oname obj1 Type
        // Ljava/awt/image/ColorModel; At 577 837 Range 215 1413 Init 215
        // LocalVar 10 added: Name ai Type [I At 734 53 Range 734 786 Init 734
        // LocalVar 11 added: Name flag Type Z At 737 19 Range 737 755 Init 737
        // LocalVar 11 chged: Name j1 Oname flag Type I At 739 2 Range 737 755
        // Init 737
        // LocalVar 9 chged: Name obj1 Oname colormodel Type Ljava/lang/Object;
        // At 1303 111 Range 215 1413 Init 215
        // LocalVar 10 added: Name ai1 Type [I At 1312 90 Range 1312 1401 Init
        // 1312
        // LocalVar 11 added: Name flag Type Z At 1315 21 Range 1315 1335 Init
        // 1315
        // LocalVar 11 chged: Name k1 Oname flag Type I At 1317 2 Range 1315
        // 1335 Init 1315
        // LocalVar 11 added: Name colorspace Type Ljava/awt/color/ColorSpace;
        // At 1346 60 Range 1346 1405 Init 1346
        // Decompiling method: getType Signature: ()I
        // Max stack: 5, #locals: 2, #params: 1
        // Code length: 160 bytes, Code offset: 7637
        // Line Number Table found: 10 entries
        // Parameter 0 added: Name this Type
        // Lfr/apteryx/imageio/dicom/DicomReader$ImageInfo; At 0 160 Range 0 159
        // Init 0 fixed
        // RetValue 2 added: Name <returnValue> Type I At 0 160 Range 0 159 Init
        // 0 fixed
        // LocalVar 1 added: Name flag Type Z At 17 142 Range 17 158 Init 17
        // LocalVar 1 chged: Name byte0 Oname flag Type B At 60 99 Range 17 158
        // Init 17
        private int getType() throws DicomException {
            byte byte0;
            if (bitsStored <= 8 && pixelRepresentation == 0)
                byte0 = 0;
            else if (bitsStored <= 16 && pixelRepresentation == 0)
                byte0 = 1;
            else if (bitsStored <= 16 && pixelRepresentation == 1)
                byte0 = 2;
            else
                throw new DicomException("Unsupported", "bitsStored="
                        + bitsStored + " and pixelRepresentation="
                        + pixelRepresentation);
            if (bitsAllocated != 8 && bitsAllocated != 16)
                throw new DicomException("Unsupported", "bitsAllocated="
                        + bitsAllocated);
            else
                return byte0;
        }

        // Decompiling method: read Signature:
        // (Ljava/awt/image/WritableRaster;I)V
        // Max stack: 9, #locals: 13, #params: 3
        // Code length: 948 bytes, Code offset: 9702
        // Line Number Table found: 65 entries
        // Parameter 0 added: Name this Type
        // Lfr/apteryx/imageio/dicom/DicomReader$ImageInfo; At 0 948 Range 0 947
        // Init 0 fixed
        // Parameter 1 added: Name writableraster Type
        // Ljava/awt/image/WritableRaster; At 0 948 Range 0 947 Init 0
        // Parameter 2 added: Name i Type I At 0 948 Range 0 947 Init 0
        // RetValue 13 added: Name <returnValue> Type V At 0 948 Range 0 947
        // Init 0 fixed
        // LocalVar 3 added: Name j Type I At 81 2 Range 81 82 Init 81
        // LocalVar 4 added: Name l Type J At 117 36 Range 117 152 Init 117
        // LocalVar 4 added: Name l1 Type J At 166 22 Range 166 187 Init 166
        // LocalVar 6 added: Name flag Type Z At 169 41 Range 169 209 Init 169
        // LocalVar 6 chged: Name k Oname flag Type I At 171 2 Range 169 209
        // Init 169
        // LocalVar 3 added: Name flag Type Z At 251 42 Range 251 292 Init 251
        // LocalVar 3 chged: Name i1 Oname flag Type I At 252 1 Range 251 292
        // Init 251
        // LocalVar 3 added: Name j1 Type I At 369 2 Range 369 370 Init 369
        // LocalVar 4 added: Name l2 Type J At 405 31 Range 405 435 Init 405
        // LocalVar 3 added: Name fragmentconcatenerimageinputstream Type
        // Lfr/apteryx/imageio/dicom/FragmentConcatenerImageInputStream; At 494
        // 49 Range 494 542 Init 494
        // LocalVar 4 added: Name rlereader Type
        // Lfr/apteryx/imageio/dicom/RLEReader; At 515 4 Range 515 518 Init 515
        // LocalVar 4 added: Name imagereadparam Type
        // Ljavax/imageio/ImageReadParam; At 556 31 Range 556 586 Init 556
        // LocalVar 3 added: Name bufferedimage Type
        // Ljava/awt/image/BufferedImage; At 696 204 Range 696 899 Init 696
        // LocalVar 4 added: Name graphics2d Type Ljava/awt/Graphics2D; At 701
        // 228 Range 701 928 Init 701
        // LocalVar 5 added: Name s Type Ljava/lang/String; At 718 11 Range 718
        // 728 Init 718
        // LocalVar 5 added: Name s1 Type Ljava/lang/String; At 740 11 Range 740
        // 750 Init 740
        // LocalVar 5 added: Name s2 Type Ljava/lang/String; At 762 62 Range 762
        // 823 Init 762
        // LocalVar 6 added: Name font Type Ljava/awt/Font; At 787 30 Range 787
        // 816 Init 787
        // LocalVar 7 added: Name k1 Type I At 801 34 Range 801 834 Init 801
        // LocalVar 8 added: Name i2 Type I At 813 37 Range 813 849 Init 813
        // LocalVar 9 added: Name glyphvector Type Ljava/awt/font/GlyphVector;
        // At 827 104 Range 827 930 Init 827
        // LocalVar 10 added: Name j2 Type I At 838 95 Range 838 932 Init 838
        // LocalVar 11 added: Name k2 Type I At 854 82 Range 854 935 Init 854
        // LocalVar 12 added: Name rectangle Type Ljava/awt/Rectangle; At 872 38
        // Range 872 909 Init 872
        // LocalVar 4 name l1(J) merged out into l(J)
        // LocalVar 5 name s1(Ljava/lang/String;) merged out into
        // s(Ljava/lang/String;)
        // LocalVar 5 name s2(Ljava/lang/String;) merged out into
        // s(Ljava/lang/String;)
        void read(WritableRaster writableraster, int i) throws IOException {
            if (i >= nextIndex) {
                if (nextIndex == 0)
                    if (data.TS.encapsulated) {
                        stream.seek(data.pos);
                        int j = (stream.readShort() & 0xffff) << 16
                                | stream.readShort() & 0xffff;
                        if (j != 0xfffee000)
                            throw new DicomException("ProtocoleViolation",
                                    "Item expected");
                        long l1 = (long) stream.readInt() & 0xffffffffL;
                        if (l1 > 0L) {
                            if (l1 != (long) (4 * nbFrames))
                                throw new DicomException("ProtocoleViolation",
                                        "Incorrect size of basic offset table");
                            l1 += stream.getStreamPosition();
                            for (int i1 = 0; i1 < nbFrames; i1++)
                                offsets[i1] = l1
                                        + ((long) stream.readInt() & 0xffffffffL);

                            nextIndex = nbFrames;
                        } else {
                            offsets[0] = stream.getStreamPosition();
                            nextIndex = 1;
                        }
                    } else {
                        for (int k = 0; k < nbFrames; k++)
                            offsets[k] = data.pos + (data.l / (long) nbFrames)
                                    * (long) k;

                        nextIndex = nbFrames;
                    }
                while (nextIndex <= i) {
                    stream.seek(offsets[nextIndex - 1]);
                    int l = (stream.readShort() & 0xffff) << 16
                            | stream.readShort() & 0xffff;
                    if (l != 0xfffee000)
                        throw new DicomException("ProtocoleViolation",
                                "Item expected");
                    long l2 = (long) stream.readInt() & 0xffffffffL;
                    offsets[nextIndex++] = stream.getStreamPosition() + l2;
                }
            }
            processImageStarted(i);
            stream.seek(offsets[i]);
            if (data.TS.encapsulated) {
                FragmentConcatenerImageInputStream fragmentconcatenerimageinputstream = new FragmentConcatenerImageInputStream(
                        stream);
                if (data.TS == TransferSyntax.RLE_COMPRESSION) {
                    RLEReader rlereader = new RLEReader();
                    rlereader.readRLEImage(fragmentconcatenerimageinputstream,
                            writableraster, bitsAllocated, br_subsampled);
                } else {
                    helper.setInput(fragmentconcatenerimageinputstream);
                    ImageReadParam imagereadparam = helper
                            .getDefaultReadParam();
                    imagereadparam.setDestination(new BufferedImage(colorModel,
                            writableraster, false, null));
                    helper.read(0, imagereadparam);
                    helper.setInput(null);
                    if (need_sign_propagation)
                        Operations.propagateSign(writableraster, bitsStored);
                }
            } else {
                RawReader.readRaw(stream, writableraster, data.TS.bigEndian,
                        bitsAllocated, bitsStored, shift, data.VR.equals("OW"),
                        pixelRepresentation == 1, br_subsampled);
            }
            BufferedImage bufferedimage = new BufferedImage(colorModel,
                    writableraster, false, null);
            Graphics2D graphics2d = bufferedimage.createGraphics();
            String s = new String(new char[] { 'D' });
            s = s + 'E';
            s = s + "MO";
            Font font = new Font("SansSerif", 0, Math.min(bufferedimage
                    .getHeight(), bufferedimage.getWidth()) / 3);
            int j1 = graphics2d.getFontMetrics(font).stringWidth(s);
            int k1 = graphics2d.getFontMetrics(font).getAscent();
            GlyphVector glyphvector = font.createGlyphVector(graphics2d
                    .getFontRenderContext(), s);
            int i2 = (bufferedimage.getWidth() - j1) / 2;
            int j2 = bufferedimage.getHeight()
                    - (bufferedimage.getHeight() - k1) / 2;
            Rectangle rectangle = glyphvector.getPixelBounds(graphics2d
                    .getFontRenderContext(), i2, j2);
            graphics2d.setColor(Color.BLACK);
            graphics2d.fillRect(0, rectangle.y - rectangle.height / 10,
                    bufferedimage.getWidth(), rectangle.height
                            + rectangle.height / 5);
            graphics2d.setColor(Color.WHITE);
            graphics2d.drawGlyphVector(glyphvector, i2, j2);
            processImageComplete();
            /* return; */
        }

        // Decompiling method: <init> Signature:
        // (Lfr/apteryx/imageio/dicom/DicomReader;ILfr/apteryx/imageio/dicom/DataSet;)V
        // Max stack: 9, #locals: 15, #params: 4
        // Code length: 1433 bytes, Code offset: 7881
        // Line Number Table found: 86 entries
        // LocalVar 4 hasn't been used
        // LocalVar 12 hasn't been used
        ImageInfo(int i, DataSet dataset) throws IOException {
            /* this$0 = dicomreader; */
            super();
            need_sign_propagation = false;
            nbFrames = i;
            offsets = new long[i];
            int j = dataset.findInt(0x280002);
            String s = dataset.findString(0x280004);
            int k = dataset.findInt(0x280006);
            height = dataset.findInt(0x280010);
            width = dataset.findInt(0x280011);
            bitsAllocated = dataset.findInt(0x280100);
            bitsStored = dataset.findInt(0x280101);
            int l = dataset.findInt(0x280102);
            pixelRepresentation = dataset.findInt(0x280103);
            shift = (1 + l) - bitsStored;
            data = (PixelData) dataset.findValue(0x7fe00010, PixelData.class);
            if (data == null)
                throw new DicomException("ProtocoleViolation", "No Pixel Data");
            if (shift != 0 && data.TS.encapsulated)
                throw new DicomException("Unsupported",
                        "Encapsulated transfer type and highBit != bitsStored-1");
             type = getType(); 
            Object obj1 = null;
            if (data.TS.encapsulated
                    && data.TS != TransferSyntax.RLE_COMPRESSION) {
                helper = data.TS.getReader();
                if (helper == null)
                    throw new DicomException("EnvironmentError",
                            "Cannot create helper reader");
                /*
                 * if(DicomReader.this.StringBuffer != null) { for(Iterator
                 * iterator = DicomReader.this.StringBuffer.iterator();
                 * iterator.hasNext();
                 * helper.addIIOReadWarningListener((IIOReadWarningListener)iterator.next())); }
                 * 
                 * if( != null) { for(Iterator iterator1 = .iterator();
                 * iterator1.hasNext();
                 * helper.addIIOReadProgressListener((IIOReadProgressListener)iterator1.next())); }
                 * if(_22_Unsupported_22_ != null) { for(Iterator iterator2 =
                 * _22_Unsupported_22_.iterator(); iterator2.hasNext();
                 * helper.addIIOReadUpdateListener((IIOReadUpdateListener)iterator2.next())); }
                 */
                stream.seek(data.pos);
                int i1 = (stream.readShort() & 0xffff) << 16
                        | stream.readShort() & 0xffff;
                if (i1 != 0xfffee000)
                    throw new DicomException("ProtocoleViolation",
                            "Item expected");
                long l1 = (long) stream.readInt() & 0xffffffffL;
                stream.skipBytes(l1);
                helper.setInput(new FragmentConcatenerImageInputStream(stream));
                ImageTypeSpecifier imagetypespecifier = helper
                        .getRawImageType(0);
                if (imagetypespecifier == null)
                    imagetypespecifier = (ImageTypeSpecifier) helper
                            .getImageTypes(0).next();
                SampleModel samplemodel = imagetypespecifier.getSampleModel();
                obj1 = imagetypespecifier.getColorModel();
                if ((obj1 instanceof ComponentColorModel) && j == 1)
                    obj1 = null;
                if (samplemodel.getDataType() == 1 && type == 2) {
                    if (bitsStored < 16)
                        need_sign_propagation = true;
                    samplemodel = Util.convertSampleModel(samplemodel, type);
                    obj1 = null;
                }
                sampleModel = samplemodel.getWidth() == helper.getWidth(0)
                        && samplemodel.getHeight() == helper.getHeight(0) ? samplemodel
                        : samplemodel.createCompatibleSampleModel(helper
                                .getWidth(0), helper.getHeight(0));
            } else if (k == 0 || j == 1
                    || data.TS == TransferSyntax.RLE_COMPRESSION) {
                int ai[] = new int[j];
                for (int j1 = 0; j1 < j; j1++)
                    ai[j1] = j1;

                sampleModel = new PixelInterleavedSampleModel(type, width,
                        height, j, width * j, ai);
            } else {
                sampleModel = new BandedSampleModel(type, width, height, j);
            }
            if (s != null) {
                if (s.endsWith("_422"))
                    br_subsampled = 1;
                else if (s.endsWith("_420"))
                    br_subsampled = 2;
                else
                    br_subsampled = 0;
            } else {
                br_subsampled = 0;
            }
            if (br_subsampled > 0
                    && !data.TS.encapsulated
                    && (bitsAllocated != 8 && bitsAllocated != 16
                            || pixelRepresentation != 0 || k != 0 || shift != 0))
                throw new DicomException(
                        "ProtocoleViolation",
                        "Pixel format not compatible with chrominance-subsampled photometric interpretation");
            if (br_subsampled > 0 && !data.TS.encapsulated && width % 2 == 1)
                throw new DicomException("Unsupported",
                        "Odd image width and chrominance-subsampled photometric interpretation");
            if (br_subsampled == 2 && data.TS == TransferSyntax.RLE_COMPRESSION)
                throw new DicomException(
                        "ProtocoleViolation",
                        "RLE Compression is not compatible with chrominance-subsampled photometric interpretation");
            if (helper == null || obj1 == null || "PALETTE COLOR".equals(s))
                if ("PALETTE COLOR".equals(s)) {
                    if (pixelRepresentation == 1)
                        wl.warning("Unsupported",
                                "Palette with signed pixel values");
                    obj1 = DicomColorSpace.getIndexColorModel(bitsStored,
                            new Number[][] {
                                    (Number[]) (Number[]) dataset.findValue(
                                            0x281101, Number[].class),
                                    (Number[]) (Number[]) dataset.findValue(
                                            0x281102, Number[].class) },
                            new short[][] {
                                    (short[]) (short[]) dataset.findValue(
                                            0x281201, short[].class),
                                    (short[]) (short[]) dataset.findValue(
                                            0x281202, short[].class) });
                } else {
                    int ai1[] = new int[j];
                    for (int k1 = 0; k1 < j; k1++)
                        ai1[k1] = bitsStored;

                    java.awt.color.ColorSpace colorspace = DicomColorSpace
                            .getInstance(s, j);
                    if (pixelRepresentation == 0
                            && (colorspace instanceof ICC_ColorSpace) || j > 1)
                        obj1 = new ComponentColorModel(colorspace, ai1, false,
                                false, 1, type);
                    else
                        obj1 = new VOIDefaultColorModel(type, ai1[0],
                                colorspace);
                }
            colorModel = ((ColorModel) (obj1));
            if (helper != null)
                helper.setInput(null);
            /* return; */
        }
    }

    private DicomObject obj;

    private ImageInputStream stream;

    private ImageInfo info;

    private ImageReader helper;

    final DicomWarningListener wl = new DicomWarningListener() {
        // Constants: 53
        // Interfaces: 1
        // Fields: 1
        // Methods: 3
        // Class Attributes: 2

        /* private final DicomReader this$0; *//* synthetic field */
        // Parameter 0 added: Name this Type
        // Lfr/apteryx/imageio/dicom/DicomReader$1; At 0 10 Range 0 9 Init 0
        // fixed
        // Parameter 1 added: Name dicomreader Type
        // Lfr/apteryx/imageio/dicom/DicomReader; At 0 10 Range 0 9 Init 0
        // RetValue 2 added: Name <returnValue> Type V At 0 10 Range 0 9 Init 0
        // fixed
        // Decompiling method: warning Signature:
        // (Ljava/lang/String;Ljava/lang/String;)V
        // Max stack: 3, #locals: 3, #params: 3
        // Code length: 34 bytes, Code offset: 931
        // Line Number Table found: 2 entries
        // Parameter 0 added: Name this Type
        // Lfr/apteryx/imageio/dicom/DicomReader$1; At 0 34 Range 0 33 Init 0
        // fixed
        // Parameter 1 added: Name s Type Ljava/lang/String; At 0 34 Range 0 33
        // Init 0
        // Parameter 2 added: Name s1 Type Ljava/lang/String; At 0 34 Range 0 33
        // Init 0
        // RetValue 3 added: Name <returnValue> Type V At 0 34 Range 0 33 Init 0
        // fixed
        public void warning(String s, String s1) {
            processWarningOccurred(DicomException.localize(s) + ": " + s1);
            /* return; */
        }

        // Decompiling method: warning Signature:
        // (ILjava/lang/String;Ljava/lang/String;)V
        // Max stack: 3, #locals: 4, #params: 4
        // Code length: 7 bytes, Code offset: 1007
        // Line Number Table found: 2 entries
        // Parameter 0 added: Name this Type
        // Lfr/apteryx/imageio/dicom/DicomReader$1; At 0 7 Range 0 6 Init 0
        // fixed
        // Parameter 1 added: Name i Type I At 0 7 Range 0 6 Init 0
        // Parameter 2 added: Name s Type Ljava/lang/String; At 0 7 Range 0 6
        // Init 0
        // Parameter 3 added: Name s1 Type Ljava/lang/String; At 0 7 Range 0 6
        // Init 0
        // RetValue 4 added: Name <returnValue> Type V At 0 7 Range 0 6 Init 0
        // fixed
        public void warning(int i, String s, String s1) {
            warning(s, s1);
            /* return; */
        }

        // Decompiling method: <init> Signature:
        // (Lfr/apteryx/imageio/dicom/DicomReader;)V
        // Max stack: 2, #locals: 2, #params: 2
        // Code length: 10 bytes, Code offset: 883
        // Line Number Table found: 1 entries

    };

    boolean raw;

    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 6 Range 0 5 Init 0
    // Parameter 1 added: Name s Type Ljava/lang/String; At 0 6 Range 0 5 Init 0
    // RetValue 2 added: Name <returnValue> Type V At 0 6 Range 0 5 Init 0 fixed
    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 7 Range 0 6 Init 0
    // Parameter 1 added: Name imagereader Type Ljavax/imageio/ImageReader; At 0
    // 7 Range 0 6 Init 0
    // RetValue 2 added: Name <returnValue> Type Ljavax/imageio/ImageReader; At
    // 0 7 Range 0 6 Init 0 fixed
    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 5 Range 0 4 Init 0
    // RetValue 1 added: Name <returnValue> Type Ljavax/imageio/ImageReader; At
    // 0 5 Range 0 4 Init 0 fixed
    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 5 Range 0 4 Init 0
    // RetValue 1 added: Name <returnValue> Type Ljava/util/List; At 0 5 Range 0
    // 4 Init 0 fixed
    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 5 Range 0 4 Init 0
    // RetValue 1 added: Name <returnValue> Type Ljava/util/List; At 0 5 Range 0
    // 4 Init 0 fixed
    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 5 Range 0 4 Init 0
    // RetValue 1 added: Name <returnValue> Type Ljava/util/List; At 0 5 Range 0
    // 4 Init 0 fixed
    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 5 Range 0 4 Init 0
    // RetValue 1 added: Name <returnValue> Type Ljava/util/List; At 0 5 Range 0
    // 4 Init 0 fixed
    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 5 Range 0 4 Init 0
    // RetValue 1 added: Name <returnValue> Type Ljava/util/List; At 0 5 Range 0
    // 4 Init 0 fixed
    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 5 Range 0 4 Init 0
    // RetValue 1 added: Name <returnValue> Type Ljava/util/List; At 0 5 Range 0
    // 4 Init 0 fixed
    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 5 Range 0 4 Init 0
    // RetValue 1 added: Name <returnValue> Type
    // Ljavax/imageio/stream/ImageInputStream; At 0 5 Range 0 4 Init 0 fixed
    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 6 Range 0 5 Init 0
    // Parameter 1 added: Name i Type I At 0 6 Range 0 5 Init 0
    // RetValue 2 added: Name <returnValue> Type V At 0 6 Range 0 5 Init 0 fixed
    // Parameter 0 added: Name dicomreader Type
    // Lfr/apteryx/imageio/dicom/DicomReader; At 0 5 Range 0 4 Init 0
    // RetValue 1 added: Name <returnValue> Type V At 0 5 Range 0 4 Init 0 fixed

    // Decompiling method: <init> Signature:
    // (Ljavax/imageio/spi/ImageReaderSpi;)V
    // Max stack: 4, #locals: 2, #params: 2
    // Code length: 23 bytes, Code offset: 5899
    // Line Number Table found: 4 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 23 Range 0 22 Init 0 fixed
    // Parameter 1 added: Name imagereaderspi Type
    // Ljavax/imageio/spi/ImageReaderSpi; At 0 23 Range 0 22 Init 0
    // RetValue 2 added: Name <returnValue> Type V At 0 23 Range 0 22 Init 0
    // fixed
    DicomReader(ImageReaderSpi imagereaderspi) {
        super(imagereaderspi);
        raw = false;
    }

    // Decompiling method: setRawMode Signature: (Z)V
    // Max stack: 2, #locals: 2, #params: 2
    // Code length: 6 bytes, Code offset: 5972
    // Line Number Table found: 2 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 6 Range 0 5 Init 0 fixed
    // Parameter 1 added: Name flag Type Z At 0 6 Range 0 5 Init 0
    // RetValue 2 added: Name <returnValue> Type V At 0 6 Range 0 5 Init 0 fixed
    public void setRawMode(boolean flag) {
        raw = flag;
        /* return; */
    }

    // Decompiling method: readObj Signature: ()V
    // Max stack: 6, #locals: 5, #params: 1
    // Code length: 256 bytes, Code offset: 6020
    // Exception table: 1 entries
    //           start 114 end 119 handler 122 type InterruptedException
    // Line Number Table found: 29 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 256 Range 0 255 Init 0 fixed
    // RetValue 5 added: Name <returnValue> Type V At 0 256 Range 0 255 Init 0
    // fixed
    // LocalVar 1 added: Name receivedobject Type
    // Lfr/apteryx/imageio/dicom/ReceivedObject; At 24 18 Range 24 41 Init 24
    // LocalVar 1 added: Name storagescp Type
    // Lfr/apteryx/imageio/dicom/StorageSCP; At 109 6 Range 109 114 Init 109
    // LocalVar 2 added: Name receivedobject1 Type
    // Lfr/apteryx/imageio/dicom/ReceivedObject; At 118 45 Range 118 162 Init
    // 118
    // LocalVar 1 added: Name flag Type Z At 188 54 Range 188 241 Init 188
    // LocalVar 1 chged: Name i Oname flag Type I At 204 38 Range 188 241 Init
    // 188
    // LocalVar 3 added: Name interruptedexception Type
    // Ljava/lang/InterruptedException; At 122 13 Range 122 134 Init 122
    // LocalVar 4 added: Name interruptedioexception Type
    // Ljava/io/InterruptedIOException; At 130 11 Range 130 140 Init 130
    private void readObj() throws IOException {
        if (obj == null) {
            if (input instanceof ImageInputStream) {
                stream = (ImageInputStream) input;
                obj = new DicomObject(stream, wl, raw);
            }
            int i;
            if (!(obj.ds.findValue(0x7fe00010) instanceof PixelData)) {
                i = 0;
            } else {
                i = obj.ds.findInt(0x280008);
                if (i == 0)
                    if (obj.ds.findInt(0x280010) > 0)
                        i = 1;
                    else
                        i = 0;
            }
            if (i > 0)
                info = new ImageInfo(i, obj.ds);
        }
        /* return; */
    }

    // Decompiling method: getHeight Signature: (I)I
    // Max stack: 1, #locals: 2, #params: 2
    // Code length: 23 bytes, Code offset: 6444
    // Line Number Table found: 2 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 23 Range 0 22 Init 0 fixed
    // Parameter 1 added: Name i Type I At 0 23 Range 0 22 Init 0
    // RetValue 2 added: Name <returnValue> Type I At 0 23 Range 0 22 Init 0
    // fixed
    public int getHeight(int i) throws IOException {
        readObj();
        return info != null ? info.height : 0;
    }

    // Decompiling method: getWidth Signature: (I)I
    // Max stack: 1, #locals: 2, #params: 2
    // Code length: 23 bytes, Code offset: 6519
    // Line Number Table found: 2 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 23 Range 0 22 Init 0 fixed
    // Parameter 1 added: Name i Type I At 0 23 Range 0 22 Init 0
    // RetValue 2 added: Name <returnValue> Type I At 0 23 Range 0 22 Init 0
    // fixed
    public int getWidth(int i) throws IOException {
        readObj();
        return info != null ? info.width : 0;
    }

    // Decompiling method: getSampleModel Signature:
    // (I)Ljava/awt/image/SampleModel;
    // Max stack: 1, #locals: 2, #params: 2
    // Code length: 23 bytes, Code offset: 6594
    // Line Number Table found: 2 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 23 Range 0 22 Init 0 fixed
    // Parameter 1 added: Name i Type I At 0 23 Range 0 22 Init 0
    // RetValue 2 added: Name <returnValue> Type Ljava/awt/image/SampleModel; At
    // 0 23 Range 0 22 Init 0 fixed
    public SampleModel getSampleModel(int i) throws IOException {
        readObj();
        return info != null ? info.sampleModel : null;
    }

    // Decompiling method: getColorModel Signature:
    // (I)Ljava/awt/image/ColorModel;
    // Max stack: 1, #locals: 2, #params: 2
    // Code length: 23 bytes, Code offset: 6669
    // Line Number Table found: 2 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 23 Range 0 22 Init 0 fixed
    // Parameter 1 added: Name i Type I At 0 23 Range 0 22 Init 0
    // RetValue 2 added: Name <returnValue> Type Ljava/awt/image/ColorModel; At
    // 0 23 Range 0 22 Init 0 fixed
    public ColorModel getColorModel(int i) throws IOException {
        readObj();
        return info != null ? info.colorModel : null;
    }

    // Decompiling method: getImageMetadata Signature:
    // (I)Ljavax/imageio/metadata/IIOMetadata;
    // Max stack: 1, #locals: 2, #params: 2
    // Code length: 2 bytes, Code offset: 6744
    // Line Number Table found: 1 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 2 Range 0 1 Init 0 fixed
    // Parameter 1 added: Name i Type I At 0 2 Range 0 1 Init 0
    // RetValue 2 added: Name <returnValue> Type
    // Ljavax/imageio/metadata/IIOMetadata; At 0 2 Range 0 1 Init 0 fixed
    public IIOMetadata getImageMetadata(int i) {
        return null;
    }

    // Decompiling method: getStreamMetadata Signature:
    // ()Ljavax/imageio/metadata/IIOMetadata;
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 5 bytes, Code offset: 6784
    // Line Number Table found: 1 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 5 Range 0 4 Init 0 fixed
    // RetValue 1 added: Name <returnValue> Type
    // Ljavax/imageio/metadata/IIOMetadata; At 0 5 Range 0 4 Init 0 fixed
    public IIOMetadata getStreamMetadata() throws IOException {
        return getDicomMetadata();
    }

    // Decompiling method: getDicomMetadata Signature:
    // ()Lfr/apteryx/imageio/dicom/DicomMetadata;
    // Max stack: 3, #locals: 1, #params: 1
    // Code length: 16 bytes, Code offset: 6837
    // Line Number Table found: 2 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 16 Range 0 15 Init 0 fixed
    // RetValue 1 added: Name <returnValue> Type
    // Lfr/apteryx/imageio/dicom/DicomMetadata; At 0 16 Range 0 15 Init 0 fixed
    public DicomMetadata getDicomMetadata() throws IOException {
        readObj();
        return new DicomMetadata(obj);
    }

    // Decompiling method: getImageTypes Signature: (I)Ljava/util/Iterator;
    // Max stack: 5, #locals: 3, #params: 2
    // Code length: 70 bytes, Code offset: 6905
    // Line Number Table found: 6 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 70 Range 0 69 Init 0 fixed
    // Parameter 1 added: Name i Type I At 0 70 Range 0 69 Init 0
    // RetValue 3 added: Name <returnValue> Type Ljava/util/Iterator; At 0 70
    // Range 0 69 Init 0 fixed
    // LocalVar 2 added: Name arraylist Type Ljava/util/ArrayList; At 38 28
    // Range 38 65 Init 38
    public Iterator getImageTypes(int i) throws IOException {
        readObj();
        if (info == null || i >= info.nbFrames) {
            throw new IndexOutOfBoundsException();
        } else {
            ArrayList arraylist = new ArrayList(1);
            arraylist.add(new ImageTypeSpecifier(info.colorModel,
                    info.sampleModel));
            return arraylist.iterator();
        }
    }

    // Decompiling method: getTransformedImageTypes Signature:
    // (I)Ljava/util/Iterator;
    // Max stack: 5, #locals: 4, #params: 2
    // Code length: 130 bytes, Code offset: 7043
    // Line Number Table found: 10 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 130 Range 0 129 Init 0 fixed
    // Parameter 1 added: Name i Type I At 0 130 Range 0 129 Init 0
    // RetValue 4 added: Name <returnValue> Type Ljava/util/Iterator; At 0 130
    // Range 0 129 Init 0 fixed
    // LocalVar 2 added: Name arraylist Type Ljava/util/ArrayList; At 38 88
    // Range 38 125 Init 38
    // LocalVar 3 added: Name applyluttarget Type
    // Lfr/apteryx/imageio/dicom/DicomObject$ApplyLUTTarget; At 60 55 Range 60
    // 114 Init 60
    public Iterator getTransformedImageTypes(int i) throws IOException {
        readObj();
        if (info == null || i >= info.nbFrames)
            throw new IndexOutOfBoundsException();
        ArrayList arraylist = new ArrayList(1);
        DicomObject.ApplyLUTTarget applyluttarget = new DicomObject.ApplyLUTTarget(
                info.sampleModel, info.colorModel);
        if (!obj.applyLUT(applyluttarget, i, wl)) {
            applyluttarget = new DicomObject.ApplyLUTTarget(read(i, null));
            obj.applyLUT(applyluttarget, i, wl);
        }
        arraylist.add(new ImageTypeSpecifier(applyluttarget.cm,
                applyluttarget.sm));
        return arraylist.iterator();
    }

    // Decompiling method: getNumImages Signature: (Z)I
    // Max stack: 1, #locals: 2, #params: 2
    // Code length: 23 bytes, Code offset: 7257
    // Line Number Table found: 2 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 23 Range 0 22 Init 0 fixed
    // Parameter 1 added: Name flag Type Z At 0 23 Range 0 22 Init 0
    // RetValue 2 added: Name <returnValue> Type I At 0 23 Range 0 22 Init 0
    // fixed
    public int getNumImages(boolean flag) throws IOException {
        readObj();
        return info != null ? info.nbFrames : 0;
    }

    // Decompiling method: changeUShortToShort Signature:
    // (Ljava/awt/image/DataBuffer;)Ljava/awt/image/DataBufferShort;
    // Max stack: 5, #locals: 3, #params: 2
    // Code length: 25 bytes, Code offset: 7332
    // Line Number Table found: 2 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 25 Range 0 24 Init 0 fixed
    // Parameter 1 added: Name databuffer Type Ljava/awt/image/DataBuffer; At 0
    // 25 Range 0 24 Init 0
    // RetValue 3 added: Name <returnValue> Type
    // Ljava/awt/image/DataBufferShort; At 0 25 Range 0 24 Init 0 fixed
    // LocalVar 2 added: Name databufferushort Type
    // Ljava/awt/image/DataBufferUShort; At 4 14 Range 4 17 Init 4
    private DataBufferShort changeUShortToShort(DataBuffer databuffer) {
        DataBufferUShort databufferushort = (DataBufferUShort) databuffer;
        return new DataBufferShort(databufferushort.getBankData(),
                databufferushort.getSize(), databufferushort.getOffsets());
    }

    // Decompiling method: read Signature:
    // (ILjavax/imageio/ImageReadParam;)Ljava/awt/image/BufferedImage;
    // Max stack: 6, #locals: 4, #params: 3
    // Code length: 112 bytes, Code offset: 7399
    // Line Number Table found: 9 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 112 Range 0 111 Init 0 fixed
    // Parameter 1 added: Name i Type I At 0 112 Range 0 111 Init 0
    // Parameter 2 added: Name imagereadparam Type
    // Ljavax/imageio/ImageReadParam; At 0 112 Range 0 111 Init 0
    // RetValue 4 added: Name <returnValue> Type Ljava/awt/image/BufferedImage;
    // At 0 112 Range 0 111 Init 0 fixed
    // LocalVar 3 added: Name bufferedimage Type Ljava/awt/image/BufferedImage;
    // At 74 37 Range 74 110 Init 74
    public BufferedImage read(int i, ImageReadParam imagereadparam)
            throws IOException {
        readObj();
        if (info == null || i < 0 || i >= info.nbFrames)
            throw new IndexOutOfBoundsException();
        BufferedImage bufferedimage;
        if (imagereadparam == null)
            bufferedimage = new BufferedImage(info.colorModel, Raster
                    .createWritableRaster(info.sampleModel, null),
                    info.colorModel.isAlphaPremultiplied(), null);
        else
            bufferedimage = getDestination(imagereadparam, getImageTypes(i),
                    getWidth(i), getHeight(i));
        info.read(bufferedimage.getRaster(), i);
        return bufferedimage;
    }

    // Decompiling method: readRaster Signature:
    // (ILjavax/imageio/ImageReadParam;)Ljava/awt/image/Raster;
    // Max stack: 3, #locals: 4, #params: 3
    // Code length: 57 bytes, Code offset: 7591
    // Line Number Table found: 6 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 57 Range 0 56 Init 0 fixed
    // Parameter 1 added: Name i Type I At 0 57 Range 0 56 Init 0
    // Parameter 2 added: Name imagereadparam Type
    // Ljavax/imageio/ImageReadParam; At 0 57 Range 0 56 Init 0
    // RetValue 4 added: Name <returnValue> Type Ljava/awt/image/Raster; At 0 57
    // Range 0 56 Init 0 fixed
    // LocalVar 3 added: Name writableraster Type
    // Ljava/awt/image/WritableRaster; At 45 11 Range 45 55 Init 45
    public Raster readRaster(int i, ImageReadParam imagereadparam)
            throws IOException {
        readObj();
        if (info == null || i < 0 || i >= info.nbFrames) {
            throw new IndexOutOfBoundsException();
        } else {
            WritableRaster writableraster = Raster.createWritableRaster(
                    info.sampleModel, null);
            info.read(writableraster, i);
            return writableraster;
        }
    }

    // Decompiling method: canReadRaster Signature: ()Z
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 2 bytes, Code offset: 7716
    // Line Number Table found: 1 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 2 Range 0 1 Init 0 fixed
    // RetValue 1 added: Name <returnValue> Type Z At 0 2 Range 0 1 Init 0 fixed
    public boolean canReadRaster() {
        return true;
    }

    // Decompiling method: setInput Signature: (Ljava/lang/Object;ZZ)V
    // Max stack: 4, #locals: 4, #params: 4
    // Code length: 73 bytes, Code offset: 7756
    // Line Number Table found: 9 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 73 Range 0 72 Init 0 fixed
    // Parameter 1 added: Name obj1 Type Ljava/lang/Object; At 0 73 Range 0 72
    // Init 0
    // Parameter 2 added: Name flag Type Z At 0 73 Range 0 72 Init 0
    // Parameter 3 added: Name flag1 Type Z At 0 73 Range 0 72 Init 0
    // RetValue 4 added: Name <returnValue> Type V At 0 73 Range 0 72 Init 0
    // fixed
    public void setInput(Object obj1, boolean flag, boolean flag1) {
        obj = null;
        info = null;
        if (helper != null) {
            helper.setInput(null);
            helper = null;
        }
        if (obj1 != null && !(obj1 instanceof ImageInputStream)) {
            throw new IllegalArgumentException(
                    "input is not an ImageInputStream nor a StorageSCP, nor a ReceivedObject");
        } else {
            super.setInput(obj1, flag, flag1);
            return;
        }
    }

    // Decompiling method: reset Signature: ()V
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 19 bytes, Code offset: 7899
    // Line Number Table found: 3 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 19 Range 0 18 Init 0 fixed
    // RetValue 1 added: Name <returnValue> Type V At 0 19 Range 0 18 Init 0
    // fixed
    public void reset() {
        super.reset();
        if (helper != null)
            helper.reset();
        /* return; */
    }

    // Decompiling method: dispose Signature: ()V
    // Max stack: 4, #locals: 1, #params: 1
    // Code length: 22 bytes, Code offset: 7964
    // Line Number Table found: 3 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 22 Range 0 21 Init 0 fixed
    // RetValue 1 added: Name <returnValue> Type V At 0 22 Range 0 21 Init 0
    // fixed
    public void dispose() {
        if (helper != null)
            helper.dispose();
        setInput(null, false, false);
        /* return; */
    }

    // Decompiling method: addIIOReadWarningListener Signature:
    // (Ljavax/imageio/event/IIOReadWarningListener;)V
    // Max stack: 2, #locals: 2, #params: 2
    // Code length: 21 bytes, Code offset: 8032
    // Line Number Table found: 4 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 21 Range 0 20 Init 0 fixed
    // Parameter 1 added: Name iioreadwarninglistener Type
    // Ljavax/imageio/event/IIOReadWarningListener; At 0 21 Range 0 20 Init 0
    // RetValue 2 added: Name <returnValue> Type V At 0 21 Range 0 20 Init 0
    // fixed
    public void addIIOReadWarningListener(
            IIOReadWarningListener iioreadwarninglistener) {
        super.addIIOReadWarningListener(iioreadwarninglistener);
        if (helper != null)
            helper.addIIOReadWarningListener(iioreadwarninglistener);
        /* return; */
    }

    // Decompiling method: addIIOReadProgressListener Signature:
    // (Ljavax/imageio/event/IIOReadProgressListener;)V
    // Max stack: 2, #locals: 2, #params: 2
    // Code length: 21 bytes, Code offset: 8103
    // Line Number Table found: 4 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 21 Range 0 20 Init 0 fixed
    // Parameter 1 added: Name iioreadprogresslistener Type
    // Ljavax/imageio/event/IIOReadProgressListener; At 0 21 Range 0 20 Init 0
    // RetValue 2 added: Name <returnValue> Type V At 0 21 Range 0 20 Init 0
    // fixed
    public void addIIOReadProgressListener(
            IIOReadProgressListener iioreadprogresslistener) {
        super.addIIOReadProgressListener(iioreadprogresslistener);
        if (helper != null)
            helper.addIIOReadProgressListener(iioreadprogresslistener);
        /* return; */
    }

    // Decompiling method: addIIOReadUpdateListener Signature:
    // (Ljavax/imageio/event/IIOReadUpdateListener;)V
    // Max stack: 2, #locals: 2, #params: 2
    // Code length: 21 bytes, Code offset: 8174
    // Line Number Table found: 4 entries
    // Parameter 0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReader;
    // At 0 21 Range 0 20 Init 0 fixed
    // Parameter 1 added: Name iioreadupdatelistener Type
    // Ljavax/imageio/event/IIOReadUpdateListener; At 0 21 Range 0 20 Init 0
    // RetValue 2 added: Name <returnValue> Type V At 0 21 Range 0 20 Init 0
    // fixed
    public void addIIOReadUpdateListener(
            IIOReadUpdateListener iioreadupdatelistener) {
        super.addIIOReadUpdateListener(iioreadupdatelistener);
        if (helper != null)
            helper.addIIOReadUpdateListener(iioreadupdatelistener);
        /* return; */
    }

    // Decompiling method: access$000 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;Ljava/lang/String;)V
    // Max stack: 2, #locals: 2, #params: 2
    // Code length: 6 bytes, Code offset: 8245
    // Line Number Table found: 1 entries

    /*
     * static void access$000(DicomReader dicomreader, String s) {
     * dicomreader.processWarningOccurred(s); return; }
     *  
     */

    // Decompiling method: access$102 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;Ljavax/imageio/ImageReader;)Ljavax/imageio/ImageReader;
    // Max stack: 3, #locals: 2, #params: 2
    // Code length: 7 bytes, Code offset: 8365
    // Line Number Table found: 1 entries
    /*
     * static ImageReader access$102(DicomReader dicomreader, ImageReader
     * imagereader) { return dicomreader.helper = imagereader; }
     *  
     */

    // Decompiling method: access$100 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;)Ljavax/imageio/ImageReader;
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 5 bytes, Code offset: 8416
    // Line Number Table found: 1 entries
    /*
     * static ImageReader access$100(DicomReader dicomreader) { return
     * dicomreader.helper; }
     *  
     */

    // Decompiling method: access$200 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;)Ljava/util/List;
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 5 bytes, Code offset: 8465
    // Line Number Table found: 1 entries
    /*
     * static java.util.List access$200(DicomReader dicomreader) { return
     * dicomreader.warningListeners; }
     *  
     */

    // Decompiling method: access$300 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;)Ljava/util/List;
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 5 bytes, Code offset: 8514
    // Line Number Table found: 1 entries
    /*
     * static java.util.List access$300(DicomReader dicomreader) { return
     * dicomreader.warningListeners; }
     *  
     */

    // Decompiling method: access$400 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;)Ljava/util/List;
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 5 bytes, Code offset: 8563
    // Line Number Table found: 1 entries
    /*
     * static java.util.List access$400(DicomReader dicomreader) { return
     * dicomreader.progressListeners; }
     *  
     */

    // Decompiling method: access$500 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;)Ljava/util/List;
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 5 bytes, Code offset: 8612
    // Line Number Table found: 1 entries
    /*
     * static java.util.List access$500(DicomReader dicomreader) { return
     * dicomreader.progressListeners; }
     *  
     */

    // Decompiling method: access$600 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;)Ljava/util/List;
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 5 bytes, Code offset: 8661
    // Line Number Table found: 1 entries
    /*
     * static java.util.List access$600(DicomReader dicomreader) { return
     * dicomreader.updateListeners; }
     *  
     */

    // Decompiling method: access$700 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;)Ljava/util/List;
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 5 bytes, Code offset: 8710
    // Line Number Table found: 1 entries
    /*
     * static java.util.List access$700(DicomReader dicomreader) { return
     * dicomreader.updateListeners; }
     *  
     */

    // Decompiling method: access$800 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;)Ljavax/imageio/stream/ImageInputStream;
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 5 bytes, Code offset: 8759
    // Line Number Table found: 1 entries
    /*
     * static ImageInputStream access$800(DicomReader dicomreader) { return
     * dicomreader.stream; }
     *  
     */

    // Decompiling method: access$900 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;I)V
    // Max stack: 2, #locals: 2, #params: 2
    // Code length: 6 bytes, Code offset: 8808
    // Line Number Table found: 1 entries
    /*
     * static void access$900(DicomReader dicomreader, int i) {
     * dicomreader.processImageStarted(i); return; }
     *  
     */

    // Decompiling method: access$1000 Signature:
    // (Lfr/apteryx/imageio/dicom/DicomReader;)V
    // Max stack: 1, #locals: 1, #params: 1
    // Code length: 5 bytes, Code offset: 8858
    // Line Number Table found: 1 entries
    /*
     * static void access$1000(DicomReader dicomreader) {
     * dicomreader.processImageComplete(); return; }
     *  
     */
}
