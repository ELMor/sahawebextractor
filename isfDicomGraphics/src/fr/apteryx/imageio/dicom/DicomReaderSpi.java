//Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: fieldsfirst debugmode 
//Source File Name:   DicomReaderSpi.java
//Class Version:      48.0

package fr.apteryx.imageio.dicom;

import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;
import javax.imageio.ImageReader;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

//Referenced classes of package fr.apteryx.imageio.dicom:
//         StorageSCP, ReceivedObject, DicomReader

//flag ACC_SUPER is set
public final class DicomReaderSpi extends ImageReaderSpi
{
 // Constants:          166
 // Interfaces:         0
 // Fields:             5
 // Methods:            7
 // Class Attributes:   1


 private static final byte HEADER[] = "DICM".getBytes();
 private static final int PREAMBLE = 128;
 /* static Class class$javax$imageio$stream$ImageInputStream; */ /* synthetic field */
 /* static Class class$fr$apteryx$imageio$dicom$StorageSCP; */ /* synthetic field */
 /* static Class class$fr$apteryx$imageio$dicom$ReceivedObject; */ /* synthetic field */

 // Decompiling method: <init>  Signature: ()V
 // Max stack: 6, #locals: 1, #params: 1
 // Code length: 172 bytes, Code offset: 2258
 // Line Number Table found: 13 entries
 // Parameter  0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReaderSpi; At 0 172 Range 0 171 Init 0 fixed
 // RetValue   1 added: Name <returnValue> Type V At 0 172 Range 0 171 Init 0 fixed
 public DicomReaderSpi()
 {
     /* super(); */
     vendorName = "Apteryx";
     version = "1.08pre";
     names = (new String[] {
         "dicom"
     });
     MIMETypes = (new String[] {
         "application/dicom"
     });
     nativeStreamMetadataFormatClassName = "DicomMetadataFormat";
     nativeStreamMetadataFormatName = "fr_apteryx_imageio_dicom_1.0";
     pluginClassName = "fr.apteryx.imageio.dicom.DicomReader";
     suffixes = (new String[] {
         "dcm"
     });
     supportsStandardStreamMetadataFormat = true;
     inputTypes = (new Class[] {
         javax.imageio.stream.ImageInputStream.class
     });
     writerSpiNames = (new String[] {
         "fr.apteryx.imageio.dicom.DicomWriterSpi"
     });
     /* return; */
 }

 // Decompiling method: getDescription  Signature: (Ljava/util/Locale;)Ljava/lang/String;
 // Max stack: 1, #locals: 2, #params: 2
 // Code length: 3 bytes, Code offset: 2516
 // Line Number Table found: 1 entries
 // Parameter  0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReaderSpi; At 0 3 Range 0 2 Init 0 fixed
 // Parameter  1 added: Name locale Type Ljava/util/Locale; At 0 3 Range 0 2 Init 0
 // RetValue   2 added: Name <returnValue> Type Ljava/lang/String; At 0 3 Range 0 2 Init 0 fixed
 public String getDescription(Locale locale)
 {
     return "DICOM V3.0 reader";
 }

 // Decompiling method: readHeader  Signature: (Ljavax/imageio/stream/ImageInputStream;)Z
 // Max stack: 2, #locals: 2, #params: 1
 // Code length: 39 bytes, Code offset: 2557
 // Line Number Table found: 4 entries
 // Parameter  0 added: Name imageinputstream Type Ljavax/imageio/stream/ImageInputStream; At 0 39 Range 0 38 Init 0
 // RetValue   2 added: Name <returnValue> Type Z At 0 39 Range 0 38 Init 0 fixed
 // LocalVar   1 added: Name abyte0 Type [B At 23 9 Range 23 31 Init 23
 static boolean readHeader(ImageInputStream imageinputstream)
     throws IOException
 {
     if(imageinputstream.skipBytes(128) != 128)
     {
         return false;
     } else
     {
         byte abyte0[] = new byte[HEADER.length];
         imageinputstream.readFully(abyte0);
         return Arrays.equals(abyte0, HEADER);
     }
 }

 // Decompiling method: canDecodeInput  Signature: (Ljava/lang/Object;)Z
 // Max stack: 1, #locals: 6, #params: 2
 // Code length: 73 bytes, Code offset: 2656
 // Exception table: 4 entries
 //           start  36 end 41 handler 49 type IOException
 //           start  36 end 41 handler 62 type any
 //           start  49 end 53 handler 62 type any
 //           start  62 end 64 handler 62 type any
 // Line Number Table found: 14 entries
 // Parameter  0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReaderSpi; At 0 73 Range 0 72 Init 0 fixed
 // Parameter  1 added: Name obj Type Ljava/lang/Object; At 0 73 Range 0 72 Init 0
 // RetValue   6 added: Name <returnValue> Type Z At 0 73 Range 0 72 Init 0 fixed
 // Fixed Exception table: 4 entries
 //           start  36 end 41 handler 49 type IOException
 //           start  36 end 41 handler 62 type any
 //           start  49 end 53 handler 62 type any
 // LocalVar   2 added: Name imageinputstream Type Ljavax/imageio/stream/ImageInputStream; At 29 36 Range 29 64 Init 29
 // LocalVar   3 added: Name flag Type Z At 40 8 Range 40 47 Init 40
 // LocalVar   3 added: Name ioexception Type Ljava/io/IOException; At 49 1 Range 49 49 Init 49
 // LocalVar   4 added: Name flag1 Type Z At 51 10 Range 51 60 Init 51
 // LocalVar   5 added: Name exception Type Ljava.lang.Exception; At 62 10 Range 62 71 Init 62
 public boolean canDecodeInput(Object obj)
     throws IOException
 {
     ImageInputStream imageinputstream;
     if(!(obj instanceof ImageInputStream))
         return false;
     imageinputstream = (ImageInputStream)obj;
     imageinputstream.mark();
     boolean flag = readHeader(imageinputstream);
     imageinputstream.reset();
     return flag;
 }

 // Decompiling method: createReaderInstance  Signature: (Ljava/lang/Object;)Ljavax/imageio/ImageReader;
 // Max stack: 3, #locals: 2, #params: 2
 // Code length: 9 bytes, Code offset: 2861
 // Line Number Table found: 1 entries
 // Parameter  0 added: Name this Type Lfr/apteryx/imageio/dicom/DicomReaderSpi; At 0 9 Range 0 8 Init 0 fixed
 // Parameter  1 added: Name obj Type Ljava/lang/Object; At 0 9 Range 0 8 Init 0
 // RetValue   2 added: Name <returnValue> Type Ljavax/imageio/ImageReader; At 0 9 Range 0 8 Init 0 fixed
 public ImageReader createReaderInstance(Object obj)
 {
     return new DicomReader(this);
 }

 // Decompiling method: _mthclass$  Signature: (Ljava/lang/String;)Ljava/lang/Class;
 // Max stack: 2, #locals: 2, #params: 1
 // Code length: 18 bytes, Code offset: 2908
 // Exception table: 1 entries
 //           start  0 end 4 handler 5 type ClassNotFoundException
 // Line Number Table found: 1 entries
 // Parameter  0 added: Name s Type Ljava/lang/String; At 0 18 Range 0 17 Init 0
 // RetValue   2 added: Name <returnValue> Type Ljava/lang/Class; At 0 18 Range 0 17 Init 0 fixed
 // LocalVar   1 added: Name classnotfoundexception Type Ljava/lang/ClassNotFoundException; At 5 9 Range 5 13 Init 5
/*
 static Class _mthclass$(String s)
 {
     return Class.forName(s);
     ClassNotFoundException classnotfoundexception;
     classnotfoundexception;
     throw (new NoClassDefFoundError()).initCause(classnotfoundexception);
 }

*/

 // Decompiling method: <clinit>  Signature: ()V
 // Max stack: 1, #locals: 0, #params: 0
 // Code length: 9 bytes, Code offset: 2978
 // Line Number Table found: 1 entries
 // RetValue   0 added: Name <returnValue> Type V At 0 9 Range 0 8 Init 0 fixed
 static 
 {
     /* HEADER = "DICM".getBytes(); */
 }
}
