/* Cloner - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

abstract class Cloner
{
    /*synthetic*/ static Class class$java$util$Vector;
    /*synthetic*/ static Class class$java$util$ArrayList;
    /*synthetic*/ static Class class$fr$apteryx$imageio$dicom$DataSet;
    
    static void cloneCollection(Collection collection,
				Collection collection_0_) {
	collection_0_.clear();
	Iterator iterator = collection.iterator();
	while (iterator.hasNext())
	    collection_0_.add(cloneElement(iterator.next()));
    }
    
    static Object cloneElement(Object object) {
	if (object == null)
	    return null;
	if (object.getClass().isArray()) {
	    int i = Array.getLength(object);
	    Object object_1_
		= Array.newInstance(object.getClass().getComponentType(), i);
	    for (int i_2_ = 0; i_2_ < i; i_2_++)
		Array.set(object_1_, i_2_,
			  cloneElement(Array.get(object, i_2_)));
	    return object_1_;
	}
	if (object instanceof Vector ) {
	    Vector vector = new Vector(((Vector) object).size());
	    cloneCollection((Collection) object, vector);
	    return vector;
	}
	if (object instanceof ArrayList) {
	    ArrayList arraylist = new ArrayList(((ArrayList) object).size());
	    cloneCollection((Collection) object, arraylist);
	    return arraylist;
	}
	if (object instanceof DataSet) {
	    DataSet dataset = new DataSet();
	    Iterator iterator = ((DataSet) object).entrySet().iterator();
	    while (iterator.hasNext()) {
		Map.Entry entry = (Map.Entry) iterator.next();
		dataset.put(entry.getKey(), cloneElement(entry.getValue()));
	    }
	    return dataset;
	}
	if (object instanceof Number || object instanceof Boolean
	    || object instanceof String || object instanceof File
	    || object instanceof Class)
	    return object;
	if (object instanceof BitSet)
	    return ((BitSet) object).clone();
	return object;
    }
    
}
