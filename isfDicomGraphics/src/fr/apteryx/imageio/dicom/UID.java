/* UID - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

public abstract class UID
{
    public static final String VerificationSOPClass = "1.2.840.10008.1.1";
    public static final String DicomApplicationContext
	= "1.2.840.10008.3.1.1.1";
    public static final String MediaStorageDirectoryStorage
	= "1.2.840.10008.1.3.10";
    public static final String HardCopyColorImageStorage
	= "1.2.840.10008.5.1.1.29";
    public static final String HardCopyGrayscaleImageStorage
	= "1.2.840.10008.5.1.1.30";
    public static final String ComputedRadiographyImageStorage
	= "1.2.840.10008.5.1.4.1.1.1";
    public static final String DigitalXRayImageStorageForPresentation
	= "1.2.840.10008.5.1.4.1.1.1.1";
    public static final String DigitalXRayImageStorageForProcessing
	= "1.2.840.10008.5.1.4.1.1.1.1.1";
    public static final String DigitalMammographyImageStorageForPresentation
	= "1.2.840.10008.5.1.4.1.1.1.2";
    public static final String DigitalMammographyImageStorageForProcessing
	= "1.2.840.10008.5.1.4.1.1.1.2.1";
    public static final String DigitalIntraOralXRayImageStorageForPresentation
	= "1.2.840.10008.5.1.4.1.1.1.2";
    public static final String DigitalIntraOralXRayImageStorageForProcessing
	= "1.2.840.10008.5.1.4.1.1.1.2.1";
    public static final String CTImageStorage = "1.2.840.10008.5.1.4.1.1.2";
    public static final String EnhancedCTImageStorage
	= "1.2.840.10008.5.1.4.1.1.2.1";
    public static final String Retired_UltraSoundMultiFrameImageStorage
	= "1.2.840.10008.5.1.4.1.1.3";
    public static final String UltraSoundMultiFrameImageStorage
	= "1.2.840.10008.5.1.4.1.1.3.1";
    public static final String MRImageStorage = "1.2.840.10008.5.1.4.1.1.4";
    public static final String EnhancedMRImageStorage
	= "1.2.840.10008.5.1.4.1.1.4.1";
    public static final String Retired_NuclearMedicineImageStorage
	= "1.2.840.10008.5.1.4.1.1.5";
    public static final String Retired_UltraSoundImageStorage
	= "1.2.840.10008.5.1.4.1.1.6";
    public static final String UltraSoundImageStorage
	= "1.2.840.10008.5.1.4.1.1.6.1";
    public static final String SecondaryCaptureImageStorage
	= "1.2.840.10008.5.1.4.1.1.7";
    public static final String MFSingleBitSecondaryCaptureImageStorage
	= "1.2.840.10008.5.1.4.1.1.7.1";
    public static final String MFGrayscaleByteSecondaryCaptureImageStorage
	= "1.2.840.10008.5.1.4.1.1.7.2";
    public static final String MFGrayscaleWordSecondaryCaptureImageStorage
	= "1.2.840.10008.5.1.4.1.1.7.3";
    public static final String MFTrueColorSecondaryCaptureImageStorage
	= "1.2.840.10008.5.1.4.1.1.7.4";
    public static final String XRayAngiographicImageStorage
	= "1.2.840.10008.5.1.4.1.1.12.1";
    public static final String XRayRadiofluoroscopicImageStorage
	= "1.2.840.10008.5.1.4.1.1.12.2";
    public static final String XRayAngiographicBiPlaneImageStorage
	= "1.2.840.10008.5.1.4.1.1.12.3";
    public static final String NuclearMedicineImageStorage
	= "1.2.840.10008.5.1.4.1.1.20";
    public static final String VLEndoscopicImageStorage
	= "1.2.840.10008.5.1.4.1.1.77.1.1";
    public static final String VLMicroscopicImageStorage
	= "1.2.840.10008.5.1.4.1.1.77.1.2";
    public static final String VLSlideCoordinatesMicroscopicImageStorage
	= "1.2.840.10008.5.1.4.1.1.77.1.3";
    public static final String VLPhotographicImageStorage
	= "1.2.840.10008.5.1.4.1.1.77.1.4";
    public static final String OphthalmicPhotography8BitImageStorage
	= "1.2.840.10008.5.1.4.1.1.77.1.5.1";
    public static final String OphthalmicPhotography16BitImageStorage
	= "1.2.840.10008.5.1.4.1.1.77.1.5.2";
    public static final String PositronEmissionTomographyImageStorage
	= "1.2.840.10008.5.1.4.1.1.128";
    public static final String RTImageStorage
	= "1.2.840.10008.5.1.4.1.1.481.1";
    public static final String RTDoseStorage = "1.2.840.10008.5.1.4.1.1.481.2";
    public static final String PatientRootQueryRetrieveInformationModelFind
	= "1.2.840.10008.5.1.4.1.2.1.1";
    public static final String PatientRootQueryRetrieveInformationModelMove
	= "1.2.840.10008.5.1.4.1.2.1.2";
    public static final String PatientRootQueryRetrieveInformationModelGet
	= "1.2.840.10008.5.1.4.1.2.1.3";
    public static final String StudyRootQueryRetrieveInformationModelFind
	= "1.2.840.10008.5.1.4.1.2.2.1";
    public static final String StudyRootQueryRetrieveInformationModelMove
	= "1.2.840.10008.5.1.4.1.2.2.2";
    public static final String StudyRootQueryRetrieveInformationModelGet
	= "1.2.840.10008.5.1.4.1.2.2.3";
    public static final String PatientStudyOnlyQueryRetrieveInformationModelFind
	= "1.2.840.10008.5.1.4.1.2.3.1";
    public static final String PatientStudyOnlyQueryRetrieveInformationModelMove
	= "1.2.840.10008.5.1.4.1.2.3.2";
    public static final String PatientStudyOnlyQueryRetrieveInformationModelGet
	= "1.2.840.10008.5.1.4.1.2.3.3";
    public static final String ModalityWorklistInformationModelFind
	= "1.2.840.10008.5.1.4.31";
    public static final String GeneralPurposeWorklistInformationModelFind
	= "1.2.840.10008.5.1.4.32.1";
    public static final String ImplicitLittleEndianTS = "1.2.840.10008.1.2";
    public static final String ExplicitLittleEndianTS = "1.2.840.10008.1.2.1";
    public static final String DeflatedExplicitLittleEndianTS
	= "1.2.840.10008.1.2.1.99";
    public static final String ExplicitBigEndianTS = "1.2.840.10008.1.2.2";
    public static final String JPEG_prefix = "1.2.840.10008.1.2.4.";
    public static final String JPEGBaseline_1TS = "1.2.840.10008.1.2.4.50";
    public static final String JPEGExtended_2_4TS = "1.2.840.10008.1.2.4.51";
    public static final String JPEGProgressive_10_12TS
	= "1.2.840.10008.1.2.4.56";
    public static final String JPEGLossless_14TS = "1.2.840.10008.1.2.4.57";
    public static final String JPEGLossless_15TS = "1.2.840.10008.1.2.4.58";
    public static final String JPEGLossless_14SV1TS = "1.2.840.10008.1.2.4.70";
    public static final String JPEGLS_LosslessTS = "1.2.840.10008.1.2.4.80";
    public static final String JPEGLS_NearLosslessTS
	= "1.2.840.10008.1.2.4.81";
    public static final String JPEG2000_LosslessTS = "1.2.840.10008.1.2.4.90";
    public static final String JPEG2000TS = "1.2.840.10008.1.2.4.91";
    public static final String RLECompressionTS = "1.2.840.10008.1.2.5";
    public static final String UTCSynchonization = "1.2.840.10008.15.1.1";
    static final String MyPrivateRecordUID = "1.2.826.0.1.3680043.2.228.0.1";
    static final String MyImplementationClassUID
	= "1.2.826.0.1.3680043.2.228.0.2";
    static final int SOPInstanceMagic = 1;
    static final int StudyInstanceMagic = 2;
    static final int SeriesInstanceMagic = 3;
    static long lasttime;
    
    static synchronized String createUID(int i) {
	return "0.0.0.0.0";
    }
    
    UID() {
	/* empty */
    }
}
