/* Day - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.text.DateFormat;
import java.util.Date;

public class Day extends Date
{
    public Day() {
	this(new Date());
    }
    
    public Day(Date date) {
	super(date.getTime());
    }
    
    public String toString() {
	return DateFormat.getDateInstance().format(this);
    }
}
