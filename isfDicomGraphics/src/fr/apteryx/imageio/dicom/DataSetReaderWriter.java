/* DataSetReaderWriter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.text.SimpleDateFormat;
import java.util.Locale;

abstract class DataSetReaderWriter
{
    static final String VM_DELIM = "\\";
    private static ThreadLocal formatsTL = new ThreadLocal() {
	protected Object initialValue() {
	    return new Formats();
	}
    };
    static final Formats formats = (Formats) formatsTL.get();
    
    static class Formats
    {
	SimpleDateFormat dicom3DA
	    = new SimpleDateFormat("yyyyMMdd", Locale.US);
	SimpleDateFormat compatDA
	    = new SimpleDateFormat("yyyy.MM.dd", Locale.US);
	SimpleDateFormat DTWrite
	    = new SimpleDateFormat("yyyyMMddhhmmss.SSS000Z", Locale.US);
	SimpleDateFormat DTRead
	    = new SimpleDateFormat("yyyyMMddhhmmss.SSSZ", Locale.US);
	SimpleDateFormat dicom3TM
	    = new SimpleDateFormat("HHmmss.SSS", Locale.US);
	SimpleDateFormat compatTM
	    = new SimpleDateFormat("HH:mm:ss.SSS", Locale.US);
    }
}
