/*
 * Created on 03-mar-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package isf.fsmon.actions.syncDiff;

import isf.fsmon.Action;
import isf.fsmon.Manager.Nodo;

import java.io.File;

/**
 * @author UF371231
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Delete extends Action {

	/* (non-Javadoc)
	 * @see isf.fsmon.Action#perform(isf.fsmon.Manager.Nodo)
	 */
	public void perform(Nodo newFile) throws Exception {
		String destName=Util.getCompound(newFile);
		File destFile=new File(destName);
		if(destFile.exists())
			destFile.delete();
		
	}

}
