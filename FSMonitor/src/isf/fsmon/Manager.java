/*
 * Created on 01-feb-2005
 *
 */
package isf.fsmon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

/**
 * @author UF371231
 *  
 */
public class Manager extends Vector {

	public class Nodo extends Hashtable {

		public boolean isdir = false;

		public String name = null;

		protected Object chkVal = null;
		
		protected NodoRaiz nodoRaiz=null;

		protected int detectedAction = 0; //1:A�adido, 2:Modificado, 3:Borrar

		protected void calculateChkVal(Check chk) throws Exception {
			File file = new File(name);
			name = file.getCanonicalPath();
			chkVal = chk.init(file);
			if (file.isDirectory()) {
				isdir = true;
				readDir(file,chk);
			}
		}

		private boolean changed(Check checker) {
			if (checker.init(new File(name)).equals(chkVal))
				return true;
			return false;
		}

		private void readDir(File f, Check chk) throws Exception {
			File entradas[] = f.listFiles();
			for (int i = 0; i < entradas.length; i++) {
				Nodo nuevo=new Nodo(nodoRaiz,entradas[i].getCanonicalPath());
				nuevo.calculateChkVal(chk);
				put(nuevo.name,nuevo);
			}
		}

		public String getCanonicalName() {
			return name;
		}

		public String getRelativePath(){
			if(nodoRaiz==null)
				nodoRaiz=(NodoRaiz)this;
			return name.substring(nodoRaiz.getCanonicalName().length()+1);
		}
		
		protected Nodo(NodoRaiz nr, String fileName) throws Exception {
			nodoRaiz=nr;
			name = fileName;
		}
		
	}

	private class NodoRaiz extends Nodo implements Runnable {
		
		protected Check checker = null;

		private long sleepTime = 0;

		public boolean monitorAdd = false;

		public boolean monitorDel = false;

		public boolean monitorUpd = false;

		public Action addAction = null;

		public Action delAction = null;

		public Action updAction = null;
		
		public InicializaAcciones iniAcciones= null;
		
		public NodoRaiz(Properties p, String raiz) throws Exception {
			super(null,p.getProperty(raiz + "name"));
			nodoRaiz=this;
			String chk = p.getProperty(raiz + "fileChecker",
					"isf.fsmon.check.DefaultCheck");
			checker = (Check) Class.forName(chk).newInstance();

			String aux = p.getProperty(raiz + "sleeptimeinseconds", "5");
			sleepTime = Long.parseLong(aux);

			monitorAdd = p.getProperty(raiz + "monitor.add", "0").equals("1") ? true
					: false;
			monitorDel = p.getProperty(raiz + "monitor.del", "0").equals("1") ? true
					: false;
			monitorUpd = p.getProperty(raiz + "monitor.upd", "1").equals("1") ? true
					: false;

			String action;
			action = p.getProperty(raiz + "InizializaAcciones",null);
			iniAcciones = (InicializaAcciones) Class.forName(action).newInstance();
			action = p.getProperty(raiz + "addAction","isf.fsmon.actions.DefaultAddAction");
			addAction = (Action) Class.forName(action).newInstance();
			action = p.getProperty(raiz + "delAction","isf.fsmon.actions.DefaultDelAction");
			delAction = (Action) Class.forName(action).newInstance();
			action = p.getProperty(raiz + "updAction","isf.fsmon.actions.DefaultUpdAction");
			updAction = (Action) Class.forName(action).newInstance();

			int numParams=Integer.parseInt(p.getProperty(raiz+"params","0"));
			Hashtable params=new Hashtable();
			for(int i=0;i<numParams;i++){
				String nombre=p.getProperty(raiz+"param."+(i+1)+".nombre");
				String value=p.getProperty(raiz+"param."+(i+1)+".valor");
				params.put(nombre,value);
			}
			iniAcciones.Ini(params);
			add(this);
			calculateChkVal(checker);
		}
		
		private void markFiles(NodoRaiz nr, Nodo current) throws Exception {
			//Aqui se hace el trabajo sucio.
			//He comprobado que un directorio solo cambia su fecha de
			// modificacion
			//si se borra o se a�ade un archivo. No si se modifica, pienso usar
			// esto.
			if(current.detectedAction!=0)
				return;
			
			if ((monitorDel || monitorAdd) && current.isdir) {
				File dir = new File(current.name);
				if (checker.isChanged(dir, current.chkVal)) {
					File updated[]=dir.listFiles();
					//Comprobamos los borrados
					Enumeration oldKeys=current.keys();
					while(oldKeys.hasMoreElements()){
						String oldName=(String)oldKeys.nextElement();
						Nodo hijo=((Nodo)current.get(oldName));
						hijo.detectedAction=3; //Suponemos que esta borrado
						for(int i=0;i<updated.length;i++){
							if(updated[i].getCanonicalPath().equals(oldName)){
								hijo.detectedAction=0;
								break;
							}
						}
					}
					//Comprobamos los a�adidos
					for(int i=0;i<updated.length;i++){
						String newName=updated[i].getCanonicalPath();
						if(!current.containsKey(newName)){
							Nodo nuevoNodo=new Nodo(this,newName);
							nuevoNodo.calculateChkVal(checker);
							nuevoNodo.detectedAction=1; //A�adido
							current.put(newName,nuevoNodo);
						}
					}
				}
			}

			if (monitorUpd) {
				if (!current.isdir) { //Update de archivo
					if (checker.isChanged(new File(current.name), current.chkVal)) {
						current.detectedAction = 2;
					}
				}
			}
			if (current.isdir) {
				Enumeration enum=current.elements();
				while(enum.hasMoreElements()){
					Nodo hijo=(Nodo)enum.nextElement();
					markFiles(nr, hijo);
				}
			}
		}

		/**
		 * @param padre
		 * @param hijo
		 */
		private boolean ejecutaAccionesEnNodo(Nodo padre, Nodo hijo) throws Exception {
			switch (hijo.detectedAction) {
			case 1: //A�adido
				addAction.perform(hijo);
				hijo.detectedAction=0;
				break;
			case 2: //Modificado
				updAction.perform(hijo);
				hijo.chkVal = checker.init(new File(hijo.name));
				hijo.detectedAction=0;
				break;
			case 3: //Borrado
				delAction.perform(hijo);
				if(padre!=null){
					padre.remove(hijo.name);
				}else{
					return true;
				}
				hijo.detectedAction=0;
				break;
			}
			if (hijo.isdir) {
				Enumeration n=hijo.elements();
				while(n.hasMoreElements()){
					Nodo nieto=(Nodo)n.nextElement();
					ejecutaAccionesEnNodo(hijo,nieto);
				}
			}
			return false;
		}

		private void calculateChkVal() throws Exception {
			super.calculateChkVal(checker);
		}

		public void run() {
			while (true) {
				try {
					markFiles(this, this);
					if(ejecutaAccionesEnNodo(null,this)){
						// Root ha sido borrado
						return;
					}
					Thread.sleep(1000 * sleepTime);
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}

	public static void main(String args[]) {
		if (args.length != 1) {
			System.out.println("missing file.properties");
		} else {
			Properties p = new Properties();
			try {
				p.load(new FileInputStream(args[0]));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
				return;
			} catch (IOException e1) {
				e1.printStackTrace();
				return;
			}
			int monNumber = Integer.parseInt(p.getProperty("fsmon.dir", "0"));
			Manager monitorManager = new Manager();
			for (int i = 0; i < monNumber; i++) {
				try {
					monitorManager.new NodoRaiz(p, "fsmon.dir." + (i + 1) + ".");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			for (int i = 0; i < monitorManager.size(); i++) {
				new Thread((Runnable) monitorManager.elementAt(i)).start();
			}
		}
	}
}