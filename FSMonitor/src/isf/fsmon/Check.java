/*
 * Created on 01-feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package isf.fsmon;

import java.io.File;

/**
 * @author UF371231
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface Check {
	//Retorna un objeto usado para representar el fichero.
	//Normalmente dicho objeto contendr� el tama�o, fecha/hora de modificacion, CRC...
	public Object init(File f);
	public boolean isChanged(File f, Object oldCheck);
}
