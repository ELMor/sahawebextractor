/*
 * Created on 01-feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package isf.fsmon.check;


/**
 * @author UF371231
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FileAttrHolder {
	protected long creationDate=0;
	protected long modificationDate=0;
	
	public FileAttrHolder(long cd, long md){
		creationDate=cd;
		modificationDate=md;
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof FileAttrHolder){
			if(creationDate==0 || modificationDate==0)
				return false;
			FileAttrHolder other=(FileAttrHolder)obj;
			if(other.modificationDate==this.modificationDate){
				if(other.creationDate==this.creationDate){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
