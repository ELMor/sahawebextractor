/*
 * Created on 22-jul-2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package f8.keyboard;

import f8.Core;
import f8.ICalc;
import f8.commands.Command;
import f8.commands.Operation;
import f8.exceptions.F8Exception;
import f8.objects.types.Proc;
import f8.platform.Calc;

/**
 * @author elinares
 * 
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class Tecla implements TeclaVirtual {
	Menu menu;

	String emision = null;

	Operation op = null;

	boolean isVarSolver = false;

	int backSpaces = 0;

	public Tecla(Menu m) {
		menu = m;
	}

	public Tecla(String nombre) {
		menu = new Menu(nombre);
	}

	public Tecla(String nombre, int bs) {
		this(nombre);
		backSpaces = bs;
	}

	public Tecla(String nombre, String emit) {
		this(nombre);
		emision = emit;
	}

	public Tecla(String nombre, Operation _op) {
		this(nombre);
		op = _op;
	}

	public Tecla(String nombre, Operation _op, boolean isvs) {
		this(nombre, _op);
		isVarSolver = isvs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.VirtualKey#isDir()
	 */
	public boolean isDir() {
		return menu.size() > 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.VirtualKey#getTitle(byte, byte)
	 */
	public String getTitle() {
		return menu.getTitle();
	}

	public void setTitle(String tit) {
		menu.setTitle(tit);
	}

	public void setMenu() {
		// Core.setMenu(1, menu, null, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.VirtualKey#pressed(f8.ui.CalcInterface, byte, byte)
	 */
	public void pressed() throws F8Exception {

		try {
			Calc.ref.setBusy();
			if (op != null) {
				op.exec(this);
			} else {
				String em = getEmit();
				Command s = Core.lookup(em);
				if (s != null) {
					if (Core.isSetting(ICalc.ALG_MOD)) {
						if (s.isAlgebraic()) {
							if (s.isInfix()) {
								if (s.isOperator()) {
									Calc.ref.emit(em, 0);
								} else {
									Calc.ref.emit(" " + em + " ", backSpaces);
								}
							} else {
								Calc.ref.emit(em + "()", 1);
							}
						} else {
							if (this instanceof TeclaVar) {
								Calc.ref.emit(em, 0);
							} else {
								Calc.ref.flush();
								s.exec(null);
							}
						}
					} else if (Core.isSetting(ICalc.PRG_MOD)) {
						Calc.ref.emit(" " + em + " ", 0);
					} else if (isDir()) {
						setMenu();
					} else {
						Core.saveStack();
						Calc.ref.flush();
						if (s instanceof Proc) {
							s.exec(null);
						} else {
							s.store();
						}
					}
				} else {
					Calc.ref.emit(em, backSpaces);
				}
			}
		} catch (F8Exception e) {
			throw e;
		} finally {
			Calc.ref.clearBusy();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#getEmit()
	 */
	public String getEmit() {
		String name = (emision == null) ? getTitle() : emision;
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#isVarSolver()
	 */
	public boolean isVarSolver() {
		return isVarSolver;
	}
}
