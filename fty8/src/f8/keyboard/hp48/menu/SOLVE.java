/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import java.util.Vector;

import antlr.collections.AST;
import f8.Core;
import f8.apps.solver.EqSolv;
import f8.commands.Operation;
import f8.commands.alg.CommandSequence;
import f8.commands.math.mkl;
import f8.commands.storage.STO;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.exceptions.UndefinedException;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.keyboard.Menu;
import f8.keyboard.Tecla;
import f8.keyboard.TeclaVirtual;
import f8.objects.Stackable;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;
import f8.platform.Calc;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class SOLVE extends Menu implements ObjectParserTokenTypes {
	public SOLVE() throws F8Exception {
		super("SOLVE");

		try {
			appendKey(solverWithEQDefined());
		} catch (UndefinedException e) {
			Calc.ref.output("No EQ, store one!");
			appendKey(solverWithoutEQDefined());
		}

		appendKey(new Tecla("ROOT", new Operation() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see f8.kernel.Operation#exec()
			 */
			public void exec(Object extern) {
				// TODO Auto-generated method stub
			}
		}));
		appendKey(new Tecla("NEW", new Operation() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see f8.kernel.Operation#exec()
			 */
			public void exec(Object extern) {
				// TODO Auto-generated method stub
			}
		}));
		appendKey(new Tecla("EDEQ", new Operation() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see f8.kernel.Operation#exec()
			 */
			public void exec(Object extern) {
				// TODO Auto-generated method stub
			}
		}));
		appendKey(new Tecla("STEQ"));
		appendKey(new Tecla("CAT", new Operation() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see f8.kernel.Operation#exec()
			 */
			public void exec(Object extern) {
				// TODO Auto-generated method stub
			}
		}));
	}

	private TeclaVirtual solverWithoutEQDefined() throws F8Exception {
		return new Tecla("SOLVR", new Operation() {

			public void exec(Object extern) throws F8Exception {
				
				Stackable eq = (Stackable) Core.lookup("EQ");
				if (eq != null) {
					Menu solve=new SOLVE();
					Calc.ref.setDynMenu(solve);
					solve.get(0).pressed();
				}
			}
		});
	}

	private Menu solverWithEQDefined() throws F8Exception {
		Menu m = new Menu("SOLVR");
		
		try {
			Stackable eq = (Stackable) Core.lookup("EQ");
			if (eq == null) {
				throw new UndefinedException(SOLVE.this);
			}
			if (eq instanceof InfixExp) {
				InfixExp ieq = (InfixExp) eq;
				Vector v = mkl.incogOf(ieq.getAST());
				for (int i = 0; i < v.size(); i++) {
					String vName = (String) v.elementAt(i);
					m.appendKey(new Tecla(vName,new varSolverKey(vName), true));
				}
				if (ieq.getAST().getType() == ASSIGN) {
					AST fc = ieq.getAST().getFirstChild();
					m.appendKey(new Tecla("LEFT", new expSolverKey("LEFT", fc)));
					m.appendKey(new Tecla("RIGHT", new expSolverKey("RIGHT", fc.getNextSibling())));
				} else {
					m.appendKey(new Tecla("EXPR", new expSolverKey("EXPR", ieq.getAST())));
				}
				return m;
			} else {
				throw new BadArgumentTypeException(SOLVE.this);
			}
		} catch (ClassCastException k) {
			throw new BadArgumentTypeException(SOLVE.this);
		}
	}

	public class varSolverKey extends Operation {
		String nombre;

		public varSolverKey(String name) {
			nombre = name;
		}

		public void exec(Object extern) throws F8Exception {
			Calc.ref.flush();
			switch (Calc.ref.getShiftMode()) {
			case 0: // Normal
				if (Core.check(1)) {
					Core.push(new Literal(nombre));
					new STO().exec(null);
					//Core.temporaryLabels("", nombre + ":" + val);
				} else {
					throw new TooFewArgumentsException(SOLVE.this);
				}
				break;
			case 1:
				// Aqui se inicia el solver
				EqSolv.solve((InfixExp) Core.lookup("EQ"), nombre);
				break;
			case 2:
				//RCL
				Stackable k = (Stackable) Core.lookup(nombre);
				if (k == null) {
					Core.push(new Literal(nombre));
				} else {
					Core.push(k);
				}
				break;
			}
		}
	}

	private class expSolverKey extends Operation {
		AST exp;

		String nombre;

		public expSolverKey(String name, AST e) {
			exp = e;
			nombre = name;
		}

		public void exec(Object extern) throws F8Exception {
			
			CommandSequence run = InfixExp.decode(exp);
			run.exec(null);
			Core.peek().setTag(nombre);
		}
	}
}
