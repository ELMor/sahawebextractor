package f8.keyboard.hp48.menu;

import f8.Core;
import f8.commands.Operation;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.BadArgumentValueException;
import f8.exceptions.F8Exception;
import f8.keyboard.Menu;
import f8.keyboard.Tecla;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.platform.Calc;

public class FONTS extends Menu {
	public FONTS(){
		super("Font");
		appendKey(new Tecla("pcSt",new Operation() {
			public void exec(Object extern) throws F8Exception {
				Calc.ref.flush();
				Core.check(1);
				Stackable i=Core.peek();
				if(i instanceof Double){
					Core.pop();
					int k=((Double)i).intValue();
					if(k>=40 && k<=70){
						Calc.ref.pantalla.initGeometry(k);
					}else
						throw new BadArgumentValueException((Tecla)extern);
				}else{
					throw new BadArgumentTypeException((Tecla)extern);
				}
			}
		}));
		appendKey(new Tecla("NOTIF",new Operation() {
			public void exec(Object extern) throws F8Exception {
				Calc.ref.flush();
				Core.check(1);
				Stackable i=Core.peek();
				if(i instanceof Double){
					Core.pop();
					int k=((Double)i).intValue();
					if(k>=10 && k<=14)
						Calc.ref.initFonts(k, -1,-1,-1 );
					else
						throw new BadArgumentValueException((Tecla)extern);
				}else{
					throw new BadArgumentTypeException((Tecla)extern);
				}
			}
		}));
		appendKey(new Tecla("STACK",new Operation() {
			public void exec(Object extern) throws F8Exception {
				Calc.ref.flush();
				Core.check(1);
				Stackable i=Core.peek();
				if(i instanceof Double){
					Core.pop();
					int k=((Double)i).intValue();
					if(k>=10 && k<=20)
						Calc.ref.initFonts(-1, -1,-1,k);
					else
						throw new BadArgumentValueException((Tecla)extern);
				}else{
					throw new BadArgumentTypeException((Tecla)extern);
				}
			}
		}));
		appendKey(new Tecla("MENUS",new Operation() {
			public void exec(Object extern) throws F8Exception {
				Calc.ref.flush();
				Core.check(1);
				Stackable i=Core.peek();
				if(i instanceof Double){
					Core.pop();
					int k=((Double)i).intValue();
					if(k>=10 && k<=18)
						Calc.ref.initFonts(-1,-1, k,-1);
					else
						throw new BadArgumentValueException((Tecla)extern);
				}else{
					throw new BadArgumentTypeException((Tecla)extern);
				}
			}
		}));
		appendKey(new Tecla("KEYB",new Operation() {
			public void exec(Object extern) throws F8Exception {
				Calc.ref.flush();
				Core.check(1);
				Stackable i=Core.peek();
				if(i instanceof Double){
					Core.pop();
					int k=((Double)i).intValue();
					if(k>=10 && k<=20)
						Calc.ref.initFonts(-1, k,-1,-1);
					else
						throw new BadArgumentValueException((Tecla)extern);
				}else{
					throw new BadArgumentTypeException((Tecla)extern);
				}
			}
		}));
	}
}
