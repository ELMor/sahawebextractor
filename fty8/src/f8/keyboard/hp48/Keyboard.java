package f8.keyboard.hp48;

import java.util.Vector;

import f8.Core;
import f8.ICalc;
import f8.IMachineStatus;
import f8.commands.Operation;
import f8.exceptions.F8Exception;
import f8.keyboard.ENTER;
import f8.keyboard.HKeyOperation;
import f8.keyboard.Menu;
import f8.keyboard.Tecla;
import f8.keyboard.TeclaText;
import f8.keyboard.TeclaVirtual;
import f8.keyboard.WithEnter;
import f8.keyboard.hp48.menu.MEMORY;
import f8.keyboard.hp48.menu.MODES;
import f8.keyboard.hp48.menu.MTH;
import f8.keyboard.hp48.menu.PLOT;
import f8.keyboard.hp48.menu.PRG;
import f8.keyboard.hp48.menu.SOLVE;
import f8.keyboard.hp48.menu.STAT;
import f8.keyboard.hp48.menu.SYMB;
import f8.keyboard.hp48.menu.TIME;
import f8.keyboard.hp48.menu.UNITS;
import f8.keyboard.hp48.menu.VAR;
import f8.platform.Calc;

public final class Keyboard {
	IMachineStatus ims;
	int maxTeclas = 0;
	static Vector<GrupoTeclas> grupos = new Vector<GrupoTeclas>();

	private Operation opSolve = new Operation() {
		public void exec(Object extern) {
			try {
				Calc.ref.setDynMenu(new SOLVE());
			} catch (F8Exception e) {
			}
		}
	};
	private Operation opLstSt = new Operation() {
		public void exec(Object extern) throws F8Exception {
			Core.restoreStack();
		}
	};
	private Operation opHome = new Operation() {
		public void exec(Object extern) {
			Core.homeDir();
			Calc.ref.pantalla.dynamicNeedRefresh = true;
		}
	};
	private Operation opCst = new Operation() {
		public void exec(Object extern) throws F8Exception {
			Calc.ref.setDynMenu(null);
		}
	};
	
	public Keyboard(IMachineStatus ims, int numTeclas) {
		this.ims = ims;
		maxTeclas = numTeclas;
		
		int cond[] = getAcceptStatus();
		cond[Calc.SM_KEYBNDX] = 0; // Numerico
		GrupoTeclas numerico = new GrupoTeclas(cond, new MultiTecla[] {
			new MultiTecla(new MTH(),  new WithEnter("\u0083"),new WithEnter("y\u0083x", "YROOTX")),
			new MultiTecla(new PRG(),  new Tecla("BACK", new HKeyOperation(ICalc.KEYBACK)),new WithEnter("DROP")),
			new MultiTecla(new MODES(),new WithEnter("INV"), new Tecla("SWAP")),
			new MultiTecla(new PLOT(), new WithEnter("x\u00B2", "SQ"),new WithEnter("x^y", "^")),
			new MultiTecla(new SYMB(), new WithEnter("*"),new WithEnter("/")),
			new MultiTecla(new STAT(), new Tecla("EEX", "E"),new TeclaText("''", 1)),
			new MultiTecla(new TIME(), new WithEnter("STO"),new WithEnter("RCL")),
			new MultiTecla(new UNITS(),new WithEnter("+"),new WithEnter("-")),
			new MultiTecla(new Tecla("SOLVE", opSolve), new WithEnter("LN"),new WithEnter("EXP")),
			new MultiTecla(new MEMORY(), new Tecla("LsStk",opLstSt), new Tecla("\u008D", " \u008D ")),
			new MultiTecla(new VAR(),  new WithEnter("CHS"),new WithEnter("EVAL")),
			new MultiTecla(new ENTER(), new WithEnter("LOG"),new WithEnter("ALOG")) });
		
		cond = getAcceptStatus();
		cond[Calc.SM_KEYBNDX] = 1; // Menus
		GrupoTeclas menus = new GrupoTeclas(cond,new MultiTecla[] {
			new MultiTecla(new Tecla("1"), new WithEnter("SIN"),	new Tecla("#")),
			new MultiTecla(new Tecla("2"), new WithEnter("ASIN"),new Tecla("_")),
			new MultiTecla(new Tecla("3"), new WithEnter("\u0088"),new Tecla("::", 1)),
			new MultiTecla(new Tecla("4"), new WithEnter("COS"),new Tecla("()", 1)),
			new MultiTecla(new Tecla("5"), new WithEnter("ACOS"),new Tecla("[]", 1)),
			new MultiTecla(new Tecla("6"), new WithEnter("\u0084","\u0084"), new Tecla("\u00AB\u00BB", 1)),
			new MultiTecla(new Tecla("7"), new WithEnter("TAN"),new WithEnter("=")),
			new MultiTecla(new Tecla("8"), new WithEnter("ATAN"),new Tecla(",")),
			new MultiTecla(new Tecla("9"), new WithEnter("\u0085"), new Tecla("{}", 1)),
			new MultiTecla(new Tecla("."), new WithEnter("\u0087"),new Tecla("HOME", opHome)),
			new MultiTecla(new Tecla("0"), new Tecla("CST",opCst), new Tecla("\"\"", 1)),
			new MultiTecla(new Tecla("SPC", " "), new Tecla("USR"),	new WithEnter("PURGE")) });
		
		cond = getAcceptStatus();
		cond[Calc.SM_KEYBNDX] = 3; // Alfabetico
		// Menu grafico
		Operation opToogleStack = new Operation() {
			public void exec(Object extern) throws F8Exception {
				Tecla t = null;
				if (extern != null && extern instanceof Tecla) {
					// Es una Tecla
					t = (Tecla) extern;
				}
				if (Calc.scrMode == Calc.SCRMODE_PLOT) {
					Calc.scrMode = Calc.SCRMODE_STACK;
					if (t != null)
						t.setTitle("GRAPHICS");
				} else {
					Calc.scrMode = Calc.SCRMODE_PLOT;
					if (t != null)
						t.setTitle("STACK");
				}
			}

		};

		GrupoTeclas alfabeticoNoEditando = new GrupoTeclas(cond,
				new MultiTecla[] {
			new MultiTecla(new WithEnter("+"), new TeclaText("A"),	new TeclaText("B")),
			new MultiTecla(new Tecla("SWAP"), new TeclaText("C"),new TeclaText("D")),
			new MultiTecla(new Tecla("*"), new TeclaText("E"),new TeclaText("F")),
			new MultiTecla(new Tecla("-"), new TeclaText("G"),new TeclaText("H")),
			new MultiTecla(new Tecla("GRAPH", opToogleStack),new TeclaText("I"), new TeclaText("J")),
			new MultiTecla(new Tecla("/"), new TeclaText("K"),new TeclaText("L")),
			new MultiTecla(new Tecla("DROP"), new TeclaText("M"),new TeclaText("N")),
			new MultiTecla(new Tecla("CST", opCst), new TeclaText("O"), new TeclaText("P")),
			new MultiTecla(new Tecla("HOME", opHome), new TeclaText("Q"), new TeclaText("R")),
			new MultiTecla(new TeclaText("Y"), new TeclaText("S"),new TeclaText("T")),
			new MultiTecla(new TeclaText("Z"), new TeclaText("U"),new TeclaText("V")),
			new MultiTecla(new Tecla("SPC", " "),new TeclaText("W"), new TeclaText("X")) });
		
		cond = getAcceptStatus();
		cond[Calc.SM_KEYBNDX] = 3; // Alfabetico
		cond[Calc.SM_ISEDITING] = 1; // en edicion
		
		GrupoTeclas alfabeticoEditando = new GrupoTeclas(cond,
				new MultiTecla[] {
			new MultiTecla(new Tecla("BACK", new HKeyOperation(ICalc.KEYBACK)), new TeclaText("A"),	new TeclaText("B")),
			new MultiTecla(new Tecla("(\u0090)", new HKeyOperation(ICalc.KEYCURUP)), new TeclaText("C"),new TeclaText("D")),
			new MultiTecla(new Tecla("DEL", new HKeyOperation(ICalc.KEYDEL)), new TeclaText("E"),new TeclaText("F")),
			new MultiTecla(new Tecla("(\u008E)", new HKeyOperation(ICalc.KEYCURLEFT)), new TeclaText("G"),new TeclaText("H")),
			new MultiTecla(new Tecla("GRAPH", opToogleStack),new TeclaText("I"), new TeclaText("J")),
			new MultiTecla(new Tecla("(\u008D)", new HKeyOperation(ICalc.KEYCURRIGHT)), new TeclaText("K"),new TeclaText("L")),
			new MultiTecla(new Tecla("UP", "UPDIR"), new TeclaText("M"), new TeclaText("N")),
			new MultiTecla(new Tecla("(\u008F)", new HKeyOperation(ICalc.KEYCURDOWN)), new TeclaText("O"),new TeclaText("P")),
			new MultiTecla(new Tecla("HOME", opHome),new TeclaText("Q"), new TeclaText("R")),
			new MultiTecla(new TeclaText("Y"), new TeclaText("S"),new TeclaText("T")),
			new MultiTecla(new TeclaText("Z"), new TeclaText("U"),new TeclaText("V")),
			new MultiTecla(new Tecla("SPC", " "),new TeclaText("W"), new TeclaText("X")) });
		
		cond = getAcceptStatus();
		cond[Calc.SM_KEYBNDX] = 2; // Griego
		GrupoTeclas alfabeticoGriego = new GrupoTeclas(cond, new MultiTecla[] {
			new MultiTecla(new TeclaText("\u0081"),	new TeclaText("\u0082"), new Tecla("")),
			new MultiTecla(new WithEnter("\u0089"),	new WithEnter("\u008a"), new WithEnter("\u008b")),
			new MultiTecla(new TeclaText("\u008c"),	new TeclaText("\u0091"), new TeclaText("\u0092")),
			new MultiTecla(new TeclaText("\u0093"),	new TeclaText("\u0094"), new TeclaText("\u0095")),
			new MultiTecla(new TeclaText("\u0096"),	new TeclaText("\u0097"), new TeclaText("\u0098")),
			new MultiTecla(new TeclaText("\u0099"),	new TeclaText("\u009a"), new TeclaText("\u009b")),
			new MultiTecla(new TeclaText("\u009c"),	new TeclaText("\u009d"), new TeclaText("\u009f")),
			new MultiTecla(new TeclaText(""), new TeclaText(""),new TeclaText("")),
			new MultiTecla(new TeclaText("x"), new TeclaText("y"),	new TeclaText("z")),
			new MultiTecla(new TeclaText("i"), new TeclaText("j"),	new TeclaText("k")),
			new MultiTecla(new TeclaText("u"), new TeclaText("v"),	new TeclaText("w")),
			new MultiTecla(new Tecla("SPC", " "), new WithEnter("DROP"),new WithEnter("SWAP")) });
	}

	// Devuelve una cadena de estados TRUE siempre
	private int[] getAcceptStatus() {
		int sz = ims.getMachineStatus().length;
		int k[] = new int[sz];
		for (int i = 0; i < sz; i++)
			k[i] = -1;
		return k;
	}

	public MultiTecla get(int i) {
		GrupoTeclas gt;
		try {
			if (i >= maxTeclas)
				return null;
			gt = findGrupo(ims.getMachineStatus());
			return gt.getTeclas()[i];
		} catch (NullPointerException npe) {
			return null;
		}
	}

	public class MultiTecla {
		TeclaVirtual mt[] = new TeclaVirtual[3];

		public MultiTecla(TeclaVirtual nor, TeclaVirtual lef, TeclaVirtual rig) {
			mt[0] = nor;
			mt[1] = lef;
			mt[2] = rig;
		}

		public MultiTecla(MultiTecla base) {
			mt[0] = base.mt[0];
			mt[1] = base.mt[1];
			mt[2] = base.mt[2];
		}

		public void setFunc(int i, Menu m) {
			mt[i] = m;
		}

		public TeclaVirtual get(int shift) {
			return mt[shift];
		}
	}

	class GrupoTeclas {
		int cnd[];
		MultiTecla tec[];

		public GrupoTeclas(int cond[], MultiTecla teclas[]) {
			cnd = cond;
			tec = teclas;
			grupos.addElement(this);
		}

		public boolean match(int status[]) {
			boolean ret = true;
			for (int i = 0; ret && i < status.length; i++) {
				if (cnd[i] < 0)
					continue;
				ret = ret && (status[i] == cnd[i]);
			}
			return ret;
		}

		public MultiTecla[] getTeclas() {
			return tec;
		}
	}

	public static GrupoTeclas findGrupo(int status[]) {
		for (int i = grupos.size() - 1; i >= 0; i--) {
			GrupoTeclas sig = (GrupoTeclas) grupos.elementAt(i);
			if (sig.match(status))
				return sig;
		}
		// ELM ESTO ES UNA �APA
		return (GrupoTeclas) grupos.elementAt(0);
	}

}
