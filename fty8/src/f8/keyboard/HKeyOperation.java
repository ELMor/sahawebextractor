package f8.keyboard;

import f8.commands.Operation;
import f8.exceptions.F8Exception;
import f8.platform.Calc;

public class HKeyOperation extends Operation {
	int action;
	public HKeyOperation(int act) {
		super();
		action=act;
	}

	public void exec(Object extern) throws F8Exception {
		Calc.ref.enqueueKeyAction(action, true, null);
	}

}
