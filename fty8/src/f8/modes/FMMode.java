package f8.modes;

import java.io.IOException;

import f8.Core;
import f8.ICalc;
import f8.commands.Storable;
import f8.platform.Calc;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class FMMode extends CheckGroup {
	public FMMode() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 479;
	}

	public Storable getInstance() {
		return new FMMode();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public boolean execBefore() {
		
		int set = ((Core.getSetting(ICalc.PER_MOD) == ICalc.COMMA) ? ICalc.PERIOD
				: ICalc.COMMA);
		Core.setSettings(ICalc.PER_MOD, set);
		Calc.ref.loadNumberFormat();
		return false;
	}

	public String toString() {
		return ("0_0");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "FM";
	}
}
