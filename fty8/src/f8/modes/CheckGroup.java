/*
 * Created on 09-sep-2003
 *
 
 
 */
package f8.modes;

import java.util.Vector;

import f8.Core;
import f8.ICalc;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.exceptions.TooFewArgumentsException;
import f8.keyboard.Menu;
import f8.keyboard.WithEnter;
import f8.keyboard.hp48.menu.MODES;
import f8.objects.types.Double;
import f8.objects.types.Int;
import f8.platform.Calc;

/**
 * @author elinares
 * 
 * 
 * 
 */
public abstract class CheckGroup extends NonAlgebraic {
	public abstract String getGroupName();

	public abstract boolean execBefore() throws F8Exception;

	public void exec(Object extern) throws F8Exception {
		if (execBefore()) {
			return;
		}
		Vector v = (Vector) Menu.groups.get(getGroupName());
		if (v == null) {
			return;
		}
		int sz = v.size();
		WithEnter c = null;
		if (sz == 1) {
			c = (WithEnter) v.elementAt(0);
			c.setChecked(!c.isChecked());
		} else {
			String search = toString();
			for (int i = 0; i < v.size(); i++) {
				c = (WithEnter) v.elementAt(i);
				c.setChecked(c.getEmit().equals(search));
			}
		}

		if (Calc.ref.getDynamic() instanceof MODES) {
			//Core.refreshProgMenu();
		}
	}

	protected boolean setTipeAndPrecission(int tipo) throws F8Exception {
		if(tipo==ICalc.STD){
			//Continuar
		}else if (Core.check(1)) {
			Command a = (Command) Core.pop();
			int n=-1;

			if (a instanceof Int) {
				n = ((Int) a).n;
			} else if (a instanceof Double) {
				n = ((Double) a).doubleValue().toInt();
			}
			if (n >= 0) {
				Core.setSettings(ICalc.DOU_PRE, n);
			} else {
				throw new IndexRangeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
		Core.setSettings(ICalc.DOU_MOD, tipo);
		Calc.ref.loadNumberFormat();
		return false;
	}
}
