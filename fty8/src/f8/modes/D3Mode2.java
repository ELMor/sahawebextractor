package f8.modes;

import java.io.IOException;

import f8.Core;
import f8.ICalc;
import f8.commands.Storable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class D3Mode2 extends CheckGroup {
	public D3Mode2() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 485;
	}

	public Storable getInstance() {
		return new D3Mode2();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public boolean execBefore() {
		
		Core.setSettings(ICalc.MOD_3D, ICalc.RAZ);
		return false;
	}

	public String toString() {
		return ("3D_2");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "3D";
	}
}
