package f8.modes;

import java.io.IOException;

import f8.Core;
import f8.ICalc;
import f8.commands.Storable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class DEC extends CheckGroup {
	public DEC() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 482;
	}

	public Storable getInstance() {
		return new DEC();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public boolean execBefore() {
		
		Core.setSettings(ICalc.INT_MOD, ICalc.DEC);
		return false;
	}

	public String toString() {
		return ("DEC");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "IM";
	}
}
