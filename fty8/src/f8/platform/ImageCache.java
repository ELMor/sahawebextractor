package f8.platform;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Enumeration;
import java.util.Hashtable;

import f8.objects.Stackable;
import f8.platform.glyph.Glyph;
import f8.platform.glyph.Glyph_AST;


public class ImageCache {
	private Hashtable<String, ICE> cache;
	int needScroll=0;
	int msz=10;
	
	class ICE {
		String mathExpr;
		BufferedImage imagen;
		ScrollPos sp;
		long time;
		public ICE(String me, BufferedImage img){
			mathExpr=me;
			imagen=img;
			sp=new ScrollPos();
			time=System.currentTimeMillis();
		}
	}
	
	public ImageCache(int maxsz){
		msz=maxsz;
		invalidateImageCache();
	}
	
	public synchronized BufferedImage put(String k, BufferedImage i){
		if(cache.size()>=msz){
			removeOlder();
		}
		ICE ice=(ICE)cache.get(k);
		if(ice==null){
			cache.put(k, new ICE(k,i));
		}else{
			ice.time=System.currentTimeMillis();
		}
		return i;
	}
	
	public synchronized void removeOlder() {
		String oldKey=null;
		ICE ice=null;
		long oldDat=System.currentTimeMillis();
		for(Enumeration<String> e=cache.keys();e.hasMoreElements();){
			String curKey=(String)e.nextElement();
			ice=(ICE)cache.get(curKey);
			if(oldDat>ice.time){
				oldKey=curKey;
				oldDat=ice.time;
			}
		}
		if(oldKey==null)
			return;
		cache.remove(oldKey);
		ScrollPos sp=ice.sp;
		if(sp!=null && sp.cutX>0 || sp.cutY>0)
			needScroll--;
	}
	
	public synchronized BufferedImage get(String k){
		ICE ice=(ICE)cache.get(k);
		if(ice!=null){
			ice.time=System.currentTimeMillis();
		}
		return ice==null ? null : ice.imagen;
	}
	
	public int size(){
		return cache.size();
	}
	
	public ScrollPos getScrollPos(BufferedImage i){
		for(Enumeration<ICE> e=cache.elements();e.hasMoreElements();){
			ICE ice=(ICE)e.nextElement();
			if(ice.imagen==i)
				return ice.sp;
		}
		return null;
	}
	
	public boolean isScroll(){
		return needScroll>0; 
	}
	
	public class ScrollPos {
		int cutX,cutY;
		int dirX, dirY;
		int lastX, lastY;
		int borderTicks=3; //se usa para que en los extremos se detenga algo mas
		
		boolean initialized=false;
		
		private ScrollPos(){
			
		}
		
		public void init(int cx, int cy) {
			cutX=cx;
			cutY=cy;
			dirX=Math.min(cx/5,15);
			dirY=Math.min(cy/3,10);
			initialized=true;
			if(cx>0 || cy>0)
				needScroll++;
		}
		
		public boolean isInit(){
			return initialized;
		}
		
		public void calNextScrollPos(){
			if(borderTicks<5 && borderTicks>0){
				borderTicks--;
				return;
			}else if(borderTicks==0){
				borderTicks=5;
			}
			lastX+=dirX;
			if(lastX>cutX || lastX<0){
				dirX=-dirX;
				if(lastX>cutX){
					borderTicks--;
					lastX=cutX-1;
				}else{
					borderTicks--;
					lastX=0;
				}
				lastY+=dirY;
				if(lastY>cutY || lastY<0){
					dirY=-dirY;
					if(lastY>cutY){
						lastY=cutY;
					}else{
						lastY=0;
					}
				}
			}
		}
		
		public int getX(){
			return lastX;
		}
		
		//El tama�o vertical puede cambiar
		public int getY(int newCutY){
			if(cutY==newCutY)
				return lastY;
			if(cutX>0 || cutY>0)
				needScroll--;
			init(cutX,newCutY);
			return lastY;
		}
				
	}

	public void invalidateImageCache(){
		cache=new Hashtable<String, ICE>(msz);
	}
	public synchronized BufferedImage cacheManager(
			int anchura, int stackAreaSize, int lineasVisibles, 
			boolean ml, Stackable obj,int maxWidth, Color bkg, F8Font f, int sz){
		String mathStr;
		boolean err=false;
		try {
			mathStr= obj==null ? "" : obj.toString();
		}catch(NullPointerException npe){
			mathStr = "<UNKNOWN/INVALID>";
			err=true;
		}
		
		ICE iRet=cache.get(mathStr);
		BufferedImage ret=iRet!=null?cache.get(mathStr).imagen:null;
		
		//Cache?
		if(ret!=null)
			return ret;
		//Missed, build..
		if(!err && ml){
			try {
				Glyph ori=new Glyph_AST(null,obj.getAST(),sz);
				ret=ori.getImage(bkg);
			} catch (IllegalArgumentException e) {
				ret=drawStackLineAsText(anchura,stackAreaSize,lineasVisibles, maxWidth, mathStr, bkg, f);
			}
		}else{
			ret=drawStackLineAsText(anchura,stackAreaSize,lineasVisibles, maxWidth, mathStr, bkg, f);
		}
		//...and store
		cache.put(mathStr, new ICE(mathStr,ret));
		return ret;
	}
	
	private BufferedImage drawStackLineAsText(int anchura, int stackAreaSize,int lineasVisibles, int maxWidth, String cnt, Color bkg, F8Font bmf){
		BufferedImage equIma=new BufferedImage(2*anchura,2*stackAreaSize,BufferedImage.TYPE_INT_RGB);
		Graphics g=equIma.getGraphics();
		g.setColor(bkg);
		g.fillRect(0, 0, 2*anchura, 2*stackAreaSize);
		String firstLevel[]=new String[250];
		LineSplitter ls=new LineSplitter(cnt, maxWidth, firstLevel, bmf);
		int lins=ls.split(lineasVisibles);
		int gfh=bmf.getFontHeight();
		for(int i=0;i<lins;i++){
			if(i==lins-1){
				int w=bmf.stringWidth(firstLevel[i]);
				bmf.getViewer(firstLevel[i]).paint(maxWidth-w, i*gfh, g);
			}else{ //Ocupa todo el ancho
				bmf.getViewer(firstLevel[i]).paint(0, i*gfh, g);
			}
			if(i*gfh>equIma.getHeight())
				break;
		}
		int maxHeight=Math.min(lins*gfh, equIma.getHeight());
		return equIma.getSubimage(0, 0, maxWidth, maxHeight);
	}


}
