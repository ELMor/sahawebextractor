package f8.platform.glyph;

import antlr.collections.AST;

public class Glyph_Integ extends GlyphHFlow {

	public Glyph_Integ(Glyph cpad, AST f, AST v, AST i, AST e, int sz){
		super(cpad);
		Glyph fun=new Glyph_AST(this,f,sz);
		Glyph ini=new Glyph_AST(this,i,sz-2);
		Glyph end=new Glyph_AST(this,e,sz-2);
		int h=fun.getHeight();
		int hi=Math.max(40, h);
		Glyph integ=new Glyph_Image(this,"Integral",hi/3,hi,(hi-h)/2+fun.getBase());
		integ.setTheCenter(true);
		Glyph dx=new Glyph_String(null,"\u0088"+v.getText(),sz);
		GlyphVFlow lft=new GlyphVFlow(this);
		lft.addHijo(end);
		lft.addHijo(integ);
		lft.addHijo(ini);
		addHijo(lft);
		addHijo(fun);
		addHijo(dx);
	}
	
}
