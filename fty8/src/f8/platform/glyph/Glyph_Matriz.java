package f8.platform.glyph;

import antlr.collections.AST;

public class Glyph_Matriz extends Glyph_Parentesis {
	public Glyph_Matriz(Glyph cpad, AST nodo, int sz){
		super(cpad,null);
		Glyph_SqareObjects sq=new Glyph_SqareObjects(this,nodo,sz);
		init(sq);
	}
}
