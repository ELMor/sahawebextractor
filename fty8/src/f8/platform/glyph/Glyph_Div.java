package f8.platform.glyph;

import antlr.collections.AST;

public class Glyph_Div extends GlyphVFlow {
	public int getBase() {
		return ((Glyph)elems.elementAt(0)).getHeight()-1;
	}

	public Glyph_Div(Glyph padre, AST num, AST den, int fontSize) {
		super(padre);
		Glyph nume = new Glyph_AST(this, num, fontSize - 1);
		Glyph deno = new Glyph_AST(this, den, fontSize - 1);
		addHijo(nume);
		addHijo(new Glyph_Barra(this,6+Math.max(nume.getWidth(), deno.getWidth()),3,1));
		addHijo(deno);
	}
}
