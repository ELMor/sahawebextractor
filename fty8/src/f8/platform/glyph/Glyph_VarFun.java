package f8.platform.glyph;

import java.awt.Graphics;

import antlr.collections.AST;

public class Glyph_VarFun extends GlyphHFlow {

	public Glyph elevadoA=null;
	
	public Glyph_VarFun(Glyph cpad, AST nodo, int sz){
		super(cpad);
		addHijo(new Glyph_String(this, nodo.getText(), sz));
		AST firstChild = nodo.getFirstChild();
		if (firstChild != null) { // Es una Funcion
			GlyphHFlow ar=new GlyphHFlow(this);
			for (AST arg = firstChild; arg != null; arg = arg.getNextSibling()) {
				if (arg != firstChild) {
					ar.addHijo(new Glyph_String(this, ",", sz));
				}
				ar.addHijo(new Glyph_AST(ar, firstChild, sz));
			}
			addHijo(new Glyph_Parentesis(this, ar));
		}
	}

	public void dibuja(Graphics g) {
		//Se distingue si hay un exponente en la funcion
		//Se desea dibujar:
		//   2
		//cos (x)
		//
		//Mejor que
		//         2
		// (cos(x))
		if(elevadoA==null){
			super.dibuja(g);
		}else{
			Glyph max=new GlyphEmpty(null);
			for(int i=0;i<elems.size();i++){
				Glyph elem=((Glyph)elems.elementAt(i));
				if(max.getBase()<elem.getBase()){
					max=elem;
				}
			}
			int cx=0;
			Glyph elem=((Glyph)elems.elementAt(0));
			int maxBase=Math.max(max.getBase(), elevadoA.getBase()+elem.getBase());
			//Dibujamos el nombre de la funcion
			elem.setX(0);
			cx+=1+elem.getWidth();
			elem.setY(maxBase-elem.getBase());
			elem.dibuja(g);
			//Ahora el elevado
			elevadoA.setX(cx);
			cx+=1+elevadoA.getWidth();
			elevadoA.setY(maxBase-elevadoA.getBase()-elem.getBase());
			elevadoA.padre=this;
			elevadoA.dibuja(g);
			//Y el resto normal.
			for(int i=1;i<elems.size();i++){
				elem=((Glyph)elems.elementAt(i));
				elem.setX(cx);
				elem.setY(maxBase-elem.getBase());
				elem.dibuja(g);
				cx+=1+elem.getWidth();
			}
		}
	}
	
}
