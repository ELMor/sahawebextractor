package f8.platform.glyph;

import java.awt.Graphics;

public class Glyph_Barra extends GlyphEmpty {
	
	public Glyph_Barra(Glyph pad,int w, int h, int hb) {
		super(pad,w,h,hb);
	}
	public void dibuja(Graphics g) {
		g.setColor(fillColor);
		g.fillRect(realX(), realY()+base-1, width, 2);
	}
}
