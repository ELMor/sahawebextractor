package f8.platform;

import java.awt.Graphics;

public class F8StaticFonts {
	private static F8Font bmFonts[]=new F8Font[11];
	public static void init(Graphics g){
		for(int i=10;i<21;i++){
			bmFonts[i-10]=new F8Font(g,i,false);
		}
	}
	
	public static F8Font getF8Font(int size){
		return bmFonts[size-10];
	}
}
