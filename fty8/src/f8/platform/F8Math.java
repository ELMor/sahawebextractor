package f8.platform;


public class F8Math {

	public static double hypot(double a, double b) {
		double r;
		if (Math.abs(a) > Math.abs(b)) {
			r = b / a;
			r = Math.abs(a) * Math.sqrt(1 + r * r);
		} else if (b != 0) {
			r = a / b;
			r = Math.abs(b) * Math.sqrt(1 + r * r);
		} else {
			r = 0.0;
		}
		return r;
	}
	public static int toInt(String s) {
		return Integer.parseInt(s);
	}

	public static float toFloat(String s) {
		return Float.parseFloat(s);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.mth.MathKernel#fromBIN(java.lang.String)
	 */
	public static int fromBIN(String s) {
		return (int) Integer.parseInt(s, 2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.mth.MathKernel#fromDEC(java.lang.String)
	 */
	public static int fromDEC(String s) {
		return (int) Integer.parseInt(s, 10);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.mth.MathKernel#fromHEX(java.lang.String)
	 */
	public static int fromHEX(String s) {
		return (int) Integer.parseInt(s, 16);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.mth.MathKernel#fromOCT(java.lang.String)
	 */
	public static int fromOCT(String s) {
		return (int) Integer.parseInt(s, 8);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.mth.MathKernel#toBIN(int)
	 */
	public static String toBIN(int d) {
		return Integer.toBinaryString(d);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.mth.MathKernel#toDEC(int)
	 */
	public static String toDEC(int d) {
		return Integer.toString(d);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.mth.MathKernel#toHEX(int)
	 */
	public static String toHEX(int d) {
		return Integer.toHexString(d);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.mth.MathKernel#toOCT(int)
	 */
	public static String toOCT(int d) {
		return Integer.toOctalString(d);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.mth.MathKernel#format(int, int)
	 */
	public static String format(int d, int mode) {
		switch (mode) {
		case 1:
			return toDEC(d);
		case 2:
			return toHEX(d);
		case 3:
			return toOCT(d);
		case 4:
			return toBIN(d);
		}
		return toDEC(d);
	}

}
