package f8.platform;

public class LineSplitter {
	String texto;
	int maxWidth;
	String arr[];
	F8Font fnt;
	
	public LineSplitter(String texto, int maxWidth, String arr[], F8Font fnt){
		this.texto=texto;
		this.arr=arr;
		this.maxWidth=maxWidth;
		this.fnt=fnt;
	}
	
	public int split(int lineasVisibles) {
		int editLines;
		editLines = 0;
		int off = 0, len, maxChars = texto.length();
		while (true) {// Por cada linea de edicion
			if (maxChars - off < 15) {// Quedan menos de 15 caracteres
				len = maxChars - off;
			} else {// Nos quedamos solo con 15
				len = 15;
			}
			while (true) {// Por cada caracter de cada linea
				// Tama�o de la subcadena escogida.
				// Queda espacio?
				int lastChar = off + len;
				String subs = texto.substring(off, off + len);
				int pixsForSubs = fnt.stringWidth(subs);
				if (lastChar < maxChars && pixsForSubs < maxWidth)
					len++;
				else { // Nos pasamos o terminamos la cadena
					if (lastChar < maxChars)
						len--; // El ultimo caracter hizo que se desbordase
					break;
				}
			}
			// Nos quedamos con uno menos
			arr[editLines] = texto.substring(off, off + len);
			editLines++;
			off += len;
			len = 0;
			if (off >= maxChars || editLines >=lineasVisibles )
				break;
		}
		return editLines;
	}


}
