package f8.platform;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;


public class Properties extends HashMap {
	
	private static byte buffer[]=new byte[1024];
	private static int pos=-1;
	private static int max=0;
	private static InputStream is=null;
	
	public String readLine(InputStream is){
		StringBuffer sb=new StringBuffer();
		int r;
		boolean noLine=true;
		do{
			r=read();
			if(r=='\r' || r=='\n' || r==-1)
				noLine=false;
			else
				sb.append((char)r);
		}while(noLine);
		if(sb.length()==0){
			if(r==10){
				return readLine(is);
			}else{
				return null;
			}
		}
		return sb.toString();
	}

	public Properties(String fname){
		is=this.getClass().getResourceAsStream(fname);
		String line;
		while((line=readLine(is))!=null){
			int pos=line.indexOf("=");
			if(pos<0){
				
			}else{
				put(line.substring(0,pos),line.substring(pos+1));
			}
		}
	}
	
	public String getProperty(String name){
		return (String)get(name);
	}
	
	public int read(){
		if(pos<0 || pos==buffer.length){
			try {
				max=is.read(buffer);
			} catch (IOException e) {
				return -1;
			}
			if(max>0)
				pos=0;
			if(max==-1)
				return -1;
		}
		return buffer[pos++];
	}

}
