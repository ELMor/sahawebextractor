/*
 * Created on 11-sep-2003
 *
 
 
 */
package f8.platform;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import f8.apps.display.CSys;
import f8.exceptions.F8Exception;
import f8.objects.types.Ereal;
import f8.objects.types.Grob;
import f8.objects.types.Matrix;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class Plot extends Canvas  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5194371035832745441L;
	int xOff,yOff,lcdW,lcdH;
	Color bkgColor;
	double usrW,usrH;
	boolean connect = false;
	F8Font font;
	private Graphics gImg;
	private BufferedImage img;

	public CSys lcd; 
	public CSys usr; 

	public CSys.Point mark;
	public CSys.Point currentPoint;
	public CSys.Point cursor;

	private boolean markOn = false;

	int ptype;

	public Plot( int pX, int pY, int lcdWidth, int lcdHeight, F8Font f,Color bkg){
		this.bkgColor=bkg;
		font=f;
		img=new BufferedImage(lcdWidth, lcdHeight, BufferedImage.TYPE_INT_BGR);
		gImg=img.getGraphics();
		xOff=pX;
		yOff=pY;
		lcdW=lcdWidth;
		lcdH=lcdHeight;
		lcd=new CSys("lcd");
		lcd.setRectangle(
				lcd.vector(0, 0), 
				lcd.vector( lcdWidth, 0), 
				lcd.vector(0,  lcdHeight),
				lcd.vector( lcdWidth,  lcdHeight));
		cursor=lcd.center();
		usr=new CSys("usr");
		clear();
	}
	
	public void reset(double usrWidth, double usrHeight) {
		usrW=usrWidth;
		usrH=usrHeight;
		double rot[][]=new double[2][2];
		double des[][]=new double[2][1];
		rot[0][0]=  lcdW/usrW; rot[0][1]=  0;
		rot[1][0]=  0;         rot[1][1]=  -lcdH/usrH;  	
		des[0][0]=lcdW/2;
		des[1][0]=lcdH/2;
		try {
			usr.setTransform("lcd",rot,des);
		} catch (F8Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		usr.setRectangle(
				usr.vector(-usrW/2,  usrH/2), 
				usr.vector( usrW/2,  usrH/2), 
				usr.vector(-usrW/2, -usrH/2),
				usr.vector( usrW/2, -usrH/2));
		clear();
	}

	public void box(CSys.Point c1, CSys.Point c2) {
		CSys lcdcs = CSys.getCoorSystem("lcd");
		c1 = lcdcs.transform(c1);
		c2 = lcdcs.transform(c2);
		gImg.drawRect(
				c1.v[0].min(c2.v[0]).toInt(), 
				c1.v[1].min(c2.v[1]).toInt(), 
				c1.v[0].minus(c2.v[0]).abs().toInt(), 
				c1.v[1].minus(c2.v[1]).abs().toInt()
				);
	}

	public void circle(CSys.Point c1, CSys.Point c2) {
		CSys lcdcs = CSys.getCoorSystem("lcd");
		c1 = lcdcs.transform(c1);
		c2 = lcdcs.transform(c2);
		int radius=graphDistance(c1, c2);
		gImg.drawArc(
				c1.v[0].toInt(), 
				c1.v[1].toInt(), 
				radius,radius, 
				0, 360);
	}

	public void clear() {
		gImg.setColor(bkgColor);
		gImg.fillRect(0, 0, img.getWidth(null), img.getHeight(null));
	}

	public CSys.Point getPlotCursor() {
		return cursor;
	}

	public CSys.Point getMark() {
		return mark;
	}

	private int graphDistance(CSys.Point c1, CSys.Point c2) {
		CSys lcdcs = CSys.getCoorSystem("lcd");
		c1 = lcdcs.transform(c1);
		c2 = lcdcs.transform(c2);
		return 
				((c2.v[0].minus(c1.v[0])).mul(c2.v[0].minus(c1.v[0]))).plus
				((c2.v[1].minus(c1.v[1])).mul(c2.v[1].minus(c1.v[1]))).sqrt().toInt()
				;
	}

	public boolean isMarkOn() {
		return markOn;
	}

	public void label() {
	}

	public void lineTo(CSys.Point c1, CSys.Point c2) {
		CSys lcdcs = CSys.getCoorSystem("lcd");
		c1 = lcdcs.transform(c1);
		c2 = lcdcs.transform(c2);
		gImg.setColor(new Color(0));
		gImg.drawLine(
				c1.v[0].toInt(), c1.v[1].toInt(), 
				c2.v[0].toInt(), c2.v[1].toInt()
		);
	}

	public void initPoint(CSys.Point p) {
		currentPoint = p.copy();
	}

	public void nextPoint(CSys.Point o) {
		lineTo(currentPoint, o);
		currentPoint = o;
	}

	public void paint(Graphics g) {
		// create the graphics object that will be used to repaint the image
		g.drawImage(img, xOff, yOff, null); // draw the image...
		//Cursor
		g.drawLine(
				xOff + cursor.v[0].toInt() - 0, yOff + cursor.v[1].toInt() - 4,
				xOff + cursor.v[0].toInt() + 0, yOff + cursor.v[1].toInt() + 4
		);
		g.drawLine(
				xOff + cursor.v[0].toInt() - 4, yOff + cursor.v[1].toInt() + 0,
				xOff + cursor.v[0].toInt() + 4, yOff + cursor.v[1].toInt() - 0
		);
		//Marca
		if (markOn) {
			g.drawLine(
					xOff + mark.v[0].toInt() - 4, yOff + mark.v[1].toInt() - 4,
					xOff + mark.v[0].toInt() + 4, yOff + mark.v[1].toInt() + 4
			);
			g.drawLine(
					xOff + mark.v[0].toInt() - 4, yOff + mark.v[1].toInt() + 4,
					xOff + mark.v[0].toInt() + 4, yOff + mark.v[1].toInt() - 4
			);
		}
	}

	public void repl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.LCDControl#setConnected(boolean)
	 */
	public void setConnected(boolean c) {
		connect = c;
	}

	public void setCursor(CSys.Point c) {
		cursor.v[0] = c.v[0];
		cursor.v[1] = c.v[1];
		repaint();
	}

	public void setMark(CSys.Point c) {
		if (isMarkOn() && c.equals(mark)) {
			markOn = false;
		} else {
			mark = c.copy();
			markOn = true;
		}
		repaint();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.LCDControl#setPlotType(int)
	 */
	public void setPlotType(int tipo) {
		ptype = tipo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.LCDControl#setProjectation(f8.types.Matrix)
	 */
	public void setProjectation(Matrix pm) {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.LCDControl#showAxes()
	 */
	public void showAxes() {
		CSys.Vector xsize = lcd.vector(2, 0);
		CSys.Vector ysize = lcd.vector(0, 2);
		
		CSys.Vector xstep = usr.vector(usrW / 16, 0);
		CSys.Vector ystep = usr.vector(0 , usrH / 8);

		lineTo(lcd.left(), lcd.right());
		lineTo(lcd.top(), lcd.bottom());

		CSys.Point left = lcd.left();

		for (int i = 0; i < 16; i++) {
			try {
				lineTo(left.minus(ysize.div(Ereal.dos)), left.plus(ysize));
				left = left.plus(xstep);
			} catch (F8Exception e) {
			}
		}

		CSys.Point top = lcd.bottom();

		for (int i = 0; i < 8; i++) {
			try {
				lineTo(top.minus(xsize.div(Ereal.dos)), top.plus(xsize));
				top = top.plus(ystep);
			} catch (F8Exception e) {
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.LCDControl#showLabels()
	 */
	public void showLabels(String mx, String my, String Mx, String My) {
		CSys.Point l;
		l = lcd.left();
		font.getViewer(mx).paint(l.v[0].toInt(), (int)l.v[1].toInt()-font.getFontHeight(), gImg);
		l = lcd.right();
		font.getViewer(Mx).paint(l.v[0].toInt() - (font.stringWidth(Mx)),l.v[1].toInt()-font.getFontHeight(), gImg);
		l = lcd.bottom();
		font.getViewer(my).paint(l.v[0].toInt(), l.v[1].toInt() - (font.getFontHeight()), gImg);
		l = lcd.top();
		font.getViewer(My).paint(l.v[0].toInt(), l.v[1].toInt(), gImg);
	}

	public void subs() {
	}
	
	public void setGrob(Grob gro){
		gImg.drawImage(gro.getImage(), 0, 0, null);
	}
	public Grob getGrob(){
		return new Grob(img);
	}
}
