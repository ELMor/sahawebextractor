// $ANTLR 2.7.2: "f8/kernel/yacc/object.g" -> "ObjectParser.java"$

package f8.kernel.yacc;
import java.util.Vector;

import antlr.ASTFactory;
import antlr.ASTPair;
import antlr.CommonAST;
import antlr.NoViableAltException;
import antlr.ParserSharedInputState;
import antlr.RecognitionException;
import antlr.Token;
import antlr.TokenBuffer;
import antlr.TokenStream;
import antlr.TokenStreamException;
import antlr.collections.AST;
import antlr.collections.impl.ASTArray;
import antlr.collections.impl.BitSet;

public class ObjectParser extends antlr.LLkParser       implements ObjectParserTokenTypes
 {

protected ObjectParser(TokenBuffer tokenBuf, int k) {
  super(tokenBuf,k);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

public ObjectParser(TokenBuffer tokenBuf) {
  this(tokenBuf,5);
}

protected ObjectParser(TokenStream lexer, int k) {
  super(lexer,k);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

public ObjectParser(TokenStream lexer) {
  this(lexer,5);
}

public ObjectParser(ParserSharedInputState state) {
  super(state,5);
  tokenNames = _tokenNames;
  buildTokenTypeASTClassMap();
  astFactory = new ASTFactory(getTokenTypeToASTClassMap());
}

	public final void hp48Object(
		Vector<CommonAST> itemVector
	) throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST hp48Object_AST = null;
		antlr.CommonAST n1_AST = null;
		antlr.CommonAST n2_AST = null;
		
		try {      // for error handling
			{
			int _cnt3=0;
			_loop3:
			do {
				if ((_tokenSet_0.member(LA(1)))) {
					object();
					n1_AST = (antlr.CommonAST)returnAST;
					if ( inputState.guessing==0 ) {
						itemVector.addElement(n1_AST);
					}
				}
				else if ((LA(1)==TRXHEAD)) {
					trxobject();
					n2_AST = (antlr.CommonAST)returnAST;
					if ( inputState.guessing==0 ) {
						itemVector.addElement(n2_AST);
					}
				}
				else {
					if ( _cnt3>=1 ) { break _loop3; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt3++;
			} while (true);
			}
			match(Token.EOF_TYPE);
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_1);
			} else {
			  throw ex;
			}
		}
		returnAST = hp48Object_AST;
	}
	
	public final void object() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST object_AST = null;
		antlr.CommonAST p_AST = null;
		antlr.CommonAST i_AST = null;
		antlr.CommonAST v_AST = null;
		antlr.CommonAST m_AST = null;
		Token  id = null;
		antlr.CommonAST id_AST = null;
		antlr.CommonAST li_AST = null;
		Token  n1 = null;
		antlr.CommonAST n1_AST = null;
		antlr.CommonAST n2_AST = null;
		antlr.CommonAST n3_AST = null;
		antlr.CommonAST o_AST = null;
		antlr.CommonAST s_AST = null;
		antlr.CommonAST po_AST = null;
		antlr.CommonAST cmd_AST = null;
		Token  tag = null;
		antlr.CommonAST tag_AST = null;
		antlr.CommonAST tagged_AST = null;
		antlr.CommonAST d_AST = null;
		antlr.CommonAST gr_AST = null;
		
		try {      // for error handling
			{
			switch ( LA(1)) {
			case OPENPROG:
			case OPENPROGOC:
			{
				program();
				p_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=p_AST;
						
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case SCOMI:
			{
				infixExpression();
				i_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=i_AST;
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case ID:
			{
				id = LT(1);
				id_AST = (antlr.CommonAST)astFactory.create(id);
				match(ID);
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=id_AST;
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case LLAA:
			{
				list();
				li_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=li_AST;
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case INTNUMBER:
			{
				n1 = LT(1);
				n1_AST = (antlr.CommonAST)astFactory.create(n1);
				match(INTNUMBER);
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=n1_AST;
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case LPAREN:
			{
				complex();
				n2_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=n2_AST;
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case NUMBER:
			{
				nunit();
				n3_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=n3_AST;
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case MULT:
			case DIV:
			case CARET:
			case ASSIGN:
			case PLUS:
			case MINUS:
			case LT:
			case LE:
			case LEOC:
			case EQ:
			case DISTI:
			case GE:
			case GEOC:
			case GT:
			{
				operador();
				o_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=o_AST;
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case STRING_LITERAL:
			{
				string();
				s_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=s_AST;
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case LITERAL_FOR:
			case LITERAL_START:
			case FLECHA:
			case LITERAL_IF:
			case LITERAL_IFERR:
			case LITERAL_DO:
			case LITERAL_WHILE:
			{
				pob();
				po_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=po_AST;
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case TOUNIT:
			case VTO:
			case TOV2:
			case TOV3:
			case DTOR:
			case RTOD:
			case RTOB:
			case BTOR:
			case OBJTO:
			case EQTO:
			case TOARRY:
			case TOLIST:
			case TOSTR:
			case TOTAG:
			case RTOC:
			case CTOR:
			case FSINT:
			case FCINT:
			case FSINTC:
			case FCINTC:
			{
				specialCommand();
				cmd_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(1)).add((antlr.CommonAST)astFactory.create(ID)));
							 object_AST.setText(cmd_AST.getText());
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case DPUNTOS:
			{
				antlr.CommonAST tmp2_AST = null;
				tmp2_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(DPUNTOS);
				tag = LT(1);
				tag_AST = (antlr.CommonAST)astFactory.create(tag);
				match(ID);
				antlr.CommonAST tmp3_AST = null;
				tmp3_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(DPUNTOS);
				object();
				tagged_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(TAG)).add(tag_AST).add(tagged_AST));
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case LITERAL_DIR:
			{
				directorio();
				d_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=d_AST; 
						
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case GROB:
			{
				grob();
				gr_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					object_AST = (antlr.CommonAST)currentAST.root;
					object_AST=gr_AST;
							
					currentAST.root = object_AST;
					currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
						object_AST.getFirstChild() : object_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			default:
				if ((LA(1)==CORO) && (LA(2)==NUMBER)) {
					vector();
					v_AST = (antlr.CommonAST)returnAST;
					if ( inputState.guessing==0 ) {
						object_AST = (antlr.CommonAST)currentAST.root;
						object_AST=v_AST;
								
						currentAST.root = object_AST;
						currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
							object_AST.getFirstChild() : object_AST;
						currentAST.advanceChildToEnd();
					}
				}
				else if ((LA(1)==CORO) && (LA(2)==CORO)) {
					matriz();
					m_AST = (antlr.CommonAST)returnAST;
					if ( inputState.guessing==0 ) {
						object_AST = (antlr.CommonAST)currentAST.root;
						object_AST=m_AST;
								
						currentAST.root = object_AST;
						currentAST.child = object_AST!=null &&object_AST.getFirstChild()!=null ?
							object_AST.getFirstChild() : object_AST;
						currentAST.advanceChildToEnd();
					}
				}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = object_AST;
	}
	
	public final void trxobject() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST trxobject_AST = null;
		Token  t = null;
		antlr.CommonAST t_AST = null;
		Token  a = null;
		antlr.CommonAST a_AST = null;
		antlr.CommonAST o_AST = null;
		
		try {      // for error handling
			antlr.CommonAST tmp4_AST = null;
			tmp4_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(TRXHEAD);
			antlr.CommonAST tmp5_AST = null;
			tmp5_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(ID);
			antlr.CommonAST tmp6_AST = null;
			tmp6_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(LPAREN);
			t = LT(1);
			t_AST = (antlr.CommonAST)astFactory.create(t);
			match(NUMBER);
			antlr.CommonAST tmp7_AST = null;
			tmp7_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(RPAREN);
			antlr.CommonAST tmp8_AST = null;
			tmp8_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(ID);
			antlr.CommonAST tmp9_AST = null;
			tmp9_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(LPAREN);
			a = LT(1);
			a_AST = (antlr.CommonAST)astFactory.create(a);
			match(ID);
			antlr.CommonAST tmp10_AST = null;
			tmp10_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(RPAREN);
			antlr.CommonAST tmp11_AST = null;
			tmp11_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(ID);
			antlr.CommonAST tmp12_AST = null;
			tmp12_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(LPAREN);
			{
			if ((LA(1)==PUNTO)) {
				antlr.CommonAST tmp13_AST = null;
				tmp13_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(PUNTO);
			}
			else if ((LA(1)==COMMA)) {
				antlr.CommonAST tmp14_AST = null;
				tmp14_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(COMMA);
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			antlr.CommonAST tmp15_AST = null;
			tmp15_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(RPAREN);
			antlr.CommonAST tmp16_AST = null;
			tmp16_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(PYC);
			object();
			o_AST = (antlr.CommonAST)returnAST;
			if ( inputState.guessing==0 ) {
				trxobject_AST = (antlr.CommonAST)currentAST.root;
				trxobject_AST=o_AST; 
						
				currentAST.root = trxobject_AST;
				currentAST.child = trxobject_AST!=null &&trxobject_AST.getFirstChild()!=null ?
					trxobject_AST.getFirstChild() : trxobject_AST;
				currentAST.advanceChildToEnd();
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_3);
			} else {
			  throw ex;
			}
		}
		returnAST = trxobject_AST;
	}
	
	public final void program() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST program_AST = null;
		antlr.CommonAST n1_AST = null;
		
		try {      // for error handling
			{
			if ((LA(1)==OPENPROG)) {
				match(OPENPROG);
			}
			else if ((LA(1)==OPENPROGOC)) {
				match(OPENPROGOC);
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			seqOb();
			n1_AST = (antlr.CommonAST)returnAST;
			{
			switch ( LA(1)) {
			case CLOSEPROG:
			{
				match(CLOSEPROG);
				break;
			}
			case CLOSEPROGOC:
			{
				match(CLOSEPROGOC);
				break;
			}
			case EOF:
			{
				match(Token.EOF_TYPE);
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
			if ( inputState.guessing==0 ) {
				program_AST = (antlr.CommonAST)currentAST.root;
				program_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(PROGRAM)).add(n1_AST));
				currentAST.root = program_AST;
				currentAST.child = program_AST!=null &&program_AST.getFirstChild()!=null ?
					program_AST.getFirstChild() : program_AST;
				currentAST.advanceChildToEnd();
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = program_AST;
	}
	
	public final void infixExpression() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST infixExpression_AST = null;
		antlr.CommonAST n1_AST = null;
		antlr.CommonAST n2_AST = null;
		
		try {      // for error handling
			match(SCOMI);
			if ( inputState.guessing==0 ) {
				infixExpression_AST = (antlr.CommonAST)currentAST.root;
				infixExpression_AST=((antlr.CommonAST)astFactory.create(INFIX));
				currentAST.root = infixExpression_AST;
				currentAST.child = infixExpression_AST!=null &&infixExpression_AST.getFirstChild()!=null ?
					infixExpression_AST.getFirstChild() : infixExpression_AST;
				currentAST.advanceChildToEnd();
			}
			assignExpr();
			n1_AST = (antlr.CommonAST)returnAST;
			if ( inputState.guessing==0 ) {
				infixExpression_AST = (antlr.CommonAST)currentAST.root;
				infixExpression_AST.addChild(n1_AST);
			}
			{
			if ((LA(1)==VBAR)) {
				varAt();
				n2_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					infixExpression_AST = (antlr.CommonAST)currentAST.root;
					infixExpression_AST.addChild(n2_AST);
				}
			}
			else if ((LA(1)==EOF||LA(1)==SCOMI)) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			{
			if ((LA(1)==SCOMI)) {
				match(SCOMI);
			}
			else if ((LA(1)==EOF)) {
				match(Token.EOF_TYPE);
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = infixExpression_AST;
	}
	
	public final void vector() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST vector_AST = null;
		Token  n1 = null;
		antlr.CommonAST n1_AST = null;
		
		try {      // for error handling
			match(CORO);
			if ( inputState.guessing==0 ) {
				vector_AST = (antlr.CommonAST)currentAST.root;
				vector_AST=((antlr.CommonAST)astFactory.create(VECTOR));
				currentAST.root = vector_AST;
				currentAST.child = vector_AST!=null &&vector_AST.getFirstChild()!=null ?
					vector_AST.getFirstChild() : vector_AST;
				currentAST.advanceChildToEnd();
			}
			{
			int _cnt42=0;
			_loop42:
			do {
				if ((LA(1)==NUMBER)) {
					n1 = LT(1);
					n1_AST = (antlr.CommonAST)astFactory.create(n1);
					match(NUMBER);
					if ( inputState.guessing==0 ) {
						vector_AST = (antlr.CommonAST)currentAST.root;
						vector_AST.addChild(n1_AST);
					}
				}
				else {
					if ( _cnt42>=1 ) { break _loop42; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt42++;
			} while (true);
			}
			{
			if ((LA(1)==CORC)) {
				match(CORC);
			}
			else if ((LA(1)==EOF)) {
				match(Token.EOF_TYPE);
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_4);
			} else {
			  throw ex;
			}
		}
		returnAST = vector_AST;
	}
	
	public final void matriz() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST matriz_AST = null;
		antlr.CommonAST n1_AST = null;
		
		try {      // for error handling
			match(CORO);
			if ( inputState.guessing==0 ) {
				matriz_AST = (antlr.CommonAST)currentAST.root;
				matriz_AST=((antlr.CommonAST)astFactory.create(MATRIZ));
				currentAST.root = matriz_AST;
				currentAST.child = matriz_AST!=null &&matriz_AST.getFirstChild()!=null ?
					matriz_AST.getFirstChild() : matriz_AST;
				currentAST.advanceChildToEnd();
			}
			{
			int _cnt47=0;
			_loop47:
			do {
				if ((LA(1)==CORO)) {
					vector();
					n1_AST = (antlr.CommonAST)returnAST;
					if ( inputState.guessing==0 ) {
						matriz_AST = (antlr.CommonAST)currentAST.root;
						matriz_AST.addChild(n1_AST);
					}
				}
				else {
					if ( _cnt47>=1 ) { break _loop47; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt47++;
			} while (true);
			}
			match(CORC);
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = matriz_AST;
	}
	
	public final void list() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST list_AST = null;
		antlr.CommonAST n1_AST = null;
		
		try {      // for error handling
			match(LLAA);
			if ( inputState.guessing==0 ) {
				list_AST = (antlr.CommonAST)currentAST.root;
				list_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(1)).add((antlr.CommonAST)astFactory.create(LIST)));
				currentAST.root = list_AST;
				currentAST.child = list_AST!=null &&list_AST.getFirstChild()!=null ?
					list_AST.getFirstChild() : list_AST;
				currentAST.advanceChildToEnd();
			}
			{
			_loop35:
			do {
				if ((_tokenSet_0.member(LA(1)))) {
					object();
					n1_AST = (antlr.CommonAST)returnAST;
					if ( inputState.guessing==0 ) {
						list_AST = (antlr.CommonAST)currentAST.root;
						list_AST.addChild(n1_AST);
					}
				}
				else {
					break _loop35;
				}
				
			} while (true);
			}
			{
			if ((LA(1)==LLAC)) {
				match(LLAC);
			}
			else if ((LA(1)==EOF)) {
				match(Token.EOF_TYPE);
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = list_AST;
	}
	
	public final void complex() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST complex_AST = null;
		Token  n1 = null;
		antlr.CommonAST n1_AST = null;
		Token  n2 = null;
		antlr.CommonAST n2_AST = null;
		
		try {      // for error handling
			match(LPAREN);
			n1 = LT(1);
			n1_AST = (antlr.CommonAST)astFactory.create(n1);
			match(NUMBER);
			antlr.CommonAST tmp34_AST = null;
			tmp34_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(COMMA);
			n2 = LT(1);
			n2_AST = (antlr.CommonAST)astFactory.create(n2);
			match(NUMBER);
			match(RPAREN);
			if ( inputState.guessing==0 ) {
				complex_AST = (antlr.CommonAST)currentAST.root;
				complex_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(COMPLEX)).add(n1_AST).add(n2_AST));
				currentAST.root = complex_AST;
				currentAST.child = complex_AST!=null &&complex_AST.getFirstChild()!=null ?
					complex_AST.getFirstChild() : complex_AST;
				currentAST.advanceChildToEnd();
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_5);
			} else {
			  throw ex;
			}
		}
		returnAST = complex_AST;
	}
	
	public final void nunit() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST nunit_AST = null;
		Token  n2 = null;
		antlr.CommonAST n2_AST = null;
		antlr.CommonAST n3_AST = null;
		
		try {      // for error handling
			n2 = LT(1);
			n2_AST = (antlr.CommonAST)astFactory.create(n2);
			match(NUMBER);
			if ( inputState.guessing==0 ) {
				nunit_AST = (antlr.CommonAST)currentAST.root;
				nunit_AST=n2_AST;
				currentAST.root = nunit_AST;
				currentAST.child = nunit_AST!=null &&nunit_AST.getFirstChild()!=null ?
					nunit_AST.getFirstChild() : nunit_AST;
				currentAST.advanceChildToEnd();
			}
			{
			if ((LA(1)==UNDERSCO)) {
				antlr.CommonAST tmp36_AST = null;
				tmp36_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(UNDERSCO);
				uExpr();
				n3_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					nunit_AST = (antlr.CommonAST)currentAST.root;
					nunit_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(UNIT)).add(nunit_AST).add(n3_AST));
					currentAST.root = nunit_AST;
					currentAST.child = nunit_AST!=null &&nunit_AST.getFirstChild()!=null ?
						nunit_AST.getFirstChild() : nunit_AST;
					currentAST.advanceChildToEnd();
				}
			}
			else if ((_tokenSet_5.member(LA(1)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_5);
			} else {
			  throw ex;
			}
		}
		returnAST = nunit_AST;
	}
	
	public final void operador() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST operador_AST = null;
		Token  n1 = null;
		antlr.CommonAST n1_AST = null;
		Token  i1 = null;
		antlr.CommonAST i1_AST = null;
		Token  n2 = null;
		antlr.CommonAST n2_AST = null;
		Token  n3 = null;
		antlr.CommonAST n3_AST = null;
		Token  n4 = null;
		antlr.CommonAST n4_AST = null;
		Token  n5 = null;
		antlr.CommonAST n5_AST = null;
		Token  c1 = null;
		antlr.CommonAST c1_AST = null;
		antlr.CommonAST c2_AST = null;
		Token  c3 = null;
		antlr.CommonAST c3_AST = null;
		Token  c6 = null;
		antlr.CommonAST c6_AST = null;
		antlr.CommonAST c4_AST = null;
		Token  c5 = null;
		antlr.CommonAST c5_AST = null;
		
		try {      // for error handling
			{
			switch ( LA(1)) {
			case CARET:
			{
				n1 = LT(1);
				n1_AST = (antlr.CommonAST)astFactory.create(n1);
				match(CARET);
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(n1_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case ASSIGN:
			{
				i1 = LT(1);
				i1_AST = (antlr.CommonAST)astFactory.create(i1);
				match(ASSIGN);
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(i1_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case MULT:
			{
				n2 = LT(1);
				n2_AST = (antlr.CommonAST)astFactory.create(n2);
				match(MULT);
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(n2_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case DIV:
			{
				n3 = LT(1);
				n3_AST = (antlr.CommonAST)astFactory.create(n3);
				match(DIV);
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(n3_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case PLUS:
			{
				n4 = LT(1);
				n4_AST = (antlr.CommonAST)astFactory.create(n4);
				match(PLUS);
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(n4_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case MINUS:
			{
				n5 = LT(1);
				n5_AST = (antlr.CommonAST)astFactory.create(n5);
				match(MINUS);
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(n5_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case LT:
			{
				c1 = LT(1);
				c1_AST = (antlr.CommonAST)astFactory.create(c1);
				match(LT);
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(c1_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case LE:
			case LEOC:
			{
				{
				if ((LA(1)==LE)) {
					antlr.CommonAST tmp37_AST = null;
					tmp37_AST = (antlr.CommonAST)astFactory.create(LT(1));
					match(LE);
				}
				else if ((LA(1)==LEOC)) {
					antlr.CommonAST tmp38_AST = null;
					tmp38_AST = (antlr.CommonAST)astFactory.create(LT(1));
					match(LEOC);
				}
				else {
					throw new NoViableAltException(LT(1), getFilename());
				}
				
				}
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(c2_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case EQ:
			{
				c3 = LT(1);
				c3_AST = (antlr.CommonAST)astFactory.create(c3);
				match(EQ);
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(c3_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case DISTI:
			{
				c6 = LT(1);
				c6_AST = (antlr.CommonAST)astFactory.create(c6);
				match(DISTI);
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(c6_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case GE:
			case GEOC:
			{
				{
				if ((LA(1)==GE)) {
					antlr.CommonAST tmp39_AST = null;
					tmp39_AST = (antlr.CommonAST)astFactory.create(LT(1));
					match(GE);
				}
				else if ((LA(1)==GEOC)) {
					antlr.CommonAST tmp40_AST = null;
					tmp40_AST = (antlr.CommonAST)astFactory.create(LT(1));
					match(GEOC);
				}
				else {
					throw new NoViableAltException(LT(1), getFilename());
				}
				
				}
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(c4_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case GT:
			{
				c5 = LT(1);
				c5_AST = (antlr.CommonAST)astFactory.create(c5);
				match(GT);
				if ( inputState.guessing==0 ) {
					operador_AST = (antlr.CommonAST)currentAST.root;
					operador_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(OPERADOR)).add(c5_AST));
					currentAST.root = operador_AST;
					currentAST.child = operador_AST!=null &&operador_AST.getFirstChild()!=null ?
						operador_AST.getFirstChild() : operador_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = operador_AST;
	}
	
	public final void string() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST string_AST = null;
		Token  n1 = null;
		antlr.CommonAST n1_AST = null;
		
		try {      // for error handling
			n1 = LT(1);
			n1_AST = (antlr.CommonAST)astFactory.create(n1);
			match(STRING_LITERAL);
			if ( inputState.guessing==0 ) {
				string_AST = (antlr.CommonAST)currentAST.root;
				string_AST=n1_AST;
				currentAST.root = string_AST;
				currentAST.child = string_AST!=null &&string_AST.getFirstChild()!=null ?
					string_AST.getFirstChild() : string_AST;
				currentAST.advanceChildToEnd();
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = string_AST;
	}
	
	public final void pob() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST pob_AST = null;
		Token  n1 = null;
		antlr.CommonAST n1_AST = null;
		antlr.CommonAST n2_AST = null;
		Token  n3 = null;
		antlr.CommonAST n3_AST = null;
		antlr.CommonAST s1_AST = null;
		Token  s2 = null;
		antlr.CommonAST s2_AST = null;
		Token  f1 = null;
		antlr.CommonAST f1_AST = null;
		antlr.CommonAST f2_AST = null;
		Token  r1 = null;
		antlr.CommonAST r1_AST = null;
		antlr.CommonAST r2_AST = null;
		antlr.CommonAST i1_AST = null;
		antlr.CommonAST i2_AST = null;
		antlr.CommonAST i3_AST = null;
		antlr.CommonAST e1_AST = null;
		antlr.CommonAST e2_AST = null;
		antlr.CommonAST d1_AST = null;
		antlr.CommonAST d2_AST = null;
		antlr.CommonAST w1_AST = null;
		antlr.CommonAST w2_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case LITERAL_FOR:
			{
				match(LITERAL_FOR);
				n1 = LT(1);
				n1_AST = (antlr.CommonAST)astFactory.create(n1);
				match(ID);
				seqOb();
				n2_AST = (antlr.CommonAST)returnAST;
				{
				if ((LA(1)==LITERAL_NEXT)) {
					match(LITERAL_NEXT);
					if ( inputState.guessing==0 ) {
						pob_AST = (antlr.CommonAST)currentAST.root;
						pob_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(FORNEXT)).add(n1_AST).add(n2_AST));
						currentAST.root = pob_AST;
						currentAST.child = pob_AST!=null &&pob_AST.getFirstChild()!=null ?
							pob_AST.getFirstChild() : pob_AST;
						currentAST.advanceChildToEnd();
					}
				}
				else if ((LA(1)==NUMBER)) {
					n3 = LT(1);
					n3_AST = (antlr.CommonAST)astFactory.create(n3);
					match(NUMBER);
					match(LITERAL_STEP);
					if ( inputState.guessing==0 ) {
						pob_AST = (antlr.CommonAST)currentAST.root;
						pob_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(4)).add((antlr.CommonAST)astFactory.create(FORSTEP)).add(n1_AST).add(n2_AST).add(n3_AST));
						currentAST.root = pob_AST;
						currentAST.child = pob_AST!=null &&pob_AST.getFirstChild()!=null ?
							pob_AST.getFirstChild() : pob_AST;
						currentAST.advanceChildToEnd();
					}
				}
				else {
					throw new NoViableAltException(LT(1), getFilename());
				}
				
				}
				break;
			}
			case LITERAL_START:
			{
				match(LITERAL_START);
				seqOb();
				s1_AST = (antlr.CommonAST)returnAST;
				{
				if ((LA(1)==LITERAL_NEXT)) {
					match(LITERAL_NEXT);
					if ( inputState.guessing==0 ) {
						pob_AST = (antlr.CommonAST)currentAST.root;
						pob_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(STARTNEXT)).add(s1_AST));
						currentAST.root = pob_AST;
						currentAST.child = pob_AST!=null &&pob_AST.getFirstChild()!=null ?
							pob_AST.getFirstChild() : pob_AST;
						currentAST.advanceChildToEnd();
					}
				}
				else if ((LA(1)==NUMBER)) {
					s2 = LT(1);
					s2_AST = (antlr.CommonAST)astFactory.create(s2);
					match(NUMBER);
					match(LITERAL_STEP);
					if ( inputState.guessing==0 ) {
						pob_AST = (antlr.CommonAST)currentAST.root;
						pob_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(STARTSTEP)).add(s1_AST).add(s2_AST));
						currentAST.root = pob_AST;
						currentAST.child = pob_AST!=null &&pob_AST.getFirstChild()!=null ?
							pob_AST.getFirstChild() : pob_AST;
						currentAST.advanceChildToEnd();
					}
				}
				else {
					throw new NoViableAltException(LT(1), getFilename());
				}
				
				}
				break;
			}
			case LITERAL_IF:
			{
				match(LITERAL_IF);
				seqOb();
				i1_AST = (antlr.CommonAST)returnAST;
				match(LITERAL_THEN);
				seqOb();
				i2_AST = (antlr.CommonAST)returnAST;
				{
				if ((LA(1)==LITERAL_END)) {
					match(LITERAL_END);
					if ( inputState.guessing==0 ) {
						pob_AST = (antlr.CommonAST)currentAST.root;
						pob_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(IF)).add(i1_AST).add(i2_AST));
						currentAST.root = pob_AST;
						currentAST.child = pob_AST!=null &&pob_AST.getFirstChild()!=null ?
							pob_AST.getFirstChild() : pob_AST;
						currentAST.advanceChildToEnd();
					}
				}
				else if ((LA(1)==LITERAL_ELSE)) {
					match(LITERAL_ELSE);
					seqOb();
					i3_AST = (antlr.CommonAST)returnAST;
					match(LITERAL_END);
					if ( inputState.guessing==0 ) {
						pob_AST = (antlr.CommonAST)currentAST.root;
						pob_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(4)).add((antlr.CommonAST)astFactory.create(IFELSE)).add(i1_AST).add(i2_AST).add(i3_AST));
						currentAST.root = pob_AST;
						currentAST.child = pob_AST!=null &&pob_AST.getFirstChild()!=null ?
							pob_AST.getFirstChild() : pob_AST;
						currentAST.advanceChildToEnd();
					}
				}
				else {
					throw new NoViableAltException(LT(1), getFilename());
				}
				
				}
				break;
			}
			case LITERAL_IFERR:
			{
				match(LITERAL_IFERR);
				seqOb();
				e1_AST = (antlr.CommonAST)returnAST;
				match(LITERAL_THEN);
				seqOb();
				e2_AST = (antlr.CommonAST)returnAST;
				match(LITERAL_END);
				if ( inputState.guessing==0 ) {
					pob_AST = (antlr.CommonAST)currentAST.root;
					pob_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(IFERR)).add(e1_AST).add(e2_AST));
					currentAST.root = pob_AST;
					currentAST.child = pob_AST!=null &&pob_AST.getFirstChild()!=null ?
						pob_AST.getFirstChild() : pob_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case LITERAL_DO:
			{
				match(LITERAL_DO);
				seqOb();
				d1_AST = (antlr.CommonAST)returnAST;
				match(LITERAL_UNTIL);
				seqOb();
				d2_AST = (antlr.CommonAST)returnAST;
				match(LITERAL_END);
				if ( inputState.guessing==0 ) {
					pob_AST = (antlr.CommonAST)currentAST.root;
					pob_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(DOUNTIL)).add(d1_AST).add(d2_AST));
					currentAST.root = pob_AST;
					currentAST.child = pob_AST!=null &&pob_AST.getFirstChild()!=null ?
						pob_AST.getFirstChild() : pob_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case LITERAL_WHILE:
			{
				match(LITERAL_WHILE);
				seqOb();
				w1_AST = (antlr.CommonAST)returnAST;
				match(LITERAL_REPEAT);
				seqOb();
				w2_AST = (antlr.CommonAST)returnAST;
				match(LITERAL_END);
				if ( inputState.guessing==0 ) {
					pob_AST = (antlr.CommonAST)currentAST.root;
					pob_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(WHILEREPEAT)).add(w1_AST).add(w2_AST));
					currentAST.root = pob_AST;
					currentAST.child = pob_AST!=null &&pob_AST.getFirstChild()!=null ?
						pob_AST.getFirstChild() : pob_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			default:
				if ((LA(1)==FLECHA) && (LA(2)==ID) && (LA(3)==ID||LA(3)==OPENPROG||LA(3)==OPENPROGOC) && (_tokenSet_0.member(LA(4))) && (_tokenSet_6.member(LA(5)))) {
					antlr.CommonAST tmp61_AST = null;
					tmp61_AST = (antlr.CommonAST)astFactory.create(LT(1));
					match(FLECHA);
					if ( inputState.guessing==0 ) {
						pob_AST = (antlr.CommonAST)currentAST.root;
						pob_AST=(antlr.CommonAST)astFactory.create(LOCALVAR);
						currentAST.root = pob_AST;
						currentAST.child = pob_AST!=null &&pob_AST.getFirstChild()!=null ?
							pob_AST.getFirstChild() : pob_AST;
						currentAST.advanceChildToEnd();
					}
					{
					int _cnt83=0;
					_loop83:
					do {
						if ((LA(1)==ID)) {
							f1 = LT(1);
							f1_AST = (antlr.CommonAST)astFactory.create(f1);
							match(ID);
							if ( inputState.guessing==0 ) {
								pob_AST = (antlr.CommonAST)currentAST.root;
								pob_AST.addChild(f1_AST);
							}
						}
						else {
							if ( _cnt83>=1 ) { break _loop83; } else {throw new NoViableAltException(LT(1), getFilename());}
						}
						
						_cnt83++;
					} while (true);
					}
					program();
					f2_AST = (antlr.CommonAST)returnAST;
					if ( inputState.guessing==0 ) {
						pob_AST = (antlr.CommonAST)currentAST.root;
						pob_AST.addChild(f2_AST);
					}
				}
				else if ((LA(1)==FLECHA) && (LA(2)==ID) && (LA(3)==ID||LA(3)==SCOMI) && (_tokenSet_7.member(LA(4))) && (_tokenSet_8.member(LA(5)))) {
					antlr.CommonAST tmp62_AST = null;
					tmp62_AST = (antlr.CommonAST)astFactory.create(LT(1));
					match(FLECHA);
					if ( inputState.guessing==0 ) {
						pob_AST = (antlr.CommonAST)currentAST.root;
						pob_AST=(antlr.CommonAST)astFactory.create(LOCALVAR);
						currentAST.root = pob_AST;
						currentAST.child = pob_AST!=null &&pob_AST.getFirstChild()!=null ?
							pob_AST.getFirstChild() : pob_AST;
						currentAST.advanceChildToEnd();
					}
					{
					int _cnt85=0;
					_loop85:
					do {
						if ((LA(1)==ID)) {
							r1 = LT(1);
							r1_AST = (antlr.CommonAST)astFactory.create(r1);
							match(ID);
							if ( inputState.guessing==0 ) {
								pob_AST = (antlr.CommonAST)currentAST.root;
								pob_AST.addChild(r1_AST);
							}
						}
						else {
							if ( _cnt85>=1 ) { break _loop85; } else {throw new NoViableAltException(LT(1), getFilename());}
						}
						
						_cnt85++;
					} while (true);
					}
					infixExpression();
					r2_AST = (antlr.CommonAST)returnAST;
					if ( inputState.guessing==0 ) {
						pob_AST = (antlr.CommonAST)currentAST.root;
						pob_AST.addChild(r2_AST);
					}
				}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = pob_AST;
	}
	
	public final void specialCommand() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST specialCommand_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case TOUNIT:
			{
				antlr.CommonAST tmp63_AST = null;
				tmp63_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp63_AST);
				match(TOUNIT);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case VTO:
			{
				antlr.CommonAST tmp64_AST = null;
				tmp64_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp64_AST);
				match(VTO);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case TOV2:
			{
				antlr.CommonAST tmp65_AST = null;
				tmp65_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp65_AST);
				match(TOV2);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case TOV3:
			{
				antlr.CommonAST tmp66_AST = null;
				tmp66_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp66_AST);
				match(TOV3);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case DTOR:
			{
				antlr.CommonAST tmp67_AST = null;
				tmp67_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp67_AST);
				match(DTOR);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case RTOD:
			{
				antlr.CommonAST tmp68_AST = null;
				tmp68_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp68_AST);
				match(RTOD);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case RTOB:
			{
				antlr.CommonAST tmp69_AST = null;
				tmp69_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp69_AST);
				match(RTOB);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case BTOR:
			{
				antlr.CommonAST tmp70_AST = null;
				tmp70_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp70_AST);
				match(BTOR);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case OBJTO:
			{
				antlr.CommonAST tmp71_AST = null;
				tmp71_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp71_AST);
				match(OBJTO);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case EQTO:
			{
				antlr.CommonAST tmp72_AST = null;
				tmp72_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp72_AST);
				match(EQTO);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case TOARRY:
			{
				antlr.CommonAST tmp73_AST = null;
				tmp73_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp73_AST);
				match(TOARRY);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case TOLIST:
			{
				antlr.CommonAST tmp74_AST = null;
				tmp74_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp74_AST);
				match(TOLIST);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case TOSTR:
			{
				antlr.CommonAST tmp75_AST = null;
				tmp75_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp75_AST);
				match(TOSTR);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case TOTAG:
			{
				antlr.CommonAST tmp76_AST = null;
				tmp76_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp76_AST);
				match(TOTAG);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case RTOC:
			{
				antlr.CommonAST tmp77_AST = null;
				tmp77_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp77_AST);
				match(RTOC);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case CTOR:
			{
				antlr.CommonAST tmp78_AST = null;
				tmp78_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp78_AST);
				match(CTOR);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case FSINT:
			{
				antlr.CommonAST tmp79_AST = null;
				tmp79_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp79_AST);
				match(FSINT);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case FCINT:
			{
				antlr.CommonAST tmp80_AST = null;
				tmp80_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp80_AST);
				match(FCINT);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case FSINTC:
			{
				antlr.CommonAST tmp81_AST = null;
				tmp81_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp81_AST);
				match(FSINTC);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			case FCINTC:
			{
				antlr.CommonAST tmp82_AST = null;
				tmp82_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp82_AST);
				match(FCINTC);
				specialCommand_AST = (antlr.CommonAST)currentAST.root;
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = specialCommand_AST;
	}
	
	public final void directorio() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST directorio_AST = null;
		Token  varName = null;
		antlr.CommonAST varName_AST = null;
		Token  obj = null;
		antlr.CommonAST obj_AST = null;
		
		try {      // for error handling
			antlr.CommonAST tmp83_AST = null;
			tmp83_AST = (antlr.CommonAST)astFactory.create(LT(1));
			astFactory.addASTChild(currentAST, tmp83_AST);
			match(LITERAL_DIR);
			if ( inputState.guessing==0 ) {
				directorio_AST = (antlr.CommonAST)currentAST.root;
				directorio_AST=(antlr.CommonAST)astFactory.create(DIRECTORIO);
				currentAST.root = directorio_AST;
				currentAST.child = directorio_AST!=null &&directorio_AST.getFirstChild()!=null ?
					directorio_AST.getFirstChild() : directorio_AST;
				currentAST.advanceChildToEnd();
			}
			{
			_loop10:
			do {
				if ((LA(1)==ID)) {
					varName = LT(1);
					varName_AST = (antlr.CommonAST)astFactory.create(varName);
					astFactory.addASTChild(currentAST, varName_AST);
					match(ID);
					obj = LT(1);
					obj_AST = (antlr.CommonAST)astFactory.create(obj);
					astFactory.addASTChild(currentAST, obj_AST);
					match(OBJ);
					if ( inputState.guessing==0 ) {
						directorio_AST = (antlr.CommonAST)currentAST.root;
						directorio_AST.addChild(varName_AST); directorio_AST.addChild(obj_AST);
					}
				}
				else {
					break _loop10;
				}
				
			} while (true);
			}
			antlr.CommonAST tmp84_AST = null;
			tmp84_AST = (antlr.CommonAST)astFactory.create(LT(1));
			astFactory.addASTChild(currentAST, tmp84_AST);
			match(LITERAL_END);
			directorio_AST = (antlr.CommonAST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = directorio_AST;
	}
	
	public final void grob() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST grob_AST = null;
		
		try {      // for error handling
			antlr.CommonAST tmp85_AST = null;
			tmp85_AST = (antlr.CommonAST)astFactory.create(LT(1));
			astFactory.addASTChild(currentAST, tmp85_AST);
			match(GROB);
			grob_AST = (antlr.CommonAST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_2);
			} else {
			  throw ex;
			}
		}
		returnAST = grob_AST;
	}
	
	public final void uExpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST uExpr_AST = null;
		
		try {      // for error handling
			{
			if ((LA(1)==ID)) {
				uEExpr();
				astFactory.addASTChild(currentAST, returnAST);
			}
			else if ((LA(1)==NUMBER)) {
				antlr.CommonAST tmp86_AST = null;
				tmp86_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp86_AST);
				match(NUMBER);
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			{
			if ((LA(1)==MULT||LA(1)==DIV) && (LA(2)==ID||LA(2)==NUMBER) && (_tokenSet_5.member(LA(3))) && (_tokenSet_9.member(LA(4))) && (_tokenSet_10.member(LA(5)))) {
				{
				if ((LA(1)==MULT)) {
					antlr.CommonAST tmp87_AST = null;
					tmp87_AST = (antlr.CommonAST)astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp87_AST);
					match(MULT);
				}
				else if ((LA(1)==DIV)) {
					antlr.CommonAST tmp88_AST = null;
					tmp88_AST = (antlr.CommonAST)astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp88_AST);
					match(DIV);
				}
				else {
					throw new NoViableAltException(LT(1), getFilename());
				}
				
				}
				uExpr();
				astFactory.addASTChild(currentAST, returnAST);
			}
			else if ((_tokenSet_5.member(LA(1))) && (_tokenSet_9.member(LA(2))) && (_tokenSet_10.member(LA(3))) && (_tokenSet_10.member(LA(4))) && (_tokenSet_10.member(LA(5)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			uExpr_AST = (antlr.CommonAST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_5);
			} else {
			  throw ex;
			}
		}
		returnAST = uExpr_AST;
	}
	
	public final void uEExpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST uEExpr_AST = null;
		
		try {      // for error handling
			antlr.CommonAST tmp89_AST = null;
			tmp89_AST = (antlr.CommonAST)astFactory.create(LT(1));
			astFactory.addASTChild(currentAST, tmp89_AST);
			match(ID);
			{
			if ((LA(1)==CARET) && (LA(2)==NUMBER) && (_tokenSet_5.member(LA(3))) && (_tokenSet_9.member(LA(4))) && (_tokenSet_10.member(LA(5)))) {
				antlr.CommonAST tmp90_AST = null;
				tmp90_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.makeASTRoot(currentAST, tmp90_AST);
				match(CARET);
				antlr.CommonAST tmp91_AST = null;
				tmp91_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.addASTChild(currentAST, tmp91_AST);
				match(NUMBER);
			}
			else if ((_tokenSet_5.member(LA(1))) && (_tokenSet_9.member(LA(2))) && (_tokenSet_10.member(LA(3))) && (_tokenSet_10.member(LA(4))) && (_tokenSet_10.member(LA(5)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			uEExpr_AST = (antlr.CommonAST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_5);
			} else {
			  throw ex;
			}
		}
		returnAST = uEExpr_AST;
	}
	
	public final void seqOb() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST seqOb_AST = null;
		antlr.CommonAST n1_AST = null;
		
		try {      // for error handling
			if ( inputState.guessing==0 ) {
				seqOb_AST = (antlr.CommonAST)currentAST.root;
				seqOb_AST=(antlr.CommonAST)astFactory.create(SEQOB);
				currentAST.root = seqOb_AST;
				currentAST.child = seqOb_AST!=null &&seqOb_AST.getFirstChild()!=null ?
					seqOb_AST.getFirstChild() : seqOb_AST;
				currentAST.advanceChildToEnd();
			}
			{
			int _cnt22=0;
			_loop22:
			do {
				if ((_tokenSet_0.member(LA(1))) && (_tokenSet_11.member(LA(2))) && (_tokenSet_12.member(LA(3))) && (_tokenSet_10.member(LA(4))) && (_tokenSet_10.member(LA(5)))) {
					object();
					n1_AST = (antlr.CommonAST)returnAST;
					if ( inputState.guessing==0 ) {
						seqOb_AST = (antlr.CommonAST)currentAST.root;
						seqOb_AST.addChild(n1_AST);
					}
				}
				else {
					if ( _cnt22>=1 ) { break _loop22; } else {throw new NoViableAltException(LT(1), getFilename());}
				}
				
				_cnt22++;
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_13);
			} else {
			  throw ex;
			}
		}
		returnAST = seqOb_AST;
	}
	
	public final void literalIdentifier() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST literalIdentifier_AST = null;
		Token  n1 = null;
		antlr.CommonAST n1_AST = null;
		
		try {      // for error handling
			match(SCOMI);
			n1 = LT(1);
			n1_AST = (antlr.CommonAST)astFactory.create(n1);
			match(ID);
			if ( inputState.guessing==0 ) {
				literalIdentifier_AST = (antlr.CommonAST)currentAST.root;
				literalIdentifier_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(LITERAL)).add(n1_AST));
				currentAST.root = literalIdentifier_AST;
				currentAST.child = literalIdentifier_AST!=null &&literalIdentifier_AST.getFirstChild()!=null ?
					literalIdentifier_AST.getFirstChild() : literalIdentifier_AST;
				currentAST.advanceChildToEnd();
			}
			{
			if ((LA(1)==SCOMI)) {
				match(SCOMI);
			}
			else if ((LA(1)==EOF)) {
				match(Token.EOF_TYPE);
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_1);
			} else {
			  throw ex;
			}
		}
		returnAST = literalIdentifier_AST;
	}
	
	public final void assignExpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST assignExpr_AST = null;
		
		try {      // for error handling
			testExpr();
			astFactory.addASTChild(currentAST, returnAST);
			{
			if ((LA(1)==ASSIGN)) {
				antlr.CommonAST tmp95_AST = null;
				tmp95_AST = (antlr.CommonAST)astFactory.create(LT(1));
				astFactory.makeASTRoot(currentAST, tmp95_AST);
				match(ASSIGN);
				assignExpr();
				astFactory.addASTChild(currentAST, returnAST);
			}
			else if ((_tokenSet_14.member(LA(1)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			assignExpr_AST = (antlr.CommonAST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_14);
			} else {
			  throw ex;
			}
		}
		returnAST = assignExpr_AST;
	}
	
	public final void varAt() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST varAt_AST = null;
		Token  n1 = null;
		antlr.CommonAST n1_AST = null;
		antlr.CommonAST n2_AST = null;
		
		try {      // for error handling
			antlr.CommonAST tmp96_AST = null;
			tmp96_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(VBAR);
			antlr.CommonAST tmp97_AST = null;
			tmp97_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(LPAREN);
			n1 = LT(1);
			n1_AST = (antlr.CommonAST)astFactory.create(n1);
			match(ID);
			antlr.CommonAST tmp98_AST = null;
			tmp98_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(ASSIGN);
			addExpr();
			n2_AST = (antlr.CommonAST)returnAST;
			antlr.CommonAST tmp99_AST = null;
			tmp99_AST = (antlr.CommonAST)astFactory.create(LT(1));
			match(RPAREN);
			if ( inputState.guessing==0 ) {
				varAt_AST = (antlr.CommonAST)currentAST.root;
				varAt_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(VARAT)).add(n1_AST).add(n2_AST));
				currentAST.root = varAt_AST;
				currentAST.child = varAt_AST!=null &&varAt_AST.getFirstChild()!=null ?
					varAt_AST.getFirstChild() : varAt_AST;
				currentAST.advanceChildToEnd();
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_15);
			} else {
			  throw ex;
			}
		}
		returnAST = varAt_AST;
	}
	
	public final void testExpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST testExpr_AST = null;
		
		try {      // for error handling
			boolExpr();
			astFactory.addASTChild(currentAST, returnAST);
			{
			if ((_tokenSet_16.member(LA(1)))) {
				{
				switch ( LA(1)) {
				case LT:
				{
					antlr.CommonAST tmp100_AST = null;
					tmp100_AST = (antlr.CommonAST)astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp100_AST);
					match(LT);
					break;
				}
				case LE:
				{
					antlr.CommonAST tmp101_AST = null;
					tmp101_AST = (antlr.CommonAST)astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp101_AST);
					match(LE);
					break;
				}
				case EQ:
				{
					antlr.CommonAST tmp102_AST = null;
					tmp102_AST = (antlr.CommonAST)astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp102_AST);
					match(EQ);
					break;
				}
				case GE:
				{
					antlr.CommonAST tmp103_AST = null;
					tmp103_AST = (antlr.CommonAST)astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp103_AST);
					match(GE);
					break;
				}
				case GT:
				{
					antlr.CommonAST tmp104_AST = null;
					tmp104_AST = (antlr.CommonAST)astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp104_AST);
					match(GT);
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				boolExpr();
				astFactory.addASTChild(currentAST, returnAST);
			}
			else if ((_tokenSet_17.member(LA(1)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			testExpr_AST = (antlr.CommonAST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_17);
			} else {
			  throw ex;
			}
		}
		returnAST = testExpr_AST;
	}
	
	public final void boolExpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST boolExpr_AST = null;
		
		try {      // for error handling
			addExpr();
			astFactory.addASTChild(currentAST, returnAST);
			{
			if (((LA(1) >= LITERAL_AND && LA(1) <= LITERAL_XOR))) {
				{
				switch ( LA(1)) {
				case LITERAL_AND:
				{
					antlr.CommonAST tmp105_AST = null;
					tmp105_AST = (antlr.CommonAST)astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp105_AST);
					match(LITERAL_AND);
					break;
				}
				case LITERAL_OR:
				{
					antlr.CommonAST tmp106_AST = null;
					tmp106_AST = (antlr.CommonAST)astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp106_AST);
					match(LITERAL_OR);
					break;
				}
				case LITERAL_XOR:
				{
					antlr.CommonAST tmp107_AST = null;
					tmp107_AST = (antlr.CommonAST)astFactory.create(LT(1));
					astFactory.addASTChild(currentAST, tmp107_AST);
					match(LITERAL_XOR);
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				}
				}
				addExpr();
				astFactory.addASTChild(currentAST, returnAST);
			}
			else if ((_tokenSet_18.member(LA(1)))) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			boolExpr_AST = (antlr.CommonAST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_18);
			} else {
			  throw ex;
			}
		}
		returnAST = boolExpr_AST;
	}
	
	public final void addExpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST addExpr_AST = null;
		
		try {      // for error handling
			multExpr();
			astFactory.addASTChild(currentAST, returnAST);
			{
			_loop59:
			do {
				if ((LA(1)==PLUS||LA(1)==MINUS)) {
					{
					if ((LA(1)==PLUS)) {
						antlr.CommonAST tmp108_AST = null;
						tmp108_AST = (antlr.CommonAST)astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp108_AST);
						match(PLUS);
					}
					else if ((LA(1)==MINUS)) {
						antlr.CommonAST tmp109_AST = null;
						tmp109_AST = (antlr.CommonAST)astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp109_AST);
						match(MINUS);
					}
					else {
						throw new NoViableAltException(LT(1), getFilename());
					}
					
					}
					multExpr();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop59;
				}
				
			} while (true);
			}
			addExpr_AST = (antlr.CommonAST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_19);
			} else {
			  throw ex;
			}
		}
		returnAST = addExpr_AST;
	}
	
	public final void multExpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST multExpr_AST = null;
		
		try {      // for error handling
			exponentExpr();
			astFactory.addASTChild(currentAST, returnAST);
			{
			_loop63:
			do {
				if ((LA(1)==MULT||LA(1)==DIV)) {
					{
					if ((LA(1)==MULT)) {
						antlr.CommonAST tmp110_AST = null;
						tmp110_AST = (antlr.CommonAST)astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp110_AST);
						match(MULT);
					}
					else if ((LA(1)==DIV)) {
						antlr.CommonAST tmp111_AST = null;
						tmp111_AST = (antlr.CommonAST)astFactory.create(LT(1));
						astFactory.makeASTRoot(currentAST, tmp111_AST);
						match(DIV);
					}
					else {
						throw new NoViableAltException(LT(1), getFilename());
					}
					
					}
					exponentExpr();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop63;
				}
				
			} while (true);
			}
			multExpr_AST = (antlr.CommonAST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_20);
			} else {
			  throw ex;
			}
		}
		returnAST = multExpr_AST;
	}
	
	public final void exponentExpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST exponentExpr_AST = null;
		
		try {      // for error handling
			postfixExpr();
			astFactory.addASTChild(currentAST, returnAST);
			{
			_loop66:
			do {
				if ((LA(1)==CARET)) {
					antlr.CommonAST tmp112_AST = null;
					tmp112_AST = (antlr.CommonAST)astFactory.create(LT(1));
					astFactory.makeASTRoot(currentAST, tmp112_AST);
					match(CARET);
					postfixExpr();
					astFactory.addASTChild(currentAST, returnAST);
				}
				else {
					break _loop66;
				}
				
			} while (true);
			}
			exponentExpr_AST = (antlr.CommonAST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_21);
			} else {
			  throw ex;
			}
		}
		returnAST = exponentExpr_AST;
	}
	
	public final void postfixExpr() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST postfixExpr_AST = null;
		Token  n1 = null;
		antlr.CommonAST n1_AST = null;
		antlr.CommonAST n2_AST = null;
		Token  n3 = null;
		antlr.CommonAST n3_AST = null;
		Token  u1 = null;
		antlr.CommonAST u1_AST = null;
		antlr.CommonAST u2_AST = null;
		antlr.CommonAST n4_AST = null;
		antlr.CommonAST n5_AST = null;
		antlr.CommonAST n6_AST = null;
		antlr.CommonAST n7_AST = null;
		Token  derid = null;
		antlr.CommonAST derid_AST = null;
		antlr.CommonAST derfun_AST = null;
		antlr.CommonAST intdown_AST = null;
		antlr.CommonAST intup_AST = null;
		antlr.CommonAST intfun_AST = null;
		Token  intid = null;
		antlr.CommonAST intid_AST = null;
		Token  sigid = null;
		antlr.CommonAST sigid_AST = null;
		antlr.CommonAST sigdown_AST = null;
		antlr.CommonAST sigup_AST = null;
		antlr.CommonAST sigfun_AST = null;
		
		try {      // for error handling
			switch ( LA(1)) {
			case NUMBER:
			{
				nunit();
				n4_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					postfixExpr_AST = (antlr.CommonAST)currentAST.root;
					postfixExpr_AST=n4_AST;
					currentAST.root = postfixExpr_AST;
					currentAST.child = postfixExpr_AST!=null &&postfixExpr_AST.getFirstChild()!=null ?
						postfixExpr_AST.getFirstChild() : postfixExpr_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case MINUS:
			{
				antlr.CommonAST tmp113_AST = null;
				tmp113_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(MINUS);
				postfixExpr();
				n7_AST = (antlr.CommonAST)returnAST;
				if ( inputState.guessing==0 ) {
					postfixExpr_AST = (antlr.CommonAST)currentAST.root;
					postfixExpr_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(2)).add((antlr.CommonAST)astFactory.create(NEG)).add(n7_AST));
					currentAST.root = postfixExpr_AST;
					currentAST.child = postfixExpr_AST!=null &&postfixExpr_AST.getFirstChild()!=null ?
						postfixExpr_AST.getFirstChild() : postfixExpr_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case DERIV:
			{
				antlr.CommonAST tmp114_AST = null;
				tmp114_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(DERIV);
				derid = LT(1);
				derid_AST = (antlr.CommonAST)astFactory.create(derid);
				match(ID);
				antlr.CommonAST tmp115_AST = null;
				tmp115_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(LPAREN);
				assignExpr();
				derfun_AST = (antlr.CommonAST)returnAST;
				antlr.CommonAST tmp116_AST = null;
				tmp116_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(RPAREN);
				if ( inputState.guessing==0 ) {
					postfixExpr_AST = (antlr.CommonAST)currentAST.root;
					postfixExpr_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(tmp114_AST)).add(derfun_AST).add(derid_AST));
					currentAST.root = postfixExpr_AST;
					currentAST.child = postfixExpr_AST!=null &&postfixExpr_AST.getFirstChild()!=null ?
						postfixExpr_AST.getFirstChild() : postfixExpr_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case INTEG:
			{
				antlr.CommonAST tmp117_AST = null;
				tmp117_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(INTEG);
				antlr.CommonAST tmp118_AST = null;
				tmp118_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(LPAREN);
				assignExpr();
				intdown_AST = (antlr.CommonAST)returnAST;
				antlr.CommonAST tmp119_AST = null;
				tmp119_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(COMMA);
				assignExpr();
				intup_AST = (antlr.CommonAST)returnAST;
				antlr.CommonAST tmp120_AST = null;
				tmp120_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(COMMA);
				assignExpr();
				intfun_AST = (antlr.CommonAST)returnAST;
				antlr.CommonAST tmp121_AST = null;
				tmp121_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(COMMA);
				intid = LT(1);
				intid_AST = (antlr.CommonAST)astFactory.create(intid);
				match(ID);
				antlr.CommonAST tmp122_AST = null;
				tmp122_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(RPAREN);
				if ( inputState.guessing==0 ) {
					postfixExpr_AST = (antlr.CommonAST)currentAST.root;
					postfixExpr_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(5)).add((antlr.CommonAST)astFactory.create(tmp117_AST)).add(intdown_AST).add(intup_AST).add(intfun_AST).add(intid_AST));
					currentAST.root = postfixExpr_AST;
					currentAST.child = postfixExpr_AST!=null &&postfixExpr_AST.getFirstChild()!=null ?
						postfixExpr_AST.getFirstChild() : postfixExpr_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			case SIGMA:
			{
				antlr.CommonAST tmp123_AST = null;
				tmp123_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(SIGMA);
				antlr.CommonAST tmp124_AST = null;
				tmp124_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(LPAREN);
				sigid = LT(1);
				sigid_AST = (antlr.CommonAST)astFactory.create(sigid);
				match(ID);
				antlr.CommonAST tmp125_AST = null;
				tmp125_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(ASSIGN);
				assignExpr();
				sigdown_AST = (antlr.CommonAST)returnAST;
				antlr.CommonAST tmp126_AST = null;
				tmp126_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(COMMA);
				assignExpr();
				sigup_AST = (antlr.CommonAST)returnAST;
				antlr.CommonAST tmp127_AST = null;
				tmp127_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(COMMA);
				assignExpr();
				sigfun_AST = (antlr.CommonAST)returnAST;
				antlr.CommonAST tmp128_AST = null;
				tmp128_AST = (antlr.CommonAST)astFactory.create(LT(1));
				match(RPAREN);
				if ( inputState.guessing==0 ) {
					postfixExpr_AST = (antlr.CommonAST)currentAST.root;
					postfixExpr_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(5)).add((antlr.CommonAST)astFactory.create(tmp123_AST)).add(sigid_AST).add(sigdown_AST).add(sigup_AST).add(sigfun_AST));
					currentAST.root = postfixExpr_AST;
					currentAST.child = postfixExpr_AST!=null &&postfixExpr_AST.getFirstChild()!=null ?
						postfixExpr_AST.getFirstChild() : postfixExpr_AST;
					currentAST.advanceChildToEnd();
				}
				break;
			}
			default:
				boolean synPredMatched69 = false;
				if (((LA(1)==ID) && (LA(2)==LPAREN))) {
					int _m69 = mark();
					synPredMatched69 = true;
					inputState.guessing++;
					try {
						{
						match(ID);
						match(LPAREN);
						}
					}
					catch (RecognitionException pe) {
						synPredMatched69 = false;
					}
					rewind(_m69);
					inputState.guessing--;
				}
				if ( synPredMatched69 ) {
					n1 = LT(1);
					n1_AST = (antlr.CommonAST)astFactory.create(n1);
					match(ID);
					parenArgs();
					n2_AST = (antlr.CommonAST)returnAST;
					if ( inputState.guessing==0 ) {
						postfixExpr_AST = (antlr.CommonAST)currentAST.root;
						postfixExpr_AST=n1_AST; n1_AST.addChild(n2_AST);
						currentAST.root = postfixExpr_AST;
						currentAST.child = postfixExpr_AST!=null &&postfixExpr_AST.getFirstChild()!=null ?
							postfixExpr_AST.getFirstChild() : postfixExpr_AST;
						currentAST.advanceChildToEnd();
					}
				}
				else if ((LA(1)==ID) && (_tokenSet_22.member(LA(2)))) {
					n3 = LT(1);
					n3_AST = (antlr.CommonAST)astFactory.create(n3);
					match(ID);
					if ( inputState.guessing==0 ) {
						postfixExpr_AST = (antlr.CommonAST)currentAST.root;
						postfixExpr_AST=n3_AST;
						currentAST.root = postfixExpr_AST;
						currentAST.child = postfixExpr_AST!=null &&postfixExpr_AST.getFirstChild()!=null ?
							postfixExpr_AST.getFirstChild() : postfixExpr_AST;
						currentAST.advanceChildToEnd();
					}
				}
				else {
					boolean synPredMatched71 = false;
					if (((LA(1)==ID) && (LA(2)==UNDERSCO))) {
						int _m71 = mark();
						synPredMatched71 = true;
						inputState.guessing++;
						try {
							{
							match(ID);
							match(UNDERSCO);
							}
						}
						catch (RecognitionException pe) {
							synPredMatched71 = false;
						}
						rewind(_m71);
						inputState.guessing--;
					}
					if ( synPredMatched71 ) {
						u1 = LT(1);
						u1_AST = (antlr.CommonAST)astFactory.create(u1);
						match(ID);
						antlr.CommonAST tmp129_AST = null;
						tmp129_AST = (antlr.CommonAST)astFactory.create(LT(1));
						match(UNDERSCO);
						uExpr();
						u2_AST = (antlr.CommonAST)returnAST;
						if ( inputState.guessing==0 ) {
							postfixExpr_AST = (antlr.CommonAST)currentAST.root;
							AST uno=(antlr.CommonAST)astFactory.make( (new ASTArray(1)).add((antlr.CommonAST)astFactory.create(NUMBER)));
							uno.setText("1");
							AST theUnit=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(UNIT)).add(uno).add(u2_AST));
							postfixExpr_AST=(antlr.CommonAST)astFactory.make( (new ASTArray(3)).add((antlr.CommonAST)astFactory.create(MULT)).add(u1_AST).add(theUnit));
							postfixExpr_AST.setText("*");
							
							currentAST.root = postfixExpr_AST;
							currentAST.child = postfixExpr_AST!=null &&postfixExpr_AST.getFirstChild()!=null ?
								postfixExpr_AST.getFirstChild() : postfixExpr_AST;
							currentAST.advanceChildToEnd();
						}
					}
					else {
						boolean synPredMatched73 = false;
						if (((LA(1)==LPAREN) && (LA(2)==NUMBER) && (LA(3)==COMMA))) {
							int _m73 = mark();
							synPredMatched73 = true;
							inputState.guessing++;
							try {
								{
								match(LPAREN);
								match(NUMBER);
								match(COMMA);
								match(NUMBER);
								match(RPAREN);
								}
							}
							catch (RecognitionException pe) {
								synPredMatched73 = false;
							}
							rewind(_m73);
							inputState.guessing--;
						}
						if ( synPredMatched73 ) {
							complex();
							n5_AST = (antlr.CommonAST)returnAST;
							if ( inputState.guessing==0 ) {
								postfixExpr_AST = (antlr.CommonAST)currentAST.root;
								postfixExpr_AST=n5_AST;
								currentAST.root = postfixExpr_AST;
								currentAST.child = postfixExpr_AST!=null &&postfixExpr_AST.getFirstChild()!=null ?
									postfixExpr_AST.getFirstChild() : postfixExpr_AST;
								currentAST.advanceChildToEnd();
							}
						}
						else if ((LA(1)==LPAREN) && (_tokenSet_23.member(LA(2))) && (_tokenSet_24.member(LA(3)))) {
							match(LPAREN);
							assignExpr();
							n6_AST = (antlr.CommonAST)returnAST;
							match(RPAREN);
							if ( inputState.guessing==0 ) {
								postfixExpr_AST = (antlr.CommonAST)currentAST.root;
								postfixExpr_AST=n6_AST;
								currentAST.root = postfixExpr_AST;
								currentAST.child = postfixExpr_AST!=null &&postfixExpr_AST.getFirstChild()!=null ?
									postfixExpr_AST.getFirstChild() : postfixExpr_AST;
								currentAST.advanceChildToEnd();
							}
						}
					else {
						throw new NoViableAltException(LT(1), getFilename());
					}
					}}}
				}
				catch (RecognitionException ex) {
					if (inputState.guessing==0) {
						reportError(ex);
						consume();
						consumeUntil(_tokenSet_22);
					} else {
					  throw ex;
					}
				}
				returnAST = postfixExpr_AST;
			}
			
	public final void parenArgs() throws RecognitionException, TokenStreamException {
		
		returnAST = null;
		ASTPair currentAST = new ASTPair();
		antlr.CommonAST parenArgs_AST = null;
		
		try {      // for error handling
			match(LPAREN);
			{
			if ((_tokenSet_23.member(LA(1)))) {
				assignExpr();
				astFactory.addASTChild(currentAST, returnAST);
				{
				_loop77:
				do {
					if ((LA(1)==COMMA)) {
						match(COMMA);
						assignExpr();
						astFactory.addASTChild(currentAST, returnAST);
					}
					else {
						break _loop77;
					}
					
				} while (true);
				}
			}
			else if ((LA(1)==RPAREN)) {
			}
			else {
				throw new NoViableAltException(LT(1), getFilename());
			}
			
			}
			match(RPAREN);
			parenArgs_AST = (antlr.CommonAST)currentAST.root;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				consume();
				consumeUntil(_tokenSet_22);
			} else {
			  throw ex;
			}
		}
		returnAST = parenArgs_AST;
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"\"program\"",
		"\"literal\"",
		"\"infix\"",
		"\"vector\"",
		"\"matriz\"",
		"\"list\"",
		"\"OPERADOR\"",
		"\"string\"",
		"\"localvar\"",
		"\"complex\"",
		"\"fornext\"",
		"\"forstep\"",
		"\"if\"",
		"\"ifelse\"",
		"\"dountil\"",
		"\"whilerepeat\"",
		"\"startstep\"",
		"\"startnext\"",
		"\"iferr\"",
		"\"seqob\"",
		"\"unit\"",
		"\"neg\"",
		"\"inv\"",
		"\"tag\"",
		"\"varAt\"",
		"\"deriv\"",
		"\"integ\"",
		"\"sigma\"",
		"\"directorio\"",
		"\"GROB\"",
		"ID",
		"INTNUMBER",
		"DPUNTOS",
		"TRXHEAD",
		"LPAREN",
		"NUMBER",
		"RPAREN",
		"PUNTO",
		"COMMA",
		"PYC",
		"\"DIR\"",
		"OBJ",
		"\"END\"",
		"UNDERSCO",
		"MULT",
		"DIV",
		"CARET",
		"STRING_LITERAL",
		"ASSIGN",
		"PLUS",
		"MINUS",
		"LT",
		"LE",
		"LEOC",
		"EQ",
		"DISTI",
		"GE",
		"GEOC",
		"GT",
		"OPENPROG",
		"OPENPROGOC",
		"CLOSEPROG",
		"CLOSEPROGOC",
		"SCOMI",
		"LLAA",
		"LLAC",
		"CORO",
		"CORC",
		"\"AND\"",
		"\"OR\"",
		"\"XOR\"",
		"VBAR",
		"\"FOR\"",
		"\"NEXT\"",
		"\"STEP\"",
		"\"START\"",
		"FLECHA",
		"\"IF\"",
		"\"THEN\"",
		"\"ELSE\"",
		"\"IFERR\"",
		"\"DO\"",
		"\"UNTIL\"",
		"\"WHILE\"",
		"\"REPEAT\"",
		"TOUNIT",
		"VTO",
		"TOV2",
		"TOV3",
		"DTOR",
		"RTOD",
		"RTOB",
		"BTOR",
		"OBJTO",
		"EQTO",
		"TOARRY",
		"TOLIST",
		"TOSTR",
		"TOTAG",
		"RTOC",
		"CTOR",
		"FSINT",
		"FCINT",
		"FSINTC",
		"FCINTC",
		"SL_COMMENT",
		"WS",
		"ESC",
		"DIGIT",
		"HEXCHAR",
		"OCTCHAR",
		"LETRAS",
		"GRIEGA",
		"ALMOA",
		"DIRECTOR",
		"AMPERSAND",
		"ANGLE",
		"XMEDIA",
		"TRIDOWN",
		"SQRT",
		"STACKMARK",
		"PI",
		"ALPHA",
		"FLECHAOC",
		"FLECHALF",
		"FLECHADO",
		"FLECHAUP",
		"IPSILMAY",
		"DELTAMIN",
		"IPSILMIN",
		"NUMAY",
		"DELTAMAY",
		"LAMBDAMIN",
		"RHOMAY",
		"SIGMAMIN",
		"TAUMAY",
		"OMEGAMIN",
		"DELTAMAY2",
		"PIMAY",
		"OMEGAMAY",
		"INFINITO"
	};
	
	protected void buildTokenTypeASTClassMap() {
		tokenTypeToASTClassMap=null;
	};
	
	private static final long[] mk_tokenSet_0() {
		long[] data = { -262929307926528L, 35184350302297L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	private static final long[] mk_tokenSet_1() {
		long[] data = { 2L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());
	private static final long[] mk_tokenSet_2() {
		long[] data = { -192423124795390L, 35184372068479L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_2 = new BitSet(mk_tokenSet_2());
	private static final long[] mk_tokenSet_3() {
		long[] data = { -262791868973054L, 35184350302297L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_3 = new BitSet(mk_tokenSet_3());
	private static final long[] mk_tokenSet_4() {
		long[] data = { -192423124795390L, 35184372068607L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_4 = new BitSet(mk_tokenSet_4());
	private static final long[] mk_tokenSet_5() {
		long[] data = { -186925566656510L, 35184372072319L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_5 = new BitSet(mk_tokenSet_5());
	private static final long[] mk_tokenSet_6() {
		long[] data = { -51819317297150L, 35184350302335L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_6 = new BitSet(mk_tokenSet_6());
	private static final long[] mk_tokenSet_7() {
		long[] data = { 18015244081168384L, 8L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_7 = new BitSet(mk_tokenSet_7());
	private static final long[] mk_tokenSet_8() {
		long[] data = { 6194561395531448322L, 3848L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_8 = new BitSet(mk_tokenSet_8());
	private static final long[] mk_tokenSet_9() {
		long[] data = { -46184320204798L, 35184372088703L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_9 = new BitSet(mk_tokenSet_9());
	private static final long[] mk_tokenSet_10() {
		long[] data = { -10999948115966L, 35184372088831L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_10 = new BitSet(mk_tokenSet_10());
	private static final long[] mk_tokenSet_11() {
		long[] data = { -51819317297150L, 35184372068479L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_11 = new BitSet(mk_tokenSet_11());
	private static final long[] mk_tokenSet_12() {
		long[] data = { -12099459743742L, 35184372088831L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_12 = new BitSet(mk_tokenSet_12());
	private static final long[] mk_tokenSet_13() {
		long[] data = { 70918499991554L, 21766150L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_13 = new BitSet(mk_tokenSet_13());
	private static final long[] mk_tokenSet_14() {
		long[] data = { 5497558138882L, 2056L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_14 = new BitSet(mk_tokenSet_14());
	private static final long[] mk_tokenSet_15() {
		long[] data = { 2L, 8L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_15 = new BitSet(mk_tokenSet_15());
	private static final long[] mk_tokenSet_16() {
		long[] data = { 6160924290242838528L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_16 = new BitSet(mk_tokenSet_16());
	private static final long[] mk_tokenSet_17() {
		long[] data = { 4509097185509378L, 2056L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_17 = new BitSet(mk_tokenSet_17());
	private static final long[] mk_tokenSet_18() {
		long[] data = { 6165433387428347906L, 2056L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_18 = new BitSet(mk_tokenSet_18());
	private static final long[] mk_tokenSet_19() {
		long[] data = { 6165433387428347906L, 3848L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_19 = new BitSet(mk_tokenSet_19());
	private static final long[] mk_tokenSet_20() {
		long[] data = { 6192454985192570882L, 3848L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_20 = new BitSet(mk_tokenSet_20());
	private static final long[] mk_tokenSet_21() {
		long[] data = { 6193299410122702850L, 3848L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_21 = new BitSet(mk_tokenSet_21());
	private static final long[] mk_tokenSet_22() {
		long[] data = { 6194425310029545474L, 3848L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_22 = new BitSet(mk_tokenSet_22());
	private static final long[] mk_tokenSet_23() {
		long[] data = { 18015244081168384L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_23 = new BitSet(mk_tokenSet_23());
	private static final long[] mk_tokenSet_24() {
		long[] data = { 6194562495043076096L, 1792L, 0L, 0L};
		return data;
	}
	public static final BitSet _tokenSet_24 = new BitSet(mk_tokenSet_24());
	
	}
