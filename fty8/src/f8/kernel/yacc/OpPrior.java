/*
 * Created on 28-ago-2003
 *
 
 
 */
package f8.kernel.yacc;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class OpPrior implements ObjectParserTokenTypes {
	public static byte[] val = new byte[128];

	static {
		val[LITERAL] = 120;
		val[NUMBER] = 110;
		val[ID] = 110;
		val[UNIT] = 4;
		val[CARET] = 100;
		val[DIV] = 64;
		val[MULT] = 64;
		val[MINUS] = 32;
		val[PLUS] = 32;
		val[LE] = 16;
		val[GE] = 16;
		val[LT] = 16;
		val[GT] = 16;
		val[EQ] = 16;
	}
}
