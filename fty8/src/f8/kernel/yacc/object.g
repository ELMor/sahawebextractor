header {
package f8.kernel.yacc;
import java.util.Vector;
import java.io.*;
import f8.kernel.*;
import f8.objects.*;
}

class ObjectParser extends Parser;

options {
	codeGenMakeSwitchThreshold = 3;
	codeGenBitsetTestThreshold = 4;
	buildAST=true;
	ASTLabelType = "antlr.CommonAST"; // change default of "AST"
	k=5;
}

tokens {
	PROGRAM="program"; LITERAL="literal"; INFIX="infix";
	VECTOR="vector"; MATRIZ="matriz";
	LIST="list"; OPERADOR="OPERADOR";
	STRING="string"; LOCALVAR="localvar"; COMPLEX="complex";
	FORNEXT="fornext"; FORSTEP="forstep";
	IF="if"; IFELSE="ifelse"; DOUNTIL="dountil";
	WHILEREPEAT="whilerepeat";
	STARTSTEP="startstep"; STARTNEXT="startnext";
	IFERR="iferr"; SEQOB="seqob"; UNIT="unit";
	NEG="neg"; INV="inv"; TAG="tag"; VARAT="varAt";
	DERIV="deriv"; INTEG="integ"; SIGMA="sigma";
	DIRECTORIO="directorio"; GROB="GROB";
}

hp48Object![Vector itemVector] : 
	(  n1:object 		{itemVector.addElement(#n1);}
	  |n2:trxobject     {itemVector.addElement(#n2);}
	)+ 
	EOF!
	;

object! : 
    ( p:program	
    	{#object=#p;
    	}
	| i:infixExpression		
		{#object=#i;
		}
	| v:vector				
		{#object=#v;
		}
	| m:matriz				
		{#object=#m;
		}
	| id:ID			
		{#object=#id;
		}
	| li:list				
		{#object=#li;
		}
	| n1:INTNUMBER				
		{#object=#n1;
		}
	| n2:complex			
		{#object=#n2;
		}
	| n3:nunit			
		{#object=#n3;
		}
	| o:operador			
		{#object=#o;
		}
	| s:string				
		{#object=#s;
		}
	| po:pob
		{#object=#po;
		}
	| cmd: specialCommand
		{#object=#(#[ID]);
		 #object.setText(#cmd.getText());
		}
	| DPUNTOS tag:ID DPUNTOS tagged:object
		{#object=#(#[TAG],tag,tagged);
		}
	| d:directorio
	    { #object=#d; 
	    }
	| gr:grob
		{ #object=#gr;
		}
	)
	;

trxobject! :
    TRXHEAD 
    		ID LPAREN t:NUMBER RPAREN 
            ID LPAREN a:ID RPAREN 
            ID LPAREN (PUNTO|COMMA) RPAREN 
    PYC
	o:object			
		{ #trxobject=#o; 
		}
	;

directorio :
	"DIR" 			{#directorio=#[DIRECTORIO];}
	(varName:ID 
	obj:OBJ         {#directorio.addChild(#varName); #directorio.addChild(#obj);}
	)* 
	"END"
;

grob :
	GROB 
	;
	
nunit! :
	n2:NUMBER
		{#nunit=#n2;}
	
 	(UNDERSCO n3:uExpr 			
 		{#nunit=#(#[UNIT],#nunit,#n3);}	
 	)?
	;

uExpr
	: (uEExpr|NUMBER) ((MULT^|DIV^) uExpr )?	
	;

uEExpr
	: ID ( CARET^ NUMBER )?
	;
	
seqOb!
	: 	    				{ #seqOb=#[SEQOB]; }
		(n1:object			{ #seqOb.addChild(#n1); }
		)+		
	;

string! :
	n1:STRING_LITERAL
		{#string=#n1;}	
	;

operador! :
	( n1:CARET
		{#operador=#(#[OPERADOR],#n1);}
	| i1:ASSIGN
		{#operador=#(#[OPERADOR],#i1);}
	| n2:MULT
		{#operador=#(#[OPERADOR],#n2);}
	| n3:DIV
		{#operador=#(#[OPERADOR],#n3);}
	| n4:PLUS
		{#operador=#(#[OPERADOR],#n4);}
	| n5:MINUS
		{#operador=#(#[OPERADOR],#n5);}
	| c1:LT
		{#operador=#(#[OPERADOR],#c1);}
	| ( c21:LE   {#operador=#(#[OPERADOR],#c21);}
	  | c22:LEOC {#operador=#(#[OPERADOR],#c22);} 
	  )
	| c3:EQ
		{#operador=#(#[OPERADOR],#c3);}
	| c6:DISTI
		{#operador=#(#[OPERADOR],#c6);}
	| ( c41:GE   {#operador=#(#[OPERADOR],#c41);}
	  | c42:GEOC {#operador=#(#[OPERADOR],#c42);}
	  ) 
	| c5:GT
		{#operador=#(#[OPERADOR],#c5);}
	);

program! : 
	(OPENPROG! | OPENPROGOC!)
	n1:seqOb					
	(CLOSEPROG! | CLOSEPROGOC! | EOF!)			
								{#program=#(#[PROGRAM],#n1);}
	;

literalIdentifier!:
	SCOMI!
	n1:ID 						{#literalIdentifier=#(#[LITERAL],#n1);}
	(SCOMI!|EOF!)
	;
	
list!:
	LLAA! 						{#list=#(#[LIST]);}
	(n1:object					{#list.addChild(#n1);}
	)*
	(LLAC!|EOF!)				
	;
	
infixExpression! :
	SCOMI!  					{#infixExpression=(#[INFIX]);}
	  n1:assignExpr 			{#infixExpression.addChild(#n1);}
	  ( n2:varAt                {#infixExpression.addChild(#n2);}
	  )?
	  
	(SCOMI!|EOF!)
	;
	
vector! :
	CORO! 						{#vector=(#[VECTOR]);}
	(n1:NUMBER					{#vector.addChild(#n1);}
	)+ 
	(CORC!|EOF!)
	;
	
complex! :
	LPAREN! n1:NUMBER PYC n2:NUMBER RPAREN!
		{#complex=#(#[COMPLEX],#n1,#n2);}
	;

matriz! :
	CORO! 						{#matriz=(#[MATRIZ]);}
	(n1:vector					{#matriz.addChild(#n1);}
	)+ 
	CORC!
	;
	
assignExpr
	:	testExpr
		(
			ASSIGN^
			assignExpr 
		)?
	;

testExpr
	:
		boolExpr
		(
			(LT^|LE^|EQ^|GE^|GT^|LEOC^|GEOC^|DISTI^)
			boolExpr
		)?
	;

boolExpr
	:
		addExpr
		(
			("AND"^|"OR"^|"XOR")
			addExpr
		)?
	;

addExpr
	:	multExpr 
		(
			(PLUS^|MINUS^)
			multExpr
		)*
	;

multExpr
	:	exponentExpr
		(
			(MULT^|DIV^)
			exponentExpr
		)*
	;

exponentExpr
	:  postfixExpr
	   (
	   		CARET^ postfixExpr
	   )*
	;

postfixExpr!
	: (ID LPAREN)=> n1:ID n2:parenArgs 
		{#postfixExpr=#n1; #n1.addChild(#n2);}
	| n3:ID
		{#postfixExpr=#n3;}
    | (ID UNDERSCO)=> u1:ID UNDERSCO u2:uExpr 
        { AST uno=#(#[NUMBER]);
          uno.setText("1");
          AST theUnit=#(#[UNIT],uno,#u2);
          #postfixExpr=#(#[MULT],#u1,theUnit);
          #postfixExpr.setText("*");
        }
	| n4:nunit
		{#postfixExpr=#n4;}
	| (LPAREN NUMBER COMMA NUMBER RPAREN)=> n5:complex
		{#postfixExpr=#n5;}
	| LPAREN! n6:assignExpr RPAREN!
		{#postfixExpr=#n6;}
	| MINUS n7:postfixExpr
		{#postfixExpr=#(#[NEG],#n7);}
	| DERIV derid:ID LPAREN derfun:assignExpr RPAREN
		{#postfixExpr=#(#[DERIV],#derfun,#derid);}
	| INTEG LPAREN intdown:assignExpr COMMA 
	               intup:assignExpr COMMA 
	               intfun:assignExpr COMMA 
	               intid:ID RPAREN
		{#postfixExpr=#(#[INTEG],#intdown,#intup,#intfun,#intid);}
	| SIGMA LPAREN sigid:ID ASSIGN sigdown:assignExpr COMMA 
	               sigup:assignExpr COMMA 
	               sigfun:assignExpr RPAREN
		{#postfixExpr=#(#[SIGMA],#sigid,#sigdown,#sigup,#sigfun);}	
	| SIGMA LPAREN  sigdown2:assignExpr COMMA 
	                sigup2:assignExpr COMMA 
	                sigfun2:assignExpr COMMA 
	                sigid2:ID RPAREN
		{#postfixExpr=#(#[SIGMA],#sigid2,#sigdown2,#sigup2,#sigfun2);}	
	;
	
parenArgs
	:	
      LPAREN!
      (
         assignExpr
         (
            COMMA!
	        assignExpr
         )*
      )?
      RPAREN!
	;
	
varAt!
	: VBAR LPAREN n1:ID ASSIGN n2:addExpr RPAREN	
							{#varAt=#(#[VARAT],#n1,#n2);}
	;

pob! 
	: "FOR" n1:ID n2:seqOb				
	  ("NEXT"				{#pob=#(#[FORNEXT],#n1,#n2);}
	  |n3:NUMBER "STEP"		{#pob=#(#[FORSTEP],#n1,#n2,#n3);}
	  )
	| "START" s1:seqOb				
	  ("NEXT"				{#pob=#(#[STARTNEXT],#s1);}
	  |s2:NUMBER "STEP"		{#pob=#(#[STARTSTEP],#s1,#s2);}
	  )
	| FLECHA             	{#pob=#[LOCALVAR]; }
	  (f1:ID 				{#pob.addChild(#f1);}
	  )+ 
	  f2:program  			{#pob.addChild(#f2);}
	| FLECHA 			 	{#pob=#[LOCALVAR]; }
	  (r1:ID 				{#pob.addChild(#r1);}
	  )+ 
	  r2:infixExpression	{#pob.addChild(#r2);}
	| "IF" i1:seqOb
	  "THEN" i2:seqOb
	  ( "END"				{#pob=#(#[IF],#i1,#i2);}
	   |"ELSE"
	    i3:seqOb
	    "END"				{#pob=#(#[IFELSE],#i1,#i2,#i3);}
	  )
	| "IFERR" e1:seqOb
	  "THEN" e2:seqOb
	  "END"					{#pob=#(#[IFERR],#e1,#e2);}
	| "DO" d1:seqOb
	  "UNTIL" d2:seqOb
	  "END"					{#pob=#(#[DOUNTIL],#d1,#d2);}
	| "WHILE" w1:seqOb
	  "REPEAT" w2:seqOb
	  "END"					{#pob=#(#[WHILEREPEAT],#w1,#w2);}
;

specialCommand :
	 TOUNIT
	|VTO
	|TOV2
	|TOV3
	|DTOR
	|RTOD
	|RTOB
	|BTOR
	|OBJTO
	|EQTO
	|TOARRY
	|TOLIST
	|TOSTR
	|TOTAG
	|RTOC
	|CTOR
	|FSINT
	|FCINT
	|FSINTC
	|FCINTC
;

class ObjectLexer extends Lexer;
options {
	k=5;
	charVocabulary='\u0003'..'\u01ff';
}

SL_COMMENT
	:	"@"
		(~('\n'|'\r'))* ('\n'|'\r'('\n')?)
		{$setType(Token.SKIP); newline();}
	;
	
WS	:	(' '
	|	'\t'
	|	'\n'
	|	'\r')
		{ _ttype = Token.SKIP; }
	;


protected
ESC	:	'\\'
		(	'n'
		|	'r'
		|	't'
		|	'b'
		|	'f'
		|	'"'
		|	'\''
		|	'\\'
		|	('0'..'3')
			(
				options {
					warnWhenFollowAmbig = false;
				}
			:	('0'..'9')
				(	
					options {
						warnWhenFollowAmbig = false;
					}
				:	'0'..'9'
				)?
			)?
		|	('4'..'7')
			(
				options {
					warnWhenFollowAmbig = false;
				}
			:	('0'..'9')
			)?
		)
	;

// string literals
STRING_LITERAL
	:	'"' (ESC|~('"'|'\\'))* '"'
	;

protected BINCHAR: '0'|'1';
protected DIGIT: '0'..'9';
protected HEXCHAR: (DIGIT|'A'..'F'|'a'..'f');
protected OCTCHAR: ('0'..'7');
protected LETRAS:  ('a'..'z'|'A'..'Z');
protected GRIEGA: 
		  XMEDIA 
		| TRIDOWN 
		| ALPHA 
		| IPSILMAY 
		| DELTAMIN 
		| IPSILMIN	
		| NUMAY	
		| DELTAMAY
		| LAMBDAMIN 
		| RHOMAY 
		| SIGMAMIN
		| TAUMAY 
		| OMEGAMIN	
		| DELTAMAY2	
		| PIMAY	
		| OMEGAMAY;
NUMBER : 
	('-')? (DIGIT)+ ((PUNTO) (DIGIT)*)?  ('E' ('-')? DIGIT (DIGIT (DIGIT)?)?)?
;

INTNUMBER : ALMOA (HEXCHAR)+ ('h'|'H'|'d'|'D'|'o'|'O'|'b'|'B') ;

ID options { testLiterals = true; }
	:	(LETRAS|GRIEGA)(LETRAS|GRIEGA|DIGIT)*
		| "�F"
		| "�C"
		| "�R"
		| "�"
		| PI
	;

GROB : "GROB" (WS)+ NUMBER (WS)+ NUMBER (WS)+ (HEXCHAR)+ ;
	
LPAREN:	  '(' ;
RPAREN:	  ')' ;
CARET:    '^' ;
MULT:     '*' ; 
DIV:      '/' ;
PLUS:     '+' ;
MINUS:    '-' ;
ASSIGN:   '=' ;
COMMA :   ',' ;
PUNTO :   '.' ;
ALMOA:    '#' ;
LLAA:     '{' ;
LLAC:     '}' ;
CORO:     '[' ;   
CORC:     ']' ;
SCOMI:	  '\'';
UNDERSCO: '_' ;
DPUNTOS : ':' ;
LT      : '<' ;
VBAR    : '|' ;
LE      : "<=";
EQ      : "==";
GE      : ">=";
GT      : '>';
OPENPROG: "<<";
CLOSEPROG:">>";
PYC     : ';';

FLECHA:   (FLECHAOC | "->");
TOUNIT:	  FLECHA "UNIT";
VTO:	  "V" FLECHA;
TOV2:	  FLECHA "V2";
TOV3:	  FLECHA "V3";
DTOR:	  "D" FLECHA "R";
RTOD:	  "R" FLECHA "D";
RTOB:	  "R" FLECHA "B";
BTOR:	  "B" FLECHA "R";
OBJTO:	  "OBJ" FLECHA;
EQTO:	  "EQ" FLECHA;
TOARRY:	  FLECHA "ARRY";
TOLIST:	  FLECHA "LIST";
TOSTR:	  FLECHA "STR";
TOTAG:	  FLECHA "TAG";
RTOC:	  "R" FLECHA "C";
CTOR:	  "C" FLECHA "R";
FSINT:	  "FS?";
FCINT:	  "FC?";
FSINTC:	  "FS?C";
FCINTC:	  "FC?C";
TRXHEAD:  "%%HP:";
DIRECTOR: "DIR";

AMPERSAND: '&';

ANGLE:	  '\u0080';
protected XMEDIA:	  '\u0081';
protected TRIDOWN:  '\u0082';
SQRT:     '\u0083';
INTEG:    '\u0084';
SIGMA:	  '\u0085';
STACKMARK:'\u0086';
PI:       '\u0087';
DERIV:	  '\u0088';
LEOC:     '\u0089';
GEOC:     '\u008A';
DISTI:	  '\u008B';
protected ALPHA:    '\u008C';
FLECHAOC: '\u008D';
FLECHALF: '\u008E';
FLECHADO: '\u008F';
FLECHAUP: '\u0090';
protected IPSILMAY: '\u0091';
protected DELTAMIN: '\u0092';
protected IPSILMIN: '\u0093';
protected NUMAY:    '\u0094';
protected DELTAMAY: '\u0095';
protected LAMBDAMIN:'\u0096';
protected RHOMAY:   '\u0097';
protected SIGMAMIN: '\u0098';
protected TAUMAY:   '\u0099';
protected OMEGAMIN: '\u009A';
protected DELTAMAY2:'\u009B';
protected PIMAY:    '\u009C';
protected OMEGAMAY: '\u009D';
protected INFINITO: '\u009F';

CLOSEPROGOC: '\u00BB';
OPENPROGOC:  '\u00AB';


	