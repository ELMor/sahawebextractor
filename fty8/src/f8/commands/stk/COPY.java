package f8.commands.stk;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class COPY extends NonAlgebraic {
	public COPY() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 4;
	}

	public Storable getInstance() {
		return new COPY();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		if (Core.check(1)) {
			Core.push(Core.pop());
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("COPY");
	}
}
