package f8.commands.stk;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.objects.types.Double;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class DEPTH extends NonAlgebraic {
	public DEPTH() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 17;
	}

	public Storable getInstance() {
		return new DEPTH();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) {
		Core.push(new Double(Core.size()));
	}

	public String toString() {
		return ("DEPTH");
	}
}
