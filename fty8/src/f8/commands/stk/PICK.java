package f8.commands.stk;

import java.io.IOException;

import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.FunctionDomainException;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.types.Double;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class PICK extends NonAlgebraic {
	public PICK() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 16;
	}

	public Storable getInstance() {
		return new PICK();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		if (Core.check(1)) {
			Command pick = Core.pop();
			if (pick instanceof Double) {
				int pck = ((Double) pick).doubleValue().toInt();
				if (pck < 1) {
					throw new FunctionDomainException(this);
				}
				if (Core.check(pck)) {
					Core.push(Core.peek(pck - 1));
				} else {
					throw new TooFewArgumentsException(this);
				}
			} else {
				throw new TooFewArgumentsException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("PICK");
	}
}
