package f8.commands.stk;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class SWAP extends NonAlgebraic {
	public SWAP() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 49;
	}

	public Storable getInstance() {
		return new SWAP();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		if (Core.check(2)) {
			Stackable a = Core.peek(0);
			Stackable b = Core.peek(1);
			Core.poke(1, a);
			Core.poke(0, b);
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("SWAP");
	}
}
