/*
 * Created on 01-oct-2003
 *
 
 
 */
package f8.commands.compare;

import java.io.IOException;

import antlr.CommonAST;
import antlr.Token;
import antlr.collections.AST;
import f8.commands.Dispatch2;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.InfixExp;
import f8.objects.types.Int;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public abstract class Comparison extends Dispatch2 {
	public abstract boolean compare(Ereal v1, Ereal v2);

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfIntInt(f8.kernel.types.Int,
	 *      f8.kernel.types.Int)
	 */
	public Stackable prfIntInt(Int a, Int b) throws F8Exception {
		boolean v = compare(new Ereal(a.n), new Ereal(b.n));
		return (new Double(v ? 1 : 0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfIntDouble(f8.kernel.types.Int,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfIntDouble(Int a, Double b) throws F8Exception {
		boolean v = compare(new Ereal(a.n), b.doubleValue());
		return (new Double(v ? 1 : 0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleInt(f8.kernel.types.Double,
	 *      f8.kernel.types.Int)
	 */
	public Stackable prfDoubleInt(Double a, Int b) throws F8Exception {
		boolean v = compare(a.doubleValue(), new Ereal(b.n));
		return (new Double(v ? 1 : 0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleDouble(f8.kernel.types.Double,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		boolean v = compare(a.doubleValue(), b.doubleValue());
		return (new Double(v ? 1 : 0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfUnitUnit(f8.kernel.types.Unit,
	 *      f8.kernel.types.Unit)
	 */
	public Stackable prfUnitUnit(Unit a, Unit b) throws F8Exception {
		Unit converted = b.convert(a);
		return new Double(compare(a.getValor(), converted.getValor()) ? 1 : 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b) throws F8Exception {
		return composeExpression(a.getAST(), b.getAST());
	}

	private Stackable composeExpression(AST aa, AST ba) {
		AST padre=new CommonAST(getToken());
		padre.setText(toString());
		padre.addChild(aa);
		padre.addChild(ba);
		return new InfixExp(padre);
	}

	public Stackable prfInfixExpInt(InfixExp a, Int b) throws F8Exception {
		return composeExpression(a.getAST(), b.getAST());
	}

	public Stackable prfInfixExpUnit(InfixExp a, Unit b) throws F8Exception {
		return composeExpression(a.getAST(), b.getAST());
	}

	public int getID() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void loadState(DIS ds) throws IOException {
		// TODO Auto-generated method stub
		
	}

	public void saveState(DOS ds) throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	public abstract Token getToken();

}
