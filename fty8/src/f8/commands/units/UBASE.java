package f8.commands.units;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;
import f8.objects.types.TempUnit;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class UBASE extends NonAlgebraic implements ObjectParserTokenTypes {
	public UBASE() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 106;
	}

	public Storable getInstance() {
		return new UBASE();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("UBASE");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		
		if (Core.check(1)) {
			Stackable u = Core.peek();
			if (u instanceof Unit || u instanceof TempUnit) {
				Core.pop();
				Unit uu = (Unit) u;
				Core.push(uu.ubase());
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

}
