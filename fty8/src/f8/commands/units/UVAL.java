package f8.commands.units;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class UVAL extends NonAlgebraic {
	public UVAL() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 2484;
	}

	public Storable getInstance() {
		return new UVAL();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("UVAL");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec(Object extern) throws TooFewArgumentsException {
		
		Stackable[] arg = new Stackable[1];
		arg[0] = new Unit(null);
		String dsp = Core.dispatch(arg);
		if (dsp != null) {
			Unit ar = (Unit) arg[0];
			Core.push(new Double(ar.getValor()));
		} else {
			throw new TooFewArgumentsException(this);
		}
	}
}
