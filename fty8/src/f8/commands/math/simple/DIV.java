package f8.commands.math.simple;

import java.io.IOException;

import antlr.collections.AST;
import f8.Core;
import f8.ICalc;
import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.DivideByZeroException;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.InfixExp;
import f8.objects.types.Int;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class DIV extends Dispatch2 implements Derivable {
	public DIV() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 81;
	}

	public Storable getInstance() {
		return new DIV();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("/");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST args, String var) throws F8Exception {
		AST num;
		AST den;
		num = args;
		den = num.getNextSibling();
		return mkl.res(mkl.div(DER.deriveFunction(num, var), den), 
				       mkl.mul(num,mkl.div(DER.deriveFunction(den, var), mkl.ele(den, mkl.num(2)))));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfIntInt(f8.kernel.types.Int,
	 *      f8.kernel.types.Int)
	 */
	public Stackable prfIntInt(Int a, Int b) throws F8Exception {
		Int A = (Int) a;
		Int B = (Int) b;
		int na = A.n;
		int nb = B.n;
		Stackable c;

		if (nb != 0) {
			if ((na % nb) == 0) {
				c = new Int(na / nb);
			} else {
				Ereal x = new Ereal(na);
				Ereal y = new Ereal(nb);
				c = new Double(x.div(y));
			}
			return c;
		} else {
			throw new DivideByZeroException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleDouble(f8.kernel.types.Double,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		if (!b.doubleValue().isZero()) {
			if(Core.isSetting(ICalc.EXACT_MODE)
			   && a.doubleValue().isIntegral()
			   && b.doubleValue().isIntegral()){
				InfixExp ret= new InfixExp(mkl.div(mkl.num(a.doubleValue().toInt()),mkl.num(b.doubleValue().toInt())));
				ret.aprox=new Double(a.doubleValue().div(b.doubleValue()));
				return ret;
			}
			return new Double(a.doubleValue().div(b.doubleValue()));
		} else {
			throw new DivideByZeroException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfComplexComplex(f8.kernel.types.Complex,
	 *      f8.kernel.types.Complex)
	 */
	public Stackable prfComplexComplex(Complex a, Complex b) throws F8Exception {
		return a.div(b);
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleUnit(f8.kernel.types.Double,
	 *      f8.kernel.types.Unit)
	 */
	public Stackable prfDoubleUnit(Double a, Unit b) throws F8Exception {
		Ereal val = a.doubleValue().div(b.getValor());
		return (new Unit(val, mkl.inv(b.getUnidad())));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfUnitDouble(f8.kernel.types.Unit,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfUnitDouble(Unit a, Double b) throws F8Exception {
		return new Unit(a.getValor().div(b.doubleValue()), a.getUnidad());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfUnitUnit(f8.kernel.types.Unit,
	 *      f8.kernel.types.Unit)
	 */
	public Stackable prfUnitUnit(Unit a, Unit b) throws F8Exception {
		Ereal v = a.getValor().div(b.getValor());
		AST u = mkl.div(a.getUnidad(), b.getUnidad());
		return new Unit(v, u);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpInfixExp(f8.types.InfixExp,
	 *      f8.types.InfixExp)
	 */
	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b)
			throws F8Exception {
		return new InfixExp(mkl.div(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfComplexInfixExp(f8.types.Complex, f8.types.InfixExp)
	 */
	public Stackable prfComplexInfixExp(Complex a, InfixExp b)
			throws F8Exception {
		return new InfixExp(mkl.div(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleInfixExp(f8.types.Double, f8.types.InfixExp)
	 */
	public Stackable prfDoubleInfixExp(Double a, InfixExp b) throws F8Exception {
		return new InfixExp(mkl.div(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpComplex(f8.types.InfixExp, f8.types.Complex)
	 */
	public Stackable prfInfixExpComplex(InfixExp a, Complex b)
			throws F8Exception {
		return new InfixExp(mkl.div(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpDouble(f8.types.InfixExp, f8.types.Double)
	 */
	public Stackable prfInfixExpDouble(InfixExp a, Double b) throws F8Exception {
		return new InfixExp(mkl.div(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfComplexDouble(f8.types.Complex, f8.types.Double)
	 */
	public Stackable prfComplexDouble(Complex a, Double b) throws F8Exception {
		return a.div(new Complex(b.doubleValue(), Ereal.cero)).mutate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleComplex(f8.types.Double, f8.types.Complex)
	 */
	public Stackable prfDoubleComplex(Double a, Complex b) throws F8Exception {
		return new Complex(a.doubleValue(), Ereal.cero).div(b).mutate();
	}

}
