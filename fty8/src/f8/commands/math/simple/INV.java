package f8.commands.math.simple;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.InfixExp;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class INV extends Dispatch1 implements Derivable {
	public String getLaTeX(AST child) {
		return "\\frac{1}"+InfixExp.getLaTex(child);
	}

	public INV() {
		// Aqui estaba systemInstall
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public int getID() {
		return 105;
	}

	public Storable getInstance() {
		return new INV();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("INV");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST args, String var) throws F8Exception {
		return DER.deriveFunction(mkl.div(mkl.num(1), args), var);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#f(double)
	 */
	public Stackable prfDouble(Double x) throws F8Exception {
		return new Double(x.doubleValue().inv());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#f(f8.kernel.types.Unit)
	 */
	public Stackable prfUnit(Unit a) throws F8Exception {
		return new Unit(a.getValor().inv(), mkl.div(mkl.num(1), a.getUnidad()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#undo()
	 */
	public String undo() {
		return "INV";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch1#prfComplex(f8.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return new Complex(Ereal.uno, Ereal.cero).div(x);
	}

}
