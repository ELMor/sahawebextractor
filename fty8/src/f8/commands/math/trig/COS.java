package f8.commands.math.trig;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Storable;
import f8.commands.math.Trigonometric;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class COS extends Trigonometric {
	
	public COS() {
		// Aqui estaba systemInstall
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public int getID() {
		return 57;
	}

	public Storable getInstance() {
		return new COS();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public Stackable prfDouble(Double x) {
		return new Double(Settings2RAD(x.doubleValue()).cos());
	}

	public String toString() {
		return ("COS");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST args, String var) throws F8Exception {
		return mkl.res(mkl.num(0), mkl.mul(mkl.fca("SIN", args), DER
				.deriveFunction(args, var)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#undo()
	 */
	public String undo() {
		return "ACOS";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.cos();
	}

}
