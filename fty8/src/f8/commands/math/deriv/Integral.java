/*
 * Created on 17-ago-2003
 *
 
 
 */
package f8.commands.math.deriv;

import java.io.IOException;

import antlr.CommonAST;
import antlr.collections.AST;
import f8.Core;
import f8.commands.Command;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class Integral extends Command {
	AST full;

	public Integral(AST tree) {
		full = tree;
	}
	
	public Integral(){
		full=null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public void exec(Object extern) throws F8Exception {
		if (Core.check(4)) {
			Stackable from = Core.peek(3);
			Stackable to = Core.peek(2);
			Stackable exp = Core.peek(1);
			Stackable var = Core.peek(0);
			if ((var instanceof Literal || (InfixExp.isLiteral(var) != null))
					&& (from instanceof Double || from instanceof InfixExp)
					&& (to instanceof Double || to instanceof InfixExp)
					&& (exp instanceof Double || exp instanceof InfixExp)) {
				Core.pop(4);

				AST integral = new CommonAST();
				integral.setText("\u0084");
				integral.setType(INTEG);
				integral.addChild(from.getAST());
				integral.addChild(to.getAST());
				integral.addChild(exp.getAST());
				integral.addChild(var.getAST());
				Core.push(new InfixExp(integral));
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getID()
	 */
	public int getID() {
		return 1118;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new Integral(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#saveState(f8.pfr.io.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
		Command.saveState(ds, full);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#loadState(f8.pfr.io.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {
		AST[] nodes = new AST[1];
		Command.loadState(ds, nodes);
		full = nodes[0];
	}

	public String toString() {
		return "\u0084";
	}

	public static String forma(AST sigma) {
		AST child = sigma.getFirstChild();
		String sigId = InfixExp.forma(child);
		child = child.getNextSibling();
		String sigDo = InfixExp.forma(child);
		child = child.getNextSibling();
		String sigUp = InfixExp.forma(child);
		child = child.getNextSibling();
		String sigFu = InfixExp.forma(child);
		return "\u0084(" + sigId + "=" + sigDo + "," + sigUp + "," + sigFu + ")";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		// TODO Auto-generated method stub
		return false;
	}

}
