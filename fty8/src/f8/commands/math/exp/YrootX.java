package f8.commands.math.exp;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Dispatch2;
import f8.commands.math.mkl;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.InfixExp;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public class YrootX extends Dispatch2 implements Derivable {
	
	public YrootX() {
	}

	public int getID() {
		return 7001;
	}

	public boolean isAlgebraic() {
		return true;
	}

	public boolean isInfix() {
		return true;
	}

	public boolean isOperator() {
		return true;
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds) throws IOException {
	}

	public AST deriveWithArgs(AST expr, String var) throws F8Exception {
		AST a1=expr;
		AST a2=a1.getNextSibling();
		mkl.ele(a1, mkl.div(mkl.uno, a2));
		return new CARET().deriveWithArgs(a1, var);
	}

	public Stackable prfDoubleDouble(Double x, Double y) throws F8Exception {
		if (x.doubleValue().gt(Ereal.cero)) {
			return new Double(x.doubleValue().nroot(y.doubleValue()));
		} else if (x.doubleValue().isNegative()) {
			throw new IllegalArgumentException("Base negative!");
		} else {
			return new Double(0);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.FunArgArg#prfInfixExpDouble(f8.kernel.types.InfixExp,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfInfixExpDouble(InfixExp a, Double b) {
		return new InfixExp(mkl.ele(a.getAST(), mkl.div(mkl.uno, mkl.num(b.doubleValue()))));
	}

	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b) {
		return new InfixExp(mkl.ele(a.getAST(), mkl.div(mkl.uno, b.getAST())));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn2#f(f8.kernel.types.Unit, double)
	 */
	public Stackable prfUnitDouble(Unit un, Double vexp) {
		return (
				new Unit(
						un.getValor().nroot(vexp.doubleValue()), 
						mkl.ele(un.getUnidad(), mkl.num(vexp.doubleValue().inv()))
				));
	}

	public String toString() {
		return ("YROOTX");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfComplexComplex(f8.kernel.types.Complex,
	 *      f8.kernel.types.Complex)
	 */
	public Stackable prfComplexComplex(Complex a, Complex b) throws F8Exception {
		return a.power(new Complex(Ereal.uno,Ereal.cero).div(b));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfComplexDouble(f8.kernel.types.Complex,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfComplexDouble(Complex a, Double b) throws F8Exception {
		return a.power(b.doubleValue().inv());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleComplex(f8.kernel.types.Double,
	 *      f8.kernel.types.Complex)
	 */
	public Stackable prfDoubleComplex(Double a, Complex b) throws F8Exception {
		return new Complex(a.doubleValue(), Ereal.cero).power(new Complex(Ereal.uno,Ereal.cero).div(b));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfComplexInfixExp(f8.types.Complex, f8.types.InfixExp)
	 */
	public Stackable prfComplexInfixExp(Complex a, InfixExp b)
			throws F8Exception {
		return new InfixExp(
				mkl.ele(
						a.getAST(), 
						mkl.div(mkl.uno, b.getAST())
				));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleInfixExp(f8.types.Double, f8.types.InfixExp)
	 */
	public Stackable prfDoubleInfixExp(Double a, InfixExp b) throws F8Exception {
		return new InfixExp(mkl.ele(a.getAST(), mkl.div(mkl.uno, b.getAST())));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpComplex(f8.types.InfixExp, f8.types.Complex)
	 */
	public Stackable prfInfixExpComplex(InfixExp a, Complex b)
			throws F8Exception {
		return new InfixExp(mkl.ele(a.getAST(),mkl.div(mkl.uno, b.getAST())));
	}

	
}
