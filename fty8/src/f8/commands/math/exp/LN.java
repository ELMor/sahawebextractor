package f8.commands.math.exp;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.InfixExp;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class LN extends Dispatch1 implements Derivable {
	public LN() {
		// Aqui estaba systemInstall
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public int getID() {
		return 102;
	}

	public Storable getInstance() {
		return new LN();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public Stackable prfDouble(Double arg) throws F8Exception {
		if (arg.doubleValue().isNegative())
			return new Complex(arg.doubleValue(), Ereal.cero).log();
		return new Double(arg.doubleValue().ln());
	}

	public String toString() {
		return ("LN");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST arg, String var) throws F8Exception {
		return mkl.div(DER.deriveFunction(arg, var), arg);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#undo()
	 */
	public String undo() {
		return "EXP";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.ln();
	}

	public String getLaTeX(AST child) {
		return "\\ln \\left("+InfixExp.getLaTex(child)+"\\right)";
	}

}
