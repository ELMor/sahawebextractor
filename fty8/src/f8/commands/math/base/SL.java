package f8.commands.math.base;


import java.io.IOException;

import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.InvalidDimensionException;
import f8.objects.Stackable;
import f8.objects.types.Ereal;
import f8.objects.types.F8Vector;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class SL extends Dispatch2 {
	public SL() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3053;
	}

	public Storable getInstance() {
		return new SL();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("SL");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8VectorF8Vector(f8.kernel.types.F8Vector,
	 *      f8.kernel.types.F8Vector)
	 */
	public Stackable prfF8VectorF8Vector(F8Vector a, F8Vector it)
			throws F8Exception {
		Ereal[] y = it.x;
		int nx = a.x.length;
		int ny = y.length;
		Ereal[] z = new Ereal[nx];

		// x[0] x[1] x[2]
		// y[0] y[1] y[2]
		if ((nx == ny) && (nx == 3)) {
			z[0] = a.x[1].mul(y[2]).minus(a.x[2].mul(y[1]));
			z[1] = a.x[2].mul(y[0]).minus(a.x[0].mul(y[2]));
			z[2] = a.x[0].mul(y[1]).minus(a.x[1].mul(y[0]));

			return (new F8Vector(z));
		} else {
			throw new InvalidDimensionException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
