/*
 * Created on 17-ago-2003
 *
 
 
 */
package f8.commands.symb;

import java.io.IOException;

import antlr.CommonAST;
import antlr.collections.AST;
import f8.Core;
import f8.commands.Command;
import f8.commands.Storable;
import f8.commands.stk.EVAL;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 */
public final class Sigma extends Command {
	AST full;

	public Sigma(){
		this(null);
	}
	
	public Sigma(AST tree) {
		full = tree;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public void exec(Object extern) throws F8Exception {
		
		if (Core.check(4)) {
			Stackable var = Core.peek(3);
			Stackable from = Core.peek(2);
			Stackable to = Core.peek(1);
			Stackable exp = Core.peek(0);
			String varName = null;
			if ((var instanceof Literal || (varName = InfixExp.isLiteral(var)) != null)
					&& (from instanceof Double || from instanceof InfixExp)
					&& (to instanceof Double || to instanceof InfixExp)
					&& (exp instanceof Double || exp instanceof InfixExp)) {
				Core.pop(4);

				Core.push(from);
				new EVAL().exec(null);
				Stackable nfrom = Core.pop();

				Core.push(to);
				new EVAL().exec(null);
				Stackable nto = Core.pop();

				if (nfrom instanceof Double && nto instanceof Double) {
					String prg = " 0 ";
					prg += nfrom.toString() + " ";
					prg += nto.toString() + " ";
					prg += "FOR " + varName + " " + exp.toString()
							+ " EVAL + NEXT ";
					Core.enter(prg);
				} else {
					AST sigma = new CommonAST();
					sigma.setText("\u0085");
					sigma.setType(SIGMA);
					sigma.addChild(var.getAST());
					sigma.addChild(from.getAST());
					sigma.addChild(to.getAST());
					sigma.addChild(exp.getAST());
					Core.push(new InfixExp(sigma));
				}
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getID()
	 */
	public int getID() {
		return 1117;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new Sigma(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#saveState(f8.pfr.io.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
		Command.saveState(ds, full);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#loadState(f8.pfr.io.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {
		AST[] nodes = new AST[1];
		Command.loadState(ds, nodes);
		full = nodes[0];
	}

	public String toString() {
		return "\u0085";
	}

	public static String forma(AST sigma) {
		AST child = sigma.getFirstChild();
		String sigId = InfixExp.forma(child);
		child = child.getNextSibling();
		String sigDo = InfixExp.forma(child);
		child = child.getNextSibling();
		String sigUp = InfixExp.forma(child);
		child = child.getNextSibling();
		String sigFu = InfixExp.forma(child);
		return "\u0085(" + sigId + "=" + sigDo + "," + sigUp + "," + sigFu
				+ ")";

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
