/*
 * Created on 01-oct-2003
 *
 
 
 */
package f8.commands;

/**
 * @author elinares
 * 
 * 
 * 
 */
public abstract class NonAlgebraic extends Command {

	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
