package f8.commands.prog.brch;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.commands.prog.UtilHelper;
import f8.exceptions.F8Exception;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class WHILEREPEAT extends NonAlgebraic {
	Command whi;

	Command rep;

	public WHILEREPEAT(){
		this(null);
	}
	
	public WHILEREPEAT(AST def) {
		if (def == null) {
			return;
		}

		AST wh = def.getFirstChild();
		AST re = wh.getNextSibling();
		whi = Command.createFromAST(wh);
		rep = Command.createFromAST(re);
	}

	public int getID() {
		return 10;
	}

	public Storable getInstance() {
		return new WHILEREPEAT(null);
	}

	public void loadState(DIS ds) throws IOException {
		whi = Command.loadFromStorage(ds);
		rep = Command.loadFromStorage(ds);
	}

	public void saveState(DOS ds)  throws IOException {
		ds.writeInt(whi.getID());
		whi.saveState(ds);
		ds.writeInt(rep.getID());
		rep.saveState(ds);
	}

	public void exec(Object extern) throws F8Exception {
		int res = 0;

		do {
			whi.exec(null);
			res = UtilHelper.evalCondition();

			if (res == 1) {
				rep.exec(null);
			} else {
				break;
			}
		} while (true);
	}

	public String toString() {
		if( whi==null ){
			return ("WHILE 0 REPEAT  END");
		}
		return ("WHILE " + whi.toString() + " REPEAT " + rep.toString() + " END");
	}
}
