package f8.commands.prog.brch;

import java.io.IOException;

import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class STOP extends NonAlgebraic {
	public STOP() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 47;
	}

	public Storable getInstance() {
		return new STOP();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) {
		//CalcGUISuperWaba.ref.pause();
	}

	public String toString() {
		return ("STOP");
	}
}
