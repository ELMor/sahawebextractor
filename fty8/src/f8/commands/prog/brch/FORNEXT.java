/*
 * Created on 17-ago-2003
 *
 
 
 */
package f8.commands.prog.brch;

import java.io.IOException;
import java.util.Hashtable;

import antlr.collections.AST;
import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.commands.prog.UtilHelper;
import f8.exceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class FORNEXT extends NonAlgebraic {
	Command var;

	Command body;

	Command incr;

	public FORNEXT(){
		this(null);
	}
	
	public FORNEXT(AST fn) {
		super();

		if (fn == null) {
			return;
		}

		AST ch1 = fn.getFirstChild();
		AST ch2 = ch1.getNextSibling();
		var = Command.createFromAST(ch1);
		body = Command.createFromAST(ch2);

		if (fn.getType() == ObjectParserTokenTypes.FORNEXT) {
			incr = new Double(1);
		} else {
			incr = Command.createFromAST(ch2.getNextSibling());
		}
	}

	public void exec(Object extern) throws F8Exception {
		Command tope = Core.pop();
		Command inic = Core.pop();
		String vname = var.toString();
		Core.pushLocalEnvir();
		Hashtable ht = Core.getCurrentLocalEnvir();
		ht.put(vname, inic);

		do {
			body.exec(null);

			Core.push((Stackable) Core.lookup(vname));
			incr.exec(null);
			Core.lookup("+").exec(null);
			inic = Core.peek();
			ht.put(vname, inic);
			tope.exec(null);
			Core.lookup("\u0089").exec(null);
		} while (UtilHelper.evalCondition() == 1);
		Core.popLocalEnvir();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getID()
	 */
	public int getID() {
		return 117;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new FORNEXT(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#saveState(f8.pfr.io.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
		ds.writeInt(var.getID());
		var.saveState(ds);

		ds.writeInt(body.getID());
		body.saveState(ds);

		ds.writeInt(incr.getID());
		incr.saveState(ds);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#loadState(f8.pfr.io.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {
		var = (Command) Command.loadFromStorage(ds);
		body = (Command) Command.loadFromStorage(ds);
		incr = (Command) Command.loadFromStorage(ds);
	}

	public String toString() {
		if(var==null||body==null)
			return "FOR i NEXT";
		return "FOR " + var + " " + body.toString() + " " + incr.toString()
				+ " STEP";
	}
}
