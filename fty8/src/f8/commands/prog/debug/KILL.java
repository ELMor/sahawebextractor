/*
 * Created on 06-oct-2003
 *
 
 
 */
package f8.commands.prog.debug;

import f8.Core;
import f8.commands.Operation;
import f8.exceptions.F8Exception;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class KILL extends Operation {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		while (Core.getProcDebug() != null)
			;
	}

}
