/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.commands.prog.debug;

import f8.Core;
import f8.commands.Operation;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class SST extends Operation {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		Debuggable dbg = Core.getProcDebug();
		if (dbg != null) {
			dbg.execDebug(false);
			if (dbg.isAnyMore()) {
				//Core.temporaryLabels(null, dbg.nextCommand());
				Core.setProcDebug(dbg);
			} else {
				//Core.temporaryLabels(null, "(returning...)");
			}

		} else {
			throw new TooFewArgumentsException("No program to debug");
		}
	}

}
