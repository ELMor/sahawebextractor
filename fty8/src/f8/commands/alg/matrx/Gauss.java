package f8.commands.alg.matrx;


import java.io.IOException;

import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.objects.Stackable;
import f8.objects.types.Int;
import f8.objects.types.Matrix;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class Gauss extends Dispatch2 {
	public Gauss() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 64;
	}

	public Storable getInstance() {
		return new Gauss();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("GAUSS");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfIntMatrix(f8.kernel.types.Int,
	 *      f8.kernel.types.Matrix)
	 */
	public Stackable prfIntMatrix(Int a, Matrix b) throws F8Exception {
		Matrix M = b;
		int C = a.intValue();

		if ((C >= 0) && (C <= M.columnNumber())) {
			MatrixOperations.reduce(M.x, M.rowNumber(), C);
			return M;
		} else {
			throw new IndexRangeException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
