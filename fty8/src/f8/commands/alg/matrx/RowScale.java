package f8.commands.alg.matrx;

import java.io.IOException;

import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.commands.alg.DoubleValue;
import f8.commands.alg.IntValue;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Ereal;
import f8.objects.types.Matrix;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class RowScale extends NonAlgebraic {
	public RowScale() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 71;
	}

	public Storable getInstance() {
		return new RowScale();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	// m i c rowscale
	public void exec(Object extern) throws F8Exception {
		if (Core.check(3)) {
			Stackable M = Core.peek(2);
			Command I = Core.peek(1);
			Command C = Core.peek(0);

			if (M instanceof Matrix && I instanceof IntValue
					&& C instanceof DoubleValue) {
				Matrix m = (Matrix) M;
				int i = ((IntValue) I).intValue();
				Ereal cc = ((DoubleValue) C).doubleValue();
				double c=Double.parseDouble(cc.toString());
				Core.pop(3);

				for (int k = 0; k < m.c; k++) {
					m.x[i][k] *= c;
				}

				Core.push(M);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("ROWSCALE");
	}
}
