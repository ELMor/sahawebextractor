package f8.commands.alg.matrx;


import java.io.IOException;

import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Int;
import f8.objects.types.Matrix;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class DIM extends Dispatch1 {
	public DIM() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 63;
	}

	public Storable getInstance() {
		return new DIM();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("DIM");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfMatrix(f8.kernel.types.Matrix)
	 */
	public Stackable prfMatrix(Matrix a) throws F8Exception {
		return new Int(a.size());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
