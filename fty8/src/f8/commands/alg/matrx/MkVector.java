package f8.commands.alg.matrx;


import java.io.IOException;

import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.InvalidDimensionException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.F8Vector;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class MkVector extends Dispatch1 {
	public MkVector() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 69;
	}

	public Storable getInstance() {
		return new MkVector();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("VECTOR");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double x) throws F8Exception {
		int c = x.doubleValue().toInt();

		if (c >= 0) {
			return (new F8Vector(c));
		} else {
			throw new InvalidDimensionException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
