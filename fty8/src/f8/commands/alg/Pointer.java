package f8.commands.alg;

import f8.commands.Command;
import f8.objects.Stackable;

public abstract class Pointer extends Stackable {
	public abstract Command getItem();
}
