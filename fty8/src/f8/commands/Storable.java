package f8.commands;

import java.io.IOException;

import f8.platform.io.DIS;
import f8.platform.io.DOS;

public interface Storable {
	
	public int getID();

	public void saveState(DOS ds) throws IOException;

	public void loadState(DIS ds) throws IOException;
}
