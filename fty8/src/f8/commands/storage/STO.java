package f8.commands.storage;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.CircularReferenceException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Directory;
import f8.objects.types.Grob;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;
import f8.platform.Calc;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class STO extends NonAlgebraic {
	public STO() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 5;
	}

	public Storable getInstance() {
		return new STO();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		if (Core.check(2)) {
			Command a = Core.peek(0);
			Command b = Core.peek(1);
			String name;
			if (a instanceof Literal) {
				name = ((Literal) a).nombre;
				Core.pop(2);
				if(name.equals("PICT"))
					Calc.ref.getLCD().setGrob((Grob)b);
				else
					storeVar(name, b);
			} else if (a instanceof InfixExp
					&& ((name = InfixExp.isLiteral(a)) != null)) {
				Core.pop(2);
				if(name.equals("PICT"))
					Calc.ref.getLCD().setGrob((Grob)b);
				else
					storeVar(name, b);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public static void storeVar(String name, Command b)
			throws CircularReferenceException {
		Stackable j;
		
		try {
			j = (Stackable) b;
			if (j instanceof InfixExp) {
				Vector incog = mkl.incogOf(j.getAST());
				if (incog.contains(name) ) {
					throw new CircularReferenceException(new STO());
				}
			}
		} catch (ClassCastException cce) {
		}
		//Tenemos en cuenta si es un directorio
		if(b instanceof Directory){
			Directory dir=(Directory)b;
			dir.setName(Core.currentDict(), name);
		}
		Hashtable h = Core.currentDict().getHT();
		h.put(name, b);
		Calc.ref.pantalla.dynamicNeedRefresh=true;
	}

	public String toString() {
		return ("STO");
	}
}
