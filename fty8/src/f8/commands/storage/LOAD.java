package f8.commands.storage;

import java.io.IOException;
import java.util.Vector;

import antlr.TokenStreamException;
import antlr.collections.AST;
import f8.Core;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.F8String;
import f8.platform.Calc;
import f8.platform.io.DIS;
import f8.platform.io.DOS;
import f8.platform.io.StMan;

public class LOAD extends NonAlgebraic {

	public int getID() {
		// TODO Auto-generated method stub
		return 7008;
	}

	public void loadState(DIS ds) throws IOException {


	}

	public void saveState(DOS ds) throws IOException {
		

	}

	public String toString() {
		return "LOAD";
	}

	public void exec(Object extern) throws F8Exception {
		if(!Core.check(1))
			throw new TooFewArgumentsException(this);
		Stackable s=Core.peek();
		if(!(s instanceof F8String))
			throw new BadArgumentTypeException(this);
		Core.pop();
		String err="";
		loadFileFromDumpDir(err, ((F8String)s).getContenido());
		if(err.length()>0){
			Calc.ref.output(err);
		}
	}

	public static String loadFileFromDumpDir(String errores, String name) 
		throws F8Exception {
		String cnt=StMan.loadFrom("dump/"+name);
		if(name.endsWith(".f8")){
			if(name.startsWith("set")){
				int last=name.indexOf(".");
				if(last==-1)
					cnt=null;
				else
					last=Integer.parseInt(name.substring(3,last));
				if(cnt!=null){
					Core.settings[last]=Integer.parseInt(cnt);
				}
			}else if(name.startsWith("stack")){
				int last=name.indexOf(".");
				if(last==-1)
					cnt=null;
				else
					last=Integer.parseInt(name.substring(3,last));
				if(cnt!=null){
					Vector vItems=new Vector();
					try {
						Stackable.parse(cnt, vItems);
						if(vItems.size()>0){
							Core.userStack.ensureCapacity(last);
							Core.userStack.setElementAt(vItems.elementAt(0), last);
						}
					} catch (TokenStreamException e1) {
						e1.printStackTrace();
					}
				}
			}
		}else{
			try {
				Vector vItems=new Vector();
				Stackable.parse(cnt, vItems);
				if(vItems.size()>0){
					Stackable s=(Stackable)Stackable.createFromAST((AST)vItems.elementAt(0));
					Core.homeDir.put(name,s);
				}
			} catch (F8Exception e1) {
				errores+=name+":"+e1.getMessage()+"\n";
				F8String erObj=new F8String(cnt);
				erObj.setTag(e1.getMessage());
				Core.homeDir.put(name,erObj);
			} catch(TokenStreamException e1){
				errores+=name+":"+e1.getMessage()+"\n";
				F8String erObj=new F8String(cnt);
				erObj.setTag(e1.getMessage());
				Core.homeDir.put(name,erObj);
			}
		}
		return errores;
	}
}
