package f8.commands.storage;

import java.io.IOException;
import java.util.Hashtable;

import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.NonEmptyDirException;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Directory;
import f8.objects.types.InfixExp;
import f8.objects.types.Lista;
import f8.objects.types.Literal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class PURGE extends NonAlgebraic {
	public PURGE() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 510;
	}

	public Storable getInstance() {
		return new PURGE();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {

		if (Core.check(1)) {
			Command a = Core.peek(0);
			String name;
			if (a instanceof InfixExp
					&& ((name = InfixExp.isLiteral(a)) != null)) {
				Core.pop();
				purgeVar(name);
			} else if (a instanceof Lista) {
				Core.pop();
				Lista la = (Lista) a;
				for (int i = 0; i < la.size(); i++) {
					if (la.get(i) instanceof Literal) {
						name = ((Literal) la.get(i)).nombre;
						Hashtable itemHolder = Core.currentDict().getHT();
						if ((Stackable) itemHolder.get(name) instanceof Directory) {
							throw new NonEmptyDirException(this);
						} else {
							itemHolder.remove(name);
						}
					} else {
						throw new BadArgumentTypeException(this);
					}
				}
				//Core.refreshProgMenu();
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public static void purgeVar(String name) throws NonEmptyDirException {
		Hashtable itemHolder = Core.currentDict().getHT();
		if ((Stackable) itemHolder.get(name) instanceof Directory) {
			throw new NonEmptyDirException(new PURGE());
		} else {
			itemHolder.remove(name);
		}
		//Core.refreshProgMenu();
	}

	public String toString() {
		return ("PURGE");
	}
}
