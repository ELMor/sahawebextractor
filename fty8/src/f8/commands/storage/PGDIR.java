package f8.commands.storage;

import java.io.IOException;
import java.util.Hashtable;

import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Directory;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class PGDIR extends NonAlgebraic {
	public PGDIR() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 509;
	}

	public Storable getInstance() {
		return new PGDIR();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		

		if (Core.check(1)) {
			Command a = Core.peek(0);
			String name;
			Hashtable itemHolder = Core.currentDict().getHT();
			if (a instanceof Literal) {
				name = ((Literal) a).nombre;
				removeDir(itemHolder, name);
			} else if (a instanceof InfixExp
					&& ((name = InfixExp.isLiteral(a)) != null)) {
				removeDir(itemHolder, name);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	private void removeDir(Hashtable itemHolder, String name)
			throws F8Exception {
		Core.pop();
		if ((Stackable) itemHolder.get(name) instanceof Directory) {
			itemHolder.remove(name);
		} else {
			throw new BadArgumentTypeException(this);
		}
		//Core.refreshProgMenu();
	}

	public String toString() {
		return ("PGDIR");
	}
}
