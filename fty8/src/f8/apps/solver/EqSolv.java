/*
 * Created on 21-sep-2003
 *
 
 
 */
package f8.apps.solver;

import java.util.Vector;

import antlr.collections.AST;
import f8.Core;
import f8.commands.Command;
import f8.commands.alg.CommandSequence;
import f8.commands.math.mkl;
import f8.commands.stk.EVAL;
import f8.commands.storage.PURGE;
import f8.commands.storage.STO;
import f8.exceptions.BadArgumentValueException;
import f8.exceptions.CircularReferenceException;
import f8.exceptions.F8Exception;
import f8.exceptions.NonEmptyDirException;
import f8.exceptions.UndefinedException;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.InfixExp;
import f8.objects.types.Lista;
import f8.objects.types.Literal;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class EqSolv {
	
	/**
	 * MES Multiple Equation Solver
	 * El parametro de entrada puede ser de varias formas:
	 * Lista de ecuaciones
	 * Si el primere elemento es, a su vez, una lista; el primero elemento puede ser
	 * un GROB y los siguientes, strings, describen las variables/parametros del grupo 
	 * de ecuaciones
	 * @param l
	 */
	public static void solve(Lista l){
		
	}
	/**
	 * Dada una expresion y una variable, trata de encontrar un zero
	 * @param exp Expresion a resolver
	 * @param name Variable
	 * @throws F8Exception
	 */
	public static void solve(InfixExp exp, String name) throws F8Exception {
		// Primero evaluamos exp para comprobar que la unica variable
		// que puede no estar definida es name
		checkFunctionParams(exp, name);
		AST e = exp.getAST();
		if (e.getType() == ObjectParserTokenTypes.ASSIGN) {
			exp = new InfixExp(mkl.res(e.getFirstChild(), e.getFirstChild()
					.getNextSibling()));
		}
		//Intentamos optimizar la expresion evaluando a numero 
		//todos sus componentes
		exp = optimize(exp,name);
		//La convertimos en una secuencia de comandos de pila
		CommandSequence run = InfixExp.decode(exp.getAST());
		//Maximo error permitido
		Ereal tolerance1=new Ereal("1E-15");
		//Valor inicial si existe se coloca
		Solverable initX = (Solverable) Core.lookup(name);
		if (initX == null) {
			//Valor inicial
			initX = new Double(0);
		}
		try {
			//Borramos el valor de la variable
			PURGE.purgeVar(name);
		} catch (NonEmptyDirException e1) {
		}
		//Seleccion del rango inicial
		//Rango inicial X
		Solverable leftX = initX.amplia(Ereal.uno.neg());
		Solverable rightX = initX.amplia(Ereal.uno);
		//Rango Inicial Y
		Solverable initY = fdex(run, name, initX);
		Solverable lastInitY = null;
		Solverable leftY = fdex(run, name, leftX);
		Solverable rightY = fdex(run, name, rightX);
		
		while ((lastInitY == null)
				|| (!initY.tolerance(initY.zero(), tolerance1) && 
					!initY.tolerance(lastInitY, tolerance1))) {
			Ereal compYs = leftY.compare(rightY);
			//Rangos de Y negativos
			if ((leftY.sign() < 0) && (rightY.sign() < 0)) {
				if (compYs.isNegative() || compYs.isZero()) {
					leftX = rightX;
					leftY = rightY;
					rightX = rightX.amplia(compYs.eq(0) ? Ereal.dos.neg() : compYs.neg());
					rightY = fdex(run, name, rightX);
				} else {
					rightX = leftX;
					rightY = leftY;
					leftX = leftX.amplia(compYs.neg());
					leftY = fdex(run, name, leftX);
				}
			//Rangos de Y positivos
			} else if ((leftY.sign() > 0) && (rightY.sign() > 0)) {
				if (compYs.isNegative() || compYs.isZero()) {
					rightX = leftX;
					rightY = leftY;
					leftX = leftX.amplia(compYs.eq(0) ? Ereal.dos : compYs);
					leftY = fdex(run, name, leftX);
				} else {
					leftX = rightX;
					leftY = rightY;
					rightX = rightX.amplia(compYs);
					rightY = fdex(run, name, rightX);
				}
			//Cambio de signo! posible cero entre leftX y LeftY
			} else {
				initX = initX.mean(leftX, rightX);
				initY = fdex(run, name, initX);
				if ((leftY.sign() != initY.sign())) {
					rightX = initX;
					rightY = initY;
					lastInitY = rightY.mean(leftY, initY);
				} else {
					leftX = initX;
					leftY = initY;
					lastInitY = rightY.mean(rightY, initY);
				}
			}
		}
		try {
			STO.storeVar(name, (Stackable) initX);
		} catch (CircularReferenceException e3) {
		}
		if (initY.tolerance(initY.zero(), new Ereal("1E-10"))) { // Zero
			((Stackable) initX).setTag(name + " " + "Zero");
		} else { // Max/Min
			((Stackable) initX).setTag(name + " " + "Maximum / Minimum");
		}
		Core.push(((Stackable) initX));
	}

	/**
	 * substituye todos los paramtros simbolicos por sus valores
	 * @param exp
	 * @return
	 */
	public static InfixExp optimize(InfixExp exp, String exceptName) {
		//Borramos exceptName
		Command en=Core.lookup(exceptName);
		if(en!=null){
			Core.push(new Literal(exceptName));
			try {
				new PURGE().exec(null);
			}catch (F8Exception e0){
			}
		}
		try {
			Core.push(exp);
			new EVAL().exec(null);
		} catch (F8Exception e2) {
		}
		exp = (InfixExp) Core.pop();
		if(en!=null){
			Core.push((Stackable)en);
			Core.push(new Literal(exceptName));
			try{
				new STO().exec(null);
			}catch(F8Exception e4){
			}
		}
		return exp;
	}
	/**
	 * Comprueba que todos los parametros estan definidos 
	 * @param exp Expresion a comprobar
	 * @param name Variable de la expresion (se permite que no este definida)
	 * @throws UndefinedException
	 */
	public static void checkFunctionParams(InfixExp exp, String name)
			throws UndefinedException {
		Vector inco = mkl.incogOf(exp.getAST());
		for (int i = 0; i < inco.size(); i++) {
			String vn = (String) inco.elementAt(i);
			if ((Core.lookup(vn) == null) & !vn.equals(name)) {
				throw new UndefinedException(
						"Equation solver|Some vars not defined!");
			}
		}
	}
	/**
	 * Retorna el valor de la expresion
	 * @param r Expresion en forma de CommandSequence
	 * @param n Nombre de la variable
	 * @param x Valor de la variable
	 * @return
	 * @throws F8Exception
	 */
	public static Solverable fdex(CommandSequence r, String n, Solverable x)
			throws F8Exception {
		STO.storeVar(n, (Stackable) x);
		r.exec(null);
		Stackable pp=Core.pop();
		if(pp instanceof Double)
			return (Solverable)pp;
		if(pp instanceof InfixExp){
			Stackable rr=((InfixExp)pp).aprox;
			if(rr!=null && (rr instanceof Double || rr instanceof Complex))
				return (Solverable)rr;
		}
		return (Solverable) pp;
	}
	/**
	 * Overload de fdex. Se asegura que el valor es Real
	 * @param r
	 * @param n
	 * @param x
	 * @return
	 * @throws F8Exception
	 */
	public static Ereal fdexDouble(CommandSequence r, String n, Ereal x)
			throws F8Exception {
		Solverable ret = fdex(r, n, new Double(x));
		if (ret instanceof Double) {
			return ((Double) ret).doubleValue();
		}
		throw new BadArgumentValueException(
				"Expression does not| evaluate to double");
	}
}
