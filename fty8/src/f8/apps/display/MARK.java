package f8.apps.display;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.platform.Calc;
import f8.platform.Plot;
import f8.platform.io.DIS;
import f8.platform.io.DOS;


public final class MARK extends NonAlgebraic {
	public MARK() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 401;
	}

	public Storable getInstance() {
		return new MARK();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		Plot lcd = Calc.ref.getLCD();
		if (Core.check(1)) {
			CSys.Point c = StackChecker.UserCoordinates(Core.peek());
			if (c != null) {
				Core.pop();
				lcd.setMark(c);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("MARK");
	}
}
