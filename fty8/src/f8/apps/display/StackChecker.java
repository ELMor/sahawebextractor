/*
 * Created on 14-sep-2003
 *
 
 
 */
package f8.apps.display;

import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Int;
import f8.objects.types.Lista;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class StackChecker {
	public static CSys.Point UserCoordinates(Stackable esto) throws F8Exception {
		CSys.Point c = null;
		if (esto instanceof Lista) { // Display coo
			Lista list = (Lista) esto;
			if (list.size() == 2) {
				if (list.get(0) instanceof Int && list.get(1) instanceof Int) {
					// Coordenadas fisicas o de pantalla, copiar
					c = CSys.getCoorSystem("lcd").coor(
							((Int) list.get(0)).intValue(),
							((Int) list.get(1)).intValue());
				}
			}
		} else if (esto instanceof Complex) {
			Complex cmp = (Complex) esto;
			c = CSys.getCoorSystem("usr").coor(
					cmp.complexValue()[0].toInt(),
					cmp.complexValue()[1].toInt());
		}
		return c;
	}
}
