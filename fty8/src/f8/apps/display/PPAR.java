/*
 * Created on 09-oct-2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package f8.apps.display;

import java.util.Vector;

import f8.Core;
import f8.commands.storage.STO;
import f8.exceptions.CircularReferenceException;
import f8.exceptions.IndexRangeException;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.Lista;
import f8.objects.types.Literal;

/**
 * @author elinares
 * 
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PPAR {
	public static Ereal Xmin;

	public static Ereal Xmax;

	public static Ereal Ymin;

	public static Ereal Ymax;

	public static Ereal Ox;

	public static Ereal Oy;

	public static String indep;

	public static String depen;

	public static Ereal height;

	public static Ereal width;

	public static void savePPAR() {
		Lista ppar;
		Vector val = new Vector();
		val.addElement(new Complex(Xmin, Ymin));
		val.addElement(new Complex(Xmax, Ymax));
		val.addElement(new Literal(indep));
		val.addElement(new Double(0));
		val.addElement(new Complex(Ereal.cero, Ereal.cero));
		val.addElement(new Literal("FUNCTION"));
		val.addElement(new Literal(depen));
		ppar = new Lista(val);
		try {
			STO.storeVar("PPAR", ppar);
		} catch (CircularReferenceException e) {
		}
	}

	public static Lista initPPAR() {
		/* Vamos a crear PPAR si no lo esta ya */
		Lista ppar = loadPPAR();
		if (ppar == null) {
			Vector val = new Vector();
			val.addElement(new Complex("-6.5","-3.2"));
			val.addElement(new Complex("6.5", "3.2"));
			val.addElement(new Literal("X"));
			val.addElement(new Double(0));
			val.addElement(new Complex(Ereal.cero, Ereal.cero));
			val.addElement(new Literal("FUNCTION"));
			val.addElement(new Literal("Y"));
			ppar = new Lista(val);
			try {
				STO.storeVar("PPAR", ppar);
			} catch (CircularReferenceException e) {
			}
		}
		try {
			Xmin = ((Complex) ppar.get(0)).re;
			Ymin = ((Complex) ppar.get(0)).im;
			Xmax = ((Complex) ppar.get(1)).re;
			Ymax = ((Complex) ppar.get(1)).im;
			Ox = ((Complex) ppar.get(4)).re;
			Oy = ((Complex) ppar.get(4)).im;
			indep = ((Literal) ppar.get(2)).nombre;
			depen = ((Literal) ppar.get(6)).nombre;
			height = Ymax.minus(Ymin);
			width = Xmax.minus(Xmin);
		} catch (IndexRangeException e) {
		}
		return ppar;
	}

	public static Lista loadPPAR() {
		try {
			/* Vamos a crear PPAR si no lo esta ya */
			return (Lista) Core.lookupUser("PPAR", false, true, false);
		} catch (ClassCastException e) {
			return null;
		}
	}
}
