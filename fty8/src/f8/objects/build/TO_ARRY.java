/*
 * Created on 14-sep-2003
 *
 
 
 */
package f8.objects.build;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.BadArgumentValueException;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.F8Vector;
import f8.objects.types.Lista;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class TO_ARRY extends NonAlgebraic {
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "\u008DARRY";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 3059;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		
		if (Core.check(1)) {
			Stackable sL = Core.peek();
			if (sL instanceof Lista) {
				Lista l = (Lista) sL;
				int sz = l.size();
				if (sz == 1) {
					mkVector( l);
				} else if (sz == 2) {
					mkMatrix( l);
				} else {
					throw new BadArgumentValueException(this);
				}
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	private void mkVector(Lista l) throws IndexRangeException,
			BadArgumentValueException, TooFewArgumentsException {
		int sz;
		if (l.get(0) instanceof Double) {
			sz = ((Double) l.get(0)).intValue();
			Core.pop();
			if (Core.check(sz)) {
				Ereal[] vals = new Ereal[sz];
				try {
					for (int i = 1; i <= sz; i++) {
						vals[sz - i] = ((Double) Core.pop()).doubleValue();
					}
					Core.push(new F8Vector(vals));
				} catch (Exception e) {
					throw new BadArgumentValueException(this);
				}
			} else {
				throw new TooFewArgumentsException(this);
			}
		} else {
			throw new BadArgumentValueException(this);
		}
	}

	private void mkMatrix(Lista l) throws IndexRangeException,
			TooFewArgumentsException, BadArgumentValueException {
		if (l.get(0) instanceof Double && l.get(1) instanceof Double) {
			int row = ((Double) l.get(0)).intValue();
			int col = ((Double) l.get(1)).intValue();
			Core.pop();
			if (Core.check(row * col)) {
				double[][] vals = new double[row][col];
				for (int i = 0; i < row; i++) {
					for (int j = 0; i < col; j++) {
						vals[i][j] = 
							java.lang.Double.parseDouble(((Double) Core.peek((row - i) * (col - j))).doubleValue().toString())
							;
					}
				}
				Core.pop(row * col);
			} else {
				throw new TooFewArgumentsException(this);
			}
		} else {
			throw new BadArgumentValueException(this);
		}
	}
}
