package f8.objects.build;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class TO_UNIT extends NonAlgebraic {
	public TO_UNIT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 508;
	}

	public Storable getInstance() {
		return new TO_UNIT();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("\u008DUNIT");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		
		Stackable[] arg = new Stackable[2];
		arg[0] = new Double(0);
		arg[1] = new Unit(null);
		String dsp = Core.dispatch(arg);
		if (dsp != null) {
			Core.push(new Unit(((Double) arg[0]).doubleValue(), ((Unit) arg[1]).getUnidad()));
		} else {
			throw new BadArgumentTypeException(this);
		}
	}
}
