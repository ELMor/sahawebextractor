package f8.objects.types;

import java.io.IOException;

import f8.Core;
import f8.commands.Command;
import f8.commands.Storable;
import f8.commands.alg.CholeskyDecomposition;
import f8.commands.alg.EigenvalueDecomposition;
import f8.commands.alg.LUDecomposition;
import f8.commands.alg.QRDecomposition;
import f8.commands.alg.SingularValueDecomposition;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.objects.Stackable;
import f8.objects.build.TO_LIST;
import f8.platform.F8Math;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * Jama = Java Matrix class.
 * <P>
 * The Java Matrix Class provides the fundamental operations of numerical linear
 * algebra. Various constructors create Matrices from two dimensional arrays of
 * double precision floating point numbers. Various "gets" and "sets" provide
 * access to submatrices and matrix elements. Several methods implement basic
 * matrix arithmetic, including matrix addition and multiplication, matrix
 * norms, and element-by-element array operations. Methods for reading and
 * printing matrices are also included. All the operations in this version of
 * the Matrix Class involve real matrices. Complex matrices may be handled in a
 * future version.
 * <P>
 * Five fundamental matrix decompositions, which consist of pairs or triples of
 * matrices, permutation vectors, and the like, produce results in five
 * decomposition classes. These decompositions are accessed by the Matrix class
 * to compute solutions of simultaneous linear equations, determinants, inverses
 * and other matrix functions. The five decompositions are:
 * <P>
 * <UL>
 * <LI>Cholesky Decomposition of symmetric, positive definite matrices.
 * <LI>LU Decomposition of rectangular matrices.
 * <LI>QR Decomposition of rectangular matrices.
 * <LI>Singular Value Decomposition of rectangular matrices.
 * <LI>Eigenvalue Decomposition of both symmetric and nonsymmetric square
 * matrices.
 * </UL>
 * <DL>
 * <DT><B>Example of use:</B></DT>
 * <P>
 * <DD>Solve a linear system A x = b and compute the residual norm, ||b - A
 * x||.
 * <P>
 * 
 * <PRE>
 * 
 * double[][] vals = {{1.,2.,3},{4.,5.,6.},{7.,8.,10.}}; Matrix A = new
 * Matrix(vals); Matrix b = Matrix.random(3,1); Matrix x = A.solve(b); Matrix r =
 * A.times(x).minus(b); double rnorm = r.normInf();
 * 
 * </PRE>
 * 
 * </DD>
 * </DL>
 * 
 * @author The MathWorks, Inc. and the National Institute of Standards and
 *         Technology.
 * @version 5 August 1998
 */

public final class Matrix extends Array {

	/*
	 * ------------------------ Public Methods ------------------------
	 */

	/**
	 * Construct a matrix from a copy of a 2-D array.
	 * 
	 * @param A
	 *            Two-dimensional array of doubles.
	 * @exception IllegalArgumentException
	 *                All rows must have the same length
	 */
	public static Matrix constructWithCopy(double[][] A) throws F8Exception {
		int m = A.length;
		int n = A[0].length;
		Matrix X = new Matrix(m, n);
		double[][] C = X.getArray();
		for (int i = 0; i < m; i++) {
			if (A[i].length != n) {
				throw new BadArgumentTypeException(
						"All rows must have the same length.");
			}
			for (int j = 0; j < n; j++) {
				C[i][j] = A[i][j];
			}
		}
		return X;
	}

	/**
	 * Generate identity matrix
	 * 
	 * @param m
	 *            Number of rows.
	 * @param n
	 *            Number of colums.
	 * @return An m-by-n matrix with ones on the diagonal and zeros elsewhere.
	 */
	public static Matrix identity(int m, int n) {
		Matrix A = new Matrix(m, n);
		double[][] X = A.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				X[i][j] = ((i == j) ? 1.0 : 0.0);
			}
		}
		return A;
	}

	public int c;

	public int r;

	public double[][] x;

	/**
	 * Construct a matrix from a one-dimensional packed array
	 * 
	 * @param vals
	 *            One-dimensional array of doubles, packed by columns (ala
	 *            Fortran).
	 * @param m
	 *            Number of rows.
	 * @exception IllegalArgumentException
	 *                Array length must be a multiple of m.
	 */
	public Matrix(double[] vals, int m) throws F8Exception {
		this.r = m;
		c = ((m != 0) ? (vals.length / m) : 0);
		if ((m * c) != vals.length) {
			throw new BadArgumentTypeException(
					"Array length must be a multiple of m.");
		}
		x = new double[m][c];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < c; j++) {
				x[i][j] = vals[i + (j * m)];
			}
		}
	}

	/**
	 * Construct a matrix from a 2-D array.
	 * 
	 * @param A
	 *            Two-dimensional array of doubles.
	 * @exception BadArgumentTypeException
	 *                All rows must have the same length
	 * @see #constructWithCopy
	 */
	public Matrix(double[][] A) {
		r = A.length;
		if (r > 0) {
			c = A[0].length;
		} else {
			c = 0;
		}
		this.x = A;
	}

	/**
	 * Construct a matrix quickly without checking arguments.
	 * 
	 * @param A
	 *            Two-dimensional array of doubles.
	 * @param m
	 *            Number of rows.
	 * @param n
	 *            Number of colums.
	 */
	public Matrix(double[][] A, int m, int n) {
		this.x = A;
		this.r = m;
		this.c = n;
	}

	/*
	 * ------------------------ Constructors ------------------------
	 */

	/**
	 * Construct an m-by-n matrix of zeros.
	 * 
	 * @param m
	 *            Number of rows.
	 * @param n
	 *            Number of colums.
	 */
	public Matrix(int m, int n) {
		this.r = m;
		this.c = n;
		x = new double[m][n];
	}

	public Matrix(){
		this(0,0);
	}
	
	/**
	 * Construct an m-by-n constant matrix.
	 * 
	 * @param m
	 *            Number of rows.
	 * @param n
	 *            Number of colums.
	 * @param s
	 *            Fill the matrix with this scalar value.
	 */
	public Matrix(int m, int n, double s) {
		this.r = m;
		this.c = n;
		x = new double[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				x[i][j] = s;
			}
		}
	}

	/**
	 * Element-by-element left division, C = A.\B
	 * 
	 * @param B
	 *            another matrix
	 * @return A.\B
	 */
	public Matrix arrayLeftDivide(Matrix B) throws F8Exception {
		checkMatrixDimensions(B);
		Matrix X = new Matrix(r, c);
		double[][] C = X.getArray();
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				C[i][j] = B.x[i][j] / x[i][j];
			}
		}
		return X;
	}

	/**
	 * Element-by-element left division in place, A = A.\B
	 * 
	 * @param B
	 *            another matrix
	 * @return A.\B
	 */
	public Matrix arrayLeftDivideEquals(Matrix B) throws F8Exception {
		checkMatrixDimensions(B);
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				x[i][j] = B.x[i][j] / x[i][j];
			}
		}
		return this;
	}

	/**
	 * Element-by-element right division, C = A./B
	 * 
	 * @param B
	 *            another matrix
	 * @return A./B
	 */
	public Matrix arrayRightDivide(Matrix B) throws F8Exception {
		checkMatrixDimensions(B);
		Matrix X = new Matrix(r, c);
		double[][] C = X.getArray();
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				C[i][j] = x[i][j] / B.x[i][j];
			}
		}
		return X;
	}

	/**
	 * Element-by-element right division in place, A = A./B
	 * 
	 * @param B
	 *            another matrix
	 * @return A./B
	 */
	public Matrix arrayRightDivideEquals(Matrix B) throws F8Exception {
		checkMatrixDimensions(B);
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				x[i][j] = x[i][j] / B.x[i][j];
			}
		}
		return this;
	}

	/**
	 * Element-by-element multiplication, C = A.*B
	 * 
	 * @param B
	 *            another matrix
	 * @return A.*B
	 */
	public Matrix arrayTimes(Matrix B) throws F8Exception {
		checkMatrixDimensions(B);
		Matrix X = new Matrix(r, c);
		double[][] C = X.getArray();
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				C[i][j] = x[i][j] * B.x[i][j];
			}
		}
		return X;
	}

	/**
	 * Element-by-element multiplication in place, A = A.*B
	 * 
	 * @param B
	 *            another matrix
	 * @return A.*B
	 */
	public Matrix arrayTimesEquals(Matrix B) throws F8Exception {
		checkMatrixDimensions(B);
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				x[i][j] = x[i][j] * B.x[i][j];
			}
		}
		return this;
	}

	/*
	 * ------------------------ Private Methods ------------------------
	 */

	/** Check if size(A) == size(B) * */
	private void checkMatrixDimensions(Matrix B) throws F8Exception {
		if ((B.r != r) || (B.c != c)) {
			throw new BadArgumentTypeException("Matrix dimensions must agree.");
		}
	}

	/**
	 * Cholesky Decomposition
	 * 
	 * @return CholeskyDecomposition
	 * @see CholeskyDecomposition
	 */
	public CholeskyDecomposition chol() {
		return new CholeskyDecomposition(this);
	}

	public int columnNumber() {
		return (c);
	}

	/**
	 * Matrix condition (2 norm)
	 * 
	 * @return ratio of largest to smallest singular value.
	 */
	public double cond() {
		return new SingularValueDecomposition(this).cond();
	}

	public Stackable copia() {
		Matrix nm = new Matrix(r, c);
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				nm.x[i][j] = x[i][j];
			}
		}
		return nm;
	}

	/**
	 * Make a deep copy of a matrix
	 */
	public Matrix copy() {
		Matrix X = new Matrix(r, c);
		double[][] C = X.getArray();
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				C[i][j] = x[i][j];
			}
		}
		return X;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() throws F8Exception {
		for (int i = 0; i < x[0].length; i++) {
			for (int j = 0; j < x.length; j++) {
				Core.push(new Double(new Ereal(""+x[i][j])));
			}
		}
		Core.push(new Double(x[0].length));
		Core.push(new Double(x.length));
		Core.push(new Double(2));
		new TO_LIST().exec(null);
		return false;
	}

	/**
	 * Matrix determinant
	 * 
	 * @return determinant
	 */
	public double det() throws F8Exception {
		return new LUDecomposition(this).det();
	}

	/**
	 * Eigenvalue Decomposition
	 * 
	 * @return EigenvalueDecomposition
	 * @see EigenvalueDecomposition
	 */
	public EigenvalueDecomposition eig() {
		return new EigenvalueDecomposition(this);
	}

	// range assumed OK
	public Command get(int i) {
		Ereal y[]=new Ereal[x[i].length];
		for(int j=0;j<x[i].length;j++)
			y[j]=new Ereal(""+x[i][j]);
		return (new F8Vector(y));
	}

	/**
	 * Get a single element.
	 * 
	 * @param i
	 *            Row index.
	 * @param j
	 *            Column index.
	 * @return A(i,j)
	 * @exception ArrayIndexOutOfBoundsException
	 */
	public double get(int i, int j) {
		return x[i][j];
	}

	/**
	 * Access the internal two-dimensional array.
	 * 
	 * @return Pointer to the two-dimensional array of matrix elements.
	 */
	public double[][] getArray() {
		return x;
	}

	/**
	 * Copy the internal two-dimensional array.
	 * 
	 * @return Two-dimensional array copy of matrix elements.
	 */
	public double[][] getArrayCopy() {
		double[][] C = new double[r][c];
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				C[i][j] = x[i][j];
			}
		}
		return C;
	}

	/**
	 * Get column dimension.
	 * 
	 * @return n, the number of columns.
	 */
	public int getColumnDimension() {
		return c;
	}

	/**
	 * Make a one-dimensional column packed copy of the internal array.
	 * 
	 * @return Matrix elements packed in a one-dimensional array by columns.
	 */
	public double[] getColumnPackedCopy() {
		double[] vals = new double[r * c];
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				vals[i + (j * r)] = x[i][j];
			}
		}
		return vals;
	}

	public int getID() {
		return 67;
	}

	public Storable getInstance() {
		return new Matrix(new double[0][0]);
	}

	/**
	 * Get a submatrix.
	 * 
	 * @param i0
	 *            Initial row index
	 * @param i1
	 *            Final row index
	 * @param j0
	 *            Initial column index
	 * @param j1
	 *            Final column index
	 * @return A(i0:i1,j0:j1)
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public Matrix getMatrix(int i0, int i1, int j0, int j1) {
		Matrix X = new Matrix(i1 - i0 + 1, j1 - j0 + 1);
		double[][] B = X.getArray();
		try {
			for (int i = i0; i <= i1; i++) {
				for (int j = j0; j <= j1; j++) {
					B[i - i0][j - j0] = x[i][j];
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
		return X;
	}

	/**
	 * Get a submatrix.
	 * 
	 * @param i0
	 *            Initial row index
	 * @param i1
	 *            Final row index
	 * @param c
	 *            Array of column indices.
	 * @return A(i0:i1,c(:))
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public Matrix getMatrix(int i0, int i1, int[] c) {
		Matrix X = new Matrix(i1 - i0 + 1, c.length);
		double[][] B = X.getArray();
		try {
			for (int i = i0; i <= i1; i++) {
				for (int j = 0; j < c.length; j++) {
					B[i - i0][j] = x[i][c[j]];
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
		return X;
	}

	/**
	 * Get a submatrix.
	 * 
	 * @param r
	 *            Array of row indices.
	 * @param i0
	 *            Initial column index
	 * @param i1
	 *            Final column index
	 * @return A(r(:),j0:j1)
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public Matrix getMatrix(int[] r, int j0, int j1) {
		Matrix X = new Matrix(r.length, j1 - j0 + 1);
		double[][] B = X.getArray();
		try {
			for (int i = 0; i < r.length; i++) {
				for (int j = j0; j <= j1; j++) {
					B[i][j - j0] = x[r[i]][j];
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
		return X;
	}

	/**
	 * Get a submatrix.
	 * 
	 * @param r
	 *            Array of row indices.
	 * @param c
	 *            Array of column indices.
	 * @return A(r(:),c(:))
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public Matrix getMatrix(int[] r, int[] c) {
		Matrix X = new Matrix(r.length, c.length);
		double[][] B = X.getArray();
		try {
			for (int i = 0; i < r.length; i++) {
				for (int j = 0; j < c.length; j++) {
					B[i][j] = x[r[i]][c[j]];
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
		return X;
	}

	/**
	 * Get row dimension.
	 * 
	 * @return m, the number of rows.
	 */
	public int getRowDimension() {
		return r;
	}

	/**
	 * Make a one-dimensional row packed copy of the internal array.
	 * 
	 * @return Matrix elements packed in a one-dimensional array by rows.
	 */
	public double[] getRowPackedCopy() {
		double[] vals = new double[r * c];
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				vals[(i * c) + j] = x[i][j];
			}
		}
		return vals;
	}

	public String getTypeName() {
		return "Matrix";
	}

	/**
	 * Matrix inverse or pseudoinverse
	 * 
	 * @return inverse(A) if A is square, pseudoinverse otherwise.
	 */
	public Matrix inverse() throws F8Exception {
		return solve(identity(r, r));
	}

	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
		r = ds.readInt();
		c = ds.readInt();
		x = new double[r][c];

		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				x[i][j] = ds.readDouble();
			}
		}
	}

	/**
	 * LU Decomposition
	 * 
	 * @return LUDecomposition
	 * @see LUDecomposition
	 */
	public LUDecomposition lu() {
		return new LUDecomposition(this);
	}

	/**
	 * C = A - B
	 * 
	 * @param B
	 *            another matrix
	 * @return A - B
	 */
	public Matrix minus(Matrix B) throws F8Exception {
		checkMatrixDimensions(B);
		Matrix X = new Matrix(r, c);
		double[][] C = X.getArray();
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				C[i][j] = x[i][j] - B.x[i][j];
			}
		}
		return X;
	}

	/**
	 * A = A - B
	 * 
	 * @param B
	 *            another matrix
	 * @return A - B
	 */
	public Matrix minusEquals(Matrix B) throws F8Exception {
		checkMatrixDimensions(B);
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				x[i][j] = x[i][j] - B.x[i][j];
			}
		}
		return this;
	}

	/**
	 * One norm
	 * 
	 * @return maximum column sum.
	 */
	public double norm1() {
		double f = 0;
		for (int j = 0; j < c; j++) {
			double s = 0;
			for (int i = 0; i < r; i++) {
				s += Math.abs(x[i][j]);
			}
			f = Math.max(f, s);
		}
		return f;
	}

	/**
	 * Two norm
	 * 
	 * @return maximum singular value.
	 */
	public double norm2() {
		return (new SingularValueDecomposition(this).norm2());
	}

	/**
	 * Frobenius norm
	 * 
	 * @return sqrt of sum of squares of all elements.
	 */
	public double normF() {
		double f = 0;
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				f = F8Math.hypot(f, x[i][j]);
			}
		}
		return f;
	}

	/**
	 * Infinity norm
	 * 
	 * @return maximum row sum.
	 */
	public double normInf() {
		double f = 0;
		for (int i = 0; i < r; i++) {
			double s = 0;
			for (int j = 0; j < c; j++) {
				s += Math.abs(x[i][j]);
			}
			f = Math.max(f, s);
		}
		return f;
	}

	/**
	 * C = A + B
	 * 
	 * @param B
	 *            another matrix
	 * @return A + B
	 */
	public Matrix plus(Matrix B) throws F8Exception {
		checkMatrixDimensions(B);
		Matrix X = new Matrix(r, c);
		double[][] C = X.getArray();
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				C[i][j] = x[i][j] + B.x[i][j];
			}
		}
		return X;
	}

	/**
	 * A = A + B
	 * 
	 * @param B
	 *            another matrix
	 * @return A + B
	 */
	public Matrix plusEquals(Matrix B) throws F8Exception {
		checkMatrixDimensions(B);
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				x[i][j] = x[i][j] + B.x[i][j];
			}
		}
		return this;
	}

	public void put(Command a, int i) throws F8Exception {

		if (a instanceof F8Vector) {
			if ((0 <= i) && (i < x.length)) {
				Ereal[] y = ((F8Vector) a).x;
				double[] yy=new double[y.length];
				for(int ijk=0;ijk<yy.length;ijk++)
					yy[ijk]=java.lang.Double.parseDouble(y[ijk].toString());
				if (y.length == c) {
					x[i] = yy;
				} else {
					throw new IndexRangeException(this);
				}
			} else {
				throw new IndexRangeException(this);
			}
		} else {
			throw new BadArgumentTypeException(this);
		}
	}

	/**
	 * QR Decomposition
	 * 
	 * @return QRDecomposition
	 * @see QRDecomposition
	 */
	public QRDecomposition qr() {
		return new QRDecomposition(this);
	}

	/**
	 * Matrix rank
	 * 
	 * @return effective numerical rank, obtained from SVD.
	 */
	public int rank() {
		return new SingularValueDecomposition(this).rank();
	}

	public int rowNumber() {
		return (r);
	}

	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		ds.writeInt(r);
		ds.writeInt(c);

		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				ds.writeDouble(x[i][j]);
			}
		}
	}

	/**
	 * Set a single element.
	 * 
	 * @param i
	 *            Row index.
	 * @param j
	 *            Column index.
	 * @param s
	 *            A(i,j).
	 * @exception ArrayIndexOutOfBoundsException
	 */
	public void set(int i, int j, double s) {
		x[i][j] = s;
	}

	/**
	 * Set a submatrix.
	 * 
	 * @param i0
	 *            Initial row index
	 * @param i1
	 *            Final row index
	 * @param j0
	 *            Initial column index
	 * @param j1
	 *            Final column index
	 * @param X
	 *            A(i0:i1,j0:j1)
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public void setMatrix(int i0, int i1, int j0, int j1, Matrix X) {
		try {
			for (int i = i0; i <= i1; i++) {
				for (int j = j0; j <= j1; j++) {
					x[i][j] = X.get(i - i0, j - j0);
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
	}

	/**
	 * Set a submatrix.
	 * 
	 * @param i0
	 *            Initial row index
	 * @param i1
	 *            Final row index
	 * @param c
	 *            Array of column indices.
	 * @param X
	 *            A(i0:i1,c(:))
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public void setMatrix(int i0, int i1, int[] c, Matrix X) {
		try {
			for (int i = i0; i <= i1; i++) {
				for (int j = 0; j < c.length; j++) {
					x[i][c[j]] = X.get(i - i0, j);
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
	}

	/**
	 * Set a submatrix.
	 * 
	 * @param r
	 *            Array of row indices.
	 * @param j0
	 *            Initial column index
	 * @param j1
	 *            Final column index
	 * @param X
	 *            A(r(:),j0:j1)
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public void setMatrix(int[] r, int j0, int j1, Matrix X) {
		try {
			for (int i = 0; i < r.length; i++) {
				for (int j = j0; j <= j1; j++) {
					x[r[i]][j] = X.get(i, j - j0);
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
	}

	/**
	 * Set a submatrix.
	 * 
	 * @param r
	 *            Array of row indices.
	 * @param c
	 *            Array of column indices.
	 * @param X
	 *            A(r(:),c(:))
	 * @exception ArrayIndexOutOfBoundsException
	 *                Submatrix indices
	 */
	public void setMatrix(int[] r, int[] c, Matrix X) {
		try {
			for (int i = 0; i < r.length; i++) {
				for (int j = 0; j < c.length; j++) {
					x[r[i]][c[j]] = X.get(i, j);
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
	}

	public int size() {
		return (r);
	}

	/**
	 * Solve A*X = B
	 * 
	 * @param B
	 *            right hand side
	 * @return solution if A is square, least squares solution otherwise
	 */
	public Matrix solve(Matrix B) throws F8Exception {
		return ((r == c) ? (new LUDecomposition(this)).solve(B)
				: (new QRDecomposition(this)).solve(B));
	}

	/**
	 * Solve X*A = B, which is also A'*X' = B'
	 * 
	 * @param B
	 *            right hand side
	 * @return solution if A is square, least squares solution otherwise.
	 */
	public Matrix solveTranspose(Matrix B) throws F8Exception {
		return transpose().solve(B.transpose());
	}

	/**
	 * Singular Value Decomposition
	 * 
	 * @return SingularValueDecomposition
	 * @see SingularValueDecomposition
	 */
	public SingularValueDecomposition svd() {
		return new SingularValueDecomposition(this);
	}

	/**
	 * Multiply a matrix by a scalar, C = s*A
	 * 
	 * @param s
	 *            scalar
	 * @return s*A
	 */
	public Matrix times(double s) {
		Matrix X = new Matrix(r, c);
		double[][] C = X.getArray();
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				C[i][j] = s * x[i][j];
			}
		}
		return X;
	}

	/**
	 * Linear algebraic matrix multiplication, A * B
	 * 
	 * @param B
	 *            another matrix
	 * @return Matrix product, A * B
	 * @exception BadArgumentTypeException
	 *                Matrix inner dimensions must agree.
	 */
	public Matrix times(Matrix B) throws BadArgumentTypeException {
		if (B.r != c) {
			throw new BadArgumentTypeException(
					"Matrix inner dimensions must agree.");
		}
		Matrix X = new Matrix(r, B.c);
		double[][] C = X.getArray();
		double[] Bcolj = new double[c];
		for (int j = 0; j < B.c; j++) {
			for (int k = 0; k < c; k++) {
				Bcolj[k] = B.x[k][j];
			}
			for (int i = 0; i < r; i++) {
				double[] Arowi = x[i];
				double s = 0;
				for (int k = 0; k < c; k++) {
					s += (Arowi[k] * Bcolj[k]);
				}
				C[i][j] = s;
			}
		}
		return X;
	}

	/**
	 * Multiply a matrix by a scalar in place, A = s*A
	 * 
	 * @param s
	 *            scalar
	 * @return replace A by s*A
	 */
	public Matrix timesEquals(double s) {
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				x[i][j] = s * x[i][j];
			}
		}
		return this;
	}

	public String toString() {
		StringBuffer s = new StringBuffer("[");

		for (int i = 0; i < x.length; i++) {
			s.append('[');
			if (c > 0) {
				for (int j = 0; j < c; j++) {
					s.append(Double.dtos(new Double(new Ereal(""+x[i][j]))));
					s.append(' ');
				}
			}

			if (i != (x.length - 1)) {
				s.append("]\n");
			} else {
				s.append("]");
			}
		}

		s.append(']');

		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret + s.toString();
	}

	/**
	 * Matrix trace.
	 * 
	 * @return sum of the diagonal elements.
	 */
	public double trace() {
		double t = 0;
		for (int i = 0; i < Math.min(r, c); i++) {
			t += x[i][i];
		}
		return t;
	}

	/**
	 * Matrix transpose.
	 * 
	 * @return A'
	 */
	public Matrix transpose() {
		Matrix X = new Matrix(c, r);
		double[][] C = X.getArray();
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				C[j][i] = x[i][j];
			}
		}
		return X;
	}

	/**
	 * Unary minus
	 * 
	 * @return -A
	 */
	public Matrix uminus() {
		Matrix X = new Matrix(r, c);
		double[][] C = X.getArray();
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				C[i][j] = -x[i][j];
			}
		}
		return X;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Matrix) {
			Matrix cobj = (Matrix) obj;
			if (r != cobj.r || c != cobj.c)
				return false;
			for (int i = 0; i < r; i++)
				for (int j = 0; j < c; j++)
					if (x[i][j] != cobj.x[i][j])
						return false;
			return true;
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		if (obj instanceof Matrix) {
			Matrix cobj = (Matrix) obj;
			if (r != cobj.r || c != cobj.c)
				return false;
			double sumTol = 0;
			for (int i = 0; i < r; i++)
				for (int j = 0; j < c; j++)
					sumTol += x[i][j] - cobj.x[i][j];
			return sumTol < tol;
		} else {
			return false;
		}
	}
	public String getLaTeX() {
		StringBuffer sb=new StringBuffer();
		sb.append("\\left( \\begin{array}{");
		for(int i=0;i<c;i++)
			sb.append("c");
		sb.append("}");
		for(int j=0;j<r;j++){
			for(int i=0;i<c;i++){
				String tmp=""+x[j][i];
				if(tmp.endsWith(".0"))
					tmp=tmp.substring(0,tmp.length()-2);
				sb.append(tmp);
				if(i!=c-1)
					sb.append(" & ");
			}
			if(j!=r-1)
				sb.append("\\\\");
		}
		sb.append("\\end{array} \\right)");
		return sb.toString();
	}


}
