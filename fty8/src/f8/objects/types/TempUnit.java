/*
 * Created on 16-sep-2003
 *
 
 
 */
package f8.objects.types;

import antlr.collections.AST;
import f8.Core;
import f8.ICalc;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.objects.Stackable;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class TempUnit extends Unit {
	/**
	 * @param v
	 * @param u
	 */
	public TempUnit(Ereal v, AST u) {
		super(v, u);
	}
	public TempUnit(){
		this(new Ereal(0),null);
	}
	/**
	 * @param def
	 */
	public TempUnit(AST def) {
		super(def);
	}

	/**
	 * @param value
	 * @param unit
	 */
	public TempUnit(String value, AST unit) {
		super(value, unit);
	}

	public boolean convert(ICalc cl, Unit to) {
		String fromS = getUnidad().getText();
		String toS = to.getUnidad().getText();
		TempUnit aux = (TempUnit) ubase();
		if (toS.equals("�C")) {
			Core.push(aux.toC());
			return false;
		}
		if (toS.equals("�R")) {
			Core.push(aux.toR());
			return false;
		}
		if (toS.equals("�F")) {
			Core.push(aux.toF());
			return false;
		}
		Core.push(aux);
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.types.Unit#ubase()
	 */
	public Unit ubase() {
		String nom = getUnidad().getText();
		AST K = mkl.fca("K", null);
		if (nom.equals("�C")) {
			return new TempUnit(getValor().plus(new Ereal("273.15")),K);
		}
		if (nom.equals("�F")) {
			return new TempUnit(
				new Ereal("255.3722222222222222").plus(
						new Ereal(5).mul(getValor()).div(new Ereal(9))
						),K);
		}
		if (nom.equals("�R")) {
			return new TempUnit(
					new Ereal(5).mul(getValor()).div(new Ereal(9)),K);
		}
		return this;
	}

	public TempUnit toC() {
		return new TempUnit(getValor().minus(new Ereal("273.15")), mkl.fca("�C", null));
	}

	public TempUnit toF() {
		return new TempUnit(
			getValor().minus(new Ereal("255.372222222222222222")).mul(new Ereal(9)).div(new Ereal(5))
			, mkl.fca("�F", null)
			);
	}

	public TempUnit toR() {
		return new TempUnit(getValor().mul(new Ereal(9)).div(new Ereal(5)),mkl.fca("�R", null));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.types.utils.Copiable#copia()
	 */
	public Stackable copia() {
		return new TempUnit(getValor(), mkl.copyAST(getUnidad()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 430;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new TempUnit(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StackableObject#getTypeName()
	 */
	public String getTypeName() {
		return "TempUnit";
	}
}
