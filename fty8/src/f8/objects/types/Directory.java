/*
 * Created on 06-ago-2003
 *
 
 
 */
package f8.objects.types;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import antlr.collections.AST;
import f8.Core;
import f8.commands.Command;
import f8.commands.Storable;
import f8.keyboard.hp48.menu.VAR;
import f8.objects.Stackable;
import f8.platform.Calc;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class Directory extends Stackable {
	public String getLaTeX() {
		return toString();
	}

	Hashtable ht = new Hashtable();

	String nombre=null;

	public Directory(){
		this(null);
	}
	
	public Directory(Directory parent, String name) {
		setName(parent, name);
	}
	public void setName(Directory parent, String name) {
		if (name.startsWith("'")) {
			name = name.substring(1);
		}

		if (name.endsWith("'")) {
			name = name.substring(0, name.length() - 1);
		}

		nombre = name;

		if (!nombre.equals("System") && !nombre.equals("")) {
			parent.put(nombre, this);
		}
	}
	public Directory(Vector childs){
		if(childs==null)
			return;
		for(int i=0;i+1<childs.size();i+=2){
			String name=((AST)childs.elementAt(i)).getText();
			Command cnt=Command.createFromAST(((AST)childs.elementAt(i+1)));
			ht.put(name, cnt);
		}
	}

	public Stackable copia() {
		return null; // Los directorios no los quiero copiar
	}

	public String getTypeName() {
		return "Directory";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 127;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new Directory(null, "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		ds.writeStringSafe(nombre);

		Vector k = new Vector();
		for(Enumeration e=ht.keys();e.hasMoreElements();)
			k.addElement(e.nextElement());
		ds.writeInt(k.size());

		for (int i = 0; i < k.size(); i++) {
			Storable item = (Storable) ht.get(k.elementAt(i));
			ds.writeStringSafe((String) k.elementAt(i));
			ds.writeInt(item.getID());
			item.saveState(ds);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
		nombre = ds.readStringSafe();

		int sz = ds.readInt();

		for (int i = 0; i < sz; i++) {
			String n = ds.readStringSafe();
			Storable obj = Command.loadFromStorage(ds);
			ht.put(n, obj);
		}
	}

	public Hashtable<String, Directory> getHT() {
		return ht;
	}

	public String getNombre() {
		return nombre;
	}

	public void put(String name, Object obj) {
		ht.put(name, obj);
	}

	public Command get(String name) {
		return (Command) ht.get(name);
	}

	public void exec(Object extern) {
		Core.changeToDir(nombre);
		if (Calc.ref.getDynamic() instanceof VAR) {
			//Core.setMenu(1, new VAR(), null, null);
		}
	}

	public String toString() {
		String ret = "DIR ";
		Vector v = new Vector();
		for(Enumeration e=ht.keys();e.hasMoreElements();)
			v.addElement(e.nextElement());
		for (int i = 0; i < v.size(); i++) {
			ret += (" " + (String) v.elementAt(i) + " " + ((Command) ht.get(v
					.elementAt(i))).toString());
		}
		return ret + " END";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#sign()
	 */
	public Stackable sign() {
		return new Double(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#zero()
	 */
	public Stackable zero() {
		return new Directory(null, null);
	}

}
