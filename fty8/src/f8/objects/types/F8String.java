package f8.objects.types;

import java.io.IOException;

import f8.Core;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.ParseException;
import f8.objects.Stackable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class F8String extends Stackable {
	String s;

	public F8String(String s) {
		this.s = s;
	}
	
	public F8String(){
		this("");
	}
	
	public Stackable copia() {
		return new F8String(new String(s));
	}

	public String getTypeName() {
		return "String";
	}
	public String getLaTeX() {
		return "\\mbox{"+s+"}";
	}


	public int getID() {
		return 48;
	}

	public Storable getInstance() {
		return new F8String(null);
	}

	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
		s = ds.readStringSafe();
	}

	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		ds.writeStringSafe(s);
	}

	public String toString() {
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret + "\"" + s + "\"";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() throws F8Exception {
		try {
			Core.enter(s);
		} catch (F8Exception e) {
			throw new ParseException(this);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof F8String) {
			return ((F8String) obj).s.equals(s);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return equals(obj);
	}

	//Retorna el contenido bruto, sin comillas ni tgas
	public String getContenido(){
		return s;
	}
	
}
