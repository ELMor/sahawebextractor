package f8.objects.types;

import java.io.IOException;

import f8.Core;
import f8.apps.solver.Solverable;
import f8.commands.Storable;
import f8.commands.alg.ComplexValue;
import f8.objects.Stackable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class Complex extends Stackable implements ComplexValue,
		Solverable {
	
	public Ereal re;

	public Ereal im;

	public Complex(){
		this(Ereal.cero,Ereal.cero);
	}
	
	public Complex(String re, String im){
		this(new Ereal(re),new Ereal(im));
	}
	
	public Complex(Ereal x, Ereal y) {
		this.re = new Ereal(x);
		this.im = new Ereal(y);
	}

	public Complex(Complex k) {
		re = k.re;
		im = k.im;
	}

	public Stackable mutate() {
		if (im.isZero())
			return new Double(re);
		return new Complex(this);
	}

	public Stackable copia() {
		return new Complex(re, im);
	}
	
	public String getTypeName() {
		return "Complex";
	}

	public int getID() {
		return 60;
	}

	public Storable getInstance() {
		return new Complex(Ereal.cero, Ereal.cero);
	}

	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
		re = new Ereal(ds.readStringSafe());
		im = new Ereal(ds.readStringSafe());
	}

	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		ds.writeStringSafe(re.toString());
		ds.writeStringSafe(im.toString());
	}

	public Ereal[] complexValue() {
		Ereal[] c = new Ereal[2];
		c[0] = new Ereal(re);
		c[1] = new Ereal(im);

		return (c);
	}

	public String toString() {
		
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		if(im.eq(0))
			return ret + Double.dtos(new Double(re));
		else
			return ret + "(" 
			           + Double.dtos(new Double(re)) 
			           + ";"
			           + Double.dtos(new Double(im)) 
					   + ")";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose()
	 */
	public boolean decompose() {
		Core.push(new Double(re));
		Core.push(new Double(im));
		return false;
	}

	// ------------------------------------------------------------------------
	// Various functions

	public Complex sum(Complex b){
		return new Complex(
				re.plus(b.re),
				im.plus(b.im)
				);
	}
	
	public Complex res(Complex b){
		return new Complex(
				re.minus(b.re),
				im.minus(b.im)
				);
	}
	
	public boolean equals(Complex b, Ereal tolerance) {
		Ereal t1 = re.minus(b.re);
		Ereal t2 = im.minus(b.im);

		return t1.sqr().plus(t2.sqr()).le(tolerance.sqr());
	}

	/**
	 * Returns the absolute value of the complex number.
	 * <p>
	 * Adapted from Numerical Recipes in C - The Art of Scientific Computing<br>
	 * ISBN 0-521-43108-5
	 */
	public Ereal abs() {
		Ereal absRe = re.abs();
		Ereal absIm = im.abs();

		if (absRe.isZero() && absIm.isZero()) {
			return Ereal.cero;
		} else if (absRe.gt(absIm)) {
			Ereal temp = absIm.div(absRe);
			return absRe.mul(Ereal.uno.plus(temp.sqr()).sqrt());
		} else {
			Ereal temp = absRe.div(absIm);
			return absIm.mul(Ereal.uno.plus(temp.sqr()).sqrt());
		}
	}

	/**
	 * Returns the square of the absolute value (re*re+im*im).
	 */
	public Ereal abs2() {
		return re.sqr().plus(im.sqr());
	}

	/**
	 * Returns the argument of this complex number (Math.atan2(re,im))
	 */
	public Ereal arg() {
		return im.atan2(re);
	}

	/**
	 * Returns the negative value of this complex number.
	 */
	public Complex neg() {
		return new Complex(re.neg(), im.neg());
	}

	/**
	 * Multiply the complex number with a double value.
	 * 
	 * @return The result of the multiplication
	 */
	public Complex mul(Ereal b) {
		return new Complex(re.mul(b), im.mul(b));
	}

	/**
	 * Multiply the complex number with another complex value.
	 * 
	 * @return The result of the multiplication
	 */
	public Complex mul(Complex b) {
		return new Complex(
				re.mul(b.re).minus(im.mul(b.im)), 
				im.mul(b.re).plus(re.mul(b.im))
				);
	}

	/**
	 * Returns the result of dividing this complex number by the parameter
	 * 
	 * <pre>
	 * b
	 * </pre>.
	 */
	public Complex div(Complex b) {
		// Adapted from Numerical Recipes in C - The Art of Scientific Computing
		// ISBN 0-521-43108-5
		Ereal resRe, resIm;
		Ereal r, den;

		if (b.re.abs().ge(b.im.abs())) {
			r = b.im.div(b.re);
			den = b.re.plus(r.mul(b.im));
			resRe = re.plus(r.mul(im)).div(den);
			resIm = im.minus(r.mul(re)).div(den);
		} else {
			r = b.re.div(b.im);
			den = b.im.plus(r.mul(b.re));
			resRe = re.mul(r).plus(im).div(den);
			resIm = im.mul(r).minus(re).div(den);
		}

		return new Complex(resRe, resIm);
	}

	/**
	 * Returns the value of this complex number raised to the power of a real
	 * component (in double precision).
	 * <p>
	 * This method considers special cases where a simpler algorithm would
	 * return "ugly" results.<br>
	 * For example when the expression (-1e40)^0.5 is evaluated without
	 * considering the special case, the argument of the base is the double
	 * number closest to pi. When sin and cos are used for the final evaluation
	 * of the result, the slight difference of the argument from pi causes a
	 * non-zero value for the real component of the result. Because the value of
	 * the base is so high, the error is magnified.Although the error is normal
	 * for floating point calculations, the consideration of commonly occuring
	 * special cases improves the accuracy and aesthetics of the results.
	 * <p>
	 * If you know a more elegant way to solve this problem, please let me know
	 * at nathanfunk@hotmail.com .
	 */
	public Complex power(Ereal exponent) {
		// z^exp = abs(z)^exp * (cos(exp*arg(z)) + i*sin(exp*arg(z)))
		Ereal scalar = abs().pow(exponent);
		boolean specialCase = false;
		int factor = 0;

		// consider special cases to avoid floating point errors
		// for power expressions such as (-1e20)^2
		if (im.isZero()&& re.isNegative()) {
			specialCase = true;
			factor = 2;
		}
		if (re.isZero() && im.gt(Ereal.cero)) {
			specialCase = true;
			factor = 1;
		}
		if (re.isZero() && im.isNegative()) {
			specialCase = true;
			factor = -1;
		}

		if (specialCase && exponent.mul(factor).eq(exponent.mul(factor).toInt())) {
			short[] cSin = { 0, 1, 0, -1 }; // sin of 0, pi/2, pi, 3pi/2
			short[] cCos = { 1, 0, -1, 0 }; // cos of 0, pi/2, pi, 3pi/2

			int x = (exponent.mul(factor).toInt()) % 4;
			if (x < 0)
				x = 4 + x;

			return new Complex(scalar.mul(cCos[x]), scalar.mul(cSin[x]));
		}

		Ereal temp = exponent.mul(arg());

		return new Complex(scalar.mul(temp.cos()), scalar.mul(temp.sin()));
	}

	/**
	 * Returns the value of this complex number raised to the power of a complex
	 * exponent
	 */
	public Complex power(Complex exponent) {
		if (exponent.im.isZero())
			return power(exponent.re);

		Ereal temp1Re = abs().log();
		Ereal temp1Im = arg();

		Ereal temp2Re = (temp1Re.mul(exponent.re)).minus(temp1Im.mul(exponent.im));
		Ereal temp2Im = (temp1Re.mul(exponent.im)).plus(temp1Im.mul(exponent.re));

		Ereal scalar = temp2Re.exp();

		return new Complex(
				scalar.mul(temp2Im.cos()), 
				scalar.mul(temp2Im.sin())
						);
	}

	/**
	 * Returns the logarithm of this complex number.
	 */
	public Complex log() {
		return new Complex(abs().log(), arg());
	}

	/**
	 * Returns the ln of this complex number
	 */
	
	public Complex ln(){
		return log().div(new Complex(Ereal.E,Ereal.cero).log());
	}
	/**
	 * Calculates the square root of this object.<br>
	 * Adapted from Numerical Recipes in C - The Art of Scientific Computing
	 * (ISBN 0-521-43108-5)
	 */
	public Complex sqrt() {
		Complex c;
		Ereal absRe, absIm, w, r;

		if (re.isZero() && im.isZero()) {
			c = new Complex(Ereal.cero, Ereal.cero);
		} else {
			absRe =re.abs();
			absIm =im.abs();

			if (absRe.ge(absIm)) {
				r = absIm.div(absRe);
				w=r.sqr().plus(Ereal.uno).sqrt().plus(Ereal.uno).div(Ereal.dos).sqrt().mul(absRe.sqrt());
			} else {
				r = absRe.div(absIm);
				w=r.sqr().plus(Ereal.uno).sqrt().plus(r).div(Ereal.dos).sqrt().mul(absIm.sqrt());
			}

			if (re.gt(Ereal.cero)) {
				c = new Complex(w, im.div(w.mul(Ereal.dos)));
			} else {
				if (im.isNegative())
					w = w.neg();
				c = new Complex(im.div(w.mul(Ereal.dos)), w);
			}
		}

		return c;
	}

	// ------------------------------------------------------------------------
	// Trigonometric functions

	/**
	 * Returns the sine of this complex number.
	 */
	public Complex sin() {
		Ereal izRe, izIm;
		Ereal temp1Re, temp1Im;
		Ereal temp2Re, temp2Im;
		Ereal scalar;

		// sin(z) = ( exp(i*z) - exp(-i*z) ) / (2*i)
		izRe = im.neg();
		izIm = re.copy();

		// first exp
		scalar = izRe.exp();
		temp1Re = scalar.mul(izIm.cos());
		temp1Im = scalar.mul(izIm.sin());

		// second exp
		scalar = izRe.neg().exp();
		temp2Re = scalar.mul(izIm.neg().cos());
		temp2Im = scalar.mul(izIm.neg().sin());

		temp1Re = temp1Re.minus(temp2Re);
		temp1Im = temp1Im.minus(temp2Im);

		return new Complex(temp1Im.div(Ereal.dos), temp1Re.neg().div(Ereal.dos));
	}

	/**
	 * Returns the cosine of this complex number.
	 */
	public Complex cos() {
		Ereal izRe, izIm;
		Ereal temp1Re, temp1Im;
		Ereal temp2Re, temp2Im;
		Ereal scalar;

		// cos(z) = ( exp(i*z) + exp(-i*z) ) / 2
		izRe = im.neg();
		izIm = re.copy();

		// first exp
		scalar = izRe.exp();
		temp1Re = scalar.mul(izIm.cos());
		temp1Im = scalar.mul(izIm.sin());

		// second exp
		scalar = izRe.neg().exp();
		temp2Re = scalar.mul(izIm.neg().cos());
		temp2Im = scalar.mul(izIm.neg().sin());

		temp1Re = temp1Re.plus(temp2Re);
		temp1Im = temp1Im.plus(temp2Im);

		return new Complex(temp1Re.div(Ereal.dos), temp1Im.div(Ereal.dos));
	}

	/**
	 * Returns the tangent of this complex number.
	 */
	public Complex tan() {
		return sin().div(cos());
	}

	// ------------------------------------------------------------------------
	// Inverse trigonometric functions

	public Complex asin() {
		Complex result;
		Ereal tempRe, tempIm;

		// asin(z) = -i * log(i*z + sqrt(1 - z*z))

		tempRe = Ereal.uno.minus((re.sqr()).minus(im.sqr()));
		tempIm = Ereal.cero.minus(re.mul(im).plus(im.mul(re)));

		result = new Complex(tempRe, tempIm);
		result = result.sqrt();

		result.re = result.re.minus(im);
		result.im = result.im.plus(re);

		tempRe = result.abs().log();
		tempIm = result.arg();

		result.re = tempIm;
		result.im = tempRe.neg();

		return result;
	}

	public Complex acos() {
		Complex result;
		Ereal tempRe, tempIm;

		// acos(z) = -i * log( z + i * sqrt(1 - z*z) )

		tempRe = Ereal.uno.minus((re.sqr()).minus(im.sqr()));
		tempIm = Ereal.cero.minus((re.mul(im)).plus(im.mul(re)));

		result = new Complex(tempRe, tempIm);
		result = result.sqrt();

		tempRe = result.im.neg();
		tempIm = result.re;

		result.re = re.plus(tempRe);
		result.im = im.plus(tempIm);

		tempRe = result.abs().log();
		tempIm = result.arg();

		result.re = tempIm;
		result.im = tempRe.neg();

		return result;
	}

	public Complex atan() {
		// atan(z) = -i/2 * log((i-z)/(i+z))

		Ereal tempRe, tempIm;
		Complex result = new Complex(re.neg(), Ereal.uno.minus(im));

		tempRe = re;
		tempIm = Ereal.uno.plus(im);

		result = result.div(new Complex(tempRe, tempIm));

		tempRe = result.abs().log();
		tempIm = result.arg();

		result.re = tempIm.div(Ereal.dos);
		result.im = tempRe.neg().div(Ereal.dos);

		return result;
	}

	// ------------------------------------------------------------------------
	// Hyperbolic trigonometric functions

	public Complex sinh() {
		Ereal scalar;
		Ereal temp1Re, temp1Im;
		Ereal temp2Re, temp2Im;
		// sinh(z) = ( exp(z) - exp(-z) ) / 2

		// first exp
		scalar = re.exp();
		temp1Re = scalar.mul(im.cos());
		temp1Im = scalar.mul(im.sin());

		// second exp
		scalar = re.neg().exp();
		temp2Re = scalar.mul(im.neg().cos());
		temp2Im = scalar.mul(im.neg().sin());

		temp1Re = temp1Re.minus(temp2Re);
		temp1Im = temp1Im.minus(temp2Im);

		return new Complex(temp1Re.div(Ereal.dos), temp1Im.div(Ereal.dos));
	}

	public Complex cosh() {
		Ereal scalar;
		Ereal temp1Re, temp1Im;
		Ereal temp2Re, temp2Im;
		// cosh(z) = ( exp(z) + exp(-z) ) / 2

		// first exp
		scalar = re.exp();
		temp1Re = scalar.mul(im.cos());
		temp1Im = scalar.mul(im.sin());

		// second exp
		scalar = re.neg().exp();
		temp2Re = scalar.mul(im.neg().cos());
		temp2Im = scalar.mul(im.neg().sin());

		temp1Re = temp1Re.plus(temp2Re);
		temp1Im = temp1Im.plus(temp2Im);

		return new Complex(temp1Re.div(Ereal.dos), temp1Im.div(Ereal.dos));
	}

	public Complex tanh() {
		return sinh().div(cosh());
	}

	// ------------------------------------------------------------------------
	// Inverse hyperbolic trigonometric functions

	public Complex asinh() {
		Complex result;
		// asinh(z) = log(z + sqrt(z*z + 1))

		result = new Complex(
				re.sqr().minus(im.sqr()).plus(Ereal.uno), 
				re.mul(im).plus(im.mul(re))
				);

		result = result.sqrt();

		result=result.sum(this);

		Ereal temp = result.arg();
		result.re = result.abs().log();
		result.im = temp;

		return result;
	}

	public Complex acosh() {
		Complex result;

		// acosh(z) = log(z + sqrt(z*z - 1))

		result = new Complex(
				re.sqr().minus(im.sqr()).minus(Ereal.uno), 
				re.mul(im).plus(im.mul(re))
						);

		result = result.sqrt();

		result=result.sum(this);

		Ereal temp = result.arg();
		result.re = result.abs().log();
		result.im = temp;

		return result;
	}

	public Complex atanh() {
		// atanh(z) = 1/2 * log( (1+z)/(1-z) )

		Ereal tempRe, tempIm;
		Complex result = new Complex(re.plus(Ereal.uno), im);

		tempRe = Ereal.uno.minus(re);
		tempIm = im.neg();

		result = result.div(new Complex(tempRe, tempIm));

		tempRe = result.abs().log();
		tempIm = result.arg();

		result.re = tempRe.div(Ereal.dos);
		result.im = tempIm.div(Ereal.dos);

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Complex) {
			Complex cobj = (Complex) obj;
			if (re.eq(cobj.re) && im.eq(cobj.im))
				return true;
		} else if (obj instanceof Double) {
			Double cobj = (Double) obj;
			if (im.isZero() && re.eq(cobj.doubleValue()))
				return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Solverable obj, Ereal tol) {
		if (obj instanceof Complex) {
			Complex cobj = (Complex) obj;
			if (re.minus(cobj.re).abs().plus(im.minus(cobj.im).abs()).le(tol))
				return true;
			return false;

		} else if (obj instanceof Double) {
			Double cobj = (Double) obj;
			if (re.minus(cobj.doubleValue()).abs().plus(im.abs()).le(tol))
				return true;
			return false;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#sign()
	 */
	public int sign() {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#zero()
	 */
	public Solverable zero() {
		return zeroComplex;
	}

	public static Complex zeroComplex = new Complex(Ereal.cero, Ereal.cero);

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#amplia(double)
	 */
	public Solverable amplia(Ereal pct) {
		Ereal r = re.copy(), i = im.copy();
		if (r.isZero())
			r = r.plus(pct);
		if (i.isZero())
			i = i.plus(pct);
		r = r.plus(r.mul(pct));
		i = i.plus(i.mul(pct));
		return new Complex(r, i);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#compare(f8.symbolic.Solverable)
	 */
	public Ereal compare(Solverable obj) {
		Complex co=null;
		if(obj instanceof Complex)
			co=(Complex)obj;
		if(obj instanceof Double)
			co=new Complex(((Double)obj).doubleValue(),Ereal.cero);
		if(obj==null)
			return Ereal.cero;
		return res(co).abs();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#mean(f8.symbolic.Solverable,
	 *      f8.symbolic.Solverable)
	 */
	public Solverable mean(Solverable o1, Solverable o2) {
		if (o1 instanceof Complex && o2 instanceof Complex) {
			Complex c1=(Complex) o1;
			Complex c2=(Complex) o2;
			return c1.sum(c2).div(new Complex(Ereal.dos,Ereal.cero));
		}else if(o1 instanceof Complex && o2 instanceof Double){
			Complex c1=(Complex) o1;
			Complex c2=new Complex(((Double)o2).doubleValue(),Ereal.cero);
			return mean(c1,c2);
		}else if(o1 instanceof Double && o2 instanceof Complex){
			Complex c1=new Complex(((Double)o1).doubleValue(),Ereal.cero);
			Complex c2=(Complex) o2;
			return mean(c1,c2);
		}else if(o1 instanceof Double && o2 instanceof Double){
			Double d1=(Double)o1;
			return d1.mean(o1, o2);
		}
		return new Complex(Ereal.cero,Ereal.cero);
	}

	public String getLaTeX(){
		return "\\left("+re+","+im+"\\right)";
	}
	
}
