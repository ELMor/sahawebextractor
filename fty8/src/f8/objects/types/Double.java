package f8.objects.types;

import java.io.IOException;

import antlr.collections.AST;
import f8.apps.solver.Solverable;
import f8.commands.Storable;
import f8.commands.alg.ComplexValue;
import f8.commands.alg.DoubleValue;
import f8.commands.math.mkl;
import f8.objects.Stackable;
import f8.platform.Calc;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public class Double extends Stackable implements DoubleValue,
		ComplexValue, Solverable {
	private Ereal x;

	public Double(Ereal x) {
		this.setX(new Ereal(x));
	}
	
	public Double(int x){
		this.setX(new Ereal(x));
	}

	public Double(){
		
	}
	
	public Ereal[] complexValue() {
		Ereal[] y = new Ereal[2];
		y[0] = new Ereal(doubleValue());
		y[1] = new Ereal(0);
		return y;
	}

	public Stackable copia() {
		return new Double(doubleValue());
	}
	public String getLaTeX() {
		return toString();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StackableObject#getAST()
	 */
	public AST getAST() {
		return mkl.num(doubleValue());
	}

	public int getID() {
		return 73;
	}

	public Storable getInstance() {
		return new Double(0);
	}

	public String getTypeName() {
		return "Double";
	}

	public int intValue() {
		return doubleValue().val.toInteger();
	}

	public void loadState(DIS ds) throws IOException{
		super.loadState(ds);
		setX(new Ereal(ds.readStringSafe()));
	}

	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		ds.writeStringSafe(""+doubleValue());
	}

	public String toString() {
		return dtos(this);
	}

	public static String dtos(Double d) {
		
		String ret = d.getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret+d.doubleValue().val.toString(Calc.ref.getNumberFormat());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Double) {
			Real val2=((Double) obj).doubleValue().val;
			return (doubleValue().val.equalTo(val2));
		} else if (obj instanceof Complex) {
			return ((Complex) obj).equals(this);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Solverable obj, Ereal tol) {
		if (obj instanceof Double) {
			Double dobj=(Double)obj;
			Ereal diff=doubleValue().minus(dobj.doubleValue());
			Ereal absDiff=diff.abs();
			boolean tolres=absDiff.lt(tol);
			return  tolres;
		} else if (obj instanceof Complex) {
			return ((Complex) obj).tolerance(this, tol);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#sign()
	 */
	public int sign() {
		return (doubleValue().isNegative()) ? (-1) : (doubleValue().isZero() ? 0 : 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#zero()
	 */
	public Solverable zero() {
		return zeroDouble;
	}

	public static Double zeroDouble = new Double(0);

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#amplia(double)
	 */
	public Solverable amplia(Ereal pct) {
		Ereal k = new Ereal(doubleValue());
		if (k.isZero())
			return new Double(pct);
		if (k.isNegative()){
			k=k.mul(new Ereal(1).minus(pct));
		}else{
			k=k.mul(new Ereal(1).plus(pct));
		}
		return new Double(k);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#compare(f8.symbolic.Solverable)
	 */
	public Ereal compare(Solverable obj) {
		if (obj instanceof Double) {
			return doubleValue().minus(((Double) obj).doubleValue());
		}
		return new Ereal(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#mean(f8.symbolic.Solverable,
	 *      f8.symbolic.Solverable)
	 */
	public Solverable mean(Solverable o1, Solverable o2) {
		if (o1 instanceof Double && o2 instanceof Double) {
			Double d1=(Double)o1;
			Double d2=(Double)o2;
			return new Double(d1.doubleValue().plus(d2.doubleValue()).div(new Ereal(2)));
		} else
			return new Double(0);
	}

	public void setX(Ereal x) {
		this.x = x;
	}

	public Ereal doubleValue(){
		return x;
	}
	
}
