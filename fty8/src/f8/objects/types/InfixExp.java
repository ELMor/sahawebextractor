/*
 * Created on 14-ago-2003
 *
 
 
 */
package f8.objects.types;

import java.io.IOException;
import java.util.Vector;

import antlr.CommonAST;
import antlr.Token;
import antlr.collections.AST;
import f8.commands.Command;
import f8.commands.CommandDict;
import f8.commands.Storable;
import f8.commands.alg.CommandSequence;
import f8.commands.alg.Ejecutable;
import f8.commands.math.mkl;
import f8.commands.prog.debug.Debuggable;
import f8.commands.symb.Sigma;
import f8.exceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.kernel.yacc.OpPrior;
import f8.objects.Stackable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class InfixExp extends Stackable implements
		ObjectParserTokenTypes, Debuggable, Ejecutable {

	/**
	 * Se emplea cuando se desea mantener el calculo completo, pero
	 * es necesario disponer de un valor de aproximacion.
	 */
	public Stackable aprox=null;
	
	/**
	 * Construye una cadena "( arg1,arg2,... )"
	 * 
	 * @param arg
	 *            Lista de argumentos
	 * @return cadena de caracteres
	 */
	public static String astArgs(AST arg) {
		String k = "(";

		while (arg != null) {
			k += forma(arg);
			arg = arg.getNextSibling();

			if (arg != null) {
				k += ",";
			}
		}

		return k + ")";
	}

	/**
	 * Retorna un generic array cuya ejecucion en la pila supone la evaluacion
	 * de la expresion infija
	 * 
	 * @param exp
	 *            Expresion a evaluar
	 * @return GenericArray.exec() evalua la expresion infija representada por
	 *         el arbol exp
	 */
	public static CommandSequence decode(AST exp) {
		// Aqui se van a guardar los StkObs
		Vector vret = new Vector();

		// Primero se a�aden al GArray los hijos, y luego el padre.
		for (AST child = exp.getFirstChild(); 
		         child != null; 
		         child = child.getNextSibling()) {
			if (child.getType() != UNIT) {
				vret.addElement(decode(child));
			} else {
				vret.addElement(Command.createFromAST(child));
			}
		}
		if (exp.getType() == MULT || exp.getType() == PLUS) {
			int n = exp.getNumberOfChildren();
			Command k = Command.createFromAST(exp);
			for (int i = 0; i < n - 1; i++)
				vret.addElement(k);
		} else {
			vret.addElement(Command.createFromAST(exp));
		}
		return new CommandSequence(vret);
	}

	/**
	 * Construye la representacion textual de la expresion nodo
	 * 
	 * @param nodo
	 *            Expresion
	 * @return Cadena
	 */
	public static String forma(AST nodo) {
		int tipo = nodo.getType();
		AST child = nodo.getFirstChild();

		switch (tipo) {
		case COMPLEX:
			return "(" + forma(child) + "+" + forma(child.getNextSibling())
					+ "*i)";
		case LITERAL:
			nodo = nodo.getFirstChild();
			child = nodo.getFirstChild();
		case ID:
			if (child == null) {
				return nodo.getText();
			} else {
				return nodo.getText() + astArgs(child);
			}
		case NUMBER:
			return new Double(new Ereal(nodo.getText())).toString();
		case UNIT:
			return child.getText() + "_" + forma(child.getNextSibling());
		case NEG:
			return "-" + forma(child);
		case INV:
			return "INV(" + forma(child) + ")";
		case DERIV:
			String derFu = forma(child);
			child = child.getNextSibling();
			String derId = forma(child);
			return "\u0088" + derId + "(" + derFu + ")";
		case INTEG:
			String intDo = forma(child);
			child = child.getNextSibling();
			String intUp = forma(child);
			child = child.getNextSibling();
			String intFu = forma(child);
			child = child.getNextSibling();
			String intId = forma(child);
			return "\u0084(" + intDo + "," + intUp + "," + intFu + "," + intId
					+ ")";
		case SIGMA:
			return Sigma.forma(nodo);
		default:
			return normalOp(nodo);
		}
	}

	public static String isLiteral(Command a) {
		if (a instanceof InfixExp) {
			InfixExp iea = (InfixExp) a;
			if (iea.getAST().getType() == ID) {
				return iea.getAST().getText();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	//Tratamiento especial de las operaciones infijas
	private static String normalOp(AST opp) {
		AST child;
		int pprio = OpPrior.val[opp.getType()];
		String ret = "";

		for (child = opp.getFirstChild(); 
		     child != null; 
		     child = child.getNextSibling()) {
			
			if (child != opp.getFirstChild()) {
				ret += opp.getText();
			}

			int cprio = OpPrior.val[child.getType()];

			if (   (pprio > cprio)
				|| ((child != opp.getFirstChild()) && (pprio == cprio) && ((cprio == 64) || (cprio == 32)))) {
				ret += ("(" + forma(child) + ")");
			} else {
				ret += forma(child);
			}
		}

		return ret;
	}

	AST expression = null;

	CommandSequence run = null;

	/**
	 * Crea nua expresion infija. El arbol es exp1
	 * 
	 * @param exp1
	 */
	public InfixExp(AST exp1) {
		AST k;
		if (exp1 == null) {
			return;
		}
		if (exp1.getType() != INFIX) {
			k = new CommonAST(new Token(INFIX));
			k.addChild(exp1);
		} else {
			k = exp1;
		}
		expression = k;
		aprox=null;
	}

	public InfixExp(){
		this(null);
	}
	
	public Stackable copia() {
		return new InfixExp(mkl.copyAST(expression));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() throws F8Exception {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		return run.decompose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof InfixExp) {
			return mkl.opEquals(expression, ((InfixExp) obj).expression);
		} else {
			return false;
		}
	}

	public void exec(Object extern) throws F8Exception {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		run.exec(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#execDebug()
	 */
	public boolean execDebug(boolean si) throws F8Exception {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		return run.execDebug(si);
	}

	public AST getAST() {
		return expression.getFirstChild();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Ejecutable#getEjecutable()
	 */
	public CommandSequence getEjecutable() {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		return run;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getID()
	 */
	public int getID() {
		return 119;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new InfixExp(null);
	}

	public String getTypeName() {
		return "InfixExp";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#isAnyMore()
	 */
	public boolean isAnyMore() {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		return run.isAnyMore();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#loadState(f8.pfr.io.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
		AST[] exp = new CommonAST[1];
		Command.loadState(ds, exp);
		expression = exp[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#nextCommand()
	 */
	public String nextCommand() {
		if (run == null) {
			run = decode(expression.getFirstChild());
		}
		return run.nextCommand();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#saveState(f8.pfr.io.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		Command.saveState(ds, expression);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return equals(obj);
	}

	public String toString() {
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		if(expression==null)
			return "''";
		AST fc = expression.getFirstChild();
		String eExp = forma(fc);
		String vAtv = "";
		fc = fc.getNextSibling();
		if (fc != null) {
			AST idt = fc.getFirstChild();
			AST val = idt.getNextSibling();
			vAtv = "|(" + idt.toString() + "=" + val.toString() + ")";
		}
		return ret + "'" + eExp + vAtv + "'";
	}

	public String getLaTeX(){
		return getLaTex(expression.getFirstChild());
	}
	
	public static String getLaTex(AST nodo){
		if(nodo==null)
			return "null";
		int tipo = nodo.getType();
		
		AST child = nodo.getFirstChild();
				
		switch (tipo) {
		case COMPLEX:
			return getLaTex(child) + "+" + getLaTex(child.getNextSibling())+ "*{i}";
		case LITERAL:
			nodo = nodo.getFirstChild();
			child = nodo.getFirstChild();
			if (child == null) {
				return "{"+nodo.getText()+"}";
			} else {
				//Function call
				return "{"+nodo.getText()+"}" + astArgsLaTeX(child);
			}
		case ID:
			Command cmd=CommandDict.getInstance(nodo.getText());
			if(cmd!=null){
				return cmd.getLaTeX(child); 
			}else if (child == null) {
				return "{"+nodo.getText()+"}";
			} else {
				//Llamada a funcion. Si se eleva a una potencia, lo ideal seria
				//que se mostrara cos2(x) mejor que (cos(x))2
				return "{"+nodo.getText()+"}" + astArgsLaTeX(child);
			}
		case NUMBER:
			return "{"+new Double(new Ereal(nodo.getText())).toString()+"}";
		case UNIT:
			return "{"+child.getText()+"}" +  getLaTex(child.getNextSibling());
		case NEG:
			return "-" + getLaTex(child);
		case INV:
			return "\\frac{1}" + getLaTex(child);
		case DERIV:
			String derFu = getLaTex(child);
			child = child.getNextSibling();
			String derId = getLaTex(child);
			return "\\frac\\partial{\\partial "+derId+"}"+derFu; 
		case INTEG:
			String intDo = getLaTex(child);
			child = child.getNextSibling();
			String intUp = getLaTex(child);
			child = child.getNextSibling();
			String intFu = getLaTex(child);
			child = child.getNextSibling();
			String intId = getLaTex(child);
			return "\\int_{"+intDo+"}^{"+intUp+"}"+intFu+"\\partial "+intId;
		case SIGMA:
			child = nodo.getFirstChild();
			String sigId = InfixExp.forma(child);
			child = child.getNextSibling();
			String sigDo = InfixExp.forma(child);
			child = child.getNextSibling();
			String sigUp = InfixExp.forma(child);
			child = child.getNextSibling();
			String sigFu = InfixExp.forma(child);
			return "\\sum_{"+sigId+"="+sigDo+"}^"+sigUp+"{"+sigFu+"}";
		default:
			int antPriori = OpPrior.val[nodo.getType()];
			String ret = "";
			for (;child != null;child = child.getNextSibling()) {
				boolean noFirst=child != nodo.getFirstChild();
				boolean processedYet=false;
				if (noFirst) {
					switch(nodo.getType()){
					case MULT:  ret+="\\cdot"; break;
					case DIV:   ret="\\frac{"+ret+"}"; break;
					case PLUS:  ret+="+"; break;
					case MINUS:	ret+="-"; break;
					case CARET:
						if(child.getType()==DIV){
							AST num=child.getFirstChild();
							AST den=num.getNextSibling();
							ret="\\sqrt["+getLaTex(den)+"]"+ret;
							if(!mkl.opEquals(num, mkl.uno)){
								ret+="^"+getLaTex(num);
							}
							processedYet=true;
						}else{
							ret+="^";
						}
					break;
					}
				}else{
					//es el primer hijo. Comprobamos que es una llamada a funcion
					if(child.getType()==ID && 
					   child.getFirstChild()!=null &&
					   nodo.getType()==CARET){
						ret="{"+child.getText()+"}^"+
						        getLaTex(child.getNextSibling())+
						        astArgsLaTeX(child.getFirstChild());
						processedYet=true;
						child=child.getNextSibling();
					}
				}
				int sigPrio = OpPrior.val[child.getType()];
				if(!processedYet){
					if (   (antPriori > sigPrio)
						|| (noFirst && 
						   (antPriori == sigPrio) && 
						    ((sigPrio == 64) || (sigPrio == 32)))) {
						ret += ("{\\left(" + getLaTex(child) + "\\right)}");
					} else {
						ret += "{"+getLaTex(child)+"}";
					}
				}
			}
			return ret;
		}
	}

	public static String astArgsLaTeX(AST arg) {
		String k = " \\left(";

		while (arg != null) {
			k += getLaTex(arg);
			arg = arg.getNextSibling();

			if (arg != null) {
				k += ",";
			}
		}

		return k + "\\right)";
	}
}
