package f8.objects.constants;

import java.io.IOException;

import f8.commands.Storable;
import f8.commands.alg.Const;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class PI extends Const {
	public PI() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 77;
	}

	public Storable getInstance() {
		return new PI();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public Stackable value() {
		return new Double(Ereal.PI);
	}

	public String toString() {
		return ("\u0087");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() {
		return false;
	}

}
