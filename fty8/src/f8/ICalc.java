/*
 * Created on 29-jul-2003
 *
 
 
 */
package f8;

import f8.keyboard.Menu;
import f8.keyboard.TeclaVirtual;
import f8.keyboard.hp48.Keyboard;
import f8.platform.Plot;
import f8.platform.QueuedAction;

/**
 * @author elinares
 * 
 * 
 * 
 */
public interface ICalc {

	// Constantes de settings
	public final int ALG_MOD = 0;
	
	public final int ANG_MOD = 1;
	public final int DEG = 1;
	public final int GRAD = 3;

	public final int BEEP = 2;
	
	public final int CLOCK = 3;
	
	public final int CNCT = 4;

	public final int DOU_MOD = 5;
	public final int STD = 1;
	public final int FIX = 2;
	public final int SCI = 3;
	public final int ENG = 4;
	
	public final int DOU_PRE = 6;
	public final int DEC = 1;
	public final int HEX = 2;
	public final int OCT = 3;
	public final int BIN = 4;
	
	public final int INT_MOD = 7;

	public final int LAS_ARG = 8;
	
	public final int LAS_CMD = 9;
	
	public final int LAS_STA = 10;
	
	public final int MULTILI = 11;
	
	public final int MOD_3D = 12;
	
	public final int PER_MOD = 13;
	public final int COMMA = 1;
	public final int PERIOD = 2;

	public final int PRG_MOD = 14;
	public final int XYZ = 1;
	public final int RAD = 2;
	public final int RAA = 3;

	public final int RAZ = 2;
	public final int SYM_MOD = 15;
	
	public final int ICAL_BUTT=16;
	
	public final int EXACT_MODE=17;
	
	//Constantes de eventos de usuario
	public final int KEYCURUP=1;
	public final int KEYCURDOWN=2;
	public final int KEYCURLEFT=3;
	public final int KEYCURRIGHT=4;
	public final int KEYBACK=5;
	public final int KEYDEL=6;
	public final int GESTUP=7;
	public final int GESTDOWN=8;
	public final int GESTLEFT=9;
	public final int GESTUPRIGHT=10;
	/**
	 * Devuelve el estado de las techas shift
	 * 
	 * @return 0 no hay shift, 1 Left shift, 2 Right shift
	 */
	public int getShiftMode();

	/**
	 * Coloca el shift
	 * 
	 * @param smode
	 *            0 no hay shift, 1 Left shift, 2 Right shift
	 */
	public void setShiftMode(int smode);

	/**
	 * Devuelve el estado de edicion en modo programacion
	 * 
	 * @return verdadero o falso
	 */
	public boolean getPgmMode();

	public void setPgmMode(boolean em);

	/**
	 * Obtiene una cadena que representa el status label
	 * @return
	 */
	public String getStatusLabel(int labelNumber);
	
	/**
	 * Procesamiento de la pulsacion de una tecla
	 * @param i
	 *            Numero de la tecla, comenzando por 0 arriba a la izquierda.
	 * @param isUser
	 * 			Si es un evento de usuario
	 */
	public void key(QueuedAction action);

	/**
	 * Devuelve las cadenas de texto de la botonera correspondiente
	 * 
	 * @return array de String con los textos de los botones
	 */
	public TeclaVirtual[] getUpArray();

	public boolean[] getUpIsDirectory();

	public boolean[] getUpIsOn();

	/**
	 * Obtiene el diccionario de usuario
	 * 
	 * @return diccionario de usuario
	 */

	// public Hashtable getUserDict();
	public Menu getDynamic();
	public void setDynamic(Menu m);
	public boolean isDynSelected();
	public int getDynIndex();
	
	public void setDynMenu(Menu m);

	public void nxtDynPage();

	public void preDynPage();

	public boolean[] getUpVarSolver();

	public void press(TeclaVirtual vk);

	public abstract int getDynPage();

	public abstract void setUpMenuPage(int i);

	/**
	 * Coloca str al final de la cadena de edicion
	 * 
	 * @param str
	 *            Cadena a a�adir
	 */
	public abstract void emit(String str, int backSpaces);

	public boolean flush();


	/**
	 * contenido del campo de edicion
	 * 
	 * @return El contenido del campo de edicion
	 */
	public abstract String getEditText();
	public abstract void setEditText(String txt);
	public abstract boolean isEditing();
	public abstract int getEditTextInserPos();
	public abstract void setEditTextInserPos(int i);


	/**
	 * Obtener el control LCD para dibujar graficos y plots
	 * 
	 * @return
	 */
	public Plot getLCD();

	/**
	 * Realiza un loggin
	 * 
	 * @param txt
	 *            Texto a logear
	 */
	public abstract void log(String txt);

	/**
	 * Muestra un popup
	 * 
	 * @param errNumber
	 *            Codigo de ICalcErr cuyo texto muestra
	 */
	public abstract void output(int errNumber);

	/**
	 * Muestra un popup
	 * 
	 * @param msg
	 *            Mensaje del popup
	 */
	public abstract void output(String msg);

	public void setInitialEdit(String s);

	/**
	 * Returna las teclas que componen el teclado en 3 grupos de 3 subgrupos de 12 teclas cada uno
	 * de forma que las primeras 12 entradas son el modo normal del menu 0, las 12 siguientes son
	 * el modo left-shift del menu 0, las 12 siguientes el modo right shift del menu 0. Luego
	 * vienen las 36 teclas del menu 1 y las 36 teclas de menu 2
	 * @return Menu con las teclas 
	 */
	public Keyboard getKeyb();
	public int getKeybPage();
}
