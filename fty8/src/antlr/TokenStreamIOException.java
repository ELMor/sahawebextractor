package antlr;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: TokenStreamIOException.java,v 1.5 2008/04/15 17:19:40 elinares690715 Exp $
 */

/**
 * Wraps an IOException in a TokenStreamException
 */
public class TokenStreamIOException extends TokenStreamException {
	public Exception io;

	/**
	 * TokenStreamIOException constructor comment.
	 * 
	 * @param s
	 *            java.lang.String
	 */
	public TokenStreamIOException(Exception io) {
		super(io.getMessage());
		this.io = io;
	}
}
