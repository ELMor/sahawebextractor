package f8.platform.glyph;

import java.awt.Graphics;

import antlr.collections.AST;
/**
 * Coloca a los hijos centradamente por columnas.
 * @author elinares
 *
 */
public class Glyph_SqareObjects extends Glyph {
	Glyph childs[][]=null;
	
	int altu=0,anch=0,base=0;
	private final int sq=3;
	
	public int getBase() {
		return getHeight()/2;
	}

	public int getHeight(){
		return altu+2*sq;
	}
	
	public int getWidth(){
		return anch+2*sq;
	}
	
	public Glyph_SqareObjects(Glyph cpad, AST def, int sz){
		super(cpad);
		int d1=def.getNumberOfChildren();
		int d2=def.getFirstChild().getNumberOfChildren();
		childs=new Glyph[d1][d2];
		int i=0,j=0;
		for(AST c=def.getFirstChild();c!=null;c=c.getNextSibling()){
			j=0;
			for(AST d=c.getFirstChild();d!=null;d=d.getNextSibling()){
				childs[i][j]=new Glyph_AST(this,d,sz-4);
				j++;
			}
			i++;
		}
		//Fijamos posiciones
		int maxRowHeight[]=new int[d1]; //Maxima altura de cada fila
		int maxRowBase[]=new int[d1];
		int maxColWidth[]=new int[d2]; //Maxima anchura de cada columna
		for(i=0;i<d1;i++){
			for(j=0;j<d2;j++){
				int ch=childs[i][j].getHeight();
				int cw=childs[i][j].getWidth();
				int cb=childs[i][j].getBase();
				if(maxRowBase[i]<cb)
					maxRowBase[i]=cb;
				if(maxRowHeight[i]<ch)
					maxRowHeight[i]=ch;
				if(maxColWidth[j]<cw)
					maxColWidth[j]=cw;
			}
		}
		int x=0,y=0;
		for(i=0;i<d1;i++){
			for(j=0;j<d2;j++){
				childs[i][j].setX( (j*sq)+ x+(maxColWidth[j]-childs[i][j].getWidth())/2);
				childs[i][j].setY( (i*sq)+ y+ maxRowBase[i]-childs[i][j].getBase());
				x+=maxColWidth[j];
			}
			x=0;
			y+=maxRowHeight[i];
		}
		for(i=0;i<d1;i++)
			altu+=maxRowHeight[i];
		for(j=0;j<d2;j++)
			anch+=maxColWidth[j];
	}

	public void dibuja(Graphics g) {
		for(int i=0;i<childs.length;i++)
			for(int j=0;j<childs[0].length;j++)
				childs[i][j].dibuja(g);
	}
	
}
