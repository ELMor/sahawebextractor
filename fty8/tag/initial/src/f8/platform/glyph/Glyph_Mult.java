package f8.platform.glyph;

import java.awt.Graphics;

public class Glyph_Mult extends Glyph {
	
	public int getBase() {
		return 3;
	}

	public Glyph_Mult(Glyph cpad){
		super(cpad);
	}

	public void dibuja(Graphics g) {
		g.setColor(fillColor);
		g.fillArc(realX()+1, realY()+2, 3, 3, 0, 360);
	}

	public int getHeight() {
		return 6;
	}

	public int getWidth() {
		return 5;
	}

	
}
