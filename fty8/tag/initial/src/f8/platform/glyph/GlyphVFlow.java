package f8.platform.glyph;

import java.awt.Graphics;
/**
 * Representa una pila vertical de Glyphs
 * Se suponen centrados en Horizontal y que
 * uno de los Glyphs es el centro de la serie
 * Vertical.
 * @author elinares
 *
 */
public class GlyphVFlow extends GlyphFlow {
	public GlyphVFlow(Glyph cpad){
		super(cpad);
	}
	
	public int getBase() {
		int ret=0;
		for(int i=0;i<elems.size();i++){
			Glyph elem=((Glyph)elems.elementAt(i));
			if(elem.isTheCenter()){
				ret+=elem.getBase();
				break;
			}else{
				ret+=elem.getHeight();
			}
		}
		return ret;
	}
	

	public int getHeight() {
		int ret=0;
		for(int i=0;i<elems.size();i++){
			ret+=((Glyph)elems.elementAt(i)).getHeight();
		}
		return ret;
	}

	public int getWidth() {
		int ret=0;
		for(int i=0;i<elems.size();i++){
			ret=Math.max(ret,((Glyph)elems.elementAt(i)).getWidth());
		}
		return ret;
	}

	public void dibuja(Graphics g) {
		int w=getWidth(),cy=0;
		for(int i=0;i<elems.size();i++){
			Glyph elem=((Glyph)elems.elementAt(i));
			elem.setX((w-elem.getWidth())/2);
			elem.setY(cy);
			cy+=elem.getHeight();
		}
		super.dibuja(g);
	}


}
