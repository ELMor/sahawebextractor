package f8.platform.glyph;

import antlr.collections.AST;
import f8.commands.math.mkl;

/**
 * Dibuja una sumatoria. Se trata de una composición:
 *  e
 * Sum fun
 * v=i
 * 
 * En el que e,Sum,v=i comonoen un VFlow que es el primer componente de este HFlow
 * @author elinares
 *
 */
public class Glyph_Sigma extends GlyphHFlow {

	public Glyph_Sigma(Glyph cpad, AST f, AST v, AST i, AST e, int sz){
		super(cpad);
		GlyphVFlow lft=new GlyphVFlow(this);
		Glyph end=new Glyph_AST(this,e,sz);
		lft.addHijo(end);
		Glyph fun=new Glyph_AST(this,f,sz);
		int h=fun.getHeight();
		int hi=Math.max(32,h);
		Glyph sum=new Glyph_Image(this,"Sumatoria",hi,hi,(hi-h)/2+fun.getBase());
		sum.setTheCenter(true);
		lft.addHijo(sum);
		Glyph ini=new Glyph_AST(this,mkl.equ(v, i),sz);
		lft.addHijo(ini);
		addHijo(lft);
		addHijo(fun);
	}
	
}
