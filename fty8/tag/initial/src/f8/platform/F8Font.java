package f8.platform;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.io.File;

public class F8Font {	
	private Font fuente=null;
	private FontMetrics fm=null;
	private boolean rev=false;
	private int tall=-1;

	public F8Font(Graphics g, int size, boolean reversed) {
		try {
			Font aux=Font.createFont(Font.TRUETYPE_FONT, new File("bin/HPGOS2-ELM.ttf"));
			fuente=aux.deriveFont((float)size);
			fm=g.getFontMetrics(fuente);
			tall=fm.getHeight();
			rev=reversed;
		} catch (Exception e) {
			throw new RuntimeException("No puedo cargar la fuente");
		}
	}
	
	public int getFontHeight(){
		return fm.getHeight();
	}
	
	public int stringWidth(String s){
		return fm.stringWidth(s);
	}
	
	public _Viewer getViewer(String s){
		return new _Viewer(s);
	}
	
	public class _Viewer {
		String texto;
		public _Viewer(String s) {
			texto=s;
		}
		public int getWidth(){
			return stringWidth(texto);
		}
		public int getHeight(){
			return getFontHeight();
		}
		public void paint(int x, int y, Graphics g){
			g.setFont(fuente);
			if(rev)
				g.setXORMode(Color.BLACK);
			else
				g.setXORMode(Color.WHITE);
			g.drawString(texto, x, y+tall);
			g.setPaintMode();
		}
	}
}
