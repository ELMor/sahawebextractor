package f8.platform.io;

import java.io.DataOutputStream;
import java.io.IOException;

public class DOS extends DataOutputStream {
	public DOS(DataOutputStream out){
		super(out);
	}
	public void writeStringSafe(String s) throws IOException {
		if (s != null) {
			writeShort(s.length());
			write(s.getBytes("ISO-8859-1"));
		} else {
			writeShort(-1);
		}
	}


}
