/*
 * Created on 28-jul-2003
 *
 
 
 */
package f8.keyboard;


import f8.platform.Calc;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class ShiftKey extends Tecla {
	int dir;

	public ShiftKey(int direccion) {
		super((direccion == 1) ? "Left" : "Right");
		dir = direccion;
	}

	public void pressed() {
		int shStatus = Calc.ref.getShiftMode();
		if (dir == 1) {
			if (shStatus == 1) {
				Calc.ref.setShiftMode(0);
			} else {
				Calc.ref.setShiftMode(1);
			}
		} else {
			if (shStatus == 2) {
				Calc.ref.setShiftMode(0);
			} else {
				Calc.ref.setShiftMode(2);
			}
		}
		switch (Calc.ref.getShiftMode()) {
		case 0:
			//Core.setMenu(0, null, Keyboard.normMiddle(), Keyboard.normDown());
			break;
		case 1:
			//Core.setMenu(0, null, Keyboard.leftMiddle(), Keyboard.leftDown());
			break;
		case 2:
			//Core.setMenu(0, null, Keyboard.rightMiddle(), Keyboard.rightDown());
			break;
		}
	}
}
