/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import f8.Core;
import f8.keyboard.Menu;
import f8.keyboard.SinEfecto;
import f8.keyboard.TeclaVar;
import f8.objects.types.Directory;

/**
 * @author elinares
 */
public final class VAR extends Menu {
	public VAR() {
		super("VAR"); // VAR
	}
	public void notifyChangeStatus() {
		Hashtable ht = Core.currentDict().getHT();
		Vector udv = new Vector();
		for(Enumeration e=ht.keys();e.hasMoreElements();)
			udv.addElement(e.nextElement());
		int uds = udv.size();
		Vector vkm = new Vector();
		for (int i = 0; (i < 6) || (i < uds); i++) {
			if (i < uds) {
				String name = (String) udv.elementAt(i);
				if (ht.get(name) instanceof Directory) {
					vkm.addElement(new TeclaVar(name, true));
				} else {
					vkm.addElement(new TeclaVar(name, false));
				}
			} else {
				vkm.addElement(new SinEfecto(""));
			}
		}
		setContent(vkm);
	}
}
