/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import f8.commands.prog.debug.DBUG;
import f8.commands.prog.debug.KILL;
import f8.commands.prog.debug.NEXTSTEP;
import f8.commands.prog.debug.SST;
import f8.commands.prog.debug.SST_INTO;
import f8.keyboard.Menu;
import f8.keyboard.Tecla;
import f8.keyboard.WithEnter;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class PRG extends Menu {
	public PRG() {
		super("PRG"); // PRG
		appendKey(stkMenu());
		appendKey(objMenu());
		appendKey(dsplMenu());
		appendKey(ctrlMenu());
		appendKey(brchMenu());
		appendKey(testMenu());
	}

	public Menu stkMenu() {
		Menu m = new Menu("STK");
		m.appendKey(new WithEnter("OVER"));
		m.appendKey(new WithEnter("ROT"));
		m.appendKey(new WithEnter("ROLL"));
		m.appendKey(new WithEnter("ROLLD"));
		m.appendKey(new WithEnter("PICK"));
		m.appendKey(new WithEnter("DEPTH"));
		m.appendKey(new WithEnter("DUP"));
		m.appendKey(new WithEnter("DUP2"));
		m.appendKey(new WithEnter("DUPN"));
		m.appendKey(new WithEnter("DROP2"));
		m.appendKey(new WithEnter("DRPN", "DROPN"));
		return m;
	}

	public Menu objMenu() {
		Menu m = new Menu("OBJ");
		m.appendKey(new WithEnter("OBJ\u008D"));
		m.appendKey(new WithEnter("EQ\u008D"));
		m.appendKey(new WithEnter("\u008DARR"));
		m.appendKey(new WithEnter("\u008DLIST"));
		m.appendKey(new WithEnter("\u008DSTR"));
		m.appendKey(new WithEnter("\u008DTAG"));
		m.appendKey(new WithEnter("R\u008DC"));
		m.appendKey(new WithEnter("C\u008DR"));
		m.appendKey(new WithEnter("DTAG"));
		m.appendKey(new WithEnter("\u008DUNIT"));
		m.appendKey(new WithEnter("TYPE"));
		m.appendKey(new WithEnter("VTYPE"));
		m.appendKey(new WithEnter("SIZE"));
		m.appendKey(new WithEnter("POS"));
		m.appendKey(new WithEnter("REPL"));
		m.appendKey(new WithEnter("SUB"));
		m.appendKey(new WithEnter("NUM"));
		m.appendKey(new WithEnter("CHR"));
		m.appendKey(new WithEnter("PUT"));
		m.appendKey(new WithEnter("GET"));
		m.appendKey(new WithEnter("PUTI"));
		m.appendKey(new WithEnter("GETI"));
		return m;
	}

	public Menu dsplMenu() {
		Menu m = new Menu("DSPL");
		m.appendKey(new WithEnter(" "));
		return m;
	}

	public static Menu ctrlMenu() {
		Menu m = new Menu("CTRL");
		m.appendKey(new Tecla("DBUG", new DBUG()));
		m.appendKey(new Tecla("SST", new SST()));
		m.appendKey(new Tecla("SST\u008F", new SST_INTO()));
		m.appendKey(new Tecla("NEXT", new NEXTSTEP()));
		m.appendKey(new Tecla("HALT"));
		m.appendKey(new Tecla("KILL", new KILL()));
		return m;
	}

	public Menu brchMenu() {
		Menu m = new Menu("BRCH");
		m.appendKey(new WithEnter("IF"));
		m.appendKey(new WithEnter("CASE"));
		m.appendKey(new WithEnter("START"));
		m.appendKey(new WithEnter("FOR"));
		m.appendKey(new WithEnter("DO"));
		m.appendKey(new WithEnter("WHILE"));
		m.appendKey(new WithEnter("THEN"));
		m.appendKey(new WithEnter("END"));
		m.appendKey(new WithEnter("NEXT"));
		m.appendKey(new WithEnter("STEP"));
		m.appendKey(new WithEnter("UNTIL"));
		m.appendKey(new WithEnter("REPEAT"));
		m.appendKey(new WithEnter("ELSE"));
		m.appendKey(new WithEnter("IFERR"));
		m.appendKey(new WithEnter("IFT"));
		m.appendKey(new WithEnter("IFTE"));
		return m;
	}

	public Menu testMenu() {
		Menu m = new Menu("TEST");
		m.appendKey(new WithEnter("AND"));
		m.appendKey(new WithEnter("OR"));
		m.appendKey(new WithEnter("XOR"));
		m.appendKey(new WithEnter("NOT"));
		m.appendKey(new WithEnter("SAME"));
		m.appendKey(new WithEnter("TYPE"));
		m.appendKey(new WithEnter("=="));
		m.appendKey(new WithEnter("\u008B")); // DISTINCT
		m.appendKey(new WithEnter("<"));
		m.appendKey(new WithEnter(">"));
		m.appendKey(new WithEnter("\u0089")); // <=
		m.appendKey(new WithEnter("\u008A")); // >=
		m.appendKey(new WithEnter("SF"));
		m.appendKey(new WithEnter("CF"));
		m.appendKey(new WithEnter("FS?"));
		m.appendKey(new WithEnter("FC?"));
		m.appendKey(new WithEnter("FS?C"));
		m.appendKey(new WithEnter("FC?C"));
		return m;
	}
}
