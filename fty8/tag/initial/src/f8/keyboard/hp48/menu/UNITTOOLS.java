/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import f8.keyboard.Menu;
import f8.keyboard.WithEnter;

/**
 * @author elinares
 */
public final class UNITTOOLS extends Menu {
	public UNITTOOLS() {
		super("UTOOL");
		appendKey(new WithEnter("CONVERT"));
		appendKey(new WithEnter("UBASE"));
		appendKey(new WithEnter("UVAL"));
		appendKey(new WithEnter("UFACT"));
		appendKey(new WithEnter("\u008DUNIT"));
	}
}
