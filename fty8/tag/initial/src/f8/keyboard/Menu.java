/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard;

import java.util.Hashtable;
import java.util.Vector;

import f8.commands.Operation;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.keyboard.hp48.menu.VAR;
import f8.objects.Stackable;
import f8.objects.types.Lista;
import f8.platform.Calc;
import f8.platform.Properties;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class Menu implements TeclaVirtual {
	public static Hashtable groups = new Hashtable();
	public static Properties menusDefs;
	public static int menus;
	/**
	 * Para volver a el con long-press-up
	 */
	public Menu padre=null;
	public int page=0,index=0;
	
	static {
		menusDefs=new Properties("/menu.properties");
		menus=Integer.parseInt(menusDefs.getProperty("size"));
	}
	public static Menu getMenuNumber(int i){
		if(i>0 && i<=menus){
			String k="m."+i+".c";
			String cn=menusDefs.getProperty(k);
			try {
				return (Menu)(Class.forName(cn).newInstance());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static int getNumberMenu(Menu m){
		String c=m.getClass().getName();
		for(int i=1;i<=menus;i++){
			String k="m."+i+".c";
			String o=menusDefs.getProperty(k);
			if(o.equals(c))
				return i;
		}
		return 0;
	}
	
	Operation preset = null;

	Vector content = null;

	String nombre;

	public Menu(String n) {
		this(n, new Vector());
	}

	public Menu(String n, Operation op) {
		this(n, new Vector());
		preset = op;
	}

	public Menu(String title, Vector dir) {
		nombre = title;
		content = dir;
	}
	
	public void setPadre(Menu m){
		padre=m;
	}
	
	public Menu getPadre(){
		return padre;
	}

	public static void createAndDisplayCustomMenu(Lista cst) throws F8Exception {
		Menu m = new Menu("Custom");
		Lista lcst = (Lista) cst;
		for (int i = 0; i < lcst.size(); i++) {
			Stackable mnu = (Stackable) lcst.get(i);
			if (mnu instanceof Lista) {
				Lista lmnu = (Lista) mnu;
				if (lmnu.size() == 0) {
					m.appendKey(new SinEfecto(""));
				} else if (lmnu.size() == 1) {
					m.appendKey(new TeclaCST(lmnu.get(0)));
				} else if (lmnu.size() == 2) {
					Stackable shift = (Stackable) lmnu.get(1);
					if (shift instanceof Lista) {
						m.appendKey(new TeclaCST(lmnu.get(0), (Lista) lmnu
								.get(1)));
					} else {
						throw new BadArgumentTypeException(m);
					}
				}
			} else {
				m.appendKey(new TeclaCST(mnu));
			}
		}
		Calc.ref.setDynMenu(m);
	}

	public void appendKey(TeclaVirtual vk) {
		content.addElement(vk);
		//Colocamos el padre
		if(vk instanceof Menu){
			((Menu)vk).setPadre(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#getColor()
	 */
	public int getColor() {
		return 0;
	}

	public TeclaVirtual get(int i) {
		if (i >= content.size()) {
			return new SinEfecto("");
		}
		if (content.elementAt(i) == null) {
			return new SinEfecto("");
		}
		return (TeclaVirtual) content.elementAt(i);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#getEmit()
	 */
	public String getEmit() {
		return nombre;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#getGroupCheck()
	 */
	public String getGroupCheck() {
		return null;
	}

	public TeclaVirtual[] getContentArray(int offset, int tam) {
		TeclaVirtual[] ret = new TeclaVirtual[tam];
		int i = offset;
		int j = 0;
		for (; j < tam; i++, j++) {
			if (i < content.size()) {
				ret[j] = get(i);
			}
		}
		return ret;
	}

	public String getTitle() {
		return nombre;
	}
	
	public void setTitle(String tit){
		nombre=tit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#isChecked()
	 */
	public boolean isChecked() {
		return false;
	}

	public boolean isDir() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#isVarSolver()
	 */
	public boolean isVarSolver() {
		return false;
	}

	/**
	 * Esta funci�n se llama para cada menu que este activado si ha cambiado.
	 * Por ejemplo se borra una variable o se crea en el menu VAR
	 * 
	 */
	public void notifyChangeStatus() {
	}

	public void pressed() throws F8Exception {
		if (this instanceof VAR) {
			notifyChangeStatus();
		}
		if (preset != null) {
			preset.exec(null);
		}
		Calc.ref.setDynamic(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#setChequed(boolean)
	 */
	public void setChequed(boolean is) {
		// Nada!
	}

	public void setContent(Vector dir) {
		content = dir;
	}

	public void setVirtualKeyAt(int k, TeclaVirtual vk) {
		content.setElementAt(vk, k);
	}

	public int size() {
		return content.size();
	}
}
