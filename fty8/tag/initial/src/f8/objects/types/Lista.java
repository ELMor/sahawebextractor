/*
 * Created on 13-ago-2003
 *
 
 
 */
package f8.objects.types;

import java.io.IOException;
import java.util.Vector;

import f8.commands.Storable;
import f8.commands.alg.CommandSequence;
import f8.exceptions.IndexRangeException;
import f8.objects.Stackable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class Lista extends CommandSequence {
	/**
	 * Representa una lista
	 * 
	 * @param a
	 */
	public Lista(Vector a) {
		super(a);
	}
	public Lista(){
		this(null);
	}
	public String getTypeName() {
		return "Lista";
	}

	public int getID() {
		return 120;
	}

	public String toString() {
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret + "{ " + super.toString() + " }";
	}

	public Stackable copia() {
		Vector copia = new Vector();
		for (int i = 0; i < obList.size(); i++) {
			if (obList.elementAt(i) instanceof Stackable) {
				copia.addElement(((Stackable) obList.elementAt(i)).copia());
			} else {
				copia.addElement(obList.elementAt(i));
			}
		}
		return new Lista(copia);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#loadState(f8.platform.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#saveState(f8.platform.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	public Stackable add(Stackable esto) {
		if (esto instanceof Lista) {
			try {
				Lista l = (Lista) esto;
				for (int i = 0; i < l.size(); i++)
					obList.addElement(l.get(i));
			} catch (IndexRangeException e) {
				e.printStackTrace();
			}
		} else {
			obList.addElement(esto);
		}
		return this;
	}

}
