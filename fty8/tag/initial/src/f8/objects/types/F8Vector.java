package f8.objects.types;

import java.io.IOException;

import f8.Core;
import f8.commands.Command;
import f8.commands.Storable;
import f8.commands.alg.DoubleValue;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.objects.Stackable;
import f8.objects.build.TO_LIST;
import f8.platform.Calc;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class F8Vector extends Array {
	public Ereal[] x;

	public F8Vector(int d) {
		initSize(d);
	}

	private void initSize(int d) {
		this.x = new Ereal[d];
	}

	public F8Vector(Ereal[] ax) {
		copy(ax);
	}

	private void copy(Ereal[] ax) {
		initSize(ax.length);
		for(int i=0;i<ax.length;i++)
			x[i]=new Ereal(ax[i]);
	}
	
	public F8Vector(){
		this(0);
	}

	public Stackable copia() {
		return new F8Vector(x);
	}

	public int getID() {
		return 92;
	}

	public Storable getInstance() {
		return new F8Vector(0);
	}

	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
		int sz = ds.readInt();
		x = new Ereal[sz];
		for (int i = 0; i < sz; i++) {
			x[i] = new Ereal(ds.readStringSafe());
		}
	}

	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		ds.writeInt(x.length);

		for (int i = 0; i < x.length; i++) {
			ds.writeStringSafe(x[i].toString());
		}
	}

	public int size() {
		return (x.length);
	}

	// range assumed OK
	public Command get(int i) throws IndexRangeException {
		try {
			return (new Double(x[i]));
		} catch (Exception e) {
			throw new IndexRangeException(this);
		}

	}

	public void put(Command a, int i) throws F8Exception {
		if (a instanceof DoubleValue) {
			if ((0 <= i) && (i < x.length)) {
				Ereal y = ((DoubleValue) a).doubleValue();
				x[i] = y;
			} else {
				throw new IndexRangeException(this);
			}
		} else {
			throw new BadArgumentTypeException(this);
		}
	}

	public String toString() {
		StringBuffer s = new StringBuffer("[");

		for (int i = 0; i < x.length; i++) {
			s.append(x[i].val.toString(Calc.ref.getNumberFormat()));
			if (i < x.length - 1)
				s.append(' ');
		}

		if (x.length == 0)
			s.append("<empty vector>");

		s.append(']');

		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret + s.toString();
	}

	public String getTypeName() {
		return "F8Vector";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() throws F8Exception {
		for (int i = 0; i < x.length; i++) {
			Core.push(new Double(x[i]));
		}
		Core.push(new Double(x.length));
		Core.push(new Double(1));
		new TO_LIST().exec(null);
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof F8Vector) {
			F8Vector cobj = (F8Vector) obj;
			if (cobj.x.length != x.length) {
				return false;
			}
			for (int i = 0; i < x.length; i++) {
				if (x[i] != cobj.x[i])
					return false;
			}
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, Ereal tol) {
		if (obj instanceof F8Vector) {
			F8Vector cobj = (F8Vector) obj;
			if (cobj.x.length != x.length) {
				return false;
			}
			Ereal sumTol = Ereal.cero;
			for (int i = 0; i < x.length; i++) {
				sumTol = sumTol.plus(x[i].minus(cobj.x[i]).abs());
			}
			return sumTol.le(tol);
		}
		return false;
	}
	
	public String getLaTeX() {
		StringBuffer sb=new StringBuffer();
		sb.append("\\left( \\begin{array}{");
		int l=x.length;
		for(int i=0;i<l;i++)
			sb.append("c");
		sb.append("}");
		for(int i=0;i<l;i++){
			String tmp=""+x[i];
			if(tmp.endsWith(".0"))
				tmp=tmp.substring(0, tmp.length()-2);
			sb.append(tmp);
			if(i!=l-1)
				sb.append("\\\\");
		}
		sb.append("\\end{array} \\right)");
		return sb.toString();
	}

	public Ereal abs(){
		Ereal ret=Ereal.cero;
		for(int i=0;i<x.length;i++)
			ret=ret.plus(x[i].sqr());
		return ret.sqrt();
	}

}
