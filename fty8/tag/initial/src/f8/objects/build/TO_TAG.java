/*
 * Created on 14-sep-2003
 *
 
 
 */
package f8.objects.build;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.F8String;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class TO_TAG extends NonAlgebraic {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 600;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		
		if (Core.check(2)) {
			Stackable obj = Core.peek(1);
			Stackable tag = Core.peek(0);
			if (tag instanceof F8String) {
				Core.pop();
				tag.setTag(null);
				obj.setTag(tag.toString());
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "\u008DTAG";
	}

}
