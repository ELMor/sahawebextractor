/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.exceptions;

import f8.commands.Command;

;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class GenericException extends f8.exceptions.F8Exception {

	/**
	 * 
	 */
	public GenericException(Command c) {
		super(c);

	}

	/**
	 * @param message
	 */
	public GenericException(String message) {
		super(message);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		return ICalcErr.Generic;
	}

}
