package f8.exceptions;

import f8.commands.Command;
import f8.keyboard.TeclaVirtual;

public class CanNotIsolateExpressionException extends F8Exception {

	public CanNotIsolateExpressionException(Command c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	public CanNotIsolateExpressionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CanNotIsolateExpressionException(TeclaVirtual v) {
		super(v);
		// TODO Auto-generated constructor stub
	}

	public int getNdx() {
		// TODO Auto-generated method stub
		return ICalcErr.CanNotIsolateException;
	}

}
