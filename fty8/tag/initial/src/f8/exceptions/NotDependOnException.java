package f8.exceptions;

import f8.commands.Command;
import f8.keyboard.TeclaVirtual;

public class NotDependOnException extends F8Exception {

	public NotDependOnException(TeclaVirtual v) {
		super(v);
		// TODO Auto-generated constructor stub
	}

	public NotDependOnException(Command c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	public NotDependOnException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public int getNdx() {
		return ICalcErr.NotDependOn;
	}

}
