/*
 * Created on 08-oct-2003
 *
 
 
 */
package f8.apps.display;

import java.io.IOException;

import f8.Core;
import f8.apps.solver.EqSolv;
import f8.commands.Command;
import f8.commands.Storable;
import f8.commands.alg.CommandSequence;
import f8.commands.alg.Ejecutable;
import f8.exceptions.BadArgumentValueException;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Ereal;
import f8.platform.Calc;
import f8.platform.Plot;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class DRAW extends Command {
	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getID()
	 */
	public int getID() {
		return 6000;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#loadState(f8.platform.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#saveState(f8.platform.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		try {
			Plot lcdc = Calc.ref.getLCD();
			Calc.scrMode=Calc.SCRMODE_PLOT;
			// Mostramos el LCD
			//Core.selectUpperTab(1);
			PPAR.initPPAR();
			Stackable eq = (Stackable) Core.lookupUser("EQ", false, true, false);
			CommandSequence run = ((Ejecutable) eq).getEjecutable();

			CSys lcdcs = CSys.getCoorSystem("lcd");
			CSys usrcs = CSys.getCoorSystem("usr");
			lcdc.reset(
					Double.parseDouble(""+PPAR.width.toString()), 
					Double.parseDouble(""+PPAR.height.toString())
					);
			lcdc.showAxes();
			// Inicializacion
			int endX = lcdcs.right().v[0].toInt();
			Ereal initY = EqSolv.fdexDouble(run, PPAR.indep, PPAR.Xmin);
			CSys.Point init = usrcs.coor(PPAR.Xmin.toInt(), initY.toInt());
			lcdc.initPoint(init);
			CSys.Vector step = lcdcs.vector(2, 0);
			for (int i = 0; i < endX; i += 2) {
				init = usrcs.transform(init.plus(step));
				init.v[1] = EqSolv.fdexDouble(run, PPAR.indep, init.v[0]);
				lcdc.nextPoint(init);
			}
		} catch (ClassCastException e) {
			throw new BadArgumentValueException("PPAR is bad, remove");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "DRAW";
	}
}
