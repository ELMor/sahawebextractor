package f8.apps.display;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.platform.Calc;
import f8.platform.Plot;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class BOX extends NonAlgebraic {
	public BOX() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 403;
	}

	public String toString() {
		return ("BOX");
	}

	public Storable getInstance() {
		return new BOX();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		Plot lcd = Calc.ref.getLCD();
		if (Core.check(2)) {
			CSys.Point c1 = StackChecker.UserCoordinates(Core.peek(1));
			CSys.Point c2 = StackChecker.UserCoordinates(Core.peek());
			if (c1 != null && c2 != null) {
				Core.pop(2);
				lcd.box(c1, c2);
				;
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

}
