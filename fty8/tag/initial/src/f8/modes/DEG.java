package f8.modes;

import java.io.IOException;

import f8.Core;
import f8.ICalc;
import f8.commands.Storable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class DEG extends CheckGroup {
	public DEG() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 489;
	}

	public Storable getInstance() {
		return new DEG();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public boolean execBefore() {
		
		Core.setSettings(ICalc.ANG_MOD, ICalc.DEG);
		return false;
	}

	public String toString() {
		return ("DEG");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "AM";
	}
}
