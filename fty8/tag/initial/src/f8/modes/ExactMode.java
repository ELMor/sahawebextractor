package f8.modes;

import java.io.IOException;

import f8.Core;
import f8.ICalc;
import f8.commands.Storable;
import f8.platform.Calc;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class ExactMode extends CheckGroup {
	public ExactMode() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 7014;
	}

	public Storable getInstance() {
		return new ExactMode();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public boolean execBefore() {
		Core.setSettings(ICalc.EXACT_MODE, !Core.isSetting(ICalc.EXACT_MODE));
		Calc.ref.pantalla.dynamicNeedRefresh=true;
		return false;
	}

	public String toString() {
		return ("0_2");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "EM";
	}
}
