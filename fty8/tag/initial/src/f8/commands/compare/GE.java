package f8.commands.compare;

import java.io.IOException;

import antlr.Token;
import antlr.collections.AST;
import f8.commands.Storable;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;


public final class GE extends Comparison {
	public Token getToken() {
		return new Token(GEOC);
	}
	public GE() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 83;
	}

	public Storable getInstance() {
		return new GE();
	}

	public String getLaTeX(AST child) {
		
		return "\\ge";
	}
	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("\u008A");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.ovl.Comparison#compare(double, double)
	 */
	public boolean compare(Ereal v1, Ereal v2) {
		return v1.ge(v2);
	}

}
