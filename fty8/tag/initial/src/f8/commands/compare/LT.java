package f8.commands.compare;

import java.io.IOException;

import antlr.Token;
import antlr.collections.AST;
import f8.commands.Storable;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;


public final class LT extends Comparison {
	public Token getToken() {
		return new Token(LT);
	}
	public LT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 86;
	}

	public Storable getInstance() {
		return new LT();
	}

	public String getLaTeX(AST child) {
		
		return "\\lt";
	}
	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("<");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.ovl.Comparison#compare(double, double)
	 */
	public boolean compare(Ereal v1, Ereal v2) {
		return v1.lt(v2);
	}

}
