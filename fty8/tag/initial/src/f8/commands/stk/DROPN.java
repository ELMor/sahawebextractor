package f8.commands.stk;

import java.io.IOException;

import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.types.Double;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class DROPN extends NonAlgebraic {
	public DROPN() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 22;
	}

	public Storable getInstance() {
		return new DROPN();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		if (Core.check(1)) {
			Command a = Core.pop();
			if (a instanceof Double) {
				int rot = ((Double) a).doubleValue().toInt();
				if (Core.check(rot)) {
					Core.pop(rot);
				} else {
					throw new TooFewArgumentsException(this);
				}
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("DROPN");
	}
}
