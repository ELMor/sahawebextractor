package f8.commands.units;

import antlr.collections.AST;
import f8.commands.math.mkl;
import f8.objects.types.Unit;

public class UnitManager {
	static AST metro = mkl.fca("m", null);
	static AST sqMeter = mkl.ele(metro, mkl.num(2));
	static AST cuMeter = mkl.ele(metro, mkl.num(3));
	static AST segun = mkl.fca("s", null);
	static AST mps = mkl.div(metro, segun);
	static AST mps2 = mkl.div(mps, segun);
	static AST kg = mkl.fca("kg", null);
	static AST mol = mkl.fca("mol", null);
	static AST N = mkl.mul(kg, mkl.div(metro, mkl.ele(segun, mkl.num(2))));
	static AST J = mkl.mul(N, metro);
	static AST W = mkl.div(J, segun);
	static AST Pa = mkl.div(kg, mkl.mul(metro, mkl.ele(segun, mkl.num(2))));
	static AST A = mkl.fca("A", null);
	static AST cd = mkl.fca("cd", null);
	static AST cdpm2 = mkl.div(cd, mkl.ele(metro, mkl.num(2)));
	static AST m2ps2 = mkl.mul(mkl.ele(metro, mkl.num(2)), mkl.ele(segun, mkl
			.num(2)));
	static AST gK = mkl.fca("�K", null);
	static AST angulo = mkl.fca("angulo", null);
	static AST arcos = mkl.fca("arcos", null);


	public static Unit getUnitDef(String ref){
		if(ref.equals("m")) return ( new Unit(null));
		if(ref.equals("cm")) return ( new Unit(0.01, metro));
		if(ref.equals("mm")) return ( new Unit(0.001, metro));
		if(ref.equals("yd")) return ( new Unit(0.9144, metro));
		if(ref.equals("ft")) return ( new Unit(0.3048, metro));
		if(ref.equals("in")) return ( new Unit(0.0254, metro));
		if(ref.equals("Mpc")) return ( new Unit(3.08567818585E22, metro));
		if(ref.equals("pc")) return ( new Unit(3.08567818585E16, metro));
		if(ref.equals("lyr")) return ( new Unit(9.46052840488E15, metro));
		if(ref.equals("au")) return ( new Unit(14959790E4, metro));
		if(ref.equals("km")) return ( new Unit(1000, metro));
		if(ref.equals("mi")) return ( new Unit(1609.344, metro));
		if(ref.equals("nmi")) return ( new Unit(1852, metro));
		if(ref.equals("miUS")) return ( new Unit(1609.34721869, metro));
		if(ref.equals("chain")) return ( new Unit(20.1168402337, metro));
		if(ref.equals("rd")) return ( new Unit(5.02921005842, metro));
		if(ref.equals("fath")) return ( new Unit(1.82880365761, metro));
		if(ref.equals("ftUS")) return ( new Unit(0.304800609601, metro));
		if(ref.equals("mil")) return ( new Unit(0.0000254, metro));
		if(ref.equals("micro")) return ( new Unit(0.000001, metro));
		if(ref.equals("argst")) return ( new Unit(0.0000000001, metro));
		if(ref.equals("fermi")) return ( new Unit(1E-15, metro));

		if(ref.equals("b")) return ( new Unit(1E-28, sqMeter));
		if(ref.equals("ha")) return ( new Unit(10000, sqMeter));
		if(ref.equals("a")) return ( new Unit(100, sqMeter));
		if(ref.equals("acre")) return ( new Unit(4046.87260987, sqMeter));

		if(ref.equals("fbm")) return ( new Unit(0.002359737216, cuMeter));
		if(ref.equals("pk")) return ( new Unit(0.0088097675, cuMeter));
		if(ref.equals("bu")) return ( new Unit(0.03523907, cuMeter));
		if(ref.equals("bbl")) return ( new Unit(0.158987294928, cuMeter));
		if(ref.equals("tsp")) return ( new Unit(4.92892159375E-6, cuMeter));
		if(ref.equals("tbsp")) return ( new Unit(1.47867647813E-5, cuMeter));
		if(ref.equals("ozUK")) return ( new Unit(0.000028413075, cuMeter));
		if(ref.equals("ozfl")) return ( new Unit(2.95735295625E-5, cuMeter));
		if(ref.equals("cu")) return ( new Unit(2.365882365E-4, cuMeter));
		if(ref.equals("ml")) return ( new Unit(0.000001, cuMeter));
		if(ref.equals("pt")) return ( new Unit(0.000473176473, cuMeter));
		if(ref.equals("qt")) return ( new Unit(0.000946352946, cuMeter));
		if(ref.equals("gal")) return ( new Unit(0.003785411784, cuMeter));
		if(ref.equals("galC")) return ( new Unit(0.00454609, cuMeter));
		if(ref.equals("galUK")) return ( new Unit(0.004546092, cuMeter));
		if(ref.equals("l")) return ( new Unit(0.001, cuMeter));
		if(ref.equals("st")) return ( new Unit(1.0, cuMeter));

		if(ref.equals("s")) return ( new Unit(null));
		if(ref.equals("Hz")) return ( new Unit(1, mkl.div(mkl.num(1), segun)));
		if(ref.equals("h")) return ( new Unit(3600, segun));
		if(ref.equals("min")) return ( new Unit(60, segun));
		if(ref.equals("d")) return ( new Unit(86400, segun));
		if(ref.equals("yr")) return ( new Unit(31556925.9747, segun));

		if(ref.equals("ga")) return ( new Unit(9.80665, mps2));
		if(ref.equals("c")) return ( new Unit(299792458, mps));
		if(ref.equals("knot")) return ( new Unit(0.514444444444, mps));
		if(ref.equals("mph")) return ( new Unit(0.44704, mps));
		if(ref.equals("kph")) return ( new Unit(0.277777777778, mps));

		if(ref.equals("kg")) return ( new Unit(1, kg));
		if(ref.equals("g")) return ( new Unit(0.001, kg));
		if(ref.equals("lb")) return ( new Unit(0.45359237, kg));
		if(ref.equals("mol")) return ( new Unit(1, mol));
		if(ref.equals("u")) return ( new Unit(1.66057E-27, kg));
		if(ref.equals("grain")) return ( new Unit(0.06479891E-3, kg));
		if(ref.equals("ct")) return ( new Unit(0.2E-3, kg));
		if(ref.equals("ozt")) return ( new Unit(31.103475E-3, kg));
		if(ref.equals("t")) return ( new Unit(1000000E-3, kg));
		if(ref.equals("tonUK")) return ( new Unit(1016046.9088E-3, kg));
		if(ref.equals("ton")) return ( new Unit(907184.74E-3, kg));
		if(ref.equals("lbt")) return ( new Unit(373.2417E-3, kg));
		if(ref.equals("slug")) return ( new Unit(14593.9029372E-3, kg));
		if(ref.equals("oz")) return ( new Unit(28.349523125E-3, kg));

		if(ref.equals("N")) return ( new Unit(1, N));
		if(ref.equals("pdl")) return ( new Unit(138.254954376E-3, N));
		if(ref.equals("lbf")) return ( new Unit(4448.22161526E-3, N));
		if(ref.equals("kip")) return ( new Unit(4448221.61526E-3, N));
		if(ref.equals("gf")) return ( new Unit(9.80665E-3, N));
		if(ref.equals("dyn")) return ( new Unit(0.01E-3, N));

		if(ref.equals("J")) return ( new Unit(1, J));
		if(ref.equals("MeV")) return ( new Unit(1.60219E-13, J));
		if(ref.equals("eV")) return ( new Unit(1.60219E-19, J));
		if(ref.equals("therm")) return ( new Unit(105506000, J));
		if(ref.equals("Btu")) return ( new Unit(1055.05585262, J));
		if(ref.equals("cal")) return ( new Unit(4.1868, J));
		if(ref.equals("kcal")) return ( new Unit(4186.8, J));
		if(ref.equals("erg")) return ( new Unit(0.0000001, J));

		if(ref.equals("W")) return ( new Unit(1, W));
		if(ref.equals("hp")) return ( new Unit(745.699871582, W));

		if(ref.equals("Pa")) return ( new Unit(1, Pa));
		if(ref.equals("inH20")) return ( new Unit(248.84, Pa));
		if(ref.equals("inHg")) return ( new Unit(3386.38815789, Pa));
		if(ref.equals("mmHg")) return ( new Unit(133.322368421, Pa));
		if(ref.equals("torr")) return ( new Unit(133.322368421, Pa));
		if(ref.equals("psi")) return ( new Unit(6894.75729317, Pa));
		if(ref.equals("bar")) return ( new Unit(100000, Pa));
		if(ref.equals("atm")) return ( new Unit(101325, Pa));

		if(ref.equals("A")) return ( new Unit(1, A));
		if(ref.equals("C")) return ( new Unit(1, mkl.mul(A, segun)));
		if(ref.equals("V")) return ( new Unit(1, mkl.mul(kg, mkl.div(mkl.ele(metro, mkl
					.num(2)), mkl.mul(A, mkl.ele(segun, mkl.num(3)))))));
		if(ref.equals("F")) return ( new Unit(1, mkl.mul(mkl.ele(A, mkl.num(2)), mkl.div(mkl
					.ele(segun, mkl.num(4)), mkl.mul(kg, mkl.ele(metro, mkl
					.num(2)))))));
		if(ref.equals("Ohm")) return ( new Unit(1, mkl.mul(kg, mkl.div(mkl.ele(metro, mkl
					.num(2)), mkl.mul(mkl.ele(A, mkl.num(2)), mkl.ele(segun,
					mkl.num(3)))))));
		if(ref.equals("Wb")) return ( new Unit(1, mkl.mul(kg, mkl.div(mkl.ele(metro, mkl
					.num(2)), mkl.mul(A, mkl.ele(segun, mkl.num(2)))))));
		if(ref.equals("T")) return ( new Unit(1, mkl.div(kg, mkl.mul(A, mkl.ele(segun, mkl
					.num(2))))));
		if(ref.equals("S")) return ( new Unit(1, mkl.mul(mkl.ele(A, mkl.num(2)), mkl.div(mkl
					.ele(segun, mkl.num(3)), mkl.mul(kg, mkl.ele(metro, mkl
					.num(2)))))));
		if(ref.equals("mho")) return ( new Unit(1, mkl.mul(mkl.ele(A, mkl.num(2)), mkl.div(
					mkl.ele(segun, mkl.num(3)), mkl.mul(kg, mkl.ele(metro, mkl
							.num(2)))))));
		if(ref.equals("H")) return ( new Unit(1, mkl.mul(kg, mkl.div(mkl.ele(metro, mkl
					.num(2)), mkl.mul(mkl.ele(A, mkl.num(2)), mkl.ele(segun,
					mkl.num(2)))))));
		if(ref.equals("Fdy")) return ( new Unit(96487, mkl.mul(A, segun)));

		if(ref.equals("cd")) return ( new Unit(1, cd));
		if(ref.equals("lam")) return ( new Unit(3183.09886184, cdpm2));
		if(ref.equals("lm")) return ( new Unit(7.957747154559E-2, cd));
		if(ref.equals("sb")) return ( new Unit(10000, cdpm2));
		if(ref.equals("ph")) return ( new Unit(795.774715459, cdpm2));
		if(ref.equals("lx")) return ( new Unit(7.95774715459E-2, cdpm2));
		if(ref.equals("flam")) return ( new Unit(3.42625909964, cdpm2));
		if(ref.equals("fc")) return ( new Unit(0.856564774909, cdpm2));

		if(ref.equals("R")) return ( new Unit(0.000258, mkl.mul(A, mkl.div(segun, kg))));
		if(ref.equals("Ci")) return ( new Unit(37E9, mkl.inv(segun)));
		if(ref.equals("Bq")) return ( new Unit(1, mkl.inv(segun)));
		if(ref.equals("Sv")) return ( new Unit(1, m2ps2));
		if(ref.equals("rem")) return ( new Unit(0.01, m2ps2));
		if(ref.equals("rad")) return ( new Unit(0.01, m2ps2));
		if(ref.equals("Gy")) return ( new Unit(1, m2ps2));

		if(ref.equals("P")) return ( new Unit(0.1, mkl.div(kg, mkl.mul(metro, segun))));
		if(ref.equals("St")) return ( new Unit(0.0001, mkl.div(mkl.ele(metro, mkl.num(2)),
					segun)));

		if(ref.equals("K")) return ( new Unit(null));
		if(ref.equals("�C")) return ( new Unit(1, gK));
		if(ref.equals("�F")) return ( new Unit(5 / 9, gK));
		if(ref.equals("�R")) return ( new Unit(5 / 9, gK));

		if(ref.equals("r")) return ( new Unit(1.0 / 2.0 / 3.14159265359, angulo));
		if(ref.equals("�")) return ( new Unit(1.0 / 360.0, angulo));
		if(ref.equals("grad")) return ( new Unit(1.0 / 400.0, angulo));

		if(ref.equals("arcs")) return ( new Unit(1.0 / 360.0 / 36.0 / 100, arcos));
		if(ref.equals("arcmin")) return ( new Unit(1.0 / 6.0 / 36.0 / 100, arcos));
		if(ref.equals("sr")) return ( new Unit(1031.32403124 / 360 / 36, arcos));
		
		return null;
	}
}
