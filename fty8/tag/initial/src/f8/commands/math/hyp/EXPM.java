package f8.commands.math.hyp;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Storable;
import f8.commands.math.Trigonometric;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class EXPM extends Trigonometric {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public int getID() {
		return 3030;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#undo()
	 */
	public Storable getInstance() {
		return new EXPM();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public Stackable prfDouble(Double x) {
		Complex k = new Complex(Ereal.E, Ereal.cero).power(new Complex(x.doubleValue(), Ereal.cero));
		return new Complex(k.re.minus(Ereal.uno), k.im).mutate();
	}

	public String toString() {
		return ("EXPM");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST args, String var) throws F8Exception {
		return mkl.res(mkl.num(0), mkl.mul(DER.deriveFunction(args, var), mkl
				.div(mkl.num(1), mkl.ele(mkl.res(mkl.num(1), mkl.ele(args, mkl
						.num(2))), mkl.num(0.5)))));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		Complex k = new Complex(Ereal.E, Ereal.cero).power(x);
		return new Complex(k.re.minus(Ereal.uno), k.im).mutate();
	}

}
