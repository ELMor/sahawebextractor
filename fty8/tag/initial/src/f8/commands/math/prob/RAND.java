package f8.commands.math.prob;


import java.io.IOException;

import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.commands.alg.IntValue;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.F8Vector;
import f8.objects.types.Int;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class RAND extends Dispatch1 {
	public RAND() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3022;
	}

	public String toString() {
		return ("RAND");
	}

	public Storable getInstance() {
		return new RAND();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	Ereal length(Ereal[] x) {
		Ereal max = Ereal.cero;

		for (int i = 0; i < x.length; i++) {
			Ereal m = x[i].abs();

			if (m.gt(max)) {
				max = m;
			}
		}

		if (max.isZero()) {
			return (max);
		}

		Ereal r = Ereal.cero;

		for (int i = 0; i < x.length; i++) {
			Ereal y = x[i].div(max);
			r = r.plus(y.sqr());
		}

		return (max.mul(r.sqrt()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfInt(f8.kernel.types.Int)
	 */
	public Stackable prfInt(Int a) throws F8Exception {
		int A = ((IntValue) a).intValue();

		if (A < 0) {
			A = -A;
		}
		return new Int(A);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		Ereal r=a.doubleValue().copy();
		if (r.isNegative()) {
			r = r.neg();
		}

		return new Double(r);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex a) throws F8Exception {
		Ereal m[]=new Ereal[2];
		m[0]=a.re;
		m[1]=a.im;
		Ereal r = length(m);
		return new Double(r);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfVector(f8.platform.utl.Vector)
	 */
	public Stackable prfVector(F8Vector a) throws F8Exception {
		return new Double(length(a.x));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
