/*
 * Created on 30-ago-2003
 *
 
 
 */
package f8.commands.math;

import java.util.Vector;

import antlr.CommonAST;
import antlr.Token;
import antlr.collections.AST;
import f8.Core;
import f8.ICalc;
import f8.commands.Command;
import f8.commands.Dispatch1;
import f8.commands.math.deriv.DER;
import f8.commands.math.ints.PRIME;
import f8.exceptions.BadArgumentValueException;
import f8.exceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;
import f8.objects.types.TempUnit;
import f8.objects.types.Unit;

/**
 * @author elinares
 * 
 * Tratamiento de arboles
 */
public final class mkl implements ObjectParserTokenTypes {
	public static Ereal[] darg = new Ereal[2];
	public static final AST uno = num(1);
	
	private static boolean symbolicMode(){
		return Core.getSetting(ICalc.SYM_MOD)==1;
	}
	private static boolean exactMode(){
		return Core.isSetting(ICalc.EXACT_MODE);
	}
	/**
	 * Comprueba si son numeros.
	 * 
	 * @param n1
	 *            Nodo a comprobar
	 * @param n2
	 *            Nodo a comprobar
	 * @return true si n1 y n2 son nodos de tipo Number.
	 */
	public static boolean bothNumbers(AST n1, AST n2) {
		if(n1.getType() == NEG){
			boolean bn=bothNumbers(n1.getFirstChild(), n2);
			if(bn){
				darg[0]=Ereal.cero.minus(darg[0]);
				return true;
			}
			return false;
		}
		if(n2.getType() == NEG){
			boolean bn=bothNumbers(n1, n2.getFirstChild());
			if(bn){
				darg[1]=Ereal.cero.minus(darg[1]);
				return true;
			}
			return false;
		}
		if (n1.getType() != NUMBER) {
			return false;
		}
		if (n2.getType() != NUMBER) {
			return false;
		}
		darg[0] = new Ereal(n1.getText());
		darg[1] = new Ereal(n2.getText());
		return true;
	}

	/**
	 * Crea un nuevo AST sin unir a sus hermanos.
	 * 
	 * @param t
	 *            El que se quiere clonar
	 * @return nuevo t sin hermanos.
	 */
	public static AST ccc(AST t) {
		if (t == null) {
			return null;
		}
		CommonAST ret = new CommonAST();
		ret.initialize(t);
		ret.addChild(t.getFirstChild());
		return ret;
	}

	public static AST chainMCD(AST fa1, AST fa2) {
		if ((fa1.getType() == MULT) && (fa2.getType() == MULT)) {
			AST fa11 = fa1;
			AST fa21 = fa2;
			AST ret = null;
			AST iret = null;
			AST faa1 = copyAST(fa11);
			AST faa2 = copyAST(fa21);
			AST ch1 = faa1.getFirstChild();
			while (ch1 != null) {
				AST ch2 = faa2.getFirstChild();
				while (ch2 != null) {
					if (opEquals(ch1, ch2)) {
						remove(faa1, ch1);
						remove(faa2, ch2);
						if (ret == null) {
							ret = iret = copyAST(ch1);
						} else {
							iret.setNextSibling(copyAST(ch1));
							iret = iret.getNextSibling();
						}
					}
					ch2 = ch2.getNextSibling();
				}
				ch1 = ch1.getNextSibling();
			}
			return ret;
		}
		if ((fa1.getType() == MULT)) {
			return chainMCD(fa1, nor(MULT, "*", fa2, num(1)));
		}
		if (fa2.getType() == MULT) {
			return chainMCD(nor(MULT, "*", fa1, num(1)), fa2);
		}
		return chainMCD(nor(MULT, "*", fa1, num(1)),
				nor(MULT, "*", fa2, num(1)));
	}

	/**
	 * Copia-Clona un AST
	 * @param t El modelo
	 * @return  La Copia
	 */
	public static AST copyAST(AST t) {
		AST father = new CommonAST();
		AST child;
		father.initialize(t);
		for (child = t.getFirstChild(); child != null; child = child
				.getNextSibling()) {
			father.addChild(copyAST(child));
		}
		return father;
	}

	/**
	 * Crea un arbol con la expresion derGenerica
	 * 
	 * @param f
	 *            Nombre de la funcion
	 * @param args
	 *            Lista de argumentos
	 * @param var
	 *            Nombre de la variable sobre la que queremos derivar
	 * @return Arbol de derivacion (dF(G(x))/dx -> derF(G(x),dG(x)/dx)
	 */
	public static AST derFgen(String f, AST args, String var)
			throws F8Exception {
		AST nID = new CommonAST(new Token(ID));
		nID.setText(f);
		AST k = args;
		while (k != null) {
			nID.addChild(ccc(k));
			k = k.getNextSibling();
		}
		k = args;
		while (k != null) {
			nID.addChild(DER.deriveFunction(k, var));
			k = k.getNextSibling();
		}
		return nID;
	}

	/**
	 * Crea una division a1/a2. Si son numeros, devuelve un numero con la
	 * division Si el numerador es 0 devuelve 0 Si el denominador es 1 devuelve
	 * el numerador Se transforma (a11/a12)/a2 => a11/(a2*a12) Se transforma
	 * a1/(a21/a22) => (a1*a22)/a21
	 * 
	 * @param a1
	 *            Numerador
	 * @param a2
	 *            Denominador
	 * @return Division (con simplificaciones)
	 */
	public static AST div(AST up, AST down) {
		AST reduced=reduceDiv(up,down);
		if(reduced!=null)
			return reduced;
		if(exactMode() 
				&& bothNumbers(up, down) 
				&& darg[0].isIntegral() 
				&& darg[1].isIntegral() ){
			//Descomposicion en primos
			try {
				int[][] ul=PRIME.primeDecomp(darg[0].toInt());
				int[][] dl=PRIME.primeDecomp(darg[1].toInt());
				
				for(int i=0;i<ul[0].length;i++){
					for(int j=0;j<dl[0].length;j++){
						if(ul[0][i]==dl[0][j]){
							int ptimes=ul[1][i];
							int ctimes=dl[1][j];	
							if(ptimes<ctimes){
								ul[1][i]=0;
								dl[1][j]=ctimes-ptimes;
							}else{
								ul[1][i]=ptimes-ctimes;
								dl[1][j]=0;
							}
						}else if(ul[0][i]<dl[0][j]){
							break;
						}
					}
				}
				int nu=fromPrimes(ul);
				int nd=fromPrimes(dl);
				up.setText(""+nu);
				down.setText(""+nd);
				AST ret=reduceDiv(up,down);
				if(ret!=null)
					return ret;
				ret=nor(DIV, "/", up, down);
				return ret;
			} catch (BadArgumentValueException e) {
				e.printStackTrace();
			}
		}else if(bothNumbers(up, down)) {
			return num(darg[0].div(darg[1])); 
		} 
		if (opEquals(getBaseOf(up), getBaseOf(down))) {
			AST e1 = getExponentOf(up);
			AST e2 = getExponentOf(down);
			return ele(getBaseOf(up), res(e1, e2));
		}
		int t1 = up.getType();
		int t2 = down.getType();
		if ((t1 == MULT) && (t2 == MULT)) {
			if (removeEqualsFromChainsOfFathers(up, down)) {
				return div(singleMul(up), singleMul(down));
			}
		} else if (t1 == MULT) {
			if (removeNodeEquivFromChainOfFather(up, down)) {
				return singleMul(up);
			}
		} else if (t2 == MULT) {
			if (removeNodeEquivFromChainOfFather(down, up)) {
				return inv(singleMul(down));
			}
		} else if (t1 == DIV) {
			AST nume = up.getFirstChild();
			AST deno = nume.getNextSibling();
			return div(ccc(nume), mul(ccc(deno), ccc(down)));
		} else if (t2 == DIV) {
			AST nume = down.getFirstChild();
			AST deno = nume.getNextSibling();
			return div(mul(up, ccc(deno)), ccc(nume));
		}
		return nor(DIV, "/", ccc(up), ccc(down));
	}
	private static AST reduceDiv(AST up, AST down) {
		if ((up == null) && (down == null)) {
			return num(1); // Fue simplificado
		}
		if (iComp(up, Ereal.cero)) {
			return num(0);
		}
		if (iComp(down, Ereal.uno)) {
			return ccc(up);
		}
		return null;
	}
	private static int fromPrimes(int[][] ul) {
		int nu=1;
		for(int i=0;i<ul[0].length;i++){
			for(int j=0;j<ul[1][i];j++){
				nu*=ul[0][i];
			}
		}
		return nu;
	}

	/**
	 * Crea un arbol a1^a2 Si a1 y a2 son numeros se devuelve un numero Si a1 es
	 * 0, se devuelve 0 Si a2 es 0, se devuelve 1 Si a1 es 1, se devuelve 1 Si
	 * a2 es 1, se devuelve a1
	 * 
	 * @param a1
	 *            base
	 * @param a2
	 *            exponente
	 * @return El arbol de elevacion
	 */
	public static AST ele(AST a1, AST a2) {
		if(!exactMode()){
			if (bothNumbers(a1, a2)) {
				return num(darg[0].pow(darg[1]));
			}
		}
		if (iComp(a1, Ereal.cero)) {
			return num(0);
		}
		if (iComp(a2, Ereal.cero)) {
			return num(1);
		}
		if (iComp(a1, Ereal.uno)) {
			return num(1);
		}
		if (iComp(a2, Ereal.uno)) {
			return ccc(a1);
		}
		if (isNumber(a2) && (new Ereal(a2.getText()).isNegative())) {
			return div(num(1), ele(a1, num(new Ereal(a2.getText()).neg())));
		}
		if( a1.getType()==CARET ){
			AST base=a1.getFirstChild();
			AST oldExpon=base.getNextSibling();
			return ele(base,mkl.mul(oldExpon, a2));
		}
		return nor(CARET, "^", a1, a2);
	}

	/**
	 * Crea un arbol de asignacion
	 * 
	 * @param a1
	 *            Izquierdo
	 * @param a2
	 *            Derecho
	 * @return Izquierdo=Derecho
	 */
	public static AST equ(AST a1, AST a2) {
		return nor(ASSIGN, "=", a1, a2);
	}
	/**
	 * Desnormaliza una potencia
	 * @param caret AST que contiene la potencia (base^expo)
	 * @return (base*base^(expo-1))
	 */
	public static AST expandCaret(AST caret) {
		AST base = caret.getFirstChild();
		AST expon = base.getNextSibling();
		if ((expon.getType() == NUMBER) && (base.getType() != ID)) {
			double exponNum = java.lang.Double.parseDouble(expon.getText());
			if (exponNum > 1) {
				return nor(MULT, "*", base, ele(base, num(exponNum - 1)));
			}
		}
		return caret;
	}
	/**
	 * Expande un producto
	 * @param prod AST productorio de todos sus hijos
	 * @return Expansion del productorio (Ej. a*(b+c) -> a*b+a*c)
	 */
	public static AST expandMul(AST prod) {
		AST child = prod.getFirstChild();
		AST ret = null;
		while (child != null) {
			if (child.getType() == PLUS) {
				remove(prod, child);
				prod = productCardinality(prod);
				child = copyAST(ccc(child));
				AST sumChild = child.getFirstChild();
				ret = null;
				while (sumChild != null) {
					AST prdHo = copyAST(prod);
					AST schHo = copyAST(sumChild);
					if (ret == null) {
						ret = nor(MULT, "*", prdHo, schHo);
					} else {
						AST retHo = copyAST(ret);
						ret = nor(PLUS, "+", retHo,
								nor(MULT, "*", prdHo, schHo));
					}
					sumChild = sumChild.getNextSibling();
				}
				return ret;
			}
			if (child.getType() == MINUS) {
				remove(prod, child);
				AST posi = child.getFirstChild();
				AST nega = posi.getNextSibling();
				return nor(MINUS, "-", nor(MULT, "*", prod, posi), nor(MULT,
						"*", prod, nega));
			}
			child = child.getNextSibling();
		}
		return prod;
	}

	private static AST factorizar(AST ch1, AST ch2, AST cel) {
		AST res;
		if (opEquals(cel, ch1)) {
			if (opEquals(cel, ch2)) {
				res = mul(num(2), cel);
			} else {
				AST copyOfCEL = copyAST(cel);
				AST fatherCEL = new CommonAST(new Token(MULT));
				fatherCEL.setFirstChild(copyOfCEL);
				removeEqualsFromChainsOfFathers(ch2, fatherCEL);
				res = mul(sum(num(1), ch2), cel);
			}
		} else {
			if (opEquals(cel, ch2)) {
				AST copyOfCEL = copyAST(cel);
				AST fatherCEL = new CommonAST(new Token(MULT));
				fatherCEL.setFirstChild(copyOfCEL);
				removeEqualsFromChainsOfFathers(ch1, fatherCEL);
				res = mul(sum(num(1), ch1), cel);
			} else {
				AST cel1 = copyAST(cel);
				AST cel2 = copyAST(cel);
				AST fatherOfcel = new CommonAST();
				fatherOfcel.setFirstChild(cel1);
				removeEqualsFromChainsOfFathers(ch1, fatherOfcel);
				fatherOfcel.setFirstChild(cel2);
				removeEqualsFromChainsOfFathers(ch2, fatherOfcel);
				res = mul(sum(ch1, ch2), cel);
			}
		}
		return res;
	}

	/**
	 * Crea un arbol de llamada a funcion
	 * 
	 * @param f
	 *            Nombre de la funcion
	 * @param a1
	 *            Argumento
	 * @return Devuelve el arbol que representa f(a1)
	 */
	public static AST fca(String f, AST a1) {
		AST nID = new CommonAST(new Token(ID));
		nID.setText(f);
		Command k = Core.lookup(f);
		if ((k != null) && k instanceof Dispatch1) {
			Dispatch1 fk = (Dispatch1) k;
			String undoF = fk.undo();
			if (a1 != null && undoF != null && undoF.equals(a1.getText())) {
				return a1.getFirstChild();
			}
		}
		if (a1 != null) {
			nID.addChild(ccc(a1));
		}
		return nID;
	}

	/**
	 * Crea un arbol de llamada a funcion
	 * 
	 * @param f
	 *            Nombre de la funcion
	 * @param a1
	 *            Primer argumento
	 * @param a2
	 *            Segundo argumento
	 * @return Devuelve el arbol f(a1,a2)
	 */
	public static AST fca2(String f, AST a1, AST a2) {
		AST nID = new CommonAST(new Token(ID));
		nID.setText(f);
		nID.addChild(ccc(a1));
		nID.addChild(ccc(a2));
		return nID;
	}

	/**
	 * Crea un arbol de llamada a funcion
	 * 
	 * @param f
	 *            Nombre de la funcion
	 * @param args
	 *            Lista de argumentos
	 * @return Devuelve el arbol f(args,arg.getNextSibling(),...)
	 */
	public static AST fun(String f, AST headOfChain) {
		return opSiblings(ID, f, headOfChain);
	}

	public static AST getBaseOf(AST op) {
		switch (op.getType()) {
		case CARET:
			return op.getFirstChild();
		}
		return op;
	}

	public static AST getExponentOf(AST op) {
		switch (op.getType()) {
		case CARET:
			return op.getFirstChild().getNextSibling();
		}
		return num(1);
	}

	/**
	 * Devuelve true si ambos nodos son numeros iguales
	 * 
	 * @param n1
	 *            Primer nodo
	 * @param n2
	 *            Segundo nodo
	 * @return true si ambos son numeros e iguales
	 */
	public static boolean iComp(AST n1, AST n2) {
		if(!isNumber(n1))
			return false;
		Ereal n1e=new Ereal(darg[0]);
		if(!isNumber(n2))
			return false;
		return n1e.eq(darg[0]);
	}

	/**
	 * Comprueba si d y n son iguales
	 * 
	 * @param n
	 *            Nodo
	 * @param d
	 *            double
	 * @return true si el numero que representa el nod es igual a d
	 */
	public static boolean iComp(AST n, Ereal d) {
		if (n.getType() != NUMBER) {
			return false;
		}
		Ereal d2 = new Ereal(n.getText());
		return d2.eq(d);
	}

	public static AST insertBefore(AST father, AST child, AST newchild) {
		AST k = father.getFirstChild();
		AST lk;
		if (k == child) {
			father.setFirstChild(newchild);
			newchild.setNextSibling(k);
			return father;
		}
		lk = child;
		for (k = k.getNextSibling(); k != null; k = k.getNextSibling()) {
			if (k == child) {
				lk.setNextSibling(newchild);
				newchild.setNextSibling(k);
				return father;
			}
			lk = k;
		}
		return father;
	}

	/**
	 * Devuelve el inverso de la expresion Si a1 es una division (num/den), se
	 * develve (den/num) Si a1 es INV(b) se devuelve b
	 * 
	 * @param a1
	 *            Expresion a invertir
	 * @return INV(a1)
	 */
	public static AST inv(AST a1) {
		if (a1.getType() == DIV) {
			AST nume = a1.getFirstChild();
			AST deno = nume.getNextSibling();
			return div(deno, nume);
		} else if (a1.getType() == INV) {
			return a1.getFirstChild();
		} else {
			AST ret = new CommonAST(new Token(INV));
			ret.addChild(ccc(a1));
			return ret;
		}
	}

	/**
	 * Comprueba si es un numero.
	 * 
	 * @param n1
	 *            Nodo a comprobar
	 * @return true si n1 es un nodo de tipo Number.
	 */
	public static boolean isNumber(AST n1) {
		if(n1.getType() == NEG){
			boolean bn=isNumber(n1.getFirstChild());
			if(bn){
				darg[0]=Ereal.cero.minus(darg[0]);
				return true;
			}
			return false;
		}
		if (n1.getType() != NUMBER) {
			return false;
		}
		darg[0] = new Ereal(n1.getText());
		return true;
	}

	public static AST joinProducts(AST mp) {
		if (mp.getType() != MULT) {
			return mp; // No hacemos nada
		}
		AST child = mp.getFirstChild();
		while (child != null) {
			if (child.getType() == MULT) {
				remove(mp, child);
				child = joinProducts(child);
				mp.addChild(child.getFirstChild());
			}
			child = child.getNextSibling();
		}
		return mp;
	}

	public static AST joinSumas(AST ms) {
		if (ms.getType() != PLUS) {
			return ms; // No hacemos nada
		}
		AST child = ms.getFirstChild();
		while (child != null) {
			if (child.getType() == PLUS) {
				remove(ms, child);
				child = joinSumas(child);
				ms.addChild(child.getFirstChild());
			}
			child = child.getNextSibling();
		}
		//Acumulamos todos los sumandos numericos en uno
		Ereal accu=new Ereal(0);
		child = ms.getFirstChild();
		while(child!=null){
			if(child.getType()==NUMBER){
				accu=accu.plus(new Ereal(child.getText()));
				remove(ms,child);
			}
			child = child.getNextSibling();
		}
		//Si la suma es distinto de cero, a�adimos el nodo
		if(!accu.isZero()){
			ms.addChild(num(accu));
		}
		//Rastreamos ahora las fracciones si es que estamos en exactMode
		reduceFractions(ms);
		return sumaCardinality(ms);
	}
	private static boolean reduceFractions(AST ms) {
		AST child;
		int checked=0;
		if(exactMode()){
			Ereal num=new Ereal(0);
			Ereal den=new Ereal(1);
			child=ms.getFirstChild();
			while(child!=null){
				if(child.getType()==DIV){
					checked++;
					AST nume=child.getFirstChild();
					AST deno=nume.getNextSibling();
					if(bothNumbers(nume, deno)){
						num=num.mul(darg[1]).plus(den.mul(darg[0]));
						den=den.mul(darg[1]);
						remove(ms,child);
					}
				}else if(isNumber(child)){
					checked++;
					num=num.plus(den.mul(darg[0]));
					remove(ms,child);
				}
				child=child.getNextSibling();
			}
			if(!num.isZero()){
				ms.addChild(mkl.div(num(num), num(den)));
			}
		}
		return checked>1;
	}

	/**
	 * Devuelve el producto de los nodos Si a1 o a2 == 0, devuelve 0 Si ambos
	 * son numeros, se devuelve un numero Si a1==1, se devuelve a2 Si a2==1, se
	 * devuelve a1 Si a1==INV(a2), se devuelve 1 Si a1 o a2 es un producto, se
	 * a�ade a2 o a1 al mismo Si a1=(numerador/den) y a2=den, se devuelve
	 * numerador
	 * 
	 * @param a1
	 *            Primer nodo
	 * @param a2
	 *            Segundo nodo
	 * @return nodo que representa el producto de a1 y a2
	 */
	public static AST mul(AST a1, AST a2) {
		return colect(singleMul(joinProducts(nor(MULT, "*", a1, a2))));
	}

	/**
	 * Se devuelve un cambio de signo Si a1==NEG(b), se devuelve b.
	 * 
	 * @param a1
	 *            Nodo que se quiere negativizar
	 * @return -a1
	 */
	public static AST neg(AST a1) {
		if (a1.getType() == NEG) {
			return ccc(a1.getFirstChild());
		}else if(isNumber(a1)){
			return num(Ereal.cero.minus(darg[0]));
		}else if(a1.getType()==DIV){
			return nor(DIV,"/",
					neg(ccc(a1.getFirstChild())),
					ccc(a1.getFirstChild().getNextSibling()));
		}
		AST ret = new CommonAST(new Token(NEG));
		ret.setText("CHS");
		ret.addChild(ccc(a1));
		return ret;
	}

	public static AST nodeEquivInChain(AST chain, AST node) {
		for (; chain != null; chain = chain.getNextSibling()) {
			if (opEquals(node, chain)) {
				return chain;
			}
		}
		return null;
	}

	/**
	 * Devuelve un nodo operador infijo (+,-,*,/,^)
	 * 
	 * @param tipo
	 *            (ver ObjectParserTokenTypes)
	 * @param f
	 *            Nombre del operador
	 * @param a1
	 *            Izquierdo
	 * @param a2
	 *            Derecho
	 * @return Nodo que representa la operacion
	 */
	public static AST nor(int tipo, String f, AST a1, AST a2) {
		CommonAST exp = new CommonAST(new Token(tipo));
		exp.setText(f);
		exp.addChild(ccc(a1));
		exp.addChild(ccc(a2));
		return exp;
	}

	/**
	 * Crea un nodo de tipo numero
	 * 
	 * @param number
	 *            Valor
	 * @return Nodo de tipo numero que representa Valor.
	 */
	public static AST num(Ereal number) {
		CommonAST exp = new CommonAST(new Token(NUMBER));
		exp.setText("" + number);
		return exp;
	}

	public static AST num(double number) {
		return num(new Ereal(""+number));
	}

	public static boolean opEquals(AST n1, AST n2) {
		int t1 = n1.getType();

		// Si no son del mismo tipo, no son iguales
		if (t1 != n2.getType()) {
			return false;
		}
		AST ch11 = n1.getFirstChild();
		AST ch12 = null;
		AST ch21 = n2.getFirstChild();
		AST ch22 = null;

		if (ch11 != null) {
			ch12 = ch11.getNextSibling();
		}
		if (ch21 != null) {
			ch22 = ch21.getNextSibling();
		}

		if (ch11 == null) {
			if (t1 == NUMBER) {
				return java.lang.Double.parseDouble(n1.getText()) == java.lang.Double.parseDouble(n2.getText());
			}
			return n1.getText().equals(n2.getText());
		}

		// Obviamente
		if (ch11 == ch21) {
			return true;
		}
		if (((ch12 == null) && (ch22 != null))
				|| ((ch11 != null) && (ch21 == null))) {
			return false;
		}
		switch (t1) {
		case NEG:
		case INV:
			return opEquals(ch11, ch21);
		case MULT:
		case PLUS:
			return unorderedChainsEquals(n1, n2);
		case ID:
			if (!n1.getText().equals(n2.getText())) {
				return false;
			}
			return orderedChainsEquals(n1, n2);
		}
		return opEquals(ch11, ch21) && opEquals(ch12, ch22);
	}

	public static AST opSiblings(int tipo, String text, AST t) {
		if (t == null) {
			return null;
		}
		AST ret = new CommonAST(new Token(tipo));
		ret.setText(text);
		while (t != null) {
			ret.addChild(t);
			t = t.getNextSibling();
		}
		return ret;
	}

	public static boolean orderedChainsEquals(AST f1, AST f2) {
		int sz1 = f1.getNumberOfChildren();
		if (sz1 != f2.getNumberOfChildren()) {
			return false;
		}
		AST p1 = f1.getFirstChild();
		AST p2 = f2.getFirstChild();
		while (p1 != null) {
			if (!opEquals(p1, p2)) {
				return false;
			}
			p1 = p1.getNextSibling();
			p2 = p2.getNextSibling();
		}
		return true;
	}

	private static AST productCardinality(AST prod) {
		int nc = prod.getNumberOfChildren();
		if (nc == 0) {
			return num(1);
		}
		if (nc == 1) {
			return prod.getFirstChild();
		}
		return prod;
	}

	public static AST remove(AST father, AST child) {
		AST chainPointer = father.getFirstChild();
		if (chainPointer == null) {
			return father;
		}
		AST lastPointer;
		if (chainPointer == child) {
			father.setFirstChild(chainPointer.getNextSibling());
			return father;
		}
		lastPointer = chainPointer;
		for (chainPointer = chainPointer.getNextSibling(); chainPointer != null; chainPointer = chainPointer
				.getNextSibling()) {
			if (chainPointer == child) {
				lastPointer.setNextSibling(chainPointer.getNextSibling());
				return father;
			}
			lastPointer = chainPointer;
		}
		return father;
	}

	public static boolean removeEqualsFromChainsOfFathers(AST fa1, AST fa2) {
		AST ch1 = fa1.getFirstChild();
		AST ch2 = fa2.getFirstChild();
		AST p1 = ch1;
		AST p2 = ch2;
		boolean changed = false;
		while (p2 != null) {
			p1 = caretNodeEquivInChain(ch1, p2);
			if (p1 != null) {
				AST exp1 = getExponentOf(p1);
				AST exp2 = getExponentOf(p2);
				subst(fa1, p1, mkl.ele(getBaseOf(p1), mkl.res(exp1, exp2)));
				remove(fa2, p2);
				changed = true;
			}
			if (p2 != null) {
				p2 = p2.getNextSibling();
			}
		}
		return changed;
	}

	public static AST caretNodeEquivInChain(AST chain, AST node) {
		AST nodeBase = getBaseOf(node);
		while (chain != null) {
			AST nodeChainBase = getBaseOf(chain);
			if (opEquals(nodeBase, nodeChainBase)) {
				return chain;
			}
			chain = chain.getNextSibling();
		}
		return null;
	}

	public static boolean removeNodeEquivFromChainOfFather(AST fa1, AST node) {
		AST p1 = caretNodeEquivInChain(fa1.getFirstChild(), node);
		if (p1 != null) {
			AST exp1 = getExponentOf(p1);
			AST exp2 = getExponentOf(node);
			subst(fa1, p1, mkl.ele(getBaseOf(p1), mkl.res(exp1, exp2)));
			return true;
		}
		return false;
	}

	public static AST res(AST posi, AST nega) {
		if ((posi == null) && (nega == null)) {
			return num(0);
		}
		if(posi==null)
			return neg(ccc(nega));
		if(nega==null)
			return ccc(posi);
		if (bothNumbers(posi, nega)) {
			return num(darg[0].minus(darg[1]));
		}
		if (iComp(posi, Ereal.cero)) {
			return neg(ccc(nega));
		}
		if (iComp(nega, Ereal.cero)) {
			return ccc(posi);
		}
		if (opEquals(posi, nega)) {
			return num(0);
		}
		if (opEquals(posi, neg(nega))) {
			return mul(num(2), posi);
		}
		int t1 = posi.getType();
		int t2 = nega.getType();
		if ((t1 == PLUS) && (t2 == PLUS)) {
			removeEqualsFromChainsOfFathers(posi, nega);
			return nor(MINUS, "-", singleSum(posi), singleSum(nega));
		} else if (t1 == PLUS) {
			if (removeNodeEquivFromChainOfFather(posi, nega)) {
				return singleSum(posi);
			}
		} else if (t2 == PLUS) {
			if (removeNodeEquivFromChainOfFather(nega, posi)) {
				return neg(singleSum(nega));
			}
		} else if (t1 == MINUS) {
			AST posiposi = posi.getFirstChild();
			AST posinega = posiposi.getNextSibling();
			return res(ccc(posiposi), sum(ccc(posinega), ccc(nega)));
		} else if (t2 == MINUS) {
			AST negaposi = nega.getFirstChild();
			AST neganega = negaposi.getNextSibling();
			return res(sum(posi, ccc(neganega)), ccc(negaposi));
		}
		AST ret=null;
		if(exactMode()){
			ret=nor(PLUS,"+",ccc(posi),mkl.neg(ccc(nega)));
			if(!reduceFractions(ret))
				ret=nor(MINUS, "-", ccc(posi), ccc(nega));
			else
				ret=sumaCardinality(ret);
		}else{
			ret=nor(MINUS, "-", ccc(posi), ccc(nega));
		}
		return ret;
	}

	public static AST singleMul(AST prod) {
		AST ch1;

		if (prod.getNumberOfChildren() == 0) {
			//Ha habido simplificacion
			return num(1);
		}

		/*
		 * Quitamos las unidades
		 */
		removeUnitsFromProduct(prod);
		// Primero subir a este nivel los productos que haya dentro
		// y aupar las divisiones
		ch1 = prod.getFirstChild();
		while (ch1 != null) {
			if (iComp(ch1, Ereal.cero)) {
				return num(0);
			}
			if (ch1.getType() == MULT) {
				remove(prod, ch1);
				return mul(prod, ccc(ch1));
			}
			if (ch1.getType() == DIV) {
				AST nume = ch1.getFirstChild();
				AST deno = nume.getNextSibling();
				remove(prod, ch1);
				return div(mul(prod, ccc(nume)), ccc(deno));
			}
			if (ch1.getType() == INV) {
				AST deno = ch1.getFirstChild();
				removeNodeEquivFromChainOfFather(prod, deno);
			}
			ch1 = ch1.getNextSibling();
		}
		
		// No hay productos dentro de este producto, ni divisiones
		// Comprobamos las igualdades entre operandos
		ch1 = prod.getFirstChild();
		while (ch1 != null) {
			// Ahora se solucionan los numeros literales
			AST ch2 = ch1.getNextSibling();
			for (; ch2 != null; ch2 = ch2.getNextSibling()) {
				if (bothNumbers(ch1, ch2)) {
					subst(prod, ch1, num(darg[0].mul(darg[1])));
					remove(prod, ch2);
					continue;
				}

				// Dos iguales
				AST base = getBaseOf(ch1);
				if (opEquals(base, getBaseOf(ch2))) {
					AST e1 = getExponentOf(ch1);
					AST e2 = getExponentOf(ch2);
					AST newch1 = ele(base, sum(e1, e2));
					subst(prod, ch1, newch1);
					remove(prod, ch2);
					ch1 = newch1;
					continue;
				}
			}
			ch1 = ch1.getNextSibling();
		}

		return productCardinality(prod);
	}

	private static void removeUnitsFromProduct(AST prod) {
		AST ch1;
		ch1 = prod.getFirstChild();
		while(ch1!=null){
			if (iComp(ch1, Ereal.uno)) {
				remove(prod, ch1);
				ch1 = ch1.getNextSibling();
				continue;
			}
			ch1=ch1.getNextSibling();
		}
	}

	public static AST singleSum(AST suma) {
		AST ch1;
		
		if(suma.getType()!=PLUS)
			return suma;
		
		if (suma.getNumberOfChildren() == 0) {
			return num(0);
		}
		// Acumulamos sumandos numericos
		ch1 = suma.getFirstChild();
		while (ch1 != null) {
			if (iComp(ch1, Ereal.cero)) {
				remove(suma, ch1);
				ch1 = ch1.getNextSibling();
				continue;
			}
			if (ch1.getType() == PLUS) {
				remove(suma, ch1);
				return sum(suma, ccc(ch1));
			}
			if (ch1.getType() == MINUS) {
				AST posi = ch1.getFirstChild();
				AST nega = posi.getNextSibling();
				remove(suma, ch1);
				return res(sum(suma, ccc(posi)), ccc(nega));
			}
			if (ch1.getType() == NEG) {
				AST nega = ch1.getFirstChild();
				removeNodeEquivFromChainOfFather(suma, nega);
			}
			ch1 = ch1.getNextSibling();
		}
		
		// Comprobamos las igualdades entre operandos
		ch1 = suma.getFirstChild();
		while (ch1 != null) {
			AST ch2 = ch1.getNextSibling();
			for (; ch2 != null; ch2 = ch2.getNextSibling()) {
				// Suma de productos del mismo coeficiente
				AST cel = chainMCD(ch1, ch2);
				if ((cel != null) && (cel.getType() != NUMBER)) {
					AST res = factorizar(ch1, ch2, cel);
					subst(suma, ch1, res);
					remove(suma, ch2);
					ch1 = res;
					continue;
				}
			}
			ch1 = ch1.getNextSibling();
		}
		return sumaCardinality(suma);
	}

	/**
	 * Substitucion de un nodo por otro
	 * 
	 * @param father
	 *            El padre
	 * @param child
	 *            El hijo a sustituir
	 * @param newchild
	 *            El nuevo hijo. Se coloca en el mismo lugar que el anterior.
	 * @return El nuevo arbol con el hijo sustituido (si se encontr�)
	 */
	public static AST subst(AST father, AST child, AST newchild) {
		AST chainPointer = father.getFirstChild();
		AST lastPointer;
		if (chainPointer == child) {
			father.setFirstChild(newchild);
			newchild.setNextSibling(chainPointer.getNextSibling());
			return father;
		}
		lastPointer = chainPointer;
		for (chainPointer = chainPointer.getNextSibling(); chainPointer != null; chainPointer = chainPointer
				.getNextSibling()) {
			if (chainPointer == child) {
				lastPointer.setNextSibling(newchild);
				newchild.setNextSibling(chainPointer.getNextSibling());
				return father;
			}
			lastPointer = chainPointer;
		}
		return father;
	}

	public static AST sum(AST a1, AST a2) {
		return singleSum(joinSumas(nor(PLUS, "+", a1, a2)));
	}

	private static AST sumaCardinality(AST suma) {
		int nc = suma.getNumberOfChildren();
		if (nc == 1) {
			return suma.getFirstChild();
		}
		if (nc == 0) {
			return num(0);
		}
		return suma;
	}

	public static boolean unorderedChainsEquals(AST father1, AST father2) {
		int sz1 = father1.getNumberOfChildren();
		if (sz1 != father2.getNumberOfChildren()) {
			return false;
		}
		Vector nodeList1 = new Vector();
		AST aux = father1.getFirstChild();
		while (aux != null) {
			nodeList1.addElement(aux);
			aux = aux.getNextSibling();
		}
		AST headOfChain2 = father2.getFirstChild();
		AST found;
		do {
			found = nodeEquivInChain(headOfChain2, (AST) nodeList1.elementAt(0));
			nodeList1.removeElementAt(0);
		} while ((nodeList1.size() > 0) && (found != null));

		return found != null;
	}

	public static Vector incogOf(AST exp) {
		Vector ret = new Vector();
		recSearch(ret, exp);
		return ret;
	}

	public static void recSearch(Vector v, AST e) {
		AST child;
		switch (e.getType()) {
		default:
			child = e.getFirstChild();
			while (child != null) {
				recSearch(v, child);
				child = child.getNextSibling();
			}
			break;
		case NUMBER:
		case UNIT:
			break;
		case ID:
			if (e.getNumberOfChildren() == 0) {
				Command s = Core.lookup(e.getText());
				if (s == null) {
					s = new Literal(e.getText());
				}
				if (s instanceof InfixExp) {
					recSearch(v, ((InfixExp) s).getAST());
				} else if (s instanceof Literal) {
					if (!((Literal) s).nombre.equals(e.getText())) {
						recSearch(v, ((Literal) s).getAST());
					} else {
						asSetAdd(v, e.getText());
					}
				} else if (s instanceof Double) {
					asSetAdd(v, e.getText());
				} else if (s instanceof Unit) {
					asSetAdd(v, e.getText());
				} else if (s instanceof TempUnit) {
					asSetAdd(v, e.getText());
				}
			} else {
				child = e.getFirstChild();
				while (child != null) {
					recSearch(v, child);
					child = child.getNextSibling();
				}
			}
		}
	}

	private static void asSetAdd(Vector v, Object o) {
		if (!v.contains(o)) {
			v.addElement(o);
		}
	}
	
	//COLECT
	public static AST colect(AST nodo) {
		nodo = colectChildsOf(nodo);
		AST child = nodo.getFirstChild();
		switch (nodo.getType()) {
		case PLUS:
			return mkl.singleSum(nodo);
		case MINUS:
			return mkl.res(mkl.ccc(child), mkl.ccc(child.getNextSibling()));
		case MULT:
			return mkl.singleMul(nodo);
		case DIV:
			return mkl.div(mkl.ccc(child), mkl.ccc(child.getNextSibling()));
		case CARET:
			return mkl.ele(mkl.ccc(child), mkl.ccc(child.getNextSibling()));
		case ID:
			if (nodo.getFirstChild() != null) {
				return mkl.fun(nodo.getText(), nodo.getFirstChild());
			}
		}
		return nodo;
	}

	public static AST colectChildsOf(AST nodo) {
		AST child = nodo.getFirstChild();
		while (child != null) {
			AST childColected = colect(child);
			if (child != childColected) {
				mkl.subst(nodo, child, childColected);
			}
			child = child.getNextSibling();
		}
		return nodo;
	}

	/**
	 * intenta expandir una expresion. Por ahora solo multiplicacion y potencia.
	 * @param nodo
	 * @return
	 */
	public static AST expand(AST nodo) {
		nodo = expandChildsOf(nodo);
		switch (nodo.getType()) {
		case MULT:
			return mkl.expandMul(mkl.joinProducts(nodo));
		case CARET:
			return mkl.expandCaret(nodo);
		}
		return nodo;
	}

	public static AST expandChildsOf(AST nodo) {
		AST child = nodo.getFirstChild();
		while (child != null) {
			AST childExpanded = expand(child);
			if (child != childExpanded) {
				mkl.subst(nodo, child, childExpanded);
			}
			child = child.getNextSibling();
		}
		return nodo;
	}

}
