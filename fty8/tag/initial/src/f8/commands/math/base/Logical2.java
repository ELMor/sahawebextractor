/*
 * Created on 01-oct-2003
 *
 
 
 */
package f8.commands.math.base;

import f8.commands.Dispatch2;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;

/**
 * @author elinares
 * 
 * 
 * 
 */
public abstract class Logical2 extends Dispatch2 {
	public abstract Ereal logic(Ereal l, Ereal r);

	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		return new Double(logic(a.doubleValue(), b.doubleValue()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfixOperator()
	 */
	public boolean isInfixOperator() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
