package f8.commands.math.exp;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class ALOG extends Dispatch1 implements Derivable {
	public ALOG() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 511;
	}

	public Storable getInstance() {
		return new ALOG();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public Stackable prfDouble(Double xArg) {
		return new Double(xArg.doubleValue().exp10());
	}

	public Stackable prfComplex(Complex x) throws F8Exception {
		return new Complex(new Ereal(10), Ereal.cero).power(x);
	}

	public String toString() {
		return ("ALOG");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST arg, String var) throws F8Exception {
		return mkl.div(
				mkl.mul(DER.deriveFunction(arg, var), 
						mkl.num(Ereal.E.log().inv())), 
				arg);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#undo()
	 */
	public String undo() {
		return "LOG";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
