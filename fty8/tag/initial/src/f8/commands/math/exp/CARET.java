package f8.commands.math.exp;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.InfixExp;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class CARET extends Dispatch2 implements Derivable {

	public CARET() {
		// Aqui estaba systemInstall
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST exp, String var) throws F8Exception {
		AST a1 = exp;
		AST a2 = a1.getNextSibling();
		return mkl.sum(
			mkl.mul(mkl.mul(DER.deriveFunction(a1, var), a2), 
					mkl.ele(a1, mkl.res(a2, mkl.num(1)))), 
			mkl.mul(mkl.mul(mkl.fca("LN", a1), DER.deriveFunction(a2, var)), 
					mkl.ele(a1, a2)));
	}

	public int getID() {
		return 56;
	}

	public Storable getInstance() {
		return new CARET();
	}

	public int getTipo() {
		return ObjectParserTokenTypes.CARET;
	}

	public void loadState(DIS ds) throws IOException {
	}

	// x^y
	public Stackable prfDoubleDouble(Double x, Double y) {
		if (x.doubleValue().gt(Ereal.cero)) {
			return new Double(x.doubleValue().pow(y.doubleValue()));
		} else if (x.doubleValue().isNegative()) {
			int n = y.doubleValue().floor().toInt();
			Ereal ell = x.doubleValue().neg().log();

			if ((n & 1) == 0) {
				return new Double(y.doubleValue().mul(ell).exp());
			} else {
				return new Double(y.doubleValue().mul(ell).exp().neg());
			}
		} else {
			return new Double(Ereal.cero);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.FunArgArg#prfInfixExpDouble(f8.kernel.types.InfixExp,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfInfixExpDouble(InfixExp a, Double b) {
		return new InfixExp(mkl.ele(a.getAST(), mkl.num(b.doubleValue())));
	}

	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b) {
		return new InfixExp(mkl.ele(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn2#f(f8.kernel.types.Unit, double)
	 */
	public Stackable prfUnitDouble(Unit un, Double vexp) {
		return new Unit(
				un.getValor().pow(vexp.doubleValue()), 
				mkl.ele(un.getUnidad(), mkl.num(vexp.doubleValue())));
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("^");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfComplexComplex(f8.kernel.types.Complex,
	 *      f8.kernel.types.Complex)
	 */
	public Stackable prfComplexComplex(Complex a, Complex b) throws F8Exception {
		return a.power(b);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfComplexDouble(f8.kernel.types.Complex,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfComplexDouble(Complex a, Double b) throws F8Exception {
		return a.power(b.doubleValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleComplex(f8.kernel.types.Double,
	 *      f8.kernel.types.Complex)
	 */
	public Stackable prfDoubleComplex(Double a, Complex b) throws F8Exception {
		return new Complex(a.doubleValue(), Ereal.cero).power(b);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfComplexInfixExp(f8.types.Complex, f8.types.InfixExp)
	 */
	public Stackable prfComplexInfixExp(Complex a, InfixExp b)
			throws F8Exception {
		return new InfixExp(mkl.ele(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleInfixExp(f8.types.Double, f8.types.InfixExp)
	 */
	public Stackable prfDoubleInfixExp(Double a, InfixExp b) throws F8Exception {
		return new InfixExp(mkl.ele(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpComplex(f8.types.InfixExp, f8.types.Complex)
	 */
	public Stackable prfInfixExpComplex(InfixExp a, Complex b)
			throws F8Exception {
		return new InfixExp(mkl.ele(a.getAST(), b.getAST()));
	}

}
