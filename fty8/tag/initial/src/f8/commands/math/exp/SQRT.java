package f8.commands.math.exp;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.InfixExp;
import f8.objects.types.Int;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class SQRT extends Dispatch1 implements Derivable {
	public Stackable prfUnit(Unit a) throws F8Exception {
		return new Unit(
				a.getValor().sqrt(), 
				mkl.ele(a.getUnidad(), mkl.num(0.5))
			);
	}

	public String getLaTeX(AST child) {
		return "\\sqrt{"+InfixExp.getLaTex(child)+"}";
	}

	public SQRT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 59;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public Storable getInstance() {
		return new SQRT();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public Stackable prfDouble(Double x) {
		if (x.doubleValue().isNegative()) {
			return new Complex(Ereal.cero, x.doubleValue().neg().sqrt());
		} else {
			return new Double(x.doubleValue().sqrt());
		}
	}

	public String toString() {
		return ("\u0083");
	}

	public Stackable prfInfix(InfixExp a) throws F8Exception {
		return new InfixExp(
					mkl.ele(a.getAST(), mkl.div(mkl.uno, mkl.num(2)))
				);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST arg, String var) throws F8Exception {
		return mkl.div(
			DER.deriveFunction(arg, var), 
			mkl.mul(mkl.num(2), mkl.fca("\u0083", arg))
			);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#undo()
	 */
	public String undo() {
		return "SQ";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.sqrt();
	}

	public Stackable prfInt(Int x) throws F8Exception {
		return new Int( new Ereal(x.n).sqrt().toInt() );
	}
	
}
