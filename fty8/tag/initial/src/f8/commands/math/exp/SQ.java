package f8.commands.math.exp;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.InfixExp;
import f8.objects.types.Int;
import f8.objects.types.Matrix;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class SQ
 extends Dispatch1 implements Derivable {
	
	public SQ() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 512;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public Storable getInstance() {
		return new SQ();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("SQ");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST arg, String var) throws F8Exception {
		return mkl.mul(mkl.mul(mkl.num(2), arg),DER.deriveFunction(arg, var));
	}

	public Stackable prfUnit(Unit a) throws F8Exception {
		return new Unit(a.getValor().sqr(), mkl.ele(a.getUnidad(), mkl.num(2)));
	}

	public Stackable prfInfix(InfixExp a) throws F8Exception {
		return new InfixExp(
				mkl.ele(a.getAST(),mkl.num(2))
				);
	}

	public Stackable prfDouble(Double x) throws F8Exception {
		return new Double(x.doubleValue().sqr());
	}

	public String getLaTeX(AST child) {
		switch(child.getType()){
		default:
			return "{\\left("+InfixExp.getLaTex(child)+"\\right)^2}";
		case ID: 
			return "{{"+InfixExp.getLaTex(child)+"}^2}";
		}
	}

	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.power(Ereal.dos);
	}

	public Stackable prfInt(Int x) throws F8Exception {
		return new Int(x.n*x.n);
	}

	public Stackable prfMatrix(Matrix x) throws F8Exception {
		return x.times(x);
	}
	
	
}
