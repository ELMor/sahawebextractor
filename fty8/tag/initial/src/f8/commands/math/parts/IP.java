package f8.commands.math.parts;


import java.io.IOException;

import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class IP extends Dispatch1 {
	public IP() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3012;
	}

	public String toString() {
		return ("IP");
	}

	public Storable getInstance() {
		return new IP();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		return new Double(a.doubleValue().toInt());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
