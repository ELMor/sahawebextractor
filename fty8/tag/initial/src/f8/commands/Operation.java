package f8.commands;

import f8.exceptions.F8Exception;

public abstract class Operation {
	/**
	 * Comportamiento por defecto de exec es un push
	 * @param extern TODO
	 * 
	 */
	public abstract void exec(Object extern) throws F8Exception;

}
