package f8.commands.symb;

import java.util.Vector;

import antlr.collections.AST;
import f8.commands.math.mkl;
import f8.exceptions.BadArgumentValueException;
import f8.kernel.yacc.ObjectParserTokenTypes;

/**
 * Concentramos aqui las funciones de tratamiento de polinomios
 * @author elinares
 */
public class Polinomios implements ObjectParserTokenTypes {
	
	public static AST ensureGet(Vector cfs, int pos){
		while(cfs.size()<pos+1){
			cfs.addElement(mkl.num(0));
		}
		return (AST)cfs.elementAt(pos);
	}
	
	public static void ensurePut(Vector cfs, int pos, AST c){
		ensureGet(cfs, pos);
		cfs.setElementAt(c, pos);
	}
	
	public static void ensureSum(Vector cfs, int pos, AST a){
		cfs.setElementAt(
			mkl.sum(
				ensureGet(cfs, pos), 
				a)
			, pos);
	}
	
	public static AST coefInMultiply(AST exp, String x){
		AST multi=mkl.num(1);
		for(AST child=exp.getFirstChild();
	    child!=null;
	    child=child.getNextSibling()){
			if(child.getType()==ID && child.getText().equals(x)){
				continue;
			}
			if(child.getType()==CARET 
					&& child.getFirstChild().getType()==ID 
					&& child.getFirstChild().getText().equals(x)){
				continue;
			}
			multi=mkl.mul(multi, child);
		}
		return multi;
	}
	
	public static AST exponentInMultiply(AST exp, String x){
		for(AST child=exp.getFirstChild();
		    child!=null;
		    child=child.getNextSibling()){
			
			if(child.getType()==ID && child.getText().equals(x)){
				return mkl.num(1);
			}
			if(child.getType()==CARET 
					&& child.getFirstChild().getType()==ID 
					&& child.getFirstChild().getText().equals(x)){
				return child.getFirstChild().getNextSibling();
			}
		}
		return mkl.num(0);
	}
	
	public static void coefs(Vector cfs, AST exp, String x) throws BadArgumentValueException{
		//Lo vamos a usar seguro...
		AST child=exp.getFirstChild();
		switch(exp.getType()){
		case ID:
			if(exp.getText().equals(x)){
				ensureSum(cfs, 1, mkl.num(1));
			}else{
				ensureSum(cfs, 0, exp);
			}
			break;
		case PLUS:
			while(child!=null){
				coefs(cfs,child,x);
				child=child.getNextSibling();
			}
			break;
		case MINUS:
			coefs(cfs,child,x);
			child=child.getNextSibling();
			child=mkl.neg(child);
			coefs(cfs,child,x);
			break;
		case MULT:
			AST expon=exponentInMultiply(exp, x);
			if(!mkl.isNumber(expon) || !mkl.darg[0].isIntegral())
				throw new BadArgumentValueException("Exponent is not an integer");
			ensureSum(cfs, mkl.darg[0].toInt(), coefInMultiply(exp, x));
			break;
		case DIV:
			coefs(cfs,
				mkl.mul(
					child.getFirstChild(), 
					mkl.inv(child.getFirstChild().getNextSibling()))
					,x);
			break;
		default:
			throw new BadArgumentValueException("Function is not Polynomial");
		}
	}
	
	public static boolean isPol(AST exp, String x){
		Vector v=new Vector();
		try {
			coefs(v,exp,x);
			return true;
		}catch(BadArgumentValueException bave){
			return false;
		}
	}
}
