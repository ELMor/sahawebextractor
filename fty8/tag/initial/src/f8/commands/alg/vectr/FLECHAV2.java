package f8.commands.alg.vectr;


import java.io.IOException;

import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.F8Vector;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class FLECHAV2 extends Dispatch2 {
	public FLECHAV2() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3041;
	}

	public Storable getInstance() {
		return new FLECHAV2();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("\u008DV2");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		Ereal[] d = new Ereal[2];
		d[0] = a.doubleValue();
		d[1] = b.doubleValue();
		return new F8Vector(d);
	}

}
