package f8.commands.alg.vectr;

import java.io.IOException;

import f8.Core;
import f8.commands.Command;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.F8Vector;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class FLECHAV3 extends Command {
	public FLECHAV3() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3042;
	}

	public Storable getInstance() {
		return new FLECHAV3();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("\u008DV3");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		if (Core.check(3)) {
			Stackable a, b, c;
			a = Core.peek(2);
			b = Core.peek(1);
			c = Core.peek(0);
			if (a instanceof Double && b instanceof Double
					&& c instanceof Double) {
				Core.pop(3);
				Ereal[] d = new Ereal[3];
				d[0] = ((Double) a).doubleValue();
				d[1] = ((Double) b).doubleValue();
				d[2] = ((Double) c).doubleValue();
				Core.push(new F8Vector(d));
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

}
