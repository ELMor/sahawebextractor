package f8.commands.alg.vectr;

import java.io.IOException;
import java.util.Vector;

import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.commands.alg.CommandSequence;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.F8Vector;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class VFLECHA extends Dispatch1 {
	public VFLECHA() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3040;
	}

	public Storable getInstance() {
		return new VFLECHA();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("V\u008D");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch1#prfVector(f8.types.F8Vector)
	 */
	public Stackable prfVector(F8Vector x) throws F8Exception {
		Vector v = new Vector();
		CommandSequence res = new CommandSequence(v);
		for (int i = 0; i < x.size(); i++)
			v.addElement(new Double(x.x[i]));
		return res;
	}

}
