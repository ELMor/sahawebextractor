package f8.commands.alg.matrx;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.commands.alg.IntValue;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Matrix;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class RowSwap extends NonAlgebraic {
	public RowSwap() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 79;
	}

	public Storable getInstance() {
		return new RowSwap();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	// m i j rowswap
	public void exec(Object extern) throws F8Exception {
		if (Core.check(3)) {
			Stackable a = Core.peek(2);
			Stackable b = Core.peek(1);
			Stackable c = Core.peek(0);

			if (a instanceof Matrix && b instanceof IntValue
					&& c instanceof IntValue) {
				Matrix A = (Matrix) a;
				int i = ((IntValue) b).intValue();
				int j = ((IntValue) c).intValue();

				if ((0 <= i) && (i < A.r) && (0 <= j) && (j < A.r)) {
					Core.pop(3);

					double[] t = A.x[i];
					A.x[i] = A.x[j];
					A.x[j] = t;
					Core.push(a);
				} else {
					throw new IndexRangeException(this);
				}
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("ROWSWAP");
	}
}
