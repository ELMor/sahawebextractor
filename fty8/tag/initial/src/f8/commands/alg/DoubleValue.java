package f8.commands.alg;

import f8.objects.types.Ereal;

public interface DoubleValue {
	public Ereal doubleValue();
}
