/*
 * Created on 22-ago-2003
 *
 
 
 */
package f8.commands.prog;

import f8.Core;
import f8.commands.Command;
import f8.commands.prog.brch.IF;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.types.Double;
import f8.objects.types.Int;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class UtilHelper {

	public static int evalCondition() throws F8Exception {
		if (Core.size() == 0) {
			throw new TooFewArgumentsException(new IF(null));
		}

		Command c = Core.pop();

		if ((c instanceof Int || c instanceof Double)
				&& (java.lang.Double.parseDouble(c.toString())) == 0) {
			return 0;
		}

		return 1;
	}
}
