package f8.commands.storage;

import java.io.IOException;

import antlr.collections.AST;
import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.exceptions.UndefinedException;
import f8.objects.Stackable;
import f8.objects.types.InfixExp;
import f8.platform.Calc;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public class RCL extends NonAlgebraic {

	public RCL() {
	}

	public int getID() {
		return 7006;
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds) throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		if(Core.check(1)){
			Stackable a=Core.peek();
			String name;
			if(a instanceof InfixExp && ((name = InfixExp.isLiteral(a)) != null)) {
				Core.pop();
				if(name.equals("PICT")){
					Core.push(Calc.ref.getLCD().getGrob());
				}else{
					Command ps=Core.lookup(name);
					if(ps==null){
						//throw new UndefinedException(this);
					}else if(!(ps instanceof Stackable)){
						throw new UndefinedException(this);
					}else{
						Core.push((Stackable)ps);
					}
				}
			}
		}else{
			throw new TooFewArgumentsException(this);
		}
		
	}

	public String getLaTeX(AST child) {
		return "RCL";
	}

	public String toString() {
		return "RCL";
	}

}
