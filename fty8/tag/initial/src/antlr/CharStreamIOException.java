package antlr;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: CharStreamIOException.java,v 1.5 2008/04/15 17:19:40 elinares690715 Exp $
 */

/**
 * Wrap an IOException in a CharStreamException
 */
public class CharStreamIOException extends CharStreamException {
	public Exception io;

	public CharStreamIOException(Exception io) {
		super(io.getMessage());
		this.io = io;
	}
}
