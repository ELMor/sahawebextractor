package antlr;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: ParserSharedInputState.java,v 1.5 2008/04/15 17:19:39 elinares690715 Exp $
 */

/**
 * This object contains the data associated with an input stream of tokens.
 * Multiple parsers share a single ParserSharedInputState to parse the same
 * stream of tokens.
 */
public class ParserSharedInputState {
	/** Where to get token objects */
	protected TokenBuffer input;

	/** Are we guessing (guessing>0)? */
	public int guessing = 0;

	/** What file (if known) caused the problem? */
	protected String filename;

	public void reset() {
		guessing = 0;
		filename = null;
		input.reset();
	}
}
