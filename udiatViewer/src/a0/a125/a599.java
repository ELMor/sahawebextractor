// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a125;

import a0.a167;
import java.awt.Graphics;
import java.awt.Rectangle;

public abstract class a599
{

    int a603;
    int a604;
    int a605;
    int a606;
    int a366;
    int a367;
    int a607;
    int a608;
    int a609;
    int a610;
    float a611;
    a167 a0;

    public abstract void a299(int i, int j);

    public abstract void a300(int i, int j);

    public abstract void a107(Graphics g, boolean flag);

    public void a112()
    {
        Rectangle rectangle = a0.a208();
        int i = rectangle.x + rectangle.width / 2;
        int j = rectangle.y + rectangle.height / 2;
        a605 -= i;
        a606 -= j;
        a366 -= i;
        a367 -= j;
        a606 = -a606;
        a367 = -a367;
        a605 += i;
        a606 += j;
        a366 += i;
        a367 += j;
    }

    public void a113()
    {
        Rectangle rectangle = a0.a208();
        int i = rectangle.x + rectangle.width / 2;
        int j = rectangle.y + rectangle.height / 2;
        a605 -= i;
        a606 -= j;
        a366 -= i;
        a367 -= j;
        a605 = -a605;
        a366 = -a366;
        a605 += i;
        a606 += j;
        a366 += i;
        a367 += j;
    }

    public void a114()
    {
        Rectangle rectangle = a0.a208();
        int i = rectangle.x + rectangle.width / 2;
        int j = rectangle.y + rectangle.height / 2;
        a605 -= i;
        a606 -= j;
        a366 -= i;
        a367 -= j;
        int k = -a606;
        int l = a605;
        int i1 = -a367;
        int j1 = a366;
        a605 = k + i;
        a606 = l + j;
        a366 = i1 + i;
        a367 = j1 + j;
    }

    public void a115()
    {
        Rectangle rectangle = a0.a208();
        int i = rectangle.x + rectangle.width / 2;
        int j = rectangle.y + rectangle.height / 2;
        a605 -= i;
        a606 -= j;
        a366 -= i;
        a367 -= j;
        int k = a606;
        int l = -a605;
        int i1 = a367;
        int j1 = -a366;
        a605 = k + i;
        a606 = l + j;
        a366 = i1 + i;
        a367 = j1 + j;
    }

    void a600(a167 a167_1)
    {
        a0 = a167_1;
        a603 = a0.a206();
        a604 = a0.a207();
        a611 = a0.a205();
    }

    void a601()
    {
        if(a603 == a0.a206() && a604 == a0.a207() && a611 == a0.a205())
        {
            a607 = a605 - a603;
            a608 = a606 - a604;
            a609 = a366 - a603;
            a610 = a367 - a604;
            return;
        }
        a603 = a0.a206();
        a604 = a0.a207();
        if(a611 == a0.a205())
        {
            a605 = a603 + a607;
            a606 = a604 + a608;
            a366 = a603 + a609;
            a367 = a604 + a610;
            return;
        } else
        {
            a605 = a603 + a602(a607);
            a606 = a604 + a602(a608);
            a366 = a603 + a602(a609);
            a367 = a604 + a602(a610);
            return;
        }
    }

    int a602(int i)
    {
        double d = 1.0D + (double)((a0.a205() - a611) / a611);
        return (int)Math.round(d * (double)i);
    }

    public a599()
    {
    }
}
