// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a125;

import a0.a167;
import java.awt.Color;
import java.awt.Graphics;

// Referenced classes of package a0.a125:
//            a599

public class a787 extends a599
{

    int a305;
    int a306;
    boolean a790;
    a167 a0;

    public a787(a167 a167, boolean flag)
    {
        a600(a167);
        a790 = flag;
    }

    public void a299(int i, int j)
    {
        super.a605 = i;
        super.a606 = j;
        super.a366 = i;
        super.a367 = j;
    }

    public void a300(int i, int j)
    {
        a305 = super.a366;
        a306 = super.a367;
        super.a366 = i;
        super.a367 = j;
    }

    public void a107(Graphics g, boolean flag)
    {
        g.setColor(Color.cyan);
        if(flag)
        {
            a601();
            a788(g);
            return;
        } else
        {
            Color color = new Color(192, 63, 255);
            g.setXORMode(color);
            a789(g, a305, a306);
            a789(g, super.a366, super.a367);
            return;
        }
    }

    void a788(Graphics g)
    {
        int i = super.a366 - super.a605;
        if(i < 0)
        {
            super.a605 += i;
            i = -i;
            super.a366 += i;
        }
        int j = super.a367 - super.a606;
        if(j < 0)
        {
            super.a606 += j;
            j = -j;
            super.a367 += j;
        }
        if(i > 0 && j > 0)
        {
            if(a790)
            {
                g.drawOval(super.a605, super.a606, i, j);
                return;
            }
            g.drawRect(super.a605, super.a606, i, j);
        }
    }

    void a789(Graphics g, int i, int j)
    {
        int k = i - super.a605;
        int l = j - super.a606;
        if(k > 0 && l > 0)
        {
            if(a790)
            {
                g.drawOval(super.a605, super.a606, k, l);
                return;
            }
            g.drawRect(super.a605, super.a606, k, l);
        }
    }
}
