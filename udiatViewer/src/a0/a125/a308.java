// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a125;

import a282.a470;
import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.TextComponent;
import java.awt.TextField;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

// Referenced classes of package a0.a125:
//            a680, a961

class a308 extends Dialog
{
    class a680
        implements ActionListener
    {

        private final a308 a681; /* synthetic field */

        public void actionPerformed(ActionEvent actionevent)
        {
            a681.a310 = a681.a312.getText();
            a681.dispose();
            a681.a311.repaint();
        }

        a680()
        {
        }
    }


    String a310;
    Frame a311;
    TextField a312;

    public a308(Frame frame, String s)
    {
        super(frame, s, true);
        a310 = "";
        a311 = frame;
        a680 a680_1 = new a680(this);
        Panel panel = new Panel();
        a312 = new TextField(35);
        a470 a470_1 = new a470("OK");
        a470_1.a287(a680_1);
        panel.add(a312);
        panel.add(a470_1);
        add("Center", panel);
        pack();
        Dimension dimension = getSize();
        setBounds(256, 200, dimension.width + 5, dimension.height + 15);
        pack();
        setResizable(false);
        enableEvents(64L);
    }

    public void processWindowEvent(WindowEvent windowevent)
    {
        switch(windowevent.getID())
        {
        case 201: 
            dispose();
            break;
        }
        super.processWindowEvent(windowevent);
    }

    public String a309()
    {
        return a310;
    }
}
