// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a125;

import a0.a167;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;

// Referenced classes of package a0.a125:
//            a599

public class a596 extends a599
{

    static final int a304 = 10;
    static final Dimension a360 = new Dimension(50, 30);
    boolean a258;
    int a597;
    int a598;

    public a596(a167 a167_1)
    {
        super.a0 = a167_1;
        a258 = true;
    }

    public void a299(int i, int j)
    {
        super.a605 = i;
        super.a606 = j;
    }

    public void a300(int i, int j)
    {
        if(a258)
        {
            a258 = false;
            return;
        } else
        {
            super.a0.repaint(a597, a598, a360.width, a360.height);
            super.a605 = i;
            super.a606 = j;
            return;
        }
    }

    public void a107(Graphics g, boolean flag)
    {
        Rectangle rectangle = super.a0.a208();
        if(rectangle.contains(super.a605, super.a606))
        {
            a597 = super.a605 + 10;
            if(a597 + a360.width > rectangle.x + rectangle.width)
                a597 = super.a605 - 10 - a360.width;
            a598 = super.a606 + 10;
            if(a598 + a360.height > rectangle.y + rectangle.height)
                a598 = super.a606 - 10 - a360.height;
            g.setColor(Color.cyan);
            g.fillRect(a597, a598, a360.width, a360.height);
            g.setFont(new Font("SansSerif", 1, 11));
            int i = super.a0.a209(super.a605, super.a606);
            String s;
            if(super.a0.a210())
                s = "HU " + i;
            else
                s = "SI " + i;
            Dimension dimension = a185(g, s);
            int j = a597 + (a360.width - dimension.width) / 2;
            int k = a598 + a303(g) + (a360.height - dimension.height) / 2;
            g.setColor(Color.black);
            g.drawString(s, j, k);
        }
    }

    Dimension a185(Graphics g, String s)
    {
        if(s == null)
        {
            return null;
        } else
        {
            FontMetrics fontmetrics = g.getFontMetrics();
            return new Dimension(fontmetrics.stringWidth(s), fontmetrics.getHeight());
        }
    }

    int a303(Graphics g)
    {
        FontMetrics fontmetrics = g.getFontMetrics();
        return fontmetrics.getAscent();
    }

}
