// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;

// Referenced classes of package a0:
//            a510

public class a490
{

    Image a236;
    Image a500;
    Dimension a501;
    Graphics a502;
    Component a421;
    boolean a503;
    boolean a504;
    int a505;
    int a506;
    int a507;
    int a508;
    float a220;
    int a509;

    public a490(Component component)
    {
        a503 = false;
        a504 = false;
        a220 = 2.0F;
        a509 = 200;
        a421 = component;
        a505 = component.getSize().width / 2 - a509 / 2;
        a506 = component.getSize().height / 2 - a509 / 2;
    }

    public void a220(boolean flag)
    {
        if(flag)
            a220 += 0.1F;
        else
            a220 -= 0.1F;
        if(a220 > 3F)
            a220 = 3F;
        if(a220 < 1.0F)
            a220 = 1.0F;
        a111();
        a421.repaint();
    }

    public void a491(boolean flag)
    {
        int i = a509;
        if(flag)
            a509 += 10;
        else
            a509 -= 10;
        if(a509 > 250)
            a509 = 250;
        if(a509 < 30)
            a509 = 30;
        a505 -= (a509 - i) / 2;
        a506 -= (a509 - i) / 2;
        a111();
        a421.repaint();
    }

    public void a492(int i, int j)
    {
        a503 = a499(i, j);
        a507 = i - a505;
        a508 = j - a506;
    }

    public void a109(int i, int j)
    {
        if(a503)
        {
            a504 = true;
            a421.setCursor(new Cursor(12));
            a505 = i - a507;
            a506 = j - a508;
            a421.repaint();
        }
    }

    public void a493()
    {
        a421.setCursor(Cursor.getDefaultCursor());
        a503 = false;
        a504 = false;
    }

    public Graphics a494()
    {
        Dimension dimension = a421.getSize();
        if(a500 == null || dimension.width != a501.width || dimension.height != a501.height)
        {
            a500 = a421.createImage(dimension.width, dimension.height);
            a501 = dimension;
            a502 = a500.getGraphics();
        }
        a502.setColor(a421.getBackground());
        a502.fillRect(0, 0, dimension.width, dimension.height);
        a502.setColor(a421.getForeground());
        return a502;
    }

    public void a495(Graphics g)
    {
        if(a500 != null)
            g.drawImage(a500, 0, 0, null);
    }

    public void a111()
    {
        a236 = null;
    }

    public void a107(Graphics g, Image image, int i, int j, int k, int l)
    {
        if(image == null)
            return;
        if(a509 > k)
            a509 = k - 2;
        if(a509 > l)
            a509 = l - 2;
        int i1 = Math.round(((float)a509 * (a220 - 1.0F)) / a220 / 2.0F);
        if(a505 < i - i1)
            a505 = i - i1;
        if(a506 < j - i1)
            a506 = j - i1;
        if(a505 > ((i + k) - a509) + i1)
            a505 = ((i + k) - a509) + i1;
        if(a506 > ((j + l) - a509) + i1)
            a506 = ((j + l) - a509) + i1;
        if(a504 || a236 == null)
            a236 = a497(a498(image, a496(a505, i), a496(a506, j)), true);
        g.drawImage(a236, a505, a506, a509, a509, a421);
        g.setColor(Color.cyan);
        g.drawRect(a505, a506, a509 - 1, a509 - 1);
    }

    private final int a496(int i, int j)
    {
        float f = (a220 - 1.0F) / 2.0F;
        int k = Math.round((f * (float)a509) / a220);
        return (i + k) - j;
    }

    private Image a497(Image image, boolean flag)
    {
        if(a220 < 1.2F)
            return image;
        if(flag)
            return image;
        else
            return a421.createImage(new FilteredImageSource(image.getSource(), new a510()));
    }

    private Image a498(Image image, int i, int j)
    {
        int k = Math.round((float)a509 / a220);
        return a421.createImage(new FilteredImageSource(image.getSource(), new CropImageFilter(i, j, k, k)));
    }

    private boolean a499(int i, int j)
    {
        Rectangle rectangle = new Rectangle(a505, a506, a509, a509);
        return rectangle.contains(i, j);
    }
}
