// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;

public class a674
{

    Component a421;
    Image a236;
    boolean a675;
    boolean a199;
    int a676;
    int a677;
    int a678;
    int a679;

    public a674(Component component)
    {
        a675 = false;
        a199 = true;
        a421 = component;
        a678 = 0;
        a679 = 0;
    }

    public void a492(int i, int j)
    {
        if(a675)
        {
            return;
        } else
        {
            a421.setCursor(new Cursor(13));
            a676 = i;
            a677 = j;
            return;
        }
    }

    public void a109(int i, int j)
    {
        if(a675)
            return;
        a678 = i - a676;
        if(a678 < 0)
            a678 = 0;
        if(a679 < 0)
            a679 = 0;
        a679 = j - a677;
        a421.repaint();
    }

    public void a493()
    {
        if(a675)
        {
            return;
        } else
        {
            a421.setCursor(Cursor.getDefaultCursor());
            return;
        }
    }

    public void a111()
    {
        a236 = null;
        a675 = false;
        a199 = true;
    }

    public void a220()
    {
        if(a678 == 0 || a679 == 0)
        {
            return;
        } else
        {
            a675 = true;
            a199 = true;
            a421.repaint();
            return;
        }
    }

    public boolean a675()
    {
        return a675;
    }

    public void a199(Graphics g)
    {
        if(a199)
        {
            a199 = false;
            g.setColor(a421.getBackground());
            g.fillRect(0, 0, a421.getSize().width, a421.getSize().height);
            g.setColor(a421.getForeground());
        }
    }

    public void a107(Graphics g, Image image, int i, int j, int k, int l)
    {
        if(a676 < i)
            a676 = i;
        if(a677 < j)
            a677 = j;
        int i1 = (i + k) - a676 - 1;
        int j1 = (j + l) - a677 - 1;
        if(a678 > i1)
            a678 = i1;
        if(a679 > j1)
            a679 = j1;
        if(a675)
        {
            if(image == null)
                return;
            Image image1;
            if(a236 == null)
                a236 = image1 = a498(image, a676 - i, a677 - j);
            int k1 = a678 * 4;
            int l1 = a679 * 4;
            int i2 = a421.getSize().width;
            int j2 = a421.getSize().height;
            int k2;
            int i3;
            if(i2 > j2)
            {
                if(l1 > j2)
                {
                    k2 = j2;
                    int l2 = k1 * j2;
                    l2 /= l1;
                    l1 = k2;
                    k1 = l2;
                } else
                {
                    k2 = l1;
                }
                if(k1 > i2)
                {
                    i3 = i2;
                    k2 = l1 * i2;
                    k2 /= k1;
                } else
                {
                    i3 = k1;
                }
            } else
            {
                if(k1 > i2)
                {
                    i3 = i2;
                    k2 = l1 * i2;
                    k2 /= k1;
                    l1 = k2;
                    k1 = i3;
                } else
                {
                    i3 = k1;
                }
                if(l1 > j2)
                {
                    k2 = j2;
                    i3 = k1 * j2;
                    i3 /= l1;
                } else
                {
                    k2 = l1;
                }
            }
            int j3 = (i2 - i3) / 2;
            int k3 = (j2 - k2) / 2;
            g.drawImage(a236, j3, k3, i3, k2, a421);
            return;
        } else
        {
            g.setColor(Color.cyan);
            g.drawRect(a676, a677, a678, a679);
            return;
        }
    }

    private Image a497(Image image)
    {
        return image;
    }

    private Image a498(Image image, int i, int j)
    {
        return a421.createImage(new FilteredImageSource(image.getSource(), new CropImageFilter(i, j, a678, a679)));
    }
}
