// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a282;

import java.awt.Component;
import java.awt.Container;
import java.awt.Panel;

// Referenced classes of package a282:
//            a429

public class a540 extends Component
{

    String a545;

    public void a541(String s)
    {
        a545 = s;
        a429 a429_1 = a429.a430;
        if(s == null)
        {
            a429_1.a432(this);
            return;
        } else
        {
            a429_1.a431(this);
            return;
        }
    }

    public String a542()
    {
        return a545;
    }

    public Panel a543()
    {
        for(Object obj = this; obj != null; obj = ((Component) (obj)).getParent())
            if(obj instanceof Panel)
                return (Panel)obj;

        return null;
    }

    public Panel a544()
    {
        Panel panel = a543();
        Panel panel1 = null;
        for(Container container = panel.getParent(); container != null; container = container.getParent())
        {
            if(!(container instanceof Panel))
                continue;
            panel1 = (Panel)container;
            break;
        }

        if(panel1 == null)
            return null;
        Component acomponent[] = panel1.getComponents();
        int i = 0;
        int j = acomponent.length;
        while(!acomponent[i].equals(panel)) 
            if(++i == j)
                return null;
        j--;
        if(i < j)
            i++;
        else
            i--;
        Component component = acomponent[i];
        if(component instanceof Panel)
            return (Panel)component;
        else
            return null;
    }

    public a540()
    {
    }
}
