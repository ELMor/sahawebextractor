// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a282;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Label;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;

// Referenced classes of package a282:
//            a969

public class a906 extends Component
{
    private final class a969 extends Component
    {

        a969()
        {
        }
    }


    static final int a910 = 4;
    int a484;
    String a485;
    Image a486;
    int a911;
    protected static final Component a421;
    protected static final MediaTracker a489;

    public a906()
    {
        a911 = 1;
        a485 = null;
        a486 = null;
    }

    public a906(String s)
    {
        a911 = 1;
        a485 = s;
        a486 = null;
    }

    public a906(URL url, String s)
    {
        a911 = 1;
        a485 = s;
        a909(url, s);
    }

    public String a471()
    {
        return a485;
    }

    public void a472(String s)
    {
        if(s.equals(a485))
        {
            return;
        } else
        {
            a485 = s;
            invalidate();
            repaint();
            return;
        }
    }

    public int a907()
    {
        return a911;
    }

    public synchronized void a908(int i)
    {
        switch(i)
        {
        case 0: // '\0'
        case 1: // '\001'
        case 2: // '\002'
            a911 = i;
            return;
        }
        throw new IllegalArgumentException("improper alignment: " + i);
    }

    public Image a473()
    {
        return a486;
    }

    public void a474(URL url, String s)
    {
        a485 = s;
        a909(url, s);
        invalidate();
        repaint();
    }

    public void a475(int i)
    {
        a484 = i;
    }

    public void paint(Graphics g)
    {
        int i = getSize().width;
        int j = getSize().height;
        java.awt.Color color = getBackground();
        g.setColor(color);
        g.fillRect(0, 0, i, j);
        if(a486 == null)
        {
            a481(g);
            return;
        } else
        {
            int k = (i - a486.getWidth(this)) / 2;
            int l = (j - a486.getHeight(this)) / 2;
            g.drawImage(a486, k, l, this);
            return;
        }
    }

    public Dimension getPreferredSize()
    {
        if(a486 == null)
        {
            return a482();
        } else
        {
            int i = a486.getWidth(this) + 4;
            int j = a486.getHeight(this) + 4;
            return new Dimension(i, j);
        }
    }

    public Dimension getMinimumSize()
    {
        return getPreferredSize();
    }

    private void a909(URL url, String s)
    {
        if(url == null)
            a486 = getToolkit().getImage(s);
        else
            try
            {
                URL url1 = new URL(url, s);
                a486 = getToolkit().getImage(url1);
            }
            catch(MalformedURLException malformedurlexception)
            {
                System.err.println("Container. Icon not found");
                System.err.println(malformedurlexception);
                a486 = null;
            }
        synchronized(a489)
        {
            a489.addImage(a486, 0);
            try
            {
                a489.waitForID(0, 5000L);
            }
            catch(InterruptedException _ex)
            {
                a486 = null;
                s = "<>";
            }
            a489.removeImage(a486, 0);
        }
    }

    private void a481(Graphics g)
    {
        if(a485 == null)
            return;
        int i = getSize().width;
        int j = getSize().height;
        java.awt.Font font = getFont();
        if(font != null)
        {
            FontMetrics fontmetrics = getFontMetrics(getFont());
            g.setColor(getForeground());
            if(a911 == 0)
                g.drawString(a485, 3, ((j / 2 + fontmetrics.getHeight() / 2) - fontmetrics.getMaxDescent()) + 1);
            if(a911 == 1)
                g.drawString(a485, i / 2 - fontmetrics.stringWidth(a485) / 2, ((j / 2 + fontmetrics.getHeight() / 2) - fontmetrics.getMaxDescent()) + 1);
            if(a911 == 2)
                g.drawString(a485, i - fontmetrics.stringWidth(a485), ((j / 2 + fontmetrics.getHeight() / 2) - fontmetrics.getMaxDescent()) + 1);
        }
    }

    private Dimension a482()
    {
        if(a485 == null)
            if(a484 == 0)
                return new Dimension(4, 4);
            else
                return new Dimension(a484, 4);
        java.awt.Font font = getFont();
        if(font != null)
        {
            FontMetrics fontmetrics = getFontMetrics(getFont());
            if(a484 == 0)
                return new Dimension(fontmetrics.stringWidth(a485) + 4, fontmetrics.getHeight() + 2);
            else
                return new Dimension(a484, fontmetrics.getHeight() + 2);
        }
        if(a484 == 0)
            return new Dimension(4, 4);
        else
            return new Dimension(a484, 4);
    }

    static 
    {
        a421 = new a969();
        a489 = new MediaTracker(a421);
    }
}
