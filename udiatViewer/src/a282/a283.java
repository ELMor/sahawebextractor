// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a282;

import java.awt.AWTEventMulticaster;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;

public class a283 extends Thread
{

    int a289;
    int a290;
    int a291;
    ActionListener a292;

    public void a284(int i)
    {
        if(i < 0)
            i = -i;
        if(i < 100)
        {
            a291 = 1;
            a290 = i;
            return;
        }
        if(i < 1000)
        {
            a291 = 10;
            a290 = i / 10;
            return;
        }
        if(i < 10000)
        {
            a291 = 100;
            a290 = i / 100;
            return;
        } else
        {
            a291 = 1000;
            a290 = i / 1000;
            return;
        }
    }

    public int a285()
    {
        return a290 * a291;
    }

    public void a286()
    {
        a289 = 0;
    }

    public void run()
    {
        while(a289 < a290) 
        {
            if(a292 != null)
                a289++;
            try
            {
                Thread.sleep(a291);
            }
            catch(InterruptedException _ex)
            {
                System.err.println("TipTimer thread has been interrupted.");
            }
        }
        if(a292 != null)
            a292.actionPerformed(new ActionEvent(this, 1001, "showTipWindow"));
    }

    public void a287(ActionListener actionlistener)
    {
        a292 = AWTEventMulticaster.add(a292, actionlistener);
    }

    public void a288(ActionListener actionlistener)
    {
        a292 = AWTEventMulticaster.remove(a292, actionlistener);
        a292 = null;
    }

    public a283()
    {
        a290 = 15;
        a291 = 100;
    }
}
