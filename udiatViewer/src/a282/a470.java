// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a282;

import java.awt.AWTEvent;
import java.awt.AWTEventMulticaster;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;

// Referenced classes of package a282:
//            a540, a456

public class a470 extends a540
{
    private final class a456 extends Component
    {

        a456()
        {
        }
    }


    static final int a483 = 16;
    int a484;
    String a485;
    Image a486;
    protected boolean a487;
    protected boolean a488;
    ActionListener a292;
    protected static final Component a421;
    protected static final MediaTracker a489;

    public a470()
    {
        this("");
    }

    public a470(String s)
    {
        a487 = false;
        a488 = true;
        a486 = null;
        a485 = s;
        enableEvents(16L);
    }

    public a470(String s, boolean flag)
    {
        a487 = false;
        a488 = true;
        a486 = null;
        a485 = s;
        a488 = flag;
        enableEvents(16L);
    }

    public a470(URL url, String s, String s1)
    {
        a487 = false;
        a488 = true;
        a485 = s1;
        enableEvents(16L);
        a478(url, s);
    }

    public a470(URL url, String s, String s1, boolean flag)
    {
        a487 = false;
        a488 = true;
        a485 = s1;
        a488 = flag;
        enableEvents(16L);
        a478(url, s);
    }

    public String a471()
    {
        return a485;
    }

    public void a472(String s)
    {
        a485 = s;
        invalidate();
        repaint();
    }

    public Image a473()
    {
        return a486;
    }

    public void a474(URL url, String s, String s1)
    {
        a485 = s1;
        a478(url, s);
        invalidate();
        repaint();
    }

    public void a474(URL url, String s, String s1, boolean flag)
    {
        a485 = s1;
        a488 = flag;
        a478(url, s);
        invalidate();
        repaint();
    }

    public void a475(int i)
    {
        a484 = i;
    }

    public void a476(boolean flag)
    {
        a487 = flag;
        repaint();
    }

    public boolean a477()
    {
        return a487;
    }

    public void paint(Graphics g)
    {
        int i = getSize().width - 1;
        int j = getSize().height - 1;
        Color color = getBackground();
        Color color1;
        Color color2;
        if(a487)
        {
            color1 = color.darker();
            color2 = color.brighter();
        } else
        {
            color1 = color.brighter();
            color2 = color.darker();
        }
        g.setColor(color);
        g.fillArc(0, 0, 16, j, 90, 180);
        g.fillArc(i - 16, 0, 16, j, 270, 180);
        g.fillRect(8, 0, i - 16, j);
        g.setColor(color1);
        g.drawLine(8, 0, i - 8, 0);
        g.setColor(color2);
        g.drawLine(8, j, i - 8, j);
        g.setColor(color1);
        g.drawArc(0, 0, 16, j, 90, 140);
        g.setColor(color2);
        g.drawArc(0, 0, 16, j, 230, 40);
        g.setColor(color1);
        g.drawArc(i - 16, 0, 16, j, 50, 40);
        g.setColor(color2);
        g.drawArc(i - 16, 0, 16, j, 270, 140);
        if(a486 == null)
        {
            a481(g);
            return;
        }
        int k = (i - a479()) / 2;
        int l = (j - a480()) / 2;
        if(a487)
        {
            g.drawImage(a486, k + 1, l + 1, this);
            return;
        } else
        {
            g.drawImage(a486, k, l, this);
            return;
        }
    }

    public Dimension getPreferredSize()
    {
        if(a486 == null)
        {
            return a482();
        } else
        {
            int i = a479() + 16;
            int j = a480() + 8;
            return new Dimension(i, j);
        }
    }

    public Dimension getMinimumSize()
    {
        return getPreferredSize();
    }

    public void a287(ActionListener actionlistener)
    {
        a292 = AWTEventMulticaster.add(a292, actionlistener);
        enableEvents(16L);
    }

    public void a288(ActionListener actionlistener)
    {
        a292 = AWTEventMulticaster.remove(a292, actionlistener);
    }

    public void processMouseEvent(MouseEvent mouseevent)
    {
        switch(mouseevent.getID())
        {
        case 503: 
        case 504: 
        default:
            break;

        case 501: 
            if(a488)
                a487 = true;
            else
                a487 = !a487;
            repaint();
            break;

        case 502: 
            if(a292 != null)
                a292.actionPerformed(new ActionEvent(this, 1001, a485));
            if(a488 && a487)
            {
                a487 = false;
                repaint();
            }
            break;

        case 505: 
            if(a488 && a487)
            {
                a487 = false;
                repaint();
            }
            break;
        }
        super.processMouseEvent(mouseevent);
    }

    private void a478(URL url, String s)
    {
        if(url == null)
            a486 = getToolkit().getImage(s);
        else
            try
            {
                URL url1 = new URL(url, s);
                a486 = getToolkit().getImage(url1);
            }
            catch(MalformedURLException malformedurlexception)
            {
                System.err.println("Container. Icon not found");
                System.err.println(malformedurlexception);
                a485 = "<>";
                a486 = null;
                return;
            }
        synchronized(a489)
        {
            a489.addImage(a486, 0);
            try
            {
                a489.waitForID(0, 5000L);
            }
            catch(InterruptedException _ex)
            {
                a486 = null;
                a485 = "<>";
            }
            a489.removeImage(a486, 0);
        }
    }

    private int a479()
    {
        int i = a486.getWidth(this);
        if(i < 0)
            return 16;
        else
            return i;
    }

    private int a480()
    {
        int i = a486.getHeight(this);
        if(i < 0)
            return 16;
        else
            return i;
    }

    private void a481(Graphics g)
    {
        int i = getSize().width - 1;
        int j = getSize().height - 1;
        java.awt.Font font = getFont();
        if(font != null)
        {
            FontMetrics fontmetrics = getFontMetrics(getFont());
            g.setColor(getForeground());
            if(a487)
            {
                g.drawString(a485, (i / 2 - fontmetrics.stringWidth(a485) / 2) + 1, ((j / 2 + fontmetrics.getHeight() / 2) - fontmetrics.getMaxDescent()) + 2);
                return;
            }
            g.drawString(a485, i / 2 - fontmetrics.stringWidth(a485) / 2, ((j / 2 + fontmetrics.getHeight() / 2) - fontmetrics.getMaxDescent()) + 1);
        }
    }

    private Dimension a482()
    {
        java.awt.Font font = getFont();
        if(font != null)
        {
            FontMetrics fontmetrics = getFontMetrics(getFont());
            if(a484 == 0)
                return new Dimension(fontmetrics.stringWidth(a485) + 16, fontmetrics.getHeight() + 8);
            else
                return new Dimension(a484, fontmetrics.getHeight() + 8);
        }
        if(a484 == 0)
            return new Dimension(16, 16);
        else
            return new Dimension(a484, 16);
    }

    static 
    {
        a421 = new a456();
        a489 = new MediaTracker(a421);
    }
}
