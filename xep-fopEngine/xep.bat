@echo off
rem   This batch file encapsulates a standard XEP call. 

set CP=C:\tmp\xep\lib\xep.jar;C:\tmp\xep\lib\saxon.jar;C:\tmp\xep\lib\xt.jar

if x%OS%==xWindows_NT goto WINNT
"c:\java\j2sdk1.4.2_05\jre\bin\java" -classpath "%CP%" com.renderx.xep.XSLDriver "-DCONFIG=C:\tmp\xep\xep.xml" %1 %2 %3 %4 %5 %6 %7 %8 %9
goto END

:WINNT
"c:\java\j2sdk1.4.2_05\jre\bin\java" -classpath "%CP%" com.renderx.xep.XSLDriver "-DCONFIG=C:\tmp\xep\xep.xml" %*

:END


set CP=
