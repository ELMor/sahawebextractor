/*
 * PSText - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.IOException;

public class PSText {
	public static final int TRM_FILL = 0;

	public static final int TRM_STROKE = 1;

	public static final int TRM_CLIP = 2;

	String value;

	float letterSpacing = 0.0F;

	float wordSpacing = 0.0F;

	int renderMode = 0;

	PSText(String string, float f, float f_0_, int i) {
		value = string;
		letterSpacing = f;
		wordSpacing = f_0_;
		renderMode = i;
	}

	public void write(PSOutputStream psoutputstream) throws IOException {
		char[] cs = new char[value.length() * 4];
		int i = 0;
		for (int i_1_ = 0; i_1_ != value.length(); i_1_++) {
			char c = value.charAt(i_1_);
			switch (c) {
			case '\0':
			case '\001':
			case '\002':
			case '\003':
			case '\004':
			case '\005':
			case '\006':
			case '\007':
			case '\010':
			case '\t':
			case '\n':
			case '\013':
			case '\014':
			case '\r':
			case '\016':
			case '\017':
			case '\020':
			case '\021':
			case '\022':
			case '\023':
			case '\024':
			case '\025':
			case '\026':
			case '\027':
			case '\030':
			case '\031':
			case '\032':
			case '\033':
			case '\034':
			case '\035':
			case '\036':
			case '\037':
				cs[i++] = '\\';
				cs[i++] = '0';
				cs[i++] = (char) (48 + (c >> 3 & 0x7));
				cs[i++] = (char) (48 + (c & 0x7));
				break;
			case '(':
			case ')':
			case '\\':
				cs[i++] = '\\';
				cs[i++] = c;
				break;
			default:
				cs[i++] = c;
			}
		}
		String string = new String(cs, 0, i);
		switch (renderMode) {
		case 0:
			if (letterSpacing == 0.0F) {
				if (wordSpacing == 0.0F)
					psoutputstream.println("(" + string + ") show");
				else
					psoutputstream.println("" + wordSpacing + " 0 32 ("
							+ string + ") widthshow");
			} else if (wordSpacing == 0.0F)
				psoutputstream.println("" + letterSpacing + " 0 (" + string
						+ ") ashow");
			else
				psoutputstream.println("" + wordSpacing + " 0 32 "
						+ letterSpacing + " 0 (" + string + ") awidthshow");
			break;
		case 1:
			psoutputstream.println("(" + string + ") false charpath");
			break;
		case 2:
			psoutputstream.println("(" + string + ") true charpath");
			break;
		}
	}
}