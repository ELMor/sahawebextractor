/*
 * ASCIIHEX_Encode - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ASCIIHEX_Encode extends FilterOutputStream {
	private static int charsPerRow = 64;

	protected static final char[] hd = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	private int offset = 0;

	public ASCIIHEX_Encode(OutputStream outputstream) {
		super(outputstream);
	}

	public void write(int i) throws IOException {
		if (offset > charsPerRow) {
			super.write(10);
			offset = 0;
		}
		super.write(hd[(i & 0xf0) >> 4]);
		super.write(hd[i & 0xf]);
		offset += 2;
	}

	public void write(byte[] is) throws IOException {
		write(is, 0, is.length);
	}

	public void write(byte[] is, int i, int i_0_) throws IOException {
		for (int i_1_ = i; i_1_ < i + i_0_; i_1_++)
			write(is[i_1_]);
	}

	public void EOD() throws IOException {
		super.write(62);
	}
}