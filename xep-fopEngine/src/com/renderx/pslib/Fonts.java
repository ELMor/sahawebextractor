/*
 * Fonts - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.IOException;
import java.util.Enumeration;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.Encoding;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.Metric;
import com.renderx.util.Hashtable;

public class Fonts {
	private Hashtable reencodedFonts = new Hashtable();

	String getPSFontName(FontDescriptor fontdescriptor, int i) {
		String string = fontdescriptor.record.getMetric().fontName + "." + i;
		reencodedFonts.put(string, fontdescriptor);
		return string;
	}

	void write(PSOutputStream psoutputstream, Hashtable hashtable)
			throws IOException {
		Enumeration enumeration = hashtable.keys();
		while (enumeration.hasMoreElements()) {
			FontDescriptor fontdescriptor = (FontDescriptor) hashtable
					.get(enumeration.nextElement());
			if (fontdescriptor.record.embed) {
				if (!fontdescriptor.record.getMetric().isCFF)
					psoutputstream.print("\n%%BeginResource: font "
							+ (fontdescriptor.record.getMetric().fontName)
							+ "\n");
				switch (fontdescriptor.record.datatype) {
				case 2:
					new TTFont(fontdescriptor).write(psoutputstream);
					break;
				case 1:
					new T1Font(fontdescriptor).write(psoutputstream);
					break;
				case 3:
				case 4:
					if (psoutputstream.doc.LANGUAGE_LEVEL >= 3)
						new CFFFont(fontdescriptor).write(psoutputstream);
					else
						psoutputstream.doc.errorHandler
								.error("CFF font embedding is a feature of Postscript Level 3; font "
										+ fontdescriptor.record.getMetric().fontName
										+ " not embed");
					break;
				default:
					psoutputstream.doc.errorHandler
							.error("Unsupported font type in PSDocument.setfont() for font "
									+ fontdescriptor.record.getMetric().fontName);
				}
				if (!fontdescriptor.record.getMetric().isCFF)
					psoutputstream.print("\n%%EndResource\n");
			}
		}
		enumeration = reencodedFonts.keys();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			int i = new Integer(string.substring(string.lastIndexOf(".") + 1))
					.intValue();
			FontDescriptor fontdescriptor = (FontDescriptor) reencodedFonts
					.get(string);
			Encoding encoding = (Encoding) fontdescriptor.encodingTable.get(i);
			psoutputstream.print("/" + string + ".enc [ ");
			Encoding encoding_0_ = new Encoding();
			boolean bool = false;
			Metric metric = fontdescriptor.record.getMetric();
			for (int i_1_ = 0; i_1_ < 256; i_1_++) {
				char c = encoding.byte2uni[i_1_];
				if (c != 0 && fontdescriptor.used.checkGlyph(c)) {
					encoding_0_.uni2byte.put(c, new Integer(i_1_));
					encoding_0_.table[i_1_] = metric.ucm(c);
					encoding_0_.byte2uni[i_1_] = c;
					bool = true;
				}
			}
			if (!bool) {
				CharMetrics charmetrics = metric.ucm(' ');
				do {
					if (charmetrics == null) {
						charmetrics = metric.missingGlyph;
						if (charmetrics == null)
							break;
					}
				} while (false);
				encoding_0_.assignCode(' ', charmetrics);
			}
			int i_2_ = encoding_0_.getLastCode();
			for (int i_3_ = 0; i_3_ <= i_2_; i_3_++) {
				if (i_3_ % 8 == 7)
					psoutputstream.println();
				else
					psoutputstream.print(" ");
				if (encoding_0_.table[i_3_] == null)
					psoutputstream.print("/.notdef");
				else
					psoutputstream.print("/" + encoding_0_.table[i_3_].n);
			}
			psoutputstream.println("] def");
			psoutputstream.println("/" + string + " /"
					+ fontdescriptor.record.getMetric().fontName + " " + string
					+ ".enc REENCODEFONT");
		}
	}
}