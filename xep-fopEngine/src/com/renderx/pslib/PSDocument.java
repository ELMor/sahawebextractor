/*
 * PSDocument - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.Encoding;
import com.renderx.fonts.FontCatalog;
import com.renderx.fonts.FontConfigurationException;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.FontRecord;
import com.renderx.fonts.U28;
import com.renderx.graphics.EPSImage;
import com.renderx.graphics.ImageFactory;
import com.renderx.graphics.ImageFormatException;
import com.renderx.graphics.UnregisteredMIMETypeException;
import com.renderx.sax.Serializer;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.Base64OutputStream;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;

public class PSDocument {
	static final String BACKUP_PAGE_DEVICE_NAME = "XEPPS_BackupPageDevice";

	static final String DOCUMENT_PAGE_DEVICE_NAME = "XEPPS_DocumentPageDevice";

	public boolean DOCUMENT_MANAGER_COMMENTS = false;

	public int LANGUAGE_LEVEL = 3;

	public boolean UNICODE_ANNOTATIONS = true;

	public boolean CLONE_EPS = true;

	public boolean DROP_UNUSED_DESTINATIONS = true;

	final ImageFactory imageFactory;

	final FontCatalog fontCatalog;

	final ErrorHandler errorHandler;

	PSOutputStream out = null;

	PSOutputStream targetStream = null;

	private Info info = null;

	private Pages pages = null;

	private Images images = null;

	private Fonts fonts = null;

	private SpotColors spotColors = null;

	private Names names = null;

	private Bookmarks bookmarks = null;

	private final Hashtable pagedevice = new Hashtable();

	private Hashtable currentDevice = null;

	private final Hashtable pageDeviceEntries = new Hashtable();

	private final Hashtable pageboundaries = new Hashtable();

	private Hashtable currentPageBoundaries = null;

	private final List printermarks = new List();

	private List currentPrinterMarks = null;

	private float letterSpacing = 0.0F;

	private float wordSpacing = 0.0F;

	private float fontSize = 0.0F;

	private float fontStretch = 1.0F;

	int textRenderMode = 0;

	String initialDestination = null;

	String zoom = null;

	String viewMode = null;

	float firstPageHeight = 0.0F;

	public U28 u28;

	class ImageSizeSetterHandler implements ContentHandler {
		private final ContentHandler parent;

		private final String height;

		private final String width;

		ImageSizeSetterHandler(ContentHandler contenthandler, String string,
				String string_0_) {
			parent = contenthandler;
			height = string_0_;
			width = string;
		}

		public void startDocument() throws SAXException {
			parent.startDocument();
			AttributesImpl attributesimpl = new AttributesImpl();
			parent.startPrefixMapping("svg", "http://www.w3.org/2000/svg");
			attributesimpl.addAttribute("", "width", "width", "CDATA", width);
			attributesimpl
					.addAttribute("", "height", "height", "CDATA", height);
			parent.startElement("http://www.w3.org/2000/svg", "svg", "svg",
					attributesimpl);
		}

		public void endDocument() throws SAXException {
			parent.endElement("http://www.w3.org/2000/svg", "svg", "svg");
			parent.endPrefixMapping("svg");
			parent.endDocument();
		}

		public void setDocumentLocator(Locator locator) {
			parent.setDocumentLocator(locator);
		}

		public void startPrefixMapping(String string, String string_1_)
				throws SAXException {
			parent.startPrefixMapping(string, string_1_);
		}

		public void endPrefixMapping(String string) throws SAXException {
			parent.endPrefixMapping(string);
		}

		public void startElement(String string, String string_2_,
				String string_3_, Attributes attributes) throws SAXException {
			parent.startElement(string, string_2_, string_3_, attributes);
		}

		public void endElement(String string, String string_4_, String string_5_)
				throws SAXException {
			parent.endElement(string, string_4_, string_5_);
		}

		public void characters(char[] cs, int i, int i_6_) throws SAXException {
			parent.characters(cs, i, i_6_);
		}

		public void ignorableWhitespace(char[] cs, int i, int i_7_)
				throws SAXException {
			parent.ignorableWhitespace(cs, i, i_7_);
		}

		public void processingInstruction(String string, String string_8_)
				throws SAXException {
			parent.processingInstruction(string, string_8_);
		}

		public void skippedEntity(String string) throws SAXException {
			parent.skippedEntity(string);
		}
	}

	public class PSU28 extends U28 {
		PSU28() {
			super(fontCatalog);
		}

		protected void flushTextBuffer() throws IOException {
			if (itxt != 0) {
				if (!fontSpecified) {
					setFont(curFont, fontSize, fontStretch, fontEncoding);
					fontSpecified = true;
				}
				show(new String(txt, 0, itxt), curFont, fontEncoding);
				itxt = 0;
			}
		}

		protected boolean isOneByteText(FontRecord fontrecord) {
			switch (fontrecord.datatype) {
			case 2:
				if (fontrecord.embed)
					return false;
				return true;
			case 1:
				return true;
			case 3:
				return true;
			case 4:
				return false;
			default:
				throw new RuntimeException("Invalid data type in font record");
			}
		}

		protected boolean isProcessAsUnicode(FontRecord fontrecord) {
			return false;
		}

		public void processMultiByteTextUnicode(String string)
				throws IOException {
			processMultiByteTextCID(string);
		}

		public void processMultiByteTextCID(String string) throws IOException {
			if (txt.length < string.length())
				txt = new char[string.length()];
			Object object = null;
			Encoding encoding;
			if (!curFont.canExtend) {
				if (curFont.encodingTable.length() == 0)
					throw new RuntimeException(
							"Empty encoding table in font descriptor");
				encoding = (Encoding) curFont.encodingTable.get(fontEncoding);
			} else if (curFont.encodingTable.length() != 0) {
				try {
					encoding = (Encoding) curFont.encodingTable
							.get(fontEncoding);
				} catch (Exception exception) {
					throw new RuntimeException("Wrong encoding is specified");
				}
			} else {
				encoding = new Encoding();
				curFont.encodingTable.put(0, encoding);
			}
			for (int i = 0; i < string.length(); i++) {
				char c = string.charAt(i);
				int i_9_ = encoding.getCode(c);
				if (i_9_ == -1) {
					CharMetrics charmetrics = curFont.record.getMetric().ucm(c);
					if (charmetrics == null) {
						charmetrics = curFont.record.getMetric().missingGlyph;
						if (charmetrics == null)
							continue;
						c = charmetrics.uc;
						i_9_ = encoding.getCode(c);
					}
					if (i_9_ == -1) {
						Encoding encoding_10_ = null;
						int i_11_;
						for (i_11_ = 0; i_11_ < curFont.encodingTable.length(); i_11_++) {
							encoding_10_ = (Encoding) curFont.encodingTable
									.get(i_11_);
							if (encoding_10_ == null)
								throw new RuntimeException(
										"Null encoding in font descriptor");
							i_9_ = encoding_10_.getCode(c);
							if (i_9_ != -1)
								break;
						}
						if (i_9_ == -1) {
							if (!curFont.canExtend) {
								curFont.hasUnmappedChars = true;
								i_11_ = fontEncoding;
								charmetrics = curFont.record.getMetric().missingGlyph;
								if (charmetrics == null)
									continue;
								c = charmetrics.uc;
								i_9_ = encoding.getCode(c);
							} else {
								if (encoding_10_.hasRoom())
									i_11_--;
								else {
									encoding_10_ = new Encoding();
									curFont.encodingTable.put(i_11_,
											encoding_10_);
								}
								i_9_ = encoding_10_.assignCode(c, charmetrics);
							}
						}
						if (i_11_ != fontEncoding) {
							flushTextBuffer();
							fontSpecified = false;
							fontEncoding = i_11_;
							encoding = encoding_10_;
						}
					}
				}
				txt[itxt++] = (char) i_9_;
				curFont.used.markGlyph(c);
			}
			flushTextBuffer();
		}
	}

	public void setInitialDestination(String string) {
		initialDestination = string;
	}

	public void setInitialZoom(String string) {
		zoom = string;
	}

	public void setViewMode(String string) {
		viewMode = string;
	}

	public PSDocument(String string, File file, ImageFactory imagefactory,
			FontCatalog fontcatalog, ErrorHandler errorhandler)
			throws IOException {
		this(new FileOutputStream(string), file, imagefactory, fontcatalog,
				errorhandler);
	}

	public PSDocument(OutputStream outputstream, File file,
			ImageFactory imagefactory, FontCatalog fontcatalog,
			ErrorHandler errorhandler) throws IOException {
		errorHandler = errorhandler;
		fontCatalog = fontcatalog;
		imageFactory = imagefactory;
		u28 = new PSU28();
		targetStream = new PSOutputStream(this, outputstream);
		out = targetStream;
		if (file != null) {
			if (!file.exists()) {
				errorHandler
						.error("Directory "
								+ file
								+ " does not exist; disk caching in PostScript generator disabled.");
				file = null;
			} else if (!file.isDirectory()) {
				errorHandler
						.error("File "
								+ file
								+ " is not a directory; disk caching in PostScript generator disabled.");
				file = null;
			} else if (!file.canWrite()) {
				errorHandler
						.error("Directory "
								+ file
								+ " is not writable; disk caching in PostScript generator disabled.");
				file = null;
			}
		}
		pages = new Pages(this, file);
		info = new Info();
		fonts = new Fonts();
		images = new Images();
		spotColors = new SpotColors();
		names = new Names();
		bookmarks = new Bookmarks();
		currentPageBoundaries = pageboundaries;
		currentPrinterMarks = printermarks;
		currentDevice = pagedevice;
		setPageDeviceEntry("PageOffset", "[0 0]");
		setPageDeviceEntry("Margins", "[0 0]");
	}

	protected PSDocument(OutputStream outputstream, File file)
			throws IOException, FontConfigurationException {
		this(outputstream, file, new URLCache(), new DefaultErrorHandler());
	}

	private PSDocument(OutputStream outputstream, File file, URLCache urlcache,
			ErrorHandler errorhandler) throws IOException,
			FontConfigurationException {
		this(outputstream, file, new FontCatalog(urlcache, errorhandler),
				urlcache, errorhandler);
	}

	private PSDocument(OutputStream outputstream, File file,
			FontCatalog fontcatalog, URLCache urlcache,
			ErrorHandler errorhandler) throws IOException {
		this(outputstream, file, new ImageFactory(fontcatalog, urlcache,
				errorhandler), fontcatalog, errorhandler);
	}

	public void setLanguageLevel(int i) {
		LANGUAGE_LEVEL = i;
	}

	public void setUnicodeAnnotations(boolean bool) {
		UNICODE_ANNOTATIONS = bool;
	}

	public void setCloneEps(boolean bool) {
		CLONE_EPS = bool;
	}

	public void setDestinationTreatment(boolean bool) {
		DROP_UNUSED_DESTINATIONS = bool;
	}

	public void setInfo(String string, String string_12_) {
		info.set(string, string_12_);
	}

	public void setPageDeviceEntry(String string, String string_13_) {
		if (string.startsWith("/"))
			string = string.substring(1);
		currentDevice.put(string, string_13_);
		pageDeviceEntries.put(string, string);
	}

	public String getPageDeviceEntry(String string) {
		if (string.startsWith("/"))
			string = string.substring(1);
		return (String) currentDevice.get(string);
	}

	public void setPageBoundaryEntry(String string, Object object) {
		currentPageBoundaries.put(string, object);
	}

	public Hashtable getPageBoundaries() {
		return currentPageBoundaries;
	}

	public void setPrinterMarkEntry(String string) {
		currentPrinterMarks.append(string);
	}

	public void endDocument() throws IOException {
		if (pages.length() == 0)
			pages.insertBlankPage(1);
		writeHeader();
		writeProlog();
		writeDocumentSetup();
		pages.write(out);
		writeTrailer();
		out.println("%%EOF");
		out.flush();
	}

	public void beginPage(float f, float f_14_, int i, String string)
			throws IOException {
		if (pages.length() == 0)
			firstPageHeight = f_14_;
		pages.beginPage(i, string);
		out = pages.currentPage.contentStream;
		currentPageBoundaries = pages.currentPage.pageboundaries;
		currentPrinterMarks = pages.currentPage.printermarks;
		Pages.PageBoundaries pageboundaries = new Pages.PageBoundaries(
				this.pageboundaries);
		float[] fs = pageboundaries.getMediaBoxOffset();
		float f_15_ = f + fs[0] + fs[2];
		float f_16_ = f_14_ + fs[1] + fs[3];
		String string_17_ = "[" + f_15_ + " " + f_16_ + "]";
		String string_18_ = (String) pagedevice.get("PageSize");
		if (string_18_ == null) {
			currentDevice = pagedevice;
			setPageDeviceEntry("PageSize", string_17_);
			currentDevice = pages.currentPage.pagedevice;
		} else {
			currentDevice = pages.currentPage.pagedevice;
			if (!string_18_.equals(string_17_))
				setPageDeviceEntry("PageSize", string_17_);
		}
	}

	public void endPage() throws IOException {
		Pages.PageBoundaries pageboundaries = new Pages.PageBoundaries(
				currentPageBoundaries);
		Pages.PageBoundaries pageboundaries_19_ = new Pages.PageBoundaries(
				this.pageboundaries);
		String string = (String) pagedevice.get("PageSize");
		if (currentDevice.containsKey("PageSize"))
			string = (String) currentDevice.get("PageSize");
		StringTokenizer stringtokenizer = new StringTokenizer(string);
		String string_20_ = stringtokenizer.nextToken();
		string_20_ = string_20_.substring(1);
		String string_21_ = stringtokenizer.nextToken();
		string_21_ = string_21_.substring(0, string_21_.length() - 1);
		float[] fs = pageboundaries_19_.getMediaBoxOffset();
		if (pageboundaries.isMediaBoxOffset())
			fs = pageboundaries.getMediaBoxOffset();
		float f = new Float(string_20_).floatValue();
		float f_22_ = new Float(string_21_).floatValue();
		boolean bool = pageboundaries.isPageOffset();
		if (pageboundaries_19_.isPageOffset()
				&& currentDevice.containsKey("PageSize"))
			bool = true;
		if (bool) {
			f += fs[0] + fs[2];
			f_22_ += fs[1] + fs[3];
			string = "[" + f + " " + f_22_ + "]";
			currentDevice.put("PageSize", string);
		}
		if (pageboundaries_19_.isPageOffset() || pageboundaries.isPageOffset()) {
			float[] fs_23_ = pageboundaries_19_.getBleeds();
			if (pageboundaries.isBleeds())
				fs_23_ = pageboundaries.getBleeds();
			out.println("grestore");
			out.println("gsave");
			out.println("newpath");
			out.println("0 0 moveto");
			out.println("0 " + string_21_ + " lineto");
			out.println(string_20_ + " " + string_21_ + " lineto");
			out.println(string_20_ + " 0 lineto");
			out.println("closepath");
			out.println("clippath");
			out.println("" + (fs[0] - fs_23_[0]) + " " + (fs[1] - fs_23_[1])
					+ " moveto");
			out.println("" + (f - fs[2] + fs_23_[2]) + " "
					+ (fs[1] - fs_23_[1]) + " lineto");
			out.println("" + (f - fs[2] + fs_23_[2]) + " "
					+ (f_22_ - fs[3] + fs_23_[3]) + " lineto");
			out.println("" + (fs[0] - fs_23_[0]) + " "
					+ (f_22_ - fs[3] + fs_23_[3]) + " lineto");
			out.println("closepath");
			out.println("clip");
			if (pageboundaries.isBleedMarkWidth()
					|| pageboundaries_19_.isBleedMarkWidth()) {
				float f_24_ = pageboundaries_19_.getBleedMarkWidth();
				if (pageboundaries.isBleedMarkWidth())
					f_24_ = pageboundaries.getBleedMarkWidth();
				drawMarks(fs, fs_23_, f_24_, f, f_22_);
			}
			if (pageboundaries.isCropMarkWidth()
					|| pageboundaries_19_.isCropMarkWidth()) {
				float f_25_ = pageboundaries_19_.getCropMarkWidth();
				if (pageboundaries.isCropMarkWidth())
					f_25_ = pageboundaries.getCropMarkWidth();
				float[] fs_26_ = { 0.0F, 0.0F, 0.0F, 0.0F };
				drawMarks(fs, fs_26_, f_25_, f, f_22_);
			}
			Enumeration enumeration = currentPrinterMarks.elements();
			if (!printermarks.isEmpty())
				enumeration = printermarks.elements();
			while (enumeration.hasMoreElements()) {
				String string_27_ = (String) enumeration.nextElement();
				Object object = null;
				XMLReader xmlreader;
				try {
					xmlreader = XMLReaderFactory.createXMLReader();
				} catch (SAXException saxexception) {
					errorHandler.error("Cannot create XML reader: "
							+ saxexception.getMessage());
					continue;
				}
				StringWriter stringwriter = new StringWriter();
				Base64OutputStream base64outputstream = new Base64OutputStream(
						stringwriter);
				try {
					String string_28_ = String.valueOf(f) + "pt";
					String string_29_ = String.valueOf(f_22_) + "pt";
					xmlreader.setContentHandler(new ImageSizeSetterHandler(
							new Serializer(base64outputstream), string_28_,
							string_29_));
				} catch (IOException ioexception) {
					errorHandler
							.error("Cannot constructs serializer from an output stream: "
									+ ioexception.getMessage());
					continue;
				}
				stringwriter.write("data:image/svg+xml;base64,");
				try {
					xmlreader.parse(string_27_);
					base64outputstream.close();
					stringwriter.close();
				} catch (IOException ioexception) {
					errorHandler.warning("Invalid URL or non-existent file: "
							+ string_27_ + "; setting ignored");
					continue;
				} catch (SAXException saxexception) {
					errorHandler.error("Cannot parse a document: "
							+ saxexception.getMessage());
					continue;
				}
				String string_30_ = stringwriter.toString();
				try {
					placeImage(new URLSpec(string_30_), "image/svg+xml");
				} catch (IOException ioexception) {
					errorHandler.error("Invalid URL or non-existent file: "
							+ string_30_);
				}
			}
			out.println("grestore");
		}
		pages.endPage();
		out = targetStream;
		currentDevice = pagedevice;
		currentPageBoundaries = this.pageboundaries;
		currentPrinterMarks = printermarks;
	}

	public void drawMarks(float[] fs, float[] fs_31_, float f, float f_32_,
			float f_33_) {
		try {
			out.println("0.0 0.0 0.0 setrgbcolor");
			out.println("" + f + " setlinewidth");
			out.println("newpath");
			out.println("" + (fs[0] - fs_31_[0]) + " " + (fs[1] - fs_31_[1])
					+ " moveto");
			out.println("0 " + (fs[1] - fs_31_[1]) + " lineto");
			out.println("stroke");
			out.println("newpath");
			out.println("" + (fs[0] - fs_31_[0]) + " " + (fs[1] - fs_31_[0])
					+ " moveto");
			out.println("" + (fs[0] - fs_31_[0]) + " 0 lineto");
			out.println("stroke");
			out.println("0.0 0.0 0.0 setrgbcolor");
			out.println("" + f + " setlinewidth");
			out.println("newpath");
			out.println("" + (fs[0] - fs_31_[0]) + " "
					+ (f_33_ - fs[3] + fs_31_[3]) + " moveto");
			out.println("0 " + (f_33_ - fs[3] + fs_31_[3]) + " lineto");
			out.println("stroke");
			out.println("newpath");
			out.println("" + (fs[0] - fs_31_[0]) + " "
					+ (f_33_ - fs[3] + fs_31_[3]) + " moveto");
			out.println("" + (fs[0] - fs_31_[0]) + " " + f_33_ + " lineto");
			out.println("stroke");
			out.println("0.0 0.0 0.0 setrgbcolor");
			out.println("" + f + " setlinewidth");
			out.println("newpath");
			out.println("" + (f_32_ - fs[2] + fs_31_[2]) + " "
					+ (f_33_ - fs[3] + fs_31_[3]) + " moveto");
			out.println("" + f_32_ + " " + (f_33_ - fs[3] + fs_31_[3])
					+ " lineto");
			out.println("stroke");
			out.println("newpath");
			out.println("" + (f_32_ - fs[2] + fs_31_[2]) + " "
					+ (f_33_ - fs[3] + fs_31_[3]) + " moveto");
			out.println("" + (f_32_ - fs[2] + fs_31_[2]) + " " + f_33_
					+ " lineto");
			out.println("stroke");
			out.println("0.0 0.0 0.0 setrgbcolor");
			out.println("" + f + " setlinewidth");
			out.println("newpath");
			out.println("" + (f_32_ - fs[2] + fs_31_[2]) + " "
					+ (fs[1] - fs_31_[1]) + " moveto");
			out.println("" + f_32_ + " " + (fs[1] - fs_31_[1]) + " lineto");
			out.println("stroke");
			out.println("newpath");
			out.println("" + (f_32_ - fs[2] + fs_31_[1]) + " "
					+ (fs[1] - fs_31_[1]) + " moveto");
			out.println("" + (f_32_ - fs[2] + fs_31_[1]) + " 0 lineto");
			out.println("stroke");
		} catch (IOException ioexception) {
			errorHandler.warning("Cannot draw printer marks "
					+ ioexception.getMessage());
		}
	}

	public void setFont(FontDescriptor fontdescriptor, float f, float f_34_)
			throws IOException {
		setFont(fontdescriptor, f, f_34_, 0);
	}

	public void setFont(FontDescriptor fontdescriptor, float f, float f_35_,
			int i) throws IOException {
		if (fontdescriptor != null) {
			fontSize = f;
			if (f != 0.0F) {
				String string = fonts.getPSFontName(fontdescriptor, i);
				if (f_35_ == 1.0F
						&& fontdescriptor.record.extraSlantAngle == 0.0)
					out.println("/" + string + " " + f + " selectfont");
				else {
					float f_36_ = (float) Math
							.tan((fontdescriptor.record.extraSlantAngle) * 0.017453292519943295);
					out.println("/" + string + " [" + f * f_35_ + " 0 " + f
							* f_36_ + " " + f + " 0 0] selectfont");
				}
			}
		}
	}

	public void placeImage(URLSpec urlspec, String string) throws IOException {
		try {
			placeImage(imageFactory.makeImage(urlspec, string));
		} catch (MalformedURLException malformedurlexception) {
			errorHandler.exception("Bad image file URL", malformedurlexception);
		} catch (ImageFormatException imageformatexception) {
			errorHandler.exception("Cannot parse image file",
					imageformatexception);
		} catch (UnregisteredMIMETypeException unregisteredmimetypeexception) {
			errorHandler.error("Unknown MIME type: " + string);
		}
	}

	public void placeImage(com.renderx.graphics.Image image) throws IOException {
		if (CLONE_EPS && image instanceof EPSImage) {
			EPS eps = new EPS((EPSImage) image);
			out.println("BeginEPSF");
			out.println("" + (float) -eps.bbox[0] + " " + (float) -eps.bbox[1]
					+ " translate");
			eps.writeEPS(out);
			out.println("EndEPSF");
		} else {
			try {
				Image image_37_ = images.addImg(image);
				out.println("ImageForm" + image_37_.id + " execform");
			} catch (ImageFormatException imageformatexception) {
				errorHandler.exception("Error parsing image",
						imageformatexception);
			} catch (InternalException internalexception) {
				errorHandler.warning("" + internalexception);
			}
		}
	}

	public void addNamedDestination(String string, float f, float f_38_)
			throws IOException {
		string = names.makePSName(string);
		names.addDestination(string, pages.currentNumber, f, f_38_);
	}

	public void addLocalLink(float f, float f_39_, float f_40_, float f_41_,
			String string) throws IOException {
		string = names.makePSName(string);
		names.markUsed(string);
		out.println("[/Subtype /Link");
		out.println("/Rect [" + f + " " + f_39_ + " " + f_40_ + " " + f_41_
				+ "]");
		out.println("/Border [0 0 0 [0]]");
		out.println("/Dest /" + string);
		out.println("/ANN pdfmark");
	}

	public void addWebLink(float f, float f_42_, float f_43_, float f_44_,
			String string, boolean bool) throws IOException {
		out.println("[/Subtype /Link");
		out.println("/Rect [" + f + " " + f_42_ + " " + f_43_ + " " + f_44_
				+ "]");
		out.println("/Border [0 0 0 [0]]");
		out.createAction(string, bool);
		out.println("/ANN pdfmark");
	}

	public void addTextAnnotation(float f, float f_45_, float f_46_,
			float f_47_, String string, String string_48_, float[] fs)
			throws IOException {
		out.println("[/Subtype /Text");
		out.println("/Rect [" + f + " " + f_45_ + " " + f_46_ + " " + f_47_
				+ "]");
		out.println("/Border [0 0 0 [0]]");
		if (fs != null && fs.length == 3)
			out.println("/Color [" + fs[0] + " " + fs[1] + " " + fs[2] + "]");
		if (string_48_ != null) {
			out.print("/Title ");
			out.print_annotation(string_48_, 255);
			out.println();
		}
		out.print("/Contents ");
		out.print_annotation(string, 65535);
		out.println();
		out.println("/ANN pdfmark");
	}

	public void addLocalBookmark(String string, int i, int i_49_,
			String string_50_, boolean bool) throws IOException {
		string_50_ = names.makePSName(string_50_);
		bookmarks.addBookmark(string, i, i_49_, string_50_, null, bool, false);
		names.markUsed(string_50_);
	}

	public void addWebBookmark(String string, int i, int i_51_,
			String string_52_, boolean bool, boolean bool_53_)
			throws IOException {
		bookmarks.addBookmark(string, i, i_51_, null, string_52_, bool,
				bool_53_);
	}

	public void transform(float f, float f_54_, float f_55_, float f_56_,
			float f_57_, float f_58_) throws IOException {
		out.println("[" + f + " " + f_54_ + " " + f_55_ + " " + f_56_ + " "
				+ f_57_ + " " + f_58_ + "] concat");
	}

	public void scale(float f, float f_59_) throws IOException {
		if (f != 1.0F || f_59_ != 1.0F)
			out.println("" + f + " " + f_59_ + " scale");
	}

	public void rotate(float f) throws IOException {
		if (f != 0.0F)
			out.println("" + f + " rotate");
	}

	public void translate(float f, float f_60_) throws IOException {
		if (f != 0.0F || f_60_ != 0.0F)
			out.println("" + f + " " + f_60_ + " translate");
	}

	public void moveTo(float f, float f_61_) throws IOException {
		out.println("" + f + " " + f_61_ + " moveto");
	}

	public void lineTo(float f, float f_62_) throws IOException {
		out.println("" + f + " " + f_62_ + " lineto");
	}

	public void line(float f, float f_63_, float f_64_, float f_65_, float f_66_)
			throws IOException {
		setLineWidth(f_66_);
		moveTo(f, f_63_);
		lineTo(f_64_, f_65_);
	}

	public void curveTo(float f, float f_67_, float f_68_, float f_69_,
			float f_70_, float f_71_) throws IOException {
		out.println("" + f + " " + f_67_ + " " + f_68_ + " " + f_69_ + " "
				+ f_70_ + " " + f_71_ + " curveto");
	}

	public void rectangle(float f, float f_72_, float f_73_, float f_74_)
			throws IOException {
		out.println("" + f + " " + f_72_ + " " + f_73_ + " " + f_74_
				+ " rectfill");
	}

	public void setCharSpacing(float f) {
		letterSpacing = f;
	}

	public void setWordSpacing(float f) {
		wordSpacing = f;
	}

	public void setFontSize(float f) {
		fontSize = f;
		u28.resetFont();
	}

	public void setFontStretch(float f) {
		fontStretch = f;
		u28.resetFont();
	}

	public void show(String string, FontDescriptor fontdescriptor, int i)
			throws IOException {
		if (string != null && string.length() != 0 && fontSize != 0.0F)
			new PSText(string, letterSpacing, wordSpacing, textRenderMode)
					.write(out);
	}

	public void gsave() throws IOException {
		out.println("gsave");
	}

	public void grestore() throws IOException {
		out.println("grestore");
	}

	public void setDash(float f, float f_75_, float f_76_) throws IOException {
		String string = "";
		if (f == 0.0F && f_75_ == 0.0F)
			string = "[] ";
		else
			string = "[" + f + " " + f_75_ + "]";
		out.println(string + " " + f_76_ + " setdash");
	}

	public void setLineCap(int i) throws IOException {
		if (i < 0 || i > 2)
			errorHandler.warning("Wrong line cap style: " + i);
		else
			out.println("" + i + " setlinecap");
	}

	public void setLineWidth(float f) throws IOException {
		if ((double) f < 0.0)
			errorHandler.warning("Wrong line width: " + f);
		else
			out.println("" + f + " setlinewidth");
	}

	public void setCMYKColor(float f, float f_77_, float f_78_, float f_79_)
			throws IOException {
		out.println("" + f + " " + f_77_ + " " + f_78_ + " " + f_79_
				+ " setcmykcolor");
	}

	public void setRGBColor(float f, float f_80_, float f_81_)
			throws IOException {
		out.println("" + f + " " + f_80_ + " " + f_81_ + " setrgbcolor");
	}

	public void setGrayColor(float f) throws IOException {
		out.println("" + f + " setgray");
	}

	public void setSpotColor(float f, String string, float[] fs)
			throws IOException {
		String string_82_ = spotColors.getPSSpotColorName(string, fs);
		if (string_82_ == null) {
			spotColors.addSpotColor(string, fs);
			string_82_ = spotColors.getPSSpotColorName(string, fs);
		}
		out.print("/" + string_82_ + " /ColorSpace findresource "
				+ "setcolorspace");
		out.println(" " + f + " setcolor");
	}

	public void rectclip(float f, float f_83_, float f_84_, float f_85_)
			throws IOException {
		out.println("" + f + " " + f_83_ + " " + f_84_ + " " + f_85_
				+ " rectclip");
	}

	public void newpath() throws IOException {
		out.println("newpath");
	}

	public void closePath() throws IOException {
		out.println("closepath");
	}

	public void fill() throws IOException {
		out.println("fill");
	}

	public void stroke() throws IOException {
		out.println("stroke");
	}

	public void setTextPos(float f, float f_86_) throws IOException {
		moveTo(f, f_86_);
	}

	private void writeHeader() throws IOException {
		out.println("%!PS-Adobe-" + (LANGUAGE_LEVEL == 2 ? "2.1" : "3.0"));
		info.writeHeaderComment(out, "Title");
		info.writeHeaderComment(out, "Creator");
		out.println("%%Pages: " + pages.length());
		if (DOCUMENT_MANAGER_COMMENTS) {
			Hashtable hashtable = u28.fontStateTable;
			boolean bool = true;
			Enumeration enumeration = hashtable.keys();
			while (enumeration.hasMoreElements()) {
				FontDescriptor fontdescriptor = ((FontDescriptor) hashtable
						.get(enumeration.nextElement()));
				if (!fontdescriptor.record.embed) {
					out.print(bool ? "%%DocumentNeededResources: " : "%%+ ");
					out.println("font "
							+ fontdescriptor.record.getMetric().fontName);
					bool = false;
				}
			}
			bool = true;
			enumeration = hashtable.keys();
			while (enumeration.hasMoreElements()) {
				FontDescriptor fontdescriptor = ((FontDescriptor) hashtable
						.get(enumeration.nextElement()));
				if (fontdescriptor.record.embed) {
					out.print(bool ? "%%DocumentSuppliedResources: " : "%%+ ");
					out.println("font "
							+ fontdescriptor.record.getMetric().fontName);
					bool = false;
				}
			}
		}
		out.println("%%EndComments");
	}

	private void writeDocumentSetup() throws IOException {
		out.println("%%BeginSetup");
		Hashtable hashtable = u28.fontStateTable;
		if (DOCUMENT_MANAGER_COMMENTS) {
			Enumeration enumeration = hashtable.keys();
			while (enumeration.hasMoreElements()) {
				FontDescriptor fontdescriptor = ((FontDescriptor) hashtable
						.get(enumeration.nextElement()));
				if (!fontdescriptor.record.embed)
					out.println("%%IncludeResource: font "
							+ fontdescriptor.record.getMetric().fontName);
			}
		}
		images.write(out);
		fonts.write(out, hashtable);
		spotColors.write(out);
		info.writePDFMarks(out);
		writeDocView();
		names.write(out, DROP_UNUSED_DESTINATIONS);
		bookmarks.write(out);
		writePageDevice();
		out.println("%%EndSetup");
	}

	void writePageDevice() throws IOException {
		int i = pageDeviceEntries.size();
		out.println("/XEPPS_BackupPageDevice " + i + " dict");
		Enumeration enumeration = pageDeviceEntries.keys();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			out
					.println("dup /"
							+ string
							+ " dup currentpagedevice exch known {dup currentpagedevice exch get put} {pop pop} ifelse ");
		}
		out.println("def");
		out.println("" + pagedevice.size() + " dict");
		enumeration = pagedevice.keys();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			String string_87_ = (String) pagedevice.get(string);
			out.println("dup /" + string + " " + string_87_ + " put");
		}
		out.println("SafeSetPageDevice");
		out.println("/XEPPS_DocumentPageDevice " + i + " dict");
		enumeration = pageDeviceEntries.keys();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			out
					.println("dup /"
							+ string
							+ " dup currentpagedevice exch known {dup currentpagedevice exch get put} {pop pop} ifelse ");
		}
		out.println("def");
	}

	void writeDocView() throws IOException {
		if (viewMode == null || "auto".equals(viewMode))
			viewMode = bookmarks.isEmpty() ? "show-none" : "show-bookmarks";
		if ("show-bookmarks".equals(viewMode))
			out.println("[/PageMode /UseOutlines");
		else if ("show-thumbnails".equals(viewMode))
			out.println("[/PageMode /UseThumbs");
		else if ("show-none".equals(viewMode))
			out.println("[/PageMode /UseNone");
		else if ("full-screen".equals(viewMode))
			out.println("[/PageMode /FullScreen");
		else {
			out.println("[/PageMode /UseNone");
			errorHandler.warning("Wrong value for view mode: " + viewMode
					+ "; parameter ignored");
		}
		Names.Destination destination = null;
		if (initialDestination != null) {
			destination = names.getDestination(initialDestination);
			if (destination == null)
				errorHandler.warning("Unresolved initial destination: '"
						+ initialDestination + "'; parameter ignored");
		}
		if (destination != null || zoom != null) {
			if (destination == null)
				destination = new Names.Destination(1, 0.0F, firstPageHeight);
			out.println("/Page " + destination.page);
			if (zoom == null)
				zoom = "auto";
			zoom = zoom.trim();
			if ("fit".equals(zoom))
				out.println("/View [/Fit]");
			else if ("fit-width".equals(zoom))
				out.println("/View [/FitH " + destination.y + "]");
			else if ("fit-height".equals(zoom))
				out.println("/View [/FitV " + destination.x + "]");
			else {
				float f = 0.0F;
				try {
					if (zoom.equals("auto"))
						f = 0.0F;
					else if (zoom.endsWith("%"))
						f = Float.valueOf(zoom.substring(0, zoom.length() - 1))
								.floatValue() / 100.0F;
					else
						f = Float.valueOf(zoom).floatValue();
				} catch (NumberFormatException numberformatexception) {
					errorHandler.warning("Wrong value for zoom factor: " + zoom
							+ "; parameter ignored");
					f = 0.0F;
				}
				out.println("/View [/XYZ " + destination.x + " "
						+ destination.y + " " + f + "]");
			}
		}
		out.println("/DOCVIEW pdfmark ");
	}

	public void writeProlog() throws IOException {
		out.println("%%BeginProlog");
		out.println("%%BeginResource: procset Common_XEP_procedures");
		out
				.println("/REENCODEFONT {\n  /Enc exch def findfont dup length dict copy\n  dup /Encoding Enc put definefont\n} bind def");
		out
				.println("/BeginEPSF {\n  /b4_Inc_state save def\n  /dict_count countdictstack def\n  /op_count count 1 sub def\n  userdict begin\n  /showpage { } def\n  0 setgray 0 setlinecap\n  1 setlinewidth 0 setlinejoin\n  10 setmiterlimit [ ] 0 setdash newpath\n  /languagelevel where\n  { pop languagelevel 1 ne\n    {false setstrokeadjust false setoverprint\n    } if\n  } if\n} bind def");
		out
				.println("/EndEPSF {\n  count op_count sub {pop} repeat\n  countdictstack dict_count sub {end} repeat\n  b4_Inc_state restore\n} bind def");
		out
				.println("/pdfmark where {pop} {userdict /pdfmark /cleartomark load put} ifelse");
		out
				.println("/CreateDataArray {\n  /temp exch def\n  [\n  {\n    temp 16384 string readstring\n    not { exit } if\n  } loop\n  dup length 0 eq { pop } if\n  ]\n} bind def");
		out
				.println("/EqualArray {\n  /array2 exch def\n  /array1 exch def\n  /lenarray array1 length def\n  lenarray array2 length ne {false} {\n    true\n    0 1 lenarray 1 sub {\n      /idxarr exch def\n      array1 idxarr get array2 idxarr get ne {pop false exit} if\n    } for\n  } ifelse\n} bind def");
		out
				.println("/EqualDict {\n  /dict2 exch def\n  /dict1 exch def\n  /lendict dict1 length def\n  lendict dict2 length ne {false} {\n    true\n    dict1 {\n      exch dict2 exch get ne {pop false exit} if\n    } forall \n  } ifelse\n} bind def");
		out
				.println("/DeepEqual {\n  /obj2 exch def\n  /obj1 exch def\n  obj1 obj2 eq {true} {\n    obj1 type obj2 type ne {false} {\n      obj1 type /dicttype eq {\n        obj1 obj2 EqualDict\n      } {\n        obj1 type /arraytype eq {\n          obj1 obj2 EqualArray\n        } {false} ifelse\n      } ifelse\n    } ifelse\n  } ifelse\n} bind def");
		out
				.println("/SafeSetPageDevice {\n  dup length /safedict exch dict def \n  {\n    /devicevalue exch def\n    /devicekey exch def\n    currentpagedevice devicekey known {\n      currentpagedevice devicekey get devicevalue DeepEqual not {\n        safedict devicekey devicevalue put\n      } if\n    } if\n  } forall\n  safedict length 0 ne { safedict setpagedevice} if\n} bind def");
		out.println("%%EndResource");
		out.println("%%EndProlog");
	}

	public void writeTrailer() throws IOException {
		out.println("%%Trailer");
		out.println("XEPPS_BackupPageDevice SafeSetPageDevice");
	}
}