/*
 * Images - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.renderx.fonts.FontCatalog;
import com.renderx.fonts.FontConfigurationException;
import com.renderx.graphics.ImageFactory;
import com.renderx.graphics.ImageFormatException;
import com.renderx.graphics.UnregisteredMIMETypeException;
import com.renderx.util.Array;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;
import com.renderx.util.User;

import junit.framework.Assert;
import junit.framework.TestCase;

public class Images {
	static final int MAX_ID_LENGTH = 80;

	private int counter = 0;

	private final Hashtable imgIndex = new Hashtable();

	private final Hashtable imgIdTable = new Hashtable();

	private final Array imgArray = new Array();

	public static class Test extends TestCase {
		ErrorHandler errorHandler = new DefaultErrorHandler();

		URLCache urlcache = new URLCache();

		FontCatalog fontCatalog;

		ImageFactory imageFactory;

		public void setUp() {
			try {
				fontCatalog = new FontCatalog(urlcache, errorHandler);
				imageFactory = new ImageFactory(fontCatalog, urlcache,
						errorHandler);
			} catch (IOException ioexception) {
				Assert.fail("Cann't create fontCatalog or imageFactory: "
						+ ioexception);
			} catch (FontConfigurationException fontconfigurationexception) {
				Assert.fail("Cannot create font catalog: "
						+ fontconfigurationexception);
			}
		}

		public void test_addImg() throws IOException, ImageFormatException,
				UnregisteredMIMETypeException {
			Images images = new Images();
			PSDocument psdocument = new PSDocument(new ByteArrayOutputStream(),
					new File(User.getProperty("user.dir")), imageFactory,
					fontCatalog, errorHandler);
			String string = (User.getProperty("user.dir") + File.separator + "test_png.png");
			String string_0_ = "com/renderx/pslib/TEST_DATA/test_png.png";
			resourceToFile(string_0_, string);
			com.renderx.graphics.Image image = psdocument.imageFactory
					.makeImage(new URLSpec("resource:" + string_0_),
							"image/png");
			images.addImg(image);
			Assert.assertTrue(images.getSize() == 1);
			images.addImg(image);
			Assert.assertTrue(images.getSize() == 1);
			String string_1_ = (User.getProperty("user.dir") + File.separator + "test_gif.gif");
			string_0_ = "com/renderx/pslib/TEST_DATA/test_gif.gif";
			resourceToFile(string_0_, string_1_);
			image = psdocument.imageFactory.makeImage(new URLSpec("resource:"
					+ string_0_), "image/gif");
			images.addImg(image);
			Assert.assertTrue(images.getSize() == 2);
			images.addImg(image);
			Assert.assertTrue(images.getSize() == 2);
			psdocument.out.close();
			new File(string).delete();
			new File(string_1_).delete();
		}

		private void resourceToFile(String string, String string_2_)
				throws IOException {
			InputStream inputstream = ClassLoader
					.getSystemResourceAsStream(string);
			FileOutputStream fileoutputstream = new FileOutputStream(string_2_);
			byte[] is = new byte[1024];
			int i;
			while ((i = inputstream.read(is, 0, 1024)) >= 0)
				fileoutputstream.write(is, 0, i);
			fileoutputstream.close();
		}
	}

	Image addImg(com.renderx.graphics.Image image) throws ImageFormatException,
			IOException {
		Image image_3_ = (Image) imgIndex.get(image);
		if (image_3_ != null)
			return image_3_;
		boolean bool = image.source.scheme != 3;
		String string = bool ? "$" + ++counter : makeUniqueId(image.source);
		image_3_ = new Image(image, string);
		imgArray.put(imgArray.length(), image_3_);
		imgIndex.put(image, image_3_);
		imgIdTable.put(image_3_.id, image_3_);
		return image_3_;
	}

	private String makeUniqueId(URLSpec urlspec) {
		boolean bool = true;
		StringBuffer stringbuffer = new StringBuffer();
		String string = urlspec.toString();
		for (int i = string.indexOf(':') + 1; i < string.length(); i++) {
			char c = string.charAt(i);
			if ("0123456789_-.abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
					.indexOf(c) == -1)
				bool = true;
			else {
				if (bool)
					stringbuffer.append(':');
				stringbuffer.append(c);
				bool = false;
			}
		}
		if (stringbuffer.length() == 0)
			return "$" + ++counter;
		String string_4_ = stringbuffer.toString();
		if (string_4_.length() > 80)
			string_4_ = ":~" + string_4_.substring(string_4_.length() + 2 - 80);
		if (imgIdTable.containsKey(string_4_))
			return "$" + ++counter + string_4_;
		imgIdTable.put(string_4_, string_4_);
		return string_4_;
	}

	int getSize() {
		return imgArray.length();
	}

	void write(PSOutputStream psoutputstream) throws IOException {
		if (imgIndex.size() > 0) {
			psoutputstream.println("\n");
			for (int i = 0; i < imgArray.length(); i++) {
				Image image = (Image) imgArray.get(i);
				try {
					image.write(psoutputstream);
				} catch (ImageFormatException imageformatexception) {
					psoutputstream.doc.errorHandler.exception("",
							imageformatexception);
				}
			}
		}
	}
}