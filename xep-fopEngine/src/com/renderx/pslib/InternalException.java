/*
 * InternalException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

public class InternalException extends RuntimeException {
	public InternalException() {
		/* empty */
	}

	public InternalException(Exception exception) {
		super(exception.toString());
	}

	public InternalException(String string) {
		super(string);
	}
}