/*
 * TTFont - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.Encoding;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.Metric;
import com.renderx.util.Array;
import com.renderx.util.SeekableInput;

public class TTFont {
	FontDescriptor fdesc;

	public TTFont(FontDescriptor fontdescriptor) {
		fdesc = fontdescriptor;
	}

	public void write(PSOutputStream psoutputstream) throws IOException {
		Metric metric = fdesc.record.getMetric();
		psoutputstream.println("%!PS-TrueTypeFont-65536-65536-1");
		psoutputstream.println("11 dict begin");
		psoutputstream.println("/FontName /" + metric.fontName + " def");
		psoutputstream.println("/FontType 42 def");
		psoutputstream.println("/PaintType 0 def");
		float[] fs = metric.fontBBox;
		psoutputstream.println("/FontBBox [" + fs[0] + " " + fs[1] + " "
				+ fs[2] + " " + fs[3] + "] def");
		psoutputstream.println("/FontMatrix [1 0 0 1 0 0] def");
		int i = 0;
		String string = "/.notdef 0 def\n";
		Array array = new Array();
		for (int i_0_ = 0; i_0_ < 65536; i_0_++) {
			CharMetrics charmetrics = (CharMetrics) metric.uniTable
					.get((char) i_0_);
			if (charmetrics != null && fdesc.used.checkGlyph((char) i_0_)) {
				i++;
				string += "/" + charmetrics.n + " " + charmetrics.c + " def\n";
				array.put(charmetrics.c, charmetrics.n);
			}
		}
		Encoding encoding = (Encoding) fdesc.encodingTable.get(0);
		psoutputstream.println("/Encoding " + (encoding.getLastCode() + 1)
				+ " array");
		psoutputstream.println("0 1 " + encoding.getLastCode()
				+ " {1 index exch /.notdef put } for");
		for (int i_1_ = 0; i_1_ < encoding.getLastCode() + 1; i_1_++)
			psoutputstream.print("dup "
					+ i_1_
					+ " /"
					+ (encoding.table[i_1_] == null ? ".notdef"
							: encoding.table[i_1_].n) + " put\n");
		psoutputstream.println("def");
		psoutputstream.println("/CharStrings " + (i + 1) + " dict dup begin");
		psoutputstream.print(string);
		psoutputstream.println("end readonly def");
		psoutputstream.print("/sfnts [");
		int i_2_ = 32768;
		do {
			try {
				if (fdesc.record.subset) {
					SeekableInput seekableinput = fdesc.record
							.openSeekableFontStream();
					try {
						com.renderx.fonts.TTFont ttfont_3_ = new com.renderx.fonts.TTFont(
								seekableinput, (fdesc.record.subfont));
						for (int i_4_ = 0; i_4_ < 65536; i_4_++) {
							if (fdesc.used.checkGlyph((char) i_4_)) {
								CharMetrics charmetrics = ((CharMetrics) metric.uniTable
										.get((char) i_4_));
								ttfont_3_.addGlyph(charmetrics.c);
							}
						}
						ttfont_3_.createSubsetTables();
						int i_5_ = 0;
						ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
						bytearrayoutputstream.write(ttfont_3_
								.createSubsetHeader());
						Enumeration enumeration = ttfont_3_.tableslist.keys();
						while (enumeration.hasMoreElements()) {
							while ((i_5_ + bytearrayoutputstream.size() & 0x3) > 0)
								bytearrayoutputstream.write(0);
							i_5_ += bytearrayoutputstream.size();
							writeSFNTString(psoutputstream,
									bytearrayoutputstream.toByteArray());
							bytearrayoutputstream = new ByteArrayOutputStream();
							String string_6_ = (String) enumeration
									.nextElement();
							if (string_6_.equals("loca"))
								bytearrayoutputstream.write(ttfont_3_.loca);
							else if (string_6_.equals("glyf")) {
								int i_7_ = 0;
								for (int i_8_ = 0; i_8_ < ttfont_3_.numGlyphs; i_8_++) {
									i_5_ += i_7_;
									if (ttfont_3_.used.checkGlyph((char) i_8_)) {
										i_7_ = (int) ((ttfont_3_.glyphoffsets[i_8_ + 1]) - (ttfont_3_.glyphoffsets[i_8_]));
										if (bytearrayoutputstream.size() != 0) {
											writeSFNTString(psoutputstream,
													bytearrayoutputstream
															.toByteArray());
											bytearrayoutputstream = new ByteArrayOutputStream();
										}
										bytearrayoutputstream
												.write(
														ttfont_3_.glyphdata,
														(int) (ttfont_3_.glyphoffsets[i_8_]),
														i_7_);
									} else
										i_7_ = 0;
								}
							} else if (string_6_.equals("hmtx"))
								bytearrayoutputstream.write(ttfont_3_.hmtx);
							else if (string_6_.equals("vmtx"))
								bytearrayoutputstream.write(ttfont_3_.vmtx);
							else
								bytearrayoutputstream
										.write((byte[]) ttfont_3_.tables
												.get(string_6_));
						}
						writeSFNTString(psoutputstream, bytearrayoutputstream
								.toByteArray());
						break;
					} finally {
						seekableinput.close();
					}
				}
				byte[] is = new byte[i_2_];
				boolean bool = false;
				InputStream inputstream = fdesc.record.openFontStream();
				int i_9_;
				while ((i_9_ = inputstream.read(is)) != -1)
					writeSFNTString(psoutputstream, is, 0, i_9_);
			} catch (IOException ioexception) {
				psoutputstream.doc.errorHandler.exception(("Broken font file '"
						+ (fdesc.record.fontfile) + "'."), ioexception);
				break;
			}
		} while (false);
		psoutputstream.print("] def\n");
		psoutputstream.print("FontName currentdict end definefont pop\n");
	}

	private void writeSFNTString(PSOutputStream psoutputstream, byte[] is)
			throws IOException {
		writeSFNTString(psoutputstream, is, 0, is.length);
	}

	private void writeSFNTString(PSOutputStream psoutputstream, byte[] is,
			int i, int i_10_) throws IOException {
		if (i_10_ > 65535)
			throw new IOException("Font " + fdesc.record.getMetric().fontName
					+ " is unsupported, please report to manufacturer.");
		psoutputstream.print("<");
		new ASCIIHEX_Encode(psoutputstream).write(is, i, i_10_);
		psoutputstream.print(">\n");
	}
}