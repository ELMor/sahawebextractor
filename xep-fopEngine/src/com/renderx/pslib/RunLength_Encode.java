/*
 * RunLength_Encode - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class RunLength_Encode extends FilterOutputStream {
	byte[] buffer = new byte[128];

	boolean sameSequence = false;

	int length = 0;

	byte lastByte = 0;

	public RunLength_Encode(OutputStream outputstream) {
		super(outputstream);
	}

	public void write(int i) throws IOException {
		if (length == 0)
			buffer[0] = (byte) i;
		else if (sameSequence) {
			if (lastByte != (byte) i) {
				flushBuffer(length);
				buffer[0] = (byte) i;
				sameSequence = false;
			}
		} else if (lastByte == (byte) i) {
			flushBuffer(length - 1);
			sameSequence = true;
		} else
			buffer[length] = (byte) i;
		lastByte = (byte) i;
		if (++length >= 128)
			flushBuffer(length);
	}

	public void write(byte[] is) throws IOException {
		write(is, 0, is.length);
	}

	public void write(byte[] is, int i, int i_0_) throws IOException {
		for (int i_1_ = i; i_1_ < i + i_0_; i_1_++)
			write(is[i_1_]);
	}

	public void EOD() throws IOException {
		flushBuffer(length);
		super.write(128);
	}

	private void flushBuffer(int i) throws IOException {
		if (i != 0) {
			if (sameSequence) {
				super.write(257 - i);
				super.write(lastByte);
			} else {
				super.write(i - 1);
				for (int i_2_ = 0; i_2_ < i; i_2_++)
					super.write(buffer[i_2_]);
			}
			length -= i;
		}
	}
}