/*
 * License - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

public class License {
	public static boolean check(InputStream inputstream, String string)
			throws IOException {
		int i = string.indexOf(':');
		RSAPublicKey rsapublickey = new RSAPublicKey(new BigInteger(string
				.substring(0, i), 16), new BigInteger(string.substring(i + 1),
				16));
		CheckSigned checksigned = new CheckSigned();
		checksigned.load(inputstream);
		return checksigned.checkSignature(rsapublickey);
	}
}