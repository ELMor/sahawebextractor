/*
 * MD5 - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

import junit.framework.Assert;
import junit.framework.TestCase;

public class MD5 {
	private static final int HASH_LENGTH = 16;

	private static final int DATA_LENGTH = 64;

	private byte[] buffer;

	private int buffered;

	private long count;

	private static final long MAX_COUNT = 2305843009213693951L;

	private int[] data = new int[16];

	private int[] digest = new int[4];

	private byte[] tmp = new byte[64];

	public static final class Test extends TestCase {
		public void testMD5() {
			MD5 md5 = new MD5();
			Assert.assertEquals(Hex.toString(md5.digest("message digest"
					.getBytes())), "F96B697D7CB7938D525A2F31AAF161D0");
		}
	}

	public MD5() {
		reset();
		buffer = new byte[64];
	}

	public void update(byte i) {
		byte[] is = { i };
		update(is, 0, 1);
	}

	public void update(byte[] is) {
		update(is, 0, is.length);
	}

	public void update(byte[] is, int i, int i_0_) {
		count += (long) i_0_;
		if (count > 2305843009213693951L)
			throw new CryptoException("MD5: Maximum input length exceeded");
		int i_1_;
		for (int i_2_ = 64; i_0_ >= (i_1_ = i_2_ - buffered); buffered = 0) {
			System.arraycopy(is, i, buffer, buffered, i_1_);
			engineTransform(buffer);
			i_0_ -= i_1_;
			i += i_1_;
		}
		if (i_0_ > 0) {
			System.arraycopy(is, i, buffer, buffered, i_0_);
			buffered += i_0_;
		}
	}

	public byte[] digest() {
		return engineDigest(buffer, buffered);
	}

	public byte[] digest(byte[] is) {
		update(is, 0, is.length);
		return digest();
	}

	protected void reset() {
		buffered = 0;
		count = 0L;
		digest[0] = 1732584193;
		digest[1] = -271733879;
		digest[2] = -1732584194;
		digest[3] = 271733878;
	}

	protected long bitcount() {
		return count * 8L;
	}

	protected void engineTransform(byte[] is) {
		byte2int(is, 0, data, 0, 16);
		transform(data);
	}

	public byte[] engineDigest(byte[] is, int i) {
		int i_3_ = i;
		if (i_3_ != 0)
			System.arraycopy(is, 0, tmp, 0, i_3_);
		tmp[i_3_++] = (byte) -128;
		if (i_3_ > 56) {
			while (i_3_ < 64)
				tmp[i_3_++] = (byte) 0;
			byte2int(tmp, 0, data, 0, 16);
			transform(data);
			i_3_ = 0;
		}
		while (i_3_ < 56)
			tmp[i_3_++] = (byte) 0;
		byte2int(tmp, 0, data, 0, 14);
		long l = bitcount();
		data[14] = (int) l;
		data[15] = (int) (l >>> 32);
		transform(data);
		byte[] is_4_ = new byte[16];
		int i_5_ = 0;
		for (int i_6_ = 0; i_6_ < 4; i_6_++) {
			int i_7_ = digest[i_6_];
			is_4_[i_5_++] = (byte) i_7_;
			is_4_[i_5_++] = (byte) (i_7_ >>> 8);
			is_4_[i_5_++] = (byte) (i_7_ >>> 16);
			is_4_[i_5_++] = (byte) (i_7_ >>> 24);
		}
		reset();
		return is_4_;
	}

	protected static int F(int i, int i_8_, int i_9_) {
		return i_9_ ^ i & (i_8_ ^ i_9_);
	}

	protected static int G(int i, int i_10_, int i_11_) {
		return i_10_ ^ i_11_ & (i ^ i_10_);
	}

	protected static int H(int i, int i_12_, int i_13_) {
		return i ^ i_12_ ^ i_13_;
	}

	protected static int I(int i, int i_14_, int i_15_) {
		return i_14_ ^ (i | i_15_ ^ 0xffffffff);
	}

	protected static int FF(int i, int i_16_, int i_17_, int i_18_, int i_19_,
			int i_20_, int i_21_) {
		i += i_19_ + i_21_ + F(i_16_, i_17_, i_18_);
		i = i << i_20_ | i >>> -i_20_;
		return i + i_16_;
	}

	protected static int GG(int i, int i_22_, int i_23_, int i_24_, int i_25_,
			int i_26_, int i_27_) {
		i += i_25_ + i_27_ + G(i_22_, i_23_, i_24_);
		i = i << i_26_ | i >>> -i_26_;
		return i + i_22_;
	}

	protected static int HH(int i, int i_28_, int i_29_, int i_30_, int i_31_,
			int i_32_, int i_33_) {
		i += i_31_ + i_33_ + H(i_28_, i_29_, i_30_);
		i = i << i_32_ | i >>> -i_32_;
		return i + i_28_;
	}

	protected static int II(int i, int i_34_, int i_35_, int i_36_, int i_37_,
			int i_38_, int i_39_) {
		i += i_37_ + i_39_ + I(i_34_, i_35_, i_36_);
		i = i << i_38_ | i >>> -i_38_;
		return i + i_34_;
	}

	protected void transform(int[] is) {
		int i = digest[0];
		int i_40_ = digest[1];
		int i_41_ = digest[2];
		int i_42_ = digest[3];
		i = FF(i, i_40_, i_41_, i_42_, is[0], 7, -680876936);
		i_42_ = FF(i_42_, i, i_40_, i_41_, is[1], 12, -389564586);
		i_41_ = FF(i_41_, i_42_, i, i_40_, is[2], 17, 606105819);
		i_40_ = FF(i_40_, i_41_, i_42_, i, is[3], 22, -1044525330);
		i = FF(i, i_40_, i_41_, i_42_, is[4], 7, -176418897);
		i_42_ = FF(i_42_, i, i_40_, i_41_, is[5], 12, 1200080426);
		i_41_ = FF(i_41_, i_42_, i, i_40_, is[6], 17, -1473231341);
		i_40_ = FF(i_40_, i_41_, i_42_, i, is[7], 22, -45705983);
		i = FF(i, i_40_, i_41_, i_42_, is[8], 7, 1770035416);
		i_42_ = FF(i_42_, i, i_40_, i_41_, is[9], 12, -1958414417);
		i_41_ = FF(i_41_, i_42_, i, i_40_, is[10], 17, -42063);
		i_40_ = FF(i_40_, i_41_, i_42_, i, is[11], 22, -1990404162);
		i = FF(i, i_40_, i_41_, i_42_, is[12], 7, 1804603682);
		i_42_ = FF(i_42_, i, i_40_, i_41_, is[13], 12, -40341101);
		i_41_ = FF(i_41_, i_42_, i, i_40_, is[14], 17, -1502002290);
		i_40_ = FF(i_40_, i_41_, i_42_, i, is[15], 22, 1236535329);
		i = GG(i, i_40_, i_41_, i_42_, is[1], 5, -165796510);
		i_42_ = GG(i_42_, i, i_40_, i_41_, is[6], 9, -1069501632);
		i_41_ = GG(i_41_, i_42_, i, i_40_, is[11], 14, 643717713);
		i_40_ = GG(i_40_, i_41_, i_42_, i, is[0], 20, -373897302);
		i = GG(i, i_40_, i_41_, i_42_, is[5], 5, -701558691);
		i_42_ = GG(i_42_, i, i_40_, i_41_, is[10], 9, 38016083);
		i_41_ = GG(i_41_, i_42_, i, i_40_, is[15], 14, -660478335);
		i_40_ = GG(i_40_, i_41_, i_42_, i, is[4], 20, -405537848);
		i = GG(i, i_40_, i_41_, i_42_, is[9], 5, 568446438);
		i_42_ = GG(i_42_, i, i_40_, i_41_, is[14], 9, -1019803690);
		i_41_ = GG(i_41_, i_42_, i, i_40_, is[3], 14, -187363961);
		i_40_ = GG(i_40_, i_41_, i_42_, i, is[8], 20, 1163531501);
		i = GG(i, i_40_, i_41_, i_42_, is[13], 5, -1444681467);
		i_42_ = GG(i_42_, i, i_40_, i_41_, is[2], 9, -51403784);
		i_41_ = GG(i_41_, i_42_, i, i_40_, is[7], 14, 1735328473);
		i_40_ = GG(i_40_, i_41_, i_42_, i, is[12], 20, -1926607734);
		i = HH(i, i_40_, i_41_, i_42_, is[5], 4, -378558);
		i_42_ = HH(i_42_, i, i_40_, i_41_, is[8], 11, -2022574463);
		i_41_ = HH(i_41_, i_42_, i, i_40_, is[11], 16, 1839030562);
		i_40_ = HH(i_40_, i_41_, i_42_, i, is[14], 23, -35309556);
		i = HH(i, i_40_, i_41_, i_42_, is[1], 4, -1530992060);
		i_42_ = HH(i_42_, i, i_40_, i_41_, is[4], 11, 1272893353);
		i_41_ = HH(i_41_, i_42_, i, i_40_, is[7], 16, -155497632);
		i_40_ = HH(i_40_, i_41_, i_42_, i, is[10], 23, -1094730640);
		i = HH(i, i_40_, i_41_, i_42_, is[13], 4, 681279174);
		i_42_ = HH(i_42_, i, i_40_, i_41_, is[0], 11, -358537222);
		i_41_ = HH(i_41_, i_42_, i, i_40_, is[3], 16, -722521979);
		i_40_ = HH(i_40_, i_41_, i_42_, i, is[6], 23, 76029189);
		i = HH(i, i_40_, i_41_, i_42_, is[9], 4, -640364487);
		i_42_ = HH(i_42_, i, i_40_, i_41_, is[12], 11, -421815835);
		i_41_ = HH(i_41_, i_42_, i, i_40_, is[15], 16, 530742520);
		i_40_ = HH(i_40_, i_41_, i_42_, i, is[2], 23, -995338651);
		i = II(i, i_40_, i_41_, i_42_, is[0], 6, -198630844);
		i_42_ = II(i_42_, i, i_40_, i_41_, is[7], 10, 1126891415);
		i_41_ = II(i_41_, i_42_, i, i_40_, is[14], 15, -1416354905);
		i_40_ = II(i_40_, i_41_, i_42_, i, is[5], 21, -57434055);
		i = II(i, i_40_, i_41_, i_42_, is[12], 6, 1700485571);
		i_42_ = II(i_42_, i, i_40_, i_41_, is[3], 10, -1894986606);
		i_41_ = II(i_41_, i_42_, i, i_40_, is[10], 15, -1051523);
		i_40_ = II(i_40_, i_41_, i_42_, i, is[1], 21, -2054922799);
		i = II(i, i_40_, i_41_, i_42_, is[8], 6, 1873313359);
		i_42_ = II(i_42_, i, i_40_, i_41_, is[15], 10, -30611744);
		i_41_ = II(i_41_, i_42_, i, i_40_, is[6], 15, -1560198380);
		i_40_ = II(i_40_, i_41_, i_42_, i, is[13], 21, 1309151649);
		i = II(i, i_40_, i_41_, i_42_, is[4], 6, -145523070);
		i_42_ = II(i_42_, i, i_40_, i_41_, is[11], 10, -1120210379);
		i_41_ = II(i_41_, i_42_, i, i_40_, is[2], 15, 718787259);
		i_40_ = II(i_40_, i_41_, i_42_, i, is[9], 21, -343485551);
		digest[0] += i;
		digest[1] += i_40_;
		digest[2] += i_41_;
		digest[3] += i_42_;
	}

	private static void byte2int(byte[] is, int i, int[] is_43_, int i_44_,
			int i_45_) {
		while (i_45_-- > 0)
			is_43_[i_44_++] = (is[i++] & 0xff | (is[i++] & 0xff) << 8
					| (is[i++] & 0xff) << 16 | (is[i++] & 0xff) << 24);
	}
}