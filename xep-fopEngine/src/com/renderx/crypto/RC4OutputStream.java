/*
 * RC4OutputStream - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class RC4OutputStream extends FilterOutputStream {
	private RC4 cipher = null;

	public RC4OutputStream(OutputStream outputstream, byte[] is) {
		super(outputstream);
		cipher = new RC4(is);
	}

	public synchronized void write(byte[] is, int i, int i_0_)
			throws IOException {
		if (cipher == null)
			throw new IOException("stream closed");
		if (i_0_ > 0) {
			byte[] is_1_ = new byte[i_0_];
			cipher.crypt(is, i, i_0_, is_1_, 0);
			for (int i_2_ = 0; i_2_ < i_0_; i_2_++)
				out.write(is_1_[i_2_]);
		}
	}

	public synchronized void write(int i) throws IOException {
		byte[] is = { (byte) i };
		write(is, 0, 1);
	}

	public synchronized void flush() throws IOException {
		if (cipher == null)
			throw new IOException("stream closed");
		super.flush();
	}

	public synchronized void close() throws IOException {
		if (cipher == null)
			throw new IOException("stream closed");
		super.flush();
		super.close();
		cipher = null;
	}
}