/*
 * CheckSigned - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import com.renderx.util.Args;
import com.renderx.util.Base64InputStream;

public final class CheckSigned {
	private static final int WAITING = 0;

	private static final int DATA_INFO = 1;

	private static final int DATA_FIRST = 2;

	private static final int DATA = 3;

	private static final int SIGNATURE_INFO = 4;

	private static final int SIGNATURE = 5;

	private static final int SIGNATURE_END = 6;

	private static final int END = 7;

	private byte[] PGPendOfLineData = { 13, 10 };

	private PGPSignature signature;

	private byte[] pgpHash;

	private int cr_count = 0;

	private int nextbyte = -1;

	public boolean checkSignature(RSAPublicKey rsapublickey) {
		if (signature != null)
			return signature.check(rsapublickey, pgpHash);
		return false;
	}

	private String readLine(InputStream inputstream) throws IOException {
		if (cr_count > 0) {
			if (--cr_count > 0)
				return "";
		} else
			nextbyte = inputstream.read();
		if (nextbyte == -1)
			return null;
		StringBuffer stringbuffer = new StringBuffer();
		while (nextbyte != 10 && nextbyte != -1) {
			if (nextbyte == 13)
				cr_count++;
			else {
				if (cr_count > 0)
					break;
				stringbuffer.append((char) (nextbyte & 0xff));
			}
			nextbyte = inputstream.read();
			if (nextbyte == 10)
				cr_count = 0;
		}
		return stringbuffer.toString();
	}

	public void load(InputStream inputstream) throws IOException {
		int i = 0;
		MD5 md5 = new MD5();
		Object object = null;
		Object object_0_ = null;
		String string = "";
		cr_count = 0;
		nextbyte = -1;
		String string_1_;
		while_0_: while ((string_1_ = readLine(inputstream)) != null) {
			switch (i) {
			case 0:
				if (string_1_.startsWith("-----BEGIN ")) {
					if (string_1_.startsWith("-----BEGIN PGP SIGNED MESSAGE"))
						i = 1;
					else
						throw new IOException("Bad begin : " + string_1_);
				}
				break;
			case 1:
				if (string_1_.length() == 0)
					i = 2;
				break;
			case 2:
			case 3:
				if (string_1_.startsWith("-----END "))
					throw new IOException("Bad end : " + string_1_);
				if (string_1_.startsWith("-----BEGIN ")) {
					if (string_1_.startsWith("-----BEGIN PGP SIGNATURE"))
						i = 4;
					else
						throw new IOException("unexpected begin : " + string_1_);
				} else {
					if (string_1_.length() > 2 && string_1_.charAt(0) == '-')
						string_1_ = string_1_.substring(2);
					if (i != 3)
						i = 3;
					else
						md5.update(PGPendOfLineData);
					md5.update(ISOLatin1.toByteArray(string_1_, 0, string_1_
							.length()));
				}
				break;
			case 4:
				if (string_1_.length() == 0)
					i = 5;
				break;
			case 5:
				if (string_1_.startsWith("=")) {
					if (string_1_.length() != 5)
						throw new IOException("checksum length incorrect");
					byte[] is = base64decode(string_1_.substring(1, 5));
					int i_2_ = ((is[0] & 0xff) << 16 | (is[1] & 0xff) << 8 | is[2] & 0xff);
					byte[] is_3_ = base64decode(string);
					int i_4_ = PGPCRC.checksum(is_3_);
					if (i_2_ != i_4_)
						throw new IOException("Incorrect checksum");
					signature = new PGPSignature(new DataInputStream(
							new ByteArrayInputStream(is_3_)));
					signature.addExtasToHash(md5);
					pgpHash = md5.digest();
					i = 6;
				} else
					string += (String) string_1_;
				break;
			case 6:
				if (!string_1_.startsWith("-----END "))
					throw new IOException("End excected." + string_1_);
				break while_0_;
			default:
				throw new IOException("State machine failure.");
			}
		}
	}

	public byte[] base64decode(String string) throws IOException {
		Base64InputStream base64inputstream = new Base64InputStream(string);
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		Object object = null;
		byte[] is;
		try {
			byte[] is_5_ = new byte[1024];
			boolean bool = false;
			int i;
			while ((i = base64inputstream.read(is_5_)) != -1)
				bytearrayoutputstream.write(is_5_, 0, i);
			is = bytearrayoutputstream.toByteArray();
		} finally {
			base64inputstream.close();
			bytearrayoutputstream.close();
		}
		return is;
	}

	public static void main(String[] strings) throws IOException {
		Args args = new Args(strings);
		if (args.arguments.length() != 1) {
			System.err
					.println("call: java com.renderx.crypto.CheckSigned publickey\n");
			System.exit(1);
		}
		CheckSigned checksigned = new CheckSigned();
		checksigned.load(System.in);
		String string = (String) args.arguments.car();
		int i = string.indexOf(':');
		RSAPublicKey rsapublickey = new RSAPublicKey(new BigInteger(string
				.substring(0, i), 16), new BigInteger(string.substring(i + 1),
				16));
		boolean bool = checksigned.checkSignature(rsapublickey);
		System.out.println(bool ? "OK" : "ERROR");
	}
}