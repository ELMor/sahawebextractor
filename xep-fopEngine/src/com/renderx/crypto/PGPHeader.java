/*
 * PGPHeader - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

import java.io.DataInput;
import java.io.IOException;

public final class PGPHeader {
	private static final int CTB_TYPE_MASK = 124;

	private static final int CTB_TYPE_SHIFT = 2;

	private static final int CTB_LLEN_MASK = 3;

	public int type;

	public int len;

	public int lensize;

	public PGPHeader(DataInput datainput) throws IOException {
		read(datainput);
	}

	public void read(DataInput datainput) throws IOException {
		byte i = datainput.readByte();
		type = (byte) ctbToType(i);
		byte i_0_ = (byte) ctbToLengthSpecifier(i);
		switch (i_0_) {
		case 0:
			len = datainput.readByte() & 0xff;
			break;
		case 1:
			len = datainput.readShort() & 0xffff;
			break;
		case 2:
			len = datainput.readInt();
			break;
		case 3:
			len = -1;
			break;
		default:
			throw new CryptoException("Bad value for CTB (" + i_0_ + ")");
		}
		lensize = 0;
	}

	public static final int ctbToType(int i) {
		return (i & 0x7c) >> 2;
	}

	public static final int ctbToLengthSpecifier(int i) {
		return i & 0x3;
	}
}