/*
 * SVGParserHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.FontCatalog;
import com.renderx.fonts.KernVector;
import com.renderx.graphics.ImageFactory;
import com.renderx.graphics.ImageFormatException;
import com.renderx.graphics.UnregisteredMIMETypeException;
import com.renderx.graphics.vector.ExternalImage;
import com.renderx.graphics.vector.FontSpec;
import com.renderx.graphics.vector.GraphicGroup;
import com.renderx.graphics.vector.ImageTree;
import com.renderx.graphics.vector.Path;
import com.renderx.graphics.vector.Subpath;
import com.renderx.graphics.vector.Text;
import com.renderx.sax.InputSource;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.svg.css.StyleSheet;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class SVGParserHandler extends DefaultHandler {
	Hashtable idtable = new Hashtable();

	int skip = 0;

	public static final String WHITESPACE = " \t\n\r";

	private final ImageFactory imageFactory;

	private final ErrorHandler errHandler;

	private final FontCatalog fontCatalog;

	protected final Hashtable fontCache = new Hashtable();

	public ImageTree tree = null;

	private SVGParserContext context = null;

	private StyleSheet stylesheet = null;

	StringBuffer styleText = null;

	private URLSpec baseURL = null;

	public double textOriginX = 0.0;

	public double textOriginY = 0.0;

	public double textOffsetX = 0.0;

	public double textOffsetY = 0.0;

	public int currentTextAnchor = -2147483648;

	public double nextGlyphPosX = 0.0;

	public double nextGlyphPosY = 0.0;

	public List textX = null;

	public List textY = null;

	public List textDX = null;

	public List textDY = null;

	public List textRotate = null;

	public GraphicGroup textGroup = null;

	boolean drawNextBlank = false;

	CharMetrics lastMetric = null;

	Text lastTextContour = null;

	boolean textOpacityPending = false;

	boolean clipPathPending = false;

	final Hashtable clipPathTable = new Hashtable();

	final Hashtable markerTable = new Hashtable();

	SVGParserHandler(ImageFactory imagefactory, FontCatalog fontcatalog,
			ErrorHandler errorhandler, double d, double d_0_) {
		imageFactory = imagefactory;
		fontCatalog = fontcatalog;
		errHandler = errorhandler;
		tree = new ImageTree(d, d_0_);
	}

	public void setBaseURL(URLSpec urlspec) {
		baseURL = urlspec;
	}

	private double getLength(Attributes attributes, String string, double d)
			throws SVGParseException {
		String string_1_ = getAttrValue(attributes, string);
		if (string_1_ == null)
			throw new SVGParseException("Missing attribute " + string
					+ " on element " + context.element.name);
		return context.extractLength(string_1_, d);
	}

	private double getOptionalLength(Attributes attributes, String string,
			double d, double d_2_) throws SVGParseException {
		String string_3_ = getAttrValue(attributes, string);
		if (string_3_ == null)
			return d_2_;
		try {
			return context.extractLength(string_3_, d);
		} catch (SVGParseException svgparseexception) {
			if (errHandler != null)
				errHandler.error("Attribute ignored: " + string + "=\""
						+ string_3_ + "\"; " + svgparseexception.getMessage());
			return d_2_;
		}
	}

	private static String getAttrValue(Attributes attributes, String string) {
		String string_4_ = "";
		if (string.startsWith("xml:")) {
			string_4_ = "http://www.w3.org/XML/1998/namespace";
			string = string.substring(4);
		} else if (string.startsWith("xlink:")) {
			string_4_ = "http://www.w3.org/1999/xlink";
			string = string.substring(6);
		}
		return attributes.getValue(string_4_, string);
	}

	private void applyTransformAttribute(Attributes attributes, String string)
			throws SVGParseException {
		String string_5_ = getAttrValue(attributes, string);
		if (string_5_ != null) {
			SVGAttrValue.TransformList transformlist = new SVGAttrValue.TransformList(
					string_5_);
			Enumeration enumeration = transformlist.transforms.elements();
			while (enumeration.hasMoreElements()) {
				SVGAttrValue.TransformList.Transform transform = ((SVGAttrValue.TransformList.Transform) enumeration
						.nextElement());
				if ("matrix".equals(transform.type))
					context.curGroup.transform(transform.params[0],
							transform.params[1], transform.params[2],
							transform.params[3], transform.params[4],
							transform.params[5]);
				else if ("translate".equals(transform.type))
					context.curGroup.translate(transform.params[0],
							transform.params[1]);
				else if ("rotate".equals(transform.type)) {
					context.curGroup.translate(transform.params[1],
							transform.params[2]);
					context.curGroup.rotate(transform.params[0]);
					context.curGroup.translate(-transform.params[1],
							-transform.params[2]);
				} else if ("scale".equals(transform.type))
					context.curGroup.scale(transform.params[0],
							transform.params[1]);
				else if ("skewX".equals(transform.type))
					context.curGroup.skew(transform.params[0], 0.0);
				else if ("skewY".equals(transform.type))
					context.curGroup.skew(0.0, transform.params[0]);
				else
					throw new SVGParseException(
							"Internal error: incorrect transformation step "
									+ transform.type);
			}
		}
	}

	private GraphicGroup getTransformedGroup(Attributes attributes)
			throws SVGParseException {
		String string = getAttrValue(attributes, "transform");
		if (string != null || context.opacity != 1.0
				|| context.clipPath != null)
			pushGroup();
		if (string != null)
			applyTransformAttribute(attributes, "transform");
		if (context.clipPath != null)
			applyClipping();
		setOpacity();
		return context.curGroup;
	}

	private void setOpacity() throws SVGParseException {
		if (context.opacity != 1.0)
			context.curGroup.opacity = context.opacity;
	}

	private void applyClipping() throws SVGParseException {
		if (context.clipPath != null) {
			GraphicGroup graphicgroup = (GraphicGroup) clipPathTable
					.get(context.clipPath);
			if (graphicgroup == null) {
				if (errHandler != null)
					errHandler.error("No 'clipPath' element found with id='"
							+ context.clipPath + "'");
			} else if (graphicgroup.children.length() != 0)
				context.curGroup.clippath = graphicgroup;
		}
	}

	private GraphicGroup pushGroup() {
		GraphicGroup graphicgroup = new GraphicGroup();
		if (context.curGroup != null)
			context.curGroup.append(graphicgroup);
		context.curGroup = graphicgroup;
		return graphicgroup;
	}

	private void setViewport(Attributes attributes, double d, double d_6_)
			throws SVGParseException {
		context.setViewport(d, d_6_);
		String string = getAttrValue(attributes, "overflow");
		if (string == null || !"visible".equals(string.trim()))
			context.curGroup.clipRect(0.0, 0.0, d, d_6_);
	}

	private void setViewbox(Attributes attributes) throws SVGParseException {
		String string = getAttrValue(attributes, "viewBox");
		if (string != null) {
			StringTokenizer stringtokenizer = new StringTokenizer(string,
					", \t\n\r");
			if (!stringtokenizer.hasMoreTokens())
				throw new SVGParseException(
						"Invalid viewBox attribute: no data");
			double d = new SVGAttrValue.Length(stringtokenizer.nextToken()).len;
			if (!stringtokenizer.hasMoreTokens())
				throw new SVGParseException(
						"Invalid viewBox attribute: required 4 numbers, only 1 found");
			double d_7_ = new SVGAttrValue.Length(stringtokenizer.nextToken()).len;
			if (!stringtokenizer.hasMoreTokens())
				throw new SVGParseException(
						"Invalid viewBox attribute: required 4 numbers, only 2 found");
			double d_8_ = new SVGAttrValue.Length(stringtokenizer.nextToken()).len;
			if (!stringtokenizer.hasMoreTokens())
				throw new SVGParseException(
						"Invalid viewBox attribute: required 4 numbers, only 3 found");
			double d_9_ = new SVGAttrValue.Length(stringtokenizer.nextToken()).len;
			if (stringtokenizer.hasMoreTokens())
				throw new SVGParseException(
						"Invalid viewBox attribute: extra data after 4 numbers");
			scaleToViewport(attributes, d, d_7_, d_8_, d_9_);
		}
	}

	private void scaleToViewport(Attributes attributes, double d, double d_10_,
			double d_11_, double d_12_) throws SVGParseException {
		boolean bool = true;
		boolean bool_13_ = true;
		double d_14_ = 0.5;
		double d_15_ = 0.5;
		String string = getAttrValue(attributes, "preserveAspectRatio");
		if (string != null) {
			StringTokenizer stringtokenizer = new StringTokenizer(string);
			if (!stringtokenizer.hasMoreTokens())
				throw new SVGParseException(
						"Invalid preserveAspectRatio attribute: no data");
			String string_16_ = stringtokenizer.nextToken();
			if (string_16_.equals("defer")) {
				if (!stringtokenizer.hasMoreTokens())
					throw new SVGParseException(
							"Invalid preserveAspectRatio attribute: 'defer' shall be followed by alignment specifier");
				string_16_ = stringtokenizer.nextToken();
			}
			if ("none".equals(string_16_))
				bool = false;
			else if ("xMinYMin".equals(string_16_)) {
				d_14_ = 0.0;
				d_15_ = 0.0;
			} else if ("xMinYMid".equals(string_16_)) {
				d_14_ = 0.0;
				d_15_ = 0.5;
			} else if ("xMinYMax".equals(string_16_)) {
				d_14_ = 0.0;
				d_15_ = 1.0;
			} else if ("xMidYMin".equals(string_16_)) {
				d_14_ = 0.5;
				d_15_ = 0.0;
			} else if ("xMidYMid".equals(string_16_)) {
				d_14_ = 0.5;
				d_15_ = 0.5;
			} else if ("xMidYMax".equals(string_16_)) {
				d_14_ = 0.5;
				d_15_ = 1.0;
			} else if ("xMaxYMin".equals(string_16_)) {
				d_14_ = 1.0;
				d_15_ = 0.0;
			} else if ("xMaxYMid".equals(string_16_)) {
				d_14_ = 1.0;
				d_15_ = 0.5;
			} else if ("xMaxYMax".equals(string_16_)) {
				d_14_ = 1.0;
				d_15_ = 1.0;
			} else
				throw new SVGParseException(
						"Invalid alignment value in preserveAspectRatio: "
								+ string_16_);
			if (stringtokenizer.hasMoreTokens())
				bool_13_ = !"slice".equals(stringtokenizer.nextToken());
		}
		double d_17_ = context.viewportX / d_11_;
		double d_18_ = context.viewportY / d_12_;
		if (bool) {
			double d_19_ = d_17_ < d_18_ == bool_13_ ? d_17_ : d_18_;
			pushGroup().transform(d_19_, 0.0, 0.0, d_19_, -d * d_19_,
					-d_10_ * d_19_);
			context.curGroup.translate(
					((context.viewportX / d_19_ - d_11_) * d_14_),
					((context.viewportY / d_19_ - d_12_) * d_15_));
		} else
			pushGroup().transform(d_17_, 0.0, 0.0, d_18_, -d * d_17_,
					-d_10_ * d_18_);
		context.setViewport(d_11_, d_12_);
	}

	public void characters(char[] cs, int i, int i_20_) throws SAXException {
		if (skip <= 0) {
			if (styleText != null)
				styleText.append(cs, i, i_20_);
			else if (context.fonts != null && context.fonts.length != 0
					&& context.fonts[0] != null && textGroup != null) {
				CharMetrics charmetrics = null;
				for (int i_21_ = 0; i_21_ < context.fonts.length
						&& charmetrics == null; i_21_++)
					charmetrics = context.fonts[i_21_].getMetric().ucm(' ');
				CharMetrics charmetrics_22_ = context.fonts[0].getMetric().missingGlyph;
				if (charmetrics_22_ == null)
					charmetrics_22_ = charmetrics;
				double d = context.fontSize * context.fontStretch;
				if (charmetrics != null)
					d *= (double) charmetrics.wx / 1000.0;
				else if (charmetrics_22_ != null)
					d *= (double) charmetrics_22_.wx / 1000.0;
				d += context.wordSpacing;
				double d_23_ = context.fontSize * context.fontStretch;
				if (charmetrics_22_ != null)
					d_23_ *= (double) charmetrics_22_.wx / 1000.0;
				int i_24_ = i;
				int i_25_ = i + i_20_;
				while (i_24_ < i_25_) {
					if (" \t\n\r".indexOf(cs[i_24_]) != -1) {
						if (drawNextBlank)
							nextGlyphPosX += d + context.letterSpacing;
						lastMetric = null;
						lastTextContour = null;
						i_24_++;
						drawNextBlank = context.preserveSpace;
					} else {
						drawNextBlank = true;
						int i_26_ = getFontNumber(cs[i_24_]);
						double d_27_ = d_23_;
						if (i_26_ != -1) {
							CharMetrics charmetrics_28_ = context.fonts[i_26_]
									.getMetric().ucm(cs[i_24_]);
							double d_29_ = 0.0;
							if (lastMetric != null
									&& lastMetric.kernTable != null) {
								KernVector kernvector = ((KernVector) lastMetric.kernTable
										.get(cs[i_24_]));
								if (kernvector != null) {
									d_29_ = kernvector.X;
									nextGlyphPosX += (context.fontSize
											* context.fontStretch * d_29_ / 1000.0);
								}
							}
							d_27_ = (context.fontSize * context.fontStretch
									* (double) charmetrics_28_.wx / 1000.0);
							lastMetric = charmetrics_28_;
							if (context.textRotate != 0.0) {
								GraphicGroup graphicgroup = new GraphicGroup();
								lastTextContour = (new Text(
										context.fill,
										context.stroke,
										context.fillRule,
										context.clipRule,
										context.fillOpacity,
										context.strokeOpacity,
										new FontSpec(
												(context.fonts[i_26_].familyName),
												context.fontWeight,
												context.fontStyle,
												context.fontVariant,
												context.fontSize,
												context.fontStretch),
										new String(cs, i_24_, 1), 0.0, 0.0));
								graphicgroup
										.translate(
												nextGlyphPosX,
												(nextGlyphPosY + (context.baselineShift)));
								graphicgroup.rotate(-context.textRotate);
								graphicgroup.append(lastTextContour);
								textGroup.append(graphicgroup);
							} else if (d_29_ == 0.0 && lastTextContour != null
									&& context.letterSpacing == 0.0) {
								StringBuffer stringbuffer = new StringBuffer();
								Text text = lastTextContour;
								text.value = stringbuffer.append(text.value)
										.append(cs[i_24_]).toString();
							} else {
								lastTextContour = new Text(
										context.fill,
										context.stroke,
										context.fillRule,
										context.clipRule,
										context.fillOpacity,
										context.strokeOpacity,
										new FontSpec(
												(context.fonts[i_26_].familyName),
												context.fontWeight,
												context.fontStyle,
												(context.fontVariant),
												context.fontSize,
												(context.fontStretch)),
										new String(cs, i_24_, 1),
										nextGlyphPosX,
										(nextGlyphPosY + context.baselineShift));
								textGroup.append(lastTextContour);
							}
						}
						textOffsetX = nextGlyphPosX + d_27_;
						textOffsetY = nextGlyphPosY;
						nextGlyphPosX = textOffsetX + context.letterSpacing;
						i_24_++;
					}
				}
			}
		}
	}

	private int getFontNumber(char c) {
		for (int i = 0; i < context.fonts.length; i++) {
			if (context.fonts[i].getMetric().exists(c))
				return i;
		}
		return -1;
	}

	public void endElement(String string, String string_30_, String string_31_)
			throws SAXException {
		if (skip > 0)
			skip--;
		else {
			lastMetric = null;
			lastTextContour = null;
			try {
				switch (context.element.id) {
				case 2:
					clipPathPending = false;
					break;
				case 18:
					closeTextChunk();
					textOriginX = textOffsetX = nextGlyphPosX = 0.0;
					textOriginY = textOffsetY = nextGlyphPosY = 0.0;
					textOpacityPending = false;
					break;
				case 20:
					textOpacityPending = false;
					break;
				case 22: {
					String string_32_ = styleText.toString();
					styleText = null;
					stylesheet = new StyleSheet(string_32_, errHandler);
					break;
				}
				}
			} catch (Exception exception) {
				if (errHandler != null)
					errHandler.exception("", exception);
			}
			context = context.prev;
		}
	}

	public void startElement(String string, String string_33_,
			String string_34_, Attributes attributes) throws SAXException {
		if (skip > 0)
			skip++;
		else {
			lastMetric = null;
			lastTextContour = null;
			SVGElement svgelement = SVGElement.get(string, string_33_);
			if (svgelement == null)
				skip = 1;
			else if (styleText != null)
				skip = 1;
			else {
				context = SVGParserContext.createContext(context, svgelement,
						baseURL, fontCatalog, fontCache, errHandler);
				if (!svgelement.transparent) {
					try {
						context.setProperties(attributes, stylesheet,
								errHandler);
						if (context.hidden) {
							context = context.prev;
							skip = 1;
						} else if (tree.root == null) {
							if (svgelement.id != 17)
								throw new SVGParseException(
										"Top-level element for an SVG image must be 'svg'");
							if (tree.width == 0.0)
								tree.width = getLength(attributes, "width", 0.0);
							if (tree.height == 0.0)
								tree.height = getLength(attributes, "height",
										0.0);
							tree.root = context.curGroup = new GraphicGroup();
							context.curGroup.transform(1.0, 0.0, 0.0, -1.0,
									0.0, tree.height);
							setViewport(attributes, tree.width, tree.height);
							setViewbox(attributes);
							setOpacity();
						} else {
							switch (svgelement.id) {
							case 17: {
								if (clipPathPending)
									throw new SVGParseException(
											"SVG element 'svg' is not allowed as a descendant of 'clipPath' element");
								double d = getOptionalLength(attributes, "x",
										context.viewportX, 0.0);
								double d_35_ = getOptionalLength(attributes,
										"y", context.viewportY, 0.0);
								double d_36_ = getOptionalLength(attributes,
										"width", context.viewportX,
										context.viewportX);
								double d_37_ = getOptionalLength(attributes,
										"height", context.viewportY,
										context.viewportY);
								pushGroup();
								context.curGroup.translate(d, d_35_);
								setViewport(attributes, d_36_, d_37_);
								setViewbox(attributes);
								applyClipping();
								setOpacity();
								break;
							}
							case 5: {
								if (clipPathPending && context.clipPath != null
										&& errHandler != null) {
									context.clipPath = null;
									errHandler
											.warning("'clip-path' property is not supported on SVG 'clipPath' element and its descendants.");
								}
								GraphicGroup graphicgroup = new GraphicGroup();
								context.curGroup.append(graphicgroup);
								context.curGroup = graphicgroup;
								applyTransformAttribute(attributes, "transform");
								applyClipping();
								setOpacity();
								break;
							}
							case 3: {
								if (clipPathPending)
									throw new SVGParseException(
											"SVG element 'defs' is not allowed as a descendant of 'clipPath' element");
								GraphicGroup graphicgroup = new GraphicGroup();
								context.curGroup.append(graphicgroup);
								context.curGroup = graphicgroup;
								setOpacity();
								break;
							}
							case 9: {
								if (clipPathPending)
									throw new SVGParseException(
											"SVG element 'marker' is not allowed as a descendant of 'clipPath' element");
								String string_38_ = getAttrValue(attributes,
										"id");
								double d = getOptionalLength(attributes,
										"markerWidth", 0.0, 1.7999999999999998);
								double d_39_ = getOptionalLength(attributes,
										"markerHeight", 0.0, 1.7999999999999998);
								GraphicGroup graphicgroup = new GraphicGroup();
								context.curGroup = graphicgroup;
								setViewport(attributes, d, d_39_);
								setViewbox(attributes);
								setOpacity();
								double d_40_ = getOptionalLength(attributes,
										"refX", context.viewportX, 0.0);
								double d_41_ = getOptionalLength(attributes,
										"refY", context.viewportY, 0.0);
								double d_42_ = (context.curGroup.matrix[0]
										* d_40_ + context.curGroup.matrix[2]
										* d_41_ + context.curGroup.matrix[4]);
								double d_43_ = (context.curGroup.matrix[1]
										* d_40_ + context.curGroup.matrix[3]
										* d_41_ + context.curGroup.matrix[5]);
								if (d_42_ != 0.0 || d_43_ != 0.0) {
									GraphicGroup graphicgroup_44_ = new GraphicGroup();
									graphicgroup_44_.translate(-d_42_, -d_43_);
									graphicgroup_44_.append(graphicgroup);
									graphicgroup = graphicgroup_44_;
								}
								String string_45_ = getAttrValue(attributes,
										"markerUnits");
								String string_46_ = getAttrValue(attributes,
										"orient");
								SVGMarker svgmarker = new SVGMarker(string_38_,
										graphicgroup, d, d_39_,
										(string_45_ == null ? "strokeWidth"
												: string_45_),
										(string_46_ == null ? "0" : string_46_));
								markerTable.put(string_38_, svgmarker);
								break;
							}
							case 20:
								if (context.opacity != 1.0
										&& errHandler != null)
									errHandler
											.warning("Opacity is not supported on SVG 'tspan' element.");
								if (getAttrValue(attributes, "x") != null) {
									closeTextChunk();
									openTextChunk(attributes);
								} else
									startTextSpan(attributes);
								break;
							case 18:
								if (clipPathPending && context.clipPath != null
										&& errHandler != null) {
									context.clipPath = null;
									errHandler
											.warning("'clip-path' property is not supported on SVG 'clipPath' element and its descendants.");
								}
								if (context.opacity != 1.0)
									textOpacityPending = true;
								getTransformedGroup(attributes);
								openTextChunk(attributes);
								break;
							case 15: {
								if (clipPathPending && context.clipPath != null
										&& errHandler != null) {
									context.clipPath = null;
									errHandler
											.warning("'clip-path' property is not supported on SVG 'clipPath' element and its descendants.");
								}
								double d = getLength(attributes, "width",
										context.viewportX);
								double d_47_ = getLength(attributes, "height",
										context.viewportY);
								if (!(d <= 0.0) && !(d_47_ <= 0.0)) {
									double d_48_ = getOptionalLength(
											attributes, "x", context.viewportX,
											0.0);
									double d_49_ = getOptionalLength(
											attributes, "y", context.viewportY,
											0.0);
									String string_50_ = getAttrValue(
											attributes, "rx");
									String string_51_ = getAttrValue(
											attributes, "ry");
									double d_52_ = 0.0;
									double d_53_ = 0.0;
									if (string_50_ != null
											|| string_51_ != null) {
										if (string_50_ != null)
											d_52_ = (context.extractLength(
													string_50_,
													context.viewportX));
										if (string_51_ != null)
											d_53_ = (context.extractLength(
													string_51_,
													context.viewportY));
										if (string_50_ == null)
											d_52_ = d_53_;
										if (string_51_ == null)
											d_53_ = d_52_;
									}
									Path path = new Path(context.fill,
											context.stroke, context.fillRule,
											context.clipRule,
											context.fillOpacity,
											context.strokeOpacity);
									if (d_52_ <= 0.0 || d_53_ <= 0.0)
										path.rectangle(d_48_, d_49_, d, d_47_);
									else {
										if (d_52_ * 2.0 > d)
											d_52_ = d / 2.0;
										if (d_53_ * 2.0 > d_47_)
											d_53_ = d_47_ / 2.0;
										path.moveto(d_48_ + d_52_, d_49_);
										path.lineto(d_48_ + d - d_52_, d_49_);
										path.quarterarcto(d_48_ + d, d_49_
												+ d_53_, true);
										path.lineto(d_48_ + d, d_49_ + d_47_
												- d_53_);
										path.quarterarcto(d_48_ + d - d_52_,
												d_49_ + d_47_, false);
										path.lineto(d_48_ + d_52_, d_49_
												+ d_47_);
										path.quarterarcto(d_48_,
												(d_49_ + d_47_ - d_53_), true);
										path.lineto(d_48_, d_49_ + d_53_);
										path.quarterarcto(d_48_ + d_52_, d_49_,
												false);
										path.closepath();
									}
									getTransformedGroup(attributes)
											.append(path);
									break;
								}
								break;
							}
							case 1: {
								if (clipPathPending && context.clipPath != null
										&& errHandler != null) {
									context.clipPath = null;
									errHandler
											.warning("'clip-path' property is not supported on SVG 'clipPath' element and its descendants.");
								}
								double d = getLength(attributes, "r",
										context.viewportXY);
								if (!(d <= 0.0)) {
									double d_54_ = getOptionalLength(
											attributes, "cx",
											context.viewportX, 0.0);
									double d_55_ = getOptionalLength(
											attributes, "cy",
											context.viewportY, 0.0);
									Path path = new Path(context.fill,
											context.stroke, context.fillRule,
											context.clipRule,
											context.fillOpacity,
											context.strokeOpacity);
									path.circle(d_54_, d_55_, d);
									getTransformedGroup(attributes)
											.append(path);
									break;
								}
								break;
							}
							case 4: {
								if (clipPathPending && context.clipPath != null
										&& errHandler != null) {
									context.clipPath = null;
									errHandler
											.warning("'clip-path' property is not supported on SVG 'clipPath' element and its descendants.");
								}
								double d = getLength(attributes, "rx",
										context.viewportX);
								double d_56_ = getLength(attributes, "ry",
										context.viewportY);
								if (!(d <= 0.0) && !(d_56_ <= 0.0)) {
									double d_57_ = getOptionalLength(
											attributes, "cx",
											context.viewportX, 0.0);
									double d_58_ = getOptionalLength(
											attributes, "cy",
											context.viewportY, 0.0);
									Path path = new Path(context.fill,
											context.stroke, context.fillRule,
											context.clipRule,
											context.fillOpacity,
											context.strokeOpacity);
									path.ellipse(d_57_, d_58_, d, d_56_);
									getTransformedGroup(attributes)
											.append(path);
									break;
								}
								break;
							}
							case 7: {
								if (clipPathPending && context.clipPath != null
										&& errHandler != null) {
									context.clipPath = null;
									errHandler
											.warning("'clip-path' property is not supported on SVG 'clipPath' element and its descendants.");
								}
								Path path = new Path(null, context.stroke,
										context.fillRule, context.clipRule,
										context.fillOpacity,
										context.strokeOpacity);
								path.moveto(getOptionalLength(attributes, "x1",
										(context.viewportX), 0.0),
										getOptionalLength(attributes, "y1",
												(context.viewportY), 0.0));
								path.lineto(getOptionalLength(attributes, "x2",
										(context.viewportX), 0.0),
										getOptionalLength(attributes, "y2",
												(context.viewportY), 0.0));
								getTransformedGroup(attributes).append(path);
								appendMarkers(path);
								break;
							}
							case 10: {
								if (clipPathPending && context.clipPath != null
										&& errHandler != null) {
									context.clipPath = null;
									errHandler
											.warning("'clip-path' property is not supported on SVG 'clipPath' element and its descendants.");
								}
								Path path = new Path(context.fill,
										context.stroke, context.fillRule,
										context.clipRule, context.fillOpacity,
										context.strokeOpacity);
								SVGAttrValue.PathData pathdata = (new SVGAttrValue.PathData(
										getAttrValue(attributes, "d")));
								Enumeration enumeration = pathdata.chunks
										.elements();
								double d = 0.0;
								double d_59_ = 0.0;
								double d_60_ = 0.0;
								double d_61_ = 0.0;
								while (enumeration.hasMoreElements()) {
									SVGAttrValue.PathData.Chunk chunk = ((SVGAttrValue.PathData.Chunk) enumeration
											.nextElement());
									switch (chunk.command) {
									case 'M':
										path.moveto(chunk.params[0],
												chunk.params[1]);
										d = d_59_ = d_60_ = d_61_ = 0.0;
										break;
									case 'm':
										path.moveto(
												(path.curX + chunk.params[0]),
												(path.curY + chunk.params[1]));
										d = d_59_ = d_60_ = d_61_ = 0.0;
										break;
									case 'Z':
									case 'z':
										path.closepath();
										d = d_59_ = d_60_ = d_61_ = 0.0;
										break;
									case 'L':
										path.lineto(chunk.params[0],
												chunk.params[1]);
										d = d_59_ = d_60_ = d_61_ = 0.0;
										break;
									case 'l':
										path.lineto(
												(path.curX + chunk.params[0]),
												(path.curY + chunk.params[1]));
										d = d_59_ = d_60_ = d_61_ = 0.0;
										break;
									case 'V':
										path.lineto(path.curX, chunk.params[0]);
										d = d_59_ = d_60_ = d_61_ = 0.0;
										break;
									case 'v':
										path.lineto(path.curX,
												(path.curY + chunk.params[0]));
										d = d_59_ = d_60_ = d_61_ = 0.0;
										break;
									case 'H':
										path.lineto(chunk.params[0], path.curY);
										d = d_59_ = d_60_ = d_61_ = 0.0;
										break;
									case 'h':
										path.lineto(
												(path.curX + chunk.params[0]),
												path.curY);
										d = d_59_ = d_60_ = d_61_ = 0.0;
										break;
									case 'C':
										path.curveto(chunk.params[0],
												chunk.params[1],
												chunk.params[2],
												chunk.params[3],
												chunk.params[4],
												chunk.params[5]);
										d = chunk.params[4] - chunk.params[2];
										d_59_ = (chunk.params[5] - chunk.params[3]);
										d_60_ = d_61_ = 0.0;
										break;
									case 'c':
										path.curveto(path.curX
												+ chunk.params[0], path.curY
												+ chunk.params[1], path.curX
												+ chunk.params[2], path.curY
												+ chunk.params[3], path.curX
												+ chunk.params[4], path.curY
												+ chunk.params[5]);
										d = chunk.params[4] - chunk.params[2];
										d_59_ = (chunk.params[5] - chunk.params[3]);
										d_60_ = d_61_ = 0.0;
										break;
									case 'S':
										path.curveto(path.curX + d, path.curY
												+ d_59_, chunk.params[0],
												chunk.params[1],
												chunk.params[2],
												chunk.params[3]);
										d = chunk.params[2] - chunk.params[0];
										d_59_ = (chunk.params[3] - chunk.params[1]);
										d_60_ = d_61_ = 0.0;
										break;
									case 's':
										path.curveto(path.curX + d, path.curY
												+ d_59_, path.curX
												+ chunk.params[0], path.curY
												+ chunk.params[1], path.curX
												+ chunk.params[2], path.curY
												+ chunk.params[3]);
										d = chunk.params[2] - chunk.params[0];
										d_59_ = (chunk.params[3] - chunk.params[1]);
										d_60_ = d_61_ = 0.0;
										break;
									case 'Q':
										path.quadcurveto(chunk.params[0],
												chunk.params[1],
												chunk.params[2],
												chunk.params[3]);
										d_60_ = (chunk.params[2] - chunk.params[0]);
										d_61_ = (chunk.params[3] - chunk.params[1]);
										d = d_59_ = 0.0;
										break;
									case 'q':
										path.quadcurveto(path.curX
												+ chunk.params[0], path.curY
												+ chunk.params[1], path.curX
												+ chunk.params[2], path.curY
												+ chunk.params[3]);
										d_60_ = (chunk.params[2] - chunk.params[0]);
										d_61_ = (chunk.params[3] - chunk.params[1]);
										d = d_59_ = 0.0;
										break;
									case 'T':
										path.quadcurveto(path.curX + d_60_,
												path.curY + d_61_,
												chunk.params[0],
												chunk.params[1]);
										d_60_ = chunk.params[0]
												- (path.curX + d_60_);
										d_61_ = chunk.params[1]
												- (path.curY + d_61_);
										d = d_59_ = 0.0;
										break;
									case 't':
										path.quadcurveto(path.curX + d_60_,
												path.curY + d_61_,
												(path.curX + chunk.params[0]),
												(path.curY + chunk.params[1]));
										d_60_ = chunk.params[0] - d_60_;
										d_61_ = chunk.params[1] - d_61_;
										d = d_59_ = 0.0;
										break;
									case 'A':
										path.arcto(chunk.params[0],
												chunk.params[1],
												chunk.params[2],
												chunk.params[3] != 0.0,
												chunk.params[4] != 0.0,
												chunk.params[5],
												chunk.params[6]);
										d = d_59_ = d_60_ = d_61_ = 0.0;
										break;
									case 'a':
										path.arcto(chunk.params[0],
												chunk.params[1],
												chunk.params[2],
												chunk.params[3] != 0.0,
												chunk.params[4] != 0.0,
												path.curX + chunk.params[5],
												(path.curY + chunk.params[6]));
										d = d_59_ = d_60_ = d_61_ = 0.0;
										break;
									default:
										throw new SVGParseException(
												"Internal error: invalid command in parsed path data");
									}
								}
								getTransformedGroup(attributes).append(path);
								appendMarkers(path);
								break;
							}
							case 11:
							case 12: {
								if (clipPathPending && context.clipPath != null
										&& errHandler != null) {
									context.clipPath = null;
									errHandler
											.warning("'clip-path' property is not supported on SVG 'clipPath' element and its descendants.");
								}
								Path path = new Path(context.fill,
										context.stroke, context.fillRule,
										context.clipRule, context.fillOpacity,
										context.strokeOpacity);
								String string_62_ = getAttrValue(attributes,
										"points");
								if (string_62_ == null)
									throw new SVGParseException(
											"'"
													+ svgelement.name
													+ "' element misses required 'points' attribute");
								StringTokenizer stringtokenizer = new StringTokenizer(
										string_62_, ", \t\r\n");
								boolean bool = true;
								while (stringtokenizer.hasMoreTokens()) {
									double d = (context.extractLength(
											stringtokenizer.nextToken(),
											context.viewportX));
									if (!stringtokenizer.hasMoreTokens())
										throw new SVGParseException(
												"Odd number of tokens in 'points' attribute");
									double d_63_ = (context.extractLength(
											stringtokenizer.nextToken(),
											context.viewportY));
									if (bool) {
										path.moveto(d, d_63_);
										bool = false;
									} else
										path.lineto(d, d_63_);
								}
								if (svgelement.id == 11)
									path.closepath();
								getTransformedGroup(attributes).append(path);
								appendMarkers(path);
								break;
							}
							case 6: {
								if (clipPathPending)
									throw new SVGParseException(
											"SVG element 'image' is not allowed as a descendant of 'clipPath' element");
								String string_64_ = getAttrValue(attributes,
										"xlink:href");
								if (string_64_ == null)
									throw new SVGParseException(
											"svg:image has no xlink:href attribute");
								Object object = null;
								ExternalImage externalimage;
								try {
									externalimage = (new ExternalImage(
											imageFactory
													.makeImage(new URLSpec(
															context.baseURL,
															string_64_))));
								} catch (IOException ioexception) {
									throw new SVGParseException(ioexception
											.toString());
								} catch (ImageFormatException imageformatexception) {
									throw new SVGParseException(
											imageformatexception.toString());
								} catch (UnregisteredMIMETypeException unregisteredmimetypeexception) {
									throw new SVGParseException(
											unregisteredmimetypeexception
													.toString());
								}
								double d = getOptionalLength(attributes,
										"width", context.viewportX,
										(externalimage.image.width));
								double d_65_ = getOptionalLength(attributes,
										"height", context.viewportY,
										(externalimage.image.height));
								if (!(d <= 0.0) && !(d_65_ <= 0.0)) {
									double d_66_ = getOptionalLength(
											attributes, "x", context.viewportX,
											0.0);
									double d_67_ = getOptionalLength(
											attributes, "y", context.viewportY,
											0.0);
									pushGroup();
									context.curGroup.translate(d_66_, d_67_);
									setViewport(attributes, d, d_65_);
									String string_68_ = getAttrValue(
											attributes, "transform");
									if (string_68_ != null)
										applyTransformAttribute(attributes,
												"transform");
									applyClipping();
									setOpacity();
									scaleToViewport(attributes, 0.0, 0.0,
											externalimage.image.width,
											(externalimage.image.height));
									context.curGroup.transform(1.0, 0.0, 0.0,
											-1.0, 0.0,
											(externalimage.image.height));
									context.curGroup.append(externalimage);
									break;
								}
								break;
							}
							case 2: {
								if (clipPathPending)
									throw new SVGParseException(
											"SVG element 'clipPath' is not allowed as a descendant of 'clipPath' element");
								if (context.clipPath != null
										&& errHandler != null) {
									context.clipPath = null;
									errHandler
											.warning("'clip-path' property is not supported on SVG 'clipPath' element and its descendants.");
								}
								String string_69_ = getAttrValue(attributes,
										"id");
								if (string_69_ == null)
									throw new SVGParseException(
											"SVG element 'clipPath' should have 'id' attribute");
								clipPathPending = true;
								context.curGroup = new GraphicGroup();
								applyTransformAttribute(attributes, "transform");
								clipPathTable.put(string_69_, context.curGroup);
								break;
							}
							case 22:
								if (clipPathPending)
									throw new SVGParseException(
											"SVG element 'style' is not allowed as a descendant of 'clipPath' element");
								if (stylesheet != null)
									throw new SVGParseException(
											"Multiple 'style' elements in the SVG file: only the first is retained");
								styleText = new StringBuffer();
								break;
							}
						}
					} catch (Exception exception) {
						if (errHandler != null)
							errHandler.error("SVG element '" + svgelement.name
									+ "' ignored: " + exception.toString());
						context = context.prev;
						skip = 1;
					}
				}
			}
		}
	}

	private SVGMarker retrieveMarker(String string) {
		return string == null ? null : (SVGMarker) markerTable.get(string);
	}

	private void appendMarkers(Path path) throws SVGParseException {
		if (clipPathPending) {
			SVGMarker svgmarker = retrieveMarker(context.markerStart);
			SVGMarker svgmarker_70_ = retrieveMarker(context.markerMid);
			SVGMarker svgmarker_71_ = retrieveMarker(context.markerEnd);
			Enumeration enumeration = path.subs.elements();
			while (enumeration.hasMoreElements()) {
				Subpath subpath = (Subpath) enumeration.nextElement();
				if (!subpath.segs.isEmpty()) {
					Enumeration enumeration_72_ = subpath.segs.elements();
					Subpath.Segment segment = (Subpath.Segment) enumeration_72_
							.nextElement();
					Subpath.Segment segment_73_ = (Subpath.Segment) subpath.segs
							.last();
					double d = subpath.startX - segment_73_.xend;
					double d_74_ = subpath.startY - segment_73_.yend;
					if (!subpath.closed)
						drawMarker(svgmarker, subpath.startX, subpath.startY,
								0.0, 0.0, segment.startSlopeX(), segment
										.startSlopeY());
					else if (subpath.startX == segment_73_.xend
							&& subpath.startY == segment_73_.yend)
						drawMarker(svgmarker, subpath.startX, subpath.startY,
								segment_73_.endSlopeX(), segment_73_
										.endSlopeY(), segment.startSlopeX(),
								segment.startSlopeY());
					else
						drawMarker(svgmarker, subpath.startX, subpath.startY,
								d, d_74_, segment.startSlopeX(), segment
										.startSlopeY());
					Subpath.Segment segment_75_ = segment;
					while (enumeration_72_.hasMoreElements()) {
						Subpath.Segment segment_76_ = (Subpath.Segment) enumeration_72_
								.nextElement();
						if (segment_76_.startSlopeX() != 0.0
								|| segment_76_.startSlopeY() != 0.0) {
							if (segment_76_.beginsArc)
								drawMarker(svgmarker_70_, segment_76_.xbeg,
										segment_76_.ybeg, segment_75_
												.endSlopeX(), segment_75_
												.endSlopeY(), segment_76_
												.startSlopeX(), segment_76_
												.startSlopeY());
							segment_75_ = segment_76_;
						}
					}
					if (!subpath.closed)
						drawMarker(svgmarker_71_, segment_73_.xend,
								segment_73_.yend, segment_73_.endSlopeX(),
								segment_73_.endSlopeY(), 0.0, 0.0);
					else if (subpath.startX == segment_73_.xend
							&& subpath.startY == segment_73_.yend)
						drawMarker(svgmarker_71_, segment_73_.xend,
								segment_73_.yend, segment_73_.endSlopeX(),
								segment_73_.endSlopeY(), segment.startSlopeX(),
								segment.startSlopeY());
					else {
						drawMarker(svgmarker_70_, segment_73_.xend,
								segment_73_.yend, segment_73_.endSlopeX(),
								segment_73_.endSlopeY(), d, d_74_);
						drawMarker(svgmarker_71_, subpath.startX,
								subpath.startY, d, d_74_,
								segment.startSlopeX(), segment.startSlopeY());
					}
				}
			}
		}
	}

	protected void drawMarker(SVGMarker svgmarker, double d, double d_77_,
			double d_78_, double d_79_, double d_80_, double d_81_) {
		if (svgmarker != null
				&& (svgmarker.width != 0.0 && svgmarker.height != 0.0)
				&& (!svgmarker.useStrokeWidth || (context.stroke != null && context.stroke.thickness != 0.0))) {
			GraphicGroup graphicgroup = context.curGroup;
			GraphicGroup graphicgroup_82_ = pushGroup();
			graphicgroup_82_.translate(d, d_77_);
			if (svgmarker.autoRotate) {
				double d_83_ = Math.sqrt(d_78_ * d_78_ + d_79_ * d_79_);
				double d_84_ = Math.sqrt(d_80_ * d_80_ + d_81_ * d_81_);
				double d_85_;
				double d_86_;
				double d_87_;
				if (d_83_ == 0.0) {
					d_85_ = d_80_;
					d_86_ = d_81_;
					d_87_ = d_84_;
				} else if (d_84_ == 0.0) {
					d_85_ = d_78_;
					d_86_ = d_79_;
					d_87_ = d_83_;
				} else {
					d_85_ = d_78_ / d_83_ + d_80_ / d_84_;
					d_86_ = d_79_ / d_83_ + d_81_ / d_84_;
					d_87_ = Math.sqrt(d_85_ * d_85_ + d_86_ * d_86_);
					if (d_87_ == 0.0) {
						d_85_ = d_79_;
						d_86_ = -d_78_;
						d_87_ = d_83_;
					}
				}
				if (d_87_ != 0.0) {
					d_85_ /= d_87_;
					d_86_ /= d_87_;
					graphicgroup_82_.transform(d_85_, d_86_, -d_86_, d_85_,
							0.0, 0.0);
				}
			} else
				graphicgroup_82_.rotate(svgmarker.orientation);
			if (svgmarker.useStrokeWidth) {
				double d_88_ = context.stroke.thickness / 0.6;
				graphicgroup_82_.scale(d_88_, d_88_);
			}
			graphicgroup_82_.append(svgmarker.body);
			context.curGroup = graphicgroup;
		}
	}

	public void openTextChunk(Attributes attributes) throws SVGParseException {
		String string = getFirstTokenFromAttr(attributes, "x");
		if (string != null)
			nextGlyphPosX = context.extractLength(string, context.viewportX);
		startTextSpan(attributes);
		context.curGroup.append(textGroup = new GraphicGroup());
		textGroup.scale(1.0, -1.0);
		textOriginX = textOffsetX = nextGlyphPosX;
		textOriginY = textOffsetY = nextGlyphPosY;
		currentTextAnchor = context.textAnchor;
		drawNextBlank = context.preserveSpace;
	}

	public void closeTextChunk() {
		switch (currentTextAnchor) {
		case 0:
			break;
		case 1:
			textGroup.translate((textOriginX - textOffsetX) / 2.0,
					(textOriginY - textOffsetY) / 2.0);
			nextGlyphPosX = textOffsetX = textOffsetX
					+ (textOriginX - textOffsetX) / 2.0;
			nextGlyphPosY = textOffsetY = textOffsetY
					+ (textOriginY - textOffsetY) / 2.0;
			break;
		case 2:
			textGroup.translate(textOriginX - textOffsetX, textOriginY
					- textOffsetY);
			nextGlyphPosX = textOffsetX = textOriginX;
			nextGlyphPosY = textOffsetY = textOriginY;
			break;
		}
		textGroup = null;
	}

	public void startTextSpan(Attributes attributes) throws SVGParseException {
		String string = getFirstTokenFromAttr(attributes, "y");
		if (string != null)
			nextGlyphPosY = -context.extractLength(string, context.viewportY);
		String string_89_ = getFirstTokenFromAttr(attributes, "dx");
		if (string_89_ != null)
			nextGlyphPosX += context.extractLength(string_89_,
					context.viewportX);
		String string_90_ = getFirstTokenFromAttr(attributes, "dy");
		if (string_90_ != null)
			nextGlyphPosY -= context.extractLength(string_90_,
					context.viewportY);
		String string_91_ = getFirstTokenFromAttr(attributes, "rotate");
		if (string_91_ != null)
			context.textRotate = new SVGAttrValue.Angle(string_91_).angle;
	}

	String getFirstTokenFromAttr(Attributes attributes, String string)
			throws SVGParseException {
		String string_92_ = getAttrValue(attributes, string);
		if (string_92_ == null)
			return null;
		StringTokenizer stringtokenizer = new StringTokenizer(string_92_,
				" \n\r\t,");
		return (stringtokenizer.hasMoreTokens() ? stringtokenizer.nextToken()
				: null);
	}

	public static void main(String[] strings) throws Exception {
		String string = null;
		if (strings.length == 1) {
			string = strings[0] + ".ps";
			System.out.println("Dumping graphic tree to " + string);
		} else if (strings.length == 2)
			string = strings[1];
		else {
			System.out
					.println("Usage: com.renderx.svg.SVGParserHandler <input file name> [<output file name>]");
			System.exit(0);
		}
		XMLReader xmlreader = XMLReaderFactory.createXMLReader();
		DefaultErrorHandler defaulterrorhandler = new DefaultErrorHandler();
		URLCache urlcache = new URLCache();
		FontCatalog fontcatalog = new FontCatalog(urlcache, defaulterrorhandler);
		ImageFactory imagefactory = new ImageFactory(fontcatalog, urlcache,
				defaulterrorhandler);
		SVGParserHandler svgparserhandler = new SVGParserHandler(imagefactory,
				fontcatalog, defaulterrorhandler, 0.0, 0.0);
		xmlreader.setContentHandler(svgparserhandler);
		InputSource inputsource = new InputSource(new URLSpec(strings[0]));
		xmlreader.parse(inputsource);
		svgparserhandler.tree.dump(
				new PrintStream(new FileOutputStream(string)), "");
	}
}