/*
 * SVGParserContext - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg;

import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import com.renderx.fonts.FontCatalog;
import com.renderx.fonts.FontRecord;
import com.renderx.graphics.vector.GraphicGroup;
import com.renderx.graphics.vector.OpaqueColor;
import com.renderx.graphics.vector.PaintSpec;
import com.renderx.graphics.vector.StrokeSpec;
import com.renderx.svg.css.Selector;
import com.renderx.svg.css.Style;
import com.renderx.svg.css.StyleSheet;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.util.URLSpec;

import org.xml.sax.Attributes;

public class SVGParserContext {
	private static final int P_BASELINE_SHIFT = 1;

	private static final int P_CLIP_RULE = 2;

	private static final int P_CLIP_PATH = 3;

	private static final int P_COLOR = 4;

	private static final int P_FILL = 5;

	private static final int P_FILL_RULE = 6;

	private static final int P_FONT = 7;

	private static final int P_FONT_FAMILY = 8;

	private static final int P_FONT_SIZE = 9;

	private static final int P_FONT_SIZE_ADJUST = 10;

	private static final int P_FONT_STRETCH = 11;

	private static final int P_FONT_STYLE = 12;

	private static final int P_FONT_VARIANT = 13;

	private static final int P_FONT_WEIGHT = 14;

	private static final int P_LETTER_SPACING = 15;

	private static final int P_MARKER = 16;

	private static final int P_MARKER_END = 17;

	private static final int P_MARKER_MID = 18;

	private static final int P_MARKER_START = 19;

	private static final int P_STROKE = 20;

	private static final int P_STROKE_WIDTH = 21;

	private static final int P_STROKE_LINECAP = 22;

	private static final int P_STROKE_LINEJOIN = 23;

	private static final int P_STROKE_MITERLIMIT = 24;

	private static final int P_STROKE_DASHARRAY = 25;

	private static final int P_STROKE_DASHOFFSET = 26;

	private static final int P_TEXT_ANCHOR = 27;

	private static final int P_TEXT_DECORATION = 28;

	private static final int P_VISIBILITY = 29;

	private static final int P_WORD_SPACING = 30;

	private static final int P_XML_BASE = 31;

	private static final int P_XML_SPACE = 32;

	private static final int P_FILL_OPACITY = 33;

	private static final int P_STROKE_OPACITY = 34;

	private static final int P_OPACITY = 35;

	private static final int P_XML_LANG = 36;

	private static final Hashtable proptab = new Hashtable();

	private static final List primaryProperties;

	private static final Hashtable primaryPropIndex;

	public GraphicGroup curGroup = null;

	public SVGElement element = null;

	public OpaqueColor currentColor = null;

	public double fillOpacity = 1.0;

	public double strokeOpacity = 1.0;

	public double opacity = 1.0;

	public PaintSpec fill = null;

	public int fillRule = 0;

	public String clipPath = null;

	public int clipRule = 0;

	public StrokeSpec stroke = null;

	public String markerStart = null;

	public String markerMid = null;

	public String markerEnd = null;

	public double baselineShift = 0.0;

	public String fontFamily = null;

	public int fontWeight = -2147483648;

	public String fontStyle = null;

	public String fontVariant = null;

	public String language = null;

	public double fontSize = 0.0;

	public double fontStretch = 0.0;

	public double letterSpacing = 0.0;

	public double wordSpacing = 0.0;

	public double textRotate = 0.0;

	public boolean preserveSpace = false;

	public boolean underline = false;

	public boolean overline = false;

	public boolean linethrough = false;

	public FontRecord[] fonts = null;

	public URLSpec baseURL = null;

	public Hashtable classes = new Hashtable();

	public String nodeID = null;

	public boolean hidden = false;

	public final FontCatalog fontCatalog;

	protected final Hashtable fontCache;

	public static final String DEFAULT_FONT_FAMILY = "Helvetica";

	public static final double DEFAULT_FONT_SIZE = 10.0;

	public static final int TEXT_ANCHOR_START = 0;

	public static final int TEXT_ANCHOR_MIDDLE = 1;

	public static final int TEXT_ANCHOR_END = 2;

	public int textAnchor = -2147483648;

	public double viewportX = 0.0;

	public double viewportY = 0.0;

	public double viewportXY = 0.0;

	public SVGParserContext prev = null;

	public SVGParserContext next = null;

	public SVGParserContext(SVGParserContext svgparsercontext_0_,
			URLSpec urlspec, FontCatalog fontcatalog, Hashtable hashtable) {
		prev = svgparsercontext_0_;
		baseURL = urlspec;
		fontCatalog = fontcatalog;
		fontCache = hashtable;
		if (svgparsercontext_0_ != null)
			svgparsercontext_0_.next = this;
	}

	public void initialize(SVGElement svgelement, ErrorHandler errorhandler)
			throws SVGParseException {
		element = svgelement;
		fillOpacity = 1.0;
		strokeOpacity = 1.0;
		opacity = 1.0;
		if (prev == null) {
			fill = currentColor = new OpaqueColor.Grayscale(0.0);
			fillRule = 0;
			clipPath = null;
			clipRule = 0;
			stroke = new StrokeSpec(null, new SVGAttrValue.Length("1").len);
			stroke.lineCap = 0;
			stroke.lineJoin = 0;
			stroke.miterLimit = 4.0;
			stroke.dashArray = null;
			stroke.dashPhase = 0.0;
			markerStart = null;
			markerMid = null;
			markerEnd = null;
			fontSize = 10.0;
			fontFamily = "Helvetica";
			fontWeight = 400;
			fontStyle = "normal";
			fontVariant = "normal";
			language = "none";
			baselineShift = 0.0;
			fontStretch = 1.0;
			letterSpacing = 0.0;
			wordSpacing = 0.0;
			preserveSpace = false;
			underline = false;
			overline = false;
			linethrough = false;
			textAnchor = 0;
			textRotate = 0.0;
			hidden = false;
			selectFont(errorhandler);
		} else {
			nodeID = null;
			classes.clear();
			curGroup = prev.curGroup;
			fill = prev.fill;
			fillRule = prev.fillRule;
			clipPath = null;
			clipRule = prev.clipRule;
			stroke = prev.stroke;
			currentColor = prev.currentColor;
			markerStart = prev.markerStart;
			markerMid = prev.markerMid;
			markerEnd = prev.markerEnd;
			fonts = prev.fonts;
			fontSize = prev.fontSize;
			fontFamily = prev.fontFamily;
			fontWeight = prev.fontWeight;
			fontStyle = prev.fontStyle;
			fontVariant = prev.fontVariant;
			language = prev.language;
			baselineShift = prev.baselineShift;
			fontStretch = prev.fontStretch;
			letterSpacing = prev.letterSpacing;
			wordSpacing = prev.wordSpacing;
			baseURL = prev.baseURL;
			preserveSpace = prev.preserveSpace;
			textAnchor = prev.textAnchor;
			textRotate = prev.textRotate;
			hidden = prev.hidden;
			viewportX = prev.viewportX;
			viewportY = prev.viewportY;
			viewportXY = prev.viewportXY;
		}
	}

	public static SVGParserContext createContext(
			SVGParserContext svgparsercontext, SVGElement svgelement,
			URLSpec urlspec, FontCatalog fontcatalog, Hashtable hashtable,
			ErrorHandler errorhandler) throws SVGParseException {
		SVGParserContext svgparsercontext_1_ = (svgparsercontext == null
				|| svgparsercontext.next == null ? new SVGParserContext(
				svgparsercontext, urlspec, fontcatalog, hashtable)
				: svgparsercontext.next);
		svgparsercontext_1_.initialize(svgelement, errorhandler);
		return svgparsercontext_1_;
	}

	public void setProperties(Attributes attributes, StyleSheet stylesheet,
			ErrorHandler errorhandler) throws SVGParseException {
		nodeID = attributes.getValue("", "id");
		String string = attributes.getValue("", "class");
		if (string != null) {
			StringTokenizer stringtokenizer = new StringTokenizer(string);
			while (stringtokenizer.hasMoreTokens())
				classes.put(stringtokenizer.nextToken(), "dummyobject");
		}
		Style style = null;
		if (stylesheet != null) {
			style = new Style();
			for (int i = 0; i < stylesheet.size(); i++) {
				if (matches(this, stylesheet.getSelector(i).nodelist))
					style.merge(stylesheet.getStyle(i));
			}
		}
		String string_2_ = attributes.getValue("", "style");
		if (string_2_ != null) {
			Style style_3_ = new Style(string_2_, errorhandler);
			if (style == null)
				style = style_3_;
			else {
				Enumeration enumeration = style_3_.properties();
				while (enumeration.hasMoreElements()) {
					String string_4_ = (String) enumeration.nextElement();
					style.put(string_4_, style_3_.get(string_4_));
				}
			}
		}
		Enumeration enumeration = primaryProperties.elements();
		while (enumeration.hasMoreElements()) {
			String string_5_ = (String) enumeration.nextElement();
			String string_6_ = attributes.getValue("", string_5_);
			if (string_6_ == null && style != null)
				string_6_ = style.getCleanValue(string_5_);
			if (string_6_ != null)
				processAttribute(string_5_, string_6_, errorhandler);
		}
		if (style != null) {
			enumeration = style.properties();
			while (enumeration.hasMoreElements()) {
				String string_7_ = (String) enumeration.nextElement();
				if (primaryPropIndex.get(string_7_) == null)
					processAttribute(string_7_, style.getValue(string_7_),
							errorhandler);
			}
		}
		int i = attributes.getLength();
		while (i-- > 0) {
			String string_8_ = attributes.getURI(i);
			String string_9_ = attributes.getLocalName(i);
			String string_10_ = attributes.getQName(i);
			if (string_10_ == null || !string_10_.startsWith("xmlns:")) {
				if ("http://www.w3.org/XML/1998/namespace".equals(string_8_)) {
					string_9_ = "xml:" + string_9_;
					processAttribute(string_9_, attributes.getValue(i),
							errorhandler);
				} else if ("".equals(string_8_) && !"style".equals(string_9_)
						&& primaryPropIndex.get(string_9_) == null)
					processAttribute(string_9_, attributes.getValue(i),
							errorhandler);
			}
		}
	}

	private static boolean matches(SVGParserContext svgparsercontext, List list) {
		return matches(svgparsercontext, list, true);
	}

	private static boolean matches(SVGParserContext svgparsercontext,
			List list, boolean bool) {
		if (list.isEmpty())
			return true;
		if (svgparsercontext == null)
			return false;
		Selector.Node node = (Selector.Node) list.car();
		if ((node.elementName != null && !node.elementName
				.equals(svgparsercontext.element.name))
				|| (node.elementID != null && !node.elementID
						.equals(svgparsercontext.nodeID))
				|| (node.className != null && svgparsercontext.classes
						.get(node.className) == null)) {
			if (bool)
				return false;
			return matches(svgparsercontext.prev, list, false);
		}
		return matches(svgparsercontext.prev, list.cdr(), false);
	}

	private void processAttribute(String string, String string_11_,
			ErrorHandler errorhandler) throws SVGParseException {
		Integer integer = (Integer) proptab.get(string);
		if (integer != null && !string_11_.equals("inherit")) {
			try {
				switch (integer.intValue()) {
				case 1: {
					String string_12_ = string_11_.trim();
					double d = prev == null ? fontSize : prev.fontSize;
					if (string_12_.equals("baseline"))
						string_12_ = "0";
					else if (string_12_.equals("sub"))
						string_12_ = "-25%";
					else if (string_12_.equals("super"))
						string_12_ = "50%";
					baselineShift = extractLength(string_12_, d);
					break;
				}
				case 3:
					clipPath = getIdRef(string_11_);
					break;
				case 2: {
					String string_13_ = string_11_.trim();
					if (string_13_.equals("nonzero"))
						clipRule = 0;
					else if (string_13_.equals("evenodd"))
						clipRule = 1;
					else
						throw new SVGParseException(
								"Invalid value for 'clip-rule': \""
										+ string_13_ + "\"");
					break;
				}
				case 4: {
					OpaqueColor opaquecolor = currentColor;
					currentColor = null;
					currentColor = new SVGAttrValue.Color(string_11_)
							.getColorSpec(opaquecolor);
					break;
				}
				case 5:
					fill = null;
					fill = new SVGAttrValue.Paint(string_11_)
							.getPaintSpec(currentColor);
					break;
				case 6: {
					String string_14_ = string_11_.trim();
					if (string_14_.equals("nonzero"))
						fillRule = 0;
					else if (string_14_.equals("evenodd"))
						fillRule = 1;
					else
						throw new SVGParseException(
								"Invalid value for 'fill-rule': \""
										+ string_14_ + "\"");
					break;
				}
				case 33: {
					double d = 1.0;
					String string_15_ = string_11_.trim();
					try {
						d = Double.parseDouble(string_15_);
					} catch (NumberFormatException numberformatexception) {
						/* empty */
					}
					if (d > 1.0)
						d = 1.0;
					if (d < 0.0)
						d = 0.0;
					fillOpacity = d;
					break;
				}
				case 7: {
					String string_16_ = null;
					Object object = null;
					String string_17_ = "normal";
					String string_18_ = "normal";
					String string_19_ = "normal";
					StringTokenizer stringtokenizer = new StringTokenizer(
							string_11_);
					while (stringtokenizer.hasMoreTokens()) {
						String string_20_ = stringtokenizer.nextToken();
						if (!"normal".equals(string_20_)) {
							if ("italic".equals(string_20_)
									|| "oblique".equals(string_20_)
									|| "backslant".equals(string_20_))
								string_17_ = string_20_;
							else if ("bold".equals(string_20_)
									|| "bolder".equals(string_20_)
									|| "light".equals(string_20_)
									|| "lighter".equals(string_20_)
									|| "100".equals(string_20_)
									|| "200".equals(string_20_)
									|| "300".equals(string_20_)
									|| "400".equals(string_20_)
									|| "500".equals(string_20_)
									|| "600".equals(string_20_)
									|| "700".equals(string_20_)
									|| "800".equals(string_20_)
									|| "900".equals(string_20_))
								string_18_ = string_20_;
							else if ("small-caps".equals(string_20_))
								string_19_ = string_20_;
							else {
								int i = string_20_.indexOf('/');
								if (i != -1)
									string_20_ = string_20_.substring(0, i);
								string_16_ = string_20_;
								break;
							}
						}
					}
					if (stringtokenizer.hasMoreTokens()) {
						String string_21_ = stringtokenizer.nextToken();
						while (stringtokenizer.hasMoreTokens())
							string_21_ += " " + stringtokenizer.nextToken();
						processAttribute("font-family", string_21_,
								errorhandler);
						processAttribute("font-size", string_16_, errorhandler);
						processAttribute("font-style", string_17_, errorhandler);
						processAttribute("font-weight", string_18_,
								errorhandler);
						processAttribute("font-variant", string_19_,
								errorhandler);
						break;
					}
					break;
				}
				case 8: {
					String string_22_ = string_11_.trim();
					if (!fontFamily.equals(string_22_)) {
						fontFamily = string_22_;
						selectFont(errorhandler);
					}
					break;
				}
				case 9: {
					double d = extractLength(string_11_, fontSize);
					if (fontSize != d) {
						fontSize = d;
						selectFont(errorhandler);
					}
					break;
				}
				case 10:
					break;
				case 11: {
					String string_23_ = new SVGAttrValue.Enum(string_11_).word;
					if ("wider".equals(string_23_))
						fontStretch *= 1.2;
					else if ("narrower".equals(string_23_))
						fontStretch /= 1.2;
					else if ("ultra-condensed".equals(string_23_))
						fontStretch /= 2.074;
					else if ("extra-condensed".equals(string_23_))
						fontStretch /= 1.728;
					else if ("condensed".equals(string_23_))
						fontStretch /= 1.44;
					else if ("semi-condensed".equals(string_23_))
						fontStretch /= 1.2;
					else if ("normal".equals(string_23_))
						fontStretch = 1.0;
					else if ("semi-expanded".equals(string_23_))
						fontStretch *= 1.2;
					else if ("expanded".equals(string_23_))
						fontStretch *= 1.44;
					else if ("extra-expanded".equals(string_23_))
						fontStretch *= 1.728;
					else if ("ultra-expanded".equals(string_23_))
						fontStretch *= 2.074;
					else
						fontStretch = new SVGAttrValue.Numeric(string_11_).num;
					break;
				}
				case 12: {
					String string_24_ = new SVGAttrValue.Enum(string_11_).word;
					if (!fontStyle.equals(string_24_)) {
						fontStyle = string_24_;
						selectFont(errorhandler);
					}
					break;
				}
				case 13: {
					String string_25_ = new SVGAttrValue.Enum(string_11_).word;
					if (!fontVariant.equals(string_25_)) {
						fontVariant = string_25_;
						selectFont(errorhandler);
					}
					break;
				}
				case 14: {
					String string_26_ = string_11_.trim();
					int i = fontWeight;
					if (string_26_.equals("bolder"))
						i += 200;
					else if (string_26_.equals("lighter"))
						i -= 200;
					else if (string_26_.equals("normal"))
						i = 400;
					else if (string_26_.equals("bold"))
						i = 700;
					else {
						try {
							i = Integer.parseInt(string_26_);
						} catch (NumberFormatException numberformatexception) {
							/* empty */
						}
					}
					if (i > 900)
						i = 900;
					else if (i < 100)
						i = 100;
					if (fontWeight != i) {
						fontWeight = i;
						selectFont(errorhandler);
					}
					break;
				}
				case 15:
					letterSpacing = extractLength(string_11_, fontSize);
					break;
				case 35: {
					double d = 1.0;
					String string_27_ = string_11_.trim();
					try {
						d = Double.parseDouble(string_27_);
					} catch (NumberFormatException numberformatexception) {
						/* empty */
					}
					if (d > 1.0)
						d = 1.0;
					if (d < 0.0)
						d = 0.0;
					opacity = d;
					break;
				}
				case 20:
					stroke = stroke.cc();
					stroke.paint = null;
					stroke.paint = new SVGAttrValue.Paint(string_11_)
							.getPaintSpec(currentColor);
					break;
				case 21:
					stroke = stroke.cc();
					stroke.thickness = extractLength(string_11_, viewportXY);
					break;
				case 22: {
					stroke = stroke.cc();
					String string_28_ = new SVGAttrValue.Enum(string_11_).word;
					if (string_28_.equals("butt"))
						stroke.lineCap = 0;
					else if (string_28_.equals("round"))
						stroke.lineCap = 1;
					else if (string_28_.equals("square"))
						stroke.lineCap = 2;
					break;
				}
				case 23: {
					stroke = stroke.cc();
					String string_29_ = new SVGAttrValue.Enum(string_11_).word;
					if (string_29_.equals("miter"))
						stroke.lineJoin = 0;
					else if (string_29_.equals("round"))
						stroke.lineJoin = 1;
					else if (string_29_.equals("bevel"))
						stroke.lineJoin = 2;
					break;
				}
				case 24: {
					stroke = stroke.cc();
					double d = new SVGAttrValue.Numeric(string_11_).num;
					if (d < 1.0)
						throw new SVGParseException(
								"Invalid value for 'stroke-miterlimit': \"" + d
										+ "\"");
					stroke.miterLimit = d;
					break;
				}
				case 25: {
					Vector vector = new Vector();
					if (!"none".equals(string_11_.trim())) {
						StringTokenizer stringtokenizer = new StringTokenizer(
								string_11_, " \t\r\n,");
						while (stringtokenizer.hasMoreTokens())
							vector.addElement(new Double(extractLength(
									stringtokenizer.nextToken(), viewportXY)));
					}
					stroke = stroke.cc();
					if (vector.size() > 0) {
						stroke.dashArray = new double[vector.size()];
						for (int i = 0; i < vector.size(); i++)
							stroke.dashArray[i] = ((Double) vector.elementAt(i))
									.doubleValue();
					} else
						stroke.dashArray = null;
					break;
				}
				case 26:
					stroke = stroke.cc();
					stroke.dashPhase = extractLength(string_11_, viewportXY);
					break;
				case 34: {
					double d = 1.0;
					String string_30_ = string_11_.trim();
					try {
						d = Double.parseDouble(string_30_);
					} catch (NumberFormatException numberformatexception) {
						/* empty */
					}
					if (d > 1.0)
						d = 1.0;
					if (d < 0.0)
						d = 0.0;
					strokeOpacity = d;
					break;
				}
				case 16:
					markerStart = getIdRef(string_11_);
					markerMid = markerStart;
					markerEnd = markerStart;
					break;
				case 19:
					markerStart = getIdRef(string_11_);
					break;
				case 18:
					markerMid = getIdRef(string_11_);
					break;
				case 17:
					markerEnd = getIdRef(string_11_);
					break;
				case 27: {
					String string_31_ = new SVGAttrValue.Enum(string_11_).word;
					if (string_31_.equals("start"))
						textAnchor = 0;
					else if (string_31_.equals("middle"))
						textAnchor = 1;
					else if (string_31_.equals("end"))
						textAnchor = 2;
					break;
				}
				case 28: {
					StringTokenizer stringtokenizer = new StringTokenizer(
							string_11_, " \t\r\n,");
					while (stringtokenizer.hasMoreTokens()) {
						String string_32_ = stringtokenizer.nextToken();
						if (string_32_.equals("underline"))
							underline = true;
						else if (string_32_.equals("overline"))
							overline = true;
						else if (string_32_.equals("line-through"))
							linethrough = true;
					}
					break;
				}
				case 29:
					hidden = ("hidden".equals(string_11_) || "collapse"
							.equals(string_11_));
					break;
				case 30:
					wordSpacing = extractLength(string_11_, fontSize);
					break;
				case 31:
					try {
						baseURL = new URLSpec(baseURL, string_11_);
					} catch (Exception exception) {
						if (errorhandler != null)
							errorhandler
									.error("xml:base value ignored: cannot convert '"
											+ string_11_
											+ "' to a URL: "
											+ exception.toString());
					}
					preserveSpace = "preserve".equals(string_11_.trim());
					break;
				case 32:
					preserveSpace = "preserve".equals(string_11_.trim());
					break;
				case 36:
					language = string_11_.trim();
					break;
				}
			} catch (Exception exception) {
				if (errorhandler != null)
					errorhandler.error("SVG property " + string + "=\""
							+ string_11_ + "\" ignored: "
							+ exception.toString());
			}
		}
	}

	public double extractLength(String string, double d)
			throws SVGParseException {
		SVGAttrValue.Length length = new SVGAttrValue.Length(string);
		switch (length.base) {
		case 0:
			return length.len;
		case 1:
			return length.len * fontSize;
		case 2: {
			double d_33_ = (fonts == null || fonts.length == 0 ? 0.5
					: (double) fonts[0].getMetric().xHeight / 1000.0);
			return length.len * fontSize * d_33_;
		}
		case 3:
			return length.len * d;
		default:
			throw new SVGParseException(
					"Internal error: incorrect base in a length value");
		}
	}

	public void setViewport(double d, double d_34_) {
		viewportX = d;
		viewportY = d_34_;
		viewportXY = Math.sqrt((d * d + d_34_ * d_34_) / 2.0);
	}

	public void selectFont(ErrorHandler errorhandler) throws SVGParseException {
		String string = (fontFamily + ":" + fontWeight + ":" + fontStyle + ":"
				+ fontVariant + ":" + language);
		fonts = (FontRecord[]) fontCache.get(string);
		if (fonts == null) {
			List list = fontCatalog.findAll(fontFamily, fontWeight, fontStyle,
					fontVariant, language, errorhandler);
			if (list == null || list.length() == 0)
				fonts = null;
			else {
				fonts = new FontRecord[list.length()];
				Enumeration enumeration = list.elements();
				for (int i = 0; i < fonts.length; i++)
					fonts[i] = (FontRecord) enumeration.nextElement();
				fontCache.put(string, fonts);
			}
		}
	}

	private static String getIdRef(String string) {
		string = string.trim();
		if ("none".equals(string))
			return null;
		if (string.startsWith("url(") && string.endsWith(")"))
			string = string.substring(4, string.length() - 1).trim();
		else if (string.startsWith("url (") && string.endsWith(")"))
			string = string.substring(5, string.length() - 1).trim();
		if (string.length() == 0)
			return null;
		if (!string.startsWith("#"))
			return null;
		string = string.substring(1).trim();
		if (string.startsWith("xpointer(id(") && string.endsWith("))"))
			string = string.substring(12, string.length() - 2).trim();
		return string;
	}

	static {
		proptab.put("baseline-shift", new Integer(1));
		proptab.put("clip-path", new Integer(3));
		proptab.put("clip-rule", new Integer(2));
		proptab.put("color", new Integer(4));
		proptab.put("fill", new Integer(5));
		proptab.put("fill-rule", new Integer(6));
		proptab.put("fill-opacity", new Integer(33));
		proptab.put("font", new Integer(7));
		proptab.put("font-family", new Integer(8));
		proptab.put("font-size", new Integer(9));
		proptab.put("font-size-adjust", new Integer(10));
		proptab.put("font-stretch", new Integer(11));
		proptab.put("font-style", new Integer(12));
		proptab.put("font-variant", new Integer(13));
		proptab.put("font-weight", new Integer(14));
		proptab.put("letter-spacing", new Integer(15));
		proptab.put("marker", new Integer(16));
		proptab.put("marker-end", new Integer(17));
		proptab.put("marker-mid", new Integer(18));
		proptab.put("marker-start", new Integer(19));
		proptab.put("opacity", new Integer(35));
		proptab.put("stroke", new Integer(20));
		proptab.put("stroke-width", new Integer(21));
		proptab.put("stroke-linecap", new Integer(22));
		proptab.put("stroke-linejoin", new Integer(23));
		proptab.put("stroke-miterlimit", new Integer(24));
		proptab.put("stroke-dasharray", new Integer(25));
		proptab.put("stroke-dashoffset", new Integer(26));
		proptab.put("stroke-opacity", new Integer(34));
		proptab.put("text-anchor", new Integer(27));
		proptab.put("text-decoration", new Integer(28));
		proptab.put("visibility", new Integer(29));
		proptab.put("word-spacing", new Integer(30));
		proptab.put("xml:base", new Integer(31));
		proptab.put("xml:space", new Integer(32));
		proptab.put("xml:lang", new Integer(36));
		primaryProperties = new List();
		primaryPropIndex = new Hashtable();
		primaryProperties.append("font");
		primaryProperties.append("font-family");
		primaryProperties.append("font-weight");
		primaryProperties.append("font-style");
		primaryProperties.append("font-variant");
		primaryProperties.append("font-size");
		primaryProperties.append("font-size-adjust");
		primaryProperties.append("color");
		primaryProperties.append("xml:lang");
		primaryPropIndex.put("font", "");
		primaryPropIndex.put("font-family", "");
		primaryPropIndex.put("font-weight", "");
		primaryPropIndex.put("font-style", "");
		primaryPropIndex.put("font-variant", "");
		primaryPropIndex.put("font-size", "");
		primaryPropIndex.put("font-size-adjust", "");
		primaryPropIndex.put("color", "");
		primaryPropIndex.put("xml:lang", "");
	}
}