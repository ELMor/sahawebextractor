/*
 * Token - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg.css;

public interface Token {
	public static final int UNKNOWN = -1;

	public static final int EOF = 0;

	public static final int LEFT_CURLY_BRACE = 1;

	public static final int RIGHT_CURLY_BRACE = 2;

	public static final int EQUAL = 3;

	public static final int PLUS = 4;

	public static final int MINUS = 5;

	public static final int COMMA = 6;

	public static final int DOT = 7;

	public static final int SEMI_COLON = 8;

	public static final int DIVIDE = 10;

	public static final int LEFT_PARENTHESIS = 14;

	public static final int RIGHT_PARENTHESIS = 15;

	public static final int COLON = 16;

	public static final int SPACE = 17;

	public static final int COMMENT = 18;

	public static final int STRING = 19;

	public static final int IDENT = 20;

	public static final int IMPORTANT_SYMBOL = 23;

	public static final int HASH = 27;

	public static final int IMPORT_SYMBOL = 28;

	public static final int NUMBER_UNIT = 51;

	public static final int FUNCTION = 52;

	public static final int END_SELECTOR = 53;

	public static final int SOLITARY_PSEUDO_ELEMENT = 54;

	public static final int SOLITARY_PSEUDO_CLASS = 55;

	public static final int SOLITARY_CLASS = 56;

	public static final int SOLITARY_ID = 57;

	public static final int ELEMENT_NAME = 58;

	public static final int SIMPLE_SELECTOR = 59;

	public static final int SELECTOR = 60;

	public static final int ID = 61;

	public static final int CLASS = 62;
}