/*
 * CSSParseException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg.css;

public class CSSParseException extends Exception {
	public CSSParseException() {
		super("");
	}

	public CSSParseException(String string) {
		super(string);
	}
}