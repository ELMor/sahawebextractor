/*
 * Lexer - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg;

import java.io.IOException;
import java.io.InputStreamReader;

public class Lexer {
	public static final String blanks = " \t\r\n\014";

	public static final String token_delimiters = " \t\r\n\014'\"+()[]{},/:;";

	public static final String single_chars = "()[]{},/:;";

	public static final String quotes = "'\"";

	public static final String digits = "0123456789";

	public static final String hexdigits = "0123456789ABCDEFabcdef";

	private char[] data = null;

	private int length = 0;

	private int cursor = 0;

	public Lexer() {
		/* empty */
	}

	public Lexer(String string) {
		this();
		setSource(string);
	}

	public Lexer(char[] cs) {
		this();
		setSource(cs);
	}

	public void setSource(String string) {
		setSource(string.toCharArray());
	}

	public void setSource(char[] cs) {
		setSource(cs, 0, cs.length);
	}

	public void setSource(char[] cs, int i, int i_0_) {
		data = cs;
		cursor = i;
		length = i + i_0_;
	}

	private void skip_whitespace() {
		for (/**/; cursor < length; cursor++) {
			if (" \t\r\n\014".indexOf(data[cursor]) == -1)
				break;
		}
	}

	public int nextToken(Token token) throws SVGParseException {
		token.reset();
		skip_whitespace();
		if (cursor >= length)
			return token.type = 1;
		if ("()[]{},/:;".indexOf(data[cursor]) != -1) {
			switch (data[cursor++]) {
			case '(':
				return token.type = 2;
			case ')':
				return token.type = 3;
			case '[':
				return token.type = 4;
			case ']':
				return token.type = 5;
			case '{':
				return token.type = 6;
			case '}':
				return token.type = 7;
			case ',':
				return token.type = 8;
			case '/':
				return token.type = 9;
			case ':':
				return token.type = 10;
			case ';':
				return token.type = 11;
			default:
				throw new SVGParseException(
						"Internal error: unrecognized separator character");
			}
		}
		if ("'\"".indexOf(data[cursor]) != -1) {
			char c = data[cursor];
			int i = ++cursor;
			do {
				if (cursor == length)
					throw new SVGParseException("Unterminated quoted string");
			} while (data[cursor++] != c);
			token.stringValue = new String(data, i, cursor - i - 1);
			return token.type = 14;
		}
		int i = cursor++;
		for (/**/; cursor < length; cursor++) {
			if (" \t\r\n\014'\"+()[]{},/:;".indexOf(data[cursor]) != -1)
				break;
		}
		if (data[i] == '#') {
			if (cursor - i == 7
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 1]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 2]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 3]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 4]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 5]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 6]) != -1) {
				int i_1_ = Integer.parseInt(new String(data, i + 1, 2), 16);
				int i_2_ = Integer.parseInt(new String(data, i + 3, 2), 16);
				int i_3_ = Integer.parseInt(new String(data, i + 5, 2), 16);
				token.numValue = (double) ((i_1_ << 16) + (i_2_ << 8) + i_3_);
				return token.type = 12;
			}
			if (cursor - i == 4
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 1]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 2]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 3]) != -1) {
				int i_4_ = Integer.parseInt(new String(data, i + 1, 1), 16) * 17;
				int i_5_ = Integer.parseInt(new String(data, i + 2, 1), 16) * 17;
				int i_6_ = Integer.parseInt(new String(data, i + 3, 1), 16) * 17;
				token.numValue = (double) ((i_4_ << 16) + (i_5_ << 8) + i_6_);
				return token.type = 12;
			}
		}
		int i_7_ = i;
		int i_8_ = 1;
		if (data[i_7_] == '-') {
			i_7_++;
			i_8_ = -1;
		} else if (data[i_7_] == '+')
			i_7_++;
		if (i_7_ < cursor
				&& "0123456789".indexOf(data[i_7_]) != -1
				|| (i_7_ < cursor - 1 && data[i_7_] == '.' && "0123456789"
						.indexOf(data[i_7_ + 1]) != -1)) {
			boolean bool = false;
			double d = 1.0;
			token.numValue = 0.0;
			for (/**/; i_7_ < cursor; i_7_++) {
				if ("0123456789".indexOf(data[i_7_]) != -1) {
					int i_9_ = data[i_7_] - 48;
					if (bool) {
						d /= 10.0;
						token.numValue += d * (double) i_9_;
					} else {
						token.numValue *= 10.0;
						token.numValue += (double) i_9_;
					}
				} else {
					if (data[i_7_] != '.' || bool)
						break;
					bool = true;
				}
			}
			token.numValue *= (double) i_8_;
			if (i_7_ < cursor
					&& (data[i_7_] == 'e' || data[i_7_] == 'E')
					&& ((i_7_ < cursor - 1 && "0123456789"
							.indexOf(data[i_7_ + 1]) != -1) || (i_7_ < cursor - 2
							&& (data[i_7_ + 1] == '+' || data[i_7_ + 1] == '-') && "0123456789"
							.indexOf(data[i_7_ + 2]) != -1))) {
				i_7_++;
				int i_10_ = 1;
				if (data[i_7_] == '-') {
					i_7_++;
					i_10_ = -1;
				} else if (data[i_7_] == '+')
					i_7_++;
				int i_11_ = 0;
				for (/**/; i_7_ < cursor
						&& "0123456789".indexOf(data[i_7_]) != -1; i_7_++) {
					i_11_ *= 10.0;
					i_11_ += data[i_7_] - 48;
				}
				i_11_ *= i_10_;
				token.numValue *= Math.pow(10.0, (double) i_11_);
			}
			if (i_7_ != cursor)
				token.stringValue = new String(data, i_7_, cursor - i_7_);
			return token.type = 15;
		}
		token.stringValue = new String(data, i, cursor - i);
		return token.type = 13;
	}

	public static Token getSingleToken(String string) throws SVGParseException {
		Lexer lexer = new Lexer(string);
		Token token = new Token();
		if (lexer.nextToken(token) == 1)
			throw new SVGParseException("No meaningful data");
		Token token_12_ = new Token();
		if (lexer.nextToken(token_12_) != 1)
			throw new SVGParseException("Extra tokens in \"" + string.trim()
					+ "\"");
		return token;
	}

	public static void main(String[] strings) {
		char[] cs = new char[8192];
		int i = 0;
		Lexer lexer = new Lexer();
		Token token = new Token();
		for (;;) {
			try {
				InputStreamReader inputstreamreader = new InputStreamReader(
						System.in);
				i = inputstreamreader.read(cs);
			} catch (IOException ioexception) {
				System.err.println(ioexception);
				System.exit(1);
			}
			if (i < 0)
				System.exit(0);
			lexer.setSource(new String(cs, 0, i).trim());
			try {
				do {
					lexer.nextToken(token);
					token.dump(System.out);
				} while (token.type != 1);
			} catch (SVGParseException svgparseexception) {
				System.err.println(svgparseexception.toString());
			}
			System.out.println();
		}
	}
}