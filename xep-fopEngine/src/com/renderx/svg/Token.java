/*
 * Token - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg;

import java.io.PrintStream;

class Token {
	public static final int INVALID_TYPE = -2147483648;

	public static final int END_OF_DATA = 1;

	public static final int LEFT_PARENTHESIS = 2;

	public static final int RIGHT_PARENTHESIS = 3;

	public static final int LEFT_BRACKET = 4;

	public static final int RIGHT_BRACKET = 5;

	public static final int LEFT_BRACE = 6;

	public static final int RIGHT_BRACE = 7;

	public static final int COMMA = 8;

	public static final int SLASH = 9;

	public static final int COLON = 10;

	public static final int SEMICOLON = 11;

	public static final int COLOR = 12;

	public static final int STRING = 13;

	public static final int QUOTED_STRING = 14;

	public static final int QUANTITY = 15;

	public int type = -2147483648;

	public double numValue = Double.MIN_VALUE;

	public String stringValue = null;

	public Token() {
		/* empty */
	}

	public Token(int i) {
		type = i;
	}

	public String getTypeName() {
		switch (type) {
		case -2147483648:
			return "<INVALID TYPE>";
		case 1:
			return "<END OF DATA>";
		case 3:
			return "RIGHT PARENTHESIS";
		case 2:
			return "LEFT PARENTHESIS";
		case 5:
			return "RIGHT BRACKET";
		case 4:
			return "LEFT BRACKET";
		case 7:
			return "RIGHT BRACE";
		case 6:
			return "LEFT BRACE";
		case 8:
			return "COMMA";
		case 9:
			return "SLASH";
		case 10:
			return "COLON";
		case 11:
			return "SEMICOLON";
		case 12:
			return "COLOR";
		case 13:
			return "STRING";
		case 14:
			return "QUOTED STRING";
		case 15:
			return "QUANTITY";
		default:
			return "<BAD TOKEN TYPE - INTERNAL ERROR>";
		}
	}

	public void dump(PrintStream printstream) {
		printstream.print(getTypeName());
		switch (type) {
		case 13:
		case 14:
			printstream.print(": \"" + stringValue + "\"");
			break;
		case 12:
			printstream.print(": #" + Long.toHexString(Math.round(numValue)));
			break;
		case 15:
			printstream.print(": " + numValue
					+ (stringValue == null ? "" : stringValue));
			break;
		}
		printstream.println();
	}

	public void reset() {
		numValue = Double.MIN_VALUE;
		stringValue = null;
	}
}