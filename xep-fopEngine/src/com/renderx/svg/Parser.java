/*
 * Parser - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;

import com.renderx.fonts.FontCatalog;
import com.renderx.fonts.FontConfigurationException;
import com.renderx.graphics.ImageFactory;
import com.renderx.graphics.vector.ImageTree;
import com.renderx.sax.InputSource;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.sax.XPP;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;
import com.renderx.util.User;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;

public class Parser {
	private XPP xpp;

	private static final String DEFAULT_STYLESHEET = "resource:com/renderx/svg/preprocessor.xsl";

	private final ErrorHandler errHandler;

	private final ImageFactory imageFactory;

	private final FontCatalog fontCatalog;

	class FNFilter extends XMLFilterImpl {
		public FNFilter(XMLReader xmlreader) {
			super(xmlreader);
		}

		public void parse(org.xml.sax.InputSource inputsource)
				throws SAXException, IOException {
			try {
				super.parse(inputsource);
			} catch (IOException ioexception) {
				if (errHandler != null)
					errHandler.error("File could not be retrieved from "
							+ inputsource.getSystemId() + "; skipped");
				super.parse(new org.xml.sax.InputSource(new StringReader(
						"<void/>")));
			}
		}

		public void parse(String string) throws SAXException, IOException {
			try {
				super.parse(string);
			} catch (IOException ioexception) {
				if (errHandler != null)
					errHandler.error("File could not be retrieved from "
							+ string + "; skipped");
				super.parse(new org.xml.sax.InputSource(new StringReader(
						"<void/>")));
			}
		}
	}

	protected Parser() throws IOException, SAXException,
			FontConfigurationException {
		this(new URLCache(new DefaultErrorHandler()), new DefaultErrorHandler());
	}

	private Parser(URLCache urlcache, ErrorHandler errorhandler)
			throws IOException, SAXException, FontConfigurationException {
		this(new FontCatalog(urlcache, errorhandler), urlcache, errorhandler);
	}

	private Parser(FontCatalog fontcatalog, URLCache urlcache,
			ErrorHandler errorhandler) throws IOException, SAXException {
		this(new ImageFactory(fontcatalog, urlcache, errorhandler),
				fontcatalog, errorhandler);
	}

	public Parser(ImageFactory imagefactory, FontCatalog fontcatalog,
			ErrorHandler errorhandler) throws IOException, SAXException {
		imageFactory = imagefactory;
		fontCatalog = fontcatalog;
		errHandler = errorhandler;
		String string = User.getProperty("com.renderx.svg.preprocessor");
		if (string == null)
			string = "resource:com/renderx/svg/preprocessor.xsl";
		xpp = new XPP(XMLReaderFactory.createXMLReader(), new InputSource(
				string));
	}

	public void setErrorHandler(org.xml.sax.ErrorHandler errorhandler) {
		xpp.setErrorHandler(errorhandler);
	}

	public ImageTree parse(XMLReader xmlreader,
			org.xml.sax.InputSource inputsource, URLSpec urlspec, double d,
			double d_0_) throws IOException, SAXException {
		SVGParserHandler svgparserhandler = new SVGParserHandler(imageFactory,
				fontCatalog, errHandler, d, d_0_);
		svgparserhandler.setBaseURL(urlspec);
		xpp.setXMLReader(new FNFilter(xmlreader));
		xpp.parse(inputsource, svgparserhandler);
		if (svgparserhandler.tree == null || svgparserhandler.tree.root == null)
			throw new SVGParseException(
					"Internal error: SVG parser misconfigured");
		return svgparserhandler.tree;
	}

	public static void main(String[] strings) throws Exception {
		String string = null;
		if (strings.length == 1) {
			string = strings[0] + ".ps";
			System.out.println("Dumping graphic tree to " + string);
		} else if (strings.length == 2)
			string = strings[1];
		else {
			System.out
					.println("Usage: com.renderx.svg.Parser <input file name> [<output file name>]");
			System.exit(0);
		}
		URLSpec urlspec = new URLSpec(strings[0]);
		new Parser().parse(XMLReaderFactory.createXMLReader(),
				new InputSource(urlspec), urlspec, 0.0, 0.0).dump(
				new PrintStream(new FileOutputStream(string)), "");
	}
}