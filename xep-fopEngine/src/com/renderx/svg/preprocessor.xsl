<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- ===================================================================

This stylesheet normalizes an SVG tree before passing it to the formatter.

==================================================================== -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <xsl:output method="xml" indent="no"/>
  <xsl:param name="system-language">en</xsl:param>
  
  <!-- Top-level -->
  <xsl:template match="/">
    <xsl:apply-templates select="svg:svg"/>
  </xsl:template>
  
  <xsl:template match="/svg:svg">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <svg:defs>
        <!-- Move style to the top -->
        <xsl:apply-templates select="//svg:style" mode="copy"/>
        <!-- Collect all clip paths, markers, and paint servers -->
        <xsl:apply-templates select="//svg:clipPath" mode="copy"/>
        <xsl:apply-templates select="//svg:pattern" mode="copy"/>
        <xsl:apply-templates select="//svg:linearGradient" mode="copy"/>
        <xsl:apply-templates select="//svg:radialGradient" mode="copy"/>
        <xsl:apply-templates select="//svg:marker" mode="copy"/>
      </svg:defs>
      <svg:g>
        <xsl:apply-templates select="@class"/>
        <xsl:apply-templates/>
      </svg:g>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="svg:style 
                     | svg:linearGradient 
                     | svg:radialGradient
                     | svg:clipPath
                     | svg:pattern"/>
                     
  <xsl:template match="svg:style 
                     | svg:linearGradient 
                     | svg:radialGradient
                     | svg:clipPath
                     | svg:pattern" mode="copy">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- Resolve references -->
  <xsl:template match="svg:use | svg:tref">
    <xsl:variable name="doc-url">
      <xsl:call-template name="extract-doc-url">
        <xsl:with-param name="url" select="@xlink:href"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="fragment-id">
      <xsl:call-template name="extract-fragment-id">
        <xsl:with-param name="url" select="@xlink:href"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$doc-url = ''">
        <xsl:apply-templates select="." mode="resolved-uri">
          <xsl:with-param name="fragment" select="//*[@id=$fragment-id]"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="resolved-uri">
          <xsl:with-param name="fragment" select="document($doc-url, /)//*[@id=$fragment-id]"/>
        </xsl:apply-templates>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="svg:use" mode="resolved-uri">
    <xsl:param name="fragment"/>
    <xsl:variable name="x">
      <xsl:choose>
        <xsl:when test="@x"><xsl:value-of select="@x"/></xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="y">
      <xsl:choose>
        <xsl:when test="@y"><xsl:value-of select="@y"/></xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <svg:g transform="{normalize-space(concat(@transform, ' translate(', $x, ',', $y, ')'))}">
      <xsl:apply-templates select="@*[not(local-name()='height')
                                  and not(local-name()='width')
                                  and not(local-name()='x')
                                  and not(local-name()='y')
                                  and not(local-name()='href')
                                  and not(local-name()='transform')]"/>
      <xsl:choose>
        <xsl:when test="$fragment[self::svg:svg or self::svg:symbol]">
          <svg:svg width="100%" height="100%">
            <xsl:apply-templates select="$fragment/@*"/>
            <xsl:apply-templates select="@width | @height"/>
            <xsl:apply-templates select="$fragment/node()"/>
          </svg:svg>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="$fragment"/>
        </xsl:otherwise>
      </xsl:choose>
    </svg:g>
  </xsl:template>

  <xsl:template match="svg:tref" mode="resolved-uri">
    <xsl:param name="fragment"/>
    <svg:tspan>
      <xsl:apply-templates select="@*"/>
      <xsl:value-of select="$fragment"/>
    </svg:tspan>
  </xsl:template>
  
  <!-- Suppress 'defs' and 'symbol' -->
  <xsl:template match="svg:defs"/>
  <xsl:template match="svg:symbol"/>
  
  <!-- Disable processing of elements with extensions -->
  <xsl:template match="*[@requiredExtensions]"/>

  <!-- Disable processing of elements with unsupported features -->
  <xsl:template match="*[@requiredFeatures[normalize-space()='']
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVG')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVG-static')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVG-dynamic')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVG-animation')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVGDOM')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVGDOM-static')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVGDOM-dynamic')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVGDOM-animation')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#ContainerAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Style')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#ViewportAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Text')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#PaintAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#OpacityAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#GraphicsAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#BasicGraphicsAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#ColorProfile')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Gradient')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Pattern')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Clip')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Mask')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Filter')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#BasicFilter')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#XlinkAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Font')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#BasicFont')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Extensibility')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#ExternalResourcesRequired')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#View')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Script')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Animation')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Hyperlinking')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Cursor')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#DocumentEventsAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#GraphicalEventsAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#AnimationEventsAttribute')]"/>

  <!-- Conditional processing inside 'switch' -->
  <xsl:template match="svg:switch">
    <svg:g>
      <xsl:apply-templates select="@*"/>
      <xsl:variable name="preselected-children" select="*[not(@requiredExtensions)
                 and not(@requiredFeatures[normalize-space()=''] 
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVG')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVG-static')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVG-dynamic')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVG-animation')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVGDOM')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVGDOM-static')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVGDOM-dynamic')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#SVGDOM-animation')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#ContainerAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Style')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#ViewportAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Text')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#PaintAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#OpacityAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#GraphicsAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#BasicGraphicsAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#ColorProfile')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Gradient')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Pattern')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Clip')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Mask')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Filter')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#BasicFilter')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#XlinkAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Font')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#BasicFont')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Extensibility')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#ExternalResourcesRequired')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#View')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Script')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Animation')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Hyperlinking')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#Cursor')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#DocumentEventsAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#GraphicalEventsAttribute')
                      or contains(@requiredFeatures, 'http://www.w3.org/TR/SVG11/feature#AnimationEventsAttribute'))]"/>
      <xsl:variable name="preselected-children-with-language" 
                    select="$preselected-children[@systemLanguage = $system-language]"/>

      <xsl:variable name="preselected-children-with-no-language" 
                    select="$preselected-children[not(@systemLanguage)]"/>

      <xsl:choose>
        <xsl:when test="$preselected-children-with-language">
          <xsl:apply-templates select="$preselected-children-with-language[1]"/>
        </xsl:when>
        <xsl:when test="$preselected-children-with-no-language">
          <xsl:apply-templates select="$preselected-children-with-no-language[1]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="$preselected-children[1]"/>
        </xsl:otherwise>
      </xsl:choose>        
    </svg:g>
  </xsl:template>

  <!-- Default: identity transformation -->
  <xsl:template match="@*|*|text()" priority="-10">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <!-- Utility: get an URL with no fragment ID from a string -->
  <xsl:template name="extract-doc-url">
    <xsl:param name="url"/>
    <xsl:variable name="dirty-url" select="normalize-space($url)"/>
    <xsl:variable name="clean-url">
      <xsl:choose>
        <xsl:when test="starts-with($dirty-url, 'url(') 
                    and substring($dirty-url, string-length($dirty-url)) = ')'">
          <xsl:value-of select="normalize-space(substring($dirty-url, 5, string-length($dirty-url) - 5))"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$dirty-url"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="substring-before(concat($clean-url, '#'), '#')"/>
  </xsl:template>

  <!-- Utility: get fragment ID from a string -->
  <xsl:template name="extract-fragment-id">
    <xsl:param name="url"/>
    <xsl:variable name="dirty-url" select="normalize-space($url)"/>
    <xsl:variable name="clean-url">
      <xsl:choose>
        <xsl:when test="starts-with($dirty-url, 'url(') 
                    and substring($dirty-url, string-length($dirty-url)) = ')'">
          <xsl:value-of select="normalize-space(substring($dirty-url, 5, string-length($dirty-url) - 5))"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$dirty-url"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:if test="contains($clean-url, '#')">
      <xsl:variable name="dirty-fragment" select="substring-after($clean-url, '#')"/>
      <xsl:choose>
        <xsl:when test="starts-with($dirty-fragment, 'xpointer(id(')
                    and substring($dirty-fragment, string-length($dirty-url)-1) = '))'">
          <xsl:value-of select="normalize-space(substring($dirty-fragment, 13, string-length($dirty-fragment) - 14))"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$dirty-fragment"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
