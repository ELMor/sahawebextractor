/*
 * PathLexer - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg;

import java.io.IOException;
import java.io.InputStreamReader;

public class PathLexer {
	public static final String blanks = " \t\r\n\014,";

	public static final String digits = "0123456789";

	public static final String commands = "MmZzLlHhVvCcSsQqTtAa";

	private char[] data = null;

	private int length = 0;

	private int cursor = 0;

	public PathLexer() {
		/* empty */
	}

	public PathLexer(String string) {
		this();
		setSource(string);
	}

	public PathLexer(char[] cs) {
		this();
		setSource(cs);
	}

	public void setSource(String string) {
		setSource(string.toCharArray());
	}

	public void setSource(char[] cs) {
		setSource(cs, 0, cs.length);
	}

	public void setSource(char[] cs, int i, int i_0_) {
		data = cs;
		cursor = i;
		length = i + i_0_;
	}

	private void skip_whitespace() {
		for (/**/; cursor < length; cursor++) {
			if (" \t\r\n\014,".indexOf(data[cursor]) == -1)
				break;
		}
	}

	public int nextToken(Token token) throws SVGParseException {
		token.reset();
		skip_whitespace();
		if (cursor >= length)
			return token.type = 1;
		if ("MmZzLlHhVvCcSsQqTtAa".indexOf(data[cursor]) != -1) {
			token.stringValue = "" + data[cursor++];
			return token.type = 13;
		}
		int i = 1;
		if (data[cursor] == '-') {
			cursor++;
			i = -1;
		} else if (data[cursor] == '+')
			cursor++;
		if (cursor >= length)
			throw new SVGParseException("Premature end of data");
		if ("0123456789".indexOf(data[cursor]) == -1
				&& (cursor >= length - 1 || data[cursor] != '.' || "0123456789"
						.indexOf(data[cursor + 1]) == -1))
			throw new SVGParseException(
					"Invalid data: command character or digit expected");
		boolean bool = false;
		double d = 1.0;
		token.numValue = 0.0;
		for (/**/; cursor < length; cursor++) {
			if ("0123456789".indexOf(data[cursor]) != -1) {
				int i_1_ = data[cursor] - 48;
				if (bool) {
					d /= 10.0;
					token.numValue += d * (double) i_1_;
				} else {
					token.numValue *= 10.0;
					token.numValue += (double) i_1_;
				}
			} else {
				if (data[cursor] != '.' || bool)
					break;
				bool = true;
			}
		}
		token.numValue *= (double) i;
		if (cursor < length && (data[cursor] == 'e' || data[cursor] == 'E')) {
			if (++cursor >= length)
				throw new SVGParseException("Premature end of data");
			int i_2_ = 1;
			if (data[cursor] == '-') {
				cursor++;
				i_2_ = -1;
			} else if (data[cursor] == '+')
				cursor++;
			if (cursor >= length)
				throw new SVGParseException("Premature end of data");
			if ("0123456789".indexOf(data[cursor]) == -1)
				throw new SVGParseException("Invalid data: digit expected");
			int i_3_ = 0;
			for (/**/; cursor < length
					&& "0123456789".indexOf(data[cursor]) != -1; cursor++) {
				i_3_ *= 10.0;
				i_3_ += data[cursor] - 48;
			}
			i_3_ *= i_2_;
			token.numValue *= Math.pow(10.0, (double) i_3_);
		}
		return token.type = 15;
	}

	public static void main(String[] strings) {
		char[] cs = new char[8192];
		int i = 0;
		PathLexer pathlexer = new PathLexer();
		Token token = new Token();
		for (;;) {
			try {
				InputStreamReader inputstreamreader = new InputStreamReader(
						System.in);
				i = inputstreamreader.read(cs);
			} catch (IOException ioexception) {
				System.err.println(ioexception);
				System.exit(1);
			}
			if (i < 0)
				System.exit(0);
			pathlexer.setSource(cs, 0, i);
			try {
				do {
					pathlexer.nextToken(token);
					token.dump(System.out);
				} while (token.type != 1);
			} catch (SVGParseException svgparseexception) {
				System.err.println(svgparseexception.toString());
			}
			System.out.println();
		}
	}
}