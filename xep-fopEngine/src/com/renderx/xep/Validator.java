/*
 * Validator - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep;

import java.io.IOException;
import java.net.URL;

import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;

import com.renderx.sax.JAXPSourceConverter;
import com.renderx.util.URLSpec;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Validator {
	private static final String VALIDATOR_STYLESHEET = "resource:/com/renderx/xep/folint.xsl";

	private static final int DEFAULT_STRICTNESS = 1;

	private final ValidatorCore core;

	public Validator() throws SAXException, IOException {
		this("resource:/com/renderx/xep/folint.xsl", 1);
	}

	public Validator(String string) throws SAXException, IOException {
		this(string, 1);
	}

	public Validator(int i) throws SAXException, IOException {
		this("resource:/com/renderx/xep/folint.xsl", i);
	}

	public Validator(String string, int i) throws SAXException, IOException {
		core = new ValidatorCore(new URLSpec(string), i);
	}

	public int validate(Source source) throws SAXException, IOException {
		JAXPSourceConverter jaxpsourceconverter = new JAXPSourceConverter(
				source);
		try {
			core.validate(jaxpsourceconverter.source,
					jaxpsourceconverter.parser);
		} finally {
			if (jaxpsourceconverter.stream != null)
				jaxpsourceconverter.stream.close();
		}
		return getErrorCount();
	}

	public int getErrorCount() {
		return core.getErrorCount();
	}

	public int getWarningCount() {
		return core.getWarningCount();
	}

	public void setValidationEventHandler(
			ValidationEventHandler validationeventhandler) {
		core.setValidationEventHandler(validationeventhandler);
	}

	public static void main(String[] strings) {
		if (strings.length == 0)
			System.err
					.println("Usage: com.renderx.xep.Validator <list of file names>");
		else {
			Object object = null;
			Validator validator;
			try {
				validator = new Validator();
			} catch (Exception exception) {
				System.err.println("Cannot create Validator: "
						+ exception.toString());
				return;
			}
			for (int i = 0; i < strings.length; i++) {
				Object object_0_ = null;
				InputSource inputsource;
				try {
					if ("-".equals(strings[i])) {
						inputsource = new InputSource(System.in);
						inputsource.setSystemId(new URL("stdin")
								.toExternalForm());
					} else
						inputsource = new InputSource(new URLSpec(strings[i])
								.toString());
				} catch (Exception exception) {
					System.err.println("Cannot create input source for file "
							+ strings[i] + ": " + exception.toString());
					continue;
				}
				try {
					validator.validate(new SAXSource(inputsource));
					int i_1_ = validator.getErrorCount();
					int i_2_ = validator.getWarningCount();
					System.err.print(("-".equals(strings[i]) ? "<stdin>"
							: strings[i])
							+ ": ");
					if (i_1_ == 0) {
						if (i_2_ == 0)
							System.err.println("validation successful!");
						else
							System.err.println("" + i_2_ + " warning"
									+ (i_2_ == 1 ? "" : "s"));
					} else {
						System.err.print("" + i_1_ + " error"
								+ (i_1_ == 1 ? "" : "s"));
						if (i_2_ > 0)
							System.err.print(", " + i_2_ + " warning"
									+ (i_2_ == 1 ? "" : "s"));
						System.err.println();
					}
				} catch (SAXException saxexception) {
					System.err.println(strings[i]
							+ " could not be parsed due to structure errors");
					System.err.println(saxexception.toString());
				} catch (IOException ioexception) {
					System.err.println(strings[i]
							+ " could not be parsed due to input problems");
					System.err.println(ioexception.toString());
				} catch (Exception exception) {
					System.err.println(strings[i] + " could not be parsed");
					System.err.println(exception.toString());
				}
			}
		}
	}
}