/*
 * FormatterCore - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Properties;

import com.renderx.sax.NullSource;
import com.renderx.sax.SAXStorage;
import com.renderx.util.List;
import com.renderx.xep.cmp.FO;
import com.renderx.xep.fmt.BkMaker;
import com.renderx.xep.gen.H4base;
import com.renderx.xep.gen.SAX;
import com.renderx.xep.lib.Conf;
import com.renderx.xep.lib.ConfigurationException;
import com.renderx.xep.lib.FormatterException;
import com.renderx.xep.lib.Logger;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.Parser;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class FormatterCore {
	protected final Conf config;

	private class ContentHandlerDriver extends DefaultHandler {
		private final ContentHandler generator;

		private String systemId = null;

		private Session session = null;

		private ValidatorCore validator = null;

		private SAXStorage storage = null;

		private FO fo = null;

		private ContentHandler handler = null;

		ContentHandlerDriver(String string, ContentHandler contenthandler,
				Logger logger) {
			generator = contenthandler;
			systemId = string;
			session = new Session(config, logger);
		}

		public void startDocument() throws SAXException {
			if (session == null)
				throw new RuntimeException(
						"Attempt to reuse a ContentHandlerDriver object returned by com.renderx.xep.FormatterCore");
			session.openDocument();
			if (systemId != null)
				session.event("system-id", systemId);
			if (config.VALIDATE) {
				try {
					validator = new ValidatorCore(config.VALIDATOR_STYLESHEET,
							config.STRICTNESS);
					validator.setValidationEventHandler(new ValidationLogger(
							session));
				} catch (Exception exception) {
					session
							.exception(
									"Cannot initialize validator; validation is turned off",
									exception);
				}
			}
			if (validator == null) {
				session.openState("compile");
				Parser parser = new Parser(session);
				fo = new FO();
				handler = parser.createContentHandler(systemId, fo);
			} else {
				storage = new SAXStorage();
				handler = storage;
			}
			handler.startDocument();
		}

		public void endDocument() throws SAXException {
			if (handler != null) {
				handler.endDocument();
				handler = null;
				if (validator == null)
					session.closeState("compile");
				else {
					session.openState("validate");
					try {
						validator.validate(new NullSource(systemId), storage);
					} catch (IOException ioexception) {
						throw new FormatterException(ioexception);
					}
					int i = validator.getWarningCount();
					int i_0_ = validator.getErrorCount();
					session.event("validation", getValidationMessage(i_0_, i));
					if (i_0_ != 0 && config.DISCARD_IF_NOT_VALID)
						throw new FormatterException("Invalid XSL FO source"
								+ (systemId == null ? ": " : " '" + systemId
										+ "': ") + i_0_ + " error"
								+ (i_0_ == 1 ? "" : "s")
								+ " found during validation");
					session.closeState("validate");
					session.openState("compile");
					Parser parser = new Parser(session);
					fo = new FO();
					storage.setContentHandler(parser.createContentHandler(
							systemId, fo));
					storage.play();
					storage = null;
					session.closeState("compile");
				}
				session.openState("format");
				List list = new BkMaker(fo.sequences, fo.outline, session)
						.mkBook();
				fo.masters = null;
				fo.sequences = null;
				session.closeState("format");
				session.openState("generate");
				if (generator instanceof H4base)
					session.event("output-format", ((H4base) generator)
							.outFormat());
				SAX sax = new SAX(list, fo, session);
				sax.setContentHandler(generator);
				sax.traverse();
				session.closeState("generate");
				session.closeDocument();
				session = null;
			}
		}

		public void startElement(String string, String string_1_,
				String string_2_, Attributes attributes) throws SAXException {
			if (handler != null)
				handler.startElement(string, string_1_, string_2_, attributes);
		}

		public void endElement(String string, String string_3_, String string_4_)
				throws SAXException {
			if (handler != null)
				handler.endElement(string, string_3_, string_4_);
		}

		public void characters(char[] cs, int i, int i_5_) throws SAXException {
			if (handler != null)
				handler.characters(cs, i, i_5_);
		}

		public void ignorableWhitespace(char[] cs, int i, int i_6_)
				throws SAXException {
			if (handler != null)
				handler.ignorableWhitespace(cs, i, i_6_);
		}

		public void processingInstruction(String string, String string_7_)
				throws SAXException {
			if (handler != null)
				handler.processingInstruction(string, string_7_);
		}

		public void skippedEntity(String string) throws SAXException {
			if (handler != null)
				handler.skippedEntity(string);
		}

		public void setDocumentLocator(Locator locator) {
			/* empty */
		}

		public void startPrefixMapping(String string, String string_8_)
				throws SAXException {
			if (handler != null)
				handler.startPrefixMapping(string, string_8_);
		}

		public void endPrefixMapping(String string) throws SAXException {
			if (handler != null)
				handler.endPrefixMapping(string);
		}

		public void warning(SAXParseException saxparseexception)
				throws SAXException {
			session.warning("Parse warning: "
					+ formatSAXParserMessage(saxparseexception));
		}

		public void error(SAXParseException saxparseexception)
				throws SAXException {
			session.error("Parse error: "
					+ formatSAXParserMessage(saxparseexception));
		}

		public void fatalError(SAXParseException saxparseexception)
				throws SAXException {
			session.error("Fatal parse error: "
					+ formatSAXParserMessage(saxparseexception));
			if (handler != null) {
				handler.endDocument();
				handler = null;
				if (validator == null) {
					session.closeState("compile");
					fo = null;
				} else
					storage = null;
				throw saxparseexception;
			}
		}
	}

	private static final class ValidationLogger implements
			ValidationEventHandler {
		private final Logger logger;

		public ValidationLogger(Logger logger) {
			this.logger = logger;
		}

		public void warning(String string, String string_9_, String string_10_,
				int i, int i_11_) {
			if (logger != null)
				logger.warning(formatSAXParserMessage(string, string_9_,
						string_10_, i, i_11_));
		}

		public void error(String string, String string_12_, String string_13_,
				int i, int i_14_) {
			if (logger != null)
				logger.error(formatSAXParserMessage(string, string_12_,
						string_13_, i, i_14_));
		}
	}

	public FormatterCore(Conf conf) {
		config = conf;
	}

	public synchronized void cleanup() {
		config.cleanup();
	}

	public void render(InputSource inputsource, XMLReader xmlreader,
			ContentHandler contenthandler, Logger logger) throws SAXException,
			IOException {
		ContentHandlerDriver contenthandlerdriver = new ContentHandlerDriver(
				inputsource.getSystemId(), contenthandler, logger);
		xmlreader.setContentHandler(contenthandlerdriver);
		xmlreader.setErrorHandler(contenthandlerdriver);
		xmlreader.parse(inputsource);
	}

	public ContentHandler createContentHandler(String string,
			ContentHandler contenthandler, Logger logger)
			throws ConfigurationException {
		return new ContentHandlerDriver(string, contenthandler, logger);
	}

	private static String getValidationMessage(int i, int i_15_) {
		String string = "OK";
		if (i > 0 || i_15_ > 0) {
			string = "total: ";
			if (i > 0) {
				string += "" + i + " error";
				if (i > 1)
					string += "s";
				if (i_15_ > 0)
					string += ", ";
			}
			if (i_15_ > 0) {
				string += "" + i_15_ + " warning";
				if (i_15_ > 1)
					string += "s";
			}
		}
		return string;
	}

	private static String formatSAXParserMessage(String string,
			String string_16_, String string_17_, int i, int i_18_) {
		String string_19_ = string_17_ == null ? "" : string_17_ + ": ";
		if (i != -1) {
			string_19_ += "line " + i;
			if (i_18_ != -1)
				string_19_ += ", column " + i_18_;
		}
		if (string_19_.length() > 0)
			string_19_ += ": ";
		string_19_ += (String) string;
		return string_19_;
	}

	private static String formatSAXParserMessage(
			SAXParseException saxparseexception) {
		return formatSAXParserMessage(saxparseexception.getMessage(),
				saxparseexception.getPublicId(), saxparseexception
						.getSystemId(), saxparseexception.getLineNumber(),
				saxparseexception.getColumnNumber());
	}

	public ContentHandler createGenerator(OutputStream outputstream,
			String string, Properties properties, Logger logger)
			throws ConfigurationException {
		return createGenerator(outputstream, string, properties, new Session(
				config, logger));
	}

	protected ContentHandler createGenerator(OutputStream outputstream,
			String string, Properties properties, Session session)
			throws ConfigurationException {
		if (string == null)
			throw new ConfigurationException("Format cannot be 'null'");
		Object object = null;
		if (this != null) {
			/* empty */
		}
		String string_20_ = Conf.getBackendClassName(string);
		if (string_20_ == null)
			throw new ConfigurationException("Unknown output format: " + string);
		Class var_class;
		try {
			var_class = Class.forName(string_20_);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new ConfigurationException("Cannot locate generator for '"
					+ string + "': class " + string_20_
					+ " not found in the classpath");
		} catch (Exception exception) {
			throw new ConfigurationException(
					("Cannot access generator class for '" + string
							+ "' (class " + string_20_ + ")"), exception);
		}
		if (!H4base.class.isAssignableFrom(var_class))
			throw new ConfigurationException("Invalid generator for '" + string
					+ "': class " + var_class.getName()
					+ " is not a subclass of com.renderx.xep.gen.H4base");
		Properties properties_21_ = new Properties();
		Properties properties_22_ = config.getBackendOptions(string);
		if (properties_22_ != null) {
			Enumeration enumeration = properties_22_.propertyNames();
			while (enumeration.hasMoreElements()) {
				String string_23_ = (String) enumeration.nextElement();
				properties_21_.put(string_23_, properties_22_
						.getProperty(string_23_));
			}
		}
		if (properties != null) {
			Enumeration enumeration = properties.propertyNames();
			while (enumeration.hasMoreElements()) {
				String string_24_ = (String) enumeration.nextElement();
				properties_21_.put(string_24_, properties
						.getProperty(string_24_));
			}
		}
		Object object_25_ = null;
		H4base h4base;
		try {
			h4base = (H4base) (var_class.getDeclaredConstructor(new Class[] {
					OutputStream.class, Session.class, Properties.class })
					.newInstance(new Object[] { outputstream, session,
							properties_21_ }));
		} catch (Exception exception) {
			throw new ConfigurationException(
					("Cannot instantiate generator for '" + string
							+ "' (class " + var_class.getName() + ")"),
					exception);
		}
		return h4base;
	}
}