/*
 * NoPageMasterException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.xep.pre.Attr;

public class NoPageMasterException extends Exception {
	public Attr name = null;

	public NoPageMasterException() {
		/* empty */
	}

	public NoPageMasterException(String string) {
		super(string);
	}

	public NoPageMasterException(Attr attr) {
		super("No page master with name '" + attr.word() + "'");
		name = attr;
	}
}