/*
 * Item - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import java.util.Enumeration;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.FontRecord;
import com.renderx.fonts.Metric;
import com.renderx.graphics.Image;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.xep.fmt.BPB;
import com.renderx.xep.fmt.BkMaker;
import com.renderx.xep.fmt.Instruction;
import com.renderx.xep.fmt.InstructionDispatcher;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.lib.Util;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Attributed;
import com.renderx.xep.pre.Elem;

public abstract class Item extends Xattr {
	public static final short PHRASE_BREAK = 1;

	public static final short ISP_BREAK = 2;

	public static final short LINE_BREAK = 4;

	public static final short DSP_BREAK = 8;

	public static final short BLOCK_BREAK = 16;

	public static final short HYPHENATED = 32;

	public static final short COMPLEX = 64;

	public static final short KEEP_FILLED = 256;

	public static final short SAVE_SWITCH = 512;

	public static final short USED = 8192;

	public static final short SKIPPED = 16384;

	protected short status = 0;

	public static final short CXT_AUTO = 0;

	public static final short CXT_COLUMN = 1;

	public static final short CXT_PAGE = 2;

	public static final short CXT_ODD_PAGE = 3;

	public static final short CXT_EVEN_PAGE = 4;

	private static final Hashtable cxts = new Hashtable();

	public static final short BEFORE_EDGE = 0;

	public static final short AFTER_EDGE = 1;

	public static final short START_EDGE = 2;

	public static final short END_EDGE = 3;

	private static final Attn[] margin_edge;

	private static final Attn[] padding_edge;

	private static final Attn[] border_edge;

	private static final Attn[] edge_indent;

	public static final Endb endb;

	public static final Endli endli;

	public static final Endf endf;

	public static final Ends ends;

	public static final Endl endl;

	public static final FootnotesPending footnotesPending;

	public static final FootnotesAppended footnotesAppended;

	public static final short FL_NONE = 0;

	public static final short FL_BEFORE = 1;

	public static final short FL_START = 2;

	public static final short FL_END = 3;

	public static final short FL_INSIDE = 4;

	public static final short FL_OUTSIDE = 5;

	private static final Hashtable fltab;

	public static final short CL_NONE = 0;

	public static final short CL_START = 1;

	public static final short CL_END = 2;

	public static final short CL_BOTH = 3;

	public static final short CL_INSIDE = 4;

	public static final short CL_OUTSIDE = 8;

	private static final Hashtable cltab;

	public static final Endw endw;

	public static final Empty empty;

	public static final Filled filled;

	public static final Eofl eofl;

	public static class Bookmark extends Item implements Instruction {
		public Item.Id id;

		public Integer bno;

		public Item.Bookmark parent;

		public String label = "";

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public void pass(InstructionDispatcher instructiondispatcher) {
			instructiondispatcher.process(this);
		}

		public Bookmark(Attributed attributed, Item.Bookmark bookmark_0_,
				Session session) {
			super(attributed);
			parent = bookmark_0_;
			bno = new Integer(session.nextno());
		}
	}

	public static class Level extends Item {
		public List bnos = new List();

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Level() {
			setFlags(279);
		}

		public void register(Item.Block block) {
			bnos.cons(block.bno);
			block.level = this;
		}
	}

	public static class Eofl extends Item {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		Eofl() {
			setFlags(95);
		}
	}

	public static class Filled extends Item implements Instruction {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public void pass(InstructionDispatcher instructiondispatcher) {
			instructiondispatcher.process(this);
		}

		public Filled() {
			setFlags(799);
		}
	}

	public static class Empty extends Item {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Empty() {
			setFlags(23);
		}
	}

	public static class Softsync extends Sync {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Softsync(String string) {
			super(string);
		}
	}

	public static class Sync extends Fctl {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Sync(String string) {
			super(string);
			setFlags(23);
		}
	}

	public static class Label extends Fctl {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Label(String string) {
			super(string);
		}
	}

	public static class Jump extends Fctl {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Jump(String string) {
			super(string);
		}

		public Jump(Flow flow) {
			super((String) null);
			adr = flow;
		}
	}

	public static class Fork extends Fctl {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Fork(String string) {
			super(string);
			setFlags(23);
		}
	}

	public abstract static class Fctl extends Item {
		public Flow adr = null;

		public String id = null;

		public Fctl(String string) {
			id = string;
			setFlags(320);
		}

		public String toString() {
			return super.toString() + ":" + id;
		}
	}

	public static class Endw extends Item {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Endw() {
			super(AttList.empty);
			setFlags(95);
		}
	}

	public static class Wide extends Item {
		public final Integer wno;

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Wide(Attributed attributed, Session session) {
			super(attributed);
			wno = new Integer(session.nextno());
			setFlags(95);
		}

		public Wide(Session session) {
			this(AttList.empty, session);
		}
	}

	public static class Clear extends Exception {
		public short edge = 0;

		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Clear(Attributed attributed) {
			super(attributed);
			edge = ((Short) Item.cltab.get(attributed.get(Attn.$clear)))
					.shortValue();
			setFlags(5);
		}
	}

	public static class Float extends Exception {
		public short edge = 0;

		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Float(Attributed attributed) {
			super(attributed);
			flow = new Flow();
			edge = ((Short) Item.fltab.get(attributed.get(Attn.$float)))
					.shortValue();
			setFlags(1);
		}
	}

	public static class Remark extends Exception {
		public static final short BND_PAGE = 0;

		public static final short BND_SEQ = 1;

		public static final short BND_DOC = 2;

		public static final short POS_FIRST = 0;

		public static final short POS_SFIRST = 1;

		public static final short POS_EFIRST = 2;

		public static final short POS_SLAST = 3;

		public static final short POS_ELAST = 4;

		public static final short POS_LAST = 5;

		public Attr name;

		public short boundary;

		public short pos;

		private static final Hashtable idtab = new Hashtable();

		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Remark(Attributed attributed) {
			super(attributed);
			name = attributed.get(Attn.$retrieve_class_name);
			boundary = ((Short) idtab.get(attributed
					.get(Attn.$retrieve_boundary))).shortValue();
			pos = ((Short) idtab.get(attributed.get(Attn.$retrieve_position)))
					.shortValue();
		}

		static {
			idtab.put(Attr.newWord("page"), new Short((short) 0));
			idtab.put(Attr.newWord("page-sequence"), new Short((short) 1));
			idtab.put(Attr.newWord("document"), new Short((short) 2));
			idtab.put(Attr.newWord("first-starting-within-page"), new Short(
					(short) 1));
			idtab.put(Attr.newWord("first-including-carryover"), new Short(
					(short) 0));
			idtab.put(Attr.newWord("last-starting-within-page"), new Short(
					(short) 3));
			idtab.put(Attr.newWord("last-ending-within-page"), new Short(
					(short) 4));
		}
	}

	public static class Break extends Exception {
		public short context;

		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Break(Attributed attributed, String string) {
			super(attributed);
			context = ((Short) Item.cxts.get(attributed.get(Attn
					.oldName("break-" + string)))).shortValue();
			setFlags(23);
		}
	}

	public static class Keep extends Exception {
		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Keep(Attributed attributed) {
			super(attributed);
			setFlags(23);
		}
	}

	public static class Pinpoint extends Exception implements Instruction {
		public int x = 0;

		public int y = 0;

		public Attr writing_mode;

		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public void pass(InstructionDispatcher instructiondispatcher) {
			instructiondispatcher.process(this);
		}

		public Pinpoint(Attributed attributed) {
			super(attributed);
			writing_mode = attributed.get(Attn.$writing_mode);
		}
	}

	public static class Endk extends Tag {
		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Endk(Attributed attributed, Attr attr) {
			super(attributed);
			id = attr;
		}
	}

	public static class Key extends Tag {
		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Key(Attributed attributed, Session session) {
			super(attributed);
			id = (attributed.containsKey(Attn.$id) ? (Attr) attributed
					.get(Attn.$id) : Attr.newWord("rx:key@" + session.nextno()));
		}
	}

	public static class Lid extends Id {
		public Lid(Attributed attributed, Attr attr) {
			super(attributed);
			id = Attr.newWord("rx:last@" + attr.word());
		}
	}

	public static class Id extends Tag implements Instruction {
		public static final Item.Id UNRESOLVED = new Item.Id();

		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public void pass(InstructionDispatcher instructiondispatcher) {
			instructiondispatcher.process(this);
		}

		public Id(Attributed attributed) {
			super(attributed);
			this.id = (attributed.containsKey(Attn.$id) ? (Attr) attributed
					.get(Attn.$id) : Attr.newWord(""));
		}

		private Id() {
			super(AttList.empty);
			this.id = Attr.newWord("");
			page = new BkMaker.Page(0);
		}
	}

	public abstract static class Tag extends Exception {
		int desc_count;

		public BkMaker.Page page = null;

		public int x = 0;

		public int y = 0;

		public Attr id;

		public Attr writing_mode;

		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Tag(Attributed attributed) {
			super(attributed);
			writing_mode = attributed.get(Attn.$writing_mode);
		}
	}

	public static class PageKey extends Exception {
		public List terms = new List();

		public static class Term extends Xattr {
			public final Attr ref_key;

			public final Item.Text RS;

			public final boolean merge;

			public final boolean link;

			public List ranges;

			Term(Attributed attributed, Session session) {
				super(attributed);
				ref_key = attributed.get(Attn.$ref_key);
				RS = new Item.Text(
						attributed.get(Attn.$range_separator).word(),
						attributed, session);
				merge = attributed.get(Attn.$merge_subsequent_page_numbers)
						.bool();
				link = attributed.get(Attn.$link_back).bool();
			}
		}

		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public PageKey(Attributed attributed) {
			super(attributed);
		}
	}

	public static class PageNumber extends Exception {
		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public PageNumber(Attributed attributed) {
			super(attributed);
		}
	}

	public static class FootnotesAppended extends Item {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public FootnotesAppended() {
			super(AttList.empty);
			setFlags(512);
		}
	}

	public static class FootnotesPending extends Exception {
		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public FootnotesPending() {
			super(AttList.empty);
		}
	}

	public static class Footnote extends Exception {
		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Footnote(Attributed attributed) {
			super(attributed);
			flow = new Flow();
		}
	}

	public static class Rotated extends Exception {
		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Rotated(Attributed attributed) {
			super(attributed);
			setFlags(31);
		}
	}

	public static class Fixed extends Exception {
		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Fixed(Attributed attributed) {
			super(attributed);
			flow = new Flow();
		}
	}

	public static class Absolute extends Exception {
		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Absolute(Attributed attributed) {
			super(attributed);
			flow = new Flow();
		}
	}

	public static class Exception extends Item {
		public Flow flow = null;

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public void pass(ExceptionDispatcher exceptiondispatcher) {
			exceptiondispatcher.process(this);
		}

		public Exception(Attributed attributed) {
			super(attributed);
		}
	}

	public abstract static class Washer extends Item {
		public int thickness;

		public static class Inline extends Item.Washer implements Instruction {
			public void pass(ItemDispatcher itemdispatcher) {
				itemdispatcher.process(this);
			}

			public void pass(InstructionDispatcher instructiondispatcher) {
				instructiondispatcher.process(this);
			}

			public Inline(int i) {
				super(i);
				setFlags(2);
			}
		}

		public static class Display extends Item.Washer {
			public void pass(ItemDispatcher itemdispatcher) {
				itemdispatcher.process(this);
			}

			public Display(int i) {
				super(i);
				setFlags(31);
			}
		}

		public Washer(int i) {
			thickness = i;
		}
	}

	public abstract static class Space extends Item {
		private static final int FORCE = 2147483647;

		private String tag;

		private int base;

		private Attn tagmin;

		private Attn tagopt;

		private Attn tagmax;

		public int minimum;

		public int maximum;

		public int optimum;

		public int precedence;

		public boolean retain;

		public boolean nil = false;

		public static class Inline extends Item.Space {
			public void pass(ItemDispatcher itemdispatcher) {
				itemdispatcher.process(this);
			}

			public Inline(Attributed attributed, String string) {
				super(attributed, string);
			}

			public Inline() {
				this(AttList.empty, "space-start");
			}
		}

		public static class Display extends Item.Space {
			public void pass(ItemDispatcher itemdispatcher) {
				itemdispatcher.process(this);
			}

			public Display(Attributed attributed, String string) {
				super(attributed, string);
				setFlags(23);
			}

			public Display() {
				this(AttList.empty, "space-before");
			}
		}

		public Space(Attributed attributed, String string) {
			super(attributed);
			tag = string;
			Attr attr = attributed.get(Attn.oldName(string + ".precedence"));
			if (attr instanceof Attr.Word)
				precedence = 2147483647;
			else
				precedence = attr.count();
			retain = (attributed.get(Attn.oldName(string + ".conditionality")) == Attr.retain);
			tagmin = Attn.oldName(string + ".minimum");
			tagopt = Attn.oldName(string + ".optimum");
			tagmax = Attn.oldName(string + ".maximum");
			dimensions(1000);
			nil = precedence == 0 && minimum == maximum && optimum == 0;
			base = optimum = -2147483648;
		}

		public void dimensions(int i) {
			if (i != base) {
				minimum = this.get(tagmin).length_or_ratio(i);
				maximum = this.get(tagmax).length_or_ratio(i);
				optimum = this.get(tagopt).length_or_ratio(i);
				if (minimum > maximum) {
					if (a.containsKey(tagmax))
						minimum = maximum;
					else
						maximum = minimum;
				}
				if (!a.containsKey(tagopt))
					optimum = minimum;
				if (optimum < minimum)
					minimum = optimum;
				if (maximum < optimum)
					maximum = optimum;
				base = i;
			}
		}

		public Item.Space merge(Item.Space space_1_) {
			Item.Space space_2_ = (Item.Space) space_1_.clone();
			if (optimum == -2147483648) {
				minimum = space_1_.minimum;
				optimum = space_1_.optimum;
				maximum = space_1_.maximum;
				precedence = space_1_.precedence;
			} else if (space_1_.precedence < precedence)
				space_2_.minimum = space_2_.optimum = space_2_.maximum = 0;
			else if (precedence != 2147483647) {
				if (space_1_.precedence > precedence
						|| space_1_.optimum > optimum) {
					space_2_.minimum = space_1_.minimum - minimum;
					minimum = space_1_.minimum;
					space_2_.optimum = space_1_.optimum - optimum;
					optimum = space_1_.optimum;
					space_2_.maximum = space_1_.maximum - maximum;
					maximum = space_1_.maximum;
					if (space_2_.minimum > space_2_.optimum)
						space_2_.minimum = space_2_.optimum;
					if (space_2_.maximum < space_2_.optimum)
						space_2_.maximum = space_2_.optimum;
					precedence = space_1_.precedence;
				} else
					space_2_.minimum = space_2_.optimum = space_2_.maximum = 0;
			}
			return space_2_;
		}

		public String toString() {
			if (base == -2147483648)
				return (super.toString() + ": "
						+ this.get(Attn.oldName(tag + ".minimum")) + ","
						+ this.get(Attn.oldName(tag + ".optimum")) + ","
						+ this.get(Attn.oldName(tag + ".maximum")) + ","
						+ this.get(Attn.oldName(tag + ".precedence")) + "," + this
						.get(Attn.oldName(tag + ".conditionality")));
			return (super.toString()
					+ ": "
					+ minimum
					+ ","
					+ optimum
					+ ","
					+ maximum
					+ ","
					+ (precedence == 2147483647 ? "force" : Integer
							.toString(precedence)) + "," + (retain ? "retain"
					: "discard"));
		}
	}

	public static class Image extends Inline implements Instruction {
		public com.renderx.graphics.Image im = null;

		public double[] scale = { 1.0, 1.0 };

		private int base = -2147483648;

		Item.Span span = null;

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public void pass(InstructionDispatcher instructiondispatcher) {
			instructiondispatcher.process(this);
		}

		public Image(Attributed attributed, Session session) {
			super(attributed);
			
			Attr ibr;
			try {
				ibr = attributed.get(Attn.$image_base_url);
			} catch (InternalException e) {
				ibr=new Attr.Word("/");
			}
			im = (session.config.imageFactory.makeImage(
					attributed.get(Attn.$src), 
					ibr,
					attributed.get(Attn.$content_type), session));
			tt = new Item.TextTraits(attributed, session);
			Attn[] attns = { Attn.$block_progression_dimension_minimum,
					Attn.$block_progression_dimension_optimum,
					Attn.$block_progression_dimension_maximum };
			for (int i = 0; i != attns.length; i++) {
				if (attributed.get(attns[i]) instanceof Attr.Ratio)
					this.put(attns[i], Attr.auto);
			}
			setFlags(10);
		}

		public void dimensions(int i) {
			if (i != base) {
				double[] ds = { im.width * 1000.0, im.height * 1000.0 };
				Attr[] attrs = { this.get(Attn.$content_width),
						this.get(Attn.$content_height) };
				int[] is = {
						(Attr.dim_range(this,
								Attn.$inline_progression_dimension_minimum,
								Attn.$inline_progression_dimension_optimum,
								Attn.$inline_progression_dimension_maximum, i)),
						(Attr.dim_range(this,
								Attn.$block_progression_dimension_minimum,
								Attn.$block_progression_dimension_optimum,
								Attn.$block_progression_dimension_maximum, i)) };
				for (int i_3_ = 0; i_3_ != 2; i_3_++) {
					if (attrs[i_3_] instanceof Attr.Length)
						scale[i_3_] = (double) attrs[i_3_].length() / ds[i_3_];
					else if (attrs[i_3_] instanceof Attr.Ratio)
						scale[i_3_] = attrs[i_3_].ratio();
					else if (is[i_3_] != -2147483648
							&& attrs[i_3_] == Attr.newWord("scale-to-fit"))
						scale[i_3_] = (double) is[i_3_] / ds[i_3_];
				}
				if (this.get(Attn.$scaling) == Attr.newWord("uniform")) {
					if (attrs[0] != Attr.auto && attrs[1] != Attr.auto) {
						if (scale[0] < scale[1])
							scale[1] = scale[0];
						else
							scale[0] = scale[1];
					} else if (attrs[0] == Attr.auto)
						scale[0] = scale[1];
					else
						scale[1] = scale[0];
				}
				for (int i_4_ = 0; i_4_ != 2; i_4_++) {
					if (is[i_4_] == -2147483648)
						is[i_4_] = (int) Math.round(ds[i_4_] * scale[i_4_]);
				}
				ascender = is[1];
				descender = 0;
				width = is[0];
				tt.line_height = tt.font_size = tt.x_height = tt.max_ascender = ascender;
				tt.lea = tt.min_descender = 0;
				tt.wsp = tt.csp = 0;
				tt.varcsp = tt.varwsp = false;
				span.tt = tt;
				span.ascender = ascender;
				span.descender = descender;
				base = i;
			}
		}
	}

	public static class Leader extends Inline implements Instruction {
		private int base = -2147483648;

		Item.Span span = null;

		public Item.Text dot = null;

		public int length;

		public int len_opt;

		public int len_min;

		public int len_max;

		public int patwidth;

		private static final Hashtable leader_patterns = new Hashtable();

		public short leader_pattern;

		public static final short SPACE = 0;

		public static final short RULE = 1;

		public static final short DOTS = 2;

		public static final short USE_CONTENT = 3;

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public void pass(InstructionDispatcher instructiondispatcher) {
			instructiondispatcher.process(this);
		}

		public Leader(Attributed attributed, Session session) {
			super(attributed);
			try {
				leader_pattern = ((Short) leader_patterns.get(attributed
						.get(Attn.$leader_pattern))).shortValue();
			} catch (NullPointerException nullpointerexception) {
				session.error("invalid leader-pattern "
						+ attributed.get(Attn.$leader_pattern));
				leader_pattern = (short) 0;
			}
			tt = new Item.TextTraits(attributed, session);
			tt.varcsp = false;
			tt.varwsp = false;
			while_10_: for (;;) {
				switch (leader_pattern) {
				case 0:
					ascender = descender = 0;
					break while_10_;
				case 1:
					if (attributed.get(Attn.$rule_style) == Attr.none)
						leader_pattern = (short) 0;
					tt.min_descender = descender = -(tt.max_ascender = ascender = (attributed
							.get(Attn.$rule_thickness).length() / 2));
					tt.x_height = 0;
					break while_10_;
				case 2:
					dot = new Item.Text(
							attributed.get(Attn.$leader_dot).word(),
							attributed, session);
					if (this.get(Attn.$writing_mode) == Attr.rl_tb) {
						int i = dot.chars.length / 2;
						int i_5_ = dot.chars.length - 1;
						for (int i_6_ = 0; i_6_ != i; i_6_++) {
							char c = dot.chars[i_6_];
							dot.chars[i_6_] = dot.chars[i_5_];
							dot.chars[i_5_] = c;
							i_5_--;
						}
					}
					dot.tt.varcsp = dot.tt.varwsp = false;
					if (Util.ne0(dot.width)) {
						ascender = dot.ascender;
						descender = dot.descender;
					} else {
						leader_pattern = (short) 0;
						break;
					}
					break while_10_;
				case 3:
					dot = new Item.Text(attributed.get(Attn.$leader_content)
							.word(), attributed, session);
					if (this.get(Attn.$writing_mode) == Attr.rl_tb) {
						int i = dot.chars.length / 2;
						int i_7_ = dot.chars.length - 1;
						for (int i_8_ = 0; i_8_ != i; i_8_++) {
							char c = dot.chars[i_8_];
							dot.chars[i_8_] = dot.chars[i_7_];
							dot.chars[i_7_] = c;
							i_7_--;
						}
					}
					dot.tt.varcsp = dot.tt.varwsp = false;
					if (Util.ne0(dot.width)) {
						ascender = tt.max_ascender;
						descender = tt.min_descender;
					} else {
						leader_pattern = (short) 0;
						break;
					}
					break while_10_;
				default:
					throw new InternalException("leader-pattern="
							+ leader_pattern);
				}
			}
			setFlags(10);
		}

		public void dimensions(int i) {
			if (i != base) {
				len_opt = this.get(Attn.$leader_length_optimum)
						.length_or_ratio(i);
				len_min = this.get(Attn.$leader_length_minimum)
						.length_or_ratio(i);
				len_max = this.get(Attn.$leader_length_maximum)
						.length_or_ratio(i);
				if (len_opt < len_min)
					len_min = len_opt;
				if (len_max < len_opt)
					len_max = len_opt;
				width = length = len_opt;
				if (leader_pattern == 2 || leader_pattern == 3) {
					patwidth = this.get(Attn.$leader_pattern_width).length();
					if (patwidth < dot.width)
						patwidth = dot.width;
					int i_9_ = len_opt / patwidth * patwidth;
					int i_10_ = i_9_ + patwidth;
					if (i_9_ >= len_min && i_10_ <= len_max)
						width = length = i_10_ - len_opt < len_opt - i_9_ ? i_10_
								: i_9_;
					else if (i_9_ >= len_min)
						width = length = i_9_;
					else if (i_10_ <= len_max)
						width = length = i_10_;
				}
				span.tt = tt;
				span.ascender = ascender;
				span.descender = descender;
				base = i;
			}
		}

		static {
			leader_patterns.put(Attr.newWord("space"), new Short((short) 0));
			leader_patterns.put(Attr.newWord("rule"), new Short((short) 1));
			leader_patterns.put(Attr.newWord("dots"), new Short((short) 2));
			leader_patterns.put(Attr.newWord("use-content"), new Short(
					(short) 3));
		}
	}

	public static class PageLid extends PageId {
		public PageLid(Attributed attributed, Session session) {
			super(attributed, session);
			this.put(Attn.$ref_id, Attr.newWord("rx:last@"
					+ this.get(Attn.$ref_id).word()));
		}
	}

	public static class PageId extends Text {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public PageId(Attributed attributed, Session session) {
			super("0", attributed, session);
		}
	}

	public static class Text extends Inline implements Instruction {
		public char[] chars;

		public int nwsp;

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public void pass(InstructionDispatcher instructiondispatcher) {
			instructiondispatcher.process(this);
		}

		public Text(char[] cs, int i, Attributed attributed, Session session) {
			this(cs, i, attributed, session, null, null);
		}

		public Text(char[] cs, int i, Attributed attributed, Session session,
				Attributed attributed_11_, Item.TextTraits texttraits) {
			super(attributed);
			chars = new char[i];
			System.arraycopy(cs, 0, chars, 0, i);
			tt = (attributed == attributed_11_ ? texttraits
					: new Item.TextTraits(attributed, session));
			width = 0;
			double d = 0.0;
			double d_12_ = 0.0;
			nwsp = 0;
			Metric metric = tt.te.getMetric();
			for (int i_13_ = 0; i_13_ != i; i_13_++) {
				char c = chars[i_13_];
				CharMetrics charmetrics = metric.ucm(c);
				if (charmetrics == null && tt.tes.length() != 1) {
					Enumeration enumeration = tt.tes.elements();
					while (enumeration.hasMoreElements()) {
						FontRecord fontrecord;
						Metric metric_14_ = (fontrecord = (FontRecord) enumeration
								.nextElement()).getMetric();
						if ((charmetrics = metric_14_.ucm(c)) != null) {
							tt.multifont = true;
							if (tt.fss == Attr.auto) {
								metric = metric_14_;
								tt.te = fontrecord;
							}
							break;
						}
					}
				}
				if (charmetrics == null)
					charmetrics = metric.missingGlyph;
				if (charmetrics != null) {
					width += charmetrics.wx;
					if (d < (double) charmetrics.b[3])
						d = (double) charmetrics.b[3];
					if (d_12_ > (double) charmetrics.b[1])
						d_12_ = (double) charmetrics.b[1];
					if (c == ' ')
						nwsp++;
					else if (c == '\u00a0')
						tt.merge_white_space = false;
				}
			}
			width = (int) Math.round((double) width * tt.font_stretch
					* (double) tt.font_size / 1000.0);
			width += i * tt.csp + nwsp * tt.wsp;
			ascender = (int) Math.round(d / 1000.0 * (double) tt.font_size);
			descender = (int) Math
					.round(d_12_ / 1000.0 * (double) tt.font_size);
			if (ascender < tt.max_ascender)
				ascender = tt.max_ascender;
			if (descender > tt.min_descender)
				descender = tt.min_descender;
			if (nwsp != i)
				setFlags(10);
		}

		public Text(String string, Attributed attributed, Session session) {
			this(string.toCharArray(), string.length(), attributed, session);
		}

		public String toString() {
			return super.toString() + ":" + new String(chars);
		}
	}

	public abstract static class Inline extends Item {
		public Item.TextTraits tt = null;

		public int width;

		public int offset;

		public int ascender;

		public int descender;

		public Inline(Attributed attributed) {
			super(attributed);
		}

		public Inline() {
			this(AttList.empty);
		}
	}

	public static class Endl extends Item {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Endl() {
			setFlags(15);
		}
	}

	public static class Ends extends Item {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Ends() {
			setFlags(64);
		}
	}

	public static class Anchor extends Span {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Anchor(Attributed attributed, Session session) {
			super(attributed, session);
		}
	}

	public static class Span extends Inline {
		public Item.PropList proplist = new Item.PropList();

		public final Integer sno;

		public BPB.Inline bpb = null;

		public Hashtable markers = null;

		public boolean first_word = false;

		public int base = -2147483648;

		public int start_indent;

		public int end_indent;

		public Attr writing_mode;

		private int[] abs_margin = { 0, 0, 0, 0 };

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Span(Attributed attributed, Session session) {
			super(attributed);
			sno = new Integer(session.nextno());
			Area.fixBorderWidth(attributed);
			writing_mode = (attributed.containsKey(Attn.$direction) ? (Attr) (attributed
					.get(Attn.$direction) == Attr.ltr ? Attr.lr_tb : Attr.rl_tb)
					: attributed.get(Attn.$writing_mode));
			if (writing_mode != Attr.lr_tb && writing_mode != Attr.rl_tb) {
				session.warning("writing mode '" + writing_mode
						+ "' is not supported");
				writing_mode = Attr.lr_tb;
			}
			tt = new Item.TextTraits(attributed, session);
			offset = width = 0;
			ascender = tt.max_ascender;
			descender = tt.min_descender;
			setFlags(64);
		}

		public void dimensions(Item.Span span_15_, int i) {
			if (base != i) {
				int[] is = { 0, 0, 0, 0 };
				for (int i_16_ = 0; i_16_ != 4; i_16_++) {
					Attr attr = this.get(Item.margin_edge[i_16_]);
					Attr attr_17_ = this.get(Item.padding_edge[i_16_]);
					Attr attr_18_ = this.get(Item.edge_indent[i_16_]);
					if (attr instanceof Attr.Ratio) {
						Attr.Length length;
						this.put(Item.margin_edge[i_16_], length = (Attr
								.newLength((int) Math.round(attr.ratio()
										* (double) i))));
					} else if (attr instanceof Attr.Word) {
						Attr.Length length;
						this.put(Item.margin_edge[i_16_], length = Attr
								.newLength(0));
					}
					if (attr_17_ instanceof Attr.Ratio) {
						Attr.Length length;
						this.put(Item.padding_edge[i_16_], length = (Attr
								.newLength((int) Math.round(attr_17_.ratio()
										* (double) i))));
					}
					if (attr_18_ instanceof Attr.Ratio) {
						Attr.Length length;
						this.put(Item.edge_indent[i_16_], length = (Attr
								.newLength((int) Math.round(attr_18_.ratio()
										* (double) i))));
					}
					abs_margin[i_16_] = this.get(Item.margin_edge[i_16_])
							.length();
					if (span_15_ != null)
						abs_margin[i_16_] += (span_15_.abs_margin[i_16_]
								+ span_15_.get(Item.border_edge[i_16_])
										.length() + span_15_.get(
								Item.padding_edge[i_16_]).length());
					is[i_16_] = (abs_margin[i_16_]
							+ this.get(Item.border_edge[i_16_]).length() + this
							.get(Item.padding_edge[i_16_]).length());
				}
				start_indent = is[2];
				end_indent = is[3];
				base = i;
			}
		}

		public String toString() {
			return super.toString() + ": " + sno;
		}
	}

	public static class Endf extends Endb {
		Endf() {
			setFlags(8);
		}
	}

	public static class Endli extends Endb {
		Endli() {
			setFlags(8);
		}
	}

	public static class Endb extends Item {
		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		Endb() {
			setFlags(599);
		}
	}

	public static class Table extends Frame {
		public Flow header = new Flow();

		public Flow footer = new Flow();

		public TableContext context;

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Table(Attributed attributed, TableContext tablecontext) {
			super(attributed, tablecontext.session);
			context = tablecontext;
		}
	}

	public static class Frame extends Block {
		public static final short NORMAL = 0;

		public static final short ROTATED = 1;

		public static final short ABSOLUTE = 2;

		public static final short FIXED = 3;

		public short mode = 0;

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Frame(Attributed attributed, Session session) {
			super(attributed, session);
		}
	}

	public static class ListBlock extends Block {
		public ListContext context;

		Attr.Length alabel_end_indent;

		Attr.Length abody_indent;

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public ListBlock(Attributed attributed, ListContext listcontext) {
			super(attributed, listcontext.session);
			context = listcontext;
		}

		public void dimensions(Item.Block block, Item.ListBlock listblock_19_,
				Hashtable hashtable, int i) {
			super.dimensions(block, listblock_19_, hashtable, i);
			int i_20_ = (start_indent + this.get(
					Attn.$provisional_distance_between_starts).length_or_ratio(
					base));
			abody_indent = Attr.newLength(i_20_);
			alabel_end_indent = Attr.newLength(i
					- i_20_
					+ this.get(Attn.$provisional_label_separation)
							.length_or_ratio(base));
		}
	}

	public static class ListItem extends Block {
		public ListItem(Attributed attributed, Session session) {
			super(attributed, session);
			setFlags(8);
		}
	}

	public static class Block extends Item {
		public static final short WST_PRESERVE = 0;

		public static final short WST_IGNORE = 1;

		public static final short WST_IGNORE_BEFORE_LF = 2;

		public static final short WST_IGNORE_AFTER_LF = 4;

		public static final short WST_IGNORE_AROUND_LF = 6;

		public static final short LFT_PRESERVE = 0;

		public static final short LFT_IGNORE = 1;

		public static final short LFT_AS_SPACE = 2;

		public static final short LFT_AS_ZWSP = 3;

		public static final short FONT_HEIGHT = 0;

		public static final short MAX_HEIGHT = 1;

		public static final short LINE_HEIGHT = 2;

		public static final short START = 0;

		public static final short CENTER = 1;

		public static final short END = 2;

		public static final short JUSTIFY = 3;

		public static final short INSIDE = 4;

		public static final short OUTSIDE = 5;

		public static final short STRING = 6;

		private static final Hashtable wstCodes = new Hashtable();

		private static final Hashtable lftCodes = new Hashtable();

		private static final Hashtable alignCodes = new Hashtable();

		public static final short DISPLACE_AUTO = -1;

		public static final short DISPLACE_NONE = 0;

		public static final short DISPLACE_LINE = 1;

		public static final short DISPLACE_INDENT = 2;

		public static final short DISPLACE_BLOCK = 3;

		private static final Hashtable displaces;

		public final Integer bno;

		public Item.PropList proplist = new Item.PropList();

		public BPB.Block bpb = null;

		public Item.Level level = null;

		public Hashtable markers = null;

		public Item.TextTraits tt;

		public int base = -2147483648;

		public int content_width;

		public int start_indent;

		public int end_indent;

		public int before_indent;

		public int after_indent;

		public int text_indent;

		public int last_indent;

		public int gap;

		public short wst;

		public short lft;

		public boolean collapse;

		public boolean wrap;

		public boolean keep_together;

		public short lss;

		public boolean consider_shifts;

		public int orphans;

		public int widows;

		public Attr writing_mode;

		public short displace;

		public int[] abs_margin = { 0, 0, 0, 0 };

		public short align = -1;

		public short align_last = -1;

		public String align_string = "";

		public void pass(ItemDispatcher itemdispatcher) {
			itemdispatcher.process(this);
		}

		public Block(Attributed attributed, Session session) {
			super(attributed);
			bno = new Integer(session.nextno());
			Area.fixBorderWidth(attributed);
			wst = ((Short) wstCodes.get(attributed
					.get(Attn.$white_space_treatment))).shortValue();
			lft = ((Short) lftCodes.get(attributed
					.get(Attn.$linefeed_treatment))).shortValue();
			collapse = attributed.get(Attn.$white_space_collapse).bool();
			wrap = attributed.get(Attn.$wrap_option) == Attr.wrap;
			Attr attr = attributed.get(Attn.$line_stacking_strategy);
			lss = (attr == Attr.font_height ? (short) 0
					: attr == Attr.max_height ? (short) 1 : (short) 2);
			consider_shifts = (attributed
					.get(Attn.$line_height_shift_adjustment) == Attr.consider_shifts);
			orphans = attributed.get(Attn.$orphans).count();
			widows = attributed.get(Attn.$widows).count();
			keep_together = ((attributed.get(Attn.$keep_together_within_column) != Attr.auto) || (attributed
					.get(Attn.$keep_together_within_page) != Attr.auto));
			if (alignCodes.containsKey(attributed.get(Attn.$text_align)))
				align = ((Short) alignCodes.get(attributed
						.get(Attn.$text_align))).shortValue();
			else {
				align = (short) 6;
				align_string = attributed.get(Attn.$text_align).word();
			}
			align_last = (attributed.get(Attn.$text_align_last) == Attr.relative ? align == 3 ? (short) 0
					: align
					: ((Short) alignCodes.get(attributed
							.get(Attn.$text_align_last))).shortValue());
			writing_mode = attributed.get(Attn.$writing_mode);
			if (writing_mode != Attr.lr_tb && writing_mode != Attr.rl_tb) {
				session.warning("writing mode '" + writing_mode
						+ "' is not supported");
				writing_mode = Attr.lr_tb;
			}
			displace = ((Short) displaces.get(attributed
					.get(Attn.$intrusion_displace))).shortValue();
			if (displace == -1)
				displace = this instanceof Item.Frame ? (short) 3 : (short) 1;
			if (this.get(Attn.$reference_orientation).count() != 0
					&& displace != 0)
				displace = (short) 3;
			tt = new Item.TextTraits(attributed, session);
			setFlags(87);
		}

		public void dimensions(Item.Block block_21_, Item.ListBlock listblock,
				Hashtable hashtable, int i) {
			int i_22_ = i;
			if (!(block_21_ instanceof Item.Frame))
				i_22_ -= block_21_.start_indent + block_21_.end_indent;
			if (i_22_ != base) {
				this.resetall();
				boolean bool = false;
				if (this.get(Attn.$end_indent) == Attr.auto)
					this.put(Attn.$end_indent, Attr.newLength(0));
				else if (this.get(Attn.$end_indent) instanceof Attr.Word) {
					String string = this.get(Attn.$end_indent).word();
					Attr.Length length = Attr.zerolength;
					if (string.startsWith("label-end:")) {
						if (hashtable.containsKey(string))
							length = (Attr.Length) hashtable.get(string);
						else if (listblock != null)
							hashtable.put(string,
									(length = listblock.alabel_end_indent));
						bool = true;
					} else if (string.startsWith("body-start:")) {
						if (hashtable.containsKey(string))
							length = (Attr.Length) hashtable.get(string);
						else if (listblock != null)
							hashtable.put(string,
									length = listblock.abody_indent);
					}
					this.put(Attn.$end_indent, length);
				}
				if (this.get(Attn.$start_indent) == Attr.auto)
					this.put(Attn.$start_indent, Attr.newLength(0));
				else if (this.get(Attn.$start_indent) instanceof Attr.Word) {
					String string = this.get(Attn.$start_indent).word();
					Attr.Length length = Attr.zerolength;
					if (string.startsWith("body-start:")) {
						if (hashtable.containsKey(string))
							length = (Attr.Length) hashtable.get(string);
						else if (listblock != null)
							hashtable.put(string,
									length = listblock.abody_indent);
					} else if (string.startsWith("label-end:")) {
						if (hashtable.containsKey(string))
							length = (Attr.Length) hashtable.get(string);
						else if (listblock != null)
							hashtable.put(string,
									(length = listblock.alabel_end_indent));
						bool = true;
					}
					this.put(Attn.$start_indent, length);
				}
				content_width = (Attr.dim_range(this,
						Attn.$inline_progression_dimension_minimum,
						Attn.$inline_progression_dimension_optimum,
						Attn.$inline_progression_dimension_maximum, i_22_));
				gap = 0;
				int[] is = { 0, 0, 0, 0 };
				for (int i_23_ = 0; i_23_ != 4; i_23_++) {
					Attr attr = this.get(Item.margin_edge[i_23_]);
					Attr attr_24_ = this.get(Item.padding_edge[i_23_]);
					if (attr instanceof Attr.Ratio)
						this.put(Item.margin_edge[i_23_], (attr = (Attr
								.newLength((int) Math.round(attr.ratio()
										* (double) i_22_)))));
					if (attr_24_ instanceof Attr.Ratio) {
						Attr.Length length;
						this.put(Item.padding_edge[i_23_], (length = (Attr
								.newLength((int) Math.round(attr_24_.ratio()
										* (double) i_22_)))));
					}
					Attr attr_25_ = this.get(Item.edge_indent[i_23_]);
					if (attr_25_ instanceof Attr.Ratio)
						attr_25_ = Attr.newLength((int) Math.round(attr_25_
								.ratio()
								* (double) i));
					switch (i_23_) {
					case 0:
					case 1:
						abs_margin[i_23_] = attr.length();
						if (!(block_21_ instanceof Item.Frame))
							abs_margin[i_23_] += (block_21_.abs_margin[i_23_]
									+ block_21_.get(Item.border_edge[i_23_])
											.length() + block_21_.get(
									Item.padding_edge[i_23_]).length());
						is[i_23_] = (abs_margin[i_23_]
								+ this.get(Item.border_edge[i_23_]).length() + this
								.get(Item.padding_edge[i_23_]).length());
						break;
					case 2:
					case 3: {
						int i_26_;
						int i_27_;
						if (writing_mode == block_21_.writing_mode) {
							i_26_ = i_23_;
							int i_28_ = 2;
							i_27_ = 3;
						} else {
							i_26_ = i_23_ == 2 ? 3 : 2;
							int i_29_ = 3;
							i_27_ = 2;
						}
						if (!this.containsKey(Item.edge_indent[i_23_])
								&& !this.containsKey(Item.margin_edge[i_23_]))
							this
									.put(
											Item.edge_indent[i_23_],
											(attr_25_ = (block_21_ instanceof Item.Frame ? (Attr) Attr
													.newLength(0)
													: block_21_
															.get(Item.edge_indent[i_26_]))));
						if (this.containsKey(Item.edge_indent[i_23_])) {
							is[i_23_] = attr_25_.length();
							abs_margin[i_23_] = (is[i_23_]
									- this.get(Item.border_edge[i_23_])
											.length() - this.get(
									Item.padding_edge[i_23_]).length());
							this
									.put(
											Item.margin_edge[i_23_],
											(Attr
													.newLength(block_21_ instanceof Item.Frame ? abs_margin[i_23_]
															: (abs_margin[i_23_]
																	- block_21_.abs_margin[i_26_]
																	- block_21_
																			.get(
																					Item.border_edge[i_26_])
																			.length() - block_21_
																	.get(
																			Item.padding_edge[i_26_])
																	.length()))));
						} else {
							abs_margin[i_23_] = attr.length();
							if (!(block_21_ instanceof Item.Frame))
								abs_margin[i_23_] += (block_21_.abs_margin[i_26_]
										+ block_21_
												.get(Item.border_edge[i_26_])
												.length() + block_21_.get(
										Item.padding_edge[i_26_]).length());
							is[i_23_] = (abs_margin[i_23_]
									+ this.get(Item.border_edge[i_23_])
											.length() + this.get(
									Item.padding_edge[i_23_]).length());
						}
						if (i_23_ == 3 && content_width != -2147483648) {
							int i_30_ = is[3];
							is[3] = i_22_ - is[2] - content_width;
							if (!(block_21_ instanceof Item.Frame))
								is[3] += (block_21_.start_indent + block_21_.end_indent);
							gap += is[3] - i_30_;
							abs_margin[3] = (is[3]
									- this.get(Attn.$border_end_width_length)
											.length() - this.get(
									Attn.$padding_end_length).length());
							this
									.put(
											Attn.$margin_end,
											(Attr
													.newLength(block_21_ instanceof Item.Frame ? abs_margin[3]
															: (abs_margin[3]
																	- block_21_.abs_margin[i_27_]
																	- block_21_
																			.get(
																					Item.border_edge[i_27_])
																			.length() - block_21_
																	.get(
																			Item.padding_edge[i_27_])
																	.length()))));
						}
						break;
					}
					}
				}
				this.put(Attn.$start_indent, Attr
						.newLength(start_indent = is[2]));
				this.put(Attn.$end_indent, Attr.newLength(end_indent = is[3]));
				this.put(Attn.$before_indent, Attr
						.newLength(before_indent = is[0]));
				this.put(Attn.$after_indent, Attr
						.newLength(after_indent = is[1]));
				text_indent = this.get(Attn.$text_indent)
						.length_or_ratio(i_22_);
				last_indent = this.get(Attn.$last_line_end_indent)
						.length_or_ratio(i_22_);
				if (content_width == -2147483648 && bool) {
					content_width = i_22_ - start_indent - end_indent;
					gap += end_indent;
				}
				base = i_22_;
			} else {
				if (this.get(Attn.$end_indent) instanceof Attr.Word) {
					String string = this.get(Attn.$end_indent).word();
					if (string.startsWith("label-end:")
							&& !hashtable.containsKey(string))
						hashtable
								.put(
										string,
										this
												.get(Attn.$inline_progression_dimension_optimum));
				}
				if (this.get(Attn.$start_indent) instanceof Attr.Word) {
					String string = this.get(Attn.$start_indent).word();
					if (string.startsWith("body-start:")
							&& !hashtable.containsKey(string))
						hashtable.put(string, this.get(Attn.$start_indent));
				}
			}
		}

		public String toString() {
			return super.toString() + ":" + bno;
		}

		static {
			wstCodes.put(Attr.newWord("preserve"), new Short((short) 0));
			wstCodes.put(Attr.newWord("ignore"), new Short((short) 1));
			wstCodes.put(Attr.newWord("ignore-if-before-linefeed"), new Short(
					(short) 2));
			wstCodes.put(Attr.newWord("ignore-if-after-linefeed"), new Short(
					(short) 4));
			wstCodes.put(Attr.newWord("ignore-if-surrounding-linefeed"),
					new Short((short) 6));
			lftCodes.put(Attr.newWord("preserve"), new Short((short) 0));
			lftCodes.put(Attr.newWord("ignore"), new Short((short) 1));
			lftCodes.put(Attr.newWord("treat-as-space"), new Short((short) 2));
			lftCodes.put(Attr.newWord("treat-as-zero-width-space"), new Short(
					(short) 3));
			alignCodes.put(Attr.newWord("start"), new Short((short) 0));
			alignCodes.put(Attr.newWord("center"), new Short((short) 1));
			alignCodes.put(Attr.newWord("middle"), new Short((short) 1));
			alignCodes.put(Attr.newWord("end"), new Short((short) 2));
			alignCodes.put(Attr.newWord("justify"), new Short((short) 3));
			alignCodes.put(Attr.newWord("inside"), new Short((short) 4));
			alignCodes.put(Attr.newWord("outside"), new Short((short) 5));
			alignCodes.put(Attr.newWord("left"), new Short((short) 0));
			alignCodes.put(Attr.newWord("right"), new Short((short) 2));
			displaces = new Hashtable();
			displaces.put(Attr.newWord("auto"), new Short((short) -1));
			displaces.put(Attr.newWord("none"), new Short((short) 0));
			displaces.put(Attr.newWord("line"), new Short((short) 1));
			displaces.put(Attr.newWord("indent"), new Short((short) 2));
			displaces.put(Attr.newWord("block"), new Short((short) 3));
		}
	}

	public static class TextTraits {
		public Attr fss;

		public List tes;

		public FontRecord te;

		public boolean multifont = false;

		public boolean merge_white_space = true;

		public int font_size;

		public int max_ascender;

		public int min_descender;

		public int x_height;

		public int wsp;

		public int csp;

		public int line_height;

		public int lea;

		public int offset = -2147483648;

		public double font_stretch;

		public Attr font_weight;

		public Attr font_style;

		public Attr font_variant;

		public boolean varwsp;

		public boolean varcsp;

		public TextTraits(Attributed attributed, Session session) {
			fss = attributed.get(Attn.$font_selection_strategy);
			font_weight = attributed.get(Attn.$font_weight);
			font_style = attributed.get(Attn.$font_style);
			font_variant = attributed.get(Attn.$font_variant);
			String string = attributed.get(Attn.$language).word();
			Attr attr = attributed.get(Attn.$country);
			if (attr != Attr.none)
				string += "-" + attr.word();
			tes = session.findAllFonts(
					attributed.get(Attn.$font_family).word(), font_weight
							.count(), font_style.word(), font_variant.word(),
					string);
			Enumeration enumeration = tes.elements();
			while (enumeration.hasMoreElements()) {
				FontRecord fontrecord = (FontRecord) enumeration.nextElement();
				if (fontrecord.getMetric().isCIDFont) {
					merge_white_space = false;
					break;
				}
			}
			te = (FontRecord) tes.car();
			Metric metric = te.getMetric();
			font_size = attributed.get(Attn.$font_size).length();
			Attr attr_31_ = attributed.get(Attn.$font_size_adjust);
			if (attr_31_ instanceof Attr.Ratio)
				font_size = (int) Math.round((double) font_size
						* attr_31_.ratio() * 1000.0 / (double) metric.xHeight);
			Attr attr_32_ = attributed.get(Attn.$text_altitude);
			Attr attr_33_ = attributed.get(Attn.$text_depth);
			max_ascender = (attr_32_ instanceof Attr.Word ? Math
					.round(metric.ascender / 1000.0F * (float) font_size)
					: attr_32_.length());
			min_descender = (attr_33_ instanceof Attr.Word ? Math
					.round(metric.descender / 1000.0F * (float) font_size)
					: attr_33_.length());
			x_height = Math.round(metric.xHeight / 1000.0F * (float) font_size);
			varwsp = ((attributed.get(Attn.$word_spacing_minimum) instanceof Attr.Word) && (attributed
					.get(Attn.$word_spacing_maximum) instanceof Attr.Word));
			wsp = Attr.length_range(attributed, Attn.$word_spacing_minimum,
					Attn.$word_spacing_optimum, Attn.$word_spacing_maximum, 0);
			varcsp = ((attributed.get(Attn.$letter_spacing_minimum) instanceof Attr.Word) && (attributed
					.get(Attn.$letter_spacing_maximum) instanceof Attr.Word));
			csp = Attr.length_range(attributed, Attn.$letter_spacing_minimum,
					Attn.$letter_spacing_optimum, Attn.$letter_spacing_maximum,
					0);
			font_stretch = attributed.get(Attn.$font_stretch).ratio();
			int i = max_ascender - min_descender;
			line_height = Attr.length_range(attributed,
					Attn.$line_height_minimum, Attn.$line_height_optimum,
					Attn.$line_height_maximum, font_size);
			lea = (line_height - i) / 2;
		}
	}

	public static final class PropList {
		private Hashtable tab = new Hashtable();

		public List put(short i, AttList attlist) {
			Elem.Descriptor descriptor = Elem.getDescriptor(i);
			List list = (List) tab.get(descriptor);
			if (list == null)
				tab.put(descriptor, list = new List());
			list.snoc(attlist);
			return list;
		}

		public List get(short i) {
			return (List) tab.get(Elem.getDescriptor(i));
		}

		public AttList get0(short i) {
			List list = get(i);
			return list != null ? (AttList) list.car() : null;
		}

		public boolean contains(short i) {
			return get(i) != null;
		}
	}

	public abstract void pass(ItemDispatcher itemdispatcher);

	public final boolean breaksPhrase() {
		return (status & 0x1) != 0;
	}

	public final boolean breaksInlineSpace() {
		return (status & 0x2) != 0;
	}

	public final boolean breaksLine() {
		return (status & 0x4) != 0;
	}

	public final boolean breaksDisplaySpace() {
		return (status & 0x8) != 0;
	}

	public final boolean breaksBlock() {
		return (status & 0x10) != 0;
	}

	public final boolean isAtomic() {
		return (status & 0x40) == 0;
	}

	public final boolean keepsFilled() {
		return (status & 0x100) != 0;
	}

	public final boolean ss() {
		return (status & 0x200) != 0;
	}

	public final boolean isHyphenated() {
		return (status & 0x20) != 0;
	}

	public final boolean isSkipped() {
		return (status & 0x4000) != 0;
	}

	public final Item setFlags(int i) {
		status |= i;
		return this;
	}

	public final Item clearFlags(int i) {
		status &= i ^ 0xffffffff;
		return this;
	}

	public final void skip() {
		setFlags(16384);
	}

	public Item(Attributed attributed) {
		super(attributed);
	}

	public Item() {
		this(AttList.empty);
	}

	public void dimensions(int i) {
		/* empty */
	}

	public String toString() {
		return this.getClass().getName();
	}

	public final Item cow() {
		if ((status & 0x2000) != 0) {
			Item item_34_ = (Item) this.clone();
			item_34_.clearFlags(8192);
			return item_34_;
		}
		setFlags(8192);
		return this;
	}

	static {
		cxts.put(Attr.auto, new Short((short) 0));
		cxts.put(Attr.newWord("column"), new Short((short) 1));
		cxts.put(Attr.newWord("page"), new Short((short) 2));
		cxts.put(Attr.newWord("odd-page"), new Short((short) 3));
		cxts.put(Attr.newWord("even-page"), new Short((short) 4));
		margin_edge = new Attn[] { Attn.$margin_before, Attn.$margin_after,
				Attn.$margin_start, Attn.$margin_end };
		padding_edge = new Attn[] { Attn.$padding_before_length,
				Attn.$padding_after_length, Attn.$padding_start_length,
				Attn.$padding_end_length };
		border_edge = new Attn[] { Attn.$border_before_width_length,
				Attn.$border_after_width_length,
				Attn.$border_start_width_length, Attn.$border_end_width_length };
		edge_indent = new Attn[] { Attn.$before_indent, Attn.$after_indent,
				Attn.$start_indent, Attn.$end_indent };
		endb = new Endb();
		endli = new Endli();
		endf = new Endf();
		ends = new Ends();
		endl = new Endl();
		footnotesPending = new FootnotesPending();
		footnotesAppended = new FootnotesAppended();
		fltab = new Hashtable();
		fltab.put(Attr.newWord("none"), new Short((short) 0));
		fltab.put(Attr.newWord("before"), new Short((short) 1));
		fltab.put(Attr.newWord("start"), new Short((short) 2));
		fltab.put(Attr.newWord("end"), new Short((short) 3));
		fltab.put(Attr.newWord("left"), new Short((short) 2));
		fltab.put(Attr.newWord("right"), new Short((short) 3));
		fltab.put(Attr.newWord("inside"), new Short((short) 4));
		fltab.put(Attr.newWord("outside"), new Short((short) 5));
		cltab = new Hashtable();
		cltab.put(Attr.newWord("none"), new Short((short) 0));
		cltab.put(Attr.newWord("start"), new Short((short) 1));
		cltab.put(Attr.newWord("end"), new Short((short) 2));
		cltab.put(Attr.newWord("left"), new Short((short) 1));
		cltab.put(Attr.newWord("right"), new Short((short) 2));
		cltab.put(Attr.newWord("both"), new Short((short) 3));
		cltab.put(Attr.newWord("inside"), new Short((short) 4));
		cltab.put(Attr.newWord("outside"), new Short((short) 8));
		endw = new Endw();
		empty = new Empty();
		filled = new Filled();
		eofl = new Eofl();
	}
}