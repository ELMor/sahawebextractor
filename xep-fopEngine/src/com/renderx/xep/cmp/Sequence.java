/*
 * Sequence - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.xep.lib.NumStr;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Attributed;

public class Sequence extends Xattr {
	public SM master;

	public Flow title = null;

	public Flow flow = null;

	public Attr flowname = null;

	public Hashtable sctab = new Hashtable();

	public List pilist = new List();

	public List lids = new List();

	public NumStr numstr = null;

	public Sequence(Attributed attributed) {
		super(attributed);
		numstr = new NumStr(attributed.get(Attn.$format).word(), attributed
				.get(Attn.$grouping_size).count(), attributed.get(
				Attn.$grouping_separator).word());
	}
}