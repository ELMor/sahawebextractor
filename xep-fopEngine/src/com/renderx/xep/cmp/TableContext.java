/*
 * TableContext - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import java.util.Enumeration;

import com.renderx.util.Array;
import com.renderx.util.Matrix;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Attributed;

public class TableContext extends Xattr {
	private static final short CELLS_ONLY = 0;

	private static final short HAS_ROWS = 1;

	public static final short HEADER = 0;

	public static final short BODY = 1;

	public static final short FOOTER = 2;

	public int table_width;

	final Session session;

	private Array cols = new Array();

	private Array rows = new Array();

	private Array rowgroups = new Array();

	private Matrix cells = new Matrix();

	private int irgr = 0;

	private int irow = -1;

	private int icol = 0;

	private short mode = -1;

	private int max_row_length = 1;

	private boolean new_row_started = true;

	private boolean ends_row = false;

	private int gap;

	private boolean auto;

	private boolean width_auto;

	private class Cell {
		int[] O;

		int width_min;

		int width_max;

		int extrawidth;

		int n_columns_spanned;

		int n_rows_spanned;

		boolean keep_together;

		Item.Frame outer;

		Item.Frame inner;

		Flow flow_prolog;

		Flow flow_body;

		Flow flow_epilog;

		private String idtail;

		Cell() {
			O = new int[4];
			n_columns_spanned = -1;
			n_rows_spanned = -1;
			keep_together = false;
			outer = null;
			inner = null;
			flow_prolog = null;
			flow_body = null;
			flow_epilog = null;
			O[0] = O[1] = O[2] = O[3] = -1;
		}

		Cell(Attributed attributed, int i, int i_0_) {
			O = new int[4];
			n_columns_spanned = -1;
			n_rows_spanned = -1;
			keep_together = false;
			outer = null;
			inner = null;
			flow_prolog = null;
			flow_body = null;
			flow_epilog = null;
			O[0] = i;
			O[1] = i_0_;
			idtail = ":" + O[0] + ":" + O[1];
			n_columns_spanned = attributed.get(Attn.$number_columns_spanned)
					.count();
			n_rows_spanned = attributed.get(Attn.$number_rows_spanned).count();
			if (n_columns_spanned < 1)
				n_columns_spanned = 1;
			if (n_rows_spanned < 1)
				n_rows_spanned = 1;
			O[2] = O[0] + n_rows_spanned - 1;
			O[3] = O[1] + n_columns_spanned - 1;
			keep_together = (attributed.get(Attn.$keep_together_within_page) != Attr.auto || (attributed
					.get(Attn.$keep_together_within_column) != Attr.auto));
		}

		String id(String string, int i) {
			return string + ":" + i + idtail;
		}
	}

	private class Rowgroup extends Xattr {
		int[] bounds = { -1, -1 };

		Flow flow_prolog;

		Flow flow_epilog;

		short part;

		Rowgroup(Attributed attributed, Flow flow, Flow flow_1_, int i,
				short i_2_) {
			super(attributed);
			flow_prolog = flow;
			flow_epilog = flow_1_;
			bounds[0] = i;
			part = i_2_;
		}
	}

	private class Row extends Xattr {
		public Item.Level level;

		int row_height;

		boolean auto;

		boolean keep_together;

		Row(Attributed attributed) {
			super(attributed);
			level = new Item.Level();
			row_height = 0;
			keep_together = false;
			row_height = attributed.get(
					Attn.$block_progression_dimension_minimum).length_or_ratio(
					0);
			if (row_height == 0)
				row_height = attributed.get(
						Attn.$block_progression_dimension_optimum)
						.length_or_ratio(0);
			auto = row_height == 0;
			keep_together = ((attributed.get(Attn.$keep_together_within_column) != Attr.auto) || (attributed
					.get(Attn.$keep_together_within_page) != Attr.auto));
		}

		Row() {
			super(AttList.empty);
			level = new Item.Level();
			row_height = 0;
			keep_together = false;
		}
	}

	private class Column extends Xattr {
		int number;

		int width;

		int width_fixed;

		double width_proportional;

		boolean auto;

		int width_min;

		int width_max;

		double vol;

		private Column(Attributed attributed) {
			super(attributed);
			number = attributed.get(Attn.$column_number).count();
			width_proportional = ((auto = (attributed
					.get(Attn.$column_width_proportional) == Attr.auto)) ? 1.0
					: attributed.get(Attn.$column_width_proportional).ratio());
			width_fixed = ((attributed.get(Attn.$column_width_fixed) instanceof Attr.Length) ? (width_fixed = attributed
					.get(Attn.$column_width_fixed).length())
					: 0);
		}

		Column() {
			this(AttList.empty);
		}
	}

	TableContext(Attributed attributed, Session session) {
		super(attributed);
		this.session = session;
		table_width = Attr.dim_range(attributed,
				Attn.$inline_progression_dimension_minimum,
				Attn.$inline_progression_dimension_optimum,
				Attn.$inline_progression_dimension_maximum, 0);
		width_auto = table_width == -2147483648;
		if (width_auto) {
			auto = true;
			table_width = 0;
		} else
			auto = attributed.get(Attn.$table_layout) == Attr.auto;
		gap = attributed.get(
				Attn.$border_separation_inline_progression_direction).length();
	}

	void column(Attributed attributed) {
		Column column = new Column(attributed);
		cols.put(column.number - 1, column);
		if (column.number > max_row_length)
			max_row_length = column.number;
	}

	void startRowgroup(short i, Attributed attributed, Flow flow, Flow flow_3_) {
		irow = cells.length();
		rowgroups.put(irgr, new Rowgroup(attributed, flow, flow_3_, irow, i));
		mode = (short) 0;
	}

	void endRowgroup(short i) {
		if (mode == 0)
			newRow();
		((Rowgroup) rowgroups.get(irgr)).bounds[1] = cells.length();
		irgr++;
	}

	private void newRow() {
		if (!new_row_started) {
			Array array = (Array) cells.get(irow);
			int i = array == null ? 0 : array.length();
			if (i > max_row_length)
				max_row_length = i;
			irow++;
			new_row_started = true;
		}
	}

	void startRow(Attributed attributed) {
		mode = (short) 1;
		Row row = new Row(attributed);
		rows.put(irow, row);
		new_row_started = false;
	}

	void endRow() {
		newRow();
	}

	void startCell(Attributed attributed, Item.Frame frame,
			Item.Frame frame_4_, Flow flow, Flow flow_5_, Flow flow_6_) {
		if (mode == 1
				&& (attributed.get(Attn.$ends_row).bool() || attributed.get(
						Attn.$starts_row).bool()))
			session.warning("(starts|ends)-row inside a row");
		if (attributed.get(Attn.$starts_row).bool())
			newRow();
		if (attributed.get(Attn.$ends_row).bool())
			ends_row = true;
		new_row_started = false;
		icol = attributed.get(Attn.$column_number).count() - 1;
		if (cells.get(irow, icol) != null)
			session.error("table cells overlap, skipping a cell");
		else {
			Cell cell = new Cell(attributed, irow, icol);
			cell.outer = frame;
			cell.inner = frame_4_;
			cell.flow_prolog = flow;
			cell.flow_body = flow_5_;
			cell.flow_epilog = flow_6_;
			int i = irow + cell.n_rows_spanned;
			int i_7_ = icol + cell.n_columns_spanned;
			if (cells.get(i - 1, i_7_ - 1) != null)
				session.error("table cells overlap, skipping a cell");
			else {
				for (int i_8_ = irow; i_8_ != i; i_8_++) {
					for (int i_9_ = icol; i_9_ != i_7_; i_9_++) {
						if (cells.get(i_8_, i_9_) != null)
							session.warning("table cells overlap");
						cells.put(i_8_, i_9_, cell);
					}
				}
				cell.width_min = frame_4_.get(
						Attn.$inline_progression_dimension_minimum)
						.length_or_ratio(0);
				cell.width_max = frame_4_.get(
						Attn.$inline_progression_dimension_maximum)
						.length_or_ratio(2147483647);
				cell.extrawidth = (frame_4_
						.get(Attn.$border_start_width_length)
						.length_or_ratio(0)
						+ frame_4_.get(Attn.$border_end_width_length)
								.length_or_ratio(0)
						+ frame_4_.get(Attn.$padding_start_length)
								.length_or_ratio(0)
						+ frame_4_.get(Attn.$padding_end_length)
								.length_or_ratio(0) + gap);
				frame_4_.a.remove(Attn.$inline_progression_dimension_minimum);
				frame_4_.a.remove(Attn.$inline_progression_dimension_optimum);
				frame_4_.a.remove(Attn.$inline_progression_dimension_maximum);
			}
		}
	}

	void endCell() {
		if (ends_row) {
			newRow();
			ends_row = false;
		}
	}

	private void fix() {
		for (int i = 0; i != cells.length(); i++) {
			if (rows.get(i) == null)
				rows.put(i, new Row());
		}
		for (int i = 0; i != max_row_length; i++) {
			if (cols.get(i) == null)
				cols.put(i, new Column());
		}
		for (int i = 0; i != cells.length(); i++) {
			Array array = (Array) cells.get(i);
			if (array == null)
				cells.put(i, array = new Array(max_row_length));
			for (int i_10_ = 0; i_10_ != max_row_length; i_10_++) {
				if (array.get(i_10_) == null)
					array.put(i_10_, new Cell());
			}
		}
	}

	public void pin(int i, boolean bool) {
		if (bool || i != table_width) {
			if (auto) {
				for (int i_11_ = 0; i_11_ != cols.length(); i_11_++) {
					Column column = (Column) cols.get(i_11_);
					if (column.auto) {
						column.width_fixed = 0;
						column.width_proportional = column.vol;
					}
				}
				boolean bool_12_;
				do {
					fit(i);
					bool_12_ = false;
					for (int i_13_ = 0; i_13_ != cols.length(); i_13_++) {
						Column column = (Column) cols.get(i_13_);
						if (column.auto) {
							if (column.width < column.width_min) {
								column.width_fixed = column.width_min;
								column.width_proportional = 0.0;
								bool_12_ = true;
							} else if (width_auto
									&& column.width > column.width_max) {
								column.width_fixed = column.width_max;
								column.width_proportional = 0.0;
								bool_12_ = true;
							}
						}
					}
				} while (bool_12_);
			} else
				fit(i);
		}
	}

	private void fit(int i) {
		int i_14_ = i;
		int i_15_ = 0;
		double d = 0.0;
		for (int i_16_ = 0; i_16_ != cols.length(); i_16_++) {
			Column column = (Column) cols.get(i_16_);
			if (column.get(Attn.$column_width_fixed) instanceof Attr.Ratio)
				column.width_fixed = (int) Math.round((double) i
						* column.get(Attn.$column_width_fixed).ratio());
			i_14_ -= column.width_fixed;
			i_15_ += column.width_fixed;
			d += column.width_proportional;
		}
		if (width_auto)
			table_width = d == 0.0 ? i_15_ : i;
		else if (d == 0.0) {
			if (i_15_ > i)
				table_width = i_15_;
			else {
				for (int i_17_ = 0; i_17_ != cols.length(); i_17_++) {
					Column column = (Column) cols.get(i_17_);
					if (!column.auto && column.width_proportional == 0.0)
						d += column.width_proportional = 1.0;
				}
				table_width = d == 0.0 ? i_15_ : i;
			}
		} else
			table_width = i;
		int i_18_ = i_14_;
		Column column = null;
		for (int i_19_ = 0; i_19_ != cols.length(); i_19_++) {
			Column column_20_ = (Column) cols.get(i_19_);
			column_20_.width = column_20_.width_fixed;
			if (column_20_.width_proportional != 0.0) {
				int i_21_ = (int) Math.round((double) i_14_
						* (column_20_.width_proportional / d));
				column_20_.width += i_21_;
				i_18_ -= i_21_;
				column = column_20_;
			}
		}
		if (column != null)
			column.width += i_18_;
		for (int i_22_ = 0; i_22_ != rowgroups.length(); i_22_++) {
			Rowgroup rowgroup = (Rowgroup) rowgroups.get(i_22_);
			if (rowgroup != null) {
				for (int i_23_ = rowgroup.bounds[0]; i_23_ != rowgroup.bounds[1]; i_23_++) {
					Array array = (Array) cells.get(i_23_);
					for (int i_24_ = 0; i_24_ != array.length(); i_24_++) {
						Cell cell = (Cell) array.get(i_24_);
						if (cell.O[0] == i_23_ && cell.O[1] == i_24_) {
							int i_25_ = gap / 2;
							int i_26_ = ((Column) cols.get(cell.O[1])).width
									- gap;
							if (cell.O[1] != 0) {
								for (int i_27_ = 0; i_27_ != cell.O[1]; i_27_++)
									i_25_ += ((Column) cols.get(i_27_)).width;
							}
							if (cell.n_columns_spanned != 1) {
								for (int i_28_ = cell.O[1] + 1; (i_28_ != cell.O[1]
										+ cell.n_columns_spanned); i_28_++)
									i_26_ += ((Column) cols.get(i_28_)).width;
							}
							cell.outer.a.put(Attn.$start_indent, Attr
									.newLength(i_25_));
							cell.outer.a.put(
									Attn.$inline_progression_dimension_optimum,
									Attr.newLength(i_26_));
						}
					}
				}
			}
		}
	}

	private void measure() {
		if (auto) {
			for (int i = 0; i != cells.length(); i++) {
				Array array = (Array) cells.get(i);
				for (int i_29_ = 0; i_29_ != array.length(); i_29_++) {
					Cell cell = (Cell) array.get(i_29_);
					if (cell.O[0] == i && cell.O[1] == i_29_) {
						int i_30_ = 0;
						int i_31_ = 0;
						double d = 0.0;
						int i_32_ = 0;
						int i_33_ = 0;
						Enumeration enumeration = cell.flow_body.elements();
						while (enumeration.hasMoreElements()) {
							Item item = (Item) enumeration.nextElement();
							if (item.breaksPhrase()) {
								if (i_32_ > i_30_)
									i_30_ = i_32_;
								i_32_ = 0;
							}
							if (item.breaksLine()) {
								if (i_33_ > i_31_)
									i_31_ = i_33_;
								i_33_ = 0;
							}
							if (item instanceof Item.Inline) {
								Item.Inline inline = (Item.Inline) item;
								inline.dimensions(0);
								i_32_ += inline.width;
								i_33_ += inline.width;
								d += (double) ((inline.ascender - inline.descender) * inline.width);
							} else if (item instanceof Item.Frame) {
								int i_34_;
								if (item instanceof Item.Table)
									i_34_ = (((Item.Table) item).context.table_width);
								else
									i_34_ = (Attr
											.length_range(
													item,
													(Attn.$inline_progression_dimension_minimum),
													(Attn.$inline_progression_dimension_optimum),
													(Attn.$inline_progression_dimension_maximum),
													0));
								if (i_30_ < i_34_)
									i_30_ = i_34_;
								if (i_31_ < i_34_)
									i_31_ = i_34_;
							}
						}
						if (i_32_ > i_30_)
							i_30_ = i_32_;
						if (i_33_ > i_31_)
							i_31_ = i_33_;
						if (i_30_ < cell.width_min)
							i_30_ = cell.width_min;
						if (i_31_ > cell.width_max)
							i_31_ = cell.width_max;
						if (i_31_ < i_30_)
							i_31_ = i_30_;
						i_30_ += cell.extrawidth;
						i_31_ += cell.extrawidth;
						d /= (double) (cell.n_rows_spanned * cell.n_columns_spanned);
						i_30_ /= cell.n_columns_spanned;
						i_31_ /= cell.n_columns_spanned;
						for (int i_35_ = cell.O[1]; i_35_ <= cell.O[3]; i_35_++) {
							Column column = (Column) cols.get(i_35_);
							if (column.width_min < i_30_)
								column.width_min = i_30_;
							if (column.width_max < i_31_)
								column.width_max = i_31_;
							column.vol += d;
						}
					}
				}
			}
			double d = 0.0;
			for (int i = 0; i != cols.length(); i++)
				d += ((Column) cols.get(i)).vol;
			if (d != 0.0 && cols.length() != 0) {
				d /= (double) cols.length();
				for (int i = 0; i != cols.length(); i++) {
					Column column = (Column) cols.get(i);
					column.vol = Math.sqrt(column.vol / d);
				}
			}
		}
	}

	void gen(Item.Table table, Flow flow) {
		int i = table.bno.intValue();
		fix();
		flow.append(Item.empty);
		Flow flow_36_ = new Flow();
		for (int i_37_ = 0; i_37_ != rowgroups.length(); i_37_++) {
			Rowgroup rowgroup = (Rowgroup) rowgroups.get(i_37_);
			Flow flow_38_;
			switch (rowgroup.part) {
			case 0:
				flow_38_ = table.header;
				break;
			case 2:
				flow_38_ = table.footer;
				break;
			case 1:
				flow_38_ = flow_36_;
				break;
			default:
				throw new InternalException("invalid table part: "
						+ rowgroup.part);
			}
			if (rowgroup.flow_prolog != null)
				flow_38_.append(rowgroup.flow_prolog);
			for (int i_39_ = rowgroup.bounds[0]; i_39_ != rowgroup.bounds[1]; i_39_++) {
				Row row = (Row) rows.get(i_39_);
				boolean bool = row.keep_together;
				boolean bool_40_ = false;
				if (row.get(Attn.$break_before) != Attr.auto)
					flow_38_.append(new Item.Break(row, "before"));
				flow_38_.append(Item.empty);
				Array array = (Array) cells.get(i_39_);
				for (int i_41_ = array.length() - 1; i_41_ != -1; i_41_--) {
					Cell cell = (Cell) array.get(i_41_);
					if (cell.O[0] == i_39_ && cell.O[1] == i_41_)
						flow_38_
								.append(new Item.Fork(cell.id("table-cell", i)));
					if (cell.keep_together) {
						bool = true;
						if (cell.O[2] != i_39_)
							bool_40_ = true;
					}
				}
				if (!row.auto)
					flow_38_.append(new Item.Washer.Display(row.row_height));
				if (!bool)
					flow_38_.append(Item.filled);
				flow_38_.append(new Item.Jump("table-row-epilog:" + i + ":"
						+ i_39_));
				if ((row.get(Attn.$keep_with_previous_within_column) != Attr.auto)
						|| (row.get(Attn.$keep_with_previous_within_page) != Attr.auto))
					flow_38_.append(new Item.Keep((Attributed) row));
				for (int i_42_ = 0; i_42_ != array.length(); i_42_++) {
					Cell cell = (Cell) array.get(i_42_);
					if (cell.O[0] == i_39_ && cell.O[1] == i_42_) {
						String string = cell.id("table-cell", i);
						flow_38_.append(new Item.Label(string)).append(
								cell.flow_prolog).append(cell.flow_body)
								.append(Item.filled).append(
										new Item.Softsync(string)).append(
										new Item.Softsync(string)).append(
										cell.flow_epilog).append(
										new Item.Sync(string))
								.append(Item.eofl);
						for (int i_43_ = i_39_; i_43_ != i_39_
								+ cell.n_rows_spanned; i_43_++)
							((Row) rows.get(i_43_)).level.register(cell.inner);
					}
				}
				if (row.get(Attn.$keep_with_next_within_column) != Attr.auto
						|| row.get(Attn.$keep_with_next_within_page) != Attr.auto)
					flow_38_.append(new Item.Keep((Attributed) row));
				flow_38_.append(new Item.Label("table-row-epilog:" + i + ":"
						+ i_39_));
				for (int i_44_ = 0; i_44_ != array.length(); i_44_++) {
					Cell cell = (Cell) array.get(i_44_);
					if (cell.O[2] == i_39_ && cell.O[3] == i_44_)
						flow_38_.append(new Item.Softsync(cell.id("table-cell",
								i)));
				}
				for (int i_45_ = 0; i_45_ != array.length(); i_45_++) {
					Cell cell = (Cell) array.get(i_45_);
					if (cell.O[2] == i_39_ && cell.O[3] == i_45_)
						flow_38_.append(new Item.Softsync(cell.id("table-cell",
								i)));
				}
				if (!bool_40_)
					flow_38_.append(Item.filled);
				for (int i_46_ = 0; i_46_ != array.length(); i_46_++) {
					Cell cell = (Cell) array.get(i_46_);
					if (cell.O[2] == i_39_ && cell.O[3] == i_46_)
						flow_38_
								.append(new Item.Sync(cell.id("table-cell", i)));
				}
				flow_38_.append(row.level);
				if (row.get(Attn.$break_after) != Attr.auto)
					flow_38_.append(new Item.Break(row, "after"));
			}
			if (rowgroup.flow_epilog != null)
				flow_38_.append(rowgroup.flow_epilog);
		}
		flow.append(flow_36_);
		Object object = null;
		flow.append(table.footer);
		table.footer = new Flow();
		measure();
		pin(table_width, true);
	}
}