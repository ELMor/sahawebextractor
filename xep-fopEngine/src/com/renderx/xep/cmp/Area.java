/*
 * Area - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Attributed;

public class Area extends Xattr {
	public int[][] r;

	public Area(int[] is, Attributed attributed) {
		super(attributed);
		r = new int[3][];
		init(is);
	}

	public Area(Attributed attributed) {
		super(attributed);
		r = new int[3][];
	}

	public void init(int[] is) {
		int i = a.get(Attn.$padding_start_length)
				.length_or_ratio(is[2] - is[0]);
		int i_0_ = a.get(Attn.$padding_after_length).length_or_ratio(
				is[3] - is[1]);
		int i_1_ = a.get(Attn.$padding_end_length).length_or_ratio(
				is[2] - is[0]);
		int i_2_ = a.get(Attn.$padding_before_length).length_or_ratio(
				is[3] - is[1]);
		r[0] = (int[]) is.clone();
		r[1] = (Rectangle.shrink(r[0], (new int[] {
				a.get(Attn.$border_start_width_length).length(),
				a.get(Attn.$border_after_width_length).length(),
				a.get(Attn.$border_end_width_length).length(),
				a.get(Attn.$border_before_width_length).length() })));
		r[2] = Rectangle.shrink(r[1], new int[] { i, i_0_, i_1_, i_2_ });
	}

	static void fixBorderWidth(Attributed attributed) {
		if (attributed.get(Attn.$border_before_style) != Attr.none
				&& !attributed.containsKey(Attn.$border_before_width_length))
			attributed.put(Attn.$border_before_width_length, Attr
					.newLength(3000));
		if (attributed.get(Attn.$border_after_style) != Attr.none
				&& !attributed.containsKey(Attn.$border_after_width_length))
			attributed.put(Attn.$border_after_width_length, Attr
					.newLength(3000));
		if (attributed.get(Attn.$border_start_style) != Attr.none
				&& !attributed.containsKey(Attn.$border_start_width_length))
			attributed.put(Attn.$border_start_width_length, Attr
					.newLength(3000));
		if (attributed.get(Attn.$border_end_style) != Attr.none
				&& !attributed.containsKey(Attn.$border_end_width_length))
			attributed.put(Attn.$border_end_width_length, Attr.newLength(3000));
	}
}