/*
 * NoMasterException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.CompilerException;

public class NoMasterException extends CompilerException {
	public Attr name;

	public NoMasterException() {
		/* empty */
	}

	public NoMasterException(Attr attr) {
		super("No sequence master with name '" + attr.word() + "'");
		name = attr;
	}
}