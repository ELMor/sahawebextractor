/*
 * RootHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.util.List;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.CompilerException;
import com.renderx.xep.pre.ElementHandler;
import com.renderx.xep.pre.PI;
import com.renderx.xep.pre.PIHandler;

public class RootHandler implements ElementHandler, PIHandler {
	private ElementHandler subhandler = null;

	private final Session session;

	private final FO fo;

	private Sequence seq;

	public RootHandler(FO fo, Session session) {
		this.fo = fo;
		this.session = session;
	}

	public void startElement(short i, AttList attlist) throws CompilerException {
		if (subhandler == null) {
			switch (i) {
			case 1:
				if (attlist.containsKey(Attn.$initial_destination))
					fo.initial_destination = attlist
							.get(Attn.$initial_destination);
				if (attlist.containsKey(Attn.$id))
					fo.id = attlist.get(Attn.$id);
				break;
			case 106:
				session.openState("meta-info");
				break;
			case 105:
				fo.metainfo.put(attlist.get(Attn.$name).word(), attlist.get(
						Attn.$value).word());
				break;
			case 2:
				session.openState("declarations");
				subhandler = new NullHandler();
				break;
			case 104:
				session.openState("outline");
				subhandler = new OutlineHandler(fo.outline, session);
				break;
			case 4:
				session.openState("masters");
				subhandler = new LMSHandler(fo.masters, session);
				break;
			case 16:
				session.openState("sequence");
				seq = new Sequence(attlist);
				subhandler = new SequenceHandler(seq, fo.masters, session);
				break;
			default:
				throw new CompilerException("invalid element id: " + i);
			}
		} else
			subhandler.startElement(i, attlist);
	}

	public void endElement(short i) throws CompilerException {
		if (subhandler == null) {
			switch (i) {
			case 1:
				resolveForcePageCount();
				if (fo.id != null)
					((Sequence) fo.sequences.last()).lids.snoc(fo.id);
				break;
			case 106:
				session.closeState("meta-info");
				break;
			case 105:
				break;
			default:
				throw new CompilerException("invalid element id: " + i);
			}
		} else {
			switch (i) {
			case 104:
				subhandler = null;
				session.closeState("outline");
				break;
			case 4:
				subhandler = null;
				session.closeState("masters");
				break;
			case 2:
				subhandler = null;
				session.closeState("declarations");
				break;
			case 16:
				fo.sequences.snoc(seq);
				if (seq.containsKey(Attn.$id))
					seq.lids.snoc(seq.get(Attn.$id));
				seq = null;
				subhandler = null;
				session.closeState("sequence");
				break;
			default:
				subhandler.endElement(i);
			}
		}
	}

	public void processingInstruction(String string, String string_0_)
			throws CompilerException {
		if (subhandler == null)
			fo.pilist.append(new PI(string, string_0_));
		else if (subhandler instanceof PIHandler)
			((PIHandler) subhandler).processingInstruction(string, string_0_);
	}

	private void resolveForcePageCount() {
		if (!fo.sequences.isEmpty()) {
			Sequence sequence = (Sequence) fo.sequences.car();
			List list = fo.sequences.cdr();
			for (;;) {
				if (sequence.get(Attn.$force_page_count) == Attr.auto) {
					if (list.isEmpty()) {
						sequence.put(Attn.$force_page_count, Attr
								.newWord("no-force"));
						break;
					}
					Sequence sequence_1_ = (Sequence) list.car();
					if (sequence_1_.get(Attn.$initial_page_number) instanceof Attr.Count)
						sequence
								.put(
										Attn.$force_page_count,
										Attr
												.newWord(sequence_1_
														.get(
																Attn.$initial_page_number)
														.count() % 2 == 1 ? "end-on-even"
														: "end-on-odd"));
					else if (sequence_1_.get(Attn.$initial_page_number) == Attr
							.newWord("auto-even"))
						sequence.put(Attn.$force_page_count, Attr
								.newWord("end-on-odd"));
					else if (sequence_1_.get(Attn.$initial_page_number) == Attr
							.newWord("auto-odd"))
						sequence.put(Attn.$force_page_count, Attr
								.newWord("end-on-even"));
					else
						sequence.put(Attn.$force_page_count, Attr
								.newWord("no-force"));
					sequence = sequence_1_;
					list = list.cdr();
				} else {
					if (list.isEmpty())
						break;
					sequence = (Sequence) list.car();
					list = list.cdr();
				}
			}
		}
	}
}