/*
 * ListContext - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.util.Array;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attributed;

public class ListContext extends Xattr {
	Array rows = new Array();

	Array cells = new Array();

	private int irow = -1;

	private int icol = 0;

	final Session session;

	class Cell extends Xattr {
		int[] O = new int[2];

		Flow flow;

		private String idtail;

		Cell(Attributed attributed, int i, int i_0_) {
			super(attributed);
			O[0] = irow;
			O[1] = icol;
			idtail = ":" + O[0] + ":" + O[1];
		}

		String id(String string, int i) {
			return string + ":" + i + idtail;
		}
	}

	class Row extends Xattr {
		Flow flow_prolog;

		Flow flow_epilog;

		Row(Attributed attributed, Flow flow, Flow flow_1_) {
			super(attributed);
			flow_prolog = flow;
			flow_epilog = flow_1_;
		}

		Row() {
			this(AttList.empty, null, null);
		}
	}

	ListContext(Attributed attributed, Session session) {
		super(attributed);
		this.session = session;
		icol = 0;
		irow = 0;
	}

	void startRow(Attributed attributed, Flow flow, Flow flow_2_) {
		Row row = new Row(attributed, flow, flow_2_);
		rows.put(irow, row);
	}

	void endRow() {
		/* empty */
	}

	private void newRow() {
		cells.put(irow, new Cell[2]);
	}

	void startCell(Attributed attributed, Flow flow) {
		if (icol == 0) {
			newRow();
			if (rows.get(irow) == null)
				rows.put(irow, new Row());
		}
		Cell cell = new Cell(attributed, irow, icol);
		cell.flow = flow;
		((Cell[]) cells.get(irow))[icol] = cell;
	}

	void endCell() {
		if (icol == 0)
			icol++;
		else {
			icol = 0;
			irow++;
		}
	}

	void gen(Item.ListBlock listblock, Flow flow) {
		int i = listblock.bno.intValue();
		for (int i_3_ = 0; i_3_ != this.cells.length(); i_3_++) {
			Row row = (Row) rows.get(i_3_);
			flow.append(Item.empty);
			if (row.flow_prolog != null)
				flow.append(row.flow_prolog);
			Cell[] cells = (Cell[]) this.cells.get(i_3_);
			for (int i_4_ = 0; i_4_ != 2; i_4_++) {
				Cell cell = cells[i_4_];
				flow.append(new Item.Fork(cell.id("list-cell", i)));
			}
			flow.append(Item.filled);
			flow.append(new Item.Jump("list-row-epilog:" + i + ":" + i_3_));
			for (int i_5_ = 0; i_5_ != 2; i_5_++) {
				Cell cell = cells[i_5_];
				String string = cell.id("list-cell", i);
				flow.append(new Item.Label(string)).append(cell.flow).append(
						Item.filled).append(new Item.Sync(string)).append(
						Item.eofl);
			}
			flow.append(new Item.Label("list-row-epilog:" + i + ":" + i_3_));
			for (int i_6_ = 0; i_6_ != 2; i_6_++) {
				Cell cell = cells[i_6_];
				flow.append(new Item.Sync(cell.id("list-cell", i)));
			}
			if (row.flow_epilog != null)
				flow.append(row.flow_epilog);
		}
	}
}