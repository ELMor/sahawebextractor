/*
 * FO - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.xep.pre.Attr;

public class FO {
	public Hashtable metainfo = new Hashtable();

	public List pilist = new List();

	public Hashtable masters = new Hashtable();

	public Attr id = null;

	public Attr initial_destination = null;

	public Outline outline = new Outline();

	public List sequences = new List();

	public FO() {
		metainfo.put("author", "Unknown");
		metainfo.put("title", "Untitled");
	}
}