/*
 * ItemDispatcher - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

public interface ItemDispatcher {
	public void process(Item.Block block);

	public void process(Item.Frame frame);

	public void process(Item.ListBlock listblock);

	public void process(Item.Table table);

	public void process(Item.Endb endb);

	public void process(Item.Span span);

	public void process(Item.Anchor anchor);

	public void process(Item.Ends ends);

	public void process(Item.Endl endl);

	public void process(Item.Text text);

	public void process(Item.PageId pageid);

	public void process(Item.Leader leader);

	public void process(Item.Image image);

	public void process(Item.Space.Display display);

	public void process(Item.Space.Inline inline);

	public void process(Item.Washer.Display display);

	public void process(Item.Washer.Inline inline);

	public void process(Item.Exception exception);

	public void process(Item.Fork fork);

	public void process(Item.Jump jump);

	public void process(Item.Label label);

	public void process(Item.Sync sync);

	public void process(Item.Softsync softsync);

	public void process(Item.Empty empty);

	public void process(Item.Filled filled);

	public void process(Item.Eofl eofl);

	public void process(Item.Level level);

	public void process(Item.Bookmark bookmark);

	public void process(Item.Wide wide);

	public void process(Item.Endw endw);

	public void process(Item.FootnotesAppended footnotesappended);
}