/*
 * ValidatorCore - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep;

import java.io.IOException;
import java.util.StringTokenizer;

import com.jclark.xsl.sax.XSLProcessor;
import com.jclark.xsl.sax.XSLProcessorImpl;
import com.renderx.sax.InputSource;
import com.renderx.sax.XMLReaderAdapter;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.URLSpec;

import org.xml.sax.ErrorHandler;
import org.xml.sax.HandlerBase;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

class ValidatorCore {
	private XSLProcessor xt = new XSLProcessorImpl();

	private MessageHandler handler = new MessageHandler();

	protected static class DefaultValidationEventHandler implements
			ValidationEventHandler {
		public void warning(String string, String string_0_, String string_1_,
				int i, int i_2_) {
			formatMessage("warning", string, string_0_, string_1_, i, i_2_);
		}

		public void error(String string, String string_3_, String string_4_,
				int i, int i_5_) {
			formatMessage("error", string, string_3_, string_4_, i, i_5_);
		}

		private void formatMessage(String string, String string_6_,
				String string_7_, String string_8_, int i, int i_9_) {
			System.err.print(string_8_);
			if (i != -1) {
				System.err.print(": line " + i);
				if (i_9_ != -1)
					System.err.print(", column " + i_9_);
			}
			System.err.println();
			int i_10_ = 0;
			StringTokenizer stringtokenizer = new StringTokenizer(string_6_);
			while (stringtokenizer.hasMoreTokens()) {
				String string_11_ = stringtokenizer.nextToken();
				if (i_10_ + string_11_.length() > 71) {
					System.err.println();
					System.err.print(string_11_);
					i_10_ = string_11_.length();
				} else {
					if (i_10_ > 0) {
						System.err.print(" ");
						i_10_++;
					}
					System.err.print(string_11_);
					i_10_ += string_11_.length();
				}
			}
			System.err.println();
			System.err.println();
		}
	}

	protected static class MessageHandler implements ErrorHandler {
		private int warningCount = 0;

		private int errorCount = 0;

		private ValidationEventHandler handler = null;

		public void reset() {
			errorCount = warningCount = 0;
		}

		public int getErrorCount() {
			return errorCount;
		}

		public int getWarningCount() {
			return warningCount;
		}

		protected void dispatchMessage(SAXParseException saxparseexception)
				throws SAXParseException {
			String string = saxparseexception.getMessage();
			if (string != null && string.startsWith("[warning]")) {
				warningCount++;
				handler.warning(string.substring("[warning]".length()).trim(),
						saxparseexception.getPublicId(), saxparseexception
								.getSystemId(), saxparseexception
								.getLineNumber(), saxparseexception
								.getColumnNumber());
			} else if (string != null && string.startsWith("[error]")) {
				errorCount++;
				handler.error(string.substring("[error]".length()).trim(),
						saxparseexception.getPublicId(), saxparseexception
								.getSystemId(), saxparseexception
								.getLineNumber(), saxparseexception
								.getColumnNumber());
			} else {
				errorCount++;
				handler.error(string, saxparseexception.getPublicId(),
						saxparseexception.getSystemId(), saxparseexception
								.getLineNumber(), saxparseexception
								.getColumnNumber());
			}
		}

		public void warning(SAXParseException saxparseexception)
				throws SAXParseException {
			dispatchMessage(saxparseexception);
		}

		public void error(SAXParseException saxparseexception)
				throws SAXParseException {
			dispatchMessage(saxparseexception);
		}

		public void fatalError(SAXParseException saxparseexception)
				throws SAXParseException {
			dispatchMessage(saxparseexception);
		}

		public void setValidationEventHandler(
				ValidationEventHandler validationeventhandler) {
			handler = validationeventhandler;
		}
	}

	public ValidatorCore(URLSpec urlspec, int i) throws SAXException,
			IOException {
		setXMLReader(XMLReaderFactory.createXMLReader());
		xt.setDocumentHandler(new HandlerBase());
		xt.setErrorHandler(handler);
		handler.setValidationEventHandler(new DefaultValidationEventHandler());
		xt.loadStylesheet(new InputSource(urlspec));
		xt.setParameter("strictness", "" + i);
	}

	public int validate(org.xml.sax.InputSource inputsource, XMLReader xmlreader)
			throws SAXException, IOException {
		handler.reset();
		setXMLReader(xmlreader);
		xt.parse(inputsource);
		return handler.getErrorCount();
	}

	public int getErrorCount() {
		return handler.getErrorCount();
	}

	public int getWarningCount() {
		return handler.getWarningCount();
	}

	private void setXMLReader(XMLReader xmlreader) throws SAXException {
		try {
			xmlreader.setFeature(
					"http://xml.org/sax/features/namespace-prefixes", true);
			xt.setParser(new org.xml.sax.helpers.XMLReaderAdapter(xmlreader));
		} catch (SAXException saxexception) {
			xt.setParser(new XMLReaderAdapter(xmlreader));
		}
	}

	public void setValidationEventHandler(
			ValidationEventHandler validationeventhandler) {
		handler.setValidationEventHandler(validationeventhandler);
	}
}