/*
 * FOTransformer - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep;

import java.io.IOException;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.URIResolver;
import javax.xml.transform.sax.SAXResult;

import com.renderx.sax.JAXPTransformerAdapter;
import com.renderx.util.Hashtable;
import com.renderx.util.User;
import com.renderx.xep.lib.DefaultLogger;
import com.renderx.xep.lib.Logger;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class FOTransformer {
	private static final String DEFAULT_RESOLVER_CLASS_NAME = "none";

	private final Transformer transformer;

	private final Formatter engine;

	private Logger storedLogger = null;

	private URIResolver storedResolver = null;

	private ErrorListener storedListener = null;

	private final Hashtable storedParameters = new Hashtable();

	private static class LoggerListener implements ErrorListener {
		private final Logger logger;

		LoggerListener(Logger logger) {
			this.logger = logger;
		}

		public void warning(TransformerException transformerexception)
				throws TransformerException {
			if (logger != Logger.NULL_LOGGER)
				logger.warning(transformerexception.getMessageAndLocation());
		}

		public void error(TransformerException transformerexception)
				throws TransformerException {
			if (logger != Logger.NULL_LOGGER)
				logger.error(transformerexception.getMessageAndLocation());
		}

		public void fatalError(TransformerException transformerexception)
				throws TransformerException {
			if (logger != Logger.NULL_LOGGER)
				logger.exception(transformerexception.getMessageAndLocation(),
						transformerexception);
		}
	}

	public FOTransformer(Formatter formatter) {
		this(formatter, new DefaultLogger());
	}

	public FOTransformer(Formatter formatter, Logger logger) {
		this((Transformer) null, formatter, logger);
	}

	public FOTransformer(Source source, Formatter formatter)
			throws TransformerConfigurationException {
		this(source, formatter, new DefaultLogger());
	}

	public FOTransformer(Source source, Formatter formatter, Logger logger)
			throws TransformerConfigurationException {
		this(createTransformer(source, new LoggerListener(logger)), formatter,
				logger);
	}

	public FOTransformer(Transformer transformer, Formatter formatter) {
		this(transformer, formatter, new DefaultLogger());
	}

	public FOTransformer(Transformer transformer, Formatter formatter,
			Logger logger) {
		this.transformer = transformer;
		engine = formatter;
		storedLogger = logger;
	}

	private static Transformer createTransformer(Source source,
			ErrorListener errorlistener)
			throws TransformerConfigurationException {
		TransformerFactory transformerfactory = createTransformerFactory(errorlistener);
		Transformer transformer = transformerfactory.newTransformer(source);
		transformer.setErrorListener(errorlistener);
		URIResolver uriresolver = transformerfactory.getURIResolver();
		if (uriresolver != null)
			transformer.setURIResolver(uriresolver);
		return transformer;
	}

	private static TransformerFactory createTransformerFactory(
			ErrorListener errorlistener)
			throws TransformerConfigurationException {
		Object object = null;
		TransformerFactory transformerfactory;
		try {
			transformerfactory = TransformerFactory.newInstance();
		} catch (TransformerFactoryConfigurationError transformerfactoryconfigurationerror) {
			throw new TransformerConfigurationException(
					transformerfactoryconfigurationerror.toString());
		}
		if (!transformerfactory
				.getFeature("http://javax.xml.transform.sax.SAXResult/feature"))
			throw new TransformerConfigurationException(
					"TransformerFactory does not support SAX result: cannot create FOTransformer");
		transformerfactory.setErrorListener(errorlistener);
		String string = User.getProperty("com.renderx.jaxp.uriresolver");
		if (string == null)
			string = "none";
		if (!"none".equals(string)) {
			try {
				transformerfactory.setURIResolver((URIResolver) Class.forName(
						string).newInstance());
			} catch (Exception exception) {
				throw new TransformerConfigurationException(
						"Cannot create URI resolver " + string + ": "
								+ exception.toString());
			}
		}
		return transformerfactory;
	}

	private XMLReader getReader() throws SAXException {
		if (transformer == null)
			return null;
		return new JAXPTransformerAdapter(transformer);
	}

	public void transform(Source source, FOTarget fotarget)
			throws TransformerException, SAXException, IOException {
		if (transformer == null)
			engine.render(source, fotarget, storedLogger);
		else {
			ContentHandler contenthandler = engine.createContentHandler(source
					.getSystemId(), fotarget, storedLogger);
			if (storedListener == null)
				transformer.setErrorListener(new ErrorListener() {
					public void warning(
							TransformerException transformerexception)
							throws TransformerException {
						storedLogger.warning(transformerexception
								.getMessageAndLocation());
					}

					public void error(TransformerException transformerexception)
							throws TransformerException {
						storedLogger.error(transformerexception
								.getMessageAndLocation());
					}

					public void fatalError(
							TransformerException transformerexception)
							throws TransformerException {
						storedLogger.exception(transformerexception
								.getMessageAndLocation(), transformerexception);
					}
				});
			transformer.transform(source, new SAXResult(contenthandler));
		}
	}

	public void clearParameters() {
		if (transformer == null)
			storedParameters.clear();
		else
			transformer.clearParameters();
	}

	public Object getParameter(String string) {
		return (transformer == null ? storedParameters.get(string)
				: transformer.getParameter(string));
	}

	public void setParameter(String string, Object object) {
		if (transformer == null)
			storedParameters.put(string, object);
		else
			transformer.setParameter(string, object);
	}

	public URIResolver getURIResolver() {
		return (transformer == null ? storedResolver : transformer
				.getURIResolver());
	}

	public ErrorListener getErrorListener() {
		return (transformer == null ? storedListener : transformer
				.getErrorListener());
	}

	public void setURIResolver(URIResolver uriresolver) {
		if (transformer == null)
			storedResolver = uriresolver;
		else
			transformer.setURIResolver(uriresolver);
	}

	public void setErrorListener(ErrorListener errorlistener) {
		if (transformer == null)
			storedListener = errorlistener;
		else
			transformer.setErrorListener(errorlistener);
	}

	public Logger getLogger() {
		return storedLogger;
	}

	public void setLogger(Logger logger) {
		storedLogger = logger;
		if (transformer != null)
			transformer.setErrorListener(new LoggerListener(logger));
	}
}