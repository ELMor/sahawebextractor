/*
 * XSLDriver - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;

import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.Args;
import com.renderx.util.Hashtable;
import com.renderx.util.URLSpec;
import com.renderx.xep.gen.backends.EmbeddedImageFilter;
import com.renderx.xep.lib.DefaultLogger;
import com.renderx.xep.lib.LicenseException;

import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class XSLDriver {
	public static final short EC_OK = 0;

	public static final short EC_ERROR = 1;

	private static final Hashtable formats = new Hashtable();

	private static final short FO = 1;

	private static final short XSLT = 2;

	private static final short XEP = 3;

	private static final short SWITCHES = 0;

	private static final short INPUT = 1;

	private static final short OUTPUT = 2;

	private static boolean sysexit;

	private static class ExitException extends Exception {
		ExitException(int i) {
			super("" + i);
		}
	}

	static class QuietLogger extends DefaultLogger {
		public void openDocument() {
			/* empty */
		}

		public void closeDocument() {
			/* empty */
		}

		public void event(String string, String string_0_) {
			/* empty */
		}

		public void openState(String string) {
			/* empty */
		}

		public void closeState(String string) {
			/* empty */
		}

		public void info(String string) {
			/* empty */
		}
	}

	private XSLDriver() {
		/* empty */
	}

	private static void expt(String string, Exception exception) {
		System.err.print(string + ": ");
		System.err.println(exception.toString());
	}

	private static void exit(int i) throws ExitException {
		if (sysexit)
			System.exit(i);
		else if (i != 0)
			throw new ExitException(i);
	}

	public static void main(String[] strings) throws Exception {
		String string = null;
		String xsl = null;
		String string_2_ = null;
		String[] strings_3_ = null;
		DefaultLogger defaultlogger = new DefaultLogger();
		Hashtable hashtable = new Hashtable();
		Properties properties = new Properties();
		Args args = new Args(strings);
		int i = 0;
		try {
			short i_4_ = 0;
			for (int i_5_ = 0; i_5_ != args.tokens.length; i_5_++) {
				switch (i_4_) {
				case 0:
					switch (args.tokens[i_5_].typ) {
					case 2: {
						String string_6_ = args.tokens[i_5_].s0;
						String string_7_ = args.tokens[i_5_].s1;
						if (string_6_.startsWith("com.renderx.xep."))
							string_6_ = string_6_.substring("com.renderx.xep."
									.length());
						properties.put(string_6_, string_7_);
						break;
					}
					case 0: {
						String string_8_ = args.tokens[i_5_].s0.toLowerCase();
						String string_9_ = args.tokens[i_5_].s1;
						if (string_8_.equals("help") || string_8_.equals("h")) {
							printUsage();
							exit(0);
						} else if (string_8_.equals("version")) {
							System.err.println("XEP 4.1 build 20050109");
							if (args.tokens.length == 1)
								exit(0);
						} else if (string_8_.equals("quiet")
								|| string_8_.equals("q"))
							defaultlogger = new QuietLogger();
						else if (string_8_.equals("valid"))
							properties.put("VALIDATE", "false");
						else if (string_8_.equals("hosted"))
							sysexit = false;
						else {
							i_5_--;
							i_4_++;
						}
						break;
					}
					case 3:
						i_5_--;
						i_4_++;
						break;
					}
					break;
				case 1:
					switch (args.tokens[i_5_].typ) {
					case 2:
						System.err
								.println("error: unexpected command line argument #"
										+ (i_5_ + 1)
										+ "(input parameter): '"
										+ strings[i_5_] + "'");
						break;
					case 0: {
						String string_10_ = args.tokens[i_5_].s0.toLowerCase();
						String string_11_ = args.tokens[i_5_].s1;
						if (i == 0) {
							if (string_10_.equals("xml")) {
								i = 2;
								string = args.tokens[++i_5_].s0;
							} else if (string_10_.equals("fo")) {
								i = 1;
								string = args.tokens[++i_5_].s0;
								i_4_++;
							} else if (i == 0 && string_10_.equals("xep")) {
								i = 3;
								string = args.tokens[++i_5_].s0;
								i_4_++;
							} else {
								i_5_--;
								i_4_++;
							}
						} else if (i == 2) {
							if (string_10_.equals("xsl"))
								xsl = args.tokens[++i_5_].s0;
							else if (string_10_.equals("param")) {
								String string_12_ = args.tokens[++i_5_].s0;
								int i_13_ = string_12_.indexOf('=');
								if (i_13_ == -1)
									hashtable.put(string_12_, "");
								else
									hashtable.put(string_12_
											.substring(0, i_13_), string_12_
											.substring(i_13_ + 1));
							} else {
								i_5_--;
								i_4_++;
							}
						}
						break;
					}
					case 3:
						if (i == 0) {
							i = 2;
							string = args.tokens[i_5_].s0;
						} else {
							i_5_--;
							i_4_++;
						}
						break;
					}
					break;
				case 2:
					switch (args.tokens[i_5_].typ) {
					case 2:
						System.err
								.println("error: unexpected command line argument #"
										+ (i_5_ + 1)
										+ " (output parameter): '"
										+ strings[i_5_] + "'");
						break;
					case 0: {
						String string_14_ = args.tokens[i_5_].s0.toLowerCase();
						String string_15_ = args.tokens[i_5_].s1;
						if (!string_14_.equals("out")) {
							if (string_14_.equals("format"))
								args.tokens[i_5_ + 1].typ = (short) 0;
							else if (formats.containsKey(string_14_)) {
								if (strings_3_ != null)
									System.err
											.println("error: duplicate output format");
								strings_3_ = (String[]) formats.get(string_14_);
							} else
								System.err
										.println("error: unexpected command line argument #"
												+ (i_5_ + 1)
												+ " (output parameter): '"
												+ strings[i_5_] + "'");
						}
						break;
					}
					case 3:
						if (string_2_ != null) {
							String string_16_ = args.tokens[i_5_].s0;
							int i_17_ = string_16_.indexOf('=');
							if (i_17_ != -1 && i == 2) {
								System.err
										.println("warning: trailing transformation parameters are deprecated, use -param instead");
								hashtable.put(string_16_.substring(0, i_17_),
										string_16_.substring(i_17_ + 1));
							} else
								System.err.println("error: duplicate output");
						} else
							string_2_ = args.tokens[i_5_].s0;
						break;
					default:
						break;
					}
					break;
				}
			}
		} catch (ArrayIndexOutOfBoundsException arrayindexoutofboundsexception) {
			System.err.println("error: too few command line arguments");
			exit(1);
		}
		if (i == 0)
			i = 2;
		if (string == null)
			string = "-";
		if (strings_3_ == null)
			strings_3_ = (String[]) formats.get("pdf");
		if (strings_3_[1] == null) {
			System.err.println("Generation of " + strings_3_[0]
					+ " not supported");
			enumerateFormats();
			exit(1);
		}
		if (string_2_ == null) {
			if (string.equals("-"))
				string_2_ = "-";
			else {
				int i_18_ = string.lastIndexOf(".");
				string_2_ = ((i_18_ == -1 ? string : string.substring(0, i_18_))
						+ "." + strings_3_[1]);
			}
		}
		FormatterImpl formatterimpl = null;
		Properties properties_19_ = new Properties();
		Properties properties_20_ = new Properties();
		Enumeration enumeration = properties.propertyNames();
		while (enumeration.hasMoreElements()) {
			String string_21_ = (String) enumeration.nextElement();
			String string_22_ = (String) properties.get(string_21_);
			if (string_21_.startsWith(strings_3_[2])) {
				string_21_ = string_21_.substring(strings_3_[2].length());
				properties_20_.put(string_21_, string_22_);
			} else if (string_21_.indexOf('.') == -1)
				properties_19_.put(string_21_, string_22_);
		}
		try {
			formatterimpl = new FormatterImpl(properties_19_, defaultlogger);
		} catch (LicenseException licenseexception) {
			System.err.println("License check failed: ");
			System.err.println(licenseexception.getMessage());
			System.err
					.println("If you keep getting this error after the activation procedure, ");
			System.err
					.println("please contact support@renderx.com attaching your license.xml file. ");
			exit(1);
		} catch (Exception exception) {
			expt("Formatter initialization failed", exception);
			exit(1);
		}
		try {
			File file = null;
			java.io.FilterOutputStream filteroutputstream = null;
			try {
				if (string_2_.equals("-")) {
					file = new File("stdout") {
						public boolean delete() {
							return false;
						}
					};
					filteroutputstream = System.out;
				} else {
					file = new File(string_2_);
					filteroutputstream = new BufferedOutputStream(
							new FileOutputStream(file));
				}
			} catch (IOException ioexception) {
				System.err.println("error: cannot open file " + string_2_
						+ " for writing: " + ioexception.getMessage());
				exit(1);
			} catch (Exception exception) {
				expt("error: problem accessing file '" + string_2_ + "'",
						exception);
				exit(1);
			}
			FOTarget fotarget = new FOTarget(filteroutputstream, strings_3_[1],
					properties_20_);
			try {
				try {
					InputSource inputsource;
					if (string.equals("-")) {
						inputsource = new InputSource(System.in);
						inputsource.setSystemId(new URL("file:stdin")
								.toExternalForm());
					} else
						inputsource = new InputSource(makeSystemId(string));
					SAXSource saxsource = new SAXSource(XMLReaderFactory.createXMLReader(), inputsource);
					switch (i) {
					case 1:
						formatterimpl.render(saxsource, fotarget);
						break;
					case 3: {
						XMLReader xmlreader = XMLReaderFactory
								.createXMLReader();
						ContentHandler contenthandler = formatterimpl
								.createGenerator(fotarget, defaultlogger);
						xmlreader.setContentHandler(new EmbeddedImageFilter(
								contenthandler, defaultlogger));
						defaultlogger.openDocument();
						if (inputsource.getSystemId() != null)
							defaultlogger.event("system-id", inputsource
									.getSystemId());
						xmlreader.parse(inputsource);
						defaultlogger.closeDocument();
						break;
					}
					case 2: {
						Source source = null;
						if (xsl != null)
							source = (new SAXSource(XMLReaderFactory
									.createXMLReader(), new InputSource(
									makeSystemId(xsl))));
						else if (!string.equals("-")) {
							try {
								SAXSource saxsource_24_ = new SAXSource(
										inputsource);
								TransformerFactory transformerfactory = TransformerFactory
										.newInstance();
								source = (transformerfactory
										.getAssociatedStylesheet(saxsource_24_,
												null, null, null));
								if (source != null)
									defaultlogger
											.info("Stylesheet specified by PI: "
													+ source.getSystemId());
							} catch (TransformerConfigurationException transformerconfigurationexception) {
								/* empty */
							}
						}
						Object object = null;
						FOTransformer fotransformer;
						if (source != null)
							fotransformer = new FOTransformer(source,
									formatterimpl);
						else
							fotransformer = new FOTransformer(formatterimpl);
						fotransformer.setLogger(defaultlogger);
						Enumeration enumeration_25_ = hashtable.keys();
						while (enumeration_25_.hasMoreElements()) {
							String string_26_ = (String) enumeration_25_
									.nextElement();
							String string_27_ = (String) hashtable
									.get(string_26_);
							fotransformer.setParameter(string_26_, string_27_);
						}
						fotransformer.transform(saxsource, fotarget);
						break;
					}
					}
				} finally {
					try {
						filteroutputstream.close();
					} catch (Exception exception) {
						/* empty */
					}
				}
			} catch (IOException ioexception) {
				System.err.println("\nI/O error: " + ioexception.toString());
				file.delete();
				exit(1);
			} catch (SAXException saxexception) {
				System.err.println("\nParse error: "
						+ saxexception.getMessage());
				file.delete();
				exit(1);
			} catch (Exception exception) {
				expt("error: formatting failed", exception);
				file.delete();
				exit(1);
			}
		} finally {
			formatterimpl.cleanup();
		}
		exit(0);
	}

	private static String makeSystemId(String string) throws ExitException {
		try {
			return new URLSpec(string).toString();
		} catch (Exception exception) {
			expt("error parsing location", exception);
			exit(1);
			return null;
		}
	}

	private static void complain(String string) {
		System.err.println(string);
		printUsage();
	}

	private static void printUsage() {
		System.err.println("XEP 4.1 build 20050109");
		System.err
				.println("java com.renderx.xep.XSLDriver \n       {<option>}\n       {-quiet | -version | -valid | -hosted | -help}\n       ( [-xml] <infile> [-xsl <stylesheet>] {-param <name=value>}\n         | -fo <infile> \n         | -xep <infile> )\n       [[-<output format>] <outfile>]");
		enumerateFormats();
	}

	private static void enumerateFormats() {
		System.err.print("Available output formats:");
		Enumeration enumeration = formats.keys();
		String string = " ";
		while (enumeration.hasMoreElements()) {
			String string_28_ = (String) enumeration.nextElement();
			String[] strings = (String[]) formats.get(string_28_);
			if (strings[1] != null) {
				System.err.print(string + string_28_ + " (" + strings[0] + ")");
				string = ", ";
			}
		}
		System.err.println(".");
	}

	static {
		formats.put("pdf", new String[] { "PDF", "pdf", "H4PDF." });
		formats.put("ps", new String[] { "Postscript", "ps", "H4PS." });
		formats.put("xep", new String[] { "XEP", "xep", "H4XEP." });
		formats.put("at", formats.get("xep"));
		formats.put("pcl", new String[] { "PCL", null, null });
		formats.put("mif", new String[] { "MIF", null, null });
		formats.put("txt", new String[] { "Plain Text", null, null });
		formats.put("awt", new String[] { "AWT Preview", null, null });
		sysexit = true;
	}
}