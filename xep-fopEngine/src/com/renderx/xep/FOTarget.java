/*
 * FOTarget - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep;

import java.io.OutputStream;
import java.util.Properties;

public class FOTarget {
	final OutputStream stream;

	final String format;

	final Properties options;

	public FOTarget(OutputStream outputstream, String string) {
		this(outputstream, string, new Properties());
	}

	public FOTarget(OutputStream outputstream, String string,
			Properties properties) {
		stream = outputstream;
		format = string;
		options = properties;
	}
}