/*
 * AttList - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.renderx.xep.lib.InternalException;

public class AttList implements Attributed, Cloneable {
	public static final AttList empty = new AttList();

	private static final double Kgrow = 1.2;

	private Object[] atts;

	private int len;

	private class Enumerator implements Enumeration {
		private int i = 0;

		public final Object nextElement() {
			if (i == len)
				throw new NoSuchElementException(
						"attempt to access element past the end of a list");
			Object object = atts[i];
			i += 2;
			return object;
		}

		public final boolean hasMoreElements() {
			return i != len;
		}
	}

	private AttList(AttList attlist_0_) {
		atts = new Object[attlist_0_.atts.length];
		len = attlist_0_.len;
		System.arraycopy(attlist_0_.atts, 0, atts, 0, len);
	}

	public AttList() {
		len = 0;
		atts = new Object[2];
	}

	public int size() {
		return len / 2;
	}

	public final boolean containsKey(Attn attn) {
		for (int i = 0; i != len; i += 2) {
			if (atts[i] == attn)
				return true;
		}
		return false;
	}

	public Attr remove(Attn attn) {
		Object object = null;
		for (int i = 0; i != len; i += 2) {
			if (atts[i] == attn) {
				object = atts[i + 1];
				len -= 2;
				System.arraycopy(atts, i + 2, atts, i, len - i);
				break;
			}
		}
		return (Attr) object;
	}

	public static Attr getDefault(Attn attn) {
		if (attn.defaultValue == null)
			throw new InternalException("attribute '" + attn
					+ "' is required but missing");
		return attn.defaultValue;
	}

	public final Attr get(Attn attn) {
		Object object = null;
		for (int i = 0; i != len; i += 2) {
			if (atts[i] == attn) {
				object = atts[i + 1];
				break;
			}
		}
		if (object instanceof Expr)
			object = null;
		if (object == null)
			object = getDefault(attn);
		return (Attr) object;
	}

	public final void put(Attn attn, Attr attr) {
		for (int i = 0; i != len; i += 2) {
			if (atts[i] == attn) {
				atts[i + 1] = attr;
				return;
			}
		}
		if (len == atts.length) {
			Object[] objects = new Object[(int) (1.2 * (double) len) + 2 & ~0x1];
			System.arraycopy(atts, 0, objects, 0, len);
			atts = objects;
			Object object = null;
		}
		atts[len] = attn;
		atts[len + 1] = attr;
		len += 2;
	}

	public Object clone() {
		AttList attlist_1_ = new AttList(this);
		return attlist_1_;
	}

	public String toString() {
		String string = "";
		if (len == 0)
			return string;
		int i = 0;
		for (;;) {
			string += atts[i] + "=\"" + atts[i + 1] + "\"";
			i += 2;
			if (i == len)
				break;
			string += " ";
		}
		return string;
	}

	public final Enumeration keys() {
		return new Enumerator();
	}
}