/*
 * Elem - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import com.renderx.util.Array;
import com.renderx.util.Hashtable;
import com.renderx.xep.lib.InternalException;

public final class Elem {
	static final String NS_FO = "http://www.w3.org/1999/XSL/Format";

	static final String NS_RX = "http://www.renderx.com/XSL/Extensions";

	private static final Hashtable nametab = new Hashtable();

	private static Array infoarray = new Array(256);

	public static Descriptor $pcdata;

	public static final int CHANGE_WRITING_MODE = 1;

	public static final int USE_WRITING_MODE = 2;

	public static final int RESET_INDENTS = 4;

	public static final int OUT_OF_LINE = 8;

	public static final int SIZED_AREA = 16;

	public static final int ENABLE_TEXT_CHILDREN = 32;

	public static final short INVALID_ELEMENT = -1;

	public static final short PCDATA = 0;

	public static final short ROOT = 1;

	public static final short DECLARATIONS = 2;

	public static final short COLOR_PROFILE = 3;

	public static final short LAYOUT_MASTER_SET = 4;

	public static final short PAGE_SEQUENCE_MASTER = 5;

	public static final short SINGLE_PAGE_MASTER_REFERENCE = 6;

	public static final short REPEATABLE_PAGE_MASTER_REFERENCE = 7;

	public static final short REPEATABLE_PAGE_MASTER_ALTERNATIVES = 8;

	public static final short CONDITIONAL_PAGE_MASTER_REFERENCE = 9;

	public static final short SIMPLE_PAGE_MASTER = 10;

	public static final short REGION_BODY = 11;

	public static final short REGION_BEFORE = 12;

	public static final short REGION_AFTER = 13;

	public static final short REGION_START = 14;

	public static final short REGION_END = 15;

	public static final short PAGE_SEQUENCE = 16;

	public static final short TITLE = 17;

	public static final short STATIC_CONTENT = 18;

	public static final short FLOW = 19;

	public static final short BLOCK = 20;

	public static final short BLOCK_CONTAINER = 21;

	public static final short BIDI_OVERRIDE = 22;

	public static final short CHARACTER = 23;

	public static final short INITIAL_PROPERTY_SET = 24;

	public static final short EXTERNAL_GRAPHIC = 25;

	public static final short INSTREAM_FOREIGN_OBJECT = 26;

	public static final short INLINE = 27;

	public static final short INLINE_CONTAINER = 28;

	public static final short LEADER = 29;

	public static final short PAGE_NUMBER = 30;

	public static final short PAGE_NUMBER_CITATION = 31;

	public static final short TABLE_AND_CAPTION = 32;

	public static final short TABLE_CAPTION = 33;

	public static final short TABLE = 34;

	public static final short TABLE_COLUMN = 35;

	public static final short TABLE_HEADER = 36;

	public static final short TABLE_FOOTER = 37;

	public static final short TABLE_BODY = 38;

	public static final short TABLE_ROW = 39;

	public static final short TABLE_CELL = 40;

	public static final short LIST_BLOCK = 41;

	public static final short LIST_ITEM = 42;

	public static final short LIST_ITEM_LABEL = 43;

	public static final short LIST_ITEM_BODY = 44;

	public static final short FLOAT = 45;

	public static final short FOOTNOTE = 46;

	public static final short FOOTNOTE_BODY = 47;

	public static final short BASIC_LINK = 48;

	public static final short WRAPPER = 49;

	public static final short MARKER = 50;

	public static final short RETRIEVE_MARKER = 51;

	public static final short MULTI_SWITCH = 52;

	public static final short MULTI_CASE = 53;

	public static final short MULTI_TOGGLE = 54;

	public static final short MULTI_PROPERTY_SET = 55;

	public static final short FLOW_SECTION = 101;

	public static final short BOOKMARK_LABEL = 102;

	public static final short BOOKMARK = 103;

	public static final short OUTLINE = 104;

	public static final short META_FIELD = 105;

	public static final short META_INFO = 106;

	public static final short PINPOINT = 107;

	public static final short PAGE_INDEX = 108;

	public static final short RULER = 109;

	public static final short INDEX_ITEM = 110;

	public static final short BEGIN_INDEX_RANGE = 111;

	public static final short END_INDEX_RANGE = 112;

	public static final short PAGE_NUMBER_CITATION_LAST = 201;

	private static boolean initialized = false;

	public static final class Descriptor {
		public final short id;

		public final String name;

		public final String nsuri;

		public final boolean changeWritingMode;

		public final boolean useWritingMode;

		public final boolean resetIndents;

		public final boolean isOutOfLine;

		public final boolean isSizedArea;

		public final boolean enableTextChildren;

		public final Hashtable usedProps = new Hashtable();

		private Array requiredProps = null;

		private Descriptor(String string, String string_0_, short i, int i_1_) {
			nsuri = string;
			name = string_0_;
			id = i;
			changeWritingMode = (i_1_ & 0x1) > 0;
			useWritingMode = (i_1_ & 0x2) > 0;
			resetIndents = (i_1_ & 0x4) > 0;
			isOutOfLine = (i_1_ & 0x8) > 0;
			isSizedArea = (i_1_ & 0x10) > 0;
			enableTextChildren = (i_1_ & 0x20) > 0;
		}

		private void addProperty(Attn attn) {
			usedProps.put(attn, new Boolean(true));
		}

		private void removeProperty(Attn attn) {
			usedProps.remove(attn);
		}

		private void addRequiredProperty(Attn attn) {
			addProperty(attn);
			if (requiredProps == null)
				requiredProps = new Array(1);
			requiredProps.put(requiredProps.length(), attn);
		}

		public void checkRequired(Hashtable hashtable) throws ParserException {
			if (requiredProps != null) {
				for (int i = 0; i < requiredProps.length(); i++) {
					Attn attn = (Attn) requiredProps.get(i);
					if (hashtable.get(attn) == null)
						throw new ParserException("Element " + name
								+ " lacks required attribute " + attn.name
								+ "; element ignored");
				}
			}
		}

		private void addBorderProperties() {
			addProperty(Attn.$border_before_width_length);
			addProperty(Attn.$border_after_width_length);
			addProperty(Attn.$border_start_width_length);
			addProperty(Attn.$border_end_width_length);
			addProperty(Attn.$border_before_width_conditionality);
			addProperty(Attn.$border_after_width_conditionality);
			addProperty(Attn.$border_start_width_conditionality);
			addProperty(Attn.$border_end_width_conditionality);
			addProperty(Attn.$border_before_color);
			addProperty(Attn.$border_after_color);
			addProperty(Attn.$border_start_color);
			addProperty(Attn.$border_end_color);
			addProperty(Attn.$border_before_style);
			addProperty(Attn.$border_after_style);
			addProperty(Attn.$border_start_style);
			addProperty(Attn.$border_end_style);
		}

		private void addCollapsedBorderProperties() {
			addProperty(Attn.$border_before_precedence);
			addProperty(Attn.$border_after_precedence);
			addProperty(Attn.$border_start_precedence);
			addProperty(Attn.$border_end_precedence);
			addProperty(Attn.$border_before_width_collapsed);
			addProperty(Attn.$border_after_width_collapsed);
			addProperty(Attn.$border_start_width_collapsed);
			addProperty(Attn.$border_end_width_collapsed);
			addProperty(Attn.$border_before_width_conditionality);
			addProperty(Attn.$border_after_width_conditionality);
			addProperty(Attn.$border_start_width_conditionality);
			addProperty(Attn.$border_end_width_conditionality);
			addProperty(Attn.$border_before_color);
			addProperty(Attn.$border_after_color);
			addProperty(Attn.$border_start_color);
			addProperty(Attn.$border_end_color);
			addProperty(Attn.$border_before_style);
			addProperty(Attn.$border_after_style);
			addProperty(Attn.$border_start_style);
			addProperty(Attn.$border_end_style);
			addProperty(Attn.$table_element_class);
		}

		private void addPaddingProperties() {
			addProperty(Attn.$padding_before_length);
			addProperty(Attn.$padding_after_length);
			addProperty(Attn.$padding_start_length);
			addProperty(Attn.$padding_end_length);
			addProperty(Attn.$padding_before_conditionality);
			addProperty(Attn.$padding_after_conditionality);
			addProperty(Attn.$padding_start_conditionality);
			addProperty(Attn.$padding_end_conditionality);
		}

		private void addBackgroundProperties() {
			addProperty(Attn.$background_color);
			addProperty(Attn.$background_repeat);
			addProperty(Attn.$background_image);
			addProperty(Attn.$background_attachment);
			addProperty(Attn.$background_position_horizontal);
			addProperty(Attn.$background_position_vertical);
			addProperty(Attn.$background_content_width);
			addProperty(Attn.$background_content_height);
			addProperty(Attn.$background_content_type);
			addProperty(Attn.$background_scaling);
			addProperty(Attn.$image_base_url);
		}

		private void addBlockMarginProperties() {
			addProperty(Attn.$margin_before);
			addProperty(Attn.$margin_after);
			addProperty(Attn.$margin_start);
			addProperty(Attn.$margin_end);
			addProperty(Attn.$start_indent);
			addProperty(Attn.$end_indent);
			addProperty(Attn.$space_before_minimum);
			addProperty(Attn.$space_before_optimum);
			addProperty(Attn.$space_before_maximum);
			addProperty(Attn.$space_before_conditionality);
			addProperty(Attn.$space_before_precedence);
			addProperty(Attn.$space_after_minimum);
			addProperty(Attn.$space_after_optimum);
			addProperty(Attn.$space_after_maximum);
			addProperty(Attn.$space_after_conditionality);
			addProperty(Attn.$space_after_precedence);
		}

		private void addInlineMarginProperties() {
			addProperty(Attn.$margin_before);
			addProperty(Attn.$margin_after);
			addProperty(Attn.$margin_start);
			addProperty(Attn.$margin_end);
			addProperty(Attn.$space_start_minimum);
			addProperty(Attn.$space_start_optimum);
			addProperty(Attn.$space_start_maximum);
			addProperty(Attn.$space_start_conditionality);
			addProperty(Attn.$space_start_precedence);
			addProperty(Attn.$space_end_minimum);
			addProperty(Attn.$space_end_optimum);
			addProperty(Attn.$space_end_maximum);
			addProperty(Attn.$space_end_conditionality);
			addProperty(Attn.$space_end_precedence);
		}

		private void addAreaDimensionProperties() {
			addProperty(Attn.$block_progression_dimension_minimum);
			addProperty(Attn.$block_progression_dimension_optimum);
			addProperty(Attn.$block_progression_dimension_maximum);
			addProperty(Attn.$inline_progression_dimension_minimum);
			addProperty(Attn.$inline_progression_dimension_optimum);
			addProperty(Attn.$inline_progression_dimension_maximum);
		}

		private void addFontProperties() {
			addProperty(Attn.$font_family);
			addProperty(Attn.$font_selection_strategy);
			addProperty(Attn.$font_size);
			addProperty(Attn.$font_stretch);
			addProperty(Attn.$font_size_adjust);
			addProperty(Attn.$font_style);
			addProperty(Attn.$font_variant);
			addProperty(Attn.$font_weight);
			addProperty(Attn.$glyph_orientation_horizontal);
			addProperty(Attn.$glyph_orientation_vertical);
		}

		private void addHyphenationProperties() {
			addProperty(Attn.$country);
			addProperty(Attn.$language);
			addProperty(Attn.$script);
			addProperty(Attn.$hyphenate);
			addProperty(Attn.$hyphenation_character);
			addProperty(Attn.$hyphenation_push_character_count);
			addProperty(Attn.$hyphenation_remain_character_count);
		}

		private void addLineHeightProperties() {
			addProperty(Attn.$line_height_minimum);
			addProperty(Attn.$line_height_optimum);
			addProperty(Attn.$line_height_maximum);
			addProperty(Attn.$line_height_conditionality);
			addProperty(Attn.$line_height_precedence);
			addProperty(Attn.$line_height_shift_adjustment);
		}

		private void addBaselineProperties() {
			addProperty(Attn.$alignment_adjust);
			addProperty(Attn.$alignment_baseline);
			addProperty(Attn.$baseline_shift);
			addProperty(Attn.$dominant_baseline);
		}

		private void addLetterSpacingProperties() {
			addProperty(Attn.$letter_spacing_minimum);
			addProperty(Attn.$letter_spacing_optimum);
			addProperty(Attn.$letter_spacing_maximum);
			addProperty(Attn.$letter_spacing_conditionality);
			addProperty(Attn.$letter_spacing_precedence);
			addProperty(Attn.$word_spacing_minimum);
			addProperty(Attn.$word_spacing_optimum);
			addProperty(Attn.$word_spacing_maximum);
			addProperty(Attn.$word_spacing_conditionality);
			addProperty(Attn.$word_spacing_precedence);
		}

		private void addWordSpacingProperties() {
			addProperty(Attn.$word_spacing_minimum);
			addProperty(Attn.$word_spacing_optimum);
			addProperty(Attn.$word_spacing_maximum);
			addProperty(Attn.$word_spacing_conditionality);
			addProperty(Attn.$word_spacing_precedence);
		}

		private void addWhitespaceTreatmentProperties() {
			addProperty(Attn.$linefeed_treatment);
			addProperty(Attn.$white_space_treatment);
			addProperty(Attn.$white_space_collapse);
		}

		private void addTextEffectsProperties() {
			addProperty(Attn.$text_decoration);
			addProperty(Attn.$score_spaces);
			addProperty(Attn.$text_shadow);
			addProperty(Attn.$text_transform);
		}

		private void addBlockKeepBreakProperties() {
			addProperty(Attn.$break_after);
			addProperty(Attn.$break_before);
			addProperty(Attn.$keep_with_next_within_page);
			addProperty(Attn.$keep_with_next_within_column);
			addProperty(Attn.$keep_with_previous_within_page);
			addProperty(Attn.$keep_with_previous_within_column);
			addProperty(Attn.$keep_together_within_page);
			addProperty(Attn.$keep_together_within_column);
		}

		private void addRelativePositionProperties() {
			addProperty(Attn.$relative_position);
			addProperty(Attn.$top);
			addProperty(Attn.$bottom);
			addProperty(Attn.$right);
			addProperty(Attn.$left);
		}

		private void addCommonRegionProperties() {
			addBorderProperties();
			addPaddingProperties();
			addBackgroundProperties();
			addProperty(Attn.$margin_before);
			addProperty(Attn.$margin_after);
			addProperty(Attn.$margin_start);
			addProperty(Attn.$margin_end);
			addProperty(Attn.$region_name);
			addProperty(Attn.$display_align);
			addProperty(Attn.$clip);
			addProperty(Attn.$overflow);
			addProperty(Attn.$reference_orientation);
			addProperty(Attn.$writing_mode);
		}
	}

	public static Descriptor getDescriptor(short i) throws InternalException {
		Descriptor descriptor = (Descriptor) infoarray.get(i);
		if (descriptor == null)
			throw new InternalException("Invalid object id: " + i);
		return descriptor;
	}

	public static String getName(short i) {
		Descriptor descriptor = (Descriptor) infoarray.get(i);
		return descriptor == null ? null : descriptor.name;
	}

	public static Descriptor getDescriptor(String string) {
		return (Descriptor) nametab.get(string);
	}

	private static Descriptor createDescriptor(String string, String string_4_,
			short i, int i_5_) {
		Descriptor descriptor = new Descriptor(string, string_4_, i, i_5_);
		if ("http://www.w3.org/1999/XSL/Format".equals(string)
				|| "http://www.renderx.com/XSL/Extensions".equals(string))
			nametab.put(string_4_, descriptor);
		else
			throw new InternalException(
					"Invalid namespace URI in element descriptor initialization");
		infoarray.put(i, descriptor);
		return descriptor;
	}

	private static Descriptor createDescriptor(String string, String string_6_,
			short i) {
		return createDescriptor(string, string_6_, i, 0);
	}

	public static synchronized void init() throws InternalException {
		if (!initialized) {
			Attn.init();
			$pcdata = createDescriptor("http://www.renderx.com/XSL/Extensions",
					"pcdata", (short) 0);
			$pcdata.addRequiredProperty(Attn.$TEXT);
			$pcdata.addFontProperties();
			$pcdata.addHyphenationProperties();
			$pcdata.addLineHeightProperties();
			$pcdata.addLetterSpacingProperties();
			$pcdata.addWordSpacingProperties();
			$pcdata.addWhitespaceTreatmentProperties();
			$pcdata.addTextEffectsProperties();
			$pcdata.addProperty(Attn.$color);
			$pcdata.addProperty(Attn.$text_altitude);
			$pcdata.addProperty(Attn.$text_depth);
			$pcdata.addProperty(Attn.$visibility);
			$pcdata.addProperty(Attn.$wrap_option);
			Descriptor descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions", "flow-section",
					(short) 101, 5);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addBlockMarginProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addProperty(Attn.$clear);
			descriptor.addProperty(Attn.$clip);
			descriptor.addProperty(Attn.$column_count);
			descriptor.addProperty(Attn.$column_gap);
			descriptor.addProperty(Attn.$display_align);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$intrusion_displace);
			descriptor.addProperty(Attn.$overflow);
			descriptor.addProperty(Attn.$reference_orientation);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions", "bookmark-label",
					(short) 102, 32);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions", "bookmark",
					(short) 103);
			descriptor.addProperty(Attn.$external_destination);
			descriptor.addProperty(Attn.$internal_destination);
			descriptor.addProperty(Attn.$collapse_subtree);
			descriptor.addProperty(Attn.$show_destination);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions", "outline",
					(short) 104);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions", "meta-field",
					(short) 105);
			descriptor.addRequiredProperty(Attn.$name);
			descriptor.addRequiredProperty(Attn.$value);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions", "meta-info",
					(short) 106);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions", "pinpoint",
					(short) 107);
			descriptor.addRequiredProperty(Attn.$value);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions",
					"begin-index-range", (short) 111);
			descriptor.addRequiredProperty(Attn.$id);
			descriptor.addRequiredProperty(Attn.$key);
			descriptor.addProperty(Attn.$role);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions", "end-index-range",
					(short) 112);
			descriptor.addRequiredProperty(Attn.$ref_id);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions", "page-index",
					(short) 108);
			descriptor.addBaselineProperties();
			descriptor.addLineHeightProperties();
			descriptor.addTextEffectsProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addFontProperties();
			descriptor.addProperty(Attn.$color);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$keep_together_within_line);
			descriptor.addProperty(Attn.$keep_with_next_within_line);
			descriptor.addProperty(Attn.$keep_with_previous_within_line);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$list_separator);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions", "index-item",
					(short) 110);
			descriptor.addRequiredProperty(Attn.$ref_key);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addInlineMarginProperties();
			descriptor.addBaselineProperties();
			descriptor.addLineHeightProperties();
			descriptor.addTextEffectsProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addFontProperties();
			descriptor.addProperty(Attn.$color);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$keep_together_within_line);
			descriptor.addProperty(Attn.$keep_with_next_within_line);
			descriptor.addProperty(Attn.$keep_with_previous_within_line);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$direction);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$link_back);
			descriptor.addProperty(Attn.$range_separator);
			descriptor.addProperty(Attn.$merge_subsequent_page_numbers);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions", "ruler",
					(short) 109, 4);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addProperty(Attn.$inline_progression_dimension_minimum);
			descriptor.addProperty(Attn.$inline_progression_dimension_optimum);
			descriptor.addProperty(Attn.$inline_progression_dimension_maximum);
			descriptor.addProperty(Attn.$text_align);
			descriptor.addProperty(Attn.$format);
			descriptor.addProperty(Attn.$letter_value);
			descriptor.addProperty(Attn.$grouping_separator);
			descriptor.addProperty(Attn.$grouping_size);
			descriptor.addFontProperties();
			descriptor.addHyphenationProperties();
			descriptor.addLineHeightProperties();
			descriptor.addLetterSpacingProperties();
			descriptor.addWordSpacingProperties();
			descriptor.addWhitespaceTreatmentProperties();
			descriptor.addBaselineProperties();
			descriptor.addTextEffectsProperties();
			descriptor.addProperty(Attn.$color);
			descriptor.addProperty(Attn.$text_altitude);
			descriptor.addProperty(Attn.$text_depth);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"root", (short) 1);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$media_usage);
			descriptor.addProperty(Attn.$initial_destination);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"declarations", (short) 2);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"color-profile", (short) 3);
			descriptor.addRequiredProperty(Attn.$color_profile_name);
			descriptor.addRequiredProperty(Attn.$src);
			descriptor.addProperty(Attn.$rendering_intent);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"layout-master-set", (short) 4);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"page-sequence-master", (short) 5);
			descriptor.addRequiredProperty(Attn.$master_name);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"single-page-master-reference", (short) 6);
			descriptor.addRequiredProperty(Attn.$master_reference);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"repeatable-page-master-reference", (short) 7);
			descriptor.addRequiredProperty(Attn.$master_reference);
			descriptor.addProperty(Attn.$maximum_repeats);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"repeatable-page-master-alternatives", (short) 8);
			descriptor.addProperty(Attn.$maximum_repeats);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"conditional-page-master-reference", (short) 9);
			descriptor.addRequiredProperty(Attn.$master_reference);
			descriptor.addProperty(Attn.$page_position);
			descriptor.addProperty(Attn.$odd_or_even);
			descriptor.addProperty(Attn.$blank_or_not_blank);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"simple-page-master", (short) 10);
			descriptor.addRequiredProperty(Attn.$master_name);
			descriptor.addProperty(Attn.$margin_before);
			descriptor.addProperty(Attn.$margin_after);
			descriptor.addProperty(Attn.$margin_start);
			descriptor.addProperty(Attn.$margin_end);
			descriptor.addProperty(Attn.$page_height);
			descriptor.addProperty(Attn.$page_width);
			descriptor.addProperty(Attn.$reference_orientation);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"region-body", (short) 11);
			descriptor.addCommonRegionProperties();
			descriptor.addProperty(Attn.$column_count);
			descriptor.addProperty(Attn.$column_gap);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"region-before", (short) 12);
			descriptor.addCommonRegionProperties();
			descriptor.addProperty(Attn.$extent);
			descriptor.addProperty(Attn.$precedence);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"region-after", (short) 13);
			descriptor.addCommonRegionProperties();
			descriptor.addProperty(Attn.$extent);
			descriptor.addProperty(Attn.$precedence);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"region-start", (short) 14);
			descriptor.addCommonRegionProperties();
			descriptor.addProperty(Attn.$extent);
			descriptor.addProperty(Attn.$precedence);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"region-end", (short) 15);
			descriptor.addCommonRegionProperties();
			descriptor.addProperty(Attn.$extent);
			descriptor.addProperty(Attn.$precedence);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"page-sequence", (short) 16);
			descriptor.addRequiredProperty(Attn.$master_reference);
			descriptor.addProperty(Attn.$country);
			descriptor.addProperty(Attn.$format);
			descriptor.addProperty(Attn.$language);
			descriptor.addProperty(Attn.$letter_value);
			descriptor.addProperty(Attn.$grouping_separator);
			descriptor.addProperty(Attn.$grouping_size);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$initial_page_number);
			descriptor.addProperty(Attn.$force_page_count);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"title", (short) 17);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addInlineMarginProperties();
			descriptor.addLineHeightProperties();
			descriptor.addFontProperties();
			descriptor.addHyphenationProperties();
			descriptor.addProperty(Attn.$visibility);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"static-content", (short) 18, 1);
			descriptor.addRequiredProperty(Attn.$flow_name);
			descriptor.addProperty(Attn.$start_indent);
			descriptor.addProperty(Attn.$end_indent);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"flow", (short) 19, 1);
			descriptor.addProperty(Attn.$id);
			descriptor.addRequiredProperty(Attn.$flow_name);
			descriptor.addProperty(Attn.$start_indent);
			descriptor.addProperty(Attn.$end_indent);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"block", (short) 20, 50);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addBlockMarginProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addFontProperties();
			descriptor.addHyphenationProperties();
			descriptor.addLineHeightProperties();
			descriptor.addWhitespaceTreatmentProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$clear);
			descriptor.addProperty(Attn.$orphans);
			descriptor.addProperty(Attn.$hyphenation_keep);
			descriptor.addProperty(Attn.$hyphenation_ladder_count);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$intrusion_displace);
			descriptor.addProperty(Attn.$last_line_end_indent);
			descriptor.addProperty(Attn.$line_stacking_strategy);
			descriptor.addProperty(Attn.$text_align);
			descriptor.addProperty(Attn.$text_align_last);
			descriptor.addProperty(Attn.$text_altitude);
			descriptor.addProperty(Attn.$text_depth);
			descriptor.addProperty(Attn.$text_indent);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$widows);
			descriptor.addProperty(Attn.$wrap_option);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"block-container", (short) 21, 21);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addBlockMarginProperties();
			descriptor.addAreaDimensionProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$absolute_position);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$intrusion_displace);
			descriptor.addProperty(Attn.$clear);
			descriptor.addProperty(Attn.$clip);
			descriptor.addProperty(Attn.$overflow);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$display_align);
			descriptor.addProperty(Attn.$reference_orientation);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"bidi-override", (short) 22, 32);
			descriptor.addRelativePositionProperties();
			descriptor.addLineHeightProperties();
			descriptor.addFontProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$unicode_bidi);
			descriptor.addProperty(Attn.$direction);
			descriptor.addProperty(Attn.$start_indent);
			descriptor.addProperty(Attn.$end_indent);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"character", (short) 23);
			descriptor.addRequiredProperty(Attn.$character);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addInlineMarginProperties();
			descriptor.addFontProperties();
			descriptor.addHyphenationProperties();
			descriptor.addLineHeightProperties();
			descriptor.addLetterSpacingProperties();
			descriptor.addWordSpacingProperties();
			descriptor.addWhitespaceTreatmentProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addBaselineProperties();
			descriptor.addTextEffectsProperties();
			descriptor.addProperty(Attn.$color);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$treat_as_word_space);
			descriptor.addProperty(Attn.$suppress_at_line_break);
			descriptor.addProperty(Attn.$keep_with_next_within_line);
			descriptor.addProperty(Attn.$keep_with_previous_within_line);
			descriptor.addProperty(Attn.$glyph_orientation_horizontal);
			descriptor.addProperty(Attn.$glyph_orientation_vertical);
			descriptor.addProperty(Attn.$text_altitude);
			descriptor.addProperty(Attn.$text_depth);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"initial-property-set", (short) 24);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addFontProperties();
			descriptor.addHyphenationProperties();
			descriptor.addLineHeightProperties();
			descriptor.addLetterSpacingProperties();
			descriptor.addWordSpacingProperties();
			descriptor.addWhitespaceTreatmentProperties();
			descriptor.addTextEffectsProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$color);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"external-graphic", (short) 25);
			descriptor.addRequiredProperty(Attn.$src);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addInlineMarginProperties();
			descriptor.addAreaDimensionProperties();
			descriptor.addBaselineProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$content_width);
			descriptor.addProperty(Attn.$content_height);
			descriptor.addProperty(Attn.$content_type);
			descriptor.addProperty(Attn.$display_align);
			descriptor.addProperty(Attn.$scaling);
			descriptor.addProperty(Attn.$scaling_method);
			descriptor.addProperty(Attn.$clip);
			descriptor.addProperty(Attn.$keep_with_next_within_line);
			descriptor.addProperty(Attn.$keep_with_previous_within_line);
			descriptor.addProperty(Attn.$overflow);
			descriptor.addProperty(Attn.$text_align);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$image_base_url);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"instream-foreign-object", (short) 26);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addInlineMarginProperties();
			descriptor.addBaselineProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addAreaDimensionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$content_width);
			descriptor.addProperty(Attn.$content_height);
			descriptor.addProperty(Attn.$content_type);
			descriptor.addProperty(Attn.$display_align);
			descriptor.addProperty(Attn.$keep_with_next_within_line);
			descriptor.addProperty(Attn.$keep_with_previous_within_line);
			descriptor.addProperty(Attn.$scaling);
			descriptor.addProperty(Attn.$scaling_method);
			descriptor.addProperty(Attn.$clip);
			descriptor.addProperty(Attn.$overflow);
			descriptor.addProperty(Attn.$text_align);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"inline", (short) 27, 32);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addInlineMarginProperties();
			descriptor.addAreaDimensionProperties();
			descriptor.addBaselineProperties();
			descriptor.addLineHeightProperties();
			descriptor.addTextEffectsProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addFontProperties();
			descriptor.addHyphenationProperties();
			descriptor.addProperty(Attn.$color);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$wrap_option);
			descriptor.addProperty(Attn.$keep_together_within_line);
			descriptor.addProperty(Attn.$keep_with_next_within_line);
			descriptor.addProperty(Attn.$keep_with_previous_within_line);
			descriptor.addProperty(Attn.$text_altitude);
			descriptor.addProperty(Attn.$text_depth);
			descriptor.addProperty(Attn.$direction);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$start_indent);
			descriptor.addProperty(Attn.$end_indent);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"inline-container", (short) 28, 21);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addInlineMarginProperties();
			descriptor.addAreaDimensionProperties();
			descriptor.addBaselineProperties();
			descriptor.addLineHeightProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$clip);
			descriptor.addProperty(Attn.$overflow);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$display_align);
			descriptor.addProperty(Attn.$reference_orientation);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$keep_together_within_line);
			descriptor.addProperty(Attn.$keep_with_next_within_line);
			descriptor.addProperty(Attn.$keep_with_previous_within_line);
			descriptor.addProperty(Attn.$keep_together_within_line);
			descriptor.addProperty(Attn.$text_altitude);
			descriptor.addProperty(Attn.$text_depth);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$start_indent);
			descriptor.addProperty(Attn.$end_indent);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"leader", (short) 29, 32);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addInlineMarginProperties();
			descriptor.addBaselineProperties();
			descriptor.addLineHeightProperties();
			descriptor.addTextEffectsProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addFontProperties();
			descriptor.addProperty(Attn.$color);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$keep_with_next_within_line);
			descriptor.addProperty(Attn.$keep_with_previous_within_line);
			descriptor.addProperty(Attn.$leader_alignment);
			descriptor.addProperty(Attn.$leader_pattern);
			descriptor.addProperty(Attn.$leader_pattern_width);
			descriptor.addProperty(Attn.$rule_style);
			descriptor.addProperty(Attn.$rule_thickness);
			descriptor.addProperty(Attn.$leader_length_minimum);
			descriptor.addProperty(Attn.$leader_length_optimum);
			descriptor.addProperty(Attn.$leader_length_maximum);
			descriptor.addProperty(Attn.$leader_dot);
			descriptor.addProperty(Attn.$leader_content);
			descriptor.addProperty(Attn.$direction);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"page-number", (short) 30);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addInlineMarginProperties();
			descriptor.addBaselineProperties();
			descriptor.addLineHeightProperties();
			descriptor.addTextEffectsProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addFontProperties();
			descriptor.addProperty(Attn.$color);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$keep_with_next_within_line);
			descriptor.addProperty(Attn.$keep_with_previous_within_line);
			descriptor.addProperty(Attn.$direction);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"page-number-citation", (short) 31);
			descriptor.addRequiredProperty(Attn.$ref_id);
			descriptor.addFontProperties();
			descriptor.addLetterSpacingProperties();
			descriptor.addWordSpacingProperties();
			descriptor.addLineHeightProperties();
			descriptor.addWhitespaceTreatmentProperties();
			descriptor.addTextEffectsProperties();
			descriptor.addProperty(Attn.$color);
			descriptor.addProperty(Attn.$text_altitude);
			descriptor.addProperty(Attn.$text_depth);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$direction);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor(
					"http://www.renderx.com/XSL/Extensions",
					"page-number-citation-last", (short) 201);
			descriptor.addRequiredProperty(Attn.$ref_id);
			descriptor.addFontProperties();
			descriptor.addLetterSpacingProperties();
			descriptor.addWordSpacingProperties();
			descriptor.addLineHeightProperties();
			descriptor.addWhitespaceTreatmentProperties();
			descriptor.addTextEffectsProperties();
			descriptor.addProperty(Attn.$color);
			descriptor.addProperty(Attn.$text_altitude);
			descriptor.addProperty(Attn.$text_depth);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$direction);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"table-and-caption", (short) 32, 2);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addBlockMarginProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$clear);
			descriptor.addProperty(Attn.$intrusion_displace);
			descriptor.addProperty(Attn.$caption_side);
			descriptor.addProperty(Attn.$text_align);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"table-caption", (short) 33, 21);
			descriptor.addAreaDimensionProperties();
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addBlockMarginProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$clear);
			descriptor.addProperty(Attn.$clip);
			descriptor.addProperty(Attn.$overflow);
			descriptor.addProperty(Attn.$display_align);
			descriptor.addProperty(Attn.$intrusion_displace);
			descriptor.addProperty(Attn.$reference_orientation);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"table", (short) 34, 17);
			descriptor.addBorderProperties();
			descriptor.addCollapsedBorderProperties();
			descriptor.addBackgroundProperties();
			descriptor.addPaddingProperties();
			descriptor.addBlockMarginProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$inline_progression_dimension_minimum);
			descriptor.addProperty(Attn.$inline_progression_dimension_optimum);
			descriptor.addProperty(Attn.$inline_progression_dimension_maximum);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$intrusion_displace);
			descriptor.addProperty(Attn.$clear);
			descriptor
					.addProperty(Attn.$border_separation_block_progression_direction);
			descriptor
					.addProperty(Attn.$border_separation_inline_progression_direction);
			descriptor.addProperty(Attn.$table_layout);
			descriptor.addProperty(Attn.$border_collapse);
			descriptor.addProperty(Attn.$table_omit_header_at_break);
			descriptor.addProperty(Attn.$table_omit_footer_at_break);
			descriptor.addProperty(Attn.$table_omit_initial_header);
			descriptor.addProperty(Attn.$table_omit_final_footer);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$table_identifier);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"table-column", (short) 35);
			descriptor.addProperty(Attn.$column_number);
			descriptor.addProperty(Attn.$column_width_fixed);
			descriptor.addProperty(Attn.$column_width_proportional);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"table-header", (short) 36);
			descriptor.addBorderProperties();
			descriptor.addCollapsedBorderProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$table_identifier);
			descriptor.addProperty(Attn.$body_number);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"table-footer", (short) 37);
			descriptor.addBorderProperties();
			descriptor.addCollapsedBorderProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$table_identifier);
			descriptor.addProperty(Attn.$body_number);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"table-body", (short) 38);
			descriptor.addBorderProperties();
			descriptor.addCollapsedBorderProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$table_identifier);
			descriptor.addProperty(Attn.$body_number);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"table-row", (short) 39);
			descriptor.addBlockKeepBreakProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$block_progression_dimension_minimum);
			descriptor.addProperty(Attn.$block_progression_dimension_optimum);
			descriptor.addProperty(Attn.$block_progression_dimension_maximum);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"table-cell", (short) 40, 21);
			descriptor.addBorderProperties();
			descriptor.addCollapsedBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addAreaDimensionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$clear);
			descriptor.addProperty(Attn.$intrusion_displace);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$table_identifier);
			descriptor.addProperty(Attn.$body_number);
			descriptor.addProperty(Attn.$row_number);
			descriptor.addProperty(Attn.$column_number);
			descriptor.addProperty(Attn.$number_columns_spanned);
			descriptor.addProperty(Attn.$number_rows_spanned);
			descriptor.addProperty(Attn.$empty_cells);
			descriptor.addProperty(Attn.$display_align);
			descriptor.addProperty(Attn.$relative_align);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$reference_orientation);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"list-block", (short) 41, 2);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addBlockMarginProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$intrusion_displace);
			descriptor.addProperty(Attn.$clear);
			descriptor.addProperty(Attn.$provisional_distance_between_starts);
			descriptor.addProperty(Attn.$provisional_label_separation);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"list-item", (short) 42, 2);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addBlockMarginProperties();
			descriptor.addBlockKeepBreakProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$intrusion_displace);
			descriptor.addProperty(Attn.$clear);
			descriptor.addProperty(Attn.$relative_align);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"list-item-label", (short) 43);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$keep_together_within_page);
			descriptor.addProperty(Attn.$keep_together_within_column);
			descriptor.addProperty(Attn.$start_indent);
			descriptor.addProperty(Attn.$end_indent);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"list-item-body", (short) 44);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$keep_together_within_page);
			descriptor.addProperty(Attn.$keep_together_within_column);
			descriptor.addProperty(Attn.$start_indent);
			descriptor.addProperty(Attn.$end_indent);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"float", (short) 45, 8);
			descriptor.addProperty(Attn.$float);
			descriptor.addProperty(Attn.$clear);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$z_index);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"footnote", (short) 46);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"footnote-body", (short) 47, 8);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"basic-link", (short) 48, 32);
			descriptor.addBorderProperties();
			descriptor.addPaddingProperties();
			descriptor.addBackgroundProperties();
			descriptor.addInlineMarginProperties();
			descriptor.addBaselineProperties();
			descriptor.addLineHeightProperties();
			descriptor.addTextEffectsProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addFontProperties();
			descriptor.addHyphenationProperties();
			descriptor.addRelativePositionProperties();
			descriptor.addProperty(Attn.$color);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$wrap_option);
			descriptor.addProperty(Attn.$keep_together_within_line);
			descriptor.addProperty(Attn.$keep_with_next_within_line);
			descriptor.addProperty(Attn.$keep_with_previous_within_line);
			descriptor.addProperty(Attn.$internal_destination);
			descriptor.addProperty(Attn.$external_destination);
			descriptor.addProperty(Attn.$show_destination);
			descriptor.addProperty(Attn.$indicate_destination);
			descriptor.addProperty(Attn.$destination_placement_offset);
			descriptor.addProperty(Attn.$target_processing_context);
			descriptor.addProperty(Attn.$target_stylesheet);
			descriptor.addProperty(Attn.$target_presentation_context);
			descriptor.addProperty(Attn.$text_altitude);
			descriptor.addProperty(Attn.$text_depth);
			descriptor.addProperty(Attn.$direction);
			descriptor.addProperty(Attn.$writing_mode);
			descriptor.addProperty(Attn.$visibility);
			descriptor.addProperty(Attn.$z_index);
			descriptor.addProperty(Attn.$start_indent);
			descriptor.addProperty(Attn.$end_indent);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"wrapper", (short) 49, 32);
			descriptor.addProperty(Attn.$id);
			descriptor.addProperty(Attn.$role);
			descriptor.addProperty(Attn.$key);
			descriptor.addProperty(Attn.$start_indent);
			descriptor.addProperty(Attn.$end_indent);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"marker", (short) 50, 32);
			descriptor.addRequiredProperty(Attn.$marker_class_name);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"retrieve-marker", (short) 51);
			descriptor.addRequiredProperty(Attn.$retrieve_class_name);
			descriptor.addProperty(Attn.$retrieve_position);
			descriptor.addProperty(Attn.$retrieve_boundary);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"multi-switch", (short) 52);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"multi-case", (short) 53);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"multi-toggle", (short) 54);
			descriptor = createDescriptor("http://www.w3.org/1999/XSL/Format",
					"multi-property-set", (short) 55);
			initialized = true;
		}
	}
}