/* Bracketed - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public final class Bracketed extends Expr
{
    protected Attr arg = null;
    
    public Bracketed() {
	/* empty */
    }
    
    public Bracketed(Attr attr) {
	arg = attr;
    }
    
    public String toString() {
	return "(" + arg + ")";
    }
    
    public Attr initialize(Attr[] attrs) throws ParserException {
	arg = attrs[0];
	return preevaluate();
    }
    
    public Attr preevaluate() throws ParserException {
	return arg.preevaluate();
    }
    
    public Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	return arg.evaluate(parsercontext, attn);
    }
}
