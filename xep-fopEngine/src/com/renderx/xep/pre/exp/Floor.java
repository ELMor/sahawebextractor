/* Floor - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserException;

public final class Floor extends UnaryArithmetic
{
    public Floor() {
	/* empty */
    }
    
    public Floor(Attr attr) {
	super(attr);
    }
    
    public String toString() {
	return "floor(" + arg + ")";
    }
    
    protected Attr calculate(Attr attr) throws ParserException {
	if (attr instanceof Expr)
	    return attr == arg ? this : new Floor(attr);
	if (attr instanceof Attr.Count)
	    return attr;
	if (attr instanceof Attr.Ratio)
	    return Attr.Count.create((int) Math.floor(attr.ratio()));
	throw new ParserException("Cannot take floor() of type "
				  + attr.getTypeName());
    }
}
