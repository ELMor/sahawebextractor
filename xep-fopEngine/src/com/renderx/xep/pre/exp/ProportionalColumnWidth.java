/* ProportionalColumnWidth - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public final class ProportionalColumnWidth extends Expr
{
    protected Attr arg = null;
    
    public ProportionalColumnWidth() {
	/* empty */
    }
    
    public ProportionalColumnWidth(Attr attr) {
	arg = attr;
    }
    
    public String toString() {
	return "proportional-column-width(" + arg + ")";
    }
    
    public final Attr initialize(Attr[] attrs) throws ParserException {
	if (attrs == null)
	    throw new ParserException
		      ("proportional-column-width() must have an argument");
	if (attrs.length != 1)
	    throw new ParserException
		      ("Invalid number of arguments proportional-column-width()");
	arg = attrs[0];
	return preevaluate();
    }
    
    public final Attr preevaluate() throws ParserException {
	Attr attr = arg.preevaluate();
	if (attr instanceof Expr || attr instanceof Attr.Count
	    || attr instanceof Attr.Ratio)
	    return attr == arg ? this : new ProportionalColumnWidth(attr);
	throw new ParserException("Cannot use type " + attr.getTypeName()
				  + "inside proportional-column-width()");
    }
    
    public final Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	if (attn != Attn.$column_width_fixed)
	    throw new ParserException
		      ("Cannot use proportional-column-width() inside attribute "
		       + attn.name);
	Attr attr = arg.evaluate(parsercontext, attn);
	if (attr instanceof Expr || attr instanceof Attr.Count
	    || attr instanceof Attr.Ratio)
	    parsercontext.proportionalColumnWidth = attr;
	else
	    throw new ParserException("Cannot use type " + attr.getTypeName()
				      + "inside proportional-column-width()");
	return Attr.zerolength;
    }
}
