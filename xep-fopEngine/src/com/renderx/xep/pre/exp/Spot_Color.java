/* Spot_Color - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.graphics.SpotColorList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class Spot_Color extends Expr
{
    protected Attr C = null;
    protected Attr T = null;
    protected Attr A = null;
    
    public String toString() {
	return "spotcolor(" + C + ", " + T + ", " + A + ")";
    }
    
    public Spot_Color() {
	/* empty */
    }
    
    public Spot_Color(Attr attr, Attr attr_0_, Attr attr_1_) {
	C = attr;
	T = attr_0_;
	A = attr_1_;
    }
    
    public Attr initialize(Attr[] attrs) throws ParserException {
	if (attrs == null)
	    throw new ParserException
		      ("spotcolor() function requires 2 or more arguments");
	if (attrs.length < 2)
	    throw new ParserException
		      ("spotcolor() function requires 2 or more arguments");
	C = attrs[0];
	T = attrs[1];
	A = selectAlternateColor(attrs);
	return preevaluate();
    }
    
    public final Attr preevaluate() throws ParserException {
	return calculate(C.preevaluate(), T.preevaluate(),
			 A == null ? null : A.preevaluate());
    }
    
    public final Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	return calculate(C.evaluate(parsercontext, attn),
			 T.evaluate(parsercontext, attn),
			 A == null ? null : A.evaluate(parsercontext, attn));
    }
    
    protected final Attr calculate(Attr attr, Attr attr_2_, Attr attr_3_)
	throws ParserException {
	if (attr instanceof Expr || attr_2_ != null && attr_2_ instanceof Expr)
	    return (attr == C && attr_2_ == A && attr_3_ == T ? this
		    : new Spot_Color(attr, attr_2_, attr_3_));
	if (!(attr instanceof Attr.Word))
	    throw new ParserException
		      ("Argument of spotcolor() function cannot be of type "
		       + attr.getTypeName());
	if (attr_3_ != null && !(attr_3_ instanceof Attr.Color.Grayscale)
	    && !(attr_3_ instanceof Attr.Color.CMYK))
	    throw new ParserException
		      ("Argument of spotcolor() function cannot be of type "
		       + attr_2_.getTypeName());
	String string = attr.word();
	Attr.Color.Opaque opaque = (Attr.Color.Opaque) attr_3_;
	if (opaque == null) {
	    double[] ds = SpotColorList.dflt.getCMYK(string);
	    if (ds != null)
		opaque = Attr.Color.CMYK.create(ds[0], ds[1], ds[2], ds[3]);
	    else
		opaque = Attr.Color.Grayscale.create(0.0);
	}
	return Attr.Color.SpotColor.create(string, opaque,
					   extractColorComponent(attr_2_));
    }
    
    private double extractColorComponent(Attr attr) throws ParserException {
	double d = 0.0;
	if (attr instanceof Attr.Percentage)
	    d = attr.percentage() / 100.0;
	else if (attr instanceof Attr.Ratio)
	    d = attr.ratio();
	else if (attr instanceof Attr.Count)
	    d = (double) attr.count();
	else
	    throw new ParserException
		      ("Argument of spotcolor() function cannot be of type "
		       + attr.getTypeName());
	if (d < 0.0)
	    d = 0.0;
	if (d > 1.0)
	    d = 1.0;
	return d;
    }
    
    protected final Attr selectAlternateColor(Attr[] attrs)
	throws ParserException {
	if (attrs.length < 3)
	    return null;
	Attr attr = attrs[2];
	if (attr instanceof Attr.Word) {
	    if ("#Grayscale".equals(attr.word())) {
		if (attrs.length != 4)
		    throw new ParserException
			      ("Built-in #Grayscale profile takes 1 argument");
		return new Grayscale_Color()
			   .initialize(new Attr[] { attrs[3] });
	    }
	    if ("#CMYK".equals(attr.word())) {
		if (attrs.length != 7)
		    throw new ParserException
			      ("Built-in #CMYK profile takes 4 arguments");
		return new CMYK_Color().initialize(new Attr[]
						   { attrs[3], attrs[4],
						     attrs[5], attrs[6] });
	    }
	    throw new ParserException
		      ("Invalid profile name for alternate color (should be #Grayscale or #CMYK): "
		       + attr.word());
	}
	throw new ParserException
		  ("Color profile name in spotcolor() function cannot be of type "
		   + attr.getTypeName());
    }
}
