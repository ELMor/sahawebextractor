/* CMYK_Color - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class CMYK_Color extends Expr
{
    protected Attr C = null;
    protected Attr M = null;
    protected Attr Y = null;
    protected Attr K = null;
    
    public String toString() {
	return "cmyk(" + C + ", " + M + ", " + Y + ", " + K + ")";
    }
    
    public CMYK_Color() {
	/* empty */
    }
    
    public CMYK_Color(Attr attr, Attr attr_0_, Attr attr_1_, Attr attr_2_) {
	C = attr;
	M = attr_0_;
	Y = attr_1_;
	K = attr_2_;
    }
    
    public Attr initialize(Attr[] attrs) throws ParserException {
	if (attrs == null)
	    throw new ParserException("cmyk() function requires 4 arguments");
	if (attrs.length != 4)
	    throw new ParserException("cmyk() function requires 4 arguments");
	C = attrs[0];
	M = attrs[1];
	Y = attrs[2];
	K = attrs[3];
	return preevaluate();
    }
    
    public final Attr preevaluate() throws ParserException {
	return calculate(C.preevaluate(), M.preevaluate(), Y.preevaluate(),
			 K.preevaluate());
    }
    
    public final Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	return calculate(C.evaluate(parsercontext, attn),
			 M.evaluate(parsercontext, attn),
			 Y.evaluate(parsercontext, attn),
			 K.evaluate(parsercontext, attn));
    }
    
    protected final Attr calculate(Attr attr, Attr attr_3_, Attr attr_4_,
				   Attr attr_5_) throws ParserException {
	if (attr instanceof Expr || attr_3_ instanceof Expr
	    || attr_4_ instanceof Expr || attr_5_ instanceof Expr)
	    return (attr == C && attr_3_ == M && attr_4_ == Y && attr_5_ == K
		    ? this : new CMYK_Color(attr, attr_3_, attr_4_, attr_5_));
	double d = extractColorComponent(attr);
	double d_6_ = extractColorComponent(attr_3_);
	double d_7_ = extractColorComponent(attr_4_);
	double d_8_ = extractColorComponent(attr_5_);
	return Attr.Color.CMYK.create(d, d_6_, d_7_, d_8_);
    }
    
    private double extractColorComponent(Attr attr) throws ParserException {
	double d = 0.0;
	if (attr instanceof Attr.Percentage)
	    d = attr.percentage() / 100.0;
	else if (attr instanceof Attr.Ratio)
	    d = attr.ratio();
	else if (attr instanceof Attr.Count)
	    d = (double) attr.count();
	else
	    throw new ParserException
		      ("Argument of cmyk() function cannot be of type "
		       + attr.getTypeName());
	if (d < 0.0)
	    d = 0.0;
	if (d > 1.0)
	    d = 1.0;
	return d;
    }
}
