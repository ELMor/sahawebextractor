/* UnaryPlus - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserException;

public final class UnaryPlus extends UnaryArithmetic
{
    public UnaryPlus() {
	/* empty */
    }
    
    public UnaryPlus(Attr attr) {
	super(attr);
    }
    
    public String toString() {
	return "+(" + arg + ")";
    }
    
    protected Attr calculate(Attr attr) throws ParserException {
	if (attr instanceof Expr)
	    return attr == arg ? this : new UnaryPlus(attr);
	if (attr instanceof Attr.Numeric)
	    return attr;
	throw new ParserException("Cannot apply unary plus to type "
				  + attr.getTypeName());
    }
}
