/* UnaryMinus - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserException;

public final class UnaryMinus extends UnaryArithmetic
{
    public UnaryMinus() {
	/* empty */
    }
    
    public UnaryMinus(Attr attr) {
	super(attr);
    }
    
    public String toString() {
	return "-(" + arg + ")";
    }
    
    protected Attr calculate(Attr attr) throws ParserException {
	if (attr instanceof Expr)
	    return attr == arg ? this : new UnaryMinus(attr);
	if (attr instanceof Attr.Numeric)
	    return ((Attr.Numeric) attr).minus();
	throw new ParserException("Cannot invert sign of type "
				  + attr.getTypeName());
    }
}
