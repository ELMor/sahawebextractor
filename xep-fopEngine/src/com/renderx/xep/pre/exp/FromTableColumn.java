/* FromTableColumn - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class FromTableColumn extends PropertyValue
{
    public FromTableColumn() {
	/* empty */
    }
    
    public FromTableColumn(Attr attr) {
	super(attr);
    }
    
    protected String getName() {
	return "from-table-column";
    }
    
    protected Attr calculate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	ParserContext parsercontext_0_;
	for (parsercontext_0_ = parsercontext; parsercontext_0_ != null;
	     parsercontext_0_ = parsercontext_0_.prev) {
	    if (parsercontext_0_.element.id == 40)
		break;
	}
	if (parsercontext_0_ == null)
	    throw new ParserException
		      ("from-table-column() is only meaningful inside a table cell");
	ParserContext parsercontext_1_;
	for (parsercontext_1_ = parsercontext_0_.prev;
	     parsercontext_1_ != null;
	     parsercontext_1_ = parsercontext_1_.prev) {
	    if (parsercontext_1_.element.id == 34)
		break;
	}
	if (parsercontext_1_ == null)
	    throw new ParserException
		      ("Bad table structure: table-cell outside table");
	ParserContext.TableColumn tablecolumn
	    = ((ParserContext.TableColumn)
	       parsercontext_1_.tableColumns
		   .get(parsercontext_0_.columnNumber));
	if (tablecolumn == null)
	    return attn.defaultValue;
	Attr attr = (Attr) tablecolumn.context.specified.get(attn);
	if (attr == null)
	    attr = (Attr) tablecolumn.context.inherited.get(attn);
	return attr == null ? attn.defaultValue : attr;
    }
}
