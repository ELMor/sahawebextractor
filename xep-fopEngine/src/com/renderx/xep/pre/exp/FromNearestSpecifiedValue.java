/* FromNearestSpecifiedValue - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class FromNearestSpecifiedValue extends PropertyValue
{
    public FromNearestSpecifiedValue() {
	/* empty */
    }
    
    public FromNearestSpecifiedValue(Attr attr) {
	super(attr);
    }
    
    protected String getName() {
	return "from-nearest-specified-value";
    }
    
    protected Attr calculate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	for (ParserContext parsercontext_0_ = parsercontext.prev;
	     parsercontext_0_ != null;
	     parsercontext_0_ = parsercontext_0_.prev) {
	    Attr attr = (Attr) parsercontext_0_.specified.get(attn);
	    if (attr != null)
		return attr;
	}
	return attn.defaultValue;
    }
}
