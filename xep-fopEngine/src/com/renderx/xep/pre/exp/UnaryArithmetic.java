/* UnaryArithmetic - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public abstract class UnaryArithmetic extends Expr
{
    protected Attr arg = null;
    
    public UnaryArithmetic() {
	this(null);
    }
    
    public UnaryArithmetic(Attr attr) {
	arg = attr;
    }
    
    public abstract String toString();
    
    public final Attr initialize(Attr[] attrs) throws ParserException {
	if (attrs == null)
	    throw new ParserException
		      ("A unary arithmetic expression must have an argument");
	if (attrs.length != 1)
	    throw new ParserException
		      ("Invalid number of arguments in a unary arithmetic expression");
	arg = attrs[0];
	return preevaluate();
    }
    
    public final Attr preevaluate() throws ParserException {
	return calculate(arg.preevaluate());
    }
    
    public final Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	return calculate(arg.evaluate(parsercontext, attn));
    }
    
    protected abstract Attr calculate(Attr attr) throws ParserException;
}
