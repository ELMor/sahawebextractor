/* ZeroArityExpr - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public abstract class ZeroArityExpr extends Expr
{
    public abstract String toString();
    
    public Attr preevaluate() {
	return this;
    }
    
    public final Attr initialize(Attr[] attrs) throws ParserException {
	if (attrs == null)
	    return preevaluate();
	if (attrs.length == 0)
	    return preevaluate();
	throw new ParserException("A zero-arity expression " + toString()
				  + "cannot have arguments");
    }
    
    public abstract Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException;
}
