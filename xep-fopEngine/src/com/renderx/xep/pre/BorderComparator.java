/*
 * BorderComparator - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import com.renderx.util.Hashtable;

public class BorderComparator {
	private static final Hashtable styleRankTable = new Hashtable();

	private static final Hashtable elementRankTable;

	public static final int comparePrecedence(Attr attr, Attr attr_0_) {
		if (attr == attr_0_)
			return 0;
		if (attr == null)
			return -1;
		if (attr_0_ == null)
			return 1;
		if (attr == Attr.force)
			return 1;
		if (attr_0_ == Attr.force)
			return -1;
		if (attr instanceof Attr.Count && attr_0_ instanceof Attr.Count)
			return attr.count() - attr_0_.count();
		if (attr instanceof Attr.Count)
			return 1;
		if (attr_0_ instanceof Attr.Count)
			return -1;
		return 0;
	}

	public static final int compareWidthAndStyle(Attr attr, Attr attr_1_,
			Attr attr_2_, Attr attr_3_) {
		boolean bool = attr_1_ == null || attr_1_ == Attr.none;
		boolean bool_4_ = attr_3_ == null || attr_3_ == Attr.none;
		if (bool & bool_4_)
			return 0;
		if (bool)
			return -1;
		if (bool_4_)
			return 1;
		if (attr_1_ == Attr.hidden && attr_3_ == Attr.hidden)
			return 0;
		if (attr_1_ == Attr.hidden)
			return 1;
		if (attr_3_ == Attr.hidden)
			return -1;
		if (attr == attr_2_)
			return compareStyle(attr_1_, attr_3_);
		if (attr == null && attr_2_ != null)
			return 1;
		if (attr != null && attr_2_ == null)
			return -1;
		if (attr instanceof Attr.Length && attr_2_ instanceof Attr.Length) {
			int i = attr.length() - attr_2_.length();
			return i == 0 ? compareStyle(attr_1_, attr_3_) : i;
		}
		if (attr instanceof Attr.Length)
			return 1;
		if (attr_2_ instanceof Attr.Length)
			return -1;
		return compareStyle(attr_1_, attr_3_);
	}

	public static final int compareStyle(Attr attr, Attr attr_5_) {
		if (attr == attr_5_)
			return 0;
		if (attr == null)
			return -1;
		if (attr_5_ == null)
			return 1;
		if (attr instanceof Attr.Word && attr_5_ instanceof Attr.Word) {
			Integer integer = (Integer) styleRankTable.get(attr.word());
			Integer integer_6_ = (Integer) styleRankTable.get(attr_5_.word());
			if (integer == null && integer_6_ == null)
				return 0;
			if (integer_6_ == null)
				return 1;
			if (integer == null)
				return -1;
			return integer.intValue() - integer_6_.intValue();
		}
		if (attr instanceof Attr.Word)
			return 1;
		if (attr_5_ instanceof Attr.Word)
			return -1;
		return 0;
	}

	public static final int compareElementClass(Attr attr, Attr attr_7_) {
		if (attr == attr_7_)
			return 0;
		if (attr == null)
			return -1;
		if (attr_7_ == null)
			return 1;
		if (attr instanceof Attr.Word && attr_7_ instanceof Attr.Word) {
			Integer integer = (Integer) elementRankTable.get(attr.word());
			Integer integer_8_ = (Integer) elementRankTable.get(attr_7_.word());
			if (integer == null && integer_8_ == null)
				return 0;
			if (integer_8_ == null)
				return 1;
			if (integer == null)
				return -1;
			return integer.intValue() - integer_8_.intValue();
		}
		if (attr instanceof Attr.Word)
			return 1;
		if (attr_7_ instanceof Attr.Word)
			return -1;
		return 0;
	}

	static {
		styleRankTable.put("none", new Integer(0));
		styleRankTable.put("inset", new Integer(1));
		styleRankTable.put("groove", new Integer(2));
		styleRankTable.put("outset", new Integer(3));
		styleRankTable.put("ridge", new Integer(4));
		styleRankTable.put("dotted", new Integer(5));
		styleRankTable.put("dashed", new Integer(6));
		styleRankTable.put("solid", new Integer(7));
		styleRankTable.put("double", new Integer(8));
		styleRankTable.put("hidden", new Integer(9));
		elementRankTable = new Hashtable();
		elementRankTable.put("none", new Integer(0));
		elementRankTable.put("table", new Integer(1));
		elementRankTable.put("column", new Integer(2));
		elementRankTable.put("footer", new Integer(3));
		elementRankTable.put("header", new Integer(4));
		elementRankTable.put("body", new Integer(5));
		elementRankTable.put("row", new Integer(6));
		elementRankTable.put("cell", new Integer(7));
	}
}