/*
 * ElementHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

public interface ElementHandler {
	public void startElement(short i, AttList attlist) throws CompilerException;

	public void endElement(short i) throws CompilerException;
}