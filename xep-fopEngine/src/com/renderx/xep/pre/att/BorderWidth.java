/* BorderWidth - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class BorderWidth extends Typed
{
    private ColorSpec sideColor;
    
    public BorderWidth(String string, String string_0_, String string_1_)
	throws ParserException {
	super(string, string_0_, 8);
	this.addAlias("thin", "0.5pt");
	this.addAlias("medium", "1.5pt");
	this.addAlias("thick", "4.5pt");
	sideColor = (ColorSpec) Attn.byName(string_1_);
    }
    
    public void process(Attr attr, ParserContext parsercontext)
	throws ParserException {
	if (parsercontext.specified.get(sideColor) == null)
	    sideColor.process(parsercontext.color, parsercontext);
	super.process(attr, parsercontext);
    }
}
