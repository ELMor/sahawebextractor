/* FontFamily - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import java.util.Enumeration;

import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.Token;

public class FontFamily extends Attn
{
    public FontFamily(String string, String string_0_) {
	super(string);
	defaultValue = Attr.Word.create(string_0_);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	java.util.Stack stack = AttributeParser.parseShorthand(string);
	Enumeration enumeration = stack.elements();
	String string_1_ = "";
	while (enumeration.hasMoreElements()) {
	    Token token = (Token) enumeration.nextElement();
	    if (token.type == 8)
		string_1_ += ",";
	    else if (token.type != 8192)
		parsercontext.session.warning("Unexpected token in " + name
					      + ": " + token.getTypeName());
	    else
		string_1_
		    += token.value.evaluate(parsercontext, this).toString();
	}
	parsercontext.addAttribute(this, Attr.Word.create(string_1_));
    }
}
