/* PageSize - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.Token;

public class PageSize extends Attn
{
    private final Parsed width = (Parsed) Attn.$page_width;
    private final Parsed height = (Parsed) Attn.$page_height;
    
    public PageSize(String string) throws ParserException {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	if (string.trim().equals("inherit")) {
	    width.process("inherit", parsercontext);
	    height.process("inherit", parsercontext);
	} else {
	    Object object = null;
	    Object object_0_ = null;
	    java.util.Stack stack = AttributeParser.parseShorthand(string);
	    switch (stack.size()) {
	    case 1: {
		Token token = (Token) stack.elementAt(0);
		if (token.type != 8192)
		    throw new ParserException("Incorrect shorthand structure: "
					      + name + "=\"" + string + "\"");
		if (token.value instanceof Attr.Word) {
		    String string_1_ = token.value.word();
		    boolean bool
			= (parsercontext.session.config.PAGE_WIDTH
			   < parsercontext.session.config.PAGE_HEIGHT);
		    if (string_1_.equals("portrait")) {
			width.process((bool ? "default-page-width()"
				       : "default-page-height()"),
				      parsercontext);
			height.process((bool ? "default-page-height()"
					: "default-page-width()"),
				       parsercontext);
			break;
		    }
		    if (string_1_.equals("landscape")) {
			width.process((bool ? "default-page-height()"
				       : "default-page-width()"),
				      parsercontext);
			height.process((bool ? "default-page-width()"
					: "default-page-height()"),
				       parsercontext);
			break;
		    }
		}
		width.process(token.value, parsercontext);
		height.process(token.value, parsercontext);
		break;
	    }
	    case 2: {
		Token token = (Token) stack.elementAt(0);
		Token token_2_ = (Token) stack.elementAt(1);
		if (token.type != 8192 || token_2_.type != 8192)
		    throw new ParserException("Incorrect shorthand structure: "
					      + name + "=\"" + string + "\"");
		width.process(token.value, parsercontext);
		height.process(token_2_.value, parsercontext);
		break;
	    }
	    default:
		throw new ParserException("Too many tokens in shorthand: "
					  + name + "=\"" + string + "\"");
	    }
	}
    }
}
