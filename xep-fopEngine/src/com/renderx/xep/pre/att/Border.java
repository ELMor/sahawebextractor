/* Border - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class Border extends Attn
{
    private Attn top;
    private Attn bottom;
    private Attn left;
    private Attn right;
    
    public Border(String string) throws ParserException {
	super(string);
	top = Attn.byName(string + "-top");
	bottom = Attn.byName(string + "-bottom");
	right = Attn.byName(string + "-right");
	left = Attn.byName(string + "-left");
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	top.process(string, parsercontext);
	bottom.process(string, parsercontext);
	right.process(string, parsercontext);
	left.process(string, parsercontext);
    }
}
