/* ReferenceOrientation - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class ReferenceOrientation extends Typed
{
    public ReferenceOrientation(String string) throws ParserException {
	super(string, "0", 1);
    }
    
    protected Attr normalize(Attr attr, ParserContext parsercontext)
	throws ParserException {
	int i = super.normalize(attr, parsercontext).count();
	int i_0_ = i % 360;
	switch (i_0_) {
	case 0:
	case 90:
	case 180:
	case 270:
	    break;
	case -270:
	case -180:
	case -90:
	    i_0_ += 360;
	    break;
	default:
	    throw new ParserException
		      ("Invalid value for reference orientation: " + i
		       + " (not a multiple of 90)");
	}
	parsercontext.refOrientation = i_0_;
	return Attr.Count.create(i_0_);
    }
}
