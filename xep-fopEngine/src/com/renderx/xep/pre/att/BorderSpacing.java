/* BorderSpacing - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.Token;

public class BorderSpacing extends Attn implements Alias
{
    private final Parsed ip
	= (Parsed) Attn.$border_separation_inline_progression_direction;
    private final Parsed bp
	= (Parsed) Attn.$border_separation_block_progression_direction;
    
    public BorderSpacing(String string) throws ParserException {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	if (string.trim().equals("inherit")) {
	    ip.process("inherit", parsercontext);
	    bp.process("inherit", parsercontext);
	} else {
	    Object object = null;
	    Object object_0_ = null;
	    java.util.Stack stack = AttributeParser.parseShorthand(string);
	    switch (stack.size()) {
	    case 1: {
		Token token = (Token) stack.elementAt(0);
		if (token.type != 8192)
		    throw new ParserException("Incorrect shorthand structure: "
					      + name + "=\"" + string + "\"");
		ip.process(token.value, parsercontext);
		bp.process(token.value, parsercontext);
		break;
	    }
	    case 2: {
		Token token = (Token) stack.elementAt(0);
		Token token_1_ = (Token) stack.elementAt(1);
		if (token.type != 8192 || token_1_.type != 8192)
		    throw new ParserException("Incorrect shorthand structure: "
					      + name + "=\"" + string + "\"");
		ip.process(token.value, parsercontext);
		bp.process(token_1_.value, parsercontext);
		break;
	    }
	    default:
		throw new ParserException("Too many tokens in shorthand: "
					  + name + "=\"" + string + "\"");
	    }
	}
    }
    
    public Attn resolveAlias(ParserContext parsercontext) {
	return bp;
    }
}
