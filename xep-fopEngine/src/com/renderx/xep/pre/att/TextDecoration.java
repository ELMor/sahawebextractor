/* TextDecoration - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import java.util.Enumeration;

import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.Token;

public class TextDecoration extends Attn
{
    public TextDecoration(String string, String string_0_) {
	super(string);
	if (string_0_.equals("none"))
	    string_0_ = "";
	defaultValue = Attr.Word.create(string_0_);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	boolean bool = false;
	boolean bool_1_ = false;
	boolean bool_2_ = false;
	boolean bool_3_ = false;
	Attr attr = (Attr) parsercontext.inherited.get(this);
	if (attr != null) {
	    String string_4_ = attr.word();
	    bool = string_4_.indexOf("underline") != -1;
	    bool_1_ = string_4_.indexOf("overline") != -1;
	    bool_2_ = string_4_.indexOf("line-through") != -1;
	    bool_3_ = string_4_.indexOf("blink") != -1;
	}
	java.util.Stack stack = AttributeParser.parseShorthand(string);
	Enumeration enumeration = stack.elements();
	while (enumeration.hasMoreElements()) {
	    Token token = (Token) enumeration.nextElement();
	    if (token.type != 8192)
		parsercontext.session.warning("Unexpected token in " + name
					      + ": " + token.getTypeName());
	    else {
		Attr attr_5_ = token.value.evaluate(parsercontext, this);
		if (!(attr_5_ instanceof Attr.Word))
		    parsercontext.session.warning("Unexpected token in " + name
						  + ": " + attr_5_.toString());
		else {
		    String string_6_ = attr_5_.word();
		    if (stack.size() == 1 && "none".equals(string_6_)) {
			parsercontext.inherited.remove(this);
			return;
		    }
		    if ("no-underline".equals(string_6_))
			bool = false;
		    else if (string_6_.indexOf("underline") != -1)
			bool = true;
		    if ("no-overline".equals(string_6_))
			bool_1_ = false;
		    else if (string_6_.indexOf("overline") != -1)
			bool_1_ = true;
		    if ("no-line-through".equals(string_6_))
			bool_2_ = false;
		    else if (string_6_.indexOf("line-through") != -1)
			bool_2_ = true;
		    if ("no-blink".equals(string_6_))
			bool_3_ = false;
		    else if (string_6_.indexOf("blink") != -1)
			bool_3_ = true;
		}
	    }
	}
	String string_7_
	    = ((bool ? "underline " : "") + (bool_1_ ? "overline " : "")
	       + (bool_2_ ? "line-through " : "") + (bool_3_ ? "blink " : ""));
	parsercontext.addAttribute(this, Attr.Word.create(string_7_));
    }
}
