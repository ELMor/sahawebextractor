/* FontSize - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class FontSize extends Typed implements ResolvablePercents
{
    public FontSize(String string, String string_0_) throws ParserException {
	super(string, string_0_, 8);
	this.addAlias("xx-small", "6.94pt");
	this.addAlias("x-small", "8.33pt");
	this.addAlias("small", "10pt");
	this.addAlias("medium", "12pt");
	this.addAlias("large", "14.4pt");
	this.addAlias("x-large", "17.28pt");
	this.addAlias("xx-large", "20.74pt");
	this.addAlias("smaller", "83.3%");
	this.addAlias("larger", "120%");
    }
    
    public Attr.Scalable getPercentBase(ParserContext parsercontext) {
	return parsercontext.fontSize;
    }
    
    protected Attr normalize(Attr attr, ParserContext parsercontext)
	throws ParserException {
	parsercontext.fontSize
	    = (Attr.Length) super.normalize(attr, parsercontext);
	return parsercontext.fontSize;
    }
}
