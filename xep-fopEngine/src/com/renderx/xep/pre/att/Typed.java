/* Typed - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class Typed extends Parsed
{
    int types = 0;
    public static final int INTEGER = 1;
    public static final int REALNUMBER = 2;
    public static final int PERCENTAGE = 4;
    public static final int LENGTH = 8;
    public static final int ANGLE = 16;
    public static final int STRING = 32;
    
    public Typed(String string, String string_0_, int i)
	throws ParserException {
	super(string);
	types = i;
	setDefault(string_0_);
    }
    
    public Typed(String string, String string_1_, int i, String[] strings)
	throws ParserException {
	this(string, string_1_, i);
	this.addKeywords(strings);
    }
    
    protected Attr normalize(Attr attr, ParserContext parsercontext)
	throws ParserException {
	Attr attr_2_ = attr.cast(types);
	if (attr_2_ != null)
	    return attr_2_;
	throw new ParserException("Attribute type mismatch: " + name
				  + " cannot have value '" + attr
				  + "' of type " + attr.getTypeName());
    }
    
    protected void setDefault(String string) throws ParserException {
	defaultValue = AttributeParser.parse(string);
	if (!(defaultValue instanceof Attr.Word))
	    defaultValue = defaultValue.cast(types);
    }
}
