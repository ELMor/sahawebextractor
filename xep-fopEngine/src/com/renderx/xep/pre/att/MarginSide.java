/* MarginSide - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class MarginSide extends Typed
{
    private Attn correspondingIndent = null;
    
    public MarginSide(String string, String string_0_, String string_1_)
	throws ParserException {
	super(string, string_0_, 12);
	correspondingIndent = Attn.byName(string_1_);
    }
    
    public void process(Attr attr, ParserContext parsercontext)
	throws ParserException {
	if (parsercontext.specified.get(correspondingIndent) == null
	    && (!(attr instanceof Attr.Word) || !attr.word().equals("auto"))) {
	    parsercontext.removeAttribute(correspondingIndent);
	    super.process(attr, parsercontext);
	}
    }
}
