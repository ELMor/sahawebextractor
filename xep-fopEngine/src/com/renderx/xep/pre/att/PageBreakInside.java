/* PageBreakInside - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class PageBreakInside extends Attn
{
    public PageBreakInside(String string) throws ParserException {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	if (string.trim().equals("inherit"))
	    Attn.$keep_together_within_column.process("inherit",
						      parsercontext);
	Attr attr
	    = AttributeParser.parse(string).evaluate(parsercontext, this);
	if (!(attr instanceof Attr.Word))
	    throw new ParserException("Invalid property value: " + name + "=\""
				      + string + "\"");
	String string_0_ = attr.word();
	if (string_0_.equals("avoid"))
	    Attn.$keep_together_within_column.process("always", parsercontext);
	else if (!string_0_.equals("auto"))
	    throw new ParserException("Invalid keyword in property value: "
				      + name + "=\"" + string + "\"");
    }
}
