/* BackgroundPosition - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.Token;

public class BackgroundPosition extends Attn
{
    private final Parsed horz = (Parsed) Attn.$background_position_horizontal;
    private final Parsed vert = (Parsed) Attn.$background_position_vertical;
    
    public BackgroundPosition(String string) throws ParserException {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	if (string.trim().equals("inherit")) {
	    horz.process("inherit", parsercontext);
	    vert.process("inherit", parsercontext);
	} else {
	    Object object = null;
	    Object object_0_ = null;
	    java.util.Stack stack = AttributeParser.parseShorthand(string);
	    switch (stack.size()) {
	    case 1: {
		Token token = (Token) stack.elementAt(0);
		if (token.type != 8192)
		    throw new ParserException("Incorrect shorthand structure: "
					      + name + "=\"" + string + "\"");
		horz.process("50%", parsercontext);
		vert.process("50%", parsercontext);
		processToken(token, horz, parsercontext);
		break;
	    }
	    case 2: {
		Token token = (Token) stack.elementAt(0);
		Token token_1_ = (Token) stack.elementAt(1);
		if (token.type != 8192 || token_1_.type != 8192)
		    throw new ParserException("Incorrect shorthand structure: "
					      + name + "=\"" + string + "\"");
		if (token.value instanceof Attr.Word
		    != token_1_.value instanceof Attr.Word)
		    throw new ParserException("Incorrect shorthand structure: "
					      + name + "=\"" + string + "\"");
		horz.process("50%", parsercontext);
		vert.process("50%", parsercontext);
		processToken(token, horz, parsercontext);
		processToken(token_1_, vert, parsercontext);
		break;
	    }
	    default:
		throw new ParserException("Too many tokens in shorthand: "
					  + name + "=\"" + string + "\"");
	    }
	}
    }
    
    private void processToken
	(Token token, Parsed parsed, ParserContext parsercontext)
	throws ParserException {
	if (token.value instanceof Attr.Word) {
	    String string = token.value.word();
	    if (!"center".equals(string)) {
		if ("top".equals(string))
		    vert.process("0%", parsercontext);
		else if ("bottom".equals(string))
		    vert.process("100%", parsercontext);
		else if ("left".equals(string))
		    horz.process("0%", parsercontext);
		else if ("right".equals(string))
		    horz.process("100%", parsercontext);
		else
		    parsercontext.session.warning
			("Ignored unexpected token in " + name + ": \""
			 + string + "\"");
	    }
	} else
	    parsed.process(token.value, parsercontext);
    }
}
