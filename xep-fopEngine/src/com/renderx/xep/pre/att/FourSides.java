/* FourSides - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.Token;

public class FourSides extends Attn
{
    private AbsoluteSide top;
    private AbsoluteSide bottom;
    private AbsoluteSide left;
    private AbsoluteSide right;
    
    public FourSides(String string, String string_0_) throws ParserException {
	super(string);
	int i = string_0_.indexOf('*');
	if (i == -1)
	    throw new ParserException
		      ("The third argument to create an AbsoluteSide must contain a wildcard character");
	String string_1_ = string_0_.substring(0, i);
	String string_2_ = string_0_.substring(i + 1);
	top = (AbsoluteSide) Attn.byName(string_1_ + "top" + string_2_);
	bottom = (AbsoluteSide) Attn.byName(string_1_ + "bottom" + string_2_);
	right = (AbsoluteSide) Attn.byName(string_1_ + "right" + string_2_);
	left = (AbsoluteSide) Attn.byName(string_1_ + "left" + string_2_);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	java.util.Stack stack = AttributeParser.parseShorthand(string);
	switch (stack.size()) {
	case 1: {
	    Token token = (Token) stack.elementAt(0);
	    if (token.type != 8192)
		throw new ParserException("Incorrect shorthand structure: "
					  + name + "=\"" + string + "\"");
	    top.process(token.value, parsercontext);
	    bottom.process(token.value, parsercontext);
	    right.process(token.value, parsercontext);
	    left.process(token.value, parsercontext);
	    break;
	}
	case 2: {
	    Token token = (Token) stack.elementAt(0);
	    Token token_3_ = (Token) stack.elementAt(1);
	    if (token.type != 8192 || token_3_.type != 8192)
		throw new ParserException("Incorrect shorthand structure: "
					  + name + "=\"" + string + "\"");
	    top.process(token.value, parsercontext);
	    bottom.process(token.value, parsercontext);
	    left.process(token_3_.value, parsercontext);
	    right.process(token_3_.value, parsercontext);
	    break;
	}
	case 3: {
	    Token token = (Token) stack.elementAt(0);
	    Token token_4_ = (Token) stack.elementAt(1);
	    Token token_5_ = (Token) stack.elementAt(2);
	    if (token.type != 8192 || token_4_.type != 8192
		|| token_5_.type != 8192)
		throw new ParserException("Incorrect shorthand structure: "
					  + name + "=\"" + string + "\"");
	    top.process(token.value, parsercontext);
	    left.process(token_4_.value, parsercontext);
	    right.process(token_4_.value, parsercontext);
	    bottom.process(token_5_.value, parsercontext);
	    break;
	}
	case 4: {
	    Token token = (Token) stack.elementAt(0);
	    Token token_6_ = (Token) stack.elementAt(1);
	    Token token_7_ = (Token) stack.elementAt(2);
	    Token token_8_ = (Token) stack.elementAt(3);
	    if (token.type != 8192 || token_6_.type != 8192
		|| token_7_.type != 8192 || token_8_.type != 8192)
		throw new ParserException("Incorrect shorthand structure: "
					  + name + "=\"" + string + "\"");
	    top.process(token.value, parsercontext);
	    right.process(token_6_.value, parsercontext);
	    bottom.process(token_7_.value, parsercontext);
	    left.process(token_8_.value, parsercontext);
	    break;
	}
	default:
	    throw new ParserException("Too many tokens in shorthand: " + name
				      + "=\"" + string + "\"");
	}
    }
}
