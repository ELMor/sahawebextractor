/* AbsoluteSide - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.WritingMode;

public class AbsoluteSide extends Attn implements Alias
{
    private Parsed[] substitutions = new Parsed[4];
    private int side = -1;
    
    public Attn resolveAlias(ParserContext parsercontext) {
	WritingMode writingmode = parsercontext.activeMode;
	if (parsercontext.element.changeWritingMode
	    && parsercontext.prev != null)
	    writingmode = parsercontext.prev.activeMode;
	return substitutions[writingmode.sides[side]];
    }
    
    public AbsoluteSide(String string, int i, String string_0_)
	throws ParserException {
	super(string);
	side = i;
	int i_1_ = string_0_.indexOf('*');
	if (i_1_ == -1)
	    throw new ParserException
		      ("The third argument to create an AbsoluteSide must contain a wildcard character");
	String string_2_ = string_0_.substring(0, i_1_);
	String string_3_ = string_0_.substring(i_1_ + 1);
	substitutions[0]
	    = (Parsed) Attn.byName(string_2_ + "start" + string_3_);
	substitutions[1] = (Parsed) Attn.byName(string_2_ + "end" + string_3_);
	substitutions[2]
	    = (Parsed) Attn.byName(string_2_ + "before" + string_3_);
	substitutions[3]
	    = (Parsed) Attn.byName(string_2_ + "after" + string_3_);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	((Parsed) resolveAlias(parsercontext)).process(string, parsercontext);
    }
    
    public void process(Attr attr, ParserContext parsercontext)
	throws ParserException {
	((Parsed) resolveAlias(parsercontext)).process(attr, parsercontext);
    }
}
