/* WritingModeSpec - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class WritingModeSpec extends Enumerated
{
    public WritingModeSpec(String string, String string_0_)
	throws ParserException {
	super(string, string_0_,
	      new String[] { "lr-tb", "rl-tb", "tb-lr", "tb-rl", "lr-bt",
			     "rl-bt", "bt-lr", "bt-rl" });
	this.addAlias("lr", "lr-tb");
	this.addAlias("rl", "rl-tb");
	this.addAlias("tb", "tb-rl");
    }
    
    public void process(Attr attr, ParserContext parsercontext)
	throws ParserException {
	attr = attr.evaluate(parsercontext, this);
	Attr attr_1_ = (Attr) aliases.get(attr);
	if (attr_1_ != null)
	    attr = attr_1_.evaluate(parsercontext, this);
	if (attr instanceof Expr)
	    throw new ParserException("Could not evaluate expression "
				      + attr.toString() + " in attribute "
				      + name);
	if (keywords.containsKey(attr)) {
	    if (!attr.word().equals("lr-tb") && !attr.word().equals("rl-tb")) {
		parsercontext.session.info
		    ("Writing mode '" + attr.word()
		     + "' not yet supported; defaulting to "
		     + Attn.$writing_mode.defaultValue.word());
		attr = Attn.$writing_mode.defaultValue;
	    }
	    parsercontext.addAttribute(this, attr);
	    parsercontext.setWritingMode(attr.word());
	} else
	    this.normalize(attr, parsercontext);
    }
}
