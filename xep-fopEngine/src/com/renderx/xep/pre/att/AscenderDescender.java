/* AscenderDescender - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class AscenderDescender extends Typed implements ResolvablePercents
{
    public AscenderDescender(String string, String string_0_)
	throws ParserException {
	super(string, string_0_, 8);
	this.addKeyword("use-font-metrics");
    }
    
    public Attr.Scalable getPercentBase(ParserContext parsercontext) {
	return parsercontext.fontSize;
    }
}
