/* ResolvablePercents - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public interface ResolvablePercents
{
    public Attr.Scalable getPercentBase(ParserContext parsercontext)
	throws ParserException;
}
