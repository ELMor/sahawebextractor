/* Literal - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class Literal extends Attn
{
    public Literal(String string) {
	super(string);
	defaultValue = null;
    }
    
    public Literal(String string, String string_0_) throws ParserException {
	this(string);
	defaultValue = Attr.Word.create(string_0_);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	parsercontext.addAttribute(this, Attr.Word.create(string));
    }
}
