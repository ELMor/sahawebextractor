/* ProgressionDimension - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class ProgressionDimension extends Typed implements ResolvablePercents
{
    public Attn perpendicular;
    
    public ProgressionDimension(String string, String string_0_)
	throws ParserException {
	super(string, string_0_, 44);
    }
    
    public Attr.Scalable getPercentBase(ParserContext parsercontext)
	throws ParserException {
	ParserContext parsercontext_1_;
	for (parsercontext_1_ = parsercontext.prev;
	     parsercontext_1_ != null && !parsercontext_1_.element.isSizedArea;
	     parsercontext_1_ = parsercontext_1_.prev) {
	    /* empty */
	}
	if (parsercontext_1_ == null || parsercontext_1_.prev == null)
	    return null;
	boolean bool = (parsercontext_1_.prev.activeMode.isHorizontal
			== parsercontext.prev.activeMode.isHorizontal);
	boolean bool_2_ = parsercontext_1_.refOrientation % 180 == 0;
	Attn attn = bool == bool_2_ ? (Attn) this : perpendicular;
	Attr attr = (Attr) parsercontext_1_.specified.get(attn);
	if (attr instanceof Attr.Length)
	    return (Attr.Length) attr;
	return null;
    }
}
