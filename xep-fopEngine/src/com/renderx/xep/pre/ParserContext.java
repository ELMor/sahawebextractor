/*
 * ParserContext - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import com.renderx.util.Array;
import com.renderx.util.Hashtable;
import com.renderx.util.URLSpec;
import com.renderx.xep.lib.Session;

public class ParserContext implements Cloneable {
	public final Session session;

	public URLSpec baseURL = null;

	public Elem.Descriptor element = null;

	public WritingMode activeMode = null;

	public WritingMode inheritedMode = null;

	public Hashtable specified = new Hashtable();

	public Hashtable inherited = null;

	private boolean independent = false;

	public Attr.Length fontSize = (Attr.Length) Attn.$font_size.defaultValue;

	public Attr.Color color = (Attr.Color) Attn.$color.defaultValue;

	public int refOrientation = 0;

	public Attr direction = Attn.$direction.defaultValue;

	public boolean bidiOverride = false;

	public int listid = 0;

	public boolean extraFlowSection = false;

	public boolean extraTableRow = false;

	public Hashtable markerInfo = null;

	public Array busyCells = null;

	public Array tableColumns = null;

	public int nextColumn;

	public int tableid;

	public int bodyNumber;

	public int rowNumber;

	public int columnNumber;

	public Attr proportionalColumnWidth = null;

	public boolean endsRow;

	public boolean collapseBorders = true;

	public boolean useCollapsePrecedence = false;

	public final ParserContext prev;

	public ParserContext next = null;

	public static class TableColumn {
		public int colspan = 1;

		public ParserContext context = null;

		public TableColumn(int i, ParserContext parsercontext) {
			colspan = i;
			context = (ParserContext) parsercontext.clone();
		}
	}

	public ParserContext(ParserContext parsercontext_0_, Session session) {
		prev = parsercontext_0_;
		this.session = session;
		if (parsercontext_0_ != null)
			parsercontext_0_.next = this;
	}

	public Object clone() {
		ParserContext parsercontext_1_ = new ParserContext(null, session);
		parsercontext_1_.baseURL = baseURL;
		parsercontext_1_.element = element;
		parsercontext_1_.activeMode = activeMode;
		parsercontext_1_.inheritedMode = inheritedMode;
		parsercontext_1_.refOrientation = refOrientation;
		parsercontext_1_.bidiOverride = bidiOverride;
		parsercontext_1_.direction = direction;
		parsercontext_1_.fontSize = fontSize;
		parsercontext_1_.color = color;
		parsercontext_1_.listid = listid;
		parsercontext_1_.extraFlowSection = extraFlowSection;
		parsercontext_1_.extraTableRow = extraTableRow;
		parsercontext_1_.nextColumn = nextColumn;
		parsercontext_1_.endsRow = endsRow;
		parsercontext_1_.collapseBorders = collapseBorders;
		parsercontext_1_.useCollapsePrecedence = useCollapsePrecedence;
		parsercontext_1_.specified = specified == null ? null
				: (Hashtable) specified.clone();
		parsercontext_1_.inherited = inherited == null ? null
				: (Hashtable) inherited.clone();
		parsercontext_1_.markerInfo = markerInfo == null ? null
				: (Hashtable) markerInfo.clone();
		parsercontext_1_.tableColumns = tableColumns == null ? null
				: (Array) tableColumns.clone();
		parsercontext_1_.busyCells = busyCells == null ? null
				: (Array) busyCells.clone();
		parsercontext_1_.independent = true;
		parsercontext_1_.next = null;
		return parsercontext_1_;
	}

	public void initialize(Elem.Descriptor descriptor, String string)
			throws ParserException {
		element = descriptor;
		if (prev == null) {
			inherited = new Hashtable();
			independent = true;
			activeMode = inheritedMode = WritingMode.$defaultmode;
		} else {
			baseURL = prev.baseURL;
			ParserContext parsercontext_2_ = prev;
			if (element.isOutOfLine && session.config.XEP2_COMPATIBLE_MODE) {
				while (parsercontext_2_.element.id != 19) {
					if ((parsercontext_2_ = parsercontext_2_.prev) == null) {
						parsercontext_2_ = prev;
						break;
					}
				}
			} else if (element.id == 50) {
				if (string == null)
					throw new ParserException(
							"fo:marker element has no marker-class-name attribute; element ignored");
				ParserContext parsercontext_3_ = prev;
				while (parsercontext_3_.element.id != 16) {
					if ((parsercontext_3_ = parsercontext_3_.prev) == null)
						throw new ParserException(
								"fo:marker element outside fo:page-sequence; element ignored");
				}
				if (string.startsWith("[ghost]"))
					string = string.substring("[ghost]".length());
				parsercontext_2_ = (ParserContext) parsercontext_3_.markerInfo
						.get(string);
				if (parsercontext_2_ == null)
					throw new ParserException(
							"fo:marker element with no corresponding fo:retrieve_marker; element ignored");
			}
			inherited = parsercontext_2_.inherited;
			independent = false;
			listid = parsercontext_2_.listid;
			nextColumn = 1;
			fontSize = parsercontext_2_.fontSize;
			color = parsercontext_2_.color;
			extraFlowSection = false;
			extraTableRow = false;
			endsRow = false;
			collapseBorders = parsercontext_2_.collapseBorders;
			useCollapsePrecedence = parsercontext_2_.useCollapsePrecedence;
			if (parsercontext_2_.element.resetIndents
					&& session.config.XEP2_COMPATIBLE_MODE
					&& (inherited.containsKey(Attn.$start_indent) || inherited
							.containsKey(Attn.$end_indent))) {
				inherited = (Hashtable) inherited.clone();
				independent = true;
				inherited.remove(Attn.$start_indent);
				inherited.remove(Attn.$end_indent);
			}
			inheritedMode = parsercontext_2_.inheritedMode;
			if (element.changeWritingMode) {
				activeMode = inheritedMode;
				bidiOverride = false;
				direction = activeMode.direction;
			} else {
				activeMode = parsercontext_2_.activeMode;
				bidiOverride = parsercontext_2_.bidiOverride;
				direction = parsercontext_2_.direction;
			}
			if (element.id == 16)
				markerInfo = new Hashtable();
			if (element.id == 34)
				tableColumns = new Array();
			if (element.id == 38 || element.id == 36 || element.id == 37)
				busyCells = new Array();
		}
	}

	public void clear() {
		baseURL = null;
		activeMode = inheritedMode = null;
		if (independent)
			inherited.clear();
		inherited = null;
		independent = false;
		specified.clear();
		markerInfo = null;
		tableColumns = null;
		busyCells = null;
		nextColumn = 1;
		refOrientation = 0;
		bidiOverride = false;
	}

	public void addAttribute(Attn attn, Attr attr) throws ParserException {
		specified.put(attn, attr);
		if (attn.inheritable) {
			if (!independent) {
				inherited = (Hashtable) inherited.clone();
				independent = true;
			}
			inherited.put(attn, attr);
		}
	}

	public void removeAttribute(Attn attn) throws ParserException {
		specified.remove(attn);
		if (inherited.get(attn) != null) {
			if (!independent) {
				inherited = (Hashtable) inherited.clone();
				independent = true;
			}
			inherited.remove(attn);
		}
	}

	public void setWritingMode(String string) throws ParserException {
		inheritedMode = WritingMode.byName(string);
		if (element.changeWritingMode) {
			activeMode = inheritedMode;
			bidiOverride = false;
			direction = activeMode.direction;
		}
	}

	public void decreaseBusyCells() throws ParserException {
		if (busyCells == null)
			throw new ParserException("Misplaced table row element ignored");
		for (int i = 0; i < busyCells.length(); i++) {
			Integer integer = (Integer) busyCells.get(i);
			if (integer != null) {
				int i_4_ = integer.intValue();
				busyCells.put(i, (i_4_ <= 1 ? (Integer) null : new Integer(
						i_4_ - 1)));
			}
		}
	}

	public void setCollapsedBorderModel(String string) throws ParserException {
		collapseBorders = true;
		useCollapsePrecedence = false;
		if ("separate".equals(string))
			collapseBorders = false;
		else if ("collapse-with-precedence".equals(string))
			useCollapsePrecedence = true;
	}

	public void copySpecifiedValue(Attn attn, Attn attn_5_) {
		Attr attr = (Attr) specified.get(attn);
		if (attr != null)
			specified.put(attn_5_, attr);
	}
}