/*
 * AttributeParser - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Stack;
import java.util.Vector;

import com.renderx.util.Hashtable;
import com.renderx.xep.lib.InternalException;

public class AttributeParser {
	private static Hashtable parserCache = new Hashtable();

	private static Hashtable shorthandCache = new Hashtable();

	private static Vector productions = new Vector();

	private static final Token T_LeftPar = new Token(2);

	private static final Token T_RightPar = new Token(4);

	private static final Token T_Comma = new Token(8);

	private static final Token T_Plus = new Token(16);

	private static final Token T_Minus = new Token(32);

	private static final Token T_Asterisk = new Token(64);

	private static final Token T_Slash = new Token(128);

	private static final Token T_Divide = new Token(256);

	private static final Token T_Modulo = new Token(512);

	private static final Token T_UnaryPlus = new Token(1024);

	private static final Token T_UnaryMinus = new Token(2048);

	private static final Token T_Expr = new Token(8192);

	private static boolean initialized = false;

	private static class Production {
		public Class generator;

		protected Token[] match;

		protected boolean[] mask;

		protected int arity = 0;

		protected int inhibitors = 0;

		public Production(Class var_class, Token[] tokens, int i)
				throws InternalException {
			generator = var_class;
			match = tokens;
			inhibitors = i;
			mask = new boolean[tokens.length];
			for (int i_0_ = 0; i_0_ < tokens.length; i_0_++) {
				if (tokens[i_0_].type == 8192) {
					mask[i_0_] = true;
					arity++;
				} else
					mask[i_0_] = false;
			}
		}

		public boolean reduce(Stack stack, Token token) throws ParserException {
			if ((inhibitors & token.type) > 0)
				return false;
			if (stack.size() < match.length)
				return false;
			int i = stack.size() - match.length;
			for (int i_1_ = 0; i_1_ < match.length; i_1_++) {
				Token token_2_ = (Token) stack.elementAt(i + i_1_);
				Token token_3_ = match[i_1_];
				if ((token_2_.type & token_3_.type) == 0)
					return false;
				if (token_3_.value != null && token_2_.value != token_3_.value)
					return false;
			}
			Attr[] attrs = null;
			if (arity > 0) {
				attrs = new Attr[arity];
				int i_4_ = 0;
				for (int i_5_ = 0; i_5_ < mask.length; i_5_++) {
					if (mask[i_5_])
						attrs[i_4_++] = ((Token) stack.elementAt(i + i_5_)).value;
				}
			}
			Object object = null;
			Expr expr;
			try {
				expr = (Expr) generator.newInstance();
			} catch (InstantiationException instantiationexception) {
				throw new ParserException("Cannot instantiate expression: "
						+ instantiationexception.toString());
			} catch (IllegalAccessException illegalaccessexception) {
				throw new ParserException(
						"Cannot access constructor for an expression: "
								+ illegalaccessexception.toString());
			}
			Attr attr = expr.initialize(attrs);
			stack.setSize(i + 1);
			stack.setElementAt(new Token(8192, attr), i);
			return true;
		}
	}

	private static synchronized Attr getCached(String string) {
		return (Attr) parserCache.get(string);
	}

	private static synchronized void cache(String string, Attr attr) {
		parserCache.put(string, attr);
	}

	private static synchronized Stack getCachedStack(String string) {
		return (Stack) shorthandCache.get(string);
	}

	private static synchronized void cacheStack(String string, Stack stack) {
		shorthandCache.put(string, stack);
	}

	private static Stack makeStack(String string, boolean bool)
			throws ParserException {
		Stack stack = new Stack();
		AttributeLexer attributelexer = new AttributeLexer(string, bool);
		Token token = attributelexer.nextToken();
		while (token.type != 1) {
			stack.push(token);
			token = attributelexer.nextToken();
			Enumeration enumeration = productions.elements();
			while (enumeration.hasMoreElements()) {
				Production production = (Production) enumeration.nextElement();
				if (production.reduce(stack, token))
					enumeration = productions.elements();
			}
		}
		if (stack.empty())
			throw new ParserException(
					"No meaningful characters in attribute value");
		return stack;
	}

	public static Attr parse(String string) throws ParserException {
		Attr attr = getCached(string);
		if (attr != null)
			return attr;
		Stack stack = makeStack(
				(string.trim().equals("inherit") ? "from-parent()" : string),
				false);
		if (stack.size() > 1)
			throw new ParserException("Could not parse attribute value \""
					+ string + "\"");
		Token token = (Token) stack.pop();
		if (token.type != 8192)
			throw new ParserException(
					"Attribute reduced to an unexpected token: "
							+ token.getTypeName());
		cache(string, token.value);
		return token.value;
	}

	public static Stack parseShorthand(String string) throws ParserException {
		Stack stack = getCachedStack(string);
		if (stack != null)
			return stack;
		stack = makeStack(string, true);
		cacheStack(string, stack);
		return stack;
	}

	private static void createProduction(Class var_class, Token[] tokens)
			throws InternalException {
		createProduction(var_class, tokens, 0);
	}

	private static void createProduction(Class var_class, Token[] tokens, int i)
			throws InternalException {
		productions.addElement(new Production(var_class, tokens, i));
	}

	private static void createFunction(Class var_class, String string, int i)
			throws InternalException {
		Token token = new Token(4096, Attr.Word.create(string));
		Token[] tokens;
		if (i == 0)
			tokens = new Token[] { token, T_RightPar };
		else {
			tokens = new Token[i * 2 + 1];
			tokens[0] = token;
			for (int i_6_ = 0; i_6_ < i; i_6_++)
				tokens[2 * i_6_ + 1] = T_Expr;
			for (int i_7_ = 1; i_7_ < i; i_7_++)
				tokens[2 * i_7_] = T_Comma;
			tokens[2 * i] = T_RightPar;
		}
		createProduction(var_class, tokens);
	}

	public static synchronized void init() throws InternalException {
		if (!initialized) {
			createProduction(com.renderx.xep.pre.exp.Bracketed.class,
					new Token[] { T_LeftPar, T_Expr, T_RightPar });
			createProduction(com.renderx.xep.pre.exp.UnaryPlus.class,
					new Token[] { T_UnaryPlus, T_Expr });
			createProduction(com.renderx.xep.pre.exp.UnaryMinus.class,
					new Token[] { T_UnaryMinus, T_Expr });
			createProduction(com.renderx.xep.pre.exp.Plus.class, new Token[] {
					T_Expr, T_Plus, T_Expr }, 832);
			createProduction(com.renderx.xep.pre.exp.Minus.class, new Token[] {
					T_Expr, T_Minus, T_Expr }, 832);
			createProduction(com.renderx.xep.pre.exp.Divide.class, new Token[] {
					T_Expr, T_Divide, T_Expr });
			createProduction(com.renderx.xep.pre.exp.Multiply.class,
					new Token[] { T_Expr, T_Asterisk, T_Expr });
			createProduction(com.renderx.xep.pre.exp.Modulo.class, new Token[] {
					T_Expr, T_Modulo, T_Expr });
			createFunction(com.renderx.xep.pre.exp.Floor.class, "floor", 1);
			createFunction(com.renderx.xep.pre.exp.Ceiling.class, "ceiling", 1);
			createFunction(com.renderx.xep.pre.exp.Round.class, "round", 1);
			createFunction(com.renderx.xep.pre.exp.Absolute.class, "abs", 1);
			createFunction(com.renderx.xep.pre.exp.Minimum.class, "min", 2);
			createFunction(com.renderx.xep.pre.exp.Maximum.class, "max", 2);
			createFunction(com.renderx.xep.pre.exp.RGB_Color.class, "rgb", 3);
			createFunction(com.renderx.xep.pre.exp.RGB_ICC_Color.class,
					"rgb-icc", 5);
			createFunction(com.renderx.xep.pre.exp.RGB_ICC_Color.class,
					"rgb-icc", 6);
			createFunction(com.renderx.xep.pre.exp.RGB_ICC_Color.class,
					"rgb-icc", 7);
			createFunction(com.renderx.xep.pre.exp.RGB_ICC_Color.class,
					"rgb-icc", 8);
			createFunction(com.renderx.xep.pre.exp.RGB_ICC_Color.class,
					"rgb-icc", 9);
			createFunction(com.renderx.xep.pre.exp.RGB_ICC_Color.class,
					"rgb-icc", 10);
			createFunction(com.renderx.xep.pre.exp.RGB_ICC_Color.class,
					"rgb-icc", 11);
			createFunction(com.renderx.xep.pre.exp.SystemColor.class,
					"system-color", 1);
			createFunction(com.renderx.xep.pre.exp.SystemFont.class,
					"system-font", 1);
			createFunction(com.renderx.xep.pre.exp.SystemFont.class,
					"system-font", 2);
			createFunction(
					com.renderx.xep.pre.exp.InheritedPropertyValue.class,
					"inherited-property-value", 0);
			createFunction(
					com.renderx.xep.pre.exp.InheritedPropertyValue.class,
					"inherited-property-value", 1);
			createFunction(com.renderx.xep.pre.exp.FromParent.class,
					"from-parent", 0);
			createFunction(com.renderx.xep.pre.exp.FromParent.class,
					"from-parent", 1);
			createFunction(
					com.renderx.xep.pre.exp.FromNearestSpecifiedValue.class,
					"from-nearest-specified-value", 0);
			createFunction(
					com.renderx.xep.pre.exp.FromNearestSpecifiedValue.class,
					"from-nearest-specified-value", 1);
			createFunction(com.renderx.xep.pre.exp.BodyStart.class,
					"body-start", 0);
			createFunction(com.renderx.xep.pre.exp.LabelEnd.class, "label-end",
					0);
			createFunction(com.renderx.xep.pre.exp.FromTableColumn.class,
					"from-table-column", 0);
			createFunction(com.renderx.xep.pre.exp.FromTableColumn.class,
					"from-table-column", 1);
			createFunction(com.renderx.xep.pre.exp.MergePropertyValues.class,
					"merge-property-values", 0);
			createFunction(com.renderx.xep.pre.exp.MergePropertyValues.class,
					"merge-property-values", 1);
			createFunction(
					com.renderx.xep.pre.exp.ProportionalColumnWidth.class,
					"proportional-column-width", 1);
			createFunction(com.renderx.xep.pre.exp.DefaultPageWidth.class,
					"default-page-width", 0);
			createFunction(com.renderx.xep.pre.exp.DefaultPageHeight.class,
					"default-page-height", 0);
			Attn.init();
			initialized = true;
		}
	}

	public static void main(String[] strings) {
		init();
		char[] cs = new char[8192];
		int i = 0;
		for (;;) {
			try {
				InputStreamReader inputstreamreader = new InputStreamReader(
						System.in);
				i = inputstreamreader.read(cs);
			} catch (IOException ioexception) {
				System.err.println(ioexception);
				System.exit(1);
			}
			if (i < 0)
				System.exit(0);
			try {
				Attr attr = parse(new String(cs, 0, i));
				System.out.println(attr.toString());
			} catch (ParserException parserexception) {
				System.err.println(parserexception.toString());
			} catch (InternalException internalexception) {
				System.err.println(internalexception.toString());
			}
		}
	}

	public static void PrintStackState(PrintStream printstream, Stack stack) {
		printstream.println("-------------------------");
		Enumeration enumeration = stack.elements();
		while (enumeration.hasMoreElements()) {
			Token token = (Token) enumeration.nextElement();
			printstream.println("  " + token.getTypeName()
					+ (token.value == null ? "" : ": " + token.value));
		}
	}
}