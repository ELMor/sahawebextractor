/*
 * Attributed - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

public interface Attributed extends Cloneable {
	public boolean containsKey(Attn attn);

	public Attr get(Attn attn);

	public void put(Attn attn, Attr attr);

	public Attr remove(Attn attn);

	public Object clone();
}