/*
 * AttributeSieve - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

public class AttributeSieve {
	public NameAndValue[][] turns = null;

	public int[] turnCounts = null;

	public static class NameAndValue {
		public Attn att = null;

		public String value = null;

		public NameAndValue() {
			/* empty */
		}

		public NameAndValue(Attn attn, String string) {
			att = attn;
			value = string;
		}
	}

	public AttributeSieve() {
		int i = 16;
		turns = new NameAndValue[i][];
		turnCounts = new int[i];
		for (int i_0_ = 0; i_0_ < i; i_0_++) {
			turns[i_0_] = new NameAndValue[1];
			turnCounts[i_0_] = 0;
		}
	}

	public void put(String string, String string_1_) throws ParserException {
		Attn attn = Attn.byName(string);
		if (attn == null || attn.turn == -1)
			throw new ParserException("Unrecognized attribute name: " + string);
		NameAndValue[] nameandvalues = turns[attn.turn];
		int i = turnCounts[attn.turn];
		if (nameandvalues.length <= i) {
			NameAndValue[] nameandvalues_2_ = new NameAndValue[2 * i];
			for (int i_3_ = 0; i_3_ < i; i_3_++)
				nameandvalues_2_[i_3_] = nameandvalues[i_3_];
			nameandvalues = turns[attn.turn] = nameandvalues_2_;
		}
		nameandvalues[i] = new NameAndValue(attn, string_1_);
		turnCounts[attn.turn]++;
	}

	public void clear() {
		for (int i = 0; i < turns.length; i++) {
			NameAndValue[] nameandvalues = turns[i];
			for (int i_4_ = 0; i_4_ < turnCounts[i]; i_4_++) {
				if (nameandvalues[i_4_] != null) {
					nameandvalues[i_4_].att = null;
					nameandvalues[i_4_].value = null;
					nameandvalues[i_4_] = null;
				}
			}
			turnCounts[i] = 0;
		}
	}
}