/*
 * BidiResolver - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import com.renderx.util.List;
import com.renderx.xep.lib.Session;

public class BidiResolver implements ElementHandler, PIHandler {
	private final Session session;

	private final ElementHandler handler;

	private boolean delayed = false;

	private final List queue = new List();

	public class TextRecord implements Record {
		public String text;

		public AttList attributes;

		public TextRecord(String string, AttList attlist) {
			text = string;
			attributes = attlist;
		}

		public void play() throws CompilerException {
			attributes.put(Attn.$TEXT, Attr.Word.createUncached(text));
			startElement((short) 0, attributes);
			endElement((short) 0);
		}
	}

	public class EndRecord implements Record {
		public short element;

		public EndRecord(short i) {
			element = i;
		}

		public void play() throws CompilerException {
			handler.endElement(element);
		}
	}

	public class StartRecord implements Record {
		public short element;

		public AttList attributes;

		public StartRecord(short i, AttList attlist) {
			element = i;
			attributes = attlist;
		}

		public void play() throws CompilerException {
			handler.startElement(element, attributes);
		}
	}

	static interface Record {
		public void play() throws CompilerException;
	}

	public BidiResolver(ElementHandler elementhandler, Session session) {
		handler = elementhandler;
		this.session = session;
	}

	public void reset() {
		queue.clear();
	}

	public void startElement(short i, AttList attlist) throws CompilerException {
		if (delayed)
			queue.append(new StartRecord(i, attlist));
		else
			handler.startElement(i, attlist);
	}

	public void endElement(short i) throws CompilerException {
		if (delayed)
			queue.append(new EndRecord(i));
		else
			handler.endElement(i);
	}

	public void processingInstruction(String string, String string_0_)
			throws CompilerException {
		if (handler instanceof PIHandler)
			((PIHandler) handler).processingInstruction(string, string_0_);
	}

	public void characters(String string, AttList attlist, Attr attr,
			ParserContext parsercontext) {
		try {
			boolean bool = attr != parsercontext.direction
					&& attr != Bidi.NEUTRAL;
			boolean bool_1_ = (attr == Bidi.RIGHTTOLEFT || (parsercontext.direction == Bidi.RIGHTTOLEFT && attr == Bidi.NEUTRAL));
			string = Bidi.shape(string, bool_1_);
			if (bool) {
				AttList attlist_2_ = (AttList) attlist.clone();
				attlist_2_.put(Attn.$direction, attr);
				startElement((short) 22, attlist_2_);
			}
			if (delayed)
				queue.append(new TextRecord(string, attlist));
			else {
				attlist.put(Attn.$TEXT, Attr.Word.createUncached(string));
				startElement((short) 0, attlist);
				endElement((short) 0);
			}
			if (bool)
				endElement((short) 22);
		} catch (CompilerException compilerexception) {
			session.error("Error while processing text node: "
					+ compilerexception);
		}
	}
}