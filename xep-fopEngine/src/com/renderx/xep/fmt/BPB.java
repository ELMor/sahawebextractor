/*
 * BPB - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import com.renderx.graphics.Image;
import com.renderx.xep.cmp.Item;
import com.renderx.xep.cmp.Xattr;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Attributed;

public class BPB extends Xattr implements Instruction {
	public int[] r;

	public Image im = null;

	public boolean draw = true;

	public boolean first;

	protected Block par = null;

	public Attr writing_mode = Attr.lr_tb;

	public static class Anchor extends Inline {
		public Item.Id id;

		public int[] rabs;

		Anchor(Item.Anchor anchor_0_, int[] is, Session session) {
			super((Item.Span) anchor_0_, is, session);
		}
	}

	public static class Inline extends BPB {
		Inline(Item.Span span, int[] is, Session session) {
			super((Attributed) span, is, session);
		}
	}

	public static class Table extends Frame {
		boolean header = false;

		int dy = 0;

		Table(Item.Table table_1_, int[] is, Session session) {
			super((Item.Frame) table_1_, is, session);
		}
	}

	public static class ListItem extends Block {
		BPB.Block alt = null;

		ListItem(Item.ListItem listitem_2_, int[] is, Session session) {
			super((Item.Block) listitem_2_, is, session);
		}
	}

	public static class Frame extends Block {
		int y0s = 2147483647;

		int y0e = 2147483647;

		Edge start_edge = Edge.empty;

		Edge end_edge = Edge.empty;

		Frame(Item.Frame frame_3_, int[] is, Session session) {
			super((Item.Block) frame_3_, is, session);
		}
	}

	public static class Block extends BPB {
		int r1 = 2147483647;

		int margin_after;

		int shiftx = 0;

		int shifty = 0;

		BPB.Block desc = null;

		int baseline = 2147483647;

		private boolean shifted = false;

		boolean first_line = false;

		public Integer bno;

		public AttList ruler = null;

		Block(Item.Block block_4_, int[] is, Session session) {
			super((Attributed) block_4_, is, session);
			bno = block_4_.bno;
			ruler = block_4_.proplist.get0((short) 109);
			margin_after = block_4_.get(Attn.$margin_after).length();
		}

		final void shift() {
			if (!shifted) {
				int i = r1 - r[1];
				Attr attr = this.get(Attn.$display_align);
				if (attr != Attr.newWord("before")) {
					if (attr == Attr.newWord("center"))
						shifty = -i / 2;
					else if (attr == Attr.newWord("after"))
						shifty = -i;
				}
				if (par != null) {
					int i_5_ = par.get(Attn.$reference_orientation).count();
					switch (i_5_ / 90) {
					case 0:
						shiftx += par.shiftx;
						shifty += par.shifty;
						break;
					case 1:
						shiftx += par.shifty;
						shifty -= par.shiftx;
						break;
					case 2:
						shiftx -= par.shiftx;
						shifty -= par.shifty;
						break;
					case 3:
						shiftx -= par.shifty;
						shifty += par.shiftx;
						break;
					default:
						throw new InternalException(
								"illegal value of reference-orientation: "
										+ par.get(Attn.$reference_orientation));
					}
				}
				shifted = true;
			}
		}
	}

	public void pass(InstructionDispatcher instructiondispatcher) {
		instructiondispatcher.process(this);
	}

	BPB(Attributed attributed, int[] is, Session session) {
		super(attributed);
		r = is;
		Attr attr = attributed.get(Attn.$background_image);
		if (attr != Attr.none)
			im = (session.config.imageFactory.makeImage(attr, attributed
					.get(Attn.$image_base_url), attributed
					.get(Attn.$background_content_type), session));
		writing_mode = attributed.get(Attn.$writing_mode);
	}

	public Object clone() {
		BPB bpb_6_ = (BPB) super.clone();
		bpb_6_.r = (int[]) r.clone();
		return bpb_6_;
	}
}