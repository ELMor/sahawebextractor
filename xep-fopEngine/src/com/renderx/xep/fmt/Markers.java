/*
 * Markers - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.xep.cmp.Flow;
import com.renderx.xep.pre.Attr;

class Markers extends Hashtable {
	public Object clone() {
		Markers markers_0_ = (Markers) super.clone();
		Enumeration enumeration = markers_0_.keys();
		while (enumeration.hasMoreElements()) {
			Object object = enumeration.nextElement();
			markers_0_.put(object, ((Marker) markers_0_.get(object)).clone());
		}
		return markers_0_;
	}

	void Starting(Hashtable hashtable) {
		Enumeration enumeration = hashtable.keys();
		while (enumeration.hasMoreElements()) {
			Attr attr = (Attr) enumeration.nextElement();
			Flow flow = (Flow) hashtable.get(attr);
			Marker marker = (Marker) this.get(attr);
			if (marker == null)
				this.put(attr, marker = new Marker());
			marker.putStarting(flow);
		}
	}

	void Ending(Hashtable hashtable) {
		Enumeration enumeration = hashtable.keys();
		while (enumeration.hasMoreElements()) {
			Attr attr = (Attr) enumeration.nextElement();
			Flow flow = (Flow) hashtable.get(attr);
			Marker marker = (Marker) this.get(attr);
			if (marker == null)
				this.put(attr, marker = new Marker());
			marker.putEnding(flow);
		}
	}

	void First(Hashtable hashtable) {
		Enumeration enumeration = hashtable.keys();
		while (enumeration.hasMoreElements()) {
			Attr attr = (Attr) enumeration.nextElement();
			Flow flow = (Flow) hashtable.get(attr);
			Marker marker = (Marker) this.get(attr);
			if (marker == null)
				this.put(attr, marker = new Marker());
			marker.put(0, flow);
		}
	}

	void Last(Hashtable hashtable) {
		Enumeration enumeration = hashtable.keys();
		while (enumeration.hasMoreElements()) {
			Attr attr = (Attr) enumeration.nextElement();
			Flow flow = (Flow) hashtable.get(attr);
			Marker marker = (Marker) this.get(attr);
			if (marker == null)
				this.put(attr, marker = new Marker());
			marker.put(5, flow);
		}
	}

	void Reconcile() {
		Enumeration enumeration = this.elements();
		while (enumeration.hasMoreElements()) {
			Marker marker = (Marker) enumeration.nextElement();
			marker.reconcile();
		}
	}
}