/*
 * LineCounter - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import java.util.Enumeration;

import com.renderx.util.Applicator;
import com.renderx.util.List;
import com.renderx.util.Stack;
import com.renderx.xep.cmp.Flow;
import com.renderx.xep.cmp.Item;
import com.renderx.xep.lib.Util;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;

final class LineCounter implements Cloneable {
	TD cur;

	boolean empty = true;

	boolean edge = true;

	boolean hyphenated = false;

	int width;

	int max_width;

	Item.Space.Inline iaccu = new Item.Space.Inline();

	static class Phrase implements Cloneable {
		private TD cur;

		private Flow flow;

		private Stack sstack;

		private Item.Span s;

		List words = new List();

		boolean empty = true;

		boolean hyphenated = false;

		int width;

		int max_width;

		Phrase(TD td) {
			cur = td;
			flow = td.flow;
			sstack = (Stack) td.sstack.map(new Stack(), new Applicator() {
				public Object f(Object object) {
					return ((Item.Span) object).clone();
				}
			});
			s = (Item.Span) td.s.clone();
		}

		void restore() {
			cur.flow = flow;
			cur.sstack = sstack;
			cur.s = s;
		}

		void size(List list, LineCounter linecounter) {
			Item item = null;
			Enumeration enumeration = list.elements();
			while (enumeration.hasMoreElements()) {
				item = (Item) enumeration.nextElement();
				if (item.breaksInlineSpace())
					linecounter.iaccu = new Item.Space.Inline();
				if (item instanceof Item.Inline) {
					Item.Inline inline = (Item.Inline) item;
					width += inline.width;
					if (item instanceof Item.Span) {
						((Item.Span) item).dimensions(cur.s, cur.width);
						cur.sstack.push(cur.s);
						Item.Span span = cur.s;
						cur.s = (Item.Span) item;
						if (Util.ne0(cur.s.start_indent))
							linecounter.iaccu = new Item.Space.Inline();
						int i = cur.s.start_indent - span.start_indent;
						if (Util.ne0(i))
							width += i;
					}
				} else if (item instanceof Item.Washer.Inline)
					width += ((Item.Washer.Inline) item).thickness;
				else if (item instanceof Item.Space.Inline) {
					Item.Space.Inline inline = (Item.Space.Inline) item;
					if (Util.ne0(inline.optimum)) {
						Item.Space.Inline inline_1_ = ((Item.Space.Inline) linecounter.iaccu
								.merge(inline));
						width += inline_1_.optimum;
					}
				} else if (item instanceof Item.Ends) {
					if (Util.ne0(cur.s.end_indent))
						linecounter.iaccu = new Item.Space.Inline();
					Item.Span span = cur.s;
					cur.s = (Item.Span) cur.sstack.pop();
					int i = span.end_indent - cur.s.end_indent;
					if (Util.ne0(i))
						width += i;
				}
			}
			hyphenated = (item instanceof Item.Text && ((Item.Text) item)
					.isHyphenated());
		}
	}

	LineCounter(TD td, boolean bool) {
		cur = td;
		width = 0;
		max_width = td.width;
		if (bool)
			max_width -= td.b.text_indent;
		prolog();
	}

	void size(Phrase phrase) {
		phrase.width = width;
		phrase.max_width = max_width;
		phrase.size(phrase.words, this);
	}

	void add(Phrase phrase) {
		empty = empty && phrase.empty;
		width = phrase.width;
		max_width = phrase.max_width;
	}

	void prolog() {
		if (!cur.sstack.isEmpty()) {
			int i = 0;
			Enumeration enumeration = cur.sstack.reverse().elements();
			enumeration.nextElement();
			Item.Span span;
			do {
				span = (enumeration.hasMoreElements() ? (Item.Span) enumeration
						.nextElement() : cur.s);
				int i_2_ = span.start_indent - i;
				if (span.get(Attn.$border_start_width_conditionality) == Attr
						.newWord("discard"))
					i_2_ -= span.get(Attn.$border_start_width_length).length();
				if (span.get(Attn.$padding_start_conditionality) == Attr
						.newWord("discard"))
					i_2_ -= span.get(Attn.$padding_start_length).length();
				if (Util.ne0(i_2_))
					width += i_2_;
				i = span.start_indent;
			} while (span != cur.s);
		}
	}
}