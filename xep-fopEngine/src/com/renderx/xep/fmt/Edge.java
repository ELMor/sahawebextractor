/*
 * Edge - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import java.util.Enumeration;

import com.renderx.util.List;

final class Edge extends List {
	static final Edge empty = new Edge();

	private static final class Tooth {
		final int y0;

		final int y1;

		final int depth;

		Tooth(int i, int i_0_, int i_1_) {
			y0 = i;
			y1 = i_0_;
			depth = i_1_;
		}

		public String toString() {
			return "y0=" + y0 + " y1=" + y1 + " depth=" + depth;
		}
	}

	Edge add(int i, int i_2_, int i_3_) {
		Edge edge_4_ = new Edge();
		if (this.isEmpty())
			edge_4_.snoc(new Tooth(i, i_2_, i_3_));
		else {
			Tooth tooth = (Tooth) this.car();
			List list = this.cdr();
			for (;;) {
				if (tooth.y0 > i_2_)
					edge_4_.snoc(tooth);
				else {
					if (tooth.y1 < i) {
						edge_4_.snoc(new Tooth(i, i_2_, i_3_)).snoc(tooth)
								.append(list);
						break;
					}
					if (tooth.y1 < i_2_) {
						edge_4_.snoc(new Tooth(tooth.y1, i_2_, i_3_));
						i_2_ = tooth.y1;
					} else if (tooth.y1 > i_2_) {
						edge_4_.snoc(new Tooth(i_2_, tooth.y1, tooth.depth));
						tooth = new Tooth(tooth.y0, i_2_, tooth.depth);
					}
					if (tooth.depth < i_3_) {
						if (tooth.y0 < i) {
							edge_4_.snoc(new Tooth(i, i_2_, i_3_));
							tooth = new Tooth(tooth.y0, i, tooth.depth);
						} else
							tooth = new Tooth(tooth.y0, tooth.y1, i_3_);
					}
					edge_4_.snoc(tooth);
					if (i < tooth.y0)
						i_2_ = tooth.y0;
					else {
						edge_4_.append(list);
						break;
					}
				}
				if (list.isEmpty()) {
					edge_4_.snoc(new Tooth(i, i_2_, i_3_));
					break;
				}
				tooth = (Tooth) list.car();
				list = list.cdr();
			}
		}
		return edge_4_;
	}

	int depth(int i, int i_5_) {
		int i_6_ = -2147483648;
		if (!this.isEmpty()) {
			Enumeration enumeration = this.elements();
			while (enumeration.hasMoreElements()) {
				Tooth tooth = (Tooth) enumeration.nextElement();
				if (tooth.y0 <= i_5_ && i < tooth.y1) {
					if (i_6_ < tooth.depth)
						i_6_ = tooth.depth;
				} else if (tooth.y1 < i)
					break;
			}
		}
		return i_6_;
	}

	Edge dip(int i) {
		Edge edge_7_ = this;
		if (!this.isEmpty()) {
			edge_7_ = new Edge();
			Enumeration enumeration = this.elements();
			while (enumeration.hasMoreElements()) {
				Tooth tooth = (Tooth) enumeration.nextElement();
				if (tooth.depth > i)
					edge_7_
							.snoc(new Tooth(tooth.y0, tooth.y1, tooth.depth - i));
			}
		}
		return edge_7_;
	}

	int y0() {
		return this.isEmpty() ? 2147483647 : ((Tooth) this.last()).y0;
	}
}