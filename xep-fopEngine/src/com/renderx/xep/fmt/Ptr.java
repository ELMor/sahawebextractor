/*
 * Ptr - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import java.util.Enumeration;

import com.renderx.util.Applicator;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.util.Stack;
import com.renderx.xep.cmp.Item;

final class Ptr {
	List tab = new List();

	Hashtable syn = new Hashtable();

	LineBuffer lb = null;

	boolean atwide = false;

	Item.Wide wide = null;

	TD orig = null;

	private static final short LN = 0;

	private static final short CP = 1;

	Ptr() {
		/* empty */
	}

	Ptr(TD td) {
		this();
		orig = td;
		add(td);
	}

	Ptr(TD td, TD td_0_) {
		this(td_0_);
		td_0_.b.bpb = td.b.bpb;
		td_0_.s = (Item.Span) td.s.clone();
		td_0_.sstack = (Stack) td.sstack.map(new Stack(), new Applicator() {
			public Object f(Object object) {
				return ((Item.Span) object).clone();
			}
		});
	}

	void add(TD td) {
		tab.snoc(td);
	}

	TD del() {
		return (TD) tab.shift();
	}

	void nxt() {
		add(del());
	}

	void restore(Ptr ptr_2_) {
		syn = ptr_2_.syn;
		tab = ptr_2_.tab;
		lb = ptr_2_.lb;
		atwide = ptr_2_.atwide;
		wide = ptr_2_.wide;
		orig = ptr_2_.orig;
	}

	Ptr ln() {
		Ptr ptr_3_ = copy((short) 0);
		return ptr_3_;
	}

	Ptr cp() {
		Ptr ptr_4_ = copy((short) 1);
		TD td = (TD) ptr_4_.tab.car();
		if (lb != null)
			ptr_4_.lb = lb.cp(td);
		return ptr_4_;
	}

	private Ptr copy(short i) {
		Ptr ptr_5_ = new Ptr();
		ptr_5_.atwide = atwide;
		ptr_5_.wide = wide;
		ptr_5_.orig = orig;
		Hashtable hashtable = new Hashtable();
		Enumeration enumeration = syn.keys();
		while (enumeration.hasMoreElements()) {
			Object object = enumeration.nextElement();
			hashtable.put(syn.get(object), object);
		}
		Enumeration enumeration_6_ = tab.elements();
		while (enumeration_6_.hasMoreElements()) {
			TD td = (TD) enumeration_6_.nextElement();
			ptr_5_.tab.append(i == 0 ? td.ln() : td.cp());
			if (hashtable.containsKey(td))
				ptr_5_.syn.put(hashtable.get(td), ptr_5_.tab.last());
		}
		return ptr_5_;
	}

	boolean empties() {
		Enumeration enumeration = tab.elements();
		while (enumeration.hasMoreElements()) {
			TD td = (TD) enumeration.nextElement();
			if (td.empty != 0 || td.group != Group.nogroup)
				return true;
		}
		return false;
	}

	void newPage() {
		/* empty */
	}

	void newColumn(boolean bool) {
		Enumeration enumeration = tab.elements();
		while (enumeration.hasMoreElements()) {
			TD td = (TD) enumeration.nextElement();
			td.container_edge = bool;
			if (!td.filled && td.empty < 4)
				td.empty = (short) 4;
		}
	}

	boolean movedSince(Ptr ptr_7_) {
		if (orig != ptr_7_.orig)
			return false;
		if (tab.length() != ptr_7_.tab.length())
			return true;
		if (tab.length() == 0)
			return false;
		TD td = (TD) tab.car();
		TD td_8_ = (TD) ptr_7_.tab.car();
		do {
			if (((TD) ptr_7_.tab.car()).tno == td.tno) {
				Enumeration enumeration = tab.elements();
				Enumeration enumeration_9_ = ptr_7_.tab.elements();
				do {
					if (!enumeration.hasMoreElements())
						return false;
				} while (((TD) enumeration.nextElement()).flow == ((TD) enumeration_9_
						.nextElement()).flow);
				return true;
			}
			ptr_7_.nxt();
		} while (ptr_7_.tab.car() != td_8_);
		return true;
	}
}