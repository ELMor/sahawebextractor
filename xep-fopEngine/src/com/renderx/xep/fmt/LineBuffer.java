/*
 * LineBuffer - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import java.util.Enumeration;

import com.renderx.util.Applicator;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.util.Stack;
import com.renderx.xep.cmp.Flow;
import com.renderx.xep.cmp.Item;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.lib.Util;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;

final class LineBuffer implements Cloneable {
	private final Session session;

	List words = new List();

	private MTF fmt;

	TD cur;

	boolean empty = true;

	boolean edge = true;

	boolean hyphenated = false;

	int width;

	int max_width;

	int height_before;

	int height_after;

	Item.Space.Inline iaccu = new Item.Space.Inline();

	static class Phrase implements Cloneable {
		private final List.Mark strmmark;

		private final boolean footnotes_pending;

		private final MTF fmt;

		private final TD cur;

		private final List topfloats;

		private final List footnotes;

		private final List bodynotes;

		private final Markers markers;

		private final Flow flow;

		private final Stack sstack;

		private final Item.Span s;

		private final Session session;

		List words = new List();

		boolean empty = true;

		boolean hyphenated = false;

		int width;

		int max_width;

		int height_before;

		int height_after;

		private static final short BASELINE = 0;

		private static final short BEFORE = 1;

		private static final short TEXT_BEFORE = 2;

		private static final short MIDDLE = 3;

		private static final short CENTRAL = 4;

		private static final short TEXT_AFTER = 5;

		private static final short AFTER = 6;

		private static final Hashtable baseline_xlat = new Hashtable();

		Phrase(MTF mtf) {
			fmt = mtf;
			cur = mtf.cur;
			session = mtf.session;
			strmmark = mtf.strm.mark();
			footnotes_pending = mtf.footnotes_pending;
			flow = cur.flow;
			sstack = (Stack) cur.sstack.map(new Stack(), new Applicator() {
				public Object f(Object object) {
					return ((Item.Span) object).clone();
				}
			});
			s = (Item.Span) cur.s.clone();
			topfloats = (List) mtf.bkm.topfloats.clone();
			footnotes = (List) mtf.bkm.footnotes.clone();
			bodynotes = (List) mtf.bkm.bodynotes.clone();
			markers = (Markers) mtf.bkm.page.markers.clone();
		}

		void restore() {
			fmt.strm.cut(strmmark);
			fmt.footnotes_pending = footnotes_pending;
			fmt.bkm.topfloats.clear().append(topfloats);
			fmt.bkm.footnotes.clear().append(footnotes);
			fmt.bkm.bodynotes.clear().append(bodynotes);
			fmt.bkm.page.markers.clear();
			Enumeration enumeration = markers.keys();
			while (enumeration.hasMoreElements()) {
				Object object = enumeration.nextElement();
				fmt.bkm.page.markers.put(object, markers.get(object));
			}
			cur.flow = flow;
			cur.sstack = sstack;
			cur.s = s;
		}

		void size(LineBuffer linebuffer) {
			List list = new List();
			Item item = null;
			Enumeration enumeration = words.elements();
			while (enumeration.hasMoreElements()) {
				item = (Item) enumeration.nextElement();
				if (item.breaksInlineSpace())
					linebuffer.iaccu = new Item.Space.Inline();
				if (item instanceof Item.Inline) {
					Item.Inline inline = (Item.Inline) item;
					width += inline.width;
					if (inline.tt.offset == -2147483648) {
						inline.offset = cur.s.offset;
						if (item instanceof Item.Span) {
							Attr attr = inline.get(Attn.$baseline_shift);
							if (attr instanceof Attr.Word) {
								if (attr == Attr.newWord("sub"))
									inline.offset -= cur.s.tt.x_height / 2;
								else if (attr == Attr.newWord("super"))
									inline.offset += cur.s.tt.x_height;
								else if (attr != Attr.newWord("baseline"))
									throw new InternalException(
											"illegal value for baseline-shift: "
													+ attr.word());
							} else
								inline.offset += attr
										.length_or_ratio(cur.s.tt.line_height);
							Attr attr_1_ = inline.get(Attn.$alignment_baseline);
							if (attr_1_ == Attr.auto)
								attr_1_ = Attr.newWord("baseline");
							try {
								switch (((Short) baseline_xlat.get(attr_1_))
										.shortValue()) {
								case 0:
									break;
								case 1:
									inline.offset += cur.s.tt.max_ascender;
									break;
								case 2:
									inline.offset += cur.s.tt.max_ascender;
									break;
								case 3:
									inline.offset += cur.s.tt.x_height / 2;
									break;
								case 4:
									inline.offset += (cur.s.tt.max_ascender + cur.s.tt.min_descender) / 2;
									break;
								case 5:
									inline.offset += cur.s.tt.min_descender;
									break;
								case 6:
									inline.offset += cur.s.tt.min_descender;
									break;
								}
							} catch (Exception exception) {
								throw new InternalException("invalid baseline "
										+ attr_1_);
							}
							Attr attr_2_ = inline.get(Attn.$alignment_adjust);
							if (attr_2_ instanceof Attr.Word) {
								if (attr_2_ == Attr.auto)
									attr_2_ = attr_1_;
								try {
									switch (((Short) baseline_xlat.get(attr_2_))
											.shortValue()) {
									case 0:
										break;
									case 1:
										inline.offset -= inline.ascender;
										break;
									case 2:
										inline.offset -= inline.tt.max_ascender;
										break;
									case 3:
										inline.offset -= inline.tt.x_height / 2;
										break;
									case 4:
										inline.offset -= (inline.ascender + inline.descender) / 2;
										break;
									case 5:
										inline.offset -= inline.tt.min_descender;
										break;
									case 6:
										inline.offset -= inline.descender;
										break;
									}
								} catch (Exception exception) {
									throw new InternalException(
											"invalid baseline " + attr_1_);
								}
							} else
								inline.offset += attr_2_
										.length_or_ratio(inline.tt.font_size);
						}
						inline.tt.offset = inline.offset;
					} else
						inline.offset = inline.tt.offset;
					int i = cur.b.consider_shifts ? inline.offset : 0;
					boolean bool = false;
					boolean bool_3_ = false;
					int i_4_;
					int i_5_;
					switch (cur.b.lss) {
					case 0:
						i_4_ = height_before;
						i_5_ = height_after;
						break;
					case 1:
						i_4_ = cur.s.tt.lea + inline.tt.max_ascender + i;
						i_5_ = cur.s.tt.lea - inline.tt.min_descender - i;
						break;
					case 2:
						i_4_ = inline.tt.lea + inline.ascender + i;
						i_5_ = inline.tt.lea - inline.descender - i;
						break;
					default:
						throw new InternalException(
								"illegal value for cur.b.lss");
					}
					if (height_before < i_4_)
						height_before = i_4_;
					if (height_after < i_5_)
						height_after = i_5_;
					if (item instanceof Item.Span) {
						((Item.Span) item).dimensions(cur.s, cur.width);
						cur.sstack.push(cur.s);
						Item.Span span = cur.s;
						cur.s = (Item.Span) item;
						cur.s.bpb = (cur.s instanceof Item.Anchor ? (BPB.Inline) new BPB.Anchor(
								((Item.Anchor) cur.s),
								(new int[] { 0, 0, 0, 0 }), session)
								: new BPB.Inline(cur.s,
										new int[] { 0, 0, 0, 0 }, session));
						cur.s.bpb.par = cur.b.bpb;
						if (Util.ne0(cur.s.start_indent))
							linebuffer.iaccu = new Item.Space.Inline();
						int i_6_ = cur.s.start_indent - span.start_indent;
						if (Util.ne0(i_6_)) {
							list.append(new Item.Washer.Inline(i_6_));
							width += i_6_;
						}
					}
					list.append(item);
				} else if (item instanceof Item.Washer.Inline) {
					width += ((Item.Washer.Inline) item).thickness;
					list.append(item);
				} else if (item instanceof Item.Space.Inline) {
					Item.Space.Inline inline = (Item.Space.Inline) item;
					if (Util.ne0(inline.optimum) || inline.precedence != 0) {
						Item.Space.Inline inline_7_ = ((Item.Space.Inline) linebuffer.iaccu
								.merge(inline));
						width += inline_7_.optimum;
						list.append(inline_7_);
					}
				} else if (item instanceof Item.Ends) {
					if (Util.ne0(cur.s.end_indent))
						linebuffer.iaccu = new Item.Space.Inline();
					Item.Span span = cur.s;
					cur.s = (Item.Span) cur.sstack.pop();
					int i = span.end_indent - cur.s.end_indent;
					list.append(item);
					if (Util.ne0(i)) {
						list.append(new Item.Washer.Inline(i));
						width += i;
					}
				}
			}
			hyphenated = (item instanceof Item.Text && ((Item.Text) item)
					.isHyphenated());
			words = list;
			max_width = cur.width;
			int i = cur.start_edge.depth(cur.y - height_after - height_before,
					cur.y);
			int i_8_ = cur.end_edge.depth(cur.y - height_after - height_before,
					cur.y);
			if (i > cur.b.start_indent)
				max_width -= i - cur.b.start_indent;
			if (i_8_ > cur.b.end_indent)
				max_width -= i_8_ - cur.b.end_indent;
			if (cur.b.bpb.first_line)
				max_width -= cur.b.text_indent;
		}

		static {
			baseline_xlat.put(Attr.newWord("baseline"), new Short((short) 0));
			baseline_xlat
					.put(Attr.newWord("before-edge"), new Short((short) 1));
			baseline_xlat.put(Attr.newWord("text-before-edge"), new Short(
					(short) 2));
			baseline_xlat.put(Attr.newWord("middle"), new Short((short) 3));
			baseline_xlat.put(Attr.newWord("central"), new Short((short) 4));
			baseline_xlat.put(Attr.newWord("text-after-edge"), new Short(
					(short) 5));
			baseline_xlat.put(Attr.newWord("after-edge"), new Short((short) 6));
			baseline_xlat.put(Attr.newWord("top"), baseline_xlat.get(Attr
					.newWord("before-edge")));
			baseline_xlat.put(Attr.newWord("text-top"), baseline_xlat.get(Attr
					.newWord("text-before-edge")));
			baseline_xlat.put(Attr.newWord("text-bottom"), baseline_xlat
					.get(Attr.newWord("text-after-edge")));
			baseline_xlat.put(Attr.newWord("bottom"), baseline_xlat.get(Attr
					.newWord("after-edge")));
			baseline_xlat.put(Attr.newWord("alphabetic"), baseline_xlat
					.get(Attr.newWord("baseline")));
			baseline_xlat.put(Attr.newWord("mathematical"), baseline_xlat
					.get(Attr.newWord("central")));
			baseline_xlat.put(Attr.newWord("hanging"), baseline_xlat.get(Attr
					.newWord("text-before-edge")));
			baseline_xlat.put(Attr.newWord("ideographic"), baseline_xlat
					.get(Attr.newWord("text-after-edge")));
		}
	}

	LineBuffer(MTF mtf) {
		fmt = mtf;
		cur = mtf.cur;
		session = mtf.session;
		width = 0;
		max_width = cur.width;
		if (cur.b.bpb.first_line)
			max_width -= cur.b.text_indent;
		height_before = cur.b.tt.max_ascender + cur.b.tt.lea;
		height_after = -cur.b.tt.min_descender + cur.b.tt.lea;
		prolog();
	}

	LineBuffer cp(TD td) {
		try {
			LineBuffer linebuffer_9_ = (LineBuffer) this.clone();
			linebuffer_9_.words = (List) words.clone();
			linebuffer_9_.cur = td;
			return linebuffer_9_;
		} catch (CloneNotSupportedException clonenotsupportedexception) {
			return null;
		}
	}

	void size(Phrase phrase, boolean bool) {
		phrase.width = width;
		phrase.height_before = height_before;
		phrase.height_after = height_after;
		if (bool)
			phrase.size(this);
		else {
			Stack stack = (Stack) cur.sstack.clone();
			Item.Span span = cur.s;
			List list = phrase.words;
			phrase.size(this);
			cur.sstack = stack;
			cur.s = span;
			phrase.words = list;
		}
	}

	void add(Phrase phrase) {
		empty = empty && phrase.empty;
		hyphenated = phrase.hyphenated;
		width = phrase.width;
		max_width = phrase.max_width;
		height_before = phrase.height_before;
		height_after = phrase.height_after;
		Object object = null;
		Enumeration enumeration = phrase.words.elements();
		while (enumeration.hasMoreElements()) {
			Item item = (Item) enumeration.nextElement();
			words.cons(item);
		}
	}

	void prolog() {
		if (!cur.sstack.isEmpty()) {
			Phrase phrase = new Phrase(fmt);
			int i = 0;
			Enumeration enumeration = cur.sstack.reverse().elements();
			Item.Span span = cur.s;
			cur.s = (Item.Span) enumeration.nextElement();
			cur.sstack = new Stack();
			Item.Span span_10_;
			do {
				span_10_ = (enumeration.hasMoreElements() ? (Item.Span) enumeration
						.nextElement()
						: span);
				Item.Span span_11_ = (Item.Span) span_10_.clone();
				phrase.words.append(span_11_);
			} while (span_10_ != span);
			size(phrase, true);
			height_before = phrase.height_before;
			height_after = phrase.height_after;
			Enumeration enumeration_12_ = phrase.words.elements();
			while (enumeration_12_.hasMoreElements()) {
				Item item = (Item) enumeration_12_.nextElement();
				if (item instanceof Item.Span) {
					Item.Span span_13_ = (Item.Span) item;
					int i_14_ = span_13_.start_indent - i;
					span_13_.bpb = (span_13_ instanceof Item.Anchor ? (BPB.Inline) new BPB.Anchor(
							((Item.Anchor) span_13_), new int[] { 0, 0, 0, 0 },
							session)
							: new BPB.Inline(span_13_,
									new int[] { 0, 0, 0, 0 }, session));
					span_13_.bpb.par = cur.b.bpb;
					if (span_13_.get(Attn.$border_start_width_conditionality) == Attr
							.newWord("discard")) {
						i_14_ -= span_13_.get(Attn.$border_start_width_length)
								.length();
						span_13_.bpb.put(Attn.$border_start_width_length,
								Attr.zerolength);
					}
					if (span_13_.get(Attn.$padding_start_conditionality) == Attr
							.newWord("discard")) {
						i_14_ -= span_13_.get(Attn.$padding_start_length)
								.length();
						span_13_.bpb.put(Attn.$padding_start_length,
								Attr.zerolength);
					}
					if (Util.ne0(i_14_)) {
						words.cons(new Item.Washer.Inline(i_14_));
						width += i_14_;
					}
					words.cons(span_13_);
					i = span_13_.start_indent;
				}
			}
		}
	}

	void epilog() {
		if (hyphenated) {
			Item.Text text = (Item.Text) words.car();
			Item.Text text_15_ = new Item.Text(text.get(
					Attn.$hyphenation_character).word(), text.a, session);
			text_15_.tt = text.tt;
			text_15_.offset = text.offset;
			words.cons(text_15_);
			width += text_15_.width;
		}
		if (!cur.sstack.isEmpty()) {
			Item.Span span = cur.s;
			Enumeration enumeration = cur.sstack.elements();
			while (enumeration.hasMoreElements()) {
				Item.Span span_16_ = (Item.Span) enumeration.nextElement();
				words.cons(Item.ends);
				int i = span.end_indent - span_16_.end_indent;
				if (span.get(Attn.$border_end_width_conditionality) == Attr
						.newWord("discard")) {
					i -= span.get(Attn.$border_end_width_length).length();
					span.bpb
							.put(Attn.$border_end_width_length, Attr.zerolength);
				}
				if (span.get(Attn.$padding_end_conditionality) == Attr
						.newWord("discard")) {
					i -= span.get(Attn.$padding_end_length).length();
					span.bpb.put(Attn.$padding_end_length, Attr.zerolength);
				}
				if (Util.ne0(i)) {
					words.cons(new Item.Washer.Inline(i));
					width += i;
				}
				span = span_16_;
			}
		}
	}
}