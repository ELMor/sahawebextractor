/*
 * Line - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import java.util.Enumeration;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.KernVector;
import com.renderx.fonts.Metric;
import com.renderx.util.Array;
import com.renderx.util.List;
import com.renderx.util.Stack;
import com.renderx.xep.cmp.Item;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.lib.Util;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;

public final class Line implements Instruction {
	public int x;

	public int y;

	public int wsp = 0;

	public int csp = 0;

	public List words = new List();

	BPB.Block bpb;

	Attr writing_mode;

	int width;

	int max_width;

	boolean pageids = false;

	short align = -1;

	private Session session;

	public void pass(InstructionDispatcher instructiondispatcher) {
		instructiondispatcher.process(this);
	}

	public Line(List list, LineBuffer linebuffer, boolean bool, Session session) {
		this.session = session;
		linebuffer.epilog();
		x = linebuffer.cur.x;
		y = linebuffer.cur.y;
		max_width = linebuffer.max_width;
		width = linebuffer.width;
		bpb = linebuffer.cur.b.bpb;
		writing_mode = linebuffer.cur.b.writing_mode;
		int i = linebuffer.cur.start_edge.depth(y - linebuffer.height_after, y
				+ linebuffer.height_before);
		if (i > linebuffer.cur.b.start_indent)
			x += i - linebuffer.cur.b.start_indent;
		if (linebuffer.cur.b.bpb.first_line)
			x += linebuffer.cur.b.text_indent;
		if (bool) {
			max_width -= linebuffer.cur.b.last_indent;
			align = linebuffer.cur.b.align_last;
		} else
			align = linebuffer.cur.b.align;
		List list_0_ = new List();
		Item.Text text = null;
		boolean bool_1_ = false;
		Enumeration enumeration = linebuffer.words.elements();
		while (enumeration.hasMoreElements()) {
			Item item = (Item) enumeration.nextElement();
			if (item instanceof Item.Text) {
				Item.Text text_2_ = (Item.Text) item;
				if (text != null
						&& text_2_.tt == text.tt
						&& (text_2_.tt.merge_white_space || (text_2_.nwsp == 0) == (text.nwsp == 0))) {
					if (bool_1_) {
						words = words.cdr().cons(
								text = (Item.Text) text.clone());
						bool_1_ = false;
					}
					char[] cs = new char[text.chars.length
							+ text_2_.chars.length];
					System.arraycopy(text_2_.chars, 0, cs, 0,
							text_2_.chars.length);
					System.arraycopy(text.chars, 0, cs, text_2_.chars.length,
							text.chars.length);
					text.chars = cs;
					text.width += text_2_.width;
					text.nwsp += text_2_.nwsp;
					if (text.ascender < text_2_.ascender)
						text.ascender = text_2_.ascender;
					if (text.descender > text_2_.descender)
						text.descender = text_2_.descender;
					text_2_ = text;
				} else {
					words.cons(text_2_);
					text = text_2_;
					bool_1_ = true;
				}
				if (item instanceof Item.PageId)
					pageids = true;
			} else {
				text = null;
				words.cons(item);
				if (item instanceof Item.Span)
					list_0_.cons(((Item.Span) item).bpb);
			}
		}
		list.append(list_0_);
		if (session.config.KERN) {
			List list_3_ = new List();
			Enumeration enumeration_4_ = words.elements();
			while (enumeration_4_.hasMoreElements()) {
				Object object = enumeration_4_.nextElement();
				if (object instanceof Item.Text) {
					Item.Text text_5_ = (Item.Text) object;
					if (text_5_.chars.length == 0)
						list_3_.append(text_5_);
					else {
						Metric metric = text_5_.tt.te.getMetric();
						if (text_5_.tt.te.ligatures != null
								&& text_5_.tt.csp == 0) {
							for (int i_6_ = 0; i_6_ < text_5_.chars.length; i_6_++) {
								char c = text_5_.chars[i_6_];
								Array array = (Array) text_5_.tt.te.ligatures
										.get(c);
								if (array != null) {
									int i_7_ = 0;
									while_19_: for (/**/; i_7_ < array
											.length(); i_7_++) {
										char[] cs = (char[]) array.get(i_7_);
										if (metric.ucm(cs[0]) != null) {
											boolean bool_8_ = false;
											for (int i_9_ = 1; i_9_ < cs.length; i_9_++) {
												if ((i_6_ + i_9_ >= text_5_.chars.length)
														|| ((text_5_.chars[i_6_
																+ i_9_]) != cs[i_9_]))
													continue while_19_;
											}
											int i_10_ = cs.length - 1;
											char[] cs_11_ = (new char[(text_5_.chars.length - i_10_)]);
											for (int i_12_ = 0; i_12_ < i_6_; i_12_++)
												cs_11_[i_12_] = text_5_.chars[i_12_];
											cs_11_[i_6_] = cs[0];
											for (int i_13_ = i_6_ + 1; i_13_ < (text_5_.chars.length - i_10_); i_13_++)
												cs_11_[i_13_] = (text_5_.chars[i_13_
														+ i_10_]);
											text_5_.chars = cs_11_;
											double d = (double) (metric
													.ucm(cs[0]).wx);
											d -= (double) metric.ucm(c).wx;
											for (int i_14_ = 1; i_14_ < cs.length; i_14_++)
												d -= (double) (metric
														.ucm(cs[i_14_]).wx);
											text_5_.width += (int) (Math
													.round(d
															* (text_5_.tt.font_stretch)
															* (double) (text_5_.tt.font_size)
															/ 1000.0));
											break;
										}
									}
								}
							}
						}
						boolean bool_15_ = false;
						int i_16_ = 0;
						while (i_16_ != text_5_.chars.length - 1) {
							CharMetrics charmetrics = metric
									.ucm(text_5_.chars[i_16_]);
							if (charmetrics != null
									&& charmetrics.kernTable != null) {
								KernVector kernvector = ((KernVector) charmetrics.kernTable
										.get(text_5_.chars[i_16_ + 1]));
								if (kernvector != null) {
									int i_17_ = ((int) ((double) text_5_.tt.font_size * kernvector.X) / 1000);
									if (i_17_ != 0) {
										Item.Text text_18_ = new Item.Text(
												text_5_.chars, i_16_ + 1,
												text_5_.a, session, text_5_.a,
												text_5_.tt);
										if (!bool_15_) {
											text_5_ = (Item.Text) text_5_
													.clone();
											bool_15_ = true;
										}
										text_5_.nwsp -= text_18_.nwsp;
										text_5_.width -= text_18_.width;
										width += i_17_;
										text_18_.width += i_17_;
										text_18_.offset = text_5_.offset;
										list_3_.append(text_18_);
										char[] cs = new char[(text_5_.chars.length
												- i_16_ - 1)];
										for (int i_19_ = 0; i_19_ != cs.length; i_19_++)
											cs[i_19_] = (text_5_.chars[i_19_
													+ i_16_ + 1]);
										text_5_.chars = cs;
										i_16_ = 0;
										continue;
									}
								}
							}
							i_16_++;
						}
						list_3_.append(text_5_);
					}
				} else
					list_3_.append(object);
				words = list_3_;
			}
		}
		if (max_width < width && linebuffer.cur.b.wrap)
			align = (short) 3;
	}

	void align() {
		int i = max_width - width;
		List list = new List();
		Enumeration enumeration = words.elements();
		while (enumeration.hasMoreElements()) {
			Object object = enumeration.nextElement();
			if (object instanceof Item.Space.Inline) {
				Item.Space.Inline inline = (Item.Space.Inline) object;
				int i_20_;
				if (Util.ne0(i) && align == 3) {
					if (i > 0) {
						if (inline.maximum - inline.optimum > i)
							i_20_ = inline.optimum + i;
						else
							i_20_ = inline.maximum;
					} else if (inline.minimum - inline.optimum < i)
						i_20_ = inline.optimum + i;
					else
						i_20_ = inline.minimum;
					i += inline.optimum - i_20_;
				} else
					i_20_ = inline.optimum;
				if (Util.ne0(i_20_))
					list.append(new Item.Washer.Inline(i_20_));
			} else
				list.append(object);
		}
		List list_21_ = words;
		words = list;
		list = list_21_.clear();
		if (Util.ne0(i)) {
			switch (align) {
			case 0:
				break;
			case 1:
			case 2: {
				if (words.last() instanceof Item.Text) {
					Item.Text text = (Item.Text) words.last();
					if (text.chars.length != 0)
						i += text.tt.csp;
				}
				Line line_22_ = this;
				line_22_.x = line_22_.x + (align == 1 ? i / 2 : i);
				break;
			}
			case 3: {
				list_21_ = new List();
				Enumeration enumeration_23_ = words.elements();
				while (enumeration_23_.hasMoreElements()) {
					Item item = (Item) enumeration_23_.nextElement();
					if (item instanceof Item.Leader) {
						item.dimensions(max_width);
						list_21_.cons(item);
					}
				}
				List list_24_;
				for (/**/; !list_21_.isEmpty(); list_21_ = list_24_) {
					int i_25_ = i / list_21_.length();
					int i_26_ = i;
					list_24_ = new List();
					Enumeration enumeration_27_ = list_21_.elements();
					while (enumeration_27_.hasMoreElements()) {
						Item.Leader leader = (Item.Leader) enumeration_27_
								.nextElement();
						int i_28_ = leader.width + i_25_;
						if (i_28_ > leader.len_max)
							i_28_ = adjust_leader_length(leader.len_max, leader);
						else if (i_28_ < leader.len_min)
							i_28_ = adjust_leader_length(leader.len_min, leader);
						else {
							i_28_ = adjust_leader_length(i_28_, leader);
							list_24_.cons(leader);
						}
						i -= i_28_ - leader.width;
						leader.width = i_28_;
					}
					if (i_26_ == i && list_21_.length() == list_24_.length())
						break;
					i_26_ = i;
				}
				int i_29_ = 0;
				int i_30_ = 0;
				Item item = null;
				Enumeration enumeration_31_ = words.elements();
				for (;;) {
					if (!enumeration_31_.hasMoreElements()) {
						if (item instanceof Item.Text) {
							Item.Text text = (Item.Text) item;
							if (text.chars.length != 0) {
								if (text.tt.varcsp)
									i_29_--;
								else
									i += text.tt.csp;
							}
						}
						break;
					}
					item = (Item) enumeration_31_.nextElement();
					if (item instanceof Item.Text) {
						Item.Text text = (Item.Text) item;
						if (text.tt.varcsp)
							i_29_ += text.chars.length;
						if (text.tt.varwsp)
							i_30_ += text.nwsp;
					}
				}
				if (i > 0) {
					if (i_30_ != 0) {
						if (i * 3 >= max_width && i_29_ != 0) {
							wsp = max_width / i_30_ / 4;
							csp = (i - wsp * i_30_) / i_29_;
						} else
							wsp = i / i_30_;
					} else if (i_29_ != 0)
						csp = i / i_29_;
				} else if (i_30_ != 0) {
					if (-i_29_ * i * 6 >= i_30_ * max_width && i_29_ != 0) {
						wsp = -max_width / i_29_ / 8;
						csp = (i - wsp * i_30_) / i_29_;
					} else
						wsp = i / i_30_;
				} else if (i_29_ != 0)
					csp = i / i_29_;
				Enumeration enumeration_32_ = words.elements();
				while (enumeration_32_.hasMoreElements()) {
					Item item_33_ = (Item) enumeration_32_.nextElement();
					if (item_33_ instanceof Item.Text) {
						Item.Text text = (Item.Text) item_33_;
						if (text.tt.varwsp)
							text.width += wsp * text.nwsp;
						if (text.tt.varcsp)
							text.width += csp * text.chars.length;
					}
				}
				break;
			}
			case 6:
				break;
			default:
				throw new InternalException("Illegal value for align: " + align);
			}
		}
		Stack stack = new Stack();
		Item.Span span = null;
		Attr attr = writing_mode;
		Stack stack_34_ = new Stack();
		Enumeration enumeration_35_ = words.elements();
		while (enumeration_35_.hasMoreElements()) {
			Item item = (Item) enumeration_35_.nextElement();
			if (item instanceof Item.Span) {
				stack.push(span);
				span = (Item.Span) item;
				attr = span.writing_mode;
				stack_34_.push(list);
				list = new List();
				if (attr == writing_mode)
					list.snoc(span);
				else
					list.cons(Item.ends);
			} else if (item instanceof Item.Ends) {
				if (attr == writing_mode)
					list.snoc(Item.ends);
				else
					list.cons(span);
				span = (Item.Span) stack.pop();
				attr = span != null ? span.writing_mode : writing_mode;
				List list_36_ = list;
				list = (List) stack_34_.pop();
				if (attr == writing_mode)
					list.append(list_36_);
				else
					list.prepend(list_36_);
			} else if (attr == writing_mode)
				list.snoc(item);
			else {
				if (item instanceof Item.Text) {
					item = item.cow();
					drow((Item.Text) item);
				}
				list.cons(item);
			}
		}
		List list_37_ = words;
		words = list;
		list = list_37_.clear();
		int i_38_ = x;
		stack = new Stack();
		span = null;
		enumeration_35_ = words.elements();
		while (enumeration_35_.hasMoreElements()) {
			Item item = (Item) enumeration_35_.nextElement();
			if (item instanceof Item.Span) {
				stack.push(span);
				span = (Item.Span) item;
				span.first_word = true;
				span.bpb.r[0] = (i_38_
						- span.bpb.get(Attn.$border_start_width_length)
								.length() - span.bpb.get(
						Attn.$padding_start_length).length());
				span.bpb.writing_mode = writing_mode;
			} else if (item instanceof Item.Ends) {
				span.bpb.r[2] = (i_38_
						+ span.bpb.get(Attn.$border_end_width_length).length() + span.bpb
						.get(Attn.$padding_end_length).length());
				span.bpb.r[3] = (y
						+ span.offset
						+ span.ascender
						+ span.bpb.get(Attn.$border_before_width_length)
								.length() + span.bpb.get(
						Attn.$padding_before_length).length());
				span.bpb.r[1] = (y
						+ span.offset
						+ span.descender
						- span.bpb.get(Attn.$border_after_width_length)
								.length() - span.bpb.get(
						Attn.$padding_after_length).length());
				span = (Item.Span) stack.pop();
			} else if (item instanceof Item.Inline) {
				if (span != null && span.first_word) {
					span.first_word = false;
					Enumeration enumeration_39_ = stack.elements();
					Item.Span span_40_ = span;
					do {
						if (span_40_.bpb != null)
							span_40_.bpb.draw = true;
						span_40_ = (Item.Span) enumeration_39_.nextElement();
					} while (enumeration_39_.hasMoreElements());
				}
				Item.Inline inline = (Item.Inline) item;
				i_38_ += inline.width;
				list.append(item);
			} else if (item instanceof Item.Washer) {
				i_38_ += ((Item.Washer) item).thickness;
				list.append(item);
			} else
				throw new InternalException("illegal instruction in line:"
						+ item.getClass().getName());
		}
		List list_41_ = words;
		words = list;
		list = list_41_.clear();
		if (writing_mode == Attr.rl_tb) {
			x = -i_38_;
			Enumeration enumeration_42_ = words.elements();
			while (enumeration_42_.hasMoreElements()) {
				Item item = (Item) enumeration_42_.nextElement();
				if (item instanceof Item.Text) {
					item = item.cow();
					drow((Item.Text) item);
				}
				list.cons(item);
			}
			words = list;
		}
	}

	private static int adjust_leader_length(int i, Item.Leader leader) {
		if (leader.leader_pattern != 2 && leader.leader_pattern != 3)
			return i;
		int i_43_ = leader.patwidth;
		if (i > leader.width) {
			int i_44_ = i / i_43_ * i_43_;
			if (i_44_ >= leader.width)
				return i_44_;
		} else if (i < leader.width) {
			int i_45_ = i / i_43_ * i_43_ + i_43_;
			if (i_45_ <= leader.width)
				return i_45_;
		}
		return i;
	}

	private static void drow(Item.Text text) {
		int i = text.chars.length - 1;
		char[] cs = text.chars;
		text.chars = new char[cs.length];
		int i_46_ = 0;
		while (i_46_ != text.chars.length) {
			text.chars[i_46_] = cs[i];
			i_46_++;
			i--;
		}
	}
}