/*
 * Cntl - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

public class Cntl {
	public static class Translate extends Cntl implements Instruction {
		public int x;

		public int y;

		public void pass(InstructionDispatcher instructiondispatcher) {
			instructiondispatcher.process(this);
		}

		public Translate(int i, int i_0_) {
			x = i;
			y = i_0_;
		}
	}

	public static class Rotate extends Cntl implements Instruction {
		public int phi;

		public void pass(InstructionDispatcher instructiondispatcher) {
			instructiondispatcher.process(this);
		}

		public Rotate(int i) {
			phi = i;
		}
	}
}