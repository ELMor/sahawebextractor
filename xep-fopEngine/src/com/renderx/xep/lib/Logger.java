/*
 * Logger - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.lib;

import com.renderx.util.ErrorHandler;

public interface Logger extends ErrorHandler {
	public static final Logger NULL_LOGGER = null;

	public void openDocument();

	public void closeDocument();

	public void event(String string, String string_0_);

	public void openState(String string);

	public void closeState(String string);

	public void info(String string);

	public void warning(String string);

	public void error(String string);

	public void exception(String string, Exception exception);
}