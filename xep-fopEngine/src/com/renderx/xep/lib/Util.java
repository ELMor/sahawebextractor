/*
 * Util - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.lib;

import com.renderx.util.User;

public class Util {
	public static boolean eq0(int i) {
		return Math.abs(i) <= 50;
	}

	public static boolean ne0(int i) {
		return Math.abs(i) > 50;
	}

	public static boolean gt0(int i) {
		return i > 50;
	}

	public static boolean lt0(int i) {
		return i < -50;
	}

	public static int phi(int i) {
		for (;;) {
			if (i < 0)
				i += 360;
			else {
				if (i < 360)
					break;
				i -= 360;
			}
		}
		return i;
	}

	public static String i2s(int i, String string) {
		char[] cs = string.toCharArray();
		int i_0_;
		for (i_0_ = 0; i_0_ != cs.length; i_0_++) {
			if (Character.isLetterOrDigit(cs[i_0_]))
				break;
		}
		String string_1_ = string.substring(0, i_0_);
		int i_2_ = i_0_;
		for (/**/; i_0_ != cs.length; i_0_++) {
			if (!Character.isLetterOrDigit(cs[i_0_]))
				break;
		}
		int i_3_ = i_0_ - i_2_;
		String string_4_ = string.substring(i_0_, cs.length);
		if (i_3_ == 0)
			return string_1_ + string_4_;
		StringBuffer stringbuffer = new StringBuffer();
		String string_5_;
		if (cs[i_2_] == 'I' || cs[i_2_] == 'i') {
			char[] cs_6_ = { 'M', 'D', 'C', 'L', 'X', 'V', 'I' };
			int i_7_ = 0;
			int i_8_ = 1000;
			for (;;) {
				int i_9_ = i_8_;
				if (i >= i_9_) {
					for (i_0_ = 0; i_0_ != i / i_9_; i_0_++)
						stringbuffer.append(cs_6_[2 * i_7_]);
					if (i_9_ == 1)
						break;
					i %= i_9_;
				}
				i_8_ /= 10;
				i_9_ = 9 * i_8_;
				if (i >= i_9_) {
					stringbuffer.append(cs_6_[2 * i_7_ + 2]).append(
							cs_6_[2 * i_7_]);
					i %= i_9_;
				}
				i_9_ = 5 * i_8_;
				if (i >= i_9_) {
					stringbuffer.append(cs_6_[2 * i_7_ + 1]);
					i %= i_9_;
				}
				i_9_ = 4 * i_8_;
				if (i >= i_9_) {
					stringbuffer.append(cs_6_[2 * i_7_ + 2]).append(
							cs_6_[2 * i_7_ + 1]);
					i %= i_9_;
				}
				if (i == 0)
					break;
				i_7_++;
			}
			string_5_ = stringbuffer.toString();
			if (cs[i_2_] == 'i')
				string_5_ = string_5_.toLowerCase();
		} else if (cs[i_2_] == 'A' || cs[i_2_] == 'a') {
			int i_10_ = 26;
			do {
				if (i > i_10_)
					stringbuffer.insert(0, (char) (i % i_10_ + 65));
				else {
					i_10_++;
					stringbuffer.insert(0, (char) (i % i_10_ + 65 - 1));
				}
				i /= i_10_;
			} while (i != 0);
			string_5_ = stringbuffer.toString();
			if (cs[i_2_] == 'a')
				string_5_ = string_5_.toLowerCase();
		} else {
			string_5_ = Integer.toString(i);
			if (string_5_.length() < i_3_) {
				char[] cs_11_ = new char[i_3_ - string_5_.length()];
				for (i_0_ = 0; i_0_ != cs_11_.length; i_0_++)
					cs_11_[i_0_] = '0';
				string_5_ = new String(cs_11_) + string_5_;
			}
		}
		return string_1_ + string_5_ + string_4_;
	}

	public static String getStrProperty(String string, String string_12_) {
		String string_13_ = User.getProperty(string);
		return (string_13_ == null ? string_12_
				: string_13_.equals("none") ? null : string_13_);
	}

	public static boolean getBoolProperty(String string, boolean bool) {
		String string_14_ = User.getProperty(string);
		return string_14_ != null ? string_14_.equals("true") : bool;
	}

	public static int getIntProperty(String string, int i) {
		String string_15_ = User.getProperty(string);
		return string_15_ != null ? Integer.valueOf(string_15_).intValue() : i;
	}

	public static double getDblProperty(String string, double d) {
		String string_16_ = User.getProperty(string);
		return (string_16_ != null ? Double.valueOf(string_16_).doubleValue()
				: d);
	}

	public static int getLenProperty(String string, int i) {
		String string_17_ = User.getProperty(string);
		return (string_17_ != null ? Integer.valueOf(string_17_).intValue() * 1000
				: i);
	}
}