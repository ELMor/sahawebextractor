/*
 * Session - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.lib;

import com.renderx.util.Hashtable;
import com.renderx.util.List;

public class Session implements Logger {
	protected final Logger logger;

	public final Conf config;

	public final Hashtable exprCache = new Hashtable();

	protected final Hashtable fontCache = new Hashtable();

	public int counter = 0;

	public Session() throws ConfigurationException {
		this(new Conf());
	}

	public Session(Conf conf) {
		this(conf, conf.logger);
	}

	public Session(Conf conf, Logger logger) {
		config = conf;
		this.logger = logger;
	}

	public List findAllFonts(String string, int i, String string_0_,
			String string_1_, String string_2_) {
		String string_3_ = (string + ":" + i + ":" + string_0_ + ":"
				+ string_1_ + ":" + string_2_);
		List list = (List) fontCache.get(string_3_);
		if (list == null) {
			list = config.fontCatalog.findAll(string, i, string_0_, string_1_,
					string_2_, logger);
			fontCache.put(string_3_, list);
		}
		return list;
	}

	public int nextno() {
		return ++counter;
	}

	public void cleanup() {
		exprCache.clear();
		fontCache.clear();
	}

	public void openDocument() {
		if (logger != null) {
			synchronized (logger) {
				logger.openDocument();
			}
		}
	}

	public void closeDocument() {
		if (logger != null) {
			synchronized (logger) {
				logger.closeDocument();
			}
		}
	}

	public void event(String string, String string_4_) {
		if (logger != null) {
			synchronized (logger) {
				logger.event(string, string_4_);
			}
		}
	}

	public void openState(String string) {
		if (logger != null) {
			synchronized (logger) {
				logger.openState(string);
			}
		}
	}

	public void closeState(String string) {
		if (logger != null) {
			synchronized (logger) {
				logger.closeState(string);
			}
		}
	}

	public void info(String string) {
		if (logger != null) {
			synchronized (logger) {
				logger.info(string);
			}
		}
	}

	public void warning(String string) {
		if (logger != null) {
			synchronized (logger) {
				logger.warning(string);
			}
		}
	}

	public void error(String string) {
		if (logger != null) {
			synchronized (logger) {
				logger.error(string);
			}
		}
	}

	public void exception(String string, Exception exception) {
		if (logger != null) {
			synchronized (logger) {
				logger.exception(string, exception);
			}
		}
	}
}