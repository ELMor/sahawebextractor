/*
 * NumStr - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.lib;

public class NumStr {
	private String prefix;

	private String suffix;

	private NumFormat nf;

	private static class ArmenianTraditional extends NumFormat {
		private static char[][] registers = {
				"\u0531\u0532\u0533\u0534\u0535\u0536\u0537\u0538\u0539"
						.toCharArray(),
				"\u053a\u053b\u053c\u053d\u053e\u053f\u0540\u0541\u0542"
						.toCharArray(),
				"\u0543\u0544\u0545\u0546\u0547\u0548\u0549\u054a\u054b"
						.toCharArray(),
				"\u054c\u054d\u054e\u054f\u0550\u0551\u0552\u0553\u0554"
						.toCharArray() };

		private ArmenianTraditional() {
			/* empty */
		}

		String format(int i) {
			if (i > 9999)
				i = 9999;
			StringBuffer stringbuffer = new StringBuffer();
			int i_0_ = 0;
			for (;;) {
				if (i % 10 == 0) {
					i /= 10;
					i_0_++;
				} else {
					stringbuffer.append(registers[i_0_++][i % 10 - 1]);
					if ((i /= 10) <= 0)
						break;
				}
			}
			return stringbuffer.reverse().toString();
		}
	}

	private static class EnglishTraditional extends NumFormat {
		private boolean lcase;

		EnglishTraditional(String string) {
			lcase = string.charAt(string.length() - 1) == 'i';
		}

		String format(int i) {
			char[] cs = { 'M', 'D', 'C', 'L', 'X', 'V', 'I' };
			int i_1_ = 0;
			int i_2_ = 1000;
			StringBuffer stringbuffer = new StringBuffer();
			for (;;) {
				int i_3_ = i_2_;
				if (i >= i_3_) {
					for (int i_4_ = 0; i_4_ != i / i_3_; i_4_++)
						stringbuffer.append(cs[2 * i_1_]);
					if (i_3_ == 1)
						break;
					i %= i_3_;
				}
				i_2_ /= 10;
				i_3_ = 9 * i_2_;
				if (i >= i_3_) {
					stringbuffer.append(cs[2 * i_1_ + 2]).append(cs[2 * i_1_]);
					i %= i_3_;
				}
				i_3_ = 5 * i_2_;
				if (i >= i_3_) {
					stringbuffer.append(cs[2 * i_1_ + 1]);
					i %= i_3_;
				}
				i_3_ = 4 * i_2_;
				if (i >= i_3_) {
					stringbuffer.append(cs[2 * i_1_ + 2]).append(
							cs[2 * i_1_ + 1]);
					i %= i_3_;
				}
				if (i == 0)
					break;
				i_1_++;
			}
			String string = stringbuffer.toString();
			if (lcase)
				string = string.toLowerCase();
			return string;
		}
	}

	private static class CyrillicAlphabetic extends AlphabeticFormat {
		private static char[][] letters = {
				"\u0410\u0411\u0412\u0413\u0414\u0415\u0401\u0416\u0417\u0418\u0419\u041a\u041b\u041c\u041d\u041e\u041f\u0420\u0421\u0422\u0423\u0424\u0425\u0426\u0427\u0428\u0429\u042a\u042b\u042c\u042d\u042e\u042f"
						.toCharArray(),
				"\u0430\u0431\u0432\u0433\u0434\u0435\u0451\u0436\u0437\u0438\u0439\u043a\u043b\u043c\u043d\u043e\u043f\u0440\u0441\u0442\u0443\u0444\u0445\u0446\u0447\u0448\u0449\u044a\u044b\u044c\u044d\u044e\u044f"
						.toCharArray() };

		CyrillicAlphabetic(String string) {
			super(string);
		}

		char[] setupLetters(char c) {
			return letters[c == '\u0410' ? 0 : 1];
		}
	}

	private static class HiraganaIroha extends AlphabeticFormat {
		private static char[] letters = "\u3044\u308d\u306f\u306b\u307b\u3078\u3068\u3061\u308a\u306c\u308b\u3092\u308f\u304b\u3088\u305f\u308c\u305d\u3064\u306d\u306a\u3089\u3080\u3046\u3090\u306e\u304a\u304f\u3084\u307e\u3051\u3075\u3053\u3048\u3066\u3042\u3055\u304d\u3086\u3081\u307f\u3057\u3091\u3072\u3082\u305b\u3059"
				.toCharArray();

		HiraganaIroha(String string) {
			super(string);
		}

		char[] setupLetters(char c) {
			return letters;
		}
	}

	private static class HiraganaAlphabetic extends AlphabeticFormat {
		private static char[] letters = "\u3042\u3044\u3046\u3048\u304a\u304b\u304d\u304f\u3051\u3053\u3055\u3057\u3059\u305b\u305d\u305f\u3061\u3064\u3066\u3068\u306a\u306b\u306c\u306d\u306e\u306f\u3072\u3075\u3078\u307b\u307e\u307f\u3080\u3081\u3082\u3084\u3086\u3088\u3089\u308a\u308b\u308c\u308d\u308f\u3092\u3093"
				.toCharArray();

		HiraganaAlphabetic(String string) {
			super(string);
		}

		char[] setupLetters(char c) {
			return letters;
		}
	}

	private static class KatakanaIroha extends AlphabeticFormat {
		private static char[] letters = "\u30a4\u30ed\u30cf\u30cb\u30db\u30d8\u30c8\u30c1\u30ea\u30cc\u30eb\u30f2\u30ef\u30ab\u30e8\u30bf\u30ec\u30bd\u30c4\u30cd\u30ca\u30e9\u30e0\u30a6\u30f0\u30ce\u30aa\u30af\u30e4\u30de\u30b1\u30d5\u30b3\u30a8\u30c6\u30a2\u30b5\u30ad\u30e6\u30e1\u30df\u30b7\u30f1\u30d2\u30e2\u30bb\u30b9"
				.toCharArray();

		KatakanaIroha(String string) {
			super(string);
		}

		char[] setupLetters(char c) {
			return letters;
		}
	}

	private static class KatakanaAlphabetic extends AlphabeticFormat {
		private static char[] letters = "\u30a2\u30a4\u30a6\u30a8\u30aa\u30ab\u30ad\u30af\u30b1\u30b3\u30b5\u30b7\u30b9\u30bb\u30bd\u30bf\u30c1\u30c4\u30c6\u30c8\u30ca\u30cb\u30cc\u30cd\u30ce\u30cf\u30d2\u30d5\u30d8\u30db\u30de\u30df\u30e0\u30e1\u30e2\u30e4\u30e6\u30e8\u30e9\u30ea\u30eb\u30ec\u30ed\u30ef\u30f2\u30f3"
				.toCharArray();

		KatakanaAlphabetic(String string) {
			super(string);
		}

		char[] setupLetters(char c) {
			return letters;
		}
	}

	private static class EnglishAlphabetic extends AlphabeticFormat {
		private static char[][] letters = {
				"ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray(),
				"abcdefghijklmnopqrstuvwxyz".toCharArray() };

		EnglishAlphabetic(String string) {
			super(string);
		}

		char[] setupLetters(char c) {
			return letters[c == 'A' ? 0 : 1];
		}
	}

	private abstract static class AlphabeticFormat extends NumFormat {
		private char[] letters;

		AlphabeticFormat(String string) {
			letters = setupLetters(string.charAt(string.length() - 1));
		}

		String format(int i) {
			int i_5_ = letters.length;
			StringBuffer stringbuffer = new StringBuffer();
			do
				stringbuffer.append(letters[(i - 1) % i_5_]);
			while ((i = (i - 1) / i_5_) != 0);
			return stringbuffer.reverse().toString();
		}

		abstract char[] setupLetters(char c);
	}

	private static class CanjiNumeric extends RadicalFormat {
		CanjiNumeric(String string, int i, String string_6_) {
			super(string, i, string_6_);
		}

		char[] setupDigits() {
			return "\u3007\u4e00\u4e8c\u4e09\u56db\u4e94\u516d\u4e03\u516b\u4e5d"
					.toCharArray();
		}
	}

	private static class ExtendedIndicArabic extends RadicalFormat {
		ExtendedIndicArabic(String string, int i, String string_7_) {
			super(string, i, string_7_);
		}

		char[] setupDigits() {
			return "\u06f0\u06f1\u06f2\u06f3\u06f4\u06f5\u06f6\u06f7\u06f8\u06f9"
					.toCharArray();
		}
	}

	private static class IndicArabic extends RadicalFormat {
		IndicArabic(String string, int i, String string_8_) {
			super(string, i, string_8_);
		}

		char[] setupDigits() {
			return "\u0660\u0661\u0662\u0663\u0664\u0665\u0666\u0667\u0668\u0669"
					.toCharArray();
		}
	}

	private static class WesternArabic extends RadicalFormat {
		WesternArabic(String string, int i, String string_9_) {
			super(string, i, string_9_);
		}

		char[] setupDigits() {
			return "0123456789".toCharArray();
		}
	}

	private abstract static class RadicalFormat extends NumFormat {
		private int gsize;

		private String gsep;

		private char[] digits;

		private String padding = null;

		RadicalFormat(String string, int i, String string_10_) {
			gsize = i;
			gsep = string_10_;
			digits = setupDigits();
			if (string.length() > 1)
				padding = string.substring(0, string.length() - 1);
		}

		String format(int i) {
			StringBuffer stringbuffer = new StringBuffer();
			int i_11_ = digits.length;
			do
				stringbuffer.append(digits[i % i_11_]);
			while ((i /= i_11_) != 0);
			if (padding != null && stringbuffer.length() < padding.length() + 1)
				stringbuffer.append((Object) new StringBuffer(padding
						.substring(0, (padding.length() + 1 - stringbuffer
								.length()))).reverse());
			if (gsize > 0 && gsep.length() > 0) {
				int i_12_ = 0;
				int i_13_ = 0;
				int i_14_ = stringbuffer.length();
				while (i_12_ < i_14_) {
					if (i_12_ > 0 && i_12_ % gsize == 0) {
						stringbuffer.insert(i_13_, gsep);
						i_13_ += gsep.length();
					}
					i_12_++;
					i_13_++;
				}
			}
			return stringbuffer.reverse().toString();
		}

		abstract char[] setupDigits();
	}

	private static class DefaultFormat extends NumFormat {
		private static NumFormat df = new WesternArabic("1", -2147483648, "");

		private DefaultFormat() {
			/* empty */
		}

		String format(int i) {
			return df.format(i);
		}
	}

	private abstract static class NumFormat {
		private NumFormat() {
			/* empty */
		}

		abstract String format(int i);
	}

	public NumStr(String string, int i, String string_15_) {
		char[] cs = string.toCharArray();
		int i_16_;
		for (i_16_ = 0; i_16_ != cs.length; i_16_++) {
			if (Character.isLetterOrDigit(cs[i_16_]))
				break;
		}
		prefix = string.substring(0, i_16_);
		int i_17_ = i_16_;
		for (/**/; i_16_ != cs.length; i_16_++) {
			if (!Character.isLetterOrDigit(cs[i_16_]))
				break;
		}
		suffix = string.substring(i_16_, cs.length);
		String string_18_ = string.substring(i_17_, i_16_);
		if (string_18_.length() > 0) {
			switch (string_18_.charAt(string_18_.length() - 1)) {
			case '1':
				nf = new WesternArabic(string_18_, i, string_15_);
				break;
			case 'A':
			case 'a':
				nf = new EnglishAlphabetic(string_18_);
				break;
			case 'I':
			case 'i':
				nf = new EnglishTraditional(string_18_);
				break;
			case '\u0661':
				nf = new IndicArabic(string_18_, i, string_15_);
				break;
			case '\u06f1':
				nf = new ExtendedIndicArabic(string_18_, i, string_15_);
				break;
			case '\u30a2':
				nf = new KatakanaAlphabetic(string_18_);
				break;
			case '\u30a4':
				nf = new KatakanaIroha(string_18_);
				break;
			case '\u3042':
				nf = new HiraganaAlphabetic(string_18_);
				break;
			case '\u3044':
				nf = new HiraganaIroha(string_18_);
				break;
			case '\u4e00':
				nf = new CanjiNumeric(string_18_, i, string_15_);
				break;
			case '\u0410':
			case '\u0430':
				nf = new CyrillicAlphabetic(string_18_);
				break;
			case '\u0531':
				nf = new ArmenianTraditional();
				break;
			}
		}
		if (nf == null)
			nf = new DefaultFormat();
	}

	public String format(int i) {
		if (i < 0)
			i = -i;
		if (i == 0)
			i = 1;
		return prefix + nf.format(i) + suffix;
	}
}