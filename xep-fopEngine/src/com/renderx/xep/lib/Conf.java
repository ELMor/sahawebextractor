/*
 * Conf - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.lib;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.StringTokenizer;

import com.renderx.crypto.CheckSignedXML;
import com.renderx.fonts.FontCatalog;
import com.renderx.hyphen.HyphenatorFactory;
import com.renderx.hyphen.TeXHyphenatorFactory;
import com.renderx.sax.CarbonCopyParser;
import com.renderx.sax.ConfigHandler;
import com.renderx.sax.ConfigHandlerAdapter;
import com.renderx.sax.InputSource;
import com.renderx.sax.NullSource;
import com.renderx.sax.SAXStorage;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.Hashtable;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;
import com.renderx.util.URLUtil;
import com.renderx.util.User;
import com.renderx.xep.cmp.ImageFactory;
import com.renderx.xep.pre.Parser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class Conf {
	public static final String NAME = "XEP";

	public static final String TAG = "XEP";

	public static final String VERSION_NUMBER = "4.1 build 20050109";

	public static final String PUBLICKEY = "8F6F0559B81D00517D296DC703CDC2636A23225C392F80FBB6D29D2AAB3D29B6D58DB38E97A61F2C259C46D77002E5C3F5DC3811E0FFF08FFCFCCE475EB047C3:13";

	public static final boolean DEBUG = false;

	public static final boolean TRIAL_MODE = true;

	public static final int SCALE = 1000;

	public static final int RNDERR = 50;

	private static final String OPTION_PREFIX = "com.renderx.xep.";

	private static final String CONFIG = "xep.xml";

	private static final Properties optionTypes = new Properties();

	private static final Properties defaultOptions = new Properties();

	protected static final Properties backendClasses;

	protected static final Properties backendAliases;

	public final String ANNOTATION;

	public final boolean PUT_STAMP;

	public final String APPLICATION_ID;

	public File TMPDIR;

	public final URLSpec BROKENIMAGE;

	public final URLSpec VALIDATOR_STYLESHEET;

	public final boolean VALIDATE;

	public final boolean DISCARD_IF_NOT_VALID;

	public final int STRICTNESS;

	public final boolean XEP2_COMPATIBLE_MODE;

	public final boolean ENABLE_BIDI;

	public final boolean PAGEWIDE_FOOTNOTES;

	public final boolean DISCARD_AFTER_HEADER;

	public final int MIN_PAGE_HEIGHT;

	public final int MIN_PAGE_WIDTH;

	public final int PAGE_HEIGHT;

	public final int PAGE_WIDTH;

	public final int MAX_PAGE_HEIGHT;

	public final int MAX_PAGE_WIDTH;

	public final int LTF;

	public final int CJF;

	public final boolean KERN;

	public final double SHIFT_TARGET_X;

	public final double SHIFT_TARGET_Y;

	public final boolean IGNORE_SPANNED_COLUMN_WIDTH;

	public final boolean ALLOW_NULL_SYSTEM_ID;

	public final URLCache cache;

	public final Logger logger;

	public final FontCatalog fontCatalog;

	public final ImageFactory imageFactory;

	public final HyphenatorFactory hyphenFactory;

	public final Hashtable generatorOptions = new Hashtable();

	private class Config implements ConfigHandler {
		private static final String NSURI = "http://www.renderx.com/XEP/config";

		public final Hashtable options = new Hashtable();

		private int skip = 0;

		public void startDocument(Hashtable hashtable) {
			/* empty */
		}

		public void endDocument(Hashtable hashtable) {
			/* empty */
		}

		public void characters(char[] cs, int i, int i_0_, Hashtable hashtable) {
			if (skip > 0) {
				/* empty */
			}
		}

		public void processingInstruction(String string, String string_1_,
				Hashtable hashtable) {
			if (skip > 0) {
				/* empty */
			}
		}

		public Config() {
			/* empty */
		}

		public void startElement(String string, String string_2_,
				String string_3_, Attributes attributes, Hashtable hashtable) {
			if (skip > 0)
				skip++;
			else if ("".equals(string)
					|| "http://www.renderx.com/XEP/config".equals(string)) {
				if (string_2_.equals("option")) {
					String string_4_ = getRequiredValue(string_2_, attributes,
							"name");
					String string_5_ = getRequiredValue(string_2_, attributes,
							"value");
					if (string_4_ != null && string_5_ != null) {
						String string_6_ = (String) hashtable
								.get("generator-format");
						if (string_6_ == null) {
							String string_7_ = string_4_.trim().toUpperCase()
									.replace('-', '_');
							String string_8_ = (String) Conf.optionTypes
									.get(string_7_);
							if (string_8_ == null)
								Conf.this
										.printError("Invalid option name in the argument list: "
												+ string_4_);
							else if ("URI".equals(string_8_)) {
								try {
									options.put(string_7_, new URLSpec(
											((URLSpec) hashtable.get("base")),
											string_5_));
								} catch (MalformedURLException malformedurlexception) {
									Conf.this
											.printError("Invalid value in the configuration file: "
													+ string_4_
													+ "=\""
													+ string_5_
													+ "\"; setting ignored");
								}
							} else
								options.put(string_7_, string_5_);
						} else {
							Properties properties = (Properties) generatorOptions
									.get(string_6_);
							if (properties == null)
								generatorOptions.put(string_6_,
										(properties = new Properties()));
							properties.put(string_4_, string_5_);
						}
					}
				} else if (string_2_.equals("generator-options")) {
					String string_9_ = getRequiredValue(string_2_, attributes,
							"format");
					if (string_9_ != null) {
						String string_10_ = Conf.backendAliases
								.getProperty(string_9_.trim().toLowerCase());
						if (string_10_ == null)
							Conf.this
									.printError("Invalid format name for generator options: '"
											+ string_9_ + "'");
						hashtable.put("generator-format", string_10_);
					}
				}
			}
		}

		public void endElement(String string, String string_11_,
				String string_12_, Hashtable hashtable) {
			if (skip > 0)
				skip--;
			else if (!"".equals(string)
					&& !"http://www.renderx.com/XEP/config".equals(string)) {
				/* empty */
			}
		}

		private final String getRequiredValue(String string,
				Attributes attributes, String string_13_) {
			String string_14_ = attributes.getValue(string_13_);
			if (string_14_ == null) {
				Conf.this.printError("Mandatory attribute '" + string_13_
						+ "' is missing on element '" + string
						+ "'; element skipped");
				skip = 1;
			}
			return string_14_;
		}

		public final void parse(org.xml.sax.InputSource inputsource,
				XMLReader xmlreader) throws SAXException, IOException {
			xmlreader.setContentHandler(new ConfigHandlerAdapter(this,
					new URLSpec(inputsource.getSystemId())));
			xmlreader.parse(inputsource);
		}
	}

	private class LicenseReader extends DefaultHandler {
		private boolean insideAnnotation = false;

		private boolean insideProduct = false;

		private StringBuffer annotationBuffer = null;

		private String expirationDate = null;

		boolean hasStamp = false;

		public LicenseReader(URLSpec urlspec) throws LicenseException {
			boolean bool = true;
			/*
			try {
				CarbonCopyParser carboncopyparser = new CarbonCopyParser(
						XMLReaderFactory.createXMLReader());
				
				bool = (CheckSignedXML
						.check(
								new InputSource(urlspec),
								carboncopyparser,
								"8F6F0559B81D00517D296DC703CDC2636A23225C392F80FBB6D29D2AAB3D29B6D58DB38E97A61F2C259C46D77002E5C3F5DC3811E0FFF08FFCFCCE475EB047C3:13"));
				SAXStorage saxstorage = carboncopyparser.getCC();
				saxstorage.setContentHandler(this);
				saxstorage.play();
				
			} catch (SAXException saxexception) {
				throw new LicenseException(("Cannot parse license file '"
						+ urlspec + "'"), saxexception);
			} catch (IOException ioexception) {
				throw new LicenseException(("Cannot read license file '"
						+ urlspec + "'"), ioexception);
			} catch (Exception exception) {
				exception.printStackTrace();
				throw new LicenseException(("Error processing license file '"
						+ urlspec + "'"), exception);
			}
			
			if (!bool)
				throw new LicenseException(
						"License check failed on license file '" + urlspec
								+ "'");
			if (compareDate(expirationDate) < 0)
				throw new LicenseException("License in file '" + urlspec
						+ "' expired " + expirationDate);
			*/
		}

		public void startElement(String string, String string_15_,
				String string_16_, Attributes attributes) {
			if (string_15_.equals("license"))
				expirationDate = attributes.getValue("expires");
			else if (string_15_.equals("product")
					&& "XEP".equals(attributes.getValue("name")))
				insideProduct = true;
			else if (insideProduct && string_15_.equals("feature")) {
				String string_17_ = attributes.getValue("name");
				if ("stamp".equals(string_17_))
					hasStamp = true;
				else if ("annotation".equals(string_17_)) {
					annotationBuffer = new StringBuffer();
					insideAnnotation = true;
				}
			}
		}

		public void endElement(String string, String string_18_,
				String string_19_) {
			if (string_18_.equals("product"))
				insideProduct = false;
			if (string_18_.equals("feature"))
				insideAnnotation = false;
		}

		public void characters(char[] cs, int i, int i_20_) {
			if (insideAnnotation)
				annotationBuffer.append(cs, i, i_20_);
		}

		public String getAnnotation() {
			if (annotationBuffer == null)
				return null;
			return annotationBuffer.toString().trim();
		}
	}

	private static final void createOption(String string, String string_21_,
			Object object) {
		optionTypes.put(string, string_21_);
		defaultOptions.put(string, object);
	}

	public static final String getBackendClassName(String string) {
		if (string == null)
			return null;
		String string_22_ = backendAliases.getProperty(string.trim()
				.toLowerCase());
		if (string_22_ == null)
			return null;
		return backendClasses.getProperty(string_22_);
	}

	public Properties getBackendOptions(String string) {
		if (string == null)
			return null;
		String string_23_ = backendAliases.getProperty(string.trim()
				.toLowerCase());
		if (string_23_ == null)
			return null;
		return (Properties) generatorOptions.get(string_23_);
	}

	public void cleanup() {
		imageFactory.clearCache();
		cache.cleanup();
	}

	protected URLCache getCache() {
		return new URLCache(TMPDIR, logger);
	}

	private static final String getDefaultConfigFile(Properties properties) {
		String string = properties.getProperty("CONFIG");
		if (string == null)
			string = properties.getProperty("com.renderx.xep.CONFIG");
		if (string == null)
			string = User.getProperty("com.renderx.xep.CONFIG");
		if (string == null)
			string = "xep.xml";
		return string;
	}

	private static final org.xml.sax.InputSource createInputSource(String string)
			throws ConfigurationException {
		try {
			return new InputSource(string);
		} catch (IOException ioexception) {
			throw new ConfigurationException(ioexception);
		}
	}

	private static final XMLReader createXMLReader()
			throws ConfigurationException {
		try {
			return XMLReaderFactory.createXMLReader();
		} catch (SAXException saxexception) {
			throw new ConfigurationException(saxexception);
		}
	}

	public Conf() throws ConfigurationException {
		this(new Properties(), new DefaultLogger());
	}

	public Conf(Properties properties, Logger logger)
			throws ConfigurationException {
		this(getDefaultConfigFile(properties), properties, logger);
	}

	public Conf(String string, Properties properties, Logger logger)
			throws ConfigurationException {
		this(createInputSource(string), createXMLReader(), properties, logger);
	}

	public Conf(org.xml.sax.InputSource inputsource, XMLReader xmlreader,
			Properties properties, Logger logger) throws ConfigurationException {
		this.logger = logger;
		Enumeration enumeration = properties.propertyNames();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			if (!"CONFIG".equals(string) && optionTypes.get(string) == null)
				printError("Invalid option name in the argument list: "
						+ string);
		}
		enumeration = User.getProperties().propertyNames();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			if (string.startsWith("com.renderx.xep.")) {
				String string_24_ = string.substring("com.renderx.xep."
						.length());
				if (string_24_.indexOf('.') == -1
						&& !"CONFIG".equals(string_24_)
						&& optionTypes.get(string_24_) == null)
					printError("Invalid name of the configuration system property: "
							+ string);
			}
		}
		String string = inputsource.getSystemId();
		SAXStorage saxstorage = new SAXStorage();
		try {
			xmlreader.setContentHandler(saxstorage);
			xmlreader.parse(inputsource);
		} catch (IOException ioexception) {
			throw new ConfigurationException(
					"Error while reading configuration file: "
							+ ioexception.toString());
		} catch (SAXException saxexception) {
			throw new ConfigurationException(
					"Structure error in configuration file: "
							+ saxexception.toString());
		}
		Config config = new Config();
		try {
			config.parse(new NullSource(string), saxstorage);
		} catch (IOException ioexception) {
			throw new ConfigurationException(
					"Error while reading configuration file: "
							+ ioexception.toString());
		} catch (SAXException saxexception) {
			throw new ConfigurationException(
					"Structure error in configuration file: "
							+ saxexception.toString());
		}
		URLSpec urlspec = null;
		boolean bool = false;
		Hashtable hashtable = new Hashtable();
		enumeration = optionTypes.propertyNames();
		while (enumeration.hasMoreElements()) {
			String string_25_ = (String) enumeration.nextElement();
			String string_26_ = optionTypes.getProperty(string_25_);
			Object object = properties.getProperty(string_25_);
			if (object == null)
				object = config.options.get(string_25_);
			if (object == null)
				object = User.getProperty("com.renderx.xep." + string_25_);
			if (object == null)
				object = defaultOptions.getProperty(string_25_);
			if (string_26_.equals("URI") && object instanceof String) {
				if (urlspec == null && string != null && !bool) {
					try {
						urlspec = new URLSpec(string);
					} catch (MalformedURLException malformedurlexception) {
						printError("Cannot make base directory from system ID: '"
								+ string + "'");
						bool = true;
					}
				}
				try {
					object = new URLSpec(urlspec, (String) object);
				} catch (MalformedURLException malformedurlexception) {
					throw new ConfigurationException(
							"Invalid URL in the value of property "
									+ string_25_ + ": '" + object + "'");
				}
			}
			hashtable.put(string_25_, object);
		}
		LicenseReader licensereader = new LicenseReader((URLSpec) hashtable
				.get("LICENSE"));
		ANNOTATION = licensereader.getAnnotation();
		PUT_STAMP = licensereader.hasStamp;
		APPLICATION_ID = "XEP 4.1 build 20050109";
		VALIDATOR_STYLESHEET = (URLSpec) hashtable.get("VALIDATOR_STYLESHEET");
		BROKENIMAGE = (URLSpec) hashtable.get("BROKENIMAGE");
		VALIDATE = getBooleanOption("VALIDATE", hashtable);
		DISCARD_IF_NOT_VALID = getBooleanOption("DISCARD_IF_NOT_VALID",
				hashtable);
		STRICTNESS = getIntOption("STRICTNESS", hashtable);
		XEP2_COMPATIBLE_MODE = getBooleanOption("XEP2_COMPATIBLE_MODE",
				hashtable);
		PAGEWIDE_FOOTNOTES = getBooleanOption("PAGEWIDE_FOOTNOTES", hashtable);
		DISCARD_AFTER_HEADER = getBooleanOption("DISCARD_AFTER_HEADER",
				hashtable);
		ENABLE_BIDI = getBooleanOption("ENABLE_BIDI", hashtable);
		MIN_PAGE_HEIGHT = getLengthOption("MIN_PAGE_HEIGHT", hashtable);
		MIN_PAGE_WIDTH = getLengthOption("MIN_PAGE_WIDTH", hashtable);
		PAGE_HEIGHT = getLengthOption("PAGE_HEIGHT", hashtable);
		PAGE_WIDTH = getLengthOption("PAGE_WIDTH", hashtable);
		MAX_PAGE_HEIGHT = getLengthOption("MAX_PAGE_HEIGHT", hashtable);
		MAX_PAGE_WIDTH = getLengthOption("MAX_PAGE_WIDTH", hashtable);
		LTF = getIntOption("LTF", hashtable);
		KERN = getBooleanOption("KERN", hashtable);
		CJF = getIntOption("CJF", hashtable);
		SHIFT_TARGET_X = getDoubleOption("SHIFT_TARGET_X", hashtable);
		SHIFT_TARGET_Y = getDoubleOption("SHIFT_TARGET_Y", hashtable);
		IGNORE_SPANNED_COLUMN_WIDTH = getBooleanOption(
				"IGNORE_SPANNED_COLUMN_WIDTH", hashtable);
		ALLOW_NULL_SYSTEM_ID = getBooleanOption("ALLOW_NULL_SYSTEM_ID",
				hashtable);
		String string_27_ = (String) hashtable.get("TMPDIR");
		if ("none".equals(string_27_.trim()))
			TMPDIR = null;
		else {
			if (string_27_.toLowerCase().startsWith("file:")) {
				Object object = null;
				URL url;
				try {
					url = new URL(string_27_);
				} catch (MalformedURLException malformedurlexception) {
					throw new ConfigurationException("File URL '" + string_27_
							+ "'in TMPDIR option cannot be parsed: "
							+ malformedurlexception.getMessage());
				}
				TMPDIR = URLUtil.toFile(url);
			} else
				TMPDIR = new File(string_27_);
			if (!TMPDIR.exists())
				throw new ConfigurationException("Temporary directory "
						+ TMPDIR + " does not exist");
			if (!TMPDIR.isDirectory())
				throw new ConfigurationException("Temporary directory "
						+ TMPDIR + " is not a directory");
			if (!TMPDIR.canWrite())
				throw new ConfigurationException("Temporary directory "
						+ TMPDIR + " is not writeable");
		}
		cache = getCache();
		TeXHyphenatorFactory texhyphenatorfactory = null;
		try {
			texhyphenatorfactory = new TeXHyphenatorFactory(new NullSource(
					string), saxstorage, this.logger);
		} catch (Exception exception) {
			if (this.logger != null)
				this.logger
						.exception(
								"Hyphenation failed to initialize; hyphenation disabled",
								exception);
		}
		hyphenFactory = texhyphenatorfactory;
		try {
			fontCatalog = new FontCatalog(new NullSource(string), saxstorage,
					cache, this.logger);
		} catch (Exception exception) {
			throw new ConfigurationException(exception);
		}
		imageFactory = new ImageFactory(fontCatalog, cache, this.logger,
				BROKENIMAGE);
		Parser.init();
	}

	private static final int compareDate(String string) throws LicenseException {
		if (string == null)
			return 0;
		StringTokenizer stringtokenizer = new StringTokenizer(string,
				"-/ \t\r\n");
		try {
			if (!stringtokenizer.hasMoreTokens())
				return 0;
			int i = Integer.parseInt(stringtokenizer.nextToken());
			if (i >= 0 && i <= 99)
				i += 2000;
			else if (i < 2000)
				return 0;
			if (!stringtokenizer.hasMoreTokens())
				return 0;
			int i_28_ = Integer.parseInt(stringtokenizer.nextToken()) - 1;
			if (i_28_ < 0 || i_28_ >= 12)
				return 0;
			if (!stringtokenizer.hasMoreTokens())
				return 0;
			int i_29_ = Integer.parseInt(stringtokenizer.nextToken());
			if (i_29_ <= 0 || i_29_ > 31)
				return 0;
			if (stringtokenizer.hasMoreTokens())
				return 0;
			GregorianCalendar gregoriancalendar = new GregorianCalendar();
			int i_30_ = gregoriancalendar.get(1);
			int i_31_ = gregoriancalendar.get(2);
			int i_32_ = gregoriancalendar.get(5);
			if (i_30_ != i)
				return i - i_30_;
			if (i_31_ != i_28_)
				return i_28_ - i_31_;
			if (i_32_ != i_29_)
				return i_29_ - i_32_;
		} catch (NumberFormatException numberformatexception) {
			/* empty */
		}
		return 0;
	}

	private void printWarning(String string) {
		if (logger != null) {
			synchronized (logger) {
				logger.warning(string);
			}
		}
	}

	private void printError(String string) {
		if (logger != null) {
			synchronized (logger) {
				logger.error(string);
			}
		}
	}

	private void printException(String string, Exception exception) {
		if (logger != null) {
			synchronized (logger) {
				logger.exception(string, exception);
			}
		}
	}

	private boolean getBooleanOption(String string, Hashtable hashtable) {
		String string_33_ = (String) hashtable.get(string);
		if ("true".equalsIgnoreCase(string_33_.trim()))
			return true;
		if ("false".equalsIgnoreCase(string_33_.trim()))
			return false;
		if ("yes".equalsIgnoreCase(string_33_.trim()))
			return true;
		if ("no".equalsIgnoreCase(string_33_.trim()))
			return false;
		String string_34_ = (String) defaultOptions.get(string);
		printError("Invalid boolean value " + string + "='" + string_33_
				+ "'; reverting to the default value of '" + string_34_ + "'");
		return "true".equalsIgnoreCase(string_34_);
	}

	private int getIntOption(String string, Hashtable hashtable) {
		String string_35_ = (String) hashtable.get(string);
		try {
			return Integer.parseInt(string_35_.trim());
		} catch (NumberFormatException numberformatexception) {
			String string_36_ = (String) defaultOptions.get(string);
			printError("Invalid integer value " + string + "='" + string_35_
					+ "'; reverting to the default value of '" + string_36_
					+ "'");
			try {
				return Integer.parseInt(string_36_);
			} catch (NumberFormatException numberformatexception_37_) {
				throw new RuntimeException("Invalid integer value " + string
						+ "='" + string_36_ + "' in the default values: "
						+ numberformatexception_37_.getMessage());
			}
		}
	}

	private double getDoubleOption(String string, Hashtable hashtable) {
		String string_38_ = (String) hashtable.get(string);
		try {
			return Double.valueOf(string_38_.trim()).doubleValue();
		} catch (NumberFormatException numberformatexception) {
			String string_39_ = (String) defaultOptions.get(string);
			printError("Invalid double value " + string + "='" + string_38_
					+ "'; reverting to the default value of '" + string_39_
					+ "'");
			try {
				return Double.valueOf(string_39_).doubleValue();
			} catch (NumberFormatException numberformatexception_40_) {
				throw new RuntimeException("Invalid double value " + string
						+ "='" + string_39_ + "' in the default values: "
						+ numberformatexception_40_.getMessage());
			}
		}
	}

	private int getLengthOption(String string, Hashtable hashtable) {
		String string_41_ = (String) hashtable.get(string);
		try {
			return parseLengthSpecifier(string_41_.trim());
		} catch (NumberFormatException numberformatexception) {
			String string_42_ = (String) defaultOptions.get(string);
			printError("Invalid length specifier " + string + "='" + string_41_
					+ "'; reverting to the default value of '" + string_42_
					+ "'");
			try {
				return parseLengthSpecifier(string_42_);
			} catch (NumberFormatException numberformatexception_43_) {
				throw new RuntimeException("Invalid length specifier " + string
						+ "='" + string_42_ + "' in the default values: "
						+ numberformatexception_43_.getMessage());
			}
		}
	}

	private static final int parseLengthSpecifier(String string)
			throws NumberFormatException {
		double d = 1.0;
		if (string.endsWith("pt")) {
			d = 1.0;
			string = string.substring(0, string.length() - 2);
		} else if (string.endsWith("in")) {
			d = 72.0;
			string = string.substring(0, string.length() - 2);
		} else if (string.endsWith("px")) {
			d = 0.6;
			string = string.substring(0, string.length() - 2);
		} else if (string.endsWith("pc")) {
			d = 12.0;
			string = string.substring(0, string.length() - 2);
		} else if (string.endsWith("cm")) {
			d = 28.346456692913385;
			string = string.substring(0, string.length() - 2);
		} else if (string.endsWith("mm")) {
			d = 2.834645669291339;
			string = string.substring(0, string.length() - 2);
		} else
			throw new NumberFormatException("Unknown measurement units in '"
					+ string + "'");
		return (int) Math.round(Double.valueOf(string.trim()).doubleValue() * d
				* 1000.0);
	}

	static {
		createOption("TMPDIR", "file", ".");
		createOption("LICENSE", "URI", "license.xml");
		createOption("VALIDATOR_STYLESHEET", "URI",
				"resource:com/renderx/xep/folint.xsl");
		createOption("BROKENIMAGE", "URI", "404.gif");
		createOption("VALIDATE", "boolean", "true");
		createOption("DISCARD_IF_NOT_VALID", "boolean", "true");
		createOption("STRICTNESS", "int", "1");
		createOption("XEP2_COMPATIBLE_MODE", "boolean", "true");
		createOption("ENABLE_BIDI", "boolean", "true");
		createOption("PAGEWIDE_FOOTNOTES", "boolean", "true");
		createOption("DISCARD_AFTER_HEADER", "boolean", "false");
		createOption("MIN_PAGE_HEIGHT", "length", "18pt");
		createOption("MIN_PAGE_WIDTH", "length", "18pt");
		createOption("PAGE_HEIGHT", "length", "11in");
		createOption("PAGE_WIDTH", "length", "8in");
		createOption("MAX_PAGE_HEIGHT", "length", "200in");
		createOption("MAX_PAGE_WIDTH", "length", "200in");
		createOption("LTF", "int", "20");
		createOption("CJF", "int", "100");
		createOption("KERN", "boolean", "true");
		createOption("SHIFT_TARGET_X", "double", "0.9");
		createOption("SHIFT_TARGET_Y", "double", "0.9");
		createOption("IGNORE_SPANNED_COLUMN_WIDTH", "boolean", "false");
		createOption("ALLOW_NULL_SYSTEM_ID", "boolean", "false");
		backendClasses = new Properties();
		backendClasses.put("PDF", "com.renderx.xep.gen.backends.H4PDF");
		backendClasses.put("PostScript", "com.renderx.xep.gen.backends.H4PS");
		backendClasses.put("XEP", "com.renderx.xep.gen.backends.H4XEP");
		backendAliases = new Properties();
		backendAliases.put("pdf", "PDF");
		backendAliases.put("postscript", "PostScript");
		backendAliases.put("ps", "PostScript");
		backendAliases.put("xep", "XEP");
		backendAliases.put("at", "XEP");
		backendAliases.put("xml", "XEP");
	}
}