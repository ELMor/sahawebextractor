/* H4PDF - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.gen.backends;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.StringTokenizer;

import com.renderx.pdflib.PDFDocument;
import com.renderx.pdflib.PDFExternalFileException;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.util.URLSpec;
import com.renderx.xep.gen.H4base;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class H4PDF extends H4base
{
    private static final String XEPURI = "http://www.renderx.com/XEP/xep";
    private static final short O_COMPRESS = 0;
    private static final short O_UNICODE_ANNOTATIONS = 1;
    private static final short O_OWNERPASSWORD = 2;
    private static final short O_USERPASSWORD = 3;
    private static final short O_USERPRIVILEGES = 4;
    private static final short O_DROP_UNUSED_DESTINATIONS = 5;
    private static final short O_LINEARIZE = 6;
    private static final short O_INITIAL_ZOOM = 7;
    private static final short O_VIEW_MODE = 8;
    private static final short O_PDF_VERSION = 9;
    private static final short O_CROP_OFFSET = 10;
    private static final short O_CROP_MARK_WIDTH = 11;
    private static final short O_BLEED = 12;
    private static final short O_BLEED_MARK_WIDTH = 13;
    private static final short O_PRINTER_MARK = 14;
    private static final Hashtable optab = new Hashtable();
    private boolean COMPRESS;
    private boolean UNICODE_ANNOTATIONS;
    private boolean DROP_UNUSED_DESTINATIONS;
    private boolean LINEARIZE;
    private String OWNERPASSWORD;
    private String USERPASSWORD;
    private String USERPRIVILEGES;
    private String ZOOM;
    private String VIEW_MODE;
    private String PDF_VERSION;
    private Hashtable PAGE_BOUNDARIES = new Hashtable();
    private List PRINTER_MARKS = new List();
    private boolean needsAnnotation = false;
    private int USERFLAGS = -64;
    private PDFDocument pdf = null;
    
    public static final class PageBoundaries
    {
	private float[] mediaBox = null;
	private float[] bleedBox = null;
	private float cropMarkWidth = -1.0F;
	private float bleedMarkWidth = -1.0F;
	
	public PageBoundaries() {
	    /* empty */
	}
	
	public PageBoundaries(PageBoundaries pageboundaries_0_) {
	    if (pageboundaries_0_.isMediaBox())
		mediaBox = pageboundaries_0_.getMediaBox();
	    else
		mediaBox = null;
	    if (pageboundaries_0_.isBleedBox())
		bleedBox = pageboundaries_0_.getBleedBox();
	    else
		bleedBox = null;
	    if (pageboundaries_0_.isCropMarkWidth())
		cropMarkWidth = pageboundaries_0_.getCropMarkWidth();
	    else
		cropMarkWidth = -1.0F;
	    if (pageboundaries_0_.isBleedMarkWidth())
		bleedMarkWidth = pageboundaries_0_.getBleedMarkWidth();
	    else
		bleedMarkWidth = -1.0F;
	}
	
	public boolean isMediaBox() {
	    return mediaBox != null;
	}
	
	public boolean isBleedBox() {
	    return bleedBox != null;
	}
	
	public boolean isCropMarkWidth() {
	    return cropMarkWidth != -1.0F;
	}
	
	public boolean isBleedMarkWidth() {
	    return bleedMarkWidth != -1.0F;
	}
	
	public float[] getMediaBox() {
	    return (isMediaBox() ? mediaBox : new float[]
		    { 0.0F, 0.0F, 0.0F, 0.0F });
	}
	
	public float[] getBleedBox() {
	    return (isBleedBox() ? bleedBox : new float[]
		    { 0.0F, 0.0F, 0.0F, 0.0F });
	}
	
	public float getCropMarkWidth() {
	    return isCropMarkWidth() ? cropMarkWidth : 0.0F;
	}
	
	public float getBleedMarkWidth() {
	    return isBleedMarkWidth() ? bleedMarkWidth : 0.0F;
	}
	
	public void setMediaBox(float[] fs) {
	    mediaBox = fs;
	}
	
	public void setBleedBox(float[] fs) {
	    bleedBox = fs;
	}
	
	public void setCropMarkWidth(float f) {
	    cropMarkWidth = f;
	}
	
	public void setBleedMarkWidth(float f) {
	    bleedMarkWidth = f;
	}
    }
    
    private static class Permission
    {
	static final int canNothing = -64;
	static final int canPrint = 4;
	static final int canModify = 8;
	static final int canCopy = 16;
	static final int canAnnotate = 32;
	
	private Permission() {
	    /* empty */
	}
    }
    
    public H4PDF(OutputStream outputstream, Session session,
		 Properties properties) throws IOException {
	super(outputstream, session, properties);
    }
    
    protected void resetDefaultOptions() {
	COMPRESS = true;
	UNICODE_ANNOTATIONS = true;
	DROP_UNUSED_DESTINATIONS = true;
	LINEARIZE = false;
	OWNERPASSWORD = "";
	USERPASSWORD = "";
	USERPRIVILEGES = "annotate";
	ZOOM = "auto";
	VIEW_MODE = "auto";
	PDF_VERSION = "1.4";
	PAGE_BOUNDARIES = new Hashtable();
	PRINTER_MARKS = new List();
    }
    
    protected void setOption(String string, String string_1_) {
	Object object = optab.get(string);
	if (object == null)
	    session.warning("Unrecognized PDF option " + string
			    + ": setting ignored");
	else {
	    switch (((Short) object).shortValue()) {
	    case 0:
		string_1_ = string_1_.trim().toLowerCase();
		if ("true".equals(string_1_) || "yes".equals(string_1_))
		    COMPRESS = true;
		else if ("false".equals(string_1_) || "no".equals(string_1_))
		    COMPRESS = false;
		else
		    session.warning("Invalid COMPRESS value: " + string_1_
				    + "; setting ignored");
		break;
	    case 6:
		string_1_ = string_1_.trim().toLowerCase();
		if ("true".equals(string_1_) || "yes".equals(string_1_))
		    LINEARIZE = true;
		else if ("false".equals(string_1_) || "no".equals(string_1_))
		    LINEARIZE = false;
		else
		    session.warning("Invalid LINEARIZE value: " + string_1_
				    + "; setting ignored");
		break;
	    case 5:
		string_1_ = string_1_.trim().toLowerCase();
		if ("true".equals(string_1_) || "yes".equals(string_1_))
		    DROP_UNUSED_DESTINATIONS = true;
		else if ("false".equals(string_1_) || "no".equals(string_1_))
		    DROP_UNUSED_DESTINATIONS = false;
		else
		    session.warning
			("Invalid value for DROP_UNUSED_DESTINATIONS: "
			 + string_1_ + "; setting ignored");
		break;
	    case 1:
		string_1_ = string_1_.trim().toLowerCase();
		if ("true".equals(string_1_) || "yes".equals(string_1_))
		    UNICODE_ANNOTATIONS = true;
		else if ("false".equals(string_1_) || "no".equals(string_1_))
		    UNICODE_ANNOTATIONS = false;
		else
		    session.warning("Invalid UNICODE_ANNOTATIONS value: "
				    + string_1_ + "; setting ignored");
		break;
	    case 2:
		OWNERPASSWORD = string_1_;
		break;
	    case 3:
		USERPASSWORD = string_1_;
		break;
	    case 4:
		USERPRIVILEGES = string_1_;
		break;
	    case 7:
		ZOOM = string_1_;
		break;
	    case 8:
		VIEW_MODE = string_1_;
		break;
	    case 9:
		PDF_VERSION = string_1_;
		break;
	    case 10:
	    case 12: {
		float[] fs = parseOffsets(string, string_1_);
		if (pdf != null)
		    pdf.setPageBoundaryEntry(string, fs);
		else
		    PAGE_BOUNDARIES.put(string, fs);
		break;
	    }
	    case 11:
	    case 13:
		string_1_ = string_1_.trim();
		if ("none".equals(string_1_))
		    string_1_ = "0pt";
		try {
		    string_1_
			= String.valueOf(parseLengthSpecifier(string_1_));
		} catch (NumberFormatException numberformatexception) {
		    session.error(numberformatexception.getMessage()
				  + "; option '" + string + "' ignored");
		    break;
		}
		if (pdf != null)
		    pdf.setPageBoundaryEntry(string, string_1_);
		else
		    PAGE_BOUNDARIES.put(string, string_1_);
		break;
	    case 14: {
		String string_2_ = resolveURL(string, string_1_);
		if (pdf != null)
		    pdf.setPrinterMarkEntry(string_2_);
		else
		    PRINTER_MARKS.append(string_2_);
		break;
	    }
	    }
	}
    }
    
    private final float[] parseOffsets(String string, String string_3_) {
	int i = 0;
	int i_4_ = 0;
	int i_5_ = 0;
	int i_6_ = 0;
	string_3_.trim();
	StringTokenizer stringtokenizer
	    = new StringTokenizer(string_3_, " ,\t\n\r\014");
	int i_7_ = 0;
	while (stringtokenizer.hasMoreTokens()) {
	    stringtokenizer.nextToken();
	    i_7_++;
	}
	stringtokenizer = new StringTokenizer(string_3_);
	try {
	    switch (i_7_) {
	    case 1:
		if (stringtokenizer.hasMoreTokens()) {
		    String string_8_ = stringtokenizer.nextToken();
		    string_8_.trim();
		    i = parseLengthSpecifier(string_8_);
		    i_4_ = parseLengthSpecifier(string_8_);
		    i_5_ = parseLengthSpecifier(string_8_);
		    i_6_ = parseLengthSpecifier(string_8_);
		}
		break;
	    case 2:
		if (stringtokenizer.hasMoreTokens()) {
		    String string_9_ = stringtokenizer.nextToken();
		    string_9_.trim();
		    i = parseLengthSpecifier(string_9_);
		    i_5_ = parseLengthSpecifier(string_9_);
		    if (stringtokenizer.hasMoreTokens()) {
			string_9_ = stringtokenizer.nextToken();
			string_9_.trim();
			i_4_ = parseLengthSpecifier(string_9_);
			i_6_ = parseLengthSpecifier(string_9_);
		    }
		}
		break;
	    case 3:
		if (stringtokenizer.hasMoreTokens()) {
		    String string_10_ = stringtokenizer.nextToken();
		    string_10_.trim();
		    i = parseLengthSpecifier(string_10_);
		    if (stringtokenizer.hasMoreTokens()) {
			string_10_ = stringtokenizer.nextToken();
			string_10_.trim();
			i_4_ = parseLengthSpecifier(string_10_);
			i_6_ = parseLengthSpecifier(string_10_);
			if (stringtokenizer.hasMoreTokens()) {
			    string_10_ = stringtokenizer.nextToken();
			    string_10_.trim();
			    i_5_ = parseLengthSpecifier(string_10_);
			}
		    }
		}
		break;
	    case 4:
		if (stringtokenizer.hasMoreTokens()) {
		    String string_11_ = stringtokenizer.nextToken();
		    string_11_.trim();
		    i = parseLengthSpecifier(string_11_);
		    if (stringtokenizer.hasMoreTokens()) {
			string_11_ = stringtokenizer.nextToken();
			string_11_.trim();
			i_4_ = parseLengthSpecifier(string_11_);
			if (stringtokenizer.hasMoreTokens()) {
			    string_11_ = stringtokenizer.nextToken();
			    string_11_.trim();
			    i_5_ = parseLengthSpecifier(string_11_);
			    if (stringtokenizer.hasMoreTokens()) {
				string_11_ = stringtokenizer.nextToken();
				string_11_.trim();
				i_6_ = parseLengthSpecifier(string_11_);
			    }
			}
		    }
		}
		break;
	    default:
		session.warning("Incorrect number of tokens in " + string
				+ "; setting ignored");
	    }
	} catch (NumberFormatException numberformatexception) {
	    session.warning(numberformatexception.getMessage() + "; option '"
			    + string + "' ignored");
	}
	return (new float[]
		{ (float) i_6_, (float) i_5_, (float) i_4_, (float) i });
    }
    
    private static final int parseLengthSpecifier(String string)
	throws NumberFormatException {
	double d = 1.0;
	if (string.endsWith("pt")) {
	    d = 1.0;
	    string = string.substring(0, string.length() - 2);
	} else if (string.endsWith("in")) {
	    d = 72.0;
	    string = string.substring(0, string.length() - 2);
	} else if (string.endsWith("px")) {
	    d = 0.6;
	    string = string.substring(0, string.length() - 2);
	} else if (string.endsWith("pc")) {
	    d = 12.0;
	    string = string.substring(0, string.length() - 2);
	} else if (string.endsWith("cm")) {
	    d = 28.346456692913385;
	    string = string.substring(0, string.length() - 2);
	} else if (string.endsWith("mm")) {
	    d = 2.834645669291339;
	    string = string.substring(0, string.length() - 2);
	} else
	    throw new NumberFormatException("Unknown measurement units in '"
					    + string + "'");
	return (int) Math.round(Double.valueOf(string.trim()).doubleValue()
				* d);
    }
    
    private final String resolveURL(String string, String string_12_) {
	if (string_12_ != null && string_12_.trim().startsWith("url")) {
	    String string_13_ = string_12_.trim().substring(3).trim();
	    if (string_13_.length() > 2 && string_13_.charAt(0) == '('
		&& string_13_.charAt(string_13_.length() - 1) == ')') {
		string_13_
		    = string_13_.substring(1, string_13_.length() - 1).trim();
		if (string_13_.length() > 2) {
		    if ((string_13_.charAt(0) == '\''
			 && string_13_.charAt(string_13_.length() - 1) == '\'')
			|| (string_13_.charAt(0) == '\"'
			    && (string_13_.charAt(string_13_.length() - 1)
				== '\"')))
			string_13_
			    = string_13_.substring(1, string_13_.length() - 1);
		    return string_13_;
		}
	    }
	}
	session.warning("Incorrect syntax in " + string
			+ "; should be: url('<sorce name>')");
	return string_12_;
    }
    
    public String outFormat() {
	return "pdf";
    }
    
    protected String getPropertyPrefix() {
	return "com.renderx.xep.H4PDF.";
    }
    
    protected String getProcessingInstructionPrefix() {
	return "xep-pdf-";
    }
    
    private void initDocument() {
	try {
	    pdf = new PDFDocument(outstream, session.config.imageFactory,
				  session.config.fontCatalog, session);
	} catch (IOException ioexception) {
	    session.exception("Cannot initialize PDF document", ioexception);
	    throw new RuntimeException("Invalid output stream"
				       + ioexception.toString());
	}
	pdf.setTMPDIR(session.config.TMPDIR);
	pdf.setCompression(COMPRESS);
	pdf.setUnicodeAnnotations(UNICODE_ANNOTATIONS);
	pdf.setDestinationTreatment(DROP_UNUSED_DESTINATIONS);
	pdf.setLinearize(LINEARIZE);
	pdf.setInitialZoom(ZOOM);
	pdf.setViewMode(VIEW_MODE);
	pdf.setPDFVersion(PDF_VERSION);
	if (session.config.ANNOTATION != null)
	    needsAnnotation = true;
	if (OWNERPASSWORD.length() != 0 || USERPASSWORD.length() != 0) {
	    if (OWNERPASSWORD.length() == 0)
		OWNERPASSWORD = USERPASSWORD;
	    int i = -64;
	    if (USERPRIVILEGES != null) {
		StringTokenizer stringtokenizer
		    = new StringTokenizer(USERPRIVILEGES, " \t\r\n,+;:");
		while (stringtokenizer.hasMoreTokens()) {
		    String string = stringtokenizer.nextToken();
		    if ("print".equalsIgnoreCase(string))
			i |= 0x4;
		    else if ("annotate".equalsIgnoreCase(string))
			i |= 0x20;
		    else if ("modify".equalsIgnoreCase(string))
			i |= 0x8;
		    else if ("copy".equalsIgnoreCase(string))
			i |= 0x10;
		    else
			session.warning("Invalid token in USERPRIVILEGES: "
					+ string + "; token ignored");
		}
	    }
	    pdf.setEncryption(USERPASSWORD, OWNERPASSWORD, i);
	}
	Enumeration enumeration = PAGE_BOUNDARIES.keys();
	while (enumeration.hasMoreElements()) {
	    String string = (String) enumeration.nextElement();
	    Object object = PAGE_BOUNDARIES.get(string);
	    pdf.setPageBoundaryEntry(string, object);
	}
	enumeration = PRINTER_MARKS.elements();
	while (enumeration.hasMoreElements()) {
	    String string = (String) enumeration.nextElement();
	    pdf.setPrinterMarkEntry(string);
	}
    }
    
    private void writeHeader() throws PDFLibInternalError {
	try {
	    pdf.startDocument();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setInfo(String string, String string_14_)
	throws PDFLibInternalError {
	try {
	    pdf.setInfo(string, string_14_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void beginPage(float f, float f_15_, String string)
	throws PDFLibInternalError {
	try {
	    pdf.beginPage(f, f_15_, string);
	    if (needsAnnotation) {
		pdf.addTextAnnotation(0.0F, f_15_ - Sticker.height,
				      Sticker.width, f_15_,
				      Sticker
					  .makeText(session.config.ANNOTATION),
				      Sticker.title, Sticker.color);
		needsAnnotation = false;
	    }
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void endPage() throws PDFLibInternalError {
	try {
	    pdf.endPage();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void close() throws PDFLibInternalError {
	try {
	    pdf.endDocument();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void rotate(float f) throws PDFLibInternalError {
	try {
	    pdf.rotate(f);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void scale(float f, float f_16_) throws PDFLibInternalError {
	try {
	    pdf.scale(f, f_16_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void translate(float f, float f_17_) throws PDFLibInternalError {
	try {
	    pdf.translate(f, f_17_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void transform(float f, float f_18_, float f_19_, float f_20_,
			   float f_21_,
			   float f_22_) throws PDFLibInternalError {
	try {
	    pdf.transform(f, f_18_, f_19_, f_20_, f_21_, f_22_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void moveTo(float f, float f_23_) throws PDFLibInternalError {
	try {
	    pdf.moveTo(f, f_23_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void lineTo(float f, float f_24_) throws PDFLibInternalError {
	try {
	    pdf.lineTo(f, f_24_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void curveTo(float f, float f_25_, float f_26_, float f_27_,
			 float f_28_, float f_29_) {
	try {
	    pdf.curveTo(f, f_25_, f_26_, f_27_, f_28_, f_29_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void rectangle(float f, float f_30_, float f_31_, float f_32_)
	throws PDFLibInternalError {
	try {
	    pdf.rectangle(f, f_30_, f_31_, f_32_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setTextPos(float f, float f_33_) throws PDFLibInternalError {
	try {
	    pdf.setTextPos(f, f_33_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setCharSpacing(float f) throws PDFLibInternalError {
	try {
	    pdf.setCharSpacing(f);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setWordSpacing(float f) throws PDFLibInternalError {
	try {
	    pdf.setWordSpacing(f);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setFontStretch(float f) {
	pdf.setHorzScale(f);
    }
    
    private void processText(String string)
	throws PDFLibInternalError, IOException {
	pdf.processText(string);
    }
    
    private void setFont(String string, int i, String string_34_,
			 String string_35_, float f) {
	pdf.selectFont(string, i, string_34_, string_35_);
	pdf.setFontSize(f);
    }
    
    private void save() throws PDFLibInternalError {
	try {
	    pdf.save();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void restore() throws PDFLibInternalError {
	try {
	    pdf.restore();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setBorderStyle(String string, int i)
	throws PDFLibInternalError {
	try {
	    pdf.setBorderStyle(string, (float) i);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setDash(float f, float f_36_, float f_37_)
	throws PDFLibInternalError {
	try {
	    pdf.setDash(f, f_36_, f_37_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setLineCap(int i) throws PDFLibInternalError {
	try {
	    pdf.setLineCap(i);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setLineWidth(float f) throws PDFLibInternalError {
	try {
	    pdf.setLineWidth(f);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void placeImage(String string, String string_38_,
			    String string_39_) throws PDFLibInternalError {
	try {
	    try {
		pdf.placeImage(new URLSpec((string_38_ == null ? null
					    : new URLSpec(string_38_)),
					   string),
			       string_39_);
	    } catch (PDFExternalFileException pdfexternalfileexception) {
		session.warning("Cannot insert image: "
				+ pdfexternalfileexception.getMessage());
	    }
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setRGBColor(float f, float f_40_, float f_41_)
	throws PDFLibInternalError {
	try {
	    pdf.setRGBColor(f, f_40_, f_41_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setGray(float f) throws PDFLibInternalError {
	try {
	    pdf.setGray(f);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setCMYKColor
	(float f, float f_42_, float f_43_, float f_44_)
	throws PDFLibInternalError {
	try {
	    pdf.setCMYKColor(f, f_42_, f_43_, f_44_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void setSpotColor(float f, String string, float[] fs)
	throws PDFLibInternalError {
	try {
	    pdf.setSpotColor(f, string, fs);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void clip() throws PDFLibInternalError {
	try {
	    pdf.clip();
	    pdf.endPath();
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void closePath() throws PDFLibInternalError {
	try {
	    pdf.closePath();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void fill() throws PDFLibInternalError {
	try {
	    pdf.fill();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void stroke() throws PDFLibInternalError {
	try {
	    pdf.stroke();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void fill_stroke() throws PDFLibInternalError {
	try {
	    pdf.fillStroke();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void endPath() throws PDFLibInternalError {
	try {
	    pdf.endPath();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void addLocalLink(String string, float f, float f_45_, float f_46_,
			      float f_47_,
			      String string_48_) throws PDFLibInternalError {
	try {
	    pdf.addLocalLink(string, f, f_45_, f_46_, f_47_, string_48_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void addWebLink(float f, float f_49_, float f_50_, float f_51_,
			    String string,
			    boolean bool) throws PDFLibInternalError {
	try {
	    pdf.addWebLink(f, f_49_, f_50_, f_51_, string, bool);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void addLocalBookmark
	(String string, String string_52_, int i, int i_53_, boolean bool)
	throws PDFLibInternalError {
	try {
	    pdf.addLocalBookmark(string, string_52_, i, i_53_, bool);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void addWebBookmark(String string, int i, int i_54_,
				String string_55_, boolean bool,
				boolean bool_56_) throws PDFLibInternalError {
	try {
	    pdf.addWebBookmark(string, i, i_54_, string_55_, bool, bool_56_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    private void addNamedDestination(String string, float f, float f_57_)
	throws PDFLibInternalError {
	try {
	    pdf.addNamedDestination(string, f, f_57_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new PDFLibInternalError(exception);
	}
    }
    
    public void startDocument() throws SAXException {
	super.startDocument();
    }
    
    public void endDocument() throws SAXException {
	try {
	    close();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Exception exception) {
	    throw new InternalException("fatal error at the end of document "
					+ exception);
	}
	super.endDocument();
    }
    
    public void startElement(String string, String string_58_,
			     String string_59_,
			     Attributes attributes) throws SAXException {
	if ("http://www.renderx.com/XEP/xep".equals(string)) {
	    try {
		switch (((Short) H4base.idtab.get(string_58_)).shortValue()) {
		case 0: {
		    initDocument();
		    writeHeader();
		    setBorderStyle("solid", 0);
		    setInfo("Creator",
			    normalize_space(attributes.getValue("creator")));
		    setInfo("Author",
			    normalize_space(attributes.getValue("author")));
		    setInfo("Title",
			    normalize_space(attributes.getValue("title")));
		    setInfo("Subject",
			    normalize_space(attributes.getValue("subject")));
		    setInfo("Keywords",
			    normalize_space(attributes.getValue("keywords")));
		    String string_60_
			= attributes.getValue("initial-destination");
		    if (string_60_ != null)
			pdf.setInitialDestination(string_60_);
		    break;
		}
		case 1:
		    beginPage(parseLength(attributes.getValue("width")),
			      parseLength(attributes.getValue("height")),
			      attributes.getValue("page-id"));
		    break;
		case 2:
		    rotate(parseAngle(attributes.getValue("phi")));
		    break;
		case 3:
		    translate(parseLength(attributes.getValue("x")),
			      parseLength(attributes.getValue("y")));
		    break;
		case 4:
		    transform(parseFloat(attributes.getValue("a")),
			      parseFloat(attributes.getValue("b")),
			      parseFloat(attributes.getValue("c")),
			      parseFloat(attributes.getValue("d")),
			      parseFloat(attributes.getValue("e")),
			      parseFloat(attributes.getValue("f")));
		    break;
		case 6:
		    setWordSpacing(parseLength(attributes.getValue("value")));
		    break;
		case 7:
		    setCharSpacing(parseLength(attributes.getValue("value")));
		    break;
		case 8:
		    setFontStretch(parseFloat(attributes.getValue("value")));
		    break;
		case 9:
		    setFont(attributes.getValue("family"),
			    parseInteger(attributes.getValue("weight")),
			    attributes.getValue("style"),
			    attributes.getValue("variant"),
			    parseLength(attributes.getValue("size")));
		    break;
		case 10:
		    setTextPos(parseLength(attributes.getValue("x")),
			       parseLength(attributes.getValue("y")));
		    processText(attributes.getValue("value"));
		    break;
		case 11: {
		    save();
		    float f = parseLength(attributes.getValue("thickness"));
		    if (!(f <= 0.0F)) {
			float f_61_
			    = parseLength(attributes.getValue("x-from"));
			float f_62_
			    = parseLength(attributes.getValue("y-from"));
			float f_63_
			    = parseLength(attributes.getValue("x-till"));
			float f_64_
			    = parseLength(attributes.getValue("y-till"));
			String string_65_ = attributes.getValue("style");
			if (string_65_.equals("dotted")) {
			    float f_66_
				= (getLineLength(f_61_, f_62_, f_63_, f_64_)
				   / f);
			    float f_67_
				= (f_66_
				   - (float) (2.0
					      * (Math.floor
						 ((double) (f_66_ / 2.0F)))));
			    float f_68_ = 2.0F - f_67_ / 2.0F;
			    if (f_66_ > 2.0F && (double) f_68_ > 1.5)
				f_68_--;
			    setDash(f_68_ * f, 0.0F, 2.0F * f);
			    setLineCap(1);
			} else if (string_65_.equals("dashed")) {
			    float f_69_
				= (getLineLength(f_61_, f_62_, f_63_, f_64_)
				   / f);
			    float f_70_
				= (f_69_ + 4.0F
				   - (float) (10.0 * (Math.floor
						      ((double) ((f_69_ + 4.0F)
								 / 10.0F)))));
			    float f_71_ = 10.0F - f_70_ / 2.0F;
			    if (f_71_ < 8.0F || f_69_ < 6.0F)
				f_71_ -= 5.0F;
			    if (f_71_ == 10.0F)
				f_71_ = 0.0F;
			    setDash(f_71_ * f, 6.0F * f, 4.0F * f);
			    setLineCap(0);
			} else
			    setLineCap(0);
			setLineWidth(f);
			moveTo(f_61_, f_62_);
			lineTo(f_63_, f_64_);
			stroke();
			restore();
		    }
		    break;
		}
		case 15: {
		    float f = parseFloat(attributes.getValue("scale-x"));
		    float f_72_ = parseFloat(attributes.getValue("scale-y"));
		    if (f != 0.0F && f_72_ != 0.0F) {
			String string_73_ = attributes.getValue("src");
			String string_74_ = attributes.getValue("base");
			String string_75_ = attributes.getValue("type");
			save();
			translate(parseLength(attributes.getValue("x-from")),
				  parseLength(attributes.getValue("y-from")));
			scale(f, f_72_);
			placeImage(string_73_, string_74_, string_75_);
			restore();
		    }
		    break;
		}
		case 16:
		    setRGBColor(parseFloat(attributes.getValue("red")),
				parseFloat(attributes.getValue("green")),
				parseFloat(attributes.getValue("blue")));
		    break;
		case 17:
		    setGray(parseFloat(attributes.getValue("gray")));
		    break;
		case 18:
		    setCMYKColor(parseFloat(attributes.getValue("cyan")),
				 parseFloat(attributes.getValue("magenta")),
				 parseFloat(attributes.getValue("yellow")),
				 parseFloat(attributes.getValue("black")));
		    break;
		case 19: {
		    String string_76_ = attributes.getValue("colorant");
		    float f = parseFloat(attributes.getValue("tint"));
		    if (attributes.getValue("alt-gray") != null)
			setSpotColor(f, string_76_,
				     new float[] { parseFloat(attributes
								  .getValue
							      ("alt-gray")) });
		    else if (attributes.getValue("alt-red") != null)
			setSpotColor
			    (f, string_76_,
			     (new float[]
			      { parseFloat(attributes.getValue("alt-red")),
				parseFloat(attributes.getValue("alt-green")),
				parseFloat(attributes
					       .getValue("alt-blue")) }));
		    else if (attributes.getValue("alt-cyan") != null)
			setSpotColor
			    (f, string_76_,
			     (new float[]
			      { parseFloat(attributes.getValue("alt-cyan")),
				parseFloat(attributes.getValue("alt-magenta")),
				parseFloat(attributes.getValue("alt-yellow")),
				parseFloat(attributes
					       .getValue("alt-black")) }));
		    else
			setSpotColor(f, string_76_, new float[] { 0.0F });
		    break;
		}
		case 20:
		    setSpotColor(parseFloat(attributes.getValue("tint")),
				 "All", new float[] { 0.0F });
		    break;
		case 12: {
		    float f = parseLength(attributes.getValue("x-from"));
		    float f_77_ = parseLength(attributes.getValue("y-from"));
		    float f_78_ = parseLength(attributes.getValue("x-till"));
		    float f_79_ = parseLength(attributes.getValue("y-till"));
		    moveTo(f, f_77_);
		    lineTo(f, f_79_);
		    lineTo(f_78_, f_79_);
		    lineTo(f_78_, f_77_);
		    closePath();
		    fill();
		    break;
		}
		case 5: {
		    float f = parseLength(attributes.getValue("x-from"));
		    float f_80_ = parseLength(attributes.getValue("y-from"));
		    float f_81_ = parseLength(attributes.getValue("x-till"));
		    float f_82_ = parseLength(attributes.getValue("y-till"));
		    save();
		    moveTo(f, f_80_);
		    lineTo(f, f_82_);
		    lineTo(f_81_, f_82_);
		    lineTo(f_81_, f_80_);
		    closePath();
		    clip();
		    break;
		}
		case 13:
		    moveTo(parseLength(attributes.getValue("x-from")),
			   parseLength(attributes.getValue("y-from")));
		    break;
		case 14:
		    lineTo(parseLength(attributes.getValue("x-till")),
			   parseLength(attributes.getValue("y-till")));
		    break;
		case 22:
		    addLocalLink(attributes.getValue("destination-id"),
				 parseLength(attributes.getValue("x-from")),
				 parseLength(attributes.getValue("y-from")),
				 parseLength(attributes.getValue("x-till")),
				 parseLength(attributes.getValue("y-till")),
				 "retain");
		    break;
		case 23:
		    addWebLink(parseLength(attributes.getValue("x-from")),
			       parseLength(attributes.getValue("y-from")),
			       parseLength(attributes.getValue("x-till")),
			       parseLength(attributes.getValue("y-till")),
			       attributes.getValue("destination"),
			       ("new".equals
				(attributes.getValue("show-destination"))));
		    break;
		case 24:
		    addLocalBookmark
			(attributes.getValue("destination-id"),
			 normalize_space(attributes.getValue("label")),
			 parseInteger(attributes.getValue("id")),
			 parseInteger(attributes.getValue("parent-id")),
			 "false"
			     .equals(attributes.getValue("collapse-subtree")));
		    break;
		case 25:
		    addWebBookmark
			(normalize_space(attributes.getValue("label")),
			 parseInteger(attributes.getValue("id")),
			 parseInteger(attributes.getValue("parent-id")),
			 attributes.getValue("destination"),
			 "false"
			     .equals(attributes.getValue("collapse-subtree")),
			 "new"
			     .equals(attributes.getValue("show-destination")));
		    break;
		case 26:
		    break;
		case 21:
		    addNamedDestination(attributes.getValue("name"),
					parseLength(attributes.getValue("x")),
					parseLength(attributes.getValue("y")));
		    break;
		default:
		    throw new InternalException("unknown instruction '"
						+ string_58_ + "'");
		}
	    } catch (RuntimeException runtimeexception) {
		throw runtimeexception;
	    } catch (Exception exception) {
		throw new InternalException("fatal error at element: "
					    + string_58_ + " : " + exception);
	    }
	}
    }
    
    public void endElement
	(String string, String string_83_, String string_84_)
	throws SAXException {
	if ("http://www.renderx.com/XEP/xep".equals(string)) {
	    try {
		switch (((Short) H4base.idtab.get(string_83_)).shortValue()) {
		case 0:
		    break;
		case 1:
		    endPage();
		    break;
		case 2:
		case 3:
		case 4:
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
		    break;
		case 5:
		    restore();
		    break;
		case 13:
		    closePath();
		    fill();
		    break;
		case 14:
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
		case 26:
		    break;
		default:
		    throw new InternalException("unknown instruction '"
						+ string_83_ + "'");
		}
	    } catch (RuntimeException runtimeexception) {
		throw runtimeexception;
	    } catch (Exception exception) {
		throw new InternalException("fatal error at element: "
					    + string_83_ + " : " + exception);
	    }
	}
    }
    
    private static final int parseInteger(String string) {
	return Integer.valueOf(string).intValue();
    }
    
    private static final float parseFloat(String string) {
	return Float.valueOf(string).floatValue();
    }
    
    private static final float parseAngle(String string) {
	return parseFloat(string);
    }
    
    private static final float parseLength(String string) {
	return parseFloat(string) / 1000.0F;
    }
    
    private static final int parseCount(String string) {
	return parseInteger(string);
    }
    
    private String normalize_space(String string) {
	if (string == null)
	    return null;
	StringTokenizer stringtokenizer = new StringTokenizer(string);
	String string_85_ = "";
	while (stringtokenizer.hasMoreTokens())
	    string_85_ += " " + stringtokenizer.nextToken();
	return string_85_.trim();
    }
    
    private static final float getLineLength(float f, float f_86_, float f_87_,
					     float f_88_) {
	if (f == f_87_)
	    return f_86_ > f_88_ ? f_86_ - f_88_ : f_88_ - f_86_;
	if (f_86_ == f_88_)
	    return f > f_87_ ? f - f_87_ : f_87_ - f;
	float f_89_ = f - f_87_;
	float f_90_ = f_86_ - f_88_;
	return (float) Math.sqrt((double) (f_89_ * f_89_ + f_90_ * f_90_));
    }
    
    static {
	optab.put("COMPRESS", new Short((short) 0));
	optab.put("UNICODE_ANNOTATIONS", new Short((short) 1));
	optab.put("OWNERPASSWORD", new Short((short) 2));
	optab.put("USERPASSWORD", new Short((short) 3));
	optab.put("USERPRIVILEGES", new Short((short) 4));
	optab.put("DROP_UNUSED_DESTINATIONS", new Short((short) 5));
	optab.put("LINEARIZE", new Short((short) 6));
	optab.put("INITIAL_ZOOM", new Short((short) 7));
	optab.put("VIEW_MODE", new Short((short) 8));
	optab.put("PDF_VERSION", new Short((short) 9));
	optab.put("CROP_OFFSET", new Short((short) 10));
	optab.put("CROP_MARK_WIDTH", new Short((short) 11));
	optab.put("BLEED", new Short((short) 12));
	optab.put("BLEED_MARK_WIDTH", new Short((short) 13));
	optab.put("PRINTER_MARK", new Short((short) 14));
    }
}
