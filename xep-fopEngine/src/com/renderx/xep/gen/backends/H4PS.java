/*
 * H4PS - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.gen.backends;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.StringTokenizer;

import com.renderx.pslib.PSDocument;
import com.renderx.pslib.Strings;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.util.URLSpec;
import com.renderx.xep.gen.H4base;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class H4PS extends H4base {
	private static final String XEPURI = "http://www.renderx.com/XEP/xep";

	private static final short O_LANGUAGE_LEVEL = 0;

	private static final short O_UNICODE_ANNOTATIONS = 1;

	private static final short O_DROP_UNUSED_DESTINATIONS = 2;

	private static final short O_CLONE_EPS = 3;

	private static final short O_PAGE_DEVICE = 4;

	private static final short O_INITIAL_ZOOM = 5;

	private static final short O_VIEW_MODE = 6;

	private static final short O_CROP_OFFSET = 7;

	private static final short O_CROP_MARK_WIDTH = 8;

	private static final short O_BLEED = 9;

	private static final short O_BLEED_MARK_WIDTH = 10;

	private static final short O_PRINTER_MARK = 11;

	private static final Hashtable optab = new Hashtable();

	private PSDocument ps = null;

	private int LANGUAGE_LEVEL;

	private boolean UNICODE_ANNOTATIONS;

	private boolean DROP_UNUSED_DESTINATIONS;

	private boolean CLONE_EPS;

	private Hashtable PAGE_DEVICE = new Hashtable();

	private String ZOOM;

	private String VIEW_MODE;

	private Hashtable PAGE_BOUNDARIES = new Hashtable();

	private List PRINTER_MARKS = new List();

	private boolean needsAnnotation = false;

	public H4PS(OutputStream outputstream, Session session,
			Properties properties) throws IOException {
		super(outputstream, session, properties);
	}

	protected void resetDefaultOptions() {
		LANGUAGE_LEVEL = 3;
		UNICODE_ANNOTATIONS = true;
		DROP_UNUSED_DESTINATIONS = true;
		CLONE_EPS = true;
		ZOOM = "auto";
		VIEW_MODE = "auto";
		PAGE_BOUNDARIES = new Hashtable();
		PRINTER_MARKS = new List();
	}

	protected void setOption(String string, String string_0_) {
		Object object = optab.get(string);
		if (object == null)
			session.warning("Unrecognized PostScript option " + string
					+ ": setting ignored");
		else {
			switch (((Short) object).shortValue()) {
			case 0:
				string_0_ = string_0_.trim();
				if ("2".equals(string_0_) || "3".equals(string_0_))
					LANGUAGE_LEVEL = Integer.parseInt(string_0_);
				else
					session
							.warning("Incorrect or unsupported LANGUAGE_LEVEL value: "
									+ string_0_ + "; setting ignored");
				break;
			case 1:
				string_0_ = string_0_.trim().toLowerCase();
				if ("true".equals(string_0_) || "yes".equals(string_0_))
					UNICODE_ANNOTATIONS = true;
				else if ("false".equals(string_0_) || "no".equals(string_0_))
					UNICODE_ANNOTATIONS = false;
				else
					session.warning("Invalid UNICODE_ANNOTATIONS value: "
							+ string_0_ + "; setting ignored");
				break;
			case 2:
				string_0_ = string_0_.trim().toLowerCase();
				if ("true".equals(string_0_) || "yes".equals(string_0_))
					DROP_UNUSED_DESTINATIONS = true;
				else if ("false".equals(string_0_) || "no".equals(string_0_))
					DROP_UNUSED_DESTINATIONS = false;
				else
					session
							.warning("Invalid value for DROP_UNUSED_DESTINATIONS: "
									+ string_0_ + "; setting ignored");
				break;
			case 3:
				string_0_ = string_0_.trim().toLowerCase();
				if ("true".equals(string_0_) || "yes".equals(string_0_))
					CLONE_EPS = true;
				else if ("false".equals(string_0_) || "no".equals(string_0_))
					CLONE_EPS = false;
				else
					session.warning("Invalid CLONE_EPS value: " + string_0_
							+ "; setting ignored");
				break;
			case 4: {
				string_0_ = string_0_.trim();
				StringTokenizer stringtokenizer = new StringTokenizer(string_0_);
				if (stringtokenizer.hasMoreTokens()) {
					String string_1_ = stringtokenizer.nextToken();
					if (!stringtokenizer.hasMoreTokens())
						session
								.warning("No value specified for PAGE_DEVICE parameter "
										+ string_1_ + "; setting ignored");
					else {
						string_0_ = string_0_.substring(string_1_.length())
								.trim();
						if (ps != null)
							ps.setPageDeviceEntry(string_1_, string_0_);
						else
							PAGE_DEVICE.put(string_1_, string_0_);
					}
				}
				break;
			}
			case 5:
				ZOOM = string_0_;
				break;
			case 6:
				VIEW_MODE = string_0_;
				break;
			case 7:
			case 9: {
				float[] fs = parseOffsets(string, string_0_);
				if (ps != null)
					ps.setPageBoundaryEntry(string, fs);
				else
					PAGE_BOUNDARIES.put(string, fs);
				break;
			}
			case 8:
			case 10:
				string_0_ = string_0_.trim();
				if ("none".equals(string_0_))
					string_0_ = "0pt";
				try {
					string_0_ = String.valueOf(parseLengthSpecifier(string_0_));
				} catch (NumberFormatException numberformatexception) {
					session.error(numberformatexception.getMessage()
							+ "; option '" + string + "' ignored");
				}
				if (ps != null)
					ps.setPageBoundaryEntry(string, string_0_);
				else
					PAGE_BOUNDARIES.put(string, string_0_);
				break;
			case 11: {
				String string_2_ = resolveURL(string, string_0_);
				if (ps != null)
					ps.setPrinterMarkEntry(string_0_);
				else
					PRINTER_MARKS.append(string_0_);
				break;
			}
			}
		}
	}

	private final float[] parseOffsets(String string, String string_3_) {
		int i = 0;
		int i_4_ = 0;
		int i_5_ = 0;
		int i_6_ = 0;
		string_3_.trim();
		StringTokenizer stringtokenizer = new StringTokenizer(string_3_,
				" ,\t\n\r\014");
		int i_7_ = 0;
		while (stringtokenizer.hasMoreTokens()) {
			stringtokenizer.nextToken();
			i_7_++;
		}
		stringtokenizer = new StringTokenizer(string_3_);
		try {
			switch (i_7_) {
			case 1:
				if (stringtokenizer.hasMoreTokens()) {
					String string_8_ = stringtokenizer.nextToken();
					string_8_.trim();
					i = parseLengthSpecifier(string_8_);
					i_4_ = parseLengthSpecifier(string_8_);
					i_5_ = parseLengthSpecifier(string_8_);
					i_6_ = parseLengthSpecifier(string_8_);
				}
				break;
			case 2:
				if (stringtokenizer.hasMoreTokens()) {
					String string_9_ = stringtokenizer.nextToken();
					string_9_.trim();
					i = parseLengthSpecifier(string_9_);
					i_5_ = parseLengthSpecifier(string_9_);
					if (stringtokenizer.hasMoreTokens()) {
						string_9_ = stringtokenizer.nextToken();
						string_9_.trim();
						i_4_ = parseLengthSpecifier(string_9_);
						i_6_ = parseLengthSpecifier(string_9_);
					}
				}
				break;
			case 3:
				if (stringtokenizer.hasMoreTokens()) {
					String string_10_ = stringtokenizer.nextToken();
					string_10_.trim();
					i = parseLengthSpecifier(string_10_);
					if (stringtokenizer.hasMoreTokens()) {
						string_10_ = stringtokenizer.nextToken();
						string_10_.trim();
						i_4_ = parseLengthSpecifier(string_10_);
						i_6_ = parseLengthSpecifier(string_10_);
						if (stringtokenizer.hasMoreTokens()) {
							string_10_ = stringtokenizer.nextToken();
							string_10_.trim();
							i_5_ = parseLengthSpecifier(string_10_);
						}
					}
				}
				break;
			case 4:
				if (stringtokenizer.hasMoreTokens()) {
					String string_11_ = stringtokenizer.nextToken();
					string_11_.trim();
					i = parseLengthSpecifier(string_11_);
					if (stringtokenizer.hasMoreTokens()) {
						string_11_ = stringtokenizer.nextToken();
						string_11_.trim();
						i_4_ = parseLengthSpecifier(string_11_);
						if (stringtokenizer.hasMoreTokens()) {
							string_11_ = stringtokenizer.nextToken();
							string_11_.trim();
							i_5_ = parseLengthSpecifier(string_11_);
							if (stringtokenizer.hasMoreTokens()) {
								string_11_ = stringtokenizer.nextToken();
								string_11_.trim();
								i_6_ = parseLengthSpecifier(string_11_);
							}
						}
					}
				}
				break;
			default:
				session.warning("Incorrect number of tokens in " + string
						+ "; setting ignored");
			}
		} catch (NumberFormatException numberformatexception) {
			session.warning(numberformatexception.getMessage() + "; option '"
					+ string + "' ignored");
		}
		return (new float[] { (float) i_6_, (float) i_5_, (float) i_4_,
				(float) i });
	}

	private static final int parseLengthSpecifier(String string)
			throws NumberFormatException {
		double d = 1.0;
		if (string.endsWith("pt")) {
			d = 1.0;
			string = string.substring(0, string.length() - 2);
		} else if (string.endsWith("in")) {
			d = 72.0;
			string = string.substring(0, string.length() - 2);
		} else if (string.endsWith("px")) {
			d = 0.6;
			string = string.substring(0, string.length() - 2);
		} else if (string.endsWith("pc")) {
			d = 12.0;
			string = string.substring(0, string.length() - 2);
		} else if (string.endsWith("cm")) {
			d = 28.346456692913385;
			string = string.substring(0, string.length() - 2);
		} else if (string.endsWith("mm")) {
			d = 2.834645669291339;
			string = string.substring(0, string.length() - 2);
		} else
			throw new NumberFormatException("Unknown measurement units in '"
					+ string + "'");
		return (int) Math
				.round(Double.valueOf(string.trim()).doubleValue() * d);
	}

	private final String resolveURL(String string, String string_12_) {
		if (string_12_ != null && string_12_.trim().startsWith("url")) {
			String string_13_ = string_12_.trim().substring(3).trim();
			if (string_13_.length() > 2 && string_13_.charAt(0) == '('
					&& string_13_.charAt(string_13_.length() - 1) == ')') {
				string_13_ = string_13_.substring(1, string_13_.length() - 1)
						.trim();
				if (string_13_.length() > 2) {
					if ((string_13_.charAt(0) == '\'' && string_13_
							.charAt(string_13_.length() - 1) == '\'')
							|| (string_13_.charAt(0) == '\"' && (string_13_
									.charAt(string_13_.length() - 1) == '\"')))
						string_13_ = string_13_.substring(1, string_13_
								.length() - 1);
					return string_13_;
				}
			}
		}
		session.warning("Incorrect syntax in " + string
				+ "; should be: url('<sorce name>')");
		return string_12_;
	}

	public String outFormat() {
		return "ps";
	}

	protected String getPropertyPrefix() {
		return "com.renderx.xep.H4PS.";
	}

	protected String getProcessingInstructionPrefix() {
		return "xep-postscript-";
	}

	private void initDocument() {
		if (ps != null) {
			ps.setLanguageLevel(LANGUAGE_LEVEL);
			ps.setUnicodeAnnotations(UNICODE_ANNOTATIONS);
			ps.setCloneEps(CLONE_EPS);
			ps.setDestinationTreatment(DROP_UNUSED_DESTINATIONS);
			ps.setInitialZoom(ZOOM);
			ps.setViewMode(VIEW_MODE);
			if (session.config.ANNOTATION != null)
				needsAnnotation = true;
			Enumeration enumeration = PAGE_DEVICE.keys();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				String string_14_ = (String) PAGE_DEVICE.get(string);
				ps.setPageDeviceEntry(string, string_14_);
			}
			enumeration = PAGE_BOUNDARIES.keys();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				Object object = PAGE_BOUNDARIES.get(string);
				ps.setPageBoundaryEntry(string, object);
			}
			enumeration = PRINTER_MARKS.elements();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				ps.setPrinterMarkEntry(string);
			}
		}
	}

	public void startDocument() throws SAXException {
		super.startDocument();
		try {
			ps = new PSDocument(outstream, session.config.TMPDIR,
					session.config.imageFactory, session.config.fontCatalog,
					session);
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new InternalException("fatal error at the end of document "
					+ exception);
		}
	}

	public void endDocument() throws SAXException {
		try {
			ps.endDocument();
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		} catch (Exception exception) {
			throw new InternalException("fatal error at the end of document "
					+ exception);
		}
		super.endDocument();
	}

	public void startElement(String string, String string_15_,
			String string_16_, Attributes attributes) throws SAXException {
		if ("http://www.renderx.com/XEP/xep".equals(string)) {
			try {
				switch (((Short) H4base.idtab.get(string_15_)).shortValue()) {
				case 0: {
					initDocument();
					ps.setInfo("Creator", (Strings.normalize_space(attributes
							.getValue("creator"))));
					ps.setInfo("Author", (Strings.normalize_space(attributes
							.getValue("author"))));
					ps.setInfo("Title", (Strings.normalize_space(attributes
							.getValue("title"))));
					ps.setInfo("Subject", (Strings.normalize_space(attributes
							.getValue("subject"))));
					ps.setInfo("Keywords", (Strings.normalize_space(attributes
							.getValue("keywords"))));
					String string_17_ = attributes
							.getValue("initial-destination");
					if (string_17_ != null)
						ps.setInitialDestination(string_17_);
					break;
				}
				case 1: {
					float f = parseLength(attributes.getValue("height"));
					float f_18_ = parseLength(attributes.getValue("width"));
					ps.beginPage(f_18_, f, parseCount(attributes
							.getValue("page-number")), attributes
							.getValue("page-id"));
					if (needsAnnotation) {
						ps.addTextAnnotation(0.0F, f - Sticker.height,
								Sticker.width, f, Sticker
										.makeText(session.config.ANNOTATION),
								Sticker.title, Sticker.color);
						needsAnnotation = false;
					}
					break;
				}
				case 2:
					ps.rotate(parseAngle(attributes.getValue("phi")));
					break;
				case 3:
					ps.translate(parseLength(attributes.getValue("x")),
							parseLength(attributes.getValue("y")));
					break;
				case 4:
					ps.transform(parseFloat(attributes.getValue("a")),
							parseFloat(attributes.getValue("b")),
							parseFloat(attributes.getValue("c")),
							parseFloat(attributes.getValue("d")),
							parseFloat(attributes.getValue("e")),
							parseFloat(attributes.getValue("f")));
					break;
				case 6:
					ps
							.setWordSpacing(parseLength(attributes
									.getValue("value")));
					break;
				case 7:
					ps
							.setCharSpacing(parseLength(attributes
									.getValue("value")));
					break;
				case 8: {
					float f = parseFloat(attributes.getValue("value"));
					if (f <= 0.0F)
						session.error("negative font stretch is not allowed ("
								+ f + ")");
					else
						ps.setFontStretch(f);
					break;
				}
				case 9:
					ps.u28.selectFont(attributes.getValue("family"),
							parseInteger(attributes.getValue("weight")),
							attributes.getValue("style"), attributes
									.getValue("variant"));
					ps.setFontSize(parseLength(attributes.getValue("size")));
					break;
				case 10:
					ps.setTextPos(parseLength(attributes.getValue("x")),
							parseLength(attributes.getValue("y")));
					ps.u28.processText(attributes.getValue("value"));
					break;
				case 11: {
					ps.gsave();
					float f = parseLength(attributes.getValue("thickness"));
					if (!(f <= 0.0F)) {
						float f_19_ = parseLength(attributes.getValue("x-from"));
						float f_20_ = parseLength(attributes.getValue("y-from"));
						float f_21_ = parseLength(attributes.getValue("x-till"));
						float f_22_ = parseLength(attributes.getValue("y-till"));
						String string_23_ = attributes.getValue("style");
						if (string_23_.equals("dotted")) {
							float f_24_ = (getLineLength(f_19_, f_20_, f_21_,
									f_22_) / f);
							float f_25_ = (f_24_ - (float) (2.0 * (Math
									.floor((double) (f_24_ / 2.0F)))));
							float f_26_ = 2.0F - f_25_ / 2.0F;
							if (f_24_ > 2.0F && (double) f_26_ > 1.5)
								f_26_--;
							ps.setDash(0.0F, 2.0F * f, f_26_ * f);
							ps.setLineCap(1);
						} else if (string_23_.equals("dashed")) {
							float f_27_ = (getLineLength(f_19_, f_20_, f_21_,
									f_22_) / f);
							float f_28_ = (f_27_ + 4.0F - (float) (10.0 * (Math
									.floor((double) ((f_27_ + 4.0F) / 10.0F)))));
							float f_29_ = 10.0F - f_28_ / 2.0F;
							if (f_29_ < 8.0F || f_27_ < 6.0F)
								f_29_ -= 5.0F;
							if (f_29_ == 10.0F)
								f_29_ = 0.0F;
							ps.setDash(6.0F * f, 4.0F * f, f_29_ * f);
							ps.setLineCap(0);
						} else
							ps.setLineCap(0);
						ps.setLineWidth(f);
						ps.moveTo(f_19_, f_20_);
						ps.lineTo(f_21_, f_22_);
						ps.stroke();
						ps.grestore();
					}
					break;
				}
				case 15: {
					float f = parseFloat(attributes.getValue("scale-x"));
					float f_30_ = parseFloat(attributes.getValue("scale-y"));
					if (f != 0.0F && f_30_ != 0.0F) {
						String string_31_ = attributes.getValue("src");
						String string_32_ = attributes.getValue("base");
						String string_33_ = attributes.getValue("type");
						URLSpec urlspec = new URLSpec(
								(string_32_ == null ? null : new URLSpec(
										string_32_)), string_31_);
						ps.gsave();
						ps.translate(
								parseLength(attributes.getValue("x-from")),
								parseLength(attributes.getValue("y-from")));
						ps.scale(f, f_30_);
						ps.placeImage(urlspec, string_33_);
						ps.grestore();
					}
					break;
				}
				case 17:
					ps.setGrayColor(parseFloat(attributes.getValue("gray")));
					break;
				case 16:
					ps.setRGBColor(parseFloat(attributes.getValue("red")),
							parseFloat(attributes.getValue("green")),
							parseFloat(attributes.getValue("blue")));
					break;
				case 18:
					ps.setCMYKColor(parseFloat(attributes.getValue("cyan")),
							parseFloat(attributes.getValue("magenta")),
							parseFloat(attributes.getValue("yellow")),
							parseFloat(attributes.getValue("black")));
					break;
				case 19: {
					String string_34_ = attributes.getValue("colorant");
					float f = parseFloat(attributes.getValue("tint"));
					if (attributes.getValue("alt-gray") != null)
						ps.setSpotColor(f, string_34_,
								(new float[] { parseFloat(attributes
										.getValue("alt-gray")) }));
					else if (attributes.getValue("alt-red") != null)
						ps.setSpotColor(f, string_34_, (new float[] {
								parseFloat(attributes.getValue("alt-red")),
								parseFloat(attributes.getValue("alt-green")),
								parseFloat(attributes.getValue("alt-blue")) }));
					else if (attributes.getValue("alt-cyan") != null)
						ps
								.setSpotColor(f, string_34_, (new float[] {
										parseFloat(attributes
												.getValue("alt-cyan")),
										parseFloat(attributes
												.getValue("alt-magenta")),
										parseFloat(attributes
												.getValue("alt-yellow")),
										parseFloat(attributes
												.getValue("alt-black")) }));
					else
						ps.setSpotColor(f, string_34_, new float[] { 0.0F });
					break;
				}
				case 20:
					ps.setSpotColor(parseFloat(attributes.getValue("tint")),
							"All", new float[] { 0.0F });
					break;
				case 12: {
					float f = parseLength(attributes.getValue("x-from"));
					float f_35_ = parseLength(attributes.getValue("y-from"));
					float f_36_ = parseLength(attributes.getValue("x-till"))
							- f;
					float f_37_ = parseLength(attributes.getValue("y-till"))
							- f_35_;
					ps.rectangle(f, f_35_, f_36_, f_37_);
					ps.fill();
					break;
				}
				case 5: {
					float f = parseLength(attributes.getValue("x-from"));
					float f_38_ = parseLength(attributes.getValue("y-from"));
					float f_39_ = parseLength(attributes.getValue("x-till"))
							- f;
					float f_40_ = parseLength(attributes.getValue("y-till"))
							- f_38_;
					ps.gsave();
					ps.rectclip(f, f_38_, f_39_, f_40_);
					break;
				}
				case 13:
					ps.newpath();
					ps.moveTo(parseLength(attributes.getValue("x-from")),
							parseLength(attributes.getValue("y-from")));
					break;
				case 14:
					ps.lineTo(parseLength(attributes.getValue("x-till")),
							parseLength(attributes.getValue("y-till")));
					break;
				case 22:
					ps.addLocalLink(parseLength(attributes.getValue("x-from")),
							parseLength(attributes.getValue("y-from")),
							parseLength(attributes.getValue("x-till")),
							parseLength(attributes.getValue("y-till")),
							attributes.getValue("destination-id"));
					break;
				case 23:
					ps.addWebLink(parseLength(attributes.getValue("x-from")),
							parseLength(attributes.getValue("y-from")),
							parseLength(attributes.getValue("x-till")),
							parseLength(attributes.getValue("y-till")),
							attributes.getValue("destination"), ("new"
									.equals(attributes
											.getValue("show-destination"))));
					break;
				case 24:
					ps.addLocalBookmark(normalize_space(attributes
							.getValue("label")), parseInteger(attributes
							.getValue("id")), parseInteger(attributes
							.getValue("parent-id")), attributes
							.getValue("destination-id"), "false"
							.equals(attributes.getValue("collapse-subtree")));
					break;
				case 25:
					ps.addWebBookmark(normalize_space(attributes
							.getValue("label")), parseInteger(attributes
							.getValue("id")), parseInteger(attributes
							.getValue("parent-id")), attributes
							.getValue("destination"), "false".equals(attributes
							.getValue("collapse-subtree")), "new"
							.equals(attributes.getValue("show-destination")));
					break;
				case 26:
					break;
				case 21:
					ps.addNamedDestination(attributes.getValue("name"),
							parseLength(attributes.getValue("x")),
							parseLength(attributes.getValue("y")));
					break;
				default:
					throw new InternalException("unknown instruction '"
							+ string_15_ + "'");
				}
			} catch (RuntimeException runtimeexception) {
				throw runtimeexception;
			} catch (Exception exception) {
				throw new InternalException("fatal error at element: "
						+ string_15_ + " : " + exception);
			}
		}
	}

	public void endElement(String string, String string_41_, String string_42_)
			throws SAXException {
		if ("http://www.renderx.com/XEP/xep".equals(string)) {
			try {
				switch (((Short) H4base.idtab.get(string_41_)).shortValue()) {
				case 0:
					break;
				case 1:
					ps.endPage();
					break;
				case 2:
				case 3:
				case 4:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				case 20:
					break;
				case 5:
					ps.grestore();
					break;
				case 13:
					ps.closePath();
					ps.fill();
					break;
				case 14:
				case 21:
				case 22:
				case 23:
				case 24:
				case 25:
				case 26:
					break;
				default:
					throw new InternalException("unknown instruction '"
							+ string_41_ + "'");
				}
			} catch (RuntimeException runtimeexception) {
				throw runtimeexception;
			} catch (Exception exception) {
				throw new InternalException("fatal error at element: "
						+ string_41_ + " : " + exception);
			}
		}
	}

	private static final int parseInteger(String string) {
		return Integer.valueOf(string).intValue();
	}

	private static final float parseFloat(String string) {
		return Float.valueOf(string).floatValue();
	}

	private static final float parseAngle(String string) {
		return parseFloat(string);
	}

	private static final float parseLength(String string) {
		return parseFloat(string) / 1000.0F;
	}

	private static final int parseCount(String string) {
		return parseInteger(string);
	}

	private String normalize_space(String string) {
		if (string == null)
			return null;
		StringTokenizer stringtokenizer = new StringTokenizer(string);
		String string_43_ = "";
		while (stringtokenizer.hasMoreTokens())
			string_43_ += " " + stringtokenizer.nextToken();
		return string_43_.trim();
	}

	private static final float getLineLength(float f, float f_44_, float f_45_,
			float f_46_) {
		if (f == f_45_)
			return f_44_ > f_46_ ? f_44_ - f_46_ : f_46_ - f_44_;
		if (f_44_ == f_46_)
			return f > f_45_ ? f - f_45_ : f_45_ - f;
		float f_47_ = f - f_45_;
		float f_48_ = f_44_ - f_46_;
		return (float) Math.sqrt((double) (f_47_ * f_47_ + f_48_ * f_48_));
	}

	static {
		optab.put("LANGUAGE_LEVEL", new Short((short) 0));
		optab.put("UNICODE_ANNOTATIONS", new Short((short) 1));
		optab.put("DROP_UNUSED_DESTINATIONS", new Short((short) 2));
		optab.put("CLONE_EPS", new Short((short) 3));
		optab.put("PAGE_DEVICE", new Short((short) 4));
		optab.put("INITIAL_ZOOM", new Short((short) 5));
		optab.put("VIEW_MODE", new Short((short) 6));
		optab.put("CROP_OFFSET", new Short((short) 7));
		optab.put("CROP_MARK_WIDTH", new Short((short) 8));
		optab.put("BLEED", new Short((short) 9));
		optab.put("BLEED_MARK_WIDTH", new Short((short) 10));
		optab.put("PRINTER_MARK", new Short((short) 11));
	}
}