/* Sticker - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.gen.backends;

class Sticker
{
    static float width = 400.0F;
    static float height = 250.0F;
    static float[] color = { 1.0F, 1.0F, 0.5F };
    static String title = "About this document";
    
    static String makeText(String string) {
	string = replace(string, "$VERSION", "4.1 build 20050109");
	return string.replace('\n', '\r');
    }
    
    private static final String replace(String string, String string_0_,
					String string_1_) {
	for (int i = string.indexOf(string_0_); i != -1;
	     i = string.indexOf(string_0_, i + string_1_.length()))
	    string = (string.substring(0, i) + string_1_
		      + string.substring(i + string_0_.length()));
	return string;
    }
}
