/* H4XEP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.gen.backends;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.Properties;

import com.renderx.sax.Serializer;
import com.renderx.util.Base64OutputStream;
import com.renderx.util.Hashtable;
import com.renderx.util.URLSpec;
import com.renderx.xep.gen.H4base;
import com.renderx.xep.lib.Session;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

public class H4XEP extends H4base
{
    private static final String XEPURI = "http://www.renderx.com/XEP/xep";
    private Serializer serializer = null;
    private boolean EMBED_IMAGES;
    private final Hashtable imageIndex = new Hashtable();
    private int imageno = 0;
    private boolean newline = false;
    
    public static void init() {
	/* empty */
    }
    
    protected void resetDefaultOptions() {
	EMBED_IMAGES = false;
    }
    
    protected void setOption(String string, String string_0_) {
	if (!"EMBED_IMAGES".equals(string))
	    session.warning("Unrecognized XEP output option " + string
			    + ": setting ignored");
	else {
	    string_0_ = string_0_.trim().toLowerCase();
	    if ("true".equals(string_0_) || "yes".equals(string_0_))
		EMBED_IMAGES = true;
	    else if ("false".equals(string_0_) || "no".equals(string_0_))
		EMBED_IMAGES = false;
	    else
		session.warning("Invalid EMBED_IMAGES value: " + string_0_
				+ "; setting ignored");
	}
    }
    
    public H4XEP(OutputStream outputstream, Session session,
		 Properties properties) throws IOException {
	super(outputstream, session, properties);
    }
    
    public String outFormat() {
	return "xep";
    }
    
    protected String getPropertyPrefix() {
	return "com.renderx.xep.H4XEP.";
    }
    
    protected String getProcessingInstructionPrefix() {
	return "xep-out-";
    }
    
    public void processingInstruction(String string, String string_1_)
	throws SAXException {
	super.processingInstruction(string, string_1_);
	serializer.processingInstruction(string, string_1_);
	serializer.ignorableWhitespace("\n".toCharArray(), 0, 1);
    }
    
    public void startDocument() throws SAXException {
	super.startDocument();
	if (serializer == null) {
	    try {
		serializer = new Serializer(outstream);
	    } catch (IOException ioexception) {
		throw new SAXException(ioexception);
	    }
	}
	serializer.startDocument();
    }
    
    public void startElement(String string, String string_2_, String string_3_,
			     Attributes attributes) throws SAXException {
	Object object = null;
	if (!newline)
	    serializer.ignorableWhitespace("\n".toCharArray(), 0, 1);
	if ("http://www.renderx.com/XEP/xep".equals(string)
	    && string_2_.equals("image")) {
	    try {
		if (EMBED_IMAGES)
		    attributes = embedImage(attributes);
	    } catch (IOException ioexception) {
		session.exception("Error processing image", ioexception);
	    }
	}
	serializer.startElement("http://www.renderx.com/XEP/xep", string_2_,
				string_3_, attributes);
	newline = false;
    }
    
    public void characters(char[] cs, int i, int i_4_) throws SAXException {
	serializer.characters(cs, i, i_4_);
    }
    
    public void ignorableWhitespace(char[] cs, int i, int i_5_)
	throws SAXException {
	serializer.ignorableWhitespace(cs, i, i_5_);
    }
    
    public void endElement
	(String string, String string_6_, String string_7_)
	throws SAXException {
	serializer.endElement(string, string_6_, string_7_);
	serializer.ignorableWhitespace("\n".toCharArray(), 0, 1);
	newline = true;
    }
    
    public void endDocument() throws SAXException {
	serializer.endDocument();
	super.endDocument();
    }
    
    private Attributes embedImage(Attributes attributes) throws IOException {
	AttributesImpl attributesimpl = new AttributesImpl(attributes);
	int i = attributesimpl.getIndex("src");
	if (i == -1) {
	    session.error("No source specified for an image");
	    return attributes;
	}
	String string = attributesimpl.getValue(i);
	String string_8_ = (String) imageIndex.get(string);
	if (string_8_ != null) {
	    attributesimpl.setQName(i, "id-ref");
	    attributesimpl.setLocalName(i, "id-ref");
	    attributesimpl.setValue(i, string_8_);
	    return attributesimpl;
	}
	String string_9_ = attributesimpl.getValue("base");
	URLSpec urlspec = (string_9_ == null ? new URLSpec(string)
			   : new URLSpec(new URLSpec(string_9_), string));
	if (urlspec.scheme != 1) {
	    String string_10_ = attributesimpl.getValue("type");
	    if (string_10_ == null)
		string_10_ = "application/octet-stream";
	    InputStream inputstream
		= session.config.cache.openStream(urlspec, session);
	    StringWriter stringwriter = new StringWriter();
	    try {
		stringwriter.write("data:" + string_10_ + ";base64,");
		Base64OutputStream base64outputstream
		    = new Base64OutputStream(stringwriter);
		byte[] is = new byte[8192];
		boolean bool = false;
		int i_11_;
		while ((i_11_ = inputstream.read(is)) != -1)
		    base64outputstream.write(is, 0, i_11_);
		base64outputstream.close();
	    } finally {
		inputstream.close();
	    }
	    attributesimpl.setValue(i, stringwriter.toString());
	}
	String string_12_ = "image" + ++imageno;
	imageIndex.put(string, string_12_);
	attributesimpl.addAttribute(attributesimpl.getURI(i), "id", "id",
				    "CDATA", string_12_);
	return attributesimpl;
    }
}
