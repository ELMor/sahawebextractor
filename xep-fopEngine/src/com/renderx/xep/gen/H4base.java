/*
 * H4base - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.gen;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Properties;

import com.renderx.util.Hashtable;
import com.renderx.util.User;
import com.renderx.xep.lib.Session;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public abstract class H4base extends DefaultHandler {
	protected final OutputStream outstream;

	protected final Session session;

	protected static final Hashtable idtab = new Hashtable();

	protected static final short DOCUMENT = 0;

	protected static final short PAGE = 1;

	protected static final short ROTATE = 2;

	protected static final short TRANSLATE = 3;

	protected static final short TRANSFORM = 4;

	protected static final short CLIP = 5;

	protected static final short WORD_SPACING = 6;

	protected static final short LETTER_SPACING = 7;

	protected static final short FONT_STRETCH = 8;

	protected static final short FONT = 9;

	protected static final short TEXT = 10;

	protected static final short LINE = 11;

	protected static final short RECTANGLE = 12;

	protected static final short POLYGON = 13;

	protected static final short POINT = 14;

	protected static final short IMAGE = 15;

	protected static final short RGB_COLOR = 16;

	protected static final short GRAY_COLOR = 17;

	protected static final short CMYK_COLOR = 18;

	protected static final short SPOT_COLOR = 19;

	protected static final short REGISTRATION_COLOR = 20;

	protected static final short TARGET = 21;

	protected static final short INTERNAL_LINK = 22;

	protected static final short EXTERNAL_LINK = 23;

	protected static final short INTERNAL_BOOKMARK = 24;

	protected static final short EXTERNAL_BOOKMARK = 25;

	protected static final short PINPOINT = 26;

	public abstract String outFormat();

	protected abstract String getPropertyPrefix();

	protected abstract String getProcessingInstructionPrefix();

	protected abstract void resetDefaultOptions();

	protected abstract void setOption(String string, String string_0_);

	public H4base(OutputStream outputstream, Session session,
			Properties properties) throws IOException {
		outstream = outputstream;
		this.session = session;
		resetDefaultOptions();
		Properties properties_1_ = User.getProperties();
		String string = getPropertyPrefix();
		Enumeration enumeration = properties_1_.propertyNames();
		while (enumeration.hasMoreElements()) {
			String string_2_ = (String) enumeration.nextElement();
			if (string_2_.startsWith(string)) {
				String string_3_ = properties_1_.getProperty(string_2_);
				string_2_ = string_2_.substring(string.length());
				if (string_2_.indexOf('.') == -1)
					setOption(string_2_.replace('-', '_').toUpperCase(),
							string_3_);
			}
		}
		setOptions(properties);
	}

	protected void setOptions(Properties properties) {
		Enumeration enumeration = properties.propertyNames();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			setOption(string.replace('-', '_').toUpperCase(), properties
					.getProperty(string));
		}
	}

	public void processingInstruction(String string, String string_4_)
			throws SAXException {
		String string_5_ = getProcessingInstructionPrefix();
		if (string.startsWith(string_5_)) {
			String string_6_ = string.substring(string_5_.length());
			if (string_6_.indexOf('.') == -1)
				setOption(string_6_.replace('-', '_').toUpperCase(), string_4_);
		}
	}

	static {
		idtab.put("document", new Short((short) 0));
		idtab.put("page", new Short((short) 1));
		idtab.put("rotate", new Short((short) 2));
		idtab.put("translate", new Short((short) 3));
		idtab.put("transform", new Short((short) 4));
		idtab.put("clip", new Short((short) 5));
		idtab.put("word-spacing", new Short((short) 6));
		idtab.put("letter-spacing", new Short((short) 7));
		idtab.put("font-stretch", new Short((short) 8));
		idtab.put("font", new Short((short) 9));
		idtab.put("text", new Short((short) 10));
		idtab.put("line", new Short((short) 11));
		idtab.put("rectangle", new Short((short) 12));
		idtab.put("polygon", new Short((short) 13));
		idtab.put("point", new Short((short) 14));
		idtab.put("image", new Short((short) 15));
		idtab.put("rgb-color", new Short((short) 16));
		idtab.put("gray-color", new Short((short) 17));
		idtab.put("cmyk-color", new Short((short) 18));
		idtab.put("spot-color", new Short((short) 19));
		idtab.put("registration-color", new Short((short) 20));
		idtab.put("target", new Short((short) 21));
		idtab.put("internal-link", new Short((short) 22));
		idtab.put("external-link", new Short((short) 23));
		idtab.put("internal-bookmark", new Short((short) 24));
		idtab.put("external-bookmark", new Short((short) 25));
		idtab.put("pinpoint", new Short((short) 26));
	}
}