/*
 * SAX - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.gen;

import java.util.Enumeration;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.FontRecord;
import com.renderx.fonts.Metric;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.xep.cmp.Area;
import com.renderx.xep.cmp.FO;
import com.renderx.xep.cmp.Item;
import com.renderx.xep.cmp.Rectangle;
import com.renderx.xep.fmt.BPB;
import com.renderx.xep.fmt.BkMaker;
import com.renderx.xep.fmt.Cntl;
import com.renderx.xep.fmt.Instruction;
import com.renderx.xep.fmt.InstructionDispatcher;
import com.renderx.xep.fmt.Line;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.lib.Util;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.BorderComparator;
import com.renderx.xep.pre.PI;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

public class SAX implements InstructionDispatcher {
	private static final String uri = "http://www.renderx.com/XEP/xep";

	public static final String pfx = "xep";

	private ContentHandler handler = null;

	private final List book;

	private final FO fo;

	private final Session session;

	private AttributesImpl attrs = new AttributesImpl();

	private BkMaker.Page page;

	private int lwsp;

	private int lcsp;

	private int wsp;

	private int csp;

	private double stretch;

	private int x;

	private int y;

	private Item.TextTraits tt;

	private FontRecord te;

	private int font_size;

	private Attr.Color color;

	private Hashtable breg;

	private List anchors;

	private boolean idlePass = false;

	private Hashtable tableFrames;

	private Hashtable tableIdCounters;

	private static final short[][][] EDGES = {
			{ { 1, 0 }, { 1, 3 }, { 1, 2 }, { 1, 3 }, { 0, 2 }, { 0, 3 },
					{ 0, 0 }, { 0, 3 } },
			{ { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 1 }, { 1, 2 }, { 1, 1 },
					{ 1, 0 }, { 1, 1 } },
			{ { 1, 0 }, { 1, 1 }, { 1, 0 }, { 1, 3 }, { 0, 0 }, { 0, 3 },
					{ 0, 0 }, { 0, 1 } },
			{ { 0, 2 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 1, 2 }, { 1, 3 },
					{ 1, 2 }, { 1, 1 } } };

	private static final String[] EDGEIDSLR = { "before", "after", "start",
			"end" };

	private static final String[] EDGEIDSRL = { "before", "after", "end",
			"start" };

	private static final short NONE = -2;

	private static final short HIDDEN = -1;

	private static final short SOLID = 0;

	private static final short DOTTED = 1;

	private static final short DASHED = 2;

	private static final short DOUBLE = 3;

	private static final short RIDGE = 4;

	private static final short GROOVE = 5;

	private static final short OUTSET = 6;

	private static final short INSET = 7;

	private static final short MAIN = 0;

	private static final short LIGHTER = 1;

	private static final short DARKER = 2;

	private static final short TRANSPARENT = 3;

	private static final int[][] SOLIDEDGECOLORS = { { 0, 0, 0, 0 }, null,
			null, null, null, null, { 1, 2, 1, 2 }, { 2, 1, 2, 1 } };

	private static final int[][][] STRIPEDEDGECOLORS = { null, null, null,
			null, { { 2, 0, 1 }, { 2, 0, 1 }, { 2, 0, 1 }, { 2, 0, 1 } },
			{ { 1, 0, 2 }, { 1, 0, 2 }, { 1, 0, 2 }, { 1, 0, 2 } }, null, null };

	private static final Hashtable styles = new Hashtable();

	private class TableGrid {
		private static final int HORIZONTAL = 0;

		private static final int VERTICAL = 1;

		Vertex start = null;

		private class Chunk {
			Attr color;

			Attr style;

			int thickness;

			Attr precedence;

			Attr element_class;

			private Chunk() {
				/* empty */
			}

			public boolean sameLine(Chunk chunk_0_) {
				if (chunk_0_ == null)
					return false;
				return (color == chunk_0_.color && style == chunk_0_.style && thickness == chunk_0_.thickness);
			}

			public boolean isInvisible() {
				return (thickness == 0 || color == Attr.Color.transparent
						|| style == Attr.hidden || style == Attr.none);
			}

			public int compare(Chunk chunk_1_) {
				if (chunk_1_ == null)
					return 1;
				boolean bool = false;
				int i = BorderComparator.comparePrecedence(precedence,
						chunk_1_.precedence);
				if (i != 0)
					return i;
				if (style == Attr.hidden && chunk_1_.style != Attr.hidden)
					return 1;
				if (style != Attr.hidden && chunk_1_.style == Attr.hidden)
					return -1;
				if (style == Attr.none && chunk_1_.style != Attr.none)
					return -1;
				if (style != Attr.none && chunk_1_.style == Attr.none)
					return 1;
				if (thickness != chunk_1_.thickness)
					return thickness - chunk_1_.thickness;
				i = BorderComparator.compareStyle(style, chunk_1_.style);
				if (i != 0)
					return i;
				i = BorderComparator.compareElementClass(element_class,
						(chunk_1_.element_class));
				return i;
			}

			public void draw(Vertex vertex, Vertex vertex_2_) {
				if (!isInvisible()) {
					Attr.Color.Opaque opaque = (Attr.Color.Opaque) color;
					short i = ((Short) SAX.styles.get(style)).shortValue();
					switch (i) {
					case -2:
					case -1:
						break;
					case 1:
					case 2:
						SAX.this.drawLine(opaque, style.word(), thickness,
								vertex.x, vertex.y, vertex_2_.x, vertex_2_.y);
						break;
					case 0:
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
						if (vertex.y == vertex_2_.y)
							drawHorizontal(i, vertex, vertex_2_);
						else if (vertex.x == vertex_2_.x)
							drawVertical(i, vertex, vertex_2_);
						else
							throw new RuntimeException(
									"Line in the table grid neither vertical nor horizontal");
						break;
					default:
						throw new RuntimeException(
								"Internal error: invalid rule style index " + i);
					}
				}
			}

			private void drawHorizontal(int i, Vertex vertex, Vertex vertex_3_) {
				Attr.Color.Opaque opaque = (Attr.Color.Opaque) color;
				int i_4_ = vertex.y;
				int i_5_ = vertex.y + thickness / 2;
				int i_6_ = vertex.y - thickness / 2;
				int i_7_ = vertex.x;
				int i_8_ = vertex_3_.x;
				int i_9_ = vertex.x;
				int i_10_ = vertex_3_.x;
				int i_11_ = vertex.x;
				int i_12_ = vertex_3_.x;
				Chunk chunk_13_ = vertex.nextLine[1];
				Chunk chunk_14_ = vertex.prevLine[1];
				Chunk chunk_15_ = vertex.prevLine[0];
				Chunk chunk_16_ = vertex_3_.nextLine[1];
				Chunk chunk_17_ = vertex_3_.prevLine[1];
				Chunk chunk_18_ = vertex_3_.nextLine[0];
				if (chunk_13_ != null && chunk_13_.isInvisible())
					chunk_13_ = null;
				if (chunk_14_ != null && chunk_14_.isInvisible())
					chunk_14_ = null;
				if (chunk_15_ != null && chunk_15_.isInvisible())
					chunk_15_ = null;
				if (chunk_16_ != null && chunk_16_.isInvisible())
					chunk_16_ = null;
				if (chunk_17_ != null && chunk_17_.isInvisible())
					chunk_17_ = null;
				if (chunk_18_ != null && chunk_18_.isInvisible())
					chunk_18_ = null;
				if (chunk_13_ != null) {
					i_9_ += chunk_13_.thickness / 2;
					if (chunk_14_ == null && chunk_15_ == null)
						i_11_ -= chunk_13_.thickness / 2;
				}
				if (chunk_14_ != null) {
					i_11_ += chunk_14_.thickness / 2;
					if (chunk_13_ == null && chunk_15_ == null)
						i_9_ -= chunk_14_.thickness / 2;
				}
				if (chunk_16_ != null) {
					i_10_ -= chunk_16_.thickness / 2;
					if (chunk_17_ == null && chunk_18_ == null)
						i_12_ += chunk_16_.thickness / 2;
				}
				if (chunk_17_ != null) {
					i_12_ -= chunk_17_.thickness / 2;
					if (chunk_16_ == null && chunk_18_ == null)
						i_10_ += chunk_17_.thickness / 2;
				}
				switch (i) {
				case 0:
				case 6:
				case 7:
					SAX.this.drawHexagon(opaque, i_7_, i_4_, i_9_, i_5_, i_10_,
							i_5_, i_8_, i_4_, i_12_, i_6_, i_11_, i_6_);
					break;
				case 3:
					SAX.this.drawTetragon(opaque, i_9_, i_5_, i_10_, i_5_,
							(2 * i_8_ + i_10_) / 3, (2 * i_4_ + i_5_) / 3,
							(2 * i_7_ + i_9_) / 3, (2 * i_4_ + i_5_) / 3);
					SAX.this.drawTetragon(opaque, i_11_, i_6_, i_12_, i_6_,
							(2 * i_8_ + i_12_) / 3, (2 * i_4_ + i_6_) / 3,
							(2 * i_7_ + i_11_) / 3, (2 * i_4_ + i_6_) / 3);
					break;
				case 5:
					SAX.this.drawTetragon(opaque.darkerColor(), i_9_, i_5_,
							i_10_, i_5_, (i_8_ + i_10_) / 2, (i_4_ + i_5_) / 2,
							(i_7_ + i_9_) / 2, (i_4_ + i_5_) / 2);
					SAX.this.drawHexagon(opaque, i_7_, i_4_, (i_7_ + i_9_) / 2,
							(i_4_ + i_5_) / 2, (i_8_ + i_10_) / 2,
							(i_4_ + i_5_) / 2, i_8_, i_4_, (i_8_ + i_12_) / 2,
							(i_4_ + i_6_) / 2, (i_7_ + i_11_) / 2,
							(i_4_ + i_6_) / 2);
					SAX.this.drawTetragon(opaque.lighterColor(), i_11_, i_6_,
							i_12_, i_6_, (i_8_ + i_12_) / 2, (i_4_ + i_6_) / 2,
							(i_7_ + i_11_) / 2, (i_4_ + i_6_) / 2);
					break;
				case 4:
					SAX.this.drawTetragon(opaque.lighterColor(), i_9_, i_5_,
							i_10_, i_5_, (i_8_ + i_10_) / 2, (i_4_ + i_5_) / 2,
							(i_7_ + i_9_) / 2, (i_4_ + i_5_) / 2);
					SAX.this.drawHexagon(opaque, i_7_, i_4_, (i_7_ + i_9_) / 2,
							(i_4_ + i_5_) / 2, (i_8_ + i_10_) / 2,
							(i_4_ + i_5_) / 2, i_8_, i_4_, (i_8_ + i_12_) / 2,
							(i_4_ + i_6_) / 2, (i_7_ + i_11_) / 2,
							(i_4_ + i_6_) / 2);
					SAX.this.drawTetragon(opaque.darkerColor(), i_11_, i_6_,
							i_12_, i_6_, (i_8_ + i_12_) / 2, (i_4_ + i_6_) / 2,
							(i_7_ + i_11_) / 2, (i_4_ + i_6_) / 2);
					break;
				}
			}

			private void drawVertical(int i, Vertex vertex, Vertex vertex_19_) {
				Attr.Color.Opaque opaque = (Attr.Color.Opaque) color;
				int i_20_ = vertex.x;
				int i_21_ = vertex.x + thickness / 2;
				int i_22_ = vertex.x - thickness / 2;
				int i_23_ = vertex.y;
				int i_24_ = vertex_19_.y;
				int i_25_ = vertex.y;
				int i_26_ = vertex_19_.y;
				int i_27_ = vertex.y;
				int i_28_ = vertex_19_.y;
				Chunk chunk_29_ = vertex.nextLine[0];
				Chunk chunk_30_ = vertex.prevLine[0];
				Chunk chunk_31_ = vertex.prevLine[1];
				Chunk chunk_32_ = vertex_19_.nextLine[0];
				Chunk chunk_33_ = vertex_19_.prevLine[0];
				Chunk chunk_34_ = vertex_19_.nextLine[1];
				if (chunk_29_ != null && chunk_29_.isInvisible())
					chunk_29_ = null;
				if (chunk_30_ != null && chunk_30_.isInvisible())
					chunk_30_ = null;
				if (chunk_31_ != null && chunk_31_.isInvisible())
					chunk_31_ = null;
				if (chunk_32_ != null && chunk_32_.isInvisible())
					chunk_32_ = null;
				if (chunk_33_ != null && chunk_33_.isInvisible())
					chunk_33_ = null;
				if (chunk_34_ != null && chunk_34_.isInvisible())
					chunk_34_ = null;
				if (chunk_29_ != null) {
					i_25_ += chunk_29_.thickness / 2;
					if (chunk_30_ == null && chunk_31_ == null)
						i_27_ -= chunk_29_.thickness / 2;
				}
				if (chunk_30_ != null) {
					i_27_ += chunk_30_.thickness / 2;
					if (chunk_29_ == null && chunk_31_ == null)
						i_25_ -= chunk_30_.thickness / 2;
				}
				if (chunk_32_ != null) {
					i_26_ -= chunk_32_.thickness / 2;
					if (chunk_33_ == null && chunk_34_ == null)
						i_28_ += chunk_32_.thickness / 2;
				}
				if (chunk_33_ != null) {
					i_28_ -= chunk_33_.thickness / 2;
					if (chunk_32_ == null && chunk_34_ == null)
						i_26_ += chunk_33_.thickness / 2;
				}
				switch (i) {
				case 0:
				case 6:
				case 7:
					SAX.this.drawHexagon(opaque, i_20_, i_23_, i_21_, i_25_,
							i_21_, i_26_, i_20_, i_24_, i_22_, i_28_, i_22_,
							i_27_);
					break;
				case 3:
					SAX.this.drawTetragon(opaque, i_21_, i_25_, i_21_, i_26_,
							(2 * i_20_ + i_21_) / 3, (2 * i_24_ + i_26_) / 3,
							(2 * i_20_ + i_21_) / 3, (2 * i_23_ + i_25_) / 3);
					SAX.this.drawTetragon(opaque, i_22_, i_27_, i_22_, i_28_,
							(2 * i_20_ + i_22_) / 3, (2 * i_24_ + i_28_) / 3,
							(2 * i_20_ + i_22_) / 3, (2 * i_23_ + i_27_) / 3);
					break;
				case 5:
					SAX.this.drawTetragon(opaque.lighterColor(), i_21_, i_25_,
							i_21_, i_26_, (i_20_ + i_21_) / 2,
							(i_24_ + i_26_) / 2, (i_20_ + i_21_) / 2,
							(i_23_ + i_25_) / 2);
					SAX.this.drawHexagon(opaque, i_20_, i_23_,
							(i_20_ + i_21_) / 2, (i_23_ + i_25_) / 2,
							(i_20_ + i_21_) / 2, (i_24_ + i_26_) / 2, i_20_,
							i_24_, (i_20_ + i_22_) / 2, (i_24_ + i_28_) / 2,
							(i_20_ + i_22_) / 2, (i_23_ + i_27_) / 2);
					SAX.this.drawTetragon(opaque.darkerColor(), i_22_, i_27_,
							i_22_, i_28_, (i_20_ + i_22_) / 2,
							(i_24_ + i_28_) / 2, (i_20_ + i_22_) / 2,
							(i_23_ + i_27_) / 2);
					break;
				case 4:
					SAX.this.drawTetragon(opaque.darkerColor(), i_21_, i_25_,
							i_21_, i_26_, (i_20_ + i_21_) / 2,
							(i_24_ + i_26_) / 2, (i_20_ + i_21_) / 2,
							(i_23_ + i_25_) / 2);
					SAX.this.drawHexagon(opaque, i_20_, i_23_,
							(i_20_ + i_21_) / 2, (i_23_ + i_25_) / 2,
							(i_20_ + i_21_) / 2, (i_24_ + i_26_) / 2, i_20_,
							i_24_, (i_20_ + i_22_) / 2, (i_24_ + i_28_) / 2,
							(i_20_ + i_22_) / 2, (i_23_ + i_27_) / 2);
					SAX.this.drawTetragon(opaque.lighterColor(), i_22_, i_27_,
							i_22_, i_28_, (i_20_ + i_22_) / 2,
							(i_24_ + i_28_) / 2, (i_20_ + i_22_) / 2,
							(i_23_ + i_27_) / 2);
					break;
				}
			}
		}

		private class Vertex {
			public final int x;

			public final int y;

			public final Chunk[] prevLine = { null, null };

			public final Chunk[] nextLine = { null, null };

			public final Vertex[] prev = { null, null };

			public final Vertex[] next = { null, null };

			public Vertex(int i, int i_35_) {
				x = i;
				y = i_35_;
			}

			public Vertex(int i, int i_36_, Vertex vertex_37_,
					Vertex vertex_38_, Vertex vertex_39_, Vertex vertex_40_) {
				this(i, i_36_);
				linkThrough(vertex_37_, vertex_38_, 0);
				linkThrough(vertex_39_, vertex_40_, 1);
			}

			private void linkThrough(Vertex vertex_41_, Vertex vertex_42_, int i) {
				next[i] = vertex_42_;
				if (vertex_42_ != null) {
					vertex_42_.prev[i] = this;
					nextLine[i] = vertex_42_.prevLine[i];
				}
				prev[i] = vertex_41_;
				if (vertex_41_ != null) {
					vertex_41_.next[i] = this;
					prevLine[i] = vertex_41_.nextLine[i];
				}
			}
		}

		private TableGrid() {
			/* empty */
		}

		public void add(BPB bpb) {
			Attr attr = bpb.get(Attn.$table_element_class);
			Area area = SAX.this.makeArea(bpb);
			for (int i = 0; i != SAX.EDGES.length; i++) {
				String string = (bpb.writing_mode == Attr.rl_tb ? SAX.EDGEIDSRL[i]
						: SAX.EDGEIDSLR[i]);
				Chunk chunk = new Chunk();
				chunk.element_class = attr;
				chunk.thickness = bpb.get(
						Attn.oldName("border-" + string + "-width.collapsed"))
						.length();
				chunk.color = ((Attr.Color) bpb.get(Attn.oldName("border-"
						+ string + "-color")));
				if (!(chunk.color instanceof Attr.Color)) {
					session.error("invalid border-" + string + "-color: "
							+ chunk.color);
					chunk.color = Attr.Color.transparent;
				}
				chunk.style = bpb.get(Attn.oldName("border-" + string
						+ "-style"));
				Short var_short = (Short) SAX.styles.get(chunk.style);
				if (var_short == null) {
					session.error("invalid border-" + string + "-style: "
							+ chunk.style);
					chunk.style = Attr.Word.create("solid");
				} else if (var_short.shortValue() == -2
						|| var_short.shortValue() == -1)
					chunk.thickness = 0;
				chunk.precedence = bpb.get(Attn.oldName("border-" + string
						+ "-precedence"));
				short[][] is = SAX.EDGES[i];
				Object object = null;
				Object object_43_ = null;
				Vertex vertex;
				Vertex vertex_44_;
				if (attr == Attr.Word.create("table")) {
					vertex = getVertex(area.r[1][is[0][1]], area.r[1][is[1][1]]);
					vertex_44_ = getVertex(area.r[1][is[2][1]],
							area.r[1][is[3][1]]);
				} else {
					vertex = getVertex(area.r[0][is[0][1]], area.r[0][is[1][1]]);
					vertex_44_ = getVertex(area.r[0][is[2][1]],
							area.r[0][is[3][1]]);
				}
				if (chunk.color != Attr.Color.transparent
						&& (chunk.style == Attr.Word.create("inset") || chunk.style == Attr.Word
								.create("outset"))) {
					switch (SAX.SOLIDEDGECOLORS[var_short.shortValue()][i]) {
					case 3:
						chunk.color = Attr.Color.transparent;
					/* fall through */
					case 2:
						chunk.color = ((Attr.Color.Opaque) chunk.color)
								.darkerColor();
						break;
					case 1:
						chunk.color = ((Attr.Color.Opaque) chunk.color)
								.lighterColor();
						break;
					}
				}
				if (vertex.x == vertex_44_.x)
					tracePath(vertex, vertex_44_, chunk, 1);
				else if (vertex.y == vertex_44_.y)
					tracePath(vertex, vertex_44_, chunk, 0);
				else
					throw new RuntimeException(
							"Table border is neither horizontal nor vertical");
			}
		}

		Vertex getVertex(int i, int i_45_) {
			if (start == null) {
				start = new Vertex(i, i_45_);
				return start;
			}
			Object object = null;
			for (Vertex vertex = start; vertex != null; vertex = vertex.next[0]) {
				if (Util.eq0(i - vertex.x))
					break;
				if (vertex.x > i) {
					Vertex vertex_46_ = null;
					for (Vertex vertex_47_ = vertex; vertex_47_ != null; vertex_47_ = vertex_47_.next[1])
						vertex_46_ = new Vertex(i, vertex_47_.y,
								vertex_47_.prev[0], vertex_47_, vertex_46_,
								null);
					if (start.prev[0] != null)
						start = start.prev[0];
					break;
				}
				if (vertex.next[0] == null) {
					Vertex vertex_48_ = null;
					for (Vertex vertex_49_ = vertex; vertex_49_ != null; vertex_49_ = vertex_49_.next[1])
						vertex_48_ = new Vertex(i, vertex_49_.y, vertex_49_,
								null, vertex_48_, null);
					break;
				}
			}
			for (Vertex vertex = start; vertex != null; vertex = vertex.next[1]) {
				if (Util.eq0(i_45_ - vertex.y))
					break;
				if (vertex.y > i_45_) {
					Vertex vertex_50_ = null;
					for (Vertex vertex_51_ = vertex; vertex_51_ != null; vertex_51_ = vertex_51_.next[0])
						vertex_50_ = new Vertex(vertex_51_.x, i_45_,
								vertex_50_, null, vertex_51_.prev[1],
								vertex_51_);
					if (start.prev[1] != null)
						start = start.prev[1];
					break;
				}
				if (vertex.next[1] == null) {
					Vertex vertex_52_ = null;
					for (Vertex vertex_53_ = vertex; vertex_53_ != null; vertex_53_ = vertex_53_.next[0])
						vertex_52_ = new Vertex(vertex_53_.x, i_45_,
								vertex_52_, null, vertex_53_, null);
					break;
				}
			}
			Vertex vertex;
			for (vertex = start; Util.ne0(i - vertex.x); vertex = vertex.next[0]) {
				/* empty */
			}
			for (/**/; Util.ne0(i_45_ - vertex.y); vertex = vertex.next[1]) {
				/* empty */
			}
			return vertex;
		}

		void tracePath(Vertex vertex, Vertex vertex_54_, Chunk chunk, int i) {
			if (vertex.x > vertex_54_.x || vertex.y > vertex_54_.y)
				tracePath(vertex_54_, vertex, chunk, i);
			else {
				for (Vertex vertex_55_ = vertex; (vertex_55_.x != vertex_54_.x || vertex_55_.y != vertex_54_.y); vertex_55_ = vertex_55_.next[i]) {
					Vertex vertex_56_ = vertex_55_.next[i];
					if (vertex_56_ == null)
						throw new RuntimeException(
								"Bad grid structure: cannot trace from ["
										+ vertex.x + "," + vertex.y + "] to ["
										+ vertex_54_.x + "," + vertex_54_.y
										+ "] along axis #" + i);
					if (chunk.compare(vertex_55_.nextLine[i]) >= 0)
						vertex_55_.nextLine[i] = vertex_56_.prevLine[i] = chunk;
				}
			}
		}

		public void draw() {
			draw(1, 0);
			draw(0, 1);
		}

		private void draw(int i, int i_57_) {
			for (Vertex vertex = start; vertex != null; vertex = vertex.next[i]) {
				Vertex vertex_58_ = vertex;
				while (vertex_58_ != null) {
					Chunk chunk = vertex_58_.nextLine[i_57_];
					if (chunk == null)
						vertex_58_ = vertex_58_.next[i_57_];
					else {
						Vertex vertex_59_;
						for (vertex_59_ = vertex_58_.next[i_57_]; (chunk
								.sameLine(vertex_59_.nextLine[i_57_])
								&& (vertex_59_.nextLine[i] == null || vertex_59_.nextLine[i]
										.isInvisible()) && (vertex_59_.prevLine[i] == null || vertex_59_.prevLine[i]
								.isInvisible())); vertex_59_ = vertex_59_.next[i_57_]) {
							/* empty */
						}
						chunk.draw(vertex_58_, vertex_59_);
						vertex_58_ = vertex_59_;
					}
				}
			}
		}
	}

	private class TableData {
		private final boolean separateBorders;

		private final List areas = new List();

		TableData(boolean bool) {
			separateBorders = bool;
		}

		void add(BPB bpb) {
			areas.snoc(bpb);
		}

		void draw() {
			Enumeration enumeration = areas.elements();
			while (enumeration.hasMoreElements())
				processBackground((BPB) enumeration.nextElement());
			if (separateBorders) {
				enumeration = areas.elements();
				while (enumeration.hasMoreElements())
					processBorder((BPB) enumeration.nextElement());
			} else {
				TableGrid tablegrid = new TableGrid();
				enumeration = areas.elements();
				while (enumeration.hasMoreElements())
					tablegrid.add((BPB) enumeration.nextElement());
				tablegrid.draw();
			}
		}
	}

	public SAX(List list, FO fo, Session session) {
		book = list;
		this.fo = fo;
		this.session = session;
	}

	public void setContentHandler(ContentHandler contenthandler) {
		handler = contenthandler;
	}

	private void addAttribute(String string, String string_60_,
			String string_61_) {
		attrs.addAttribute("", string, string, string_60_, string_61_);
	}

	private void startElement(String string, Attributes attributes)
			throws SAXException {
		handler.startElement("http://www.renderx.com/XEP/xep", string, "xep:"
				+ string, attributes);
	}

	private void endElement(String string) throws SAXException {
		handler.endElement("http://www.renderx.com/XEP/xep", string, "xep:"
				+ string);
	}

	public void traverse() {
		try {
			handler.startDocument();
			Enumeration enumeration = fo.pilist.elements();
			while (enumeration.hasMoreElements()) {
				PI pi = (PI) enumeration.nextElement();
				handler.processingInstruction(pi.target, pi.data);
			}
			handler.startPrefixMapping("xep", "http://www.renderx.com/XEP/xep");
			attrs.clear();
			addAttribute("creator", "CDATA", session.config.APPLICATION_ID);
			Enumeration enumeration_62_ = fo.metainfo.keys();
			while (enumeration_62_.hasMoreElements()) {
				String string = (String) enumeration_62_.nextElement();
				addAttribute(string, "CDATA", (String) fo.metainfo.get(string));
			}
			if (fo.initial_destination != null)
				addAttribute("initial-destination", "CDATA",
						fo.initial_destination.word());
			startElement("document", attrs);
		} catch (SAXException saxexception) {
			throw new InternalException(saxexception.getMessage());
		}
		Enumeration enumeration = fo.outline.elements();
		while (enumeration.hasMoreElements())
			((Instruction) enumeration.nextElement()).pass(this);
		int i = 0;
		Enumeration enumeration_63_ = book.elements();
		while (enumeration_63_.hasMoreElements()) {
			page = (BkMaker.Page) enumeration_63_.nextElement();
			session.event("page-number", page.id);
			wsp = csp = -2147483648;
			stretch = Double.MIN_VALUE;
			tt = null;
			te = null;
			font_size = -1;
			color = null;
			try {
				attrs.clear();
				addAttribute("width", "CDATA", Integer
						.toString(page.master.width));
				addAttribute("height", "CDATA", Integer
						.toString(page.master.height));
				addAttribute("page-number", "CDATA", Integer.toString(page.no));
				addAttribute("page-id", "CDATA", page.id);
				startElement("page", attrs);
				Enumeration enumeration_64_ = page.master.pilist.elements();
				while (enumeration_64_.hasMoreElements()) {
					PI pi = (PI) enumeration_64_.nextElement();
					handler.processingInstruction(pi.target, pi.data);
				}
			} catch (SAXException saxexception) {
				throw new InternalException(saxexception.getMessage());
			}
			anchors = new List();
			if (page.master.phi != 0) {
				attrs.clear();
				addAttribute("phi", "CDATA", Integer.toString(page.master.phi));
				try {
					startElement("rotate", attrs);
					endElement("rotate");
				} catch (SAXException saxexception) {
					throw new InternalException(saxexception.getMessage());
				}
			}
			tableFrames = new Hashtable();
			idlePass = true;
			Enumeration enumeration_65_ = page.strmtab.elements();
			while (enumeration_65_.hasMoreElements()) {
				breg = new Hashtable();
				tableIdCounters = new Hashtable();
				Enumeration enumeration_66_ = ((List) enumeration_65_
						.nextElement()).elements();
				while (enumeration_66_.hasMoreElements())
					((Instruction) enumeration_66_.nextElement()).pass(this);
				breg = null;
				tableIdCounters = null;
			}
			idlePass = false;
			enumeration_65_ = page.strmtab.elements();
			while (enumeration_65_.hasMoreElements()) {
				breg = new Hashtable();
				tableIdCounters = new Hashtable();
				Enumeration enumeration_67_ = ((List) enumeration_65_
						.nextElement()).elements();
				while (enumeration_67_.hasMoreElements())
					((Instruction) enumeration_67_.nextElement()).pass(this);
				breg = null;
				tableIdCounters = null;
			}
			tableFrames = null;
			if (page.master.phi != 0) {
				attrs.clear();
				addAttribute("phi", "CDATA", Integer
						.toString(360 - page.master.phi));
				try {
					startElement("rotate", attrs);
					endElement("rotate");
				} catch (SAXException saxexception) {
					throw new InternalException(saxexception.getMessage());
				}
			}
			Enumeration enumeration_68_ = anchors.elements();
			while (enumeration_68_.hasMoreElements())
				process((BPB.Anchor) enumeration_68_.nextElement());

			try {
				attrs.clear();
				endElement("page");
			} catch (SAXException saxexception) {
				throw new InternalException(saxexception.getMessage());
			}
			page.strmtab = null;
		}
		try {
			endElement("document");
			handler.endPrefixMapping("xep");
			handler.endDocument();
		} catch (SAXException saxexception) {
			throw new InternalException(saxexception.getMessage());
		}
	}

	public void process(BPB bpb) {
		if (bpb.draw && !breg.containsKey(bpb)) {
			breg.put(bpb, bpb);
			if (idlePass) {
				if (bpb.containsKey(Attn.$table_identifier))
					addTablePart(bpb);
			} else {
				if (bpb.containsKey(Attn.$table_identifier))
					processTablePart(bpb);
				else {
					processBackground(bpb);
					processBorder(bpb);
				}
				if (bpb instanceof BPB.Block)
					processRuler((BPB.Block) bpb);
				if (bpb instanceof BPB.Anchor)
					anchors.snoc(bpb);
			}
		}
	}

	public void addTablePart(BPB bpb) {
		Attr attr = bpb.get(Attn.$table_identifier);
		Object object = null;
		TableData tabledata;
		if (bpb.get(Attn.$table_element_class) == Attr.Word.create("table")
				|| ((bpb.get(Attn.$table_element_class) == Attr.Word
						.create("header")) && tableIdCounters.get(attr) == null)) {
			Integer integer = (Integer) tableIdCounters.get(attr);
			Integer integer_69_ = (integer == null ? new Integer(1)
					: new Integer(1 + integer.intValue()));
			tableIdCounters.put(attr, integer_69_);
			tabledata = new TableData(
					bpb.get(Attn.$border_collapse) == Attr.Word
							.create("separate"));
			String string = "" + integer_69_ + ":" + attr;
			tableFrames.put(string, tabledata);
		} else {
			Integer integer = (Integer) tableIdCounters.get(attr);
			String string = "" + integer + ":" + attr;
			tabledata = (TableData) tableFrames.get(string);
		}
		tabledata.add(bpb);
	}

	public void processTablePart(BPB bpb) {
		Attr attr = bpb.get(Attn.$table_identifier);
		if (bpb.get(Attn.$table_element_class) == Attr.Word.create("table")
				|| ((bpb.get(Attn.$table_element_class) == Attr.Word
						.create("header")) && tableIdCounters.get(attr) == null)) {
			Integer integer = (Integer) tableIdCounters.get(attr);
			Integer integer_70_ = (integer == null ? new Integer(1)
					: new Integer(1 + integer.intValue()));
			tableIdCounters.put(attr, integer_70_);
			String string = "" + integer_70_ + ":" + attr;
			TableData tabledata = (TableData) tableFrames.get(string);
			tabledata.draw();
		}
	}

	public void processBackground(BPB bpb) {
		Area area = makeArea(bpb);
		Attr.Color color = (Attr.Color) bpb.get(Attn.$background_color);
		if (color != Attr.Color.transparent) {
			int i = bpb.get(Attn.$rule_thickness).length();
			processColor((Attr.Color.Opaque) color);
			try {
				this.attrs.clear();
				addAttribute("x-from", "CDATA", Integer.toString(area.r[1][0]));
				addAttribute("y-from", "CDATA", Integer.toString(area.r[1][1]));
				addAttribute("x-till", "CDATA", Integer.toString(area.r[1][2]));
				addAttribute("y-till", "CDATA", Integer.toString(area.r[1][3]));
				startElement("rectangle", this.attrs);
				endElement("rectangle");
			} catch (SAXException saxexception) {
				throw new InternalException(saxexception.getMessage());
			}
		}
		if (bpb.im != null) {
			int[] is = area.r[1];
			int i = bpb.get(Attn.$reference_orientation).count();
			if (i != 0) {
				process(new Cntl.Rotate(i));
				is = Rectangle.rotate(is, i);
			}
			this.attrs.clear();
			addAttribute("x-from", "CDATA", Integer.toString(is[0]));
			addAttribute("y-from", "CDATA", Integer.toString(is[1]));
			addAttribute("x-till", "CDATA", Integer.toString(is[2]));
			addAttribute("y-till", "CDATA", Integer.toString(is[3]));
			double[] ds = { 1.0, 1.0 };
			Attr attr = bpb.get(Attn.$background_content_width);
			Attr attr_71_ = bpb.get(Attn.$background_content_height);
			if (attr instanceof Attr.Length)
				ds[0] = (double) attr.length() / (bpb.im.width * 1000.0);
			else if (attr instanceof Attr.Ratio)
				ds[0] = attr.ratio();
			if (attr_71_ instanceof Attr.Length)
				ds[1] = (double) attr_71_.length() / (bpb.im.height * 1000.0);
			if (attr_71_ instanceof Attr.Ratio)
				ds[1] = attr_71_.ratio();
			if (bpb.get(Attn.$background_scaling) == Attr.newWord("uniform")) {
				if (ds[0] != 1.0)
					ds[1] = ds[0];
				else if (ds[1] != 1.0)
					ds[0] = ds[1];
			}
			Attr attr_72_ = bpb.get(Attn.$background_repeat);
			boolean bool = false;
			int i_73_ = 1;
			int i_74_ = 1;
			int i_75_ = is[2] - is[0];
			int i_76_ = is[3] - is[1];
			int i_77_ = (int) Math.round(bpb.im.width * 1000.0 * ds[0]);
			int i_78_ = (int) Math.round(bpb.im.height * 1000.0 * ds[1]);
			int i_79_ = is[0];
			int i_80_ = is[3] - i_78_;
			if (i_75_ < 0) {
				i_75_ = -i_75_;
				i_79_ = is[2];
			}
			if (i_76_ < 0) {
				i_76_ = -i_76_;
				i_80_ = is[3];
			}
			int i_81_;
			if (attr_72_ == Attr.newWord("repeat"))
				i_81_ = 3;
			else if (attr_72_ == Attr.newWord("repeat-x"))
				i_81_ = 1;
			else if (attr_72_ == Attr.newWord("repeat-y"))
				i_81_ = 2;
			else if (attr_72_ == Attr.newWord("no-repeat"))
				i_81_ = 0;
			else
				throw new InternalException("illegal background-repeat: "
						+ attr_72_);
			Attr[] attrs = new Attr[2];
			attrs[0] = bpb.get(Attn.$background_position_horizontal);
			attrs[1] = bpb.get(Attn.$background_position_vertical);
			if ((i_81_ & 0x1) != 0)
				i_73_ = (int) Math.round((double) i_75_ / (double) i_77_ + 0.5);
			else
				i_79_ += attrs[0].length_or_ratio(i_75_ - i_77_);
			if ((i_81_ & 0x2) != 0)
				i_74_ = (int) Math.round((double) i_76_ / (double) i_78_ + 0.5);
			else
				i_80_ -= attrs[1].length_or_ratio(i_76_ - i_78_);
			try {
				startElement("clip", this.attrs);
			} catch (SAXException saxexception) {
				throw new InternalException(saxexception.getMessage());
			}
			int i_82_ = i_79_;
			int i_83_ = i_80_;
			for (int i_84_ = 0; i_84_ != i_73_; i_84_++) {
				for (int i_85_ = 0; i_85_ != i_74_; i_85_++) {
					this.attrs.clear();
					addAttribute("src", "CDATA", bpb.im.source.toString());
					if (bpb.im.source.scheme != 3)
						addAttribute("base", "CDATA", bpb.im.source.getBase()
								.toString());
					addAttribute("type", "CDATA", bpb.im.mimetype);
					addAttribute("x-from", "CDATA", Integer.toString(i_79_));
					addAttribute("y-from", "CDATA", Integer.toString(i_80_));
					addAttribute("scale-x", "CDATA", Double.toString(ds[0]));
					addAttribute("scale-y", "CDATA", Double.toString(ds[1]));
					addAttribute("width", "CDATA", Integer.toString((int) Math
							.round(bpb.im.width * 1000.0 * ds[0])));
					addAttribute("height", "CDATA", Integer.toString((int) Math
							.round((bpb.im.height) * 1000.0 * ds[1])));
					try {
						startElement("image", this.attrs);
						endElement("image");
					} catch (SAXException saxexception) {
						throw new InternalException(saxexception.getMessage());
					}
					i_80_ -= i_78_;
				}
				i_79_ += i_77_;
				i_80_ = i_83_;
			}
			try {
				endElement("clip");
			} catch (SAXException saxexception) {
				throw new InternalException(saxexception.getMessage());
			}
			if (i != 0)
				process(new Cntl.Rotate(360 - i));
		}
	}

	public void processBorder(BPB bpb) {
		Area area = makeArea(bpb);
		for (int i = 0; i != EDGES.length; i++) {
			String string = bpb.writing_mode == Attr.rl_tb ? EDGEIDSRL[i]
					: EDGEIDSLR[i];
			int i_86_ = bpb.get(
					Attn.oldName("border-" + string + "-width.length"))
					.length();
			if (!Util.eq0(i_86_)) {
				Attr attr = bpb
						.get(Attn.oldName("border-" + string + "-color"));
				if (attr != Attr.Color.transparent) {
					if (!(attr instanceof Attr.Color.Opaque))
						session.error("invalid border-" + string + "-color: "
								+ attr);
					else {
						Attr.Color.Opaque opaque = (Attr.Color.Opaque) attr;
						Attr attr_87_ = bpb.get(Attn.oldName("border-" + string
								+ "-style"));
						Short var_short = (Short) styles.get(attr_87_);
						short i_88_ = 0;
						if (var_short == null) {
							session.error("invalid border-" + string
									+ "-style: " + attr_87_);
							attr_87_ = Attr.newWord("solid");
						} else
							i_88_ = var_short.shortValue();
						short[][] is = EDGES[i];
						int i_89_ = area.r[is[0][0]][is[0][1]];
						int i_90_ = area.r[is[1][0]][is[1][1]];
						int i_91_ = area.r[is[2][0]][is[2][1]];
						int i_92_ = area.r[is[3][0]][is[3][1]];
						int i_93_ = area.r[is[4][0]][is[4][1]];
						int i_94_ = area.r[is[5][0]][is[5][1]];
						int i_95_ = area.r[is[6][0]][is[6][1]];
						int i_96_ = area.r[is[7][0]][is[7][1]];
						switch (i_88_) {
						case -2:
						case -1:
							break;
						case 1:
						case 2:
							drawLine(opaque, attr_87_.word(), i_86_,
									(i_89_ + i_95_) / 2, (i_90_ + i_96_) / 2,
									(i_91_ + i_93_) / 2, (i_92_ + i_94_) / 2);
							break;
						case 0:
							drawTetragon(opaque, i_89_, i_90_, i_91_, i_92_,
									i_93_, i_94_, i_95_, i_96_);
							break;
						case 6:
						case 7:
							drawStripe(opaque, SOLIDEDGECOLORS[i_88_][i],
									i_89_, i_90_, i_91_, i_92_, i_93_, i_94_,
									i_95_, i_96_);
							break;
						case 3: {
							int i_97_ = (i_95_ - i_89_) / 3;
							int i_98_ = (i_96_ - i_90_) / 3;
							int i_99_ = (i_93_ - i_91_) / 3;
							int i_100_ = (i_94_ - i_92_) / 3;
							drawTetragon(opaque, i_89_, i_90_, i_91_, i_92_,
									i_91_ + i_99_, i_92_ + i_100_, i_89_
											+ i_97_, i_90_ + i_98_);
							drawTetragon(opaque, i_95_ - i_97_, i_96_ - i_98_,
									i_93_ - i_99_, i_94_ - i_100_, i_93_,
									i_94_, i_95_, i_96_);
							break;
						}
						case 4:
						case 5: {
							int i_101_ = (i_95_ - i_89_) / 4;
							int i_102_ = (i_96_ - i_90_) / 4;
							int i_103_ = (i_93_ - i_91_) / 4;
							int i_104_ = (i_94_ - i_92_) / 4;
							drawStripe(opaque, STRIPEDEDGECOLORS[i_88_][i][0],
									i_89_, i_90_, i_91_, i_92_, i_91_ + i_103_,
									i_92_ + i_104_, i_89_ + i_101_, i_90_
											+ i_102_);
							drawStripe(opaque, STRIPEDEDGECOLORS[i_88_][i][1],
									i_89_ + i_101_, i_90_ + i_102_, i_91_
											+ i_103_, i_92_ + i_104_, i_93_
											- i_103_, i_94_ - i_104_, i_95_
											- i_101_, i_96_ - i_102_);
							drawStripe(opaque, STRIPEDEDGECOLORS[i_88_][i][2],
									i_95_ - i_101_, i_96_ - i_102_, i_93_
											- i_103_, i_94_ - i_104_, i_93_,
									i_94_, i_95_, i_96_);
							break;
						}
						default:
							throw new RuntimeException(
									"Internal error: bad value for style switch: "
											+ i_88_);
						}
					}
				}
			}
		}
	}

	private void drawLine(Attr.Color.Opaque opaque, String string, int i,
			int i_105_, int i_106_, int i_107_, int i_108_) {
		processColor(opaque);
		attrs.clear();
		addAttribute("style", "CDATA", string);
		addAttribute("thickness", "CDATA", Integer.toString(i));
		addAttribute("x-from", "CDATA", Integer.toString(i_105_));
		addAttribute("y-from", "CDATA", Integer.toString(i_106_));
		addAttribute("x-till", "CDATA", Integer.toString(i_107_));
		addAttribute("y-till", "CDATA", Integer.toString(i_108_));
		try {
			startElement("line", attrs);
			endElement("line");
		} catch (SAXException saxexception) {
			/* empty */
		}
	}

	private void drawStripe(Attr.Color.Opaque opaque, int i, int i_109_,
			int i_110_, int i_111_, int i_112_, int i_113_, int i_114_,
			int i_115_, int i_116_) {
		switch (i) {
		case 3:
			return;
		case 2:
			opaque = opaque.darkerColor();
			break;
		case 1:
			opaque = opaque.lighterColor();
			break;
		}
		drawTetragon(opaque, i_109_, i_110_, i_111_, i_112_, i_113_, i_114_,
				i_115_, i_116_);
	}

	private void drawTetragon(Attr.Color.Opaque opaque, int i, int i_117_,
			int i_118_, int i_119_, int i_120_, int i_121_, int i_122_,
			int i_123_) {
		processColor(opaque);
		try {
			attrs.clear();
			addAttribute("x-from", "CDATA", Integer.toString(i));
			addAttribute("y-from", "CDATA", Integer.toString(i_117_));
			startElement("polygon", attrs);
			attrs.clear();
			addAttribute("x-till", "CDATA", Integer.toString(i_118_));
			addAttribute("y-till", "CDATA", Integer.toString(i_119_));
			startElement("point", attrs);
			endElement("point");
			attrs.clear();
			addAttribute("x-till", "CDATA", Integer.toString(i_120_));
			addAttribute("y-till", "CDATA", Integer.toString(i_121_));
			startElement("point", attrs);
			endElement("point");
			attrs.clear();
			addAttribute("x-till", "CDATA", Integer.toString(i_122_));
			addAttribute("y-till", "CDATA", Integer.toString(i_123_));
			startElement("point", attrs);
			endElement("point");
			endElement("polygon");
		} catch (SAXException saxexception) {
			/* empty */
		}
	}

	private void drawHexagon(Attr.Color.Opaque opaque, int i, int i_124_,
			int i_125_, int i_126_, int i_127_, int i_128_, int i_129_,
			int i_130_, int i_131_, int i_132_, int i_133_, int i_134_) {
		processColor(opaque);
		try {
			attrs.clear();
			addAttribute("x-from", "CDATA", Integer.toString(i));
			addAttribute("y-from", "CDATA", Integer.toString(i_124_));
			startElement("polygon", attrs);
			attrs.clear();
			addAttribute("x-till", "CDATA", Integer.toString(i_125_));
			addAttribute("y-till", "CDATA", Integer.toString(i_126_));
			startElement("point", attrs);
			endElement("point");
			attrs.clear();
			addAttribute("x-till", "CDATA", Integer.toString(i_127_));
			addAttribute("y-till", "CDATA", Integer.toString(i_128_));
			startElement("point", attrs);
			endElement("point");
			attrs.clear();
			addAttribute("x-till", "CDATA", Integer.toString(i_129_));
			addAttribute("y-till", "CDATA", Integer.toString(i_130_));
			startElement("point", attrs);
			endElement("point");
			attrs.clear();
			addAttribute("x-till", "CDATA", Integer.toString(i_131_));
			addAttribute("y-till", "CDATA", Integer.toString(i_132_));
			startElement("point", attrs);
			endElement("point");
			attrs.clear();
			addAttribute("x-till", "CDATA", Integer.toString(i_133_));
			addAttribute("y-till", "CDATA", Integer.toString(i_134_));
			startElement("point", attrs);
			endElement("point");
			endElement("polygon");
		} catch (SAXException saxexception) {
			/* empty */
		}
	}

	public void processRuler(BPB.Block block) {
		AttList attlist = block.ruler;
		if (attlist != null) {
			Area area = makeArea(block);
			String string = attlist.get(Attn.$format).word();
			Attr attr = attlist.get(Attn.$text_align);
			if (attr == Attr.newWord("inside"))
				attr = (page.number % 2 == 1 ? Attr.newWord("start") : Attr
						.newWord("end"));
			else if (attr == Attr.newWord("outside"))
				attr = (page.number % 2 == 0 ? Attr.newWord("start") : Attr
						.newWord("end"));
			if (block.writing_mode == Attr.rl_tb)
				attr = (attr == Attr.newWord("end") ? Attr.newWord("start")
						: Attr.newWord("end"));
			int i = 1;
			Item.Text text = new Item.Text(Util.i2s(i, string), attlist,
					session);
			int i_135_ = text.tt.max_ascender + text.tt.lea;
			int i_136_ = i_135_ - text.tt.min_descender + text.tt.lea;
			y = area.r[2][3] - i_135_;
			int i_137_ = Attr.dim_range(attlist,
					Attn.$inline_progression_dimension_minimum,
					Attn.$inline_progression_dimension_optimum,
					Attn.$inline_progression_dimension_maximum, area.r[2][2]
							- area.r[2][0]);
			int i_138_;
			int i_139_;
			if (block.writing_mode == Attr.rl_tb) {
				i_139_ = area.r[2][2];
				i_138_ = (i_137_ == -2147483648 ? area.r[2][0] : area.r[2][2]
						- i_137_);
			} else {
				i_138_ = area.r[2][0];
				i_139_ = (i_137_ == -2147483648 ? area.r[2][2] : area.r[2][0]
						+ i_137_);
			}
			while (y >= area.r[2][1]) {
				x = (attr == Attr.newWord("end") ? i_139_ - text.width
						: attr == Attr.newWord("center") ? (i_138_ + (i_139_ - text.width)) / 2
								: i_138_);
				process(text);
				y -= i_136_;
				i++;
				text = new Item.Text(Util.i2s(i, string), attlist, session);
			}
		}
	}

	private void process(BPB.Anchor anchor) {
		if (!idlePass) {
			attrs.clear();
			addAttribute("x-from", "CDATA", Integer.toString(anchor.rabs[0]));
			addAttribute("y-from", "CDATA", Integer.toString(anchor.rabs[1]));
			addAttribute("x-till", "CDATA", Integer.toString(anchor.rabs[2]));
			addAttribute("y-till", "CDATA", Integer.toString(anchor.rabs[3]));
			if (anchor.containsKey(Attn.$role))
				addAttribute("role", "CDATA", anchor.get(Attn.$role).word());
			if (anchor.containsKey(Attn.$internal_destination)) {
				addAttribute("destination-id", "CDATA", anchor.get(
						Attn.$internal_destination).word());
				addAttribute("destination", "CDATA", Integer
						.toString(anchor.id.page.no));
				addAttribute("destination-x", "CDATA", Integer
						.toString(anchor.id.x));
				addAttribute("destination-y", "CDATA", Integer
						.toString(anchor.id.y));
				try {
					startElement("internal-link", attrs);
					endElement("internal-link");
				} catch (SAXException saxexception) {
					throw new InternalException(saxexception.getMessage());
				}
			} else if (anchor.containsKey(Attn.$external_destination)) {
				String string = anchor.get(Attn.$external_destination).word();
				if (string != "") {
					addAttribute("destination", "CDATA", string);
					addAttribute("show-destination", "CDATA", anchor.get(
							Attn.$show_destination).word());
					try {
						startElement("external-link", attrs);
						endElement("external-link");
					} catch (SAXException saxexception) {
						throw new InternalException(saxexception.getMessage());
					}
				}
			}
		}
	}

	public void process(Cntl.Rotate rotate) {
		if (!idlePass) {
			if (rotate.phi != 0) {
				attrs.clear();
				addAttribute("phi", "CDATA", Integer.toString(rotate.phi));
				try {
					startElement("rotate", attrs);
					endElement("rotate");
				} catch (SAXException saxexception) {
					throw new InternalException(saxexception.getMessage());
				}
			}
		}
	}

	public void process(Cntl.Translate translate) {
		if (!idlePass) {
			if (translate.x != 0 || translate.y != 0) {
				attrs.clear();
				addAttribute("x", "CDATA", Integer.toString(translate.x));
				addAttribute("y", "CDATA", Integer.toString(translate.y));
				try {
					startElement("translate", attrs);
					endElement("translate");
				} catch (SAXException saxexception) {
					throw new InternalException(saxexception.getMessage());
				}
			}
		}
	}

	public void process(Line line) {
		if (!idlePass) {
			attrs.clear();
			x = line.x;
			y = line.y;
			lwsp = line.wsp;
			lcsp = line.csp;
			Enumeration enumeration = line.words.elements();
			while (enumeration.hasMoreElements())
				((Instruction) enumeration.nextElement()).pass(this);
		}
	}

	public void process(Item.Text text) {
		if (!idlePass) {
			int i = x;
			if (tt != text.tt) {
				tt = text.tt;
				tt.te = (FontRecord) tt.tes.car();
			}
			Attr attr = text.get(Attn.$color);
			Attr attr_140_ = text.get(Attn.$text_shadow);
			if (attr != Attr.Color.transparent || attr_140_ != Attr.none) {
				int i_141_ = (tt.merge_white_space ? tt.varwsp ? tt.wsp + lwsp
						: tt.wsp : 0);
				if (i_141_ != wsp) {
					this.attrs.clear();
					addAttribute("value", "CDATA", Integer.toString(i_141_));
					try {
						startElement("word-spacing", this.attrs);
						endElement("word-spacing");
					} catch (SAXException saxexception) {
						throw new InternalException(saxexception.getMessage());
					}
				}
				wsp = i_141_;
				int i_142_ = tt.varcsp ? tt.csp + lcsp : tt.csp;
				if (i_142_ != csp) {
					this.attrs.clear();
					addAttribute("value", "CDATA", Integer.toString(i_142_));
					try {
						startElement("letter-spacing", this.attrs);
						endElement("letter-spacing");
					} catch (SAXException saxexception) {
						throw new InternalException(saxexception.getMessage());
					}
					csp = i_142_;
				}
				double d = tt.font_stretch;
				if (d != stretch) {
					stretch = d;
					this.attrs.clear();
					addAttribute("value", "CDATA", Double.toString(stretch));
					try {
						startElement("font-stretch", this.attrs);
						endElement("font-stretch");
					} catch (SAXException saxexception) {
						throw new InternalException(saxexception.getMessage());
					}
				}
				if (tt.multifont) {
					int i_143_ = 0;
					int i_144_ = 0;
					int i_145_ = 0;
					int i_146_ = 0;
					boolean bool = false;
					Metric metric = (tt.fss == Attr.auto ? tt.te
							: (FontRecord) tt.tes.car()).getMetric();
					FontRecord fontrecord = tt.te;
					Metric metric_147_ = null;
					while_24_: for (;;) {
						if (i_143_ == text.chars.length)
							bool = true;
						if (bool) {
							if (i_144_ != i_143_) {
								if (tt.te != te || tt.font_size != font_size) {
									te = tt.te;
									font_size = tt.font_size;
									this.attrs.clear();
									addAttribute("family", "CDATA",
											te.familyName);
									addAttribute("weight", "CDATA", (Integer
											.toString(tt.font_weight.count())));
									addAttribute("style", "CDATA",
											tt.font_style.word());
									addAttribute("variant", "CDATA",
											tt.font_variant.word());
									addAttribute("size", "CDATA", Integer
											.toString(font_size));
									try {
										startElement("font", this.attrs);
										endElement("font");
									} catch (SAXException saxexception) {
										throw new InternalException(
												saxexception.getMessage());
									}
								}
								i_145_ = (int) Math.round((double) i_145_
										* tt.font_stretch
										* (double) tt.font_size / 1000.0);
								i_145_ += (i_143_ - i_144_) * csp + i_146_
										* wsp;
								emitText(new String(text.chars, i_144_, i_143_
										- i_144_), x, y + text.offset, i_145_,
										attr, attr_140_);
								x += i_145_;
								i_144_ = i_143_;
								i_145_ = 0;
								i_146_ = 0;
							}
							if (i_143_ == text.chars.length)
								break;
							tt.te = fontrecord;
							if (tt.fss == Attr.auto)
								metric = metric_147_;
							bool = false;
						}
						char c = text.chars[i_143_++];
						CharMetrics charmetrics = metric.ucm(c);
						while_23_: do {
							if (charmetrics == null) {
								Enumeration enumeration = tt.tes.elements();
								do {
									if (!enumeration.hasMoreElements()) {
										if (tt.fss == Attr.auto)
											fontrecord = tt.te;
										else {
											fontrecord = (FontRecord) tt.tes
													.car();
											if (bool = fontrecord != tt.te) {
												i_143_--;
												continue while_24_;
											}
										}
										break while_23_;
									}
									metric_147_ = (fontrecord = ((FontRecord) enumeration
											.nextElement())).getMetric();
								} while ((charmetrics = metric_147_.ucm(c)) == null);
								if (bool = fontrecord != tt.te) {
									i_143_--;
									continue while_24_;
								}
							} else if (tt.fss != Attr.auto) {
								fontrecord = (FontRecord) tt.tes.car();
								if (bool = fontrecord != tt.te) {
									i_143_--;
									continue while_24_;
								}
							}
						} while (false);
						if (charmetrics == null)
							charmetrics = metric.missingGlyph;
						if (charmetrics != null) {
							i_145_ += charmetrics.wx;
							if (c == ' ')
								i_146_++;
						}
					}
				} else {
					if (tt.te != te || tt.font_size != font_size) {
						te = tt.te;
						font_size = tt.font_size;
						this.attrs.clear();
						addAttribute("family", "CDATA", te.familyName);
						addAttribute("weight", "CDATA", Integer
								.toString(tt.font_weight.count()));
						addAttribute("style", "CDATA", tt.font_style.word());
						addAttribute("variant", "CDATA", tt.font_variant.word());
						addAttribute("size", "CDATA", Integer
								.toString(font_size));
						try {
							startElement("font", this.attrs);
							endElement("font");
						} catch (SAXException saxexception) {
							throw new InternalException(saxexception
									.getMessage());
						}
					}
					if (text.nwsp != text.chars.length)
						emitText(new String(text.chars), x, y + text.offset,
								text.width, attr, attr_140_);
				}
				x = i + text.width;
				try {
					Attr attr_148_ = text.get(Attn.$text_decoration);
					Metric metric = ((FontRecord) tt.tes.car()).getMetric();
					List list = new List();
					if (attr_148_ instanceof Attr.Word) {
						String string = attr_148_.word();
						int i_149_ = 0;
						for (;;) {
							int i_150_ = string.indexOf(' ', i_149_ + 1);
							if (i_150_ == -1)
								i_150_ = string.length();
							if (i_150_ - i_149_ > 1)
								list.snoc(string.substring(i_149_, i_150_));
							if (i_150_ == string.length())
								break;
							i_149_ = i_150_ + 1;
						}
					} else if (attr_148_ instanceof Attr.Composite) {
						Attr[] attrs = attr_148_.vector();
						for (int i_151_ = 0; i_151_ != attrs.length; i_151_++)
							list.snoc(attrs[i_151_].word());
					}
					Enumeration enumeration = list.elements();
					while (enumeration.hasMoreElements()) {
						String string = (String) enumeration.nextElement();
						boolean bool = false;
						int i_152_ = y + text.offset;
						if (!string.equals("none")) {
							if (string.equals("underline")) {
								i_152_ += (metric.underlinePosition
										* (float) font_size / 1000.0F);
								bool = true;
							} else if (string.equals("overline")) {
								i_152_ += ((float) (tt.x_height * 4 / 3) - (metric.underlinePosition
										* (float) font_size / 1000.0F));
								bool = true;
							} else if (string.equals("line-through")) {
								i_152_ += tt.x_height * 2 / 3;
								bool = true;
							} else if (!string.equals("blink"))
								session.error("text-decoration='" + string
										+ "'" + " is not supported");
						}
						if (bool) {
							this.attrs.clear();
							addAttribute("x-from", "CDATA", Integer.toString(x
									- text.width));
							addAttribute("x-till", "CDATA", Integer.toString(x));
							addAttribute("y-from", "CDATA", Integer
									.toString(i_152_));
							addAttribute("y-till", "CDATA", Integer
									.toString(i_152_));
							addAttribute("thickness", "CDATA", (Integer
									.toString(Math
											.round(metric.underlineThickness
													/ 1000.0F
													* (float) font_size))));
							addAttribute("style", "CDATA", "solid");
							startElement("line", this.attrs);
							endElement("line");
						}
					}
				} catch (SAXException saxexception) {
					throw new InternalException(saxexception.getMessage());
				}
			}
		}
	}

	private void emitText(String string, int i, int i_153_, int i_154_,
			Attr attr, Attr attr_155_) {
		if (attr_155_ instanceof Attr.Composite) {
			Attr[] attrs = ((Attr.Composite) attr_155_).vector;
			int i_156_ = attrs.length;
			while (i_156_-- > 0) {
				if (!(attrs[i_156_] instanceof Attr.Composite))
					session
							.error("Internal misconfiguration: text-shadow incorrectly parsed");
				else {
					Attr[] attrs_157_ = ((Attr.Composite) attrs[i_156_]).vector;
					Attr attr_158_ = attrs_157_[0];
					Attr attr_159_ = attrs_157_[1];
					Attr attr_160_ = attrs_157_[2];
					if (attr_158_ == null)
						attr_158_ = attr;
					if (attr_158_ instanceof Attr.Color
							&& attr_159_ instanceof Attr.Length
							&& attr_160_ instanceof Attr.Length)
						emitBareText(string, i + attr_159_.length(), i_153_
								- attr_160_.length(), i_154_, attr_158_);
					else
						session
								.error("Internal misconfiguration: text-shadow incorrectly parsed");
				}
			}
		}
		emitBareText(string, i, i_153_, i_154_, attr);
	}

	private void emitBareText(String string, int i, int i_161_, int i_162_,
			Attr attr) {
		if (attr != Attr.Color.transparent) {
			processColor((Attr.Color.Opaque) attr);
			attrs.clear();
			addAttribute("value", "CDATA", string);
			addAttribute("x", "CDATA", Integer.toString(i));
			addAttribute("y", "CDATA", Integer.toString(i_161_));
			addAttribute("width", "CDATA", Integer.toString(i_162_));
			try {
				startElement("text", attrs);
				endElement("text");
			} catch (SAXException saxexception) {
				throw new InternalException(saxexception.getMessage());
			}
		}
	}

	public void process(Item.Leader leader) {
		if (!idlePass) {
			switch (leader.leader_pattern) {
			case 0:
				break;
			case 1:
				processRuleLeader(leader);
				break;
			case 2:
			case 3: {
				int i = leader.width / leader.patwidth;
				if (i > 0) {
					int i_163_ = x;
					leader.dot.offset = leader.offset;
					if (leader.patwidth > leader.dot.width) {
						leader.dot.width = leader.patwidth;
						for (int i_164_ = 0; i_164_ != i; i_164_++)
							process(leader.dot);
					} else {
						Item.Text text = (Item.Text) leader.dot.clone();
						char[] cs = new char[i * text.chars.length];
						for (int i_165_ = 0; i_165_ != cs.length; i_165_ += text.chars.length)
							System.arraycopy(text.chars, 0, cs, i_165_,
									text.chars.length);
						text.chars = cs;
						text.width = leader.patwidth * i;
						process(text);
					}
					x = i_163_;
				}
				break;
			}
			default:
				throw new InternalException("leader_pattern="
						+ leader.leader_pattern);
			}
			x += leader.width;
		}
	}

	private void processRuleLeader(Item.Leader leader) {
		Attr attr = leader.get(Attn.$color);
		if (attr != Attr.Color.transparent) {
			if (!(attr instanceof Attr.Color.Opaque))
				session.error("invalid rule color: " + attr);
			else {
				Attr.Color.Opaque opaque = (Attr.Color.Opaque) attr;
				int i = leader.get(Attn.$rule_thickness).length();
				if (!Util.eq0(i)) {
					Attr attr_166_ = leader.get(Attn.$rule_style);
					Short var_short = (Short) styles.get(attr_166_);
					short i_167_ = 0;
					if (var_short == null) {
						session.error("invalid rule-style: " + attr_166_);
						attr_166_ = Attr.newWord("solid");
					} else
						i_167_ = var_short.shortValue();
					switch (i_167_) {
					case -2:
					case -1:
						break;
					case 0:
					case 1:
					case 2:
						drawLine(opaque, attr_166_.word(), i, x, y
								+ leader.offset, x + leader.width, y
								+ leader.offset);
						break;
					case 3:
						drawLine(opaque, "solid", i / 3, x, y + leader.offset
								+ i / 3, x + leader.width, y + leader.offset
								+ i / 3);
						drawLine(opaque, "solid", i / 3, x, y + leader.offset
								- i / 3, x + leader.width, y + leader.offset
								- i / 3);
						break;
					case 4:
					case 5: {
						int i_168_ = x;
						int i_169_ = x + leader.width;
						int i_170_ = y + leader.offset - i / 2;
						int i_171_ = y + leader.offset + i / 2;
						int i_172_ = i / 4;
						int i_173_ = i / 4;
						if (i_172_ > leader.width / 4)
							i_172_ = leader.width / 4;
						drawStripe(opaque, STRIPEDEDGECOLORS[i_167_][0][0],
								i_168_, i_170_, i_169_, i_170_,
								i_169_ - i_172_, i_170_ + i_173_, i_168_
										+ i_172_, i_170_ + i_173_);
						drawStripe(opaque, STRIPEDEDGECOLORS[i_167_][0][0],
								i_169_, i_171_, i_169_, i_170_,
								i_169_ - i_172_, i_170_ + i_173_, i_169_
										- i_172_, i_171_ - i_173_);
						drawStripe(opaque, STRIPEDEDGECOLORS[i_167_][0][1],
								i_168_ + i_172_, i_170_ + i_173_, i_169_
										- i_172_, i_170_ + i_173_, i_169_
										- i_172_, i_171_ - i_173_, i_168_
										+ i_172_, i_171_ - i_173_);
						drawStripe(opaque, STRIPEDEDGECOLORS[i_167_][0][2],
								i_168_, i_170_, i_168_ + i_172_, i_170_
										+ i_173_, i_168_ + i_172_, i_171_
										- i_173_, i_168_, i_171_);
						drawStripe(opaque, STRIPEDEDGECOLORS[i_167_][0][2],
								i_168_, i_171_, i_169_, i_171_,
								i_169_ - i_172_, i_171_ - i_173_, i_168_
										+ i_172_, i_171_ - i_173_);
						break;
					}
					case 6:
					case 7:
						session.error("invalid rule-style: " + attr_166_
								+ " (not applicable to leaders)");
						break;
					default:
						throw new RuntimeException(
								"Internal error: invalid rule style index "
										+ i_167_);
					}
				}
			}
		}
	}

	public void process(Item.Image image) {
		if (!idlePass) {
			int i = (int) Math.round(image.im.width * 1000.0 * image.scale[0]);
			int i_174_ = (int) Math.round(image.im.height * 1000.0
					* image.scale[1]);
			Attr attr = image.get(Attn.$overflow);
			if (image.im != null) {
				try {
					if (attr != Attr.newWord("visible")) {
						attrs.clear();
						addAttribute("x-from", "CDATA", Integer.toString(x));
						addAttribute("y-from", "CDATA", Integer.toString(y
								+ image.offset));
						addAttribute("x-till", "CDATA", Integer.toString(x
								+ image.width));
						addAttribute("y-till", "CDATA", Integer.toString(y
								+ image.offset + image.ascender));
						startElement("clip", attrs);
					}
					int i_175_ = 0;
					int i_176_ = 0;
					Attr attr_177_ = image.get(Attn.$display_align);
					if (attr_177_ == Attr.newWord("before"))
						i_176_ = image.ascender - i_174_;
					else if (attr_177_ == Attr.newWord("center"))
						i_176_ = (image.ascender - i_174_) / 2;
					Attr attr_178_ = image.get(Attn.$text_align);
					if (attr_178_ == Attr.newWord("inside"))
						attr_178_ = (page.number % 2 == 1 ? Attr
								.newWord("start") : Attr.newWord("end"));
					else if (attr_178_ == Attr.newWord("outside"))
						attr_178_ = (page.number % 2 == 0 ? Attr
								.newWord("start") : Attr.newWord("end"));
					if (image.get(Attn.$writing_mode) == Attr.rl_tb)
						attr_178_ = (attr_178_ == Attr.newWord("end") ? Attr
								.newWord("start") : Attr.newWord("end"));
					if (attr_178_ == Attr.newWord("end"))
						i_175_ = image.width - i;
					else if (attr_178_ == Attr.newWord("center"))
						i_175_ = (image.width - i) / 2;
					attrs.clear();
					addAttribute("src", "CDATA", image.im.source.toString());
					if (image.im.source.scheme != 3)
						addAttribute("base", "CDATA", image.im.source.getBase()
								.toString());
					addAttribute("type", "CDATA", image.im.mimetype);
					addAttribute("x-from", "CDATA", Integer
							.toString(x + i_175_));
					addAttribute("y-from", "CDATA", Integer.toString(y
							+ image.offset + i_176_));
					addAttribute("scale-x", "CDATA", Double
							.toString(image.scale[0]));
					addAttribute("scale-y", "CDATA", Double
							.toString(image.scale[1]));
					addAttribute("width", "CDATA", Integer.toString(i));
					addAttribute("height", "CDATA", Integer.toString(i_174_));
					startElement("image", attrs);
					endElement("image");
					if (attr != Attr.newWord("visible"))
						endElement("clip");
				} catch (SAXException saxexception) {
					throw new InternalException(saxexception.getMessage());
				}
			}
			x += image.width;
		}
	}

	public void process(Item.Washer.Inline inline) {
		if (!idlePass)
			x += inline.thickness;
	}

	public void process(Item.Filled filled) {
		if (idlePass) {
			/* empty */
		}
	}

	public void process(Item.Id id) {
		if (!idlePass) {
			attrs.clear();
			addAttribute("x", "CDATA", Integer.toString(id.x));
			addAttribute("y", "CDATA", Integer.toString(id.y));
			addAttribute("name", "CDATA", id.id.word());
			try {
				startElement("target", attrs);
				endElement("target");
			} catch (SAXException saxexception) {
				throw new InternalException(saxexception.getMessage());
			}
		}
	}

	public void process(Item.Pinpoint pinpoint) {
		if (!idlePass) {
			attrs.clear();
			addAttribute("x", "CDATA", Integer.toString(pinpoint.x));
			addAttribute("y", "CDATA", Integer.toString(pinpoint.y));
			addAttribute("value", "CDATA", pinpoint.get(Attn.$value).word());
			try {
				startElement("pinpoint", attrs);
				endElement("pinpoint");
			} catch (SAXException saxexception) {
				throw new InternalException(saxexception.getMessage());
			}
		}
	}

	public void process(Item.Bookmark bookmark) {
		if (!idlePass) {
			attrs.clear();
			addAttribute("label", "CDATA", bookmark.label);
			addAttribute("id", "CDATA", bookmark.bno.toString());
			addAttribute("parent-id", "CDATA",
					(bookmark.parent != null ? bookmark.parent.bno.toString()
							: "0"));
			addAttribute("collapse-subtree", "CDATA", (bookmark.get(
					Attn.$collapse_subtree).bool() ? "true" : "false"));
			if (bookmark.containsKey(Attn.$internal_destination)) {
				addAttribute("destination-id", "CDATA", bookmark.get(
						Attn.$internal_destination).word());
				addAttribute("destination", "CDATA", Integer
						.toString(bookmark.id.page.no));
				addAttribute("destination-x", "CDATA", Integer
						.toString(bookmark.id.x));
				addAttribute("destination-y", "CDATA", Integer
						.toString(bookmark.id.y));
				try {
					startElement("internal-bookmark", attrs);
					endElement("internal-bookmark");
				} catch (SAXException saxexception) {
					throw new InternalException(saxexception.getMessage());
				}
			} else if (bookmark.containsKey(Attn.$external_destination)) {
				String string = bookmark.get(Attn.$external_destination).word();
				if (string != "") {
					addAttribute("destination", "CDATA", string);
					addAttribute("show-destination", "CDATA", bookmark.get(
							Attn.$show_destination).word());
					try {
						startElement("external-bookmark", attrs);
						endElement("external-bookmark");
					} catch (SAXException saxexception) {
						throw new InternalException(saxexception.getMessage());
					}
				}
			}
		}
	}

	private final void processColor(Attr.Color.Opaque opaque) {
		if (!opaque.equals(color)) {
			color = opaque;
			try {
				if (opaque instanceof Attr.Color.Grayscale) {
					double d = ((Attr.Color.Grayscale) opaque).gray();
					attrs.clear();
					addAttribute("gray", "CDATA", Float.toString((float) d));
					startElement("gray-color", attrs);
					endElement("gray-color");
				} else if (opaque instanceof Attr.Color.RGB) {
					double[] ds = ((Attr.Color.RGB) opaque).r_g_b();
					attrs.clear();
					addAttribute("red", "CDATA", Float.toString((float) ds[0]));
					addAttribute("green", "CDATA", Float
							.toString((float) ds[1]));
					addAttribute("blue", "CDATA", Float.toString((float) ds[2]));
					startElement("rgb-color", attrs);
					endElement("rgb-color");
				} else if (opaque instanceof Attr.Color.CMYK) {
					double[] ds = ((Attr.Color.CMYK) opaque).c_m_y_k();
					attrs.clear();
					addAttribute("cyan", "CDATA", Float.toString((float) ds[0]));
					addAttribute("magenta", "CDATA", Float
							.toString((float) ds[1]));
					addAttribute("yellow", "CDATA", Float
							.toString((float) ds[2]));
					addAttribute("black", "CDATA", Float
							.toString((float) ds[3]));
					startElement("cmyk-color", attrs);
					endElement("cmyk-color");
				} else if (opaque instanceof Attr.Color.SpotColor) {
					Attr.Color.SpotColor spotcolor = (Attr.Color.SpotColor) opaque;
					attrs.clear();
					addAttribute("colorant", "CDATA", spotcolor.colorant);
					addAttribute("tint", "CDATA", Float
							.toString((float) spotcolor.tint()));
					if (spotcolor.altColor == null)
						addAttribute("alt-gray", "CDATA", "0");
					else if (spotcolor.altColor instanceof Attr.Color.Grayscale) {
						double d = ((Attr.Color.Grayscale) spotcolor.altColor)
								.gray();
						addAttribute("alt-gray", "CDATA", Float
								.toString((float) d));
					} else if (spotcolor.altColor instanceof Attr.Color.RGB) {
						double[] ds = ((Attr.Color.RGB) spotcolor.altColor)
								.r_g_b();
						addAttribute("alt-red", "CDATA", Float
								.toString((float) ds[0]));
						addAttribute("alt-green", "CDATA", Float
								.toString((float) ds[1]));
						addAttribute("alt-blue", "CDATA", Float
								.toString((float) ds[2]));
					} else if (spotcolor.altColor instanceof Attr.Color.CMYK) {
						double[] ds = ((Attr.Color.CMYK) spotcolor.altColor)
								.c_m_y_k();
						addAttribute("alt-cyan", "CDATA", Float
								.toString((float) ds[0]));
						addAttribute("alt-magenta", "CDATA", Float
								.toString((float) ds[1]));
						addAttribute("alt-yellow", "CDATA", Float
								.toString((float) ds[2]));
						addAttribute("alt-black", "CDATA", Float
								.toString((float) ds[3]));
					} else
						throw new RuntimeException("Unknown color type: "
								+ spotcolor.altColor.getClass().getName());
					startElement("spot-color", attrs);
					endElement("spot-color");
				} else if (opaque instanceof Attr.Color.Registration) {
					double d = ((Attr.Color.Registration) opaque).tint();
					attrs.clear();
					addAttribute("tint", "CDATA", Float.toString((float) d));
					startElement("registration-color", attrs);
					endElement("registration-color");
				}
			} catch (SAXException saxexception) {
				throw new InternalException(saxexception.getMessage());
			}
		}
	}

	private final Area makeArea(BPB bpb) {
		Area area = new Area(bpb.r, bpb);
		if (bpb.writing_mode == Attr.rl_tb) {
			for (int i = 0; i != 3; i++) {
				int i_179_ = area.r[i][0];
				area.r[i][0] = -area.r[i][2];
				area.r[i][2] = -i_179_;
			}
		}
		return area;
	}

	static {
		styles.put(Attr.none, new Short((short) -2));
		styles.put(Attr.newWord("hidden"), new Short((short) -1));
		styles.put(Attr.newWord("solid"), new Short((short) 0));
		styles.put(Attr.newWord("dotted"), new Short((short) 1));
		styles.put(Attr.newWord("dashed"), new Short((short) 2));
		styles.put(Attr.newWord("double"), new Short((short) 3));
		styles.put(Attr.newWord("ridge"), new Short((short) 4));
		styles.put(Attr.newWord("groove"), new Short((short) 5));
		styles.put(Attr.newWord("outset"), new Short((short) 6));
		styles.put(Attr.newWord("inset"), new Short((short) 7));
	}
}