/*
 * SeekableFileInputStream - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class SeekableFileInputStream extends RandomAccessFile implements
		SeekableInput {
	public SeekableFileInputStream(File file) throws IOException {
		super(file, "r");
	}

	public SeekableFileInputStream(String string) throws IOException {
		super(string, "r");
	}

	public final long readUnsignedInt() throws IOException {
		return (long) this.readInt() & 0xffffffffL;
	}
}