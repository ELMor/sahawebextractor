/*
 * DataURL - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;

public class DataURL {
	private final boolean base64;

	private final String type;

	private final String data;

	private final int dataoffset;

	URLSpec baseURL = null;

	public DataURL(URLSpec urlspec, String string) throws MalformedURLException {
		this(string);
		if (urlspec != null)
			baseURL = urlspec.getBase();
	}

	public DataURL(String string) throws MalformedURLException {
		if (!"data:".equalsIgnoreCase(string.substring(0, 5)))
			throw new MalformedURLException("Not a valid 'data' URL: " + string);
		int i = string.indexOf(',');
		if (i == -1)
			throw new MalformedURLException(
					"Invalid structure of the 'data' URL: no comma found");
		data = string;
		dataoffset = i + 1;
		String string_0_ = string.substring(5, i).trim();
		base64 = string_0_.endsWith(";base64");
		if (base64)
			string_0_ = string_0_.substring(0, string_0_.length() - 7).trim();
		if (string_0_.equals(""))
			type = "text/plain;charset=US-ASCII";
		else if (string_0_.indexOf('/') == -1)
			type = "text/plain;" + string_0_;
		else
			type = string_0_;
	}

	public InputStream openStream() throws IOException {
		StringReader stringreader = new StringReader(data);
		stringreader.skip((long) dataoffset);
		if (base64)
			return new Base64InputStream(stringreader);
		return new URLDecodeInputStream(stringreader);
	}

	public String getContentType() {
		return type;
	}

	public String toString() {
		return data;
	}

	public static void main(String[] strings) throws Exception {
		if (strings.length != 3) {
			System.err
					.println("Usage: {-urlencode | -base64 | -decode} <data URL file> <binary file>");
			System.exit(1);
		}
		if ("-base64".equals(strings[0])) {
			FileWriter filewriter = new FileWriter(strings[1]);
			filewriter.write("data:application/octet-stream;base64,");
			Base64OutputStream base64outputstream = new Base64OutputStream(
					filewriter);
			FileInputStream fileinputstream = new FileInputStream(strings[2]);
			byte[] is = new byte[1024];
			boolean bool = false;
			int i;
			while ((i = fileinputstream.read(is)) != -1)
				base64outputstream.write(is, 0, i);
			fileinputstream.close();
			base64outputstream.close();
			filewriter.close();
		} else if ("-urlencode".equals(strings[0])) {
			char[] cs = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					'A', 'B', 'C', 'D', 'E', 'F' };
			FileWriter filewriter = new FileWriter(strings[1]);
			filewriter.write("data:application/octet-stream,");
			FileInputStream fileinputstream = new FileInputStream(strings[2]);
			byte[] is = new byte[1024];
			boolean bool = false;
			int i;
			while ((i = fileinputstream.read(is)) != -1) {
				for (int i_1_ = 0; i_1_ < i; i_1_++) {
					int i_2_ = is[i_1_];
					if ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.!~*'()"
							.indexOf((char) i_2_) != -1)
						filewriter.write((char) i_2_);
					else {
						filewriter.write('%');
						filewriter.write(cs[i_2_ >> 4 & 0xf]);
						filewriter.write(cs[i_2_ & 0xf]);
					}
				}
			}
			fileinputstream.close();
			filewriter.close();
		} else {
			FileReader filereader = new FileReader(strings[1]);
			StringWriter stringwriter = new StringWriter();
			char[] cs = new char[1024];
			boolean bool = false;
			int i;
			while ((i = filereader.read(cs)) != -1)
				stringwriter.write(cs, 0, i);
			DataURL dataurl = new DataURL(stringwriter.toString());
			System.out.println("Content-type: " + dataurl.getContentType());
			InputStream inputstream = dataurl.openStream();
			FileOutputStream fileoutputstream = new FileOutputStream(strings[2]);
			byte[] is = new byte[1024];
			boolean bool_3_ = false;
			int i_4_;
			while ((i_4_ = inputstream.read(is)) != -1)
				fileoutputstream.write(is, 0, i_4_);
			filereader.close();
			stringwriter.close();
			inputstream.close();
			fileoutputstream.close();
		}
	}
}