/*
 * URLCache - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.Enumeration;

public class URLCache {
	private final Hashtable urltab = new Hashtable();

	private final List filelist = new List();

	public final File dir;

	private int fileno = 0;

	public static class ItemDescriptor {
		public File file = null;

		public byte[] contents = null;

		public String contentType = null;

		public long expiration = 0L;

		public long lastModified = 0L;

		public ItemDescriptor() {
			/* empty */
		}

		public ItemDescriptor(File file, String string) {
			this.file = file;
			contentType = string;
		}

		public ItemDescriptor(byte[] is, String string) {
			contents = is;
			contentType = string;
		}
	}

	private synchronized int nextfileno() {
		return fileno++;
	}

	public URLCache() {
		this(new DefaultErrorHandler());
	}

	public URLCache(ErrorHandler errorhandler) {
		this(null, errorhandler);
	}

	public URLCache(File file, ErrorHandler errorhandler) {
		if (file != null) {
			if (!file.exists()) {
				if (errorhandler != null)
					errorhandler.error("Directory " + file
							+ " does not exist; disk caching disabled.");
				file = null;
			} else if (!file.isDirectory()) {
				if (errorhandler != null)
					errorhandler.error("File " + file
							+ " is not a directory; disk caching disabled.");
				file = null;
			} else if (!file.canWrite()) {
				if (errorhandler != null)
					errorhandler.error("Directory " + file
							+ " is not writable; disk caching disabled.");
				file = null;
			}
		}
		dir = file;
	}

	protected ItemDescriptor lookCache(URLSpec urlspec) {
		synchronized (urltab) {
			return (ItemDescriptor) urltab.get(urlspec.toString());
		}
	}

	protected void saveCache(URLSpec urlspec, ItemDescriptor itemdescriptor) {
		synchronized (urltab) {
			urltab.put(urlspec.toString(), itemdescriptor);
		}
	}

	protected void markFile(File file) {
		if (file != null) {
			synchronized (filelist) {
				filelist.append(file);
			}
		}
	}

	public ItemDescriptor getItemDescriptor(URLSpec urlspec,
			ErrorHandler errorhandler) throws IOException {
		if ("file".equalsIgnoreCase(urlspec.getProtocol()))
			return new ItemDescriptor(URLUtil.toFile(urlspec), null);
		ItemDescriptor itemdescriptor = lookCache(urlspec);
		try {
			long l = 0L;
			long l_0_ = 0L;
			Object object = null;
			Object object_1_ = null;
			URLConnection urlconnection = null;
			String string;
			InputStream inputstream;
			if (urlspec.scheme != 3) {
				if (itemdescriptor != null)
					return itemdescriptor;
				inputstream = urlspec.openStream();
				string = urlspec.getContentType();
			} else {
				urlconnection = urlspec.url.openConnection();
				urlconnection.connect();
				if (urlconnection instanceof HttpURLConnection) {
					int i = ((HttpURLConnection) urlconnection)
							.getResponseCode();
					if (i != 200)
						throw new IOException("HTTP response status is " + i);
				}
				l = urlconnection.getLastModified();
				if (itemdescriptor != null
						&& (l == 0L || itemdescriptor.lastModified >= l)
						&& (itemdescriptor.expiration == 0L || (itemdescriptor.expiration >= System
								.currentTimeMillis())))
					return itemdescriptor;
				l_0_ = urlconnection.getExpiration();
				string = urlconnection.getContentType();
				inputstream = urlconnection.getInputStream();
			}
			File file = getTempFile();
			Object object_2_ = null;
			OutputStream outputstream;
			if (file != null)
				outputstream = new FileOutputStream(file);
			else
				outputstream = new ByteArrayOutputStream();
			try {
				try {
					byte[] is = new byte[2048];
					boolean bool = false;
					int i;
					while ((i = inputstream.read(is)) != -1)
						outputstream.write(is, 0, i);
					if (itemdescriptor == null)
						itemdescriptor = new ItemDescriptor();
					if (file != null) {
						itemdescriptor.file = file;
						itemdescriptor.contents = null;
					} else {
						itemdescriptor.file = null;
						itemdescriptor.contents = ((ByteArrayOutputStream) outputstream)
								.toByteArray();
					}
					itemdescriptor.expiration = l_0_;
					itemdescriptor.contentType = string;
					itemdescriptor.lastModified = l;
					markFile(file);
					saveCache(urlspec, itemdescriptor);
					return itemdescriptor;
				} finally {
					inputstream.close();
					outputstream.close();
					if (urlconnection != null
							&& urlconnection instanceof HttpURLConnection)
						((HttpURLConnection) urlconnection).disconnect();
				}
			} catch (IOException ioexception) {
				if (file != null)
					file.delete();
				throw ioexception;
			}
		} catch (IOException ioexception) {
			if (itemdescriptor != null) {
				errorhandler.warning("Cannot reconnect to \"" + urlspec
						+ "\": " + ioexception.toString()
						+ ". A previously cached copy will be used.");
				return itemdescriptor;
			}
			throw ioexception;
		}
	}

	public void cleanup() {
		synchronized (urltab) {
			urltab.clear();
		}
		synchronized (filelist) {
			Enumeration enumeration = filelist.elements();
			while (enumeration.hasMoreElements())
				((File) enumeration.nextElement()).delete();
			filelist.clear();
		}
	}

	public void finalize() throws Throwable {
		super.finalize();
		cleanup();
	}

	protected File getTempFile(String string) {
		if (dir == null)
			return null;
		File file;
		do
			file = new File(dir, "" + nextfileno() + string);
		while (file.exists());
		return file;
	}

	protected File getTempFile() {
		return getTempFile(".tmp");
	}

	public String getContentType(URLSpec urlspec, ErrorHandler errorhandler)
			throws IOException {
		if (urlspec.scheme != 3)
			return urlspec.getContentType();
		return getItemDescriptor(urlspec, errorhandler).contentType;
	}

	public InputStream openStream(URLSpec urlspec, ErrorHandler errorhandler)
			throws IOException {
		if (urlspec.scheme != 3)
			return urlspec.openStream();
		ItemDescriptor itemdescriptor = getItemDescriptor(urlspec, errorhandler);
		if (itemdescriptor.file != null)
			return new FileInputStream(itemdescriptor.file);
		if (itemdescriptor.contents != null)
			return new ByteArrayInputStream(itemdescriptor.contents);
		return null;
	}

	public SeekableInput openSeekableStream(URLSpec urlspec,
			ErrorHandler errorhandler) throws IOException {
		ItemDescriptor itemdescriptor = getItemDescriptor(urlspec, errorhandler);
		if (itemdescriptor.file != null)
			return new SeekableFileInputStream(itemdescriptor.file);
		if (itemdescriptor.contents != null)
			return new SeekableByteArrayInputStream(itemdescriptor.contents);
		return null;
	}
}