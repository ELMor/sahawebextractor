/*
 * TLS - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.util.Enumeration;

public class TLS {
	private final Hashtable instab = new Hashtable();

	private Thread thread = null;

	private TLSInstance instance = null;

	private Class insclass = null;

	public TLS(Class var_class) {
		insclass = var_class;
	}

	public synchronized TLSInstance instance() throws InstantiationException,
			IllegalAccessException {
		if (this.thread != Thread.currentThread()) {
			this.thread = Thread.currentThread();
			instance = (TLSInstance) instab.get(this.thread);
			if (instance == null) {
				instance = (TLSInstance) insclass.newInstance();
				List list = new List();
				Enumeration enumeration = instab.keys();
				while (enumeration.hasMoreElements()) {
					Thread thread = (Thread) enumeration.nextElement();
					if (!thread.isAlive())
						list.cons(thread);
				}
				Enumeration enumeration_0_ = list.elements();
				while (enumeration_0_.hasMoreElements()) {
					Thread thread = (Thread) enumeration_0_.nextElement();
					instab.remove(thread);
				}
				instab.put(this.thread, instance);
			}
		}
		return instance;
	}

	public synchronized void release() {
		instance = (TLSInstance) instab.remove(Thread.currentThread());
		if (instance != null)
			instance.clear();
		instance = null;
		thread = null;
	}
}