/*
 * Tree - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.util.Enumeration;

public class Tree extends List {
	public void traverse(TreeApplicator treeapplicator) {
		Object object = this.car();
		treeapplicator.pre(object);
		Enumeration enumeration = this.cdr().elements();
		while (enumeration.hasMoreElements())
			((Tree) enumeration.nextElement()).traverse(treeapplicator);
		treeapplicator.post(object);
	}
}