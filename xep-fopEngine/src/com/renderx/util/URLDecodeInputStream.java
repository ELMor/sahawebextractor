/*
 * URLDecodeInputStream - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;

public class URLDecodeInputStream extends InputStream {
	final String hexdigits = "0123456789ABCDEF";

	private Reader reader = null;

	public URLDecodeInputStream(Reader reader) {
		this.reader = reader;
	}

	public URLDecodeInputStream(String string) {
		this(new StringReader(string));
	}

	public boolean markSupported() {
		return false;
	}

	public long skip(long l) throws IOException {
		long l_0_ = 0L;
		while (l-- > 0L && read() != -1)
			l_0_++;
		return l_0_;
	}

	public int read() throws IOException {
		int i = reader.read();
		if (i == -1)
			return -1;
		if (i != 37)
			return i & 0xff;
		int i_1_ = reader.read();
		if (i_1_ != -1) {
			int i_2_ = reader.read();
			if (i_2_ != -1) {
				int i_3_ = "0123456789ABCDEF".indexOf(Character
						.toUpperCase((char) i_1_));
				int i_4_ = "0123456789ABCDEF".indexOf(Character
						.toUpperCase((char) i_2_));
				if (i_3_ != -1 && i_4_ != -1)
					return (i_3_ << 4) + i_4_;
			}
		}
		throw new IOException(
				"Invalid escape sequence in URL-encoded data stream: '%' is not followed by hex digits");
	}
}