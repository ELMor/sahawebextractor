/*
 * Matrix - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

public class Matrix extends Array {
	private int initial_cols;

	public Matrix() {
		this(4, 4);
	}

	public Matrix(int i, int i_0_) {
		super(i);
		initial_cols = i_0_;
	}

	public Object get(int i, int i_1_) {
		Array array = (Array) this.get(i);
		return array == null ? null : array.get(i_1_);
	}

	public void put(int i, int i_2_, Object object) {
		Array array = (Array) this.get(i);
		if (array == null)
			this.put(i, array = new Array(initial_cols));
		array.put(i_2_, object);
	}

	public Object[] datacopy() {
		Object[] objects = super.datacopy();
		for (int i = 0; i != this.length(); i++) {
			Array array = (Array) this.get(i);
			if (array != null)
				objects[i] = array.datacopy();
		}
		return objects;
	}
}