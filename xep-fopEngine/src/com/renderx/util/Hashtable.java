/*
 * Hashtable - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.Serializable;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public class Hashtable extends Dictionary implements Cloneable, Serializable {
	private Object[] table;

	private int halfTableLength;

	private int used;

	private int usedLimit;

	private static final int INIT_SIZE = 1;

	private static final float LOAD_FACTOR = 0.5F;

	private static class Enumerator implements Enumeration {
		private final Object[] table;

		private final int add;

		private int i;

		Enumerator(Object[] objects, int i) {
			table = objects;
			add = i;
			if (objects == null)
				this.i = -1;
			else {
				this.i = objects.length >> 1;
				while (--this.i >= 0 && objects[this.i] == null) {
					/* empty */
				}
			}
		}

		public boolean hasMoreElements() {
			return i >= 0;
		}

		public Object nextElement() {
			if (i < 0)
				throw new NoSuchElementException();
			Object object = table[i + add];
			while (--i >= 0 && table[i] == null) {
				/* empty */
			}
			return object;
		}
	}

	private final int nextIndex(int i) {
		return i == 0 ? halfTableLength - 1 : i - 1;
	}

	private final int firstIndex(Object object) {
		return object.hashCode() & halfTableLength - 1;
	}

	public Hashtable() {
		this(1);
	}

	public Hashtable(int i) {
		i = (int) ((float) (i + 1) / 0.5F);
		for (halfTableLength = 1; halfTableLength < i; halfTableLength <<= 1) {
			/* empty */
		}
		table = new Object[halfTableLength << 1];
		usedLimit = (int) ((float) halfTableLength * 0.5F);
	}

	public final int size() {
		return used;
	}

	public final boolean isEmpty() {
		return used == 0;
	}

	public final Object get(Object object) {
		if (used != 0) {
			for (int i = firstIndex(object); table[i] != null; i = nextIndex(i)) {
				if (table[i].equals(object))
					return table[i | halfTableLength];
			}
		}
		return null;
	}

	public final boolean containsKey(Object object) {
		return get(object) != null;
	}

	public final Object put(Object object, Object object_0_) {
		int i;
		for (i = firstIndex(object); table[i] != null; i = nextIndex(i)) {
			if (object.equals(table[i])) {
				i |= halfTableLength;
				Object object_1_ = table[i];
				table[i] = object_0_;
				return object_1_;
			}
		}
		if (used >= usedLimit) {
			halfTableLength = table.length;
			usedLimit = (int) ((float) halfTableLength * 0.5F);
			Object[] objects = table;
			table = new Object[halfTableLength << 1];
			int i_2_ = objects.length >> 1;
			while (i_2_ > 0) {
				i_2_--;
				if (objects[i_2_] != null) {
					int i_3_;
					for (i_3_ = firstIndex(objects[i_2_]); table[i_3_] != null; i_3_ = nextIndex(i_3_)) {
						/* empty */
					}
					table[i_3_] = objects[i_2_];
					table[i_3_ | halfTableLength] = objects[i_2_
							+ (objects.length >> 1)];
				}
			}
			for (i = firstIndex(object); table[i] != null; i = nextIndex(i)) {
				/* empty */
			}
		}
		used++;
		table[i] = object;
		table[i | halfTableLength] = object_0_;
		return null;
	}

	public Object clone() {
		try {
			Hashtable hashtable_4_ = (Hashtable) super.clone();
			hashtable_4_.table = new Object[table.length];
			System.arraycopy(table, 0, hashtable_4_.table, 0, table.length);
			return hashtable_4_;
		} catch (CloneNotSupportedException clonenotsupportedexception) {
			return null;
		}
	}

	public final Enumeration keys() {
		return new Enumerator(table, 0);
	}

	public final Enumeration elements() {
		return new Enumerator(table, halfTableLength);
	}

	public final Object remove(Object object) {
		if (used > 0) {
			for (int i = firstIndex(object); table[i] != null; i = nextIndex(i)) {
				if (table[i].equals(object)) {
					Object object_5_ = table[i | halfTableLength];
					do {
						table[i] = null;
						table[i | halfTableLength] = null;
						int i_6_ = i;
						int i_7_;
						do {
							i = nextIndex(i);
							if (table[i] == null)
								break;
							i_7_ = firstIndex(table[i]);
						} while (i <= i_7_ && i_7_ < i_6_ || i_7_ < i_6_
								&& i_6_ < i || i_6_ < i && i <= i_7_);
						table[i_6_] = table[i];
						table[i_6_ | halfTableLength] = table[i
								| halfTableLength];
					} while (table[i] != null);
					used--;
					return object_5_;
				}
			}
		}
		return null;
	}

	public final void clear() {
		int i = halfTableLength;
		while (--i >= 0) {
			table[i] = null;
			table[i | halfTableLength] = null;
		}
		used = 0;
	}
}