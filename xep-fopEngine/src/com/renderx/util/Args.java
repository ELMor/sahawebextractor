/*
 * Args - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import junit.framework.Assert;
import junit.framework.TestCase;

public class Args {
	public static final short S = 0;

	public static final short SV = 1;

	public static final short O = 2;

	public static final short A = 3;

	public Token[] tokens;

	public Hashtable switches = new Hashtable();

	public Hashtable options = new Hashtable();

	public List arguments = new List();

	public static final class Test extends TestCase {
		public void testOptions() {
			Args args = new Args(new String[] { "-Dname1=value1", "-Dname2",
					"-D" });
			Assert.assertTrue(args.options.size() == 3);
			Assert.assertTrue(args.options.get("name1").equals("value1"));
			Assert.assertTrue(args.options.get("name2").equals(""));
			Assert.assertTrue(args.options.get("").equals(""));
		}

		public void testEmptyOption() {
			Args args = new Args(new String[] { "-D=value" });
			Assert.assertTrue(args.options.size() == 1);
			Assert.assertTrue(args.options.get("").equals("value"));
		}

		public void testSwitches() {
			Args args = new Args(
					new String[] { "-x", "-key", "-param", "value" },
					new String[] { "param" });
			Assert.assertTrue(args.switches.size() == 3);
			Assert.assertTrue(args.switches.get("x").equals("x"));
			Assert.assertTrue(args.switches.get("key").equals("key"));
			Assert.assertTrue(args.switches.get("param").equals("value"));
			Assert.assertTrue(args.arguments.length() == 0);
		}

		public void testEmptySwitch() {
			Args args = new Args(new String[] { "-", "value" },
					new String[] { "" });
			Assert.assertTrue(args.arguments.length() == 2);
			Assert.assertTrue(args.arguments.car().equals("-"));
			Assert.assertTrue(args.arguments.cdr().car().equals("value"));
		}

		public void testArguments() {
			Args args = new Args(new String[] { "", "x", " -key" });
			Assert.assertTrue(args.arguments.length() == 3);
			Assert.assertTrue(args.arguments.car().equals(""));
			Assert.assertTrue(args.arguments.cdr().car().equals("x"));
			Assert.assertTrue(args.arguments.cdr().cdr().car().equals(" -key"));
		}

		public void testMixed() {
			Args args = new Args(new String[] { "-x", "-key", "value", "one",
					"-Dname=value", "two", "-y", "-", "three" }, new String[] {
					"key", "y" });
			Assert.assertTrue(args.switches.size() == 3);
			Assert.assertTrue(args.options.size() == 1);
			Assert.assertTrue(args.arguments.length() == 3);
			Assert.assertTrue(args.tokens.length == 9);
			short[] is = { 0, 0, 1, 3, 2, 3, 0, 1, 3 };
			for (int i = 0; i != is.length; i++)
				Assert.assertTrue(is[i] == args.tokens[i].typ);
		}
	}

	public static class Token {
		public short typ;

		public String s0;

		public String s1;

		Token(short i, String string) {
			this(i, string, "");
		}

		Token(short i, String string, String string_0_) {
			typ = i;
			s0 = string;
			s1 = string_0_;
		}
	}

	public Args(String[] strings) {
		this(strings, new String[0]);
	}

	public Args(String[] strings, String[] strings_1_) {
		tokens = new Token[strings.length];
		while_8_: for (int i = 0; i != strings.length; i++) {
			if (strings[i].startsWith("-D")) {
				int i_2_ = strings[i].indexOf('=');
				String string;
				String string_3_;
				if (i_2_ == -1)
					options.put(string = strings[i].substring(2),
							string_3_ = "");
				else
					options.put(string = strings[i].substring(2, i_2_),
							string_3_ = strings[i].substring(i_2_ + 1));
				tokens[i] = new Token((short) 2, string, string_3_);
			} else if (strings[i].startsWith("-") && strings[i].length() > 1) {
				String string = strings[i].substring(1);
				tokens[i] = new Token((short) 0, string);
				try {
					for (int i_4_ = 0; i_4_ != strings_1_.length; i_4_++) {
						if (string.equals(strings_1_[i_4_])) {
							switches.put(string, strings[++i]);
							tokens[i] = new Token((short) 1, strings[i]);
							tokens[i - 1].s1 = tokens[i].s0;
							continue while_8_;
						}
					}
					tokens[i].s1 = tokens[i].s0;
					switches.put(string, string);
				} catch (ArrayIndexOutOfBoundsException arrayindexoutofboundsexception) {
					throw new ArrayIndexOutOfBoundsException("switch '-"
							+ string + "' requires an argument");
				}
			} else {
				arguments.append(strings[i]);
				tokens[i] = new Token((short) 3, strings[i]);
			}
		}
	}
}