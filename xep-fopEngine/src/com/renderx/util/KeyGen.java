/*
 * KeyGen - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.security.Security;
import java.util.Date;

import cryptix.math.RandomStream;
import cryptix.mime.LegacyString;
import cryptix.pgp.KeyRingTrust;
import cryptix.pgp.PacketFactory;
import cryptix.pgp.Passphrase;
import cryptix.pgp.PublicKeyCertificate;
import cryptix.pgp.PublicKeyRing;
import cryptix.pgp.SecretKeyCertificate;
import cryptix.pgp.SecretKeyRing;
import cryptix.pgp.UserId;
import cryptix.provider.Cryptix;
import cryptix.security.CSRandomStream;
import cryptix.security.MD5;
import cryptix.security.SHA;
import cryptix.security.rsa.RSAKeyGen;
import cryptix.security.rsa.RSAKeyGenObserver;
import cryptix.security.rsa.SecretKey;

public class KeyGen implements RSAKeyGenObserver {
	private static final int MIN_KEY_LENGTH = 384;

	private static final int MAX_KEY_LENGTH = 6000;

	protected DataInputStream in;

	protected PrintStream out;

	public KeyGen(InputStream inputstream, PrintStream printstream) {
		java.security.Provider provider = Security.getProvider("Cryptix");
		if (provider == null)
			Security.addProvider(new Cryptix());
		in = (inputstream instanceof DataInputStream ? (DataInputStream) inputstream
				: new DataInputStream(inputstream));
		out = printstream;
	}

	public KeyGen() {
		this(System.in, System.out);
	}

	public static void main(String[] strings) {
		new KeyGen().begin();
	}

	protected final void begin() {
		try {
			String string = getUserName();
			int i = getKeyLength();
			RandomStream randomstream;
			SecretKey secretkey = getKey(i, randomstream = getRandomStream(i));
			System.out.println("Key generated.");
			String string_0_ = getPassphrase();
			saveNewKey(secretkey, string, string_0_, randomstream);
		} catch (Exception exception) {
			System.out.println("Key gen aborted.");
			exception.printStackTrace();
		}
	}

	protected SecretKey getKey(int i, RandomStream randomstream) {
		return new RSAKeyGen(randomstream).createKey(i, this);
	}

	protected String getUserName() throws IOException {
		out.print("Please enter your user name: ");
		out.flush();
		String string = in.readLine();
		out.println("using name '" + string + "'");
		return string;
	}

	protected int getKeyLength() throws IOException {
		int i;
		for (;;) {
			out.print("Please enter key size (512, 2048 etc).");
			out.flush();
			try {
				i = Integer.parseInt(in.readLine().trim());
			} catch (NumberFormatException numberformatexception) {
				out.println("Bad format number try again.");
				continue;
			}
			if (i < 384)
				out.println("Key bit length too short. (<384)");
			else {
				if (i <= 6000)
					break;
				out.println("Key bit length too long. (>6000)");
			}
		}
		return i;
	}

	protected RandomStream getRandomStream(int i) throws IOException {
		int i_1_ = i / 6 + 10;
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream(
				8192);
		PrintStream printstream = new PrintStream(bytearrayoutputstream);
		printstream.print(new Date());
		printstream.println("Please enter some random data");
		out.print("You will have to enter a minimum of ");
		out.print(i_1_);
		out.println(" characters;");
		printstream.print(System.currentTimeMillis());
		SHA sha = new SHA();
		MD5 md5 = new MD5();
		while (i_1_ > 0) {
			String string = in.readLine();
			sha.add(LegacyString.toByteArray(Long.toString(System
					.currentTimeMillis())));
			sha.add(LegacyString.toByteArray(string));
			i_1_ -= string.length();
			printstream.print(i_1_);
			printstream.print(string.length());
			printstream.print(System.currentTimeMillis());
			printstream.print(string);
			out.print("characters to go ");
			out.println(i_1_ < 0 ? 0 : i_1_);
			md5.add(LegacyString.toByteArray(string));
			md5.add(LegacyString.toByteArray(Long.toString(System
					.currentTimeMillis(), 16)));
		}
		try {
			printstream.write(sha.digest());
			printstream.write(md5.digest());
		} catch (IOException ioexception) {
			ioexception.printStackTrace(printstream);
		}
		return new CSRandomStream(SHA.hash(bytearrayoutputstream.toByteArray()));
	}

	protected String getPassphrase() throws IOException {
		String string = "";
		for (;;) {
			out.print(" please enter passphrase ");
			out.flush();
			String string_2_ = in.readLine();
			out.print(" please re-enter passphrase ");
			out.flush();
			string = in.readLine();
			if (string_2_.equals(string))
				return string;
			out.println("Passphrases do not match.");
			out.println("Please try again.");
		}
	}

	private final void saveNewKey(SecretKey secretkey, String string,
			String string_3_, RandomStream randomstream) throws IOException {
		UserId userid = new UserId(string);
		PublicKeyRing publickeyring = new PublicKeyRing("pubring.pgp");
		DataOutputStream dataoutputstream = new DataOutputStream(
				new FileOutputStream("pubring.pgp"));
		publickeyring.write(dataoutputstream);
		PublicKeyCertificate publickeycertificate = new PublicKeyCertificate(
				secretkey);
		PacketFactory.write(dataoutputstream, publickeycertificate);
		PacketFactory.write(dataoutputstream, new KeyRingTrust((byte) -121));
		PacketFactory.write(dataoutputstream, userid);
		PacketFactory.write(dataoutputstream, new KeyRingTrust((byte) 3));
		dataoutputstream.close();
		SecretKeyRing secretkeyring = new SecretKeyRing("secring.pgp");
		DataOutputStream dataoutputstream_4_ = new DataOutputStream(
				new FileOutputStream("secring.pgp"));
		secretkeyring.write(dataoutputstream_4_);
		SecretKeyCertificate secretkeycertificate = new SecretKeyCertificate(
				secretkey, new Passphrase(string_3_), randomstream);
		PacketFactory.write(dataoutputstream_4_, secretkeycertificate);
		PacketFactory.write(dataoutputstream_4_, userid);
		dataoutputstream_4_.close();
	}

	public void handleEvent(int i) {
		switch (i) {
		case 1:
			out.print("-");
			out.flush();
			break;
		case 3:
			out.print("+");
			out.flush();
			break;
		case 4:
			out.print("*");
			out.flush();
			break;
		case 5:
			out.println();
			out.print("p:");
			out.flush();
			break;
		case 6:
			out.println();
			out.print("q:");
			out.flush();
			break;
		case 7:
			out.println("Key resolving begins");
			break;
		case 2:
		case 8:
			break;
		default:
			out.println("Unexpected event : " + i);
		}
	}
}