/*
 * Stack - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

public class Stack extends List {
	public final Object pop() {
		return this.shift();
	}

	public final Stack push(Object object) {
		return (Stack) this.cons(object);
	}

	public final Object peek() {
		return this.car();
	}
}