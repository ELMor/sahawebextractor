/*
 * Stat - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

public class Stat {
	public static final native int getCreationTime() throws OutOfMemoryError,
			IllegalArgumentException, IndexOutOfBoundsException,
			ClassCastException, ArithmeticException, RuntimeException,
			InternalError, UnknownError;

	public static void main(String[] strings) {
		java.io.PrintStream printstream = System.out;
		new Stat();
		printstream.println(getCreationTime());
	}

	static {
		try {
			System.loadLibrary("stat");
		} catch (UnsatisfiedLinkError unsatisfiedlinkerror) {
			System.err
					.println("Cannot load the stat DLL for Java.\nMake sure that PATH points to the directory with the stat DLL.\n"
							+ unsatisfiedlinkerror);
			System.exit(1);
		}
	}
}