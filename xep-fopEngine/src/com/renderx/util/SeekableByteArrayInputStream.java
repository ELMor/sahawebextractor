/*
 * SeekableByteArrayInputStream - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class SeekableByteArrayInputStream extends DataInputStream implements
		SeekableInput {
	static class BAIS extends ByteArrayInputStream {
		final int offset;

		final int length;

		public BAIS(byte[] is, int i, int i_0_) throws IOException {
			super(is, i, i_0_);
			offset = i;
			length = i_0_;
		}

		public long length() {
			return (long) length;
		}

		public long getFilePointer() {
			return (long) (pos - offset);
		}

		public void seek(long l) {
			pos = offset + (int) l;
		}
	}

	public SeekableByteArrayInputStream(byte[] is) throws IOException {
		this(is, 0, is.length);
	}

	public SeekableByteArrayInputStream(byte[] is, int i, int i_1_)
			throws IOException {
		super(new BAIS(is, i, i_1_));
	}

	public long length() {
		return ((BAIS) in).length();
	}

	public long getFilePointer() {
		return ((BAIS) in).getFilePointer();
	}

	public void seek(long l) {
		((BAIS) in).seek(l);
	}

	public final long readUnsignedInt() throws IOException {
		return (long) this.readInt() & 0xffffffffL;
	}
}