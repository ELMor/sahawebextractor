/*
 * DeflaterOutputStream - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.Deflater;

public class DeflaterOutputStream extends java.util.zip.DeflaterOutputStream {
	public DeflaterOutputStream(OutputStream outputstream) {
		super(outputstream, new Deflater(9));
	}

	public void write(byte[] is) throws IOException {
		write(is, 0, is.length);
	}

	public void write(byte[] is, int i, int i_0_) throws IOException {
		while (i_0_ > 0) {
			super.write(is, i, i_0_ < buf.length ? i_0_ : buf.length);
			i_0_ -= buf.length;
			i += buf.length;
		}
	}
}