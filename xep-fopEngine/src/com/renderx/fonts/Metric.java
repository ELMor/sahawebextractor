/*
 * Metric - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.PrintStream;

import com.renderx.util.UnicodeTable;

public abstract class Metric {
	public static final float UNDEF = 1.4E-45F;

	private static final char[] missing_glyph_candidates = { '\u00a0', ' ' };

	public int numGlyphs = 0;

	public UnicodeTable uniTable = new UnicodeTable();

	public String fontName = null;

	public String fullName = null;

	public String family = null;

	public String weight = "Roman";

	public float italicAngle = 0.0F;

	public boolean isFixedPitch = false;

	public boolean isCIDFont = false;

	public boolean isTrueType = false;

	public boolean isCFF = false;

	public boolean isTTC = false;

	public float[] fontBBox = null;

	public float underlinePosition = 1.4E-45F;

	public float underlineThickness = 1.4E-45F;

	public String version = "";

	public String notice = "";

	public float capHeight = 1.4E-45F;

	public float xHeight = 1.4E-45F;

	public float ascender = 1.4E-45F;

	public float descender = 1.4E-45F;

	public float wx = 1.4E-45F;

	public float wy = 1.4E-45F;

	public float stdHW = 1.4E-45F;

	public float stdVW = 1.4E-45F;

	public CharMetrics missingGlyph = null;

	public boolean embeddable() {
		return true;
	}

	public boolean subsettable() {
		return true;
	}

	public CharMetrics ucm(char c) {
		return (CharMetrics) uniTable.get(c);
	}

	public boolean exists(char c) {
		return uniTable.get(c) != null;
	}

	public KernVector kern(char c, char c_0_) {
		CharMetrics charmetrics = ucm(c);
		if (charmetrics != null && charmetrics.kernTable != null) {
			KernVector kernvector = (KernVector) charmetrics.kernTable
					.get(c_0_);
			if (kernvector != null)
				return kernvector;
		}
		return new KernVector();
	}

	protected void checkData() throws FontFileFormatError {
		if (fontName == null)
			throw new FontFileFormatError("font name missing");
		if (fullName == null)
			fullName = fontName;
		if (family == null)
			family = fontName;
		if (fontBBox == null)
			throw new FontFileFormatError("bounding box missing");
		if (ascender == 1.4E-45F)
			ascender = fontBBox[3];
		if (descender == 1.4E-45F)
			descender = fontBBox[1];
		if (wx == 1.4E-45F)
			wx = fontBBox[2] - fontBBox[0];
		if (wy == 1.4E-45F)
			wy = fontBBox[3] - fontBBox[1];
		if (capHeight == 1.4E-45F)
			capHeight = ascender;
		if (xHeight == 1.4E-45F)
			xHeight = capHeight;
		if (underlinePosition == 1.4E-45F)
			underlinePosition = descender;
		if (stdHW == 1.4E-45F)
			stdHW = 50.0F;
		if (stdVW == 1.4E-45F)
			stdVW = 100.0F;
		if (underlineThickness == 1.4E-45F)
			underlineThickness = stdHW;
		if (missingGlyph == null) {
			for (int i = 0; i < missing_glyph_candidates.length; i++) {
				missingGlyph = ucm(missing_glyph_candidates[i]);
				if (missingGlyph != null)
					break;
			}
		}
		if (missingGlyph != null)
			uniTable.put('\0', missingGlyph);
	}

	public void dump(PrintStream printstream) {
		printstream.println("System name: " + fontName);
		printstream.println("Full name: " + fullName);
		printstream.println("Version: " + version);
		printstream.println("Notice: " + notice);
		printstream.println("Family: " + family);
		printstream.println("Weight: " + weight);
		printstream.println("Italic angle: " + italicAngle);
		printstream.println("Fixed pitch: " + (isFixedPitch ? "yes" : "no"));
		printstream.println("CID font: " + (isCIDFont ? "yes" : "no"));
		printstream.println("TrueType font: " + (isTrueType ? "yes" : "no"));
		printstream.println("OpenType/CFF font: " + (isCFF ? "yes" : "no"));
		printstream.println("FontBBox: " + fontBBox[0] + " " + fontBBox[1]
				+ " " + fontBBox[2] + " " + fontBBox[3]);
		printstream.println("Ascender: " + ascender);
		printstream.println("Descender: " + descender);
		printstream.println("CapHeight: " + capHeight);
		printstream.println("XHeight: " + xHeight);
		printstream.println("Default advance width - X axis: " + wx);
		printstream.println("Default advance width - Y axis: " + wy);
		printstream.println("Dominant horizontal stem thickness: " + stdHW);
		printstream.println("Dominant vertical stem thickness: " + stdVW);
		printstream.println("Underline thickness: " + underlineThickness);
		printstream.println("Underline position: " + underlinePosition);
		printstream.println("Missing glyph character: "
				+ (missingGlyph == null ? "none" : "U+"
						+ Integer.toHexString(missingGlyph.uc)));
		printstream.print("Policy: ");
		printstream
				.println(embeddable() ? (subsettable() ? "Embedding & subsetting enabled"
						: "Embedding enabled, subsetting disabled")
						: "Embedding disabled");
		printstream.println("Number of glyphs: " + numGlyphs);
		printstream.println();
		for (int i = 0; i < 65535; i++) {
			CharMetrics charmetrics = ucm((char) i);
			if (charmetrics != null) {
				if (charmetrics.uc != (char) i)
					printstream.println("[U+"
							+ Integer.toHexString(i).toUpperCase() + "]"
							+ " --> [U+"
							+ Integer.toHexString(charmetrics.uc).toUpperCase()
							+ "]");
				else
					charmetrics.dump(printstream);
			}
		}
	}

	public String toString() {
		return fullName;
	}
}