/*
 * CharMetrics - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.PrintStream;

import com.renderx.util.List;
import com.renderx.util.UnicodeTable;

public class CharMetrics {
	public char uc = '\0';

	public int c = -1;

	public String n = null;

	public float wx = 1.4E-45F;

	public float wy = 1.4E-45F;

	public float[] b = null;

	public List[] l = null;

	public UnicodeTable kernTable = null;

	public void dump(PrintStream printstream) {
		printstream.println("[U+" + Integer.toHexString(uc).toUpperCase()
				+ "] ID: " + this.c + "; WX: " + wx + "; BBox: " + b[0] + " "
				+ b[1] + " " + b[2] + " " + b[3]);
		if (kernTable != null) {
			int i = 0;
			for (char c = '\0'; c < '\uffff'; c++) {
				KernVector kernvector = (KernVector) kernTable.get(c);
				if (kernvector != null) {
					if (i == 0)
						printstream.print("    Kerning: ");
					else if (i % 3 == 0)
						printstream.print("\n             ");
					else
						printstream.print(" ");
					printstream.print("U+"
							+ Integer.toHexString(c).toUpperCase() + " ["
							+ kernvector.X + " " + kernvector.Y + "]");
					i++;
				}
			}
			if (i > 0)
				printstream.println();
		}
	}
}