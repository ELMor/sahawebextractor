/*
 * TTMetric - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.DataInput;
import java.io.IOException;
import java.io.PrintStream;

import com.renderx.util.Array;
import com.renderx.util.Hashtable;
import com.renderx.util.SeekableFileInputStream;
import com.renderx.util.SeekableInput;
import com.renderx.util.SeekableInputOffsetFilter;
import com.renderx.util.UnicodeTable;

public class TTMetric extends Metric {
	private static final char[] macTable = new char[256];

	boolean restrictEmbedding = false;

	boolean readOnlyEmbedding = true;

	boolean editableEmbedding = true;

	boolean disableSubsetting = false;

	boolean bitmapOnlyEmbedding = false;

	Hashtable glyphIndexTable = null;

	Hashtable table_offsets;

	Hashtable table_length;

	protected CFFMetric cffMetric = null;

	public long cff_offset = 0L;

	public long cff_length = 0L;

	public int subfontNumber = 1;

	public int subfontsTotal = 1;

	public static String[] DEFAULT_CHARSET_TO_UGL;

	public boolean embeddable() {
		return !bitmapOnlyEmbedding
				&& (readOnlyEmbedding || editableEmbedding || !restrictEmbedding);
	}

	public boolean subsettable() {
		return !disableSubsetting;
	}

	public TTMetric(SeekableInput seekableinput, int i) throws IOException,
			FontFileFormatError {
		isTrueType = true;
		isCIDFont = true;
		glyphIndexTable = new Hashtable();
		byte[] is = new byte[4];
		seekableinput.readFully(is);
		if (is[0] == 116 && is[1] == 116 && is[2] == 99 && is[3] == 102) {
			isTTC = true;
			seekSubfont(seekableinput, i);
			seekableinput.readFully(is);
		} else if (i != 1)
			throw new FontFileFormatError(
					"Font is not a TrueType Collection; cannot extract multiple fonts");
		parseTablesAndNames(seekableinput, is);
		long l = getTableOffset(table_offsets, "maxp");
		seekableinput.seek(l + 4L);
		numGlyphs = seekableinput.readUnsignedShort();
		long l_0_ = getTableOffset(table_offsets, "head");
		seekableinput.seek(l_0_ + 12L);
		if (seekableinput.readUnsignedInt() != 1594834165L)
			throw new FontFileFormatError(
					"Magic number in the font header does not match the spec: file probably corrupt");
		seekableinput.skipBytes(2);
		double d = 1.0 * (double) seekableinput.readUnsignedShort();
		seekableinput.skipBytes(16);
		fontBBox = new float[4];
		fontBBox[0] = readLength(seekableinput, d);
		fontBBox[1] = readLength(seekableinput, d);
		fontBBox[2] = readLength(seekableinput, d);
		fontBBox[3] = readLength(seekableinput, d);
		long l_1_ = getTableOffset(table_offsets, "OS/2");
		seekableinput.seek(l_1_);
		int i_2_ = seekableinput.readUnsignedShort();
		wx = readLength(seekableinput, d);
		switch (seekableinput.readUnsignedShort()) {
		case 100:
			weight = "Thin";
			break;
		case 200:
			weight = "Extra-light";
			break;
		case 300:
			weight = "Light";
			break;
		case 400:
			weight = "Normal";
			break;
		case 500:
			weight = "Medium";
			break;
		case 600:
			weight = "Demi-bold";
			break;
		case 700:
			weight = "Bold";
			break;
		case 800:
			weight = "Extra-Bold";
			break;
		case 900:
			weight = "Black";
			break;
		}
		seekableinput.skipBytes(2);
		int i_3_ = seekableinput.readUnsignedShort();
		restrictEmbedding = (i_3_ & 0x2) != 0;
		readOnlyEmbedding = (i_3_ & 0x4) != 0;
		editableEmbedding = (i_3_ & 0x8) != 0;
		disableSubsetting = (i_3_ & 0x100) != 0;
		bitmapOnlyEmbedding = (i_3_ & 0x200) != 0;
		seekableinput.skipBytes(58);
		ascender = readLength(seekableinput, d);
		descender = readLength(seekableinput, d);
		char c = '\0';
		if (i_2_ == 2) {
			seekableinput.skipBytes(14);
			float f = readLength(seekableinput, d);
			if (f != 0.0F)
				xHeight = f;
			float f_4_ = readLength(seekableinput, d);
			if (f_4_ != 0.0F)
				capHeight = f_4_;
			c = (char) seekableinput.readUnsignedShort();
		}
		int i_5_ = 65536;
		int[] is_6_ = null;
		Array array = null;
		if (table_offsets.get("post") != null) {
			long l_7_ = getTableOffset(table_offsets, "post");
			seekableinput.seek(l_7_);
			i_5_ = seekableinput.readInt();
			italicAngle = (float) seekableinput.readShort();
			seekableinput.skipBytes(2);
			underlinePosition = readLength(seekableinput, d);
			underlineThickness = readLength(seekableinput, d);
			isFixedPitch = seekableinput.readUnsignedInt() != 0L;
			seekableinput.readInt();
			seekableinput.readInt();
			seekableinput.readInt();
			seekableinput.readInt();
			switch (i_5_) {
			case 65536:
			case 196608:
				break;
			case 131072: {
				int i_8_ = seekableinput.readUnsignedShort();
				is_6_ = new int[numGlyphs];
				for (int i_9_ = 0; i_9_ < numGlyphs; i_9_++)
					is_6_[i_9_] = 0;
				int i_10_ = 0;
				for (int i_11_ = 0; i_11_ < i_8_; i_11_++) {
					is_6_[i_11_] = seekableinput.readUnsignedShort();
					if (i_10_ < is_6_[i_11_])
						i_10_ = is_6_[i_11_];
				}
				if (i_10_ > 257) {
					array = new Array();
					i_10_ -= 257;
					for (int i_12_ = 0; i_12_ < i_10_; i_12_++) {
						int i_13_ = seekableinput.readUnsignedByte();
						String string = "";
						for (int i_14_ = 0; i_14_ < i_13_; i_14_++)
							string += (char) seekableinput.readUnsignedByte();
						array.put(i_12_ + 258, string);
					}
				}
				break;
			}
			case 163840:
				for (int i_15_ = 0; i_15_ < numGlyphs; i_15_++)
					is_6_[i_15_] = seekableinput.readShort() + i_15_;
				break;
			default:
				throw new FontFileFormatError(
						"Wrong Format Type into post table");
			}
		}
		long l_16_ = getTableOffset(table_offsets, "hhea");
		seekableinput.seek(l_16_ + 34L);
		int i_17_ = seekableinput.readUnsignedShort();
		float[] fs = new float[i_17_];
		long l_18_ = getTableOffset(table_offsets, "hmtx");
		seekableinput.seek(l_18_);
		for (int i_19_ = 0; i_19_ < i_17_; i_19_++) {
			fs[i_19_] = readLength(seekableinput, d);
			seekableinput.skipBytes(2);
		}
		float[] fs_20_ = null;
		boolean bool = false;
		if (table_offsets.get("vmtx") != null) {
			long l_21_ = getTableOffset(table_offsets, "vhea");
			seekableinput.seek(l_21_ + 34L);
			int i_22_ = seekableinput.readUnsignedShort();
			long l_23_ = getTableOffset(table_offsets, "vmtx");
			seekableinput.seek(l_23_);
			fs_20_ = new float[i_22_];
			for (int i_24_ = 0; i_24_ < i_22_; i_24_++) {
				fs_20_[i_24_] = readLength(seekableinput, d);
				seekableinput.skipBytes(2);
			}
		}
		long[] ls = null;
		if (!isCFF) {
			seekableinput.seek(l_0_ + 50L);
			boolean bool_25_ = seekableinput.readUnsignedShort() == 0;
			ls = new long[numGlyphs];
			seekableinput.seek(getTableOffset(table_offsets, "loca"));
			if (bool_25_) {
				for (int i_26_ = 0; i_26_ < numGlyphs; i_26_++)
					ls[i_26_] = (long) (seekableinput.readUnsignedShort() * 2);
			} else {
				for (int i_27_ = 0; i_27_ < numGlyphs; i_27_++)
					ls[i_27_] = seekableinput.readUnsignedInt();
			}
		} else {
			cff_offset = getTableOffset(table_offsets, "CFF ");
			cff_length = getTableOffset(table_length, "CFF ");
			cffMetric = new CFFMetric(new SeekableInputOffsetFilter(
					seekableinput, cff_offset), fontName);
			isCIDFont = cffMetric.isCIDFont;
		}
		long l_28_ = getTableOffset(table_offsets, "cmap");
		seekableinput.seek(l_28_ + 2L);
		int i_29_ = seekableinput.readUnsignedShort();
		while (i_29_-- > 0) {
			int i_30_ = seekableinput.readUnsignedShort();
			int i_31_ = seekableinput.readUnsignedShort();
			if (i_30_ == 3 && i_31_ == 1)
				break;
			seekableinput.skipBytes(4);
		}
		if (i_29_ < 0)
			throw new FontFileFormatError(
					"Cannot use TrueType/OpenType font: no Unicode mapping table found");
		long l_32_ = seekableinput.readUnsignedInt();
		seekableinput.seek(l_28_ + l_32_);
		if (seekableinput.readUnsignedShort() != 4)
			throw new FontFileFormatError(
					"Cannot use TrueType/OpenType font: unknown format of Unicode 'cmap' table");
		int i_33_ = seekableinput.readUnsignedShort();
		seekableinput.skipBytes(2);
		int i_34_ = seekableinput.readUnsignedShort() / 2;
		seekableinput.skipBytes(6);
		for (/**/; i_33_ < 16 + 8 * i_34_; i_33_ += 65536) {
			/* empty */
		}
		int i_35_ = i_33_ / 2 - (8 + i_34_ * 4);
		int[] is_36_ = readUnsignedShortArray(seekableinput, i_34_);
		seekableinput.skipBytes(2);
		int[] is_37_ = readUnsignedShortArray(seekableinput, i_34_);
		int[] is_38_ = readUnsignedShortArray(seekableinput, i_34_);
		int[] is_39_ = readUnsignedShortArray(seekableinput, i_34_);
		int[] is_40_ = readUnsignedShortArray(seekableinput, i_35_);
		long l_41_ = isCFF ? 0L : getTableOffset(table_offsets, "glyf");
		missingGlyph = new CharMetrics();
		missingGlyph.uc = '\0';
		missingGlyph.c = 0;
		missingGlyph.b = fontBBox;
		missingGlyph.wx = fs != null ? fs[0] : 1000.0F;
		missingGlyph.wy = fs_20_ != null ? fs_20_[0] : 1000.0F;
		uniTable.put('\0', missingGlyph);
		for (int i_42_ = 0; i_42_ < i_34_; i_42_++) {
			int i_43_ = is_37_[i_42_];
			int i_44_ = is_36_[i_42_];
			int i_45_ = is_38_[i_42_];
			int i_46_ = is_39_[i_42_];
			for (int i_47_ = i_43_; i_47_ <= i_44_ && i_47_ < 65534; i_47_++) {
				CharMetrics charmetrics = new CharMetrics();
				charmetrics.uc = (char) i_47_;
				boolean bool_48_ = false;
				int i_49_;
				if (i_46_ == 0)
					i_49_ = i_47_ + i_45_ & 0xffff;
				else {
					int i_50_ = i_47_ - i_43_ + i_46_ / 2 - (i_34_ - i_42_);
					if (i_50_ < 0 || i_50_ >= is_40_.length)
						throw new FontFileFormatError(
								"Error in TrueType font: Glyph ID for U+"
										+ Integer.toHexString(i_47_)
										+ " not found in cmap table");
					i_49_ = is_40_[i_50_];
				}
				if (i_49_ > numGlyphs)
					throw new FontFileFormatError(
							"Error in TrueType font: Glyph ID " + i_49_
									+ "exceeds max number of glyphs ("
									+ numGlyphs + ")");
				if (fs != null)
					charmetrics.wx = i_49_ < fs.length ? fs[i_49_]
							: fs[fs.length - 1];
				if (fs_20_ != null)
					charmetrics.wy = (i_49_ < fs_20_.length ? fs_20_[i_49_]
							: fs_20_[fs_20_.length - 1]);
				if (isCFF)
					charmetrics.b = fontBBox;
				else {
					charmetrics.b = new float[4];
					seekableinput.seek(l_41_ + ls[i_49_]);
					seekableinput.skipBytes(2);
					charmetrics.b[0] = readLength(seekableinput, d);
					charmetrics.b[1] = readLength(seekableinput, d);
					charmetrics.b[2] = readLength(seekableinput, d);
					charmetrics.b[3] = readLength(seekableinput, d);
					if (capHeight == 1.4E-45F && charmetrics.uc == 'H')
						capHeight = charmetrics.b[3];
					if (xHeight == 1.4E-45F && charmetrics.uc == 'x')
						xHeight = charmetrics.b[3];
				}
				charmetrics.c = i_49_;
				charmetrics.n = new String(getName(i_49_, i_5_, is_6_, array));
				if (isCFF) {
					if (cffMetric.isCIDFont)
						charmetrics.c = cffMetric.getGlyphCID(i_49_);
					else
						charmetrics.n = cffMetric.getGlyphName(i_49_);
				}
				if (c == charmetrics.uc && i_49_ != 0)
					missingGlyph = charmetrics;
				uniTable.put(charmetrics.uc, charmetrics);
				glyphIndexTable.put(new Integer(charmetrics.c), charmetrics);
			}
		}
		if (table_offsets.get("kern") != null) {
			long l_51_ = getTableOffset(table_offsets, "kern");
			seekableinput.seek(l_51_ + 2L);
			int i_52_ = seekableinput.readUnsignedShort();
			while (i_52_-- > 0) {
				seekableinput.skipBytes(2);
				int i_53_ = seekableinput.readUnsignedShort();
				int i_54_ = seekableinput.readUnsignedShort();
				if (((i_54_ | ~0x4fffffff) ^ ~0x4fffffff) > 1)
					seekableinput.skipBytes(i_53_ - 6);
				else {
					int i_55_ = i_54_ & ~0x7fffffff;
					int i_56_ = i_54_ & 0x20000000;
					boolean bool_57_ = (i_54_ & 0x10000000) > 0;
					int i_58_ = seekableinput.readUnsignedShort();
					seekableinput.skipBytes(6);
					while (i_58_-- > 0) {
						Integer integer = new Integer(seekableinput
								.readUnsignedShort());
						Integer integer_59_ = new Integer(seekableinput
								.readUnsignedShort());
						double d_60_ = (double) readLength(seekableinput, d);
						CharMetrics charmetrics = (CharMetrics) glyphIndexTable
								.get(integer);
						CharMetrics charmetrics_61_ = (CharMetrics) glyphIndexTable
								.get(integer_59_);
						if (charmetrics != null && charmetrics_61_ != null) {
							char c_62_ = charmetrics_61_.uc;
							if (charmetrics.kernTable == null)
								charmetrics.kernTable = new UnicodeTable();
							KernVector kernvector = (charmetrics.kernTable
									.get(c_62_) == null ? new KernVector()
									: ((KernVector) charmetrics.kernTable
											.get(c_62_)));
							if (i_55_ == i_56_
									&& (kernvector.X == 0.0 || bool_57_))
								kernvector.X = d_60_;
							else if (i_55_ != i_56_
									&& (kernvector.Y == 0.0 || bool_57_))
								kernvector.Y = d_60_;
							charmetrics.kernTable.put(c_62_, kernvector);
						}
					}
				}
			}
		}
		this.checkData();
		table_offsets = null;
		table_length = null;
	}

	private static float readLength(SeekableInput seekableinput, double d)
			throws IOException {
		return (float) (1000.0 * (double) (float) seekableinput.readShort() / d);
	}

	private static int[] readUnsignedShortArray(DataInput datainput, int i)
			throws IOException {
		int[] is = new int[i];
		for (int i_63_ = 0; i_63_ < i; i_63_++)
			is[i_63_] = datainput.readUnsignedShort();
		return is;
	}

	private static String readName(SeekableInput seekableinput, long l, int i,
			long l_64_, int i_65_) throws IOException {
		if (i > 0)
			return readUnicodeString(seekableinput, l, i);
		if (i_65_ > 0)
			return readMacRomanString(seekableinput, l_64_, i_65_);
		return "";
	}

	private static String readUnicodeString(SeekableInput seekableinput,
			long l, int i) throws IOException {
		seekableinput.seek(l);
		char[] cs = new char[i];
		for (int i_66_ = 0; i_66_ < i; i_66_++)
			cs[i_66_] = (char) seekableinput.readUnsignedShort();
		return new String(cs);
	}

	private static String readMacRomanString(SeekableInput seekableinput,
			long l, int i) throws IOException {
		seekableinput.seek(l);
		char[] cs = new char[i];
		for (int i_67_ = 0; i_67_ < i; i_67_++)
			cs[i_67_] = macTable[seekableinput.readUnsignedByte()];
		return new String(cs);
	}

	private static long getTableOffset(Hashtable hashtable, String string)
			throws FontFileFormatError {
		Long var_long = (Long) hashtable.get(string);
		if (var_long == null)
			throw new FontFileFormatError(
					"Invalid TrueType/OpenType file structure: no '" + string
							+ "' table found");
		return var_long.longValue();
	}

	private String getName(int i, int i_68_, int[] is, Array array) {
		switch (i_68_) {
		case 65536:
		case 196608:
			if (i < DEFAULT_CHARSET_TO_UGL.length)
				return DEFAULT_CHARSET_TO_UGL[i];
			return "uni" + i;
		case 131072:
		case 163840: {
			int i_69_ = is[i];
			if (i_69_ < 258)
				return DEFAULT_CHARSET_TO_UGL[i_69_];
			return (String) array.get(i_69_);
		}
		default:
			return ".notdef";
		}
	}

	private void seekSubfont(SeekableInput seekableinput, int i)
			throws IOException, FontFileFormatError {
		byte[] is = new byte[4];
		seekableinput.readFully(is);
		int i_70_ = is[0] * 256 + is[1];
		int i_71_ = is[2] * 256 + is[3];
		if (i_70_ != 1 && i_70_ != 2 || i_71_ != 0)
			throw new FontFileFormatError(
					"Unsupported version of TrueType Collection header: "
							+ i_70_ + "." + i_71_);
		subfontsTotal = seekableinput.readInt();
		if (i > subfontsTotal || i < 1)
			throw new FontFileFormatError(
					"Invalid font number "
							+ i
							+ " in TrueType collection: should be in the range from 1 to "
							+ subfontsTotal + ".");
		subfontNumber = i;
		int[] is_72_ = new int[subfontsTotal];
		for (int i_73_ = 0; i_73_ < subfontsTotal; i_73_++)
			is_72_[i_73_] = seekableinput.readInt();
		seekableinput.seek((long) is_72_[subfontNumber - 1]);
	}

	private void parseTablesAndNames(SeekableInput seekableinput, byte[] is)
			throws IOException, FontFileFormatError {
		if (is[0] == 79 && is[1] == 84 && is[2] == 84 && is[3] == 79)
			isCFF = true;
		else {
			int i = is[0] * 256 + is[1];
			int i_74_ = is[2] * 256 + is[3];
			if (i != 1 || i_74_ != 0)
				throw new FontFileFormatError(
						"Not a TrueType/OpenType file, or unsupported TrueType version: "
								+ i + "." + i_74_);
			isCFF = false;
		}
		int i = seekableinput.readUnsignedShort();
		seekableinput.skipBytes(6);
		table_offsets = new Hashtable();
		table_length = new Hashtable();
		while (i-- > 0) {
			byte[] is_75_ = new byte[4];
			seekableinput.readFully(is_75_);
			String string = new String(is_75_);
			seekableinput.skipBytes(4);
			long l = seekableinput.readUnsignedInt();
			long l_76_ = seekableinput.readUnsignedInt();
			table_offsets.put(string, new Long(l));
			table_length.put(string, new Long(l_76_));
		}
		long l = getTableOffset(table_offsets, "name");
		seekableinput.seek(l + 2L);
		int i_77_ = seekableinput.readUnsignedShort();
		int i_78_ = seekableinput.readUnsignedShort();
		long[] ls = new long[21];
		int[] is_79_ = new int[21];
		long[] ls_80_ = new long[21];
		int[] is_81_ = new int[21];
		for (int i_82_ = 0; i_82_ <= 20; i_82_++) {
			ls[i_82_] = 0L;
			is_79_[i_82_] = 0;
			ls_80_[i_82_] = 0L;
			is_81_[i_82_] = 0;
		}
		while (i_77_-- > 0) {
			int i_83_ = seekableinput.readUnsignedShort();
			int i_84_ = seekableinput.readUnsignedShort();
			seekableinput.skipBytes(2);
			int i_85_ = seekableinput.readUnsignedShort();
			int i_86_ = seekableinput.readUnsignedShort();
			long l_87_ = l + (long) i_78_
					+ (long) seekableinput.readUnsignedShort();
			if (i_85_ <= 20) {
				if (i_83_ == 0 || i_83_ == 3 && i_84_ == 1) {
					ls[i_85_] = l_87_;
					is_79_[i_85_] = i_86_ / 2;
				} else if (i_83_ == 1 && i_84_ == 0) {
					ls_80_[i_85_] = l_87_;
					is_81_[i_85_] = i_86_;
				}
			}
		}
		notice = readName(seekableinput, ls[0], is_79_[0], ls_80_[0], is_81_[0]);
		family = readName(seekableinput, ls[1], is_79_[1], ls_80_[1], is_81_[1]);
		fullName = readName(seekableinput, ls[4], is_79_[4], ls_80_[4],
				is_81_[4]);
		version = readName(seekableinput, ls[5], is_79_[5], ls_80_[5],
				is_81_[5]);
		fontName = readName(seekableinput, ls[6], is_79_[6], ls_80_[6],
				is_81_[6]);
	}

	public CFFMetric getCFFMetric() throws FontFileFormatError {
		if (isCFF) {
			if (cffMetric != null)
				return cffMetric;
			throw new FontFileFormatError("Unparsed a OpenType file");
		}
		throw new FontFileFormatError("Not a OpenType/CFF file");
	}

	public void dump(PrintStream printstream) {
		super.dump(printstream);
		printstream.println("\nTrueType-specific font data:");
		if (isTTC)
			printstream
					.println("  Font is a subfont in a TrueType Collection file (font number "
							+ subfontNumber + ", total " + subfontsTotal + ")");
		printstream
				.println(restrictEmbedding ? "  Embedding subject to restrictions"
						: "  No embedding restrictions");
		if (readOnlyEmbedding)
			printstream.println("  Print & Preview embedding allowed");
		if (editableEmbedding)
			printstream.println("  Editable embedding allowed");
		if (bitmapOnlyEmbedding)
			printstream
					.println("  Only bitmap embedding allowed (no outline data)");
		printstream.println(disableSubsetting ? "  Subsetting not allowed"
				: "  Subsetting allowed");
	}

	public static void main(String[] strings) throws Exception {
		if (strings.length == 1)
			new TTMetric(new SeekableFileInputStream(strings[0]), 1)
					.dump(System.out);
		else if (strings.length == 2)
			new TTMetric(new SeekableFileInputStream(strings[0]), Integer
					.parseInt(strings[1])).dump(System.out);
		else
			System.err
					.println("Usage: java com.renderx.fonts.TTMetric <TTF/OTF/TTC font filename> (<TTC subfont number>)");
	}

	static {
		for (int i = 0; i < macTable.length; i++)
			macTable[i] = '\0';
		macTable[32] = ' ';
		macTable[33] = '!';
		macTable[34] = '\"';
		macTable[35] = '#';
		macTable[36] = '$';
		macTable[37] = '%';
		macTable[38] = '&';
		macTable[39] = '\'';
		macTable[40] = '(';
		macTable[41] = ')';
		macTable[42] = '*';
		macTable[43] = '+';
		macTable[44] = ',';
		macTable[45] = '-';
		macTable[46] = '.';
		macTable[47] = '/';
		macTable[48] = '0';
		macTable[49] = '1';
		macTable[50] = '2';
		macTable[51] = '3';
		macTable[52] = '4';
		macTable[53] = '5';
		macTable[54] = '6';
		macTable[55] = '7';
		macTable[56] = '8';
		macTable[57] = '9';
		macTable[58] = ':';
		macTable[59] = ';';
		macTable[60] = '<';
		macTable[61] = '=';
		macTable[62] = '>';
		macTable[63] = '?';
		macTable[64] = '@';
		macTable[65] = 'A';
		macTable[66] = 'B';
		macTable[67] = 'C';
		macTable[68] = 'D';
		macTable[69] = 'E';
		macTable[70] = 'F';
		macTable[71] = 'G';
		macTable[72] = 'H';
		macTable[73] = 'I';
		macTable[74] = 'J';
		macTable[75] = 'K';
		macTable[76] = 'L';
		macTable[77] = 'M';
		macTable[78] = 'N';
		macTable[79] = 'O';
		macTable[80] = 'P';
		macTable[81] = 'Q';
		macTable[82] = 'R';
		macTable[83] = 'S';
		macTable[84] = 'T';
		macTable[85] = 'U';
		macTable[86] = 'V';
		macTable[87] = 'W';
		macTable[88] = 'X';
		macTable[89] = 'Y';
		macTable[90] = 'Z';
		macTable[91] = '[';
		macTable[92] = '\\';
		macTable[93] = ']';
		macTable[94] = '^';
		macTable[95] = '_';
		macTable[96] = '`';
		macTable[97] = 'a';
		macTable[98] = 'b';
		macTable[99] = 'c';
		macTable[100] = 'd';
		macTable[101] = 'e';
		macTable[102] = 'f';
		macTable[103] = 'g';
		macTable[104] = 'h';
		macTable[105] = 'i';
		macTable[106] = 'j';
		macTable[107] = 'k';
		macTable[108] = 'l';
		macTable[109] = 'm';
		macTable[110] = 'n';
		macTable[111] = 'o';
		macTable[112] = 'p';
		macTable[113] = 'q';
		macTable[114] = 'r';
		macTable[115] = 's';
		macTable[116] = 't';
		macTable[117] = 'u';
		macTable[118] = 'v';
		macTable[119] = 'w';
		macTable[120] = 'x';
		macTable[121] = 'y';
		macTable[122] = 'z';
		macTable[123] = '{';
		macTable[124] = '|';
		macTable[125] = '}';
		macTable[126] = '~';
		macTable[127] = '\u007f';
		macTable[128] = '\u00c4';
		macTable[129] = '\u00c5';
		macTable[130] = '\u00c7';
		macTable[131] = '\u00c9';
		macTable[132] = '\u00d1';
		macTable[133] = '\u00d6';
		macTable[134] = '\u00dc';
		macTable[135] = '\u00e1';
		macTable[136] = '\u00e0';
		macTable[137] = '\u00e2';
		macTable[138] = '\u00e4';
		macTable[139] = '\u00e3';
		macTable[140] = '\u00e5';
		macTable[141] = '\u00e7';
		macTable[142] = '\u00e9';
		macTable[143] = '\u00e8';
		macTable[144] = '\u00ea';
		macTable[145] = '\u00eb';
		macTable[146] = '\u00ed';
		macTable[147] = '\u00ec';
		macTable[148] = '\u00ee';
		macTable[149] = '\u00ef';
		macTable[150] = '\u00f1';
		macTable[151] = '\u00f3';
		macTable[152] = '\u00f2';
		macTable[153] = '\u00f4';
		macTable[154] = '\u00f6';
		macTable[155] = '\u00f5';
		macTable[156] = '\u00fa';
		macTable[157] = '\u00f9';
		macTable[158] = '\u00fb';
		macTable[159] = '\u00fc';
		macTable[160] = '\u2020';
		macTable[161] = '\u00b0';
		macTable[162] = '\u00a2';
		macTable[163] = '\u00a3';
		macTable[164] = '\u00a7';
		macTable[165] = '\u2022';
		macTable[166] = '\u00b6';
		macTable[167] = '\u00df';
		macTable[168] = '\u00ae';
		macTable[169] = '\u00a9';
		macTable[170] = '\u2122';
		macTable[171] = '\u00b4';
		macTable[172] = '\u00a8';
		macTable[173] = '\u2260';
		macTable[174] = '\u00c6';
		macTable[175] = '\u00d8';
		macTable[176] = '\u221e';
		macTable[177] = '\u00b1';
		macTable[178] = '\u2264';
		macTable[179] = '\u2265';
		macTable[180] = '\u00a5';
		macTable[181] = '\u00b5';
		macTable[182] = '\u2202';
		macTable[183] = '\u2211';
		macTable[184] = '\u220f';
		macTable[185] = '\u03c0';
		macTable[186] = '\u222b';
		macTable[187] = '\u00aa';
		macTable[188] = '\u00ba';
		macTable[189] = '\u03a9';
		macTable[190] = '\u00e6';
		macTable[191] = '\u00f8';
		macTable[192] = '\u00bf';
		macTable[193] = '\u00a1';
		macTable[194] = '\u00ac';
		macTable[195] = '\u221a';
		macTable[196] = '\u0192';
		macTable[197] = '\u2248';
		macTable[198] = '\u2206';
		macTable[199] = '\u00ab';
		macTable[200] = '\u00bb';
		macTable[201] = '\u2026';
		macTable[202] = '\u00a0';
		macTable[203] = '\u00c0';
		macTable[204] = '\u00c3';
		macTable[205] = '\u00d5';
		macTable[206] = '\u0152';
		macTable[207] = '\u0153';
		macTable[208] = '\u2013';
		macTable[209] = '\u2014';
		macTable[210] = '\u201c';
		macTable[211] = '\u201d';
		macTable[212] = '\u2018';
		macTable[213] = '\u2019';
		macTable[214] = '\u00f7';
		macTable[215] = '\u25ca';
		macTable[216] = '\u00ff';
		macTable[217] = '\u0178';
		macTable[218] = '\u2044';
		macTable[219] = '\u20ac';
		macTable[220] = '\u2039';
		macTable[221] = '\u203a';
		macTable[222] = '\ufb01';
		macTable[223] = '\ufb02';
		macTable[224] = '\u2021';
		macTable[225] = '\u00b7';
		macTable[226] = '\u201a';
		macTable[227] = '\u201e';
		macTable[228] = '\u2030';
		macTable[229] = '\u00c2';
		macTable[230] = '\u00ca';
		macTable[231] = '\u00c1';
		macTable[232] = '\u00cb';
		macTable[233] = '\u00c8';
		macTable[234] = '\u00cd';
		macTable[235] = '\u00ce';
		macTable[236] = '\u00cf';
		macTable[237] = '\u00cc';
		macTable[238] = '\u00d3';
		macTable[239] = '\u00d4';
		macTable[240] = '\uf8ff';
		macTable[241] = '\u00d2';
		macTable[242] = '\u00da';
		macTable[243] = '\u00db';
		macTable[244] = '\u00d9';
		macTable[245] = '\u0131';
		macTable[246] = '\u02c6';
		macTable[247] = '\u02dc';
		macTable[248] = '\u00af';
		macTable[249] = '\u02d8';
		macTable[250] = '\u02d9';
		macTable[251] = '\u02da';
		macTable[252] = '\u00b8';
		macTable[253] = '\u02dd';
		macTable[254] = '\u02db';
		macTable[255] = '\u02c7';
		DEFAULT_CHARSET_TO_UGL = (new String[] { ".notdef", ".null", "CR",
				"space", "exclam", "quotedbl", "numbersign", "dollar",
				"percent", "ampersand", "quotesingle", "parenleft",
				"parenright", "asterisk", "plus", "comma", "hyphen", "period",
				"slash", "zero", "one", "two", "three", "four", "five", "six",
				"seven", "eight", "nine", "colon", "semicolon", "less",
				"equal", "greater", "question", "at", "A", "B", "C", "D", "E",
				"F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
				"R", "S", "T", "U", "V", "W", "X", "Y", "Z", "bracketleft",
				"backslash", "bracketright", "asciicircum", "underscore",
				"grave", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
				"l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w",
				"x", "y", "z", "braceleft", "bar", "braceright", "asciitilde",
				"Adieresis", "Aring", "Ccedilla", "Eacute", "Ntilde",
				"Odieresis", "Udieresis", "aacute", "agrave", "acircumflex",
				"adieresis", "atilde", "aring", "ccedilla", "eacute", "egrave",
				"ecircumflex", "edieresis", "iacute", "igrave", "icircumflex",
				"idieresis", "ntilde", "oacute", "ograve", "ocircumflex",
				"odieresis", "otilde", "uacute", "ugrave", "ucircumflex",
				"udieresis", "dagger", "degree", "cent", "sterling", "section",
				"bullet", "paragraph", "germandbls", "registered", "copyright",
				"trademark", "acute", "dieresis", "notequal", "AE", "Oslash",
				"infinity", "plusminus", "lessequal", "greaterequal", "yen",
				"mu1", "partialdiff", "summation", "product", "pi", "integral",
				"ordfeminine", "ordmasculine", "Ohm", "ae", "oslash",
				"questiondown", "exclamdown", "logicalnot", "radical",
				"florin", "approxequal", "increment", "guillemotleft",
				"guillemotright", "ellipsis", "nbspace", "Agrave", "Atilde",
				"Otilde", "OE", "oe", "endash", "emdash", "quotedblleft",
				"quotedblright", "quoteleft", "quoteright", "divide",
				"lozenge", "ydieresis", "Ydieresis", "fraction", "currency",
				"guilsinglleft", "guilsinglright", "fi", "fi", "fl", "fl",
				"daggerdbl", "periodcentered", "quotesinglbase",
				"quotedblbase", "perthousand", "Acircumflex", "Ecircumflex",
				"Aacute", "Edieresis", "Egrave", "Iacute", "Icircumflex",
				"Idieresis", "Igrave", "Oacute", "Ocircumflex", "applelogo",
				"Ograve", "Uacute", "Ucircumflex", "Ugrave", "dotlessi",
				"circumflex", "tilde", "overscore", "breve", "dotaccent",
				"ring", "cedilla", "hungarumlaut", "ogonek", "caron", "Lslash",
				"lslash", "Scaron", "scaron", "Zcaron", "zcaron", "brokenbar",
				"Eth", "eth", "Yacute", "yacute", "Thorn", "thorn", "minus",
				"multiply", "onesuperior", "twosuperior", "threesuperior",
				"onehalf", "onequarter", "threequarters", "franc", "Gbreve",
				"gbreve", "Idot", "Scedilla", "scedilla", "Cacute", "cacute",
				"Ccaron", "ccaron", "dmacron" });
	}
}