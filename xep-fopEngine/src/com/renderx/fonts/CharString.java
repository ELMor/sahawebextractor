/*
 * CharString - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

public class CharString {
	private static final int rCharString_default = 4330;

	byte[] encodedChar;

	byte[] decodedChar;

	String firstDelimiter;

	String secondDelimiter;

	byte type;

	CharString(byte[] is, String string, String string_0_) {
		init(is, string, string_0_, (byte) 1);
	}

	CharString(byte[] is, String string, String string_1_, byte i) {
		init(is, string, string_1_, i);
	}

	void init(byte[] is, String string, String string_2_, byte i) {
		encodedChar = is;
		firstDelimiter = string;
		secondDelimiter = string_2_;
		type = i;
		if (type == 1)
			decodeChar();
		else
			decodedChar = is;
	}

	private void decodeChar() {
		decodedChar = Type1Encryption.decrypt(4330, encodedChar);
	}

	public int[] composeGlyphs() {
		if (type == 1) {
			if (decodedChar[decodedChar.length - 2] == 12
					&& decodedChar[decodedChar.length - 1] == 6)
				return decodeCharString();
			return null;
		}
		if (decodedChar[decodedChar.length - 1] == 14)
			return decodeCharString();
		return null;
	}

	public int[] decodeCharString() {
		int i = 0;
		int[] is = new int[decodedChar.length];
		int[] is_3_ = new int[2];
		for (int i_4_ = 0; i_4_ < decodedChar.length; i_4_++)
			is[i_4_] = decodedChar[i_4_] & 0xff;
		int i_5_ = 0;
		int i_6_ = 0;
		while (i_5_ < is.length) {
			if (is[i_5_] >= 32 && is[i_5_] <= 246) {
				is_3_[i_6_++] = is[i_5_] - 139;
				i_5_++;
				i = 0;
			} else if (is[i_5_] >= 247 && is[i_5_] <= 250) {
				is_3_[i_6_++] = (is[i_5_] - 247) * 256 + is[i_5_ + 1] + 108;
				i_5_++;
				i_5_++;
				i = 0;
			} else if (is[i_5_] >= 251 && is[i_5_] <= 254) {
				is_3_[i_6_++] = -((is[i_5_] - 251) * 256) - is[i_5_ + 1] - 108;
				i_5_++;
				i_5_++;
				i = 0;
			} else if (is[i_5_] == 255) {
				is_3_[i_6_++] = (16777216 * is[i_5_ + 1] + 65536 * is[i_5_ + 2]
						+ 256 * is[i_5_ + 3] + is[i_5_ + 4]);
				i_5_++;
				i_5_++;
				i_5_++;
				i_5_++;
				i_5_++;
				i = 0;
			} else if (is[i_5_] < 32) {
				if (is[i_5_] == 12)
					i_5_++;
				i_5_++;
				i_6_ = 0;
				i++;
			}
			if (i_6_ == 2)
				i_6_ = 0;
		}
		if (i == 1)
			return is_3_;
		return null;
	}
}